<?php
include("token2.php");
$lastdate = '';
while (true) {
	process_one();
	$date = date('Y-m-d');
	$time = date('H:i');
	if($time== '21:00' and $lastdate != $date){
		$lastdate=$date;
		run_schedule();
	}
}
function process_one()
{
	echo "Reading...@ " . date('d-m-Y H:i:s') ."\n";
	$update_id  = 0;
	if (file_exists("last_update_id")) {
		$update_id = (int)file_get_contents("last_update_id");
	}
	$updates = get_updates($update_id);
	$i=0;
	echo "Looping...@ " . date('d-m-Y H:i:s') ."\n";
	foreach ($updates as $message)
	{
		$i++;
 		$update_id = process_message($message);
 		echo "Proses...@ " . $i ."\n";
	}
	file_put_contents("last_update_id", $update_id + 1);
	echo "write...@ " . date('d-m-Y H:i:s') ."\n";
}

function process_message($message)
{
    $updateid = $message["update_id"];
    if(isset($message['message'])){
		$message_data = $message["message"];
		if (isset($message_data["text"])) {
			$chatid = $message_data["chat"]["id"];
        	$message_id = $message_data["message_id"];
        	$text = $message_data["text"];
        	$date = $message_data["date"];

		}
	}else{
		$text = $message["callback_query"]['data'];
		$message_data = $message["callback_query"]['message'];
		$chatid = $message_data["chat"]["id"];
        $message_id = $message_data["message_id"];
        $date = $message_data["date"];
	}

	echo "input @". $chatid . "-". date('d-m-Y H:i:s',$date+18000) . " => " . $text . "\n";
	insert_log($chatid, "Request: ".$text);
	$response = create_response($text,$chatid);
	echo "respon @".$chatid." <= " . print_r($response) . "\n";
	send_reply($chatid, $message_id, $response);

    return $updateid;
}

function request_url($method)
{
	global $TOKEN;
	return "https://api.telegram.org/bot" . $TOKEN . "/". $method;
}

function get_updates($offset)
{
	$url = request_url("getUpdates")."?offset=".$offset;
	$aContext = array(
    	'http' => array(
        	'request_fulluri' => true,
        	'timeout' => 30,
    	),
	);
	$cxContext = stream_context_create($aContext);

	try {
		echo "Reading URL...@ " . date('d-m-Y H:i:s') ."\n";
     	$resp = file_get_contents($url, False, $cxContext);
        $result = json_decode($resp, true);
        echo "Finsih Read URL...@ " . date('d-m-Y H:i:s') ."\n";
        if ($result["ok"]=='true')
            return $result["result"];
	}
	catch (Exception $e) {
    	//echo $e->getMessage();
    	echo "Koneksi gagal.";
	}
	restore_error_handler();
 	return array();

}

function send_reply($chatid, $msgid, $respon)
{
	if(isset($respon['inline_keyboard'])){
		$replyMarkup=array(
			"inline_keyboard" => $respon['inline_keyboard'],
			"one_time_keyboard" => true, // Can be FALSE (hide keyboard after click)
			"resize_keyboard" => true
		);
	}elseif(isset($respon['keyboard'])){
		$replyMarkup=array(
			"keyboard" => $respon['keyboard'],
			"resize_keyboard" => true
		);
	}else{
		$replyMarkup = array(
    		'keyboard' => array(array())
		);
	}

	$encodedMarkup = json_encode($replyMarkup);
	$data = array(
        	'chat_id' => $chatid,
        	'reply_markup' => $encodedMarkup,
        	'parse_mode' => 'html',
        	'text'  => $respon['text']);

    $options = array(
    	'http' => array(
        	'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        	'method'  => 'POST',
        	'content' => http_build_query($data),
    	),
    );
    $context  = stream_context_create($options);
    $ch = curl_init(request_url('sendMessage'));
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);
/*
	print_r($context);
    $result = file_get_contents(request_url('sendMessage'), false, $context);
    print_r($result);
    */
}

function create_response($text, $chatid)
{
	$text = str_replace("_","#",$text);
	$input = explode("#",strtolower($text));

   switch ($input[0]) {
    case "register":
    	$username = isset($input[1]) ? addslashes($input[1]) : '';
    	$password = isset($input[2]) ? addslashes($input[2]) : '';
    	if($username=='')
        	$hasil = array('text'=>"Register gagal. Gunakan format : register#username#password");
        else{
			if(!user_exist($username,$password))
				$hasil = array('text'=>"Register gagal. Username atau password salah.");
			else{
				register_user($username, $chatid);
				$hasil = array('text'=>"Registrasi user berhasil.");
			}
		}
        break;
    case "beo":
    	if(chatid_exist($chatid))
    		$hasil = getbeo($chatid);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
        break;
    case "displaybeo":
    	if(chatid_exist($chatid))
    		$hasil = displaybeo($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "/showbeo":
    	if(chatid_exist($chatid))
    		$hasil = showbeo($input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "setujubeo":
    	if(chatid_exist($chatid))
    		$hasil = setujubeo($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "tolakbeo":
    	if(chatid_exist($chatid))
    		$hasil = tolakbeo($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "rejectbeo":
    	$id= isset($input[1]) ? addslashes($input[1]) : '';
    	$alasan = isset($input[2]) ? addslashes($input[2]) : '';

    	if($input[1]==""){
			$hasil = array('text'=>"ID BEO belum anda ketik. contoh rejectbeo#999#alasan reject");
		}

		if($input[2]==""){
			$hasil = array('text'=>"Alasan Belum Diisi. contoh rejectbeo#999#alasan reject");
		}

    	if(chatid_exist($chatid))
				$hasil = alasanrejectbeo($chatid, $input[1], $input[2]);
		else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
        break;
    case "setujupos":
    	if(chatid_exist($chatid))
    		$hasil = setujupos($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "tolakpos":
    	if(chatid_exist($chatid))
    		$hasil = tolakpos($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
	case "setujueditbeo":
    	if(chatid_exist($chatid))
    		$hasil = setujueditbeo($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "tolakeditbeo":
    	if(chatid_exist($chatid))
    		$hasil = tolakeditbeo($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "rejectpos":
    	$id= isset($input[1]) ? addslashes($input[1]) : '';
    	$alasan = isset($input[2]) ? addslashes($input[2]) : '';

    	if(chatid_exist($chatid))
				$hasil = alasanrejectpos($chatid, $input[1], $input[2]);
		else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
        break;
    case "setujuregist":
    	if(chatid_exist($chatid))
    		$hasil = setujuregist($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "tolakregist":
    	if(chatid_exist($chatid))
    		$hasil = tolakregist($chatid, $input[1]);
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "rejectreg":
    	$id= isset($input[1]) ? addslashes($input[1]) : '';
    	$alasan = isset($input[2]) ? addslashes($input[2]) : '';

    	if(chatid_exist($chatid))
				$hasil = alasanrejectreg($chatid, $input[1], $input[2]);
		else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
        break;
    case "report daily":
    	if(chatid_exist($chatid)){
    		if(cek_trusty($chatid, 'report daily')){
    			if(isset($input[1])){
					$hasil = reportdaily($input[1]);
				}else{
					$hasil = reportdaily(0);
				}
			}else
				$hasil = array('text'=>"Anda tidak berhak atas perintah ini.");
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "channel daily":
    	if(chatid_exist($chatid)){
    		if(cek_trusty($chatid, 'channel daily')){
    			if(isset($input[1])){
					$hasil = channeldaily($input[1]);
				}else{
					$hasil = channeldaily(0);
				}
			}else
				$hasil = array('text'=>"Anda tidak berhak atas perintah ini.");
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "channel monthly":
    	if(chatid_exist($chatid)){
    		if(cek_trusty($chatid, 'channel monthly')){
    			if(isset($input[1])){
					$hasil = channelmonthly($input[1]);
				}else{
					$hasil = channelmonthly(0);
				}
			}else
				$hasil = array('text'=>"Anda tidak berhak atas perintah ini.");
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "guest list":
    	if(chatid_exist($chatid)){
    		if(cek_trusty($chatid, 'guest list')){
    			if(isset($input[1])){
					$hasil = guestlist($input[1]);
				}else{
					$hasil = guestlist(0);
				}
			}else
				$hasil = array('text'=>"Anda tidak berhak atas perintah ini.");
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
     case "report monthly":
    	if(chatid_exist($chatid)){
    		if(cek_trusty($chatid, 'report monthly')){
				if(isset($input[1])){
					$hasil = reportmonthly($input[1]);
				}else{
					$hasil = reportmonthly(0);
				}
			}else
				$hasil = array('text'=>"Anda tidak berhak atas perintah ini.");
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "kirimemail":
					$id=$chatid;
					$ch = curl_init ('http://192.168.90.100/natura/index.php/transaksi/reservasi/email_telebot/'.$id);
					curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
					$urls = curl_exec ($ch);
    	break;
    case "ping":
    	if(chatid_exist($chatid))
    		$hasil = array('text'=>"Oke, Anda terhubung server BFM.");
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;

    case "pong":
    	if(chatid_exist($chatid))
    		$hasil = array('text'=>"Oke, Anda terhubung server BFM.");
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;

    case "menu":
    	if(chatid_exist($chatid)){
			$keyboard = array(array("Menu Sales", "Menu Reservasi"));
			$hasil = array('text'=>"Silahkan pilih Menu : 1. Sales untuk Detail Report Sales dan 2.Menu Report Reservasi untuk melihat Guest List.", 'keyboard'=>$keyboard);
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");

        break;
    case "/menu":
    	if(chatid_exist($chatid)){
			$keyboard = array(array("Menu Sales", "Menu Reservasi"));
			$hasil = array('text'=>"Silahkan pilih Menu : 1. Sales untuk Detail Report Sales dan 2.Menu Report Reservasi untuk melihat Guest List.", 'keyboard'=>$keyboard);
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");

        break;
    case "menu sales":
    	if(chatid_exist($chatid)){
			/*$keyboard = array(array("Report Daily", "Report Daily#-1"),
								array("Report Monthly","Report Monthly#-1"),
								array("Channel Daily","Channel Daily#-1"),
								array("Channel Monthly","Channel Monthly#-1"),
								array("menu")
						);*/
			$keyboard = array(array("Report Daily", "Report Daily#-1"),
								array("Report Monthly","Report Monthly#-1"),
								array("menu")
						);
			$hasil = array('text'=>"Silahkan pilih salah satu detail menu sales.", 'keyboard'=>$keyboard);
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");

        break;
    case "menu beo":
    	if(chatid_exist($chatid)){
			$keyboard = array(array("Guest List", "Guest List#+1"));
			$hasil = array('text'=>"Menu Guest List.", 'keyboard'=>$keyboard);
		}else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    case "/start":
    	if(chatid_exist($chatid))
    		$hasil = array('text'=>"Selamat Datang Di Telebot NPM, Kami siap membantu.");
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
    	break;
    default:
    	if(chatid_exist($chatid))
    		$hasil = array('text'=>"Perintah tidak dikenal.");
    	else
    		$hasil = array('text'=>"Anda Belum terdaftar.");
   		break;
   }
   return $hasil;
}

function user_exist($username,$password){
	$conn = connect();
	$sql = "select * from user where username='$username' and password=md5('$password') ";
	$result = $conn->query($sql);
	if ($result->num_rows > 0){
		mysqli_close($conn);
		return true;
	}
	else{
		mysqli_close($conn);
		return false;
	}
}

function cek_trusty($chatid, $menuname){
	$conn = connect();
	$sql = "SELECT l.`view` FROM `user` u INNER JOIN userlevelpermissions l ON u.`UserLevel`=l.`userlevelid` AND l.`tablename`='$menuname'
				WHERE telegramid='$chatid'";
	$rec = $conn->query($sql);
	mysqli_close($conn);
	if($rec->num_rows>0){
		$row = $rec->fetch_assoc();
		if($row['view']=='Y')
			return true;
		else
			return false;
	}else
		return false;
}

function chatid_exist($chatid){
	$conn = connect();
	$sql = "select * from user where TelegramID='$chatid' ";
	$result = $conn->query($sql);
	if ($result->num_rows > 0){
		mysqli_close($conn);
		return true;
	}
	else{
		mysqli_close($conn);
		return false;
	}
}

function register_user($username, $chatid){
	$conn = connect();
	$sql = "update user set TelegramID = '$chatid' where username='$username'";
	$result = $conn->query($sql);
	$cnt = $result->num_rows;
	if ($cnt > 0){
		insert_log($chatid, "Berhasil register user ='$username'");
		mysqli_close($conn);
		return true;
	}else{
		insert_log($chatid, "Gagal register user ='$username'");
		mysqli_close($conn);
		return false;
	}
}

function getbeo($chatid){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT CONCAT(r.id,'#', r.GroupName) as beoid, r.id, r.GroupName, r.Status, r.Status_approve
			FROM trans_reservasi r INNER JOIN salesman s ON r.`Sales_In_Charge`=s.`KdSalesman`
			INNER JOIN supervisor p ON s.`KdSupervisor`=p.`KdSupervisor`
			INNER JOIN `user` u ON p.`UserName`= u.`UserName`
			WHERE r.TglDokumen>='$date' and u.`TelegramID`='$chatid'";
	$rec = $conn->query($sql);
	$i=0;

	while($row = $rec->fetch_assoc()){
        //$result[$i][0] = 'bp#'.$row["beoid"];
        $result[$i][0] = array('text' => $row['GroupName'], 'callback_data' => 'displaybeo#'.$row['id']);
        $i++;
    }
    mysqli_close($conn);
     return array('text'=>'Pilih BEO', 'inline_keyboard'=>$result);
}

function displaybeo($chatid, $id){
	$conn = connect();

	$sql = "SELECT NoDokumen, TglDokumen, t.`Nama`, r.GroupName, r.`Jam_Kedatangan` , r.Contact, s.`NamaSalesman`, r.`desc_front_office`,  r.`desc_f_b_kitchen`,
			r.`desc_black_aye`, r.`desc_oemah_herborist`, r.`desc_engineering_edp`, r.`desc_accounting`, r.`dp`, r.`Total`, r.Participants
			FROM trans_reservasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel`
			INNER JOIN salesman s ON r.`Sales_In_Charge`=s.`KdSalesman`
			WHERE id='$id'";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();
	$nodokumen = $row['NoDokumen'];
	$company = $row['Nama'];
	$groupname = $row['GroupName'];
	$participants = $row['Participants'];
	$tgldokumen = $row['TglDokumen'];
	$jam = $row['Jam_Kedatangan'];
	$salesman = $row['NamaSalesman'];
	$front_office = $row['desc_front_office'];
	$kitchen = $row['desc_f_b_kitchen'];
	$black_eye = $row['desc_black_aye'];
	$oh = $row['desc_oemah_herborist'];
	$acct = $row['desc_accounting'];
	$total = $row['Total'];
	$result = "<b>BANQUET EVENT ORDER</b>
				<b>No: $nodokumen </b>

				Company		 : $company
				Group  		 : $groupname
				Participants : $participants
				Date		 : $tgldokumen
				Time		 : $jam
				Salesman	 : $salesman
				";
	if(strlen($front_office)>1){
		$result .= "
					<b>Front Office</b>
					$front_office";
	}

	if(strlen($kitchen)>1){
		$result .= "
					<b>F&B Kitchen</b>
					$kitchen";
	}

	if(strlen($black_eye)>1){
		$result .= "
					<b>Black Eye Coffee</b>
					$black_eye";
	}

	if(strlen($oh)>1){
		$result .= "
					<b>Oemah Herborist</b>
					$oh";
	}

	if(strlen($acct)>1){
		$result .= "
					<b>Oemah Herborist</b>
					$acct";
	}

	$result .= "
				<b>Total : $total</b>";

	$keyboard[0][0] = array('text'=>'Setuju', 'callback_data' => 'Setujubeo#'.$id);
	$keyboard[0][1] = array('text'=>'Tolak', 'callback_data' => 'Tolakbeo#'.$id);

	insert_log($chatid, "Berhasil Approve Reservasi dengan ID='$id'");
    mysqli_close($conn);
    return array('text'=>$result,'inline_keyboard'=>$keyboard);
}

function setujubeo($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');

	//cek apakah BEO udah di reject?
	$q="
	   SELECT * FROM trans_reservasi a WHERE a.`id`='$id' AND a.`status_reject`='0';
	   ";
	$cek = $conn->query($q);


	if($cek->num_rows>0){

			//cek apakah sudah approve sebelumnya?
			$p="
		    SELECT * FROM trans_reservasi a WHERE a.`id`='$id' AND a.`status_approve`='0';
		    ";
		    $ceks = $conn->query($p);
		    if($ceks->num_rows>0){

					create_response('kirimemail', $id);

			}else{
				$q="
			    SELECT * FROM trans_reservasi a WHERE a.`id`='$id';
			    ";
			    $cek = $conn->query($q);
			    $row = $cek->fetch_assoc();
				mysqli_close($conn);
				return array('text'=>'Maaf BEO '.$row['NoDokumen'].' Sudah Pernah Di Approve.');
			}

	}else{
		$q="
	    SELECT * FROM trans_reservasi a WHERE a.`id`='$id';
	    ";
	    $cek = $conn->query($q);
	    $row = $cek->fetch_assoc();
		mysqli_close($conn);
		return array('text'=>'Maaf BEO '.$row['NoDokumen'].' Sudah Di Reject.');
	}

}

function setujupos($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT NoDokumen, Type, NoStiker, Status_Request FROM edit_log WHERE id_request='$id' ";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();

	$nostruk = $row['NoDokumen'];
	$type = $row['Type'];
	$nostiker = $row['NoStiker'];
	$status = $row['Status_Request'];

	if($status==2 OR $status==3){
		$notesetuju ='Gagal!! Transaksi sudah pernah di setuju/ditolak.';
	}else{
		if($type=='hapus'){
			$sql1 = "update transaksi_header set status=2 where nostruk='$nostruk'";
			$sql2 = "update transaksi_detail set status=2 where nostruk='$nostruk'";
			$sql3 = "delete from transaksi_detail_voucher where nostruk='$nostruk'";
			$conn->query($sql1);
			$conn->query($sql2);
			$conn->query($sql3);

			$note = 'INFO : Edit Transaksi POS No. Struk '.$nostruk.' dengan Tipe Hapus Struk telah Approve';
		}elseif($type=='stiker'){

			$sql1 = "update transaksi_header set kdagent='$nostiker' where nostruk='$nostruk'";
			$sql2 = "update transaksi_detail set kdagent='$nostiker' where nostruk='$nostruk'";
			$conn->query($sql1);
			$conn->query($sql2);

			$note = 'INFO : Edit Transaksi POS No. Struk '.$nostruk.' dengan Tipe Ubah No. Struk telah Approve';

		}

		$notesetuju='Approve No. Struk '.$nostruk.' Berhasil.';

		$sql = "select UserName from user where TelegramID='$chatid' limit 1";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$username = $row['UserName'];

		$sqlsx = "SELECT t.`NoDokumen`, u.`TelegramID`
				FROM edit_log t INNER JOIN `user` u ON t.`Add_User_Request`=u.`UserName`
				WHERE t.`id_request`=$id";
		$recsx = $conn->query($sqlsx);
		$rowsx = $recsx->fetch_assoc();
		$userinputchatid = $rowsx['TelegramID'];

		//update edit_log
		$dates = date('Y-m-d h:i:s');
		$sql_approve = "update edit_log set Status_Request='2',Approve_Request='$username',Approve_Date='$dates',Status_Approve='1',Perangkat='2' WHERE id_request='$id'";
		$conn->query($sql_approve);

	}
	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$note));
	return array('text'=>$notesetuju);
}

function setujueditbeo($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT NoDokumen FROM trans_reservasi WHERE id='$id' ";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();
	$nodokumen = $row['NoDokumen'];

		$notesetuju='BEO '.$nodokumen.' Berhasil Di Izinkan Edit.';
		$note='BEO '.$nodokumen.' Di Izinkan Edit.';

		$sql = "select UserName from user where TelegramID='$chatid' limit 1";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$username = $row['UserName'];

		$sql = "select c.`NamaSupervisor` AS employee_name,
					  c.`email` AS email,
					  c.`UserName`,
					  d.`TelegramID`
					FROM
					  `trans_reservasi` a
					  INNER JOIN salesman b
					    ON a.`Sales_In_Charge` = b.`KdSalesman`
					  INNER JOIN supervisor c
					    ON b.`KdSupervisor` = c.`KdSupervisor`
					  INNER JOIN `user` d
					  ON d.`UserName` = c.`UserName`
					WHERE a.id = '$id'";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$userinputchatid = $row['TelegramID'];

		//update
		$dates = date('Y-m-d h:i:s');
		$sql_approve = "update trans_reservasi set status='0',status_approve='0',tgl_approve='',
		                status_reject='0',tgl_reject='',ket_reject='',edit_request='0' WHERE id='$id'";
		$conn->query($sql_approve);

	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$note));
	return array('text'=>$notesetuju);
}

function tolakeditbeo($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT NoDokumen FROM trans_reservasi WHERE id='$id' ";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();
	$nodokumen = $row['NoDokumen'];

		$notesetuju='BEO '.$nodokumen.' Tidak Di Izinkan Edit.';
		$note='BEO '.$nodokumen.' Tidak Di Izinkan Edit.';

		$sql = "select UserName from user where TelegramID='$chatid' limit 1";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$username = $row['UserName'];

		$sql = "c.`NamaSupervisor` AS employee_name,
					  c.`email` AS email,
					  c.`UserName`,
					  d.`TelegramID`
					FROM
					  `trans_reservasi` a
					  INNER JOIN salesman b
					    ON a.`Sales_In_Charge` = b.`KdSalesman`
					  INNER JOIN supervisor c
					    ON b.`KdSupervisor` = c.`KdSupervisor`
					  INNER JOIN `user` d
					  ON d.`UserName` = c.`UserName`
					WHERE a.id = '".$NoDokumen."';  ";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$userinputchatid = $row['TelegramID'];

	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$note));
	return array('text'=>$notesetuju);
}

function setujuregist($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT NoDokumen, Type, NoStiker, Tanggal, KdTravel, Status_Request FROM edit_log WHERE id_request='$id' ";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();

	$nostruk = $row['NoDokumen'];
	$type = $row['Type'];
	$nostiker = $row['NoStiker'];
	$tanggal = $row['Tanggal'];
	$kdtravel = $row['KdTravel'];
	$status = $row['Status_Request'];

	if($status==2 OR $status==3){
		$notesetuju ='Gagal!! Transaksi sudah pernah di setuju/ditolak.';
	}else{
		if($type=='tanggal'){
			$sql1 = "update register set Tanggal='$tanggal' where KdRegistrasi='$nostruk'";
			$conn->query($sql1);

			$note = 'INFO : Edit Transaksi Register No. Register '.$nostruk.' dengan Tipe Ubah Tanggal Struk telah Approve';
		}elseif($type=='travel'){

			$sql1 = "update register set KdTravel='$kdtravel' where KdRegistrasi='$nostruk'";
			$conn->query($sql1);

			$note = 'INFO : Edit Transaksi Register No. Register '.$nostruk.' dengan Tipe Ubah Travel telah Approve';

		}

		$notesetuju='Approve No. Register '.$nostruk.' Berhasil.';

		$sql = "select UserName from user where TelegramID='$chatid' limit 1";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$username = $row['UserName'];

		$sql = "SELECT t.`NoDokumen`, u.`TelegramID`
				FROM edit_log t INNER JOIN `user` u ON t.`Add_User_Request`=u.`UserName`
				WHERE t.`id_request`=$id";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$userinputchatid = $row['TelegramID'];

		//update edit_log
		$dates = date('Y-m-d h:i:s');
		$sql_approve = "update edit_log set Status_Request='2',Approve_Request='$username',Approve_Date='$dates',Status_Approve='1',Perangkat='2' WHERE id_request='$id'";
		$conn->query($sql_approve);

	}
	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$note));
	return array('text'=>$notesetuju);
}

function tolakbeo($chatid, $id){
		$conn = connect();
		//cek apakah BEO udah di approve?
		$q="
		   SELECT * FROM trans_reservasi a WHERE a.`id`='$id' AND a.`status_approve`='0';
		   ";
		$cek = $conn->query($q);
		if($cek->num_rows>0){
			//cek apakah sudah reject sebelumnya?
			$p="
		    SELECT * FROM trans_reservasi a WHERE a.`id`='$id' AND a.`status_reject`='0';
		    ";
		    $ceks = $conn->query($p);
		    if($ceks->num_rows>0){
				$status = "
				<b>INFO</b>
				Kirim Alasan Reject BEO ID $id
				format : rejectbeo#id#alasan reject
				contoh :
				rejectbeo#$id#Tour Only
				lalu KIRIM.";
				mysqli_close($conn);
				return array('text'=>$status);
			}else{
				$q="
			    SELECT * FROM trans_reservasi a WHERE a.`id`='$id';
			    ";
			    $cek = $conn->query($q);
			    $row = $cek->fetch_assoc();
			    mysqli_close($conn);
				return array('text'=>'Maaf BEO '.$row['NoDokumen'].' Sudah Pernah Di Reject.');
			}
		}else{
			$q="
		    SELECT * FROM trans_reservasi a WHERE a.`id`='$id';
		    ";
		    $cek = $conn->query($q);
		    $row = $cek->fetch_assoc();
		    mysqli_close($conn);
			return array('text'=>'Maaf BEO '.$row['NoDokumen'].' Sudah Di Approve.');
		}

}

function tolakpos($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT NoDokumen, Type, NoStiker, Status_Request FROM edit_log WHERE id_request='$id' ";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();

	$nostruk = $row['NoDokumen'];
	$type = $row['Type'];
	$nostiker = $row['NoStiker'];
	$status = $row['Status_Request'];

	if($status==2 OR $status==3){
		$note ='Gagal!! Transaksi sudah pernah di setuju/ditolak.';
	}else{
		$note = "
				<b>INFO</b>
				Kirim Alasan Reject POS ID $id
				format : rejectpos#id#alasan
				contoh :
				rejectpos#$id#kadaluarsa
				lalu KIRIM.";
	}
	mysqli_close($conn);
	return array('text'=>$note);
}

function tolakregist($chatid, $id){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT NoDokumen, Type, NoStiker, Status_Request FROM edit_log WHERE id_request='$id' ";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();

	$nostruk = $row['NoDokumen'];
	$type = $row['Type'];
	$nostiker = $row['NoStiker'];
	$status = $row['Status_Request'];

	if($status==2 OR $status==3){
		$note ='Gagal!! Transaksi sudah pernah di setuju/ditolak.';
	}else{
		$note = "
				<b>INFO</b>
				Kirim Alasan Reject POS ID $id
				format : rejectreg#id#alasan
				contoh :
				rejectreg#$id#Milenium
				lalu KIRIM.";
	}
	mysqli_close($conn);
	return array('text'=>$note);
}

function alasanrejectbeo($chatid, $id, $alasan){
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "update trans_reservasi r INNER JOIN salesman s ON r.`Sales_In_Charge`=s.`KdSalesman`
			INNER JOIN supervisor p ON s.`KdSupervisor`=p.`KdSupervisor`
			INNER JOIN `user` u ON p.`UserName`= u.`UserName`
			set r.status=0, r.status_reject=1, r.tgl_reject='$date', r.ket_reject='$alasan'
			WHERE r.status=1 AND r.id='$id'  and u.`TelegramID`='$chatid'";
	$rec = $conn->query($sql);
	$cnt = $conn->affected_rows;

	if($cnt>0){
		$status = "Berhasil Reject";
		insert_log($chatid, "Berhasil tolak reservasi");
	}
	else{
		$status = 'Gagal Reject';
		insert_log($chatid, "Gagal tolak reservasi");
	}

	$sql = "SELECT t.`NoDokumen`, GroupName, u.`TelegramID`
			FROM trans_reservasi t INNER JOIN `user` u ON t.`AddUser`=u.`UserName`
			WHERE t.`id`=$id";

	$rec = $conn->query($sql);

	if(!empty($rec))
	{
	$row = $rec->fetch_assoc();
	$userinputchatid = $row['TelegramID'];
	$notetolak = 'BEO ' .$row['NoDokumen'].'-'.$row['GroupName'].' telah di reject dengan Alasan '.$alasan;
	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$notetolak));
	}

	return array('text'=>$status);
}

function alasanrejectpos($chatid, $id, $alasan){
	$conn = connect();

	    $sql = "select UserName from user where TelegramID='$chatid' limit 1";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$username = $row['UserName'];

	$date =  date('Y-m-d h:i:s');
	$sql_reject = "update edit_log set Status_Request='3',Reject_Request='$username',Reject_Date='$date',Status_Reject='1',Reject_Remark='$alasan',Perangkat='2' WHERE id_request='$id'";
	$conn->query($sql_reject);

	$sql = "SELECT t.`NoDokumen`, u.`TelegramID`
			FROM edit_log t INNER JOIN `user` u ON t.`Add_User_Request`=u.`UserName`
			WHERE t.`id_request`=$id";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();
	$userinputchatid = $row['TelegramID'];
	$nostruk = $row['NoDokumen'];
	$notetolak = 'INFO : Edit Transaksi POS No. Struk '.$nostruk.' Telah Di Reject Dengan Alasan '.$alasan;
	$status = 'Edit Transaksi POS No. Struk '.$nostruk.' Berhasil Di Reject';
	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$notetolak));
	return array('text'=>$status);
}

function alasanrejectreg($chatid, $id, $alasan){
	$conn = connect();

	    $sql = "select UserName from user where TelegramID='$chatid' limit 1";
		$rec = $conn->query($sql);
		$row = $rec->fetch_assoc();
		$username = $row['UserName'];

	$date =  date('Y-m-d h:i:s');
	$sql_reject = "update edit_log set Status_Request='3',Reject_Request='$username',Reject_Date='$date',Status_Reject='1',Reject_Remark='$alasan',Perangkat='2' WHERE id_request='$id'";
	$conn->query($sql_reject);

	$sql = "SELECT t.`NoDokumen`, u.`TelegramID`
			FROM edit_log t INNER JOIN `user` u ON t.`Add_User_Request`=u.`UserName`
			WHERE t.`id_request`=$id";
	$rec = $conn->query($sql);
	$row = $rec->fetch_assoc();
	$userinputchatid = $row['TelegramID'];
	$nostruk = $row['NoDokumen'];
	$notetolak = 'INFO : Edit Transaksi Register No. Register '.$nostruk.' Telah Di Reject Dengan Alasan '.$alasan;
	$status = 'Edit Transaksi Register No. Register '.$nostruk.' Berhasil Di Reject';
	mysqli_close($conn);
	send_reply($userinputchatid, '', array('text'=>$notetolak));
	return array('text'=>$status);
}

function reportdaily($interval){
	$conn = connect();
	$date = date('Y-m-d');
	$sqlsales = "SELECT s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore`
				WHERE h.`Tanggal` = DATE_ADD('$date', INTERVAL $interval DAY)
				  AND h.`Status` = 1
				GROUP BY s.`KodeStore`
				";

	$sqlsalesfood = "SELECT s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN masterbarang m ON m.`PCode`=d.`PCode`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore`
				WHERE h.`Tanggal` = DATE_ADD('$date', INTERVAL $interval DAY)
				  AND h.`Status` = 1 AND m.`KdGroupBarang`='2'
				GROUP BY s.`KodeStore`
				";

	$sqlsalesbaverage = "SELECT s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN masterbarang m ON m.`PCode`=d.`PCode`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore`
				WHERE h.`Tanggal` = DATE_ADD('$date', INTERVAL $interval DAY)
				  AND h.`Status` = 1 AND m.`KdGroupBarang`='1'
				GROUP BY s.`KodeStore`
				";

	$qrykomisi = "SELECT sum(`finance_komisi_header`.Total) as komisi
            		FROM `finance_komisi_header`
            		WHERE `finance_komisi_header`.TglTransaksi = DATE_ADD('$date', INTERVAL $interval DAY)";

    if($interval==0){
    	$rptdate = $date = date('d-m-Y');
    }else{
    	$rptdate = $date = date('d')+$interval."-".date('m-Y');
    }

	$rec = $conn->query($sqlsales);
	$textsales ='';
	$totsales = 0;
	while ($row = $rec->fetch_assoc()){
		$namastore = $row['NamaStore'];
		$sales = $row['Sales'];
		$totsales += $sales;
		$textsales .= "<pre>" . str_pad($namastore,15,' ') . str_pad(number_format($sales,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>". "\n";
	}

	$textdetail = "<pre>Detail : </pre>";

	$rec = $conn->query($sqlsalesfood);
	$textsalesfood ='';
	$totsalesfood = 0;
	while ($row = $rec->fetch_assoc()){
		$sales = $row['Sales'];
		$totsalesfood += $sales;
		$textsalesfood .= "<pre>" . str_pad('Food',15,' ') . str_pad(number_format($sales,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";
	}

	$rec = $conn->query($sqlsalesbaverage);
	$textsalesbaverage ='';
	$totsalesbaverage = 0;
	while ($row = $rec->fetch_assoc()){
		$sales = $row['Sales'];
		$totsalesbaverage += $sales;
		$textsalesbaverage .= "<pre>" . str_pad('Baverage',15,' ') . str_pad(number_format($sales,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";
	}

	$rec = $conn->query($qrykomisi);
	$row = $rec->fetch_assoc();
	$komisi = $row['komisi'];
	$textkomisi = "<pre>" . str_pad('Komisi',15,' '). str_pad(number_format($komisi,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";

	$omzet = $totsales;
	$textomzet = "<pre>" . str_pad('Omzet',15,' '). str_pad(number_format($omzet,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";

	$rpt = "
<b>Daily Report $rptdate</b>

$textsales
$textdetail
$textsalesfood
$textsalesbaverage

$textkomisi
$textomzet

";
	mysqli_close($conn);
	return array('text'=>$rpt);
}

function channeldaily($interval){
	$conn = connect();
	$date = date('Y-m-d');
	$sqltiket = "SELECT t.add_date, IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','HC0003', res.KdSupervisor) AS KdSpv,
				IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','TourDesk & Frnlc-Victor', res.NamaSupervisor) AS NamaSpv,
 				SUM(t.harga) AS NilaiTiket
				FROM
				  ticket t  LEFT JOIN
				  (
					SELECT rk.`TglKonfirmasi` AS Tanggal, rs.`NoSticker`, sm.`KdSupervisor`, spv.`NamaSupervisor` FROM trans_reservasi r INNER JOIN trans_reservasi_konfirmasi rk ON r.`NoDokumen`=rk.`NoReservasi`
					INNER JOIN trans_reservasi_konfirmasi_sticker rs ON rk.`NoDokumen`=rs.`NoDokumen`
					INNER JOIN salesman sm ON r.`Sales_In_Charge`=sm.`KdSalesman`
					INNER JOIN supervisor spv ON sm.`KdSupervisor`=spv.`KdSupervisor`
				  ) res ON t.`add_date`=res.Tanggal AND t.`noidentitas`=res.NoSticker
				  LEFT JOIN `register` reg ON t.`add_date` = reg.`Tanggal` AND t.`noidentitas`=reg.`NoStiker`
				WHERE t.add_date = DATE_ADD('$date', INTERVAL $interval DAY)
				  AND t.noidentitas != '1234'
				  AND t.status = 1
				  GROUP BY NamaSpv";

    $sqlsales = "SELECT IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','HC0003', res.KdSupervisor) AS KdSpv,
					  IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','TourDesk & Frnlc-Victor', res.NamaSupervisor) AS NamaSpv,
					  SUM(
					    IF(
					      d.`Service_charge` > 0,
					      d.`Netto` * 1155 / 1000,
					      d.Netto
					    )
					  ) AS Sales
					FROM
					  transaksi_header h
					  INNER JOIN transaksi_detail d
					    ON h.`NoKassa` = d.`NoKassa`
					    AND h.`NoStruk` = d.`NoStruk`
					  LEFT JOIN
					  (
						SELECT rk.`TglKonfirmasi` AS Tanggal, rs.`NoSticker`, sm.`KdSupervisor`, spv.`NamaSupervisor` FROM trans_reservasi r INNER JOIN trans_reservasi_konfirmasi rk ON r.`NoDokumen`=rk.`NoReservasi`
						INNER JOIN trans_reservasi_konfirmasi_sticker rs ON rk.`NoDokumen`=rs.`NoDokumen`
						INNER JOIN salesman sm ON r.`Sales_In_Charge`=sm.`KdSalesman`
						INNER JOIN supervisor spv ON sm.`KdSupervisor`=spv.`KdSupervisor`
					  ) res ON h.`Tanggal`=res.Tanggal AND h.`KdAgent`=res.NoSticker
					  LEFT JOIN `register` reg ON h.`Tanggal` = reg.`Tanggal` AND h.`KdAgent`=reg.`NoStiker`
					WHERE h.`Tanggal` = DATE_ADD('$date', INTERVAL $interval DAY)
					  AND h.`Status` = 1
					GROUP BY NamaSpv";

	$sqlsunset = "SELECT IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','HC0003', res.KdSupervisor) AS KdSpv,
					  IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','TourDesk & Frnlc-Victor', res.NamaSupervisor) AS NamaSpv,
					  SUM(
					    IF(
					      d.`Service_charge` > 0,
					      d.`Netto` * 1155 / 1000,
					      d.Netto
					    )
					  ) AS Sales
					FROM
					  transaksi_header_sunset h
					  INNER JOIN transaksi_detail_sunset d
					    ON h.`NoKassa` = d.`NoKassa`
					    AND h.`NoStruk` = d.`NoStruk`
					  LEFT JOIN
					  (
						SELECT rk.`TglKonfirmasi` AS Tanggal, rs.`NoSticker`, sm.`KdSupervisor`, spv.`NamaSupervisor` FROM trans_reservasi r INNER JOIN trans_reservasi_konfirmasi rk ON r.`NoDokumen`=rk.`NoReservasi`
						INNER JOIN trans_reservasi_konfirmasi_sticker rs ON rk.`NoDokumen`=rs.`NoDokumen`
						INNER JOIN salesman sm ON r.`Sales_In_Charge`=sm.`KdSalesman`
						INNER JOIN supervisor spv ON sm.`KdSupervisor`=spv.`KdSupervisor`
					  ) res ON h.`Tanggal`=res.Tanggal AND h.`KdAgent`=res.NoSticker
					  LEFT JOIN `register` reg ON h.`Tanggal` = reg.`Tanggal` AND h.`KdAgent`=reg.`NoStiker`
					WHERE h.`Tanggal` = DATE_ADD('$date', INTERVAL $interval DAY)
					  AND h.`Status` = 1
					GROUP BY NamaSpv";

    $qrydistribusikopi = "SELECT h.`sidate`, SUM(d.`quantity`*d.`nettprice`)*1.1 AS nilai FROM salesinvoice h
    					INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
    					INNER JOIN deliveryorder o ON d.dono=o.dono
						INNER JOIN gudang g ON o.warehousecode=g.KdGudang
						INNER JOIN subdivisi s ON g.KdSubdivisi=s.KdSubdivisi
						WHERE sidate=DATE_ADD('$date', INTERVAL $interval DAY) AND h.`status`=1 AND s.kddivisi='2'";

	$qrydistribusioh = "SELECT h.`sidate`, SUM(d.`quantity`*d.`nettprice`)*1.1 AS nilai FROM salesinvoice h
    					INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
    					INNER JOIN deliveryorder o ON d.dono=o.dono
						INNER JOIN gudang g ON o.warehousecode=g.KdGudang
						INNER JOIN subdivisi s ON g.KdSubdivisi=s.KdSubdivisi
						WHERE sidate=DATE_ADD('$date', INTERVAL $interval DAY) AND h.`status`=1 AND s.kddivisi='1' and h.autoinvoice=0";

    $qryvoucher = "SELECT SUM(d.NilaiVoucher) AS pemakaian_voucher
                	FROM  transaksi_detail_voucher d inner join transaksi_header h on h.nostruk=d.nostruk
                    WHERE d.Tanggal = DATE_ADD('$date', INTERVAL $interval DAY) AND h.status='1 and d.Jenis<>5'";

	$qrykomisi = "SELECT sum(`finance_komisi_header`.Total) as komisi
	        		FROM `finance_komisi_header`
	        		WHERE `finance_komisi_header`.TglTransaksi = DATE_ADD('$date', INTERVAL $interval DAY)";

    $rec = $conn->query($sqlsales);
	$data = array();
	while ($row = $rec->fetch_assoc()){
		$kdchannel = $row['KdSpv'];
		$nama = $row['NamaSpv'];
		$sales = $row['Sales'];
		$data[$kdchannel]['NamaSpv'] = $nama;
		$data[$kdchannel]['Amt'] = $sales;
	}

	$rec = $conn->query($sqlsunset);
	while ($row = $rec->fetch_assoc()){
		$kdchannel = $row['KdSpv'];
		$nama = $row['NamaSpv'];
		$sales = $row['Sales'];
		$data[$kdchannel]['NamaSpv'] = $nama;
		$data[$kdchannel]['Amt'] += $sales;
	}

    $rec = $conn->query($sqltiket);
	while ($row = $rec->fetch_assoc()){
		$kdchannel = $row['KdSpv'];
		$nama = $row['NamaSpv'];
		$rptdate = $row['add_date'];
		$sales = $row['NilaiTiket'];
		$data[$kdchannel]['NamaSpv'] = $nama;
		$data[$kdchannel]['Amt'] += $sales;
	}


	$textrpt = "<b>Daily Channel Report $rptdate</b>\n\n";
	$omzet = 0;
	foreach($data as $key => $value){
		$nama = ($value['NamaSpv'] == null || $value['NamaSpv']=='') ? 'Non Channel' : $value['NamaSpv'];
		$nama = substr($nama, 0,14);
		$textrpt .= "<pre>" . str_pad($nama,15,' '). str_pad(number_format($value['Amt'],0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n";
		$omzet += $value['Amt'];
	}

	$tglawal = date('Y-m-d', strtotime($interval .' day', strtotime($date)));
	$tglakhir = $tglawal;
	$url = 'http://192.168.0.13/sss/update_npm.php?tgl1='.$tglawal.'&tgl2='.$tglakhir;
	$aContext = array(
    	'http' => array(
        	'request_fulluri' => true,
    	),
	);
	echo $url;
	$cxContext = stream_context_create($aContext);
	$distribusivci = 0;
	try {
     	$resp = file_get_contents($url, False, $cxContext);
        $result = json_decode($resp, true);

        foreach($result as $rec){
			$pcode = $rec['PCode'];
			$Qty = $rec['Qty'];
			$sqlharga = "SELECT Harga FROM groupharga_detail d INNER JOIN mapping_barang_vci m ON d.`PCode`=m.`PCode`
				WHERE GroupHargaID=1 AND m.`PCodeVCI`='$pcode'";
			$qryharga = $conn->query($sqlharga);
			$row = $qryharga->fetch_assoc();
			if(count($row)>0){
				$harga = $row['Harga']*1.1;
			}else{
				$harga = 0;
			}
			$distribusivci += $Qty * $harga;
		}
	}
	catch (Exception $e) {
    	//echo $e->getMessage();
    	echo "Koneksi gagal.";
	}

	$rec = $conn->query($qrydistribusioh);
	$row = $rec->fetch_assoc();
	$distribusioh = $row['nilai'];
	$textrpt .= "<pre>" . str_pad('Dist VCI',15,' '). str_pad(number_format($distribusivci+$distribusioh,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n";


	$rec = $conn->query($qrydistribusikopi);
	$row = $rec->fetch_assoc();
	$distribusikopi = $row['nilai'];
	$textrpt .= "<pre>" . str_pad('Dist Kopi',15,' '). str_pad(number_format($distribusikopi,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n\n";

	$rec = $conn->query($qryvoucher);
	$row = $rec->fetch_assoc();
	$voucher = $row['pemakaian_voucher'];
	$textrpt .= "<pre>" . str_pad('Voucher',15,' '). str_pad(number_format($voucher,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n";

	$omzet += $distribusivci+$distribusioh + $distribusikopi - $voucher;
	$textrpt .=  "<pre>" . str_pad('Omzet',15,' '). str_pad(number_format($omzet,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n\n";

	$rec = $conn->query($qrykomisi);
	$row = $rec->fetch_assoc();
	$komisi = $row['komisi'];
	$textrpt .= "<pre>" . str_pad('Komisi',15,' '). str_pad(number_format($komisi,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";

	mysqli_close($conn);
	//$textrpt = 'test';
	return array('text'=>$textrpt);
}

function channelmonthly($interval){
	$conn = connect();
	list($thn, $bln, $tgl) = explode('-',date('Y-m-d'));
	$bln = $bln*1;
	if($interval<0){
		$bln = $bln+$interval;
		if($bln<=0){
			$bln += 12;
			$thn -= 1;
		}
	}
	$sqltiket = "SELECT t.add_date, IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','HC0003', res.KdSupervisor) AS KdSpv,
				IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','TourDesk & Frnlc-Victor', res.NamaSupervisor) AS NamaSpv,
 				SUM(t.harga) AS NilaiTiket
				FROM
				  ticket t  LEFT JOIN
				  (
					SELECT rk.`TglKonfirmasi` AS Tanggal, rs.`NoSticker`, sm.`KdSupervisor`, spv.`NamaSupervisor` FROM trans_reservasi r INNER JOIN trans_reservasi_konfirmasi rk ON r.`NoDokumen`=rk.`NoReservasi`
					INNER JOIN trans_reservasi_konfirmasi_sticker rs ON rk.`NoDokumen`=rs.`NoDokumen`
					INNER JOIN salesman sm ON r.`Sales_In_Charge`=sm.`KdSalesman`
					INNER JOIN supervisor spv ON sm.`KdSupervisor`=spv.`KdSupervisor`
				  ) res ON t.`add_date`=res.Tanggal AND t.`noidentitas`=res.NoSticker
				  LEFT JOIN `register` reg ON t.`add_date` = reg.`Tanggal` AND t.`noidentitas`=reg.`NoStiker`
				WHERE Year(t.add_date)='$thn' and Month(t.add_date)='$bln'
				  AND t.noidentitas != '1234'
				  AND t.status = 1
				  GROUP BY NamaSpv";

    $sqlsales = "SELECT IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','HC0003', res.KdSupervisor) AS KdSpv,
					  IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','TourDesk & Frnlc-Victor', res.NamaSupervisor) AS NamaSpv,
					  SUM(
					    IF(
					      d.`Service_charge` > 0,
					      d.`Netto` * 1155 / 1000,
					      d.Netto
					    )
					  ) AS Sales
					FROM
					  transaksi_header h
					  INNER JOIN transaksi_detail d
					    ON h.`NoKassa` = d.`NoKassa`
					    AND h.`NoStruk` = d.`NoStruk`
					  LEFT JOIN
					  (
						SELECT rk.`TglKonfirmasi` AS Tanggal, rs.`NoSticker`, sm.`KdSupervisor`, spv.`NamaSupervisor` FROM trans_reservasi r INNER JOIN trans_reservasi_konfirmasi rk ON r.`NoDokumen`=rk.`NoReservasi`
						INNER JOIN trans_reservasi_konfirmasi_sticker rs ON rk.`NoDokumen`=rs.`NoDokumen`
						INNER JOIN salesman sm ON r.`Sales_In_Charge`=sm.`KdSalesman`
						INNER JOIN supervisor spv ON sm.`KdSupervisor`=spv.`KdSupervisor`
					  ) res ON h.`Tanggal`=res.Tanggal AND h.`KdAgent`=res.NoSticker
					  LEFT JOIN `register` reg ON h.`Tanggal` = reg.`Tanggal` AND h.`KdAgent`=reg.`NoStiker`
					WHERE Year(h.`Tanggal`) = '$thn' and month(h.Tanggal)='$bln'
					  AND h.`Status` = 1
					GROUP BY NamaSpv";

	$sqlsunset = "SELECT IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','HC0003', res.KdSupervisor) AS KdSpv,
					  IF(res.KdSupervisor IS NULL AND reg.`KdTravel`='FR00001','TourDesk & Frnlc-Victor', res.NamaSupervisor) AS NamaSpv,
					  SUM(
					    IF(
					      d.`Service_charge` > 0,
					      d.`Netto` * 1155 / 1000,
					      d.Netto
					    )
					  ) AS Sales
					FROM
					  transaksi_header_sunset h
					  INNER JOIN transaksi_detail_sunset d
					    ON h.`NoKassa` = d.`NoKassa`
					    AND h.`NoStruk` = d.`NoStruk`
					  LEFT JOIN
					  (
						SELECT rk.`TglKonfirmasi` AS Tanggal, rs.`NoSticker`, sm.`KdSupervisor`, spv.`NamaSupervisor` FROM trans_reservasi r INNER JOIN trans_reservasi_konfirmasi rk ON r.`NoDokumen`=rk.`NoReservasi`
						INNER JOIN trans_reservasi_konfirmasi_sticker rs ON rk.`NoDokumen`=rs.`NoDokumen`
						INNER JOIN salesman sm ON r.`Sales_In_Charge`=sm.`KdSalesman`
						INNER JOIN supervisor spv ON sm.`KdSupervisor`=spv.`KdSupervisor`
					  ) res ON h.`Tanggal`=res.Tanggal AND h.`KdAgent`=res.NoSticker
					  LEFT JOIN `register` reg ON h.`Tanggal` = reg.`Tanggal` AND h.`KdAgent`=reg.`NoStiker`
					WHERE Year(h.`Tanggal`) = '$thn' and month(h.Tanggal)='$bln'
					  AND h.`Status` = 1
					GROUP BY NamaSpv";


	$qrydistribusioh = "SELECT h.`sidate`, SUM(d.`quantity`*d.`nettprice`)*1.1 AS nilai FROM salesinvoice h
    					INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
    					INNER JOIN deliveryorder o ON d.dono=o.dono
						INNER JOIN gudang g ON o.warehousecode=g.KdGudang
						INNER JOIN subdivisi s ON g.KdSubdivisi=s.KdSubdivisi
						WHERE Year(h.sidate)='$thn' and Month(sidate) = '$bln'  AND h.`status`=1 AND s.kddivisi='1' and h.autoinvoice=0";

    $qryvoucher = "SELECT SUM(d.NilaiVoucher) AS pemakaian_voucher
                	FROM  transaksi_detail_voucher d inner join transaksi_header h on h.nostruk=d.nostruk
                    WHERE Year(d.`Tanggal`)='$thn' and Month(d.Tanggal)='$bln' AND h.status='1' and d.Jenis<>5";

	$qrykomisi = "SELECT sum(Total) as komisi
            		FROM `finance_komisi_header`
            		WHERE Year(TglTransaksi)='$thn' and Month(TglTransaksi) = '$bln'";

    $qrydistribusikopi = "SELECT h.`sidate`, SUM(d.`quantity`*d.`nettprice`)*1.1 AS nilai FROM salesinvoice h
    					INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
    					inner join masterbarang b on d.inventorycode=b.pcode
						WHERE Year(h.sidate)='$thn' and Month(sidate) = '$bln' AND h.`status`=1 and b.KdDivisi=2";


    $rec = $conn->query($sqlsales);
	$data = array();
	while ($row = $rec->fetch_assoc()){
		$kdchannel = $row['KdSpv'];
		$nama = $row['NamaSpv'];
		$sales = $row['Sales'];
		$data[$kdchannel]['NamaSpv'] = $nama;
		$data[$kdchannel]['Amt'] = $sales;
	}

	$rec = $conn->query($sqlsunset);
	while ($row = $rec->fetch_assoc()){
		$kdchannel = $row['KdSpv'];
		$nama = $row['NamaSpv'];
		$sales = $row['Sales'];
		$data[$kdchannel]['NamaSpv'] = $nama;
		$data[$kdchannel]['Amt'] += $sales;
	}

    $rec = $conn->query($sqltiket);
	while ($row = $rec->fetch_assoc()){
		$kdchannel = $row['KdSpv'];
		$nama = $row['NamaSpv'];
		$rptdate = $row['add_date'];
		$sales = $row['NilaiTiket'];
		$data[$kdchannel]['NamaSpv'] = $nama;
		$data[$kdchannel]['Amt'] += $sales;
	}


	$textrpt = "<b>Monthly Channel Report $bln/$thn</b>\n\n";
	$omzet = 0;
	foreach($data as $key => $value){
		$nama = ($value['NamaSpv'] == null || $value['NamaSpv']=='') ? 'Non Channel' : $value['NamaSpv'];
		$nama = substr($nama, 0,14);
		$textrpt .= "<pre>" . str_pad($nama,15,' '). str_pad(number_format($value['Amt'],0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n";
		$omzet += $value['Amt'];
	}

	$tglawal = $thn.'-'.$bln.'-'.'01';
	$tglakhir = $thn.'-'.$bln.'-'.'31';
	$url = 'http://192.168.0.13/sss/update_npm.php?tgl1='.$tglawal.'&tgl2='.$tglakhir;
	$aContext = array(
    	'http' => array(
        	'request_fulluri' => true,
    	),
	);
	echo $url;
	$cxContext = stream_context_create($aContext);
	$distribusivci = 0;
	try {
     	$resp = file_get_contents($url, False, $cxContext);
        $result = json_decode($resp, true);

        foreach($result as $rec){
			$pcode = $rec['PCode'];
			$Qty = $rec['Qty'];
			$sqlharga = "SELECT Harga FROM groupharga_detail d INNER JOIN mapping_barang_vci m ON d.`PCode`=m.`PCode`
				WHERE GroupHargaID=1 AND m.`PCodeVCI`='$pcode'";
			$qryharga = $conn->query($sqlharga);
			$row = $qryharga->fetch_assoc();
			if(count($row)>0){
				$harga = $row['Harga']*1.1;
			}else{
				$harga = 0;
			}
			$distribusivci += $Qty * $harga;
		}
	}
	catch (Exception $e) {
    	//echo $e->getMessage();
    	echo "Koneksi gagal.";
	}

	$rec = $conn->query($qrydistribusioh);
	$row = $rec->fetch_assoc();
	$distribusioh = $row['nilai'];
	$textrpt .= "<pre>" . str_pad('Dist VCI',15,' '). str_pad(number_format($distribusivci+$distribusioh,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n";


	$rec = $conn->query($qrydistribusikopi);
	$row = $rec->fetch_assoc();
	$distribusikopi = $row['nilai'];
	$textrpt .= "<pre>" . str_pad('Dist Kopi',15,' '). str_pad(number_format($distribusikopi,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n\n";

	$rec = $conn->query($qryvoucher);
	$row = $rec->fetch_assoc();
	$voucher = $row['pemakaian_voucher'];
	$textrpt .= "<pre>" . str_pad('Voucher',15,' '). str_pad(number_format($voucher,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n";

	$omzet += $distribusivci+$distribusioh + $distribusikopi - $voucher;
	$textrpt .=  "<pre>" . str_pad('Omzet',15,' '). str_pad(number_format($omzet,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>\n\n";

	$rec = $conn->query($qrykomisi);
	$row = $rec->fetch_assoc();
	$komisi = $row['komisi'];
	$textrpt .= "<pre>" . str_pad('Komisi',15,' '). str_pad(number_format($komisi,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";

	mysqli_close($conn);
	//$textrpt = 'test';
	return array('text'=>$textrpt);
}


function reportmonthly($interval){
	$conn = connect();
	list($thn, $bln, $tgl) = explode('-',date('Y-m-d'));
	$bln = $bln*1;
	if($interval<0){
		$bln = $bln+$interval;
		if($bln<=0){
			$bln += 12;
			$thn -= 1;
		}
	}


    $sqlsales = "SELECT s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore`
				WHERE Year(h.`Tanggal`)='$thn' and Month(h.Tanggal)='$bln'
				  AND h.`Status` = 1
				GROUP BY s.`KodeStore`
				";

	$sqlsalesfood = "SELECT s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN masterbarang m ON m.`PCode`=d.`PCode`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore`
				WHERE Year(h.`Tanggal`)='$thn' and Month(h.Tanggal)='$bln'
				  AND h.`Status` = 1 AND m.`KdGroupBarang`='2'
				GROUP BY s.`KodeStore`
				";

	$sqlsalesbaverage = "SELECT s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN masterbarang m ON m.`PCode`=d.`PCode`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore`
				WHERE Year(h.`Tanggal`)='$thn' and Month(h.Tanggal)='$bln'
				  AND h.`Status` = 1 AND m.`KdGroupBarang`='1'
				GROUP BY s.`KodeStore`
				";

	$qrykomisi = "SELECT sum(Total) as komisi
            		FROM `finance_komisi_header`
            		WHERE Year(TglTransaksi)='$thn' and Month(TglTransaksi) = '$bln'";


	$rec = $conn->query($sqlsales);
	$textsales ='';
	$totsales = 0;
	while ($row = $rec->fetch_assoc()){
		$namastore = $row['NamaStore'];
		$sales = $row['Sales'];
		$totsales += $sales;
		$textsales .= "<pre>" . str_pad($namastore,15,' ') . str_pad(number_format($sales,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>". "\n";
	}

	$textdetail = "<pre>Detail : </pre>";

	$rec = $conn->query($sqlsalesfood);
	$textsalesfood ='';
	$totsalesfood = 0;
	while ($row = $rec->fetch_assoc()){
		$sales = $row['Sales'];
		$totsalesfood += $sales;
		$textsalesfood .= "<pre>" . str_pad('Food',15,' ') . str_pad(number_format($sales,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";
	}

	$rec = $conn->query($sqlsalesbaverage);
	$textsalesbaverage ='';
	$totsalesbaverage = 0;
	while ($row = $rec->fetch_assoc()){
		$sales = $row['Sales'];
		$totsalesbaverage += $sales;
		$textsalesbaverage .= "<pre>" . str_pad('Baverage',15,' ') . str_pad(number_format($sales,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";
	}

	$rec = $conn->query($qrykomisi);
	$row = $rec->fetch_assoc();
	$komisi = $row['komisi'];
	$textkomisi = "<pre>" . str_pad('Komisi',15,' '). str_pad(number_format($komisi,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";

	$omzet = $totsales;
	$textomzet = "<pre>" . str_pad('Omzet',15,' '). str_pad(number_format($omzet,0,',','.'),12,' ',STR_PAD_LEFT) ."</pre>";


	$rpt = "
<b>Daily Report Bulan $bln/$thn</b>

$textsales
$textdetail
$textsalesfood
$textsalesbaverage

$textkomisi
$textomzet
";
	mysqli_close($conn);
	return array('text'=>$rpt);
}

function guestlist($interval){
	$interval += 0;
	$conn = connect();
	$date = date('Y-m-d');
	$result = '';
	$sql = "SELECT CONCAT(r.id,'#', r.GroupName) as beoid, r.id, r.GroupName, r.Status, r.Status_approve, r.nodokumen, t.Nama, r.TglDokumen,
			adultparticipants, childparticipants, kidsparticipants
			FROM trans_reservasi r
			inner join tourtravel t on r.KdTravel=t.KdTravel
			WHERE r.TglDokumen = DATE_ADD('$date', INTERVAL $interval DAY) and r.status=1 and r.status_approve=1 order by r.id";
	$rec = $conn->query($sql);
	if(mysqli_num_rows($rec)>0){
		$i=1;
		$beo = "";
		while($row = $rec->fetch_assoc()){
	        $beo .= "<pre>" .$i. " " . $row['nodokumen']. "</pre>\n";
	        $beo .= "<pre>Group : " .$row['Nama']. "</pre>\n";
	        $beo .= "<pre>Tgl   : " .$row['TglDokumen']. "</pre>\n";
	        $beo .= "<pre>Pax   : D:".$row['adultparticipants']." A:".$row['kidsparticipants']." B:".$row['kidsparticipants']."</pre>\n";
	        $beo .= "lihat detail " ."/showbeo_".$row['id']. "\n\n" ;
	        $i++;
	    }
	}else{
		$beo = "<pre>Tidak ada data.</pre>";
	}

    //$beo ="hilang...";
    mysqli_close($conn);
    return array('text' => $beo);
}

function showbeo($id){
	$conn = connect();

	$sql = "SELECT NoDokumen, TglDokumen, t.`Nama`, r.GroupName, r.`Jam_Kedatangan` , r.Contact, s.`NamaSalesman`, r.`desc_front_office`,  r.`desc_f_b_kitchen`,
			r.`desc_black_aye`, r.`desc_oemah_herborist`, r.`desc_engineering_edp`, r.`desc_accounting`, r.`dp`, r.`Total`,
			adultparticipants, childparticipants, kidsparticipants
			FROM trans_reservasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel`
			INNER JOIN salesman s ON r.`Sales_In_Charge`=s.`KdSalesman`
			WHERE id='$id'";
	$rec = $conn->query($sql);
	if(mysqli_num_rows($rec)>0){
		$row = $rec->fetch_assoc();
		$nodokumen = $row['NoDokumen'];
		$company = $row['Nama'];
		$groupname = $row['GroupName'];
		$adultparticipants = $row['adultparticipants'];
		$childparticipants = $row['childparticipants'];
		$kidparticipants = $row['kidsparticipants'];
		$tgldokumen = $row['TglDokumen'];
		$jam = $row['Jam_Kedatangan'];
		$salesman = $row['NamaSalesman'];
		$front_office = $row['desc_front_office'];
		$kitchen = $row['desc_f_b_kitchen'];
		$black_eye = $row['desc_black_aye'];
		$oh = $row['desc_oemah_herborist'];
		$acct = $row['desc_accounting'];
		$total = $row['Total'];
		$result = "<b>BANQUET EVENT ORDER</b>
					<b>No: $nodokumen </b>

					Company		 : $company
					Group  		 : $groupname
					Participants : D: $adultparticipants A: $kidparticipants B: $childparticipants
					Date		 : $tgldokumen
					Time		 : $jam
					Salesman	 : $salesman
					";
		if(strlen($front_office)>1){
			$result .= "
						<b>Front Office</b>
						$front_office";
		}

		if(strlen($kitchen)>1){
			$result .= "
						<b>F&B Kitchen</b>
						$kitchen";
		}

		if(strlen($black_eye)>1){
			$result .= "
						<b>Black Eye Coffee</b>
						$black_eye";
		}

		if(strlen($oh)>1){
			$result .= "
						<b>Oemah Herborist</b>
						$oh";
		}

		if(strlen($acct)>1){
			$result .= "
						<b>Oemah Herborist</b>
						$acct";
		}

		$result .= "
					<b>Total : $total</b>";
	}else{
		$result = "<pre>Tidak ada data</pre>";
	}
    mysqli_close($conn);
    return array('text'=>$result);
}

function run_schedule(){
	$daily = reportdaily(0);
	$monthly = reportmonthly(0);
	$conn = connect();
	$sql = "SELECT u.`TelegramID` FROM autotelegram a INNER JOIN `user` u ON a.`UserName`=u.`UserName`where ReportName='Report Daily'";
	$rec = $conn->query($sql);
	while($row = $rec->fetch_assoc()){
		$chatid = $row['TelegramID'];
		send_reply($chatid, '', $daily);
    }

    $sql = "SELECT u.`TelegramID` FROM autotelegram a INNER JOIN `user` u ON a.`UserName`=u.`UserName`where ReportName='Report Monthly'";
	$rec = $conn->query($sql);
	while($row = $rec->fetch_assoc()){
		$chatid = $row['TelegramID'];
		send_reply($chatid, '', $monthly);
    }
}

function insert_log($chatid, $keterangan){
	$logconn = connect();
	$sql = "insert into telegram_log(ChatID, Keterangan) values('$chatid','$keterangan') ";
	$rec = $logconn->query($sql);
	mysqli_close($logconn);

}

function connect(){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "db_sumber";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
	if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
	}
	return $conn;
}
?>
