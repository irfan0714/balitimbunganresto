function cekTheform()
{	var tot_realisasi = parseInt($('#v_tot_realisasi').val()); 

    if(document.getElementById("v_kas_bank").value=="")
    {
        alert("Kas Bank harus dipilih");
        document.getElementById("v_kas_bank").focus();
        return false;
    }
	else if(document.getElementById("v_no_ref").value=="")
    {
        alert("No. Referensi harus diisi.");
        document.getElementById("v_no_ref").focus();
        return false;
    }
	else if(document.getElementById("v_supplier").value=="")
    {
        alert("Nama Supplier harus dipilih");
        document.getElementById("v_supplier").focus();
        return false;
    }
    else
    {
		url = $("#base_url").val();
		tanggal = $("#v_tgl_dokumen").val();        		
		$.ajax({
			url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
			data: {tgl:tanggal,jenis:'Kas'},
			type: "POST",
			dataType: 'json',					
			success: function(data)
			{
				if(data=='0'){
					alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
					document.getElementById('v_tgl_dokumen').focus();
				}else{
					document.getElementById("theform").submit();
				}	
			},
			error: function(e) 
			{
				//alert(e);
			} 
	 	});
	}
}

function deleteTrans(nodok, url)
{	
	var r=confirm("Apakah Anda Ingin Menghapus No Dokumen "+nodok+" ?")
	if (r==true)
	{
		window.location = url+"index.php/keuangan/realisasi_uang_muka/delete_trans/"+nodok+"/";	
	}
	else
	{
  		return false;
	}
}
