function active_monthGL(id,base_url){
	$.ajax({
		url : base_url+"index.php/start/getBulanAktif",
		success : function(e){
			var tglGL     = e.split("-");
			var today     = new Date(tglGL[0], tglGL[1]-1, tglGL[2]);
			var startDate = new Date(today.getFullYear(), tglGL[1]-1, 1);
			var endDate   = new Date(today.getFullYear(), tglGL[1], tglGL[2]);
			console.log(tglGL);
			$("#"+id).datepicker({
			    format: "dd-mm-yyyy",
			    minViewMode: "date",
			    autoclose: true,
			    startDate: startDate,
			    endDate: endDate
			});
		},
		error : function(){
			alert('error');
		}
	});
}