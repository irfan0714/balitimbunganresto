<?php
include("header.php");
include("function_email.php");

$modul        = "Formula";
$file_current = "formula.php";
$file_name    = "formula_data.php";
$ses_login = $_SESSION["ses_login"];
function update_approval($v_formulanumber)
{
    $q = "
            UPDATE
                formula
            SET
                approval_status_1 = '0', 
                approval_status_2 = '0' 
            WHERE
                1
                AND formula.formulanumber = '".$v_formulanumber."'
    ";
    if(!mysql_query($q))
    {
        $msg = "Gagal update approval";
        echo "<script>alert('".$msg."');</script>";
        die();    
    }
}

$file_current = "formula.php";
$file_name    = "formula_data.php";

unset($arr_data, $arr_curr);

$ajax = $_REQUEST["ajax"];       


if($ajax)
{
    if($ajax=='search')
    {
        $search_by                = $_GET["search_by"];
        $search_keyword           = trim($_GET["search_keyword"]);
        $search_formulanumber     = trim($_GET["search_formulanumber"]);
        $search_formulaname       = trim($_GET["search_formulaname"]);
        $search_inventorycode     = trim($_GET["search_inventorycode"]);
        $search_inventoryname     = trim($_GET["search_inventoryname"]);
        $search_status            = $_GET["search_status"];
        $search_default           = $_GET["search_default"];
        $v_period                 = trim($_GET["v_period"]);
        $v_mm                     = trim($_GET["v_mm"]);
        $v_yyyy                   = trim($_GET["v_yyyy"]);
        $v_limit                  = trim($_GET["v_limit"]);
        
        $v_formulanumber_curr     = $_GET["v_formulanumber_curr"];
        
        $where_data = "";
        $where_formulanumber = "";
        $where_formulaname = "";
        $where_inventorycode = "";
        $where_inventoryname = "";
        $where_period = "";
        
        if($search_by=="data")
        {
            if($search_status)
            {
                if($search_status=="active")
                {
                    $where_data .= " AND formula.isactive = '1' ";    
                }
                else if($search_status=="inactive")
                {
                    $where_data .= " AND formula.isactive = '0' ";    
                }
            }    
            
            if($search_default)
            {
                if($search_default=="yes")
                {
                    $where_data .= " AND formula.isdefault = '1' ";    
                }
                else if($search_default=="no")
                {
                    $where_data .= " AND formula.isdefault = '0' ";    
                }    
            } 
            
            if($search_keyword)
            {
                //echo $search_keyword;
                unset($arr_keyword);
                $arr_keyword[0] = "formula.formulanumber";
                $arr_keyword[1] = "formula.formulaname";
                $arr_keyword[2] = "masterbarang.PCode";
                $arr_keyword[3] = "masterbarang.NamaLengkap";
                
                $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
                $where_data .= $where_search_keyword;
            }
            
            if($v_period=="monthly")
            {
                $where_period = " AND (SUBSTR(formula.formulanumber,9,2) = '".sprintf("%02s", $v_mm)."' AND SUBSTR(formula.formulanumber,12,2) = '".substr($v_yyyy,2,2)."') ";
            }
        }
        else if($search_by=="formulanumber")
        {
            if($search_formulanumber)
            {
                $where_formulanumber = " AND formula.formulanumber LIKE '%".$search_formulanumber."%' ";        
            }
        }
        else if($search_by=="formulaname")
        {
            if($search_formulaname)
            {
                $where_formulaname = " AND formula.formulaname LIKE '%".$search_formulaname."%' ";        
            }
        }
        else if($search_by=="inventorycode")
        {
            if($search_inventorycode)
            {
                $where_inventorycode = " AND masterbarang.PCode LIKE '%".$search_inventorycode."%' ";        
            }
        }
        else if($search_by=="inventoryname")
        {
            if($search_inventoryname)
            {
                $where_inventoryname = " AND masterbarang.NamaLengkap LIKE '%".$search_inventoryname."%' ";        
            }
        }
        
        $where_limit = ""; 
        if($v_limit!="all")
        {
            $where_limit = "LIMIT 0, ".$v_limit;
        }
        
        $q = "
        SELECT
            formula.formulanumber, 
            formula.formulaname, 
            formula.quantity, 
            formula.uominitial, 
            formula.packagingsize, 
            formula.uompackaging, 
            formula.isactive, 
            formula.isdefault, 
            formula.edituser, 
            formula.editdate, 
            formula.approval_status_1, 
            formula.approval_status_2, 
            formula.approval_date_1, 
            formula.approval_date_2 
        FROM
            formula
            INNER JOIN masterbarang ON
                masterbarang.`PCode` = formula.inventorycode
                AND masterbarang.`KdKategori` = '3'
                AND formula.archive = '0'
        WHERE
            1
            ".$where_data."
            ".$where_formulanumber."
            ".$where_formulaname."
            ".$where_inventorycode."
            ".$where_inventoryname."
            ".$where_period."
        ORDER BY
            formula.formulanumber ASC,
            formula.formulaname ASC
        ".$where_limit."
    ";
    /*echo "<pre>$q</pre>";
    die();*/
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($formulanumber, $formulaname, $quantity, $uominitial, $packagingsize, $uompackaging, $isactive, $isdefault, $edituser, $editdate, $approval_status_1, $approval_status_2, $approval_date_1, $approval_date_2) = $row;
        
        $arr_data["list_formula"][$formulanumber] = $formulanumber;
        
        $arr_data["formulaname"][$formulanumber] = $formulaname;
        $arr_data["quantity"][$formulanumber] = $quantity;
        $arr_data["uominitial"][$formulanumber] = $uominitial;
        $arr_data["packagingsize"][$formulanumber] = $packagingsize;
        $arr_data["uompackaging"][$formulanumber] = $uompackaging;
        $arr_data["isactive"][$formulanumber] = $isactive;
        $arr_data["isdefault"][$formulanumber] = $isdefault;
        $arr_data["edituser"][$formulanumber] = $edituser;
        $arr_data["editdate"][$formulanumber] = $editdate;
        $arr_data["approval_status_1"][$formulanumber] = $approval_status_1;
        $arr_data["approval_status_2"][$formulanumber] = $approval_status_2;
        $arr_data["approval_date_1"][$formulanumber] = $approval_date_1;
        $arr_data["approval_date_2"][$formulanumber] = $approval_date_2;
    }
    
    if(count($arr_data["list_formula"])>0)
    {
        $where_formula = where_array($arr_data["list_formula"], "formuladetail.formulanumber", "in");
    
        $q = "
                SELECT
                    formuladetail.formulanumber,
                    SUM(formuladetail.percentage) AS total
                FROM
                    formuladetail
                WHERE
                    1
                    ".$where_formula."
                GROUP BY
                    formuladetail.formulanumber
                ORDER BY  
                    formuladetail.formulanumber ASC 
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $total) = $row;
            
            $arr_data["total_percentage"][$formulanumber] = $total;
        }
        
    }

    ?>
    <div class="col-md-12">
    <div style="padding:5px;">
        <span style="background-color:#FFFF00;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <b>Percantage != 100%</b>
    </div>
    
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="formula_data.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
	    <thead>
			<tr>
				<th><strong><center>No</center></strong></th>
				<th><strong><center>Formula Number</center></strong></th>
				<th><strong><center>Formula Name</center></strong></th>
				<th><strong><center>Batch Qty</center></strong></th>
				<th><strong><center>Batch Unit</center></strong></th>
				<th><strong><center>Pack Qty</center></strong></th>
				<th><strong><center>Pack Unit</center></strong></th>
				<th><strong><center>Status</center></strong></th>
				<th><strong><center>Default</center></strong></th>
				<th><strong><center>Last Edited</center></strong></th>
				<th><strong><center>Approval 1</center></strong></th>
				<th><strong><center>Approval 2</center></strong></th>
				<th style="display:none;">
					Action<br/>
					<input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')">
				</th>
			</tr>
		</thead>
		<tbody>
	    <?php
	    $nomor=0;    
	     
	    $jml = count($arr_data["list_formula"]);
	    
	    $js="";
	    for($i=1;$i<=$jml;$i++) {    
	        $js.="document.getElementById('row_$i').style.background=''; ";
	    }
	 
	    
	    $arr_data["echo_status"][0] = "InActive";
	    $arr_data["echo_status"][1] = "Active";
	    
	    $arr_data["echo_default"][0] = "No";
	    $arr_data["echo_default"][1] = "Yes";
    
        $nomor = 0;    
        foreach($arr_data["list_formula"] as $formulanumber => $val)
        {    
            $nomor++;
            
            $formulaname = $arr_data["formulaname"][$formulanumber];
            $quantity = $arr_data["quantity"][$formulanumber];
            $uominitial = $arr_data["uominitial"][$formulanumber];
            $packagingsize = $arr_data["packagingsize"][$formulanumber];
            $uompackaging = $arr_data["uompackaging"][$formulanumber];
            $isactive = $arr_data["isactive"][$formulanumber];
            $isdefault = $arr_data["isdefault"][$formulanumber];
            $edituser = $arr_data["edituser"][$formulanumber];
            $editdate = $arr_data["editdate"][$formulanumber];
            $approval_status_1 = $arr_data["approval_status_1"][$formulanumber];
            $approval_status_2 = $arr_data["approval_status_2"][$formulanumber];
            $approval_date_1 = $arr_data["approval_date_1"][$formulanumber];
            $approval_date_2 = $arr_data["approval_date_2"][$formulanumber];
            
            $total_percentage = $arr_data["total_percentage"][$formulanumber];
            
            $edit_echo = $edituser." ".format_show_datetime($editdate);
                
            $style_color = "";
            $style_bg = "";
            
            if($v_formulanumber_curr==$formulanumber)
            {
                $style_color = "background:#cafdb5;";    
            }
            
            if($approval_status_1==0)
            {
                $echo_approval_1 = "Pending";
            }
            else if($approval_status_1==1)
            {
                $echo_approval_1 = "<nobr><img src='images/accept.png'> ".format_show_date($approval_date_1)."</nobr>";
            }
            else if($approval_status_1==2)
            {
                $echo_approval_1 = "<nobr><img src='images/cross.png'> ".format_show_date($approval_date_1)."</nobr>";
            }
            
            if($approval_status_2==0)
            {
                $echo_approval_2 = "Pending";
            }
            else if($approval_status_2==1)
            {
                $echo_approval_2 = "<nobr><img src='images/accept.png'> ".format_show_date($approval_date_2)."</nobr>";
            }
            else if($approval_status_2==2)
            {
                $echo_approval_2 = "<nobr><img src='images/cross.png'> ".format_show_date($approval_date_2)."</nobr>";
            }
            
            $style_bg_active = "";
            if($isactive==0)
            {
                $style_bg_active = "background:#FFFF00;";
            }
            
            $style_percentage = "";
            if($total_percentage*1!=100)
            {
                $style_percentage = "background-color:#FFFF00;";
            }
            
        ?>
        
        <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$style_bg; ?>">
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $nomor; ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>" align="center"><?php echo $formulanumber; ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $formulaname; ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>" align="right"><?php echo format_number($quantity,2); ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $uominitial; ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>" align="right"><?php echo format_number($packagingsize); ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>">&nbsp;<?php echo $uompackaging; ?>&nbsp;</td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_bg_active; ?>"><?php echo $arr_data["echo_status"][$isactive]; ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $arr_data["echo_default"][$isdefault]; ?></td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $edit_echo; ?>&nbsp;</td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $echo_approval_1; ?>&nbsp;</td>
            <td onclick="CallAjax('edit_data_baku','<?php echo $formulanumber; ?>')" style="<?php echo $style_percentage.$style_bg; ?>"><?php echo $echo_approval_2; ?>&nbsp;</td>
            <td style="<?php echo $style_bg; ?> display:none;" align="center"><input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $simulation_no; ?>"></td> 
        </tr>
        
        <?php
        }
        
    	?>
    
        <tr style="display:none;">
            <td colspan="100%" align="right">
                <select name="action" id="action" class="text">
                    <option value="print">Print</option>
                    <option value="archive">Archive</option>
                </select>
                <input type="submit" class="button_new" name="btn_submit" id="btn_submit" value="Submit">
                &nbsp;&nbsp;
            </td>
        </tr>
        
    	</tbody>
    
    </table>
    </form>
	</div>
    <?php
    
    }
    else if($ajax=="add_data")
    {    
        //echo menutab($v_inventorycode, 'pabrik');
        /*$q = "
                SELECT
                    inventorymaster.inventorycode,
                    inventorymaster.inventoryname
                FROM
                    inventorymaster
                WHERE
                    1
                    AND inventorymaster.stocktypeid = '3'
                    AND inventorymaster.imported = '0'
                ORDER BY
                    inventorymaster.inventoryname ASC
        ";*/
        $q="
        	SELECT 
			  a.`PCode`,
			  a.`NamaLengkap` 
			FROM
			  masterbarang a 
			WHERE 1 
			  AND a.`KdKategori` = '3' 
			ORDER BY a.`NamaLengkap` ASC ;
        	";
        //echo $q;die;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname) = $row;
            
            $arr_data["list_fg"][$inventorycode] = $inventorycode;
            
            $arr_data["inventoryname"][$inventorycode] = $inventorycode." :: ".$inventoryname;
        }
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname
                FROM
                    formula
                WHERE
                    1
                ORDER BY
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname) = $row;
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            $arr_data["formulaname"][$formulanumber] = $formulaname;
        }
        
        $q = "
                SELECT
                    unitofmeasurement.uominitial,    
                    unitofmeasurement.description
                FROM
                    unitofmeasurement
                WHERE
                    1
                ORDER BY
                    unitofmeasurement.uominitial ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($uominitial, $description) = $row;
            
            $arr_data["list_unitofmeasurement"][$uominitial] = $uominitial;    
            
            $arr_data["unitofmeasurement_description"][$uominitial] = $description;
        }
        
        ?>
        
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Add Add <?php echo $modul; ?>
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            <input type="hidden" name="v_del" id="v_del" value="">
			
            <table class="table table-bordered responsive">

                <tr>
                    <td class="title_table" width="250">Type</td>
                    <td>
                        <label><input type="radio" name="v_type" id="v_type" value="Reguler" checked="checked" onclick="change_type(this.value)">&nbsp;&nbsp;Reguler</label>&nbsp;&nbsp;
                        <label><input type="radio" name="v_type" id="v_type" value="Copy" onclick="change_type(this.value)">&nbsp;&nbsp;Copy</label>
                    </td>
                </tr>
                
                <tr id="tr_copy_formula" style="display:none;">
                    <td class="title_table">Formula Copy</td>
                    <td>
                        Keyword&nbsp;&nbsp;
                        <input type="text" class="form-control-new" name="v_keyword_copy_formula" id="v_keyword_copy_formula" size="10" maxlength="50" value="" onkeyup="CallAjax('ajax_search_copy_formula', this.value)">
                        
                        <span id="td_formula_copy">
                        <select name="v_formulanumber_copy" id="v_formulanumber_copy" class="form-control-new" style="width:250px;">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_formula"] as $formulanumber => $val)
                                {
                                    $formulaname = $arr_data["formulaname"][$formulanumber];
                                    ?>
                                    
                                    <option value="<?php echo $formulanumber; ?>"><?php echo $formulanumber." :: ".$formulaname; ?></option>
                                    <?php    
                                }
                            ?>
                        </select>
                        </span>
                    </td>
                </tr>
                
                <tr id="tr_formula_name">
                    <td class="title_table">Formula Name</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_formulaname" id="v_formulaname" size="50" maxlength="100" value="">
                    </td>
                </tr>
                
                <tr id="tr_finish_goods">
                    <td class="title_table">Finish Goods</td>
                    <td>
                        Keyword&nbsp;&nbsp;
                        <input type="text" class="form-control-new" name="v_keyword_fg" id="v_keyword_fg" size="10" maxlength="50" value="" onkeyup="CallAjax('ajax_search_fg', this.value)">
                        
                        <span id="td_fg">
                        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:250px;">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_fg"] as $inventorycode => $val)
                                {
                                    $inventoryname = $arr_data["inventoryname"][$inventorycode];
                                    ?>
                                    
                                    <option value="<?php echo $inventorycode; ?>"><?php echo $inventoryname; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                        </span>
                    </td>
                </tr>
                
                <tr id="tr_batch_qty">
                    <td class="title_table">Batch Qty</td>
                    <td>
                        <input type="text" class="form-control-new" size="15" style="text-align:right;" name="v_quantity" id="v_quantity" onblur="toFormat2('v_quantity')" value="">
                        
                        <select name="v_uominitial" id="v_uominitial" class="form-control-new">
                            <option value="<?php echo 'GR'; ?>"><?php echo 'Gram'; ?></option> 
                        </select>
                    </td>
                </tr>		                
                
                <tr id="tr_packagingsize">
                    <!--<td class="title_table">Packaging Size</td>-->
                    <td class="title_table">Quantity Finish Good</td>
                    <td>
                        <input type="text" class="form-control-new" size="15" style="text-align:right;" name="v_packagingsize" id="v_packagingsize" onblur="toFormat('v_packagingsize')" value="">
                        
                        <select name="v_uompackaging" id="v_uompackaging" class="form-control-new">
                            <?php 
                                foreach($arr_data["list_unitofmeasurement"] as $arr_uominitial => $val)
                                {
                                    $description = $arr_data["unitofmeasurement_description"][$arr_uominitial];       

                                    ?>
                                        <option value="<?php echo $arr_uominitial; ?>"><?php echo $description; ?></option>
                                    <?php
                                }
                            ?>
                            <option></option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Packaging Size Range</td>
                    <td>Min&nbsp;&nbsp;
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_packagingsize_min" id="v_packagingsize_min" onblur="toFormat('v_packagingsize_min')" value="">
                        &nbsp;&nbsp;Max&nbsp;&nbsp;
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_packagingsize_max" id="v_packagingsize_max" onblur="toFormat('v_packagingsize_max')" value="">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Std Production Time (day)</td>
                    <td>
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_std_production_time" id="v_std_production_time" value="" style="text-align: right;">
                    </td>
                </tr>
                <tr>
                    <td class="title_table">Berat Finish Goods Per Pcs (Gram)</td>
                    <td>
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_berat_per_pcs" id="v_berat_per_pcs" value="" style="text-align: right;">
                    </td>
                </tr>
                
                <tr id="tr_status">
                    <td class="title_table">&nbsp;</td>
                    <td>
                        <label><input type="checkbox" name="v_active" id="v_active">&nbsp;&nbsp;Active</label><br>
                        <label><input type="checkbox" name="v_default" id="v_default">&nbsp;&nbsp;Default</label>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">
                    
                    	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                    
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <?php
    }
    else if($ajax=="edit_data_baku")
    {
        $v_formulanumber = $_GET["v_formulanumber"];
        
        $q = "
                SELECT
                    formula.formulanumber, 
                    formula.formulaname, 
                    masterbarang.PCode, 
                    masterbarang.NamaLengkap, 
                    formula.quantity, 
                    formula.uominitial, 
                    formula.packagingsize, 
                    formula.uompackaging, 
                    formula.isactive, 
                    formula.isdefault, 
                    formula.edituser, 
                    formula.editdate, 
                    formula.approval_status_1, 
                    formula.approval_status_2, 
                    formula.approval_date_1, 
                    formula.approval_date_2,
                    formula.pending,
                    formula.packagingsize_min,
                    formula.packagingsize_max,
                    formula.std_production_time,
                    formula.finishgood_wgt
                FROM
                    formula
                    INNER JOIN masterbarang ON
                        masterbarang.PCode =  formula.inventorycode
                        AND formula.formulanumber = '".$v_formulanumber."'
                WHERE
                    1
                LIMIT
                    0,1
        ";
        $qry_curr = mysql_query($q);
        $arr_curr = mysql_fetch_array($qry_curr);
        
        
        $_SESSION["ses_formulanumber"]  = $v_formulanumber;
        $_SESSION["ses_data"]           = "baku";
        
        $q = "
                SELECT 
				  formuladetail.formuladetailid,
				  formuladetail.inventorycode,
				  masterbarang.`NamaLengkap`,
				  formuladetail.percentage,
				  formuladetail.quantity,
				  formuladetail.`jenis` 
				FROM
				  formuladetail 
				  INNER JOIN masterbarang 
				    ON formuladetail.inventorycode = masterbarang.`PCode`
				    AND (formuladetail.percentage * 1) != '0' 
				    AND formuladetail.`jenis`='R'
                        AND formuladetail.formulanumber = '".$v_formulanumber."' 
                ORDER BY
                     formuladetail.formuladetailid ASC
        ";
        //echo $q;
        $no = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formuladetailid, $inventorycode, $NamaLengkap, $percentage, $quantity) = $row;
            
            $arr_data["list_details"][$formuladetailid] = $formuladetailid;
            $arr_data["list_inventory"][$inventorycode] = $inventorycode;
            
            $arr_data["details_inventorycode"][$formuladetailid] = $inventorycode;
            $arr_data["details_inventoryname"][$formuladetailid] = $NamaLengkap;
            $arr_data["details_percentage"][$formuladetailid] = $percentage;
            $arr_data["details_quantity"][$formuladetailid] = $quantity;
            $no++;
        }
        
        //$arr_stock = bincard_stock(date("m"), date("Y"), $arr_data["list_inventory"], "WH000", "1");
        
        $q = "
                SELECT
                    unitofmeasurement.uominitial,    
                    unitofmeasurement.description
                FROM
                    unitofmeasurement
                WHERE
                    1
                ORDER BY
                    unitofmeasurement.uominitial ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($uominitial, $description) = $row;
            
            $arr_data["list_unitofmeasurement"][$uominitial] = $uominitial;    
            
            $arr_data["unitofmeasurement_description"][$uominitial] = $description;
        }
        
        if($arr_curr["isactive"]*1==1)
        {
            $checked_active = "checked='checked'";    
        }
        else
        {
            $checked_active = "";
        }
        
        if($arr_curr["isdefault"]*1==1)
        {
            $checked_default = "checked='checked'";    
        }
        else
        {
            $checked_default = "";
        }
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Add Formula
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" id="action" value="edit_data_baku">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_req" id="v_req" value="">
            <input type="hidden" name="v_appr" id="v_appr" value="">
            <input type="hidden" name="v_formulanumber" id="v_formulanumber" value="<?php echo $v_formulanumber; ?>">
            
            <input type="hidden" name="v_formulaname_old" id="v_formulaname_old" value="<?php echo $arr_curr["formulaname"]; ?>">
            <input type="hidden" name="v_quantity_old" id="v_quantity_old" value="<?php echo format_number($arr_curr["quantity"], 2); ?>">
            <input type="hidden" name="v_uominitial_old" id="v_uominitial_old" value="<?php echo $arr_curr["uominitial"]; ?>">
            <input type="hidden" name="v_packagingsize_old" id="v_packagingsize_old" value="<?php echo format_number($arr_curr["packagingsize"], 0); ?>">
            <input type="hidden" name="v_uompackaging_old" id="v_uompackaging_old" value="<?php echo $arr_curr["uompackaging"]; ?>">
            
            <input type="hidden" name="v_active_old" id="v_active_old" value="<?php echo $arr_curr["isactive"]; ?>">
            <input type="hidden" name="v_default_old" id="v_default_old" value="<?php echo $arr_curr["isdefault"]; ?>">
            
            <table class="table table-bordered responsive">

                <tr>
                    <td class="title_table" width="250">Formula Number</td>
                    <td colspan="3">
                        <b><?php echo $arr_curr["formulanumber"]; ?></b>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Formula Name</td>
                    <td colspan="3">
                        <input type="text" class="form-control-new" name="v_formulaname" id="v_formulaname" size="50" maxlength="100" value="<?php echo $arr_curr["formulaname"]; ?>">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Finish Goods</td>
                    <td>
                        <?php 
                            echo $arr_curr["PCode"]." :: ".$arr_curr["NamaLengkap"];
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Batch Qty</td>
                    <td>
                        <input type="text" class="form-control-new" size="15" style="text-align:right;" name="v_quantity" id="v_quantity" onblur="toFormat2('v_quantity')" value="<?php echo format_number($arr_curr["quantity"], 2); ?>">
                        
                        <select name="v_uominitial" id="v_uominitial" class="form-control-new">
                          
                                        <option value="<?php echo 'GR'; ?>"><?php echo 'Gram'; ?></option>
                                    
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Packaging Size</td>
                    <td>
                        <input type="text" class="form-control-new" size="15" style="text-align:right;" name="v_packagingsize" id="v_packagingsize" onblur="toFormat('v_packagingsize')" value="<?php echo format_number($arr_curr["packagingsize"], 0); ?>">
                        
                        <select name="v_uompackaging" id="v_uompackaging" class="form-control-new">
                            <?php 
                                foreach($arr_data["list_unitofmeasurement"] as $arr_uominitial => $val)
                                {
                                    $description = $arr_data["unitofmeasurement_description"][$arr_uominitial];       
                                    
                                    $selected = "";
                                    if($arr_uominitial==$arr_curr["uompackaging"])
                                    {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                        <option <?php echo $selected; ?> value="<?php echo $arr_uominitial; ?>"><?php echo $description; ?></option>
                                    <?php
                                }
                            ?>
                            <option></option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Packaging Size Range</td>
                    <td>Min&nbsp;&nbsp;
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_packagingsize_min" id="v_packagingsize_min" onblur="toFormat('v_packagingsize_min')" value="<?php echo format_number($arr_curr["packagingsize_min"], 0); ?>">
                        &nbsp;&nbsp;Max&nbsp;&nbsp;
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_packagingsize_max" id="v_packagingsize_max" onblur="toFormat('v_packagingsize_max')" value="<?php echo format_number($arr_curr["packagingsize_max"], 0); ?>">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Std Production Time (day)</td>
                    <td>
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_std_production_time" id="v_std_production_time" value="<?php echo $arr_curr["std_production_time"]; ?>" style="text-align: right;">
                    </td>
                </tr>
                <tr>
                    <td class="title_table">Berat Finish Goods Per Pcs (Gram)</td>
                    <td>
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_berat_per_psc" id="v_berat_per_pcs" value="<?php echo $arr_curr["finishgood_wgt"]; ?>" style="text-align: right;">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">&nbsp;</td>
                    <td style="font-weight:bold;">
                        <label><input type="checkbox" <?php echo $checked_active; ?> name="v_active" id="v_active">&nbsp;&nbsp;Active</label><br>
                        <label><input type="checkbox" <?php echo $checked_default; ?> name="v_default" id="v_default">&nbsp;&nbsp;Default</label>
                    </td>
                </tr>
               
                
                <tr height="25">
                <!-- kedua -->
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <?php 
                            if($arr_curr["pending"]==0)
                            {
                        ?>
                       
                        <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                        
                        <?php 
                            $q = "
                                    SELECT
                                        matreq.formulanumber
                                    FROM
                                        matreq
                                    WHERE
                                        matreq.formulanumber = '".$v_formulanumber."'
                                    LIMIT
                                        0,1
                            ";
                            $qry_cek = mysql_query($q);
                            $row_cek = mysql_fetch_array($qry_cek);
                            
                            if($row_cek["formulanumber"]=="")
                            {
                        ?>
                        <button type="button" onclick="confirm_delete('<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" name="btn_delete" id="btn_delete" value="Delete">Delete<i class="entypo-cancel"></i></button>
                        <?php 
                            }
                        ?>
                        
                        
                        <?php 
                            if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            {
                        ?>
                        <button type="button" onclick="confirm_approval('<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" name="btn_request" id="btn_request" value="Request Approval">Request Approval<i class="entypo-thumbs-up"></i></button>
                        <?php 
                            }
                        ?>
                        
                        <?php 
                            }
                            else
                            {
                                //cek otorisasi_user
									$cek= "
									SELECT 
									  * 
									FROM
									  `otorisasi_user` a 
									WHERE a.`UserName` = 'heru0101' OR a.`UserName` = 'mechael0101' OR a.`UserName` = 'kho2211' 
									  AND a.`Tipe` = 'approve_formula' ;
									";
									/*$cek= "
									SELECT 
									  * 
									FROM
									  `otorisasi_user` a 
									WHERE a.`UserName` = 'mechael0101' 
									  AND a.`Tipe` = 'approve_formula' ;
									";*/
									
									//apakah ada datanya?
									$baris=mysql_query($cek);
									$count = mysql_num_rows($baris);
									if($count>0){ 
											if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            						{
									              ?>
										
						                        <button type="button" onclick="confirm_approval_formula('<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" name="btn_request" id="btn_request" value="Request Approval">Approve Formula<i class="entypo-thumbs-up"></i></button>
						                 
									          <?php }
										}else{
											echo "<font color='red'>Button Save Tidak muncul, karena status formula Pending</font>";
										}
                            }
                        ?>
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="100%" style="color:#FF0000;">
                        *) Setiap terjadi perubahan data, akan membutuhkan approval 1 atau approval 2<br>
                        *) Setelah melakukan perubahan, harap untuk melakukan Request Approval
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="100%">
                        <?php
                            echo menutab($v_formulanumber, 'baku');
                        ?>
                    </td>
                </tr>
 
                
 
                <?php 
                    if($arr_curr["pending"]==0)
                    {
                ?>
                <!--<tr style="display: none;">-->
                <!--<tr>
                    <td colspan="100%">
                    	<button type="button" onclick="pop_up_details()" class="btn btn-info btn-icon btn-sm icon-left" name="btn_add_details" id="btn_add_details" value="Request Approval">Add Raw Material<i class="entypo-plus"></i></button>
                    </td>
                </tr>-->
                <?php 
                    }
                    
                    $no = 1;
                ?>
                
                
                <tr>
                    <td colspan="100%">
                    	<table class="table table-bordered responsive">
						    <thead>
								<tr>
									<th>No Raw Material</th>
									<th>PCode</th>
									<th>Nama Barang</th>
									<th>Percentage</th>
									<th>Qty</th>
									<th>Action</th>
                                    
								</tr>
							</thead>
                            
							<tbody>
	                            <tr>
	                                <td>ADD</td>
	                                <td align="center">
	                                    <input type="text" class="form-control-new" size="9" name="v_inventorycode_<?php echo $no; ?>" id="v_inventorycode_<?php echo $no; ?>" >
	                                    <button type="button" class="btn btn-info btn-sm sm-new" title="Search Pcode" value=".." onclick="pop_up_baku('<?php echo $v_formulanumber; ?>','<?php echo $no; ?>')"><i class="entypo-dot-3"></i></button>
	                                </td>
	                                <td id="td_inventoryname_<?php echo $no; ?>">&nbsp;</td>
	                                <td><input type="text" class="form-control-new" size="20" maxlength="20" name="v_percentage_<?php echo $no; ?>" id="v_percentage_<?php echo $no; ?>" onblur="toFormat6('v_percentage_<?php echo $no; ?>')" value="" style="text-align:right;" onkeyup="calculate()"></td>
	                                <!--<td align="right" id="td_quantity_<?php echo $no; ?>">&nbsp;</td>-->
	                                <td><input type="text" class="form-control-new" size="20" maxlength="20" id="td_quantity_<?php echo $no; ?>" name="td_quantity_<?php echo $no; ?>" value=""/></td>
	                                <td align="center">
	                                    <button type="button" class="btn btn-info btn-sm sm-new" value="Save" onclick="CallAjax('ajax_save_detail_baku', '<?php echo $v_formulanumber; ?>')" title="Save"><i class="entypo-floppy"></i></button>
	                                </td>
	                            </tr>
	                       </tbody>
                           
                            
                            <?php
                                if(count($arr_data["list_details"])==0)
                                {
                                    ?>
                                        <tr>
                                            <td colspan="100%" align="center">No Data</td>
                                        </tr>
                                    <?php
                                }
                            
                                $no++;
                                $no_echo = 1; 
                                foreach($arr_data["list_details"] as $formuladetailid => $val)
                                {
                                    $inventorycode = $arr_data["details_inventorycode"][$formuladetailid];
                                    $inventoryname = $arr_data["details_inventoryname"][$formuladetailid];
                                    $percentage = $arr_data["details_percentage"][$formuladetailid];
                                    $quantity = $arr_data["details_quantity"][$formuladetailid];
                                    
                                    $quantity =  $arr_curr["quantity"] * ($percentage/100);
                                    
                                    if($inventorycode!="01-00097")
                                    {
                                        
                                    }
                                    
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td>
                                            <?php echo $no_echo; ?>
                                            <input type="hidden" name="no[]" value="<?php echo $no; ?>">
                                            <input type="hidden" name="v_sid_<?php echo $no; ?>" id="v_sid_<?php echo $no; ?>" value="<?php echo $formuladetailid; ?>">
                                            <input type="hidden" name="v_inventorycode_old_<?php echo $no; ?>" id="v_inventorycode_old_<?php echo $no; ?>" value="<?php echo $inventorycode; ?>">
                                            <input type="hidden" name="v_percentage_old_<?php echo $no; ?>" id="v_percentage_old_<?php echo $no; ?>" value="<?php echo format_number($percentage, 6); ?>">
                                        </td>
                                        <td align="center">
                                            <input type="text" class="text" size="9" name="v_inventorycode_<?php echo $no; ?>" id="v_inventorycode_<?php echo $no; ?>" value="<?php echo $inventorycode; ?>" onkeyup="CallAjax('ajax_search_pcode_baku', this.value, '<?php echo $no; ?>')" onblur="CallAjax('ajax_search_pcode_baku', this.value, '<?php echo $no; ?>')">
                                            <button type="button" class="btn btn-info btn-sm sm-new" value=".." onclick="pop_up_baku('<?php echo $v_formulanumber; ?>','<?php echo $no; ?>')"><i class="entypo-dot-3"></i></button>
                                        </td>
                                        <td id="td_inventoryname_<?php echo $no; ?>"><?php echo $inventoryname; ?></td>
                                        
                                        <td align="right"><input type="text" class="text" size="20" maxlength="20" name="v_percentage_<?php echo $no; ?>" id="v_percentage_<?php echo $no; ?>" onblur="toFormat6('v_percentage_<?php echo $no; ?>')" value="<?php echo format_number($percentage, 6 ); ?>" style="text-align:right;" onkeyup="calculate()"></td>
                                        <td align="right" id="td_quantity_<?php echo $no; ?>">&nbsp;<?php echo format_number($quantity, 4); ?></td>
                                       
									   <?php  
										if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            			{
									    ?>
						                <td align="center"><input type="checkbox" name="v_del_details[]" id="v_del_details_<?php echo $no; ?>" value="<?php echo $formuladetailid; ?>"></td>
									    <?php }
										?>
                                        
                                    </tr>
                                    <?php
                                    
                                    $arr_total["percentage"] += $percentage;
                                    $arr_total["quantity"] += $quantity;
                                    $no++;
                                    $no_echo++;
                                }
                                
                                $no = $no-1;    
                            ?>
                            
                            <tr>
                                <td colspan="3">
                                    &nbsp;
                                    <input type="hidden" name="v_no" id="v_no" value="<?php echo $no; ?>">
                                </td>
                                <td id="td_total_percentage" align="right">&nbsp;<?php echo format_number($arr_total["percentage"], 6); ?></td>
                                <td id="td_total_quantity" align="right">&nbsp;<?php echo format_number($arr_total["quantity"], 4); ?></td>
                                <td align="center">
								
								    <?php 
									if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            		{
									?>
                                	<button type="submit" name="btn_delete_details" id="btn_delete_details" class="btn btn-info btn-sm sm-new" value="Del" title="Delete"><i class="entypo-trash"></i></button>
                                    <?php }
									?>	
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        
        </div>
        <?php    
    }
    else if($ajax=="edit_data_kemas")
    {
        $v_formulanumber = $_GET["v_formulanumber"];
        
        $q = "
                SELECT
                    formula.formulanumber, 
                    formula.formulaname, 
                    masterbarang.PCode, 
                    masterbarang.NamaLengkap, 
                    formula.quantity, 
                    formula.uominitial, 
                    formula.packagingsize, 
                    formula.uompackaging, 
                    formula.isactive, 
                    formula.isdefault, 
                    formula.edituser, 
                    formula.editdate, 
                    formula.approval_status_1, 
                    formula.approval_status_2, 
                    formula.approval_date_1, 
                    formula.approval_date_2,
                    formula.isactive,
                    formula.isdefault,
                    formula.pending,
                    formula.packagingsize_min,
                    formula.packagingsize_max,
                    formula.std_production_time
                FROM
                    formula
                    INNER JOIN masterbarang ON
                        masterbarang.PCode =  formula.inventorycode
                        AND formula.formulanumber = '".$v_formulanumber."'
                WHERE
                    1
                LIMIT
                    0,1
        ";
        $qry_curr = mysql_query($q);
        $arr_curr = mysql_fetch_array($qry_curr);
        
        $_SESSION["ses_formulanumber"]  = $v_formulanumber;
        $_SESSION["ses_data"]           = "kemas";
        
        $q = "
                SELECT 
				  formuladetail.formuladetailid,
				  formuladetail.inventorycode,
				  masterbarang.`NamaLengkap`,
				  formuladetail.percentage,
				  formuladetail.quantity,
				  formuladetail.`jenis` 
				FROM
				  formuladetail 
				  INNER JOIN masterbarang
				    ON formuladetail.inventorycode = masterbarang.`PCode` 
				    AND (formuladetail.quantity * 1) != '0'
				    AND formuladetail.`jenis`='P'
                        AND formuladetail.formulanumber = '".$v_formulanumber."' 
                ORDER BY
                     formuladetail.formuladetailid ASC
        ";
        //echo $q;
        $no = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formuladetailid, $inventorycode, $inventoryname, $percentage, $quantity) = $row;
            
            $arr_data["list_details"][$formuladetailid] = $formuladetailid;
            $arr_data["list_inventory"][$inventorycode] = $inventorycode;
            
            $arr_data["details_inventorycode"][$formuladetailid] = $inventorycode;
            $arr_data["details_inventoryname"][$formuladetailid] = $inventoryname;
            $arr_data["details_percentage"][$formuladetailid] = $percentage;
            $arr_data["details_quantity"][$formuladetailid] = $quantity;
            $no++;
        }
        
        //$arr_stock = bincard_stock(date("m"), date("Y"), $arr_data["list_inventory"], "WH001", "2");
        
        $q = "
                SELECT
                    unitofmeasurement.uominitial,    
                    unitofmeasurement.description
                FROM
                    unitofmeasurement
                WHERE
                    1
                ORDER BY
                    unitofmeasurement.uominitial ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($uominitial, $description) = $row;
            
            $arr_data["list_unitofmeasurement"][$uominitial] = $uominitial;    
            
            $arr_data["unitofmeasurement_description"][$uominitial] = $description;
        }
        
        if($arr_curr["isactive"]*1==1)
        {
            $checked_active = "checked='checked'";    
        }
        else
        {
            $checked_active = "";
        }
        
        if($arr_curr["isdefault"]*1==1)
        {
            $checked_default = "checked='checked'";    
        }
        else
        {
            $checked_default = "";
        }
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Edit <?php echo $modul; ?>
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" id="action" value="edit_data_kemas">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_req" id="v_req" value="">
            <input type="hidden" name="v_appr" id="v_appr" value="">
            <input type="hidden" name="v_formulanumber" id="v_formulanumber" value="<?php echo $v_formulanumber; ?>">
            
            <input type="hidden" name="v_formulaname_old" id="v_formulaname_old" value="<?php echo $arr_curr["formulaname"]; ?>">
            <input type="hidden" name="v_quantity_old" id="v_quantity_old" value="<?php echo format_number($arr_curr["quantity"], 2); ?>">
            <input type="hidden" name="v_uominitial_old" id="v_uominitial_old" value="<?php echo $arr_curr["uominitial"]; ?>">
            <input type="hidden" name="v_packagingsize_old" id="v_packagingsize_old" value="<?php echo format_number($arr_curr["packagingsize"], 0); ?>">
            <input type="hidden" name="v_uompackaging_old" id="v_uompackaging_old" value="<?php echo $arr_curr["uompackaging"]; ?>">
            
            <table class="table table-bordered responsive">

                <tr>
                    <td class="title_table" width="250">Formula Number</td>
                    <td colspan="3">
                        <b><?php echo $arr_curr["formulanumber"]; ?></b>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Formula Name</td>
                    <td colspan="3">
                        <input type="text" class="form-control-new" name="v_formulaname" id="v_formulaname" size="50" maxlength="100" value="<?php echo $arr_curr["formulaname"]; ?>">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Finish Goods</td>
                    <td>
                        <?php 
                            echo $arr_curr["inventorycode"]." :: ".$arr_curr["inventoryname"];
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Batch Qty</td>
                    <td>
                        <input type="text" class="form-control-new" size="15" style="text-align:right;" name="v_quantity" id="v_quantity" onblur="toFormat2('v_quantity')" value="<?php echo format_number($arr_curr["quantity"], 2); ?>">
                        
                        <select name="v_uominitial" id="v_uominitial" class="form-control-new">
                          
                                        <option value="<?php echo 'GR'; ?>"><?php echo 'Gram'; ?></option>
                                    
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Packaging Size</td>
                    <td>
                        <input type="text" class="form-control-new" size="15" style="text-align:right;" name="v_packagingsize" id="v_packagingsize" onblur="toFormat('v_packagingsize')" value="<?php echo format_number($arr_curr["packagingsize"], 0); ?>">
                        
                        <select name="v_uompackaging" id="v_uompackaging" class="form-control-new">
                            <?php 
                                foreach($arr_data["list_unitofmeasurement"] as $arr_uominitial => $val)
                                {
                                    $description = $arr_data["unitofmeasurement_description"][$arr_uominitial];       
                                    
                                    $selected = "";
                                    if($arr_uominitial==$arr_curr["uompackaging"])
                                    {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                        <option <?php echo $selected; ?> value="<?php echo $arr_uominitial; ?>"><?php echo $description; ?></option>
                                    <?php
                                }
                            ?>
                            <option></option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Packaging Size Range</td>
                    <td>Min&nbsp;&nbsp;
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_packagingsize_min" id="v_packagingsize_min" onblur="toFormat('v_packagingsize_min')" value="<?php echo format_number($arr_curr["packagingsize_min"], 0); ?>">
                        &nbsp;&nbsp;Max&nbsp;&nbsp;
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_packagingsize_max" id="v_packagingsize_max" onblur="toFormat('v_packagingsize_max')" value="<?php echo format_number($arr_curr["packagingsize_max"], 0); ?>">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Std Production Time (day)</td>
                    <td>
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="`" id="v_std_production_time" value="<?php echo $arr_curr["std_production_time"]; ?>" style="text-align: right;">
                    </td>
                </tr>
                <tr>
                    <td class="title_table">Berat Finish Goods Per Pcs (Gram)</td>
                    <td>
                        <input type="text" class="form-control-new" size="8" style="text-align:right;" name="v_berat_per_pcs" id="v_berat_per_pcs" value="<?php echo $arr_curr["finishgood_wgt"]; ?> style="text-align: right;">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">&nbsp;</td>
                    <td style="font-weight:bold;">
                        <label><input type="checkbox" <?php echo $checked_active; ?> name="v_active" id="v_active">&nbsp;&nbsp;Active</label><br>
                        <label><input type="checkbox" <?php echo $checked_default; ?> name="v_default" id="v_default">&nbsp;&nbsp;Default</label>
                    </td>
                </tr>
                
               
                <tr>
                    <!-- pertama -->
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <?php 
                            if($arr_curr["pending"]==0)
                            {
                        ?>
                        
                        <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                        
                        <?php 
                            $q = "
                                    SELECT
                                        matreq.formulanumber
                                    FROM
                                        matreq
                                    WHERE
                                        matreq.formulanumber = '".$v_formulanumber."'
                                    LIMIT
                                        0,1
                            ";
                            $qry_cek = mysql_query($q);
                            $row_cek = mysql_fetch_array($qry_cek);
                            
                            if($row_cek["formulanumber"]=="")
                            {
                        ?>
                        <button type="button" onclick="confirm_delete('<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" name="btn_delete" id="btn_delete" value="Delete">Delete<i class="entypo-cancel"></i></button>
                        <?php 
                            }
                        ?>
                        
                        
                        <?php 
                            if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            {
                        ?>
                        <button type="button" onclick="confirm_approval('<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" name="btn_request" id="btn_request" value="Request Approval">Request Approval<i class="entypo-thumbs-up"></i></button>
                        <?php 
                            }
                        ?>
                        
                        <?php 
                            }
                            else
                            {
                            	//cek otorisasi_user
									$cek= "
									SELECT 
									  * 
									FROM
									  `otorisasi_user` a 
									WHERE a.`UserName` = '".$ses_login."' 
									  AND a.`Tipe` = 'approve_formula' ;
									";
									/*$cek= "
									SELECT 
									  * 
									FROM
									  `otorisasi_user` a 
									WHERE a.`UserName` = 'mecahel0101' 
									  AND a.`Tipe` = 'approve_formula' ;
									";*/
									
									//apakah ada datanya?
									$baris=mysql_query($cek);
									$count = mysql_num_rows($baris);
									if($count>0){ 
											if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            						{
									              ?>
										
						                        <button type="button" onclick="confirm_approval_formula('<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" name="btn_request" id="btn_request" value="Request Approval">Approve Formula<i class="entypo-thumbs-up"></i></button>
						                 
									          <?php }
										}else{
											echo "<font color='red'>Button Save Tidak muncul, karena status formula Pending</font>";
										}                               
                            }
                        ?>
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="100%" style="color:#FF0000;">
                        *) Setiap terjadi perubahan data, akan membutuhkan approval 1 atau approval 2<br>
                        *) Setelah melakukan perubahan, harap untuk melakukan Request Approval
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="100%">
                        <?php
                            echo menutab($v_formulanumber, 'kemas');
                        ?>
                    </td>
                </tr>
 
                <!--<tr>
                    <td colspan="100%">
                    	<button type="button" onclick="pop_up_details()" class="btn btn-info btn-icon btn-sm icon-left" name="btn_add_detail" id="btn_add_detail" value="Add Packaging">Add Packaging<i class="entypo-plus"></i></button>
                    </td>
                </tr>-->
 
                
                <tr>
                    <td colspan="100%">
	                	<table class="table table-bordered responsive">
						    <thead>
								<tr>
									<th>No Packging</th>
									<th>PCode</th>
									<th>Nama Barang</th>
									<th>Qty</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php 
                                $no = 1;
                            ?>
                            
                            <tr>
                                <td>ADD</td>
                                <td align="center">
                                    <input type="text" class="form-control-new" size="15" name="v_inventorycode_<?php echo $no; ?>" id="v_inventorycode_<?php echo $no; ?>" onkeyup="CallAjax('ajax_search_pcode_kemas', this.value, '<?php echo $no; ?>')" onblur="CallAjax('ajax_search_pcode_kemas', this.value, '<?php echo $no; ?>')">
                                    <button type="button" onclick="pop_up_kemas('<?php echo $v_formulanumber; ?>','<?php echo $no; ?>')" class="btn btn-info btn-sm sm-new" name="..." id="..." value="..." title="Search Pcode"><i class="entypo-dot-3"></i></button>
                                </td>
                                <td id="td_inventoryname_<?php echo $no; ?>">&nbsp;</td>
                                <td align="right"><input type="text" class="form-control-new" size="20" maxlength="20" name="v_quantity_<?php echo $no; ?>" id="v_quantity_<?php echo $no; ?>" onblur="toFormat('v_quantity_<?php echo $no; ?>')" value="" style="text-align:right;" ></td>
                                <td align="center">
                                	<button type="button" onclick="CallAjax('ajax_save_detail_kemas', '<?php echo $v_formulanumber; ?>')" class="btn btn-info btn-sm sm-new" value="Save" title="Save"><i class="entypo-floppy"></i></button>
                                </td>
                            </tr>
                            
                            
                            
                            
                            <?php
                                if(count($arr_data["list_details"])==0)
                                {
                                    ?>
                                        <tr>
                                            <td colspan="100%" align="center">No Data</td>
                                        </tr>
                                    <?php
                                }
                            
                                $no_echo = 1;
                                $no++; 
                                foreach($arr_data["list_details"] as $formuladetailid => $val)
                                {
                                    $inventorycode = $arr_data["details_inventorycode"][$formuladetailid];
                                    $inventoryname = $arr_data["details_inventoryname"][$formuladetailid];
                                    $quantity = $arr_data["details_quantity"][$formuladetailid];
                                    
                                    if($inventorycode!="01-00097")
                                    {
                                        
                                    }
                                    
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td>
                                            <?php echo $no_echo; ?>
                                            <input type="hidden" name="no[]" value="<?php echo $no; ?>">
                                            <input type="hidden" name="v_sid_<?php echo $no; ?>" id="v_sid_<?php echo $no; ?>" value="<?php echo $formuladetailid; ?>">
                                            <input type="hidden" name="v_inventorycode_old_<?php echo $no; ?>" id="v_inventorycode_old_<?php echo $no; ?>" value="<?php echo $inventorycode; ?>">
                                            <input type="hidden" name="v_quantity_old_<?php echo $no; ?>" id="v_quantity_old_<?php echo $no; ?>" value="<?php echo format_number($quantity,0); ?>">
                                        </td>
                                        <td align="center">
                                            <input type="text" class="form-control-new" size="15" name="v_inventorycode_<?php echo $no; ?>" id="v_inventorycode_<?php echo $no; ?>" value="<?php echo $inventorycode; ?>" onkeyup="CallAjax('ajax_search_pcode_kemas', this.value, '<?php echo $no; ?>')" onblur="CallAjax('ajax_search_pcode_kemas', this.value, '<?php echo $no; ?>')">
                                           	<button type="button" onclick="pop_up_kemas('<?php echo $v_formulanumber; ?>','<?php echo $no; ?>')" class="btn btn-info btn-sm sm-new" value="..." title="Search Pcode"><i class="entypo-dot-3"></i></button>
                                        </td>
                                        <td id="td_inventoryname_<?php echo $no; ?>"><?php echo $inventoryname; ?></td>
                                        <td align="right"><input type="text" class="form-control-new" size="20" maxlength="20" name="v_quantity_<?php echo $no; ?>" id="v_quantity_<?php echo $no; ?>" onblur="toFormat('v_quantity_<?php echo $no; ?>')" value="<?php echo format_number($quantity, 0); ?>" style="text-align:right;"></td>
                                        <?php 
										if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            			{
									    ?>
                                        <td align="center"><input type="checkbox" name="v_del_details[]" id="v_del_details_<?php echo $no; ?>" value="<?php echo $formuladetailid; ?>"></td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                    
                                    $arr_total["quantity"] += $quantity;
                                    $no++;
                                    $no_echo++;
                                }
                                
                                $no = $no-1;    
                            ?>
                            
	                            <tr height="25" style="font-weight:bold;">
	                                <td colspan="3">&nbsp;</td>
	                                <td align="right">&nbsp;</td>
	                                <td align="center">
                                        <?php 
										if($arr_curr["approval_status_1"]==0 && $arr_curr["approval_status_2"]==0)
                            			{
									    ?>
	                                	<button type="submit" name="btn_delete_details" id="btn_delete_details" class="btn btn-info btn-sm sm-new" value="Del" title="Delete"><i class="entypo-trash"></i></button>
                                        <?php } ?>
	                                </td>
	                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        
        </div>
        <?php    
    }
    else if($ajax=="ajax_search_fg")
    {
        $v_keyword_fg = $_GET["v_keyword_fg"];
        
        $arr_keyword[0] = "masterbarang.PCode";
        $arr_keyword[1] = "masterbarang.NamaLengkap";
        
        $where_keyword = search_keyword($v_keyword_fg, $arr_keyword);
        
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                FROM
                    masterbarang
                WHERE
                    1
                    AND masterbarang.KdKategori = '3'
                    ".$where_keyword."
                ORDER BY
                    masterbarang.NamaLengkap ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($PCode, $NamaLengkap) = $row;
            
            $arr_data["list_fg"][$PCode] = $PCode;
            
            $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
        }
        
        ?>
        
        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:250px;">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_fg"] as $PCode => $val)
                {
                    $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                    ?>
                    
                    <option value="<?php echo $PCode; ?>"><?php echo $NamaLengkap; ?></option>
                    <?php
                }
            ?>
        </select>
        <?php
    }
    else if($ajax=="ajax_search_copy_formula")
    {
        $v_keyword_copy_formula = $_GET["v_keyword_copy_formula"];
        
        $arr_keyword[0] = "formula.formulanumber";
        $arr_keyword[1] = "formula.formulaname";
        
        $where_keyword = search_keyword($v_keyword_copy_formula, $arr_keyword);
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname
                FROM
                    formula
                WHERE
                    1
                    ".$where_keyword."
                ORDER BY
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname) = $row;
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            $arr_data["formulaname"][$formulanumber] = $formulaname;
        }
        
        ?>
        
        <select name="v_formulanumber_copy" id="v_formulanumber_copy" class="form-control-new" style="width:250px;">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber => $val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    ?>
                    
                    <option value="<?php echo $formulanumber; ?>"><?php echo $formulanumber." :: ".$formulaname; ?></option>
                    <?php
                }
            ?>
        </select>
        <?php
    }
    else if($ajax=="ajax_search_pcode_baku")
    {
        $v_inventorycode = save_char($_GET["v_inventorycode"]);
        
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                FROM
                    masterbarang
                WHERE
                    1
                    AND masterbarang.PCode = '".$v_inventorycode."'
                    AND masterbarang.KdKategori = '1'
                ORDER BY
                    masterbarang.PCode ASC
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($PCode, $NamaLengkap) = $row;
        
        if($NamaLengkap)
        {
            echo "<b>".$NamaLengkap."</b>";     
        }
        else
        {
            echo "<b>Nama Barang Tidak ditemukan...</b>";
        }
        
    }
    else if($ajax=="ajax_search_pcode_kemas")
    {
        $v_inventorycode = save_char($_GET["v_inventorycode"]);
        
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                FROM
                    masterbarang
                WHERE
                    1
                    AND masterbarang.PCode = '".$v_inventorycode."'
                    AND masterbarang.KdKategori = '2'
                ORDER BY
                    masterbarang.PCode ASC
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($PCode, $NamaLengkap) = $row;
        
        if($NamaLengkap)
        {
            echo "<b>".$NamaLengkap."</b>";     
        }
        else
        {
            echo "<b>Nama Barang Tidak ditemukan...</b>";
        }
        
    }
    else if($ajax=="ajax_save_detail_baku")
    {
    	
        $v_formulanumber = save_char($_GET["v_formulanumber"]);    
        $v_inventorycode = save_char($_GET["v_inventorycode"]); 
        $v_percentage = save_int($_GET["v_percentage"]); 
        $v_quantity = save_int($_GET["v_quantity"]);
        //echo $v_quantity;die;
        /*$q = "
                INSERT INTO 
                    formuladetail
                SET
                    formulanumber = '".$v_formulanumber."' , 
                    inventorycode = '".$v_inventorycode."' , 
                    percentage = '".$v_percentage."' , 
                    quantity = '0' , 
                    adduser = '".GetUserLogin()."',
                    adddate = NOW(),
                    edituser = '".GetUserLogin()."',
                    editdate = NOW()
        ";*/
        
        $q = "
                INSERT INTO 
                    formuladetail
                SET
                    formulanumber = '".$v_formulanumber."' , 
                    inventorycode = '".$v_inventorycode."' , 
                    percentage = '".$v_percentage."' , 
                    quantity = '".$v_quantity."' ,
                    jenis='R', 
                    adduser = '',
                    adddate = NOW(),
                    edituser = '',
                    editdate = NOW()
        ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal menyimpan";
        }
        else
        {
            update_approval($v_formulanumber);
        }

    }
    else if($ajax=="ajax_save_detail_kemas")
    {
        $v_formulanumber = save_char($_GET["v_formulanumber"]);    
        $v_inventorycode = save_char($_GET["v_inventorycode"]); 
        $v_quantity = save_int($_GET["v_quantity"]); 
        
        /*$q = "
                INSERT INTO 
                    formuladetail
                SET
                    formulanumber = '".$v_formulanumber."' , 
                    inventorycode = '".$v_inventorycode."' , 
                    percentage = '0' , 
                    quantity = '".$v_quantity."' , 
                    adduser = '".GetUserLogin()."',
                    adddate = NOW(),
                    edituser = '".GetUserLogin()."',
                    editdate = NOW()
        ";*/
        $q = "
                INSERT INTO 
                    formuladetail
                SET
                    formulanumber = '".$v_formulanumber."' , 
                    inventorycode = '".$v_inventorycode."' , 
                    percentage = '0' , 
                    quantity = '".$v_quantity."' ,
                    jenis='P', 
                    adduser = '',
                    adddate = NOW(),
                    edituser = '',
                    editdate = NOW()
        ";
        
        if(!mysql_query($q))      
        {
            $msg = "Gagal menyimpan";
        }
        else
        {
            update_approval($v_formulanumber);
        }

    }
    
    exit();
}
 

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
	
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :
            {
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                
                if(!isset($_POST["v_type"])){ $v_type = isset($_POST["v_type"]); } else { $v_type = $_POST["v_type"]; }
                if(!isset($_POST["v_formulanumber_copy"])){ $v_formulanumber_copy = isset($_POST["v_formulanumber_copy"]); } else { $v_formulanumber_copy = $_POST["v_formulanumber_copy"]; }
                
                if(!isset($_POST["v_formulaname"])){ $v_formulaname = isset($_POST["v_formulaname"]); } else { $v_formulaname = $_POST["v_formulaname"]; }
                if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }
                
                if(!isset($_POST["v_quantity"])){ $v_quantity = isset($_POST["v_quantity"]); } else { $v_quantity = $_POST["v_quantity"]; }
                if(!isset($_POST["v_uominitial"])){ $v_uominitial = isset($_POST["v_uominitial"]); } else { $v_uominitial = $_POST["v_uominitial"]; }
                if(!isset($_POST["v_packagingsize"])){ $v_packagingsize = isset($_POST["v_packagingsize"]); } else { $v_packagingsize = $_POST["v_packagingsize"]; }
                if(!isset($_POST["v_uompackaging"])){ $v_uompackaging = isset($_POST["v_uompackaging"]); } else { $v_uompackaging = $_POST["v_uompackaging"]; }
                
                if(!isset($_POST["v_active"])){ $v_active = isset($_POST["v_active"]); } else { $v_active = $_POST["v_active"]; }
                if(!isset($_POST["v_default"])){ $v_default = isset($_POST["v_default"]); } else { $v_default = $_POST["v_default"]; }
                
                if(!isset($_POST["v_packagingsize_min"])){ $v_packagingsize_min = isset($_POST["v_packagingsize_min"]); } else { $v_packagingsize_min = $_POST["v_packagingsize_min"]; }
                if(!isset($_POST["v_packagingsize_max"])){ $v_packagingsize_max = isset($_POST["v_packagingsize_max"]); } else { $v_packagingsize_max = $_POST["v_packagingsize_max"]; }
                
                if(!isset($_POST["v_std_production_time"])){ $v_std_production_time = isset($_POST["v_std_production_time"]); } else { $v_std_production_time = $_POST["v_std_production_time"]; }
                if(!isset($_POST["v_berat_per_pcs"])){ $v_berat_per_pcs = isset($_POST["v_berat_per_pcs"]); } else { $v_berat_per_pcs = $_POST["v_berat_per_pcs"]; }
                
                $v_formulaname         = save_char($v_formulaname);
                $v_quantity            = save_int($v_quantity);
                $v_packagingsize       = save_int($v_packagingsize);
                $v_packagingsize_min   = save_int($v_packagingsize_min);
                $v_packagingsize_max   = save_int($v_packagingsize_max);
                $v_std_production_time = save_int($v_std_production_time);
                $v_berat_per_pcs = save_int($v_berat_per_pcs);
                
                if($v_active=="on")
                {
                    $v_active_save = 1;
                }
                else
                {
                    $v_active_save = 0;
                }
                
                if($v_default=="on")
                {
                    $v_default_save = 1;
                }
                else
                {
                    $v_default_save = 0;
                }
                
                ## buat counter
                {
                    // FR00036/05/08 
                    $q = "
                            SELECT
                                formula.formulanumber
                            FROM
                                formula
                            WHERE
                                1
                                AND SUBSTR(formula.formulanumber, 9, 2) = '".date("m")."'
                                AND SUBSTR(formula.formulanumber, 12, 2) = '".date("y")."'
                            ORDER BY
                                formula.formulanumber DESC
                            LIMIT
                                0,1
                    ";
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry);
                    
                    $counter = (substr($row["formulanumber"],2,5)*1)+1;
                    
                    
                    
                    $v_formulanumber = "FR".sprintf("%05s", $counter)."/".date("m")."/".date("y");
                    //echo $v_formulanumber;
                }
                
                if($v_type=="Reguler")
                {
                    if($v_formulaname=="")
                    {
                        $msg = "Formula Name harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_inventorycode=="")
                    {
                        $msg = "Finish Goods harus dipilih";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_quantity*1==0)
                    {
                        $msg = "Batch Qty harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_packagingsize*1==0)
                    {
                        $msg = "Packaging Size harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_packagingsize_min*1==0)
                    {
                        $msg = "Packaging Size Range Min harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_packagingsize_max*1==0)
                    {
                        $msg = "Packaging Size Range Max harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_std_production_time*1==0)
                    {
                        $msg = "Std Production Time harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_berat_per_pcs*1==0)
                    {
                        $msg = "Beat Per Pcs harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    
                    if($v_default_save==1 && $v_active_save==0)
                    {
                        $msg = "Jika Formula ini menjadi Default, harap Status nya menjadi Active Juga";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    
                    $q = "
                            INSERT INTO 
                                formula 
                            SET
                                formulanumber = '".$v_formulanumber."' , 
                                formulaname = '".$v_formulaname."' , 
                                quantity = '".$v_quantity."' , 
                                uominitial = '".$v_uominitial."' , 
                                adduser = '".$ses_login."' , 
                                ADDDATE = NOW() , 
                                edituser = '".$ses_login."' , 
                                editdate = NOW() , 
                                packagingsize = '".$v_packagingsize."' , 
                                uompackaging = '".$v_uompackaging."' , 
                                inventorycode = '".$v_inventorycode."' , 
                                isdefault = '".$v_default_save."' , 
                                isactive = '".$v_active_save."' , 
                                approvedBy = '' , 
                                TglApprove = '' , 
                                approval_status_1 = '0' , 
                                approval_status_2 = '0' , 
                                approval_date_1 = '' , 
                                approval_date_2 = '' , 
                                archive = '0' , 
                                pending = '0',
                                packagingsize_min = '".$v_packagingsize_min."', 
                                packagingsize_max = '".$v_packagingsize_max."', 
                                std_production_time = '".$v_std_production_time."',
                                finishgood_wgt = '".$v_berat_per_pcs."'
                                
                    ";
                    
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else
                    {
                        if($v_default_save==1)
                        {
                            $formula_update = "";
                            $q = "
                                    SELECT
                                        formula.formulanumber
                                    FROM
                                        formula
                                    WHERE
                                        1
                                        AND formula.formulanumber != '".$v_formulanumber."'
                                        AND formula.isdefault = '1'
                                    ORDER BY
                                        formula.formulanumber ASC
                            ";
                            $qry = mysql_query($q);
                            while($row = mysql_fetch_array($q))
                            {
                                list($formulanumber) = $row;
                                
                                $formula_update .= $formulanumber.", ";    
                                
                                $upd = "
                                    UPDATE
                                        formula
                                    SET
                                        isdefault = '0'
                                    WHERE
                                        formulanumber = '".$formulanumber."'
                                ";
                                if(!mysql_query($upd))
                                {
                                    $msg = "Gagal Update default 0";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();    
                                }
                            }
                            
                            $formula_update = substr($formula_update, 0, -2);
                            
                            if($formula_update)
                            {
                                $msg = "Terdapat Formula ".$formula_update." Yang terupdate tidak menjadi default, karena Formula ".$v_formulanumber." menjadi Default";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                        }
                        
                        echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_baku');</script>";         
                        die();    
                    }
                }
                else if($v_type=="Copy") 
                {
                    if($v_formulanumber_copy=="")
                    {
                        $msg = "Formula Copy harus dipilih";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_formulaname=="")
                    {
                        $msg = "Formula Name harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_inventorycode=="")
                    {
                        $msg = "Finish Goods harus dipilih";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_packagingsize_min*1==0)
                    {
                        $msg = "Packaging Size Range Min harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_packagingsize_max*1==0)
                    {
                        $msg = "Packaging Size Range Max harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_std_production_time*1==0)
                    {
                        $msg = "Std Production Time harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    else if($v_berat_per_pcs*1==0)
                    {
                        $msg = "Std Production Time harus diisi";
                        echo "<script>alert('".$msg."');</script>";
                        die();        
                    }
                    
                    $q = "
                            INSERT INTO 
                                `vci`.`formula`
                                (
                                    `formulanumber`,
                                     `formulaname`,
                                     `quantity`,
                                     `uominitial`,
                                     `adduser`,
                                     `adddate`,
                                     `edituser`,
                                     `editdate`,
                                     `packagingsize`,
                                     `uompackaging`,
                                     `inventorycode`,
                                     `isdefault`,
                                     `isactive`,
                                     `approvedBy`,
                                     `TglApprove`,
                                     `approval_status_1`,
                                     `approval_status_2`,
                                     `approval_date_1`,
                                     `approval_date_2`,
                                     `archive`,
                                     `pending`,
                                     `packagingsize_min`,
                                     `packagingsize_max`,
                                     `std_production_time`
                                )
                            SELECT
                                '".$v_formulanumber."' AS `formulanumber`,
                                '".$v_formulaname."' AS `formulaname`,
                                `quantity`,
                                `uominitial`,
                                '".$ses_login."' AS `adduser`,
                                '".date("Y-m-d H:i:s")."' AS `adddate`,
                                '".$ses_login."' AS `edituser`,
                                '".date("Y-m-d H:i:s")."' AS `editdate`,
                                `packagingsize`,
                                `uompackaging`,
                                '".$v_inventorycode."' AS `inventorycode`,
                                '0' AS `isdefault`,
                                '0' AS `isactive`,
                                `approvedBy`,
                                `TglApprove`,
                                '0' AS `approval_status_1`,
                                '0' AS `approval_status_2`,
                                '0000-00-00' AS `approval_date_1`,
                                '0000-00-00' AS `approval_date_2`,
                                '0' AS `archive`,
                                '0' AS `pending`, 
                                `packagingsize_min`,
                                `packagingsize_max`,
                                `std_production_time`
                            FROM
                                formula
                            WHERE
                                1
                                AND formula.formulanumber = '".$v_formulanumber_copy."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Copy Formula";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else
                    {
                        $q = "
                                INSERT INTO 
                                    `vci`.`formuladetail`
                                    (
                                         `formulanumber`,
                                         `inventorycode`,
                                         `percentage`,
                                         `quantity`,
                                         `adduser`,
                                         `adddate`,
                                         `edituser`,
                                         `editdate`
                                    )
                                SELECT
                                         '".$v_formulanumber."' AS `formulanumber`,
                                         `inventorycode`,
                                         `percentage`,
                                         `quantity`,
                                         '".GetUserLogin()."' AS `adduser`,
                                        '".date("Y-m-d H:i:s")."' AS `adddate`,
                                        '".GetUserLogin()."' AS `edituser`,
                                        '".date("Y-m-d H:i:s")."' AS `editdate`
                                FROM
                                        `vci`.`formuladetail`
                                WHERE
                                    1
                                    AND `vci`.`formuladetail`.`formulanumber` = '".$v_formulanumber_copy."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Copy Formula Detail";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_baku');</script>";         
                        die();    
                    }
                    
                        
                }

            }
            break;
        case "edit_data_baku" :
            {
				//echo "<pre>";print_r($_POST);echo "</pre>";die;
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }
                if(!isset($_POST["v_req"])){ $v_req = isset($_POST["v_req"]); } else { $v_req = $_POST["v_req"]; }
                if(!isset($_POST["v_appr"])){ $v_appr = isset($_POST["v_appr"]); } else { $v_appr = $_POST["v_appr"]; }
                if(!isset($_POST["btn_delete_details"])){ $btn_delete_details = isset($_POST["btn_delete_details"]); } else { $btn_delete_details = $_POST["btn_delete_details"]; }
                if(!isset($_POST["v_del_details"])){ $v_del_details = isset($_POST["v_del_details"]); } else { $v_del_details = $_POST["v_del_details"]; }
                
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_formulaname"])){ $v_formulaname = isset($_POST["v_formulaname"]); } else { $v_formulaname = $_POST["v_formulaname"]; }
                if(!isset($_POST["v_quantity"])){ $v_quantity = isset($_POST["v_quantity"]); } else { $v_quantity = $_POST["v_quantity"]; }
                if(!isset($_POST["v_uominitial"])){ $v_uominitial = isset($_POST["v_uominitial"]); } else { $v_uominitial = $_POST["v_uominitial"]; }
                if(!isset($_POST["v_packagingsize"])){ $v_packagingsize = isset($_POST["v_packagingsize"]); } else { $v_packagingsize = $_POST["v_packagingsize"]; }
                if(!isset($_POST["v_uompackaging"])){ $v_uompackaging = isset($_POST["v_uompackaging"]); } else { $v_uompackaging = $_POST["v_uompackaging"]; }
                
                if(!isset($_POST["v_formulaname_old"])){ $v_formulaname_old = isset($_POST["v_formulaname_old"]); } else { $v_formulaname_old = $_POST["v_formulaname_old"]; }
                if(!isset($_POST["v_quantity_old"])){ $v_quantity_old = isset($_POST["v_quantity_old"]); } else { $v_quantity_old = $_POST["v_quantity_old"]; }
                if(!isset($_POST["v_uominitial_old"])){ $v_uominitial_old = isset($_POST["v_uominitial_old"]); } else { $v_uominitial_old = $_POST["v_uominitial_old"]; }
                if(!isset($_POST["v_packagingsize_old"])){ $v_packagingsize_old = isset($_POST["v_packagingsize_old"]); } else { $v_packagingsize_old = $_POST["v_packagingsize_old"]; }
                if(!isset($_POST["v_uompackaging_old"])){ $v_uompackaging_old = isset($_POST["v_uompackaging_old"]); } else { $v_uompackaging_old = $_POST["v_uompackaging_old"]; }
                
                if(!isset($_POST["v_active_old"])){ $v_active_old = isset($_POST["v_active_old"]); } else { $v_active_old = $_POST["v_active_old"]; }
                if(!isset($_POST["v_active"])){ $v_active = isset($_POST["v_active"]); } else { $v_active = $_POST["v_active"]; }
                
                if(!isset($_POST["v_default_old"])){ $v_default_old = isset($_POST["v_default_old"]); } else { $v_default_old = $_POST["v_default_old"]; }
                if(!isset($_POST["v_default"])){ $v_default = isset($_POST["v_default"]); } else { $v_default = $_POST["v_default"]; }
                
                if(!isset($_POST["v_packagingsize_min"])){ $v_packagingsize_min = isset($_POST["v_packagingsize_min"]); } else { $v_packagingsize_min = $_POST["v_packagingsize_min"]; }
                if(!isset($_POST["v_packagingsize_max"])){ $v_packagingsize_max = isset($_POST["v_packagingsize_max"]); } else { $v_packagingsize_max = $_POST["v_packagingsize_max"]; }
                
                if(!isset($_POST["v_std_production_time"])){ $v_std_production_time = isset($_POST["v_std_production_time"]); } else { $v_std_production_time = $_POST["v_std_production_time"]; }
                if(!isset($_POST["v_berat_per_pcs"])){ $v_berat_per_pcs = isset($_POST["v_berat_per_pcs"]); } else { $v_berat_per_pcs = $_POST["v_berat_per_pcs"]; }
                
                $v_formulaname         = save_char($v_formulaname);
                $v_formulaname_old     = save_char($v_formulaname_old);
                $v_quantity            = save_int($v_quantity);
                $v_packagingsize       = save_int($v_packagingsize);
                $v_quantity_old        = save_int($v_quantity_old);
                $v_packagingsize_old   = save_int($v_packagingsize_old);
                $v_packagingsize_min   = save_int($v_packagingsize_min);
                $v_packagingsize_max   = save_int($v_packagingsize_max);
                $v_std_production_time = save_int($v_std_production_time);
                $v_berat_per_pcs = save_int($v_berat_per_pcs);
                
                if($v_active=="on")
                {
                    $v_active_save = 1;
                }
                else
                {
                    $v_active_save = 0;
                }
                
                if($v_default=="on")
                {
                    $v_default_save = 1;
                }
                else
                {
                    $v_default_save = 0;
                }
                
                if($v_default != $v_default_old)
                {
                    if($v_default_save==1 && $v_active_save==0)
                    {
                        $msg = "Jika Formula ini menjadi Default, harap Status nya menjadi Active Juga";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                } 
                
                if($v_del==1)
                {
                    $q = "
                            SELECT
                                matreq.formulanumber
                            FROM
                                matreq
                            WHERE
                                matreq.formulanumber = '".$v_formulanumber."'
                            LIMIT
                                0,1
                    ";
                    $qry_cek = mysql_query($q);
                    $row_cek = mysql_fetch_array($qry_cek);
                    
                    if($row_cek["formulanumber"])
                    {
                        $msg = "Gagal menghapus, karena formula sudah digunakan";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search');</script>";         
                        die();    
                    }
                    else
                    {
                        $del = "
                                DELETE
                                    formula_log
                                FROM 
                                    formula_log
                                WHERE
                                    1
                                    AND formula_log.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus header log";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        /*$del = "
                                DELETE
                                    formuladetail_log
                                FROM 
                                    formuladetail_log
                                WHERE
                                    1
                                    AND formuladetail_log.formulanumber = '".$v_formulanumber."'
                        ";*/
                         $del = "
                                DELETE
                                    formuladetail
                                FROM 
                                    formuladetail
                                WHERE
                                    1
                                    AND formuladetail.formulanumber = '".$v_formulanumber."'
                        ";
                       
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus detail log 2";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        $del = "
                                DELETE
                                    formuladetail
                                FROM 
                                    formuladetail
                                WHERE
                                    1
                                    AND formuladetail.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus detail";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        
                        $del = "
                                DELETE
                                    formula
                                FROM 
                                    formula
                                WHERE
                                    1
                                    AND formula.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        else
                        {
                            $msg = "Berhasil menghapus";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search');</script>";         
                            die();    
                        } 
                    }
                }
                else if($v_req==1)
                {
                	//echo "Hello 1";die;
                    //send_approval($v_formulanumber, "edit");
                    //send_email_formula_req_approval($v_formulanumber);
                    
                    $subject = "Alert Approval Formula";
                        
                        $body = '
								<table align="center" cellpadding="0" cellspacing="0" width="50%" style="border: 3px solid #C1CDD8; ">
									<tbody>

										<tr style="background: #bad0ee; height: 50;">
											<td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
												<p style="text-transform: uppercase;">
													Alert Permohonan Approval Formula
												</p>
											</td>
										</tr>

										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										
										<tr height="25">
											<td colspan="3" style="padding: 0 0 0 10px;">
												Dear Bapak / Ibu,
												<br/><br/>
												Mohon Menyetujui Formula yang telah diajukan.<br/><br/>
												Anda dapat melihatnya dengan <a href="http://sys.bebektimbungan.com/produksi/formula.php" target="_blank">KLIK DISINI</a><br/>
												Atau copy URL berikut di broswer anda :<br/>
												http://sys.bebektimbungan.com/produksi/formula.php
											</td>
										</tr>

										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>

									</tbody>
								</table>
							';
                        
                        $to = "";
                        $to_name = "";
                        $sql = "
                                SELECT
                                    ".$db["master"].".function_email.email_address,
                                    ".$db["master"].".function_email.email_name
                                FROM
                                    ".$db["master"].".function_email
                                WHERE
                                    1
                                    AND ".$db["master"].".function_email.func_name = 'approve_formula'
                                ORDER BY
                                    ".$db["master"].".function_email.email_address ASC
                        ";
                        $qry = mysql_query($sql);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($email_address, $email_name) = $row;
                            
                            $to .= $email_address.";";
                            $to_name .= $email_name.";";
                        }
                        
                        $author = "Approval Formula";
						
                        //ini yang akan mengirim email
                        //$kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);
                    
                    $q = "
                            UPDATE
                                formula
                            SET
                                pending = '1'
                            WHERE
                                1
                                AND formula.formulanumber = '".$v_formulanumber."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal update pending";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    
                    $msg = "Berhasil mengirim Request Approval";
                    echo "<script>alert('".$msg."');</script>"; 
                    echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_baku');</script>";         
                    die();
                } 
                else if($v_appr==1)
                {
                	//echo "Hello 1";die;
                    //send_approval($v_formulanumber, "edit");
                    //send_email_formula_req_approval($v_formulanumber);
					
					   
                     
                    
                    $q = "
                            UPDATE
                                formula
                            SET
                                approval_status_1 = '1',
                                approval_status_2 = '1',
                                approval_date_1='".date('Y-m-d')."',
                                approval_date_2='".date('Y-m-d')."',
                                pending = '1'
                            WHERE
                                1
                                AND formula.formulanumber = '".$v_formulanumber."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Approve Formula";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    
                    $msg = "Berhasil Approve Formula";
                    echo "<script>alert('".$msg."');</script>"; 
                    echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_baku');</script>";         
                    die();
                }
                else
                {
                    if($btn_delete_details)
                    {
                        foreach($v_del_details as $key => $val)
                        {
                            $q = "
                                    DELETE FROM
                                        formuladetail
                                    WHERE
                                        formuladetailid = '".$val."'    
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menghapus";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                        }
                        
                        $msg = "Berhasil menghapus";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_baku');</script>";         
                        die();    
                    }
                    else
                    {
                        $upd = "
                                UPDATE 
                                    formula 
                                SET
                                    formulaname = '".$v_formulaname."' , 
                                    quantity = '".$v_quantity."' , 
                                    uominitial = '".$v_uominitial."' , 
                                    packagingsize = '".$v_packagingsize."' , 
                                    uompackaging = '".$v_uompackaging."' , 
                                    isactive = '".$v_active_save."' , 
                                    isdefault = '".$v_default_save."' , 
                                    packagingsize_min = '".$v_packagingsize_min."' , 
                                    packagingsize_max = '".$v_packagingsize_max."' , 
                                    std_production_time = '".$v_std_production_time."', 
                                    finishgood_wgt = '".$v_berat_per_pcs."', 
                                    edituser = '".GetUserLogin()."' , 
                                    editdate = NOW() 
                                WHERE
                                    formulanumber = '".$v_formulanumber."'         
                        ";
                        if(!mysql_query($upd))
                        {
                            $msg = "Gagal menyimpan";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        else
                        {
                            if(!isset($_POST["no"])){ $no = isset($_POST["no"]); } else { $no = $_POST["no"]; }
                            
                            if($v_quantity!=$v_quantity_old || $v_uominitial!=$v_uominitial_old || $v_packagingsize!=$v_packagingsize_old || $v_uompackaging!=$v_uompackaging_old)
                            {
                                update_approval($v_formulanumber);
                            }
                            
                            $total_percentage = 0;
                            foreach($no as $key => $val)
                            {
                                if(!isset($_POST["v_percentage_".$val])){ $v_percentage = isset($_POST["v_percentage_".$val]); } else { $v_percentage = $_POST["v_percentage_".$val]; }
                                
                                $total_percentage += save_int($v_percentage);
                            }
                            
                            if(save_int($total_percentage*1)!=100)
                            {
                                $msg = "Total Percentage harus 100% = ".$total_percentage;
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            //else
                            //{
                            //    $msg = "OKE = ".$total_percentage;
                            //    echo "<script>alert('".$msg."');</script>";
                            //    die();    
                            //}
                            
                            foreach($no as $key => $val)
                            {
                                if(!isset($_POST["v_sid_".$val])){ $v_sid = isset($_POST["v_sid_".$val]); } else { $v_sid = $_POST["v_sid_".$val]; }
                                if(!isset($_POST["v_inventorycode_".$val])){ $v_inventorycode = isset($_POST["v_inventorycode_".$val]); } else { $v_inventorycode = $_POST["v_inventorycode_".$val]; }
                                if(!isset($_POST["v_inventorycode_old_".$val])){ $v_inventorycode_old = isset($_POST["v_inventorycode_old_".$val]); } else { $v_inventorycode_old = $_POST["v_inventorycode_old_".$val]; }
                                
                                if(!isset($_POST["v_percentage_".$val])){ $v_percentage = isset($_POST["v_percentage_".$val]); } else { $v_percentage = $_POST["v_percentage_".$val]; }
                                if(!isset($_POST["v_quantity_".$val])){ $v_quantity = isset($_POST["v_quantity_".$val]); } else { $v_quantity = $_POST["v_quantity_".$val]; }
                                if(!isset($_POST["v_percentage_old_".$val])){ $v_percentage_old = isset($_POST["v_percentage_old_".$val]); } else { $v_percentage_old = $_POST["v_percentage_old_".$val]; }
                                
                                $v_percentage     = save_int($v_percentage);
                                $v_percentage_old = save_int($v_percentage_old);

                                if($v_percentage!=$v_percentage_old || $v_inventorycode!=$v_inventorycode_old)
                                {
                                    update_approval($v_formulanumber);
                                    
                                    if($v_sid)
                                    {
                                        $upd = "
                                                UPDATE
                                                    formuladetail
                                                SET
                                                    inventorycode = '".$v_inventorycode."',
                                                    percentage = '".$v_percentage."',
                                                    editdate = NOW(),
                                                    edituser = '".GetUserLogin()."'
                                                WHERE
                                                    1
                                                    AND formuladetailid = '".$v_sid."'
                                        ";
                                        if(!mysql_query($upd))
                                        {
                                            $msg = "Gagal menyimpan details";
                                            echo "<script>alert('".$msg."');</script>";
                                            die();    
                                        }
                                    }
                                }
                            }
                            
                            if($v_default!=$v_default_old)
                            {
                                if($v_default_save==1)
                                {
                                    $formula_update = "";
                                    $q = "
                                            SELECT
                                                formula.formulanumber
                                            FROM
                                                formula
                                            WHERE
                                                1
                                                AND formula.formulanumber != '".$v_formulanumber."'
                                                AND formula.isdefault = '1'
                                            ORDER BY
                                                formula.formulanumber ASC
                                    ";
                                    $qry = mysql_query($q);
                                    while($row = mysql_fetch_array($q))
                                    {
                                        list($formulanumber) = $row;
                                        
                                        $formula_update .= $formulanumber.", ";    
                                        
                                        $upd = "
                                            UPDATE
                                                formula
                                            SET
                                                isdefault = '0'
                                            WHERE
                                                formulanumber = '".$formulanumber."'
                                        ";
                                        if(!mysql_query($upd))
                                        {
                                            $msg = "Gagal Update default 0";
                                            echo "<script>alert('".$msg."');</script>";
                                            die();    
                                        }
                                    }
                                    
                                    $formula_update = substr($formula_update, 0, -2);
                                    
                                    if($formula_update)
                                    {
                                        $msg = "Terdapat Formula ".$formula_update." Yang terupdate tidak menjadi default, karena Formula ".$v_formulanumber." menjadi Default";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();    
                                    }
                                }
                            }
                            
                            $msg = "Berhasil menyimpan";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_baku');</script>";         
                            die();    
                        }
                    }

                }

            }
            break;
        case "edit_data_kemas" :
            {
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }
                if(!isset($_POST["v_req"])){ $v_req = isset($_POST["v_req"]); } else { $v_req = $_POST["v_req"]; }
                if(!isset($_POST["v_appr"])){ $v_appr = isset($_POST["v_appr"]); } else { $v_appr = $_POST["v_appr"]; }
                if(!isset($_POST["btn_delete_details"])){ $btn_delete_details = isset($_POST["btn_delete_details"]); } else { $btn_delete_details = $_POST["btn_delete_details"]; }
                if(!isset($_POST["v_del_details"])){ $v_del_details = isset($_POST["v_del_details"]); } else { $v_del_details = $_POST["v_del_details"]; }
                
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_formulaname"])){ $v_formulaname = isset($_POST["v_formulaname"]); } else { $v_formulaname = $_POST["v_formulaname"]; }
                if(!isset($_POST["v_quantity"])){ $v_quantity = isset($_POST["v_quantity"]); } else { $v_quantity = $_POST["v_quantity"]; }
                if(!isset($_POST["v_uominitial"])){ $v_uominitial = isset($_POST["v_uominitial"]); } else { $v_uominitial = $_POST["v_uominitial"]; }
                if(!isset($_POST["v_packagingsize"])){ $v_packagingsize = isset($_POST["v_packagingsize"]); } else { $v_packagingsize = $_POST["v_packagingsize"]; }
                if(!isset($_POST["v_uompackaging"])){ $v_uompackaging = isset($_POST["v_uompackaging"]); } else { $v_uompackaging = $_POST["v_uompackaging"]; }
                
                if(!isset($_POST["v_formulaname_old"])){ $v_formulaname_old = isset($_POST["v_formulaname_old"]); } else { $v_formulaname_old = $_POST["v_formulaname_old"]; }
                if(!isset($_POST["v_quantity_old"])){ $v_quantity_old = isset($_POST["v_quantity_old"]); } else { $v_quantity_old = $_POST["v_quantity_old"]; }
                if(!isset($_POST["v_uominitial_old"])){ $v_uominitial_old = isset($_POST["v_uominitial_old"]); } else { $v_uominitial_old = $_POST["v_uominitial_old"]; }
                if(!isset($_POST["v_packagingsize_old"])){ $v_packagingsize_old = isset($_POST["v_packagingsize_old"]); } else { $v_packagingsize_old = $_POST["v_packagingsize_old"]; }
                if(!isset($_POST["v_uompackaging_old"])){ $v_uompackaging_old = isset($_POST["v_uompackaging_old"]); } else { $v_uompackaging_old = $_POST["v_uompackaging_old"]; }
                
                if(!isset($_POST["v_active_old"])){ $v_active_old = isset($_POST["v_active_old"]); } else { $v_active_old = $_POST["v_active_old"]; }
                if(!isset($_POST["v_active"])){ $v_active = isset($_POST["v_active"]); } else { $v_active = $_POST["v_active"]; }
                
                if(!isset($_POST["v_default_old"])){ $v_default_old = isset($_POST["v_default_old"]); } else { $v_default_old = $_POST["v_default_old"]; }
                if(!isset($_POST["v_default"])){ $v_default = isset($_POST["v_default"]); } else { $v_default = $_POST["v_default"]; }
                
                if(!isset($_POST["v_packagingsize_min"])){ $v_packagingsize_min = isset($_POST["v_packagingsize_min"]); } else { $v_packagingsize_min = $_POST["v_packagingsize_min"]; }
                if(!isset($_POST["v_packagingsize_max"])){ $v_packagingsize_max = isset($_POST["v_packagingsize_max"]); } else { $v_packagingsize_max = $_POST["v_packagingsize_max"]; }
                
                if(!isset($_POST["v_std_production_time"])){ $v_std_production_time = isset($_POST["v_std_production_time"]); } else { $v_std_production_time = $_POST["v_std_production_time"]; }
                if(!isset($_POST["v_berat_per_pcs"])){ $v_berat_per_pcs = isset($_POST["v_berat_per_pcs"]); } else { $v_berat_per_pcs = $_POST["v_berat_per_pcs"]; }
                
                $v_formulaname         = save_char($v_formulaname);
                $v_formulaname_old     = save_char($v_formulaname_old);
                $v_quantity            = save_int($v_quantity);
                $v_packagingsize       = save_int($v_packagingsize);
                $v_quantity_old        = save_int($v_quantity_old);
                $v_packagingsize_old   = save_int($v_packagingsize_old);
                $v_packagingsize_min   = save_int($v_packagingsize_min);
                $v_packagingsize_max   = save_int($v_packagingsize_max);
                $v_std_production_time = save_int($v_std_production_time);
                $v_berat_per_pcs = save_int($v_berat_per_pcs);
                
                if($v_active=="on")
                {
                    $v_active_save = 1;
                }
                else
                {
                    $v_active_save = 0;
                }
                
                if($v_default=="on")
                {
                    $v_default_save = 1;
                }
                else
                {
                    $v_default_save = 0;
                }
                
                if($v_default != $v_default_old)
                {
                    if($v_default_save==1 && $v_active_save==0)
                    {
                        $msg = "Jika Formula ini menjadi Default, harap Status nya menjadi Active Juga";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                } 
                
                if($v_del==1)
                {
                    $q = "
                            SELECT
                                matreq.formulanumber
                            FROM
                                matreq
                            WHERE
                                matreq.formulanumber = '".$v_formulanumber."'
                            LIMIT
                                0,1
                    ";
                    $qry_cek = mysql_query($q);
                    $row_cek = mysql_fetch_array($qry_cek);
                    
                    if($row_cek["formulanumber"])
                    {
                        $msg = "Gagal menghapus, karena formula sudah digunakan";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search');</script>";         
                        die();    
                    }
                    else
                    {
                        $del = "
                                DELETE
                                    formula_log
                                FROM 
                                    formula_log
                                WHERE
                                    1
                                    AND formula_log.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus header log";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        /*$del = "
                                DELETE
                                    formuladetail_log
                                FROM 
                                    formuladetail_log
                                WHERE
                                    1
                                    AND formuladetail_log.formulanumber = '".$v_formulanumber."'
                        ";*/
                        $del = "
                                DELETE
                                    formuladetail
                                FROM 
                                    formuladetail
                                WHERE
                                    1
                                    AND formuladetail.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus detail log 1";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        $del = "
                                DELETE
                                    formuladetail
                                FROM 
                                    formuladetail
                                WHERE
                                    1
                                    AND formuladetail.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus detail";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        
                        
                        $del = "
                                DELETE
                                    formula
                                FROM 
                                    formula
                                WHERE
                                    1
                                    AND formula.formulanumber = '".$v_formulanumber."'
                        ";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal menghapus";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        else
                        {
                            $msg = "Berhasil menghapus";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search');</script>";         
                            die();    
                        }
                    } 
                }
                else if($v_req==1)
                {
                	//echo "Hello 2";die;
                    //send_approval($v_formulanumber, "edit");
                    //send_email_formula_req_approval($v_formulanumber);
                    
                    $q = "
                            UPDATE
                                formula
                            SET
                                pending = '1'
                            WHERE
                                1
                                AND formula.formulanumber = '".$v_formulanumber."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal update pending";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    
                    $msg = "Berhasil mengirim Request Approval";
                    echo "<script>alert('".$msg."');</script>"; 
                    echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_kemas');</script>";         
                    die();
                }else if($v_appr==1)
                {
                	//echo "Hello 2";die;
                    //send_approval($v_formulanumber, "edit");
                    //send_email_formula_req_approval($v_formulanumber);
					
					$subject = "Alert Approval Formula";
                        
                        $body = '
								<table align="center" cellpadding="0" cellspacing="0" width="50%" style="border: 3px solid #C1CDD8; ">
									<tbody>

										<tr style="background: #bad0ee; height: 50;">
											<td colspan="3" style=" padding: 5px 0px 5px 5px;font-size: 14px; font-weight: bold; text-align: center;">
												<p style="text-transform: uppercase;">
													Alert Permohonan Approval Formula
												</p>
											</td>
										</tr>

										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										
										<tr height="25">
											<td colspan="3" style="padding: 0 0 0 10px;">
												Dear Bapak / Ibu,
												<br/><br/>
												Mohon Approval Formula yang telah diajukan.<br/><br/>
												Anda dapat melihatnya dengan <a href="http://sys.bebektimbungan.com/produksi/formula.php" target="_blank">KLIK DISINI</a><br/>
												Atau copy URL berikut di broswer anda :<br/>
												http://sys.bebektimbungan.com/produksi/formula.php
											</td>
										</tr>

										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>

									</tbody>
								</table>
							';
                        
                        $to = "";
                        $to_name = "";
                        $sql = "
                                SELECT
                                    ".$db["master"].".function_email.email_address,
                                    ".$db["master"].".function_email.email_name
                                FROM
                                    ".$db["master"].".function_email
                                WHERE
                                    1
                                    AND ".$db["master"].".function_email.func_name = 'approve_formula'
                                ORDER BY
                                    ".$db["master"].".function_email.email_address ASC
                        ";
                        
                        $qry = mysql_query($sql);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($email_address, $email_name) = $row;
                            
                            $to .= $email_address.";";
                            $to_name .= $email_name.";";
                        }
                        
                        $author = "Approval Formula";
                        //ini yang akan mengirim ke email
                        //$kirim = send_email_multiple($subject, $body, $to, $to_name, $author);
                    
                    $q = "
                            UPDATE
                                formula
                            SET
                                approval_status_1 = '1',
                                approval_status_2 = '1',
                                approval_date_1='".date('Y-m-d')."',
                                approval_date_2='".date('Y-m-d')."',
                                pending = '1'
                            WHERE
                                1
                                AND formula.formulanumber = '".$v_formulanumber."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal update pending";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    
                    $msg = "Berhasil mengirim Request Approval";
                    echo "<script>alert('".$msg."');</script>"; 
                    echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_kemas');</script>";         
                    die();
                }
                else
                {
                    if($btn_delete_details)
                    {
                        foreach($v_del_details as $key => $val)
                        {
                            $q = "
                                    DELETE FROM
                                        formuladetail
                                    WHERE
                                        formuladetailid = '".$val."'    
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menghapus";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                        }
                        
                        $msg = "Berhasil menghapus";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_kemas');</script>";         
                        die();    
                    }
                    else
                    {

                        $upd = "
                                UPDATE 
                                    formula 
                                SET
                                    formulaname = '".$v_formulaname."' , 
                                    quantity = '".$v_quantity."' , 
                                    uominitial = '".$v_uominitial."' , 
                                    packagingsize = '".$v_packagingsize."' , 
                                    uompackaging = '".$v_uompackaging."' , 
                                    uompackaging = '".$v_uompackaging."' , 
                                    isactive = '".$v_active_save."' , 
                                    isdefault = '".$v_default_save."' ,
                                    packagingsize_min = '".$v_packagingsize_min."' , 
                                    packagingsize_max = '".$v_packagingsize_max."' ,
                                    std_production_time = '".$v_std_production_time."',  
                                    finishgood_wgt = '".$v_berat_per_pcs."',  
                                    edituser = '".GetUserLogin()."' , 
                                    editdate = NOW() 
                                WHERE
                                    formulanumber = '".$v_formulanumber."'         
                        ";
                        
                        if(!mysql_query($upd))
                        {
                            $msg = "Gagal menyimpan";
                            echo "<script>alert('".$msg."');</script>";
                            die();    
                        }
                        else
                        {
                            if(!isset($_POST["no"])){ $no = isset($_POST["no"]); } else { $no = $_POST["no"]; }
                            
                            if($v_quantity!=$v_quantity_old || $v_uominitial!=$v_uominitial_old || $v_packagingsize!=$v_packagingsize_old || $v_uompackaging!=$v_uompackaging_old)
                            {
                                update_approval($v_formulanumber);
                            }
                            
                            foreach($no as $key => $val)
                            {
                                if(!isset($_POST["v_sid_".$val])){ $v_sid = isset($_POST["v_sid_".$val]); } else { $v_sid = $_POST["v_sid_".$val]; }
                                if(!isset($_POST["v_inventorycode_".$val])){ $v_inventorycode = isset($_POST["v_inventorycode_".$val]); } else { $v_inventorycode = $_POST["v_inventorycode_".$val]; }
                                if(!isset($_POST["v_inventorycode_old_".$val])){ $v_inventorycode_old = isset($_POST["v_inventorycode_old_".$val]); } else { $v_inventorycode_old = $_POST["v_inventorycode_old_".$val]; }
                                
                                if(!isset($_POST["v_quantity_".$val])){ $v_quantity = isset($_POST["v_quantity_".$val]); } else { $v_quantity = $_POST["v_quantity_".$val]; }
                                if(!isset($_POST["v_quantity_old_".$val])){ $v_quantity_old = isset($_POST["v_quantity_old_".$val]); } else { $v_quantity_old = $_POST["v_quantity_old_".$val]; }
                                
                                $v_quantity     = save_int($v_quantity);
                                $v_quantity_old = save_int($v_quantity_old);
                                
                                
                                if($v_quantity!=$v_quantity_old || $v_inventorycode!=$v_inventorycode_old)
                                {
                                    update_approval($v_formulanumber);
                                    
                                    $upd = "
                                            UPDATE
                                                formuladetail
                                            SET
                                                inventorycode = '".$v_inventorycode."',
                                                quantity = '".$v_quantity."',
                                                editdate = NOW(),
                                                edituser = '".GetUserLogin()."'
                                            WHERE
                                                1
                                                AND formuladetailid = '".$v_sid."'
                                    ";
                                    if(!mysql_query($upd))
                                    {
                                        $msg = "Gagal menyimpan details";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();    
                                    }
                                }
                            }
                            
                            if($v_default!=$v_default_old)
                            {
                                if($v_default_save==1)
                                {
                                    $formula_update = "";
                                    $q = "
                                            SELECT
                                                formula.formulanumber
                                            FROM
                                                formula
                                            WHERE
                                                1
                                                AND formula.formulanumber != '".$v_formulanumber."'
                                                AND formula.isdefault = '1'
                                            ORDER BY
                                                formula.formulanumber ASC
                                    ";
                                    $qry = mysql_query($q);
                                    while($row = mysql_fetch_array($q))
                                    {
                                        list($formulanumber) = $row;
                                        
                                        $formula_update .= $formulanumber.", ";    
                                        
                                        $upd = "
                                            UPDATE
                                                formula
                                            SET
                                                isdefault = '0'
                                            WHERE
                                                formulanumber = '".$formulanumber."'
                                        ";
                                        if(!mysql_query($upd))
                                        {
                                            $msg = "Gagal Update default 0";
                                            echo "<script>alert('".$msg."');</script>";
                                            die();    
                                        }
                                    }
                                    
                                    $formula_update = substr($formula_update, 0, -2);
                                    
                                    if($formula_update)
                                    {
                                        $msg = "Terdapat Formula ".$formula_update." Yang terupdate tidak menjadi default, karena Formula ".$v_formulanumber." menjadi Default";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();    
                                    }
                                }
                            }
                                
                            $msg = "Berhasil menyimpan";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_formulanumber."', 'edit_data_kemas');</script>";         
                            die();    
                        }
                    }

                }

            }
            break;
        case "print" :
            {
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
                
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();
                }
                
                ?>
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
                        <link href="include/header.css" type="text/css" rel="stylesheet">
                        <link href="include/styles.css" type="text/css" rel="stylesheet">
                        <title><?php echo $modul; ?> - PT Victoria Care Indonesia</title>
                        <?php include("asset_head.php"); ?>
                        
                        <link rel="stylesheet" type="text/css" media="all" href="include/calendar.css" title="win2k-1" />
                        <script type="text/javascript" src="include/calendar.js"></script>
                        <script type="text/javascript" src="include/calendar-en.js"></script>
                        <script type="text/javascript" src="include/calendar-setup.js"></script>
                        
                        <script language='javascript' src='popcalendar.js'></script>
                        <link href='popcalendar.css' type='text/css' rel='stylesheet'>
                    </head>
                    <body>
                    
                    <?php 
                        foreach($v_data as $key => $val)
                        {
                            $simulation_no = $val;
                            
                            $q = "
                                    SELECT
                                        simulation_cogs.simulation_type
                                    FROM
                                        simulation_cogs
                                    WHERE
                                        1
                                        AND simulation_cogs.simulation_no = '".$simulation_no."'
                                    LIMIT
                                        0,1     
                            ";
                            $qry_type = mysql_query($q);
                            $row_type = mysql_fetch_array($qry_type);
                            
                            if($row_type["simulation_type"]=="formula")
                            {
                                $q = "
                                    SELECT
                                        simulation_cogs.simulation_no,
                                        simulation_cogs.simulation_date,
                                        simulation_cogs.inventorycode,
                                        masterbarang.NamaLengkap,
                                        simulation_cogs.formulanumber,
                                        formula.formulaname,
                                        simulation_cogs.packagingsize,
                                        simulation_cogs.uompackaging,
                                        simulation_cogs.quantity,
                                        simulation_cogs.uominitial,
                                        simulation_cogs.waste,
                                        producttype.producttypename,
                                        glfohstandard.foh,
                                        simulation_cogs.remarks
                                    FROM
                                        simulation_cogs
                                        INNER JOIN masterbarang ON
                                            masterbarang.PCode =  simulation_cogs.inventorycode
                                            AND simulation_cogs.simulation_no = '".$simulation_no."'
                                        INNER JOIN formula ON
                                            formula.formulanumber =  simulation_cogs.formulanumber
                                        INNER JOIN producttype ON
                                            producttype.producttypeid =  masterbarang.Tipe
                                        INNER JOIN glfohstandard ON
                                            glfohstandard.producttypeid =  masterbarang.Tipe
                                            AND glfohstandard.periodyear = YEAR(simulation_cogs.simulation_date)
                                            AND glfohstandard.periodmonth = MONTH(simulation_cogs.simulation_date)*1
                                    WHERE
                                        1
                                    LIMIT
                                        0,1
                            ";
                            $qry_curr = mysql_query($q);
                            $arr_curr = mysql_fetch_array($qry_curr);
                        }
                            else
                            {
                                $q = "
                                    SELECT
                                        simulation_cogs.simulation_no,
                                        simulation_cogs.simulation_date,
                                        simulation_cogs.simulation_type,
                                        simulation_cogs.inventorycode,
                                        masterbarang.NamaLengkap,
                                        simulation_cogs.packagingsize,
                                        simulation_cogs.uompackaging,
                                        simulation_cogs.quantity,
                                        simulation_cogs.uominitial,
                                        simulation_cogs.waste,
                                        producttype.producttypename,
                                        glfohstandard.foh,
                                        simulation_cogs.remarks
                                    FROM
                                        simulation_cogs
                                        INNER JOIN masterbarang ON
                                            masterbarang.PCode =  simulation_cogs.inventorycode
                                            AND simulation_cogs.simulation_no = '".$simulation_no."'
                                        INNER JOIN producttype ON
                                            producttype.producttypeid =  masterbarang.Tipe
                                        INNER JOIN glfohstandard ON
                                            glfohstandard.producttypeid =  masterbarang.Tipe
                                            AND glfohstandard.periodyear = YEAR(simulation_cogs.simulation_date)
                                            AND glfohstandard.periodmonth = MONTH(simulation_cogs.simulation_date)*1
                                    WHERE
                                        1
                                    LIMIT
                                        0,1
                                ";
                                $qry_curr = mysql_query($q);
                                $arr_curr = mysql_fetch_array($qry_curr);     
                            } 
                        
                        $arr_curr["foh_formula"] = $arr_curr["volume"] * $arr_curr["packagingsize"] * $arr_curr["foh"];
                        
                        $q = "
                                SELECT
                                    simulation_cogs_details.sid,
                                    simulation_cogs_details.inventorycode,
                                    masterbarang.NamaLengkap,
                                    simulation_cogs_details.supplierid,
                                    simulation_cogs_details.currencycode,
                                    simulation_cogs_details.currencyrate,
                                    simulation_cogs_details.amount_ori,
                                    simulation_cogs_details.amount,
                                    simulation_cogs_details.price,
                                    simulation_cogs_details.price_ori,
                                    simulation_cogs_details.foh
                                FROM
                                    simulation_cogs_details
                                    INNER JOIN masterbarang ON
                                        simulation_cogs_details.inventorycode = masterbarang.PCode
                                        AND (simulation_cogs_details.percentage*1) != '0'
                                        AND simulation_cogs_details.simulation_no = '".$simulation_no."' 
                                ORDER BY
                                     simulation_cogs_details.sid ASC
                        ";
                        $no = 1;
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($sid, $inventorycode, $inventoryname, $supplierid, $currencycode, $currencyrate, $amount_ori, $amount, $price, $price_ori, $foh) = $row;
                            
                            $arr_data["list_details_baku"][$sid] = $sid;
                            
                            $arr_data["details_baku_inventorycode"][$sid] = $inventorycode;
                            $arr_data["details_baku_inventoryname"][$sid] = $inventoryname;
                            $arr_data["details_baku_supplierid"][$sid] = $supplierid;
                            $arr_data["details_baku_currencycode"][$sid] = $currencycode;
                            $arr_data["details_baku_currencyrate"][$sid] = $currencyrate;
                            $arr_data["details_baku_amount_ori"][$sid] = $amount_ori;
                            $arr_data["details_baku_amount"][$sid] = $amount;
                            $arr_data["details_baku_price"][$sid] = $price;
                            $arr_data["details_baku_price_ori"][$sid] = $price_ori;
                            $arr_data["details_baku_foh"][$sid] = $foh;
                            $no++;
                        }
                        
                        $q = "
                                SELECT
                                    simulation_cogs_details.sid,
                                    simulation_cogs_details.inventorycode,
                                    masterbarang.NamaLengkap,
                                    simulation_cogs_details.supplierid,
                                    simulation_cogs_details.currencycode,
                                    simulation_cogs_details.currencyrate,
                                    simulation_cogs_details.amount_ori,
                                    simulation_cogs_details.amount,
                                    simulation_cogs_details.price,
                                    simulation_cogs_details.price_ori,
                                    simulation_cogs_details.foh
                                FROM
                                    simulation_cogs_details
                                    INNER JOIN masterbarang ON
                                        simulation_cogs_details.inventorycode = masterbarang.PCode
                                        AND (simulation_cogs_details.quantity*1) != '0'
                                        AND simulation_cogs_details.simulation_no = '".$simulation_no."' 
                                ORDER BY
                                     simulation_cogs_details.sid ASC
                        ";
                        $no = 1;
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($sid, $inventorycode, $inventoryname, $supplierid, $currencycode, $currencyrate, $amount_ori, $amount, $price, $price_ori, $foh) = $row;
                            
                            $arr_data["list_details_kemas"][$sid] = $sid;
                            
                            $arr_data["details_kemas_inventorycode"][$sid] = $inventorycode;
                            $arr_data["details_kemas_inventoryname"][$sid] = $inventoryname;
                            $arr_data["details_kemas_supplierid"][$sid] = $supplierid;
                            $arr_data["details_kemas_currencycode"][$sid] = $currencycode;
                            $arr_data["details_kemas_currencyrate"][$sid] = $currencyrate;
                            $arr_data["details_kemas_amount_ori"][$sid] = $amount_ori;
                            $arr_data["details_kemas_amount"][$sid] = $amount;
                            $arr_data["details_kemas_price"][$sid] = $price;
                            $arr_data["details_kemas_price_ori"][$sid] = $price_ori;
                            $arr_data["details_kemas_foh"][$sid] = $foh;
                            $no++;
                        }
                        
                        //print_r($arr_data["curr_currencycode"]);
                        
                        $q = "
                                SELECT
                                    supplier.supplierid,
                                    supplier.suppliername,
                                    supplier.title
                                FROM
                                    supplier
                                WHERE
                                    1
                                ORDER BY
                                    supplier.supplierid ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($supplierid, $suppliername, $title) = $row;
                            
                            $arr_data["list_supplier"][$supplierid] = $supplierid;
                            
                            if($title)
                            {
                                $arr_data["suppliername"][$supplierid] = $suppliername.", ".$title;
                            }
                            else
                            {
                                $arr_data["suppliername"][$supplierid] = $suppliername;
                            }
                        }
                        
                        $q = "
                                SELECT
                                    simulation_cogs.packagingsize,
                                    SUM(((simulation_cogs_details.price * simulation_cogs_details.currencyrate) * simulation_cogs_details.amount) + simulation_cogs_details.foh) AS price_formula
                                FROM
                                    simulation_cogs
                                    INNER JOIN simulation_cogs_details ON
                                        simulation_cogs.simulation_no = simulation_cogs_details.simulation_no
                                        AND simulation_cogs.simulation_no = '".$simulation_no."'
                                GROUP BY
                                    simulation_cogs.packagingsize
                        ";
                        $qry = mysql_query($q);
                        $arr_price_formula = mysql_fetch_array($qry);
                        
                        
                    ?>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td width="122"><img src="images/3s.jpg"></td>
                            <td valign="top">
                                <h2>SIMULASI COGS</h2>
                                <table>
                                    <tr >
                                        <td style="font-size:16px;">No</td>
                                        <td style="font-size:16px;">: <?php echo $arr_curr["simulation_no"]; ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="100%"><?php echo format_show_date_bulan($arr_curr["simulation_date"]); ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="100%"><?php echo ucfirst($row_type["simulation_type"]); ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="100%" align="right"><?php echo date("r"); ?></td>
                        </tr>
                    </table>
                    <hr>
                    <div align="center">
                        <table class="in_table_data" align="center">
                            <tr height="25" class="title_table">
                                <td colspan="100%"><?php echo $arr_curr["inventorycode"]." :: ".$arr_curr["inventoryname"]; ?></td>
                            </tr>
                            
                            <?php 
                                if($row_type["formula"])
                                {
                            ?>
                            <tr height="25">
                                <td><b>Formula<b></td>
                                <td><?php echo $arr_curr["formulanumber"]." :: ".$arr_curr["formulaname"]; ?></td>
                            </tr>
                            <?php 
                                }
                            ?>
                            <tr height="25">
                                <td width="200"><b>Product Type<b></td>
                                <td><?php echo $arr_curr["producttypename"]; ?></td>
                            </tr>
                            <tr height="25">
                                <td><b>Packaging Size<b></td>
                                <td><?php echo format_number($arr_curr["packagingsize"])." ".$arr_curr["uompackaging"]; ?></td>
                            </tr>
                            <tr height="25">
                                <td><b>Qty / Batch<b></td>
                                <td><?php echo format_number($arr_curr["quantity"],2)." ".$arr_curr["uominitial"]; ?></td>
                            </tr>
                            <tr height="25">
                                <td><b>Volume<b></td>
                                <td><?php echo format_number($arr_curr["volume"],2); ?></td>
                            </tr>
                            
                            <tr height="25">
                                <td><b>FOH / ML<b></td>
                                <td><?php echo format_number($arr_curr["foh"],2); ?></td>
                            </tr>
                            <tr height="25">
                                <td><b>FOH Formula<b></td>
                                <td><?php echo format_number($arr_curr["foh_formula"],2); ?></td>
                            </tr>
                            
                            <tr height="25">
                                <td><b>Waste (%)<b></td>
                                <td><?php echo format_number($arr_curr["waste"],2); ?></td>
                            </tr>
                            
                            <tr height="25">
                                <td><b>Remarks<b></td>
                                <td><?php echo $arr_curr["remarks"]; ?>&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td colspan="100%">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td colspan="100%">
                                <table width="100%" align="center">
                                    <tr class="title_table">
                                        <td colspan="100%">&nbsp;Raw Material</td>
                                    </tr>
                                    <tr class="title_table" align="center">
                                        <td rowspan="2" width="30">No</td>
                                        <td rowspan="2">Inventory Code</td>
                                        <td rowspan="2">Inventory Name</td>
                                        <td rowspan="2">Supplier</td>
                                        <td rowspan="2">Currency</td>
                                        <td rowspan="2">Rate</td>
                                        <td colspan="2">Amount</td>
                                        <td colspan="2">Price</td>
                                        <td rowspan="2">Price (IDR)</td>
                                        <td rowspan="2">FOH (IDR)</td>
                                        <td rowspan="2">Total (IDR)</td>
                                    </tr>
                                    
                                    <tr class="title_table" align="center">
                                        <td>Formula</td>
                                        <td>Simulasi</td>
                                        
                                        <td>PO</td>
                                        <td>Simulasi</td>
                                    </tr>
                                    
                                    <?php
                                        $no = 1; 
                                        foreach($arr_data["list_details_baku"] as $sid => $val)
                                        {
                                            $inventorycode = $arr_data["details_baku_inventorycode"][$sid];
                                            $inventoryname = $arr_data["details_baku_inventoryname"][$sid];
                                            $supplierid = $arr_data["details_baku_supplierid"][$sid];
                                            $currencycode = $arr_data["details_baku_currencycode"][$sid];
                                            $currencyrate = $arr_data["details_baku_currencyrate"][$sid];
                                            $amount_ori = $arr_data["details_baku_amount_ori"][$sid];
                                            $amount = $arr_data["details_baku_amount"][$sid];
                                            $price = $arr_data["details_baku_price"][$sid];
                                            $price_ori = $arr_data["details_baku_price_ori"][$sid];
                                            $foh = $arr_data["details_baku_foh"][$sid];
                                            
                                            $price_idr = ($currencyrate*$price);
                                            $total_idr = ($price_idr * $amount) + $foh; 
                                            
                                            $arr_total["amount_ori_baku"] += $amount_ori;
                                            $arr_total["amount_baku"] += $amount;
                                            $arr_total["foh_baku"] += $foh;
                                            $arr_total["total_idr_baku"] += $total_idr;
                                            
                                            
                                            ?>
                                            <tr height="25">
                                                <td>
                                                    <?php echo $no; ?>
                                                </td>
                                                <td><?php echo $inventorycode; ?></td>
                                                <td><?php echo $inventoryname; ?></td>
                                                <td><?php echo $arr_data["suppliername"][$supplierid]; ?>&nbsp;</td>
                                                <td>&nbsp;<?php echo $currencycode; ?></td>
                                                <td align="right">&nbsp;<?php echo $currencyrate; ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($amount_ori,2); ?></td>
                                                <td align="right">&nbsp;<?php echo $amount; ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($price_ori,2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($price, 2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($price_idr, 2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($foh, 2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($total_idr, 2); ?></td>
                                            </tr>
                                            <?php
                                            $no++;
                                        }
                                        
                                        $no = $no-1;    
                                    ?>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="6">
                                            Total Raw Material&nbsp;
                                        </td>
                                        <td>&nbsp;<?php echo format_number($arr_total["amount_ori_baku"],2); ?></td>
                                        <td>&nbsp;<?php echo format_number($arr_total["amount_baku"],2); ?></td>
                                        
                                        <td colspan="3">&nbsp;</td>
                                        <td>&nbsp;<?php echo format_number($arr_total["foh_baku"],2); ?></td>
                                        <td>&nbsp;<?php echo format_number($arr_total["total_idr_baku"],2); ?></td>
                                    </tr>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="12">Raw Material Price/Pcs</td>
                                        <td>&nbsp;<?php echo format_number($arr_total["total_idr_baku"]/$arr_curr["packagingsize"],2); ?></td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td colspan="100%">&nbsp;</td>
                                    </tr>
                                    
                                    <tr class="title_table">
                                        <td colspan="100%">&nbsp;Packaging</td>
                                    </tr>
                                    
                                    <?php
                                        $no = 1; 
                                        foreach($arr_data["list_details_kemas"] as $sid => $val)
                                        {
                                            $inventorycode = $arr_data["details_kemas_inventorycode"][$sid];
                                            $inventoryname = $arr_data["details_kemas_inventoryname"][$sid];
                                            $supplierid = $arr_data["details_kemas_supplierid"][$sid];
                                            $currencycode = $arr_data["details_kemas_currencycode"][$sid];
                                            $currencyrate = $arr_data["details_kemas_currencyrate"][$sid];
                                            $amount_ori = $arr_data["details_kemas_amount_ori"][$sid];
                                            $amount = $arr_data["details_kemas_amount"][$sid];
                                            $price = $arr_data["details_kemas_price"][$sid];
                                            $price_ori = $arr_data["details_kemas_price_ori"][$sid];
                                            $foh = $arr_data["details_kemas_foh"][$sid];
                                            
                                            $price_idr = ($currencyrate*$price);
                                            $total_idr = ($price_idr * $amount) + $foh; 
                                            
                                            $arr_total["amount_ori_kemas"] += $amount_ori;
                                            $arr_total["amount_kemas"] += $amount;
                                            $arr_total["foh_kemas"] += $foh;
                                            $arr_total["total_idr_kemas"] += $total_idr;
                                            
                                            
                                            ?>
                                            <tr height="25">
                                                <td>
                                                    <?php echo $no; ?>
                                                </td>
                                                <td><?php echo $inventorycode; ?></td>
                                                <td><?php echo $inventoryname; ?></td>
                                                <td><?php echo $arr_data["suppliername"][$supplierid]; ?>&nbsp;</td>
                                                <td>&nbsp;<?php echo $currencycode; ?></td>
                                                <td align="right">&nbsp;<?php echo $currencyrate; ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($amount_ori,2); ?></td>
                                                <td align="right">&nbsp;<?php echo $amount; ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($price_ori,2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($price, 2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($price_idr, 2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($foh, 2); ?></td>
                                                <td align="right">&nbsp;<?php echo format_number($total_idr, 2); ?></td>
                                            </tr>
                                            <?php
                                            $no++;
                                        }
                                        
                                        $no = $no-1; 
                                        $total_waste = ($arr_total["total_idr_baku"]+$arr_total["total_idr_kemas"]) * ($arr_curr["waste"]/100);   
                                    ?>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="6">
                                            Total Packaging&nbsp;
                                        </td>
                                        <td>&nbsp;<?php echo format_number($arr_total["amount_ori_kemas"],2); ?></td>
                                        <td>&nbsp;<?php echo format_number($arr_total["amount_kemas"],2); ?></td>
                                        
                                        <td colspan="3">&nbsp;</td>
                                        <td>&nbsp;<?php echo format_number($arr_total["foh_kemas"],2); ?></td>
                                        <td>&nbsp;<?php echo format_number($arr_total["total_idr_kemas"],2); ?></td>
                                    </tr>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="12">Packaging Price/Pcs</td>
                                        <td>&nbsp;<?php echo format_number($arr_total["total_idr_kemas"]/$arr_curr["packagingsize"],2); ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="100%">&nbsp;</td>
                                    </tr>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="11">
                                            TOTAL&nbsp;
                                        </td>
                                        <td>&nbsp;<?php echo format_number($arr_total["foh_baku"]+$arr_total["foh_kemas"],2); ?></td>
                                        <td>&nbsp;<?php echo format_number($arr_total["total_idr_baku"]+$arr_total["total_idr_kemas"],2); ?></td>
                                    </tr>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="12">
                                            WASTE&nbsp;
                                        </td>
                                        <td>&nbsp;<?php echo format_number($total_waste ,2); ?></td>
                                    </tr>
                                    
                                    <tr align="right" style="font-weight:bold;" height="25">
                                        <td colspan="12">Price/Pcs</td>
                                        <td>&nbsp;<?php echo format_number(($arr_total["total_idr_baku"]+$arr_total["total_idr_kemas"]+$total_waste)/$arr_curr["packagingsize"],2); ?></td>
                                    </tr>
                                </table> 
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="100%">&nbsp;</td>
                            </tr>
                            
                            
                        </table>
                    </div>
                    <div style="page-break-after: always;"></div>
                    <?php 
                        }
                    ?>
                    <script>window.print();</script>
                    </body>
                </html>
                
                <?php
                
            }
            break;
    }
}

function menutab($v_formulanumber, $menu){
    $str_menu = "
	    <center> 
		    <ul class=\"nav nav-tabs bordered\" >
		        <li ".($menu=='baku'?"class='active'":" ")." onclick=\"CallAjax('edit_data_baku', '".$v_formulanumber."')\"><a href=\"javascript:void(0)\">Raw Material</a></li>
		        <li ".($menu=='kemas'?"class='active'":" ")." onclick=\"CallAjax('edit_data_kemas', '".$v_formulanumber."')\"><a href=\"javascript:void(0)\">Packaging</a></li>
		    </ul>
	    </center> 
	";
	
	return $str_menu;
}

mysql_close($con);
?>



