<?php 
    include("header.php");
    
	$modul        = "Formula";
	$file_current = "formula.php";
	$file_name    = "formula_data.php";
    $ses_login = $_SESSION["ses_login"];
	$arr_data["month"][1] = "January";
	$arr_data["month"][2] = "February";
	$arr_data["month"][3] = "March";
	$arr_data["month"][4] = "April";
	$arr_data["month"][5] = "May";
	$arr_data["month"][6] = "June";
	$arr_data["month"][7] = "July";
	$arr_data["month"][8] = "August";
	$arr_data["month"][9] = "September";
	$arr_data["month"][10] = "October";
	$arr_data["month"][11] = "November";
	$arr_data["month"][12] = "December";
	
    $year_end   = date("Y");
    $year_start = date("Y")-3;
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>Formula - Modul Produksi - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
        <link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="../assets/css/NotoSans.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/neon-core.css">
        <link rel="stylesheet" href="../assets/css/neon-theme.css">
        <link rel="stylesheet" href="../assets/css/neon-forms.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/skins/black.css">
        <link rel="stylesheet" href="public/css/style.css">
        <link rel="stylesheet" href="../assets/css/my.css">

        <script src="../assets/js/jquery-1.11.0.min.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <script>
        
    function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
    {
        var centerWidth = (window.screen.width - windowWidth) / 2;
        var centerHeight = (window.screen.height - windowHeight) / 2;
        //alert('aaaa');

        newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth + 
            ',height=' + windowHeight + 
            ',left=' + centerWidth + 
            ',top=' + centerHeight
            );

        newWindow.focus();
        return newWindow.name;
    }

    function pop_up_details()
    {
        windowOpener('600', '800', 'Pop Up Details', 'vci_formula_pop_up_details.php', 'Pop Up Details')    
    }

    function pop_up_baku(id, no)
    {
        windowOpener('600', '800', 'Pop Up Baku', 'vci_formula_pop_up_baku.php?id='+id+'&v_no='+no, 'Pop Up Baku')    
    }

    function pop_up_kemas(id, no)
    {
        windowOpener('600', '800', 'Pop Up Kemas', 'vci_formula_pop_up_kemas.php?id='+id+'&v_no='+no, 'Pop Up Kemas')    
    }

    function num_format(obj) {
        obj.value = new NumberFormat(obj.value).toFormatted();
    }
      
    function createRequestObject() {
        var ro;
        var browser = navigator.appName;
        if(browser == "Microsoft Internet Explorer"){
            ro = new ActiveXObject("Microsoft.XMLHTTP");
        }else{
            ro = new XMLHttpRequest();
        }
        return ro;
    }

    var xmlhttp = createRequestObject();

    function CallAjax(tipenya,param1,param2,param3,param4,param5)
    {
        try{    
            if (!tipenya) return false;
            //document.getElementById("loading").style.display='block';
            
            if (param1 == undefined) param1 = '';
            if (param2 == undefined) param2 = '';
            if (param3 == undefined) param3 = '';
            if (param4 == undefined) param4 = '';
            if (param5 == undefined) param5 = '';
            
            var variabel;
            var arr_data;
            variabel = "";
            
            if(tipenya=='search')
            {  
                search_by = document.getElementById("search_by").value;
                search_keyword = document.getElementById("search_keyword").value;    
                search_status = document.getElementById("search_status").value;    
                search_default = document.getElementById("search_default").value;    
                search_formulanumber = document.getElementById("search_formulanumber").value;
                search_formulaname = document.getElementById("search_formulaname").value;
                search_inventorycode = document.getElementById("search_inventorycode").value;
                search_inventoryname = document.getElementById("search_inventoryname").value;
                v_period = document.getElementById("v_period").value;
                v_mm = document.getElementById("v_mm").value;
                v_yyyy = document.getElementById("v_yyyy").value;
                v_limit = document.getElementById("v_limit").value;
                
                variabel += "&search_by="+search_by;
                variabel += "&search_keyword="+search_keyword;
                variabel += "&search_status="+search_status;
                variabel += "&search_default="+search_default;
                variabel += "&search_formulanumber="+search_formulanumber;
                variabel += "&search_formulaname="+search_formulaname;
                variabel += "&search_inventorycode="+search_inventorycode;
                variabel += "&search_inventoryname="+search_inventoryname;
                variabel += "&v_period="+v_period;
                variabel += "&v_mm="+v_mm;
                variabel += "&v_yyyy="+v_yyyy;
                variabel += "&v_limit="+v_limit;
                
                variabel += "&v_formulanumber_curr="+param1;
                
                //alert(variabel);
                
                document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                document.getElementById("col-header").innerHTML   = "";
                
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        document.getElementById("col-search").innerHTML   = xmlhttp.responseText;
                        
                        if(param1)
                        {
                            CallAjax(param2 ,param1);
                        }
                    }

                    return false;
                }
                xmlhttp.send(null);
            } 
            else if(tipenya=='add_data')
            {  
                document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
            
            else if(tipenya=='edit_data_baku')
            {  
                variabel += "&v_formulanumber="+param1;
                
                //alert(variabel);
                document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                        document.getElementById("v_inventorycode_1").focus();
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
            else if(tipenya=='edit_data_kemas')
            {  
                variabel += "&v_formulanumber="+param1;
                
                //alert(variabel);
                document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
            else if(tipenya=='ajax_search_fg')
            {  
                variabel += "&v_keyword_fg="+param1;
                
                //alert(variabel);
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        document.getElementById("td_fg").innerHTML     = xmlhttp.responseText;
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
            
            else if(tipenya=='ajax_search_copy_formula')
            {  
                variabel += "&v_keyword_copy_formula="+param1;
                
                //alert(variabel);
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        document.getElementById("td_formula_copy").innerHTML     = xmlhttp.responseText;
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
            
            else if(tipenya=='ajax_search_pcode_baku')
            {  
                variabel += "&v_inventorycode="+param1;
                variabel += "&v_no="+param2;
                
                //alert(param1.length);
                if(param1.length>3)
                {
                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function() 
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {    
                            document.getElementById("td_inventoryname_"+param2).innerHTML  = xmlhttp.responseText;
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
            }
            
            else if(tipenya=='ajax_search_pcode_kemas')
            {  
                variabel += "&v_inventorycode="+param1;
                variabel += "&v_no="+param2;
                
                //alert(variabel);
                if(param1.length>3)
                {
                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function() 
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {    
                            document.getElementById("td_inventoryname_"+param2).innerHTML  = xmlhttp.responseText;
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
            }
            
            else if(tipenya=='ajax_save_detail_baku')
            {  
                v_inventorycode = document.getElementById("v_inventorycode_1").value;
                v_percentage = reform(document.getElementById("v_percentage_1").value)*1;
                v_quantity = reform(document.getElementById("td_quantity_1").value)*1;
                
                variabel += "&v_formulanumber="+param1;
                variabel += "&v_inventorycode="+v_inventorycode;
                variabel += "&v_percentage="+v_percentage;
                variabel += "&v_quantity="+v_quantity;
                
                if(v_inventorycode=="")
                {
                    alert("PCode harus dipilih...");
                    document.getElementById("v_inventorycode_1").focus();
                    return false;    
                }
                else if(v_percentage*1==0)
                {
                    alert("Percentage harus diisi...");
                    document.getElementById("v_percentage_1").focus();
                    return false;    
                }
                else
                {
                    //alert(variabel);
                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function() 
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {   
                            CallAjax('edit_data_baku' ,param1);
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
            }
            
            else if(tipenya=='ajax_save_detail_kemas')
            {  
                v_inventorycode = document.getElementById("v_inventorycode_1").value;
                v_quantity = reform(document.getElementById("v_quantity_1").value)*1;
                
                variabel += "&v_formulanumber="+param1;
                variabel += "&v_inventorycode="+v_inventorycode;
                variabel += "&v_quantity="+v_quantity;
                
                //alert(variabel);
                
                if(v_inventorycode=="")
                {
                    alert("PCode harus dipilih...");
                    document.getElementById("v_inventorycode_1").focus();
                    return false;    
                }
                else if(v_quantity*1==0)
                {
                    alert("Qty harus diisi...");
                    document.getElementById("v_quantity_1").focus();
                    return false;    
                }
                else
                {
                    //alert(variabel);
                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function() 
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {   
                            CallAjax('edit_data_kemas' ,param1);
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
            }
        }
        catch(err)
        {
            txt  = "There was an error on this page.\n\n";
            txt += "Error description : "+ err.message +"\n\n";
            txt += "Click OK to continue\n\n";
            alert(txt);
        } 

    }

    function confirm_delete(simulation_no)
    {
        var r = confirm("Anda yakin ingin menghapus "+simulation_no+" ?");
        if(r){
            document.getElementById("v_del").value = '1';
            document.getElementById("theform").submit();
        }
    }

    function confirm_approval(formulanumber)
    {
        var r = confirm("Anda yakin ingin Request Approval "+formulanumber+" ?");
        if(r){
            document.getElementById("v_req").value = '1';
            document.getElementById("theform").submit();
        }
    }
    
    function confirm_approval_formula(formulanumber)
    {
        var r = confirm("Anda yakin ingin Menyetujui Formula "+formulanumber+" ini ?");
        if(r){
            document.getElementById("v_appr").value = '1';
            document.getElementById("theform").submit();
        }
    }

    function CheckAll(param, target){
        var field = document.getElementsByName(target);
        var chkall = document.getElementById(param);
        if (chkall.checked == true){
            for (i = 0; i < field.length; i++)
                field[i].checked = true ;
        }else{
            for (i = 0; i < field.length; i++)
                field[i].checked = false ;
        }        
    }

    function reform(val)
    {    
         var a = val.split(",");
        var b = a.join("");
        //alert(b);
        return b;
    }

    function format(harga)
    {
     harga=parseFloat(harga);
     harga=harga.toFixed(0);
     //alert(harga);
     s = addSeparatorsNF(harga, '.', '.', ',');
     return s; 
    }

    function format4(harga)
    {
     harga=parseFloat(harga);
     harga=harga.toFixed(4);
     //alert(harga);
     s = addSeparatorsNF(harga, '.', '.', ',');
     return s; 
    }

    function format2(harga)
    {
     harga=parseFloat(harga);
     harga=harga.toFixed(2);
     //alert(harga);
     s = addSeparatorsNF(harga, '.', '.', ',');
     return s; 
    }

    function format6(harga)
    {
     harga=parseFloat(harga);
     harga=harga.toFixed(6);
     //alert(harga);
     s = addSeparatorsNF(harga, '.', '.', ',');
     return s; 
    }
     
    function addSeparatorsNF(nStr, inD, outD, sep)
    {
     nStr += '';
     var dpos = nStr.indexOf(inD);
     var nStrEnd = '';
     if (dpos != -1) {
      nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
      nStr = nStr.substring(0, dpos);
     }
     var rgx = /(\d+)(\d{3})/;
     while (rgx.test(nStr)) {
      nStr = nStr.replace(rgx, '$1' + sep + '$2');
     }
     return nStr + nStrEnd;
    }

    function toFormat(id)
    {
        //alert(document.getElementById(id).value);
        if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
        {
            //alert("That's not a number.")
            document.getElementById(id).value=0;
            //document.getElementById(id).focus();
        }
        document.getElementById(id).value=reform(document.getElementById(id).value);
        document.getElementById(id).value=format(document.getElementById(id).value);
    }
    function toFormat4(id)
    {
        //alert(document.getElementById(id).value);
        if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
        {
            //alert("That's not a number.")
            document.getElementById(id).value=0;
            //document.getElementById(id).focus();
        }
        document.getElementById(id).value=reform(document.getElementById(id).value);
        document.getElementById(id).value=format4(document.getElementById(id).value);
    }
    function toFormat2(id)
    {
        //alert(document.getElementById(id).value);
        if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
        {
            //alert("That's not a number.")
            document.getElementById(id).value=0;
            //document.getElementById(id).focus();
        }
        document.getElementById(id).value=reform(document.getElementById(id).value);
        document.getElementById(id).value=format2(document.getElementById(id).value);
    }
    function toFormat6(id)
    {
        //alert(document.getElementById(id).value);
        if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
        {
            //alert("That's not a number.")
            document.getElementById(id).value=0;
            //document.getElementById(id).focus();
        }
        document.getElementById(id).value=reform(document.getElementById(id).value);
        document.getElementById(id).value=format6(document.getElementById(id).value);
    }
    function isanumber(id)
    {
        if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
        {
            document.getElementById(id).value=0;
        }
        else
        {
            document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
        }
    }
      

    function start_page()
    {   
        //document.getElementById("search_user_form").focus();
    }

    function mouseover(target)
    {  
        if(target.bgColor!="#cafdb5"){        
            if (target.bgColor=='#ccccff')
                target.bgColor='#ccccff';
            else
                target.bgColor='#c1cdd8';
        }
    }
        
    function mouseout(target)
    {
        if(target.bgColor!="#cafdb5"){ 
            if (target.bgColor=='#ccccff')
                target.bgColor='#ccccff';
            else
                target.bgColor='#FFFFFF';
                
        }    
    }

    function mouseclick(target, idobject, num)
    {
                       
        //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
        for(i=0;i<num;i++){
            if (document.getElementById(idobject+'_'+i) != undefined){
                document.getElementById(idobject+'_'+i).bgColor='#f5faff';
                if (target.id == idobject+'_'+i)
                    target.bgColor='#ccccff';
            }
        }
    }

    function mouseclick1(target)
    {
        //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
        if(target.bgColor!="#cafdb5")
        {
            target.bgColor="#cafdb5";
        }
        else
        {
            target.bgColor="#FFFFFF";
        }
    }

    function change_serach_by(nilai)
    {
        if(nilai=="data")
        {
            document.getElementById('td_data').style.display    = '';    
            document.getElementById('td_formulanumber').style.display    = 'none';    
            document.getElementById('td_formulaname').style.display      = 'none';    
            document.getElementById('td_inventorycode').style.display    = 'none';    
            document.getElementById('td_inventoryname').style.display    = 'none';    
            
            document.getElementById('search_keyword').focus();        
        }
        else if(nilai=="formulanumber")
        {
            document.getElementById('td_data').style.display    = 'none';    
            document.getElementById('td_formulanumber').style.display    = '';    
            document.getElementById('td_formulaname').style.display      = 'none';    
            document.getElementById('td_inventorycode').style.display    = 'none';    
            document.getElementById('td_inventoryname').style.display    = 'none';    
            
            document.getElementById('search_formulanumber').focus();        
        }
        else if(nilai=="formulaname")
        {
            document.getElementById('td_data').style.display    = 'none';    
            document.getElementById('td_formulanumber').style.display    = 'none';    
            document.getElementById('td_formulaname').style.display      = '';    
            document.getElementById('td_inventorycode').style.display    = 'none';    
            document.getElementById('td_inventoryname').style.display    = 'none';    
            
            document.getElementById('search_formulaname').focus();        
        }
        else if(nilai=="inventorycode")
        {
            document.getElementById('td_data').style.display    = 'none';    
            document.getElementById('td_formulanumber').style.display    = 'none';    
            document.getElementById('td_formulaname').style.display      = 'none';    
            document.getElementById('td_inventorycode').style.display    = '';    
            document.getElementById('td_inventoryname').style.display    = 'none';    
            
            document.getElementById('search_inventorycode').focus();    
        }
        else if(nilai=="inventoryname")
        {
            document.getElementById('td_data').style.display    = 'none';    
            document.getElementById('td_formulanumber').style.display    = 'none';    
            document.getElementById('td_formulaname').style.display      = 'none';    
            document.getElementById('td_inventorycode').style.display    = 'none';    
            document.getElementById('td_inventoryname').style.display    = '';    
            
            document.getElementById('search_inventoryname').focus();    
        }
    } 

    function calculate()
    {
        try{
            var total_quantity;
            var total_percentage;
            
            quantity = 0;
            total_quantity = 0;
            total_percentage = 0;
            
            v_no = reform(document.getElementById('v_no').value)*1;
            v_quantity = reform(document.getElementById('v_quantity').value)*1;

            //alert(v_quantity);
            
            for(i=1;i<=v_no;i++)
            {
                v_percentage = reform(document.getElementById('v_percentage_'+i).value)*1;

                quantity = v_quantity * (v_percentage/100);
                //alert(quantity);
                
                total_quantity += (quantity*1);
                total_percentage += (v_percentage*1);
                
                //document.getElementById('td_quantity_'+i).innerHTML = format4(quantity);
                $('#td_quantity_'+i).val(format4(quantity));
            }
            
            document.getElementById('td_total_quantity').innerHTML = format4(total_quantity);
            document.getElementById('td_total_percentage').innerHTML = format4(total_percentage);
        }
        catch(err)
        {
            txt  = "There was an error on this page.\n\n";
            txt += "Error description : Calculate "+ err.message +"\n\n";
            txt += "Click OK to continue\n\n";
            alert(txt);
        }
    }

    function change_type(nilai)
    {
        if(nilai=="Reguler")
        {
            document.getElementById('tr_formula_name').style.display        = '';
            document.getElementById('tr_finish_goods').style.display        = '';
            document.getElementById('tr_batch_qty').style.display           = '';
            document.getElementById('tr_packagingsize').style.display       = '';
            document.getElementById('tr_status').style.display              = '';
            
            document.getElementById('tr_copy_formula').style.display        = 'none';
            document.getElementById('v_formulaname').focus();
        }
        else if(nilai=="Copy")
        {
            document.getElementById('tr_formula_name').style.display        = '';
            document.getElementById('tr_finish_goods').style.display        = '';
            document.getElementById('tr_batch_qty').style.display           = 'none';
            document.getElementById('tr_packagingsize').style.display       = 'none';
            document.getElementById('tr_status').style.display              = 'none';
            
            document.getElementById('tr_copy_formula').style.display        = '';
            document.getElementById('v_keyword_copy_formula').focus();
            
        }
        
    }

    function change_period(nilai)
    {
        if(nilai=="monthly")
        {           
            document.getElementById('tr_monthly').style.display  = '';    
        }
        else
        {
            document.getElementById('tr_monthly').style.display  = 'none';    
        }
    } 
    </script>

</head>
<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-10">
				Search By&nbsp;
				<select name="search_by" id="search_by" class="form-control-new" onchange="change_serach_by(this.value)">
		            <option value="data">Data</option>
		            <option value="formulanumber">Formula Number</option>
		            <option value="formulaname">Formula Name</option>
		            <option value="inventorycode">PCode</option>
		            <option value="inventoryname">Nama Barang</option>
		        </select>
				&nbsp;
				
				<span id="td_data" style="display:">
					<input type="text" size="30" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new">
					&nbsp;
					Period&nbsp;
					<select name="v_period" id="v_period" class="form-control-new" onchange="change_period(this.value)">
		                <option value="all">All</option>
		                <option value="monthly">Monthly</option>
		            </select>
					&nbsp;
					
					<span id="tr_monthly" style="display:none;">
						<select name="v_mm" id="v_mm" class="form-control-new">
			                <?php 
			                    foreach($arr_data["month"] as $key => $val)
			                    {   
			                        $selected = "";
			                        if(date("m")==$key)
			                        {
			                            $selected = "selected='selected'";    
			                        }
			                ?>
			                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
			                <?php 
			                    }
			                ?>
			            </select>
			            &nbsp;
			            <select name="v_yyyy" id="v_yyyy" class="form-control-new">
			                <?php 
			                    for($i=$year_start;$i<=$year_end;$i++)
			                    {
			                        $selected = "";
			                        if(date("Y")==$i)
			                        {
			                            $selected = "selected='selected'";    
			                        }
			                       
			                ?>
			                <option <?php echo $selected; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
			                <?php 
			                    }
			                ?>
			            </select>
			            &nbsp;
					</span>
					
					Status&nbsp;
		            <select name="search_status" id="search_status" class="form-control-new">
		                <option value="">All</option>
		                <option value="active">Active</option>
		                <option value="inactive">InActive</option>
		            </select>
					&nbsp;
					Default&nbsp;
		            <select name="search_default" id="search_default" class="form-control-new">
		                <option value="">All</option>
		                <option value="yes">Yes</option>
		                <option value="no">No</option>
		            </select>
					&nbsp;
					Limit&nbsp;
		            <select name="v_limit" id="v_limit" class="form-control-new">
		                <option value="50">50</option>
		                <option value="100">100</option>
		                <option value="all">All</option>
		            </select>
	            </span>
	            
	            <span id="td_formulanumber" style="display:none;">
		            <input type="text" size="30" maxlength="30" name="search_formulanumber" id="search_formulanumber" class="form-control-new">
		        </span>
        
		        <span id="td_formulaname" style="display:none;">
		            <input type="text" size="30" maxlength="30" name="search_formulaname" id="search_formulaname" class="form-control-new">
		        </span>
		        
		        <span id="td_inventorycode" style="display:none;">
		            <input type="text" size="30" maxlength="30" name="search_inventorycode" id="search_inventorycode" class="form-control-new">
		        </span>
		        
		        <span id="td_inventoryname" style="display:none;">
		            <input type="text" size="30" maxlength="30" name="search_inventoryname" id="search_inventoryname" class="form-control-new">
		        </span>
		        
	       </div>
	       
	       	<div class="col-md-2" align="right">
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('search'),show_loading_bar(100)">Search<i class="entypo-search"></i></button>
                
                
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('add_data'),show_loading_bar(100)">Add Data<i class="entypo-plus" ></i></button>
                
	       </div>
	      
		</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
		
<?php include("footer.php"); ?>		