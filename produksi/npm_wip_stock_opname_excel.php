<?php 
	include("header.php");
    
	$modul        = "Laporan Stock Opname WIP";

    if(!isset($_REQUEST["v_opname_date"])){ $v_opname_date = isset($_REQUEST["v_opname_date"]); } else { $v_opname_date = $_REQUEST["v_opname_date"]; }
    if(!isset($_REQUEST["v_warehouse"])){ $v_warehouse = isset($_REQUEST["v_warehouse"]); } else { $v_warehouse = $_REQUEST["v_warehouse"]; }
    
    
    $name_tambah = preg_replace("/[^A-Z0-9._-]/i", "_", strtolower($modul));
	
    $file_name = $name_tambah.".xls";
    
    header("Content-Disposition".": "."attachment;filename=$file_name");
    header("Content-type: application/vnd.ms-excel");
	
	// button
	{
		$q="
			SELECT 
			  stock_opname_wip.approval 
			FROM
			  stock_opname_wip 
			WHERE 1 
			  AND stock_opname_wip.opnamedate = '".format_save_date($v_opname_date)."' 
			  AND stock_opname_wip.warehousecode = '".$v_warehouse."' 
			GROUP BY stock_opname_wip.approval	
		";
		$qry = mysql_query($q);
		$r = mysql_fetch_array($qry);
        
        $sudah_ada = mysql_num_rows($qry);
	}
    
	// union
	{
		$q="
			SELECT 
			  unu.*,
			  formula.formulaname 
			FROM
			  (SELECT 
			    stock_opname_wip.batchnumber,
			    stock_opname_wip.warehousecode,
			    stock_opname_wip.formulanumber,
			    stock_opname_wip.physical_qty,
			    stock_opname_wip.data_qty,
			    stock_opname_wip.remarks,
			    stock_opname_wip.status_data 
			  FROM
			    stock_opname_wip 
			  WHERE 1 
			    AND stock_opname_wip.opnamedate = '".format_save_date($v_opname_date)."' 
			    AND stock_opname_wip.warehousecode = '".$v_warehouse."' 
			  UNION
			  SELECT 
			    production.batchnumber,
			    production.warehousecode,
			    production.formulanumber,
			    NULL AS physical_qty,
			    NULL AS data_qty,
			    NULL AS remarks,
			    'Regular' AS status_data 
			  FROM
			    production 
			  WHERE 1 
			    AND production.mixquantity * 1 != '0' 
			    AND production.status_batchnumber = 'Open'
                AND production.productiondate <= '".format_save_date($v_opname_date)."'  
			    AND production.warehousecode = '".$v_warehouse."') AS unu 
			  INNER JOIN formula 
			    ON unu.formulanumber = formula.formulanumber 
			  GROUP BY unu.batchnumber,
				unu.warehousecode,
				unu.formulanumber 
			ORDER BY formula.formulaname ASC, unu.batchnumber ASC  
		";
		$counter=1;
		$qry = mysql_query($q);
		while($row = mysql_fetch_array($qry))
		{
			list($batchnumber,$warehousecode,$formulanumber,$physical_qty,$data_qty,$remarks,$status_data,$formulaname)=$row;
			
			$arr_data["list_opname_wip"][$counter]=$counter;	
			$arr_data["batchnumber"][$counter]=$batchnumber;
			$arr_data["warehousecode"][$counter]=$warehousecode;
			$arr_data["formulanumber"][$counter]=$formulanumber;
			$arr_data["physical_qty"][$counter]=$physical_qty;
			$arr_data["data_qty"][$counter]=$data_qty;
			$arr_data["remarks"][$counter]=$remarks;	
			$arr_data["status_data"][$counter]=$status_data;
			$arr_data["formulaname"][$counter]=$formulaname;
			
			$counter++;		
		}
	}	
	
	$arr_bincard_wip = bincard_wip_stock($arr_data["batchnumber"], format_save_date($v_opname_date));
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $modul; ?></title>
    </head>
<body marginheight=0 marginwidth=0>  
<table>
	<tr>
 		<td colspan="7"><b>REPORT STOCK OPNAME WIP</b></td>	
 	</tr>
 	<tr>
 		<td colspan="7">
 			<table>
 				<tr>
 					<td><b>Tanggal</b></td>
 					<td style="text-align: left">:&nbsp;<?php echo format_show_date($v_opname_date); ?></td>
 				</tr>
 				<tr>
 					<td><b>Warehouse</b></td>
 					<td style="text-align: left">:&nbsp;<?php echo $v_warehouse; ?></td>
 				</tr>
 				<tr>
 					<td><b>Status</b></td>
 					<td style="text-align: left">:&nbsp;
 					<?php
 					if($r["approval"]*1==0)
 					{
						echo "No Approve";	
					}
					else
					{
						echo "Approve";	
					}
 					?>
 					</td>
 				</tr>
 			</table>
 		</td>
 	</tr>
 	<tr>
 		<td colspan="7">&nbsp;</td>
 	</tr>
</table> 
                                                                        
 <table align="left" border="1">
    <tr style="font-weight:bold; text-align:center; font-size:15px; height: 25" >
		<td style="background-color: #F24853;" valign="middle">No</td>
		<td style="background-color: #F24853;" valign="middle">Batchnumber</td>
		<td style="background-color: #F24853;" valign="middle">Formula Name</td>
		<td style="background-color: #F24853;" valign="middle">Physical Qty</td>
		<td style="background-color: #F24853;" valign="middle">Data Qty</td>
		<td style="background-color: #F24853;" valign="middle">Diff</td>
		<td style="background-color: #F24853;" valign="middle">Remarks</td>
    </tr>
    
    <?php
    if(count($arr_data["list_opname_wip"])==0)
	{
		echo "
			<tr height='20'>
				<td colspan='100%' align='center'>No Data</td>
			</tr>
		";	
	}
	else
	{
		$nomor = 1;    
	    foreach($arr_data["list_opname_wip"] as $counter => $val)
	    {    
			$batchnumber = $arr_data["batchnumber"][$counter];
			$warehousecode = $arr_data["warehousecode"][$counter];
			$formulanumber = $arr_data["formulanumber"][$counter];
			$physical_qty = $arr_data["physical_qty"][$counter];
			$data_qty = $arr_data["data_qty"][$counter];
			$remarks = $arr_data["remarks"][$counter];	
			$status_data = $arr_data["status_data"][$counter];
			$formulaname = $arr_data["formulaname"][$counter];	
			
	        $sum_akhir = 0;
        
	        if($status_data=="SO")
	        {
	            $sum_akhir = 0;   
	        }
	        else if($sudah_ada)
	        {
	           $sum_akhir = $data_qty;     
	        }
	        else
	        {
	            $sum_akhir = $arr_bincard_wip["sum_akhir"][$batchnumber];
	        }
	        
			$echo_diff = ($physical_qty*1)-($data_qty*1);
        
		    $total["physical_qty"] += $physical_qty;
		    $total["data_qty"] += $sum_akhir;
			
			?>
    
		    <tr>
		        <td><?php echo $nomor; ?></td>
		        <td><?php echo $batchnumber; ?></td>
		        <td><nobr><?php echo $formulaname; ?></nobr></td>
		        <td align="right"><?php echo format_number($physical_qty,4,"ind");?></td>
		        <td align="right"><?php echo format_number($sum_akhir,4,"ind"); ?></td>
		        <td align="right"><?php echo format_number($echo_diff,4,"ind"); ?></td>
		        <td><?php echo $remarks; ?></td>
		    </tr>
		    <?php
		    $nomor++;
		}
		
		?>
	    <tr height="25">
	        <td colspan="3">&nbsp;</td>
	        <td align="right" id="td_total_physical_qty"><b><?php echo format_number($total["physical_qty"],4,"ind"); ?></b></td>
	        <td align="right" id="td_total_data_qty"><b><?php echo format_number($total["data_qty"],4,"ind"); ?></b></td>
	        <td align="right" id="td_total_diff"><b><?php echo format_number($total["physical_qty"]-$total["data_qty"],4,"ind"); ?></b></td>
	        <td>&nbsp;</td>
	    </tr>
		<?php
	}
    ?>
     
</table> 
</body>
</html>
<?php 
    mysql_close();
?>