<?php
    include("header.php");
    
	$modul        = "WIP Stock Opname";
	$file_current = "npm_wip_stock_opname.php";
	$file_name    = "npm_wip_stock_opname_data.php";

	$q = "
		SELECT 
		  warehouseadmin.warehousecode,
		  warehouse.warehousename 
		FROM
		  warehouseadmin 
		  INNER JOIN warehouse 
		    ON warehouseadmin.warehousecode = warehouse.warehousecode 
		WHERE 1 
		  AND warehouseadmin.userid = '".$ses_login."'
		  AND warehouseadmin.warehousecode IN ('WH002')
	";
	$qry = mysql_query($q);
	while($row = mysql_fetch_array($qry))
	{
	    list($warehousecode, $warehousename) = $row;
	    
	    $arr_data["list_wh"][$warehousecode] = $warehousecode;
	    
	    $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
	    $arr_data["warehousename_only"][$warehousecode] = $warehousename;
	}

	$date_akhir = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));

	$v_date = $date_akhir."-".date('m')."-".date('Y'); 	

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="description" content="Neon Admin Panel" />
	    <meta name="author" content="" />

	    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
	    <link rel="shortcut icon" href="public/images/Logosg.png" >
	    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    	<link rel="stylesheet" href="assets/css/NotoSans.css">
	    <link rel="stylesheet" href="assets/css/bootstrap.css">
	    <link rel="stylesheet" href="assets/css/neon-core.css">
	    <link rel="stylesheet" href="assets/css/neon-theme.css">
	    <link rel="stylesheet" href="assets/css/neon-forms.css">
	    <link rel="stylesheet" href="assets/css/custom.css">
	    <link rel="stylesheet" href="assets/css/skins/black.css">
	    <link rel="stylesheet" href="public/css/style.css">
	    <link rel="stylesheet" href="assets/css/my.css">

	    <script src="assets/js/jquery-1.11.0.min.js"></script>

	    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

<script>

function start_page()
	{

	}
	
	function reform(val)
	{    
	    var a = val.split(",");
	    var b = a.join("");
	    //alert(b);
	    return b;
	}

	function format(harga)
	{
	    harga=parseFloat(harga);
	    harga=harga.toFixed(0);
	    //alert(harga);
	    s = addSeparatorsNF(harga, '.', '.', ',');
	    return s; 
	}

	function format4(harga)
	{
	    harga=parseFloat(harga);
	    harga=harga.toFixed(4);
	    //alert(harga);
	    s = addSeparatorsNF(harga, '.', '.', ',');
	    return s; 
	}

	function format6(harga)
	{
	    harga=parseFloat(harga);
	    harga=harga.toFixed(6);
	    //alert(harga);
	    s = addSeparatorsNF(harga, '.', '.', ',');
	    return s; 
	}
	 
	function addSeparatorsNF(nStr, inD, outD, sep)
	{
		nStr += '';
		var dpos = nStr.indexOf(inD);
		var nStrEnd = '';
		if (dpos != -1)
		{
			nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
			nStr = nStr.substring(0, dpos);
		}
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(nStr))
		{
			nStr = nStr.replace(rgx, '$1' + sep + '$2');
		}
		return nStr + nStrEnd;
	}

	function toFormat(id)
	{
	    //alert(document.getElementById(id).value);
	    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
	    {
	        //alert("That's not a number.")
	        document.getElementById(id).value=0;
	        //document.getElementById(id).focus();
	    }
	    document.getElementById(id).value=reform(document.getElementById(id).value);
	    document.getElementById(id).value=format(document.getElementById(id).value);
	}
	function toFormat4(id)
	{
	    //alert(document.getElementById(id).value);
	    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
	    {
	        //alert("That's not a number.")
	        document.getElementById(id).value=0;
	        //document.getElementById(id).focus();
	    }
	    document.getElementById(id).value=reform(document.getElementById(id).value);
	    document.getElementById(id).value=format4(document.getElementById(id).value);
	}
	
	function toFormat6(id)
	{
	    //alert(document.getElementById(id).value);
	    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
	    {
	        //alert("That's not a number.")
	        document.getElementById(id).value=0;
	        //document.getElementById(id).focus();
	    }
	    document.getElementById(id).value=reform(document.getElementById(id).value);
	    document.getElementById(id).value=format6(document.getElementById(id).value);
	}

	function isanumber(id)
	{
	    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
	    {
	        document.getElementById(id).value=0;
	    }
	    else
	    {
	        document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
	    }
	}

	function mouseover(target)
	{
		if(target.bgColor!="#cafdb5")
		{
			if (target.bgColor=='#ccccff')
			target.bgColor='#ccccff';
			else
			target.bgColor='#c1cdd8';
		}
	}

	function mouseout(target)
	{
		if(target.bgColor!="#cafdb5")
		{
			if (target.bgColor=='#ccccff')
			target.bgColor='#ccccff';
			else
			target.bgColor='#FFFFFF';

		}
	}

	function mouseclick(target, idobject, num)
	{
		//var pjg = document.getElementById(idobject + '_sum').innerHTML;
		for(i=0;i<num;i++)
		{
			if (document.getElementById(idobject+'_'+i) != undefined)
			{
				document.getElementById(idobject+'_'+i).bgColor='#f5faff';
				if (target.id == idobject+'_'+i)
				target.bgColor='#ccccff';
			}
		}
	}

	function mouseclick1(target)
	{
		//var pjg = document.getElementById(idobject + '_sum').innerHTML;
		if(target.bgColor!="#cafdb5")
		{
			target.bgColor="#cafdb5";
		}
		else
		{
			target.bgColor="#FFFFFF";
		}
	}

	function CheckAll(param, target)
	{
		var field = document.getElementsByName(target);
		var chkall = document.getElementById(param);
		if (chkall.checked == true)
		{
			for (i = 0; i < field.length; i++)
			field[i].checked = true ;
		}else
		{
			for (i = 0; i < field.length; i++)
			field[i].checked = false ;
		}
	}

	function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
	{
		var centerWidth = (window.screen.width - windowWidth) / 2;
		var centerHeight = (window.screen.height - windowHeight) / 2;
		//alert('aaaa');

		newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth +
			',height=' + windowHeight +
			',left=' + centerWidth +
			',top=' + centerHeight
		);

		newWindow.focus();
		return newWindow.name;
	}
  
	function createRequestObject()
	{
		var ro;
		var browser = navigator.appName;
		if(browser == "Microsoft Internet Explorer")
		{
			ro = new ActiveXObject("Microsoft.XMLHTTP");
		}else
		{
			ro = new XMLHttpRequest();
		}
		return ro;
	}

	var xmlhttp = createRequestObject();

	function CallAjax(tipenya,param1,param2,param3,param4,param5)
	{
	    try{    
	        if (!tipenya) return false;
	        //document.getElementById("loading").style.display='block';
	        
	        if (param1 == undefined) param1 = '';
	        if (param2 == undefined) param2 = '';
	        if (param3 == undefined) param3 = '';
	        if (param4 == undefined) param4 = '';
	        if (param5 == undefined) param5 = '';
	        
	        var variabel;
	        variabel = "";
	        
	        if(tipenya=='search')
	        {  
	            v_warehouse = document.getElementById("v_warehouse").value;
				v_opname_date = document.getElementById("v_opname_date").value;

				variabel += "&v_warehouse="+v_warehouse;
				variabel += "&v_opname_date="+v_opname_date;
				
				if(v_opname_date=='')
				{
					alert("Opname Date harus diisi.")
					return false;
				}
	            
	            document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
	            
	            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
	            xmlhttp.onreadystatechange = function() 
	            {
	                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
	                {    
	                    document.getElementById("col-search").innerHTML     = xmlhttp.responseText;
	                }

	                return false;
	            }
	            xmlhttp.send(null);
	        } 
			else if(tipenya=='ajax_del_detail')
			{
				v_warehousecode = document.getElementById("v_warehousecode").value;
				v_opname_date = document.getElementById("v_opnamedate").value;

				variabel += "&v_warehousecode="+v_warehousecode;
				variabel += "&v_opname_date="+v_opname_date;
				variabel += "&v_batchnumber="+param1;
				
				//alert(variabel);
				
				var x = confirm("Are you sure you want to delete?");
				if (x)
				{
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							if(param1)
							{
								CallAjax('search');	
							}
							
							document.getElementById("col-data").innerHTML     = xmlhttp.responseText;
							document.getElementById('col-load').style.display    = 'none';
						}

						return false;
					}
					
					xmlhttp.send(null);
				}
				else
				{
					if(param1)
					{
						CallAjax('search');	
					}
					
					return false;
					
				}
			}
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    } 
	}

	function calculate(no)
	{
	    try{
	        var diff;
	        
	        diff = 0;
	        
		   	v_physical_qty	= reform(document.getElementById('v_physical_qty_'+no).value)*1;
		   	v_data_qty		= reform(document.getElementById('v_data_qty_'+no).value)*1;
	       	diff 			= v_physical_qty-v_data_qty;
	       	
	    	document.getElementById('td_diff_'+no).innerHTML = format4(diff);
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description Calculate : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }    
	}
	
	function pop_up_addbatch(opnamedate,warehouse)
	{
	    windowOpener('400', '800', 'Pop Up Add Batchnumber', 'npm_wip_stock_opname_add.php?v_opnamedate='+opnamedate+'&v_warehousecode='+warehouse, 'Pop Up Add Batchnumber');
	}
    
    function copy_data_qty(nomor)
    {
        var v_data_qty;
        
        v_data_qty = reform(document.getElementById('v_data_qty_'+nomor).value)*1;
        
        document.getElementById('v_physical_qty_'+nomor).value = format4(v_data_qty);
    }
    
    function pop_up_excel()
	{
		var variabel;
        variabel = "";
            
    	var v_opname_date = document.getElementById("v_opname_date").value;
        var v_warehouse = document.getElementById("v_warehouse").value; 
        
        if(document.getElementById("v_opname_date").value=="")
		{
			alert("Opname Date");
			document.getElementById("v_opname_date").focus();  
			return false;
		}
        else if(document.getElementById("v_warehouse").value=="")
		{
			alert("Warehouse");
			return false;
		}
		else
		{
	        variabel += "&v_opname_date="+v_opname_date;  
	        variabel += "&v_warehouse="+v_warehouse;  
	        
			windowOpener('300', '400', 'Excel', 'npm_wip_stock_opname_excel.php?'+variabel, 'Excel');
		}
	}


</script> 
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-12">
			
				<ol class="breadcrumb">
					<li>
						<a href="javascript:void(0);">
							<i class="entypo-pencil"></i><?php echo $modul; ?>
						</a>
					</li>
				</ol>
				
			    <table class="table table-bordered responsive">
			        
			        <tr id="tr_period">
			            <td class="title_table" width="250">Opname Date</td>
						<td>
							<input type="text" class="form-control-new datepicker" name="v_opname_date" id="v_opname_date" size="10" maxlength="10" value="<?php echo $v_date; ?>">
						</td>
			        </tr>
			        
			        <tr>
			            <td class="title_table">Warehouse</td>
			            <td>
			                <select name="v_warehouse" id="v_warehouse" class="form-control-new">
			                	<?php
			                	if(count($arr_data["list_wh"])==3)
			                	{
									echo "<option value='All'>All</option>";	
								}
			                	?>
			                    
			                    <?php 
			                        foreach($arr_data["list_wh"] as $warehousecode => $val)
			                        {
			                        	$warehousename = $arr_data["warehousename"][$warehousecode];
			                        	
			                        	$selected="";
			                        	if($v_warehouse==$warehousecode)
			                        	{
											$selected = 'selected="selected"';
										}
			                    ?>
			                            <option <?php echo $selected; ?> value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
			                    <?php 
			                        }
			                    ?>
			                </select>
			            </td>
			        </tr>
			        
	                <tr>
	                    <td>&nbsp;</td>
	                    <td>
	                    	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('search'),show_loading_bar(100)">Search<i class="entypo-search"></i></button>
	       					<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="pop_up_excel(),show_loading_bar(100)">Excel<i class="entypo-download" ></i></button>
						</td>
	                </tr>
			    
			    </table>	
	        </div>
       	</div>
		
		<center>
		<div id="col-search" class="row"></div>
		</center>
		
<?php include("footer.php"); ?>