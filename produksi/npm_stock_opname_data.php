<?php
    include("header.php");
    
	$modul        = "Stock Opname Production";
	$file_current = "npm_stock_opname.php";
	$file_name    = "npm_stock_opname_data.php";

function ClosePeriod_php($month, $year, $ses_login)
{
    if($year>=2015)
    {
        $tbl_bincard = "bincard";
    }
    else
    {
        $tbl_bincard = "bincard_".$year;
    }
    
    if($month*1==1)
    {
        $q = "
                UPDATE stock LEFT JOIN 
                        (
                            SELECT warehousecode, inventorycode, SUM(qin) AS quantityin, SUM(qout) AS quantityout
                            FROM ".$tbl_bincard."
                            WHERE MONTH(carddate) = ".sprintf("%02s",$month)." AND YEAR(carddate) = ".$year."
                            GROUP BY warehousecode, inventorycode
                        ) tbl_bincard
                    ON stock.warehousecode = tbl_bincard.warehousecode 
                        AND stock.inventorycode = tbl_bincard.inventorycode 
                    INNER JOIN
                        (
                            SELECT warehousecode, inventorycode, 12end 
                            FROM stock
                            WHERE `year` = ".$year." - 1
                        ) previousbalance
                    ON previousbalance.warehousecode = stock.warehousecode 
                        AND previousbalance.inventorycode = stock.inventorycode
                SET
                    stock.1mutin  = tbl_bincard.quantityin,
                    stock.1mutout = tbl_bincard.quantityout,
                    stock.1end = previousbalance.12end + IFNULL(tbl_bincard.quantityin, 0) - IFNULL(tbl_bincard.quantityout, 0)
                WHERE stock.year = ".$year."
        ";    
    }
    else
    {
        $month_curr = $month*1;
        
        $q = "
                UPDATE stock LEFT JOIN 
                        (
                            SELECT warehousecode, inventorycode, SUM(qin) AS quantityin, SUM(qout) AS quantityout
                            FROM ".$tbl_bincard."
                            WHERE MONTH(carddate) = ".sprintf("%02s",$month)." AND YEAR(carddate) = ".$year."
                            GROUP BY warehousecode, inventorycode
                        ) tbl_bincard
                    ON stock.warehousecode = tbl_bincard.warehousecode 
                        AND stock.inventorycode = tbl_bincard.inventorycode 
                SET
                    stock.".$month_curr."mutin  = tbl_bincard.quantityin,
                    stock.".$month_curr."mutout = tbl_bincard.quantityout,
                    stock.".$month_curr."end = stock.".($month_curr-1)."end + IFNULL(tbl_bincard.quantityin, 0) - IFNULL(tbl_bincard.quantityout, 0)
                WHERE stock.year = ".$year."
        ";    
    }
    
    if(!mysql_query($q))
    {
        $msg = "Gagal ClosePeriod_php";
        echo "<script>alert('".$msg."');</script>"; 
        die();    
    } 
    
    // khusus barang jadi alokasikan ke WH002
    $q = "
            UPDATE 
              stock 
              INNER JOIN inventorymaster 
                ON stock.`inventorycode` = inventorymaster.`inventorycode` 
                AND stock.`year` = '".$year."' 
                AND inventorymaster.`stocktypeid` = '3' 
              INNER JOIN 
              (
                SELECT
                    stock.`inventorycode`,
                    SUM(stock.`".($month*1)."end`) AS qty
                FROM
                    stock
                    INNER JOIN inventorymaster 
                        ON stock.`inventorycode` = inventorymaster.`inventorycode` 
                        AND stock.`year` = '".$year."' 
                        AND inventorymaster.`stocktypeid` = '3' 
                GROUP BY 
                    stock.`inventorycode` 
              )AS tbl_data
                ON tbl_data.inventorycode = stock.`inventorycode`
            SET ".($month*1)."end = tbl_data.qty 
            WHERE 1 
              AND warehousecode = 'WH002' 
    ";
    if(!mysql_query($q))
    {
        $msg = "Gagal Pindahin Stock ke WH002";
        echo "<script>alert('".$msg."');</script>"; 
        die();    
    } 
    
    $q = "
            UPDATE stock 
            INNER JOIN inventorymaster ON
            stock.`inventorycode` = inventorymaster.`inventorycode`
            AND stock.`year` = '".$year."'
            AND inventorymaster.`stocktypeid` = '3'
            SET
            ".($month*1)."end = '0.0000'
            WHERE 1 AND warehousecode != 'WH002'
    ";
    if(!mysql_query($q))
    {
        $msg = "Gagal Update stock menjadi 0 untuk Warehouse Selain WH002";
        echo "<script>alert('".$msg."');</script>"; 
        die();    
    } 

    
        
}

function update_bincard($v_opnamedate, $v_warehousecode, $v_stocktypeid)
{
    $arr_date = explode("-",$v_opnamedate);
    
    $year = $arr_date[0];
    
    if($year>=2015)
    {
        $tbl_bincard = "bincard";
    }
    else
    {
        $tbl_bincard = "bincard_".$year;
    }
    
    $del = "
                DELETE
                    ".$tbl_bincard."
                FROM
                    ".$tbl_bincard."
                    INNER JOIN inventorymaster ON
                        ".$tbl_bincard.".inventorycode = inventorymaster.inventorycode
                        AND inventorymaster.stocktypeid = '".$v_stocktypeid."'
                        AND ".$tbl_bincard.".warehousecode = '".$v_warehousecode."'
                        AND ".$tbl_bincard.".carddate = '".$v_opnamedate."'
                        AND ".$tbl_bincard.".module = 'SO'
    ";
    if(!mysql_query($del))
    {
        $msg = "Gagal Delete Bincard";
        echo "<script>alert('".$msg."');</script>"; 
        die();    
    } 
    
    $q = "
            SELECT
                stockopname.inventorycode,
                stockopname.warehousecode,
                stockopname.physicalquantity,
                stockopname.dataquantity
            FROM
                stockopname
                INNER JOIN inventorymaster ON
                    stockopname.inventorycode = inventorymaster.inventorycode
                    AND inventorymaster.stocktypeid = '".$v_stocktypeid."' 
                    AND stockopname.opnamedate = '".$v_opnamedate."'
                    AND stockopname.warehousecode = '".$v_warehousecode."'
                    AND stockopname.physicalquantity != stockopname.dataquantity
                    AND stockopname.approve = '1'
            WHERE
                1
            ORDER BY
                stockopname.inventorycode ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($inventorycode, $warehousecode, $physicalquantity, $dataquantity) = $row;
        
        $diff = $physicalquantity - $dataquantity;
        
        if($diff*1>0)
        {
            $q = "
                    INSERT INTO 
                        ".$tbl_bincard."
                    SET 
                        `inventorycode` = '".$inventorycode."',
                        `carddate` = '".$v_opnamedate."',
                        `warehousecode` = '".$v_warehousecode."',
                        `referenceno` = 'SO',
                        `module` = 'SO',
                        `qin` = '".abs($diff)."',
                        `qout` = '0.000000',
                        `value` = '0.0000',
                        `adduser` = '".GetUserLogin()."',
                        `adddate` = NOW(),
                        `edituser` = '".GetUserLogin()."',
                        `editdate` = NOW()
            ";
        }
        else if($diff*1<0)
        {
            $q = "
                    INSERT INTO 
                        ".$tbl_bincard."
                    SET 
                        `inventorycode` = '".$inventorycode."',
                        `carddate` = '".$v_opnamedate."',
                        `warehousecode` = '".$v_warehousecode."',
                        `referenceno` = 'SO',
                        `module` = 'SO',
                        `qin` = '0.000000',
                        `qout` = '".abs($diff)."',
                        `value` = '0.0000',
                        `adduser` = '".GetUserLogin()."',
                        `adddate` = NOW(),
                        `edituser` = '".GetUserLogin()."',
                        `editdate` = NOW()
            ";
        }
        
        //echo $q;
        //echo "<hr>";
        
        if(!mysql_query($q))
        {
            $msg = "Gagal Update Bincard";
            echo "<script>alert('".$msg."');</script>"; 
            die();    
        } 
    }
    
}

unset($arr_data, $arr_curr);  

$ajax = $_REQUEST["ajax"];       

if($ajax)
{
    if($ajax=='search')
    {
        $search_month       = trim($_GET["search_month"]);
        $search_year        = trim($_GET["search_year"]);
        $search_warehouse   = trim($_GET["search_warehouse"]);
        
        $v_unik_curr        = $_GET["v_unik_curr"];
        
        $where = "";
        
        if($search_month)
        {
            $where .= " AND MONTH(stockopname_header.opnamedate) = '".$search_month."' ";    
        }
        
        if($search_year)
        {
            $where .= " AND YEAR(stockopname_header.opnamedate) = '".$search_year."' ";    
        }
        
        if($search_warehouse)
        {
            $where .= " AND stockopname_header.warehousecode = '".$search_warehouse."' ";    
        }
        
        $q = "
                SELECT
                    warehouse.warehousecode,
                    warehouse.warehousename
                FROM
                    warehouse
                WHERE
                    1
                ORDER BY
                    warehouse.warehousecode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
            
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
            
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
        
        $arr_data["list_stocktype"][1] = "Baku";
        $arr_data["list_stocktype"][2] = "Kemas";
        $arr_data["list_stocktype"][3] = "Barang Jadi";
        
        $q = "
            SELECT
                stockopname_header.sono,
                stockopname_header.opnamedate,
                stockopname_header.warehousecode,
                stockopname_header.stocktypeid,
                stockopname_header.approve_by,
                stockopname_header.approve_date
            FROM
                stockopname_header
            WHERE
                1
                ".$where."
            ORDER BY
                stockopname_header.opnamedate ASC
        ";
        //echo $q;
        //die();
        $counter = 0;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($sono, $opnamedate, $warehousecode, $stocktypeid, $approve_by, $approve_date) = $row;
            
            $arr_data["list_stockopname"][$counter] = $counter;
            
            $arr_data["sono"][$counter] = $sono;
            $arr_data["opnamedate"][$counter] = $opnamedate;
            $arr_data["warehousecode"][$counter] = $warehousecode;
            $arr_data["stocktypeid"][$counter] = $stocktypeid;
            $arr_data["approve_by"][$counter] = $approve_by;
            $arr_data["approve_date"][$counter] = $approve_date;
            
            $counter++;
        } 
    
        $jml = count($arr_data["list_stockopname"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }

    ?>
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
    	<thead>
			<tr >
				<th><strong><center>No</center></strong></th>
				<th><strong><center>So No</center></strong></th>
				<th><strong><center>Opname Date</center></strong></th>
				<th><strong><center>Warehouse</center></strong></th>
				<th><strong><center>Type</center></strong></th>
				<th><strong><center>Approve</center></strong></th>
			</tr>
		</thead>
		<tbody>
    		<?php
			if(count($arr_data["list_stockopname"])*1==0)
			{
				echo "
					<tr>
						<td colspan='100%' align='center'>No Data</td>
					</tr>
				";
			}

		    $nomor = 0;    
		    foreach($arr_data["list_stockopname"] as $counter => $val)
		    {    
		        $nomor++;
		        
		        $sono = $arr_data["sono"][$counter];
		        $opnamedate = $arr_data["opnamedate"][$counter];
		        $warehousecode = $arr_data["warehousecode"][$counter];
		        $stocktypeid = $arr_data["stocktypeid"][$counter];
		        $approve_by = $arr_data["approve_by"][$counter];
		        $approve_date = $arr_data["approve_date"][$counter];
		        
		        $v_unik = $opnamedate."||".$warehousecode."||".$stocktypeid;
		        
		        $style_color = "";
		        if($v_unik_curr==$v_unik)
		        {
		            $style_color = "background:#cafdb5;";    
		        }
		        
		        if($approve_by=="")
		        {
		            $style_bg_approve = "background:#FFFF00";
		            $approve_echo = "Waiting Approval";
		        }
		        else
		        {
		            $style_bg_approve = "";
		            $approve_echo = format_show_datetime($approve_date)." :: ".$approve_by;
		        }
		        
			    ?>
			    <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$style_bg; ?>">
			        <td onclick="CallAjax('edit_data','<?php echo $v_unik; ?>')" style="<?php echo $style_bg; ?>"><?php echo $nomor; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $v_unik; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo $sono; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $v_unik; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo format_show_date($opnamedate); ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $v_unik; ?>')" style="<?php echo $style_bg; ?>"><?php echo $arr_data["warehousename"][$warehousecode]; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $v_unik; ?>')" style="<?php echo $style_bg; ?>"><?php echo $arr_data["list_stocktype"][$stocktypeid]; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $v_unik; ?>')" style="<?php echo $style_bg.$style_bg_approve; ?>">&nbsp;<?php echo $approve_echo; ?></td>
                    
			    </tr>
			    <?php
		    }
		    ?>
    
    	</tbody>
    </table>
    </form>
    </div>
    <?php
    
    }
    else if($ajax=="add_data")
    {    
        $arr_data["list_month"][1] = "January";
        $arr_data["list_month"][2] = "February";
        $arr_data["list_month"][3] = "March";
        $arr_data["list_month"][4] = "April";
        $arr_data["list_month"][5] = "May";
        $arr_data["list_month"][6] = "June";
        $arr_data["list_month"][7] = "July";
        $arr_data["list_month"][8] = "August";
        $arr_data["list_month"][9] = "September";
        $arr_data["list_month"][10] = "October";
        $arr_data["list_month"][11] = "November";
        $arr_data["list_month"][12] = "December";
        
        $start_year = date("Y")-1;
        $end_year   = date("Y");
        for($i=$start_year;$i<=$end_year;$i++)
        {
            $arr_data["list_year"][$i] = $i;    
        }
        
        // cek warehouseadmin
        $q = "
                SELECT
                    warehouseadmin.warehousecode
                FROM
                    warehouseadmin
                WHERE
                    1
                    AND warehouseadmin.userid = '".$ses_login."'
                    AND warehouseadmin.warehousecode IN('WH000','WH001','WH002','WH005','WH008')
        ";
        $qry_warehouseadmin = mysql_query($q);
        while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
        {
            list($warehousecode) = $row_warehouseadmin;
            
            $arr_data["warehouseadmin"][$warehousecode] = $warehousecode;
        }
        
        
        $q = "
                SELECT
                    warehouse.warehousecode,
                    warehouse.warehousename
                FROM
                    warehouse
                WHERE
                    1
                    AND warehouse.warehousecode IN('WH000','WH001','WH002','WH005','WH008')
                ORDER BY
                    warehouse.warehousecode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
            
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
            
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
        
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Add <?php echo $modul; ?>
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">
                
                <tr>
                    <td class="title_table" width="250">Period</td>
                    <td>
                        <select name="v_month" id="v_month" class="form-control-new">
                            <?php 
                                foreach($arr_data["list_month"] as $month => $val)
                                {
                                    $selected = "";
                                    if(sprintf("%02s",$month)==date("m"))
                                    {
                                        $selected = "selected='selected'";
                                    }
                            ?>
                                    <option <?php echo $selected; ?> value="<?php echo sprintf("%02s",$month); ?>"><?php echo $arr_data["list_month"][$month]; ?></option>
                            <?php        
                                }
                            ?>        
                        </select>
                        
                        <select name="v_year" id="v_year" class="form-control-new">
                            <?php 
                                foreach($arr_data["list_year"] as $year => $val)
                                {
                                    $selected = "";
                                    if($year==date("Y"))
                                    {
                                        $selected = "selected='selected'";
                                    }
                            ?>
                                    <option <?php echo $selected; ?> value="<?php echo $year; ?>"><?php echo $arr_data["list_year"][$year]; ?></option>
                            <?php        
                                }
                            ?>            
                        </select>     
                    </td>
                </tr>

                <tr>
                    <td class="title_table">Warehouse</td>
                    <td>
                        <select name="v_warehousecode" id="v_warehousecode" class="form-control-new" onchange="CallAjax('ajax_warehouse', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
                                {
                                    if($arr_data["warehouseadmin"][$warehousecode]==$warehousecode)
                                    {
                                        $warehousename = $arr_data["warehousename"][$warehousecode];
                                ?>
                                        <option value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
                                <?php 
                                    }
                                }
                            ?>
                        </select>
                        &nbsp;
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Type</td>
                    <td id="td_type">
                        &nbsp;
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                    	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                    </td>
                </tr>
            </table>
            </form>
        
        </div>
        <?php
    }
    else if($ajax=="edit_data")
    {
        $v_unik_id = $_GET["v_unik_id"];
        
        $_SESSION["ses_unik_id"] = $v_unik_id;
        
        $arr_unik  = explode("||", $v_unik_id);
        
        $arr_date  = explode("-", $arr_unik[0]);
        
        $q = "
                SELECT
                    stockopname.inventorycode,
                    inventorymaster.inventoryname,
                    stockopname.physicalquantity,
                    stockopname.dataquantity,
                    stockopname.approve,
                    stockopname.approve_by,
                    stockopname.approve_date
                FROM
                    stockopname
                    INNER JOIN inventorymaster ON
                        stockopname.inventorycode = inventorymaster.inventorycode
                        AND stockopname.opnamedate = '".$arr_unik[0]."'
                        AND stockopname.warehousecode = '".$arr_unik[1]."'
                        AND inventorymaster.stocktypeid = '".$arr_unik[2]."'
                        AND inventorymaster.inventorycode != '01-00097'
                WHERE
                    1
                ORDER BY
                    stockopname.inventorycode ASC
        ";
        $counter = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname, $physicalquantity, $dataquantity, $approve, $approve_by, $approve_date) = $row;
            
            $arr_data["list_detail"][$counter] = $counter;
            
            $arr_data["approve"]      = $approve;
            $arr_data["approve_by"]   = $approve_by;
            $arr_data["approve_date"] = $approve_date;
            
            $arr_data["detail_inventorycode"][$counter] = $inventorycode;
            $arr_data["detail_inventoryname"][$counter] = $inventoryname;
            $arr_data["detail_physicalquantity"][$counter] = $physicalquantity;
            $arr_data["detail_dataquantity"][$counter] = $dataquantity;
            
            $counter++;
        }
        
        $q = "
                SELECT
                    warehouse.warehousecode,
                    warehouse.warehousename
                FROM
                    warehouse
                WHERE
                    1
                ORDER BY
                    warehouse.warehousecode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
            
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
            
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
        
        $arr_data["list_stocktype"][1] = "Baku";
        $arr_data["list_stocktype"][2] = "Kemas";
        $arr_data["list_stocktype"][3] = "Barang Jadi";
        
        if($arr_unik[2]==1)
        {
            $decimal_js = "6";    
            $decimal_php = 6;    
        }
        else
        {
            $decimal_js = "";    
            $decimal_php = 0;    
        }
        
        $q = "
                SELECT 
                    uni.imno
                FROM
                (
                    SELECT 
                        internalmutation.imno 
                    FROM 
                        internalmutation 
                    WHERE 
                        1
                        AND MONTH(internalmutation.imdate) = '".$arr_date[1]."'
                        AND YEAR(internalmutation.imdate) = '".$arr_date[0]."'
                        AND internalmutation.isconfirmed != '1' 
                        AND internalmutation.muttype IN ('mut') 
                        AND (fromwarehouse IN('".$arr_unik[1]."') OR towarehouse IN('".$arr_unik[1]."')) 
                        AND internalmutation.status NOT IN('1','2') 

                    UNION ALL

                    SELECT 
                        internalmutation.imno 
                    FROM 
                        internalmutation 
                    WHERE 
                        1
                        AND MONTH(internalmutation.imdate) = '".$arr_date[1]."'
                        AND YEAR(internalmutation.imdate) = '".$arr_date[0]."'
                        AND internalmutation.muttype IN ('in') 
                        AND towarehouse IN('".$arr_unik[1]."')
                        AND internalmutation.status NOT IN('1','2') 
                       
                    UNION ALL

                    SELECT 
                        internalmutation.imno 
                    FROM 
                        internalmutation 
                    WHERE 
                        1
                        AND MONTH(internalmutation.imdate) = '".$arr_date[1]."'
                        AND YEAR(internalmutation.imdate) = '".$arr_date[0]."'
                        AND internalmutation.muttype IN ('out') 
                        AND fromwarehouse IN('".$arr_unik[1]."')
                        AND internalmutation.status NOT IN('1','2') 
                ) AS uni
                WHERE
                    1
                ORDER BY
                    uni.imno ASC             
        ";
        //echo "<pre>$q</pre>";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($imno) = $row;
            
            $arr_data["list_imno_unconfirm"][$imno] = $imno;
        }
        
        $q = "
                SELECT
                    receiptgood.rgno
                FROM
                    receiptgood
                WHERE
                    1
                    AND receiptgood.warehouse = '".$arr_unik[1]."'
                    AND MONTH(receiptgood.rgdate) = '".$arr_date[1]."'
                    AND YEAR(receiptgood.rgdate) = '".$arr_date[0]."'
                    AND receiptgood.status = '0'
                ORDER BY
                    receiptgood.rgno ASC            
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($rgno) = $row;
            
            $arr_data["list_rgno_unclose"][$rgno] = $rgno;
        }
        
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="#">
						<i class="entypo-pencil"></i>Edit <?php echo $modul; ?>
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="edit_data">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_sts_approve" id="v_sts_approve" value="">
            <input type="hidden" name="v_sts_unapprove" id="v_sts_unapprove" value="">
            <input type="hidden" name="v_sts_close_period" id="v_sts_close_period" value="">
            <input type="hidden" name="v_opnamedate" id="v_opnamedate" value="<?php echo $arr_unik[0]; ?>">
            <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $arr_unik[1]; ?>">
            <input type="hidden" name="v_stocktypeid" id="v_stocktypeid" value="<?php echo $arr_unik[2]; ?>">
            
            <table class="table table-bordered responsive">

                <tr>
                    <td class="title_table" width="250">Opname Date</td>
                    <td style="font-weight:bold; ">
                        <?php echo format_show_date($arr_unik[0]); ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Warehouse</td>
                    <td style="font-weight:bold; ">
                        <?php 
                            echo $arr_data["warehousename"][$arr_unik[1]];
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Type</td>
                    <td style="font-weight:bold; ">
                       <?php 
                            echo $arr_data["list_stocktype"][$arr_unik[2]];
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Approve</td>
                    <td style="font-weight:bold; ">
                       <?php 
                        if($arr_data["approve_by"])
                        {
                            echo format_show_datetime($arr_data["approve_date"])." :: ".$arr_data["approve_by"];
                        }
                        else
                        {
                            echo "&nbsp;";
                        }
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?php 
                            if($arr_data["approve"]==0)
                            {
                        ?>
                        		<button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button>
                        <?php 
                            }
                            else
                            {
                                echo "<font color='red'>Button Save tidak muncul karena sudah di approve</font>";
                            }
                        ?>
                        &nbsp;&nbsp;
                        <?php 
                            if($menu_validation_approval["v"])
                            {
                                if($arr_data["approve"]==0)
                                {
                                    ?>
                                    	<button type="button" name="btn_approve" id="btn_approve" onclick="confirm_approve()" class="btn btn-info btn-icon btn-sm icon-left" value="Approve">Approve<i class="entypo-thumbs-up"></i></button>
                                    <?php        
                                }
                                else if($arr_data["approve"]==1)
                                {
                                    ?>
                                    	<button type="button" name="btn_unapprove" id="btn_unapprove" onclick="confirm_unapprove()" class="btn btn-info btn-icon btn-sm icon-left" value="UnApprove">UnApprove<i class="entypo-cancel"></i></button>
                                        <button type="button" name="btn_close_period" id="btn_close_period" onclick="confirm_close_period()" class="btn btn-info btn-icon btn-sm icon-left" value="Close Period">Close Period<i class="entypo-cancel"></i></button>
                                    <?php 
                                }
                            }
                        ?>
                        
                    </td>
                </tr>
                
                <?php 
                    if(count($arr_data["list_rgno_unclose"]))
                    {
                ?>
                <tr>
                    <td colspan="100%"><b>List Receipt Good UnClose</b></td>
                </tr>
                
                <tr>
                    <td colspan="100%">
                        <?php 
                            $echo = "";
                            foreach($arr_data["list_rgno_unclose"] as $rgno => $val)
                            {
                                $echo .= $rgno."<br>";    
                            }
                            $echo = substr($echo, 0, -4);
                            echo $echo;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <?php 
                    }
                ?>
                
                <?php 
                    if(count($arr_data["list_imno_unconfirm"]))
                    {
                ?>
                <tr>
                    <td colspan="100%"><b>List Internal Mutation UnConfirm</b></td>
                </tr>
                
                <tr>
                    <td colspan="100%">
                        <?php 
                            $echo = "";
                            foreach($arr_data["list_imno_unconfirm"] as $imno => $val)
                            {
                                $echo .= $imno."<br>";    
                            }
                            $echo = substr($echo, 0, -4);
                            echo $echo;
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <?php 
                    }
                ?>
                
                <tr>
                    <td colspan="100%">
                        &nbsp;
                         <?php 
                            if($arr_data["approve"]==0)
                            {
                         ?>
                         		<button type="button" onclick="pop_up_details()" class="btn btn-info btn-icon btn-sm icon-left" value="Import CSV">Import CSV<i class="entypo-download"></i></button>
                         <?php       
                            }
                        ?>
                    </td>
                </tr>
                

                <tr>
                    <td colspan="100%">
                        <table class="table table-bordered responsive">
						    <thead>
								<tr>
									<th>No</th>
									<th>PCode</th>
									<th>Nama Barang</th>
									<th>Physicalquantity</th>
									<th>Dataquantity</th>
									<th>Diff</th>
								</tr>
							</thead>
							<tbody>
                            
                            <?php
                                $arr_date = explode("-", $arr_unik[0]);
                                
                                if($arr_data["approve"]==0)
                                {
                                    //bincard_stock($month, $year, $search_inventorycode="All", $search_warehousecode="All", $search_stocktypeid="All")
                                    
                                    if($arr_unik[2]==3)
                                    {
                                        $arr_stock = bincard_stock($arr_date[1], $arr_date[0], $arr_data["detail_inventorycode"], "All", $arr_unik[2]);
                                    }
                                    else
                                    {
                                        $arr_stock = bincard_stock($arr_date[1], $arr_date[0], $arr_data["detail_inventorycode"], $arr_unik[1], $arr_unik[2]);                                    
                                    }
                                    //echo "<pre>";
                                    //print_r($arr_stock);
                                    //echo "</pre>";
                                }
                                
                                $no = 1; 
                                foreach($arr_data["list_detail"] as $counter => $val)
                                {
                                    $inventorycode = $arr_data["detail_inventorycode"][$counter];
                                    $inventoryname = $arr_data["detail_inventoryname"][$counter];
                                    $physicalquantity = $arr_data["detail_physicalquantity"][$counter];
                                    
                                    if($arr_data["approve"]==1)
                                    {
                                        $dataquantity = $arr_data["detail_dataquantity"][$counter];
                                    }
                                    else
                                    {
                                        $dataquantity = $arr_stock["akhir"][$inventorycode];
                                    }
                                    
                                    $diff = $physicalquantity - $dataquantity;                                    
                                    
                            		?>
		                             <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
		                                <td>
		                                    <?php echo format_number($no); ?>
		                                    <input type="hidden" name="v_inventorycode[<?php echo $inventorycode; ?>]" value="<?php echo $inventorycode; ?>">
		                                    <input type="hidden" style="text-align:right;" name="v_dataquantity_<?php echo $inventorycode; ?>" id="v_dataquantity_<?php echo $no; ?>" value="<?php echo $dataquantity; ?>">
		                                </td>
		                                <td><?php echo $inventorycode; ?></td>
		                                <td><?php echo $inventoryname; ?></td>
		                                <td align="right">
		                                    <input type="text" title="Double Klik Untuk Copy Data Quantity" class="text" size="15" style="text-align:right;" onkeyup="calculate()" ondblclick="copy_dataquantity('<?php echo $no; ?>')" onblur="toFormat<?php echo $decimal_js; ?>('v_physicalquantity_<?php echo $no; ?>')" name="v_physicalquantity_<?php echo $inventorycode; ?>" id="v_physicalquantity_<?php echo $no; ?>" value="<?php echo format_number($physicalquantity, $decimal_php); ?>">
		                                </td>
		                                <td align="right">&nbsp;<?php echo format_number($dataquantity, $decimal_php); ?></td>
		                                <td align="right" id="td_diff_<?php echo $no; ?>">&nbsp;<?php echo format_number($diff, $decimal_php); ?></td>
		                                
		                            </tr>
		                            <?php
                                    $arr_total["physicalquantity"] += $physicalquantity;
                                    $arr_total["dataquantity"] += $dataquantity;
                                    
                                    $no++;  
                                }
                                
                                $diff = $arr_total["physicalquantity"] - $arr_total["dataquantity"];
                                
                                $no--;
                            ?>
                            
	                            <tr style="font-weight:bold; text-align:right; ">
	                                <td colspan="3">
	                                    Total
	                                    <input type="hidden" name="jml_data" id="jml_data" value="<?php echo $no; ?>">
	                                </td>
	                                <td id="total_physicalquantity">&nbsp;<?php echo format_number($arr_total["physicalquantity"], $decimal_php); ?></td>
	                                <td id="total_dataquantity">&nbsp;<?php echo format_number($arr_total["dataquantity"], $decimal_php); ?></td>
	                                <td id="total_diff">&nbsp;<?php echo format_number($diff, $decimal_php); ?></td>
	                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
            </table>
            </form>
        </div>
        <?php    
    }
    
    else if($ajax=="ajax_warehouse")
    {
        $v_warehousecode = $_GET["v_warehousecode"];
        
        if($v_warehousecode=="WH000")
        {
            ?>
                <select name="v_stocktypeid" id="v_stocktypeid" class="form-control-new">
                    <option value="1">Baku</option>
                </select>
            <?php
        }
        else if($v_warehousecode=="WH001")
        {
            ?>
                <select name="v_stocktypeid" id="v_stocktypeid" class="form-control-new">
                    <option value="2">Kemas</option>
                </select>
            <?php
        }
        
        else if($v_warehousecode=="WH002")
        {
            ?>
                <select name="v_stocktypeid" id="v_stocktypeid" class="form-control-new">
                    <option value="1">Baku</option>
                    <option value="2">Kemas</option>
                    <option value="3">Barang Jadi</option>
                </select>
            <?php
        }
        else if($v_warehousecode=="WH005")
        {
            ?>
                <select name="v_stocktypeid" id="v_stocktypeid" class="form-control-new">
                    <option value="3">Barang Jadi</option>
                </select>
            <?php
        }
        else if($v_warehousecode=="WH008")
        {
            ?>
                <select name="v_stocktypeid" id="v_stocktypeid" class="form-control-new">
                    <option value="1">Baku</option>
                    <option value="2">Kemas</option>
                </select>
            <?php
        }
        
    }

    exit();
}
 

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :
            {
                if(!isset($_POST["v_month"])){ $v_month = isset($_POST["v_month"]); } else { $v_month = $_POST["v_month"]; }
                if(!isset($_POST["v_year"])){ $v_year = isset($_POST["v_year"]); } else { $v_year = $_POST["v_year"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_stocktypeid"])){ $v_stocktypeid = isset($_POST["v_stocktypeid"]); } else { $v_stocktypeid = $_POST["v_stocktypeid"]; }
                
                $opname_date = date("t", parsedate("01/".$v_month."/".$v_year))."-".$v_month."-".$v_year;
                
                $acc_locked = call_acc_locked("stock_opname", format_save_date($opname_date), format_save_date($opname_date));
                
                if($acc_locked["locked"][format_save_date($opname_date)]*1==1)
                {
                    $msg = "Data tanggal ".$opname_date." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    // validasi
                    {
                        if($v_warehousecode=="")
                        {
                            $msg = "Warehouse harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }                
                        else if($v_stocktypeid=="")
                        {
                            $msg = "Type harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                    }   
                        
                    $q = "
                            SELECT
                                stockopname.opnamedate
                            FROM
                                stockopname
                                INNER JOIN inventorymaster ON
                                    stockopname.inventorycode = inventorymaster.inventorycode
                                    AND inventorymaster.stocktypeid = '".$v_stocktypeid."'
                                    AND stockopname.warehousecode = '".$v_warehousecode."'
                            WHERE
                                1
                                AND MONTH(stockopname.opnamedate) = '".$v_month."'
                                AND YEAR(stockopname.opnamedate) = '".$v_year."'
                            LIMIT
                                0,1
                    ";
                    $qry_cek = mysql_query($q);
                    $row_cek = mysql_fetch_array($qry_cek);
                    
                    if($row_cek["opnamedate"])
                    {
                        $msg = "Opname Sudah ada";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();    
                    }
                    else
                    {
                        
                    // buat counter
                    {
                        $arr_opnamedate = explode("-", $opname_date);
                        
                        $v_sono_id = "SO";
                        
                        //SO0094/VCI/03/11
                        $q = "
                                SELECT
                                    stockopname_header.sono
                                FROM
                                    stockopname_header
                                WHERE
                                    1
                                    AND MONTH(stockopname_header.opnamedate) = '".$arr_opnamedate[1]."'
                                    AND YEAR(stockopname_header.opnamedate) = '".$arr_opnamedate[2]."'
                                    AND LEFT(stockopname_header.sono,2) = '".$v_sono_id."'
                                ORDER BY
                                    stockopname_header.sono DESC
                                LIMIT
                                    0,1
                        ";
                        $qry_counter = mysql_query($q);
                        $row_counter = mysql_fetch_array($qry_counter);
                        
                        $counter = (str_replace($v_sono_id, "", substr($row_counter["sono"],0,6))*1)+1;
                        
                        $sono = $v_sono_id.sprintf("%04s", $counter)."/NPM/".$arr_opnamedate[1]."/".substr($arr_opnamedate[2],2,2);
                    }  
                        
                        $q = "
                                INSERT INTO 
                                    `stockopname_header`
                                SET 
                                    `sono` = '".$sono."',
                                    `opnamedate` = '".format_save_date($opname_date)."',
                                    `warehousecode` = '".$v_warehousecode."',
                                    `stocktypeid` = '".$v_stocktypeid."',
                                    `adduser` = '".$ses_login."',
                                    `adddate` = NOW(),
                                    `edituser` = '".$ses_login."',
                                    `editdate` = NOW(),
                                    `approve_by` = '',
                                    `approve_date` = ''
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Insert opname";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        else
                        {
                            $q = "
                                    SELECT
                                        inventorymaster.inventorycode
                                    FROM
                                        inventorymaster
                                    WHERE
                                        1
                                        AND inventorymaster.stocktypeid = '".$v_stocktypeid."'
                                    ORDER BY
                                        inventorymaster.inventorycode ASC
                            ";
                            $qry = mysql_query($q);
                            while($row = mysql_fetch_array($qry))
                            {
                                list($inventorycode) = $row;
                                
                                $q = "
                                        INSERT INTO 
                                            `stockopname`
                                        SET 
                                            `opnamedate` = '".format_save_date($opname_date)."',
                                            `inventorycode` = '".$inventorycode."',
                                            `physicalquantity` = '0.0000',
                                            `dataquantity` = '0.0000',
                                            `warehousecode` = '".$v_warehousecode."',
                                            `value` = '0.0000',
                                            `adduser` = '".$ses_login."',
                                            `adddate` = NOW(),
                                            `edituser` = '".$ses_login."',
                                            `editdate` = NOW(),
                                            `approve` = '0',
                                            `isposted` = '0',
                                            `approve_by` = '',
                                            `approve_date` = ''
                                ";
                                
                                
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal insert opname";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                }
                            }
                            
                            $v_unik = format_save_date($opname_date)."||".$v_warehousecode."||".$v_stocktypeid;
                            
                            $msg = "Berhasil menyimpan";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_unik."');</script>";         
                            die();     
                        }
                        
                            
                    }
                    
                }
                
                
            }
            break;
        case "edit_data" :
            {
                if(!isset($_POST["v_opnamedate"])){ $v_opnamedate = isset($_POST["v_opnamedate"]); } else { $v_opnamedate = $_POST["v_opnamedate"]; }   
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }   
                if(!isset($_POST["v_stocktypeid"])){ $v_stocktypeid = isset($_POST["v_stocktypeid"]); } else { $v_stocktypeid = $_POST["v_stocktypeid"]; }   
                
                if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }   
                
                if(!isset($_POST["v_sts_approve"])){ $v_sts_approve = isset($_POST["v_sts_approve"]); } else { $v_sts_approve = $_POST["v_sts_approve"]; }   
                if(!isset($_POST["v_sts_unapprove"])){ $v_sts_unapprove = isset($_POST["v_sts_unapprove"]); } else { $v_sts_unapprove = $_POST["v_sts_unapprove"]; }   
                if(!isset($_POST["v_sts_close_period"])){ $v_sts_close_period = isset($_POST["v_sts_close_period"]); } else { $v_sts_close_period = $_POST["v_sts_close_period"]; }   
                
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }
                if(!isset($_POST["btn_unapprove"])){ $btn_unapprove = isset($_POST["btn_unapprove"]); } else { $btn_unapprove = $_POST["btn_unapprove"]; }
                
                $v_unik = $v_opnamedate."||".$v_warehousecode."||".$v_stocktypeid; 
                
                $acc_locked = call_acc_locked("stock_opname", $v_opnamedate, $v_opnamedate);
                
                if($acc_locked["locked"][$v_opnamedate]*1==1)
                {
                    $msg = "Data tanggal ".$v_imdate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {   
                    if($btn_save)
                    {
                        foreach($v_inventorycode as $inventorycode => $val)
                        {
                            if(!isset($_POST["v_physicalquantity_".$inventorycode])){ $v_physicalquantity = isset($_POST["v_physicalquantity_".$inventorycode]); } else { $v_physicalquantity = $_POST["v_physicalquantity_".$inventorycode]; }       
                            if(!isset($_POST["v_dataquantity_".$inventorycode])){ $v_dataquantity = isset($_POST["v_dataquantity_".$inventorycode]); } else { $v_dataquantity = $_POST["v_dataquantity_".$inventorycode]; }       
                            
                            $v_physicalquantity = save_int($v_physicalquantity);
                            $v_dataquantity = save_int($v_dataquantity);
                            
                            if($v_physicalquantity*1<0)
                            {
                                $msg = "Physicalquantity ".$inventorycode." tidak boleh minus";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();
                            }
                        }
                        
                        foreach($v_inventorycode as $inventorycode => $val)
                        {
                            if(!isset($_POST["v_physicalquantity_".$inventorycode])){ $v_physicalquantity = isset($_POST["v_physicalquantity_".$inventorycode]); } else { $v_physicalquantity = $_POST["v_physicalquantity_".$inventorycode]; }       
                            if(!isset($_POST["v_dataquantity_".$inventorycode])){ $v_dataquantity = isset($_POST["v_dataquantity_".$inventorycode]); } else { $v_dataquantity = $_POST["v_dataquantity_".$inventorycode]; }       
                            
                            $v_physicalquantity = save_int($v_physicalquantity);
                            $v_dataquantity = save_int($v_dataquantity);
                            
                            $q = "
                                    UPDATE
                                        stockopname
                                    SET
                                        physicalquantity = '".$v_physicalquantity."',
                                        dataquantity = '".$v_dataquantity."',
                                        edituser = '".$ses_login."',
                                        editdate = NOW()
                                    WHERE
                                        1
                                        AND opnamedate = '".$v_opnamedate."'
                                        AND warehousecode = '".$v_warehousecode."'
                                        AND inventorycode = '".$inventorycode."'
                            ";
                            //echo $q."<hr>";
                            if(!mysql_query($q))
                            {
                                echo $q;
                                $msg = "Gagal update opname";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                        }
                            
                        $msg = "Berhasil menyimpan";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_unik."');</script>";         
                        die();
                    }
                    
                    if($v_sts_approve)
                    {
                        $q = "
                                UPDATE
                                    stockopname_header
                                SET
                                    approve_by = '".$ses_login."',
                                    approve_date = NOW()
                                WHERE
                                    1
                                    AND opnamedate = '".$v_opnamedate."'
                                    AND warehousecode = '".$v_warehousecode."'
                                    AND stocktypeid = '".$v_stocktypeid."'
                        ";
                        
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update Approval";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        else
                        {
                            $q = "
                                    UPDATE
                                        stockopname
                                        INNER JOIN inventorymaster ON
                                            stockopname.inventorycode = inventorymaster.inventorycode
                                            AND inventorymaster.stocktypeid = '".$v_stocktypeid."'
                                    SET
                                        approve = '1',
                                        approve_by = '".$ses_login."',
                                        approve_date = NOW()
                                    WHERE
                                        1
                                        AND opnamedate = '".$v_opnamedate."'
                                        AND warehousecode = '".$v_warehousecode."'
                                        
                            "; 
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Update Approval Detail";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            } 
                            
                            update_bincard($v_opnamedate, $v_warehousecode, $v_stocktypeid);
                            
                            $msg = "Berhasil Approve";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_unik."');</script>";         
                            die();    
                              
                        }
                        
                    }
                    
                    if($v_sts_unapprove)
                    {
                        $q = "
                                UPDATE
                                    stockopname_header
                                SET
                                    approve_by = '',
                                    approve_date = NOW()
                                WHERE
                                    1
                                    AND opnamedate = '".$v_opnamedate."'
                                    AND warehousecode = '".$v_warehousecode."'
                                    AND stocktypeid = '".$v_stocktypeid."'
                        ";
                        
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update UnApproval";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        else
                        {
                            $q = "
                                    UPDATE
                                        stockopname
                                        INNER JOIN inventorymaster ON
                                            stockopname.inventorycode = inventorymaster.inventorycode
                                            AND inventorymaster.stocktypeid = '".$v_stocktypeid."'
                                    SET
                                        approve = '0',
                                        approve_by = '',
                                        approve_date = NOW()
                                    WHERE
                                        1
                                        AND opnamedate = '".$v_opnamedate."'
                                        AND warehousecode = '".$v_warehousecode."'
                            "; 
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Update UnApproval Detail";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            } 
                            
                            update_bincard($v_opnamedate, $v_warehousecode, $v_stocktypeid);
                            
                            $msg = "Berhasil UnApprove";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_unik."');</script>";         
                            die();    
                              
                        }    
                    }
                    
                    if($v_sts_close_period)
                    {
                        $arr_date = explode("-", $v_opnamedate);
                        
//                        $q = "CALL ClosePeriod('".$arr_date[1]."', '".$arr_date[0]."', '".GetUserLogin()."')";
//                        if(!mysql_query($q))
//                        {
//                            $msg = "Gagal Update Close Period";
//                            echo "<script>alert('".$msg."');</script>"; 
//                            die();    
//                        } 
                        
                        ClosePeriod_php($arr_date[1], $arr_date[0], $ses_login);
                        
                        if($arr_date[0]>=2015)
                        {
                            $tbl_bincard = "bincard";
                        }
                        else
                        {
                            $tbl_bincard = "bincard_".$year;
                        }
                        
                        
                        
                        if($arr_date[1]==1)
                        {
                            $mm_prev   = 12;
                            $yyyy_prev = ($arr_date[0])-1;
                        }
                        else
                        {
                            $mm_prev   = ($arr_date[1])-1; 
                            $yyyy_prev = $arr_date[0];
                        }
                        
                        $prev_period =  $yyyy_prev."-".sprintf("%02s", $mm_prev)."-".date("t", parsedate("01/".$mm_prev."/".$yyyy_prev));
                        
                        $period_start =  $arr_date[0]."-".sprintf("%02s", $arr_date[1])."-01";
                        $period_end   =  $arr_date[0]."-".sprintf("%02s", $arr_date[1])."-".date("t", parsedate("01/".$arr_date[1]."/".$arr_date[0]));
                        
                        $period_start_curr =  date("Y-m")."-01";
                        $period_end_curr   =  date("Y-m-t");
                        
                        if(date("m")*1==1)
                        {
                            $mm_prev   = 12;
                            $yyyy_prev = (date("Y"))-1;
                        }
                        else
                        {
                            $mm_prev   = (date("m"))-1; 
                            $yyyy_prev = date("Y");
                        }
                        
                        // update inventorywarehouse
                        $q = "
                                UPDATE    
                                    inventorywarehouse
                                    LEFT JOIN
                                    (
                                        SELECT
                                            bincard.inventorycode,
                                            bincard.warehousecode,
                                            SUM(bincard.qin-bincard.qout) AS jml_bincard
                                        FROM 
                                            bincard
                                        WHERE
                                            1
                                            AND bincard.carddate BETWEEN '".$period_start_curr."' AND '".$period_end_curr."'
                                        GROUP BY
                                            bincard.inventorycode,
                                            bincard.warehousecode
                                    )AS tbl_bincard ON
                                        tbl_bincard.inventorycode = inventorywarehouse.inventorycode    
                                        AND tbl_bincard.warehousecode = inventorywarehouse.warehousecode            
                                    LEFT JOIN 
                                    (
                                        SELECT
                                            stock.inventorycode,
                                            stock.warehousecode,
                                            stock.".$mm_prev."end    
                                        FROM
                                            stock
                                        WHERE
                                            stock.year = '".$yyyy_prev."'
                                            
                                    )AS tbl_stock ON
                                        tbl_stock.inventorycode = inventorywarehouse.inventorycode
                                        AND tbl_stock.warehousecode = inventorywarehouse.warehousecode
                                SET
                                    inventorywarehouse.quantity =  (COALESCE(tbl_stock.".$mm_prev."end,0) + COALESCE(tbl_bincard.jml_bincard,0))
                                WHERE
                                    1 
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update Inventory Warehouse";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        } 
                        
                        $q = "
                                UPDATE
                                    access
                                SET
                                    lastposted = '".$prev_period."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update Last Posted";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        } 
                        
                        $msg = "Berhasil Close Period";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_unik."');</script>";         
                        die();    
                    }
                    
                }

            }
            break;
    }
}

mysql_close($con);
?>

