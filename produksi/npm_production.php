<?php
    include("header.php");
    
	$modul        = "Production";
	$file_current = "npm_production.php";
	$file_name    = "npm_production_data.php";

	$v_from_date = "01-".date("m-Y");
	$v_to_date   = date("t", parsedate($v_from_date, "-"))."-".date("m-Y");


	$jml_warehouse = 0;
	$q = "
	        SELECT
	            gudang.KdGudang,
	            gudang.Keterangan
	        FROM
	            gudang
	        WHERE
	            1
	            AND gudang.KdGudang IN('15','05')
	        ORDER BY
	            gudang.KdGudang DESC
	";
	$qry = mysql_query($q);
	while($row = mysql_fetch_array($qry))
	{
	    list($warehousecode, $warehousename) = $row;
	    
	    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
	    $arr_data["warehousename"][$warehousecode] = str_replace("GUDANG", "", $warehousename);
	    
	    $jml_warehouse++;
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="description" content="Neon Admin Panel" />
	    <meta name="author" content="" />

	    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
	    <link rel="shortcut icon" href="public/images/Logosg.png" >
        <link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="../assets/css/NotoSans.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/neon-core.css">
        <link rel="stylesheet" href="../assets/css/neon-theme.css">
        <link rel="stylesheet" href="../assets/css/neon-forms.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/skins/black.css">
        <link rel="stylesheet" href="public/css/style.css">
        <link rel="stylesheet" href="../assets/css/my.css">

        <script src="../assets/js/jquery-1.11.0.min.js"></script>

	    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

<script>

function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
{
    var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    //alert('aaaa');

    newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth + 
        ',height=' + windowHeight + 
        ',left=' + centerWidth + 
        ',top=' + centerHeight
        );

    newWindow.focus();
    return newWindow.name;
}

function num_format(obj) {
    obj.value = new NumberFormat(obj.value).toFormatted();
}

function pop_up_qc_reject_remarks(productionid)
{
    try{
        v_qty = reform(document.getElementById("v_qty_"+productionid).value);   
        v_qty_qc = reform(document.getElementById("v_qty_qc_"+productionid).value);   
        v_qc_reject = reform(document.getElementById("v_qc_reject_"+productionid).value);   
        
        var url;
        
        url = '?v_productionid='+productionid;
        url += '&v_qty='+v_qty;
        url += '&v_qty_qc='+v_qty_qc;                                
        url += '&v_qc_reject='+v_qc_reject;
        
        if(v_qc_reject*1==0)
        {
            alert("Qty QC Reject harus diisi");
        }
        else
        {
            windowOpener(300, 400, 'QC Reject', 'npm_production_qc_reject.php'+url, 'QC Reject');    
        }
    } 
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description AJAX : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    } 
}

function confirm_delete_transfer(sid, transfer_date, batchnumber)
{
    var r = confirm("Anda yakin ingin Hapus Transfer "+batchnumber+" Tansfer Date "+transfer_date+" ?");
    if(r){
        CallAjax('delete_transfer', sid);
    }
}


function calculate_fg()
{
    try{
        var v_all_productionid;    
        var total;
        var total_qty;
        var total_qty_qc;
        var total_qc_reject;
        var total_all;
        
        var v_qty;
        var v_qty_qc;
        var v_qc_reject;
        var arr_data_productionid;

        v_all_productionid = document.getElementById("v_all_productionid").value;    

        total_qty = 0;
        total_qty_qc = 0;
        total_qc_reject = 0;
        total_all = 0;
        
        arr_data_productionid = v_all_productionid.split(",");
        
        //alert(arr_data_productionid.length);
        for(i=0;i<((arr_data_productionid.length*1)-1);i++)
        {
            //alert(arr_data_productionid[i]);
            v_qty = reform(document.getElementById("v_qty_"+arr_data_productionid[i]).value)*1;
            v_qty_qc = reform(document.getElementById("v_qty_qc_"+arr_data_productionid[i]).value)*1;
            v_qc_reject = reform(document.getElementById("v_qc_reject_"+arr_data_productionid[i]).value)*1;

            
            //alert(v_qty);
            //alert(v_qty_qc);
            //alert(v_qc_reject);
            
            total = 0;
            total = (v_qty*1) + (v_qty_qc*1) + (v_qc_reject*1);
            
            document.getElementById("td_total_"+arr_data_productionid[i]).innerHTML = format(total); 
            
            total_qty += (v_qty*1);
            total_qty_qc += (v_qty_qc*1);
            total_qc_reject += (v_qc_reject*1);
            total_all += (total*1);
            
            //alert("beres");
        } 

        document.getElementById("td_total_qty").innerHTML = format(total_qty); 
        document.getElementById("td_total_qty_qc").innerHTML = format(total_qty_qc); 
        document.getElementById("td_total_qc_reject").innerHTML = format(total_qc_reject); 
        document.getElementById("td_total_all").innerHTML = format(total_all); 
    } 
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description AJAX : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }      
}

function calculate_wip()
{
    try{
        var v_all_batchnumber;    
        var total_mixing;    
        
        v_all_batchnumber = document.getElementById("v_all_batchnumber").value;    
       
        total_mixing = 0;
        arr_data_batchnumber = v_all_batchnumber.split(",");
        for(i=0;i<((arr_data_batchnumber.length*1)-1);i++)
        {
            v_qty_mixing = reform(document.getElementById("v_qty_mixing_"+arr_data_batchnumber[i]).value);
            
            total_mixing += (v_qty_mixing*1);
        } 
        
        document.getElementById("td_total_mixing").innerHTML = format4(total_mixing);
       
    } 
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description AJAX : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }      
}

function calculate_prod_qty()
{
    try{ 
        var batch_qty;
        
        v_prd_qty_pcs = reform(document.getElementById("v_prd_qty_pcs").value)*1;
        v_packagingsize = reform(document.getElementById("v_packagingsize").value)*1;
        
        //alert(v_prd_qty_pcs);
        //alert(v_packagingsize);
        
        batch_qty = (v_prd_qty_pcs/v_packagingsize);
        
        document.getElementById("td_batch_qty").innerHTML = format4(batch_qty); 
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description AJAX : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }     
}

function calculate()
{
    try{
        var v_all_matreq; 
        var v_all_inventory;
        var arr_data_matreq;
        var arr_data_inventory;
        var total_prod_batch;
        var v_qty_kg_formula;
        var total_wip_kg;
        var qty_baku;
        var total_qty_baku;
        
        qty_baku = 0;
        total_wip_kg = 0; 
        
        v_all_matreq = document.getElementById("v_all_matreq").value;    
        v_all_inventory = document.getElementById("v_all_inventory").value;    
        v_qty_kg_formula = reform(document.getElementById("v_qty_kg_formula").value);    
        
        total_prod_batch = 0;
        arr_data_matreq = v_all_matreq.split(",");
        for(i=0;i<((arr_data_matreq.length*1)-1);i++)
        {
            v_prod_batch = reform(document.getElementById("v_prod_batch_"+arr_data_matreq[i]).value);
            
            total_prod_batch += (v_prod_batch*1);
        }
        
        document.getElementById("form_prod_batch").innerHTML = format4(total_prod_batch); 
        
        total_wip_kg = v_qty_kg_formula * total_prod_batch;
        document.getElementById("form_total_wip_kg").innerHTML = format4(total_wip_kg); 
        
        document.getElementById("prod_batch").value = total_prod_batch;
        document.getElementById("total_wip_kg").value = total_wip_kg;
        
        total_qty_baku = 0;
        arr_data_inventory = v_all_inventory.split(",");
        for(i=0;i<((arr_data_inventory.length*1)-1);i++)
        {
            v_qty_kg = reform(document.getElementById("v_qty_kg_"+arr_data_inventory[i]).value);
            
            qty_baku = (total_prod_batch * v_qty_kg);
            document.getElementById("v_qty_baku_"+arr_data_inventory[i]).value = format6(qty_baku);
            document.getElementById("td_qty_baku_"+arr_data_inventory[i]).innerHTML = format6(qty_baku);
            
            total_qty_baku += (qty_baku*1);
        }
        
        document.getElementById("td_total_mixing").innerHTML = format4(total_qty_baku); 
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description AJAX : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }         
}
  
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

var xmlhttp = createRequestObject();

function CallAjax(tipenya,param1,param2,param3,param4,param5)
{
    try{    
        if (!tipenya) return false;
        //document.getElementById("loading").style.display='block';
        
        if (param1 == undefined) param1 = '';
        if (param2 == undefined) param2 = '';
        if (param3 == undefined) param3 = '';
        if (param4 == undefined) param4 = '';
        if (param5 == undefined) param5 = '';
        
        var variabel;
        variabel = "";
                             
        if(tipenya=='search')
        {  
            search_by = document.getElementById("search_by").value;
            search_status = document.getElementById("search_status").value;
            v_type_date = document.getElementById("v_type_date").value;
            v_from_date = document.getElementById("v_from_date").value;
            v_to_date = document.getElementById("v_to_date").value;            
            search_warehouse = document.getElementById("search_warehouse").value;
            search_batchnumber = document.getElementById("search_batchnumber").value;
            search_formula = document.getElementById("search_formula").value;
            search_keyword = document.getElementById("search_keyword").value;

            variabel += "&search_by="+search_by;
            variabel += "&search_status="+search_status;
            variabel += "&v_type_date="+v_type_date;
            variabel += "&v_from_date="+v_from_date;
            variabel += "&v_to_date="+v_to_date; 
            variabel += "&search_warehouse="+search_warehouse;
            variabel += "&search_batchnumber="+search_batchnumber;
            variabel += "&search_formula="+search_formula;
            variabel += "&search_keyword="+search_keyword;
            
            variabel += "&v_batchnumber_curr="+param1;
            
            if(search_by=="batchnumber" && search_batchnumber=="")
            {
                document.getElementById("search_batchnumber").focus();
                alert('Batchnumber harus diisi');
                return false;
            }
            if(search_by=="formula" && search_formula=="")
            {
                document.getElementById("search_formula").focus();
                alert('Formula harus diisi');
                return false;
            }
            else if(search_by=="keyword" && search_keyword=="")
            {
                document.getElementById("search_keyword").focus();
                alert('Keyword harus diisi');
                return false;
            }
            
            //alert(variabel);
            
            document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            document.getElementById("col-header").innerHTML = "";
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-search").innerHTML     = xmlhttp.responseText;
                    
                    if(param1)
                    {
                        //alert("echo disiini");    
                        if(param2)
                        {
                            //alert("param 2");    
                            CallAjax(param2,param1);
                            
                        }
                        else
                        {
                            //alert("param 1");    
                            CallAjax('mixing',param1);
                        }
                    }
                }

                return false;
            }
            xmlhttp.send(null);
        } 
        else if(tipenya=='add_data')
        {  
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='add_data_wip')
        {  
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='mixing')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_warehouse')
        {  
            variabel += "&v_warehousecode="+param1;
            
            //alert(variabel);
            document.getElementById("form_formula").innerHTML = 'Loading...';
            document.getElementById("form_matreq").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            document.getElementById("v_batch_1").value = 'Loading...';
            document.getElementById("form_detail").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    arr_data = xmlhttp.responseText.split("||");
                    document.getElementById("form_prod_batch").innerHTML = '';
                    document.getElementById("form_total_wip_kg").innerHTML = '';
                    document.getElementById("prod_batch").value = 0;
                    document.getElementById("total_wip_kg").value = 0;
                        
                    document.getElementById("form_formula").innerHTML = arr_data[0];
                    document.getElementById("form_matreq").innerHTML = arr_data[1];
                    document.getElementById("v_batch_1").value = arr_data[2];
                    document.getElementById("form_detail").innerHTML = arr_data[3];
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_warehouse_wip')
        {  
            variabel += "&v_warehousecode="+param1;
            
            //alert(variabel);
            document.getElementById("form_formula").innerHTML = 'Loading...';
            document.getElementById("v_batch_1").value = 'Loading...';
            document.getElementById("form_detail").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    arr_data = xmlhttp.responseText.split("||");
                        
                    document.getElementById("form_formula").innerHTML = arr_data[0];
                    document.getElementById("v_batch_1").value = arr_data[1];
                    document.getElementById("form_detail").innerHTML = arr_data[2];
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_formula')
        {  
            v_warehousecode = document.getElementById("v_warehousecode").value;
            
            if(v_warehousecode=="")
            {
                alert("Warehouse harus dipilih..."); 
            }
            else
            {
                variabel += "&v_formulanumber="+param1;
                variabel += "&v_warehousecode="+v_warehousecode;
                
                //alert(variabel);
                document.getElementById("form_matreq").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                document.getElementById("v_batch_1").value = 'Loading...';
                document.getElementById("form_detail").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        arr_data = xmlhttp.responseText.split("||");
                        document.getElementById("form_prod_batch").innerHTML = '';
                        document.getElementById("form_total_wip_kg").innerHTML = '';
                        document.getElementById("prod_batch").value = 0;
                        document.getElementById("total_wip_kg").value = 0;
                        
                        document.getElementById("form_matreq").innerHTML = arr_data[0];
                        document.getElementById("v_batch_1").value = arr_data[1];
                        document.getElementById("form_detail").innerHTML = arr_data[2];
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
        }
        else if(tipenya=='ajax_formula_wip')
        {  
            v_warehousecode = document.getElementById("v_warehousecode").value;
            
            if(v_warehousecode=="")
            {
                alert("Warehouse harus dipilih...");
            }
            else
            {
                variabel += "&v_formulanumber="+param1;
                variabel += "&v_warehousecode="+v_warehousecode;
                
                //alert(variabel);
                document.getElementById("v_batch_1").value = 'Loading...';
                document.getElementById("form_detail").innerHTML = '<img src=\'images/img-ajax.gif\'>';
                
                xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function() 
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {    
                        arr_data = xmlhttp.responseText.split("||");
                        
                        document.getElementById("v_batch_1").value = arr_data[0];
                        document.getElementById("form_detail").innerHTML = arr_data[1];
                    }

                    return false;
                }
                xmlhttp.send(null);
            }
        }
        else if(tipenya=='ajax_formula_packaging')
        {  
            var v_warehousecode;
            
            v_warehousecode = document.getElementById("v_warehousecode").value;
            
            variabel += "&v_formulanumber="+param1;
            variabel += "&v_warehousecode="+v_warehousecode;
            
            //alert(variabel);
            document.getElementById("v_prd_qty_pcs").value = '';
            document.getElementById("td_batch_qty").innerHTML = '';
            
            document.getElementById("td_detail_matreq").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            document.getElementById("td_detail_inventory").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    arr_data = xmlhttp.responseText.split("||");
                    
                    document.getElementById("td_detail_matreq").innerHTML = arr_data[0];
                    document.getElementById("td_detail_inventory").innerHTML = arr_data[1];
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='mixing_wip')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        
        else if(tipenya=='adjustment')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='packaging')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='finish_good')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='sisa_wip')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='delete_packaging')
        {  
            variabel += "&v_productionid="+param1;
            
            var batchnumber;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    //document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                    
                    batchnumber = xmlhttp.responseText;
                    //alert(xmlhttp.responseText);
                    
                    CallAjax('search', batchnumber.trim(), 'packaging');
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='edit_packaging')
        {  
            variabel += "&v_productionid="+param1;
            
            //alert(variabel);
            document.getElementById("table_edit").innerHTML = '<div align=\'center\'><img src=\'images/img-ajax.gif\'></div>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("table_edit").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='add_packaging')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("table_edit").innerHTML = '<div align=\'center\'><img src=\'images/img-ajax.gif\'></div>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("table_edit").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='search_keyword_formula_packaging')
        {  
            variabel += "&v_keyword="+param1;
            
            //alert(variabel);
            document.getElementById("form_formula").innerHTML = 'Loading...';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("form_formula").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='search_keyword_formula_mixing')
        {  
            variabel += "&v_keyword="+param1;
            
            //alert(variabel);
            document.getElementById("form_formula").innerHTML = 'Loading...';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("form_formula").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        
        else if(tipenya=='search_keyword_formula_transfer')
        {  
            variabel += "&v_keyword="+param1;
            
            //alert(variabel);
            document.getElementById("form_formula").innerHTML = 'Loading...';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("form_formula").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='search_formula_mix_wip')
        {  
            variabel += "&v_keyword="+param1;
            
            //alert(variabel);
            document.getElementById("form_formula").innerHTML = 'Loading...';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("form_formula").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='delete_packaging')
        {  
            variabel += "&v_productionid="+param1;
            
            //alert(variabel);
            document.getElementById("table_edit").innerHTML = '<div align=\'center\'><img src=\'images/img-ajax.gif\'></div>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("table_edit").innerHTML     = xmlhttp.responseText;
                    
                    CallAjax('search',xmlhttp.responseText,'packaging');
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='transfer_in')
        {  
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                    document.getElementById('loading').style.display    = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='transfer_wip')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                    document.getElementById('loading').style.display    = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_formula_transfer')
        {  
            variabel += "&v_formulanumber="+param1;
            
            //alert(variabel);
            //document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("v_batch_1").value     = xmlhttp.responseText;
                    document.getElementById("v_batch_2").focus();
                    document.getElementById('loading').style.display    = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        
        else if(tipenya=='edit_transfer')
        {  
            variabel += "&v_sid="+param1;
            
            //alert(variabel);
            document.getElementById("table_edit_transfer").innerHTML = '<div align=\'center\'><img src=\'images/img-ajax.gif\'></div>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("table_edit_transfer").innerHTML     = xmlhttp.responseText;
                    document.getElementById('loading').style.display    = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='add_transfer')
        {  
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            document.getElementById("table_edit_transfer").innerHTML = '<div align=\'center\'><img src=\'images/img-ajax.gif\'></div>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("table_edit_transfer").innerHTML     = xmlhttp.responseText;
                    document.getElementById('loading').style.display    = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='delete_transfer')
        {  
            variabel += "&v_sid="+param1;
            
            //alert(variabel);
            document.getElementById("table_edit_transfer").innerHTML = '<div align=\'center\'><img src=\'images/img-ajax.gif\'></div>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    //document.getElementById("table_edit_transfer").innerHTML     = xmlhttp.responseText;
                    alert(xmlhttp.responseText);
                    CallAjax('search',xmlhttp.responseText,'transfer_wip');
                    document.getElementById('loading').style.display    = 'none';
                    
                }

                return false;
            }
            xmlhttp.send(null);
        }

    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description AJAX : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }  
}

function confirm_print_transfer(sid)
{
    var r = confirm("Anda yakin ingin Print Transfer ?");
    if(r){
       windowOpener(300, 400, 'Print Transfer', 'npm_production_print_transfer.php?sid='+sid, 'Print Transfer'); 
    }
}


function confirm_delete(batchnumber)
{
    var r = confirm("Anda yakin ingin Hapus "+batchnumber+" ?");
    if(r){
        document.getElementById("v_del").value = '1';
        document.getElementById("theform").submit();
    }
}

function confirm_delete_wip(batchnumber)
{
    var r = confirm("Anda yakin ingin Hapus "+batchnumber+" ?");
    if(r){
        document.getElementById("v_del").value = '1';
        document.getElementById("theform").submit();
    }
}

function confirm_delete_packaging(productionid, productiondate, batchnumber)
{
    var r = confirm("Anda yakin ingin Hapus Packaging "+batchnumber+" Pack Date "+productiondate+" ?");
    if(r){
        CallAjax('delete_packaging', productionid);
    }
}



function confirm_undelete(user_form)
{
    var r = confirm("Anda yakin ingin UnVOID "+user_form+" ?");
    if(r){
        document.getElementById("v_undel").value = '1';
        document.getElementById("theform").submit();
    }
}

function CheckAll(param, target){
    var field = document.getElementsByName(target);
    var chkall = document.getElementById(param);
    if (chkall.checked == true){
        for (i = 0; i < field.length; i++)
            field[i].checked = true ;
    }else{
        for (i = 0; i < field.length; i++)
            field[i].checked = false ;
    }        
}


function reform(val)
{    
     var a = val.split(",");
    var b = a.join("");
    //alert(b);
    return b;
}

function format(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(0);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}

function format4(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(4);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}

function format6(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(6);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}
 
function addSeparatorsNF(nStr, inD, outD, sep)
{
 nStr += '';
 var dpos = nStr.indexOf(inD);
 var nStrEnd = '';
 if (dpos != -1) {
  nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
  nStr = nStr.substring(0, dpos);
 }
 var rgx = /(\d+)(\d{3})/;
 while (rgx.test(nStr)) {
  nStr = nStr.replace(rgx, '$1' + sep + '$2');
 }
 return nStr + nStrEnd;
}

function toFormat(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format(document.getElementById(id).value);
}

function cekMR(obj)
{
	
	objek = obj.id;
	id = objek.substr(12,objek.length-12);
	var MR =  document.getElementById("no_mr").value;
	var PCode=  document.getElementById("v_pcode_"+id).value;
	var Qty=  document.getElementById("v_qty_kemas_"+id).value;

	if(MR=="0"){
		alert("Isi Terlebih Dahulu salah satu Material Request.");
		document.getElementById("v_qty_kemas_"+id).value="";
        return false;
	}else{
	 
      
    //apakah Qty melebih yang ada di Qty MR tersebut
    
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax=cekMr&v_mr='+MR+'&v_pcode='+PCode+'&v_qty_asal='+Qty, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    if(xmlhttp.responseText=='1'){
						alert("Quantity Packging Lebih Besar dari pada Quantity Material Request");
						document.getElementById("v_qty_kemas_"+id).value="";
						document.getElementById("v_qty_kemas_"+id).focus();
					}
                    
                }

                return false;
            }
            xmlhttp.send(null);
    }
}

function ambilMatreq(id)
{
    //alert(id);
    document.getElementById("no_mr").value=id;
}

function toFormat4(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format4(document.getElementById(id).value);
}
function toFormat6(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format6(document.getElementById(id).value);
}
function isanumber(id)
{
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        document.getElementById(id).value=0;
    }
    else
    {
        document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
    }
}
  

function start_page()
{   
    //document.getElementById("search_user_form").focus();
}

function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
            
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}

function change_search_by(nilai)
{
    if(nilai=="data")
    {
        document.getElementById('td_status').style.display              = '';    
        
        if(document.getElementById('search_status').value=='Open')
        {
            document.getElementById('td_date').style.display                = 'none';        
        }
        else if(document.getElementById('search_status').value=='Close' || document.getElementById('search_status').value=='All')
        {
            document.getElementById('td_date').style.display                = '';        
        }
        
        document.getElementById('td_warehouse').style.display           = '';    
        document.getElementById('td_batchnumber').style.display         = 'none';    
        document.getElementById('td_formula').style.display             = 'none';    
        document.getElementById('td_keyword').style.display             = 'none';    
    }
    else if(nilai=="batchnumber")
    {
        document.getElementById('td_status').style.display              = 'none';    
        document.getElementById('td_date').style.display                = '';    
        document.getElementById('td_warehouse').style.display           = 'none';    
        document.getElementById('td_batchnumber').style.display         = '';    
        document.getElementById('td_formula').style.display             = 'none';    
        document.getElementById('td_keyword').style.display             = 'none';    
        
        document.getElementById('search_batchnumber').focus();        
    }
    else if(nilai=="formula")
    {
        document.getElementById('td_status').style.display              = 'none';    
        document.getElementById('td_date').style.display                = '';    
        document.getElementById('td_warehouse').style.display           = 'none'; 
        document.getElementById('td_batchnumber').style.display         = 'none';    
        document.getElementById('td_formula').style.display             = '';    
        document.getElementById('td_keyword').style.display             = 'none';    
        
        document.getElementById('search_formula').focus();        
    }
    else if(nilai=="keyword")
    {
        document.getElementById('td_status').style.display              = 'none';    
        document.getElementById('td_date').style.display                = '';    
        document.getElementById('td_warehouse').style.display           = 'none'; 
        document.getElementById('td_batchnumber').style.display         = 'none';    
        document.getElementById('td_formula').style.display             = 'none';    
        document.getElementById('td_keyword').style.display             = '';    
        
        document.getElementById('search_keyword').focus();        
    }
    
} 

function change_type_date(nilai)
{
    if(nilai=="today")
    {
        document.getElementById('td_range_date').style.display         = 'none';    
    }
    else if(nilai=="7_day_before")
    {
        document.getElementById('td_range_date').style.display         = 'none';    
    }
    else if(nilai=="range_date")
    {
        document.getElementById('td_range_date').style.display         = '';    
    }
}

function change_status(nilai)
{
    if(nilai=="Open")
    {
        document.getElementById('td_date').style.display         = 'none';    
    }
    else if(nilai=="All")
    {
        document.getElementById('td_date').style.display         = '';    
    }
    else if(nilai=="Close")
    {
        document.getElementById('td_date').style.display         = '';    
    }
}

function targetBlank (url) {
    blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
}

function add_rework()
{                    
    targetBlank('npm_production_rework.php');
}
  

</script> 

<style>
    .title_table{
        background: #009490; color: white; font-weight: bold;
    }
</style> 

</head>
<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
	
			<div class="col-md-10" style="color: black;">
				Search By&nbsp;
		        <select name="search_by" id="search_by" class="form-control-new" onchange="change_search_by(this.value)" style="width:100px;">
		            <option value="data">Data</option>
		            <option value="batchnumber">BatchNumber</option>
		            <option value="formula">Formula</option>
		            <option value="keyword">Keyword</option>
		        </select>
        		&nbsp;
		        <span id="td_status" style="display:;">
		            Status&nbsp;
		            <select name="search_status" id="search_status" class="form-control-new" onchange="change_status(this.value)">
		                <option value="Open">Open</option>
		                <option value="All">All</option>
		                <option value="Close">Close</option>
		            </select>
		            &nbsp;  
		        </span>
   				
		        <span id="td_date" style="display:none;">
		        Type Date&nbsp;
		        <select name="v_type_date" id="v_type_date" class="form-control-new" onchange="change_type_date(this.value)">
		            <option value="today">Today</option>
		            <option value="7_day_before">7 day Before</option>
		            <option value="range_date">Range Date</option>
		        </select>
        		&nbsp;
        		<span id="td_range_date" style="display:none">
        			<input type="text" class="form-control-new datepicker" name="v_from_date" id="v_from_date" size="10" maxlength="10" value="<?php echo $v_from_date; ?>">
			        &nbsp;
		            up to
		            &nbsp;
		            <input type="text" class="form-control-new datepicker" name="v_to_date" id="v_to_date" size="10" maxlength="10" value="<?php echo $v_to_date; ?>">
		            &nbsp;
				</span>
        		</span>
        		
		        <span id="td_warehouse" style="display:">
		        Gudang&nbsp;
		        <select name="search_warehouse" id="search_warehouse" class="form-control-new" style="width: 170px;">
		            
		            <?php 
		                if($jml_warehouse==count($arr_data["warehouseadmin"]))
		                {
		            ?>
		            <option value="All">All</option>
		            <?php 
		                }
		            ?>
		            
		            <?php 
		                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
		                {
		                    
		                        $warehousename = $arr_data["warehousename"][$warehousecode];
		    		?>
		                        <option value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
		    		<?php                
		                       
		                }
		            ?>    
		        </select>
		        </span>
        
		        <span id="td_batchnumber" style="display:none;">
		            <input type="text" class="form-control-new" size="30" maxlength="30" name="search_batchnumber" id="search_batchnumber" placeholder="Ketik Disini">
		        </span>
		        
		        <span id="td_formula" style="display:none;">
		            <input type="text" class="form-control-new" size="30" maxlength="30" name="search_formula" id="search_formula"  placeholder="Ketik Disini">
		        </span>
		        
		        <span id="td_keyword" style="display:none;">
		            <input type="text" class="form-control-new" size="30" maxlength="30" name="search_keyword" id="search_keyword"  placeholder="Ketik Disini">
		        </span>
	    	</div>
	    
			<div class="col-md-2" align="right">
				<div class="btn-group">
					<button type="button" class="btn btn-info btn-icon btn-sm icon-left dropdown-toggle" data-toggle="dropdown">
						<i class="entypo-menu"></i>Action
					</button>
					
					<ul class="dropdown-menu" role="menu" style="right: 0px; left: auto; text-align: left;">
						<li onClick="CallAjax('search'),show_loading_bar(100)"><a href="javascript:void(0)"><i class="entypo-search"></i>&nbsp;Search</a></li>
						<li onClick="CallAjax('add_data'),show_loading_bar(100)"><a href="javascript:void(0)"><i class="entypo-plus"></i>&nbsp;Add Mixing</a></li>
						<li onClick="CallAjax('add_data_wip'),show_loading_bar(100)"><a href="javascript:void(0)"><i class="entypo-plus"></i>&nbsp;Add Mix WIP</a></li>
                        <li onClick="CallAjax('transfer_in'),show_loading_bar(100)"><a href="javascript:void(0)"><i class="entypo-plus"></i>&nbsp;Transfer IN</a></li>
						<!--<li onClick="add_rework();"><a href="javascript:void(0)"><i class="entypo-plus"></i>&nbsp;Add Rework</a></li>-->
					</ul>
				</div>
	       	</div>
	       	
    	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>

<?php include("footer.php"); ?>