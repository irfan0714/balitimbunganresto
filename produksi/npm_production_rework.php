<?php 
    include("header.php");
    
	$modul        = "Production Rework";
	$file_name    = "npm_production_rework.php"; 
 
	$cut_point_date_rework = "2014-09-25";

function update_bincard($v_productionid)
{
    $q = "DELETE FROM bincard WHERE bincard.referenceno = '".$v_productionid."'";
    if(!mysql_query($q))      
    {
        $msg = "Gagal menghapus bincard";
        echo "<script>alert('".$msg."');</script>";     
        die();
    }
    
    $q = "
            INSERT INTO bincard 
            (
                inventorycode, 
                carddate, 
                warehousecode, 
                referenceno, 
                module, 
                qin, 
                qout, 
                VALUE, 
                adduser, 
                ADDDATE, 
                edituser, 
                editdate
            )
            SELECT
                production_rework_lebih.inventorycode,
                production.productiondate,
                'WH002' AS warehousecode,
                production_rework_lebih.productionid AS referenceno,
                'RW' AS module, 
                production_rework_lebih.qty,
                '0' AS qout,
                '0' AS val,
                '".GetUserLogin()."' AS adduser,
                NOW() AS adddate,
                '".GetUserLogin()."' AS edituser,
                NOW() AS editdate
            FROM
                production
                INNER JOIN production_rework_lebih ON
                    production_rework_lebih.productionid = production.productionid
                    AND production.productionid = '".$v_productionid."'
            WHERE
                1
    ";
    if(!mysql_query($q))      
    {
        $msg = "Gagal insert bincard";
        echo "<script>alert('".$msg."');</script>";     
        die();
    }
    
}

if(!isset($_REQUEST["v_productionid"])){ $v_productionid = isset($_REQUEST["v_productionid"]); } else { $v_productionid = $_REQUEST["v_productionid"]; }
if(!isset($_GET["ajax"])){ $ajax = isset($_GET["ajax"]); } else { $ajax = $_GET["ajax"]; }
if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }
if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }

unset($arr_data);

// ambil data yang ada rework aja, baca ke 13
if(!$v_productionid) 
{
    $q = "
            SELECT
                admin_cabang.Server,
                admin_cabang.User,
                admin_cabang.Password,
                admin_cabang.Database
            FROM
                admin_cabang
            WHERE
                1
                AND admin_cabang.KdCabang = '97'
                AND admin_cabang.Database = 's3distgudang'
            LIMIT
                0,1
    ";
    $qry_db = mysql_query($q);
    $row_db = mysql_fetch_array($qry_db);

    $conn = mysql_connect($row_db["Server"], $row_db["User"], $row_db["Password"]); 
    
    $where_pcode = "";
    $q = "
            SELECT
                uni.PCode
            FROM
            (
                SELECT
                    ".$row_db["Database"].".trans_returdetail.PCode
                FROM
                    ".$row_db["Database"].".trans_returheader
                    INNER JOIN ".$row_db["Database"].".trans_returdetail ON
                        ".$row_db["Database"].".trans_returheader.NoDokumen = ".$row_db["Database"].".trans_returdetail.NoDokumen
                        AND ".$row_db["Database"].".trans_returheader.FlagRework IN('Y', 'YY')
                WHERE
                    1
                GROUP BY
                    ".$row_db["Database"].".trans_returdetail.PCode
                
                UNION ALL
                
                SELECT
                    '01-00008' AS PCode
            )AS uni
            
            ORDER BY
                uni.PCode ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($PCode) = $row;
        
        if($where_pcode=="")
        {
            $where_pcode .= " AND ( inventorymaster.inventorycode = '".$PCode."' ";
        }
        else
        {
            $where_pcode .= " OR inventorymaster.inventorycode = '".$PCode."' ";
        }
    }
    if($where_pcode)
    {
        $where_pcode .= ")";
    }
    
    $conn = mysql_connect($DBConnection->Host(), $DBConnection->User(), $DBConnection->Password());
}

if($ajax)
{
    if($ajax=="ajax_search_fg")
    {
        $v_keyword_fg = $_GET["v_keyword_fg"];
        
        $arr_keyword[0] = "inventorymaster.inventorycode";
        $arr_keyword[1] = "inventorymaster.inventoryname";
        
        $where_keyword = search_keyword($v_keyword_fg, $arr_keyword);
        
        $q = "
                SELECT
                    inventorymaster.inventorycode,
                    inventorymaster.inventoryname
                FROM
                    inventorymaster
                WHERE
                    1
                    AND inventorymaster.stocktypeid = '3'
                    AND inventorymaster.imported = '0'
                    ".$where_keyword."
                    ".$where_pcode."
                ORDER BY
                    inventorymaster.inventoryname ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname) = $row;
            
            $arr_data["list_fg"][$inventorycode] = $inventorycode;
            
            $arr_data["inventoryname"][$inventorycode] = $inventorycode." :: ".$inventoryname;
        }
        
        ?>
        
        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:250px;" onchange="CallAjax('ajax_choose_fg', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_fg"] as $inventorycode => $val)
                {
                    $inventoryname = $arr_data["inventoryname"][$inventorycode];
                    ?>
                    
                    <option value="<?php echo $inventorycode; ?>"><?php echo $inventoryname; ?></option>
                    <?php
                }
            ?>
        </select>
        <?php
    }
    else if($ajax=="ajax_choose_fg")
    {
        $v_inventorycode = $_GET["v_inventorycode"];    
        
        $q = "
                SELECT
                    inventorymaster.inventoryprefix
                FROM
                    inventorymaster
                WHERE
                    1
                    AND inventorymaster.inventorycode = '".$v_inventorycode."' 
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        
        echo $row["inventoryprefix"];
        
    }
    exit();
}

if($v_del==1)
{
    if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }    
    
    $q = "
            SELECT 
                production.productiondate
            FROM
                production
            WHERE
                1
                AND production.productionid = '".$v_productionid."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $acc_locked = call_acc_locked("production", $row["productiondate"], $row["productiondate"]);
    
    if($acc_locked["locked"][$row["productiondate"]]*1==1)
    {
        $msg = "Data tanggal ".$v_production_date." sudah di Acc Locked";
        echo "<script>alert('".$msg."');</script>"; 
        die();    
    }
    else
    {
        // cek NoDokumen slain produksi ini
        // kalo gak ada lagi, update FalgRework Ke Y
        $q = "
                SELECT
                    production_rework_return.returnno
                FROM
                    production_rework_return
                WHERE
                    production_rework_return.productionid = '".$v_productionid."'
        ";    
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($returnno) = $row;
            
            $q = "
                    SELECT
                        production_rework_return.returnno
                    FROM
                        production_rework_return
                    WHERE
                        production_rework_return.returnno = '".$returnno."'
                        AND production_rework_return.productionid != '".$v_productionid."'
                    LIMIT
                        0,1
            "; 
            $qry_cek = mysql_query($q);
            $row_cek = mysql_fetch_array($qry_cek);
            
            if($row_cek["returnno"]=="")
            {
                $q = "
                        SELECT
                            admin_cabang.Server,
                            admin_cabang.User,
                            admin_cabang.Password,
                            admin_cabang.Database
                        FROM
                            admin_cabang
                        WHERE
                            1
                            AND admin_cabang.KdCabang = '97'
                            AND admin_cabang.Database = 's3distgudang'
                        LIMIT
                            0,1
                ";
                $qry_db = mysql_query($q);
                $row_db = mysql_fetch_array($qry_db);
                
                $conn = mysql_connect($row_db["Server"], $row_db["User"], $row_db["Password"]);
                
                $q = "
                        UPDATE
                            ".$row_db["Database"].".trans_returheader
                        SET
                            FlagRework = 'Y'
                        WHERE
                            1
                            AND ".$row_db["Database"].".trans_returheader.NoDokumen = '".$returnno."'
                ";
                if(!mysql_query($q))      
                {
                    $msg = "Gagal update Y";
                    echo "<script>alert('".$msg."');</script>";     
                    die();
                }
                
                $conn = mysql_connect($DBConnection->Host(), $DBConnection->User(), $DBConnection->Password()); 
            }
        }
    
        $q = "DELETE FROM production_rework_return WHERE production_rework_return.productionid = '".$v_productionid."'";    
        if(!mysql_query($q))      
        {
            $msg = "Gagal menghapus production_rework_return";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM production_rework_lebih WHERE production_rework_lebih.productionid = '".$v_productionid."'";    
        if(!mysql_query($q))      
        {
            $msg = "Gagal menghapus production_rework_lebih";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM production_rework_usage WHERE production_rework_usage.productionid = '".$v_productionid."'";    
        if(!mysql_query($q))      
        {
            $msg = "Gagal menghapus production_rework_usage";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM production WHERE production.productionid = '".$v_productionid."'";    
        if(!mysql_query($q))      
        {
            $msg = "Gagal menghapus";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        update_bincard($v_productionid);
        echo "<script>parent.reload_page_list();</script>";         
        die();
    }    
}

if($btn_save)
{
    if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }
    
    if(!isset($_POST["v_rework_type"])){ $v_rework_type = isset($_POST["v_rework_type"]); } else { $v_rework_type = $_POST["v_rework_type"]; }
    if(!isset($_POST["v_production_date"])){ $v_production_date = isset($_POST["v_production_date"]); } else { $v_production_date = $_POST["v_production_date"]; }
    if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }
    if(!isset($_POST["v_batchnumber_1"])){ $v_batchnumber_1 = isset($_POST["v_batchnumber_1"]); } else { $v_batchnumber_1 = $_POST["v_batchnumber_1"]; }
    if(!isset($_POST["v_batchnumber_2"])){ $v_batchnumber_2 = isset($_POST["v_batchnumber_2"]); } else { $v_batchnumber_2 = $_POST["v_batchnumber_2"]; }
    if(!isset($_POST["v_remarks"])){ $v_remarks = isset($_POST["v_remarks"]); } else { $v_remarks = $_POST["v_remarks"]; }
    
    $v_batchnumber_1 = save_char($v_batchnumber_1);
    $v_batchnumber_2 = save_char($v_batchnumber_2);
    $v_remarks = save_char($v_remarks);
    
    $acc_locked = call_acc_locked("production", format_save_date($v_production_date), format_save_date($v_production_date));
    
    if($acc_locked["locked"][format_save_date($v_production_date)]*1==1)
    {
        $msg = "Data tanggal ".$v_production_date." sudah di Acc Locked";
        echo "<script>alert('".$msg."');</script>"; 
        die();    
    }
    else
    {
        if($v_inventorycode=="")
        {
            $msg = "Finish Goods harus diisi";
            echo "<script>alert('".$msg."');</script>"; 
            die();
        }
        else if($v_batchnumber_2=="")
        {
            $msg = "Batchnumber harus diisi";
            echo "<script>alert('".$msg."');</script>"; 
            die();
        }
        
        // validasi
        if($v_productionid=="")
        {
            $q = "
                    SELECT
                        production.batchnumber
                    FROM
                        production
                    WHERE
                        1
                        AND production.batchnumber = '".$v_batchnumber_1.$v_batchnumber_2."'
                    LIMIT
                        0,1
            ";
            $qry_cek_batchnumber = mysql_query($q);
            $row_cek_batchnumber = mysql_fetch_array($qry_cek_batchnumber);
            
            if($row_cek_batchnumber["batchnumber"])
            {
                $msg = "Batchnumber sudah ada";
                echo "<script>alert('".$msg."');</script>"; 
                die();    
            }
        }
        
        if($v_productionid=="")
        {
            $productionid = get_counter_int("vci","production","productionid", 100);
            
            $q = "
                    INSERT INTO 
                        production 
                    SET
                        productionid = '".$productionid."' ,
                        batchnumber = '".$v_batchnumber_1.$v_batchnumber_2."' , 
                        productiondate = '".format_save_date($v_production_date)."' , 
                        formulanumber = '' , 
                        mixquantity = '' , 
                        mixvalue = '' , 
                        packvalue = '' , 
                        mixdate = '".format_save_date($v_production_date)."' , 
                        packdate = '".format_save_date($v_production_date)."' , 
                        STATUS = '2' , 
                        adduser = '".GetUserLogin()."' , 
                        ADDDATE = '".date("Y-m-d")."' , 
                        edituser = '".GetUserLogin()."' , 
                        editdate = '".date("Y-m-d")."' , 
                        inventoryprefix = '".$v_batchnumber_1."' , 
                        adjusted = '' , 
                        batchquantity = '' , 
                        adjdate = '' , 
                        prodstat = '1' , 
                        formulanoref = '' , 
                        batchnoref = '' , 
                        packagingstatus = '1' , 
                        isposted = '0' , 
                        mixingfoh = '' , 
                        packagingfoh = '' , 
                        hasfinishgood = '0' , 
                        warehousecode = 'WH002' , 
                        productreworkcode = '".$v_inventorycode."' , 
                        reworkquantity = '' , 
                        reworkcogsvalue = '' , 
                        usereworkmaterial = '".GetUserLogin()."' , 
                        remarks = '".$v_remarks."',
                        rework_type = '".$v_rework_type."'
            ";
            if(!mysql_query($q))      
            {
                $msg = "Gagal menyimpan";
                echo "<script>alert('".$msg."');</script>";     
                die();
            }
            
            //header("Location: ".$file_name."?v_productionid=".$productionid);
            echo "<script>parent.reload_page('".$productionid."');</script>";         
            die();
        }
        else
        {
             $q = "
                    SELECT
                        production.batchnumber
                    FROM
                        production
                    WHERE
                        1
                        AND production.productionid != '".$v_productionid."'
                        AND production.batchnumber = '".$v_batchnumber_1.$v_batchnumber_2."'
                    LIMIT
                        0,1
             ";
             $qry_cek_batchnumber = mysql_query($q);
             $row_cek_batchnumber = mysql_fetch_array($qry_cek_batchnumber);

            if($row_cek_batchnumber["batchnumber"])
            {
                $msg = "Batchnumber sudah ada";
                echo "<script>alert('".$msg."');</script>"; 
                die();    
            }
            
            $q = "
                    UPDATE
                        production
                    SET
                        productiondate = '".format_save_date($v_production_date)."',
                        batchnumber = '".$v_batchnumber_1.$v_batchnumber_2."',
                        remarks = '".$v_remarks."'
                    WHERE
                        productionid = '".$v_productionid."'
                        
            ";
            if(!mysql_query($q))      
            {
                $msg = "Gagal update";
                echo "<script>alert('".$msg."');</script>";     
                die();
            }
            
            // return
            {
                if(!isset($_POST["no_return"])){ $no_return = isset($_POST["no_return"]); } else { $no_return = $_POST["no_return"]; }
                if(!isset($_POST["v_del_return"])){ $v_del_return = isset($_POST["v_del_return"]); } else { $v_del_return = $_POST["v_del_return"]; }
                
                foreach($no_return as $key => $val)
                {
                    if(!isset($_POST["v_returnno_".$val])){ $v_returnno = isset($_POST["v_returnno_".$val]); } else { $v_returnno = $_POST["v_returnno_".$val]; }
                    if(!isset($_POST["v_qty_rework_".$val])){ $v_qty_rework = isset($_POST["v_qty_rework_".$val]); } else { $v_qty_rework = $_POST["v_qty_rework_".$val]; }
                    if(!isset($_POST["v_qty_sisa_curr_".$val])){ $v_qty_sisa_curr = isset($_POST["v_qty_sisa_curr_".$val]); } else { $v_qty_sisa_curr = $_POST["v_qty_sisa_curr_".$val]; }
                    
                    $v_qty_rework = save_int($v_qty_rework);
                    $v_qty_sisa_curr = save_int($v_qty_sisa_curr);
                    
                    if($v_qty_sisa_curr*1<0)
                    {
                        $msg = "Qty Rework melebihi Qty Sisa";
                        echo "<script>alert('".$msg."');</script>";     
                        die();    
                    }
                    else
                    {
                        $upd = "
                                UPDATE
                                    production_rework_return
                                SET
                                    qty_rework = '".$v_qty_rework."'        
                                WHERE
                                    1
                                    AND productionid = '".$v_productionid."' 
                                    AND returnno = '".$v_returnno."' 
                        ";
                        if(!mysql_query($upd))      
                        {
                            $msg = "Gagal update production_rework_return";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                    }
                }
                
                foreach($v_del_return as $key => $val)
                {
                    // cek NoDokumen slain produksi ini
                    // kalo gak ada lagi, update FalgRework Ke Y
                    $q = "
                            SELECT
                                production_rework_return.returnno
                            FROM
                                production_rework_return
                            WHERE
                                production_rework_return.returnno = '".$val."'
                    ";    
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($returnno) = $row;
                        
                        $q = "
                                SELECT
                                    production_rework_return.returnno
                                FROM
                                    production_rework_return
                                WHERE
                                    production_rework_return.returnno = '".$returnno."'
                                    AND production_rework_return.productionid != '".$v_productionid."'
                                LIMIT
                                    0,1
                        "; 
                        $qry_cek = mysql_query($q);
                        $row_cek = mysql_fetch_array($qry_cek);
                        
                        if($row_cek["returnno"]=="")
                        {
                            $q = "
                                    SELECT
                                        admin_cabang.Server,
                                        admin_cabang.User,
                                        admin_cabang.Password,
                                        admin_cabang.Database
                                    FROM
                                        admin_cabang
                                    WHERE
                                        1
                                        AND admin_cabang.KdCabang = '97'
                                        AND admin_cabang.Database = 's3distgudang'
                                    LIMIT
                                        0,1
                            ";
                            $qry_db = mysql_query($q);
                            $row_db = mysql_fetch_array($qry_db);
                            
                            $conn = mysql_connect($row_db["Server"], $row_db["User"], $row_db["Password"]);
                            
                            $q = "
                                    UPDATE
                                        ".$row_db["Database"].".trans_returheader
                                    SET
                                        FlagRework = 'Y'
                                    WHERE
                                        1
                                        AND ".$row_db["Database"].".trans_returheader.NoDokumen = '".$returnno."'
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal update Y";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            }
                            
                            $conn = mysql_connect($DBConnection->Host(), $DBConnection->User(), $DBConnection->Password()); 
                        }
                    } 
                    
                    $q = "DELETE FROM production_rework_return WHERE 1 AND productionid = '".$v_productionid."' AND returnno = '".$val."' ";    
                    if(!mysql_query($q))      
                    {
                        $msg = "Gagal delete production_rework_return";
                        echo "<script>alert('".$msg."');</script>";     
                        die();
                    }
                }
            }
            
            // usage
            {
                if(!isset($_POST["no_usage"])){ $no_usage = isset($_POST["no_usage"]); } else { $no_usage = $_POST["no_usage"]; }
                if(!isset($_POST["v_del_usage"])){ $v_del_usage = isset($_POST["v_del_usage"]); } else { $v_del_usage = $_POST["v_del_usage"]; }    
                
                foreach($no_usage as $key => $val)
                {
                    if(!isset($_POST["v_usage_inventorycode_".$val])){ $v_usage_inventorycode = isset($_POST["v_usage_inventorycode_".$val]); } else { $v_usage_inventorycode = $_POST["v_usage_inventorycode_".$val]; }
                    if(!isset($_POST["v_qty_usage_".$val])){ $v_qty_usage = isset($_POST["v_qty_usage_".$val]); } else { $v_qty_usage = $_POST["v_qty_usage_".$val]; }
                    if(!isset($_POST["v_qty_usage_old_".$val])){ $v_qty_usage_old = isset($_POST["v_qty_usage_old_".$val]); } else { $v_qty_usage_old = $_POST["v_qty_usage_old_".$val]; }
                    if(!isset($_POST["v_qty_ready_".$val])){ $v_qty_ready = isset($_POST["v_qty_ready_".$val]); } else { $v_qty_ready = $_POST["v_qty_ready_".$val]; }
                    if(!isset($_POST["v_usage_remarks_".$val])){ $v_usage_remarks = isset($_POST["v_usage_remarks_".$val]); } else { $v_usage_remarks = $_POST["v_usage_remarks_".$val]; }
                    if(!isset($_POST["v_usage_remarks_old_".$val])){ $v_usage_remarks_old = isset($_POST["v_usage_remarks_old_".$val]); } else { $v_usage_remarks_old = $_POST["v_usage_remarks_old_".$val]; }
                    
                    $v_qty_usage_old = save_int($v_qty_usage_old);
                    $v_qty_usage = save_int($v_qty_usage);
                    $v_qty_ready = save_int($v_qty_ready);
                    $v_usage_remarks = save_char($v_usage_remarks);
                    $v_usage_remarks_old = save_char($v_usage_remarks_old);
                   
                   if($v_qty_usage_old!=$v_qty_usage || $v_usage_remarks_old!=$v_usage_remarks)
                   {
                       if($v_qty_usage_old!=$v_qty_usage)
                       {
                           if($v_qty_usage*1 > ($v_qty_ready+$v_qty_usage_old)*1)
                           {
                                $msg = "Stok Internal Mutation Purpose Rework tidak mencukupi";
                                echo "<script>alert('".$msg."');</script>";     
                                die();    
                           } 
                       }
                   
                        $upd = "
                                UPDATE
                                    production_rework_usage
                                SET
                                    qty_usage = '".$v_qty_usage."',
                                    remarks = '".$v_usage_remarks."'
                                WHERE
                                    1
                                    AND productionid = '".$v_productionid."' 
                                    AND inventorycode = '".$v_usage_inventorycode."' 
                        ";
                        if(!mysql_query($upd))      
                        {
                            $msg = "Gagal update production_rework_usage";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                   }
                }
                
                foreach($v_del_usage as $key => $val)
                {
                    $q = "DELETE FROM production_rework_usage WHERE 1 AND productionid = '".$v_productionid."' AND inventorycode = '".$val."' ";    
                    if(!mysql_query($q))      
                    {
                        $msg = "Gagal delete production_rework_usage";
                        echo "<script>alert('".$msg."');</script>";     
                        die();
                    }
                }
            }
            
            // lebih
            {
                if(!isset($_POST["no_lebih"])){ $no_lebih = isset($_POST["no_lebih"]); } else { $no_lebih = $_POST["no_lebih"]; }
                if(!isset($_POST["v_del_lebih"])){ $v_del_lebih = isset($_POST["v_del_lebih"]); } else { $v_del_lebih = $_POST["v_del_lebih"]; }    
                
                foreach($no_lebih as $key => $val)
                {
                    if(!isset($_POST["v_lebih_inventorycode_".$val])){ $v_lebih_inventorycode = isset($_POST["v_lebih_inventorycode_".$val]); } else { $v_lebih_inventorycode = $_POST["v_lebih_inventorycode_".$val]; }
                    if(!isset($_POST["v_qty_lebih_".$val])){ $v_qty_lebih = isset($_POST["v_qty_lebih_".$val]); } else { $v_qty_lebih = $_POST["v_qty_lebih_".$val]; }
                    if(!isset($_POST["v_lebih_remarks_".$val])){ $v_lebih_remarks = isset($_POST["v_lebih_remarks_".$val]); } else { $v_lebih_remarks = $_POST["v_lebih_remarks_".$val]; }
                    
                    $v_qty_lebih = save_int($v_qty_lebih);
                    $v_lebih_remarks = save_char($v_lebih_remarks);
                    
                    $upd = "
                            UPDATE
                                production_rework_lebih
                            SET
                                qty = '".$v_qty_lebih."',
                                remarks = '".$v_lebih_remarks."'
                            WHERE
                                1
                                AND productionid = '".$v_productionid."' 
                                AND inventorycode = '".$v_lebih_inventorycode."' 
                    ";
                    if(!mysql_query($upd))      
                    {
                        $msg = "Gagal update production_rework_lebih";
                        echo "<script>alert('".$msg."');</script>";     
                        die();
                    }
                }
                
                foreach($v_del_lebih as $key => $val)
                {
                    $q = "DELETE FROM production_rework_lebih WHERE 1 AND productionid = '".$v_productionid."' AND inventorycode = '".$val."' ";    
                    if(!mysql_query($q))      
                    {
                        $msg = "Gagal delete production_rework_lebih";
                        echo "<script>alert('".$msg."');</script>";     
                        die();
                    }
                }
            }
            
            update_bincard($v_productionid);
            $msg = "Berhasil menyimpan";
            echo "<script>alert('".$msg."');</script>";         
            echo "<script>parent.reload_page('".$v_productionid."');</script>";         
            die();
        }
    }
    
}


$q = "
        SELECT
            inventorymaster.inventorycode,
            inventorymaster.inventoryname
        FROM
            inventorymaster
        WHERE
            1
            AND inventorymaster.stocktypeid = '3'
            AND inventorymaster.imported = '0'
            ".$where_pcode."
        ORDER BY
            inventorymaster.inventoryname ASC
";
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{
    list($inventorycode, $inventoryname) = $row;
    
    $arr_data["list_fg"][$inventorycode] = $inventorycode;
    
    $arr_data["inventoryname"][$inventorycode] = $inventorycode." :: ".$inventoryname;
}

$q = "
        SELECT 
            production.productionid,
            production.rework_type,
            production.productiondate,
            production.inventoryprefix,
            production.batchnumber,
            production.productreworkcode,
            inventorymaster.inventoryname,
            production.remarks
        FROM
            production
            INNER JOIN inventorymaster ON
                production.productreworkcode = inventorymaster.inventorycode
        WHERE
            1
            AND production.productionid = '".$v_productionid."'
        LIMIT
            0,1
";
$qry_curr = mysql_query($q);
$arr_curr = mysql_fetch_array($qry_curr);

$v_production_date = date("d-m-Y");
if($arr_curr["productiondate"])
{
    $v_production_date = format_show_date($arr_curr["productiondate"]);
}

$v_batchnumber_2 = str_replace($arr_curr["inventoryprefix"],"",$arr_curr["batchnumber"]);

if($v_productionid)
{
    $_SESSION["ses_productionid"] = $v_productionid;
    $_SESSION["ses_finishgoods"]  = $arr_curr["productreworkcode"];
    
    $list_return = "";
    $q = "
            SELECT
                production_rework_return.returnno,
                production_rework_return.inventorycode,
                production_rework_return.qty_return,
                production_rework_return.qty_rework
            FROM
                production_rework_return
            WHERE
                1
                AND production_rework_return.productionid = '".$v_productionid."'
            ORDER BY
                production_rework_return.returnno ASC
    ";
    $jml = 0;
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($returnno, $inventorycode, $qty_return, $qty_rework) = $row;
        
        $arr_data["list_return"][$returnno] = $returnno;
        
        $arr_data["return_return"][$returnno] = $qty_return;
        $arr_data["return_rework"][$returnno] = $qty_rework;
        
        $list_return .= "'".$returnno."',";
        $jml++;
    }
    $list_return = substr($list_return, 0 , -1);
    
    if($jml)
    {
        $q = "
                SELECT
                    production_rework_return.productionid,
                    production_rework_return.returnno,
                    production_rework_return.inventorycode,
                    production_rework_return.qty_rework
                FROM
                    production_rework_return
                WHERE
                    1
                    AND production_rework_return.returnno IN (".$list_return.")
                ORDER BY
                    production_rework_return.returnno ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($productionid, $returnno, $inventorycode, $qty_rework) = $row;
            
            $arr_data["return_usage"][$returnno][$inventorycode] += $qty_rework;
        }
    }
    
    $q = "
            SELECT
                production_rework_lebih.inventorycode,
                inventorymaster.inventoryname,
                production_rework_lebih.qty,
                production_rework_lebih.remarks
            FROM
                production_rework_lebih
                INNER JOIN inventorymaster ON
                    production_rework_lebih.inventorycode = inventorymaster.inventorycode
            WHERE
                1
                AND production_rework_lebih.productionid = '".$v_productionid."'
            ORDER BY
                production_rework_lebih.sid ASC
    ";
    
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($inventorycode, $inventoryname, $qty, $remarks) = $row;
        
        $arr_data["list_lebih"][$inventorycode] = $inventorycode;
        
        $arr_data["inventoryname"][$inventorycode] = $inventoryname;
        $arr_data["lebih_qty"][$inventorycode] = $qty;
        $arr_data["lebih_remarks"][$inventorycode] = $remarks;
        
        
    }
    
    $q = "
            SELECT
                production_rework_usage.inventorycode,
                inventorymaster.inventoryname,
                inventorymaster.stocktypeid,
                production_rework_usage.qty_usage,
                production_rework_usage.remarks
            FROM
                production_rework_usage
                INNER JOIN inventorymaster ON
                    production_rework_usage.inventorycode = inventorymaster.inventorycode
            WHERE
                1
                AND production_rework_usage.productionid = '".$v_productionid."'
            ORDER BY
                production_rework_usage.sid ASC
    ";
    $where_usage = "";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($inventorycode, $inventoryname, $stocktypeid, $qty_usage, $remarks) = $row;
        
        $arr_data["list_usage"][$inventorycode] = $inventorycode;
        
        $arr_data["inventoryname"][$inventorycode] = $inventoryname;
        $arr_data["stocktypeid"][$inventorycode] = $stocktypeid;
        $arr_data["usage_qty"][$inventorycode] = $qty_usage;
        $arr_data["usage_remarks"][$inventorycode] = $remarks;
        
        if($where_usage=="")
        {
             $where_usage .= " AND ( inventorymaster.inventorycode = '".$inventorycode."' ";
        }
        else
        {
            $where_usage .= " OR inventorymaster.inventorycode = '".$inventorycode."' ";
        }
    }
    if($where_usage)
    {
        $where_usage .= ")";
    }
    
    
    if($where_usage)
    {
        $q = "
                SELECT 
                    inventorymaster.inventorycode, 
                    inventorymaster.inventoryname,
                    inventorymaster.stocktypeid,
                    (COALESCE(internal_rework.quantity,0) - COALESCE(tbl_usage.qty_usage,0)) AS qty_ready
                 FROM 
                    inventorymaster 
                    LEFT JOIN
                    (
                        SELECT
                            internalmutationdetail.inventorycode,
                            SUM(internalmutationdetail.quantity) as quantity
                        FROM
                            internalmutationdetail
                            INNER JOIN internalmutation ON
                                internalmutationdetail.imno = internalmutation.imno
                                AND internalmutation.purpose = '6'
                                AND internalmutation.imdate >= '".$cut_point_date_rework."'
                        WHERE
                            1
                        GROUP BY
                            internalmutationdetail.inventorycode
                    )as internal_rework ON
                        internal_rework.inventorycode = inventorymaster.inventorycode
                    
                    LEFT JOIN 
                    (
                        SELECT
                            production_rework_usage.inventorycode,
                            SUM(production_rework_usage.qty_usage) AS qty_usage
                        FROM
                            production_rework_usage
                        WHERE
                            1
                        GROUP BY
                            production_rework_usage.inventorycode
                    )as tbl_usage ON
                        tbl_usage.inventorycode = inventorymaster.inventorycode
                    
                 WHERE 
                    1
                    AND inventorymaster.stocktypeid IN('1','2','3')
                    AND (COALESCE(internal_rework.quantity,0) - COALESCE(tbl_usage.qty_usage,0)) > '0'
                    ".$where_usage."
                 ORDER BY
                    inventorymaster.stocktypeid ASC,
                    inventorymaster.inventorycode ASC 
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname, $stocktypeid, $qty_ready) = $row;  
            
            $arr_data["qty_ready"][$inventorycode] = $qty_ready;
        }
    }
    
    
    
}


//print_r($arr_data["qty_ready"]);
?>
<html>
    <head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="description" content="Neon Admin Panel" />
	    <meta name="author" content="" />

	    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
	    <link rel="shortcut icon" href="public/images/Logosg.png" >
	    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	    <link rel="stylesheet" href="assets/css/bootstrap.css">
	    <link rel="stylesheet" href="assets/css/neon-core.css">
	    <link rel="stylesheet" href="assets/css/neon-theme.css">
	    <link rel="stylesheet" href="assets/css/neon-forms.css">
	    <link rel="stylesheet" href="assets/css/custom.css">
	    <link rel="stylesheet" href="assets/css/skins/black.css">
	    <link rel="stylesheet" href="public/css/style.css">
	    <link rel="stylesheet" href="assets/css/my.css">

	    <script src="assets/js/jquery-1.11.0.min.js"></script>
        
<script type="text/javascript">


function EW_checkMyForm(EW_this) 
{
if (EW_this.dtStarting && !EW_checkeurodate(EW_this.dtStarting.value)) 
{
if (!EW_onError(EW_this, EW_this.dtStarting, "TEXT", "Incorrect date, format = dd-mm-yyyy - Tgl Lahir"))
return false; 
}
if (EW_this.dtEnding && !EW_checkeurodate(EW_this.dtEnding.value)) 
{
if (!EW_onError(EW_this, EW_this.dtEnding, "TEXT", "Incorrect date, format = dd-mm-yyyy - Tgl Lahir"))
return false; 
}
return true;
}
</script>

<script>
function change_onMouseOver (id) {
  document.getElementById(id).style.background = '#EEEEEE';
}

function change_onMouseOut (id) {
  document.getElementById(id).style.background = '#FFFFFF';
}

function targetBlank (url) {
    blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
}

function reload_page(productionid)
{
    window.location = '<?php echo $file_name."?v_productionid="; ?>'+productionid;
}

function reload_page_list()
{
    window.location = 'productionsearch.php';
}
 
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

var xmlhttp = createRequestObject();

function CallAjax(tipenya,param1,param2,param3,param4,param5)
{
    try{    
        if (!tipenya) return false;
        
        if (param1 == undefined) param1 = '';
        if (param2 == undefined) param2 = '';
        if (param3 == undefined) param3 = '';
        if (param4 == undefined) param4 = '';
        if (param5 == undefined) param5 = '';
        
        var variabel;
        var arr_data;
        variabel = "";
        
        if(tipenya=='ajax_search_fg')
        {  
            document.getElementById("loading_save").style.display='';
            variabel += "&v_keyword_fg="+param1;
            
            //alert(variabel);
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("td_fg").innerHTML     = xmlhttp.responseText;
                    document.getElementById('loading_save').style.display    = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_choose_fg')
        {  
            document.getElementById("loading_save").style.display=''; 
            
            variabel += "&v_inventorycode="+param1;
            
            //alert(variabel);
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {  
                    document.getElementById("v_batchnumber_1").value        = xmlhttp.responseText;
                    document.getElementById("v_batchnumber_2").focus();
                    document.getElementById('loading_save').style.display   = 'none';
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        

    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    } 

}

function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
            
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}

function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
{
    var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    //alert('aaaa');

    newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth + 
        ',height=' + windowHeight + 
        ',left=' + centerWidth + 
        ',top=' + centerHeight
        );

    newWindow.focus();
    return newWindow.name;
}

function pop_up_sales_return()
{
    windowOpener('600', '800', 'Pop Up Sales Return', 'npm_production_rework_pop_up_sales_return.php', 'Pop Up Sales Return')    
}

function pop_up_item_lebih()
{
    windowOpener('600', '800', 'Pop Up List Item Yang masih dapat digunakan', 'npm_production_rework_pop_up_item_lebih.php', 'Pop Up List Item Yang masih dapat digunakan')    
}

function pop_up_item_usage()
{
    windowOpener('600', '800', 'Pop Up List Penambahan Item ', 'npm_production_rework_pop_up_item_usage.php', 'Pop Up List Penambahan Item ')    
}

function calculate()
{
    try{
        jml_return = document.getElementById("jml_return").value;
        var total_rework;
        var total_sisa;
        
        total_rework = 0;
        total_sisa = 0;
        
        for(i=1;i<=jml_return;i++)
        {
            qty_rework = reform(document.getElementById("v_qty_rework_"+i).value)*1;
            qty_rework_old = reform(document.getElementById("v_qty_rework_old_"+i).value)*1;
            qty_sisa   = reform(document.getElementById("v_qty_sisa_"+i).value)*1;
            
            sisa = ((qty_rework_old*1) + (qty_sisa*1)) - (qty_rework*1);
            
            document.getElementById("td_qty_sisa_"+i).innerHTML = format(sisa);
            document.getElementById("v_qty_sisa_curr_"+i).value = format(sisa);
            
            total_rework += (qty_rework*1);
            total_sisa += (sisa*1);
        }
        
        document.getElementById("td_total_sisa").innerHTML = format(total_sisa);
        document.getElementById("td_total_rework").innerHTML = format(total_rework);
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description Calculate: "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    }     
}

function confirm_delete(batchnumber)
{
    var r = confirm("Anda yakin ingin menghapus "+batchnumber+" ?");
    if(r){
        document.getElementById("v_del").value = 1;
        document.getElementById("theform").submit();
    }
}


</script>

</head>
<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
			<div class="col-md-12">
		
				<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>

    			<form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>" enctype="multipart/form-data">
    			<input type="hidden" name="v_productionid" id="v_productionid" value="<?php echo $v_productionid; ?>">
    			<input type="hidden" name="v_del" id="v_del" value="0">
    			<table class="table table-bordered responsive">
			        <tr>
			            <td class="title_table" width="250">Type</td>
			            <td>
			                <?php 
			                    if($v_productionid)
			                    {
			                        echo ucwords(str_replace("_", " ", $arr_curr["rework_type"]));
			                    }
			                    else
			                    {
			                ?>
			                <select name="v_rework_type" id="v_rework_type" class="form-control-new">
			                    <option value="sales_return">Sales Return</option>
			                    <option value="banded" style="display:none;">Banded</option>
			                </select>
			                <?php 
			                    }
			                ?>
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="title_table">Production Date</td>
			            <td>
			                <input type="form-control-new" size="10" maxlength = "10" name="v_production_date" id="v_production_date" value="<?php echo $v_production_date; ?>" >
			                <img src="cal.gif" onClick="popUpCalendar(this, theform.v_production_date, 'dd-mm-yyyy');">
			            </td>
			        </tr>
			    
			        <tr>
			            <td class="title_table">Finish Goods</td>
			            <td>
			                <?php 
			                    if($v_productionid)
			                    {
			                        echo $arr_curr["productreworkcode"]." :: ".$arr_curr["inventoryname"];
			                        ?>
			                            <input type="hidden" name="v_inventorycode" id="v_inventorycode" value="<?php echo $arr_curr["productreworkcode"]; ?>">
			                        <?php
			                    }
			                    else
			                    {
			                ?>
			                <b>Keyword</b> 
			                <input type="text" class="form-control-new" name="v_keyword_fg" id="v_keyword_fg" size="10" maxlength="50" value="" onkeyup="CallAjax('ajax_search_fg', this.value)">
			                
			                <span id="td_fg">
			                <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:250px;" onchange="CallAjax('ajax_choose_fg', this.value)">
			                    <option value="">-</option>
			                    <?php 
			                        foreach($arr_data["list_fg"] as $inventorycode => $val)
			                        {
			                            $inventoryname = $arr_data["inventoryname"][$inventorycode];
			                            ?>
			                            
			                            <option value="<?php echo $inventorycode; ?>"><?php echo $inventoryname; ?></option>
			                            <?php
			                        }
			                    ?>
			                </select>
			                </span>
			                <?php 
			                    }
			                ?> 
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="title_table">Batchnumber</td>
			            <td>
			                <input type="text" class="form-control-new" style="background:#EEEEEE; text-align:right;" size="6" maxlength="6" name="v_batchnumber_1" id="v_batchnumber_1" readonly="readonly" value="<?php echo $arr_curr["inventoryprefix"]; ?>">
			                <input type="text" class="form-control-new" size="19" maxlength="20" name="v_batchnumber_2" id="v_batchnumber_2" value="<?php echo $v_batchnumber_2; ?>">
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="title_table">Remarks</td>
			            <td><input type="text" class="form-control-new" name="v_remarks" id="v_remarks" size="30" maxlength="255" value="<?php echo $arr_curr["remarks"]; ?>"></td>
			        </tr>
			        
			        <tr>
			            <td>&nbsp;</td>
			            <td>
			                <?php 
			                    if($v_productionid)
			                    {
			                        $q = "
			                                SELECT
			                                    productionfinishgoods.productionid2
			                                FROM
			                                    productionfinishgoods
			                                WHERE
			                                    productionfinishgoods.productionid2 = '".$v_productionid."'
			                                LIMIT
			                                    0,1
			                        ";
			                        $qry_fg = mysql_query($q);
			                        $row_fg = mysql_fetch_array($qry_fg);
			                        
			                        //$row_fg["productionid2"] = 123;
			                        
			                        if($row_fg["productionid2"])
			                        {
			                            echo "<font color='red'>Button Save tidak muncul karena sudah ada Production FG</font>";    
			                        }
			                        else
			                        {
			                        ?>
			                        	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
			                        	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_delete" id="btn_delete" onclick="confirm_delete('<?php echo $arr_curr["batchnumber"]; ?>')" value="Delete Rework">Delete Rework<i class="entypo-cancel"></i></button>
			                        <?php
			                        }
			                    }
			                    else
			                    {
			                ?>
			                		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
			                <?php 
			                    }
			                ?>
			                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="reload_page_list()" value="Close">Close<i class="entypo-cancel"></i></button>
			                <span id="loading_save" style="display: none;"><font color="red">Loading...</font></span>
			                &nbsp;
			            </td>
			        </tr>
			        
			        <tr>
			            <td colspan="100%">&nbsp;</td>
			        </tr>
			        
			        <?php 
			            if($v_productionid)
			            {
			        ?>
			            
			            <tr>
			                <td colspan="100%" style="font-weight:bold;">
			                    <?php 
			                        if(!$row_fg["productionid2"])
			                        {
			                    ?>
			                    <input type="button" class="button_new" onclick="pop_up_sales_return()" value="Add">&nbsp;
			                    <?php 
			                        }
			                    ?>
			                    List Retur Yang Akan Di Rework
			                </td>
			            </tr>
			            
			            <tr>
			                <td colspan="100%">
			                    <table width="100%">
			                        <tr class="title_table" align="center">
			                            <td width="30">No</td>
			                            <td>Return No</td>
			                            <td>Qty Return</td>
			                            <td>Qty Sisa</td>
			                            <td>Qty Rework</td>
			                            <td>&nbsp;</td>
			                            <td width="30">Del</td>
			                        </tr>
			                        
			                        <?php
			                            if(count($arr_data["list_return"])==0)
			                            {
			                                ?>
			                                    <tr>
			                                        <td colspan="100%" align="center">No Data</td>
			                                    </tr>
			                                <?php
			                            }
			                        
			                            $jml_return = 0; 
			                            $jml_sisa = 0; 
			                            $jml_rework = 0; 
			                            $no_return = 1; 
			                            foreach($arr_data["list_return"] as $returnno => $val)
			                            {
			                                $qty_return = $arr_data["return_return"][$returnno];
			                                $qty_sisa   = $arr_data["return_return"][$returnno] - $arr_data["return_usage"][$returnno][$arr_curr["productreworkcode"]];
			                                $qty_rework = $arr_data["return_rework"][$returnno];
			                        ?>
			                        
			                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
			                            <td>
			                                <?php echo $no_return; ?>
			                                <input type="hidden" name="no_return[]" value="<?php echo $no_return; ?>">
			                                <input type="hidden" name="v_returnno_<?php echo $no_return; ?>" id="v_returnno_<?php echo $no_return; ?>" value="<?php echo $returnno; ?>">
			                                
			                                <input type="hidden" name="v_qty_rework_old_<?php echo $no_return; ?>" id="v_qty_rework_old_<?php echo $no_return; ?>" value="<?php echo format_number($qty_rework); ?>">
			                                <input type="hidden" name="v_qty_sisa_<?php echo $no_return; ?>" id="v_qty_sisa_<?php echo $no_return; ?>" value="<?php echo format_number($qty_sisa); ?>">
			                                
			                                <input type="hidden" name="v_qty_sisa_curr_<?php echo $no_return; ?>" id="v_qty_sisa_curr_<?php echo $no_return; ?>" value="<?php echo format_number($qty_sisa); ?>">
			                            </td>
			                            <td><?php echo $returnno; ?></td>
			                            <td align="right"><?php echo format_number($qty_return); ?></td>
			                            <td align="right" id="td_qty_sisa_<?php echo $no_return; ?>">&nbsp;<?php echo format_number($qty_sisa); ?></td>
			                            <td align="right"><input type="text" onblur="toFormat('v_qty_rework_<?php echo $no_return; ?>'), calculate()" onkeyup="calculate()" name="v_qty_rework_<?php echo $no_return; ?>" id="v_qty_rework_<?php echo $no_return; ?>" value="<?php echo format_number($qty_rework); ?>" style="text-align:right;"></td>
			                            <td>&nbsp;</td>
			                            <td align="center"><input type="checkbox" name="v_del_return[]" id="v_del_return_<?php echo $no_return; ?>" value="<?php echo $returnno; ?>"></td>
			                        </tr>
			                        
			                        <?php
			                                $jml_return += $qty_return;
			                                $jml_sisa += $qty_sisa;
			                                $jml_rework += $qty_rework;
			                                $no_return++; 
			                            }
			                            $no_return--;
			                            
			                        ?>
			                        <input type="hidden" name="jml_return" id="jml_return" value="<?php echo $no_return; ?>">
			                        
			                        <?php 
			                            if(count($arr_data["list_return"])!=0)
			                            {
			                        ?>
			                        <tr style="font-weight:bold;" align="right" height="25">
			                            <td colspan="2">Total Rework</td>
			                            <td align="right">&nbsp;<?php echo format_number($jml_return); ?></td>
			                            <td align="right" id="td_total_sisa">&nbsp;<?php echo format_number($jml_sisa); ?></td>
			                            <td align="right" id="td_total_rework">&nbsp;<?php echo format_number($jml_rework); ?></td>
			                            <td>&nbsp;</td>
			                            <td>&nbsp;</td>
			                        </tr>
			                        <?php 
			                            }
			                        ?>
			                        
			                        
			                        <tr>
			                            <td colspan="100%">&nbsp;</td>
			                        </tr>
			                        
			                        <tr>
			                            <td colspan="100%" style="font-weight:bold;">
			                                <?php 
			                                    if(!$row_fg["productionid2"])
			                                    {
			                                ?>
			                                <input type="button" class="button_new" value="Add" onclick="pop_up_item_usage()">&nbsp;
			                                <?php 
			                                    }
			                                ?>
			                                List Penambahan Item
			                            </td>
			                        </tr>
			                        
			                        <tr class="title_table" align="center">
			                            <td width="30">No</td>
			                            <td>Inventory Code</td>
			                            <td>Inventory Name</td>
			                            <td>Qty Internal Mutation</td>
			                            <td>Qty Usage</td>
			                            <td>Remarks</td>
			                            <td width="30">Del</td>
			                        </tr>
			                        
			                        <?php 
			                            $no_usage = 1;
			                            foreach($arr_data["list_usage"] as $inventorycode => $val)
			                            {
			                                $inventoryname = $arr_data["inventoryname"][$inventorycode];
			                                $stocktypeid = $arr_data["stocktypeid"][$inventorycode];
			                                $qty_usage = $arr_data["usage_qty"][$inventorycode];
			                                $remarks = $arr_data["usage_remarks"][$inventorycode];
			                                
			                                $qty_ready = $arr_data["qty_ready"][$inventorycode];
			                                
			                                if($stocktypeid==1)
			                                {
			                                    $decimal = 4;
			                                    $decimal_php = 4;
			                                }
			                                else
			                                {
			                                    $decimal = "";
			                                    $decimal_php = 0;
			                                }
			                        ?>
			                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
			                            <td>
			                                <?php echo $no_usage; ?>
			                                <input type="hidden" name="no_usage[]" value="<?php echo $no_usage; ?>">
			                                <input type="hidden" name="v_usage_inventorycode_<?php echo $no_usage; ?>" id="v_usage_inventorycode_<?php echo $no_usage; ?>" value="<?php echo $inventorycode; ?>">
			                                <input type="hidden" name="v_qty_ready_<?php echo $no_usage; ?>" id="v_qty_ready_<?php echo $no_usage; ?>" value="<?php echo $qty_ready; ?>">
			                                                                                                                                                                             
			                                <input type="hidden" name="v_qty_usage_old_<?php echo $no_usage; ?>" id="v_qty_usage_old_<?php echo $no_usage; ?>" value="<?php echo format_number($qty_usage, $decimal); ?>">
			                            </td>
			                            <td><?php echo $inventorycode; ?></td>
			                            <td><?php echo $inventoryname; ?></td>
			                            <td align="right">&nbsp;<?php echo format_number($qty_ready, $decimal_php); ?></td>
			                            <td align="right"><input size="10" type="text" onblur="toFormat<?php echo $decimal; ?>('v_qty_usage_<?php echo $no_usage; ?>')" name="v_qty_usage_<?php echo $no_usage; ?>" id="v_qty_usage_<?php echo $no_usage; ?>" value="<?php echo format_number($qty_usage, $decimal_php); ?>" style="text-align:right;"></td>
			                            <td><input type="text" class="text" name="v_usage_remarks_<?php echo $no_usage; ?>" id="v_usage_remarks_<?php echo $no_usage; ?>" size="30" maxlength="100" value="<?php echo $remarks; ?>"></td>
			                            <td align="center"><input type="checkbox" name="v_del_usage[]" id="v_del_usage_<?php echo $no_usage; ?>" value="<?php echo $inventorycode; ?>"></td>
			                        </tr>
			                        
			                        <?php 
			                                $no_usage++;
			                            }
			                        ?>
			                        
			                        
			                        <tr>
			                            <td colspan="100%">&nbsp;</td>
			                        </tr>
			                        
			                        <tr>
			                            <td colspan="100%" style="font-weight:bold;">
			                                <?php 
			                                    if(!$row_fg["productionid2"])
			                                    {
			                                ?>
			                                <input type="button" class="button_new" value="Add" onclick="pop_up_item_lebih()">&nbsp;
			                                <?php 
			                                    }
			                                ?>
			                                List Item Yang masih dapat digunakan
			                            </td>
			                        </tr>
			                        
			                        <tr class="title_table" align="center">
			                            <td width="30">No</td>
			                            <td>Inventory Code</td>
			                            <td colspan="2">Inventory Name</td>
			                            <td>Qty</td>
			                            <td>Remarks</td>
			                            <td width="30">Del</td>
			                        </tr>
			                        
			                        <?php
			                        if(count($arr_data["list_lebih"])==0)
			                        {
			                            ?>
			                                <tr>
			                                    <td colspan="100%" align="center">No Data</td>
			                                </tr>
			                            <?php
			                        }
			                        
			                        $no_lebih = 1;
			                        foreach($arr_data["list_lebih"] as $inventorycode => $val)
			                        {
			                            $inventoryname = $arr_data["inventoryname"][$inventorycode];
			                            $qty = $arr_data["lebih_qty"][$inventorycode];
			                            $remarks = $arr_data["lebih_remarks"][$inventorycode];
			                        ?>
			                        
			                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
			                            <td>
			                                <?php echo $no_lebih; ?>
			                                <input type="hidden" name="no_lebih[]" value="<?php echo $no_lebih; ?>">
			                                <input type="hidden" name="v_lebih_inventorycode_<?php echo $no_lebih; ?>" id="v_lebih_inventorycode_<?php echo $no_lebih; ?>" value="<?php echo $inventorycode; ?>">
			                            </td>
			                            <td><?php echo $inventorycode; ?></td>
			                            <td colspan="2"><?php echo $inventoryname; ?></td>
			                            <td align="right"><input size="10" type="text" onblur="toFormat('v_qty_lebih_<?php echo $no_lebih; ?>')" name="v_qty_lebih_<?php echo $no_lebih; ?>" id="v_qty_lebih_<?php echo $no_lebih; ?>" value="<?php echo format_number($qty); ?>" style="text-align:right;"></td>
			                            <td><input type="text" class="text" name="v_lebih_remarks_<?php echo $no_lebih; ?>" id="v_lebih_remarks_<?php echo $no_lebih; ?>" size="30" maxlength="100" value="<?php echo $remarks; ?>"></td>
			                            <td align="center"><input type="checkbox" name="v_del_lebih[]" id="v_del_lebih_<?php echo $no_lebih; ?>" value="<?php echo $inventorycode; ?>"></td>
			                        </tr>
			                        
			                        <?php 
			                                $no_lebih++;
			                            }
			                        ?>
			                        
			                    </table>
			                </td>
			            </tr>
			            
			        <?php 
			            }
			        ?>
			    </table>
    			</form>
    		
    		</div>
    	</div>

<?php include("footer.php"); ?>

