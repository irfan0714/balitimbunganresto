<?php
    include("header.php");
    
	$modul        = "Stock Opname Production";
	$file_current = "npm_stock_opname.php";
	$file_name    = "npm_stock_opname_data.php";

	$arr_data["month"][1] = "January";
	$arr_data["month"][2] = "February";
	$arr_data["month"][3] = "March";
	$arr_data["month"][4] = "April";
	$arr_data["month"][5] = "May";
	$arr_data["month"][6] = "June";
	$arr_data["month"][7] = "July";
	$arr_data["month"][8] = "August";
	$arr_data["month"][9] = "September";
	$arr_data["month"][10] = "October";
	$arr_data["month"][11] = "November";
	$arr_data["month"][12] = "December";
	
    $year_end   = date("Y");
    $year_start = date("Y")-3;

	$v_from_date = "01-".date("m-Y");
	$v_to_date   = date("t", parsedate($v_from_date, "-"))."-".date("m-Y");

// cek warehouseadmin
$q = "
        SELECT
            warehouseadmin.warehousecode
        FROM
            warehouseadmin
        WHERE
            1
            AND warehouseadmin.userid = '".$ses_login."'
            AND warehouseadmin.warehousecode IN('WH000','WH001','WH002','WH005','WH008')
";
$qry_warehouseadmin = mysql_query($q);
while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
{
    list($warehousecode) = $row_warehouseadmin;
    
    $arr_data["warehouseadmin"][$warehousecode] = $warehousecode;
}

$jml_warehouse = 0;
$q = "
        SELECT
            warehouse.warehousecode,
            warehouse.warehousename
        FROM
            warehouse
        WHERE
            1
            AND warehouse.warehousecode IN ('WH000','WH001','WH002','WH005','WH008')
        ORDER BY
            warehouse.warehousecode ASC
";
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{
    list($warehousecode, $warehousename) = $row;
    
    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
    
    $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
    $jml_warehouse++;
}

$arr_data["list_month"][1] = "January";
$arr_data["list_month"][2] = "February";
$arr_data["list_month"][3] = "March";
$arr_data["list_month"][4] = "April";
$arr_data["list_month"][5] = "May";
$arr_data["list_month"][6] = "June";
$arr_data["list_month"][7] = "July";
$arr_data["list_month"][8] = "August";
$arr_data["list_month"][9] = "September";
$arr_data["list_month"][10] = "October";
$arr_data["list_month"][11] = "November";
$arr_data["list_month"][12] = "December";

$q = "
        SELECT
            YEAR(stockopname.opnamedate) as start_year
        FROM
            stockopname
        WHERE
            1
        ORDER BY
            stockopname.opnamedate ASC
        LIMIT
            0,1
";
$qry = mysql_query($q);
$row = mysql_fetch_array($qry);

$start_year = $row["start_year"];
$end_year   = date("Y");
for($i=$start_year;$i<=$end_year;$i++)
{
    $arr_data["list_year"][$i] = $i;    
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="description" content="Neon Admin Panel" />
	    <meta name="author" content="" />

	    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
	    <link rel="shortcut icon" href="public/images/Logosg.png" >
	    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    	<link rel="stylesheet" href="assets/css/NotoSans.css">
	    <link rel="stylesheet" href="assets/css/bootstrap.css">
	    <link rel="stylesheet" href="assets/css/neon-core.css">
	    <link rel="stylesheet" href="assets/css/neon-theme.css">
	    <link rel="stylesheet" href="assets/css/neon-forms.css">
	    <link rel="stylesheet" href="assets/css/custom.css">
	    <link rel="stylesheet" href="assets/css/skins/black.css">
	    <link rel="stylesheet" href="public/css/style.css">
	    <link rel="stylesheet" href="assets/css/my.css">

	    <script src="assets/js/jquery-1.11.0.min.js"></script>

	    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

<script>

function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
{
    var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    //alert('aaaa');

    newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth + 
        ',height=' + windowHeight + 
        ',left=' + centerWidth + 
        ',top=' + centerHeight
        );

    newWindow.focus();
    return newWindow.name;
}

function pop_up_details()
{
    windowOpener('300', '400', 'Pop Up CSV', 'vci_stock_opname_pop_up_csv.php', 'Pop Up CSV');
}

function num_format(obj) {
    obj.value = new NumberFormat(obj.value).toFormatted();
}
  
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

var xmlhttp = createRequestObject();

function CallAjax(tipenya,param1,param2,param3,param4,param5)
{
    try{    
        if (!tipenya) return false;
        //document.getElementById("loading").style.display='block';
        
        if (param1 == undefined) param1 = '';
        if (param2 == undefined) param2 = '';
        if (param3 == undefined) param3 = '';
        if (param4 == undefined) param4 = '';
        if (param5 == undefined) param5 = '';
        
        var variabel;
        variabel = "";
        
        if(tipenya=='search')
        {  
            search_month = document.getElementById("search_month").value;
            search_year = document.getElementById("search_year").value;
            search_warehouse = document.getElementById("search_warehouse").value;

            variabel += "&search_month="+search_month;
            variabel += "&search_year="+search_year;
            variabel += "&search_warehouse="+search_warehouse;
            
            variabel += "&v_unik_curr="+param1;
            
            //alert(variabel);
            
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-search").innerHTML     = xmlhttp.responseText;
                    document.getElementById("col-header").innerHTML = "";
                    
                    if(param1)
                    {
                        CallAjax('edit_data',param1);
                    }
                }

                return false;
            }
            xmlhttp.send(null);
        } 
        else if(tipenya=='add_data')
        {  

            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='edit_data')
        {  
            variabel += "&v_unik_id="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_warehouse')
        {  
            variabel += "&v_warehousecode="+param1;
            
            //alert(variabel);
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("td_type").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    } 
}

function confirm_delete(user_form)
{
    var r = confirm("Anda yakin ingin menghapus "+user_form+" ?");
    if(r){
        document.getElementById("v_del").value = '1';
        document.getElementById("theform").submit();
    }
}

function copy_dataquantity(no)
{
    v_dataquantity = reform(document.getElementById("v_dataquantity_"+no).value);
    
    document.getElementById("v_physicalquantity_"+no).value = format6(v_dataquantity);
}

function confirm_approve()
{
    var r = confirm("Anda yakin ingin Approve ?");
    if(r){
        document.getElementById("v_sts_approve").value = '1';
        document.getElementById("theform").submit();
    }
}

function confirm_unapprove()
{
    var r = confirm("Anda yakin ingin UnApprove ?");
    if(r){
        document.getElementById("v_sts_unapprove").value = '1';
        document.getElementById("theform").submit();
    }
}

function confirm_close_period()
{
    var r = confirm("Anda yakin ingin Close Period ?");
    if(r){
        document.getElementById("v_sts_close_period").value = '1';
        document.getElementById("theform").submit();
    }
}



function CheckAll(param, target){
    var field = document.getElementsByName(target);
    var chkall = document.getElementById(param);
    if (chkall.checked == true){
        for (i = 0; i < field.length; i++)
            field[i].checked = true ;
    }else{
        for (i = 0; i < field.length; i++)
            field[i].checked = false ;
    }        
}

function reform(val)
{    
     var a = val.split(",");
    var b = a.join("");
    //alert(b);
    return b;
}

function format(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(0);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}

function format4(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(4);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}

function format6(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(6);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}
 
function addSeparatorsNF(nStr, inD, outD, sep)
{
 nStr += '';
 var dpos = nStr.indexOf(inD);
 var nStrEnd = '';
 if (dpos != -1) {
  nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
  nStr = nStr.substring(0, dpos);
 }
 var rgx = /(\d+)(\d{3})/;
 while (rgx.test(nStr)) {
  nStr = nStr.replace(rgx, '$1' + sep + '$2');
 }
 return nStr + nStrEnd;
}

function toFormat(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format(document.getElementById(id).value);
}
function toFormat4(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format4(document.getElementById(id).value);
}
function toFormat6(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format6(document.getElementById(id).value);
}
function isanumber(id)
{
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        document.getElementById(id).value=0;
    }
    else
    {
        document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
    }
}
  

function start_page()
{   
    //document.getElementById("search_user_form").focus();
}

function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
            
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}

function calculate()
{
    try{
        var diff;
        var total_physicalquantity;
        var total_dataquantity;
        var total_diff;
        
        jml_data      = reform(document.getElementById("jml_data").value)*1;
        v_stocktypeid = reform(document.getElementById("v_stocktypeid").value)*1;
        
        diff = 0;
        total_physicalquantity = 0;
        total_dataquantity = 0;
        total_diff = 0;
        for(i=1;i<=jml_data;i++)
        {
            physicalquantity = reform(document.getElementById("v_physicalquantity_"+i).value)*1;
            dataquantity     = reform(document.getElementById("v_dataquantity_"+i).value)*1;
            
            //if(i==2)
            //{
               // alert(physicalquantity);
               // alert(dataquantity);
            //}
            
            diff = (physicalquantity*1) - (dataquantity*1);
            
            //if(i==2)
            //{
             //   alert(diff);
            //}
            
            if(diff*1!=0)
            {
                if(v_stocktypeid==1)
                {
                    document.getElementById("td_diff_"+i).innerHTML = format6(diff);
                }
                else
                {
                    document.getElementById("td_diff_"+i).innerHTML = format(diff);
                }
            }
            else
            {
                document.getElementById("td_diff_"+i).innerHTML = "&nbsp;";
            }
            
            total_physicalquantity += (physicalquantity*1);
            total_dataquantity += (dataquantity*1);
            total_diff += (diff*1);
        }
        
        if(v_stocktypeid==1)
        {
            document.getElementById("total_physicalquantity").innerHTML = format6(total_physicalquantity);
            document.getElementById("total_dataquantity").innerHTML = format6(total_dataquantity);
            document.getElementById("total_diff").innerHTML = format6(total_diff);
        }
        else
        {
            document.getElementById("total_physicalquantity").innerHTML = format(total_physicalquantity);
            document.getElementById("total_dataquantity").innerHTML = format(total_dataquantity);
            document.getElementById("total_diff").innerHTML = format(total_diff);
        }
    
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description Calculate : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    } 
}


</script> 
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-10">
				Period&nbsp;
		        <select name="search_month" id="search_month" class="form-control-new">
		            <option value="">All</option>
		            <?php 
		                foreach($arr_data["list_month"] as $month => $val)
		                {
		                    $selected = "";
		                    if(sprintf("%02s",$month)==date("m"))
		                    {
		                        $selected = "selected='selected'";
		                    }
		            ?>
		                    <option <?php echo $selected; ?> value="<?php echo sprintf("%02s",$month); ?>"><?php echo $arr_data["list_month"][$month]; ?></option>
		            <?php        
		                }
		            ?>        
		        </select>
	        	&nbsp;
		        <select name="search_year" id="search_year" class="form-control-new">
		            <option value="">All</option>
		            <?php 
		                foreach($arr_data["list_year"] as $year => $val)
		                {
		                    $selected = "";
		                    if($year==date("Y"))
		                    {
		                        $selected = "selected='selected'";
		                    }
		            ?>
		                    <option <?php echo $selected; ?> value="<?php echo $year; ?>"><?php echo $arr_data["list_year"][$year]; ?></option>
		            <?php        
		                }
		            ?>            
		        </select>
	        	&nbsp;
		        Warehouse
		        <select name="search_warehouse" id="search_warehouse" class="form-control-new">
		            
		            <?php 
		                if($jml_warehouse==count($arr_data["warehouseadmin"]))
		                {
		            ?>
		            <option value="">All</option>
		            <?php 
		                }
		            ?>
		            
		            <?php 
		                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
		                {
		                    if($arr_data["warehouseadmin"][$warehousecode]==$warehousecode)
		                    {
		                        $warehousename = $arr_data["warehousename"][$warehousecode];
		    					?>
		                        <option value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
		    					<?php                
		                    }    
		                }
		            ?>    
		        </select> 
	        </div>
	        
			<div class="col-md-2" align="right">
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('search'),show_loading_bar(100)">Search<i class="entypo-search"></i></button>
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('add_data'),show_loading_bar(100)">Add Data<i class="entypo-plus" ></i></button>
	       	</div>
	       	
       	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
		
<?php include("footer.php"); ?>