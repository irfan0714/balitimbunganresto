<?php
    include("header.php");
    
	$modul        = "Internal Mutation";
	$file_current = "npm_internal_mutation.php";
	$file_name    = "npm_internal_mutation_data.php";

	// cek warehouseadmin
	$q = "
	        SELECT
	            warehouseadmin.warehousecode
	        FROM
	            warehouseadmin
	        WHERE
	            1
	            AND warehouseadmin.userid = '".$ses_login."'
	";
	$qry_warehouseadmin = mysql_query($q);
	while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
	{
	    list($warehousecode) = $row_warehouseadmin;
	    
	    $arr_data["warehouseadmin"][$warehousecode] = $warehousecode;
	}

	$v_from_date = "01-".date("m-Y");
	$v_to_date   = date("t", parsedate($v_from_date, "-"))."-".date("m-Y");

	$q = "
	        SELECT
	            intmutpurpose.purposeid,
	            intmutpurpose.purpose
	        FROM
	            intmutpurpose
	        WHERE
	            1
	        ORDER BY
	            intmutpurpose.purpose ASC
	";
	$qry = mysql_query($q);
	while($row = mysql_fetch_array($qry))
	{
	    list($purposeid, $purpose) = $row;
	    
	    $arr_data["list_purpose"][$purposeid] = $purposeid;
	    
	    $arr_data["purpose"][$purposeid] = $purpose;
	}

	$jml_warehouse = 0;
	$q = "
	        SELECT
	            warehouse.warehousecode,
	            warehouse.warehousename
	        FROM
	            warehouse
	        WHERE
	            1
	        ORDER BY
	            warehouse.warehousecode ASC
	";
	$qry = mysql_query($q);
	while($row = mysql_fetch_array($qry))
	{
	    list($warehousecode, $warehousename) = $row;
	    
	    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
	    
	    $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
	    
	    $jml_warehouse++;
	} 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<html>
    <head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="description" content="Neon Admin Panel" />
	    <meta name="author" content="" />

	    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
	    <link rel="shortcut icon" href="public/images/Logosg.png" >
        <link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="../assets/css/NotoSans.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/neon-core.css">
        <link rel="stylesheet" href="../assets/css/neon-theme.css">
        <link rel="stylesheet" href="../assets/css/neon-forms.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/skins/black.css">
        <link rel="stylesheet" href="public/css/style.css">
        <link rel="stylesheet" href="../assets/css/my.css">

        <script src="../assets/js/jquery-1.11.0.min.js"></script>

	    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<script>

function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
{
    var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    //alert('aaaa');

    newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth + 
        ',height=' + windowHeight + 
        ',left=' + centerWidth + 
        ',top=' + centerHeight
        );

    newWindow.focus();
    return newWindow.name;
}

function pop_up_details()
{
    windowOpener('600', '800', 'Pop Up Details', 'npm_internal_mutation_pop_up_details.php', 'Pop Up Details');
}

function num_format(obj) {
    obj.value = new NumberFormat(obj.value).toFormatted();
}
  
function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

var xmlhttp = createRequestObject();

function CallAjax(tipenya,param1,param2,param3,param4,param5)
{
    try{    
        if (!tipenya) return false;
        //document.getElementById("loading").style.display='block';
        
        if (param1 == undefined) param1 = '';
        if (param2 == undefined) param2 = '';
        if (param3 == undefined) param3 = '';
        if (param4 == undefined) param4 = '';
        if (param5 == undefined) param5 = '';
        
        var variabel;
        variabel = "";
        
        if(tipenya=='search')
        {  
            document.getElementById("col-header").innerHTML = "";
            
            search_by = document.getElementById("search_by").value;
            v_type_date = document.getElementById("v_type_date").value;
            v_from_date = document.getElementById("v_from_date").value;
            v_to_date = document.getElementById("v_to_date").value;            
            search_intmutpurpose = document.getElementById("search_intmutpurpose").value;
            search_warehouse = document.getElementById("search_warehouse").value;
            search_mutation_no = document.getElementById("search_mutation_no").value;
            search_reference_no = document.getElementById("search_reference_no").value;

            variabel += "&search_by="+search_by;
            variabel += "&v_type_date="+v_type_date;
            variabel += "&v_from_date="+v_from_date;
            variabel += "&v_to_date="+v_to_date;
            variabel += "&search_intmutpurpose="+search_intmutpurpose;
            variabel += "&search_warehouse="+search_warehouse;
            variabel += "&search_mutation_no="+search_mutation_no;
            variabel += "&search_reference_no="+search_reference_no;
            
            variabel += "&v_imno_curr="+param1;
            
            if(search_by=="imno" && search_mutation_no=="")
            {
                document.getElementById("search_mutation_no").focus();
                alert('Keyword Mutation No harus diisi');
                return false;
            }
            else if(search_by=="mrno" && search_reference_no=="")
            {
                document.getElementById("search_reference_no").focus();
                alert('Keyword Reference No harus diisi');
                return false;
            }
            
            //alert(variabel);
            document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            document.getElementById("col-header").innerHTML = '';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-search").innerHTML     = xmlhttp.responseText;
                    
                    if(param1)
                    {
                        CallAjax('edit_data',param1);
                    }
                }

                return false;
            }
            xmlhttp.send(null);
        } 
        else if(tipenya=='add_data')
        {  

            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        
        else if(tipenya=='edit_data')
        {  
            variabel += "&v_imno="+param1;
            
            //alert(variabel);
            document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
            
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);
        }
        else if(tipenya=='ajax_batchnumber')
        {
            variabel += "&v_batchnumber="+param1;
            
            //alert(variabel);
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("show_formulaname_"+param2).innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);    
        }
        else if(tipenya=='ajax_matreqnumber')
        {
            variabel += "&v_matreqnumber="+param1;
            
            //alert(variabel);
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("show_formulaname_"+param2).innerHTML     = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);    
        }
        
        else if(tipenya=='cek_matreq_duplicate')
        {
            variabel += "&v_matreqnumber="+param1;
            
            //alert(variabel);
            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
            xmlhttp.onreadystatechange = function() 
            {
                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                {    
                    document.getElementById("td_info_matreq").innerHTML = xmlhttp.responseText;
                }

                return false;
            }
            xmlhttp.send(null);    
        }
    }
    catch(err)
    {
        txt  = "There was an error on this page.\n\n";
        txt += "Error description : "+ err.message +"\n\n";
        txt += "Click OK to continue\n\n";
        alert(txt);
    } 
}

function confirm_delete(user_form)
{
    var r = confirm("Anda yakin ingin VOID "+user_form+" ?");
    if(r){
        document.getElementById("v_del").value = '1';
        document.getElementById("theform").submit();
    }
}

function confirm_undelete(user_form)
{
    var r = confirm("Anda yakin ingin UnVOID "+user_form+" ?");
    if(r){
        document.getElementById("v_undel").value = '1';
        document.getElementById("theform").submit();
    }
}

function CheckAll(param, target){
    var field = document.getElementsByName(target);
    var chkall = document.getElementById(param);
    if (chkall.checked == true){
        for (i = 0; i < field.length; i++)
            field[i].checked = true ;
    }else{
        for (i = 0; i < field.length; i++)
            field[i].checked = false ;
    }        
}

function reform(val)
{    
     var a = val.split(",");
    var b = a.join("");
    //alert(b);
    return b;
}

function format(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(0);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}

function format4(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(4);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}

function format6(harga)
{
 harga=parseFloat(harga);
 harga=harga.toFixed(6);
 //alert(harga);
 s = addSeparatorsNF(harga, '.', '.', ',');
 return s; 
}
 
function addSeparatorsNF(nStr, inD, outD, sep)
{
 nStr += '';
 var dpos = nStr.indexOf(inD);
 var nStrEnd = '';
 if (dpos != -1) {
  nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
  nStr = nStr.substring(0, dpos);
 }
 var rgx = /(\d+)(\d{3})/;
 while (rgx.test(nStr)) {
  nStr = nStr.replace(rgx, '$1' + sep + '$2');
 }
 return nStr + nStrEnd;
}

function toFormat(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format(document.getElementById(id).value);
}
function toFormat4(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format4(document.getElementById(id).value);
}
function toFormat6(id)
{
    //alert(document.getElementById(id).value);
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        //alert("That's not a number.")
        document.getElementById(id).value=0;
        //document.getElementById(id).focus();
    }
    document.getElementById(id).value=reform(document.getElementById(id).value);
    document.getElementById(id).value=format6(document.getElementById(id).value);
}
function isanumber(id)
{
    if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
    {
        document.getElementById(id).value=0;
    }
    else
    {
        document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
    }
}
  

function start_page()
{   
    //document.getElementById("search_user_form").focus();
}

function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
            
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}                                                                         

function pop_up_csv(v_imno)
{
    windowOpener('300', '400', 'Pop Up CSV', 'npm_internal_mutation_pop_up_csv.php?v_imno='+v_imno, 'Pop Up CSV');
}

function change_search_by(nilai)
{
    if(nilai=="data" || nilai=="moving_confirmation")
    {
        document.getElementById('td_data').style.display                = '';    
        document.getElementById('td_mutation_no').style.display         = 'none';    
        document.getElementById('td_reference_no').style.display        = 'none';    
    }
    else if(nilai=="imno")
    {
        document.getElementById('td_data').style.display                = 'none';    
        document.getElementById('td_mutation_no').style.display         = '';    
        document.getElementById('td_reference_no').style.display        = 'none';    
        
        document.getElementById('search_mutation_no').focus();        
    }
    else if(nilai=="mrno")
    {
        document.getElementById('td_data').style.display                = 'none';    
        document.getElementById('td_mutation_no').style.display         = 'none';    
        document.getElementById('td_reference_no').style.display        = '';    
        
        document.getElementById('search_reference_no').focus();    
    }
    
} 

function change_type_date(nilai)
{
    if(nilai=="today")
    {
        document.getElementById('td_range_date').style.display         = 'none';    
    }
    else if(nilai=="7_day_before")
    {
        document.getElementById('td_range_date').style.display         = 'none';    
    }
    else if(nilai=="range_date")
    {
        document.getElementById('td_range_date').style.display         = '';    
    }
}

function change_muttype(nilai)
{
    if(nilai=="mut")
    {
        document.getElementById('v_fromwarehouse').style.display   = '';    
        document.getElementById('v_towarehouse').style.display     = '';    
    }
    else if(nilai=="in")
    {
        document.getElementById('v_fromwarehouse').style.display   = 'none';    
        document.getElementById('v_towarehouse').style.display     = '';    
    }
    else if(nilai=="out")
    {
        document.getElementById('v_fromwarehouse').style.display   = '';    
        document.getElementById('v_towarehouse').style.display     = 'none';    
    }
    else
    {
        document.getElementById('v_fromwarehouse').style.display   = 'none';    
        document.getElementById('v_towarehouse').style.display     = 'none';    
    }
    
}
</script> 

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
        
        <div class="col-md-10">
	        Search By&nbsp;
	        <select name="search_by" id="search_by" class="form-control-new" onchange="change_search_by(this.value)" style="width:100px;">
	            <option value="data">Data</option>
	            <option value="imno">Mutation No</option>
	            <option value="mrno">Reference No</option>
	            <option value="moving_confirmation">Moving Confirmation</option>
	        </select>
	        &nbsp;
	        <span id="td_data" style="display:">
	        Type Date&nbsp;
	        <select name="v_type_date" id="v_type_date" class="form-control-new" onchange="change_type_date(this.value)">
	            <option value="today">Today</option>
	            <option value="7_day_before">7 day Before</option>
	            <option value="range_date">Range Date</option>
	        </select>
	        
	        <span id="td_range_date" style="display:none">
	        <input type="text" class="form-control-new datepicker" name="v_from_date" id="v_from_date" size="10" maxlength="10" value="<?php echo $v_from_date; ?>">
	        &nbsp;
	        up to
	        &nbsp;
	        <input type="text" class="form-control-new datepicker" name="v_to_date" id="v_to_date" size="10" maxlength="10" value="<?php echo $v_to_date; ?>">
	        &nbsp;
	        </span>
	        &nbsp;Purpose&nbsp;
	        <select name="search_intmutpurpose" id="search_intmutpurpose" class="form-control-new">
	            <option value="">All</option>
	            <?php 
	                foreach($arr_data["list_purpose"] as $purposeid => $val)
	                {
	                    $purpose = $arr_data["purpose"][$purposeid];
				?>
	                    <option value="<?php echo $purposeid; ?>"><?php echo $purpose; ?></option>
				<?php                    
	                }               
	            ?>    
	        </select>
	        &nbsp;Warehouse&nbsp;
	        <select name="search_warehouse" id="search_warehouse" class="form-control-new">
	            <?php 
	                if($jml_warehouse==count($arr_data["warehouseadmin"]))
	                {
	            ?>
	            <option value="">All</option>
	            <?php 
	                }
	                
	                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
	                {
	                    if($arr_data["warehouseadmin"][$warehousecode]==$warehousecode)
	                    {
	                        $warehousename = $arr_data["warehousename"][$warehousecode];
	    		?>
	                        <option value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
	    		<?php                
	                    }    
	                }
	            ?>    
	        </select>
	        &nbsp;
	        </span>
	        
	        <span id="td_mutation_no" style="display:none;">
	            <input type="text" class="form-control-new" size="30" maxlength="30" name="search_mutation_no" id="search_mutation_no">
	        </span>
	        
	        <span id="td_reference_no" style="display:none;">
	            <input type="text" class="form-control-new" size="30" maxlength="30" name="search_reference_no" id="search_reference_no">
	        </span>
        </div>
        
		<div class="col-md-2" align="right">
       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('search'),show_loading_bar(100)">Search<i class="entypo-search"></i></button>
       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('add_data'),show_loading_bar(100)">Add Data<i class="entypo-plus" ></i></button>
       	</div>
       	
       	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
		
<?php 
	include("footer.php");
?>