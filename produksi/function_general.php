<?php
ini_set('display_errors', false);
ini_set('html_errors', false);

session_start();

function get_counter_int($db_name,$tbl_name,$pk,$limit=100)
{
  $q = "
            SELECT
                ".$tbl_name.".".$pk."
            FROM
                ".$tbl_name."
            ORDER BY
                ".$tbl_name.".".$pk."*1 DESC
            LIMIT 0,".$limit."
    ";
    $qry_id = mysql_query($q);
    $jml_data = mysql_num_rows($qry_id);
    
    $no = true;
    while($r_id = mysql_fetch_object($qry_id))
    {
        if($no)
        {
            $id = $r_id->$pk;
            $no = false;
        }
        
        $arr_data["list_id"][$r_id->$pk] = $r_id->$pk;
    }
    
    $prev_id = 0;
    
    if($jml_data!=0)
    {
        foreach($arr_data["list_id"] as $list_id=>$val)
        {
            if($prev_id*1!=0)
            {
                if( ($prev_id-1) != $val)
                {
                    $id = $val;
                    break;
                }
            }
            $prev_id = $val;
        }
    }
    else
    {
        $id = 0;
    }
    $id = ($id*1) + 1;
    
    return $id;
} 

function karakter_spesial($data, $type="input")
{
    global $db;
    
    unset($arr_data);
    $arr_data[0] = $data;
    
    if($type=="input")
    {
        $q = "
                SELECT
                    *
                FROM
                    ".$db["master"].".karakter_spesial
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".karakter_spesial.sid ASC
        ";
        $qry = mysql_query($q);
        
        $i = 1;
        while($row = mysql_fetch_array($qry))
        {
            $curr = $i-1;
            $arr_data[$i] = str_replace($row["input"], $row["output"], $arr_data[$curr]);  
            
            $i++;
        }
        
        $i--;
    }
    else if($type=="output")
    {
        $q = "
                SELECT
                    *
                FROM
                    ".$db["master"].".karakter_spesial
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".karakter_spesial.sid ASC
        ";
        $qry = mysql_query($q);
        
        $i = 1;
        while($row = mysql_fetch_array($qry))
        {
            $curr = $i-1;
            $arr_data[$i] = str_replace($row["output"], $row["input"], $arr_data[$curr]);  
            
            $i++;
        }
        
        $i--;    
    }
    
    return $arr_data[$i];
    
}

function call_acc_locked($user_form, $start_date, $to_date)
{
    $q = "
            SELECT
                uni.acc_locked_date,
                uni.user_id,
                uni.edited_date,
                uni.locked
            FROM
            (
                SELECT
                    vci.acc_locked.acc_locked_date,
                    vci.acc_locked.user_id,
                    vci.acc_locked.edited_date,
                    vci.acc_locked.locked
                FROM
                    vci.acc_locked
                WHERE
                    1
                    AND vci.acc_locked.user_form = '".$user_form."'
                    AND vci.acc_locked.acc_locked_date BETWEEN '".$start_date."' AND '".$to_date."' 
                ORDER BY
                    vci.acc_locked.edited_date DESC
            )AS uni
            GROUP BY
                uni.acc_locked_date
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($acc_locked_date, $user_id, $edited_date, $locked) = $row;
        
        $arr_curr["locked"][$acc_locked_date] = $locked; 
        $arr_curr["edited_by"][$acc_locked_date] = $edited_date." ".$user_id;
    }
    
    return $arr_curr; 
}

function search_keyword($v_keyword, $arr_keyword)
{
    $exp_keyword = explode(" ",$v_keyword);
    $jml_keyword = count($exp_keyword)-1;
    $jml_kolom   = count($arr_keyword);
    
    $where_keyword = "";
    for($key=0;$key<=$jml_keyword;$key++)
    {
        $i = 0;
        $where_keyword .= " AND ( ";
        foreach($arr_keyword as $val)
        {
            $where_keyword .= $val." LIKE '%".$exp_keyword[$key]."%' OR ";
            $i++;
            
            if($i==$jml_kolom)
            {
                $where_keyword = substr($where_keyword,0,-3);
            }
        }
        $where_keyword .= " ) ";
    }
    $where_keyword  = substr($where_keyword,0,-4);                                              
    

    
    $where_keyword .= ")";

    //echo $where_keyword;
    return $where_keyword;
}  

function where_array($list_array, $kolom, $type="in")
{
    $where = "";
    if($type=="in")
    {
        foreach($list_array as $key=>$val)
        if($where == "")
        {
            $where .= " AND (".$kolom." = '".$val."' ";
        }    
        else
        {
            $where .= " OR ".$kolom." = '".$val."' ";    
        }
        
        if($where)
        {
            $where .= ")";
        }
    }
    else if($type=="not in")
    {
        foreach($list_array as $key=>$val)
        if($where == "")
        {
            $where .= " AND (".$kolom." != '".$val."' ";
        }    
        else
        {
            $where .= " OR ".$kolom." != '".$val."' ";
        }
        
        if($where)
        {
            $where .= ")";
        }    
    }
    
    return $where;
}    

function format_show_datetime($nilai)
{
    $StartDate = "";
    if($nilai)
    {
        
        $arr_date = explode(" ", $nilai);
        
        $StartDate = format_show_date($arr_date[0])." ".$arr_date[1];
    }
    
    return $StartDate;
}

function save_char($char)
{
    $char1 = '"';
    $return = trim($char);
    
    return $return;
}

function save_int($int, $type="")
{
    if($type=="")
    {
        $first  = trim(str_replace("`", "", $int));
        $second = str_replace(",", "", $first);
        
        $return = $second;    
    }
    else if($type=="ind")
    {
        $first  = trim(str_replace("`", "", $int));
        $second = str_replace(".", "", $first);
        $third  = str_replace(",", ".", $second);
        
        $return = $third;
    }

    return $return;
}

function format_save_date($nilai)
{
    $StartDate = "";
    if($nilai)
    {
        $exp_StartDate = explode("-",$nilai);
        $StartDate     = $exp_StartDate[2]."-".$exp_StartDate[1]."-".$exp_StartDate[0];    
    }
    
    return $StartDate;
}

function format_show_date($nilai)
{
    $StartDate = "";
    if($nilai)
    {
        $exp_StartDate = explode("-",$nilai);
        $StartDate     = $exp_StartDate[2]."-".$exp_StartDate[1]."-".$exp_StartDate[0];    
    }
    
    return $StartDate;
} 

function format_number($nilai,$decimal=0,$type="")
{
    if($type=="")
    {
        if($nilai*1==0)
        {
            //$return = "&nbsp;";
            $return = number_format(0,$decimal,".",",");
            $return = "";
        }
        else
        {
            if($nilai*1<0)
            {
                //$return = "<font color='red'>".number_format($nilai,$decimal,".",",")."</font>";
                $return = number_format($nilai,$decimal,".",",");
            }
            else
            {
                $return = number_format($nilai,$decimal,".",",");
            }
        }
    }
    else if($type=="ind")
    {
        if($nilai*1==0)
        {
            //$return = "&nbsp;";
            $return = number_format(0,$decimal,",",".");
            $return = "";
        }
        else
        {
            $return = number_format($nilai,$decimal,",",".");
        }    
    }
    else if($type=="cetak")
    {
        if($nilai*1==0)
        {
            //$return = "&nbsp;";
            $return = number_format(0,$decimal,".",",");
        }
        else
        {
            $return = number_format($nilai,$decimal,".",",");
        }
    }
    return $return;
}  

function parsedate($str, $char="/") 
{
    $return = "";
    
    if($str!="")
    {
        $exp_str = explode($char,$str);
        
        $dd = substr($str, 0,2);
        $mm = substr($str, 3,2);
        $yyyy = substr($str, 6,4);
    
        $return = mktime(0,0,0,$mm, $dd, $yyyy);
        return $return;
    }
    
    return $return;
} 

function bincard_wip_stock($arr_batchnumber,$date="")
{
	unset($arr_data);
	
	$where_date="";
	if($date)
	{
		$where_date = " AND bincard_wip.carddate <= '".$date."'";	
	}
	
	$where_batchnumber = where_array($arr_batchnumber, "bincard_wip.batchnumber", "in");
	
    $q = "
    	SELECT 
	      bincard_wip.batchnumber,
	  	  bincard_wip.warehousecode,
		  SUM(bincard_wip.qin) AS sum_qin,
		  SUM(bincard_wip.qout) AS sum_out 
		FROM
		  bincard_wip 
		WHERE 1 
		  ".$where_date."
		  ".$where_batchnumber."
		GROUP BY 
		  bincard_wip.batchnumber
    ";
    //echo $q;
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
		list($batchnumber,$warehousecode,$sum_qin,$sum_out)=$row;
		
		$arr_data["sum_qin"][$batchnumber]= ($sum_qin);
		$arr_data["sum_out"][$batchnumber]= ($sum_out);
		
		$arr_data["sum_akhir"][$batchnumber]= $sum_qin-$sum_out;
		
	}   
	
	return $arr_data;
        
}          

function bincard_stock($month, $year, $search_inventorycode="All", $search_warehousecode="All")
{
    //echo $month.", ".$year.", ".$inventorycode.", ".$search_warehousecode.", ".$search_stocktypeid;
    
    $date_awal  = $year."-".sprintf("%02s",$month)."-01";
    $date_akhir = $year."-".sprintf("%02s",$month)."-".date('t',mktime(0,0,0,sprintf("%02s",$month),1,$year));
    
    $mm   = $month;
    $yyyy = $year;
    
    $where_pcode = where_array($search_inventorycode, "PCode", "in");
    
    $q = "
            SELECT
                stock.PCode,
                stock.KdGudang,
                stock.GAkhir".$mm." AS akhir,
            FROM
                stock
            WHERE
                1
                ".$where_pcode."
                AND stock.Tahun = '".$yyyy."'
                AND stock.KdGudang = '".$search_warehousecode."'
            ORDER BY
                stock.PCode
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($PCode, $KdGudang, $akhir) = $row;
        
        $arr_data["akhir"][$PCode] = $akhir;
    }
   
    
    return $arr_data;    
}

?>