<?php
    include("header.php");
   
   $modul        = "Material Request";
   $file_current = "npm_matreq.php";
   $file_name    = "npm_matreq_data.php";

   $arr_data["month"][1] = "January";
   $arr_data["month"][2] = "February";
   $arr_data["month"][3] = "March";
   $arr_data["month"][4] = "April";
   $arr_data["month"][5] = "May";
   $arr_data["month"][6] = "June";
   $arr_data["month"][7] = "July";
   $arr_data["month"][8] = "August";
   $arr_data["month"][9] = "September";
   $arr_data["month"][10] = "October";
   $arr_data["month"][11] = "November";
   $arr_data["month"][12] = "December";
   
    $year_end   = date("Y");
    $year_start = date("Y")-3;

   if(!isset($_GET["id"])){ $id = isset($_GET["id"]); } else { $id = $_GET["id"]; }

   $v_from_date = "01-".date("m-Y");
   $v_to_date   = date("t", parsedate($v_from_date, "-"))."-".date("m-Y");
   
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
        <link rel="stylesheet" href="../assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="../assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="../assets/css/NotoSans.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/neon-core.css">
        <link rel="stylesheet" href="../assets/css/neon-theme.css">
        <link rel="stylesheet" href="../assets/css/neon-forms.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/skins/black.css">
        <link rel="stylesheet" href="public/css/style.css">
        <link rel="stylesheet" href="../assets/css/my.css">

        <script src="../assets/js/jquery-1.11.0.min.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

   <script>

   function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
   {
       var centerWidth = (window.screen.width - windowWidth) / 2;
       var centerHeight = (window.screen.height - windowHeight) / 2;
       //alert('aaaa');

       newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth +
           ',height=' + windowHeight +
           ',left=' + centerWidth +
           ',top=' + centerHeight
           );

       newWindow.focus();
       return newWindow.name;
   }

   function pop_up_details()
   {
       windowOpener('600', '800', 'Pop Up Details', 'vci_internal_mutation_pop_up_details.php', 'Pop Up Details');
   }

   function num_format(obj) {
       obj.value = new NumberFormat(obj.value).toFormatted();
   }
     
   function createRequestObject() {
       var ro;
       var browser = navigator.appName;
       if(browser == "Microsoft Internet Explorer"){
           ro = new ActiveXObject("Microsoft.XMLHTTP");
       }else{
           ro = new XMLHttpRequest();
       }
       return ro;
   }

   var xmlhttp = createRequestObject();

   function CallAjax(tipenya,param1,param2,param3,param4,param5)
   {
       try{    
           if (!tipenya) return false;
           //document.getElementById("loading").style.display='block';
           
           if (param1 == undefined) param1 = '';
           if (param2 == undefined) param2 = '';
           if (param3 == undefined) param3 = '';
           if (param4 == undefined) param4 = '';
           if (param5 == undefined) param5 = '';
           
           var variabel;
           variabel = "";
           
           if(tipenya=='search')
           {  
               search_by = document.getElementById("search_by").value;
               v_type_date = document.getElementById("v_type_date").value;
               v_from_date = document.getElementById("v_from_date").value;
               v_to_date = document.getElementById("v_to_date").value;          
               search_reqtype = document.getElementById("search_reqtype").value;
               search_keyword = document.getElementById("search_keyword").value;
               search_matreqnumber = document.getElementById("search_matreqnumber").value;
               search_formulanumber = document.getElementById("search_formulanumber").value;
               search_inventorycode = document.getElementById("search_inventorycode").value;
               search_prodplan = document.getElementById("search_prodplan").value;

               variabel += "&search_by="+search_by;
               variabel += "&v_type_date="+v_type_date;
               variabel += "&v_from_date="+v_from_date;
               variabel += "&v_to_date="+v_to_date;
               variabel += "&search_reqtype="+search_reqtype;
               variabel += "&search_keyword="+search_keyword;
               variabel += "&search_matreqnumber="+search_matreqnumber;
               variabel += "&search_formulanumber="+search_formulanumber;
               variabel += "&search_inventorycode="+search_inventorycode;
               variabel += "&search_prodplan="+search_prodplan;
               
               variabel += "&v_matreqnumber_curr="+param1;
               
               document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
               document.getElementById("col-header").innerHTML = "";
               
               xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
               xmlhttp.onreadystatechange = function()
               {
                   if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                   {    
                       document.getElementById("col-search").innerHTML     = xmlhttp.responseText;
                       
                       if(param1)
                       {
                           CallAjax('edit_data',param1);
                       }
                   }

                   return false;
               }
               xmlhttp.send(null);
           }
           else if(tipenya=='add_data')
           {  
               document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
               
               xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
               xmlhttp.onreadystatechange = function()
               {
                   if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                   {    
                       document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                   }

                   return false;
               }
               xmlhttp.send(null);
           }
           
           else if(tipenya=='edit_data')
           {  
               variabel += "&v_matreqnumber="+param1;
               
               //alert(variabel);
               document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
               
               xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
               xmlhttp.onreadystatechange = function()
               {
                   if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                   {    
                       document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
                   }

                   return false;
               }
               xmlhttp.send(null);
           }
           
           else if(tipenya=='ajax_list_pcode')
           {  
               xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
               xmlhttp.onreadystatechange = function()
               {
                   if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                   {    
                       document.getElementById("select_pcode").innerHTML   = xmlhttp.responseText;
                   }

                   return false;
               }
               xmlhttp.send(null);
           }

           else if(tipenya=='choose_prodplancode')
           {  
               variabel += "&v_prodplancode="+param1;
               
               xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
               xmlhttp.onreadystatechange = function()
               {
                   if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                   {    
                       document.getElementById("select_pcode").innerHTML   = xmlhttp.responseText;
                   }

                   return false;
               }
               xmlhttp.send(null);
           }
           
           else if(tipenya=='ajax_keyword_pcode')
           {  
               v_matreq_type = document.getElementById("v_matreq_type").value;
               v_prodplancode = document.getElementById("v_prodplancode").value;
               
               variabel += "&v_keyword_pcode="+param1;
               variabel += "&v_matreq_type="+v_matreq_type;
               variabel += "&v_prodplancode="+v_prodplancode;
               
               if(v_matreq_type=="Reguler" && v_prodplancode=="")
               {
                   alert("Prod Plan Code Harus dipilih...");
                   document.getElementById('loading').style.display        = 'none';
                   return false;
               }
               else
               {
                   xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                   xmlhttp.onreadystatechange = function()
                   {
                       if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                       {    
                           document.getElementById("select_pcode").innerHTML   = xmlhttp.responseText;
                       }

                       return false;
                   }
                   xmlhttp.send(null);
               }
           }
           
           else if(tipenya=='choose_pcode')
           {  
               v_inventorycode = document.getElementById("v_inventorycode").value;
               v_matreq_type = document.getElementById("v_matreq_type").value;
               v_prodplancode = document.getElementById("v_prodplancode").value;
               
               variabel += "&v_inventorycode="+v_inventorycode;
               variabel += "&v_matreq_type="+v_matreq_type;
               variabel += "&v_prodplancode="+v_prodplancode;
               
               if(v_inventorycode=="")
               {
                   alert("PCode Harus dipilih...");
                   return false;    
               }
               else
               {
                   xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                   xmlhttp.onreadystatechange = function()
                   {
                       if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                       {    
                           //alert(xmlhttp.responseText);
                           arr_data = xmlhttp.responseText.split("||");
                           //alert(arr_data[0]);
                           
                           document.getElementById("td_form_formulanumber").innerHTML   = arr_data[0];
                           document.getElementById("v_quantity").value             = arr_data[1];
                           document.getElementById("td_qty_kg").innerHTML          = arr_data[2];
                           document.getElementById("td_packagingsize").innerHTML   = arr_data[3];
                           
                           document.getElementById("ori_qty_kg").value          = arr_data[4];
                           document.getElementById("ori_packagingsize").value   = arr_data[5];
                       }

                       return false;
                   }
                   xmlhttp.send(null);
               }
           }
		   
		   else if(tipenya=='otomatis')
           {  
			   v_pecah = document.getElementById("v_formulanumber").value;
			   arr_data = v_pecah.split("-");
			   v_formula = arr_data[0];
			   v_prodplandetailid = arr_data[1];
               v_inventorycode = document.getElementById("v_inventorycode").value;
               v_matreq_type = document.getElementById("v_matreq_type").value;
               v_prodplancode = document.getElementById("v_prodplancode").value;
			   
               
               
               variabel += "&v_inventorycode="+v_inventorycode;
               variabel += "&v_matreq_type="+v_matreq_type;
               variabel += "&v_prodplancode="+v_prodplancode;
			   variabel += "&v_formula="+v_formula;
			   variabel += "&v_prodplandetailid="+v_prodplandetailid;
			   
                   xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                   xmlhttp.onreadystatechange = function()
                   {
                       if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                       {  
                           arr_data = xmlhttp.responseText.split("||");
                           //alert(arr_data[0]);
                           
                           //document.getElementById("td_form_formulanumber").innerHTML   = arr_data[0];
                           document.getElementById("v_quantity").value             = arr_data[1];
                           document.getElementById("td_qty_kg").innerHTML          = arr_data[2];
                           document.getElementById("td_packagingsize").innerHTML   = arr_data[3];
                           
                           document.getElementById("ori_qty_kg").value          = arr_data[4];
                           document.getElementById("ori_packagingsize").value   = arr_data[5];
                       }

                       return false;
                   }
                   xmlhttp.send(null);
               
           }
		   
		   
       }
       catch(err)
       {
           txt  = "There was an error on this page.\n\n";
           txt += "Error description : "+ err.message +"\n\n";
           txt += "Click OK to continue\n\n";
           alert(txt);
       }
   }

   function confirm_delete(user_form,pbnumber)
   {
       var r = confirm("Anda yakin ingin VOID "+user_form+" ?");
       if(r){
           document.getElementById("v_del").value = '1';
           document.getElementById("theform").submit();
       }
   }

   function confirm_undelete(user_form)
   {
       var r = confirm("Anda yakin ingin UnVOID "+user_form+" ?");
       if(r){
           document.getElementById("v_undel").value = '1';
           document.getElementById("theform").submit();
       }
   }

   function CheckAll(param, target){
       var field = document.getElementsByName(target);
       var chkall = document.getElementById(param);
       if (chkall.checked == true){
           for (i = 0; i < field.length; i++)
               field[i].checked = true ;
       }else{
           for (i = 0; i < field.length; i++)
               field[i].checked = false ;
       }        
   }

   function reform(val)
   {    
        var a = val.split(",");
       var b = a.join("");
       //alert(b);
       return b;
   }

   function format(harga)
   {
    harga=parseFloat(harga);
    harga=harga.toFixed(0);
    //alert(harga);
    s = addSeparatorsNF(harga, '.', '.', ',');
    return s;
   }

   function format4(harga)
   {
    harga=parseFloat(harga);
    harga=harga.toFixed(4);
    //alert(harga);
    s = addSeparatorsNF(harga, '.', '.', ',');
    return s;
   }

   function format6(harga)
   {
    harga=parseFloat(harga);
    harga=harga.toFixed(6);
    //alert(harga);
    s = addSeparatorsNF(harga, '.', '.', ',');
    return s;
   }
   
   function addSeparatorsNF(nStr, inD, outD, sep)
   {
    nStr += '';
    var dpos = nStr.indexOf(inD);
    var nStrEnd = '';
    if (dpos != -1) {
     nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
     nStr = nStr.substring(0, dpos);
    }
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(nStr)) {
     nStr = nStr.replace(rgx, '$1' + sep + '$2');
    }
    return nStr + nStrEnd;
   }

   function toFormat(id)
   {
       //alert(document.getElementById(id).value);
       if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
       {
           //alert("That's not a number.")
           document.getElementById(id).value=0;
           //document.getElementById(id).focus();
       }
       document.getElementById(id).value=reform(document.getElementById(id).value);
       document.getElementById(id).value=format(document.getElementById(id).value);
   }
   function toFormat4(id)
   {
       //alert(document.getElementById(id).value);
       if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
       {
           //alert("That's not a number.")
           document.getElementById(id).value=0;
           //document.getElementById(id).focus();
       }
       document.getElementById(id).value=reform(document.getElementById(id).value);
       document.getElementById(id).value=format4(document.getElementById(id).value);
   }
   function toFormat6(id)
   {
       //alert(document.getElementById(id).value);
       if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
       {
           //alert("That's not a number.")
           document.getElementById(id).value=0;
           //document.getElementById(id).focus();
       }
       document.getElementById(id).value=reform(document.getElementById(id).value);
       document.getElementById(id).value=format6(document.getElementById(id).value);
   }
   function isanumber(id)
   {
       if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
       {
           document.getElementById(id).value=0;
       }
       else
       {
           document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
       }
   }
     

   function start_page()
   {  
       document.getElementById("search_keyword").focus();
   }

   function mouseover(target)
   {  
       if(target.bgColor!="#cafdb5"){        
           if (target.bgColor=='#ccccff')
               target.bgColor='#ccccff';
           else
               target.bgColor='#c1cdd8';
       }
   }
       
   function mouseout(target)
   {
       if(target.bgColor!="#cafdb5"){
           if (target.bgColor=='#ccccff')
               target.bgColor='#ccccff';
           else
               target.bgColor='#FFFFFF';
               
       }    
   }

   function mouseclick(target, idobject, num)
   {
                     
       //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
       for(i=0;i<num;i++){
           if (document.getElementById(idobject+'_'+i) != undefined){
               document.getElementById(idobject+'_'+i).bgColor='#f5faff';
               if (target.id == idobject+'_'+i)
                   target.bgColor='#ccccff';
           }
       }
   }

   function mouseclick1(target)
   {
       //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
       if(target.bgColor!="#cafdb5")
       {
           target.bgColor="#cafdb5";
       }
       else
       {
           target.bgColor="#FFFFFF";
       }
   }
     
   function change_search_by(nilai)
   {
       if(nilai=="data")
       {
           document.getElementById('td_data').style.display                 = '';    
           document.getElementById('td_matreqnumber').style.display         = 'none';    
           document.getElementById('td_formulanumber').style.display        = 'none';    
           document.getElementById('td_inventorycode').style.display        = 'none';    
           document.getElementById('td_prodplan').style.display             = 'none';        
       }
       else if(nilai=="matreqnumber")
       {
           document.getElementById('td_data').style.display                 = 'none';    
           document.getElementById('td_matreqnumber').style.display         = '';    
           document.getElementById('td_formulanumber').style.display        = 'none';    
           document.getElementById('td_inventorycode').style.display        = 'none';
           document.getElementById('td_prodplan').style.display             = 'none';                
           
           document.getElementById('search_matreqnumber').focus();        
       }
       else if(nilai=="formulanumber")
       {
           document.getElementById('td_data').style.display                 = 'none';    
           document.getElementById('td_matreqnumber').style.display         = 'none';    
           document.getElementById('td_formulanumber').style.display        = '';    
           document.getElementById('td_inventorycode').style.display        = 'none';
           document.getElementById('td_prodplan').style.display             = 'none';                
           
           document.getElementById('search_formulanumber').focus();        
       }
       else if(nilai=="inventorycode")
       {
           document.getElementById('td_data').style.display                 = 'none';    
           document.getElementById('td_matreqnumber').style.display         = 'none';    
           document.getElementById('td_formulanumber').style.display        = 'none';    
           document.getElementById('td_inventorycode').style.display        = '';        
           document.getElementById('td_prodplan').style.display             = 'none';        
           
           document.getElementById('search_inventorycode').focus();    
       }
       else if(nilai=="prodplan")
       {
           document.getElementById('td_data').style.display                 = 'none';    
           document.getElementById('td_matreqnumber').style.display         = 'none';    
           document.getElementById('td_formulanumber').style.display        = 'none';    
           document.getElementById('td_inventorycode').style.display        = 'none';        
           document.getElementById('td_prodplan').style.display             = '';        
           
           document.getElementById('search_prodplan').focus();    
       }
       
   }

   function change_type_date(nilai)
   {
       if(nilai=="today")
       {
           document.getElementById('td_range_date').style.display         = 'none';    
       }
       else if(nilai=="7_day_before")
       {
           document.getElementById('td_range_date').style.display         = 'none';    
       }
       else if(nilai=="range_date")
       {
           document.getElementById('td_range_date').style.display         = '';    
       }
   }

   function change_matreq_type(nilai)
   {
       if(nilai=="Reguler")
       {
           document.getElementById('tr_prod_plan_code').style.display    = '';    
           document.getElementById('tr_batch_qty').style.display         = '';    
           document.getElementById('tr_qty_kg').style.display            = '';    
           document.getElementById('tr_packagingsize').style.display     = '';
           document.getElementById('tr_matreqnumber_prev').style.display = 'none';
           document.getElementById('tr_kekurangan_supplier').style.display = 'none';
           
           document.getElementById("v_quantity").value             = "";
           document.getElementById("td_qty_kg").innerHTML          = "&nbsp;";
           document.getElementById("td_packagingsize").innerHTML   = "&nbsp;";
           document.getElementById("select_pcode").innerHTML   = '<select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:235px;"></select>';
           document.getElementById("td_form_formulanumber").innerHTML   = '<select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;"></select>';
       }
       else if(nilai=="Adjustment")
       {
           document.getElementById('tr_prod_plan_code').style.display    = 'none';    
           document.getElementById('tr_batch_qty').style.display         = 'none';    
           document.getElementById('tr_qty_kg').style.display            = 'none';    
           document.getElementById('tr_packagingsize').style.display     = 'none';
           document.getElementById('tr_matreqnumber_prev').style.display = '';
           document.getElementById('tr_kekurangan_supplier').style.display = '';    
           
           document.getElementById("td_form_formulanumber").innerHTML   = '<select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;"></select>';
           
           document.getElementById('v_matreqnumber_prev').focus();    
           CallAjax('ajax_list_pcode');
           
       }
       else if(nilai=="Rework")
       {
           document.getElementById('tr_prod_plan_code').style.display    = 'none';    
           document.getElementById('tr_batch_qty').style.display         = 'none';    
           document.getElementById('tr_qty_kg').style.display            = 'none';    
           document.getElementById('tr_packagingsize').style.display     = 'none';
           document.getElementById('tr_matreqnumber_prev').style.display = 'none';
           document.getElementById('tr_kekurangan_supplier').style.display = 'none';    
           
           document.getElementById("td_form_formulanumber").innerHTML   = '<select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;"></select>';
           
           document.getElementById('v_keyword_pcode').focus();    
           CallAjax('ajax_list_pcode');
       }
   }

   function change_reqtype(nilai)
   {
       v_matreq_type = document.getElementById("v_matreq_type").value;
       
       if(v_matreq_type=="Reguler")
       {
           if(nilai==0)
           {
               document.getElementById('tr_batch_qty').style.display         = '';    
               document.getElementById('tr_qty_kg').style.display            = '';    
               document.getElementById('tr_packagingsize').style.display     = '';        
           }
           else if(nilai==1)
           {
               document.getElementById('tr_batch_qty').style.display         = '';    
               document.getElementById('tr_qty_kg').style.display            = '';    
               document.getElementById('tr_packagingsize').style.display     = 'none';        
           }
           else if(nilai==2)
           {
               document.getElementById('tr_batch_qty').style.display         = '';    
               document.getElementById('tr_qty_kg').style.display            = 'none';    
               document.getElementById('tr_packagingsize').style.display     = '';        
           }
       }
       
       
   }

   function calculate()
   {
       try{
           v_quantity = document.getElementById('v_quantity').value;
           ori_qty_kg = document.getElementById('ori_qty_kg').value;
           ori_packagingsize = document.getElementById('ori_packagingsize').value;
           
           document.getElementById('td_qty_kg').innerHTML            = format4((v_quantity*ori_qty_kg));    
           document.getElementById('td_packagingsize').innerHTML     = format((v_quantity*ori_packagingsize));
       }
       catch(err)
       {
           txt  = "There was an error on this page.\n\n";
           txt += "Error description CALCULATE: "+ err.message +"\n\n";
           txt += "Click OK to continue\n\n";
           alert(txt);
       }    
   }

   function change_std()
   {
       try{
           var std;
           std = 0;
           
           v_jml_data = reform(document.getElementById('v_jml_data').value)*1;
           v_quantity = reform(document.getElementById('v_quantity').value)*1;
           
           for(i=1;i<=v_jml_data;i++)
           {
               v_inventorycode = document.getElementById('v_inventorycode_'+i).value;    
               v_stocktypeid   = document.getElementById('v_stocktypeid_'+i).value;    
               v_std_ori       = reform(document.getElementById('v_std_ori_'+i).value)*1;    
               
               std = v_quantity*v_std_ori;
               
               if(v_stocktypeid==1)
               {
                   document.getElementById('td_std_'+i).innerHTML = format6(std);
               }
               else
               {
                   document.getElementById('td_std_'+i).innerHTML = format(std);
               }
           }
           
       }
       catch(err)
       {
           txt  = "There was an error on this page.\n\n";
           txt += "Error description change_std: "+ err.message +"\n\n";
           txt += "Click OK to continue\n\n";
           alert(txt);
       }        
   }

   function copy_std_to_qty()
   {
       try{
           var std;
           std = 0;
           
           v_jml_data = reform(document.getElementById('v_jml_data').value)*1;
           v_quantity = reform(document.getElementById('v_quantity').value)*1;
           
           for(i=1;i<=v_jml_data;i++)
           {
               v_inventorycode = document.getElementById('v_inventorycode_'+i).value;    
               v_stocktypeid   = document.getElementById('v_stocktypeid_'+i).value;    
               v_std_ori       = reform(document.getElementById('v_std_ori_'+i).value)*1;    
               
               std = v_quantity*v_std_ori;
               
               if(v_stocktypeid==1)
               {
                   document.getElementById('v_quantity_'+i).value = format6(std);
               }
               else
               {
                   document.getElementById('v_quantity_'+i).value = format(std);
               }
           }
       }
       catch(err)
       {
           txt  = "There was an error on this page.\n\n";
           txt += "Error description copy_std_to_qty: "+ err.message +"\n\n";
           txt += "Click OK to continue\n\n";
           alert(txt);
       }          
   }
   </script>

<style>
    .title_table{
        background: #009490; color: white; font-weight: bold;
    }
</style> 
   
</head>


<body class="page-body skin-black" >

<div class="page-container sidebar-collapsed">
   
   <?php include("menu_kiri.php"); ?>
   
   <div class="main-content">
   
      <ol class="breadcrumb bc-3">
         <li>
            <a href="index.php">
               <i class="entypo-home"></i>Home
            </a>
         </li>
         <li>Production</li>
         <li class="active"><strong><?php echo $modul; ?></strong></li>
      </ol>
      
      <hr/>
      
      <div class="row">
      
      <iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
      
         <div class="col-md-10" style="color: black;">
            Search By&nbsp;
              <select name="search_by" id="search_by" class="form-control-new" onchange="change_search_by(this.value)">
                  <option value="data">Data</option>
                  <option value="matreqnumber" <?php if($id) echo "selected='selected'";?>>MR No</option>
                  <option value="formulanumber">Formula Number</option>
                  <option style="display:none;" value="inventorycode">PCode</option>
                  <option value="prodplan">Prod Plan</option>
              </select>
              &nbsp;
              <span id="td_data" <?php if($id) echo "style='display:none;'"; ?>>
                  Type Date&nbsp;
                  <select name="v_type_date" id="v_type_date" class="form-control-new" onchange="change_type_date(this.value)">
                      <option value="today">Today</option>
                      <option value="7_day_before">7 day Before</option>
                      <option value="range_date">Range Date</option>
                  </select>
                  <span id="td_range_date" style="display:none">
                  &nbsp;
                  <input type="text" class="form-control-new datepicker" name="v_from_date" id="v_from_date" size="10" maxlength="10" value="<?php echo $v_from_date; ?>">
                    &nbsp;
                  up to
                  &nbsp;
                  <input type="text" class="form-control-new datepicker" name="v_to_date" id="v_to_date" size="10" maxlength="10" value="<?php echo $v_to_date; ?>">
                  &nbsp;
                  </span>
                  &nbsp;Req Type&nbsp;
                  <select name="search_reqtype" id="search_reqtype" class="form-control-new">
                      <option value="">All</option>
                      <option value="3">Baku + Kemas</option>
                      <option value="1">Baku</option>
                      <option value="2">Kemas</option>
                  </select>
                  &nbsp;Keyword&nbsp;
                  <input type="text" class="form-control-new" name="search_keyword" id="search_keyword" size="30" value="">
                  
              </span>
              
              <span id="td_matreqnumber" <?php if($id) { echo "style='displaye:;'"; } else { echo "style='display:none;'"; }?>>
                  <input type="text" class="form-control-new" name="search_matreqnumber" id="search_matreqnumber" size="30" value="<?php echo $id; ?>">
              </span>
              
              <span id="td_formulanumber" style="display:none;">
                  <input type="text" class="form-control-new" name="search_formulanumber" id="search_formulanumber" size="30" value="">
              </span>
              
              <span id="td_inventorycode" style="display:none;">
                  <input type="text" class="form-control-new" name="search_inventorycode" id="search_inventorycode" size="30" value="">
              </span>
              
              <span id="td_prodplan" style="display:none;">
                  <input type="text" class="form-control-new" name="search_prodplan" id="search_prodplan" size="30" value="">
              </span>
          </div>
       
         <div class="col-md-2" align="right">
                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('search'),show_loading_bar(100)">Search<i class="entypo-search"></i></button>
                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjax('add_data'),show_loading_bar(100)">Add Data<i class="entypo-plus" ></i></button>
             </div>
          
          </div>
      
      <hr />
      
      <center>
      <div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
       <div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
      </center>
   
<?php

    if($id)
    {
        ?>
            <script>
                CallAjax('search');
            </script>
        <?php
    }

   include("footer.php");
?>