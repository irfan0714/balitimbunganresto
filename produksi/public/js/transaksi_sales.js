    //9: "tab",
    //13: "enter",
    //16: "shift",
    //18: "alt",
    //27: "esc",
    //33: "rePag",
    //34: "avPag",
    //35: "end",
    //36: "home",
    //37: "left",
    //38: "up",
    //39: "right",
    //40: "down",
    //112: "F1",
    //113: "F2",
    //114: "F3",
    //115: "F4",
    //116: "F5",
    //117: "F6",
    //118: "F7",
    //119: "F8",
    //120: "F9",
    //121: "F10",
$(document).ready(function () {
    $(document).keydown( function (e) {
        switch(e.keyCode)
        {
            // user presses the "F1"
            case 112: //alert("F1"); fokus produck
                document.getElementById('kdbrg1').focus();
                break;
            case 113: //alert("F2");cash bayar
                document.getElementById('cash_bayar').focus();
                break;
            case 120: //alert("F3");cetak
                cek_form();
                break;
            case 115: //alert("F1"); fokus produck
                document.getElementById('pelanggan').focus();
                break;
            case 116: //alert("F2");cash bayar
                document.getElementById('kdagent').focus();
                break;
        }
    });
});


function number_format(a, b, c, d)
{
	a = Math.round(a * Math.pow(10, b)) / Math.pow(10, b);
	e = a + '';
	f = e.split('.');
	if (!f[0]) {
	f[0] = '0';
	}
	if (!f[1]) {
	f[1] = '';
	}
	if (f[1].length < b) {
	g = f[1];
	for (i=f[1].length + 1; i <= b; i++) {
	g += '0';
	}
	f[1] = g;
	}
	if(d != '' && f[0].length > 3) {
	h = f[0];
	f[0] = '';
	for(j = 3; j < h.length; j+=3) {
	i = h.slice(h.length - j, h.length - j + 3);
	f[0] = d + i +  f[0] + '';
	}
	j = h.substr(0, (h.length % 3 == 0) ? 3 : (h.length % 3));
	f[0] = j + f[0];
	}
	c = (b <= 0) ? '' : c;
	
	return f[0] + c + f[1];
}

function SetNetto()
	{ 							
		document.getElementById('qty1').focus();
		
		qty1              = Number(document.getElementById('qty1').value);
		jualmtanpaformat1 = Number(document.getElementById('jualmtanpaformat1').value);
		
		netto1            = qty1 * jualmtanpaformat1;
		
		document.getElementById('netto1').value = number_format(netto1, 0, ',', '.');
		document.getElementById('nettotanpaformat1').value = netto1;		
	}
		
	function clear_trans(url)
	{
		if(confirm("Anda yakin ingin menghapus transaksi ini"))
			{
                setCookies("penjualanPOS", "", 1) ;
				document.forms["pindah"].submit();
                document.getElementById('kdbrg1').focus();
			}
		else{
				return false;
			}
	}

	function oncek()
	{
		if(document.form1.pilihan[0].checked == true){
			document.getElementById('cash_bayar').disabled=false;
			document.getElementById('cash_bayar').focus();
		
			if(document.getElementById('id_kredit').value == "")
			{ document.getElementById('id_kredit').disabled=true;}
			else { document.getElementById('id_kredit').disabled=false;}
			
			if(document.getElementById('id_debet').value == "")
			{ document.getElementById('id_debet').disabled=true;}
			else{document.getElementById('id_debet').disabled=false;}
			
			if(document.getElementById('id_voucher').value == "")
			{ document.getElementById('id_voucher').disabled=true;}
			else{document.getElementById('id_voucher').disabled=false;}
			
			if(document.getElementById('kredit_bayar').value == "")
			{document.getElementById('kredit_bayar').disabled = true;}
			else{document.getElementById('kredit_bayar').disabled = false;}
			
			if(document.getElementById('debet_bayar').value == "")
			{document.getElementById('debet_bayar').disabled = true;}
			else{document.getElementById('debet_bayar').disabled = false;}
			
			if(document.getElementById('voucher_bayar').value == "")
			{document.getElementById('voucher_bayar').disabled = true;}
			else{document.getElementById('voucher_bayar').disabled = false;}
	}
	else if(document.form1.pilihan[1].checked == true){
			if(document.getElementById('cash_bayar').value == "")
			{ document.getElementById('cash_bayar').disabled=true;}
			else { document.getElementById('cash_bayar').disabled=false;}
			
			document.getElementById('id_kredit').disabled=false;
			document.getElementById('id_kredit').focus();
		
			if(document.getElementById('id_debet').value == "")
			{ document.getElementById('id_debet').disabled=true;}
			else{document.getElementById('id_debet').disabled=false;}
			
			if(document.getElementById('id_voucher').value == "")
			{ document.getElementById('id_voucher').disabled=true;}
			else{document.getElementById('id_voucher').disabled=false;}
			
			if(document.getElementById('debet_bayar').value == "")
			{document.getElementById('debet_bayar').disabled = true;}
			else{document.getElementById('debet_bayar').disabled = false;}
			
			if(document.getElementById('voucher_bayar').value == "")
			{document.getElementById('voucher_bayar').disabled = true;}
			else{document.getElementById('voucher_bayar').disabled = false;}
	}
	else if(document.form1.pilihan[2].checked == true){
			if(document.getElementById('cash_bayar').value == "")
			{ document.getElementById('cash_bayar').disabled=true;}
			else { document.getElementById('cash_bayar').disabled=false;}
			
			if(document.getElementById('id_kredit').value == "")
			{ document.getElementById('id_kredit').disabled=true;}
			else { document.getElementById('id_kredit').disabled=false;}
			
			document.getElementById('id_debet').disabled=false;
			document.getElementById('id_debet').focus();
		
			if(document.getElementById('id_voucher').value == "")
			{ document.getElementById('id_voucher').disabled=true;}
			else{document.getElementById('id_voucher').disabled=false;}
			
			if(document.getElementById('kredit_bayar').value == "")
			{document.getElementById('kredit_bayar').disabled = true;}
			else{document.getElementById('kredit_bayar').disabled = false;}
			
			if(document.getElementById('voucher_bayar').value == "")
			{document.getElementById('voucher_bayar').disabled = true;}
			else{document.getElementById('voucher_bayar').disabled = false;}	
		}
	else if(document.form1.pilihan[3].checked == true){
			if(document.getElementById('cash_bayar').value == "")
			{ document.getElementById('cash_bayar').disabled=true;}
			else { document.getElementById('cash_bayar').disabled=false;}
			
			if(document.getElementById('id_kredit').value == "")
			{ document.getElementById('id_kredit').disabled=true;}
			else { document.getElementById('id_kredit').disabled=false;}
			
			if(document.getElementById('id_debet').value == "")
			{ document.getElementById('id_debet').disabled=true;}
			else{document.getElementById('id_debet').disabled=false;}
			
			document.getElementById('id_voucher').disabled=false;
			document.getElementById('id_voucher').focus();
			
			if(document.getElementById('kredit_bayar').value == "")
			{document.getElementById('kredit_bayar').disabled = true;}
			else{document.getElementById('kredit_bayar').disabled = false;}
			
			if(document.getElementById('debet_bayar').value == "")
			{document.getElementById('debet_bayar').disabled = true;}
			else{document.getElementById('debet_bayar').disabled = false;}			
		}	
	}
	
	function SetKembali()
	{
        var e = document.getElementById("Uang");
        var kurs = e.options[e.selectedIndex].value;
        var myarr = kurs.split("-");

		total_biaya 			= Number(document.getElementById('total_biaya').value);
		cash_bayar        		= Number(document.getElementById('cash_bayar').value);
        cash_bayar2             = cash_bayar * Number(myarr[1]);
		kredit_bayar			= Number(document.getElementById('kredit_bayar').value);
		debet_bayar				= Number(document.getElementById('debet_bayar').value);
		voucher_bayar			= Number(document.getElementById('voucher_bayar').value);
		total_bayar				= cash_bayar2 + kredit_bayar + debet_bayar + voucher_bayar;
        service_charge			= Number(document.getElementById('service_charge').value);
		kembali					= total_bayar - (total_biaya );
        //alert(service_charge);
		document.getElementById('total_bayar_hide').value = total_bayar;
		document.getElementById('total_bayar').innerHTML = number_format(total_bayar, 0, ',', '.');
		document.getElementById('cash_kembali').innerHTML = number_format(kembali, 0, ',', '.');		
	}
	
	function KreditCustomer(e,row,url)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		
		if (code == 13)
		{
			if(document.getElementById('id_kredit').value !== "")
			{
				document.getElementById('kredit_bayar').disabled=false;
				document.getElementById('kredit_bayar').focus();
			}
			else{
				document.getElementById('kredit_bayar').disabled=true;
				}
		}
	
	}
	
	function DebetCustomer(e,row,url)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		
		if (code == 13)
		{
			if(document.getElementById('id_debet').value !== "")
			{
				document.getElementById('debet_bayar').disabled=false;
				document
                    .getElementById('debet_bayar').focus();
			}
			else{
				document.getElementById('debet_bayar').disabled=true;
				}
		}
	
	}
function getCodeForSales(e,row,url,action)
{
    if(window.event)
    {
        var code = e.keyCode;
    }
    else if(e.which)
    {
        var code = e.which;
    }
    if (code == 13)
    {
        if(action == 'insert')
        {
            PCode = document.getElementById('kdbrg' + row).value;
                                            //    alert(PCode);
            document.getElementById('temp_pos2').style.display = "block";
            document.getElementById('temp_pos1').style.display = "none";

            $.ajax({
                type: "POST",
                url: url+"index.php/transaksi/pos/DetailItemForSales/"+PCode,
                success: function(msg){
					//alert(msg);alert(PCode);

                    if(msg=="salah"){
                        alert("Kode / Barcode Salah !!!");
                        document.getElementById('kdbrg1').value = "";
                        document.getElementById('kdbrg1').focus();
                        if (isNaN(PCode))
                        {
                            //alert("Must input numbers");
                            index = 1;
                            url = url + "index.php/pop/grup_barang/index/sales/";
                            window.open(url + (index), 'popuppage', 'width=750,height=500,top=100,left=150');
                        }
                    }else{
                        var jsdata = msg;
                        //var res = jsdata.replace("[[", "[");
                        //var res2 = res.replace("]]", "]");
                        //eval(res2);
                        //document.getElementById('kdbrg1').value = datajson[0].PCode;//tambahannya
                        //document.getElementById('nmbrg' + row).value = datajson[0].NamaLengkap;
                        //document.getElementById('qty' + row).value = 1;
                        //document.getElementById('jualm' + row).value = number_format(datajson[0].Harga1c, 0, ',', '.');
                        //document.getElementById('netto' + row).value = number_format(datajson[0].Harga1c, 0, ',', '.');
                        //document.getElementById('jualmtanpaformat' + row).value = datajson[0].Harga1c;
                        //document.getElementById('disk' + row).value = datajson[0].disc;
                        //document.getElementById('nettotanpaformat' + row).value = datajson[0].Harga1c;
                        //document.getElementById('brgDetail').value = msg;
                        //document.forms["salesform"].submit();
                        //alert(person);
                        //isi(msg);
                        //alert(b)
                        barisPOS();

                    }
                }
            });
        }
        else
        {
            if(document.getElementById('qty1').value == "")
            {
                alert('Qty tidak boleh kosong');
            }
            else
            {
                PCode = document.getElementById('kdbrg' + row).value;
                $.post(url+"index.php/transaksi/pos/cekkode",{ PCode:PCode},
                    function(data){
                        if(data=="ok"){
                            document.forms["salesform"].submit();
                        }else{
                            alert('Kode barang salah');
                            index = 1;
                            url = url+"index.php/pop/grup_barang/index/sales/";
                            window.open(url+(index),'popuppage','width=750,height=500,top=100,left=150');
                            // jika kode barang salah tampilkan list barang
                            //http://localhost/neracaNPM/index.php/pop/grup_barang/index/sales/1
                        }
                    });
            }
        }
    }
}
    function objectLength(obj) {
        var result = 0;
        for(var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                // or Object.prototype.hasOwnProperty.call(obj, prop)
                result++;
            }
        }
        return result;
    }

    function barisPOS(){
        deleteAll();
        document.getElementById('temp_pos1').style.display = "none";
        document.getElementById('temp_pos2').style.display = "block";

        dt = (getCookie("penjualanPOS"));

        //eval(dt);
        //alert(dt.length);
        dt2 = eval(dt);
        ttl = dt2.length;
        //alert(ttl);
        //alert("barisnya");
        //ttl=1;
        if(ttl > 1){
            b = 1;
            //alert(dt2[0]['PCode']);
            for(z=0;z < ttl;z++){
                if(z > 0 ){
                    //alert("tambah baris :"+z);
                    cloneRow(z+1);
                    //createRow();
                }
                b=b+1;
            }
            ttlNetto = 0;
            dpp = 0;
            tax = 0;
            sr = 0;
            service_charge = 0;
            dpp_charge= 0;
            for(z2=1;z2 < ttl+1;z2++){
                document.getElementById('lfield_sales_temp'+ z2).innerHTML  = z2;
                document.getElementById('lkodebarang'+ z2).innerHTML  = dt2[z2-1]['PCode'];
                document.getElementById('lnamabarang'+ z2).innerHTML = (dt2[z2-1]['NamaLengkap']).replace("+", " ");
                document.getElementById('ljumlahqty'+ z2).innerHTML = dt2[z2-1]['Qty'];
                document.getElementById('ljumlahharga'+ z2 ).innerHTML =  number_format(dt2[z2-1]['Harga1c'], 0, ',', '.');
                document.getElementById('ldisc'+ z2).innerHTML = dt2[z2-1]['disc'];
                document.getElementById('ljumlahnetto'+z2).innerHTML = number_format((dt2[z2-1]['Netto']),0,',','.');
                ttlNetto = ttlNetto + parseInt(dt2[z2-1]['Netto']);
                ppn = parseInt(dt2[z2-1]['PPN']);
                netto = parseInt(dt2[z2-1]['Netto']);
                if((dt2[z2-1]['Service_charge'])==""){
                    sr = 0;
                    service_charge =0;
                }else{
                    sr = parseInt(dt2[z2-1]['Service_charge'])  / 100;
                    service_charge = service_charge +  (netto * sr);
                    dpp_charge = dpp_charge + ((netto * sr)/((10+100)/100));
                }
                if( ppn > 0 ){
                    dpp = dpp + (netto /((100 + ppn) / 100));
                    tax = tax + (netto - (netto / ((ppn+100) / 100)));
                }else{
                    dpp =dpp + netto;
                }
            }
            ttlall = ttlNetto + service_charge;
            document.getElementById('TotalItem').innerHTML = number_format(z2 - 1 ,0,',','.');// TotalItem in cart
            document.getElementById('TotalItem2').value = (z2 - 1) ;// TotalItem in cart
            document.getElementById('dpp').value = Math.floor(dpp  + dpp_charge)  ;// dpp
            document.getElementById('tax').value =  Math.floor(tax + (dpp_charge * 0.1)) ;// tax
            document.getElementById('ttlall').innerHTML = number_format(ttlall,0,',','.'); //  total setelah service charge
            document.getElementById('TotalNetto').innerHTML = number_format(ttlNetto,0,',','.');
            document.getElementById('service_charge').value = service_charge;// TotalItem in cart
            document.getElementById('Charge').innerHTML = number_format(service_charge ,0,',','.');;// TotalItem service charge
            document.getElementById('total_biaya').value = ttlall  ;

        }else{
            document.getElementById('lfield_sales_temp1').innerHTML  = 1;
            document.getElementById('lkodebarang1').innerHTML  = dt2[0]['PCode'];
            document.getElementById('lnamabarang1').innerHTML = (dt2[0]['NamaLengkap']).replace("+", " ");
            document.getElementById('ljumlahqty1').innerHTML = dt2[0]['Qty'];
            document.getElementById('ljumlahharga1' ).innerHTML =  number_format(dt2[0]['Harga1c'], 0, ',', '.');
            document.getElementById('ldisc1').innerHTML = dt2[0]['disc'];
            document.getElementById('ljumlahnetto1').innerHTML = number_format(dt2[0]['Netto'],0,',','.');

            if((dt2[0]['Service_charge'])==""){
                sr = 0;
                service_charge = sr;
            }else{
                sr = parseInt(dt2[0]['Service_charge'])  / 100;
                service_charge = parseInt(dt2[0]['Netto']) * sr;
            }
            if( dt2[0]['PPN'] > 0  ){
                dpp = parseInt(dt2[0]['Netto']);
                tax = parseInt(dt2[0]['Netto']) * parseInt(dt2[0]['PPN']) / 100;
            }else{
                dpp = 0;
                tax = 0;
            }
            ttlall =  parseInt(dt2[0]['Netto']) + service_charge;
            document.getElementById('dpp').value = (dpp - tax)+service_charge ;// dpp
            document.getElementById('tax').value = (service_charge / 100) + tax ;// tax
            document.getElementById('ttlall').innerHTML = number_format(ttlall,0,',','.'); //  total setelah service charge
            document.getElementById('TotalNetto').innerHTML = number_format(dt2[0]['Netto'],0,',','.');
            document.getElementById('TotalItem2').value = 1;// TotalItem in cart
            document.getElementById('TotalItem').innerHTML = 1;// TotalItem in cart
            document.getElementById('service_charge').value = service_charge;// TotalItem in cart
            document.getElementById('Charge').innerHTML = number_format(service_charge ,0,',','.');;// TotalItem service charge
            document.getElementById('total_biaya').value = ttlall;
        }
        document.getElementById('kdbrg1').value = "" ;
        document.getElementById('kdbrg1').focus();
    }

    function setCookies(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }


    function cloneRow(nbaru) {
        nomor = nbaru - 1;
        var row = document.getElementById("newID" + nomor); // find row to copy
        var table = document.getElementById("tableToModify"); // find table to append to
        var clone = row.cloneNode(true); // copy children too
        $("#lfield_sales_temp" + nomor, clone).attr({"id": "lfield_sales_temp" + nbaru});
        $("#lkodebarang" + nomor, clone).attr({"id": "lkodebarang" + nbaru});
        $("#lnamabarang" + nomor, clone).attr({"id": "lnamabarang" + nbaru});
        $("#ldisc" + nomor, clone).attr({"id": "ldisc" + nbaru});
        $("#ljumlahqty" + nomor, clone).attr({"id": "ljumlahqty" + nbaru});
        $("#ljumlahharga" + nomor, clone).attr({"id": "ljumlahharga" + nbaru});
        $("#ljumlahnetto" + nomor, clone).attr({"id": "ljumlahnetto" + nbaru});
        $("#field_sales_temp" + nomor, clone).attr({"id": "field_sales_temp" + nbaru});
        clone.id = "newID"+nbaru; // change id or other attributes/contents
        table.appendChild(clone); // add new row to end of table
    }

    function deleteAll() {

        var tbl = document.getElementById("tableToModify");
        var rowLen = tbl.rows.length - 1;
        //alert(rowLen);
        for (var idx = rowLen; idx > 0; idx--)
        tbl.deleteRow(idx)
    }

