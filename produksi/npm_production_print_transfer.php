<?php 
include("header.php");

include("asset.php");

    function openFile($ourFileName)
    {
        $file = file($ourFileName);         
        $content= "<pre>";
        for($l = 0;$l<count($file);$l++){
            $content .= $file[$l];            
        }
        $content .= "</pre>";
        $content =  substr($content,5,strlen($content)-5);
        $content =  substr($content,0,strlen($content)-6);
        return $content;
    }

    function create_txt_report($paths,$filed,$str)
    {
        $strs=chr(27).chr(64).chr(27).chr(67).chr(33).$str;
         $txt=$paths."/".$filed;
        if(file_exists($txt)){
            unlink($txt);
        }
        $open_it=fopen($txt,"a+");
        fwrite($open_it,$strs);
        fclose($open_it);    
    }

    function sintak_epson()
    {
        $arr_data["escNewLine"]      = chr(10);  // New line (LF line feed)
        $arr_data["escUnerlineOn"]   = chr(27).chr(45).chr(1);  // Unerline On
        $arr_data["escUnerlineOnx2"] = chr(27).chr(45).chr(2);  // Unerline On x 2
        $arr_data["escUnerlineOff"]  = chr(27).chr(45).chr(0);  // Unerline Off
        $arr_data["escBoldOn"]       = chr(27).chr(69).chr(1);  // Bold On
        $arr_data["escBoldOff"]      = chr(27).chr(69).chr(0);  // Bold Off
        $arr_data["escNegativeOn"]   = chr(29).chr(66).chr(1);  // White On Black On'
        $arr_data["escNegativeOff"]  = chr(29).chr(66).chr(0);  // White On Black Off
        $arr_data["esc8CpiOn"]       = chr(29).chr(33).chr(16); // Font Size x2 On
        $arr_data["esc8CpiOff"]      = chr(29).chr(33).chr(0);  // Font Size x2 Off
        $arr_data["esc16Cpi"]        = chr(27).chr(77).chr(48); // Font A  -  Normal Font
        $arr_data["esc20Cpi"]        = chr(27).chr(77).chr(49); // Font B - Small Font
        $arr_data["escReset"]        = chr(27).chr(64); //chr(27) + chr(77) + chr(48); // Reset Printer
        $arr_data["escFeedAndCut"]   = chr(29).chr(86).chr(65); // Partial Cut and feed

        $arr_data["escAlignLeft"]    = chr(27).chr(97).chr(48); // Align Text to the Left
        $arr_data["escAlignCenter"]  = chr(27).chr(97).chr(49); // Align Text to the Center
        $arr_data["escAlignRight"]   = chr(27).chr(97).chr(50); // Align Text to the Right
        
        $arr_data["reset"]  =chr(27).'@'; //reset semua pengaturan printer
        $arr_data["plength"]=chr(27).'C'; //tinggi kertas, contoh $plength.chr(33) ==> tinggi 33 baris.
        $arr_data["lmargin"]=chr(27).'l'; //margin kiri, pemakaian sama dengan $plength.
        $arr_data["cond"]   =chr(15);   //condensed
        $arr_data["ncond"]  =chr(18);   //end condensed
        $arr_data["dwidth"] =chr(27).'!'.chr(16);  //tulisan melebar
        $arr_data["ndwidth"]=chr(27).'!'.chr(1);   //end tulisan melebar
        $arr_data["draft"]  =chr(27).'x'.chr(48);  //font draft
        $arr_data["nlq"]    =chr(27).'x'.chr(49);
        $arr_data["bold"]   =chr(27).'E';   //tulisan bold
        $arr_data["nbold"]  =chr(27).'F';   //end tulisan bold
        $arr_data["uline"]  =chr(27).'!'.chr(129); //garis bawah
        $arr_data["nuline"] =chr(27).'!'.chr(1); //end garis bawah
        $arr_data["dstrik"] =chr(27).'G';    //double strike (tulisan lebih tebal, 2 kali strike)
        $arr_data["ndstrik"]=chr(27).'H';    //end double strike (tulisan lebih tebal, 2 kali strike)
        $arr_data["elite"]  =chr(27).'M';    //tulisan elite
        $arr_data["pica"]   =chr(27).'P';    //tulisan pica
        $arr_data["height"] =chr(27).'!'.chr(16); //tulisan tinggi
        $arr_data["nheight"]=chr(27).'!'.chr(1);  //end tulisan tinggi
        $arr_data["spasi05"]=chr(27)."3".chr(16); //spasi 5 char
        $arr_data["spasi1"] =chr(27)."3".chr(24); //spasi 1 char
        $arr_data["fcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';    // potong kertas full
        $arr_data["pcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';    // potong kertas sebagian
        $arr_data["op_cash"]=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);   //buka cash register
                    
        return $arr_data;                           
    } 

    if(!isset($_GET["sid"])){ $sid = isset($_GET["sid"]); } else { $sid = $_GET["sid"]; }

                
    if($sid=="")
    {
        $msg = "Data harus dipilih";
        echo "<script>alert('".$msg."');</script>"; 
        die();
    }
    
    $arr_epson = sintak_epson();
    
    $total_spasi = 145;
    $total_spasi_header = 85;
    
    $total_spasi = 125;
    $total_spasi_header = 125;
    
    $jml_detail  = 5;
    $ourFileName = "transfer-wip.txt";
    $nama        = "PT. NATURA PESONA MANDIRI";
    
    $echo="";
    $productionid = $id;
    
    $q = "
            SELECT 
              formula.formulanumber,
              formula.formulaname,
              production_transfer.batchnumber,
              production_transfer.purpose,
              production_transfer.transfer_date,
              production_transfer.amount,
              production_transfer.remarks
            FROM
              production_transfer 
              INNER JOIN `production` ON
                production_transfer.`productionid` = `production`.`productionid`
                AND `production`.mixquantity*1!='0'
              INNER JOIN `formula` ON
                production.`formulanumber` = `formula`.`formulanumber`
            WHERE 1 
              AND production_transfer.sid = '".$sid."' 
            LIMIT
                0,1
    ";   
    $qry_curr = mysql_query($q);
    $arr_curr = mysql_fetch_array($qry_curr);
    
    $product_name = $arr_curr["formulanumber"]." - ".$arr_curr["formulaname"];
    
    $counter = 1;
    $arr_data["detail_inventorycode"][$productionid][$counter] = $arr_curr["formulanumber"];
    $arr_data["detail_inventoryname"][$productionid][$counter] = $arr_curr["formulaname"];
    $arr_data["detail_quantity_qc_reject"][$productionid][$counter] = $arr_curr["amount"];
    $arr_data["qc_reject_remarks"][$productionid][$counter] = $arr_curr["formulanumber"]." - ".$arr_curr["formulaname"];
    
    $jml_page = ceil(1/$jml_detail);
    
    $nama_dokumen = "TRANSFER WIP";
    $kode_dokumen = "";
    $kode_edisi   = "";
    $spasi_tanggal_terbit = 6;
    
    $grand_total = 0;
    for($i_page=1;$i_page<=$jml_page;$i_page++)
    {
        // header
        {
            $echo.=chr(18);
            $echo.=$nama;
            //$echo .= chr(27)."G";
            
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="";
            
            $limit_spasi = 25;
            for($i=0;$i<($limit_spasi-strlen(""));$i++)
            {
                $echo.=" ";
            }
            

            $limit_spasi = 47;
            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
            {
                $echo.=" ";
            }
            $echo.=$kode_dokumen; 
            $echo.="\r\n";    
        }
        
        // old 02.574.485.5.511.001
        // new 02.574.485.5.034.001
        //$echo .= chr(15).chr(27).chr(69);
        $echo.="";
        //$echo .= chr(27).chr(70).chr(18);
        
        $kurang = strlen("");
        $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2) - $kurang;
        for($i=0;$i<$limit_spasi;$i++)
        {
            $echo.=" ";
        }
        
        //$echo .= chr(27).chr(119).chr(1);
        $echo.=$nama_dokumen;
        //$echo .= chr(27).chr(119).chr(0);
        
        for($i=0;$i<$spasi_tanggal_terbit;$i++)
        {
            $echo.=" ";
        }
        
        $tanggal_terbit      = substr($arr_curr["editdate"],0,10);
        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
        
        //$echo .= "TANGGAL TERBIT : ".$tanggal_terbit_show;
        
        $echo.="\r\n";
        
        $echo.= " ";
        $kurang = strlen(" ");
        
        $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$arr_curr["batchnumber"])/2)-$kurang;
        for($i=0;$i<$limit_spasi;$i++)
        {
            $echo.=" ";
        }
        
        $echo.="No : ".$arr_curr["batchnumber"];
        
        
        for($i=0;$i<16;$i++)
        {
            $echo.=" ";
        }
        
        $echo.="";
        

        $echo.="\r\n";
        //$echo .= $arr_data["esc8CpiOff"];
        
        $echo.="\r\n"; 
        
        // baris 1
        {
            $echo.="DATE";
            $limit_spasi = (15-2);
            for($i=0;$i<($limit_spasi-strlen("DATE"));$i++)
            {
                $echo.=" ";
            }
            $echo.=": ";
            
            $echo.=format_show_date($arr_curr["transfer_date"]);
            
            $limit_spasi = 110;
            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["transfer_date"])));$i++)
            {
                $echo.=" ";
            }
            $echo.="\r\n";    
        }

        // baris 2
        {
            $echo.="PURPOSE";
            $limit_spasi = (15-2);
            for($i=0;$i<($limit_spasi-strlen("PURPOSE"));$i++)
            {
                $echo.=" ";
            }
            $echo.=": ";
            
            $echo.=$arr_curr["purpose"];
            
            $limit_spasi = 110;
            for($i=0;$i<($limit_spasi-strlen($arr_curr["purpose"]));$i++)
            {
                $echo.=" ";
            }
            
            
            $echo.="\r\n";    
        }
        
        
        
        $echo.=chr(15);
        for($i=1;$i<=$total_spasi;$i++)
        {
            $echo.="-";
        }
        $echo .= "\r\n";
        
        
        $echo.="NO";
        $limit_spasi = 3;
        for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
        {
            $echo.=" ";
        }
        
        $echo.="KETERANGAN";
        $limit_spasi = 102;
        for($i=0;$i<($limit_spasi-strlen("KETERANGAN"));$i++)
        {
            $echo.=" ";
        }
        
        
        $limit_spasi = 15;
        for($i=0;$i<($limit_spasi-strlen("QTY  "));$i++)
        {
            $echo.=" ";
        }
        $echo .= "QTY  ";
        
        $echo .= "";
        $limit_spasi = 5;
        for($i=0;$i<($limit_spasi-strlen(""));$i++)
        {
            $echo.=" ";
        }
        
        
        
        $echo.="\r\n";
        for($i=1;$i<=$total_spasi;$i++)
        {
            $echo.="-";
        }
        
        $echo.="\r\n";
        
        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
        $no_end = $no + $jml_detail;
        
        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
        {
            $detail_inventorycode = $arr_data["detail_inventorycode"][$productionid][$i_detail];
            $detail_inventoryname = $arr_data["detail_inventoryname"][$productionid][$i_detail];
            $detail_quantity_qc_reject = $arr_data["detail_quantity_qc_reject"][$productionid][$i_detail];
            $qc_reject_remarks = $arr_data["qc_reject_remarks"][$productionid][$i_detail];

            $qc_reject_remarks = substr($qc_reject_remarks, 0, 100);
            
            $decimal_php = 4;
            
            
            // kalo ada isi baru di print
            if($qc_reject_remarks)
            {
                $echo.=chr(15);
                $echo.=$no;
                $limit_spasi = 3;
                for($i=0;$i<($limit_spasi-strlen($no));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$qc_reject_remarks;
                $limit_spasi = 102;
                for($i=0;$i<($limit_spasi-strlen($qc_reject_remarks));$i++)
                {
                    $echo.=" ";
                }
                
                ## QTY
                $limit_spasi = 15;
                for($i=0;$i<($limit_spasi-strlen(format_number($detail_quantity_qc_reject, $decimal_php, "cetak")."  "));$i++)
                {
                    $echo.=" ";
                }
                $echo.=format_number($detail_quantity_qc_reject, $decimal_php, "cetak")."  ";
                
                $echo.=$uominitial;
                $limit_spasi = 5;
                for($i=0;$i<($limit_spasi-strlen($uominitial));$i++)
                {
                    $echo.=" ";
                } 
                
                
               
                $echo.=chr(18);
                
                $grand_total += $total;
                
            }
            
            $no++;
            $echo.="\r\n";
        }
        $echo.="\r\n";
        
        
        $echo.=chr(15);
        for($i=1;$i<=$total_spasi;$i++)
        {
            $echo.="-";
        }
        $echo.=chr(18);
        
        $Sincerely = ucfirst(substr($ses_login,0,-4));
        $Knowing = "Manager Produksi";
        $Approval = "Manager R&D";
        
        
        $echo.=chr(15);
        if($i_page==$jml_page)
        {
            // TOTAL
            $echo.="\r\n";
            $echo.="\r\n";
            
            $echo.="           Dibuat          ";
            $echo.="          ";
            $echo.="             Mengetahui          ";
            $echo.="          ";
            $echo.="              Diterima           ";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="         ( ".$Sincerely." )         ";
            $echo.="          ";
            $echo.="       ( ".$Knowing." )         ";
            $echo.="          ";
            $echo.="       (               )         ";
            $echo.="\r\n";
            //$echo .= "\r\n";
            
            //$echo .= "\r\n";
        }
        else
        {
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
            $echo.="\r\n";
        }
        $echo.=chr(18);
        //$echo .= chr(27);
        
        $q = "
                SELECT
                    COUNT(noreferensi) AS jml_print
                FROM
                    log_print
                WHERE
                    1
                    AND noreferensi = '".$pono."' 
                    AND form_data = 'purchaseorder' 
        ";
        $qry_jml_print = mysql_query($q);
        $row_jml_print = mysql_fetch_array($qry_jml_print);
        
        if($row_jml_print["jml_print"]*1>0)
        {
            //$echo .= $arr_epson["reset"];
            $limit_spasi = $total_spasi_header;
            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=chr(15)."COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);    
        }
        else
        {
            //$echo .= $arr_epson["reset"];
            $limit_spasi = $total_spasi_header;
            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
            {
                $echo.=" ";
            }
        
            $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
        }
        
        
        //$echo .= "\r\n";
        //$echo .= "\r\n";
        //$echo .= "\r\n";
    
    } // end page
    
    
    // update counter print
    if($ses_login!="hendri1003")
    {
        $q = "
                INSERT
                    log_print
                SET
                    form_data = 'transfer_wip',
                    noreferensi = '".$sid."' , 
                    userid = '".$ses_login."' , 
                    print_date = NOW() , 
                    print_page = 'Setengah Letter'    
        ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Insert Print Counter";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
    }
    
    
    $paths = "cetakan/";
    $name_text_file='transfer-wip-'.$ses_login.'.txt';
    create_txt_report($paths,$name_text_file,$echo);
    header("Location: ".$paths."/".$name_text_file); 
?>