<?php
    include("header.php");
    
	$modul        = "Material Request";
	$file_current = "npm_internal_mutation.php";
	$file_name    = "npm_internal_mutation_data.php";

function update_bincard($v_imno)
{
    global $ses_login;
    
    $q = "DELETE FROM bincard WHERE bincard.referenceno = '".$v_imno."'";
    if(!mysql_query($q))      
    {
        $msg = "Gagal menghapus bincard";
        echo "<script>alert('".$msg."');</script>";     
        die();
    }
    
    //  purpose != '37' itu kelebihan supplier, jangan mengurangi stok
    $q = "
            SELECT
                internalmutation.muttype,
                internalmutation.fromwarehouse,
                internalmutation.towarehouse
            FROM
                internalmutation
            WHERE
                1
                AND internalmutation.imno = '".$v_imno."'
                AND internalmutation.status = '1'
                AND internalmutation.purpose != '37'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    if($row["muttype"]=="mut")
    {
        $q = "
                INSERT INTO bincard 
                (
                    inventorycode, 
                    carddate, 
                    warehousecode, 
                    referenceno, 
                    module, 
                    qin, 
                    qout, 
                    VALUE, 
                    adduser, 
                    ADDDATE, 
                    edituser, 
                    editdate
                )
                SELECT
                    internalmutationdetail.inventorycode,
                    internalmutation.imdate,
                    internalmutation.fromwarehouse AS warehousecode,
                    internalmutation.imno AS referenceno,
                    'MM' AS module, 
                    '0' AS qin,
                    internalmutationdetail.quantity AS qout,
                    '0' AS val,
                    '".$ses_login."' AS adduser,
                    NOW() AS adddate,
                    '".$ses_login."' AS edituser,
                    NOW() AS editdate
                FROM
                    internalmutation
                    INNER JOIN internalmutationdetail ON
                        internalmutation.imno = internalmutationdetail.imno
                        AND internalmutation.imno = '".$v_imno."'
                        AND (internalmutationdetail.quantity*1)!='0'
                WHERE
                    1
        ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal insert bincard 1";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
    }
    else if($row["muttype"]=="out")
    {
        $q = "
                INSERT INTO bincard 
                (
                    inventorycode, 
                    carddate, 
                    warehousecode, 
                    referenceno, 
                    module, 
                    qin, 
                    qout, 
                    VALUE, 
                    adduser, 
                    ADDDATE, 
                    edituser, 
                    editdate
                )
                SELECT
                    internalmutationdetail.inventorycode,
                    internalmutation.imdate,
                    internalmutation.fromwarehouse AS warehousecode,
                    internalmutation.imno AS referenceno,
                    'MM' AS module, 
                    '0' AS qin,
                    internalmutationdetail.quantity AS qout,
                    '0' AS val,
                    '".$ses_login."' AS adduser,
                    NOW() AS adddate,
                    '".$ses_login."' AS edituser,
                    NOW() AS editdate
                FROM
                    internalmutation
                    INNER JOIN internalmutationdetail ON
                        internalmutation.imno = internalmutationdetail.imno
                        AND internalmutation.imno = '".$v_imno."'
                        AND (internalmutationdetail.quantity*1)!='0'
                WHERE
                    1
        ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal insert bincard 2";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }    
    }
    else if($row["muttype"]=="in")
    {
        $q = "
                INSERT INTO bincard 
                (
                    inventorycode, 
                    carddate, 
                    warehousecode, 
                    referenceno, 
                    module, 
                    qin, 
                    qout, 
                    VALUE, 
                    adduser, 
                    ADDDATE, 
                    edituser, 
                    editdate
                )
                SELECT
                    internalmutationdetail.inventorycode,
                    internalmutation.imdate,
                    internalmutation.towarehouse AS warehousecode,
                    internalmutation.imno AS referenceno,
                    'MM' AS module, 
                    internalmutationdetail.quantity AS qin,
                    '0' AS qout,
                    '0' AS val,
                    '".$ses_login."' AS adduser,
                    NOW() AS adddate,
                    '".$ses_login."' AS edituser,
                    NOW() AS editdate
                FROM
                    internalmutation
                    INNER JOIN internalmutationdetail ON
                        internalmutation.imno = internalmutationdetail.imno
                        AND internalmutation.imno = '".$v_imno."'
                        AND (internalmutationdetail.quantity*1)!='0'
                WHERE
                    1
        ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal insert bincard 3";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }    
    }
    
}

function openFile($ourFileName)
{
    $file = file($ourFileName);         
    $content= "<pre>";
    for($l = 0;$l<count($file);$l++){
        $content .= $file[$l];            
    }
    $content .= "</pre>";
    $content =  substr($content,5,strlen($content)-5);
    $content =  substr($content,0,strlen($content)-6);
    return $content;
}

function sintak_epson()
{
    $arr_data["escNewLine"]      = chr(10);  // New line (LF line feed)
    $arr_data["escUnerlineOn"]   = chr(27).chr(45).chr(1);  // Unerline On
    $arr_data["escUnerlineOnx2"] = chr(27).chr(45).chr(2);  // Unerline On x 2
    $arr_data["escUnerlineOff"]  = chr(27).chr(45).chr(0);  // Unerline Off
    $arr_data["escBoldOn"]       = chr(27).chr(69).chr(1);  // Bold On
    $arr_data["escBoldOff"]      = chr(27).chr(69).chr(0);  // Bold Off
    $arr_data["escNegativeOn"]   = chr(29).chr(66).chr(1);  // White On Black On'
    $arr_data["escNegativeOff"]  = chr(29).chr(66).chr(0);  // White On Black Off
    $arr_data["esc8CpiOn"]       = chr(29).chr(33).chr(16); // Font Size x2 On
    $arr_data["esc8CpiOff"]      = chr(29).chr(33).chr(0);  // Font Size x2 Off
    $arr_data["esc16Cpi"]        = chr(27).chr(77).chr(48); // Font A  -  Normal Font
    $arr_data["esc20Cpi"]        = chr(27).chr(77).chr(49); // Font B - Small Font
    $arr_data["escReset"]        = chr(27).chr(64); //chr(27) + chr(77) + chr(48); // Reset Printer
    $arr_data["escFeedAndCut"]   = chr(29).chr(86).chr(65); // Partial Cut and feed

    $arr_data["escAlignLeft"]    = chr(27).chr(97).chr(48); // Align Text to the Left
    $arr_data["escAlignCenter"]  = chr(27).chr(97).chr(49); // Align Text to the Center
    $arr_data["escAlignRight"]   = chr(27).chr(97).chr(50); // Align Text to the Right
    
    $arr_data["reset"]  =chr(27).'@'; //reset semua pengaturan printer
    $arr_data["plength"]=chr(27).'C'; //tinggi kertas, contoh $plength.chr(33) ==> tinggi 33 baris.
    $arr_data["lmargin"]=chr(27).'l'; //margin kiri, pemakaian sama dengan $plength.
    $arr_data["cond"]   =chr(15);   //condensed
    $arr_data["ncond"]  =chr(18);   //end condensed
    $arr_data["dwidth"] =chr(27).'!'.chr(16);  //tulisan melebar
    $arr_data["ndwidth"]=chr(27).'!'.chr(1);   //end tulisan melebar
    $arr_data["draft"]  =chr(27).'x'.chr(48);  //font draft
    $arr_data["nlq"]    =chr(27).'x'.chr(49);
    $arr_data["bold"]   =chr(27).'E';   //tulisan bold
    $arr_data["nbold"]  =chr(27).'F';   //end tulisan bold
    $arr_data["uline"]  =chr(27).'!'.chr(129); //garis bawah
    $arr_data["nuline"] =chr(27).'!'.chr(1); //end garis bawah
    $arr_data["dstrik"] =chr(27).'G';    //double strike (tulisan lebih tebal, 2 kali strike)
    $arr_data["ndstrik"]=chr(27).'H';    //end double strike (tulisan lebih tebal, 2 kali strike)
    $arr_data["elite"]  =chr(27).'M';    //tulisan elite
    $arr_data["pica"]   =chr(27).'P';    //tulisan pica
    $arr_data["height"] =chr(27).'!'.chr(16); //tulisan tinggi
    $arr_data["nheight"]=chr(27).'!'.chr(1);  //end tulisan tinggi
    $arr_data["spasi05"]=chr(27)."3".chr(16); //spasi 5 char
    $arr_data["spasi1"] =chr(27)."3".chr(24); //spasi 1 char
    $arr_data["fcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';    // potong kertas full
    $arr_data["pcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';    // potong kertas sebagian
    $arr_data["op_cash"]=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);   //buka cash register
                
    return $arr_data;                           
}

$ajax = $_REQUEST["ajax"];       

if($ajax)
{
    if($ajax=='search')
    {
        $search_by              = trim($_GET["search_by"]);
        $v_type_date            = trim($_GET["v_type_date"]);
        $v_from_date            = trim($_GET["v_from_date"]);
        $v_to_date              = trim($_GET["v_to_date"]);
        $search_intmutpurpose   = trim($_GET["search_intmutpurpose"]);
        $search_warehouse       = trim($_GET["search_warehouse"]);
        $search_mutation_no     = trim($_GET["search_mutation_no"]);
        $search_reference_no    = trim($_GET["search_reference_no"]);
        $v_imno_curr            = $_GET["v_imno_curr"];
        
        $v_today        = date("d-m-Y");
        $v_last_7day    = date("d-m-Y", parsedate($v_today,"-")-(86400*7));
        
        $where = "";
        if($search_by=="data" || $search_by=="moving_confirmation")
        {
            if($v_type_date=="today")
            {
                $where .= " AND internalmutation.imdate = '".format_save_date($v_today)."' ";
            }
            else if($v_type_date=="7_day_before")
            {
                $where .= " AND internalmutation.imdate BETWEEN '".format_save_date($v_last_7day)."' AND '".format_save_date($v_today)."' ";    
            }
            else if($v_type_date=="range_date")
            {
                $where .= " AND internalmutation.imdate BETWEEN '".format_save_date($v_from_date)."' AND '".format_save_date($v_to_date)."' ";
            }
            
            if($search_intmutpurpose)
            {
                $where .= " AND internalmutation.purpose = '".$search_intmutpurpose."' ";    
            }
            
            if($search_warehouse)
            {
                $where .= " AND 
                            (
                                internalmutation.fromwarehouse = '".$search_warehouse."' 
                                OR
                                internalmutation.towarehouse = '".$search_warehouse."' 
                            )
                            ";    
            }
            
            if($search_by=="moving_confirmation")
            {
                $where .= " AND internalmutation.movingstatus = '0' AND internalmutation.muttype = 'mut'  ";        
            }
        }
        else if($search_by=="imno")
        {
            if($search_mutation_no)
            {
                $where .= " AND internalmutation.imno LIKE '%".$search_mutation_no."%' ";    
            }
        }
        else if($search_by=="mrno")
        {
            if($search_reference_no)
            {
                $where .= " AND internalmutation.mrno LIKE '%".$search_reference_no."%' ";    
            }
        }
        
        $q = "
                SELECT
                    intmutpurpose.purposeid,
                    intmutpurpose.purpose
                FROM
                    intmutpurpose
                WHERE
                    1
                ORDER BY
                    intmutpurpose.purpose ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($purposeid, $purpose) = $row;
            
            $arr_data["list_purpose"][$purposeid] = $purposeid;
            
            $arr_data["purpose"][$purposeid] = $purpose;
        }
        
        $q = "
                SELECT
                    warehouse.warehousecode,
                    warehouse.warehousename
                FROM
                    warehouse
                WHERE
                    1
                ORDER BY
                    warehouse.warehousecode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
            
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
            
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
        
        $q = "
            SELECT
                internalmutation.imno,
                internalmutation.imdate,
                internalmutation.muttype,
                internalmutation.mrno,
                internalmutation.fromwarehouse,
                internalmutation.towarehouse,
                internalmutation.purpose,
                internalmutation.movingstatus,
                internalmutation.status,
                internalmutation.halal  
            FROM
                internalmutation
            WHERE
                1
                ".$where."
            ORDER BY
                internalmutation.imno ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($imno, $imdate, $muttype, $mrno, $fromwarehouse, $towarehouse, $purpose, $movingstatus, $status, $halal) = $row;
            
            $arr_data["list_internalmutation"][$imno] = $imno;
            
            $arr_data["imdate"][$imno] = $imdate;
            $arr_data["muttype"][$imno] = $muttype;
            $arr_data["mrno"][$imno] = $mrno;
            $arr_data["fromwarehouse"][$imno] = $fromwarehouse;
            $arr_data["towarehouse"][$imno] = $towarehouse;
            $arr_data["purpose"][$imno] = $purpose;
            $arr_data["movingstatus"][$imno] = $movingstatus;
            $arr_data["status"][$imno] = $status;
            
            /*
            // cek halal
            $status_halal = false;    
            if($mrno)
            {
                $q = "
                        SELECT
                            formula.formulaname,
                            inventorymaster.halal
                        FROM
                            formula
                            INNER JOIN matreq ON
                                formula.formulanumber = matreq.formulanumber
                                AND matreq.matreqnumber = '".$mrno."'
                            INNER JOIN inventorymaster ON
                                formula.inventorycode = inventorymaster.inventorycode
                        WHERE
                            1
                        LIMIT
                            0,1
                ";
                $qry_formula = mysql_query($q);
                $arr_formula = mysql_fetch_array($qry_formula);
                
                if($arr_formula["halal"]==1)
                {
                    $status_halal = true;    
                }
            }
            
            
            
            $q = "
                    SELECT
                        inventorymaster.halal
                    FROM
                        internalmutationdetail
                        INNER JOIN inventorymaster ON
                            inventorymaster.inventorycode = internalmutationdetail.inventorycode
                    WHERE
                        1
                        AND internalmutationdetail.imno = '".$imno."'
                        AND (internalmutationdetail.quantity*1) != '0'
                    ORDER BY
                        internalmutationdetail.imdetailid ASC
            ";
            $counter = 1;
            $qry_det = mysql_query($q);
            while($row_det = mysql_fetch_array($qry_det))
            {
                list($halal) = $row_det;
                
                $arr_data["list_detail"][$imno][$counter] = $counter;
                $arr_data["detail_halal"][$imno][$counter] = $halal;
                
                $counter++;
            }
            
            $curr_jml_detail = count($arr_data["list_detail"][$imno]);
            
            // cek halal detail
            if(!$status_halal)
            {
                $status_halal = true;
                foreach($arr_data["list_detail"][$imno] as $counter)
                {
                    $halal = $arr_data["detail_halal"][$imno][$counter];
                    
                    // kalo ketemu gak halal, langsung break aja
                    if($halal==0)
                    {
                        $status_halal = false;
                        break;
                    }
                }
            } 
            */
            
            // direct langsung dari transaksinya
            $status_halal = false;    
            if($halal==1)
            {
                $status_halal = true;    
            }
            
            if($status_halal)
            {
                $arr_data["status_halal"][$imno] = 1;
            }
            else
            {
                $arr_data["status_halal"][$imno] = 0;
            }
            
            
            
        } 
    
        $jml = count($arr_data["list_internalmutation"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }


    ?>
    <div class="col-md-12">
    <div style="padding:5px;">
        <span style="background-color:#C2EBFF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <b>Halal</b>
    </div>
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_internal_mutation_data.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
    	<thead>
			<tr >
				<th><strong><center>No</center></strong></th>
				<th><strong><center>Mutation No</center></strong></th>
				<th><strong><center>Date</center></strong></th>
				<th><strong><center>Type</center></strong></th>
				<th><strong><center>Reference No</center></strong></th>
				<th><strong><center>From</center></strong></th>
				<th><strong><center>To</center></strong></th>
				<th><strong><center>Purpose</center></strong></th>
				<th title="Moving Confirmation"><strong><center>MC</center></strong></th>
				<th><strong><center>
					Action<br/>
					<input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></center></strong>
				</th>
			</tr>
		</thead>
		<tbody>
    		<?php
			if(count($arr_data["list_internalmutation"])*1==0)
			{
				echo "
					<tr>
						<td colspan='100%' align='center'>No Data</td>
					</tr>
				";
			}

		    $nomor = 0;    
		    foreach($arr_data["list_internalmutation"] as $imno => $val)
		    {    
		        $nomor++;
		        
		        $imdate = $arr_data["imdate"][$imno];
		        $muttype = $arr_data["muttype"][$imno];
		        $mrno = $arr_data["mrno"][$imno];
		        $fromwarehouse = $arr_data["fromwarehouse"][$imno];
		        $towarehouse = $arr_data["towarehouse"][$imno];
		        $purpose = $arr_data["purpose"][$imno];
		        $movingstatus = $arr_data["movingstatus"][$imno];
		        $status = $arr_data["status"][$imno];
		        $status_halal = $arr_data["status_halal"][$imno];
		        
		        $style_color = "";
		        if($v_imno_curr==$imno)
		        {
		            $style_color = "background:#cafdb5;";    
		        }
		        
		        $movingstatus_echo = "&nbsp;";
		        $style_bg_mc = "";
		        if($muttype=="mut")
		        {
		            if($movingstatus==0)
		            {
		                $movingstatus_echo = "Process";
		                $style_bg_mc = "background: #FFFF00";    
		            }   
		            else if($movingstatus==1)
		            {
		                $movingstatus_echo = "Confirm";    
		            } 
		        }
		        
		        if($status==2)
		        {
		            $movingstatus_echo = "VOID";
		            $style_bg_mc = "background: #FF0000";    
		        }
		        
		        $style_color = "";
		        if($status_halal)
		        {
		            $style_color = "background-color:#C2EBFF;";
		        }
		        
			    ?>
			    
			    <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$style_bg; ?>">
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>"><?php echo $nomor; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>" align="center"><?php echo $imno; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>" align="center"><?php echo format_show_date($imdate); ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>"><?php echo $muttype; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>" align="center"><?php echo $mrno; ?>&nbsp;</td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>"><?php echo $arr_data["warehousename"][$fromwarehouse]; ?>&nbsp;</td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>"><?php echo $arr_data["warehousename"][$towarehouse]; ?>&nbsp;</td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg.$style_color; ?>"><?php echo $arr_data["purpose"][$purpose]; ?></td>
			        <td onclick="CallAjax('edit_data','<?php echo $imno; ?>')" style="<?php echo $style_bg_mc.$style_color; ?>" align="center"><?php echo $movingstatus_echo; ?></td>
			        <td style="<?php echo $style_bg.$style_color; ?>" align="center"><input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $imno; ?>"></td> 
			    </tr>
			    <?php
			    }
			
			?>
			    <tr>
			        <td colspan="100%" align="right">
			            <select name="action" id="action" class="form-control-new">
			                <option value="print_letter">Print Letter</option>
			                <option value="print_setengah_letter">Print 1/2 Letter</option>
			            </select>
			            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_submit" id="btn_submit" value="Submit">Print<i class="entypo-print"></i></button>
			            &nbsp;&nbsp;
			        </td>
			    </tr>
		</tbody>
			    
	</table>
	</form>
	</div>
    <?php
    
    }
    else if($ajax=="add_data")
    {    
        // cek warehouseadmin
        $q = "
                SELECT
                    warehouseadmin.warehousecode
                FROM
                    warehouseadmin
                WHERE
                    1
                    AND warehouseadmin.userid = '".$ses_login."'
        ";
        $qry_warehouseadmin = mysql_query($q);
        while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
        {
            list($warehousecode) = $row_warehouseadmin;
            
            $arr_data["warehouseadmin"][$warehousecode] = $warehousecode;
        }
        
        $q = "
                SELECT
                    intmutpurpose.purposeid,
                    intmutpurpose.purpose
                FROM
                    intmutpurpose
                WHERE
                    1
                ORDER BY
                    intmutpurpose.purpose ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($purposeid, $purpose) = $row;
            
            $arr_data["list_purpose"][$purposeid] = $purposeid;
            
            $arr_data["purpose"][$purposeid] = $purpose;
        }
        
        $q = "
                SELECT
                    warehouse.warehousecode,
                    warehouse.warehousename
                FROM
                    warehouse
                WHERE
                    1
                ORDER BY
                    warehouse.warehousecode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
            
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
            
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
        
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0)">
						<i class="entypo-pencil"></i>Add <?php echo $modul; ?>
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">
                
                <tr>
                    <td class="title_table" width="250">Date</td>
                    <td>
                        <input type="text" size="10" maxlength = "10" name="v_imdate" id="v_imdate" value="<?php echo date("d-m-Y"); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_imdate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Type</td>
                    <td>
                        <select name="v_muttype" id="v_muttype" class="form-control-new" onchange="change_muttype(this.value)">
                            <option value="">-</option>
                            <option value="in">In</option>
                            <option value="out">Out</option>
                            <option value="mut">Mutation</option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Purpose</td>
                    <td>
                        
                        <select name="v_purpose" id="v_purpose" class="form-control-new">
                            <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_purpose"] as $purposeid => $val)
                            {
                                $purpose = $arr_data["purpose"][$purposeid];
                        ?>
                                <option value="<?php echo $purposeid; ?>"><?php echo $purpose; ?></option>
                        <?php        
                            }
                        ?>
                        </select>
                        
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Reference</td>
                    <td>
                        <input type="text" name="v_mrno" id="v_mrno" class="form-control-new" size="30" maxlength="100" value="" onkeyup="CallAjax('cek_matreq_duplicate', this.value)" onblur="CallAjax('cek_matreq_duplicate', this.value)">
                        <span id="td_info_matreq"></span>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">From Warehouse</td>
                    <td id="td_fromwarehouse">
                        <select name="v_fromwarehouse" id="v_fromwarehouse" class="form-control-new">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
                                {
                                    if($arr_data["warehouseadmin"][$warehousecode]==$warehousecode)
                                    {
                                        $warehousename = $arr_data["warehousename"][$warehousecode];
                                ?>
                                        <option value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
                                <?php 
                                    }
                                }
                            ?>
                        </select>
                        &nbsp;
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">To Warehouse</td>
                    <td id="td_towarehouse">
                        <select name="v_towarehouse" id="v_towarehouse" class="form-control-new">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
                                {
                                    $warehousename = $arr_data["warehousename"][$warehousecode];
                            ?>
                                    <option value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
                            <?php 
                                }
                            ?>
                        </select>
                        &nbsp;
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Halal</td>
                    <td>
                        <label><input type="checkbox" name="v_halal" id="v_halal">&nbsp;&nbsp;Ya</label>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Note</td>
                    <td>
                        <input type="text" name="v_note" id="v_note" class="form-control-new" size="50" maxlength="100" value="">
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                    	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                    </td>
                </tr>
            </table>
            </form>
        <?php
    }
    else if($ajax=="edit_data")
    {
        $v_imno = $_GET["v_imno"];

        $q = "
                SELECT
                    *
                FROM
                    internalmutation 
                WHERE
                    1
                    AND internalmutation.imno = '".$v_imno."'
                LIMIT
                    0,1
        ";
        $qry_curr = mysql_query($q);
        $arr_curr = mysql_fetch_array($qry_curr);
        
        $_SESSION["ses_imno"]           = $arr_curr["imno"];
        $_SESSION["ses_imdate"]         = $arr_curr["imdate"];
        $_SESSION["ses_muttype"]        = $arr_curr["muttype"];
        $_SESSION["ses_fromwarehouse"]  = $arr_curr["fromwarehouse"];
        $_SESSION["ses_towarehouse"]    = $arr_curr["towarehouse"];
        
        $arr_date = explode("-", $arr_curr["imdate"]);
        
        $year  = $arr_date[0];
        $month = $arr_date[1];
        
        $checkbox_halal = "";
        if($arr_curr["halal"]==1)
        {
            $checkbox_halal = "checked='checked'";
        }
        
        
        $q = "
                SELECT
                    internalmutationdetail.imdetailid,
                    inventorymaster.stocktypeid,
                    internalmutationdetail.inventorycode,
                    inventorymaster.inventoryname,
                    internalmutationdetail.quantity,
                    inventorymaster.uominitial,
                    internalmutationdetail.batchnumber,
                    internalmutationdetail.note,
                    internalmutationdetail.matreqnumber
                FROM
                    internalmutationdetail
                    INNER JOIN inventorymaster ON
                        internalmutationdetail.inventorycode = inventorymaster.inventorycode
                        AND internalmutationdetail.imno = '".$v_imno."'
                WHERE
                    1
                ORDER BY
                    internalmutationdetail.imdetailid ASC
        ";
        $counter = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($imdetailid, $stocktypeid, $inventorycode, $inventoryname, $quantity, $uominitial, $batchnumber, $note, $matreqnumber) = $row;
            
            $arr_data["list_detail"][$counter] = $counter;
            
            $arr_data["list_inventory"][$inventorycode] = $inventorycode;
            
            $arr_data["stocktypeid"] = $stocktypeid;
            
            $arr_data["detail_imdetailid"][$counter] = $imdetailid;
            $arr_data["detail_stocktypeid"][$counter] = $stocktypeid;
            $arr_data["detail_inventorycode"][$counter] = $inventorycode;
            $arr_data["detail_inventoryname"][$counter] = $inventoryname;
            $arr_data["detail_quantity"][$counter] = $quantity;
            $arr_data["detail_uominitial"][$counter] = $uominitial;
            $arr_data["detail_batchnumber"][$counter] = $batchnumber;
            $arr_data["detail_note"][$counter] = $note;
            $arr_data["detail_matreqnumber"][$counter] = $matreqnumber;
            
            
            $arr_data["list_matreqnumber"][$matreqnumber] = $matreqnumber;
            
            $counter++;
        } 
        
        if(count($arr_data["list_matreqnumber"])*1>0)
        {
            $where_matreqnumber = where_array($arr_data["list_matreqnumber"], "matreq.matreqnumber", "in");
            
            $q = "
                    SELECT
                        matreq.matreqnumber
                    FROM
                        matreq
                    WHERE
                        1
                        AND matreq.status != '2'
                        ".$where_matreqnumber."
                    ORDER BY
                         matreq.matreqnumber ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($matreqnumber) = $row;
                
                $arr_data["matreq_exist"][$matreqnumber] = $matreqnumber;
            }
            
            $where_prod_matreq = where_array($arr_data["matreq_exist"], "productionbatchrequest.matreqnumber", "in");
            
            $q = "
                    SELECT 
                        productionbatchrequest.matreqnumber,
                        productionbatchrequest.batchnumber
                    FROM
                        productionbatchrequest
                    WHERE
                        1
                        ".$where_prod_matreq."
                    ORDER BY
                        productionbatchrequest.matreqnumber ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($matreqnumber, $batchnumber) = $row;
                
                $arr_data["productionbatchnumber"][$matreqnumber] = $batchnumber;
            }
            
        }
        
        
         $q = "
                SELECT
                    log_print.userid,
                    log_print.print_date,
                    log_print.print_page
                FROM
                    log_print
                WHERE
                    1
                    AND log_print.noreferensi = '".$v_imno."'
                    AND log_print.form_data = 'internal_mutation'
                ORDER BY
                    log_print.print_date ASC
        ";
        $counter = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($userid, $print_date, $print_page) = $row;
                  
            $arr_data["list_print"][$counter] = $counter;
            
            $arr_data["print_userid"][$counter] = $userid;
            $arr_data["print_print_date"][$counter] = $print_date;       
            $arr_data["print_print_page"][$counter] = $print_page;
            
            $counter++;
        }
        
        if($arr_curr["mrno"])
        {
            $q = "
                    SELECT
                        matreqdetail.inventorycode,
                        matreqdetail.quantity
                    FROM
                        matreqdetail
                    WHERE
                        1
                        AND matreqdetail.matreqnumber = '".$arr_curr["mrno"]."'
                    ORDER BY
                        matreqdetail.inventorycode ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($inventorycode, $quantity) = $row;
                
                $arr_data["request"][$inventorycode] = $quantity;
            }
            
            $q = "
                    SELECT
                        internalmutationdetail.inventorycode,
                        SUM(internalmutationdetail.quantity) AS quantity
                    FROM
                        internalmutationdetail
                        INNER JOIN internalmutation ON
                            internalmutationdetail.imno = internalmutation.imno
                            AND internalmutation.mrno = '".$arr_curr["mrno"]."'
                            AND internalmutation.imno != '".$v_imno."'
                            AND internalmutation.status = '1'
                    WHERE
                        1
                    GROUP BY
                        internalmutationdetail.inventorycode
                    ORDER BY
                        internalmutationdetail.inventorycode ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($inventorycode, $quantity) = $row;
                
                $arr_data["usage"][$inventorycode] = $quantity;
            }
        } 
        
        $q = "
                SELECT
                    intmutpurpose.purposeid,
                    intmutpurpose.purpose
                FROM
                    intmutpurpose
                WHERE
                    1
                ORDER BY
                    intmutpurpose.purpose ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($purposeid, $purpose) = $row;
            
            $arr_data["list_purpose"][$purposeid] = $purposeid;
            
            $arr_data["purpose"][$purposeid] = $purpose;
        }
        
        $q = "
                SELECT
                    warehouse.warehousecode,
                    warehouse.warehousename
                FROM
                    warehouse
                WHERE
                    1
                ORDER BY
                    warehouse.warehousecode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
            
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
            
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
        
        //echo sprintf("%02s", $month).",".$year.",".$arr_curr["fromwarehouse"].",".$arr_data["stocktypeid"];
        //echo "<pre>";
        //print_r($arr_data["list_inventory"]);
        //echo "</pre>";
        
        $arr_stock = bincard_stock(sprintf("%02s", $month), $year, $arr_data["list_inventory"], $arr_curr["fromwarehouse"], $arr_data["stocktypeid"]);
        
        //echo "<pre>";
        //print_r($arr_stock);
        //echo "</pre>";
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0)">
						<i class="entypo-pencil"></i>Edit <?php echo $modul; ?>
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="edit_data">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_undel" id="v_undel" value="">
            <input type="hidden" size="10"  maxlength="10" name="v_imdate_old" id="v_imdate_old" value="<?php echo format_show_date($arr_curr["imdate"]); ?>" >
            
            <table class="table table-bordered responsive">
                
                <tr height="25">
                    <td class="title_table" width="250">Mutation No</td>
                    <td>
                        <input type="hidden" size="30" maxlength="255" name="v_imno" id="v_imno" value="<?php echo $v_imno; ?>">
                        <?php echo "<b>".$v_imno."</b>";?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Date</td>
                    <td>
                        <input type="text" size="10" maxlength = "10" name="v_imdate" id="v_imdate" value="<?php echo format_show_date($arr_curr["imdate"]); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_imdate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Type</td>
                    <td>
                        <?php echo "<b>".$arr_curr["muttype"]."</b>"; ?>
                        <input type="hidden" name="v_muttype" id="v_muttype" value="<?php echo $arr_curr["muttype"]; ?>">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Purpose</td>
                    <td>
                        
                        <select name="v_purpose" id="v_purpose" class="form-control-new">
                        <?php 
                            foreach($arr_data["list_purpose"] as $purposeid => $val)
                            {
                                $purpose = $arr_data["purpose"][$purposeid];
                                
                                $selected = "";
                                if($arr_curr["purpose"]==$purposeid)
                                {
                                    $selected = "selected='selected'";    
                                }
                        ?>
                                <option <?php echo $selected; ?> value="<?php echo $purposeid; ?>"><?php echo $purpose; ?></option>
                        <?php        
                            }
                        ?>
                        </select>
                        
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Reference</td>
                    <td>
                        <input type="hidden" name="v_mrno" id="v_mrno" value="<?php echo $arr_curr["mrno"]; ?>">
                        <?php echo "<b>".$arr_curr["mrno"]."</b>"; ?>&nbsp;
                    </td>
                </tr>
                
                <?php 
                    if($arr_curr["mrno"])
                    {
                        
                        $q = "
                                SELECT
                                    formula.inventorycode,
                                    formula.formulaname
                                FROM
                                    formula
                                    INNER JOIN matreq ON
                                        formula.formulanumber = matreq.formulanumber
                                        AND matreq.matreqnumber = '".$arr_curr["mrno"]."'
                                WHERE
                                    1
                                LIMIT
                                    0,1
                        ";
                        $qry_formula = mysql_query($q);
                        $arr_formula = mysql_fetch_array($qry_formula);
                ?>
                <tr>
                    <td class="title_table">Formula Name</td>
                    <td>
                        <?php echo "<b>".$arr_formula["inventorycode"]." :: ".$arr_formula["formulaname"]."</b>"; ?>&nbsp;
                    </td>
                </tr>
                <?php 
                    }
                ?>
                
                <tr>
                    <td class="title_table">From Warehouse</td>
                    <td>
                        <?php 
                            if($arr_curr["muttype"]=="mut" || $arr_curr["muttype"]=="out")
                            {
                        ?>
                        <select name="v_fromwarehouse" id="v_fromwarehouse" class="form-control-new">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
                                {
                                    $warehousename = $arr_data["warehousename"][$warehousecode];
                                    
                                    $selected = "";
                                    if($arr_curr["fromwarehouse"]==$warehousecode)
                                    {
                                        $selected = "selected='selected'";    
                                    }
                            ?>
                                    <option <?php echo $selected; ?> value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
                            <?php 
                                }
                            ?>
                        </select>
                        <?php 
                            }
                            else
                            {
                                echo "&nbsp;";
                            }
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">To Warehouse</td>
                    <td>
                        <?php 
                            if($arr_curr["muttype"]=="mut" || $arr_curr["muttype"]=="in")
                            {
                        ?>
                        <select name="v_towarehouse" id="v_towarehouse" class="form-control-new">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $warehousecode => $val)
                                {
                                    $warehousename = $arr_data["warehousename"][$warehousecode];
                                    
                                    $selected = "";
                                    if($arr_curr["towarehouse"]==$warehousecode)
                                    {
                                        $selected = "selected='selected'";    
                                    }
                            ?>
                                    <option <?php echo $selected; ?> value="<?php echo $warehousecode; ?>"><?php echo $warehousename; ?></option>
                            <?php 
                                }
                            ?>
                        </select>
                        <?php 
                            }
                            else
                            {
                                echo "&nbsp;";
                            }
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Halal</td>
                    <td>
                        <label><input type="checkbox" <?php echo $checkbox_halal; ?> name="v_halal" id="v_halal">&nbsp;&nbsp;Ya</b></label>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Note</td>
                    <td>
                        <input type="text" name="v_note" id="v_note" class="form-control-new" size="50" maxlength="100" value="<?php echo karakter_spesial($arr_curr["note"], "output"); ?>">
                    </td>
                </tr>
               
                <tr height="25">
                    <td>&nbsp;</td>
                    <td>
                        <?php
                            if($arr_curr["movingstatus"]==0)
                            {
                        ?>
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button>
                        
                        <?php 
                                if($arr_curr["status"]==1 || $arr_curr["status"]==0)
                                {
                            ?>
                        	<button type="button" onclick="confirm_delete('<?php echo $v_imno; ?>')" name="btn_delete" id="btn_delete" value="VOID Internal Mutation">VOID Internal Mutation<i class="entypo-cancel"></i></button>
                            <?php
                                }
                                else
                                {
                            ?>
                            <button type="button" onclick="confirm_undelete('<?php echo $v_imno; ?>')" name="btn_undelete" id="btn_undelete" value="UnVOID Internal Mutation">UnVOID Internal Mutation<i class="entypo-back"></i></button>
                            <?php        
                                }
                            }
                            else
                            {
                                echo "<font color='red'>Button Save tidak muncul karena sudah di Moving Confirmation</font>";
                            }
                        ?>
                    </td>                               
                </tr>
                                                                       
                <tr>
                    <td colspan="100%">
                        <?php                             
                            if($arr_curr["movingstatus"]==0)
                            {
                        ?>
                        <button type="button" onclick="pop_up_details()" value="Add Item">Add Item<i class="entypo-plus"></i></button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" onclick="pop_up_csv('<?php echo $v_imno; ?>')" value="Add Item">Import CSV<i class="entypo-download"></i></button>
                        <?php 
                            }
                        ?>                                                          
                    </td>
                </tr>
                
                <tr>
                    <td colspan="100%">
                    	<table class="table table-bordered responsive">
						    <thead>
								<tr>
									<th>No</th>
									<th>PCode</th>
									<th>Nama Barang</th>
									<th>Stok</th>
									<?php 
	                                    if($arr_curr["mrno"])
	                                    {
	                                ?>
	                                <th>Request</th>
	                                <?php 
	                                    }
	                                ?>
	                                <th>Qty</th>
	                                <th>Unit</th>
	                                
	                                <?php
	                                    // Return Reject 
	                                    if($arr_curr["purpose"]==7 || $arr_curr["purpose"]==32 || $arr_curr["purpose"]==33 || $arr_curr["purpose"]==34)
	                                    {
	                                ?>
	                                <th>MR No</th>
	                                <?php 
	                                    }
	                                ?>
	                                <th>Note</th>
	                                <th>Del Item</th>
								</tr>
							</thead>
							<tbody>
                            
                            <?php
                                $no = 1; 
                                foreach($arr_data["list_detail"] as $counter => $val)
                                {
                                    $imdetailid = $arr_data["detail_imdetailid"][$counter];
                                    $stocktypeid = $arr_data["detail_stocktypeid"][$counter];
                                    $inventorycode = $arr_data["detail_inventorycode"][$counter];
                                    $inventoryname = $arr_data["detail_inventoryname"][$counter];
                                    $quantity = $arr_data["detail_quantity"][$counter];
                                    $uominitial = $arr_data["detail_uominitial"][$counter];
                                    $batchnumber = $arr_data["detail_batchnumber"][$counter];
                                    $note = $arr_data["detail_note"][$counter];
                                    $matreqnumber = $arr_data["detail_matreqnumber"][$counter];
                                    
                                    if($inventorycode=="01-00097" || $inventorycode=="01-000971")
                                    {
                                        //$arr_stock["akhir"][01-00097] = 0;    
                                    }
                                    
                                    if($stocktypeid==1)
                                    {
                                        $decimal_js = "6";    
                                        $decimal_php = 6;    
                                    }
                                    else
                                    {
                                        $decimal_js = "";    
                                        $decimal_php = 0;    
                                    }
                                    
                                    $req = $arr_data["request"][$inventorycode] - ($arr_data["usage"][$inventorycode] + $quantity);
                                    
                                    $status_matreq = "";
                                    if($matreqnumber)
                                    {
                                        if($arr_data["matreq_exist"][$matreqnumber]==$matreqnumber)
                                        {
                                            $productionbatchnumber = $arr_data["productionbatchnumber"][$matreqnumber];
                                            
                                            $status_matreq = "<img src='images/accept.png'> ".$productionbatchnumber;        
                                        }
                                        else
                                        {
                                            $status_matreq = "<img src='images/cross.gif'>";
                                        }
                                    }
                                    
		                            ?>
		                             <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
		                                <td>
		                                    <?php echo $no; ?>
		                                    <input type="hidden" name="no[]" value="<?php echo $no; ?>">
		                                    <input type="hidden" name="v_inventorycode_<?php echo $no; ?>" id="v_inventorycode_<?php echo $no; ?>" value="<?php echo $inventorycode; ?>">
		                                    <input type="hidden" name="v_stok_<?php echo $no; ?>" id="v_stok_<?php echo $no; ?>" value="<?php echo $arr_stock["akhir"][$inventorycode]; ?>">
		                                    <input type="hidden" name="v_request_<?php echo $no; ?>" id="v_request_<?php echo $no; ?>" value="<?php echo $req; ?>">
		                                    
		                                    <input type="hidden" name="v_quantity_old_<?php echo $no; ?>" id="v_quantity_old_<?php echo $no; ?>" value="<?php echo format_number($quantity, $decimal_php); ?>">
		                                    <input type="hidden" name="v_matreqnumber_old_<?php echo $no; ?>" id="v_matreqnumber_old_<?php echo $no; ?>" value="<?php echo $matreqnumber; ?>">
		                                    <input type="hidden" name="v_note_old_<?php echo $no; ?>" id="v_note_old_<?php echo $no; ?>" value="<?php echo $note; ?>">
		                                </td>
		                                <td><?php echo $inventorycode; ?></td>
		                                <td><?php echo $inventoryname; ?></td>
		                                <td align="right">&nbsp;<?php echo format_number($arr_stock["akhir"][$inventorycode], $decimal_php); ?></td>
		                                <?php 
		                                    if($arr_curr["mrno"])
		                                    {
		                                ?>
		                                <td align="right">&nbsp;<?php echo format_number($req, $decimal_php); ?></td>
		                                <?php 
		                                    }
		                                ?>
		                                <td align="right"><input type="text" class="text" size="10" style="text-align:right;" value="<?php echo format_number($quantity, $decimal_php); ?>" id="v_quantity_<?php echo $no; ?>" name="v_quantity_<?php echo $no; ?>" onblur="toFormat<?php echo $decimal_js; ?>('v_quantity_<?php echo $no; ?>')"></td>
		                                <td><?php echo $uominitial; ?></td>
		                                <?php
		                                    // Return Reject 
		                                    if($arr_curr["purpose"]==7 || $arr_curr["purpose"]==32 || $arr_curr["purpose"]==33 || $arr_curr["purpose"]==34)
		                                    {
		                                ?>
		                                <td>
		                                <nobr>
		                                    <input type="text" class="text" size="16" value="<?php echo $matreqnumber; ?>" id="v_matreqnumber_<?php echo $no; ?>" name="v_matreqnumber_<?php echo $no; ?>" onblur="CallAjax('ajax_matreqnumber',this.value, '<?php echo $no; ?>')" onkeyup="CallAjax('ajax_matreqnumber',this.value, '<?php echo $no; ?>')">
		                                    <span id="show_formulaname_<?php echo $no; ?>"><?php echo $status_matreq; ?></span>
		                                </nobr>
		                                </td>
		                                <?php 
		                                    }
		                                ?>
		                                <td><input type="text" class="text" size="15" name="v_note_<?php echo $no; ?>" id="v_note_<?php echo $no; ?>" value="<?php echo $note; ?>"></td>
		                                <td align="center"><input type="checkbox" name="v_del_details[]" value="<?php echo $inventorycode; ?>"></td>
		                            </tr>
		                            <?php
                                    $no++;  
                                }
                                $no--;
                                
                            ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <tr height="25" class="title_table">
                    <td colspan="100%">Informasi Data</td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Dibuat</td>
                    <td><?php echo format_show_datetime($arr_curr["adddate"])." :: ".$arr_curr["adduser"]; ?></td>
                </tr>
                
                <tr>
                    <td class="title_table">Diedit</td>
                    <td><?php echo format_show_datetime($arr_curr["editdate"])." :: ".$arr_curr["edituser"]; ?></td>
                </tr>
                
                <?php 
                    if(count($arr_data["list_print"])*1>0)
                    {
                ?>
                
                <tr >
                    <td class="title_table" valign="top">Printed</td>
                    <td>
                        <?php 
                            $echo  = "<table>";
                            $echo .= "<tr style='font-weight:bold;' align='center'>";
                            $echo .= "<td>No</td>";
                            $echo .= "<td>Date</td>";
                            $echo .= "<td>User</td>";
                            $echo .= "<td>Page</td>";
                            $echo .= "</tr>";
                            
                            $no = 1;
                            foreach($arr_data["list_print"] as $counter => $val)
                            {
                                $userid = $arr_data["print_userid"][$no];
                                $print_date = $arr_data["print_print_date"][$no];
                                $print_page = $arr_data["print_print_page"][$no];
                                
                                $echo .= "<tr>";
                                $echo .= "<td>".$counter."</td>";
                                $echo .= "<td>".format_show_datetime($print_date)."</td>";
                                $echo .= "<td>".$userid."</td>";
                                $echo .= "<td>".$print_page."</td>";
                                $echo .= "</tr>";
                                
                                $no++;
                            }
                            $echo  .= "</table>";
                            
                            echo $echo;
                        ?>    
                    </td>
                </tr>
                <?php 
                    }
                ?>
            </table>
            </form>
        <?php    
    }
    else if($ajax=="ajax_batchnumber")
    {
        $v_batchnumber = save_char($_GET["v_batchnumber"]);
        
        $q = "
                SELECT
                    formula.formulaname
                FROM
                    formula
                    INNER JOIN production ON
                        formula.formulanumber = production.formulanumber
                        AND production.batchnumber = '".$v_batchnumber."'
                WHERE
                    1
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        
        if($row["formulaname"]=="")
        {
            $formulaname_echo = "Varian Tidak Ditemukan";
        }
        else
        {
            $formulaname_echo = $row["formulaname"];
        }
        
        echo $formulaname_echo; 
    }
    else if($ajax=="ajax_matreqnumber")
    {
        $v_matreqnumber = save_char($_GET["v_matreqnumber"]);
        
        $q = "
                SELECT
                    matreq.matreqnumber
                FROM
                    matreq
                WHERE
                    1
                    AND matreq.matreqnumber = '".$v_matreqnumber."'
                    AND matreq.status != '2'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        
        if($row["matreqnumber"]!="")
        {
            $q = "
                    SELECT 
                        productionbatchrequest.batchnumber
                    FROM
                        productionbatchrequest
                    WHERE
                        1
                        AND productionbatchrequest.matreqnumber = '".$row["matreqnumber"]."'
                    LIMIT
                        0,1
            ";
            $qry_batch = mysql_query($q);
            $row_batch = mysql_fetch_array($qry_batch);
            list($batchnumber) = $row_batch;
            
            $formulaname_echo = "<img src='images/accept.png'> ".$batchnumber;
        }
        else
        {
            $formulaname_echo = "<img src='images/cross.gif'>";
        }
        
        echo $formulaname_echo; 
    }
    
    else if($ajax=="cek_matreq_duplicate")
    {
        $v_matreqnumber = save_char($_GET["v_matreqnumber"]);
        
        $q = "
                SELECT
                    internalmutation.imno
                FROM
                    internalmutation
                WHERE
                    1
                    AND internalmutation.mrno = '".$v_matreqnumber."'
                    AND LEFT(internalmutation.imno,2) = 'MB'
                    AND internalmutation.status != '2'
                ORDER BY
                    internalmutation.imno ASC
        ";
        $qry_cek = mysql_query($q);
        while($row_cek = mysql_fetch_array($qry_cek))
        {
            list($imno) = $row_cek;
            
            $imno_echo = $imno.",";
        } 
        
        $imno_echo = substr($imno_echo,0,-1);
        
        if($imno_echo && $v_matreqnumber!="")
        {
            echo "<font color='red'><b>Info : Sudah pernah ada MR ".$v_matreqnumber." dengan OPB ".$imno_echo."</b></font>";
        }
        
    }

    exit();
}
 
$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :
            {
                if(!isset($_POST["v_imdate"])){ $v_imdate = isset($_POST["v_imdate"]); } else { $v_imdate = $_POST["v_imdate"]; }
                if(!isset($_POST["v_muttype"])){ $v_muttype = isset($_POST["v_muttype"]); } else { $v_muttype = $_POST["v_muttype"]; }
                if(!isset($_POST["v_purpose"])){ $v_purpose = isset($_POST["v_purpose"]); } else { $v_purpose = $_POST["v_purpose"]; }
                if(!isset($_POST["v_mrno"])){ $v_mrno = isset($_POST["v_mrno"]); } else { $v_mrno = $_POST["v_mrno"]; }
                if(!isset($_POST["v_fromwarehouse"])){ $v_fromwarehouse = isset($_POST["v_fromwarehouse"]); } else { $v_fromwarehouse = $_POST["v_fromwarehouse"]; }
                if(!isset($_POST["v_towarehouse"])){ $v_towarehouse = isset($_POST["v_towarehouse"]); } else { $v_towarehouse = $_POST["v_towarehouse"]; }
                if(!isset($_POST["v_note"])){ $v_note = isset($_POST["v_note"]); } else { $v_note = $_POST["v_note"]; }
                if(!isset($_POST["v_halal"])){ $v_halal = isset($_POST["v_halal"]); } else { $v_halal = $_POST["v_halal"]; }
                
                $v_mrno = save_char($v_mrno);
                $v_note = save_char($v_note);
                
                $q = "
                        SELECT
                            warehouseadmin.warehousecode
                        FROM
                            warehouseadmin
                        WHERE
                            1
                            AND warehouseadmin.userid = '".$ses_login."'
                ";
                $qry_warehouseadmin = mysql_query($q);
                while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
                {
                    list($warehousecode) = $row_warehouseadmin;
                    
                    $arr_data["warehouseadmin"][$warehousecode] = $warehousecode;
                }
                
                $acc_locked = call_acc_locked("internal_mutation", format_save_date($v_imdate), format_save_date($v_imdate));
                
                if($acc_locked["locked"][format_save_date($v_imdate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_imdate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    // validasi
                    {
                        if($v_muttype=="")
                        {
                            $msg = "Type harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }                
                        else if($v_purpose=="")
                        {
                            $msg = "Purpose harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                        /*
                        if($v_purpose=="0")
                        {
                            $msg = "Mohon Hubungi IT";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        */
                        
                        if($v_muttype=="mut")
                        {
                            if($v_fromwarehouse=="")
                            {
                                $msg = "From Warehouse harus dipilih";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            else if($v_towarehouse=="")
                            {
                                $msg = "To Warehouse harus dipilih";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            
                            if($v_fromwarehouse==$v_towarehouse)
                            {
                                $msg = "From Warehouse dan To Warehouse tidak boleh sama";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                        }
                        
                        if($v_muttype=="in")
                        {
                            if($v_towarehouse=="")
                            {
                                $msg = "To Warehouse harus dipilih";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            
                            $v_fromwarehouse = ""; 
                            
                             if($v_towarehouse!=$arr_data["warehouseadmin"][$v_towarehouse])  
                             {
                                $msg = "Anda tidak memiliki Otorisasi untuk Gudang ".$v_towarehouse;
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                             }
                        }
                        
                        if($v_muttype=="out")
                        {
                            if($v_fromwarehouse=="")
                            {
                                $msg = "From Warehouse harus dipilih";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            } 
                            
                            $v_towarehouse = "";   
                        }
                        
                        if($v_muttype=="mut")
                        {
                            // selain Rework mintain No Reference
                            if( ($v_purpose!=6 && $v_purpose!=24 && $v_purpose!=7 && $v_purpose!=32 && $v_purpose!=33 && $v_purpose!=34 && $v_purpose!=36 && $v_purpose!=37) && $v_mrno=="")    
                            {
                                $msg = "Reference harus diisi";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                        }
                     
                        // cek reference
                        if($v_mrno)
                        {
                            $q = "
                                    SELECT
                                        matreq.matreqnumber
                                    FROM
                                        matreq
                                    WHERE
                                        1
                                        AND matreq.matreqnumber = '".$v_mrno."'
                                    LIMIT
                                        0,1
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            
                            if($row["matreqnumber"]=="")
                            {
                                $msg = "Reference ".$v_mrno." tidak ditemukan";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                        }
                    }
                    
                    // buat counter
                    {
                        $arr_imdate = explode("-", $v_imdate);
                        
                        //MB0094/VCI/03/11
                        
                        if($v_muttype=="mut" || $v_muttype=="out")
                        {
                            if($v_fromwarehouse=="WH000")
                            {
                                $v_imno_id = "MB";
                            }
                            else if($v_fromwarehouse=="WH001")
                            {
                                $v_imno_id = "MK";
                            }
                            else if($v_fromwarehouse=="WH002")
                            {
                                if($v_purpose==37)
                                {
                                    $v_imno_id = "MPKS";    
                                }
                                else
                                {
                                    $v_imno_id = "MP";    
                                }
                            }
                            else if($v_fromwarehouse=="WH008")
                            {
                                $v_imno_id = "MPR";
                            }
                        }
                        else if($v_muttype=="in")
                        {
                            if($v_towarehouse=="WH000")
                            {
                                $v_imno_id = "MB";
                            }
                            else if($v_towarehouse=="WH001")
                            {
                                $v_imno_id = "MK";
                            }
                            else if($v_towarehouse=="WH002")
                            {
                                $v_imno_id = "MP";
                            }    
                            else if($v_towarehouse=="WH008")
                            {
                                $v_imno_id = "MPR";
                            }  
                        }
                        
                        if(strlen($v_imno_id)*1==2)
                        {
                            $q = "
                                    SELECT
                                        internalmutation.imno
                                    FROM
                                        internalmutation
                                    WHERE
                                        1
                                        AND MONTH(internalmutation.imdate) = '".$arr_imdate[1]."'
                                        AND YEAR(internalmutation.imdate) = '".$arr_imdate[2]."'
                                        AND LEFT(internalmutation.imno,2) = '".$v_imno_id."'
                                        AND CHAR_LENGTH(internalmutation.imno) = '16'
                                    ORDER BY
                                        internalmutation.imno DESC
                                    LIMIT
                                        0,1
                            ";  
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                            
                            //$counter = (str_replace($v_imno_id, "", substr($row_counter["imno"],0,8))*1)+1;  
                            $counter = (substr(str_replace($v_imno_id,"",$row_counter["imno"]),0,4)*1)+1;
                        }
                        else if(strlen($v_imno_id)*1==3)
                        {
                            $q = "
                                    SELECT
                                        internalmutation.imno
                                    FROM
                                        internalmutation
                                    WHERE
                                        1
                                        AND MONTH(internalmutation.imdate) = '".$arr_imdate[1]."'
                                        AND YEAR(internalmutation.imdate) = '".$arr_imdate[2]."'
                                        AND LEFT(internalmutation.imno,3) = '".$v_imno_id."'
                                    ORDER BY
                                        internalmutation.imno DESC
                                    LIMIT
                                        0,1
                            ";
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                            
                            //$counter = (str_replace($v_imno_id, "", substr($row_counter["imno"],0,7))*1)+1;  
                            $counter = (substr(str_replace($v_imno_id,"",$row_counter["imno"]),0,4)*1)+1;
                        }
                        else if(strlen($v_imno_id)*1==4)
                        {
                            $q = "
                                    SELECT
                                        internalmutation.imno
                                    FROM
                                        internalmutation
                                    WHERE
                                        1
                                        AND MONTH(internalmutation.imdate) = '".$arr_imdate[1]."'
                                        AND YEAR(internalmutation.imdate) = '".$arr_imdate[2]."'
                                        AND LEFT(internalmutation.imno,4) = '".$v_imno_id."'
                                    ORDER BY
                                        internalmutation.imno DESC
                                    LIMIT
                                        0,1
                            ";
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                            
                            //$counter = (str_replace($v_imno_id, "", substr($row_counter["imno"],0,4))*1)+1;
                            $counter = (substr(str_replace($v_imno_id,"",$row_counter["imno"]),0,4)*1)+1;
                        }
                        
                        
                        $v_imno = $v_imno_id.sprintf("%04s", $counter)."/NPM/".$arr_imdate[1]."/".substr($arr_imdate[2],2,2);
                    }
                    
                    if($v_halal=="on")
                    {
                        $v_halal_save = "1";
                    }
                    else
                    {
                        $v_halal_save = "0";
                    }
                    
                    $q = "
                            INSERT INTO
                                internalmutation
                            SET
                                imno = '".$v_imno."' , 
                                mrno = '".$v_mrno."' , 
                                towarehouse = '".$v_towarehouse."' , 
                                fromwarehouse = '".$v_fromwarehouse."' , 
                                purpose = '".$v_purpose."' , 
                                muttype = '".$v_muttype."' , 
                                status = '1' , 
                                note = '".karakter_spesial($v_note, "input")."' , 
                                adduser = '".$ses_login."' , 
                                ADDDATE = NOW() , 
                                edituser = '".$ses_login."' , 
                                editdate = NOW() , 
                                imdate = '".format_save_date($v_imdate)."' , 
                                isconfirmed = '0' , 
                                recdate = '' , 
                                dono = '' , 
                                isposted = '0' , 
                                movingstatus = '0',
                                halal = '".$v_halal_save."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Insert ".$v_imno;
                        echo "<script>alert('".$msg."')</script>";
                        die();    
                    }  
                    else        
                    {
                        $opb_double_mr_echo = "";
                        
                        if($v_mrno)
                        {
                            if($v_fromwarehouse=="WH000")
                            {
                                $stocktypeid = 1;
                                
                                $q = "
                                        SELECT
                                            internalmutation.imno
                                        FROM
                                            internalmutation
                                        WHERE
                                            1
                                            AND internalmutation.mrno = '".$v_mrno."'
                                            AND internalmutation.imno != '".$v_imno."'
                                            AND LEFT(internalmutation.imno,2) = 'MB'
                                            AND internalmutation.status != '2'
                                        ORDER BY
                                            internalmutation.imno ASC
                                ";
                                $qry_cek = mysql_query($q);
                                while($row_cek = mysql_fetch_array($qry_cek))
                                {
                                    list($opb_double_mr) = $row_cek;
                                    
                                    $opb_double_mr_echo = $opb_double_mr.",";
                                }
                                
                                $opb_double_mr_echo = substr($opb_double_mr_echo, 0, -1);
                                
                            }
                            else if($v_fromwarehouse=="WH001")
                            {
                                $stocktypeid = 2;
                                
                                $q = "
                                        SELECT
                                            internalmutation.imno
                                        FROM
                                            internalmutation
                                        WHERE
                                            1
                                            AND internalmutation.mrno = '".$v_mrno."'
                                            AND internalmutation.imno != '".$v_imno."'
                                            AND LEFT(internalmutation.imno,2) = 'MK'
                                            AND internalmutation.status != '2'
                                        ORDER BY
                                            internalmutation.imno ASC
                                ";
                                $qry_cek = mysql_query($q);
                                while($row_cek = mysql_fetch_array($qry_cek))
                                {
                                    list($opb_double_mr) = $row_cek;
                                    
                                    $opb_double_mr_echo = $opb_double_mr.",";
                                }
                                
                                $opb_double_mr_echo = substr($opb_double_mr_echo, 0, -1);
                            }
                            
                            $q = "
                                    INSERT INTO
                                        internalmutationdetail
                                        (
                                            imno, 
                                            inventorycode, 
                                            quantity, 
                                            ADDDATE, 
                                            adduser, 
                                            editdate, 
                                            edituser, 
                                            note, 
                                            price
                                        )
                                    SELECT
                                        '".$v_imno."' AS imno,
                                        matreqdetail.inventorycode,
                                        '0.000000' AS quantity,
                                        '".date("Y-m-d")."' AS ADDDATE,
                                        '".$ses_login."' AS adduser,
                                        '".date("Y-m-d")."' AS editdate,
                                        '".$ses_login."' AS edituser,
                                        '' AS note,
                                        '0' AS price
                                    FROM
                                        matreqdetail
                                        INNER JOIN inventorymaster ON
                                            matreqdetail.inventorycode = inventorymaster.inventorycode
                                            AND inventorymaster.stocktypeid = '".$stocktypeid."'
                                            AND matreqdetail.matreqnumber = '".$v_mrno."'
                                            AND (matreqdetail.quantity*1)!='0'
                                    ORDER BY
                                        matreqdetail.matregdetailid ASC
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Insert Matreq";
                                echo "<script>alert('".$msg."')</script>";
                                die();    
                            }  
                        }
                        
                        if($opb_double_mr_echo)
                        {
                            $msg = "Info : Sudah pernah ada MR ".$v_mrno." dengan OPB ".$opb_double_mr_echo;
                            echo "<script>alert('".$msg."');</script>"; 
                        }
                        
                        $msg = "Berhasil menyimpan";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_imno."');</script>";         
                        die();    
                    }
                    
                }
                
                
            }
            break;
        case "edit_data" :
            {
                if(!isset($_POST["no"])){ $no = isset($_POST["no"]); } else { $no = $_POST["no"]; }   
                if(!isset($_POST["v_imno"])){ $v_imno = isset($_POST["v_imno"]); } else { $v_imno = $_POST["v_imno"]; }   
                if(!isset($_POST["v_imdate"])){ $v_imdate = isset($_POST["v_imdate"]); } else { $v_imdate = $_POST["v_imdate"]; }
                if(!isset($_POST["v_imdate_old"])){ $v_imdate_old = isset($_POST["v_imdate_old"]); } else { $v_imdate_old = $_POST["v_imdate_old"]; }
                if(!isset($_POST["v_muttype"])){ $v_muttype = isset($_POST["v_muttype"]); } else { $v_muttype = $_POST["v_muttype"]; }
                if(!isset($_POST["v_purpose"])){ $v_purpose = isset($_POST["v_purpose"]); } else { $v_purpose = $_POST["v_purpose"]; }
                if(!isset($_POST["v_fromwarehouse"])){ $v_fromwarehouse = isset($_POST["v_fromwarehouse"]); } else { $v_fromwarehouse = $_POST["v_fromwarehouse"]; }
                if(!isset($_POST["v_towarehouse"])){ $v_towarehouse = isset($_POST["v_towarehouse"]); } else { $v_towarehouse = $_POST["v_towarehouse"]; }
                if(!isset($_POST["v_note"])){ $v_note = isset($_POST["v_note"]); } else { $v_note = $_POST["v_note"]; }
                if(!isset($_POST["v_halal"])){ $v_halal = isset($_POST["v_halal"]); } else { $v_halal = $_POST["v_halal"]; }
                
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }
                
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }
                if(!isset($_POST["v_del_details"])){ $v_del_details = isset($_POST["v_del_details"]); } else { $v_del_details = $_POST["v_del_details"]; }
                
                $q = "
                        SELECT
                            warehouseadmin.warehousecode
                        FROM
                            warehouseadmin
                        WHERE
                            1
                            AND warehouseadmin.userid = '".$ses_login."'
                ";
                $qry_warehouseadmin = mysql_query($q);
                while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
                {
                    list($warehousecode) = $row_warehouseadmin;
                    
                    $arr_data["warehouseadmin"][$warehousecode] = $warehousecode;
                }
                
                $acc_locked = call_acc_locked("internal_mutation", format_save_date($v_imdate), format_save_date($v_imdate));
                
                if($acc_locked["locked"][format_save_date($v_imdate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_imdate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    if($v_del==1)
                    {   
                        $del = "
                                    UPDATE 
                                        internalmutation 
                                    SET 
                                        status = '2',
                                        editdate = NOW(),
                                        edituser = '".$ses_login."' 
                                    WHERE 
                                        imno = '".$v_imno."'";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal Void";
                            echo "<script>alert('".$msg."')</script>";
                            die();    
                        }
                        
                        update_bincard($v_imno);
                        
                        $msg = "Berhasil Void";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_imno."');</script>";         
                        die();     
                    }
                    else if($v_undel==1)
                    {
                        $del = "
                                    UPDATE 
                                        internalmutation 
                                    SET 
                                        status = '1',
                                        editdate = NOW(),
                                        edituser = '".$ses_login."' 
                                    WHERE 
                                        imno = '".$v_imno."'";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal UnVoid";
                            echo "<script>alert('".$msg."')</script>";
                            die();    
                        }
                        
                        update_bincard($v_imno);
                        
                        $msg = "Berhasil UnVOID";
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_imno."');</script>";         
                        die();         
                    }
                    else
                    {
                        // validasi
                        {
                            if($v_muttype=="")
                            {
                                $msg = "Type harus dipilih";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }                
                            else if($v_purpose=="")
                            {
                                $msg = "Purpose harus dipilih";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            
                            /*
                            if($v_purpose=="0")
                            {
                                $msg = "Mohon Hubungi IT";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            */
                            
                            if($v_muttype=="mut")
                            {
                                if($v_fromwarehouse=="")
                                {
                                    $msg = "From Warehouse harus dipilih";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                }
                                else if($v_towarehouse=="")
                                {
                                    $msg = "To Warehouse harus dipilih";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                }
                                
                                if($v_fromwarehouse==$v_towarehouse)
                                {
                                    $msg = "From Warehouse dan To Warehouse tidak boleh sama";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                }
                            }
                            
                            if($v_muttype=="in")
                            {
                                if($v_towarehouse=="")
                                {
                                    $msg = "To Warehouse harus dipilih";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                }
                                
                                $v_fromwarehouse = ""; 
                                
                                 if($v_towarehouse!=$arr_data["warehouseadmin"][$v_towarehouse])  
                                 {
                                    $msg = "Anda tidak memiliki Otorisasi untuk Gudang ".$v_towarehouse;
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                 }
                            }
                            
                            
                            if($v_muttype=="out")
                            {
                                if($v_fromwarehouse=="")
                                {
                                    $msg = "From Warehouse harus dipilih";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                } 
                                
                                $v_towarehouse = "";   
                            }
                            
                            if($v_muttype=="mut")
                            {
                                // selain Rework mintain No Reference
                                //if( ($v_purpose!=6 && $v_purpose!=24 && $v_purpose!=7)  && $v_mrno=="")    
                                //{
                                //    $msg = "Reference harus diisi";
                                //    echo "<script>alert('".$msg."');</script>"; 
                                //    die();    
                                //}
                            }
                            
                            
                            // validasi, kalo return reject, baik harus isi batchnumber nya
                            if($v_purpose==34 || $v_purpose==7 || $v_purpose==33 || $v_purpose==32)
                            {
                                foreach($no as $key2=>$val2)
                                {
                                    if(!isset($_POST["v_matreqnumber_".$val2])){ $v_matreqnumber = isset($_POST["v_matreqnumber_".$val2]); } else { $v_matreqnumber = $_POST["v_matreqnumber_".$val2]; }
                                    
                                    if($v_matreqnumber=="")
                                    {
                                        $msg = "Untuk Retur, MR No harus diisi";
                                        echo "<script>alert('".$msg."');</script>"; 
                                        die();    
                                    } 
                                } 
                            }
                            
                            
                            $exp_imdate     = explode("-", $v_imdate);
                            $exp_imdate_old = explode("-", $v_imdate_old);
                            
                            $mmyyyy_imdate      = $exp_imdate[1]."-".$exp_imdate[2];
                            $mmyyyy_imdate_old  = $exp_imdate_old[1]."-".$exp_imdate_old[2];
                            
                            if($mmyyyy_imdate!=$mmyyyy_imdate_old)
                            {
                                $msg = "Tidak bisa edit tanggal berbeda bulan";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }

                        }
                        
                        
                        
                        if($v_halal=="on")
                        {
                            $v_halal_save = "1";
                        } 
                        else
                        {
                            $v_halal_save = "0";
                        }
                        
                        $q = "
                                UPDATE
                                    internalmutation
                                SET
                                    imdate = '".format_save_date($v_imdate)."',
                                    purpose = '".$v_purpose."',
                                    fromwarehouse = '".$v_fromwarehouse."',
                                    towarehouse = '".$v_towarehouse."',
                                    note = '".karakter_spesial($v_note, "input")."',
                                    halal = '".$v_halal_save."',
                                    editdate = NOW(),
                                    edituser = '".$ses_login."'
                                WHERE
                                    imno = '".$v_imno."'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Update";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                        else
                        {
                           // echo "masuk";
                            while(list($key,$val)=@each($no))
                            {
                                if(!isset($_POST["v_inventorycode_".$val])){ $v_inventorycode = isset($_POST["v_inventorycode_".$val]); } else { $v_inventorycode = $_POST["v_inventorycode_".$val]; }
                                if(!isset($_POST["v_stok_".$val])){ $v_stok = isset($_POST["v_stok_".$val]); } else { $v_stok = $_POST["v_stok_".$val]; }
                                if(!isset($_POST["v_request_".$val])){ $v_request = isset($_POST["v_request_".$val]); } else { $v_request = $_POST["v_request_".$val]; }
                                if(!isset($_POST["v_quantity_".$val])){ $v_quantity = isset($_POST["v_quantity_".$val]); } else { $v_quantity = $_POST["v_quantity_".$val]; }
                                if(!isset($_POST["v_quantity_old_".$val])){ $v_quantity_old = isset($_POST["v_quantity_old_".$val]); } else { $v_quantity_old = $_POST["v_quantity_old_".$val]; }
                                if(!isset($_POST["v_note_".$val])){ $v_note = isset($_POST["v_note_".$val]); } else { $v_note = $_POST["v_note_".$val]; }
                                if(!isset($_POST["v_note_old_".$val])){ $v_note_old = isset($_POST["v_note_old_".$val]); } else { $v_note_old = $_POST["v_note_old_".$val]; }
                                
                                if(!isset($_POST["v_matreqnumber_".$val])){ $v_matreqnumber = isset($_POST["v_matreqnumber_".$val]); } else { $v_matreqnumber = $_POST["v_matreqnumber_".$val]; }
                                if(!isset($_POST["v_matreqnumber_old_".$val])){ $v_matreqnumber_old = isset($_POST["v_matreqnumber_old_".$val]); } else { $v_matreqnumber_old = $_POST["v_matreqnumber_old_".$val]; }
                                
                                //echo "loop".$val."<hr>";
                                
                                $v_stok = save_int($v_stok);
                                $v_request = save_int($v_request);
                                $v_quantity = save_int($v_quantity);
                                $v_quantity_old = save_int($v_quantity_old);
                                
                                $v_matreqnumber = save_char($v_matreqnumber);
                                $v_matreqnumber_old = save_char($v_matreqnumber_old);
                                
                                $v_note = save_char($v_note);
                                $v_note_old = save_char($v_note_old);
                                
                                if($v_quantity_old!=$v_quantity || $v_note_old!=$v_note || $v_matreqnumber_old!=$v_matreqnumber)
                                {
                                    
                                    if( $v_quantity > ($v_quantity_old+$v_stok) && $v_inventorycode != "01-00097" && $v_inventorycode != "01-000971" && $v_inventorycode != "01-002421" && $v_inventorycode != "01-00242" && $v_muttype!="in" && false)
                                    {
                                        if($v_purpose!=37)
                                        {
                                            $msg = "Stok tidak mencukupi untuk item ".$v_inventorycode." Mohon di cek kembali";
                                            echo "<script>alert('".$msg."');</script>";     
                                            die();    
                                        }
                                        else
                                        {
                                            $q = "
                                                    UPDATE
                                                        internalmutationdetail
                                                    SET
                                                        quantity = '".$v_quantity."',
                                                        matreqnumber = '".$v_matreqnumber."',
                                                        note = '".$v_note."',
                                                        edituser = '".$ses_login."',
                                                        editdate = NOW()
                                                    WHERE
                                                        inventorycode = '".$v_inventorycode."'
                                                        AND imno = '".$v_imno."'
                                            ";
                                            
                                            if(!mysql_query($q))      
                                            {
                                                $msg = "Gagal Update Detail";
                                                echo "<script>alert('".$msg."');</script>";     
                                                die();
                                            }    
                                        }
                                            
                                    }
                                    else
                                    {
                                        $q = "
                                                UPDATE
                                                    internalmutationdetail
                                                SET
                                                    quantity = '".$v_quantity."',
                                                    matreqnumber = '".$v_matreqnumber."',
                                                    note = '".$v_note."',
                                                    edituser = '".$ses_login."',
                                                    editdate = NOW()
                                                WHERE
                                                    inventorycode = '".$v_inventorycode."'
                                                    AND imno = '".$v_imno."'
                                        ";
                                        
                                        if(!mysql_query($q))      
                                        {
                                            $msg = "Gagal Update Detail";
                                            echo "<script>alert('".$msg."');</script>";     
                                            die();
                                        }
                                    }
                                }
                                else
                                {
                                    echo "disini";
                                }
                                
                                
                            }
                            
                            // delete detail
                            foreach($v_del_details as $key => $val)
                            {
                                $del = "
                                        DELETE FROM
                                            internalmutationdetail
                                        WHERE
                                            inventorycode = '".$val."'    
                                            AND imno = '".$v_imno."'    
                                ";
                                if(!mysql_query($del))      
                                {
                                    $msg = "Gagal Delete Details";
                                    echo "<script>alert('".$msg."');</script>";     
                                    die();
                                }    
                            }
                            
                            update_bincard($v_imno);
                            
                            $msg = "Berhasil menyimpan";
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_imno."');</script>";         
                            die();
                        }
                    } 
                }

            }
            break;
        case "print_letter" :
            {                     
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
                
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();
                }
                
                $arr_epson = sintak_epson();
                
                $total_spasi = 155;
                $jml_detail  = 33;
                $ourFileName = "internal-mutation.txt";
                $nama        = "PT. NATURA PESONA MANDIRI";
                
                header("Content-type: application/txt");
                header("Content-Disposition: attachment; filename=$ourFileName");
                //$content = openFile($ourFileName);
                
                $q = "
                        SELECT
                            warehouse.warehousecode,
                            warehouse.warehousename
                        FROM
                            warehouse
                        WHERE
                            1
                        ORDER BY
                            warehouse.warehousecode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($warehousecode, $warehousename) = $row;
                    
                    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
                    
                    $arr_data["warehousename"][$warehousecode] = $warehousename;
                }

                $echo  = "";
                foreach($v_data as $key => $val)
                {
                    $imno = $val;
                    
                    $q = "
                            SELECT
                                internalmutation.imno,
                                internalmutation.imdate,
                                internalmutation.mrno,
                                intmutpurpose.purpose,
                                internalmutation.fromwarehouse,
                                internalmutation.towarehouse,
                                internalmutation.editdate,
                                internalmutation.note,
                                internalmutation.halal
                            FROM
                                internalmutation
                                INNER JOIN intmutpurpose ON
                                    intmutpurpose.purposeid = internalmutation.purpose
                                    AND internalmutation.imno = '".$imno."'
                            WHERE
                                1
                            LIMIT
                                0,1
                    ";
                    $qry_curr = mysql_query($q);
                    $arr_curr = mysql_fetch_array($qry_curr);
                    
                    if(substr($arr_curr["imno"],0,2)=="MP")
                    {
                        die("Produksi Menggunakan Kertas Setengah Letter");    
                    }
                    
                    $diterima = "";
                    if($arr_curr["towarehouse"]=="WH002")
                    {
                        $diterima = "Produksi";
                    }
                    
                    
                    $status_halal = false;
                    
                    if($arr_curr["mrno"])
                    {
                        $q = "
                                SELECT
                                    formula.formulaname,
                                    inventorymaster.halal
                                FROM
                                    formula
                                    INNER JOIN matreq ON
                                        formula.formulanumber = matreq.formulanumber
                                        AND matreq.matreqnumber = '".$arr_curr["mrno"]."'
                                    INNER JOIN inventorymaster ON
                                        formula.inventorycode = inventorymaster.inventorycode
                                WHERE
                                    1
                                LIMIT
                                    0,1
                        ";
                        $qry_formula = mysql_query($q);
                        $arr_formula = mysql_fetch_array($qry_formula);
                        
                        if($arr_formula["halal"]==1)
                        {
                            $status_halal = true;    
                        }
                    }        
                    
                    $q = "
                            SELECT
                                inventorymaster.stocktypeid,
                                internalmutationdetail.inventorycode,
                                inventorymaster.inventoryname,
                                internalmutationdetail.quantity,
                                inventorymaster.uominitial,
                                internalmutationdetail.note,
                                inventorymaster.halal
                            FROM
                                internalmutationdetail
                                INNER JOIN inventorymaster ON
                                    inventorymaster.inventorycode = internalmutationdetail.inventorycode
                            WHERE
                                1
                                AND internalmutationdetail.imno = '".$imno."'
                                AND (internalmutationdetail.quantity*1) != '0'
                            ORDER BY
                                internalmutationdetail.imdetailid ASC
                    ";
                    $counter = 1;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($stocktypeid, $inventorycode, $inventoryname, $quantity, $uominitial, $note, $halal) = $row;
                        
                        $arr_data["list_detail"][$arr_curr["imno"]][$counter] = $counter;
                        
                        $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$counter] = $stocktypeid;
                        $arr_data["detail_inventorycode"][$arr_curr["imno"]][$counter] = $inventorycode;
                        $arr_data["detail_inventoryname"][$arr_curr["imno"]][$counter] = $inventoryname;
                        $arr_data["detail_quantity"][$arr_curr["imno"]][$counter] = $quantity;
                        $arr_data["detail_uominitial"][$arr_curr["imno"]][$counter] = $uominitial;
                        $arr_data["detail_note"][$arr_curr["imno"]][$counter] = $note;
                        
                        $arr_data["detail_halal"][$arr_curr["imno"]][$counter] = $halal;
                        
                        $counter++;
                    }
                    
                    $curr_jml_detail = count($arr_data["list_detail"][$arr_curr["imno"]]);
                    
                    // cek halal detail
                    if(!$status_halal)
                    {
                        $status_halal = true;
                        foreach($arr_data["list_detail"][$imno] as $counter)
                        {
                            $halal = $arr_data["detail_halal"][$imno][$counter];
                            
                            // kalo ketemu gak halal, langsung break aja
                            if($halal==0)
                            {
                                $status_halal = false;
                                break;
                            }
                        }
                    }
                    
                    // direct langsung dari transaction
                    $status_halal = false;    
                    if($arr_curr["halal"]==1)
                    {
                        $status_halal = true;    
                    }
                    
                    if($status_halal)
                    {
                        $kode_dokumen = "FH.PC.01";
                        $kode_edisi   = "A";
                        $nama_cetakan = "ORDER PENGELUARAN BARANG HALAL";
                        $spasi_tanggal_terbit = 20;
                    }
                    else
                    {
                        $kode_dokumen = "F.PC.03";
                        $kode_edisi   = "C";
                        $nama_cetakan = "ORDER PENGELUARAN BARANG";
                        $spasi_tanggal_terbit = 23;
                    }
                    
                    
                    
                    $jml_page = ceil($curr_jml_detail/$jml_detail);
                    
                    for($i_page=1;$i_page<=$jml_page;$i_page++)
                    {
                        $echo.=chr(15);
                        //$echo .= $arr_data["esc8CpiOn"];
                        
                        // header
                        {
                            $echo.=chr(18);
                            $echo.=chr(15);
                            $echo.=$nama;
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.="";
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.="";
                            
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo.=" ";
                            }

                            $limit_spasi = 50;
                            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=$kode_dokumen;
                            $echo.="\r\n";    
                        }
                        
                        $echo.="Jl. Raya Denpasar Bedugul KM.36 Tabanan";
                        
                        $kurang = strlen("Jl. Raya Denpasar Bedugul KM.36 Tabanan");
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen($nama_cetakan)/2) - $kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $echo.=$nama_cetakan;
                        
                        for($i=0;$i<$spasi_tanggal_terbit;$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $tanggal_terbit      = substr($arr_curr["imdate"],0,10);
                        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
                        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
                        
                        $echo.="                TANGGAL TERBIT : 02/01/2015";
                        
                        $echo.="\r\n";
                        
                        $echo.="Bali 82191 - Indonesia";
                        $kurang = strlen("Bali 82191 - Indonesia");
                        
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen("No : ".$arr_curr["imno"])/2)-$kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $echo.="No : ".$arr_curr["imno"];
                        
                        
                        for($i=0;$i<24;$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $echo.="                Edisi / Revisi : ".$kode_edisi." /00";
                        
                        
                        $echo.="\r\n";
                        
                        $echo.="  ";
                        $kurang = strlen("  ");
                        
                        if($arr_formula["formulaname"])
                        {
                            $limit_spasi = ceil(($total_spasi/2)) - (strlen($arr_formula["formulaname"])/2)-$kurang;
                            
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.=$arr_formula["formulaname"];    
                        }
                        else
                        {
                            $echo.="\r\n";    
                        }
                        
                        $echo.="\r\n";
                        $echo.=$arr_data["esc8CpiOff"];
                        
                        $echo.="\r\n"; 
                        $echo.=chr(18);
                        $echo.=chr(15);
                        
                        // baris 1
                        {
                            $echo.="TANGGAL";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=": ";
                            
                            $echo.=format_show_date($arr_curr["imdate"]);
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["imdate"])));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $limit_spasi = 55;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.="GUDANG ASAL";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG ASAL"));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=": ";
                            $echo.=$arr_data["warehousename"][$arr_curr["fromwarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["fromwarehouse"]]));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.="\r\n";    
                        }

                        $echo.=chr(18);
                        $echo.=chr(15);
                        // baris 2
                        {
                            $echo.="NO REFERENSI";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("NO REFERENSI"));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=": ";
                            
                            $echo.=$arr_curr["mrno"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["mrno"]));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $limit_spasi = 55;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.="GUDANG TUJUAN";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG TUJUAN"));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=": ";
                            $echo.=$arr_data["warehousename"][$arr_curr["towarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["towarehouse"]]));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.="\r\n";    
                        }
                        
                        $echo.=chr(18);
                        $echo.=chr(15);
                        // baris 3
                        {
                            $echo.="TUJUAN";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TUJUAN"));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=": ";
                            
                            $echo.=$arr_curr["purpose"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["purpose"]));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $limit_spasi = 55;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.="";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.="  ";
                            $echo.=""; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            
                            $echo.="\r\n";    
                        }
                        $echo.=chr(18);
                        
                        $echo.="\r\n";
                        $echo.=chr(15);
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo.="-";
                        }
                        $echo.="\r\n";
                        
                        
                        $echo .= "NO";
                        $limit_spasi = 3;
                        for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $echo.="PCODE";
                        $limit_spasi = 14;
                        for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $echo.="NAMA BARANG";
                        $limit_spasi = 73;
                        for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                        {
                            $echo.=" ";
                        }
                        
                        
                        $limit_spasi = 20;
                        for($i=0;$i<($limit_spasi-strlen("JUMLAH  "));$i++)
                        {
                            $echo.=" ";
                        }
                        $echo.="JUMLAH  ";
                        
                        $echo.="";
                        $limit_spasi = 5;
                        for($i=0;$i<($limit_spasi-strlen(""));$i++)
                        {
                            $echo.=" ";
                        }
                        
                        
                        $echo.="KETERANGAN";
                        $limit_spasi = 40;
                        for($i=0;$i<($limit_spasi-strlen("KETERANGAN"));$i++)
                        {
                            $echo.=" ";
                        }
                        
                        $echo.="\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo.="-";
                        }
                        
                        $echo.="\r\n";
                        
                        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                        $no_end = $no + $jml_detail;
                        
                        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                        {
                            $stocktypeid = $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$i_detail];
                            $inventorycode = $arr_data["detail_inventorycode"][$arr_curr["imno"]][$i_detail];
                            $inventoryname = $arr_data["detail_inventoryname"][$arr_curr["imno"]][$i_detail];
                            $quantity = $arr_data["detail_quantity"][$arr_curr["imno"]][$i_detail];
                            $uominitial = $arr_data["detail_uominitial"][$arr_curr["imno"]][$i_detail];
                            $note = $arr_data["detail_note"][$arr_curr["imno"]][$i_detail];
                            
                            
                            
                            if($stocktypeid==1)
                            {
                                $decimal_php = 6;
                            }
                            else
                            {
                                $decimal_php = 0;
                            }
                            
                            $inventoryname = substr($inventoryname, 0, 72);
                            $note = substr($note, 0, 40);
                            
                            // kalo ada isi baru di print
                            if($inventorycode)
                            {
                                //if($no%2==0)
                                //{
                                    $echo.=$arr_epson["escUnerlineOn"];
                                //}
                                $echo.=chr(15);
                                $echo.=$no;
                                $limit_spasi = 3;
                                for($i=0;$i<($limit_spasi-strlen($no));$i++)
                                {
                                    $echo.=" ";
                                }
                                
                                $echo .= $inventorycode;
                                $limit_spasi = 14;
                                for($i=0;$i<($limit_spasi-strlen($inventorycode));$i++)
                                {
                                    $echo.=" ";
                                }
                                
                                $echo.=$inventoryname;
                                $limit_spasi = 73;
                                for($i=0;$i<($limit_spasi-strlen($inventoryname));$i++)
                                {
                                    $echo.=" ";
                                }
                                
                                
                                $limit_spasi = 20;
                                for($i=0;$i<($limit_spasi-strlen(format_number($quantity, $decimal_php)."  "));$i++)
                                {
                                    $echo.=" ";
                                }
                                $echo.=format_number($quantity, $decimal_php)."  ";
                                
                                $echo.=$uominitial;
                                $limit_spasi = 5;
                                for($i=0;$i<($limit_spasi-strlen($uominitial));$i++)
                                {
                                    $echo.=" ";
                                } 
                                
                                $echo.=$note;
                                $limit_spasi = 40;
                                for($i=0;$i<($limit_spasi-strlen($note));$i++)
                                {
                                    $echo.=" ";
                                }
                                $echo.=chr(18);
                                //if($no%2==0)
                                //{ 
                                    $echo.=$arr_epson["escUnerlineOff"];
                                //}
                            }
                            
                            $no++;
                            $echo.= "\r\n";
                        }
                        
                        $echo.=chr(15);
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo.="-";
                        }
                        $echo.=chr(18);
                        
                        $echo.=chr(15);
                        if($i_page==$jml_page)
                        {
                            $echo.="\r\n";
                            $echo.="Note : ".$arr_curr["note"];
                            $echo.="\r\n";
                            $echo.="\r\n";
                            
                            $echo.="                  Diterima                  ";
                            $echo.="          ";
                            $echo.="                 Mengetahui                 ";
                            $echo.="          ";
                            $echo.="                Dikeluarkan                ";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            
                            if($diterima=="")
                            {
                                $echo.="              (              )              ";
                            }
                            else
                            {
                                $echo.="              (   ".$diterima."   )              ";
                            }
                            $echo.="          ";
                            $echo.="              ( Manager PPIC )              ";
                            $echo.="          ";
                            $echo.="              ( Bagian Gudang )             ";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            
                            $echo.="\r\n";
                            $echo.="\r\n";
                        }
                        else
                        {
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";
                            $echo.="\r\n";    
                        }
                        $echo.=chr(18);
                        
                        $q = "
                                SELECT
                                    COUNT(noreferensi) AS jml_print
                                FROM
                                    log_print
                                WHERE
                                    1
                                    AND noreferensi = '".$imno."' 
                        ";
                        $qry_jml_print = mysql_query($q);
                        $row_jml_print = mysql_fetch_array($qry_jml_print);
                        
                        if($row_jml_print["jml_print"]*1>0)
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.="COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        else
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo.=" ";
                            }
                        
                            $echo.="HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        
                        
                        $echo.="\r\n";
                        $echo.="\r\n";
                        $echo.="\r\n";
                    
                    } // end page
                    
                    
                    // update counter print
                    if($ses_login!="hendri1003")
                    {
                        $q = "
                                INSERT
                                    log_print
                                SET
                                    form_data = 'internal_mutation',
                                    noreferensi = '".$imno."' , 
                                    userid = '".$ses_login."' , 
                                    print_date = NOW() , 
                                    print_page = 'Letter'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert Print Counter";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                    }
                }
                
                echo $echo;
            }
            break;
        case "print_letter_20160127" :
            {                     
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
                
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();
                }
                
                $arr_epson = sintak_epson();
                
                $total_spasi = 155;
                $jml_detail  = 36;
                $ourFileName = "internal-mutation.txt";
                $nama        = "PT. VICTORIA CARE INDONESIA";
                
                header("Content-type: application/txt");
                header("Content-Disposition: attachment; filename=$ourFileName");
                //$content = openFile($ourFileName);
                
                $q = "
                        SELECT
                            warehouse.warehousecode,
                            warehouse.warehousename
                        FROM
                            warehouse
                        WHERE
                            1
                        ORDER BY
                            warehouse.warehousecode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($warehousecode, $warehousename) = $row;
                    
                    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
                    
                    $arr_data["warehousename"][$warehousecode] = $warehousename;
                }

                $echo  = "";
                foreach($v_data as $key => $val)
                {
                    $imno = $val;
                    
                    $q = "
                            SELECT
                                internalmutation.imno,
                                internalmutation.imdate,
                                internalmutation.mrno,
                                intmutpurpose.purpose,
                                internalmutation.fromwarehouse,
                                internalmutation.towarehouse,
                                internalmutation.editdate,
                                internalmutation.note,
                                internalmutation.halal
                            FROM
                                internalmutation
                                INNER JOIN intmutpurpose ON
                                    intmutpurpose.purposeid = internalmutation.purpose
                                    AND internalmutation.imno = '".$imno."'
                            WHERE
                                1
                            LIMIT
                                0,1
                    ";
                    $qry_curr = mysql_query($q);
                    $arr_curr = mysql_fetch_array($qry_curr);
                    
                    if(substr($arr_curr["imno"],0,2)=="MP")
                    {
                        die("Produksi Menggunakan Kertas Setengah Letter");    
                    }
                    
                    $diterima = "";
                    if($arr_curr["towarehouse"]=="WH002")
                    {
                        $diterima = "Produksi";
                    }
                    
                    
                    $status_halal = false;
                    
                    if($arr_curr["mrno"])
                    {
                        $q = "
                                SELECT
                                    formula.formulaname,
                                    inventorymaster.halal
                                FROM
                                    formula
                                    INNER JOIN matreq ON
                                        formula.formulanumber = matreq.formulanumber
                                        AND matreq.matreqnumber = '".$arr_curr["mrno"]."'
                                    INNER JOIN inventorymaster ON
                                        formula.inventorycode = inventorymaster.inventorycode
                                WHERE
                                    1
                                LIMIT
                                    0,1
                        ";
                        $qry_formula = mysql_query($q);
                        $arr_formula = mysql_fetch_array($qry_formula);
                        
                        if($arr_formula["halal"]==1)
                        {
                            $status_halal = true;    
                        }
                    }        
                    
                    $q = "
                            SELECT
                                inventorymaster.stocktypeid,
                                internalmutationdetail.inventorycode,
                                inventorymaster.inventoryname,
                                internalmutationdetail.quantity,
                                inventorymaster.uominitial,
                                internalmutationdetail.note,
                                inventorymaster.halal
                            FROM
                                internalmutationdetail
                                INNER JOIN inventorymaster ON
                                    inventorymaster.inventorycode = internalmutationdetail.inventorycode
                            WHERE
                                1
                                AND internalmutationdetail.imno = '".$imno."'
                                AND (internalmutationdetail.quantity*1) != '0'
                            ORDER BY
                                internalmutationdetail.imdetailid ASC
                    ";
                    $counter = 1;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($stocktypeid, $inventorycode, $inventoryname, $quantity, $uominitial, $note, $halal) = $row;
                        
                        $arr_data["list_detail"][$arr_curr["imno"]][$counter] = $counter;
                        
                        $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$counter] = $stocktypeid;
                        $arr_data["detail_inventorycode"][$arr_curr["imno"]][$counter] = $inventorycode;
                        $arr_data["detail_inventoryname"][$arr_curr["imno"]][$counter] = $inventoryname;
                        $arr_data["detail_quantity"][$arr_curr["imno"]][$counter] = $quantity;
                        $arr_data["detail_uominitial"][$arr_curr["imno"]][$counter] = $uominitial;
                        $arr_data["detail_note"][$arr_curr["imno"]][$counter] = $note;
                        
                        $arr_data["detail_halal"][$arr_curr["imno"]][$counter] = $halal;
                        
                        $counter++;
                    }
                    
                    $curr_jml_detail = count($arr_data["list_detail"][$arr_curr["imno"]]);
                    
                    // cek halal detail
                    if(!$status_halal)
                    {
                        $status_halal = true;
                        foreach($arr_data["list_detail"][$imno] as $counter)
                        {
                            $halal = $arr_data["detail_halal"][$imno][$counter];
                            
                            // kalo ketemu gak halal, langsung break aja
                            if($halal==0)
                            {
                                $status_halal = false;
                                break;
                            }
                        }
                    }
                    
                    // direct langsung dari transaction
                    $status_halal = false;    
                    if($arr_curr["halal"]==1)
                    {
                        $status_halal = true;    
                    }
                    
                    if($status_halal)
                    {
                        $kode_dokumen = "FH.PC.01";
                        $kode_edisi   = "A";
                        $nama_cetakan = "ORDER PENGELUARAN BARANG HALAL";
                        $spasi_tanggal_terbit = 20;
                    }
                    else
                    {
                        $kode_dokumen = "F.PC.03";
                        $kode_edisi   = "C";
                        $nama_cetakan = "ORDER PENGELUARAN BARANG";
                        $spasi_tanggal_terbit = 23;
                    }
                    
                    
                    
                    $jml_page = ceil($curr_jml_detail/$jml_detail);
                    
                    for($i_page=1;$i_page<=$jml_page;$i_page++)
                    {
                        $echo .= $arr_data["esc8CpiOn"];
                        
                        // header
                        {
                            $echo .= $nama;
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }

                            $limit_spasi = 50;
                            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= $kode_dokumen;
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "Kawasan Industri Candi Blok 5A No. 8";
                        
                        $kurang = strlen("Kawasan Industri Candi Blok 5A No. 8");
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen($nama_cetakan)/2) - $kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= $nama_cetakan;
                        
                        for($i=0;$i<$spasi_tanggal_terbit;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $tanggal_terbit      = substr($arr_curr["imdate"],0,10);
                        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
                        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
                        
                        $echo .= "                TANGGAL TERBIT : 02/01/2015";
                        
                        $echo .="\r\n";
                        
                        $echo .= "Jl. Gatot Subroto Krapyak";
                        $kurang = strlen("Jl. Gatot Subroto Krapyak");
                        
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen("No : ".$arr_curr["imno"])/2)-$kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "No : ".$arr_curr["imno"];
                        
                        
                        for($i=0;$i<24;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "                Edisi / Revisi : ".$kode_edisi." /00";
                        
                        
                        $echo .="\r\n";
                        
                        $echo .= "SEMARANG - INDONESIA";
                        $kurang = strlen("SEMARANG - INDONESIA");
                        
                        if($arr_formula["formulaname"])
                        {
                            $limit_spasi = ceil(($total_spasi/2)) - (strlen($arr_formula["formulaname"])/2)-$kurang;
                            
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= $arr_formula["formulaname"];    
                        }
                        else
                        {
                            $echo .="\r\n";    
                        }
                        
                        $echo .="\r\n";
                        $echo .= $arr_data["esc8CpiOff"];
                        
                        $echo .="\r\n"; 
                        
                        // baris 1
                        {
                            $echo .= "TANGGAL";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= format_show_date($arr_curr["imdate"]);
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["imdate"])));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 55;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG ASAL";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG ASAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["fromwarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["fromwarehouse"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }

                        // baris 2
                        {
                            $echo .= "NO REFERENSI";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("NO REFERENSI"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["mrno"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["mrno"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 55;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG TUJUAN";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG TUJUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["towarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["towarehouse"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }
                        
                        // baris 3
                        {
                            $echo .= "TUJUAN";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TUJUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["purpose"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["purpose"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 55;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "  ";
                            $echo .= ""; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        $echo .= "\r\n";
                        
                        
                        $echo .= "NO";
                        $limit_spasi = 3;
                        for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "PCODE";
                        $limit_spasi = 14;
                        for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "NAMA BARANG";
                        $limit_spasi = 73;
                        for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $limit_spasi = 20;
                        for($i=0;$i<($limit_spasi-strlen("JUMLAH  "));$i++)
                        {
                            $echo .= " ";
                        }
                        $echo .= "JUMLAH  ";
                        
                        $echo .= "";
                        $limit_spasi = 5;
                        for($i=0;$i<($limit_spasi-strlen(""));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $echo .= "KETERANGAN";
                        $limit_spasi = 40;
                        for($i=0;$i<($limit_spasi-strlen("KETERANGAN"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        $echo .= "\r\n";
                        
                        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                        $no_end = $no + $jml_detail;
                        
                        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                        {
                            $stocktypeid = $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$i_detail];
                            $inventorycode = $arr_data["detail_inventorycode"][$arr_curr["imno"]][$i_detail];
                            $inventoryname = $arr_data["detail_inventoryname"][$arr_curr["imno"]][$i_detail];
                            $quantity = $arr_data["detail_quantity"][$arr_curr["imno"]][$i_detail];
                            $uominitial = $arr_data["detail_uominitial"][$arr_curr["imno"]][$i_detail];
                            $note = $arr_data["detail_note"][$arr_curr["imno"]][$i_detail];
                            
                            
                            
                            if($stocktypeid==1)
                            {
                                $decimal_php = 6;
                            }
                            else
                            {
                                $decimal_php = 0;
                            }
                            
                            $inventoryname = substr($inventoryname, 0, 72);
                            $note = substr($note, 0, 40);
                            
                            // kalo ada isi baru di print
                            if($inventorycode)
                            {
                                //if($no%2==0)
                                //{
                                    $echo .= $arr_epson["escUnerlineOn"];
                                //}
                                
                                $echo .= $no;
                                $limit_spasi = 3;
                                for($i=0;$i<($limit_spasi-strlen($no));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $inventorycode;
                                $limit_spasi = 14;
                                for($i=0;$i<($limit_spasi-strlen($inventorycode));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $inventoryname;
                                $limit_spasi = 73;
                                for($i=0;$i<($limit_spasi-strlen($inventoryname));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                
                                $limit_spasi = 20;
                                for($i=0;$i<($limit_spasi-strlen(format_number($quantity, $decimal_php)."  "));$i++)
                                {
                                    $echo .= " ";
                                }
                                $echo .= format_number($quantity, $decimal_php)."  ";
                                
                                $echo .= $uominitial;
                                $limit_spasi = 5;
                                for($i=0;$i<($limit_spasi-strlen($uominitial));$i++)
                                {
                                    $echo .= " ";
                                } 
                                
                                $echo .= $note;
                                $limit_spasi = 40;
                                for($i=0;$i<($limit_spasi-strlen($note));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                //if($no%2==0)
                                //{ 
                                    $echo .= $arr_epson["escUnerlineOff"];
                                //}
                            }
                            
                            $no++;
                            $echo .= "\r\n";
                        }
                        
                        
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        if($i_page==$jml_page)
                        {
                            $echo .= "\r\n";
                            $echo .= "Note : ".$arr_curr["note"];
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            $echo .= "                  Diterima                  ";
                            $echo .= "          ";
                            $echo .= "                 Mengetahui                 ";
                            $echo .= "          ";
                            $echo .= "                Dikeluarkan                ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            if($diterima=="")
                            {
                                $echo .= "              (              )              ";
                            }
                            else
                            {
                                $echo .= "              (   ".$diterima."   )              ";
                            }
                            $echo .= "          ";
                            $echo .= "              ( Manager PPIC )              ";
                            $echo .= "          ";
                            $echo .= "              ( Bagian Gudang )             ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                        }
                        else
                        {
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";    
                        }
                        
                        $q = "
                                SELECT
                                    COUNT(noreferensi) AS jml_print
                                FROM
                                    log_print
                                WHERE
                                    1
                                    AND noreferensi = '".$imno."' 
                        ";
                        $qry_jml_print = mysql_query($q);
                        $row_jml_print = mysql_fetch_array($qry_jml_print);
                        
                        if($row_jml_print["jml_print"]*1>0)
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        else
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                        
                            $echo .= "HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        
                        
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                    
                    } // end page
                    
                    
                    // update counter print
                    if($ses_login!="hendri1003")
                    {
                        $q = "
                                INSERT
                                    log_print
                                SET
                                    form_data = 'internal_mutation',
                                    noreferensi = '".$imno."' , 
                                    userid = '".$ses_login."' , 
                                    print_date = NOW() , 
                                    print_page = 'Letter'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert Print Counter";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                    }
                }
                
                echo $echo;
            }
            break;
        case "print_letter_20150415" :
            {                     
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
                
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();
                }
                
                $arr_epson = sintak_epson();
                
                $total_spasi = 125;
                $jml_detail  = 36;
                $ourFileName = "internal-mutation.txt";
                $nama        = "PT. VICTORIA CARE INDONESIA";
                
                header("Content-type: application/txt");
                header("Content-Disposition: attachment; filename=$ourFileName");
                //$content = openFile($ourFileName);
                
                $q = "
                        SELECT
                            warehouse.warehousecode,
                            warehouse.warehousename
                        FROM
                            warehouse
                        WHERE
                            1
                        ORDER BY
                            warehouse.warehousecode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($warehousecode, $warehousename) = $row;
                    
                    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
                    
                    $arr_data["warehousename"][$warehousecode] = $warehousename;
                }

                $echo  = "";
                foreach($v_data as $key => $val)
                {
                    $imno = $val;
                    
                    $q = "
                            SELECT
                                internalmutation.imno,
                                internalmutation.imdate,
                                internalmutation.mrno,
                                intmutpurpose.purpose,
                                internalmutation.fromwarehouse,
                                internalmutation.towarehouse,
                                internalmutation.editdate,
                                internalmutation.note,
                                internalmutation.halal
                            FROM
                                internalmutation
                                INNER JOIN intmutpurpose ON
                                    intmutpurpose.purposeid = internalmutation.purpose
                                    AND internalmutation.imno = '".$imno."'
                            WHERE
                                1
                            LIMIT
                                0,1
                    ";
                    $qry_curr = mysql_query($q);
                    $arr_curr = mysql_fetch_array($qry_curr);
                    
                    if(substr($arr_curr["imno"],0,2)=="MP")
                    {
                        die("Produksi Menggunakan Kertas Setengah Letter");    
                    }
                    
                    
                    $status_halal = false;
                    
                    if($arr_curr["mrno"])
                    {
                        $q = "
                                SELECT
                                    formula.formulaname,
                                    inventorymaster.halal
                                FROM
                                    formula
                                    INNER JOIN matreq ON
                                        formula.formulanumber = matreq.formulanumber
                                        AND matreq.matreqnumber = '".$arr_curr["mrno"]."'
                                    INNER JOIN inventorymaster ON
                                        formula.inventorycode = inventorymaster.inventorycode
                                WHERE
                                    1
                                LIMIT
                                    0,1
                        ";
                        $qry_formula = mysql_query($q);
                        $arr_formula = mysql_fetch_array($qry_formula);
                        
                        if($arr_formula["halal"]==1)
                        {
                            $status_halal = true;    
                        }
                    }
                    
                    $q = "
                            SELECT
                                inventorymaster.stocktypeid,
                                internalmutationdetail.inventorycode,
                                inventorymaster.inventoryname,
                                internalmutationdetail.quantity,
                                inventorymaster.uominitial,
                                internalmutationdetail.note,
                                inventorymaster.halal
                            FROM
                                internalmutationdetail
                                INNER JOIN inventorymaster ON
                                    inventorymaster.inventorycode = internalmutationdetail.inventorycode
                            WHERE
                                1
                                AND internalmutationdetail.imno = '".$imno."'
                                AND (internalmutationdetail.quantity*1) != '0'
                            ORDER BY
                                internalmutationdetail.imdetailid ASC
                    ";
                    $counter = 1;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($stocktypeid, $inventorycode, $inventoryname, $quantity, $uominitial, $note, $halal) = $row;
                        
                        $arr_data["list_detail"][$arr_curr["imno"]][$counter] = $counter;
                        
                        $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$counter] = $stocktypeid;
                        $arr_data["detail_inventorycode"][$arr_curr["imno"]][$counter] = $inventorycode;
                        $arr_data["detail_inventoryname"][$arr_curr["imno"]][$counter] = $inventoryname;
                        $arr_data["detail_quantity"][$arr_curr["imno"]][$counter] = $quantity;
                        $arr_data["detail_uominitial"][$arr_curr["imno"]][$counter] = $uominitial;
                        $arr_data["detail_note"][$arr_curr["imno"]][$counter] = $note;
                        
                        $arr_data["detail_halal"][$arr_curr["imno"]][$counter] = $halal;
                        
                        $counter++;
                    }
                    
                    $curr_jml_detail = count($arr_data["list_detail"][$arr_curr["imno"]]);
                    
                    // cek halal detail
                    if(!$status_halal)
                    {
                        $status_halal = true;
                        foreach($arr_data["list_detail"][$imno] as $counter)
                        {
                            $halal = $arr_data["detail_halal"][$imno][$counter];
                            
                            // kalo ketemu gak halal, langsung break aja
                            if($halal==0)
                            {
                                $status_halal = false;
                                break;
                            }
                        }
                    }
                    
                    // direct langsung dari transaction
                    $status_halal = false;    
                    if($arr_curr["halal"]==1)
                    {
                        $status_halal = true;    
                    }
                    
                    if($status_halal)
                    {
                        $kode_dokumen = "FH.PC.01";
                        $kode_edisi   = "A";
                        $nama_cetakan = "ORDER PENGELUARAN BARANG HALAL";
                        $spasi_tanggal_terbit = 20;
                    }
                    else
                    {
                        $kode_dokumen = "F.PC.03";
                        $kode_edisi   = "C";
                        $nama_cetakan = "ORDER PENGELUARAN BARANG";
                        $spasi_tanggal_terbit = 23;
                    }
                    
                    
                    
                    $jml_page = ceil($curr_jml_detail/$jml_detail);
                    
                    for($i_page=1;$i_page<=$jml_page;$i_page++)
                    {
                        $echo .= $arr_data["esc8CpiOn"];
                        
                        // header
                        {
                            $echo .= $nama;
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }

                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= $kode_dokumen;
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "Kawasan Industri Candi Blok 5A No. 8";
                        
                        $kurang = strlen("Kawasan Industri Candi Blok 5A No. 8");
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen($nama_cetakan)/2) - $kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= $nama_cetakan;
                        
                        for($i=0;$i<$spasi_tanggal_terbit;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $tanggal_terbit      = substr($arr_curr["imdate"],0,10);
                        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
                        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
                        
                        $echo .= "TANGGAL TERBIT : 02/01/2015";
                        
                        $echo .="\r\n";
                        
                        $echo .= "Jl. Gatot Subroto Krapyak";
                        $kurang = strlen("Jl. Gatot Subroto Krapyak");
                        
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen("No : ".$arr_curr["imno"])/2)-$kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "No : ".$arr_curr["imno"];
                        
                        
                        for($i=0;$i<24;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "Edisi / Revisi : ".$kode_edisi." /00";
                        
                        
                        $echo .="\r\n";
                        
                        $echo .= "SEMARANG - INDONESIA";
                        $kurang = strlen("SEMARANG - INDONESIA");
                        
                        if($arr_formula["formulaname"])
                        {
                            $limit_spasi = ceil(($total_spasi/2)) - (strlen($arr_formula["formulaname"])/2)-$kurang;
                            
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= $arr_formula["formulaname"];    
                        }
                        else
                        {
                            $echo .="\r\n";    
                        }
                        
                        $echo .="\r\n";
                        $echo .= $arr_data["esc8CpiOff"];
                        
                        $echo .="\r\n"; 
                        
                        // baris 1
                        {
                            $echo .= "TANGGAL";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= format_show_date($arr_curr["imdate"]);
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["imdate"])));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG ASAL";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG ASAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["fromwarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["fromwarehouse"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }

                        // baris 2
                        {
                            $echo .= "NO REFERENSI";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("NO REFERENSI"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["mrno"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["mrno"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG TUJUAN";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG TUJUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["towarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["towarehouse"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }
                        
                        // baris 3
                        {
                            $echo .= "TUJUAN";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TUJUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["purpose"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["purpose"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "  ";
                            $echo .= ""; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        $echo .= "\r\n";
                        
                        
                        $echo .= "NO";
                        $limit_spasi = 3;
                        for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "PCODE";
                        $limit_spasi = 14;
                        for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "NAMA BARANG";
                        $limit_spasi = 53;
                        for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $limit_spasi = 20;
                        for($i=0;$i<($limit_spasi-strlen("JUMLAH  "));$i++)
                        {
                            $echo .= " ";
                        }
                        $echo .= "JUMLAH  ";
                        
                        $echo .= "";
                        $limit_spasi = 5;
                        for($i=0;$i<($limit_spasi-strlen(""));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $echo .= "KETERANGAN";
                        $limit_spasi = 30;
                        for($i=0;$i<($limit_spasi-strlen("KETERANGAN"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        $echo .= "\r\n";
                        
                        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                        $no_end = $no + $jml_detail;
                        
                        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                        {
                            $stocktypeid = $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$i_detail];
                            $inventorycode = $arr_data["detail_inventorycode"][$arr_curr["imno"]][$i_detail];
                            $inventoryname = $arr_data["detail_inventoryname"][$arr_curr["imno"]][$i_detail];
                            $quantity = $arr_data["detail_quantity"][$arr_curr["imno"]][$i_detail];
                            $uominitial = $arr_data["detail_uominitial"][$arr_curr["imno"]][$i_detail];
                            $note = $arr_data["detail_note"][$arr_curr["imno"]][$i_detail];
                            
                            
                            
                            if($stocktypeid==1)
                            {
                                $decimal_php = 6;
                            }
                            else
                            {
                                $decimal_php = 0;
                            }
                            
                            $inventoryname = substr($inventoryname, 0, 50);
                            $note = substr($note, 0, 28);
                            
                            // kalo ada isi baru di print
                            if($inventorycode)
                            {
                                $echo .= $no;
                                $limit_spasi = 3;
                                for($i=0;$i<($limit_spasi-strlen($no));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $inventorycode;
                                $limit_spasi = 14;
                                for($i=0;$i<($limit_spasi-strlen($inventorycode));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $inventoryname;
                                $limit_spasi = 53;
                                for($i=0;$i<($limit_spasi-strlen($inventoryname));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                
                                $limit_spasi = 20;
                                for($i=0;$i<($limit_spasi-strlen(format_number($quantity, $decimal_php)."  "));$i++)
                                {
                                    $echo .= " ";
                                }
                                $echo .= format_number($quantity, $decimal_php)."  ";
                                
                                $echo .= $uominitial;
                                $limit_spasi = 5;
                                for($i=0;$i<($limit_spasi-strlen($uominitial));$i++)
                                {
                                    $echo .= " ";
                                } 
                                
                                $echo .= $note;
                                $limit_spasi = 30;
                                for($i=0;$i<($limit_spasi-strlen($note));$i++)
                                {
                                    $echo .= " ";
                                } 
                            }
                            
                            $no++;
                            $echo .= "\r\n";
                        }
                        
                        
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        if($i_page==$jml_page)
                        {
                            $echo .= "\r\n";
                            $echo .= "Note : ".$arr_curr["note"];
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            $echo .= "             Diterima             ";
                            $echo .= "          ";
                            $echo .= "            Mengetahui            ";
                            $echo .= "          ";
                            $echo .= "            Dikeluarkan           ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "         (              )         ";
                            $echo .= "          ";
                            $echo .= "         (              )         ";
                            $echo .= "          ";
                            $echo .= "         (              )         ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                        }
                        else
                        {
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";    
                        }
                        
                        $q = "
                                SELECT
                                    COUNT(noreferensi) AS jml_print
                                FROM
                                    log_print
                                WHERE
                                    1
                                    AND noreferensi = '".$imno."' 
                        ";
                        $qry_jml_print = mysql_query($q);
                        $row_jml_print = mysql_fetch_array($qry_jml_print);
                        
                        if($row_jml_print["jml_print"]*1>0)
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        else
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                        
                            $echo .= "HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        
                        
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                    
                    } // end page
                    
                    
                    // update counter print
                    if($ses_login!="hendri1003")
                    {
                        $q = "
                                INSERT
                                    log_print
                                SET
                                    form_data = 'internal_mutation',
                                    noreferensi = '".$imno."' , 
                                    userid = '".$ses_login."' , 
                                    print_date = NOW() , 
                                    print_page = 'Letter'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert Print Counter";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                    }
                }
                
                echo $echo;
            }
            break;
        case "print_setengah_letter" :
            {
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
                
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();
                }
                
                $arr_epson = sintak_epson();
                
                $total_spasi = 125;
                $jml_detail  = 7;
                $ourFileName = "internal-mutation.txt";
                $nama        = "PT. NATURA PESONA MANDIRI";
                
                header("Content-type: application/txt");
                header("Content-Disposition: attachment; filename=$ourFileName");
                //$content = openFile($ourFileName);
                
                $q = "
                        SELECT
                            warehouse.warehousecode,
                            warehouse.warehousename
                        FROM
                            warehouse
                        WHERE
                            1
                        ORDER BY
                            warehouse.warehousecode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($warehousecode, $warehousename) = $row;
                    
                    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
                    
                    $arr_data["warehousename"][$warehousecode] = $warehousename;
                }

                $echo  = "";
                foreach($v_data as $key => $val)
                {
                    $imno = $val;
                    
                    $q = "
                            SELECT
                                internalmutation.imno,
                                internalmutation.imdate,
                                internalmutation.mrno,
                                intmutpurpose.purposeid,
                                intmutpurpose.purpose,
                                internalmutation.fromwarehouse,
                                internalmutation.towarehouse,
                                internalmutation.editdate,
                                internalmutation.note,
                                internalmutation.halal
                            FROM
                                internalmutation
                                INNER JOIN intmutpurpose ON
                                    intmutpurpose.purposeid = internalmutation.purpose
                                    AND internalmutation.imno = '".$imno."'
                            WHERE
                                1
                            LIMIT
                                0,1
                    ";
                    $qry_curr = mysql_query($q);
                    $arr_curr = mysql_fetch_array($qry_curr);
                    
                    if(substr($arr_curr["imno"],0,2)=="MP")
                    { 
                        //$jml_detail  = 8;    
                        $jml_detail  = 9;    
                    }
                    
                    $status_halal = false;
                    
                    if($arr_curr["mrno"])
                    {
                        $q = "
                                SELECT
                                    formula.formulaname,
                                    inventorymaster.halal
                                FROM
                                    formula
                                    INNER JOIN matreq ON
                                        formula.formulanumber = matreq.formulanumber
                                        AND matreq.matreqnumber = '".$arr_curr["mrno"]."'
                                    INNER JOIN inventorymaster ON
                                        formula.inventorycode = inventorymaster.inventorycode
                                WHERE
                                    1
                                LIMIT
                                    0,1
                        ";
                        $qry_formula = mysql_query($q);
                        $arr_formula = mysql_fetch_array($qry_formula);
                        
                        if($arr_formula["halal"]==1)
                        {
                            $status_halal = true;    
                        }
                    }
                    
                    
                    
                    $q = "
                            SELECT
                                inventorymaster.stocktypeid,
                                internalmutationdetail.inventorycode,
                                inventorymaster.inventoryname,
                                internalmutationdetail.quantity,
                                inventorymaster.uominitial,
                                internalmutationdetail.note,
                                inventorymaster.halal,
                                internalmutationdetail.matreqnumber
                            FROM
                                internalmutationdetail
                                INNER JOIN inventorymaster ON
                                    inventorymaster.inventorycode = internalmutationdetail.inventorycode
                            WHERE
                                1
                                AND internalmutationdetail.imno = '".$imno."'
                                AND (internalmutationdetail.quantity*1) != '0'
                            ORDER BY
                                internalmutationdetail.imdetailid ASC
                    ";
                    $counter = 1;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($stocktypeid, $inventorycode, $inventoryname, $quantity, $uominitial, $note, $halal, $matreqnumber) = $row;
                        
                        $arr_data["list_detail"][$arr_curr["imno"]][$counter] = $counter;
                        
                        $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$counter] = $stocktypeid;
                        $arr_data["detail_inventorycode"][$arr_curr["imno"]][$counter] = $inventorycode;
                        $arr_data["detail_inventoryname"][$arr_curr["imno"]][$counter] = $inventoryname;
                        $arr_data["detail_quantity"][$arr_curr["imno"]][$counter] = $quantity;
                        $arr_data["detail_uominitial"][$arr_curr["imno"]][$counter] = $uominitial;
                        $arr_data["detail_note"][$arr_curr["imno"]][$counter] = $note;
                        $arr_data["detail_matreqnumber"][$arr_curr["imno"]][$counter] = $matreqnumber;
                        
                        $arr_data["list_matreqnumber"][$matreqnumber] = $matreqnumber;
                        
                        $arr_data["detail_halal"][$arr_curr["imno"]][$counter] = $halal;
                        
                        $counter++;
                    }
                    
                    
                    if(count($arr_data["list_matreqnumber"])>0)
                    {
                        $where_prod_matreq = where_array($arr_data["list_matreqnumber"], "productionbatchrequest.matreqnumber", "in");
                
                        $q = "
                                SELECT 
                                    productionbatchrequest.matreqnumber,
                                    productionbatchrequest.batchnumber
                                FROM
                                    productionbatchrequest
                                WHERE
                                    1
                                    ".$where_prod_matreq."
                                ORDER BY
                                    productionbatchrequest.matreqnumber ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($matreqnumber, $batchnumber) = $row;
                            
                            $arr_data["detail_batchnumber"][$matreqnumber] = $batchnumber;
                        }
                    }
                    
                    $curr_jml_detail = count($arr_data["list_detail"][$arr_curr["imno"]]);
                    $jml_page = ceil($curr_jml_detail/$jml_detail);
                    
                    // cek halal detail
                    if(!$status_halal)
                    {
                        $status_halal = true;
                        foreach($arr_data["list_detail"][$imno] as $counter)
                        {
                            $halal = $arr_data["detail_halal"][$imno][$counter];
                            
                            // kalo ketemu gak halal, langsung break aja
                            if($halal==0)
                            {
                                $status_halal = false;
                                break;
                            }
                        }
                    }
                    
                    // direct langsung dari transaksinya
                    $status_halal = false;    
                    if($arr_curr["halal"]==1)
                    {
                        $status_halal = true;    
                    }
                    
                    
                    // penormoran produksi
                    if(substr($arr_curr["imno"],0,2)=="MP" || substr($arr_curr["imno"],0,4)=="MPKS")
                    {           
                        $imno_echo = $arr_curr["imno"];
                        
                        if(substr($arr_curr["imno"],0,4)=="MPKS")
                        {
                            $spasi_tanggal_terbit = 36;
                            $nama_cetakan = "BUKTI RETUR BARANG";
                        }
                        
                        if(substr($arr_curr["imno"],0,4)!="MPKS")
                        {
                            if($status_halal)
                            {
                                if($arr_curr["purposeid"]=="37")
                                {
                                    $imno_echo = str_replace("MP", "MPKS", $arr_curr["imno"]);
                                }
                                else
                                {
                                    $imno_echo = str_replace("MP", "MPH", $arr_curr["imno"]);    
                                }
                                
                                // 37 kelebihan Supplier
                                if($arr_curr["purposeid"]=="7" || $arr_curr["purposeid"]=="32" || $arr_curr["purposeid"]=="33" || $arr_curr["purposeid"]=="34" || $arr_curr["purposeid"]=="37")
                                {
                                    $nama_cetakan = "BUKTI RETUR BARANG HALAL";
                                    $kode_dokumen = "FH.PR.03";
                                }
                                else
                                {   
                                    $nama_cetakan = "ORDER PENGELUARAN BARANG HALAL";
                                    $kode_dokumen = "FH.PR.03";
                                }
                                
                                $kode_edisi   = "A";
                                $spasi_tanggal_terbit = 20;
                            }
                            else
                            {
                                if($arr_curr["purposeid"]=="37")
                                {
                                    $imno_echo = str_replace("MP", "MPKS", $arr_curr["imno"]);    
                                }
                                
                                if($arr_curr["purposeid"]=="7" || $arr_curr["purposeid"]=="32" || $arr_curr["purposeid"]=="33" || $arr_curr["purposeid"]=="34" || $arr_curr["purposeid"]=="37")
                                {    
                                    $nama_cetakan = "BUKTI RETUR BARANG";
                                    $kode_dokumen = "F.PR.03";
                                }
                                else
                                {
                                    $nama_cetakan = "ORDER PENGELUARAN BARANG";
                                    $kode_dokumen = "F.PR.03";
                                }
                                                                                            ;
                                $kode_edisi   = "A";
                                $spasi_tanggal_terbit = 26;
                            }    
                        }
                    }
                    else
                    {
                        $imno_echo = $arr_curr["imno"];
                        
                        if($status_halal)
                        {
                            $kode_dokumen = "FH.PC.01";
                            $kode_edisi   = "A";
                            $nama_cetakan = "ORDER PENGELUARAN BARANG HALAL";
                            $spasi_tanggal_terbit = 20;
                        }
                        else
                        {
                            $kode_dokumen = "F.PC.03";
                            $kode_edisi   = "C";
                            $nama_cetakan = "ORDER PENGELUARAN BARANG";
                            $spasi_tanggal_terbit = 23;
                        }
                    }
                    
                    for($i_page=1;$i_page<=$jml_page;$i_page++)
                    {
                        $echo .= $arr_data["esc8CpiOn"];
                        
                        // header
                        {
                            $echo .= $nama;
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }

                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= $kode_dokumen;
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "Jl. Raya Denpasar Bedugul KM.36 Tabanan";
                        $kurang = strlen("Jl. Raya Denpasar Bedugul KM.36 Tabanan");
                        
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen($nama_cetakan)/2)-$kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= $nama_cetakan;
                        
                        for($i=0;$i<$spasi_tanggal_terbit;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $tanggal_terbit      = substr($arr_curr["imdate"],0,10);
                        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
                        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
                        
                        $echo .= "TANGGAL TERBIT : 02/01/2015";
                        
                        $echo .="\r\n";
                        
                        $echo .= "Bali 82191 - Indonesia";
                        $kurang = strlen("Bali 82191 - Indonesia");
                        
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen("No : ".$imno_echo)/2)-$kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "No : ".$imno_echo;
                        
                        for($i=0;$i<24;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "Edisi / Revisi : ".$kode_edisi." /00";
                        
                        $echo .="\r\n";
                        
                        $echo .= "  ";
                        $kurang = strlen("  ");
                        
                        if($arr_formula["formulaname"])
                        {
                            $limit_spasi = ceil(($total_spasi/2)) - (strlen($arr_formula["formulaname"])/2)-$kurang;
                            
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= $arr_formula["formulaname"];    
                        }
                        else
                        {
                            $echo .="\r\n";    
                        }
                        
                        $echo .="\r\n";
                        $echo .= $arr_data["esc8CpiOff"];
                        
                        $echo .="\r\n"; 
                        // baris 1
                        {
                            $echo .= "TANGGAL";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= format_show_date($arr_curr["imdate"]);
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["imdate"])));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG ASAL";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG ASAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["fromwarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["fromwarehouse"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }

                        
                        // baris 2
                        {
                            $echo .= "NO REFERENSI";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("NO REFERENSI"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["mrno"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["mrno"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG TUJUAN";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG TUJUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["towarehouse"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["towarehouse"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }
                        
                        // baris 3
                        {
                            $echo .= "TUJUAN";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TUJUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["purpose"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["purpose"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "  ";
                            $echo .= ""; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        $echo .= "\r\n";
                        
                        
                        $echo .= "NO";
                        $limit_spasi = 3;
                        for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "PCODE";
                        $limit_spasi = 14;
                        for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "NAMA BARANG";
                        $limit_spasi = 53;
                        for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $limit_spasi = 20;
                        for($i=0;$i<($limit_spasi-strlen("JUMLAH  "));$i++)
                        {
                            $echo .= " ";
                        }
                        $echo .= "JUMLAH  ";
                        
                        $echo .= "";
                        $limit_spasi = 5;
                        for($i=0;$i<($limit_spasi-strlen(""));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        if($arr_curr["purposeid"]=="7" || $arr_curr["purposeid"]=="32" || $arr_curr["purposeid"]=="33" || $arr_curr["purposeid"]=="34")
                        {
                            $echo .= "KETERANGAN";
                            $limit_spasi = 15;
                            for($i=0;$i<($limit_spasi-strlen("KETERANGAN"));$i++)
                            {
                                $echo .= " ";
                            } 
                            
                            $echo .= "BATCHNUMBER";
                            $limit_spasi = 15;
                            for($i=0;$i<($limit_spasi-strlen("BATCHNUMBER"));$i++)
                            {
                                $echo .= " ";
                            }       
                        }
                        else
                        {
                            $echo .= "KETERANGAN";
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen("KETERANGAN"));$i++)
                            {
                                $echo .= " ";
                            }   
                        }

                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        $echo .= "\r\n";
                        
                        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                        $no_end = $no + $jml_detail;
                        
                        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                        {
                            $stocktypeid = $arr_data["detail_stocktypeid"][$arr_curr["imno"]][$i_detail];
                            $inventorycode = $arr_data["detail_inventorycode"][$arr_curr["imno"]][$i_detail];
                            $inventoryname = $arr_data["detail_inventoryname"][$arr_curr["imno"]][$i_detail];
                            $quantity = $arr_data["detail_quantity"][$arr_curr["imno"]][$i_detail];
                            $uominitial = $arr_data["detail_uominitial"][$arr_curr["imno"]][$i_detail];
                            $note = $arr_data["detail_note"][$arr_curr["imno"]][$i_detail];
                            $matreqnumber = $arr_data["detail_matreqnumber"][$arr_curr["imno"]][$i_detail];
                            $batchnumber =  $arr_data["detail_batchnumber"][$matreqnumber];

                            if($stocktypeid==1)
                            {
                                $decimal_php = 6;
                            }
                            else
                            {
                                $decimal_php = 0;
                            }
                            
                            $inventoryname = substr($inventoryname, 0, 50);
                            $note = substr($note, 0, 28);
                            
                            // kalo ada isi baru di print
                            if($inventorycode)
                            {
                                $echo .= $no;
                                $limit_spasi = 3;
                                for($i=0;$i<($limit_spasi-strlen($no));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $inventorycode;
                                $limit_spasi = 14;
                                for($i=0;$i<($limit_spasi-strlen($inventorycode));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $inventoryname;
                                $limit_spasi = 53;
                                for($i=0;$i<($limit_spasi-strlen($inventoryname));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                
                                $limit_spasi = 20;
                                for($i=0;$i<($limit_spasi-strlen(format_number($quantity, $decimal_php)."  "));$i++)
                                {
                                    $echo .= " ";
                                }
                                $echo .= format_number($quantity, $decimal_php)."  ";
                                
                                $echo .= $uominitial;
                                $limit_spasi = 5;
                                for($i=0;$i<($limit_spasi-strlen($uominitial));$i++)
                                {
                                    $echo .= " ";
                                } 
                                
                                
                                if($arr_curr["purposeid"]=="7" || $arr_curr["purposeid"]=="32" || $arr_curr["purposeid"]=="33" || $arr_curr["purposeid"]=="34")
                                {
                                    $note = substr($note, 0, 15);
                                    
                                    $echo .= $note;
                                    $limit_spasi = 15;
                                    for($i=0;$i<($limit_spasi-strlen($note));$i++)
                                    {
                                        $echo .= " ";
                                    } 
                                    
                                    $batchnumber = substr($batchnumber, 0, 15);
                                    $echo .= $batchnumber;
                                    $limit_spasi = 15;
                                    for($i=0;$i<($limit_spasi-strlen($batchnumber));$i++)
                                    {
                                        $echo .= " ";
                                    }    
                                }
                                else
                                {
                                    $echo .= $note;
                                    $limit_spasi = 30;
                                    for($i=0;$i<($limit_spasi-strlen($note));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                } 
                            }
                            
                            $no++;
                            $echo .= "\r\n";
                        }
                        
                        
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        if($i_page==$jml_page)
                        {
                            if($arr_curr["purposeid"]=="7" || $arr_curr["purposeid"]=="32" || $arr_curr["purposeid"]=="33" || $arr_curr["purposeid"]=="34" || $arr_curr["purposeid"]=="37")
                            {
                                $echo .= "\r\n";
                                $echo .= "Note : ".$arr_curr["note"];
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                
                                $echo .= "          ";
                                $echo .= "    Diterima   ";
                                $echo .= "     ";
                                $echo .= "   Mengetahui  ";
                                $echo .= "     ";
                                $echo .= "   Mengetahui  ";
                                $echo .= "     ";
                                $echo .= "   Mengetahui  ";
                                $echo .= "     ";
                                $echo .= "  Dikeluarkan  ";
                                $echo .= "          ";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "          ";
                                $echo .= "  (Admin PPIC) ";
                                $echo .= "     ";
                                $echo .= "   (Mgr. PPIC) ";
                                $echo .= "     ";
                                $echo .= "  (Spv. Prod)  ";
                                $echo .= "     ";
                                $echo .= "   (Div. QC)   ";
                                $echo .= "     ";
                                $echo .= "  (Admin Prod) ";
                                $echo .= "          ";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                
                                $echo .= "\r\n";
                                //$echo .= "\r\n";    
                            }
                            else
                            {
                                $echo .= "\r\n";
                                $echo .= "Note : ".$arr_curr["note"];
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                
                                $echo .= "             Diterima             ";
                                $echo .= "          ";
                                $echo .= "            Mengetahui            ";
                                $echo .= "          ";
                                $echo .= "            Dikeluarkan           ";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                $echo .= "         (              )         ";
                                $echo .= "          ";
                                $echo .= "         (              )         ";
                                $echo .= "          ";
                                $echo .= "         (              )         ";
                                $echo .= "\r\n";
                                $echo .= "\r\n";
                                
                                $echo .= "\r\n";
                                //$echo .= "\r\n";    
                            }
                        }
                        else
                        {
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            //$echo .= "\r\n";    
                        }
                        
                        $q = "
                                SELECT
                                    COUNT(noreferensi) AS jml_print
                                FROM
                                    log_print
                                WHERE
                                    1
                                    AND noreferensi = '".$imno."' 
                        ";
                        $qry_jml_print = mysql_query($q);
                        $row_jml_print = mysql_fetch_array($qry_jml_print);
                        
                        if($row_jml_print["jml_print"]*1>0)
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        else
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                        
                            $echo .= "HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        
                        
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                    
                    } // end page
                    
                    
                    // update counter print
                    if($ses_login!="hendri1003")
                    {
                        $q = "
                                INSERT
                                    log_print
                                SET
                                    form_data = 'internal_mutation',
                                    noreferensi = '".$imno."' , 
                                    userid = '".$ses_login."' , 
                                    print_date = NOW() , 
                                    print_page = 'Letter'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert Print Counter";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                    }
                }
                
                echo $echo;
            }
            break;
    }
}

mysql_close($con);
?>

