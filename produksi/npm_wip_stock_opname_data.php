<?php
    include("header.php");
    
	$modul        = "WIP Stock Opname";
	$file_current = "npm_wip_stock_opname.php";
	$file_name    = "npm_wip_stock_opname_data.php";
	
	$ajax = $_REQUEST["ajax"];       

if($ajax)
{
    if($ajax=='search')
    {
		$v_warehouse   = trim($_GET["v_warehouse"]);
		$v_opname_date = trim($_GET["v_opname_date"]);
		
		// button
		{
			$q="
				SELECT 
				  stock_opname_wip.approval 
				FROM
				  stock_opname_wip 
				WHERE 1 
				  AND stock_opname_wip.opnamedate = '".format_save_date($v_opname_date)."' 
				  AND stock_opname_wip.warehousecode = '".$v_warehouse."' 
				GROUP BY stock_opname_wip.approval	
			";
			$qry = mysql_query($q);
			$r = mysql_fetch_array($qry);
            
            $sudah_ada = mysql_num_rows($qry);
		}
        
		// union
		{
			$q="
				SELECT 
				  unu.*,
				  formula.formulaname 
				FROM
				  (SELECT 
				    stock_opname_wip.batchnumber,
				    stock_opname_wip.warehousecode,
				    stock_opname_wip.formulanumber,
				    stock_opname_wip.physical_qty,
				    stock_opname_wip.data_qty,
				    stock_opname_wip.remarks,
				    stock_opname_wip.status_data 
				  FROM
				    stock_opname_wip 
				  WHERE 1 
				    AND stock_opname_wip.opnamedate = '".format_save_date($v_opname_date)."' 
				    AND stock_opname_wip.warehousecode = '".$v_warehouse."' 
				  UNION
				  SELECT 
				    production.batchnumber,
				    production.warehousecode,
				    production.formulanumber,
				    NULL AS physical_qty,
				    NULL AS data_qty,
				    NULL AS remarks,
				    'Regular' AS status_data 
				  FROM
				    production 
				  WHERE 1 
				    AND production.mixquantity * 1 != '0' 
				    AND production.status_batchnumber = 'Open'
                    AND production.productiondate <= '".format_save_date($v_opname_date)."'  
				    AND production.warehousecode = '".$v_warehouse."') AS unu 
				  INNER JOIN formula 
				    ON unu.formulanumber = formula.formulanumber 
				  GROUP BY unu.batchnumber,
					unu.warehousecode,
					unu.formulanumber 
				ORDER BY formula.formulaname ASC, unu.batchnumber ASC  
			";
			$counter=1;
			$qry = mysql_query($q);
			while($row = mysql_fetch_array($qry))
			{
				list($batchnumber,$warehousecode,$formulanumber,$physical_qty,$data_qty,$remarks,$status_data,$formulaname)=$row;
				
				$arr_data["list_opname_wip"][$counter]=$counter;	
				$arr_data["batchnumber"][$counter]=$batchnumber;
				$arr_data["warehousecode"][$counter]=$warehousecode;
				$arr_data["formulanumber"][$counter]=$formulanumber;
				$arr_data["physical_qty"][$counter]=$physical_qty;
				$arr_data["data_qty"][$counter]=$data_qty;
				$arr_data["remarks"][$counter]=$remarks;	
				$arr_data["status_data"][$counter]=$status_data;
				$arr_data["formulaname"][$counter]=$formulaname;
				
				$counter++;		
			}
		}
		
		$arr_bincard_wip = bincard_wip_stock($arr_data["batchnumber"], format_save_date($v_opname_date));
			
        $jml = count($arr_data["list_opname_wip"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }

    
	    if(count($arr_data["list_opname_wip"])!=0)
		{
			if($r["approval"]*1==0)
			{
				?>
					<div class="col-md-12" align="left">
				    	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_new_batchnumber" id="btn_new_batchnumber" value="ADD BATCHNUMBER" onclick="pop_up_addbatch('<?php echo format_save_date($v_opname_date); ?>','<?php echo $v_warehouse; ?>');">Add Batchnumber<i class="entypo-plus"></i></button>
				    </div>
				    <br/>
				<?php
			}
		}
	
    ?>
    
    <!--
    <div class="col-md-12" align="left">
				    	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_new_batchnumber" id="btn_new_batchnumber" value="ADD BATCHNUMBER" onclick="pop_up_addbatch('<?php echo format_save_date($v_opname_date); ?>','<?php echo $v_warehouse; ?>');">Add Batchnumber<i class="entypo-plus"></i></button>
				    </div>
				    <br/>
                    -->
    
    <div class="col-md-12">
    <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
    <input type="hidden" name="action" value="add_data">
    <input type="hidden" name="v_opnamedate" id="v_opnamedate" value="<?php echo $v_opname_date; ?>">
    <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $v_warehouse; ?>">
    
    <table class="table table-bordered responsive">
    	<thead>
			<tr >
				<th><strong><center>No</center></strong></th>
				<th><strong><center>Batchnumber</center></strong></th>
				<th><strong><center>Formula Name</center></strong></th>
				<th><strong><center>Physical Qty</center></strong></th>
				<th><strong><center>Data Qty</center></strong></th>
				<th><strong><center>Diff</center></strong></th>
				<th><strong><center>Remarks</center></strong></th>
				<th><strong><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center></strong></th>
			</tr>
		</thead>
		<tbody>
		    <?php
			if(count($arr_data["list_opname_wip"])==0)
			{
				echo "
					<tr height='20'>
						<td colspan='100%' align='center'>No Data</td>
					</tr>
				";	
			}
			else
			{
			
			    $nomor = 1;    
			    foreach($arr_data["list_opname_wip"] as $counter => $val)
			    {    
					$batchnumber = $arr_data["batchnumber"][$counter];
					$warehousecode = $arr_data["warehousecode"][$counter];
					$formulanumber = $arr_data["formulanumber"][$counter];
					$physical_qty = $arr_data["physical_qty"][$counter];
					$data_qty = $arr_data["data_qty"][$counter];
					$remarks = $arr_data["remarks"][$counter];	
					$status_data = $arr_data["status_data"][$counter];
					$formulaname = $arr_data["formulaname"][$counter];	
					
			        $sum_akhir = 0;
			        
			        if($status_data=="SO")
			        {
			            $sum_akhir = 0;     
			        }
			        else if($sudah_ada)
			        {
			           $sum_akhir = $data_qty;      
			        }
			        else
			        {
			            $sum_akhir = $arr_bincard_wip["sum_akhir"][$batchnumber];
			        }
			        
					$echo_diff = ($physical_qty*1)-($sum_akhir*1);
			        
			        $total["physical_qty"] += $physical_qty;
			        $total["data_qty"] += $sum_akhir;
					
				    ?>
				    <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$style_bg; ?>">
				        <td>
				        	<input type="hidden" name="no[]" value="<?php echo $nomor; ?>"/>
				        	<?php echo $nomor; ?>
				        	<input type="hidden" name="v_batchnumber_<?php echo $nomor; ?>" id="v_batchnumber_<?php echo $nomor; ?>" value="<?php echo $batchnumber; ?>"/>
				        	<input type="hidden" name="v_formulanumber_<?php echo $nomor; ?>" id="v_formulanumber_<?php echo $nomor; ?>" value="<?php echo $formulanumber; ?>"/>
				        	<input type="hidden" name="v_status_data_<?php echo $nomor; ?>" id="v_status_data_<?php echo $nomor; ?>" value="<?php echo $status_data; ?>"/>
				        </td>
				        <td><?php echo $batchnumber; ?></td>
				        <td><?php echo $formulaname; ?></td>
				        <td align="right">
				        <?php
				        if($r["approval"]*1==0)
				        {
				        ?>
				        	<input type="text" title="Double Klik u/ Copy Data Qty" name="v_physical_qty_<?php echo $nomor; ?>" id="v_physical_qty_<?php echo $nomor; ?>" value="<?php echo format_number($physical_qty,4); ?>" class="form-control-new" style="text-align: right;" onkeyup="calculate(<?php echo $nomor; ?>)" onBlur="toFormat4('v_physical_qty_<?php echo $nomor; ?>'), calculate(<?php echo $nomor; ?>)" ondblclick="copy_data_qty('<?php echo $nomor; ?>')" />
				        <?php
				        }
				        else
				        {
				        	?>
				        	<input type="hidden" name="v_physical_qty_<?php echo $nomor; ?>" value="<?php echo format_number($physical_qty,4); ?>" />
				        	<?php
				        	
							echo format_number($physical_qty,4);
						}
				        ?>
				        </td>
				        <td align="right">
				        	<?php echo format_number($sum_akhir,4); ?>
				        	<input type="hidden" name="v_data_qty_<?php echo $nomor; ?>" id="v_data_qty_<?php echo $nomor; ?>" value="<?php echo $sum_akhir; ?>" />
				        </td>
				        <td align="right" id="td_diff_<?php echo $nomor;?>">
				        	<?php echo format_number($echo_diff,4); ?>	
				        </td>
				        <td>
				         <?php
				        if($r["approval"]*1==0)
				        {
				        ?>
				        	<input type="text" name="v_remarks_<?php echo $nomor; ?>" id="v_remarks_<?php echo $nomor; ?>" value="<?php echo $remarks; ?>" class="form-control-new" style="width: 100%"/>
				        <?php
				        }
				        else
				        {
				        	?>
				        	<input type="hidden" name="v_remarks_<?php echo $nomor; ?>" value="<?php echo $remarks; ?>" />
				        	<?php
				        	                      
							echo $remarks;
						}
				        ?>
				        </td>
				        
				        <td>
				        	<?php
				        	if($status_data=="SO")
				        	{
				        		if($r["approval"]*1==0)
				        		{
									?>
									<center><img src="images/delete.gif" title="Hapus" onclick="CallAjax('ajax_del_detail','<?php echo $batchnumber; ?>')"/></center>
									<?php	
								}
							}
							else
							{
								echo "&nbsp;";	
							}
				        	?>
				        </td>
				    </tr>
				    <?php
				    	$nomor++;
				    }
				    ?>
	    
				    <tr>
				        <td colspan="3"><input type="hidden" name="v_no_counter" id="v_no_counter" value="<?php echo $nomor-1; ?> "/></td>
				        <td align="right" id="td_total_physical_qty"><b><?php echo format_number($total["physical_qty"],4); ?></b></td>
				        <td align="right" id="td_total_data_qty"><b><?php echo format_number($total["data_qty"],4); ?></b></td>
				        <td align="right" id="td_total_diff"><b><?php echo format_number($total["physical_qty"]-$total["data_qty"],4); ?></b></td>
				        <td colspan="2">&nbsp;</td>
				    </tr>
	    
				    <tr>
				    	<td colspan="100%" align="center">
				    	<?php
					    if($r["approval"]*1==0)
					    {
					    ?>
					    	<input type="submit" class="button_new" name="btn_save" id="btn_save" value="SAVE">
				    		<input type="submit" class="button_new" name="btn_send" id="btn_send" value="SAVE & SEND">
					    <?php
					    }
					    else
					    {
					    	$menu_validation_wip_approve = menu_validation("wip_stock_opname_approve", $ses_login); 
					    	
					    	if($menu_validation_wip_approve["u"])
				            {       
						    	if($r["approval"]*1==1)
						    	{
							    ?>
							    	<input type="submit" class="button_new" name="btn_approval" id="btn_approval" value="Approve">
							    <?php
							    }
							    else
							    {
							    ?>
							    	<input type="submit" class="button_new" name="btn_unapproval" id="btn_unapproval" value="Unapprove">
							    <?php
								}
							}
							else
							{
								echo "Anda tidak dapat otoritas untuk APPROVE";
							}
						}
					    ?>
			    		
			    	</td>
			    </tr>
    	<?php
			}
    		?>
    	</tbody>
    </table>
    </form>
    </div>
    <?php
    
    }
	
	else if($ajax=='ajax_del_detail')
	{
		$v_warehousecode   = trim($_GET["v_warehousecode"]);
		$v_opname_date = trim($_GET["v_opname_date"]);
		$v_batchnumber = trim($_GET["v_batchnumber"]);
		
		$q="
			DELETE FROM stock_opname_wip 
			WHERE 1 
				AND stock_opname_wip.opnamedate='".format_save_date($v_opname_date)."'
				AND stock_opname_wip.warehousecode='".$v_warehousecode."'
				AND stock_opname_wip.batchnumber='".$v_batchnumber."'
		";
		if(!mysql_query($q))
		{
            $msg = "Gagal hapus stock opname wip";
            echo "<script>alert('".$msg."');</script>";     
            die();
		}
	}
	
    exit();
}

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :
            {
            	if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
            	if(!isset($_POST["btn_send"])){ $btn_send = isset($_POST["btn_send"]); } else { $btn_send = $_POST["btn_send"]; }
            	if(!isset($_POST["btn_approval"])){ $btn_approval = isset($_POST["btn_approval"]); } else { $btn_approval = $_POST["btn_approval"]; }
            	if(!isset($_POST["btn_unapproval"])){ $btn_unapproval = isset($_POST["btn_unapproval"]); } else { $btn_unapproval = $_POST["btn_unapproval"]; }
            	
                if(!isset($_POST["v_opnamedate"])){ $v_opnamedate = isset($_POST["v_opnamedate"]); } else { $v_opnamedate = $_POST["v_opnamedate"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["no"])){ $no = isset($_POST["no"]); } else { $no = $_POST["no"]; }
                
                $v_opnamedate = format_save_date($v_opnamedate);
                
                if($btn_save || $btn_send)
                {
	                foreach($no as $key => $val)
	                {
	                	$nomor = $key*1+1;
	                	
						if(!isset($_POST["v_batchnumber_".$nomor])){ $v_batchnumber = isset($_POST["v_batchnumber_".$nomor]); } else { $v_batchnumber = $_POST["v_batchnumber_".$nomor]; }
						if(!isset($_POST["v_formulanumber_".$nomor])){ $v_formulanumber = isset($_POST["v_formulanumber_".$nomor]); } else { $v_formulanumber = $_POST["v_formulanumber_".$nomor]; }
						if(!isset($_POST["v_physical_qty_".$nomor])){ $v_physical_qty = isset($_POST["v_physical_qty_".$nomor]); } else { $v_physical_qty = $_POST["v_physical_qty_".$nomor]; }
						if(!isset($_POST["v_data_qty_".$nomor])){ $v_data_qty = isset($_POST["v_data_qty_".$nomor]); } else { $v_data_qty = $_POST["v_data_qty_".$nomor]; }
						if(!isset($_POST["v_remarks_".$nomor])){ $v_remarks = isset($_POST["v_remarks_".$nomor]); } else { $v_remarks = $_POST["v_remarks_".$nomor]; }
						if(!isset($_POST["v_status_data_".$nomor])){ $v_status_data = isset($_POST["v_status_data_".$nomor]); } else { $v_status_data = $_POST["v_status_data_".$nomor]; }
						
						$v_physical_qty = save_int($v_physical_qty);
						$v_data_qty     = save_int($v_data_qty);
						$v_remarks      = save_char($v_remarks);
						
						$q="
							DELETE FROM stock_opname_wip 
							WHERE 1 
								AND stock_opname_wip.opnamedate='".$v_opnamedate."'
								AND stock_opname_wip.warehousecode='".$v_warehousecode."'
								AND stock_opname_wip.batchnumber='".$v_batchnumber."'
								AND stock_opname_wip.formulanumber='".$v_formulanumber."'
						";
						if(!mysql_query($q))
						{
				            $msg = "Gagal hapus stock wip";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
						
						if($btn_send)
						{
							$approval = ",approval = '1'";
						}
						
						$id = get_counter_int("vci","stock_opname_wip","sid", 100);
						
						$q="
							INSERT
				                stock_opname_wip
				            SET
				                sid = '".$id."',
				                opnamedate = '".$v_opnamedate."', 
				                warehousecode = '".$v_warehousecode."' , 
				                batchnumber = '".$v_batchnumber."', 
				                formulanumber = '".$v_formulanumber."', 
				                physical_qty = '".$v_physical_qty."', 
				                data_qty = '".$v_data_qty."', 
				                remarks = '".$v_remarks."', 
				                author_user = '".$ses_login."',
				                author_date = NOW(),
				                edited_user = '".$ses_login."',
				                edited_date = NOW(),
				                status_data = '".$v_status_data."'
				                ".$approval."
						";
						if(!mysql_query($q))
						{
				            $msg = "Gagal Insert Stock Opname WIP";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
					}
				
					if($btn_send)
	                {
						$subject = 'Stock Opname WIP #'.format_show_date($v_opnamedate).'';
						
						$body = 'Stock Opname WIP tanggal '.format_show_date($v_opnamedate).' telah berhasil dibuat.<br>Mohon di lakukan Proses Approval<br>Terima Kasih';
						
						 $q = "
					        SELECT
					            function_email.email_name,
					            function_email.email_address
					        FROM
					            function_email
					        WHERE
					            1
					            AND function_email.func_name = 'wip_stock_opname'
					        ORDER BY
					            function_email.email_name ASC
					    ";
					    $qry = mysql_query($q);
					    while($row = mysql_fetch_array($qry))
					    {
					        $email_address .= $row["email_address"].";";
					        $email_name .= $row["email_name"].";";            
					    }
					    
					    send_email_multiple($email_name, $email, $pass, $host, $subject, $body, $email_address, $email_name);
					}
				}
                
                if($btn_approval)
                {
                	foreach($no as $key => $val)
	                {
	                	$nomor = $key*1+1;
	                	
						if(!isset($_POST["v_batchnumber_".$nomor])){ $v_batchnumber = isset($_POST["v_batchnumber_".$nomor]); } else { $v_batchnumber = $_POST["v_batchnumber_".$nomor]; }
						if(!isset($_POST["v_formulanumber_".$nomor])){ $v_formulanumber = isset($_POST["v_formulanumber_".$nomor]); } else { $v_formulanumber = $_POST["v_formulanumber_".$nomor]; }
						if(!isset($_POST["v_physical_qty_".$nomor])){ $v_physical_qty = isset($_POST["v_physical_qty_".$nomor]); } else { $v_physical_qty = $_POST["v_physical_qty_".$nomor]; }
						if(!isset($_POST["v_data_qty_".$nomor])){ $v_data_qty = isset($_POST["v_data_qty_".$nomor]); } else { $v_data_qty = $_POST["v_data_qty_".$nomor]; }
						if(!isset($_POST["v_remarks_".$nomor])){ $v_remarks = isset($_POST["v_remarks_".$nomor]); } else { $v_remarks = $_POST["v_remarks_".$nomor]; }
						if(!isset($_POST["v_status_data_".$nomor])){ $v_status_data = isset($_POST["v_status_data_".$nomor]); } else { $v_status_data = $_POST["v_status_data_".$nomor]; }
						
						$v_physical_qty = save_int($v_physical_qty);
						$v_data_qty     = save_int($v_data_qty);
						$v_remarks      = save_char($v_remarks);
						
						$q="
							DELETE FROM stock_opname_wip 
							WHERE 1 
								AND stock_opname_wip.opnamedate='".$v_opnamedate."'
								AND stock_opname_wip.warehousecode='".$v_warehousecode."'
								AND stock_opname_wip.batchnumber='".$v_batchnumber."'
								AND stock_opname_wip.formulanumber='".$v_formulanumber."'
						";
						if(!mysql_query($q))
						{
				            $msg = "Gagal hapus stock wip";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
						
						$diff = $v_physical_qty-$v_data_qty;
						
						$id = get_counter_int("vci","stock_opname_wip","sid", 100);
                        
                        if($v_status_data=="SO")
                        {
                            $q="
                                INSERT
                                    stock_opname_wip
                                SET
                                    sid = '".$id."',
                                    opnamedate = '".$v_opnamedate."', 
                                    warehousecode = '".$v_warehousecode."' , 
                                    batchnumber = '".$v_batchnumber."', 
                                    formulanumber = '".$v_formulanumber."', 
                                    physical_qty = '".$v_physical_qty."', 
                                    data_qty = '0.0000', 
                                    remarks = '".$v_remarks."', 
                                    author_user = '".$ses_login."',
                                    author_date = NOW(),
                                    edited_user = '".$ses_login."',
                                    edited_date = NOW(),
                                    status_data = '".$v_status_data."',
                                    approval = '2'
                            ";    
                        }
                        else
                        {
						
						    $q="
							    INSERT
				                    stock_opname_wip
				                SET
				                    sid = '".$id."',
				                    opnamedate = '".$v_opnamedate."', 
				                    warehousecode = '".$v_warehousecode."' , 
				                    batchnumber = '".$v_batchnumber."', 
				                    formulanumber = '".$v_formulanumber."', 
				                    physical_qty = '".$v_physical_qty."', 
				                    data_qty = '".$v_data_qty."', 
				                    remarks = '".$v_remarks."', 
				                    author_user = '".$ses_login."',
				                    author_date = NOW(),
				                    edited_user = '".$ses_login."',
				                    edited_date = NOW(),
				                    status_data = '".$v_status_data."',
				                    approval = '2'
						    ";
                        }
						if(!mysql_query($q))
						{
				            $msg = "Gagal Insert Stock Opname WIP";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
						
						if($diff*1!=0)
						{
							if($diff*1<0)
							{
								$where_so = "qout = '".abs($diff)."' ";	
							}
							else
							{
								$where_so = "qin = '".abs($diff)."' ";	
							}
							
							$q="
								DELETE FROM bincard_wip 
								WHERE 1 
									AND bincard_wip.carddate='".$v_opnamedate."'
									AND bincard_wip.batchnumber='".$v_batchnumber."'
									AND bincard_wip.module='SO'
									AND bincard_wip.warehousecode='".$v_warehousecode."'
							";
							if(!mysql_query($q))
							{
					            $msg = "Gagal hapus bincard wip";
					            echo "<script>alert('".$msg."');</script>";     
					            die();
							}
							
							$sid = get_counter_int("vci","bincard_wip","sid", 100);
							
							$q="
								INSERT
					                bincard_wip
					            SET
					                sid = '".$sid."',
					                carddate = '".$v_opnamedate."', 
					                batchnumber = '".$v_batchnumber."', 
					                module = 'SO', 
				                	warehousecode = '".$v_warehousecode."',
				                	".$where_so."
							";
							if(!mysql_query($q))
							{
					            $msg = "Gagal Insert bincard wip";
					            echo "<script>alert('".$msg."');</script>";     
					            die();
							}
						}
					
						if($v_status_data=="SO")
						{
							$q="
								DELETE FROM production 
								WHERE 1 
									AND production.batchnumber='".$v_batchnumber."'
									AND production.formulanumber='".$v_formulanumber."'
									AND production.warehousecode='".$v_warehousecode."'
							";
							if(!mysql_query($q))
							{
					            $msg = "Gagal hapus production";
					            echo "<script>alert('".$msg."');</script>";     
					            die();
							}
							
							$productionid = get_counter_int("vci","production","productionid", 100);
							
							$q="
								INSERT
					                production
					            SET
					                productionid = '".$productionid."',
					                batchnumber = '".$v_batchnumber."', 
					                productiondate = '".$v_opnamedate."', 
				                	formulanumber = '".$v_formulanumber."', 
				                	mixquantity = '".$diff."',
					                adduser = '".$ses_login."', 
					                adddate = NOW(), 
					                edituser = '".$ses_login."', 
					                editdate = NOW(), 
					                warehousecode = '".$v_warehousecode."',
					                remarks = '".$v_remarks."'
							";
							if(!mysql_query($q))
							{
					            $msg = "Gagal Insert bincard wip";
					            echo "<script>alert('".$msg."');</script>";     
					            die();
							}
						}
					}	
				}
				
				if($btn_unapproval)
                {
                	foreach($no as $key => $val)
	                {
	                	$nomor = $key*1+1;
	                	
						if(!isset($_POST["v_batchnumber_".$nomor])){ $v_batchnumber = isset($_POST["v_batchnumber_".$nomor]); } else { $v_batchnumber = $_POST["v_batchnumber_".$nomor]; }
						if(!isset($_POST["v_formulanumber_".$nomor])){ $v_formulanumber = isset($_POST["v_formulanumber_".$nomor]); } else { $v_formulanumber = $_POST["v_formulanumber_".$nomor]; }
						if(!isset($_POST["v_physical_qty_".$nomor])){ $v_physical_qty = isset($_POST["v_physical_qty_".$nomor]); } else { $v_physical_qty = $_POST["v_physical_qty_".$nomor]; }
						if(!isset($_POST["v_data_qty_".$nomor])){ $v_data_qty = isset($_POST["v_data_qty_".$nomor]); } else { $v_data_qty = $_POST["v_data_qty_".$nomor]; }
						if(!isset($_POST["v_remarks_".$nomor])){ $v_remarks = isset($_POST["v_remarks_".$nomor]); } else { $v_remarks = $_POST["v_remarks_".$nomor]; }
						if(!isset($_POST["v_status_data_".$nomor])){ $v_status_data = isset($_POST["v_status_data_".$nomor]); } else { $v_status_data = $_POST["v_status_data_".$nomor]; }
						
						$v_physical_qty = save_int($v_physical_qty);
						$v_data_qty     = save_int($v_data_qty);
						$v_remarks      = save_char($v_remarks);
						
						$q="
							DELETE FROM stock_opname_wip 
							WHERE 1 
								AND stock_opname_wip.opnamedate='".$v_opnamedate."'
								AND stock_opname_wip.warehousecode='".$v_warehousecode."'
								AND stock_opname_wip.batchnumber='".$v_batchnumber."'
								AND stock_opname_wip.formulanumber='".$v_formulanumber."'
						";
						if(!mysql_query($q))
						{
				            $msg = "Gagal hapus stock wip";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
						
						$diff = $v_physical_qty-$v_data_qty;
						
						$id = get_counter_int("vci","stock_opname_wip","sid", 100);
						
                        if($v_status_data=="SO")
                        {
                            $q="
                                INSERT
                                    stock_opname_wip
                                SET
                                    sid = '".$id."',
                                    opnamedate = '".$v_opnamedate."', 
                                    warehousecode = '".$v_warehousecode."' , 
                                    batchnumber = '".$v_batchnumber."', 
                                    formulanumber = '".$v_formulanumber."', 
                                    physical_qty = '".$v_physical_qty."', 
                                    data_qty = '0.0000', 
                                    remarks = '".$v_remarks."', 
                                    author_user = '".$ses_login."',
                                    author_date = NOW(),
                                    edited_user = '".$ses_login."',
                                    edited_date = NOW(),
                                    status_data = '".$v_status_data."',
                                    approval = '0'
                            ";    
                        }
                        else
                        {
						    $q="
							    INSERT
				                    stock_opname_wip
				                SET
				                    sid = '".$id."',
				                    opnamedate = '".$v_opnamedate."', 
				                    warehousecode = '".$v_warehousecode."' , 
				                    batchnumber = '".$v_batchnumber."', 
				                    formulanumber = '".$v_formulanumber."', 
				                    physical_qty = '".$v_physical_qty."', 
				                    data_qty = '".$v_data_qty."', 
				                    remarks = '".$v_remarks."', 
				                    author_user = '".$ses_login."',
				                    author_date = NOW(),
				                    edited_user = '".$ses_login."',
				                    edited_date = NOW(),
				                    status_data = '".$v_status_data."',
				                    approval = '0'
						    ";
                        }
						if(!mysql_query($q))
						{
				            $msg = "Gagal Insert Stock Opname WIP";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
											
						$q="
							DELETE FROM bincard_wip 
							WHERE 1 
								AND bincard_wip.carddate='".$v_opnamedate."'
								AND bincard_wip.batchnumber='".$v_batchnumber."'
								AND bincard_wip.module='SO'
								AND bincard_wip.warehousecode='".$v_warehousecode."'
						";
						if(!mysql_query($q))
						{
				            $msg = "Gagal hapus bincard wip";
				            echo "<script>alert('".$msg."');</script>";     
				            die();
						}
					
						if($v_status_data=="SO")
						{
							$q="
								DELETE FROM production 
								WHERE 1 
									AND production.batchnumber='".$v_batchnumber."'
									AND production.warehousecode='".$v_warehousecode."'
							";
							if(!mysql_query($q))
							{
					            $msg = "Gagal hapus production";
					            echo "<script>alert('".$msg."');</script>";     
					            die();
							}
						}
					}		
                }
				
                if($btn_save)
                {
					$msg = "Berhasil menyimpan";	
				}
                else if($btn_send)
                {
					$msg = "Berhasil menyimpan dan mengirim";	
				}
                else if($btn_approval)
                {
					$msg = "Berhasil approval";	
				}
                else if($btn_unapproval)
                {
					$msg = "Berhasil unpproval";	
				}
				
                echo "<script>alert('".$msg."');</script>";  
                echo "<script>parent.CallAjax('search');</script>";        
                die(); 
                
            }
            break;
    }
}

mysql_close($con);
?>

