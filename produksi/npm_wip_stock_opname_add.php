<?php
    include("header.php");
    
	$modul        = "Add Batchnumber";
	$file_current = "npm_stock_opname.php";
	$file_name    = "npm_wip_stock_opname_add.php";

	unset($arr_data, $arr_curr);

	if(!isset($_REQUEST["v_opnamedate"])){ $v_opnamedate = isset($_REQUEST["v_opnamedate"]); } else { $v_opnamedate = $_REQUEST["v_opnamedate"]; }
	if(!isset($_REQUEST["v_warehousecode"])){ $v_warehousecode = isset($_REQUEST["v_warehousecode"]); } else { $v_warehousecode = $_REQUEST["v_warehousecode"]; }

	if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
	if(!isset($_POST["v_bactnumber"])){ $v_bactnumber = isset($_POST["v_bactnumber"]); } else { $v_bactnumber = $_POST["v_bactnumber"]; }
	if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }

$ajax = $_REQUEST["ajax"];       

if($ajax)
{
	if($ajax=='ajax_search_fm')
	{
		$v_keyword_fm   = trim($_GET["v_keyword_fm"]);		
		
		$arr_keyword[0] = "formula.formulanumber";
        $arr_keyword[1] = "formula.formulaname";
        
        $where_keyword = search_keyword($v_keyword_fm, $arr_keyword);
		
		$q="
			SELECT 
			  formula.formulanumber,
			  formula.formulaname 
			FROM
			  formula 
			WHERE
			  1 
			  AND formula.isdefault = '1'
			  AND formula.isactive = '1'
			  ".$where_keyword."
			ORDER BY 
			  formula.formulaname ASC 
		";
		$qry = mysql_query($q);
		while($row = mysql_fetch_array($qry))
		{
			list($formulanumber,$formulaname)=$row;	
			
			$arr_data["list_fm_ajax"][$formulanumber]=$formulanumber;
			$arr_data["formulaname_ajax"][$formulanumber]=$formulaname;
			$arr_data["formulaname_ajax"][$formulanumber]=$formulanumber." :: ".$formulaname;
		}
        
        ?>
        <select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:250px;">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_fm_ajax"] as $formulanumber => $val)
                {
                    $formulaname_ajax = $arr_data["formulaname_ajax"][$formulanumber];
                    ?>
                    
                    <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname_ajax; ?> (Def)</option>
                    <?php
                }
            ?>
        </select>
        <?php
	}
	
	exit();
}

// formula
{
	$q="
		SELECT 
		  formula.formulanumber,
		  formula.formulaname 
		FROM
		  formula 
		WHERE 
		  1 
		  AND formula.isdefault = '1'
		  AND formula.isactive = '1'
		ORDER BY 
		  formula.formulaname ASC 
	";
	$qry = mysql_query($q);
	while($row = mysql_fetch_array($qry))
	{
		list($formulanumber,$formulaname)=$row;	
		
		$arr_data["list_fm"][$formulanumber]=$formulanumber;
		$arr_data["formulaname"][$formulanumber]=$formulaname;
		$arr_data["formulaname_view"][$formulanumber]=$formulanumber." :: ".$formulaname;
	}	
}

?>
<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta name="description" content="Neon Admin Panel" />
	    <meta name="author" content="" />

	    <title><?php echo $modul; ?> - Modul Produksi - NPM</title>
	    <link rel="shortcut icon" href="public/images/Logosg.png" >
	    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	    <link rel="stylesheet" href="assets/css/bootstrap.css">
	    <link rel="stylesheet" href="assets/css/neon-core.css">
	    <link rel="stylesheet" href="assets/css/neon-theme.css">
	    <link rel="stylesheet" href="assets/css/neon-forms.css">
	    <link rel="stylesheet" href="assets/css/custom.css">
	    <link rel="stylesheet" href="assets/css/skins/black.css">
	    <link rel="stylesheet" href="public/css/style.css">
	    <link rel="stylesheet" href="assets/css/my.css">

	    <script src="assets/js/jquery-1.11.0.min.js"></script>

	    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	
	<script>
  
		function createRequestObject()
		{
			var ro;
			var browser = navigator.appName;
			if(browser == "Microsoft Internet Explorer")
			{
				ro = new ActiveXObject("Microsoft.XMLHTTP");
			}else
			{
				ro = new XMLHttpRequest();
			}
			return ro;
		}

		var xmlhttp = createRequestObject();
		
		function CallAjax(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				variabel = "";

		        if(tipenya=='ajax_search_fm')
		        {  
		            variabel += "&v_keyword_fm="+param1;
		            
		            //alert(variabel);
		            
		            xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
		            xmlhttp.onreadystatechange = function() 
		            {
		                if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
		                {    
		                    document.getElementById("td_fm").innerHTML     = xmlhttp.responseText;
		                }

		                return false;
		            }
		            xmlhttp.send(null);
		        }
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
	</script>
	
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<div class="main-content">

		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Production</li>
			<li>WIP Stock Opname</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
			
		    <div class="col-md-12" align="left">
		    
				<ol class="breadcrumb">
					<li>
						<a href="javascript:void(0);">
							<i class="entypo-plus"></i>Add <?php echo $modul; ?>
						</a>
					</li>
				</ol>
				
				<form name='theform' id='theform' method="POST" enctype="multipart/form-data">
			    <input type="hidden" name="action" value="add_data">
			     
			     <table class="table table-bordered responsive">
			        
			        <tr>
			            <td class="title_table">Opname Date</td>
			            <td>
			            	<?php echo format_show_date($v_opnamedate); ?>
			                <input type="hidden" name="v_opnamedate" id="v_opnamedate" value="<?php echo $v_opnamedate; ?>"/>
			            </td>
			        </tr> 
			        
			        <tr>
			            <td class="title_table" width="150">Wharehouse</td>
			            <td>
			            	<?php echo $v_warehousecode; ?>
			                <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $v_warehousecode; ?>"/>
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="title_table" width="150">Batch Number</td>
			            <td>
			                <input type="text" name="v_bactnumber" id="v_bactnumber" class="form-control-new" size="15" maxlength="13" value="" >
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="title_table" width="150">Formula Number</td>
			            <td>
			                <b>Keyword</b> 
			                <input type="text" class="form-control-new" name="v_keyword_fm" id="v_keyword_fm" size="10" maxlength="50" value="" onkeyup="CallAjax('ajax_search_fm', this.value)">
			            	
			                <span id="td_fm">
			                <select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:250px;">
			                    <option value="">-</option>
			                    <?php 
			                        foreach($arr_data["list_fm"] as $formulanumber => $val)
			                        {
			                            $formulaname_view = $arr_data["formulaname_view"][$formulanumber];
			                            ?>
			                            
			                            <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname_view; ?> (Def)</option>
			                            <?php
			                        }
			                    ?>
			                </select>
			                </span>
			            </td>
			        </tr> 
			                
			        <tr>
			            <td>&nbsp;</td>
			            <td colspan="3">
			            	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
			            </td>
			        </tr>  
			                         
			    </table>
			    </form>
			</div>
		</div>
	</div>
</div>
</body>
</html>

<?php

if($btn_save)
{
	if($v_bactnumber=="")
	{
		$msg = "Batch Number harus diisi";
        echo "<script>alert('".$msg."')</script>";
        die();		
	}
	else if($v_formulanumber=="")
	{
		$msg = "Formula Number harus diisi";
        echo "<script>alert('".$msg."')</script>";
        die();		
	}
	else
	{
		$v_bactnumber    = trim($v_bactnumber);
		$v_formulanumber = trim($v_formulanumber);
		$v_warehousecode = trim($v_warehousecode);
		
		// cek
		$q="
			SELECT 
			  COUNT(production.batchnumber) AS jml 
			FROM
			  production 
			WHERE 1 
			  AND production.batchnumber = '".$v_bactnumber."'
  		";
  		$qry = mysql_query($q);
  		$r = mysql_fetch_array($qry);
  		
  		if($r["jml"]*1==0)
  		{
			$id = get_counter_int("vci","stock_opname_wip","sid", 100);
			
	        $q = "
	            INSERT
	                stock_opname_wip
	            SET
	                sid = '".$id."',
	                opnamedate = '".$v_opnamedate."', 
	                warehousecode = '".$v_warehousecode."' , 
	                batchnumber = '".$v_bactnumber."', 
	                formulanumber = '".$v_formulanumber."', 
	                data_qty = '0', 
	                author_user = '".GetUserLogin()."',
	                author_date = NOW(),
	                edited_user = '".GetUserLogin()."',
	                edited_date = NOW(),
	                status_data = 'SO'
	        ";
	        if(!mysql_query($q))      
	        {
	            $msg = "Gagal Insert Stock Opname WIP";
	            echo "<script>alert('".$msg."');</script>";     
	            die();
	        }
        }
        else
        {
            $msg = "Batch Number sudah tersedia di production";
            echo "<script>alert('".$msg."');</script>";     
            die();
		}
        
        echo "<script>window.opener.CallAjax('search');</script>";                                               
        echo "<script>self.close();</script>"; 
        die();  
	}
}

mysql_close($con);
?>

