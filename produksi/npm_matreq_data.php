<?php
    include("header.php");
   
   $modul        = "Material Request";
   $file_current = "npm_matreq.php";
   $file_name    = "npm_matreq_data.php";

function openFile($ourFileName)
{
    $file = file($ourFileName);        
    $content= "<pre>";
    for($l = 0;$l<count($file);$l++){
        $content .= $file[$l];            
    }
    $content .= "</pre>";
    $content =  substr($content,5,strlen($content)-5);
    $content =  substr($content,0,strlen($content)-6);
    return $content;
}

function sintak_epson()
{
    $arr_data["escNewLine"]      = chr(10);  // New line (LF line feed)
    $arr_data["escUnerlineOn"]   = chr(27).chr(45).chr(1);  // Unerline On
    $arr_data["escUnerlineOnx2"] = chr(27).chr(45).chr(2);  // Unerline On x 2
    $arr_data["escUnerlineOff"]  = chr(27).chr(45).chr(0);  // Unerline Off
    $arr_data["escBoldOn"]       = chr(27).chr(69).chr(1);  // Bold On
    $arr_data["escBoldOff"]      = chr(27).chr(69).chr(0);  // Bold Off
    $arr_data["escNegativeOn"]   = chr(29).chr(66).chr(1);  // White On Black On'
    $arr_data["escNegativeOff"]  = chr(29).chr(66).chr(0);  // White On Black Off
    $arr_data["esc8CpiOn"]       = chr(29).chr(33).chr(16); // Font Size x2 On
    $arr_data["esc8CpiOff"]      = chr(29).chr(33).chr(0);  // Font Size x2 Off
    $arr_data["esc16Cpi"]        = chr(27).chr(77).chr(48); // Font A  -  Normal Font
    $arr_data["esc20Cpi"]        = chr(27).chr(77).chr(49); // Font B - Small Font
    $arr_data["escReset"]        = chr(27).chr(64); //chr(27) + chr(77) + chr(48); // Reset Printer
    $arr_data["escFeedAndCut"]   = chr(29).chr(86).chr(65); // Partial Cut and feed

    $arr_data["escAlignLeft"]    = chr(27).chr(97).chr(48); // Align Text to the Left
    $arr_data["escAlignCenter"]  = chr(27).chr(97).chr(49); // Align Text to the Center
    $arr_data["escAlignRight"]   = chr(27).chr(97).chr(50); // Align Text to the Right
   
    $arr_data["reset"]  =chr(27).'@'; //reset semua pengaturan printer
    $arr_data["plength"]=chr(27).'C'; //tinggi kertas, contoh $plength.chr(33) ==> tinggi 33 baris.
    $arr_data["lmargin"]=chr(27).'l'; //margin kiri, pemakaian sama dengan $plength.
    $arr_data["cond"]   =chr(15);   //condensed
    $arr_data["ncond"]  =chr(18);   //end condensed
    $arr_data["dwidth"] =chr(27).'!'.chr(16);  //tulisan melebar
    $arr_data["ndwidth"]=chr(27).'!'.chr(1);   //end tulisan melebar
    $arr_data["draft"]  =chr(27).'x'.chr(48);  //font draft
    $arr_data["nlq"]    =chr(27).'x'.chr(49);
    $arr_data["bold"]   =chr(27).'E';   //tulisan bold
    $arr_data["nbold"]  =chr(27).'F';   //end tulisan bold
    $arr_data["uline"]  =chr(27).'!'.chr(129); //garis bawah
    $arr_data["nuline"] =chr(27).'!'.chr(1); //end garis bawah
    $arr_data["dstrik"] =chr(27).'G';    //double strike (tulisan lebih tebal, 2 kali strike)
    $arr_data["ndstrik"]=chr(27).'H';    //end double strike (tulisan lebih tebal, 2 kali strike)
    $arr_data["elite"]  =chr(27).'M';    //tulisan elite
    $arr_data["pica"]   =chr(27).'P';    //tulisan pica
    $arr_data["height"] =chr(27).'!'.chr(16); //tulisan tinggi
    $arr_data["nheight"]=chr(27).'!'.chr(1);  //end tulisan tinggi
    $arr_data["spasi05"]=chr(27)."3".chr(16); //spasi 5 char
    $arr_data["spasi1"] =chr(27)."3".chr(24); //spasi 1 char
    $arr_data["fcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';    // potong kertas full
    $arr_data["pcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';    // potong kertas sebagian
    $arr_data["op_cash"]=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);   //buka cash register
               
    return $arr_data;                          
}

$ajax = $_REQUEST["ajax"];      

if($ajax)
{
    if($ajax=='search')
    {
        $search_by              = trim($_GET["search_by"]);
        $v_type_date            = trim($_GET["v_type_date"]);
        $v_from_date            = trim($_GET["v_from_date"]);
        $v_to_date              = trim($_GET["v_to_date"]);
        $search_reqtype         = trim($_GET["search_reqtype"]);
        $search_keyword         = trim($_GET["search_keyword"]);
        $search_matreqnumber    = trim($_GET["search_matreqnumber"]);
        $search_formulanumber   = trim($_GET["search_formulanumber"]);
        $search_inventorycode   = trim($_GET["search_inventorycode"]);
        $search_prodplan        = trim($_GET["search_prodplan"]);
       
        $v_matreqnumber_curr    = $_GET["v_matreqnumber_curr"];
       
        $v_today        = date("d-m-Y");
        $v_last_7day    = date("d-m-Y", parsedate($v_today,"-")-(86400*7));
       
        $where = "";
        if($search_by=="data")
        {
            if($v_type_date=="today")
            {
                $where .= " AND matreq.matreqdate = '".format_save_date($v_today)."' ";
            }
            else if($v_type_date=="7_day_before")
            {
                $where .= " AND matreq.matreqdate BETWEEN '".format_save_date($v_last_7day)."' AND '".format_save_date($v_today)."' ";    
            }
            else if($v_type_date=="range_date")
            {
                $where .= " AND matreq.matreqdate BETWEEN '".format_save_date($v_from_date)."' AND '".format_save_date($v_to_date)."' ";
            }
           
            if($search_keyword)
            {
                $arr_keyword[0] = "matreq.matreqnumber";
                $arr_keyword[1] = "matreq.formulanumber";
                $arr_keyword[2] = "formula.formulaname";
                $arr_keyword[3] = "masterbarang.PCode";
                $arr_keyword[4] = "masterbarang.NamaLengkap";
           
                $where .= search_keyword($search_keyword, $arr_keyword);    
            }
           
            if($search_reqtype)
            {
                if($search_reqtype==3)
                {
                    $where .= " AND matreq.reqtype = '0' ";        
                }
                else
                {
                    $where .= " AND matreq.reqtype = '".$search_reqtype."' ";        
                }
            }
        }
        else if($search_by=="matreqnumber")
        {  
            $arr_keyword[0] = "matreq.matreqnumber";
           
            $where .= search_keyword($search_matreqnumber, $arr_keyword);
        }
        else if($search_by=="formulanumber")
        {  
            $arr_keyword[0] = "matreq.formulanumber";
           
            $where .= search_keyword($search_formulanumber, $arr_keyword);
        }
        else if($search_by=="inventorycode")
        {  
            $arr_keyword[0] = "masterbarang.PCode";
           
            $where .= search_keyword($search_inventorycode, $arr_keyword);
        }
        else if($search_by=="prodplan")
        {  
            $arr_keyword[0] = "matreq.prodplancode";
           
            $where .= search_keyword($search_prodplan, $arr_keyword);
        }
       
        $q = "
            SELECT
                matreq.matreqnumber,
                matreq.matreqdate,
                matreq.prodplancode,
                masterbarang.PCode,
                masterbarang.NamaLengkap,
                matreq.formulanumber,
                formula.formulaname,
                matreq.reqtype,
                matreq.adjustment,
                matreq.rework,
                matreq.status,
                matreq.pbnumber
            FROM
                matreq
                INNER JOIN formula ON
                    matreq.formulanumber = formula.formulanumber
                INNER JOIN masterbarang ON
                    formula.inventorycode = masterbarang.PCode
                    ".$where."
            WHERE
                1
            ORDER BY
                matreq.matreqnumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber, $matreqdate, $prodplancode, $PCode, $NamaLengkap, $formulanumber, $formulaname, $reqtype, $adjustment, $rework, $status, $pbnumber) = $row;
           
            $arr_data["list_data"][$matreqnumber] = $matreqnumber;
           
            $arr_data["matreqdate"][$matreqnumber] = $matreqdate;
            $arr_data["prodplancode"][$matreqnumber] = $prodplancode;
            $arr_data["PCode"][$matreqnumber] = $PCode;
            $arr_data["NamaLengkap"][$matreqnumber] = $NamaLengkap;
            $arr_data["formulanumber"][$matreqnumber] = $formulanumber;
            $arr_data["formulaname"][$matreqnumber] = $formulaname;
            $arr_data["reqtype"][$matreqnumber] = $reqtype;
            $arr_data["adjustment"][$matreqnumber] = $adjustment;
            $arr_data["rework"][$matreqnumber] = $rework;
            $arr_data["status"][$matreqnumber] = $status;
            $arr_data["pbnumber"][$matreqnumber] = $pbnumber;
           
        }
   
        $jml = count($arr_data["list_data"]);
       
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }

        $arr_data["echo_reqtype"][0] = "Baku + Kemas";
        $arr_data["echo_reqtype"][1] = "Baku";
        $arr_data["echo_reqtype"][2] = "Kemas";

    ?>
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_matreq_data.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
         <tr class="title_table">
            <td>No</td>
            <td>MR No</td>
            <td>Date</td>
            <td>Per. Barang</td>
            <td>Prod Plan</td>
            <td>Formula No</td>
            <td>Formula Name</td>
            <td>Req Type</td>
            <td>Type</td>
            <td>Status</td>
            <td>
               Action<br/>
               <input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')">
            </td>
         </tr>
      <tbody style="color: black;">
          <?php
         if(count($arr_data["list_data"])*1==0)
         {
            echo "
               <tr>
                  <td colspan='100%' align='center'>No Data</td>
               </tr>
            ";
         }
      
          $nomor = 0;    
          foreach($arr_data["list_data"] as $matreqnumber => $val)
          {    
              $nomor++;
              
              $matreqdate = $arr_data["matreqdate"][$matreqnumber];
              $prodplancode = $arr_data["prodplancode"][$matreqnumber];
              $inventorycode = $arr_data["inventorycode"][$matreqnumber];
              $inventoryname = $arr_data["inventoryname"][$matreqnumber];
              $formulanumber = $arr_data["formulanumber"][$matreqnumber];
              $formulaname = $arr_data["formulaname"][$matreqnumber];
              $reqtype = $arr_data["reqtype"][$matreqnumber];
              $adjustment = $arr_data["adjustment"][$matreqnumber];
              $rework = $arr_data["rework"][$matreqnumber];
              $status = $arr_data["status"][$matreqnumber];
              $pbnumber = $arr_data["pbnumber"][$matreqnumber];
              
              $style_color = "";
              if($v_matreqnumber_curr==$matreqnumber)
              {
                  $style_color = "background:#cafdb5;";    
              }
              
              $style_bg_status = "";
              $status_echo = "Open";
              if($status==2)
              {
                  $style_bg_status = "background: #FF0000";    
                  $status_echo = "VOID";
              }
              else if($status==1)
              {
                  $style_bg_status = "";
                  $status_echo = "Close";
              }
              
              if($adjustment==1)
              {
                  $status_type = "Adjustment";
              }
              else if($rework==1)
              {
                  $status_type = "Rework";
              }
              else
              {
                  $status_type = "Reguler";
              }
          ?>
          
          <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$style_bg; ?>">
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $nomor; ?></td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo $matreqnumber; ?></td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo format_show_date($matreqdate); ?></td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo $pbnumber; ?></td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo $prodplancode; ?></td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo $formulanumber; ?></td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $formulaname; ?>&nbsp;</td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $arr_data["echo_reqtype"][$reqtype]; ?>&nbsp;</td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $status_type; ?>&nbsp;</td>
              <td onclick="CallAjax('edit_data','<?php echo $matreqnumber; ?>')" style="<?php echo $style_bg.$style_bg_status; ?>" align="center"><?php echo $status_echo; ?></td>
              <td style="<?php echo $style_color.$style_bg; ?>" align="center">
                  <?php
                      if($status=="2")
                      {
                          echo "&nbsp;";    
                      }
                      else
                      {
                  ?>
                          <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $matreqnumber; ?>">
                  <?php
                      }
                  ?>
              </td>
          </tr>
          <?php
          }
          ?>
       
          <tr>
              <td colspan="100%" align="right">
                  <select name="action" id="action" class="form-control-new">
                      <option value="print_setengah_letter">Print 1/2 Letter</option>
                  </select>
                  <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_submit" id="btn_submit" value="Submit">Print<i class="entypo-print"></i></button>
                  &nbsp;&nbsp;
              </td>
          </tr>
       </tbody>
    </table>
    </form>
    </div>
    <?php
   
    }
    else if($ajax=="add_data")
    {    
       
        $q = "
            SELECT
                gudang.KdGudang,
                gudang.Keterangan
            FROM
                gudang
            WHERE
                1
                AND gudang.KdGudang IN ('15','05')
            ORDER BY
                gudang.KdGudang ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $warehousename) = $row;
           
            $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
           
            $arr_data["warehousename"][$warehousecode] = $warehousecode." :: ".$warehousename;
        }
       
       
        // batas 1 tahun yang lalu
        $v_startingperiod = date("Y-m-d",parsedate(date("d/m/Y"))-(86400*30*12));
       
        $q = "
            SELECT
                prodplan.prodplancode,
                prodplan.startingperiod,
                prodplan.endingperiod
            FROM
                prodplan
            WHERE
                1
                AND prodplan.startingperiod >= '".$v_startingperiod."'
            ORDER BY
                prodplan.startingperiod DESC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($prodplancode, $startingperiod, $endingperiod) = $row;
           
            $arr_data["list_prodplan"][$prodplancode] = $prodplancode;
           
            $arr_data["prodplan_period"][$prodplancode] = $prodplancode." - ".format_show_date($startingperiod)." to ".format_show_date($endingperiod);
        }
       
        ?>
        <div class="col-md-12" align="left">
       
         <ol class="breadcrumb">
            <li>
               <a href="javascript:void(0)">
                  <i class="entypo-pencil"></i>Add <?php echo $modul; ?>
               </a>
            </li>
         </ol>
         
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            <input type="hidden" name="ori_qty_kg" id="ori_qty_kg" value="">
            <input type="hidden" name="ori_packagingsize" id="ori_packagingsize" value="">
           
            <table class="table table-bordered responsive" style="color: black;">
               
                <tr>
                    <td class="title_table" width="150">Date</td>
                    <td>
                       <input type="text" class="form-control-new" size="10"  maxlength = "10" name="v_matreqdate" id="v_matreqdate" value="<?php echo date("d-m-Y"); ?>" >
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_matreqdate, 'dd-mm-yyyy');">
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Type</td>
                    <td>
                        <select name="v_matreq_type" id="v_matreq_type" class="form-control-new" onchange="change_matreq_type(this.value)" style="width:325px;">
                            <option value="Reguler">Reguler</option>
                            <option value="Adjustment">Adjustment</option>
                            <option value="Rework">Rework</option>
                        </select>
                    </td>
                </tr>
               
                <tr id="tr_matreqnumber_prev" style="display: none;">
                    <td class="title_table">MR Sebelum nya</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_matreqnumber_prev" id="v_matreqnumber_prev" value="" style="width: 325px;">
                    </td>
                </tr>
               
                <tr id="tr_kekurangan_supplier" style="display: none;">
                    <td class="title_table">&nbsp;</td>
                    <td style="font-weight: bold;">
                        <label><input type="checkbox" name="v_kekurangan_supplier" id="v_kekurangan_supplier"> Kekurangan Supplier</label>
                    </td>
                </tr>
               
                <tr id="tr_prod_plan_code">
                    <td class="title_table">Prod Plan Code</td>
                    <td>
                        <select name="v_prodplancode" id="v_prodplancode" class="form-control-new" onchange="CallAjax('choose_prodplancode', this.value)" style="width:325px;">
                            <option value="">-</option>
                            <?php
                                foreach($arr_data["list_prodplan"] as $prodplancode=> $val)
                                {
                                    $prodplan_period = $arr_data["prodplan_period"][$prodplancode];
                             ?>
                                    <option value="<?php echo $prodplancode; ?>"><?php echo $prodplan_period; ?></option>
                             <?php      
                                }
                            ?>    
                        </select>        
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">PCode</td>
                    <td>
                        <input type="text" class="form-control-new" size="10" maxlength="10" name="v_keyword_pcode" id="v_keyword_pcode" placeholder="Keyword" onkeyup="CallAjax('ajax_keyword_pcode', this.value)">
                        <span id="select_pcode">
                        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:244px;">
                           
                        </select>    
                        </span>
                    </td>
                </tr>
               
               
               <tr>
                    <td class="title_table">Formula Number</td>
                    <td id="td_form_formulanumber">
                        <select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;">
                           
                        </select>    
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Req Type</td>
                    <td>
                        <select name="v_reqtype" id="v_reqtype" class="form-control-new" style="width:325px;" onchange="change_reqtype(this.value)">
                            <option value="0">Baku + Kemas</option>
                            <option value="1">Baku</option>
                            <option value="2">Kemas</option>
                        </select>    
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Warehouse</td>
                    <td>
                        <select name="v_warehousecode" id="v_warehousecode" class="form-control-new" style="width:325px;">
                            <?php
                                foreach($arr_data["list_warehouse"] as $KdGudang=>$val)
                                {
                                    $Keterangan = $arr_data["warehousename"][$KdGudang];
                             ?>
                                    <option value="<?php echo $KdGudang; ?>"><?php echo $Keterangan; ?></option>
                             <?php      
                                }
                            ?>
                        </select>    
                    </td>
                </tr>
               
                <tr id="tr_batch_qty">
                    <td class="title_table">Batch Qty</td>
                    <td>
                        <input type="text" name="v_quantity" id="v_quantity" class="form-control-new" size="50" maxlength="100" value="" style="text-align: right;" onkeyup="calculate()" onblur="calculate()">
                    </td>
                </tr>
               
                <tr id="tr_qty_kg">
                    <td class="title_table">Qty KG</td>
                    <td id="td_qty_kg" style="font-weight:bold;">
                        &nbsp;    
                    </td>
                </tr>
               
                <tr id="tr_packagingsize">
                    <td class="title_table">Packagingsize</td>
                    <td id="td_packagingsize" style="font-weight:bold;">
                        &nbsp;    
                    </td>
                </tr>
               
                 <tr>
                    <td class="title_table">Approved By</td>
                    <td>
                        <input type="text" name="v_approvedby" id="v_approvedby" class="form-control-new" size="50" maxlength="100" value="">
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Note</td>
                    <td>
                        <input type="text" name="v_note" id="v_note" class="form-control-new" size="50" maxlength="100" value="">
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                       <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                    </td>
                </tr>
            </table>
            </form>
       </div>    
    <?php
    }
    else if($ajax=="edit_data")
    { // edit matreq hello
        $v_matreqnumber = $_GET["v_matreqnumber"];

        $q = "
                SELECT
                    *
                FROM
                    matreq
                WHERE
                    1
                    AND matreq.matreqnumber = '".$v_matreqnumber."'
                LIMIT
                    0,1
        ";
        $qry_curr = mysql_query($q);
        $arr_curr = mysql_fetch_array($qry_curr);
       
        $checkbox_kekurangan_supplier = "";
        if($arr_curr["kekurangan_supplier"])
        {
            $checkbox_kekurangan_supplier = "checked='checked'";
        }
       
        if($arr_curr["adjustment"]==1)
        {
            $type_echo = "Adjustment";
        }
        else if($arr_curr["rework"]==1)
        {
            $type_echo = "Rework";
        }
        else
        {
            $type_echo = "Reguler";
        }
       
        if($arr_curr["reqtype"]==0)
        {
            $reqtype_echo = "Baku + Kemas";
        }
        else if($arr_curr["reqtype"]==1)
        {
            $reqtype_echo = "Baku";
        }
        else if($arr_curr["reqtype"]==2)
        {
            $reqtype_echo = "Kemas";
        }
       
        if($type_echo=="Reguler")
        {
            $style_tr_prod_plan_code = "";  
            $style_tr_batch_qty = "";
            $style_tr_qty_kg = "";
            $style_tr_packagingsize = "";
            $style_tr_matreqnumber_prev = "none";
            $style_tr_kekurangan_supplier = "none";
        }
        else if($type_echo=="Adjustment" || $type_echo=="Rework")
        {
            $style_tr_prod_plan_code = "none";  
            $style_tr_batch_qty = "none";
            $style_tr_qty_kg = "none";
            $style_tr_packagingsize = "none";
           
            if($type_echo=="Adjustment")
            {  
                $style_tr_matreqnumber_prev = "";
                $style_tr_kekurangan_supplier = "";  
            }
            else
            {
                $style_tr_matreqnumber_prev = "none";
                $style_tr_kekurangan_supplier = "none";
            }
        }
       
       
        $arr_date = explode("-", $arr_curr["matreqdate"]);
       
        $year  = $arr_date[0];
        $month = $arr_date[1];
         
       
         $q = "
                SELECT
                    log_print.userid,
                    log_print.print_date,
                    log_print.print_page
                FROM
                    log_print
                WHERE
                    1
                    AND log_print.noreferensi = '".$v_matreqnumber."'
                    AND log_print.form_data = 'matreq'
                ORDER BY
                    log_print.print_date ASC
        ";
        $counter = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($userid, $print_date, $print_page) = $row;
           
            $arr_data["list_print"][$counter] = $counter;
           
            $arr_data["print_userid"][$counter] = $userid;
            $arr_data["print_print_date"][$counter] = $print_date;
            $arr_data["print_print_page"][$counter] = $print_page;
           
            $counter++;
        }
       
       
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                ORDER BY
                    gudang.KdGudang ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $Keterangan) = $row;
           
            $arr_data["list_warehouse"][$KdGudang] = $KdGudang;
           
            $arr_data["warehousename"][$KdGudang] = $KdGudang." :: ".$Keterangan;
        }
       
        $q = "
                SELECT
                    matreq.prodplancode,
                    prodplan.startingperiod,
                    prodplan.endingperiod
                FROM
                    matreq
                    INNER JOIN prodplan ON
                        prodplan.prodplancode = matreq.prodplancode
                        AND matreq.matreqnumber = '".$v_matreqnumber."'
                WHERE
                    1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($prodplancode, $startingperiod, $endingperiod) = $row;
        $prodplancode_echo = $prodplancode." - ".format_show_date($startingperiod)." to ".format_show_date($endingperiod);
       
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    masterbarang.PCode,
                    masterbarang.NamaLengkap,
                    formula.quantity,
                    formula.packagingsize
                FROM
                    formula
                    INNER JOIN masterbarang ON
                        formula.inventorycode = masterbarang.PCode
                        AND formula.formulanumber = '".$arr_curr["formulanumber"]."'
                WHERE
                    1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($formulanumber, $formulaname, $PCode, $NamaLengkap, $ori_qty_kg, $ori_packagingsize) = $row;
        $formulanumber_echo = $formulanumber." :: ".$formulaname;
        $inventorycode_echo = $PCode." :: ".$NamaLengkap;
       
        $q = "
                SELECT
                    formuladetail.inventorycode,
                    formula.quantity AS qty_kg,
                    formula.packagingsize,
                    formuladetail.percentage,
                    formuladetail.quantity
                FROM
                    formula
                    INNER JOIN formuladetail ON
                        formula.formulanumber = formuladetail.formulanumber
                        AND formula.formulanumber = '".$arr_curr["formulanumber"]."'
                WHERE
                    1
                ORDER BY
                    formula.inventorycode ASC
        ";
		
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $qty_kg, $packagingsize, $percentage, $quantity) = $row;
           
            if($percentage*1!=0)
            {
                $arr_data["std"][$inventorycode] = ($percentage/100) * $qty_kg * $arr_curr["quantity"];
                $arr_data["std_ori"][$inventorycode] = ($percentage/100) * $qty_kg;
            }
            else
            {
                $arr_data["std"][$inventorycode] = $quantity * $arr_curr["quantity"];
                $arr_data["std_ori"][$inventorycode] = $quantity;
            }
        }
       
        //echo "<pre>";
        //print_r($arr_data["std"]);
        //echo "</pre>";
       
        $q = "
                SELECT
                    matreqdetail.matregdetailid,
                    masterbarang.KdKategori,
                    masterbarang.Status,
                    matreqdetail.inventorycode,
                    masterbarang.NamaLengkap,
                    matreqdetail.quantity,
					masterbarang.SatuanSt
                FROM
                    matreqdetail
                    INNER JOIN masterbarang ON
                        matreqdetail.inventorycode = masterbarang.PCode
                        AND matreqdetail.matreqnumber = '".$v_matreqnumber."'
                WHERE
                    1
                ORDER BY
                    matreqdetail.matregdetailid ASC
        ";
	
        $counter = 1;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matregdetailid, $KdKategori, $status, $inventorycode, $NamaLengkap, $quantity, $SatuanSt) = $row;
           
            $arr_data["list_detail"][$counter] = $counter;
           
            $arr_data["list_inventory"][$inventorycode] = $inventorycode;
            $arr_data["status"][$inventorycode] = $status;
           
            $arr_data["detail_matregdetailid"][$counter] = $matregdetailid;
            $arr_data["detail_tipe"][$counter] = $KdKategori;
            $arr_data["detail_inventorycode"][$counter] = $inventorycode;
            $arr_data["detail_inventoryname"][$counter] = $NamaLengkap;
            $arr_data["detail_quantity"][$counter] = $quantity;
			$arr_data["detail_uominitial"][$counter] = $SatuanSt;
           
            $counter++;
        }
       
        $arr_stock = bincard_stock(sprintf("%02s", $month), $year, $arr_data["list_inventory"], $arr_curr["warehousecode"], "All");
       
        //echo "<pre>";
        //print_r($arr_data["list_inventory"]);
        //echo "</pre>";
       
        ?>
        <div class="col-md-12" align="left">
       
         <ol class="breadcrumb">
            <li>
               <a href="javascript:void(0)">
                  <i class="entypo-pencil"></i>Edit <?php echo $modul; ?>
               </a>
            </li>
         </ol>
         
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="edit_data">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_undel" id="v_undel" value="">
            <input type="hidden" name="ori_qty_kg" id="ori_qty_kg" value="<?php echo $ori_qty_kg; ?>">
            <input type="hidden" name="ori_packagingsize" id="ori_packagingsize" value="<?php echo $ori_packagingsize; ?>">
            <input type="hidden" size="10"  maxlength="10" name="v_matreqdate_old" id="v_matreqdate_old" value="<?php echo format_show_date($arr_curr["matreqdate"]); ?>" >
            <input type="hidden" name="v_matreq_type" id="v_matreq_type" value="<?php echo $type_echo; ?>">
           
            <table class="table table-bordered responsive" style="color: black;">
               
                <tr>
                    <td class="title_table" width="150">Matreq No</td>
                    <td>
                        <input type="hidden" size="30" maxlength="255" name="v_matreqnumber" id="v_matreqnumber" value="<?php echo $v_matreqnumber; ?>">
                        <?php echo "<b>".$v_matreqnumber."</b>";?>
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Date</td>
                    <td>
                        <input type="text" class="form-control-new" size="10"  maxlength = "10" name="v_matreqdate" id="v_matreqdate" value="<?php echo format_show_date($arr_curr["matreqdate"]); ?>" >
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_matreqdate, 'dd-mm-yyyy');">
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Type</td>
                    <td>
                        <b><?php echo $type_echo; ?></b>
                    </td>
                </tr>
               
               
                <tr id="tr_prod_plan_code" style="display:<?php echo $style_tr_prod_plan_code; ?>">
                    <td class="title_table">Prod Plan Code</td>
                    <td>
                        <b><?php echo $prodplancode_echo; ?></b>
                    </td>
                </tr>
               
                <tr id="tr_matreqnumber_prev" style="display:<?php echo $style_tr_matreqnumber_prev; ?>">
                    <td class="title_table">MR Sebelumnya</td>
                    <td>
                        <b><?php echo $arr_curr["matreqnumber_prev"]; ?></b>
                    </td>
                </tr>
               
                <tr id="tr_kekurangan_supplier" style="display: <?php echo $style_tr_kekurangan_supplier; ?>;">
                    <td class="title_table">&nbsp;</td>
                    <td style="font-weight: bold;">
                        <label><input type="checkbox" <?php echo $checkbox_kekurangan_supplier; ?> name="v_kekurangan_supplier" id="v_kekurangan_supplier"> Kekurangan Supplier</label>
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">PCode</td>
                    <td>
                        <b><?php echo $inventorycode_echo; ?></b>
                    </td>
                </tr>
               
               
               <tr>
                    <td class="title_table">Formula Number</td>
                    <td id="td_form_formulanumber">
                        <b><?php echo $formulanumber_echo; ?></b>    
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Req Type</td>
                    <td>
                        <b><?php echo $reqtype_echo; ?></b>
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Warehouse</td>
                    <td>
                        <b><?php echo $arr_data["warehousename"][$arr_curr["warehousecode"]]; ?></b>
                    </td>
                </tr>
               
                <tr id="tr_batch_qty" style="display:<?php echo $style_tr_batch_qty; ?>">
                    <td class="title_table">Batch Qty</td>
                    <td>
                        <input type="text" name="v_quantity" id="v_quantity" class="form-control-new" size="50" maxlength="100" value="<?php echo format_number($arr_curr["quantity"], 4); ?>" style="text-align: right;" onkeyup="calculate(), change_std()" onblur="calculate(), change_std()">
                        <button type="button" class="btn btn-info btn-sm" value="Copy Std To Qty" onClick="copy_std_to_qty();">Copy Std To Qty</button>
                    </td>
                </tr>
               
                <tr id="tr_qty_kg" style="display:<?php echo $style_tr_qty_kg; ?>">
                    <td class="title_table">Qty KG</td>
                    <td id="td_qty_kg" style="font-weight:bold;">
                        <?php echo format_number($arr_curr["quantity"]*$ori_qty_kg,4); ?>
                    </td>
                </tr>
               
                <tr id="tr_packagingsize" style="display:<?php echo $style_tr_packagingsize; ?>">
                    <td class="title_table">Packagingsize</td>
                    <td id="td_packagingsize" style="font-weight:bold;">
                        <?php echo format_number($arr_curr["quantity"]*$ori_packagingsize,0); ?>
                    </td>
                </tr>
               
                 <tr>
                    <td class="title_table">Approved By</td>
                    <td>
                        <input type="text" name="v_approvedby" id="v_approvedby" class="form-control-new" size="50" maxlength="100" value="<?php echo $arr_curr["approvedby"]; ?>">
                    </td>
                </tr>
               
                <tr>
                    <td class="title_table">Note</td>
                    <td>
                        <input type="text" name="v_note" id="v_note" class="form-control-new" size="50" maxlength="100" value="<?php echo $arr_curr["description"]; ?>">
                    </td>
                </tr>
               
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?php
                            $imno_echo = "";
                            $q = "
                                    SELECT
                                        internalmutation.imno
                                    FROM
                                        internalmutation
                                    WHERE
                                        1
                                        AND internalmutation.mrno = '".$v_matreqnumber."'
                                        AND internalmutation.status != '2'
                                    ORDER BY
                                        internalmutation.imno
                            ";
                            $qry = mysql_query($q);
                            while($row = mysql_fetch_array($qry))
                            {
                                list($imno) = $row;
                               
                                $imno_echo .= $imno.",";
                            }
                            $imno_echo = substr($imno_echo,0,-1);
                           
                            if($imno_echo)
                            {
                                ?>
                                    <font color="red">Button Save tidak muncul karena Matreq Sudah digunakan <?php echo $imno_echo; ?></font>
                                <?php    
                            }
                            else
                            {
                        ?>
                           <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button>
                           
                            <?php
                                if($arr_curr["status"]==1 || $arr_curr["status"]==0)
                                {
                            ?>
                            <button type="button" name="btn_delete" id="btn_delete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_delete('<?php echo $v_matreqnumber; ?>')" value="VOID Matreq">VOID MATREQ<i class="entypo-cancel"></i></button>
                            <?php
                                }
                                else
                                {
                            ?>
                            <button type="button" name="btn_undelete" id="btn_undelete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_undelete('<?php echo $v_matreqnumber; ?>')" value="UnVOID Matreq">UnVOID Matreq<i class="entypo-back"></i></button>
                            <?php        
                                }
                           
                            }
                           
                        ?>
                    </td>
                </tr>
               
                <?php
                    if($type_echo=="Reguler")
                    {
                ?>
               
                <tr style="font-weight:bold;">
                    <td colspan="100%">
                        <span style="background-color:#FFFF00;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Qty Berbeda Dengan Std
                    </td>
                </tr>
                <?php
                    }
                    else
                    {
                ?>
                <tr style="font-weight:bold;">
                    <td colspan="100%">&nbsp;</td>
                </tr>
                <?php        
                    }
                ?>
               
               
                <tr>
                    <td colspan="100%">
                        <table class="table table-bordered responsive">
                        <tr class="title_table">
                           <td>No</td>
                           <td>PCode</td>
                           <td>Nama Barang</td>
                           <td>Stok</td>
                           <td align="right">Std</td>
                           <td align="right">Qty</td>
                           <td>Unit</td>
                        </tr>
                     <tbody style="color: black;">
                     
                            <?php
                                $no = 1;
                                foreach($arr_data["list_detail"] as $counter => $val)
                                {
                                    $matregdetailid = $arr_data["detail_matregdetailid"][$counter];
                                    $stocktypeid = $arr_data["detail_stocktypeid"][$counter];
                                    $inventorycode = $arr_data["detail_inventorycode"][$counter];
                                    $inventoryname = $arr_data["detail_inventoryname"][$counter];
                                    $quantity = $arr_data["detail_quantity"][$counter];
                                    $uominitial = $arr_data["detail_uominitial"][$counter];
                                   
                                    $im_active = $arr_data["im_active"][$inventorycode];
                                    
                                    $std_ori = $arr_data["std_ori"][$inventorycode];
               
                                    if($stocktypeid==1)
                                    {
                                        $decimal_js = "6";    
                                        $decimal_php = 6;    
                                    }
                                    else
                                    {
                                        $decimal_js = "4";    
                                        $decimal_php = 4;    
                                    }
                                   
                                    $arr_total["total_baku"][$stocktypeid] += $quantity;
                                   
                                   
                                    $bg_color = "";
                                    if($type_echo=="Reguler")
                                    {
                                        if( number_format($arr_data["std"][$inventorycode],$decimal_php,".","") != number_format($quantity,$decimal_php,".","") )
                                        {
                                            $bg_color = "background-color:#FFFF00;";    
                                        }
                                    }
                                   
                                    // inactive
                                    /*$im_active_echo = "";
                                    if($im_active*1==0)
                                    {
                                        $im_active_echo = " <font color='red'>(InActive)</font>";
                                    }*/
                                   
                                   
                            ?>
                             <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                <td>
                                    <?php echo $no; ?>
                                    <input type="hidden" name="no[]" value="<?php echo $no; ?>">
                                    <input type="hidden" name="v_inventorycode_<?php echo $no; ?>" id="v_inventorycode_<?php echo $no; ?>" value="<?php echo $inventorycode; ?>">
                                    <input type="hidden" name="v_stocktypeid_<?php echo $no; ?>" id="v_stocktypeid_<?php echo $no; ?>" value="<?php echo $stocktypeid; ?>">
                                    <input type="hidden" name="v_std_ori_<?php echo $no; ?>" id="v_std_ori_<?php echo $no; ?>" value="<?php echo $std_ori; ?>">
                                   
                                    <input type="hidden" name="v_im_active_<?php echo $no; ?>" id="v_im_active_<?php echo $no; ?>" value="<?php echo $im_active; ?>">
                                </td>
                                <td><?php echo $inventorycode; ?></td>
                                <td><?php echo $inventoryname.$im_active_echo; ?></td>
                                <td align="right">&nbsp;<?php echo format_number($arr_stock["akhir"][$inventorycode], $decimal_php); ?></td>
                                <td align="right" id="td_std_<?php echo $no; ?>">&nbsp;<?php echo format_number($arr_data["std"][$inventorycode], $decimal_php); ?></td>
                                <td align="right" style="<?php echo $bg_color; ?>"><input type="text" class="form-control-new" size="10" style="text-align:right;" value="<?php echo format_number($quantity, $decimal_php); ?>" id="v_quantity_<?php echo $no; ?>" name="v_quantity_<?php echo $no; ?>" onblur="toFormat<?php echo $decimal_js; ?>('v_quantity_<?php echo $no; ?>')"></td>
                                <td><?php echo $uominitial; ?></td>
                            </tr>
                            <?php
                           
                                    $no++;  
                                }
                               
                                $no--;
                            ?>
                           
                            <?php
                                if($arr_curr["reqtype"]==0 || $arr_curr["reqtype"]==1)
                                {
                            ?>
                            <tr style="text-align:right; font-weight:bold;">
                                <td colspan="5">Total Baku</td>
                                <td>&nbsp;<?php echo format_number($arr_total["total_baku"][1], 4); ?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                                }
                            ?>
                           
                     </tbody>
                        </table>
                        <input type="hidden" name="v_jml_data" id="v_jml_data" value="<?php echo $no; ?>">
                    </td>
                </tr>
               
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
               
                <tr height="25" class="title_table">
                    <td colspan="100%">Informasi Data</td>
                </tr>
               
                <tr height="25">
                    <td class="title_table">Dibuat</td>
                    <td><?php echo format_show_datetime($arr_curr["adddate"])." :: ".$arr_curr["adduser"]; ?></td>
                </tr>
               
                <tr height="25">
                    <td class="title_table">Diedit</td>
                    <td><?php echo format_show_datetime($arr_curr["editdate"])." :: ".$arr_curr["edituser"]; ?></td>
                </tr>
               
                <?php
                    if(count($arr_data["list_print"])*1>0)
                    {
                ?>
               
                <tr height="25">
                    <td class="title_table" valign="top">Printed</td>
                    <td>
                        <?php
                            $echo  = "<table class='table table-bordered responsive' style='color : black;' >";
                            $echo .= "<tr style='font-weight:bold;'>";
                            $echo .= "<td width='30'>No</td>";
                            $echo .= "<td align='center'>Date</td>";
                            $echo .= "<td>User</td>";
                            $echo .= "<td>Page</td>";
                            $echo .= "</tr>";
                           
                            $no = 1;
                            foreach($arr_data["list_print"] as $counter => $val)
                            {
                                $userid = $arr_data["print_userid"][$no];
                                $print_date = $arr_data["print_print_date"][$no];
                                $print_page = $arr_data["print_print_page"][$no];
                               
                                $echo .= "<tr>";
                                $echo .= "<td>".$counter."</td>";
                                $echo .= "<td align='center'>".format_show_datetime($print_date)."</td>";
                                $echo .= "<td>".$userid."</td>";
                                $echo .= "<td>".$print_page."</td>";
                                $echo .= "</tr>";
                               
                                $no++;
                            }
                            $echo  .= "</table>";
                           
                            echo $echo;
                        ?>    
                    </td>
                </tr>
                <?php
                    }
                ?>
               
            </table>
            </form>
       
        </div>
        <?php    
    }
    else if($ajax=="ajax_list_pcode")
    {
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                FROM
                    masterbarang
                WHERE
                    1
                    AND masterbarang.Status = 'A'
                    AND masterbarang.KdKategori = '3'
                ORDER BY
                    masterbarang.PCode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($PCode, $NamaLengkap) = $row;
           
            $arr_data["list_pcode"][$PCode] = $PCode;
            $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
        }
       
        ?>
        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:235px;" onchange="CallAjax('choose_pcode')">
            <option value="">-</option>
            <?php
                foreach($arr_data["list_pcode"] as $PCode=>$val)
                {
                    $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
            ?>
            <option value="<?php echo $PCode; ?>"><?php echo $NamaLengkap; ?></option>                    
            <?php
                }
            ?>
        </select>
       
        <?php
    }
    else if($ajax=="choose_prodplancode")
    {
        $v_prodplancode = $_GET["v_prodplancode"];
       
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                FROM
                    masterbarang
                    INNER JOIN prodplandetail ON
                        masterbarang.PCode = prodplandetail.inventorycode    
                        AND prodplandetail.prodplancode = '".$v_prodplancode."'
                WHERE
                    1
                ORDER BY
                    masterbarang.PCode ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($PCode, $NamaLengkap) = $row;
           
            $arr_data["list_pcode"][$PCode] = $PCode;
            $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
        }
       
        ?>
        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:235px;" onchange="CallAjax('choose_pcode')">
            <option value="">-</option>
            <?php
                foreach($arr_data["list_pcode"] as $PCode=>$val)
                {
                    $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
            ?>
            <option value="<?php echo $PCode; ?>"><?php echo $NamaLengkap; ?></option>                    
            <?php
                }
            ?>
        </select>
        <?php    
    }
    else if($ajax=="ajax_keyword_pcode")
    {
        $v_keyword_pcode  = save_char($_GET["v_keyword_pcode"]);
        $v_matreq_type    = $_GET["v_matreq_type"];
        $v_prodplancode   = $_GET["v_prodplancode"];
       
        $arr_keyword[0] = "masterbarang.PCode";
        $arr_keyword[1] = "masterbarang.NamaLengkap";
   
        $where_keyword  = search_keyword($v_keyword_pcode, $arr_keyword);
       
        if($v_matreq_type=="Reguler")
        {
            $q = "
                    SELECT
                        masterbarang.PCode,
                        masterbarang.NamaLengkap
                    FROM
                        masterbarang
                        INNER JOIN prodplandetail ON
                            masterbarang.PCode = prodplandetail.inventorycode    
                            AND prodplandetail.prodplancode = '".$v_prodplancode."'
                            ".$where_keyword."
                    WHERE
                        1
                    ORDER BY
                        masterbarang.PCode ASC
            ";    
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($PCode, $NamaLengkap) = $row;
               
                $arr_data["list_pcode"][$PCode] = $PCode;
                $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
            }
        }
        else if($v_matreq_type=="Adjustment" || $v_matreq_type=="Rework")
        {
            $q = "
                    SELECT
                        masterbarang.PCode,
                        masterbarang.NamaLengkap
                    FROM
                        masterbarang
                    WHERE      
                        1
                        AND masterbarang.KdKategori = '3'
                        ".$where_keyword."
                    ORDER BY
                        masterbarang.PCode ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($PCode, $NamaLengkap) = $row;
               
                $arr_data["list_pcode"][$PCode] = $PCode;
                $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
            }    
        }
       
        ?>
        <select name="v_inventorycode" id="v_inventorycode" class="form-control-new" style="width:235px;" onchange="CallAjax('choose_pcode')">
            <option value="">-</option>
            <?php
                foreach($arr_data["list_pcode"] as $PCode=>$val)
                {
                    $NamaLengkap= $arr_data["NamaLengkap"][$PCode];
					//hello
            ?>
            <option value="<?php echo $PCode; ?>"><?php echo $NamaLengkap; ?></option>                    
            <?php
                }
            ?>
        </select>
        <?php        
    }
    else if($ajax=="choose_pcode")
    {
        $v_inventorycode  = $_GET["v_inventorycode"];
        $v_matreq_type    = $_GET["v_matreq_type"];
        $v_prodplancode   = $_GET["v_prodplancode"];    
       
        if($v_matreq_type=="Reguler")
        {
            $q = "
                    SELECT 
						prodplandetail.`prodplandetailid`,
                        prodplandetail.formula,
                        formula.formulaname,
                        prodplandetail.quantity,
                        formula.quantity AS qty_kg,
                        formula.packagingsize
                    FROM
                        prodplandetail
                        INNER JOIN formula ON
                            prodplandetail.formula = formula.formulanumber
                            AND prodplandetail.`inventorycode` = formula.`inventorycode`
                    WHERE
                        1
                        AND prodplandetail.prodplancode = '".$v_prodplancode."'
                        AND prodplandetail.inventorycode = '".$v_inventorycode."'
            ";
			
            $qry = mysql_query($q);
            
           
            ?>
                <select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;" onchange="CallAjax('otomatis')">
                	<option value=""> -- Pilih Formula -- </option>
                <?php while($row = mysql_fetch_array($qry)){
                    list($prodplandetailid, $formulanumber, $formulaname, $quantity, $qty_kg, $packagingsize) = $row;?>
                    <option value="<?php echo $formulanumber."-".$prodplandetailid; ?>"><?php echo $formulanumber." :: ".$formulaname; ?></option>            
                <?php }?>
                </select>
                ||        
            <?php
                echo format_number($quantity,4);
                echo "||";
                echo "<b>".format_number($qty_kg*$quantity,4)."</b>";
                echo "||";
                echo "<b>".format_number($packagingsize*$quantity,0)."</b>";
                echo "||";
                echo $qty_kg;
                echo "||";
                echo $packagingsize;
           
        }
        else if($v_matreq_type=="Adjustment" || $v_matreq_type=="Rework")
        {
            $q = "
                    SELECT                                                        
                        formula.formulanumber,
                        formula.formulaname,
                        formula.isdefault
                    FROM
                        formula
                    WHERE
                        1
                        AND formula.isactive = '1'
                        AND formula.inventorycode = '".$v_inventorycode."'
                    ORDER BY
                        formula.isdefault DESC,
                        formula.formulanumber ASC
            ";    
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($formulanumber, $formulaname, $isdefault) = $row;
               
                $arr_data["list_formula"][$formulanumber] = $formulanumber;
               
                if($isdefault)
                {
                    $arr_data["formulaname"][$formulanumber] = $formulaname." (def)";
                }
                else
                {
                    $arr_data["formulaname"][$formulanumber] = $formulaname;
                }
            }
           
            ?>
                <select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;">
                    <option value="">-</option>
                    <?php
                        foreach($arr_data["list_formula"] as $formulanumber=>$val)
                        {
                            $formulaname = $arr_data["formulaname"][$formulanumber];
                    ?>
                    <option value="<?php echo $formulanumber; ?>"><?php echo $formulanumber." :: ".$formulaname; ?></option>            
                    <?php
                        }
                    ?>
                </select>      
            <?php    
        }
    }
    else if($ajax=="otomatis")
    {
        $v_inventorycode  = $_GET["v_inventorycode"];
        $v_matreq_type    = $_GET["v_matreq_type"];
        $v_prodplancode   = $_GET["v_prodplancode"];
        $v_formula        = $_GET["v_formula"];
		$v_prodplandetailid = $_GET["v_prodplandetailid"];		
       
        if($v_matreq_type=="Reguler")
        {
            $q = "
                    SELECT 
						prodplandetail.`prodplandetailid`,
                        prodplandetail.formula,
                        formula.formulaname,
                        prodplandetail.quantity,
                        formula.quantity AS qty_kg,
                        formula.packagingsize
                    FROM
                        prodplandetail
                        INNER JOIN formula ON
                            prodplandetail.formula = formula.formulanumber
                            AND prodplandetail.`inventorycode` = formula.`inventorycode`
                    WHERE
                        1
                        AND prodplandetail.prodplancode = '".$v_prodplancode."'
                        AND prodplandetail.inventorycode = '".$v_inventorycode."'
                        AND prodplandetail.formula='".$v_formula."'
						AND prodplandetail.`prodplandetailid`='".$v_prodplandetailid."' 
            ";
			
            $qry = mysql_query($q);
            $row = mysql_fetch_array($qry);
            list($prodplandetailid, $formulanumber, $formulaname, $quantity, $qty_kg, $packagingsize) = $row;
            echo $formulanumber;?>||<?php
                echo format_number($quantity,4);
                echo "||";
                echo "<b>".format_number($qty_kg*$quantity,4)."</b>";
                echo "||";
                echo "<b>".format_number($packagingsize*$quantity,0)."</b>";
                echo "||";
                echo $qty_kg;
                echo "||";
                echo $packagingsize;
           
        }
        else if($v_matreq_type=="Adjustment" || $v_matreq_type=="Rework")
        {
            $q = "
                    SELECT                                                        
                        formula.formulanumber,
                        formula.formulaname,
                        formula.isdefault
                    FROM
                        formula
                    WHERE
                        1
                        AND formula.isactive = '1'
                        AND formula.inventorycode = '".$v_inventorycode."'
                    ORDER BY
                        formula.isdefault DESC,
                        formula.formulanumber ASC
            ";    
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($formulanumber, $formulaname, $isdefault) = $row;
               
                $arr_data["list_formula"][$formulanumber] = $formulanumber;
               
                if($isdefault)
                {
                    $arr_data["formulaname"][$formulanumber] = $formulaname." (def)";
                }
                else
                {
                    $arr_data["formulaname"][$formulanumber] = $formulaname;
                }
            }
           
            ?>
                <select name="v_formulanumber" id="v_formulanumber" class="form-control-new" style="width:325px;">
                    <option value="">-</option>
                    <?php
                        foreach($arr_data["list_formula"] as $formulanumber=>$val)
                        {
                            $formulaname = $arr_data["formulaname"][$formulanumber];
                    ?>
                    <option value="<?php echo $formulanumber; ?>"><?php echo $formulanumber." :: ".$formulaname; ?></option>            
                    <?php
                        }
                    ?>
                </select>      
            <?php    
        }
    }
    exit();
}
 

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :
            {
                if(!isset($_POST["v_matreqdate"])){ $v_matreqdate = isset($_POST["v_matreqdate"]); } else { $v_matreqdate = $_POST["v_matreqdate"]; }
                if(!isset($_POST["v_matreq_type"])){ $v_matreq_type = isset($_POST["v_matreq_type"]); } else { $v_matreq_type = $_POST["v_matreq_type"]; }
                if(!isset($_POST["v_prodplancode"])){ $v_prodplancode = isset($_POST["v_prodplancode"]); } else { $v_prodplancode = $_POST["v_prodplancode"]; }
                if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_reqtype"])){ $v_reqtype = isset($_POST["v_reqtype"]); } else { $v_reqtype = $_POST["v_reqtype"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_quantity"])){ $v_quantity = isset($_POST["v_quantity"]); } else { $v_quantity = $_POST["v_quantity"]; }
                if(!isset($_POST["v_approvedby"])){ $v_approvedby = isset($_POST["v_approvedby"]); } else { $v_approvedby = $_POST["v_approvedby"]; }
                if(!isset($_POST["v_note"])){ $v_note = isset($_POST["v_note"]); } else { $v_note = $_POST["v_note"]; }
               
                if(!isset($_POST["v_matreqnumber_prev"])){ $v_matreqnumber_prev = isset($_POST["v_matreqnumber_prev"]); } else { $v_matreqnumber_prev = $_POST["v_matreqnumber_prev"]; }
                if(!isset($_POST["v_kekurangan_supplier"])){ $v_kekurangan_supplier = isset($_POST["v_kekurangan_supplier"]); } else { $v_kekurangan_supplier = $_POST["v_kekurangan_supplier"]; }
               
                $v_quantity = save_int($v_quantity);
                $v_approvedby = save_char($v_approvedby);
                $v_note = save_char($v_note);
               
                if($v_kekurangan_supplier=="on" && $v_matreq_type=="Adjustment")
                {
                    $v_kekurangan_supplier_save = 1;
                }
                else
                {
                    $v_kekurangan_supplier_save = 0;
                }
               
                $v_matreqnumber_prev = save_char($v_matreqnumber_prev);
               
                $acc_locked = call_acc_locked("matreq", format_save_date($v_matreqdate), format_save_date($v_matreqdate));
               
                if($acc_locked["locked"][format_save_date($v_matreqdate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_matreqdate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else
                {
                    // validasi
                    {
                        if($v_matreq_type=="Reguler")
                        {
                            if($v_prodplancode=="")
                            {
                                $msg = "Prod Plan Code harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            else if($v_inventorycode=="")
                            {
                                $msg = "PCode harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            else if($v_formulanumber=="")
                            {
                                $msg = "Formula harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            else if($v_warehousecode=="")
                            {
                                $msg = "Warehouse harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            else if($v_quantity*1==0)
                            {
                                $msg = "Batch Qty harus diisi";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                        }                
                        else if($v_matreq_type=="Adjustment" || $v_matreq_type=="Rework")
                        {
                            if($v_inventorycode=="")
                            {
                                $msg = "PCode harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            else if($v_formulanumber=="")
                            {
                                $msg = "Formula harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                            else if($v_warehousecode=="")
                            {
                                $msg = "Warehouse harus dipilih";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                           
                            if($v_matreq_type=="Adjustment")
                            {
                                /*
                                if($v_matreqnumber_prev=="")
                                {
                                    $msg = "MR sebelumnya harus diisi";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();    
                                }
                                */
                                // cek matreqnumber sebelum nya da gak didatabase
                                if($v_matreqnumber_prev)
                                {
                                    $q = "
                                            SELECT
                                                matreq.matreqnumber
                                            FROM
                                                matreq
                                            WHERE
                                                1
                                                AND matreq.matreqnumber = '".$v_matreqnumber_prev."'
                                            LIMIT
                                                0,1
                                    ";
                                    $qry = mysql_query($q);
                                    $row = mysql_fetch_array($qry);
                                    list($matreqnumber) = $row;
                                   
                                    if(!$matreqnumber)
                                    {
                                        $msg = "MR sebelumnya tidak ada Referensi";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();    
                                    }
                                }
                            }
                        }  
                    }
                   
                    // buat counter MR
                    {
                        $arr_date = explode("-", $v_matreqdate);
                       
                        //MR00001/02/15
                       
                        if($v_kekurangan_supplier_save==1)
                        {
                            $q = "
                                    SELECT
                                        matreq.matreqnumber
                                    FROM
                                        matreq
                                    WHERE
                                        1
                                        AND MONTH(matreq.matreqdate) = '".$arr_date[1]."'
                                        AND YEAR(matreq.matreqdate) = '".$arr_date[2]."'
                                        AND LEFT(matreq.matreqnumber,2) = 'MS'
                                    ORDER BY
                                        matreq.matreqnumber DESC
                                    LIMIT
                                        0,1
                            ";
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                           
                            $counter = (str_replace("MS", "", substr($row_counter["matreqnumber"],0,7))*1)+1;
                           
                            $v_matreqnumber = "MS".sprintf("%05s", $counter)."/".$arr_date[1]."/".substr($arr_date[2],2,2);  
                        }
                        else
                        {
                            $q = "
                                    SELECT
                                        matreq.matreqnumber
                                    FROM
                                        matreq
                                    WHERE
                                        1
                                        AND MONTH(matreq.matreqdate) = '".$arr_date[1]."'
                                        AND YEAR(matreq.matreqdate) = '".$arr_date[2]."'
                                        AND LEFT(matreq.matreqnumber,2) = 'MR'
                                    ORDER BY
                                        matreq.matreqnumber DESC
                                    LIMIT
                                        0,1
                            ";
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                           
                            $counter = (str_replace("MR", "", substr($row_counter["matreqnumber"],0,7))*1)+1;
                           
                            $v_matreqnumber = "MR".sprintf("%05s", $counter)."/".$arr_date[1]."/".substr($arr_date[2],2,2);
                        }
                    }
                   
                    // COUNTER PB
                    {
                        $arr_date = explode("-", $v_matreqdate);
                       
                        //MR00001/02/15
                       
                        if($v_kekurangan_supplier_save==1)
                        {
                            $q = "
                                    SELECT
                                        permintaan_barang_header.NoDokumen
                                    FROM
                                        permintaan_barang_header
                                    WHERE
                                        1
                                        AND MONTH(permintaan_barang_header.TglDokumen) = '".$arr_date[1]."'
                                        AND YEAR(permintaan_barang_header.TglDokumen) = '".$arr_date[2]."'
                                    ORDER BY
                                        permintaan_barang_header.NoDokumen DESC
                                    LIMIT
                                        0,1
                            ";
                            
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                           
                            $counter = (str_replace("PB", "", substr($row_counter["NoDokumen"],0,7))*1)+1;
                           	
                            $v_pbnumber = "PB".sprintf("%05s", $counter)."-".$arr_date[1]."-".substr($arr_date[2],2,2);  
                        }
                        else
                        {
                            $q = "
                                    SELECT
                                        permintaan_barang_header.NoDokumen
                                    FROM
                                        permintaan_barang_header
                                    WHERE
                                        1
                                        AND MONTH(permintaan_barang_header.TglDokumen) = '".$arr_date[1]."'
                                        AND YEAR(permintaan_barang_header.TglDokumen) = '".$arr_date[2]."'
                                    ORDER BY
                                        permintaan_barang_header.NoDokumen DESC
                                    LIMIT
                                        0,1
                            ";
                            
                            $qry_counter = mysql_query($q);
                            $row_counter = mysql_fetch_array($qry_counter);
                           
                            $counter = (str_replace("PB", "", substr($row_counter["NoDokumen"],0,7))*1)+1;
                           
                            $v_pbnumber = "PB".sprintf("%05s", $counter)."-".$arr_date[1]."-".substr($arr_date[2],2,2);
                        }
                    }
                    
                    
                    //mencari KdDivisi Berdasarkan inventorycode Formula
                    $div="Select masterbarang.KdDivisi from formula INNER JOIN masterbarang ON formula.inventorycode = masterbarang.PCode where formula.formulanumber='".$v_formulanumber."'";
                    $qry_div = mysql_query($div);
                    $row_div = mysql_fetch_array($qry_div);
                            
                    if($v_matreq_type=="Reguler")
                    {
                        $adjustment = 0;        
                        $rework = 0;
                       
                        // cek batch plan
                        $q = "
                                SELECT                                                        
                                    prodplandetail.formula,
                                    formula.formulaname,
                                    prodplandetail.quantity,
                                    formula.quantity AS qty_kg,
                                    formula.packagingsize
                                FROM
                                    prodplandetail
                                    INNER JOIN formula ON
                                        prodplandetail.formula = formula.formulanumber
                                        AND prodplandetail.`inventorycode` = formula.`inventorycode`
                                WHERE
                                    1
                                    AND prodplandetail.prodplancode = '".$v_prodplancode."'
                                    AND prodplandetail.inventorycode = '".$v_inventorycode."'
                                LIMIT
                                    0,1
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($formula, $formulaname, $quantity, $qty_kg, $packagingsize) = $row;
                       
                        $q = "
                                SELECT
                                    SUM(matreq.quantity) AS qty_matreq
                                FROM
                                    matreq
                                    INNER JOIN formula ON
                                        matreq.formulanumber = formula.formulanumber
                                        AND formula.inventorycode = '".$v_inventorycode."'  
                                WHERE
                                    1
                                    AND matreq.prodplancode = '".$v_prodplancode."'
                                    AND matreq.status != '2'
                                    AND matreq.formulanumber != '2'
                                    AND matreq.adjustment = '0'
                                   
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($qty_matreq) = $row;
                       
                        $qty_all_matreq = $qty_matreq+$v_quantity;
                        $qty_sisa = $quantity-$qty_matreq;
       
                        
                        if($qty_all_matreq > $quantity && false)
                        {
                            $msg = "Batch Qty melebihi Plan Qty, Sisa Batch Qty ".format_number($qty_sisa, 4);
                            echo "<script>alert('".$msg."')</script>";
                            die();    
                        }
                    }
                    else if($v_matreq_type=="Adjustment")
                    {
                        $adjustment = 1;        
                        $rework = 0;
                        $v_quantity = 1;
                    }
                    else if($v_matreq_type=="Rework")
                    {
                        $adjustment = 0;
                        $rework = 1;
                        $v_quantity = 1;
                    }
                   
                    if($v_reqtype==0)
                    {
                        $batchremain_baku      = $v_quantity;
                        $batchremain_packaging = $v_quantity;  
                    }
                    else if($v_reqtype==1)
                    {
                        $batchremain_baku      = $v_quantity;
                        $batchremain_packaging = 0;
                    }
                    else if($v_reqtype==2)
                    {
                        $batchremain_baku      = 0;
                        $batchremain_packaging = $v_quantity;
                    }
                   
                    //insert juga ke permintaan barang di natura
                    $pb = "
                    INSERT INTO `permintaan_barang_header`
					            (`NoDokumen`,
					             `TglDokumen`,
					             `TglTerima`,
					             `KdGudang`,
					             `KdDivisi`,
					             `Keterangan`,
					             `Status`,
					             `AddDate`,
					             `AddUser`)
					VALUES ('$v_pbnumber',
					        '".format_save_date($v_matreqdate)."',
					        '".format_save_date($v_matreqdate)."',
					        '".$v_warehousecode."',
					        '".$row_div['KdDivisi']."',
					        '$v_note',
					        '1',
					        '".date('Y-m-d')."',
					        '".$ses_login."');
                    ";
                	//echo $pb;die;
                    if(!mysql_query($pb))
                    {
                        $msg = "Gagal Insert";
                        echo "<script>alert('".$msg."')</script>";
                        die();    
                    }
                    
                    $pecah_formula = explode('-',$v_formulanumber);
                    $formula = $pecah_formula[0];
                    
                    //insert ke tabel matreq di natura
                    $q = "
                            INSERT INTO `matreq`
                            SET `matreqnumber` = '".$v_matreqnumber."',
                              `formulanumber` = '".$formula."',
                              `warehousecode` = '".$v_warehousecode."',
                              `matreqdate` = '".format_save_date($v_matreqdate)."',
                              `status` = '0',
                              `approvedby` = '".$v_approvedby."',
                              `adduser` = '".$ses_login."',
                              `adddate` = NOW(),
                              `edituser` = '".$ses_login."',
                              `editdate` = NOW(),
                              `description` = '".$v_note."',
                              `quantity` = '".$v_quantity."',
                              `prodplancode` = '".$v_prodplancode."',
                              `adjustment` = '".$adjustment."',
                              `reqtype` = '".$v_reqtype."',
                              `batchremain` = '".$batchremain_baku."',
                              `batchremain_packaging` = '".$batchremain_packaging."',
                              `rework` = '".$rework."',
                              `matreqnumber_prev` = '".$v_matreqnumber_prev."',
                              `kekurangan_supplier` = '".$v_kekurangan_supplier_save."',
                              `pbnumber` = '".$v_pbnumber."'
                    ";
                   
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Insert_";
                        echo "<script>alert('".$msg."')</script>";
                        die();    
                    }  
                    else        
                    {
                        // insert detail
                        if($v_matreq_type=="Reguler")
                        {
                            if($v_reqtype=="0" || $v_reqtype=="1")
                            {
                                $q = "
                                        INSERT INTO `matreqdetail`
                                        (
                                         `matreqnumber`,
                                         `inventorycode`,
                                         `quantity`,
                                         `adduser`,
                                         `adddate`,
                                         `edituser`,
                                         `editdate`,
                                         `remain`,
                                         `batchremain`,
                                         `inventorystatusqty`
                                         )
                                         SELECT
                                            '".$v_matreqnumber."' AS matreqnumber,
                                            formuladetail.inventorycode,        
                                            ((formula.quantity *(formuladetail.percentage/100)) * ".$v_quantity.") AS quantity,
                                            '".$ses_login."' AS adduser,
                                            NOW() AS adddate,
                                            '".$ses_login."' AS edituser,
                                            NOW() AS editdate,
                                            ((formula.quantity *(formuladetail.percentage/100)) * ".$v_quantity.") AS remain,
                                            ((formula.quantity *(formuladetail.percentage/100)) * ".$v_quantity.") AS batcremain,
                                            '0' AS inventorystatusqty
                                         FROM
                                            formula
                                            INNER JOIN formuladetail ON
                                                formula.formulanumber = formuladetail.formulanumber
                                                AND formula.formulanumber = '".$formula."'
                                                AND formuladetail.percentage*1!='0'
                                         WHERE
                                            1
                                ";
                               
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal Insert Detail Baku";
                                    echo "<script>alert('".$msg."')</script>";
                                    die();    
                                }
                            }
                           
                            if($v_reqtype=="0" || $v_reqtype=="2")
                            {
                                $q = "
                                        INSERT INTO `matreqdetail`
                                        (
                                         `matreqnumber`,
                                         `inventorycode`,
                                         `quantity`,
                                         `adduser`,
                                         `adddate`,
                                         `edituser`,
                                         `editdate`,
                                         `remain`,
                                         `batchremain`,
                                         `inventorystatusqty`
                                         )
                                         SELECT
                                            '".$v_matreqnumber."' AS matreqnumber,
                                            formuladetail.inventorycode,        
                                            CEIL((formuladetail.quantity * ".$v_quantity.")) AS quantity,
                                            '".$ses_login."' AS adduser,
                                            NOW() AS adddate,
                                            '".$ses_login."' AS edituser,
                                            NOW() AS editdate,
                                            CEIL((formuladetail.quantity * ".$v_quantity.")) AS remain,
                                            CEIL((formuladetail.quantity * ".$v_quantity.")) AS batcremain,
                                            '0' AS inventorystatusqty
                                         FROM
                                            formuladetail
                                            INNER JOIN masterbarang 
                                            ON formuladetail.`inventorycode` = masterbarang.`PCode`
                                         WHERE
                                            1
                                            AND formuladetail.formulanumber = '".$formula."'
                                            AND formuladetail.quantity*1!='0'
                                            AND masterbarang.`KdKategori`='2'
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal Insert Detail Kemas";
                                    echo "<script>alert('".$msg."')</script>";
                                    die();    
                                }
                            }
                        }
                        else if ($v_matreq_type=="Adjustment" || $v_matreq_type=="Rework")
                        {
                            if($v_reqtype=="0" || $v_reqtype=="1")
                            {
                                $q = "
                                        INSERT INTO `matreqdetail`
                                        (
                                         `matreqnumber`,
                                         `inventorycode`,
                                         `quantity`,
                                         `adduser`,
                                         `adddate`,
                                         `edituser`,
                                         `editdate`,
                                         `remain`,
                                         `batchremain`,
                                         `inventorystatusqty`
                                         )
                                         SELECT
                                            '".$v_matreqnumber."' AS matreqnumber,
                                            formuladetail.inventorycode,        
                                            '0' AS quantity,
                                            '".$ses_login."' AS adduser,
                                            NOW() AS adddate,
                                            '".$ses_login."' AS edituser,
                                            NOW() AS editdate,
                                            '0' AS remain,
                                            '0' AS batcremain,
                                            '0' AS inventorystatusqty
                                         FROM
                                            formula
                                            INNER JOIN formuladetail ON
                                                formula.formulanumber = formuladetail.formulanumber
                                                AND formula.formulanumber = '".$formula."'
                                                AND formuladetail.percentage*1!='0'
                                         WHERE
                                            1
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal Insert Detail Baku";
                                    echo "<script>alert('".$msg."')</script>";
                                    die();    
                                }
                            }
                           
                            if($v_reqtype=="0" || $v_reqtype=="2")
                            {
                                $q = "
                                        INSERT INTO `matreqdetail`
                                        (
                                         `matreqnumber`,
                                         `inventorycode`,
                                         `quantity`,
                                         `adduser`,
                                         `adddate`,
                                         `edituser`,
                                         `editdate`,
                                         `remain`,
                                         `batchremain`,
                                         `inventorystatusqty`
                                         )
                                         SELECT
                                            '".$v_matreqnumber."' AS matreqnumber,
                                            formuladetail.inventorycode,        
                                            '0' AS quantity,
                                            '".$ses_login."' AS adduser,
                                            NOW() AS adddate,
                                            '".$ses_login."' AS edituser,
                                            NOW() AS editdate,
                                            '0' AS remain,
                                            '0' AS batcremain,
                                            '0' AS inventorystatusqty
                                         FROM
                                            formuladetail
                                         WHERE
                                            1
                                            AND formuladetail.formulanumber = '".$formula."'
                                            AND formuladetail.quantity*1!='0'
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal Insert Detail Kemas";
                                    echo "<script>alert('".$msg."')</script>";
                                    die();    
                                }
                            }    
                        }
                        
                        $pb_detail = "
                                        INSERT INTO `permintaan_barang_detail`
                                        (`NoDokumen`,
							             `PCode`,
							             `NamaBarang`,
							             `Qty`,
							             `Satuan`)
                                         SELECT
                                            '".$v_pbnumber."' AS NoDokumen,
                                            formuladetail.inventorycode,        
                                            masterbarang.NamaLengkap,
                                            CEIL((formuladetail.quantity * ".$v_quantity.")) AS quantity,
                                            masterbarang.SatuanSt
                                         FROM formula
                                            INNER JOIN formuladetail ON
                                                formula.formulanumber = formuladetail.formulanumber 
                                        
                                            INNER JOIN `masterbarang` ON
                                                `masterbarang`.`PCode` = `formuladetail`.`inventorycode`
                                         WHERE
                                            1 AND formuladetail.formulanumber = '".$formula."'
                                ";
                        //echo $pb_detail;die;
                        if(!mysql_query($pb_detail))
                                {
                                    $msg = "Gagal Insert Permintaan Barang Detail";
                                    echo "<script>alert('".$msg."')</script>";
                                    die();    
                                }
                       
                        $msg = "Berhasil menyimpan";
                        echo "<script>alert('".$msg."');</script>";
                        echo "<script>parent.CallAjax('search', '".$v_matreqnumber."');</script>";        
                        die();    
                    }
                   
                }
               
            }
            break;
        case "edit_data" :
            {
                if(!isset($_POST["v_matreqnumber"])){ $v_matreqnumber = isset($_POST["v_matreqnumber"]); } else { $v_matreqnumber = $_POST["v_matreqnumber"]; }
                if(!isset($_POST["v_matreqdate"])){ $v_matreqdate = isset($_POST["v_matreqdate"]); } else { $v_matreqdate = $_POST["v_matreqdate"]; }
                if(!isset($_POST["v_matreqdate_old"])){ $v_matreqdate_old = isset($_POST["v_matreqdate_old"]); } else { $v_matreqdate_old = $_POST["v_matreqdate_old"]; }
                if(!isset($_POST["v_quantity"])){ $v_quantity = isset($_POST["v_quantity"]); } else { $v_quantity = $_POST["v_quantity"]; }
                if(!isset($_POST["v_approvedby"])){ $v_approvedby = isset($_POST["v_approvedby"]); } else { $v_approvedby = $_POST["v_approvedby"]; }
                if(!isset($_POST["v_note"])){ $v_note = isset($_POST["v_note"]); } else { $v_note = $_POST["v_note"]; }
                if(!isset($_POST["v_matreq_type"])){ $v_matreq_type = isset($_POST["v_matreq_type"]); } else { $v_matreq_type = $_POST["v_matreq_type"]; }
               
                if(!isset($_POST["v_kekurangan_supplier"])){ $v_kekurangan_supplier = isset($_POST["v_kekurangan_supplier"]); } else { $v_kekurangan_supplier = $_POST["v_kekurangan_supplier"]; }
               
                if($v_kekurangan_supplier=="on" && $v_matreq_type=="Adjustment")
                {
                    $v_kekurangan_supplier_save = 1;
                }
                else
                {
                    $v_kekurangan_supplier_save = 0;
                }
               
                if(!isset($_POST["no"])){ $no = isset($_POST["no"]); } else { $no = $_POST["no"]; }
               
                $v_quantity = save_int($v_quantity);
                $v_approvedby = save_char($v_approvedby);
                $v_note = save_char($v_note);
               
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }
               
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }
                if(!isset($_POST["v_del_details"])){ $v_del_details = isset($_POST["v_del_details"]); } else { $v_del_details = $_POST["v_del_details"]; }
               
               
                $acc_locked = call_acc_locked("matreq", format_save_date($v_matreqdate), format_save_date($v_matreqdate));
               
                if($acc_locked["locked"][format_save_date($v_matreqdate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_matreqdate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else
                {
                    if($v_del==1)
                    {
                    	//cek dulu apakah pbdetail sum QtyMutasi > 0
                    	$cek = "
                    	SELECT 
						  SUM(`permintaan_barang_detail`.`QtyMutasi`) AS cek,
						  `matreq`.`pbnumber` 
						FROM
						  `permintaan_barang_detail` INNER JOIN 
						  `matreq` ON `permintaan_barang_detail`.`NoDokumen`=`matreq`.`pbnumber`
						WHERE `matreq`.`matreqnumber`='".$v_matreqnumber."';
                    	";
                    	
                    	$cek_ = mysql_query($cek);
                    	$jml  = mysql_fetch_array($cek_);
                    	$hasil = $jml['cek']*1;
                    
                    	if($jml['cek']*1>0){
							echo "<script>alert('Tidak Bisa Void')</script>";die;
						}else{
							
						$cek = "
                                    UPDATE
                                        permintaan_barang_header
                                    SET
                                        Status = '2',
                                        EditDate = NOW(),
                                        EditUser = '".$ses_login."'
                                    WHERE
                                        NoDokumen = '".$jml['pbnumber']."'";
                        if(!mysql_query($cek))
                        {
                            $msg = "Gagal Void";
                            echo "<script>alert('".$msg."')</script>";
                            die();    
                        }	
							 
                        $del = "
                                    UPDATE
                                        matreq
                                    SET
                                        status = '2',
                                        editdate = NOW(),
                                        edituser = '".$ses_login."'
                                    WHERE
                                        matreqnumber = '".$v_matreqnumber."'";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal Void";
                            echo "<script>alert('".$msg."')</script>";
                            die();    
                        }
                       
                        $msg = "Berhasil Void";
                        echo "<script>alert('".$msg."');</script>";
                        echo "<script>parent.CallAjax('search', '".$v_matreqnumber."');</script>";        
                        die();
                        
                        }    
                    }
                    else if($v_undel==1)
                    {
                        $del = "
                                    UPDATE
                                        matreq
                                    SET
                                        status = '0',
                                        editdate = NOW(),
                                        edituser = '".$ses_login."'
                                    WHERE
                                        matreqnumber = '".$v_matreqnumber."'";
                        if(!mysql_query($del))
                        {
                            $msg = "Gagal UnVoid";
                            echo "<script>alert('".$msg."')</script>";
                            die();    
                        }
                       
                       
                        $msg = "Berhasil UnVOID";
                        echo "<script>alert('".$msg."');</script>";
                        echo "<script>parent.CallAjax('search', '".$v_matreqnumber."');</script>";        
                        die();        
                    }
                    else
                    {
                        // validasi
                        {    
                            $exp_date     = explode("-", $v_matreqdate);
                            $exp_date_old = explode("-", $v_matreqdate_old);
                           
                            $mmyyyy_date      = $exp_date[1]."-".$exp_date[2];
                            $mmyyyy_date_old  = $exp_date_old[1]."-".$exp_date_old[2];
                           
                            if($mmyyyy_date!=$mmyyyy_date_old)
                            {
                                $msg = "Tidak bisa edit tanggal berbeda bulan";
                                echo "<script>alert('".$msg."');</script>";
                                die();    
                            }
                           
                           
                            if($v_matreq_type=="Reguler")
                            {
                                if($v_quantity*1==0)
                                {
                                    $msg = "Batch Qty harus diisi";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();    
                                }
                               
                               
                                // cek batch plan
                                $q = "
                                        SELECT
                                            matreq.prodplancode,    
                                            matreq.reqtype,    
                                            formula.inventorycode
                                        FROM
                                            matreq
                                            INNER JOIN formula ON
                                                matreq.formulanumber = formula.formulanumber
                                                AND matreq.matreqnumber = '".$v_matreqnumber."'    
                                         WHERE
                                            1
                                         LIMIT
                                            0,1  
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($v_prodplancode, $reqtype, $v_inventorycode) = $row;
                               
                                if($reqtype==0)
                                {
                                    $batchremain_baku      = $v_quantity;
                                    $batchremain_packaging = $v_quantity;  
                                }
                                else if($reqtype==1)
                                {
                                    $batchremain_baku      = $v_quantity;
                                    $batchremain_packaging = 0;
                                }
                                else if($reqtype==2)
                                {
                                    $batchremain_baku      = 0;
                                    $batchremain_packaging = $v_quantity;
                                }
                               
                               
                                $q = "
                                        SELECT                                                        
                                            prodplandetail.formula,
                                            formula.formulaname,
                                            prodplandetail.quantity,
                                            formula.quantity AS qty_kg,
                                            formula.packagingsize
                                        FROM
                                            prodplandetail
                                            INNER JOIN formula ON
                                                prodplandetail.formula = formula.formulanumber
                                                AND prodplandetail.`inventorycode` = formula.`inventorycode`
                                        WHERE
                                            1
                                            AND prodplandetail.prodplancode = '".$v_prodplancode."'
                                            AND prodplandetail.inventorycode = '".$v_inventorycode."'
                                        LIMIT
                                            0,1
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($formula, $formulaname, $quantity, $qty_kg, $packagingsize) = $row;
                               
                                $q = "
                                        SELECT
                                            SUM(matreq.quantity) AS qty_matreq
                                        FROM
                                            matreq
                                            INNER JOIN formula ON
                                                matreq.formulanumber = formula.formulanumber
                                                AND formula.inventorycode = '".$v_inventorycode."'
                                        WHERE
                                            1
                                            AND matreq.prodplancode = '".$v_prodplancode."'
                                            AND matreq.status != '2'
                                            AND matreq.matreqnumber != '".$v_matreqnumber."'
                                            AND matreq.adjustment = '0'
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($qty_matreq) = $row;
                               
                                $qty_all_matreq = $qty_matreq+$v_quantity;
                                $qty_sisa = $quantity-$qty_matreq;
                                
                                // sgv gak usah di validasi
                                if($qty_all_matreq > $quantity && false)
                                {
                                    $msg = "Batch Qty melebihi Plan Qty, Sisa Batch Qty ".format_number($qty_sisa, 4);
                                    echo "<script>alert('".$msg."')</script>";
                                    die();    
                                }                              
                               
                            }                
                            else if($v_matreq_type=="Adjustment" || $v_matreq_type=="Rework")
                            {
                               
                            }
                           
                            foreach($no as $key_no=>$val_no)
                            {
                                if(!isset($_POST["v_inventorycode_".$val_no])){ $v_inventorycode = isset($_POST["v_inventorycode_".$val_no]); } else { $v_inventorycode = $_POST["v_inventorycode_".$val_no]; }
                                if(!isset($_POST["v_im_active_".$val_no])){ $v_im_active = isset($_POST["v_im_active_".$val_no]); } else { $v_im_active = $_POST["v_im_active_".$val_no]; }
                               
                                /*if($v_im_active*1==0)
                                {
                                    $msg = "Tidak bisa menyimpan, karena Pcode ".$v_inventorycode." status nya InActive";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();    
                                }*/
                               
                            }

                        }
                       
                        $q = "
                                UPDATE
                                    matreq
                                SET
                                    matreqdate = '".format_save_date($v_matreqdate)."',
                                    quantity = '".$v_quantity."',
                                    approvedby = '".$v_approvedby."',
                                    description = '".$v_note."',
                                    kekurangan_supplier = '".$v_kekurangan_supplier_save."',
                                    batchremain = '".$batchremain_baku."',
                                    batchremain_packaging = '".$batchremain_packaging."',
                                    editdate = NOW(),
                                    edituser = '".$ses_login."'
                                WHERE
                                    matreqnumber = '".$v_matreqnumber."'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Update";
                            echo "<script>alert('".$msg."');</script>";    
                            die();
                        }
                        else
                        {
                            while(list($key,$val)=@each($no))
                            {
                                if(!isset($_POST["v_inventorycode_".$val])){ $v_inventorycode = isset($_POST["v_inventorycode_".$val]); } else { $v_inventorycode = $_POST["v_inventorycode_".$val]; }
                                if(!isset($_POST["v_quantity_".$val])){ $v_quantity = isset($_POST["v_quantity_".$val]); } else { $v_quantity = $_POST["v_quantity_".$val]; }
                               
                                $v_quantity = save_int($v_quantity);

                                $q = "
                                        UPDATE
                                            matreqdetail
                                        SET
                                            quantity = '".$v_quantity."',
                                            remain = '".$v_quantity."',
                                            batchremain = '".$v_quantity."',
                                            edituser = '".$ses_login."',
                                            editdate = NOW()
                                        WHERE
                                            inventorycode = '".$v_inventorycode."'
                                            AND matreqnumber = '".$v_matreqnumber."'
                                ";
                               
                                if(!mysql_query($q))      
                                {
                                    $msg = "Gagal Update Detail";
                                    echo "<script>alert('".$msg."');</script>";    
                                    die();
                                }
                            }
                           
                            $msg = "Berhasil menyimpan";
                            echo "<script>alert('".$msg."');</script>";
                            echo "<script>parent.CallAjax('search', '".$v_matreqnumber."');</script>";        
                            die();
                        }
                    }
                }

            }
            break;
        case "print_setengah_letter" :
            {
            	
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
               
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
               
               
                $arr_epson = sintak_epson();
               
                $total_spasi = 95;
                $jml_detail  = 11;
                $ourFileName = "Matreq.txt";
                $nama        = "PT. NATURA PESONA MANDIRI";
               
                header("Content-type: application/txt");
                header("Content-Disposition: attachment; filename=$ourFileName");
                //$content = openFile($ourFileName);

                $echo  = "";
                foreach($v_data as $key => $val)
                {
                    $matreqnumber = $val;
                   
                    $q = "
                            SELECT
                                matreq.matreqnumber,
                                matreq.matreqdate,
                                matreq.warehousecode,
                                formula.inventorycode,
                                formula.formulanumber,
                                formula.formulaname,
                                matreq.quantity AS qty_batch,
                                (matreq.quantity*formula.quantity) AS quantity,
                                (matreq.quantity*formula.packagingsize) AS packagingsize,
                                matreq.editdate,
                                matreq.description AS note,
                                matreq.adjustment,
                                matreq.reqtype,
                                matreq.matreqnumber_prev,
                                matreq.kekurangan_supplier,
                                matreq.pbnumber
                            FROM
                                matreq
                                INNER JOIN formula ON
                                    matreq.formulanumber = formula.formulanumber
                                INNER JOIN masterbarang ON
                                    formula.inventorycode = masterbarang.PCode
                            WHERE
                                1
                                AND matreq.matreqnumber = '".$matreqnumber."'
                                AND matreq.status != '2'
                            LIMIT
                                0,1
                    ";
                    
                    $qry_curr = mysql_query($q);
                    $arr_curr = mysql_fetch_array($qry_curr);
					
					
                    if($arr_curr["adjustment"])
                    {
                        if($arr_curr["kekurangan_supplier"])
                        {
                            $arr_curr["note"] = "[Adjustment Kekurangan Supplier] MR Sebelum Nya : ".$arr_curr["matreqnumber_prev"]." ".$arr_curr["note"];
                        }
                        else
                        {
                            $arr_curr["note"] = "[Adjustment] MR Sebelum Nya : ".$arr_curr["matreqnumber_prev"]." ".$arr_curr["note"];    
                        }
                       
                    }
                   
                    //  halal
                    if($arr_curr["halal"]==1)
                    {
                        $kode_dokumen = "FH.PR.01";
                        $nama_cetakan = "MATERIAL REQUEST HALAL";
                        $kode_edisi   = "A";
                       
                        $matreqnumber_echo = str_replace("MR", "MRH", $arr_curr["matreqnumber"]);
                    }
                    else
                    {
                        $kode_dokumen = "F.PR.008";
                        $nama_cetakan = "MATERIAL REQUEST";
                        $kode_edisi   = "C";
                       
                        $matreqnumber_echo =  $arr_curr["matreqnumber"];
                        $pbnumber_echo =  $arr_curr["pbnumber"];
                    }
                   
                    $q = "
                            SELECT
                                gudang.KdGudang,
                                gudang.Keterangan
                            FROM
                                gudang
                            WHERE
                                1
                            ORDER BY
                                gudang.KdGudang ASC
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($KdGudang, $Keterangan) = $row;
                       
                        $arr_data["warehouse_warehousename"][$KdGudang] = $Keterangan;
                    }
                   
                    if($arr_curr["adjustment"])
                    {
                        $q = "
                                SELECT
                                    masterbarang.KdKategori,
                                    masterbarang.PCode,
                                    masterbarang.NamaLengkap,
                                    matreqdetail.quantity
                                FROM
                                    matreqdetail
                                    INNER JOIN masterbarang ON
                                        masterbarang.PCode = matreqdetail.inventorycode
                                WHERE
                                    1
                                    AND matreqdetail.matreqnumber = '".$matreqnumber."'
                                    AND (matreqdetail.quantity*1) != '0'
                                ORDER BY
                                    matreqdetail.matregdetailid ASC
                        ";
                       
                        $counter = 1;
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($KdKategori, $PCode, $NamaLengkap, $quantity) = $row;
                           
                            $arr_data["list_detail"][$matreqnumber][$counter] = $counter;
                           
                            $arr_data["detail_tipe"][$matreqnumber][$counter] = $KdKategori;
                            $arr_data["detail_pcode"][$matreqnumber][$counter] = $PCode;
                            $arr_data["detail_namalengkap"][$matreqnumber][$counter] = $NamaLengkap;
                            $arr_data["detail_quantity"][$matreqnumber][$counter] = $quantity;
                            $counter++;
                        }    
                    }
                    else
                    {
                    	//hello
                        $counter = 0;
                        $arr_data["list_detail"][$matreqnumber][$counter] = $counter;  
                        $arr_data["detail_pcode"][$matreqnumber][$counter] = $arr_curr["pcode"];
                        $arr_data["detail_pcode"][$matreqnumber][$counter]= $arr_curr["pcode"];
                        $arr_data["detail_formulaname"][$matreqnumber][$counter] = $arr_curr["formulaname"];
                        $arr_data["detail_qty_batch"][$matreqnumber][$counter] = $arr_curr["qty_batch"];
                        $arr_data["detail_quantity"][$matreqnumber][$counter] = $arr_curr["quantity"];
                        $arr_data["detail_packagingsize"][$matreqnumber][$counter] = $arr_curr["packagingsize"];
                       
//                        $counter++;
//                        $arr_data["list_detail"][$matreqnumber][$counter] = $counter;
//                        $arr_data["detail_inventorycode"][$matreqnumber][$counter] = "";
//                        $arr_data["detail_formulaname"][$matreqnumber][$counter] = "";
//                        $arr_data["detail_qty_batch"][$matreqnumber][$counter] = "";
//                        $arr_data["detail_quantity"][$matreqnumber][$counter] = "";
//                        $arr_data["detail_packagingsize"][$matreqnumber][$counter] = "";
                       
                        /*$counter++;
                        $arr_data["list_detail"][$matreqnumber][$counter] = $counter;
                        $arr_data["detail_pcode"][$matreqnumber][$counter] = "INFO PACKAGING :";
                        $arr_data["detail_formulaname"][$matreqnumber][$counter] = "";
                        $arr_data["detail_qty_batch"][$matreqnumber][$counter] = "";
                        $arr_data["detail_quantity"][$matreqnumber][$counter] = "";
                        $arr_data["detail_packagingsize"][$matreqnumber][$counter] = "";*/
                       
                        // kalo Baku + Kemas atau Kemas
                        // buat munculin info kemas
                        if($arr_curr["reqtype"]==0 || $arr_curr["reqtype"]==2)
                        {  
                            $q = "
                                    SELECT 
									  masterbarang.PCode,
									  masterbarang.NamaLengkap,
									  matreqdetail.quantity,
									  `masterbarang`.`SatuanSt` AS Satuan  
									FROM
									  matreqdetail 
									  INNER JOIN masterbarang 
									    ON masterbarang.PCode = matreqdetail.inventorycode 
									  INNER JOIN `matreq` 
									    ON `matreq`.`matreqnumber` = matreqdetail.`matreqnumber` 
									  INNER JOIN `permintaan_barang_detail` 
									    ON `permintaan_barang_detail`.`NoDokumen` = `matreq`.`pbnumber` 
									    AND matreqdetail.`inventorycode` = `permintaan_barang_detail`.`PCode`
									WHERE 1 
                                        AND matreqdetail.matreqnumber = '".$matreqnumber."'
                                        AND (matreqdetail.quantity*1) != '0'
                                    GROUP BY masterbarang.PCode
                                    ORDER BY
                                        matreqdetail.matregdetailid ASC
                            ";
                            //echo $q;die;
                            $counter_no = 0;
                            $qry = mysql_query($q);
                            while($row = mysql_fetch_array($qry))
                            {
                                $counter++;
                                $counter_no++;
                                list($PCode, $NamaLengkap, $quantity, $Satuan) = $row;
                               
                                $arr_data["list_detail"][$matreqnumber][$counter] = $counter;
                               
                                $arr_data["detail_pcode"][$matreqnumber][$counter] = $PCode;
                                $arr_data["detail_formulaname"][$matreqnumber][$counter] = $NamaLengkap;
                                $arr_data["detail_qty_batch"][$matreqnumber][$counter] = $arr_curr["qty_batch"];
                                $arr_data["detail_quantity"][$matreqnumber][$counter] = $arr_curr["quantity"];
                                $arr_data["detail_packagingsize"][$matreqnumber][$counter] = $quantity;
                                $arr_data["detail_satuan"][$matreqnumber][$counter] = $Satuan;
                               
                                $arr_data["detail_nomor"][$matreqnumber][$counter] = $counter_no;
                            }
                            
                                
                        }
                       
                    }
                   
                    $curr_jml_detail = count($arr_data["list_detail"][$matreqnumber]);
 
                    $jml_page = ceil($curr_jml_detail/$jml_detail);
                    $status_info_pack = false;
                   
                    //$echo .= $arr_epson["reset"];      
                   
                    for($i_page=1;$i_page<=$jml_page;$i_page++)
                    {
                        
                        $echo .= chr(27).chr(67).chr(33).chr(27).chr(15);
                        // header
                        {
                            $echo .= $nama;
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "";
                           
                            $limit_spasi = 0;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $limit_spasi = 18;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "";
                           
                            $limit_spasi = 11;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }

                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= $kode_dokumen;
                            $echo .= "\r\n";    
                        }
                       
                        $echo .= "Jl. Raya Denpasar Bedugul KM.36 Tabanan";
                       
                        $kurang = strlen("Jl. Raya Denpasar Bedugul KM.36 Tabanan");
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen($nama_cetakan)/2) - 36;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                       
                        $echo .= $nama_cetakan;
                       
                        for($i=0;$i<0;$i++)
                        {
                            $echo .= " ";
                        }
                       
                        $tanggal_terbit      = substr($arr_curr["editdate"],0,10);
                        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
                        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
                       
                        $echo .= "       TANGGAL TERBIT : ".$tanggal_terbit_show;
                       
                        $echo .="\r\n";
                       
                        $echo .="Bali 82191 - Indonesia";
                        $kurang = strlen("Bali 82191 - Indonesia");
                       
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen("No : ".$matreqnumber_echo)/2)-$kurang+3;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                       
                        $echo .= "No : ".$matreqnumber_echo;
                       
                       
                        for($i=0;$i<6;$i++)
                        {
                            $echo .= " ";
                        }
                       
                        $echo .= "Edisi / Revisi : ".$kode_edisi." /00";
                       
                       
                        $echo .="\r\n";
                       
                        $echo .= " ";
                       
                        $kurang = strlen(" ");
                       
                        if($arr_curr["formulaname"])
                        {
                            $limit_spasi = ceil(($total_spasi/2)) - (strlen($arr_curr["formulaname"])/2)-$kurang+3;
                           
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= $arr_curr["formulaname"];    
                        }
                       
                       
                       
                       
                        $echo .="\r\n";
                       
                        $echo .="\r\n";
                        // baris 1
                        {
                            $echo .= "TANGGAL";
                            $limit_spasi = (12-2);
                            for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                           
                            $echo .= format_show_date($arr_curr["matreqdate"]);
                           
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["matreqdate"])));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $limit_spasi = 24;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "PB : ".$pbnumber_echo;
                           
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen(" "));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "  ";
                            $echo .= " ";
                           
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(" "));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }

                        // baris 2
                        {
                            $echo .= "GUDANG";
                            $limit_spasi = (12-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                           
                            $echo .= $arr_data["warehouse_warehousename"][$arr_curr["warehousecode"]];
                           
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehouse_warehousename"][$arr_curr["warehousecode"]]));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= " ";
                           
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen(" "));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "  ";
                            $echo .= " ";
                           
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(" "));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }
                       
                       
                       
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        $echo .= "\r\n";
                       
                        if($arr_curr["adjustment"])
                        {
                            $echo .= "NO";
                            $limit_spasi = 3;
                            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "PCODE";
                            $limit_spasi = 10;
                            for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "NAMA BARANG";
                            $limit_spasi = 60;
                            for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                            {
                                $echo .= " ";
                            }
                           
                           
                            $limit_spasi = 15;
                            for($i=0;$i<($limit_spasi-strlen("QTY  "));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "QTY ";
                           
                            $echo .= "";
                            $limit_spasi = 5;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                           
                        }
                        else
                        {
                            $echo .= "NO";
                            $limit_spasi = 3;
                            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "PCODE";
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "NAMA BARANG";
                            $limit_spasi = 40;
                            for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                            {
                                $echo .= " ";
                            }
                           
                           
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen("QTY BATCH"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "QTY BATCH";
                           
                            $echo .= "";
                            $limit_spasi = 5;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                           
                           
                            $limit_spasi = 5;
                            for($i=0;$i<($limit_spasi-strlen("QTY"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "QTY";
                           
                            $echo .= "";
                            $limit_spasi = 5;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $limit_spasi = 10;
                            for($i=0;$i<($limit_spasi-strlen("SATUAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "SATUAN";
                           
                            $echo .= "";
                            $limit_spasi = 5;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                        }

                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                       
                        $echo .= "\r\n";
                       
                        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                        $no_end = $no + $jml_detail;
                       
                       
                       
                        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                        {
                            if($arr_curr["adjustment"])
                            {
                                $detail_stocktypeid = $arr_data["detail_stocktypeid"][$matreqnumber][$i_detail];
                                $detail_inventorycode = $arr_data["detail_pcode"][$matreqnumber][$i_detail];
                                $detail_inventoryname = $arr_data["detail_namalengkap"][$matreqnumber][$i_detail];
                                $detail_quantity = $arr_data["detail_quantity"][$matreqnumber][$i_detail];
                                $detail_uominitial = $arr_data["detail_uominitial"][$matreqnumber][$i_detail];
                               
                                $detail_inventoryname_echo = substr($detail_inventoryname, 0, 90);
                               
                                if($detail_stocktypeid==1)
                                {
                                    $decimal_php = 6;    
                                }
                                else
                                {
                                    $decimal_php = 4;    
                                }
                                // kalo ada isi baru di print
                                if($detail_inventorycode)
                                {
                                    $echo .=$no;
                                    $limit_spasi = 3;
                                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    $echo .= $detail_inventorycode;
                                    $limit_spasi = 10;
                                    for($i=0;$i<($limit_spasi-strlen($detail_inventorycode));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    $echo .= $detail_inventoryname_echo;
                                    $limit_spasi = 60;
                                    for($i=0;$i<($limit_spasi-strlen($detail_inventoryname_echo));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    ## QTY
                                    $limit_spasi = 15;
                                    for($i=0;$i<($limit_spasi-strlen(format_number($detail_quantity, $decimal_php)."  "));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                    $echo .= format_number($detail_quantity, $decimal_php)."  ";
                                   
                                    $echo .= $detail_uominitial;
                                    $limit_spasi = 5;
                                    for($i=0;$i<($limit_spasi-strlen($detail_uominitial));$i++)
                                    {
                                        $echo .= " ";
                                    }

                                }
                            }
                            else
                            {
                            	//hello
                                $detail_inventorycode = $arr_data["detail_pcode"][$matreqnumber][$i_detail];
                                $detail_formulaname = $arr_data["detail_formulaname"][$matreqnumber][$i_detail];
                                $detail_qty_batch = $arr_data["detail_qty_batch"][$matreqnumber][$i_detail];
                                $detail_quantity = $arr_data["detail_quantity"][$matreqnumber][$i_detail];
                                $detail_packagingsize = $arr_data["detail_packagingsize"][$matreqnumber][$i_detail];
                                $detail_nomor = $arr_data["detail_nomor"][$matreqnumber][$i_detail];
                                $detail_satuan = $arr_data["detail_satuan"][$matreqnumber][$i_detail];
                               
                                if($detail_inventorycode=="INFO PACKAGING :")
                                {
                                    $status_info_pack = true;
                                }
                               
                                $no_echo = $no;
                                if($status_info_pack)
                                {
                                    $no_echo = "";
                                    //$echo .= chr(15);
                                }
                               
                                $detail_formulaname_echo = substr($detail_formulaname, 0, 55);
                               
                                // kalo ada isi baru di print
                                if($detail_inventorycode)
                                {
                                    $echo .= $no_echo;
                                    $limit_spasi = 3;
                                    for($i=0;$i<($limit_spasi-strlen($no_echo));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    $echo .= $detail_inventorycode;
                                    $limit_spasi = 12;
                                    for($i=0;$i<($limit_spasi-strlen($detail_inventorycode));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    $echo .= $detail_formulaname_echo;
                                    $limit_spasi = 40;
                                    for($i=0;$i<($limit_spasi-strlen($detail_formulaname_echo));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    ## QTY
                                    $limit_spasi = 10;
                                    for($i=0;$i<($limit_spasi-strlen(format_number($detail_qty_batch, 2)."  "));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                    $echo .= format_number($detail_qty_batch, 2)."  ";
                                   
                                    $echo .= "";
                                    $limit_spasi = 5;
                                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                   
                                    ## QTY KG
                                    $limit_spasi = 15;
                                    for($i=0;$i<($limit_spasi-strlen(format_number($detail_packagingsize, 4)."  "));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                    $echo .= format_number($detail_packagingsize, 4)."  ";
                                   
                                    $echo .= "";
                                    $limit_spasi = 5;
                                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                   
                                    ## PACKAGING SIZE
                                    $limit_spasi = 8;
                                    for($i=0;$i<($limit_spasi-strlen($detail_satuan."  "));$i++)
                                    {
                                        $echo .= " ";
                                    }
                                    $echo .= $detail_satuan."  ";
                                   
                                    $echo .= "";
                                    $limit_spasi = 5;
                                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                                    {
                                        $echo .= " ";
                                    }

                                   
                                }
                            }
                           
                            $no++;
                            $echo .= "\r\n";
                        }
                       
                       
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                       
                        if($i_page==$jml_page)
                        {
                            $echo .= "\r\n";
                            $echo .= "Note : ".$arr_curr["note"];
                            $echo .= "\r\n";
                           
                            $echo .= "     Dibuat Oleh         ";
                            $echo .= "      ";
                            $echo .= "     Diketahui Oleh          ";
                            $echo .= "      ";
                            $echo .= "     Diterima Oleh           ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "   ( Adm Produksi )         ";
                            $echo .= "    ";
                            $echo .= "   ( Spv Produksi )         ";
                            $echo .= "       ";
                            $echo .= "   ( Adm Gudang )         ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                        }
                        else
                        {
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            //$echo .= "\r\n";
                            //$echo .= "\r\n";
                            //$echo .= "\r\n";    
                        }
                       
                        $q = "
                                SELECT
                                    COUNT(noreferensi) AS jml_print
                                FROM
                                    log_print
                                WHERE
                                    1
                                    AND noreferensi = '".$matreqnumber."'
                                    AND form_data = 'matreq'
                        ";
                        $qry_jml_print = mysql_query($q);
                        $row_jml_print = mysql_fetch_array($qry_jml_print);
                       
                        if($row_jml_print["jml_print"]*1>0)
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                           
                            $echo .= "COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        else
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                       
                            $echo .= "HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                       
                       
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                   
                    } // end page
                   $echo .= $arr_data["ncond"];
                   
                    // update counter print
                    if($ses_login!="hendri1003" || $ses_login!="febri0202")
                    {
                        $q = "
                                INSERT
                                    log_print
                                SET
                                    form_data = 'matreq',
                                    noreferensi = '".$matreqnumber."' ,
                                    userid = '".$ses_login."' ,
                                    print_date = NOW() ,
                                    print_page = 'Setengah Letter'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert Print Counter";
                            echo "<script>alert('".$msg."');</script>";    
                            die();
                        }
                    }
                }
               
                echo $echo;    
            }
            break;
    }
}

mysql_close($con);
?>