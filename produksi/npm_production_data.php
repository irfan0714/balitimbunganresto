<?php
    include("header.php");

	$modul        = "Production";
	$file_current = "npm_production.php";
	$file_name    = "npm_production_data.php"; 

function openFile($ourFileName)
{
    $file = file($ourFileName);         
    $content= "<pre>";
    for($l = 0;$l<count($file);$l++){
        $content .= $file[$l];            
    }
    $content .= "</pre>";
    $content =  substr($content,5,strlen($content)-5);
    $content =  substr($content,0,strlen($content)-6);
    return $content;
}

function sintak_epson()
{
    $arr_data["escNewLine"]      = chr(10);  // New line (LF line feed)
    $arr_data["escUnerlineOn"]   = chr(27).chr(45).chr(1);  // Unerline On
    $arr_data["escUnerlineOnx2"] = chr(27).chr(45).chr(2);  // Unerline On x 2
    $arr_data["escUnerlineOff"]  = chr(27).chr(45).chr(0);  // Unerline Off
    $arr_data["escBoldOn"]       = chr(27).chr(69).chr(1);  // Bold On
    $arr_data["escBoldOff"]      = chr(27).chr(69).chr(0);  // Bold Off
    $arr_data["escNegativeOn"]   = chr(29).chr(66).chr(1);  // White On Black On'
    $arr_data["escNegativeOff"]  = chr(29).chr(66).chr(0);  // White On Black Off
    $arr_data["esc8CpiOn"]       = chr(29).chr(33).chr(16); // Font Size x2 On
    $arr_data["esc8CpiOff"]      = chr(29).chr(33).chr(0);  // Font Size x2 Off
    $arr_data["esc16Cpi"]        = chr(27).chr(77).chr(48); // Font A  -  Normal Font
    $arr_data["esc20Cpi"]        = chr(27).chr(77).chr(49); // Font B - Small Font
    $arr_data["escReset"]        = chr(27).chr(64); //chr(27) + chr(77) + chr(48); // Reset Printer
    $arr_data["escFeedAndCut"]   = chr(29).chr(86).chr(65); // Partial Cut and feed

    $arr_data["escAlignLeft"]    = chr(27).chr(97).chr(48); // Align Text to the Left
    $arr_data["escAlignCenter"]  = chr(27).chr(97).chr(49); // Align Text to the Center
    $arr_data["escAlignRight"]   = chr(27).chr(97).chr(50); // Align Text to the Right
    
    $arr_data["reset"]  =chr(27).'@'; //reset semua pengaturan printer
    $arr_data["plength"]=chr(27).'C'; //tinggi kertas, contoh $plength.chr(33) ==> tinggi 33 baris.
    $arr_data["lmargin"]=chr(27).'l'; //margin kiri, pemakaian sama dengan $plength.
    $arr_data["cond"]   =chr(15);   //condensed
    $arr_data["ncond"]  =chr(18);   //end condensed
    $arr_data["dwidth"] =chr(27).'!'.chr(16);  //tulisan melebar
    $arr_data["ndwidth"]=chr(27).'!'.chr(1);   //end tulisan melebar
    $arr_data["draft"]  =chr(27).'x'.chr(48);  //font draft
    $arr_data["nlq"]    =chr(27).'x'.chr(49);
    $arr_data["bold"]   =chr(27).'E';   //tulisan bold
    $arr_data["nbold"]  =chr(27).'F';   //end tulisan bold
    $arr_data["uline"]  =chr(27).'!'.chr(129); //garis bawah
    $arr_data["nuline"] =chr(27).'!'.chr(1); //end garis bawah
    $arr_data["dstrik"] =chr(27).'G';    //double strike (tulisan lebih tebal, 2 kali strike)
    $arr_data["ndstrik"]=chr(27).'H';    //end double strike (tulisan lebih tebal, 2 kali strike)
    $arr_data["elite"]  =chr(27).'M';    //tulisan elite
    $arr_data["pica"]   =chr(27).'P';    //tulisan pica
    $arr_data["height"] =chr(27).'!'.chr(16); //tulisan tinggi
    $arr_data["nheight"]=chr(27).'!'.chr(1);  //end tulisan tinggi
    $arr_data["spasi05"]=chr(27)."3".chr(16); //spasi 5 char
    $arr_data["spasi1"] =chr(27)."3".chr(24); //spasi 1 char
    $arr_data["fcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';    // potong kertas full
    $arr_data["pcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';    // potong kertas sebagian
    $arr_data["op_cash"]=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);   //buka cash register
                
    return $arr_data;                           
}

$ajax = $_REQUEST["ajax"];       

if($ajax)
{
    if($ajax=='search')
    {
        $search_by              = trim($_GET["search_by"]);
        $search_status          = trim($_GET["search_status"]);
        $v_type_date            = trim($_GET["v_type_date"]);
        $v_from_date            = trim($_GET["v_from_date"]);
        $v_to_date              = trim($_GET["v_to_date"]);         
        $search_batchnumber     = trim($_GET["search_batchnumber"]);
        $search_warehouse       = trim($_GET["search_warehouse"]);
        $search_formula         = trim($_GET["search_formula"]);
        $search_keyword         = trim($_GET["search_keyword"]);
        $v_batchnumber_curr     = $_GET["v_batchnumber_curr"];
        
        $v_today        = date("d-m-Y");
        $v_last_7day    = date("d-m-Y", parsedate($v_today,"-")-(86400*7));
        
        $where = "";
        if($search_by=="data")
        {
            if($search_status=="Close" || $search_status=="All")
            {
                if($v_type_date=="today")
                {
                    $where .= " AND production.productiondate = '".format_save_date($v_today)."' ";
                }
                else if($v_type_date=="7_day_before")
                {
                    $where .= " AND production.productiondate BETWEEN '".format_save_date($v_last_7day)."' AND '".format_save_date($v_today)."' ";    
                }
                else if($v_type_date=="range_date")
                {
                    $where .= " AND production.productiondate BETWEEN '".format_save_date($v_from_date)."' AND '".format_save_date($v_to_date)."' ";
                }
            }
            
            if($search_warehouse!="All")
            {
                $where .= " AND production.warehousecode = '".$search_warehouse."' ";    
            }
            
            if($search_status!="All")
            {
                $where .= " AND production.status_batchnumber = '".$search_status."' ";    
            }
            
        }
        else if($search_by=="batchnumber")
        {
            if($v_type_date=="today")
            {
                $where .= " AND production.productiondate = '".format_save_date($v_today)."' ";
            }
            else if($v_type_date=="7_day_before")
            {
                $where .= " AND production.productiondate BETWEEN '".format_save_date($v_last_7day)."' AND '".format_save_date($v_today)."' ";    
            }
            else if($v_type_date=="range_date")
            {
                $where .= " AND production.productiondate BETWEEN '".format_save_date($v_from_date)."' AND '".format_save_date($v_to_date)."' ";
            } 
            
            if($search_batchnumber)
            {
                $where .= " AND production.batchnumber LIKE '%".$search_batchnumber."%' ";    
            }
        }
        else if($search_by=="formula")
        {
            if($v_type_date=="today")
            {
                $where .= " AND production.productiondate = '".format_save_date($v_today)."' ";
            }
            else if($v_type_date=="7_day_before")
            {
                $where .= " AND production.productiondate BETWEEN '".format_save_date($v_last_7day)."' AND '".format_save_date($v_today)."' ";    
            }
            else if($v_type_date=="range_date")
            {
                $where .= " AND production.productiondate BETWEEN '".format_save_date($v_from_date)."' AND '".format_save_date($v_to_date)."' ";
            } 
            
            if($search_formula)
            {
                $arr_keyword[0] = "formula.formulanumber"; 
                $arr_keyword[1] = "formula.formulaname";
                
                $where .= search_keyword($search_formula, $arr_keyword);
            }
        }
        else if($search_by=="keyword")
        {
            if($v_type_date=="today")
            {
                $where .= " AND production.productiondate = '".format_save_date($v_today)."' ";
            }
            else if($v_type_date=="7_day_before")
            {
                $where .= " AND production.productiondate BETWEEN '".format_save_date($v_last_7day)."' AND '".format_save_date($v_today)."' ";    
            }
            else if($v_type_date=="range_date")
            {
                $where .= " AND production.productiondate BETWEEN '".format_save_date($v_from_date)."' AND '".format_save_date($v_to_date)."' ";
            } 
            
            if($search_keyword)
            {
                $arr_keyword[0] = "production.batchnumber"; 
                $arr_keyword[1] = "formula.formulanumber";
                $arr_keyword[2] = "formula.formulaname";
                
                $where .= search_keyword($search_keyword, $arr_keyword);
            }
        }
        
        $q = "
            SELECT
                production.productionid,
                production.productiondate,
                production.warehousecode,
                production.batchnumber,
                formula.formulanumber,
                formula.formulaname,
                production.status_batchnumber
            FROM
                production
                INNER JOIN formula ON
                    production.formulanumber = formula.formulanumber
                    AND production.mixquantity*1!='0' 
            WHERE
                1
                ".$where."
            ORDER BY
                production.productiondate ASC
        ";
        //echo $q;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($productionid, $productiondate, $warehousecode, $batchnumber, $formulanumber, $formulaname, $status_batchnumber) = $row;
            
            $arr_data["list_data"][$batchnumber] = $batchnumber;
            
            $arr_data["list_batchnumber"][$batchnumber] = $batchnumber;
            $arr_data["list_warehousecode"][$warehousecode] = $warehousecode;
            
            $arr_data["productiondate"][$batchnumber] = $productiondate;
            $arr_data["warehousecode"][$batchnumber] = $warehousecode;
            $arr_data["formulanumber"][$batchnumber] = $formulanumber;
            $arr_data["formulaname"][$batchnumber] = $formulaname;
            $arr_data["status_batchnumber"][$batchnumber] = $status_batchnumber;
        } 
        
        $arr_stock = bincard_wip_stock($arr_data["list_batchnumber"],$arr_data["list_warehousecode"]);
        
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('15','05')
                ORDER BY
                    gudang.KdGudang ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $NamaGudang) = $row;
            
            $arr_data["list_warehouse"][$KdGudang] = $KdGudang;
            
            $NamaGudang = str_replace("GUDANG", "", $NamaGudang);
            $arr_data["warehouse_warehousename"][$KdGudang] = $NamaGudang;
        } 

    ?>
    <div class="col-md-12">
    
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_production_data.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
			<tr class="title_table">
				<td>No</td>
				<td>Date</td>
				<td>Gudang</td>
				<td>Batchnumber</td>
				<td>Formula</td>
				<!--<td>Sisa WIP (Kg)</td>-->
				<td>Status</td>
				<td>
					Action<br/>
					<input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')">
				</td>
			</tr>
		<tbody style="color: black;">
		    <?php
		    if(count($arr_data["list_data"])*1==0)
		    {
				echo "
					<tr>
						<td colspan='100%' align='center'>No Data</td>
					</tr>
				";
			}
		    
		    $nomor = 0;    
		    foreach($arr_data["list_data"] as $batchnumber => $val)
		    {    
		        $nomor++;
		        
		        $productiondate = $arr_data["productiondate"][$batchnumber];
		        $warehousecode = $arr_data["warehousecode"][$batchnumber];
		        $formulanumber = $arr_data["formulanumber"][$batchnumber];
		        $formulaname = $arr_data["formulaname"][$batchnumber];
		        $status_batchnumber = $arr_data["status_batchnumber"][$batchnumber];
		        
		        $sisa_wip = $arr_stock["sum_akhir"][$batchnumber];
		        
		        $style_color = "";
		        if($v_batchnumber_curr==$batchnumber)
		        {
		            $style_color = "background:#CAFDB5;";    
		        }
		        
		    ?>
		    
		    <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color; ?>">
		        <td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $nomor; ?></td>
		        <td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><?php echo format_show_date($productiondate); ?></td>
		        <td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $arr_data["warehouse_warehousename"][$warehousecode]; ?></td>
		        <td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $batchnumber; ?></td>
		        <td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>"><?php echo $formulanumber." :: ".$formulaname; ?></td>
		        <!--<td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>" align="right"><?php echo format_number($sisa_wip, 4); ?></td>-->
		        <td onclick="CallAjax('mixing','<?php echo $batchnumber; ?>')" style="<?php echo $style_bg; ?>" align="center"><b><?php echo $status_batchnumber; ?></b></td>
		        <td style="<?php echo $style_bg; ?>" align="center"><input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $batchnumber; ?>"></td> 
		    </tr>
		    <?php
		    }
		    ?>
    
		    <tr>
		        <td colspan="100%" align="right">
		            <select name="action" id="action" class="form-control-new">
		                <option value="close_batchnumber">Close BatchNumber</option>
		                <!--<option value="open_batchnumber">Open BatchNumber</option>-->
		            </select>
		            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_submit" id="btn_submit" value="Submit">Submit<i class="entypo-submit"></i></button>
		        </td>
		    </tr>
    	</tbody>
    </table>
    </form>
    </div>
    <?php
    
    }
    else if($ajax=="add_data")
    {    
        // cek warehouseadmin
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('15','05')
                ORDER BY
                    gudang.KdGudang ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $Keterangan) = $row;
            
            $arr_data["list_warehouse"][$KdGudang] = $KdGudang;
            
            $arr_data["warehouse_warehousename"][$KdGudang] = $KdGudang." :: ".$Keterangan;
        }
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb" class="title_table">
				<li >
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Add MIXING
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            <input type="hidden" name="prod_batch" id="prod_batch" value="">
            <input type="hidden" name="total_wip_kg" id="total_wip_kg" value="">
            
            <table class="table table-bordered responsive" style="color: black;">
                
                <tr >
                    <td width="150" class="title_table">Warehouse</td>
                    <td>
                        <select name="v_warehousecode" id="v_warehousecode" class="form-control-new" style="width: 400px;" onchange="CallAjax('ajax_warehouse', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $KdGudang=>$val)                 
                                {
                                    $Keterangan = $arr_data["warehouse_warehousename"][$KdGudang];
                                    ?>
                                    <option value="<?php echo $KdGudang; ?>"><?php echo $Keterangan; ?></option>
                                    <?php        
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Formula</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_keyword_formula" id="v_keyword_formula" size="10" placeholder="Keyword" onkeyup="CallAjax('search_keyword_formula_mixing', this.value)">
                        &nbsp;
                        <span id="form_formula">
                        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                                {
                                    $formulaname = $arr_data["formulaname"][$formulanumber];
                                    ?>
                                        <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                        </span>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">&nbsp;</td>
                    <td id="form_matreq">
                        <table class="table table-bordered responsive">
								<tr class="title_table">
									<td>Matreq No</td>
									<td>Date</td>
									<td>Batch Req</td>
									<td>Remain</td>
									<td>Prod Batch</td>
								</tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Date</td>
                    <td>
                        <input type="text" class="form-control-new" size="10" maxlength = "10" name="v_productiondate" id="v_productiondate" value="<?php echo date("d-m-Y"); ?>" class="text">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_productiondate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">BatchNumber</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_batch_1" id="v_batch_1" size="10" maxlength="6" readonly="readonly" style="text-align: right;">
                        &nbsp;
                        <input type="text" class="form-control-new" name="v_batch_2" id="v_batch_2" maxlength="30" style="width: 300px;">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Prod Batch</td>
                    <td id="form_prod_batch" style="font-weight: bold;">&nbsp;</td>
                </tr>
                
                <tr>
                    <td class="title_table">WIP QTY (Kg)</td>
                    <td id="form_total_wip_kg" style="font-weight: bold;">&nbsp;</td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td id="form_detail">
                    	<table class="table table-bordered responsive">
								<tr class="title_table">
									<td>No</th>
									<td>PCode</th>
									<td>Nama Barang</th>
									<td>Bincard</th>
									<td>Qty</th>
								</tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                    	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save Mixing<i class="entypo-check"></i></button>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <?php
    }
    else if($ajax=="add_data_wip")
    {    
        // cek warehouseadmin
        $q = "
                SELECT
                    gudang_admin.KdGudang
                FROM
                    gudang_admin
                WHERE
                    1
                    AND gudang_admin.UserName = '".$ses_login."'
                    AND gudang_admin.KdGudang IN('05','15')
        ";
        
        $qry_warehouseadmin = mysql_query($q);
        while($row_warehouseadmin = mysql_fetch_array($qry_warehouseadmin))
        {
            list($KdGudang) = $row_warehouseadmin;
            
            $arr_data["gudang_admin"][$KdGudang] = $KdGudang;
        }
        
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('05','15')
                ORDER BY
                    gudang.KdGudang ASC
        ";
       
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $Keterangan) = $row;
            
            $arr_data["list_warehouse"][$KdGudang] = $KdGudang;
            
            $arr_data["warehouse_warehousename"][$KdGudang] = $KdGudang." :: ".$Keterangan;
        }
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Add Mixing WIP
					</a>
				</li>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data_wip">
            
           	<table class="table table-bordered responsive">
                
                <tr width="250">
                    <td class="title_table">Warehouse</td>
                    <td>
                        <select name="v_warehousecode" id="v_warehousecode" class="form-control-new" style="width: 400px;" onchange="CallAjax('ajax_warehouse_wip', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $KdGudang=>$val)                 
                                {
                                    $Keterangan = $arr_data["warehouse_warehousename"][$KdGudang];
                                    ?>
                                    <option value="<?php echo $KdGudang; ?>"><?php echo $Keterangan; ?></option>
                                    <?php        
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Formula</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_keyword_formula" id="v_keyword_formula" size="10" placeholder="Keyword" onkeyup="CallAjax('search_formula_mix_wip', this.value)">
                        &nbsp;
                        <span id="form_formula">
                        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_wip', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                                {
                                    $formulaname = $arr_data["formulaname"][$formulanumber];
                                    ?>
                                        <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                        </span>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_productiondate" id="v_productiondate" value="<?php echo date("d-m-Y"); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_productiondate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">BatchNumber</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_batch_1" id="v_batch_1" size="10" maxlength="6" readonly="readonly" style="text-align: right;">
                        &nbsp;
                        <input type="text" class="form-control-new" name="v_batch_2" id="v_batch_2" maxlength="30" style="width: 300px;">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">WIP Qty</td>
                    <td><input type="text" class="form-control-new" name="v_wip_qty" id="v_wip_qty" style="text-align: right; width: 400px;" value="" onblur="toFormat4('v_wip_qty')"></td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td id="form_detail">
                    	<table class="table table-bordered responsive">
						    <thead>
								<tr>
									<th><strong><center>No</center></strong></th>
									<th><strong><center>Date</center></strong></th>
									<th><strong><center>BatchNumber</center></strong></th>
									<th><strong><center>Data Qty</center></strong></th>
									<th><strong><center>Mixing Qty</center></strong></th>
								</tr>
							</thead>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                    	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save Mixing WIP<i class="entypo-check"></i></button>
                    </td>
                </tr>
            </table>
            </form>
    	</div>
    <?php
    }
    else if($ajax=="mixing")
    { //================================================== mixing =======================================
        $v_batchnumber = $_GET["v_batchnumber"];
        
        $q = "
                SELECT
                    production.productionid,
                    production.batchnumber,
                    gudang.KdGudang,
                    gudang.Keterangan,
                    formula.formulanumber,
                    formula.formulaname,
                    production.productiondate,
                    production.batchquantity,
                    production.mixquantity
                FROM
                    production
                    INNER JOIN gudang ON
                        production.warehousecode = gudang.KdGudang
                    INNER JOIN formula ON
                        production.formulanumber = formula.formulanumber
                WHERE
                    1
                    AND production.batchnumber = '".$v_batchnumber."'
                    AND production.mixquantity*1!= '0'
                LIMIT
                    0,1
        ";
        //echo "Query 1 OK = ".$q."<br><br>";//hello mixing
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($productionid, $batchnumber, $warehousecode, $warehousename, $formulanumber, $formulaname, $productiondate, $batchquantity, $mixquantity) = $row;
        
        
        $q = "
                SELECT
                    matreq.matreqnumber,
                    matreq.matreqdate,
                    matreq.quantity,
                    matreq.batchremain,
                    productionbatchrequest.prod_batch
                FROM
                    matreq
                    INNER JOIN productionbatchrequest ON
                        matreq.matreqnumber = productionbatchrequest.matreqnumber
                        AND productionbatchrequest.batchnumber = '".$v_batchnumber."'
                        AND productionbatchrequest.production = '1'
                WHERE
                    1
                ORDER BY
                    matreq.matreqdate ASC
        ";
        //echo "Query 2 OK = ".$q."<br><br>";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber, $matreqdate, $quantity, $batchremain, $prod_batch) = $row;    
            
            $arr_data["list_matreq"][$matreqnumber] = $matreqnumber;
            $arr_data["matreq_date"][$matreqnumber] = $matreqdate;
            $arr_data["matreq_quantity"][$matreqnumber] = $quantity;
            $arr_data["matreq_batchremain"][$matreqnumber] = $batchremain;
            $arr_data["matreq_prod_batch"][$matreqnumber] = $prod_batch;
        }
        
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap,
                    SUM(productionmixing.quantity) AS qty
                FROM
                    productionmixing
                    INNER JOIN masterbarang ON
                        productionmixing.inventorycode = masterbarang.PCode
                        AND productionmixing.batchnumber = '".$v_batchnumber."'
                WHERE
                    1
                GROUP BY
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                ORDER BY
                    masterbarang.PCode ASC
        ";
        //echo "Query 3 = ".$q."<br><br>"; //HELLO
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($PCode, $NamaLengkap, $qty) = $row;    
            
            $arr_data["list_inventory"][$PCode] = $PCode;
            $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
            $arr_data["mixing_qty"][$PCode] = $qty;
        }
        

        ?>
        <div class="col-md-12" align="left">
			
            <div align="center">
                <?php echo menutab($v_batchnumber, 'mixing'); ?>  
            </div>
			
            <?php
                if(count($arr_data["list_inventory"])*1==0)
                {
            ?>
			<blockquote class="blockquote-red">
				<p>
					<small>
						<?php 
			                
			                    echo "<div align='center' style='color:red; font-size:16px;'>";
			                    echo "<br><br><br>BatchNumber ".$v_batchnumber." tidak memiliki MIXING";
			                    echo "</div>";
			                    die();    
			                
			            ?>
					</small>
				</p>        
			</blockquote>
            <?php 
                }
            ?>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="mixing">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_batchnumber" id="v_batchnumber" value="<?php echo $v_batchnumber; ?>">
            <input type="hidden" name="v_productionid" id="v_productionid" value="<?php echo $productionid; ?>">
            <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $warehousecode; ?>">
            
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Data MXING <?php echo $v_batchnumber; ?>
					</a>
				</li>
			</ol>
			
            <table class="table table-bordered responsive" style="color: black;">
                
                <tr >
                    <td width="150" class="title_table">Gudang</td>
                    <td style="font-weight:bold; "><?php echo str_replace("GUDANG", "", $warehousename); ?></td>
                </tr>
                
                <tr>
                    <td class="title_table">Formula</td>
                    <td style="font-weight:bold; "><?php echo $formulanumber." :: ".$formulaname; ?></td>
                </tr>
                
                <tr>
                    <td class="title_table">&nbsp;</td>
                    <td id="form_matreq">
                    	<table class="table table-bordered responsive">
								<tr class="title_table">
									<td align="center">Matreq No</td>
									<td align="center">Date</td>
									<td align="right">Batch Req</td>
									<td align="right">Remain</td>
									<td align="right">Prod Batch</td>
								</tr>
							<tbody style="color: black;">
                            	<?php 
                                foreach($arr_data["list_matreq"] as $matreqnumber=>$val)
                                {
                                    $matreqdate = $arr_data["matreq_date"][$matreqnumber];
                                    $quantity = $arr_data["matreq_quantity"][$matreqnumber];
                                    $batchremain = $arr_data["matreq_batchremain"][$matreqnumber];
                                    $prod_batch = $arr_data["matreq_prod_batch"][$matreqnumber];
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td align="center"><?php echo $matreqnumber; ?></td>
                                        <td style="text-align: center;"><?php echo format_show_date($matreqdate); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($quantity, 4); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($batchremain, 4); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($prod_batch, 4); ?></td>
                                    </tr> 
                                    <?php
                                }
                            	?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_productiondate" id="v_productiondate" value="<?php echo format_show_date($productiondate); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_productiondate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">BatchNumber</td>
                    <td style="font-weight:bold; ">
                        <input type="text" class="form-control-new" style="width:200px;" name="v_batchnumber" id="v_batchnumber" value="<?php echo $batchnumber; ?>" >
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Prod Batch</td>
                    <td style="font-weight: bold;"><?php echo format_number($batchquantity, 4); ?></td>
                </tr>
                
                <tr>
                    <td class="title_table">WIP QTY (Kg)</td>
                    <td style="font-weight: bold;"><?php echo format_number($mixquantity, 4); ?></td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td id="form_detail">
                    	<table class="table table-bordered responsive">
								<tr class="title_table">
									<td>No</td>
									<td>PCode</td>
									<td>Nama Barang</td>
									<td align="right">Qty</td>
								</tr>
							<tbody style="color: black;">
	                            <?php
                                $no = 1;
                                $total_mixing = 0;
                                foreach($arr_data["list_inventory"] as $PCode=>$val)
                                { 
                                    $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                                    $qty = $arr_data["mixing_qty"][$PCode];
                                    ?>
                                     <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $PCode; ?></td>
                                        <td><?php echo $NamaLengkap; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($qty, 6); ?></td>
                                    </tr> 
                                    <?php                                      
                                    $total_mixing += $qty;
                                    $no++;
                                }
	                            ?>
	                            <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="text-align: right; font-weight: bold;">
	                                <td colspan="3">Total Mixing</td>
	                                <td style="text-align: right;"><?php echo format_number($total_mixing, 6); ?></td>
	                            </tr> 
							</tbody> 
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td style="font-weight: bold;">
                        <?php 
                            // cek packaging nya
                            $q = "
                                    SELECT
                                        production.productionid
                                    FROM
                                        production
                                    WHERE
                                        1
                                        AND production.batchnumber = '".$batchnumber."'
                                        AND COALESCE(production.mixquantity, NULL, '0')*1='0'
                                    LIMIT
                                        0,1
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($productionid_pack) = $row;
                            
                            if($productionid_pack)
                            {
                                echo "<font color='red'><b>Button Save tidak muncul Karena sudah ada Packaging</b></font>";    
                            }
                            else
                            {
                        ?>
                        <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save Mixing<i class="entypo-check"></i></button>
                        <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_delete" id="btn_delete" value="Delete" onclick="confirm_delete('<?php echo $v_batchnumber; ?>')">Delete Mixing<i class="entypo-cancel"></i></button>
                        <?php 
                            }
                        ?>
                    </td>
                </tr>
            </table> 
            </form>
        </div>
   	<?php    
    } 
    // ============================================== end mixing =============================================
    else if($ajax=="ajax_warehouse")
    {
        $v_warehousecode = $_GET["v_warehousecode"];
        
        $q="
            SELECT 
              matreq.warehousecode,
              matreq.matreqnumber,
              matreq.matreqdate,
              matreq.adjustment,
              matreq.reqtype,
              matreq.formulanumber
            FROM
              matreq 
            WHERE
                1
                AND matreq.adjustment = '0'
                AND matreq.batchremain != '0'
                AND matreq.warehousecode = '".$v_warehousecode."'
            ORDER BY  
                matreq.matreqdate ASC
        ";
        
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $matreqnumber, $matreqdate, $adjustment, $reqtype, $formulanumber) = $row;
            
            $arr_data["list_formula_current"][$formulanumber] = $formulanumber;
        }
        
        if(count($arr_data["list_formula_current"])*1>0)
        {
            $where_formula = where_array($arr_data["list_formula_current"], "formula.formulanumber", "in");
            
            $q = "
                    SELECT 
                        formula.formulanumber,
                        formula.formulaname
                    FROM
                        formula
                    WHERE
                        1
                        ".$where_formula."
                    ORDER BY
                        formula.formulaname ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($formulanumber, $formulaname) = $row;
                
                $arr_data["list_formula"][$formulanumber] = $formulanumber;
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }
        
        ?>
        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    ?>
                        <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                    <?php
                }
            ?>
        </select> 
        <?php
         
        echo "||";
        ?>
        <table class="table table-bordered responsive">
		    <thead>
				<tr>
					<th><strong><center>Matreq No</center></strong></th>
					<th><strong><center>Date</center></strong></th>
					<th><strong><center>Batch Req</center></strong></th>
					<th><strong><center>Remain</center></strong></th>
					<th><strong><center>Prod Batch</center></strong></th>
				</tr>
			</thead>
        </table> 
        <?php
        echo "||";
        echo "||";
        ?>
        <table class="table table-bordered responsive">
		    <thead>
				<tr>
					<th><strong><center>No</center></strong></th>
					<th><strong><center>PCode</center></strong></th>
					<th><strong><center>Nama Barang</center></strong></th>
					<th><strong><center>Bincard</center></strong></th>
					<th><strong><center>Qty</center></strong></th>
				</tr>
			</thead>
        </table> 
        <?php
    }
    else if($ajax=="ajax_warehouse_wip")
    {
        $v_warehousecode = $_GET["v_warehousecode"];
        
        $q="
            SELECT 
              production.formulanumber
            FROM
              production 
            WHERE
                1
                AND production.mixquantity*1 != '0'
                AND production.warehousecode = '".$v_warehousecode."'
                AND production.status_batchnumber = 'Open'
            ORDER BY  
                production.productiondate ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber) = $row;
            
            $arr_data["list_formula_current"][$formulanumber] = $formulanumber;
        }
        
        if(count($arr_data["list_formula_current"])*1>0)
        {
            $where_formula = where_array($arr_data["list_formula_current"], "formula.formulanumber", "in");
            
            $q = "
                    SELECT 
                        formula.formulanumber,
                        formula.formulaname
                    FROM
                        formula
                    WHERE
                        1
                        ".$where_formula."
                    ORDER BY
                        formula.formulaname ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($formulanumber, $formulaname) = $row;
                
                $arr_data["list_formula"][$formulanumber] = $formulanumber;
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }
        
        ?>
        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_wip', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    ?>
                        <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                    <?php
                }
            ?>
        </select> 
        <?php
        echo "||";
        echo "||";
        ?>
        <table class="table table-bordered responsive">
		    <thead>
				<tr>
					<th><strong><center>No</center></strong></th>
					<th><strong><center>Date</center></strong></th>
					<th><strong><center>BatchNumber</center></strong></th>
					<th><strong><center>Data Qty</center></strong></th>
					<th><strong><center>Mixing Qty</center></strong></th>
				</tr>
			</thead>
        </table>
        <?php
    }
    else if($ajax=="ajax_formula")
    {
        $v_formulanumber = $_GET["v_formulanumber"];
        $v_warehousecode = $_GET["v_warehousecode"];
        
        $v_mm   = date("m");
        $v_yyyy = date("Y");
        
        $q = "
               SELECT
                    matreq.matreqnumber,
                    matreq.matreqdate,
                    matreq.quantity,
                    matreq.batchremain
               FROM
                    matreq
               WHERE
                    1
                    AND matreq.formulanumber = '".$v_formulanumber."' 
                    AND matreq.warehousecode = '".$v_warehousecode."' 
                    AND matreq.batchremain != '0' 
                    AND matreq.adjustment = '0' 
               ORDER BY
                    matreq.matreqdate ASC
        ";
        //echo $q;die;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber, $matreqdate, $quantity, $batchremain) = $row;
            
            $arr_data["list_data"][$matreqnumber] = $matreqnumber;
            $arr_data["matreqdate"][$matreqnumber] = $matreqdate;
            $arr_data["quantity"][$matreqnumber] = $quantity;
            $arr_data["batchremain"][$matreqnumber] = $batchremain;
        }
        
        $q = "
                SELECT
                    formula.inventorycode,
                    formula.quantity AS qty_kg_formula
                FROM
                    formula
                WHERE          
                    1
                    AND formula.formulanumber = '".$v_formulanumber."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($inventoryprefix, $qty_kg_formula) = $row;
        
        
        $q = "
                SELECT
                    masterbarang.PCode,
                    masterbarang.NamaLengkap,
                    (formula.quantity * (formuladetail.percentage/100)) AS qty_kg
                FROM
                    masterbarang
                    INNER JOIN formuladetail ON
                        formuladetail.inventorycode = masterbarang.PCode
                        AND formuladetail.percentage*1!='0' 
                    INNER JOIN formula ON
                        formuladetail.formulanumber = formula.formulanumber
                WHERE          
                    1
                    AND formula.formulanumber = '".$v_formulanumber."'
                ORDER BY
                    masterbarang.PCode ASC
        ";
       
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($PCode, $NamaLengkap, $qty_kg) = $row;
            
            $arr_data["list_baku"][$PCode] = $PCode;
            $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
            $arr_data["qty_kg"][$PCode] = $qty_kg;
        }
        
        $arr_stock = bincard_stock(sprintf("%02s", $v_mm), $v_yyyy, $arr_data["list_baku"], $v_warehousecode, 1);
        
        ?>
        <input type="hidden" name="v_qty_kg_formula" id="v_qty_kg_formula" value="<?php echo $qty_kg_formula; ?>">
        
        <table class="table table-bordered responsive">
		    <thead>
				<tr>
					<th><strong><center>Matreq No</center></strong></th>
					<th><strong><center>Date</center></strong></th>
					<th><strong><center>Batch Req</center></strong></th>
					<th><strong><center>Remain</center></strong></th>
					<th><strong><center>Prod Batch</center></strong></th>
				</tr>
			</thead>
			<tbody>
	            <?php
	            $all_matreq = "";
	            foreach($arr_data["list_data"] as $matreqnumber=>$val)
	            {
	                $matreqdate = $arr_data["matreqdate"][$matreqnumber];
	                $quantity = $arr_data["quantity"][$matreqnumber];
	                $batchremain = $arr_data["batchremain"][$matreqnumber];
	                ?>
	                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	                    <td>
	                        <?php echo $matreqnumber; ?>
	                        <input type="hidden" name="v_matreqnumber[]" value="<?php echo $matreqnumber; ?>">
	                    </td>
	                    <td style="text-align: center;"><?php echo format_show_date($matreqdate); ?></td>
	                    <td style="text-align: right;"><?php echo format_number($quantity,4); ?></td>
	                    <td style="text-align: right;">
	                        <?php echo format_number($batchremain,4); ?>
	                        <input type="hidden" name="v_matreq_batchremain_<?php echo $matreqnumber; ?>" id="v_matreq_batchremain_<?php echo $matreqnumber; ?>" value="<?php echo $batchremain; ?>">
	                    </td>
	                    <td style="text-align: right;"><input type="text" class="text" name="v_prod_batch_<?php echo $matreqnumber; ?>" id="v_prod_batch_<?php echo $matreqnumber; ?>" value="" size="12" onblur="toFormat4('v_prod_batch_<?php echo $matreqnumber; ?>')" style="text-align: right;" onblur="calculate()" onchange="calculate()" onkeyup="calculate()"></td>
	                </tr> 
	                <?php
	                $all_matreq .= $matreqnumber.",";
	            }
	            ?>
			</tbody>
        </table>
        <input type="hidden" name="v_all_matreq" id="v_all_matreq" value="<?php echo $all_matreq; ?>">
        <?php
         
        echo "||";
        echo $inventoryprefix;
        echo "||";
        ?>
        <table class="table table-bordered responsive">
		    <thead>
				<tr>
					<th><strong><center>No</center></strong></th>
					<th><strong><center>PCode</center></strong></th>
					<th><strong><center>Nama Barang</center></strong></th>
					<th><strong><center>Bincard</center></strong></th>
					<th><strong><center>Qty</center></strong></th>
				</tr>
			</thead>
			<tbody>
	            <?php 
	            $no = 1;
	            $all_inventory = "";
	            foreach($arr_data["list_baku"] as $PCode=>$val)
	            {
	                $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
	                $qty_kg = $arr_data["qty_kg"][$PCode];
	                
	                ?>
	                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	                    <td>
	                        <?php echo $no; ?>
	                        <input type="hidden" name="v_qty_kg_<?php echo $PCode; ?>" id="v_qty_kg_<?php echo $PCode; ?>" value="<?php echo $qty_kg; ?>">
	                        <input type="hidden" name="v_inventorycode[]" value="<?php echo $PCode; ?>">
	                        <input type="hidden" name="v_qty_bincard_<?php echo $PCode; ?>" value="<?php echo $arr_stock["akhir"][$PCode]; ?>">
	                    </td>
	                    <td><?php echo $PCode; ?></td>
	                    <td><?php echo $NamaLengkap; ?></td>
	                    <td style="text-align: right;"><?php echo format_number($arr_stock["akhir"][$PCode],6); ?></td>
	                    <td style="text-align: right;">
	                        <input type="hidden" name="v_qty_baku_<?php echo $PCode; ?>" id="v_qty_baku_<?php echo $PCode; ?>" value="">
	                        <span id="td_qty_baku_<?php echo $PCode; ?>"></span>
	                    </td>
	                </tr> 
	                <?php
	                $no++;
	                $all_inventory .= $PCode.",";
	            }         
	            ?>
            
	            <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="text-align: right; font-weight: bold;">
	                <td colspan="4">Total Mixing</td>
	                <td id="td_total_mixing" style="text-align: right;">&nbsp;</td>
	            </tr>  
	    	</tbody>
        </table>
        <input type="hidden" name="v_all_inventory" id="v_all_inventory" value="<?php echo $all_inventory; ?>">
   	<?php   
    }
    else if($ajax=="ajax_formula_wip")
    {
        $v_formulanumber = $_GET["v_formulanumber"];
        $v_warehousecode = $_GET["v_warehousecode"];
        
        $v_mm   = date("m");
        $v_yyyy = date("Y");
        
        
        $q = "
                SELECT
                    formula.inventorycode,
                    formula.quantity AS qty_kg_formula
                FROM
                    formula
                WHERE          
                    1
                    AND formula.formulanumber = '".$v_formulanumber."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($inventoryprefix, $qty_kg_formula) = $row;
        
        $counter = 0;
        $q = "
                SELECT
                    production.warehousecode,
                    production.productiondate,
                    production.batchnumber
                FROM
                    production
                WHERE
                    1
                    AND production.formulanumber = '".$v_formulanumber."'
                    AND production.warehousecode = '".$v_warehousecode."'
                    AND production.mixquantity*1 != '0'
                    AND production.status_batchnumber = 'Open'
                ORDER BY
                    production.productiondate ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($warehousecode, $productiondate, $batchnumber) = $row;
            
            $arr_data["list_warehousecode"][$warehousecode] = $warehousecode;
            $arr_data["list_batchnumber"][$batchnumber] = $batchnumber;
            $arr_data["productiondate"][$batchnumber] = $productiondate;
            
            $counter++;    
        }
        
        $arr_stock = bincard_wip_stock($arr_data["list_batchnumber"],$arr_data["list_warehousecode"]);
        
        echo $inventoryprefix;
        echo "||";
        ?>
        <table class="table table-bordered responsive">
		    <thead>
				<tr>
					<th><strong><center>No</center></strong></th>
					<th><strong><center>Date</center></strong></th>
					<th><strong><center>BatchNumber</center></strong></th>
					<th><strong><center>Data Qty</center></strong></th>
					<th><strong><center>Mixing Qty</center></strong></th>
				</tr>
			</thead>
			<tbody>
	            <?php 
	            $no = 1;
	            $all_batchnumber = "";
	            foreach($arr_data["list_batchnumber"] as $batchnumber=>$val)
	            {
	                $productiondate = $arr_data["productiondate"][$batchnumber];
	                
	                ?>
	                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	                    <td>
	                        <?php echo $no; ?>
	                        <input type="hidden" name="v_batchnumber[]" value="<?php echo $batchnumber; ?>">
	                    </td>
	                    <td style="text-align: center;"><?php echo format_show_date($productiondate); ?></td>
	                    <td><?php echo $batchnumber; ?></td>
	                    <td style="text-align: right;">
	                        <?php echo format_number($arr_stock["sum_akhir"][$batchnumber],4); ?>
	                        <input type="hidden" name="v_qty_bincard_<?php echo $batchnumber; ?>" value="<?php echo $arr_stock["sum_akhir"][$batchnumber]; ?>">
	                    </td>
	                    <td style="text-align: right;">
	                        <input type="text" class="text" size="20" style="text-align: right;" name="v_qty_mixing_<?php echo $batchnumber; ?>" id="v_qty_mixing_<?php echo $batchnumber; ?>" value="" onkeyup="calculate_wip()" onblur="toFormat4('v_qty_mixing_<?php echo $batchnumber; ?>')">
	                    </td>
	                </tr> 
	                <?php
	                $no++;
	                $all_batchnumber .= $batchnumber.",";
	            }         
	            ?>
	            <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="text-align: right; font-weight: bold;">
	                <td colspan="4">Total Mixing</td>
	                <td id="td_total_mixing" style="text-align: right;">&nbsp;</td>
	            </tr>  
			</tbody>
        </table>
        <input type="hidden" name="v_all_batchnumber" id="v_all_batchnumber" value="<?php echo $all_batchnumber; ?>">
   	<?php    
    }
    else if($ajax=="mixing_wip")
    { // ========================================== MIXING WIP ===============================================
        $v_batchnumber = $_GET["v_batchnumber"];
        
        $q = "
                SELECT
                    productionmixingwip.productionid
                FROM
                    productionmixingwip
                    INNER JOIN production ON
                        productionmixingwip.productionid = production.productionid
                        AND production.batchnumber = '".$v_batchnumber."'
                        AND production.mixquantity*1!='0'
                WHERE
                    1
                ORDER BY
                    productionmixingwip.productionid ASC
                LIMIT
                    0,1
        ";
        //echo $q;die;//hello
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($productionid_wip) = $row;
        //echo $productionid_wip;
        
        $q = "
                SELECT
                    production.productionid,
                    production.batchnumber,
                    gudang.KdGudang,
                    gudang.Keterangan,
                    formula.formulanumber,
                    formula.formulaname,
                    production.productiondate,
                    production.batchquantity,
                    production.mixquantity
                FROM
                    production
                    INNER JOIN gudang ON
                        production.warehousecode = gudang.KdGudang
                    INNER JOIN formula ON
                        production.formulanumber = formula.formulanumber
                WHERE
                    1
                    AND production.batchnumber = '".$v_batchnumber."'
                    AND production.mixquantity*1!= '0'
                LIMIT
                    0,1
        ";
        //echo $q;die;//hello
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($productionid, $batchnumber, $KdGudang, $Keterangan, $formulanumber, $formulaname, $productiondate, $batchquantity, $mixquantity) = $row;
        
        $counter = 0;
        $q = "
                SELECT
                    productionmixingwip.productionid2,
                    productionmixingwip.batchnumber,
                    productionmixingwip.dataquantity,
                    productionmixingwip.quantity
                FROM
                    productionmixingwip
                WHERE
                    1
                    AND productionmixingwip.productionid = '".$productionid."'
                    AND productionmixingwip.quantity*1!='0'
                ORDER BY
                    productionmixingwip.mixingwipid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($productionid2, $batchnumber, $data_qty, $quantity) = $row;   
            
            $arr_data["list_counter"][$counter] = $counter;
            $arr_data["list_productionid2"][$productionid2] = $productionid2;
            
            $arr_data["wip_productionid2"][$counter] = $productionid2;
            $arr_data["wip_batchnumber"][$counter] = $batchnumber;
            $arr_data["wip_data_qty"][$counter] = $data_qty;
            $arr_data["wip_quantity"][$counter] = $quantity;
            
            $counter++;
        }
        
        $where_productionid2 = where_array($arr_data["list_productionid2"], "production.productionid", "in");
        
        $q = "
                SELECT
                    production.productionid,
                    production.productiondate
                FROM
                    production
                WHERE
                    1
                    ".$where_productionid2."
                ORDER BY
                    production.productionid ASC
        ";
        //echo $q;die;//hello
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($data_productionid, $data_productiondate) = $row; 
            
            $arr_data["data_productiondate"][$data_productionid] = $data_productiondate;
        } 
        

        ?>
        <div class="col-md-12" align="left">
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="mixing_wip">
            <input type="hidden" name="v_del" id="v_del" value="">
            
            <input type="hidden" name="v_batchnumber" id="v_batchnumber" value="<?php echo $v_batchnumber; ?>">
            <input type="hidden" name="v_productionid" id="v_productionid" value="<?php echo $productionid; ?>">
            <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $warehousecode; ?>">
            
            <div align="center">
                <?php echo menutab($v_batchnumber, 'mixing_wip'); ?>  
            </div>
             
            <?php 
            	//huhui
                if($productionid_wip*1==0)
                {
            ?>
            <blockquote class="blockquote-red">
				<p>
					<small>
						<?php 
			            
			                echo "BatchNumber ".$v_batchnumber." tidak memiliki MIXING WIP ";
			                die();
			             
			            ?>
					</small>
				</p>
			</blockquote>
            <?php 
                }
            ?>
            
            <?php 
            if($productionid_wip*1==0)
            {
                echo "<div align='center' style='color:red; font-size:16px;'>";
                echo "<br><br><br>BatchNumber ".$v_batchnumber." tidak memiliki MIXING WIP ";
                echo "</div>";
                die();
            } 
            ?>
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Mixing WIp <?php echo $v_batchnumber; ?>
					</a>
				</li>
			</ol>
            
            <table class="table table-bordered responsive">
            
               <tr>
               		<td class="title_table" width="250">Warehouse</td>
                    <td><?php echo $KdGudang." :: ".$Keterangan; ?></td>
                </tr>
                
                <tr >
                    <td class="title_table">Formula</td>
                    <td><?php echo $formulanumber." :: ".$formulaname; ?></td>
                </tr>
                
                <tr>
                    <td class="title_table">Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_productiondate" id="v_productiondate" value="<?php echo format_show_date($productiondate); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_productiondate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">BatchNumber</td>
                    <td>
                        <input type="text" class="form-control-new" style="width:200px;" name="v_batchnumber" id="v_batchnumber" value="<?php echo $batchnumber; ?>" >
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">WIP Qty</td>
                    <td><b><?php echo format_number($mixquantity, 4); ?></b></td>
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td id="form_detail">
                    	<table class="table table-bordered responsive">
						    <thead>
								<tr>
									<th><strong><center>No</center></strong></th>
									<th><strong><center>Date</center></strong></th>
									<th><strong><center>BatchNumber</center></strong></th>
									<th><strong><center>Data Qty</center></strong></th>
									<th><strong><center>Mixing Qty</center></strong></th>
								</tr>
							</thead>
							<tbody>
	                            <?php
	                            $no = 1; 
	                            $total_wip = 0;
	                            foreach($arr_data["list_counter"] as $counter=>$val)
	                            {
	                                $productionid2 = $arr_data["wip_productionid2"][$counter];
	                                $data_productiondate = $arr_data["data_productiondate"][$productionid2];
	                                $batchnumber = $arr_data["wip_batchnumber"][$counter];
	                                $data_qty = $arr_data["wip_data_qty"][$counter];
	                                $quantity = $arr_data["wip_quantity"][$counter];
	                                
	                                ?>
	                              	<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	                                    <td><?php echo $no; ?></td>
	                                    <td style="text-align: center;"><?php echo format_show_date($data_productiondate); ?></td>
	                                    <td><?php echo $batchnumber; ?></td>
	                                    <td style="text-align: right;"><?php echo format_number($data_qty, 4); ?></td>
	                                    <td style="text-align: right;"><?php echo format_number($quantity, 4); ?></td>
	                                </tr>
	                          
	                                <?php
	                                $total_wip += $quantity;
	                                $no++;
	                            }
	                            ?>
	                            
	                            <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="text-align: right; font-weight: bold;">
	                                <td colspan="4">Total Mixing</td>
	                                <td style="text-align: right;"><?php echo format_number($total_wip, 4); ?></td>
	                            </tr> 
	                            
	                   		</tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                    <?php
                        // cek packaging nya
                            $q = "
                                    SELECT
                                        production.productionid
                                    FROM
                                        production
                                    WHERE
                                        1
                                        AND production.batchnumber = '".$batchnumber."'
                                        AND COALESCE(production.mixquantity, NULL, '0')*1='0'
                                    LIMIT
                                        0,1
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($productionid_pack) = $row;
                            
                            if($productionid_pack)
                            {
                                echo "<font color='red'><b>Button Save tidak muncul Karena sudah ada Packaging</b></font>";    
                            }
                            else
                            {
                        ?>
                        <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save Mixing WIP<i class="entypo-check"></i></button>
                        <button type="button" name="btn_delete" id="btn_delete"  onclick="confirm_delete_wip('<?php echo $v_batchnumber; ?>')" class="btn btn-info btn-icon btn-sm icon-left" value="Delete">Delete Mixing WIP<i class="entypo-cancel"></i></button>
                        <?php 
                            }
                        ?>
                    </td>
                </tr>
            </table>
            </form>
   		</div>
        <?php    
    }
    // =========================================== END MIXING WIP =============================================
    else if($ajax=="adjustment")
    {
        $v_batchnumber = $_GET["v_batchnumber"];

        ?>
        <div class="col-md-12" align="left">
        
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="adjustment">
            <input type="hidden" name="v_del" id="v_del" value="">
            
            <div align="center">
                <?php echo menutab($v_batchnumber, 'adjustment'); ?>
            </div>
            
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Adjustment <?php echo $v_batchnumber; ?>
					</a>
				</li>
			</ol>
			
            <table class="table table-bordered responsive">
                
                <tr>
                    <td class="title_table" width="250">BatchNumber</td>
                    <td><?php echo "<b>".$v_batchnumber."</b>";?></td>
                </tr>
                
            </table>
            
            </form>
    	</div>
        <?php    
    }
    else if($ajax=="packaging")
    { // =========================================== packaging =======================================
        $v_batchnumber = $_GET["v_batchnumber"];
        
        $q = "
                SELECT
                    *
                FROM
                    production
                        
                WHERE
                    1
                    AND production.batchnumber = '".$v_batchnumber."' 
                    AND production.mixquantity*1!='0'
                ORDER BY
                    production.productionid ASC
        ";
        $qry_old = mysql_query($q);
        $arr_old = mysql_fetch_array($qry_old);
        
        $counter = 0;
        $q = "
                SELECT
                    production.productionid,
                    production.productiondate,
                    formula.formulanumber,
                    formula.formulaname,
                    production.batchquantity
                FROM
                    production
                    INNER JOIN formula ON
                        production.formulanoref = formula.formulanumber
                        AND production.batchnumber = '".$v_batchnumber."' 
                        AND COALESCE(production.mixquantity, NULL, '0')*1='0'
                WHERE
                    1
                ORDER BY
                    production.productiondate ASC
        ";
        
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($productionid, $productiondate, $formulanumber, $formulaname, $batchquantity) = $row;
            
            $arr_data["list_data"][$counter] = $counter;
            $arr_data["list_productionid"][$productionid] = $productionid;
            
            $arr_data["data_productionid"][$counter] = $productionid;
            $arr_data["data_productiondate"][$counter] = $productiondate;
            $arr_data["data_formulanumber"][$counter] = $formulanumber;
            $arr_data["data_formulaname"][$counter] = $formulaname;
            $arr_data["data_batchquantity"][$counter] = $batchquantity;
            $counter++;
        }
        
        if(count($arr_data["list_productionid"])*1>0)
        {
            $where_productionid = where_array($arr_data["list_productionid"], "productionfinishgood.productionid2", "in");
        
            $q = "
                    SELECT
                        productionfinishgood.productionid,    
                        productionfinishgood.productionid2
                    FROM
                        productionfinishgood
                    WHERE
                        1
                        ".$where_productionid."
                        AND productionfinishgood.quantity*1!='0'
                    ORDER BY
                        productionfinishgood.productionid
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($productionid, $productionid2) = $row;
                
                $arr_data["fg_exists"][$productionid2] = $productionid;
            }
            
        }
        

        ?>
        <div class="col-md-12" align="left">
			
            <div align="center">
                <?php echo menutab($v_batchnumber, 'packaging'); ?>
            </div>
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Packaging <?php echo $v_batchnumber; ?>
					</a>
				</li>
			</ol>
            
            <table class="table table-bordered responsive">
                
                <?php 
                    if($arr_old["status_batchnumber"]=="Open")
                    {
                ?>
                <tr>
                    <td colspan="100%">
                        <table style="font-weight: bold; cursor: pointer;" onclick="CallAjax('add_packaging', '<?php echo $v_batchnumber; ?>')">
                            <tr>
                                <td><img src="images/add-icon.png"></td>
                                <td>Packaging</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php 
                    }
                ?>
                
					<tr class="title_table">
						<td>No.</td>
						<td>Pack Date</td>
						<td>Formula Ref</td>
						<td>Batch Qty</td>
						<td>Action</td>
					</tr>
				<tbody style="color: black;">
                
                	<?php 
                    $no = 1;
                    
                    if($ses_login=="hendri1003")
                    {
                        echo "<pre>";
                        print_r($arr_data["fg_exists"]);
                        echo "</pre>";
                    }
                    
                    foreach($arr_data["list_data"] as $counter=>$val)
                    {
                        $productionid = $arr_data["data_productionid"][$counter];
                        $productiondate = $arr_data["data_productiondate"][$counter];
                        $formulanumber = $arr_data["data_formulanumber"][$counter];
                        $formulaname = $arr_data["data_formulaname"][$counter];
                        $batchquantity = $arr_data["data_batchquantity"][$counter];
                        
                        ?>
                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                            <td><?php echo $no; ?></td>
                            <td style="text-align: center; "><?php echo format_show_date($productiondate); ?></td>
                            <td>
                                <?php 
                                    if($ses_login=="mecahel0101")
                                    {
                                        echo $productionid." - ";
                                    }
                                ?>
                            
                                <?php echo $formulanumber." :: ".$formulaname; ?>
                            </td>
                            <td style="text-align: right; "><?php echo format_number($batchquantity,4); ?></td>
                            
                            <td style="text-align: center;">
                                <img src="images/edit.gif" style="cursor: pointer;" onclick="CallAjax('edit_packaging', '<?php echo $productionid; ?>')" title="Edit Packaging <?php echo format_show_date($productiondate); ?>">
                                
                                <?php 
                                    //if(!$arr_data["fg_exists"][$productionid])
                                    //{
                                ?>
                                &nbsp;
                            
                                <img src="images/cross.gif" style="cursor: pointer;" onclick="confirm_delete_packaging('<?php echo $productionid; ?>', '<?php echo format_show_date($productiondate); ?>', '<?php echo $v_batchnumber; ?>')" title="Delete Packaging <?php echo format_show_date($productiondate); ?>">
                                <?php 
                                    //}
                                ?>
                            </td>
                            
                        </tr> 
                        <?php
                        $no++;
                        $arr_total["batchquantity"] += $batchquantity;
                    }
                	?>
                
	                <tr style="text-align: right; font-weight: bold;" >
	                    <td colspan="3">Total&nbsp;</td>
	                    <td><?php echo format_number($arr_total["batchquantity"],4); ?></td>
	                    <td>&nbsp;</td>
	                </tr>
                
	                <tr>
	                    <td colspan="100%" id="table_edit">&nbsp;</td>
	                </tr>
            	</tbody>
            </table>
    	</div>
        <?php    
    }
    // ======================================= and packaging ================================================
    else if($ajax=="finish_good")
    { // ===================================== finish goods =================================================
        $v_batchnumber = $_GET["v_batchnumber"];
        
        $q = "
                SELECT
                    production.productionid,
                    production.productiondate,
                    masterbarang.PCode,
                    masterbarang.NamaLengkap
                FROM
                    production
                    INNER JOIN formula ON
                        production.formulanoref = formula.formulanumber
                        AND production.batchnumber = '".$v_batchnumber."' 
                        AND COALESCE(production.mixquantity, NULL, '0')*1='0'
                    INNER JOIN masterbarang ON
                        masterbarang.PCode = formula.inventorycode
                       
                WHERE
                    1
                ORDER BY
                    production.productiondate ASC
        ";
        // AND inventorymaster.stocktypeid = '3'
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($productionid, $productiondate, $inventorycode, $inventoryname) = $row;
            
            $arr_data["list_data"][$counter] = $counter;
            
            $arr_data["list_production"][$productionid] = $productionid;
            
            $arr_data["data_productionid"][$counter] = $productionid;
            $arr_data["data_productiondate"][$counter] = $productiondate;
            $arr_data["data_inventorycode"][$counter] = $inventorycode;
            $arr_data["data_inventoryname"][$counter] = $inventoryname;
            $counter++;
        }
        
        if(count($arr_data["list_production"])*1>0)
        {
            $where_production = where_array($arr_data["list_production"], "productionfinishgood.productionid2", "in");
            
            $q = "
                    SELECT
                        productionfinishgood.productionid AS productionid_fg,
                        productionfinishgood.productionid2,
                        productionfinishgood.quantity,
                        productionfinishgood.quantityqc,
                        productionfinishgood.quantity_qc_reject,
                        productionfinishgood.qc_reject_remarks
                    FROM
                        productionfinishgood
                    WHERE
                        1
                        ".$where_production."
                    ORDER BY
                        productionfinishgood.productionid2 ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($productionid_fg, $productionid2, $quantity, $quantityqc, $quantity_qc_reject, $qc_reject_remarks) = $row;
                
                $arr_data["data_productionid_fg"][$productionid2] = $productionid_fg;
                
                $arr_data["list_productionid_fg"][$productionid_fg] = $productionid_fg;
                
                $arr_data["data_qty"][$productionid2] = $quantity;
                $arr_data["data_qty_qc"][$productionid2] = $quantityqc;
                $arr_data["data_qc_reject"][$productionid2] = $quantity_qc_reject;
                $arr_data["data_qc_reject_remarks"][$productionid2] = $qc_reject_remarks;
            } 
        }
        
        // cek mutasi antar gudang
        $where_production_fg_mg = where_array($arr_data["list_productionid_fg"], "mutasi_antar_gudang_detail.productionid", "in");
        $where_production_fg_do = where_array($arr_data["list_productionid_fg"], "deliveryorderdetail.productionid", "in");
        
        $q = "
                SELECT
                    uni.productionid
                FROM
                (
                SELECT
                    mutasi_antar_gudang_detail.productionid
                FROM
                    mutasi_antar_gudang_detail
                    INNER JOIN mutasi_antar_gudang ON
                        mutasi_antar_gudang_detail.mgno = mutasi_antar_gudang.mgno
                        AND mutasi_antar_gudang.status NOT IN ('2')
                WHERE
                    1
                    ".$where_production_fg_mg."
               UNION ALL
               SELECT
                    deliveryorderdetail.productionid
               FROM
                    deliveryorderdetail
                    INNER JOIN deliveryorder ON
                        deliveryorderdetail.dono = deliveryorder.dono
                        AND deliveryorder.status NOT IN ('2')
                WHERE
                    1
                    ".$where_production_fg_do."
                )AS uni
                WHERE
                    1
                ORDER BY
                    uni.productionid ASC
                
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($productionid_fg_exist_mg_do) = $row;
            
            $arr_data["exist_mg_do"][$productionid_fg_exist_mg_do] = $productionid_fg_exist_mg_do;
        } 

        ?>
        <div class="col-md-12" align="left">
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="finish_good">
            <input type="hidden" name="v_batchnumber" value="<?php echo $v_batchnumber; ?>">
            
            <div align="center">
                <?php echo menutab($v_batchnumber, 'finish_good'); ?>
            </div>
            
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>FINISH GOOD <?php echo $v_batchnumber; ?>
					</a>
				</li>
			</ol>
			
            <table class="table table-bordered responsive">
					<tr class="title_table">
						<td>No</td>
						<td>Pack Date</td>
						<td>PCode</td>
						<td>Nama Barang</td>
						<td>Qty</td>
						<td>Qty QC</td>
						<td>QC Reject</td>
						<td>Total</td>
					</tr>
				<tbody style="color: black;">
                
                	<?php
                    $all_productionid = ""; 
                    $no = 1;
                    foreach($arr_data["list_data"] as $counter=>$val)
                    {
                        $productionid = $arr_data["data_productionid"][$counter];
                        $productiondate = $arr_data["data_productiondate"][$counter];
                        $inventorycode = $arr_data["data_inventorycode"][$counter];
                        $inventoryname = $arr_data["data_inventoryname"][$counter];
                        $productionid_fg = $arr_data["data_productionid_fg"][$productionid];
                        
                        $all_productionid .= $productionid.",";
                        
                        $qty = $arr_data["data_qty"][$productionid];
                        $qty_qc = $arr_data["data_qty_qc"][$productionid];
                        $qc_reject = $arr_data["data_qc_reject"][$productionid];
                        $qc_reject_remarks = $arr_data["data_qc_reject_remarks"][$productionid];
                        
                        if($qc_reject_remarks)
                        {
                            $remarks_qc_reject_info = "Remark Qc Reject : ".$qc_reject_remarks;
                        }
                        else
                        {
                            $remarks_qc_reject_info = "Remark Qc Reject";
                        }
                        
                        $total = $qty + $qty_qc + $qc_reject;
                        
                        $arr_data["total_qty"] += $qty;
                        $arr_data["total_qty_qc"] += $qty_qc;
                        $arr_data["total_qc_reject"] += $qc_reject;
                        $arr_data["total_all"] += $total;
                        
                        ?>
                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                            <td>
                                <?php echo $no; ?>
                                <input type="hidden" name="v_productionid[]" value="<?php echo $productionid; ?>">
                                <input type="hidden" name="v_productionid_fg_<?php echo $productionid; ?>" value="<?php echo $productionid_fg; ?>">
                            </td>
                            <td style="text-align: center; "><?php echo format_show_date($productiondate); ?></td>
                            <td align="center"><?php echo $inventorycode; ?></td>
                            <td>
                                <?php 
                                    if($ses_login=="hendri1003" || $ses_login=="febri0202")
                                    {
                                        echo $productionid_fg." :: ";
                                    }
                                    echo $inventoryname; 
                                ?>
                            </td>
                            <td style="text-align: right; ">
                                <?php 
                                    if($arr_data["exist_mg_do"][$productionid_fg])
                                    {
                                        echo format_number($qty, 0);
                                    ?>
                                        <input type="hidden" class="text" name="v_qty_<?php echo $productionid; ?>" id="v_qty_<?php echo $productionid; ?>" value="<?php echo format_number($qty, 0); ?>" onblur="toFormat('v_qty_<?php echo $productionid; ?>')" size="12" style="text-align: right;" onkeyup="calculate_fg()" onblur="calculate_fg()" >
                                        
                                    <?php    
                                    }
                                    else
                                    {
                                        ?>
                                        <input type="text" class="form-control-new" name="v_qty_<?php echo $productionid; ?>" id="v_qty_<?php echo $productionid; ?>" value="<?php echo format_number($qty, 0); ?>" onblur="toFormat('v_qty_<?php echo $productionid; ?>')" size="12" style="text-align: right;" onkeyup="calculate_fg()" onblur="calculate_fg()" >
                                        <?php
                                    }
                                ?>
                                
                            </td>
                            <td style="text-align: right; ">
                                <?php 
                                    if($arr_data["exist_mg_do"][$productionid_fg])
                                    {
                                        echo format_number($qty_qc, 0);
                                        ?>
                                        <input type="hidden" class="text" name="v_qty_qc_<?php echo $productionid; ?>" id="v_qty_qc_<?php echo $productionid; ?>" value="<?php echo format_number($qty_qc, 0); ?>" onblur="toFormat('v_qty_qc_<?php echo $productionid; ?>')" size="12" style="text-align: right;" onkeyup="calculate_fg()" onblur="calculate_fg()">
                                        <?php
                                    }
                                    else
                                    {
                                ?>
                                        <input type="text" class="form-control-new" name="v_qty_qc_<?php echo $productionid; ?>" id="v_qty_qc_<?php echo $productionid; ?>" value="<?php echo format_number($qty_qc, 0); ?>" onblur="toFormat('v_qty_qc_<?php echo $productionid; ?>')" size="12" style="text-align: right;" onkeyup="calculate_fg()" onblur="calculate_fg()">
                                <?php 
                                    }
                                ?>
                            </td>
                            <td style="text-align: right; ">
                                <?php 
                                    if($arr_data["exist_mg_do"][$productionid_fg])
                                    {
                                        echo format_number($qc_reject, 0);
                                        ?>
                                        <input type="hidden" class="text" name="v_qc_reject_<?php echo $productionid; ?>" id="v_qc_reject_<?php echo $productionid; ?>" value="<?php echo format_number($qc_reject, 0); ?>" onblur="toFormat('v_qty_qc_reject_<?php echo $productionid; ?>')" size="8" style="text-align: right;" onkeyup="calculate_fg()" onblur="calculate_fg()">
                                        <?php
                                    }
                                    else
                                    {
                                ?>
                                        <input type="text" class="form-control-new" name="v_qc_reject_<?php echo $productionid; ?>" id="v_qc_reject_<?php echo $productionid; ?>" value="<?php echo format_number($qc_reject, 0); ?>" onblur="toFormat('v_qty_qc_reject_<?php echo $productionid; ?>')" size="8" style="text-align: right;" onkeyup="calculate_fg()" onblur="calculate_fg()">
                                <?php 
                                    }
                                ?>
                                <button type="button" onclick="pop_up_qc_reject_remarks('<?php echo $productionid; ?>')" class="btn btn-info btn-icon btn-sm icon-left" value="R" title="<?php echo $remarks_qc_reject_info; ?>">R<i class="entypo-cancel"></i></button>
                            </td>
                            <td style="text-align: right; " id="td_total_<?php echo $productionid; ?>"><?php echo format_number($total,0); ?></td>
                        </tr> 
                        <?php
                        $no++;
                    }
                	?>
                
	                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="text-align: right; font-weight: bold;">
	                    <td colspan="4">
	                        Total
	                        <input type="hidden" name="v_all_productionid" id="v_all_productionid" value="<?php echo $all_productionid; ?>">
	                    </td>
	                    <td style="text-align: right; " id="td_total_qty"><?php echo format_number($arr_data["total_qty"], 0); ?></td>
	                    <td style="text-align: right; " id="td_total_qty_qc"><?php echo format_number($arr_data["total_qty_qc"], 0); ?></td>
	                    <td style="text-align: right; " id="td_total_qc_reject"><?php echo format_number($arr_data["total_qc_reject"], 0); ?></td>
	                    <td style="text-align: right; " id="td_total_all"><?php echo format_number($arr_data["total_all"], 0); ?></td>
	                </tr>  
                   
	                <tr>
	                    <td colspan="100%" align="center">
	                    	<button type="submit" class="btn btn-info btn-sm" name="btn_save" id="btn_save" value="Save">Save Finish Goods<i class="entypo-plus"></i></button>
	                    </td>
	                </tr>
				</tbody>
            </table>
            </form>
        </div>
        <?php    
    }
    // ============================================ end finish goods ========================================
    else if($ajax=="sisa_wip")
    {
        $v_batchnumber = $_GET["v_batchnumber"];

        ?>
        <div class="col-md-12" align="left">
        
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="packaging">
            <input type="hidden" name="v_del" id="v_del" value="">
            <input type="hidden" name="v_undel" id="v_undel" value="">
            
            <div align="center">
                <?php echo menutab($v_batchnumber, 'sisa_wip'); ?>
            </div>
            
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Sisa WIP <?php echo $v_batchnumber; ?>
					</a>
				</li>
			</ol>
            
            <table class="table table-bordered responsive">
					<tr class="title_table">
						<td>No</td>
						<td align="center">Date</td>
						<td>Module</td>
						<td align="right">IN</td>
						<td align="right">OUT</td>
						<td align="right">Balance</td>
					</tr>
				<tbody style="color: black;">
					<?php 
				    $no = 1;
				    $q = "
				            SELECT
				                bincard_wip.carddate,
				                bincard_wip_sort_by.module_name,
				                bincard_wip.qin,
				                bincard_wip.qout
				            FROM
				                bincard_wip
				                INNER JOIN bincard_wip_sort_by ON
				                    bincard_wip.module = bincard_wip_sort_by.module
				            WHERE
				                1
				                AND batchnumber = '".$v_batchnumber."'
				            ORDER BY
				                bincard_wip.carddate ASC,
				                bincard_wip_sort_by.sort_by ASC
				    ";
				    $qry = mysql_query($q);
				    while($row = mysql_fetch_array($qry))
				    {
				        list($carddate, $module_name, $qin, $qout) = $row;
				        
				        $balance += $qin;
				        $balance -= $qout;
						?>                        
		                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
		                    <td><?php echo $no; ?></td>
		                    <td style="text-align: center;"><?php echo format_show_date($carddate); ?></td>
		                    <td><?php echo $module_name; ?></td>
		                    <td style="text-align: right;"><?php echo format_number($qin, 4); ?></td>
		                    <td style="text-align: right;"><?php echo format_number($qout, 4); ?></td>
		                    <td style="text-align: right;"><?php echo format_number($balance, 4); ?></td>
		                </tr>
						<?php
				        $no++; 
				    }
				?>
	        	</tbody>
            </table>
            </form>
        </div>
        <?php    
    }
    else if($ajax=="edit_packaging")
    {
        $v_productionid = $_GET["v_productionid"];   
        
        $q = "
                SELECT
                    production.warehousecode,
                    production.productiondate,
                    production.batchquantity,
                    production.formulanoref,
                    production.batchnumber
                FROM
                    production
                WHERE
                    1
                    AND production.productionid = '".$v_productionid."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($warehousecode, $productiondate, $batchquantity, $formulanoref, $batchnumber) = $row;
        
        $q = "
                SELECT
                    *
                FROM
                    production
                        
                WHERE
                    1
                    AND production.batchnumber = '".$batchnumber."' 
                    AND production.mixquantity*1!='0'
                ORDER BY
                    production.productionid ASC
        ";
        $qry_old = mysql_query($q);
        $arr_old = mysql_fetch_array($qry_old);
        
        $counter = 0;
        $q = "
                    SELECT 
					  uni.PCode,
					  masterbarang.NamaLengkap,
					  uni.quantity 
					FROM
					  (SELECT 
					    productionpackaging.`inventorycode` AS Pcode,
					    productionpackaging.quantity 
					  FROM
					    productionpackaging 
					  WHERE 1 
					    AND productionpackaging.productionid = '".$v_productionid."' 
					  UNION
					  ALL 
					  SELECT 
					    productionpackadditional.`inventorycode` AS PCode,
					    productionpackadditional.quantity 
					  FROM
					    productionpackadditional 
					  WHERE 1 
					    AND productionpackadditional.productionid = '".$v_productionid."') AS uni 
					  INNER JOIN masterbarang 
					    ON uni.PCode = masterbarang.PCode 
					WHERE 1 
					ORDER BY uni.PCode ASC
        ";
       
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname, $quantity) = $row;    
            
            $arr_data["list_data"][$counter] = $counter;
            
            $arr_data["list_inventory"][$inventorycode] = $inventorycode;
            
            $arr_data["inventorycode"][$counter] = $inventorycode;
            $arr_data["inventoryname"][$counter] = $inventoryname;
            $arr_data["quantity"][$counter] = $quantity;
            $counter++;
        }
        
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    formula.isdefault,
                    formula.packagingsize
                FROM
                    formula
                WHERE
                    1
                    AND formula.formulanumber = '".$formulanoref."'
                ORDER BY
                    formula.formulaname ASC,
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($formulanumber, $formulaname, $isdefault, $packagingsize) = $row; 
        
        $counter = 0;
        $q = "
                SELECT
                    matreq.matreqnumber,
                    matreq.matreqdate,
                    matreq.batchremain,
                    productionbatchrequest.prod_batch_packaging
                FROM
                    productionbatchrequest
                    INNER JOIN matreq ON
                        productionbatchrequest.matreqnumber = matreq.matreqnumber 
                WHERE
                    1
                    AND productionbatchrequest.productionid = '".$v_productionid."'
                ORDER BY
                    matreq.matreqdate ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber, $matreqdate, $batchremain, $prod_batch) = $row; 
            
            $arr_data["list_matreq"][$matreqnumber] = $matreqnumber;
            $arr_data["matreq_matreqdate"][$matreqnumber] = $matreqdate;
            $arr_data["matreq_batchremain"][$matreqnumber] = $batchremain;
            $arr_data["matreq_prod_batch"][$matreqnumber] = $prod_batch;
            
            $counter++;
        }
        
        $prod_qty_pcs = $packagingsize * $batchquantity; 
        
        $v_mm = date("m");
        $v_yyyy = date("Y");
        
        $arr_stock = bincard_stock(sprintf("%02s", $v_mm), $v_yyyy, $arr_data["list_inventory"], $warehousecode, 2);
        
        
        ?>
        
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Edit Packaging <?php echo format_show_date($productiondate); ?>
					</a>
				</li>
			</ol>
			
        	<form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="edit_packaging">
            <input type="hidden" name="v_productionid" value="<?php echo $v_productionid; ?>">
            <input type="hidden" name="v_batchnumber" value="<?php echo $batchnumber; ?>">
            <input type="hidden" name="v_del" id="v_del" value="">
            
            <table class="table table-bordered responsive" style="color: black;">
                
                <tr>
                    <td class="title_table" width="150">Formula Ref</td>
                    <td><?php echo $formulanumber." :: ".$formulaname; ?></td>
                </tr>
                
                <tr>
                    <td class="title_table">Pack Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_productiondate" id="v_productiondate" value="<?php echo format_show_date($productiondate); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_productiondate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
               <tr>
                    <td class="title_table">&nbsp;</td>
                    <td>
                    	<table class="table table-bordered responsive">
								<tr class="title_table">
									<td>Matreq No</td>
									<td>Date</td>
									<td>Batch Remain</td>
									<td>Prod Batch</td>
								</tr>
							<tbody style="color: black;">
                            	<?php 
                                foreach($arr_data["list_matreq"] as $matreqnumber=>$val)
                                {
                                    $matreqdate = $arr_data["matreq_matreqdate"][$matreqnumber];
                                    $batchremain = $arr_data["matreq_batchremain"][$matreqnumber];
                                    $prod_batch = $arr_data["matreq_prod_batch"][$matreqnumber];
                                    
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $matreqnumber; ?></td>
                                        <td><?php echo format_show_date($matreqdate); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($batchremain, 4); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($prod_batch, 4); ?></td>
                                    </tr> 
                                    <?php
                                }
                            	?>
							</tbody>
                        </table>
                    </td>
                </tr> 
                
                <tr>
                    <td class="title_table">Prod Qty (pcs)</td>
                    <td><input type="text" class="form-control-new" name="v_prd_qty_pcs" id="v_prd_qty_pcs" size="10" onblur="toFormat('v_prd_qty_pcs')" style="text-align: right;" value="<?php echo format_number($prod_qty_pcs); ?>"></td>
                </tr>
                            
                <tr>
                    <td class="title_table">Batch Qty</td>
                    <td><b><?php echo format_number($batchquantity, 4); ?></b></td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
	                	<table class="table table-bordered responsive">
								<tr class="title_table">
									<td>No</td>
									<td>PCode</td>
									<td>Nama Barang</td>
									<td>Bincard</td>
									<td>Qty</td>
								</tr>
							<tbody style="color: black;">
                            	<?php
                                $no = 1;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $inventorycode = $arr_data["inventorycode"][$counter];
                                    $inventoryname = $arr_data["inventoryname"][$counter];
                                    $quantity = $arr_data["quantity"][$counter];
                                    
                                    $qty_mutation = $arr_data["qty_mutation"][$inventorycode];
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td>
                                            <?php echo $no; ?>
                                            <input type="hidden" name="v_inventorycode[]" value="<?php echo $inventorycode; ?>">
                                            <input type="hidden" class="text" name="v_qty_bincard_<?php echo $inventorycode; ?>" id="v_qty_bincard_<?php echo $inventorycode; ?>" value="<?php echo format_number($arr_stock["akhir"][$inventorycode], 0); ?>" style="text-align: right;"> 
                                        </td>
                                        <td><?php echo $inventorycode; ?></td>
                                        <td><?php echo $inventoryname; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($arr_stock["akhir"][$inventorycode], 0); ?></td>
                                        <td style="text-align: right;">
                                            <input type="text" size="12" class="form-control-new" name="v_qty_kemas_<?php echo $inventorycode; ?>" id="v_qty_kemas_<?php echo $inventorycode; ?>" onblur="toFormat('v_qty_kemas_<?php echo $inventorycode; ?>')" value="<?php echo format_number($quantity, 0); ?>" style="text-align: right;"> 
                                            <input type="hidden" class="text" name="v_qty_kemas_old_<?php echo $inventorycode; ?>" id="v_qty_kemas_old_<?php echo $inventorycode; ?>" value="<?php echo format_number($quantity, 0); ?>" style="text-align: right;"> 
                                        </td>
                                    </tr> 
                                    <?php
                                    $no++;
                                }
                            	?>
            				</<tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?php
                            if($arr_old["status_batchnumber"]=="Open")
                            {
                        ?>
                        <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
                        <?php 
                            }
                            else
                            {
                                echo "<font color='red'><b>Button Save tidak muncul, karena Status Batchnumber sudah Close</b></font>";
                            }
                        ?>
                    </td>
                </tr>
           </table> 
        </form>
    </div>
        <?php
    }
    else if($ajax=="add_packaging")
    {
        $v_batchnumber = $_GET["v_batchnumber"];   
        
        $q = "
                SELECT
                    production.warehousecode,
                    production.formulanumber
                FROM
                    production
                WHERE
                    1
                    AND production.batchnumber = '".$v_batchnumber."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($warehousecode, $data_formulanumber) = $row;
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    formula.isdefault,
                    formula.packagingsize
                FROM
                    formula
                    INNER JOIN matreq ON
                        formula.formulanumber = matreq.formulanumber
                        -- AND matreq.batchremain*1 != '0'
                        AND matreq.reqtype IN('0','2')
                        AND matreq.warehousecode = '".$warehousecode."'
                WHERE
                    1
                    AND (formula.isactive = '1' OR formula.formulanumber = '".$data_formulanumber."')
                ORDER BY
                    formula.formulaname ASC,
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname, $isdefault, $packagingsize) = $row;
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            $arr_data["packagingsize"][$formulanumber] = $packagingsize;
            
            if($isdefault)
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname." (Def)";    
            }
            else
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }  
        
        //echo "<pre>";
        //print_r($arr_data["list_formula"]);  
        //echo "</pre>";
        
        $q = "
                SELECT
                    matreq.matreqnumber,
                    matreq.matreqdate,
                    matreq.batchremain_packaging
                FROM
                    matreq
                WHERE
                    1
                    AND matreq.formulanumber = '".$data_formulanumber."'
                    AND matreq.batchremain_packaging != '0'
                    AND LEFT(matreq.matreqnumber, 2) = 'MR'
                    AND matreq.reqtype IN('0','2')
                ORDER BY
                    matreq.matreqdate ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber, $matreqdate, $batchremain, $prod_batch) = $row; 
            
            $arr_data["list_matreq"][$matreqnumber] = $matreqnumber;
            $arr_data["matreq_matreqdate"][$matreqnumber] = $matreqdate;
            $arr_data["matreq_batchremain"][$matreqnumber] = $batchremain;
            
        }
        
        $counter = 0;
        $q = "
                  SELECT 
					  masterbarang.PCode,
					  masterbarang.NamaLengkap 
					FROM
					  masterbarang
					  INNER JOIN formuladetail 
					    ON masterbarang.PCode = formuladetail.inventorycode 
					    AND formuladetail.quantity * 1 != '0' 
					    AND formuladetail.formulanumber = '".$data_formulanumber."'
					    AND `masterbarang`.`KdKategori`='2'  
					WHERE 1 
					ORDER BY masterbarang.PCode ASC 
        ";
       
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname) = $row; 
            
            $arr_data["list_inventory"][$inventorycode] = $inventorycode; 
            $arr_data["list_data"][$counter] = $counter;
            $arr_data["inventorycode"][$counter] = $inventorycode;
            $arr_data["inventoryname"][$counter] = $inventoryname;
            
            $counter++;
        }
        
        
        $v_mm = date("m");
        $v_yyyy = date("Y");
        
        $arr_stock = bincard_stock(sprintf("%02s", $v_mm), $v_yyyy, $arr_data["list_inventory"], $warehousecode, 2);
        
        
        ?>
        <div class="col-md-12" align="left">
        
			<ol class="breadcrumb">
				<li>
					<a href="javascript:void(0);">
						<i class="entypo-pencil"></i>Add Packaging
					</a>
				</li>
			</ol>
			
        	<form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="packaging">
            <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $warehousecode; ?>">
            <input type="hidden" name="v_batchnumber" id="v_batchnumber" value="<?php echo $v_batchnumber; ?>">
            <input type="hidden" name="v_del" id="v_del" value="">
            
            <table class="table table-bordered responsive" style="color: black;">
                
                <tr>
                    <td class="title_table" width="150">Formula Ref</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_keyword_formula" id="v_keyword_formula" size="10" placeholder="Keyword" onkeyup="CallAjax('search_keyword_formula_packaging', this.value)">
                        &nbsp;
                        <span id="form_formula"> 
                        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_packaging', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                                {
                                    $formulaname = $arr_data["formulaname"][$formulanumber];
                                    
                                    $selected = "";
                                    if($data_formulanumber==$formulanumber)
                                    {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                        <option <?php echo $selected; ?> value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                       </span>  
                    </td>
                </tr>
                
                <tr>
                    <td class="title_table">Pack Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_productiondate" id="v_productiondate" value="<?php echo date("d-m-Y"); ?>" class="form-control-new">
                        <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_productiondate, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
               <tr>
                    <td class="title_table">&nbsp;</td>
                    <td id="td_detail_matreq">
                        <input type="hidden" name="v_packagingsize" id="v_packagingsize" value="<?php echo $arr_data["packagingsize"][$data_formulanumber]; ?>">
                        <table class="table table-bordered responsive">
								<tr class="title_table">
									<td>Matreq No</td>
									<td>Date</td>
									<td>Batch Remain</td>
									<td>Prod Batch</td>
								</tr>
							<tbody style="color: black;">
                            	<?php 
                                $counter = 1;
                                foreach($arr_data["list_matreq"] as $matreqnumber=>$val)
                                {
                                    $matreqdate = $arr_data["matreq_matreqdate"][$matreqnumber];
                                    $batchremain = $arr_data["matreq_batchremain"][$matreqnumber];
                                    $prod_batch = $arr_data["matreq_prod_batch"][$matreqnumber];
                                    
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td>
                                            <?php echo $matreqnumber; ?>
                                            <input type="hidden" name="v_matreqnumber[]" value="<?php echo $matreqnumber; ?>">
                                            <input type="hidden" name="v_matreq_batchremain_<?php echo $matreqnumber; ?>" id="v_matreq_batchremain_<?php echo $matreqnumber; ?>" value="<?php echo $batchremain; ?>">
                                        </td>
                                        <td><?php echo format_show_date($matreqdate); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($batchremain, 4); ?></td>
                                        <td style="text-align: right;">
                                            <!--<input type="text" class="form-control-new" size="12" name="v_prod_batch_<?php echo $matreqnumber; ?>" id="v_prod_batch_<?php echo $matreqnumber; ?>" value="" onblur="toFormat4('v_prod_batch_<?php echo $matreqnumber; ?>')" style="text-align: right;">-->
                                            <input type="text" class="form-control-new" size="12" name="v_prod_batch_<?php echo $matreqnumber; ?>" id="v_prod_batch_<?php echo $matreqnumber; ?>" value="" onblur="ambilMatreq('<?php echo $matreqnumber; ?>')" style="text-align: right;">
                                        </td>
                                    </tr> 
                                    <?php
                                    $counter++;
                                }
                            	?>
                            </tbody>
                        </table>
                    </td>
                </tr> 
                
                <tr>
                    <td class="title_table">Prod Qty (pcs)</td>
                    <td><input type="text" class="form-control-new" name="v_prd_qty_pcs" id="v_prd_qty_pcs" size="10" onblur="toFormat('v_prd_qty_pcs'), calculate_prod_qty()" style="text-align: right;" value="" onkeyup="calculate_prod_qty()"></td>
                </tr>
                            
                <tr>
                    <td class="title_table">Batch Qty</td>
                    <td id="td_batch_qty" style="font-weight: bold;">&nbsp;</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td id="td_detail_inventory">
                    	<table class="table table-bordered responsive" style="color: black;">
                    	<input type="hidden" id="no_mr" value="0">
								<tr class="title_table">
									<td>No.</td>
									<td>PCode</td>
									<td>Nama Barang</td>
									<td>Bincard</td>
									<td>Qty</td>
								</tr>
                            	<?php
                                $no = 1;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $inventorycode = $arr_data["inventorycode"][$counter];
                                    $inventoryname = $arr_data["inventoryname"][$counter];
                                    
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td>
                                            <?php echo $no; ?>                           
                                            <input type="hidden" name="v_inventorycode[]" value="<?php echo $inventorycode; ?>">
                                            <input type="hidden" class="text" name="v_qty_bincard_<?php echo $inventorycode; ?>" id="v_qty_bincard_<?php echo $inventorycode; ?>" value="<?php echo format_number($arr_stock["akhir"][$inventorycode], 0); ?>" style="text-align: right;"> 
                                        </td>
                                        <td><?php echo $inventorycode; ?>
                                        <input type="hidden"  id="v_pcode_<?php echo $inventorycode; ?>" value="<?php echo $inventorycode; ?>" >	
                                        </td>
                                        <td><?php echo $inventoryname; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($arr_stock["akhir"][$inventorycode], 0); ?></td>
                                        <td style="text-align: right;">
                                            <!--<input type="text" size="12" class="form-control-new" name="v_qty_kemas_<?php echo $inventorycode; ?>" id="v_qty_kemas_<?php echo $inventorycode; ?>" onblur="toFormat('v_qty_kemas_<?php echo $inventorycode; ?>')" value="" style="text-align: right;">-->
                                            <input type="text" size="12" class="form-control-new" name="v_qty_kemas_<?php echo $inventorycode; ?>" id="v_qty_kemas_<?php echo $inventorycode; ?>" onblur="cekMR(this)" value="" style="text-align: right;"> 
                                        </td>
                                    </tr> 
                                    <?php
                                    $no++;
                                }
                            	?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                    	<button type="submit" name="btn_delete_details" id="btn_delete_details" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save Packaging<i class="entypo-check"></i></button>
                    </td>
                </tr>
           </table> 
        </form>
        <?php
    }
    else if($ajax=="search_keyword_formula_packaging")
    {
        $v_keyword = save_char($_GET["v_keyword"]);
        
        $arr_keyword[0] = "formula.formulanumber"; 
        $arr_keyword[1] = "formula.formulaname";
        
        $where = search_keyword($v_keyword, $arr_keyword); 
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    formula.isdefault
                FROM
                    formula
                WHERE
                    1
                    AND formula.isactive = '1'
                    ".$where."
                ORDER BY
                    formula.formulaname ASC,
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname, $isdefault) = $row; 
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            if($isdefault)
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname." (Def)";    
            }
            else
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }  
        ?>
        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_packaging', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    
                    $selected = "";
                    if($data_formulanumber==$formulanumber)
                    {
                        $selected = "selected='selected'";
                    }
                    ?>
                        <option <?php echo $selected; ?> value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                    <?php
                }
            ?>
        </select> 
        <?php
    }
    else if($ajax=="search_keyword_formula_mixing")
    {
        $v_keyword = save_char($_GET["v_keyword"]);
        
        $arr_keyword[0] = "formula.formulanumber"; 
        $arr_keyword[1] = "formula.formulaname";
        
        $where = search_keyword($v_keyword, $arr_keyword); 
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    formula.isdefault
                FROM
                    formula
                WHERE
                    1
                    AND formula.isactive = '1'
                    ".$where."
                ORDER BY
                    formula.formulaname ASC,
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname, $isdefault) = $row; 
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            if($isdefault)
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname." (Def)";    
            }
            else
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }  
       
        ?>
        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    
                    $selected = "";
                    if($data_formulanumber==$formulanumber)
                    {
                        $selected = "selected='selected'";
                    }
                    ?>
                        <option <?php echo $selected; ?> value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                    <?php
                }
            ?>
        </select> 
        <?php
    }
    else if($ajax=="search_keyword_formula_transfer")
    {
        $v_keyword = save_char($_GET["v_keyword"]);
        
        $arr_keyword[0] = "formula.formulanumber"; 
        $arr_keyword[1] = "formula.formulaname";
        
        $where = search_keyword($v_keyword, $arr_keyword); 
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    formula.isdefault
                FROM
                    formula
                WHERE
                    1
                    AND formula.isactive = '1'
                    ".$where."
                ORDER BY
                    formula.formulaname ASC,
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname, $isdefault) = $row; 
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            if($isdefault)
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname." (Def)";    
            }
            else
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }  
       
        ?>
        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_transfer', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    
                    $selected = "";
                    if($data_formulanumber==$formulanumber)
                    {
                        $selected = "selected='selected'";
                    }
                    ?>
                        <option <?php echo $selected; ?> value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                    <?php
                }
            ?>
        </select> 
        <?php
    }
    else if($ajax=="delete_packaging")
    {
        $v_productionid = $_GET["v_productionid"];
        
        $q = "
                SELECT
                    formula.packagingsize,
                    formula.quantity,
                    production.batchnumber,
                    production.warehousecode
                FROM
                    production
                    INNER JOIN formula ON
                        formula.formulanumber = production.formulanoref
                        AND production.productionid = '".$v_productionid."'
                WHERE
                    1
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($packagingsize, $quantity_kg, $batchnumber, $warehousecode) = $row;
        
        $q = "DELETE FROM bincard_wip WHERE 1 AND productionid = '".$v_productionid."' AND batchnumber = '".$batchnumber."' AND module = 'PD' ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete bincard_wip";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM productionpackadditional WHERE 1 AND productionid = '".$v_productionid."'";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete productionpackadditional";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM productionpackaging WHERE 1 AND productionid = '".$v_productionid."'";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete productionpackaging";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM bincard WHERE 1 AND bincard.referenceno = '".$v_productionid."' AND module = 'MX'";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete bincard";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "
                SELECT
                    `productionbatchrequest`.matreqnumber
                FROM
                    `productionbatchrequest`
                WHERE
                    1
                    AND `productionbatchrequest`.productionid = '".$v_productionid."'
                ORDER BY
                    `productionbatchrequest`.matreqnumber
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber) = $row;
            
            $arr_data["list_matreq"][$matreqnumber] = $matreqnumber;
        }
        
        $q = "DELETE FROM productionbatchrequest WHERE 1 AND productionbatchrequest.productionid = '".$v_productionid."'";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete productionbatchrequest";
            echo "<script>alert('".$msg."');</script>";     
            die();
        } 
        
        foreach($arr_data["list_matreq"] as $key=>$val)
        {
            $q = "
                    SELECT
                        SUM(`productionbatchrequest`.prod_batch) AS jml_prod_batch
                    FROM
                        `productionbatchrequest`
                    WHERE
                        1
                        AND `productionbatchrequest`.matreqnumber = '".$val."'
            ";
            $qry = mysql_query($q);
            $row = mysql_fetch_array($qry);
            list($jml_prod_batch) = $row;
            
            $jml_prod_batch = $jml_prod_batch*1;
            
            $q = "
                    UPDATE
                        `matreq`
                    SET
                         batchremain = (quantity - ".$jml_prod_batch.")
                    WHERE
                        1
                        AND matreqnumber = '".$val."'
            ";
            if(!mysql_query($q))
            {
                $msg = "Gagal Update Matreq";
                echo "<script>alert('".$msg."');</script>";
                die();     
            }  
        }
                
        
        $q = "DELETE FROM production WHERE 1 AND production.productionid = '".$v_productionid."'";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete production";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        else
        {
            echo $batchnumber;
        } 
    }
    else if($ajax=="ajax_formula_packaging")
    {
        $v_formulanumber = $_GET["v_formulanumber"];
        $v_warehousecode = $_GET["v_warehousecode"];
        
        $v_mm = date("m");
        $v_yyyy = date("Y");
        
        $q = "
               SELECT
                    matreq.matreqnumber,
                    matreq.matreqdate,
                    matreq.quantity,
                    matreq.batchremain_packaging
               FROM
                    matreq
               WHERE
                    1
                    AND matreq.formulanumber = '".$v_formulanumber."' 
                    AND matreq.warehousecode = '".$v_warehousecode."' 
                    AND matreq.batchremain_packaging != '0' 
                    AND LEFT(matreq.matreqnumber, 2) = 'MR'
               ORDER BY
                    matreq.matreqdate ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($matreqnumber, $matreqdate, $quantity, $batchremain) = $row;
            
            $arr_data["list_data"][$matreqnumber] = $matreqnumber;
            $arr_data["matreqdate"][$matreqnumber] = $matreqdate;
            $arr_data["quantity"][$matreqnumber] = $quantity;
            $arr_data["batchremain"][$matreqnumber] = $batchremain;
        }
        
        $q = "
                SELECT 
					  masterbarang.PCode,
					  masterbarang.NamaLengkap,
					  formuladetail.quantity,
					  formula.packagingsize 
					FROM
					  masterbarang 
					  INNER JOIN formuladetail 
					    ON formuladetail.`inventorycode`= masterbarang.PCode 
					    AND formuladetail.quantity * 1 != '0' 
					    AND `masterbarang`.`KdKategori` = '2'
					  INNER JOIN formula 
					    ON formuladetail.formulanumber = formula.formulanumber 
					WHERE 1 
					  AND formula.formulanumber = '".$v_formulanumber."'
					ORDER BY masterbarang.PCode ASC ;
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $inventoryname, $qty, $packagingsize) = $row;
            
            $arr_data["list_kemas"][$inventorycode] = $inventorycode;
            $arr_data["inventoryname"][$inventorycode] = $inventoryname;
            $arr_data["qty"][$inventorycode] = $qty;
        }
        
        $arr_stock = bincard_stock(sprintf("%02s", $v_mm), $v_yyyy, $arr_data["list_kemas"], $v_warehousecode, 2);
        
        ?>
        <input type="hidden" name="v_packagingsize" id="v_packagingsize" value="<?php echo $packagingsize; ?>">
        <table class="table table-bordered responsive">
	    <thead>
			<tr>
				<th><strong><center>Matreq No</center></strong></th>
				<th><strong><center>Date</center></strong></th>
				<th><strong><center>Batch Req</center></strong></th>
				<th><strong><center>Remain</center></strong></th>
				<th><strong><center>Prod Batch_</center></strong></th>
			</tr>
		</thead>
		<tbody>
            <?php
            $all_matreq = "";
            foreach($arr_data["list_data"] as $matreqnumber=>$val)
            {
                $matreqdate = $arr_data["matreqdate"][$matreqnumber];
                $quantity = $arr_data["quantity"][$matreqnumber];
                $batchremain = $arr_data["batchremain"][$matreqnumber];
                ?>
                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                    <td>
                        <?php echo $matreqnumber; ?>
                        <input type="hidden" name="v_matreqnumber[]" value="<?php echo $matreqnumber; ?>">
                    </td>
                    <td style="text-align: center;"><?php echo format_show_date($matreqdate); ?></td>
                    <td style="text-align: right;"><?php echo format_number($quantity,4); ?></td>
                    <td style="text-align: right;">
                        <?php echo format_number($batchremain,4); ?>
                        <input type="hidden" name="v_matreq_batchremain_<?php echo $matreqnumber; ?>" id="v_matreq_batchremain_<?php echo $matreqnumber; ?>" value="<?php echo $batchremain; ?>">
                    </td>
                    <td style="text-align: right;"><input type="text" class="form-control-new" name="v_prod_batch_<?php echo $matreqnumber; ?>" id="v_prod_batch_<?php echo $matreqnumber; ?>" value="" size="12" onblur="toFormat4('v_prod_batch_<?php echo $matreqnumber; ?>')" style="text-align: right;"></td>
                </tr> 
                <?php
                $all_matreq .= $matreqnumber.",";
            }
            ?>
            </tbody>
        </table>
        <input type="hidden" name="v_all_matreq" id="v_all_matreq" value="<?php echo $all_matreq; ?>">
        <?php     
        echo "||";
        ?>
        <table class="table table-bordered responsive">
	    <thead>
			<tr>
				<th><strong><center>No</center></strong></th>
				<th><strong><center>PCode</center></strong></th>
				<th><strong><center>Nama Barang</center></strong></th>
				<th><strong><center>Bincard</center></strong></th>
				<th><strong><center>Qty</center></strong></th>
			</tr>
		</thead>
		<tbody>
            <?php 
                $no = 1;
                $all_inventory = "";
                foreach($arr_data["list_kemas"] as $inventorycode=>$val)
                {
                    $inventoryname = $arr_data["inventoryname"][$inventorycode];
                    $qty = $arr_data["qty"][$inventorycode];
                    
                    ?>
                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                        <td>
                            <?php echo $no; ?>
                            <input type="hidden" name="v_qty_kg_<?php echo $inventorycode; ?>" id="v_qty_kg_<?php echo $inventorycode; ?>" value="<?php echo $qty_kg; ?>">
                            <input type="hidden" name="v_inventorycode[]" value="<?php echo $inventorycode; ?>">
                            <input type="hidden" name="v_qty_bincard_<?php echo $inventorycode; ?>" value="<?php echo $arr_stock["akhir"][$inventorycode]; ?>">
                        </td>
                        <td><?php echo $inventorycode; ?></td>
                        <td><?php echo $inventoryname; ?></td>
                        <td style="text-align: right;"><?php echo format_number($arr_stock["akhir"][$inventorycode],0); ?></td>
                        <td style="text-align: right;">
                            <input type="text" name="v_qty_kemas_<?php echo $inventorycode; ?>" id="v_qty_kemas_<?php echo $inventorycode; ?>" value="" onblur="toFormat('v_qty_kemas_<?php echo $inventorycode; ?>')" size="12" style="text-align: right;" class="form-control-new">
                        </td>
                    </tr> 
                    <?php
                    $no++;
                }         
            ?>
           </tbody>
        </table> 
        <?php
    }
    else if($ajax=="ajax_formula_transfer")
    {
        $v_formulanumber = $_GET["v_formulanumber"];
        
        $q = "
                SELECT
                    formula.inventorycode,
                    formula.quantity AS qty_kg_formula
                FROM
                    formula
                WHERE          
                    1
                    AND formula.formulanumber = '".$v_formulanumber."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($inventoryprefix, $qty_kg_formula) = $row;
        
        echo $inventoryprefix;
    }
    else if($ajax=="search_formula_mix_wip")
    {
        $v_keyword = save_char($_GET["v_keyword"]);
        
        $arr_keyword[0] = "formula.formulanumber"; 
        $arr_keyword[1] = "formula.formulaname";
        
        $where = search_keyword($v_keyword, $arr_keyword); 
        
        $q = "
                SELECT
                    formula.formulanumber,
                    formula.formulaname,
                    formula.isdefault
                FROM
                    formula
                WHERE
                    1
                    AND formula.isactive = '1'
                    ".$where."
                ORDER BY
                    formula.formulaname ASC,
                    formula.formulanumber ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($formulanumber, $formulaname, $isdefault) = $row; 
            
            $arr_data["list_formula"][$formulanumber] = $formulanumber;
            
            if($isdefault)
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname." (Def)";    
            }
            else
            {
                $arr_data["formulaname"][$formulanumber] = $formulanumber." :: ".$formulaname;
            } 
        }  
        ?>
        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_wip', this.value)">
            <option value="">-</option>
            <?php 
                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                {
                    $formulaname = $arr_data["formulaname"][$formulanumber];
                    
                    $selected = "";
                    if($data_formulanumber==$formulanumber)
                    {
                        $selected = "selected='selected'";
                    }
                    ?>
                        <option <?php echo $selected; ?> value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                    <?php
                }
            ?>
        </select> 
        <?php
        
    }
    
    else if($ajax=="transfer_wip")
    { // ===================================== transfer wip ========================================
        $v_batchnumber = save_char($_GET["v_batchnumber"]);
        
        $q = "
                SELECT
                    *
                FROM
                    production
                WHERE
                    1
                    AND production.batchnumber = '".$v_batchnumber."' 
                    AND production.mixquantity*1!='0'
                ORDER BY
                    production.productionid ASC
        ";
        $qry_old = mysql_query($q);
        $arr_old = mysql_fetch_array($qry_old);
        
        $counter = 1;
        $q = "
                SELECT
                    `sid`,
                    `batchnumber`,
                    `purpose`,
                    `transfer_date`,
                    `amount`,
                    `remarks`,
                    `productionid`,
                    `author_user`,
                    `author_date`,
                    `edited_user`,
                    `edited_date` 
                FROM
                    production_transfer
                WHERE
                    1
                    AND production_transfer.batchnumber = '".$v_batchnumber."' 
                ORDER BY
                    production_transfer.transfer_date ASC
        ";
        //echo $q;die;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list(
                $sid,
                $batchnumber,
                $purpose,
                $transfer_date,
                $amount,
                $remarks,
                $productionid,
                $author_user,
                $author_date,
                $edited_user,
                $edited_date 
            ) = $row;
            
            $arr_data["list_data_transfer"][$counter] = $counter;
            $arr_data["transfer_sid"][$counter] = $sid;
            $arr_data["transfer_batchnumber"][$counter] = $batchnumber;
            $arr_data["transfer_purpose"][$counter] = $purpose;
            $arr_data["transfer_transfer_date"][$counter] = $transfer_date;
            $arr_data["transfer_amount"][$counter] = $amount;
            $arr_data["transfer_remarks"][$counter] = $remarks;
            $arr_data["transfer_productionid"][$counter] = $productionid;
            $arr_data["transfer_author_user"][$counter] = $author_user;
            $arr_data["transfer_author_date"][$counter] = $author_date;
            $arr_data["transfer_edited_user"][$counter] = $edited_user;
            $arr_data["transfer_edited_date"][$counter] = $edited_date;
            
            $counter++;
        }
        
        
        

        ?>
            <div align="center">
                <?php
                    echo menutab($v_batchnumber, 'transfer_wip');
                ?>
            </div>
            <table class="table table-bordered responsive">
                <tr class="title_table">
                    <td colspan="100%">Transfer <?php echo $v_batchnumber; ?></td>
                </tr>
                
                <?php 
                    if($arr_old["status_batchnumber"]=="Open")
                    {
                ?>
                <tr height="25">
                    <td colspan="100%">
                        <table style="font-weight: bold; cursor: pointer;" onclick="CallAjax('add_transfer', '<?php echo $v_batchnumber; ?>')">
                            <tr>
                                <td><img src="images/add-icon.png"></td>
                                <td>Transfer Out</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php 
                    }
                ?>
                
                <tr height="25" class="title_table" style="text-align: center;">
                    <td width="30">No</td>
                    <td width="100">Transfer Date</td>
                    <td width="400">Purpose</td>
                    <td width="30">&nbsp;</td>
                    <td width="200">Amount</td>
                    <td>Remarks</td>
                    <td width="100">Action</td>
                   
                </tr>
                
                <?php 
                    $no = 1;
                    
                    foreach($arr_data["list_data_transfer"] as $counter=>$val)
                    {
                        $sid = $arr_data["transfer_sid"][$counter];
                        $batchnumber = $arr_data["transfer_batchnumber"][$counter];
                        $purpose = $arr_data["transfer_purpose"][$counter];
                        $transfer_date = $arr_data["transfer_transfer_date"][$counter];
                        $amount = $arr_data["transfer_amount"][$counter];
                        $remarks = $arr_data["transfer_remarks"][$counter];
                        $productionid = $arr_data["transfer_productionid"][$counter];
                        $author_user = $arr_data["transfer_author_user"][$counter];
                        $author_date = $arr_data["transfer_author_date"][$counter];
                        $edited_user = $arr_data["transfer_edited_user"][$counter];
                        $edited_date = $arr_data["transfer_edited_date"][$counter];
                        
                        ?>
                        <tr height="25" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                            <td><?php echo $no; ?></td>
                            <td style="text-align: center; "><?php echo format_show_date($transfer_date); ?></td>
                           
                            <td><?php echo $purpose." (".$sid.")"; ?></td>
                            <td><!--<?php echo $muttype; ?>--></td>
                            <td style="text-align: right; "><?php echo format_number($amount, 4); ?></td>
                            <td><?php echo $remarks; ?></td>
                            
                            <td style="text-align: center;">
                                <img src="images/edit.gif" style="cursor: pointer;" onclick="CallAjax('edit_transfer', '<?php echo $sid; ?>')" title="Edit Transfer <?php echo format_show_date($transfer_date); ?>">
                                
                                
                                
                                &nbsp;
                                <img src="images/print.png" style="cursor: pointer;" onclick="confirm_print_transfer('<?php echo $sid; ?>')" title="Print Transfer <?php echo format_show_date($transfer_date); ?>">
                                &nbsp;&nbsp;&nbsp;
                                <img src="images/cross.gif" style="cursor: pointer;" onclick="confirm_delete_transfer('<?php echo $sid; ?>', '<?php echo format_show_date($transfer_date); ?>', '<?php echo $batchnumber; ?>')" title="Delete Transfer <?php echo format_show_date($transfer_date); ?>">
                                
                                
                                
                            </td>
                            
                        </tr> 
                        <?php
                        $no++;
                        $arr_total["transfer_amount"] += $amount;
                    }
                ?>
                
                
                   
                <tr style="text-align: right; height: 25px; font-weight: bold;" >
                    <td colspan="4">Total&nbsp;</td>
                    <td><?php echo format_number($arr_total["transfer_amount"],4); ?></td>
                    
                   
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    
                </tr>
                
                <tr>
                    <td colspan="100%">&nbsp;</td>
                </tr>
                
                <tr>
                    <td colspan="100%" id="table_edit_transfer">&nbsp;</td>
                </tr>
            </table>
            
        <?php    
    } // ================================================= end transfer wip =================================
    
    else if($ajax=="add_transfer")
    {
        $v_batchnumber = save_char($_GET["v_batchnumber"]);   
        
        $q = "
                SELECT
                    *
                FROM
                    production
                        
                WHERE
                    1
                    AND production.batchnumber = '".$v_batchnumber."' 
                    AND production.mixquantity*1!='0'
                ORDER BY
                    production.productionid ASC
        ";
        $qry_old = mysql_query($q);
        $arr_old = mysql_fetch_array($qry_old);
        
        $arr_data["list_batchnumber"][$v_batchnumber] = $v_batchnumber;
        $arr_data["list_warehousecode"][$arr_old["warehousecode"]] = $arr_old["warehousecode"];
        
        $arr_stock = bincard_wip_stock($arr_data["list_batchnumber"],$arr_data["list_warehousecode"]);
        
        
        ?>
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="transfer">
            <input type="hidden" name="v_productionid" id="v_productionid" value="<?php echo $arr_old["productionid"]; ?>">
            <input type="hidden" name="v_warehousecode" id="v_warehousecode" value="<?php echo $arr_old["warehousecode"]; ?>">
            <input type="hidden" name="v_batchnumber" id="v_batchnumber" value="<?php echo $v_batchnumber; ?>">
            <input type="hidden" name="v_sisa_stock" id="v_sisa_stock" value="<?php echo $arr_stock["sum_akhir"][$v_batchnumber]; ?>">
            <input type="hidden" name="v_del" id="v_del" value="">
            <table class="table table-bordered responsive" width="100%">
                <tr class="title_table">
                    <td colspan="100%">Add Transfer</td>
                </tr>
                
                
                
                <tr height="25">
                    <td class="title_table" width="150">Transfer Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_transfer_date" id="v_transfer_date" value="<?php echo date("d-m-Y"); ?>" class="form-control-new">
                        <img src="images/calendar.gif" onClick="popUpCalendar(this, theform.v_transfer_date, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                
                <tr height="25">
                    <td class="title_table">Purpose</td>
                    <td>
                        <select name="v_purpose" id="v_purpose" class="form-control-new" style="width: 300px;">
                            <option value="NATURA">NATURA</option>
                        </select>
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Amount (KG)</td>
                    <td><input type="text" class="form-control-new" name="v_amount" id="v_amount" style="text-align: right; width: 300px;" onblur="toFormat4('v_amount')"></td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Remarks</td>
                    <td><input type="text" class="form-control-new" name="v_remarks" id="v_remarks" style="width: 300px;"></td>
                </tr>
                
                <tr height="25">
                    <td>&nbsp;</td>
                    <td>
                        <button type="submit" class="btn btn-info btn-sm" name="btn_save" id="btn_save" value="Save">Save Transfer WIP<i class="entypo-check"></i></button>
                    </td>
                </tr>
           </table> 
        </form>
        <?php
    }
    
    else if($ajax=="cekMr")
    {
        $v_mr = $_GET["v_mr"];
        $v_pcode = $_GET["v_pcode"];
        $_qty = $_GET["v_qty_asal"];
        
        $q = "
                SELECT 
				  a.`quantity` 
				FROM
				  `matreqdetail` a 
				WHERE a.`matreqnumber` = '".$v_mr."' 
				  AND a.`inventorycode` = '".$v_pcode."' ;
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
        	$qty_matreq = $row['quantity'];
        }
        
        if($_qty > $qty_matreq){
		$msg = "1";
		}
        echo $msg;
        
    }
    
    else if($ajax=="transfer_in")
    {
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('15','05')
                ORDER BY
                    gudang.KdGudang ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $Keterangan) = $row;
            
            $arr_data["list_warehouse"][$KdGudang] = $KdGudang;
            
            $arr_data["warehouse_warehousename"][$KdGudang] = $KdGudang." :: ".$Keterangan;
        } 
        ?>
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="transfer_in">
            <input type="hidden" name="v_del" id="v_del" value="">
            <table class="table table-bordered responsive" width="100%">
                <tr class="title_table">
                    <td colspan="100%">Add Transfer IN</td>
                </tr>
                
                <tr height="25">
                    <td class="title_table" width="150">Transfer Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_transfer_date" id="v_transfer_date" value="<?php echo date("d-m-Y"); ?>" class="form-control-new">
                        <img src="images/calendar.gif" onClick="popUpCalendar(this, theform.v_transfer_date, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Warehouse</td>
                    <td>
                         <select name="v_warehousecode" id="v_warehousecode" class="form-control-new" style="width: 400px;">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_warehouse"] as $KdGudang=>$val)                 
                                {
                                    $Keterangan = $arr_data["warehouse_warehousename"][$KdGudang];
                                    ?>
                                    <option value="<?php echo $KdGudang; ?>"><?php echo $Keterangan; ?></option>
                                    <?php        
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Formula</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_keyword_formula" id="v_keyword_formula" size="10" placeholder="Keyword" onkeyup="CallAjax('search_keyword_formula_transfer', this.value)">
                        &nbsp;
                        <span id="form_formula">
                        <select class="form-control-new" name="v_formulanumber" id="v_formulanumber" style="width: 300px;" onchange="CallAjax('ajax_formula_transfer', this.value)">
                            <option value="">-</option>
                            <?php 
                                foreach($arr_data["list_formula"] as $formulanumber=>$val)
                                {
                                    $formulaname = $arr_data["formulaname"][$formulanumber];
                                    ?>
                                        <option value="<?php echo $formulanumber; ?>"><?php echo $formulaname; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                        </span>
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Batchnumber</td>
                    <td>
                        <input type="text" class="form-control-new" name="v_batch_1" id="v_batch_1" size="10" maxlength="6" readonly="readonly" style="text-align: right;">
                        &nbsp;
                        <input type="text" class="form-control-new" name="v_batch_2" id="v_batch_2" maxlength="30" style="width: 300px;">
                    </td>
                </tr>
                
                
                <tr height="25">
                    <td class="title_table">Purpose</td>
                    <td>
                        <select name="v_purpose" id="v_purpose" class="form-control-new" style="width: 300px;">
                            <option value="NATURA">NATURA</option>
                        </select>
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Amount (KG)</td>
                    <td><input type="text" class="form-control-new" name="v_amount" id="v_amount" style="text-align: right; width: 300px;" onblur="toFormat4('v_amount')"></td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Remarks</td>
                    <td><input type="text" class="form-control-new" name="v_remarks" id="v_remarks" style="width: 300px;"></td>
                </tr>
                
                <tr height="25">
                    <td>&nbsp;</td>
                    <td>
                        <button type="submit" class="btn btn-info btn-sm" name="btn_save" id="btn_save" value="Save">Save Transfer IN<i class="entypo-check"></i></button>
                    </td>
                </tr>
           </table> 
        </form>
        <?php
    }
    
    else if($ajax=="edit_transfer")
    {
        $v_sid = $_GET["v_sid"];   
        
        $q = "
                SELECT
                    production_transfer.sid,
                    production_transfer.warehousecode,
                    production_transfer.batchnumber,
                    production_transfer.purpose,
                    production_transfer.transfer_date,
                    production_transfer.amount,
                    production_transfer.remarks,
                    production_transfer.productionid,
                    production_transfer.author_user,
                    production_transfer.author_date,
                    production_transfer.edited_user,
                    production_transfer.edited_date
                FROM
                    production_transfer
                WHERE
                    1
                    AND production_transfer.sid = '".$v_sid."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list(
            $sid,
            $warehousecode,
            $batchnumber,
            $purpose,
            $transfer_date,
            $amount,
            $remarks,
            $productionid,
            $author_user,
            $author_date,
            $edited_user,
            $edited_date 
        ) = $row;
        
        $q = "
                SELECT
                    *
                FROM
                    production
                WHERE
                    1
                    AND production.batchnumber = '".$batchnumber."' 
                    AND production.mixquantity*1!='0'
                ORDER BY
                    production.productionid ASC
        ";
        $qry_old = mysql_query($q);
        $arr_old = mysql_fetch_array($qry_old);
        
        $arr_data["list_batchnumber"][$batchnumber] = $batchnumber;
        $arr_data["list_warehousecode"][$arr_old["warehousecode"]] = $arr_old["warehousecode"];
        
        $arr_stock = bincard_wip_stock($arr_data["list_batchnumber"],$arr_data["list_warehousecode"]);
        
        
        ?>
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="edit_transfer">
            
            <input type="hidden" name="v_sid" value="<?php echo $v_sid; ?>">
            <input type="hidden" name="v_productionid" value="<?php echo $v_productionid; ?>">
            <input type="hidden" name="v_batchnumber" value="<?php echo $batchnumber; ?>">
            <input type="hidden" name="v_warehousecode" value="<?php echo $warehousecode; ?>">
            <input type="hidden" name="v_sisa_stock" id="v_sisa_stock" value="<?php echo ($arr_stock["sum_akhir"][$v_batchnumber] + $amount); ?>">
            <!--<input type="hidden" name="v_muttype" value="<?php echo $muttype; ?>">-->
            
            
            <input type="hidden" name="v_del" id="v_del" value="">
            <table class="table table-bordered responsive" width="100%">
                <tr class="title_table">
                    <td colspan="100%">Edit Transfer <?php echo $batchnumber; ?></td>
                </tr>
                
               
               <tr height="25">
                    <td class="title_table" width="150">Transfer Date</td>
                    <td>
                        <input type="text" size="10"  maxlength = "10" name="v_transfer_date" id="v_transfer_date" value="<?php echo format_show_date($transfer_date); ?>" class="form-control-new">
                        <img src="images/calendar.gif" onClick="popUpCalendar(this, theform.v_transfer_date, 'dd-mm-yyyy');">
                    </td>
                </tr>
                
                
                
                
                <tr height="25">
                    <td class="title_table">Purpose</td>
                    <td>
                        <select name="v_purpose" id="v_purpose" class="form-control-new" style="width: 300px;">
                            <option value="NATURA" <?php if($purpose=="NATURA") echo "selected='selected'"; ?>>NATURA</option>
                        </select>
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Amount (KG)</td>
                    <td>
                        <?php echo "<b>".$muttype."</b>"; ?>
                        <input type="text" class="form-control-new" name="v_amount" id="v_amount" style="text-align: right; width: 275px;" onblur="toFormat4('v_amount')" value="<?php echo format_number($amount, 4); ?>">
                    </td>
                </tr>
                
                <tr height="25">
                    <td class="title_table">Remarks</td>
                    <td><input type="text" class="form-control-new" name="v_remarks" id="v_remarks" style="width: 300px;" value="<?php echo $remarks; ?>"></td>
                </tr>
                
                
                
                <tr height="25">
                    <td>&nbsp;</td>
                    <td>
                        <?php
                            if($arr_old["status_batchnumber"]=="Open")
                            {
                        ?>
                        
                        <button type="submit" class="btn btn-info btn-sm" name="btn_save" id="btn_save" value="Save">Save Transafer WIP edit<i class="entypo-check"></i></button>
                        <?php 
                            }
                            else
                            {
                                echo "<font color='red'><b>Button Save tidak muncul, karena Status Batchnumber sudah Close</b></font>";
                            }
                        ?>
                    </td>
                </tr>
           </table> 
        </form>
        <?php
    }
    else if($ajax=="delete_transfer")
    {
        $v_sid = $_GET["v_sid"];
        echo "Mampir kesini ya ".$v_sid;die;
        $q = "
                SELECT
                    production_transfer.batchnumber
                FROM
                    production_transfer
                WHERE
                    1
                    AND sid = '".$v_sid."'    
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($batchnumber) = $row;

        $q = "DELETE FROM bincard_wip WHERE 1 AND productionid = '".$v_sid."' AND module = 'TF' ";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete bincard_wip";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        $q = "DELETE FROM production_transfer WHERE 1 AND production_transfer.sid = '".$v_sid."'";
        if(!mysql_query($q))      
        {
            $msg = "Gagal Delete production_transfer";
            echo "<script>alert('".$msg."');</script>";     
            die();
        }
        
        echo $batchnumber;
    }
    
    exit();
}
 

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data":
            {
            	//echo "<pre>";print_r($_POST);echo "</pre>";die;
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_matreqnumber"])){ $v_matreqnumber = isset($_POST["v_matreqnumber"]); } else { $v_matreqnumber = $_POST["v_matreqnumber"]; }
                if(!isset($_POST["v_productiondate"])){ $v_productiondate = isset($_POST["v_productiondate"]); } else { $v_productiondate = $_POST["v_productiondate"]; }
                if(!isset($_POST["v_batch_1"])){ $v_batch_1 = isset($_POST["v_batch_1"]); } else { $v_batch_1 = $_POST["v_batch_1"]; }
                if(!isset($_POST["v_batch_2"])){ $v_batch_2 = isset($_POST["v_batch_2"]); } else { $v_batch_2 = $_POST["v_batch_2"]; }
                if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }
                
                if(!isset($_POST["prod_batch"])){ $prod_batch = isset($_POST["prod_batch"]); } else { $prod_batch = $_POST["prod_batch"]; }
                if(!isset($_POST["total_wip_kg"])){ $total_wip_kg = isset($_POST["total_wip_kg"]); } else { $total_wip_kg = $_POST["total_wip_kg"]; }
                
                
                $v_warehousecode = save_char($v_warehousecode);
                $v_formulanumber = save_char($v_formulanumber);
                $v_batch_1 = save_char($v_batch_1);
                $v_batch_2 = save_char($v_batch_2);
                $full_batchnumber = $v_batch_1.$v_batch_2;
                
                $prod_batch = save_int($prod_batch);
                $total_wip_kg = save_int($total_wip_kg);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    // validasi
                    {
                        if($v_warehousecode=="")
                        {
                            $msg = "Warehouse harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                                        
                        if($v_formulanumber=="")
                        {
                            $msg = "Formula harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                        $all_prod_batch = 0;
                        foreach($v_matreqnumber as $key=>$val)
                        {
                            if(!isset($_POST["v_matreq_batchremain_".$val])){ $v_matreq_batchremain = isset($_POST["v_matreq_batchremain_".$val]); } else { $v_matreq_batchremain = $_POST["v_matreq_batchremain_".$val]; }
                            if(!isset($_POST["v_prod_batch_".$val])){ $v_prod_batch = isset($_POST["v_prod_batch_".$val]); } else { $v_prod_batch = $_POST["v_prod_batch_".$val]; }
                            
                            $v_matreq_batchremain = save_int($v_matreq_batchremain);
                            $v_prod_batch = save_int($v_prod_batch);
                            
                            if($v_prod_batch > $v_matreq_batchremain)
                            {
                                $msg = "Prod Batch ".$val." melebihi Batch Remain";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            
                            $all_prod_batch += $v_prod_batch;
                        }
                        
                        if($all_prod_batch*1==0)
                        {
                            $msg = "Prod Batch harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        if($v_batch_1=="")
                        {
                            $msg = "BatchNumber harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        if($v_batch_2=="")
                        {
                            $msg = "BatchNumber harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        if($full_batchnumber=="")
                        {
                            $msg = "BatchNumber harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        $q = "
                                SELECT
                                    production.batchnumber
                                FROM
                                    production
                                WHERE
                                    1
                                    AND production.batchnumber = '".$full_batchnumber."'
                                    AND production.mixquantity*1 != '0'
                                LIMIT
                                    0,1
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($batchnumber_exist) = $row;
                        
                        if($batchnumber_exist)
                        {
                            $msg = "BatchNumber ".$batchnumber_exist." sudah ada";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        foreach($v_inventorycode as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_bincard_".$val])){ $v_qty_bincard = isset($_POST["v_qty_bincard_".$val]); } else { $v_qty_bincard = $_POST["v_qty_bincard_".$val]; }
                            if(!isset($_POST["v_qty_baku_".$val])){ $v_qty_baku = isset($_POST["v_qty_baku_".$val]); } else { $v_qty_baku = $_POST["v_qty_baku_".$val]; }
                            
                            $v_qty_bincard = save_int($v_qty_bincard)*1;
                            $v_qty_baku = save_int($v_qty_baku)*1;
                            
                            if( number_format($v_qty_baku,6,".","")*1 > number_format($v_qty_bincard,6,".","")*1 && false)
                            {
                                if(
                                    $val!="01-00068" && 
                                    $val!="01-00151" && 
                                    $val!="01-00185" && 
                                    $val!="01-000531" && 
                                    $val!="01-00150" && 
                                    $val!="01-00184" && 
                                    $val!="01-00265" &&
                                    $val!="01-00097" &&
                                    $val!="01-000971"
                                )
                                {
                                    $msg = "Stock Bincard ".$val." tidak mencukupi, mohon di cek kembali ".(number_format($v_qty_baku,6,".","")*1)." vs ".(number_format($v_qty_bincard,6,".","")*1);
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();    
                                }
                            }
                            
                            $all_prod_batch += $v_prod_batch;
                        }
                        
                    }
                    
                    $counter_productionid = get_counter_int("", "production", "productionid", 100);
                    
                    $q = "
                            INSERT `production`
                            SET `productionid` = '".$counter_productionid."',
                              `batchnumber` = '".$full_batchnumber."',
                              `productiondate` = '".format_save_date($v_productiondate)."',
                              `formulanumber` = '".$v_formulanumber."',
                              `mixquantity` = '".$total_wip_kg."',
                              `mixvalue` = '0.0000',
                              `packvalue` = '0.0000',
                              `mixdate` = '".format_save_date($v_productiondate)."',
                              `packdate` = '".format_save_date($v_productiondate)."',
                              `status` = '1',
                              `adduser` = '".$ses_login."',
                              `adddate` = NOW(),
                              `edituser` = '".$ses_login."',
                              `editdate` = NOW(),
                              `inventoryprefix` = '".$v_batch_1."',
                              `batchquantity` = '".$prod_batch."',
                              `prodstat` = '1',
                              `packagingstatus` = '0',
                              `isposted` = '0',
                              `mixingfoh` = '0.00',
                              `packagingfoh` = '0.00',
                              `hasfinishgood` = '0',
                              `warehousecode` = '".$v_warehousecode."',
                              `reworkquantity` = '0',
                              `usereworkmaterial` = '0',
                              `status_batchnumber` = 'Open'
                    ";
                    
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan production";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    
                    
                    else
                    {
                    	
                        foreach($v_inventorycode as $key=>$val)
                        {
                        	
                            if(!isset($_POST["v_qty_baku_".$val])){ $v_qty_baku = isset($_POST["v_qty_baku_".$val]); } else { $v_qty_baku = $_POST["v_qty_baku_".$val]; }
                            
                            $v_qty_baku = save_int($v_qty_baku);
                             
                            $q = "
                                    INSERT INTO `productionmixing`
                                    SET 
                                      `productionid` = '".$counter_productionid."',
                                      `batchnumber` = '".$full_batchnumber."',
                                      `inventorycode` = '".$val."',
                                      `quantity` = '".$v_qty_baku."',
                                      `value` = '0.0000',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW()
                            "; 
                            
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan productionmixing";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }    
                            
                            $q = "
                                    INSERT INTO `bincard`
                                    SET 
                                      `inventorycode` = '".$val."',
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `referenceno` = '".$counter_productionid."',
                                      `module` = 'MX',
                                      `qin` = '0.000000',
                                      `qout` = '".$v_qty_baku."',
                                      `value` = '0.0000',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW()
                            "; 
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }  
                            
                            $q = "SELECT productionid, formula.inventorycode FROM production INNER JOIN formula ON production.formulanumber = formula.formulanumber WHERE batchnumber = '".$full_batchnumber."' AND mixquantity*1!='0' LIMIT 0,1";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($productionid2, $inventorycode) = $row;
                            
                            $q = "
                                        INSERT INTO `productionmixingwip`
                                        SET 
                                          `productionid` = '".$counter_productionid."',
                                          `productionid2` = '".$productionid2."',
                                          `batchnumber` = '".$full_batchnumber."',
                                          `inventorycode` = '".$inventorycode."',
                                          `dataquantity` = '".$v_qty_baku."',
                                          `quantity` = '".$v_qty_baku."',
                                          `adduser` = '".$ses_login."',
                                          `adddate` = NOW(),
                                          `edituser` = '".$ses_login."',                   
                                          `editdate` = NOW()
                                "; 
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal menyimpan productionmixingwip";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();
                                }
                             
                        }
                        
                       
                        
                        foreach($v_matreqnumber as $key=>$val)
                        {
                            if(!isset($_POST["v_prod_batch_".$val])){ $v_prod_batch = isset($_POST["v_prod_batch_".$val]); } else { $v_prod_batch = $_POST["v_prod_batch_".$val]; }
                            
                            $v_prod_batch = save_int($v_prod_batch);
                            
                            if($v_prod_batch)
                            {
                                $q = "
                                        INSERT INTO `productionbatchrequest`
                                        SET 
                                          `productionid` = '".$counter_productionid."',
                                          `batchnumber` = '".$full_batchnumber."',
                                          `matreqnumber` = '".$val."',
                                          `production` = '1',
                                          `prod_batch` = '".$v_prod_batch."'
                                ";  
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal menyimpan productionbatchrequest";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();
                                } 
                                else
                                {
                                    $q = "
                                            SELECT
                                                SUM(`productionbatchrequest`.prod_batch) AS jml_prod_batch
                                            FROM
                                                `productionbatchrequest`
                                            WHERE
                                                1
                                                AND `productionbatchrequest`.matreqnumber = '".$val."'
                                    ";
                                    $qry = mysql_query($q);
                                    $row = mysql_fetch_array($qry);
                                    list($jml_prod_batch) = $row;
                                    
                                    $q = "
                                            UPDATE
                                                `matreq`
                                            SET
                                                 batchremain = (quantity - ".$jml_prod_batch.")
                                            WHERE
                                                1
                                                AND matreqnumber = '".$val."'
                                    ";
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal Update Matreq";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();     
                                    }
                                }     
                            }
                        }
                        
                        // insert bincard wip
                        {
                            $q = "
                                    INSERT INTO `bincard_wip`
                                    SET 
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$full_batchnumber."',
                                      `module` = 'MX',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `qin` = '".$total_wip_kg."',
                                      `qout` = '0.0000',
                                      productionid = '".$counter_productionid."'
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard_wip";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            } 
                        }
                        
                        $msg = "Berhasil menyimpan 2".$full_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$full_batchnumber."');</script>";         
                        die();    
                    }
                }
                               
            }
            break;
        case "transfer_in" :
            {
            	
            	
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_transfer_date"])){ $v_transfer_date = isset($_POST["v_transfer_date"]); } else { $v_transfer_date = $_POST["v_transfer_date"]; }
                if(!isset($_POST["v_batch_1"])){ $v_batch_1 = isset($_POST["v_batch_1"]); } else { $v_batch_1 = $_POST["v_batch_1"]; }
                if(!isset($_POST["v_batch_2"])){ $v_batch_2 = isset($_POST["v_batch_2"]); } else { $v_batch_2 = $_POST["v_batch_2"]; }
                if(!isset($_POST["v_purpose"])){ $v_purpose = isset($_POST["v_purpose"]); } else { $v_purpose = $_POST["v_purpose"]; }
                
                if(!isset($_POST["v_amount"])){ $v_amount = isset($_POST["v_amount"]); } else { $v_amount = $_POST["v_amount"]; }
                if(!isset($_POST["v_remarks"])){ $v_remarks = isset($_POST["v_remarks"]); } else { $v_remarks = $_POST["v_remarks"]; }
                
                $v_warehousecode = save_char($v_warehousecode);
                $v_formulanumber = save_char($v_formulanumber);
                $v_batch_1 = save_char($v_batch_1);
                $v_batch_2 = save_char($v_batch_2);
                $full_batchnumber = strtoupper($v_batch_1.$v_batch_2);
                
                $v_amount = save_int($v_amount);
                $v_remarks = save_char($v_remarks);
                
                    // validasi
                {
                    if($v_warehousecode=="")
                    {
                        $msg = "Warehouse harus dipilih";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();    
                    }
                                    
                    if($v_formulanumber=="")
                    {
                        $msg = "Formula harus dipilih";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();    
                    }
                    
                    if($v_batch_1=="")
                    {
                        $msg = "BatchNumber harus diisi";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();     
                    }
                    
                    if($v_batch_2=="")
                    {
                        $msg = "BatchNumber harus diisi";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();     
                    }
                    
                    if($v_amount*1==0)
                    {
                        $msg = "Amount (KG) harus diisi";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();     
                    }
                    
                    $q = "
                            SELECT
                                production.batchnumber
                            FROM
                                production
                            WHERE
                                1
                                AND production.batchnumber = '".$full_batchnumber."'
                                AND production.mixquantity*1 != '0'
                            LIMIT
                                0,1
                    ";
                    
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry);
                    list($batchnumber_exist) = $row;
                    
                    if($batchnumber_exist)
                    {
                    	
                        $msg = "BatchNumber ".$batchnumber_exist." sudah ada";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();     
                    }
                    
                }
                    
                    $counter_productionid = get_counter_int("", "production", "productionid", 100);
                    
                    $q = "
                            INSERT `production`
                            SET `productionid` = '".$counter_productionid."',
                              `batchnumber` = '".$full_batchnumber."',
                              `productiondate` = '".format_save_date($v_transfer_date)."',
                              `formulanumber` = '".$v_formulanumber."',
                              `mixquantity` = '".$v_amount."',
                              `mixvalue` = '0.0000',
                              `packvalue` = '0.0000',
                              `mixdate` = '".format_save_date($v_transfer_date)."',
                              `packdate` = '".format_save_date($v_transfer_date)."',
                              `status` = '1',
                              `adduser` = '".$ses_login."',
                              `adddate` = NOW(),
                              `edituser` = '".$ses_login."',
                              `editdate` = NOW(),
                              `inventoryprefix` = '".$v_batch_1."',
                              `batchquantity` = '0',
                              `prodstat` = '1',
                              `packagingstatus` = '0',
                              `isposted` = '0',
                              `mixingfoh` = '0.00',
                              `packagingfoh` = '0.00',
                              `hasfinishgood` = '0',
                              `warehousecode` = '".$v_warehousecode."',
                              `reworkquantity` = '0',
                              `usereworkmaterial` = '0',
                              `status_batchnumber` = 'Open'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan production";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    else
                    {
                        $counter_sid = get_counter_int("", "production_transfer", "sid", 100);
                        
                        $q = "
                            INSERT `production_transfer`
                                SET `sid` = '".$counter_sid."',
                                  `warehousecode` = '".$v_warehousecode."',
                                  `batchnumber` = '".$full_batchnumber."',
                                  `purpose` = '".$v_purpose."',
                                  `transfer_date` = '".format_save_date($v_transfer_date)."',
                                  `amount` = '".$v_amount."',
                                  `remarks` = '".$v_remarks."',
                                  `productionid` = '".$counter_productionid."',
                                  `author_user` = '".$ses_login."',
                                  `author_date` = NOW(),
                                  `edited_user` = '".$ses_login."',
                                  `edited_date` = NOW()
                        ";
                       
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan transfer";
                            echo "<script>alert('".$msg."');</script>";
                            die();
                        } 
                            
                        
                        // insert bincard wip
                        {
                            $q = "
                                    INSERT INTO `bincard_wip`
                                    SET 
                                      `carddate` = '".format_save_date($v_transfer_date)."',
                                      `batchnumber` = '".$full_batchnumber."',
                                      `module` = 'TF',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `qin` = '".$v_amount."',
                                      `qout` = '0.0000',
                                      productionid = '".$counter_productionid."'
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard_wip";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            } 
                        }
                        
                        
                        
                        $msg = "Berhasil menyimpan ".$full_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$full_batchnumber."', 'transfer_wip');</script>";         
                        die();    
                    }
             
            }
            break;    
      
        case "add_data_wip" :
            {            	
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_productiondate"])){ $v_productiondate = isset($_POST["v_productiondate"]); } else { $v_productiondate = $_POST["v_productiondate"]; }
                if(!isset($_POST["v_batch_1"])){ $v_batch_1 = isset($_POST["v_batch_1"]); } else { $v_batch_1 = $_POST["v_batch_1"]; }
                if(!isset($_POST["v_batch_2"])){ $v_batch_2 = isset($_POST["v_batch_2"]); } else { $v_batch_2 = $_POST["v_batch_2"]; }
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                if(!isset($_POST["v_wip_qty"])){ $v_wip_qty = isset($_POST["v_wip_qty"]); } else { $v_wip_qty = $_POST["v_wip_qty"]; }
                
                $v_warehousecode = save_char($v_warehousecode);
                $v_formulanumber = save_char($v_formulanumber);
                $v_batch_1 = save_char($v_batch_1);
                $v_batch_2 = save_char($v_batch_2);
                $full_batchnumber = $v_batch_1.$v_batch_2;
                
                $v_wip_qty = save_int($v_wip_qty);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    // validasi
                    {
                        if($v_warehousecode=="")
                        {
                            $msg = "Warehouse harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                                        
                        if($v_formulanumber=="")
                        {
                            $msg = "Formula harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                        if($v_batch_1=="")
                        {
                            $msg = "BatchNumber harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        if($v_batch_2=="")
                        {
                            $msg = "BatchNumber harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        if($full_batchnumber=="")
                        {
                            $msg = "BatchNumber harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        $q = "
                                SELECT
                                    production.batchnumber
                                FROM
                                    production
                                WHERE
                                    1
                                    AND production.batchnumber = '".$full_batchnumber."'
                                    AND production.mixquantity*1 != '0'
                                LIMIT
                                    0,1
                        ";
                        
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($batchnumber_exist) = $row;
                        
                        if($batchnumber_exist)
                        {
                            $msg = "BatchNumber ".$batchnumber_exist." sudah ada";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        $all_qty_mixing = 0;
                        foreach($v_batchnumber as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_bincard_".$val])){ $v_qty_bincard = isset($_POST["v_qty_bincard_".$val]); } else { $v_qty_bincard = $_POST["v_qty_bincard_".$val]; }
                            if(!isset($_POST["v_qty_mixing_".$val])){ $v_qty_mixing = isset($_POST["v_qty_mixing_".$val]); } else { $v_qty_mixing = $_POST["v_qty_mixing_".$val]; }
                            
                            $v_qty_bincard = save_int($v_qty_bincard);
                            $v_qty_mixing = save_int($v_qty_mixing);
                            
                            if($v_qty_mixing > $v_qty_bincard)
                            {
                                //$msg = "Stock WIP ".$val." tidak mencukupi, mohon di cek kembali (".$v_qty_mixing." VS ".$v_qty_bincard.")";
                                //echo "<script>alert('".$msg."');</script>"; 
                                //die();       
                            }
                            
                            $all_qty_mixing += $v_qty_mixing;
                        }
                        
                        if($all_qty_mixing*1==0)
                        {
                            $msg = "Mixing Qty harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                        if( number_format($all_qty_mixing*1,4) != number_format($v_wip_qty*1,4) )
                        {
                            $msg = "WIP Qty & Mixing Qty harus sama";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                    }
                    
                    $counter_productionid = get_counter_int("", "production", "productionid", 100);
                    
                    $q = "
                            INSERT `production`
                            SET `productionid` = '".$counter_productionid."',
                              `batchnumber` = '".$full_batchnumber."',
                              `productiondate` = '".format_save_date($v_productiondate)."',
                              `formulanumber` = '".$v_formulanumber."',
                              `mixquantity` = '".$v_wip_qty."',
                              `mixvalue` = '0.0000',
                              `packvalue` = '0.0000',
                              `mixdate` = '".format_save_date($v_productiondate)."',
                              `packdate` = '".format_save_date($v_productiondate)."',
                              `status` = '1',
                              `adduser` = '".$ses_login."',
                              `adddate` = NOW(),
                              `edituser` = '".$ses_login."',
                              `editdate` = NOW(),
                              `inventoryprefix` = '".$v_batch_1."',
                              `batchquantity` = '0',
                              `prodstat` = '1',
                              `packagingstatus` = '0',
                              `isposted` = '0',
                              `mixingfoh` = '0.00',
                              `packagingfoh` = '0.00',
                              `hasfinishgood` = '0',
                              `warehousecode` = '".$v_warehousecode."',
                              `reworkquantity` = '0',
                              `usereworkmaterial` = '0',
                              `status_batchnumber` = 'Open'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan production";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    else
                    {
                        foreach($v_batchnumber as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_mixing_".$val])){ $v_qty_mixing = isset($_POST["v_qty_mixing_".$val]); } else { $v_qty_mixing = $_POST["v_qty_mixing_".$val]; }
                            if(!isset($_POST["v_qty_bincard_".$val])){ $v_qty_bincard = isset($_POST["v_qty_bincard_".$val]); } else { $v_qty_bincard = $_POST["v_qty_bincard_".$val]; }
                            
                            $v_qty_mixing = save_int($v_qty_mixing);
                            $v_qty_bincard = save_int($v_qty_bincard);
                            
                            $q = "SELECT productionid, formula.inventorycode FROM production INNER JOIN formula ON production.formulanumber = formula.formulanumber WHERE batchnumber = '".$val."' AND mixquantity*1!='0' LIMIT 0,1";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($productionid2, $inventorycode) = $row;
                            
                            if($v_qty_mixing)
                            { 
                                $q = "
                                        INSERT INTO `productionmixingwip`
                                        SET 
                                          `productionid` = '".$counter_productionid."',
                                          `productionid2` = '".$productionid2."',
                                          `batchnumber` = '".$val."',
                                          `inventorycode` = '".$inventorycode."',
                                          `dataquantity` = '".$v_qty_bincard."',
                                          `quantity` = '".$v_qty_mixing."',
                                          `adduser` = '".$ses_login."',
                                          `adddate` = NOW(),
                                          `edituser` = '".$ses_login."',                   
                                          `editdate` = NOW()
                                "; 
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal menyimpan productionmixingwip";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();
                                }
                                
                                // insert bincard wip
                                {
                                    $q = "
                                            INSERT INTO `bincard_wip`
                                            SET 
                                              `carddate` = '".format_save_date($v_productiondate)."',
                                              `batchnumber` = '".$v_batchnumber."',
                                              `module` = 'MW',
                                              `warehousecode` = '".$v_warehousecode."',
                                              `qin` = '0.0000',
                                              `qout` = '".$v_qty_mixing."',
                                              productionid = '".$productionid2."'
                                    ";
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal menyimpan bincard_wip qout";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();
                                    } 
                                } 
                            }    
                            
                            
                        }
                      
                        // insert bincard wip
                        {
                            $q = "
                                    INSERT INTO `bincard_wip`
                                    SET 
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$full_batchnumber."',
                                      `module` = 'MW',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `qin` = '".$v_wip_qty."',
                                      `qout` = '0.0000',
                                      productionid = '".$counter_productionid."'
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard_wip";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            } 
                        }
                        
                        $msg = "Berhasil menyimpan ".$full_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$full_batchnumber."');</script>";         
                        die();    
                    }
                }
            }
            break;
        case "mixing" :
            {
            	
                if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_productiondate"])){ $v_productiondate = isset($_POST["v_productiondate"]); } else { $v_productiondate = $_POST["v_productiondate"]; }   
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }
                
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }
                
                $v_batchnumber = save_char($v_batchnumber);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    if($v_del==1)
                    {   
                        // update matreq batchremain
                        // production, productionmixing, productionbatchrequest, bincard, bincardwip
                        
                        $q = "
                                SELECT
                                    productionbatchrequest.matreqnumber
                                FROM
                                    productionbatchrequest
                                WHERE
                                    1
                                    AND productionbatchrequest.productionid = '".$v_productionid."'
                                ORDER BY
                                    productionbatchrequest.matreqnumber ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($matreqnumber) = $row;
                            
                            $arr_data["list_matreq_update"][$matreqnumber] = $matreqnumber;
                        }
                        
                        $q = "DELETE FROM productionbatchrequest WHERE 1 AND productionbatchrequest.productionid = '".$v_productionid."' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus productionbatchrequest";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        foreach($arr_data["list_matreq_update"] as $key=>$val)
                        {
                            $q = "
                                    SELECT
                                        SUM(`productionbatchrequest`.prod_batch) AS jml_prod_batch
                                    FROM
                                        `productionbatchrequest`
                                    WHERE
                                        1
                                        AND `productionbatchrequest`.matreqnumber = '".$val."'
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($jml_prod_batch) = $row;
                            
                            $q = "
                                    UPDATE
                                        `matreq`
                                    SET
                                         batchremain = (quantity - ".$jml_prod_batch.")
                                    WHERE
                                        1
                                        AND matreqnumber = '".$val."'
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Update Matreq";
                                echo "<script>alert('".$msg."');</script>";
                                die();     
                            } 
                        }
                        
                        $q = "DELETE FROM productionmixing WHERE 1 AND productionmixing.productionid = '".$v_productionid."' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus productionmixing";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        $q = "DELETE FROM bincard WHERE 1 AND bincard.referenceno = '".$v_productionid."' AND module = 'MX' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus bincard";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        $q = "DELETE FROM bincard_wip WHERE 1 AND bincard_wip.batchnumber = '".$v_batchnumber."' AND module = 'MX' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus bincard_wip";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        $q = "DELETE FROM production WHERE 1 AND production.productionid = '".$v_productionid."'";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus production";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        
                        $msg = "Berhasil Hapus ".$v_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_batchnumber."');</script>";         
                        die();     
                    }
                    else
                    {                        
                        // validasi
                        $q = "
                                UPDATE
                                    production
                                SET
                                    productiondate = '".format_save_date($v_productiondate)."',
                                    batchnumber = '".$v_batchnumber."',
                                    editdate = NOW(),
                                    edituser = '".$ses_login."'
                                WHERE
                                    productionid = '".$v_productionid."'
                        ";
                        if(!mysql_query($q))      
                        {                                  
                            echo $q;        
                            $msg = "Gagal Update production";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                        else
                        {
                            // bincard, bincar_wip, productionmixing, productionbatchrequest
                            $q = "
                                    UPDATE
                                        bincard
                                    SET
                                        carddate = '".format_save_date($v_productiondate)."',
                                        editdate = NOW(),
                                        edituser = '".$ses_login."'
                                    WHERE
                                        referenceno = '".$v_productionid."'
                                        AND module = 'MX'
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Update production";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            }     
                            
                            $q = "
                                    UPDATE
                                        productionmixing
                                    SET
                                        batchnumber = '".$v_batchnumber."',
                                        editdate = NOW(),
                                        edituser = '".$ses_login."'
                                    WHERE
                                        productionid = '".$v_productionid."'
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Update productionmixing";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            }    
                            
                            $q = "
                                    UPDATE
                                        productionbatchrequest
                                    SET
                                        batchnumber = '".$v_batchnumber."'
                                    WHERE
                                        productionid = '".$v_productionid."'
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Update productionbatchrequest";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            } 
                            
                            $q = "
                                    UPDATE `bincard_wip`
                                    SET 
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$v_batchnumber."'
                                    WHERE `productionid` = '".$v_productionid."'; 
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Update bincard_wip";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            } 
                            
                            $msg = " # Berhasil menyimpan ".$v_batchnumber;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_batchnumber."', 'mixing');</script>";         
                            die();
                        }
                    } 
                }

            }
            break;
        case "mixing_wip" :
            {            	
                if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_productiondate"])){ $v_productiondate = isset($_POST["v_productiondate"]); } else { $v_productiondate = $_POST["v_productiondate"]; }   
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }
                
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }
                
                $v_batchnumber = save_char($v_batchnumber);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    if($v_del==1)
                    {   
                        // delete productionmixingwip, bincardwip, production
                        
                        $q = "
                                SELECT
                                    productionmixingwip.productionid2,
                                    productionmixingwip.batchnumber
                                FROM
                                    productionmixingwip
                                WHERE
                                    1
                                    AND productionmixingwip.productionid = '".$v_productionid."'
                                ORDER BY
                                    productionmixingwip.productionid2 ASC
                        ";
                        
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($productionid2, $batchnumber) = $row;
                            
                            $arr_data["list_batchnumber_wip"][$productionid2] = $batchnumber;
                        }
                        
                        
                        foreach($arr_data["list_batchnumber_wip"] as $key=>$val)
                        {
                            $q = "DELETE FROM bincard_wip WHERE 1 AND bincard_wip.batchnumber = '".$val."' AND module = 'MW' ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Hapus bincard detail";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();
                            }     
                        }
                        
                        $q = "DELETE FROM bincard_wip WHERE 1 AND bincard_wip.batchnumber = '".$v_batchnumber."' AND module = 'MW' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus bincard_wip";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        $q = "DELETE FROM productionmixingwip WHERE 1 AND productionmixingwip.productionid = '".$v_productionid."'";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus productionmixingwip";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        $q = "DELETE FROM production WHERE 1 AND production.productionid = '".$v_productionid."'";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Hapus production";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();
                        }
                        
                        $msg = "Berhasil Hapus ".$v_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_batchnumber."');</script>";         
                        die();     
                    }
                    else
                    {                        
                        // validasi
                        $q = "
                                UPDATE
                                    production
                                SET
                                    productiondate = '".format_save_date($v_productiondate)."',
                                    batchnumber = '".$v_batchnumber."',
                                    editdate = NOW(),
                                    edituser = '".$ses_login."'
                                WHERE
                                    production = '".$v_productionid."'
                        ";
                        //echo $q;die;
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Update production";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                        else
                        {
                            $q = "
                                    SELECT
                                        productionmixingwip.productionid2,
                                        productionmixingwip.batchnumber
                                    FROM
                                        productionmixingwip
                                    WHERE
                                        1
                                        AND productionmixingwip.productionid = '".$v_productionid."'
                                    ORDER BY
                                        productionmixingwip.productionid2 ASC
                            ";
                            $qry = mysql_query($q);
                            while($row = mysql_fetch_array($qry))
                            {
                                list($productionid2, $batchnumber) = $row;
                                
                                $arr_data["list_batchnumber_wip"][$productionid2] = $batchnumber;
                            }
                            
                            
                            foreach($arr_data["list_batchnumber_wip"] as $key=>$val)
                            {
                                $q = "
                                        SELECT
                                            sid
                                        FROM
                                            `bincard_wip`
                                        WHERE
                                            1
                                            AND module = 'MW'
                                            AND batchnumer = '".$val."'
                                        LIMIT
                                            0,1    
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($sid_bincard_wip) = $row;
                                
                                $q = "
                                        UPDATE `bincard_wip`
                                        SET 
                                          `carddate` = '".format_save_date($v_productiondate)."'
                                        WHERE `sid` = '".$sid_bincard_wip."'; 
                                ";
                                if(!mysql_query($q))      
                                {
                                    $msg = "Gagal Update bincard_wip detail";
                                    echo "<script>alert('".$msg."');</script>";     
                                    die();
                                }      
                            } 
                            
                            // dapatin sid bincard wip, lalu diupdate aja dhe. oke brooo
                            $q = "
                                    SELECT
                                        sid
                                    FROM
                                        `bincard_wip`
                                    WHERE
                                        1
                                        AND module = 'MX'
                                        AND batchnumer = '".$v_batchnumber."'
                                    LIMIT
                                        0,1    
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($sid_bincard_wip) = $row;
                            
                            $q = "
                                    UPDATE `bincard_wip`
                                    SET 
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$v_batchnumber."'
                                    WHERE `sid` = '".$sid_bincard_wip."'; 
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Update bincard_wip";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            } 
                            
                            $msg = "Berhasil menyimpan ".$v_batchnumber;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjax('search', '".$v_batchnumber."');</script>";         
                            die();
                        }
                    } 
                }

            }
            break;
            
        case "packaging" :
            {
            	//echo "<pre>";print_r($_POST);echo "</pre>";die;
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_formulanumber"])){ $v_formulanumber = isset($_POST["v_formulanumber"]); } else { $v_formulanumber = $_POST["v_formulanumber"]; }
                if(!isset($_POST["v_productiondate"])){ $v_productiondate = isset($_POST["v_productiondate"]); } else { $v_productiondate = $_POST["v_productiondate"]; }           
                if(!isset($_POST["v_prd_qty_pcs"])){ $v_prd_qty_pcs = isset($_POST["v_prd_qty_pcs"]); } else { $v_prd_qty_pcs = $_POST["v_prd_qty_pcs"]; }           
                if(!isset($_POST["v_packagingsize"])){ $v_packagingsize = isset($_POST["v_packagingsize"]); } else { $v_packagingsize = $_POST["v_packagingsize"]; }           
                
                if(!isset($_POST["v_matreqnumber"])){ $v_matreqnumber = isset($_POST["v_matreqnumber"]); } else { $v_matreqnumber = $_POST["v_matreqnumber"]; }           
                if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }           
                
                $v_batchnumber = save_char($v_batchnumber);
                $v_prd_qty_pcs = save_int($v_prd_qty_pcs);
                $v_packagingsize = save_int($v_packagingsize);

                $batchquantity = ($v_prd_qty_pcs/$v_packagingsize);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    
                    // validasi
                    {
                        if($v_warehousecode=="")
                        {
                            $msg = "Warehouse harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                                        
                        if($v_formulanumber=="")
                        {
                            $msg = "Formula harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                        if($v_prd_qty_pcs*1==0)
                        {
                            $msg = "Prod Qty (pcs) harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }

                        $all_prod_batch = 0;
                        foreach($v_matreqnumber as $key=>$val)
                        {
                            if(!isset($_POST["v_matreq_batchremain_".$val])){ $v_matreq_batchremain = isset($_POST["v_matreq_batchremain_".$val]); } else { $v_matreq_batchremain = $_POST["v_matreq_batchremain_".$val]; }
                            if(!isset($_POST["v_prod_batch_".$val])){ $v_prod_batch = isset($_POST["v_prod_batch_".$val]); } else { $v_prod_batch = $_POST["v_prod_batch_".$val]; }
                            
                            $v_matreq_batchremain = save_int($v_matreq_batchremain);
                            $v_prod_batch = save_int($v_prod_batch);
                            
                            if($v_prod_batch > $v_matreq_batchremain)
                            {
                                $msg = "Prod Batch ".$val." melebihi Batch Remain";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            
                            $all_prod_batch += $v_prod_batch;
                        }
                        
                        if($all_prod_batch*1==0)
                        {
                            $msg = "Prod Batch harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        
                        foreach($v_inventorycode as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_bincard_".$val])){ $v_qty_bincard = isset($_POST["v_qty_bincard_".$val]); } else { $v_qty_bincard = $_POST["v_qty_bincard_".$val]; }
                            if(!isset($_POST["v_qty_kemas_".$val])){ $v_qty_kemas = isset($_POST["v_qty_kemas_".$val]); } else { $v_qty_kemas = $_POST["v_qty_kemas_".$val]; }
                            
                            $v_qty_bincard = save_int($v_qty_bincard)*1;
                            $v_qty_kemas = save_int($v_qty_kemas)*1;
                            
                            if($v_qty_kemas > $v_qty_bincard && false)
                            {
                                $msg = "Stock Bincard ".$val." tidak mencukupi, mohon di cek kembali";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();    
                            }
                            
                        }
                        
                    } 
                    
                    
                    
                    $q = "
                            SELECT
                                *
                            FROM
                                production 
                            WHERE
                                1
                                AND production.batchnumber = '".$v_batchnumber."'
                                AND production.mixquantity*1 != '0'
                            LIMIT
                                0,1
                    ";
                    $qry_old = mysql_query($q);
                    $arr_old = mysql_fetch_array($qry_old);
                    
                    $q = "
                            SELECT
                                formula.quantity AS qty_kg
                            FROM
                                formula
                            WHERE
                                1
                                AND formula.formulanumber = '".$arr_old["formulanumber"]."'
                            LIMIT
                                0,1 
                    "; 
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry);
                    list($qty_kg) = $row;
                    
                    $qout_wip = $batchquantity * $qty_kg;
                    
                    $counter_productionid = get_counter_int("", "production", "productionid", 100);
                        
                    $q = "
                            INSERT `production`
                            SET `productionid` = '".$counter_productionid."',
                              `batchnumber` = '".$v_batchnumber."',
                              `productiondate` = '".format_save_date($v_productiondate)."',
                              `formulanumber` = '".$arr_old["formulanumber"]."',
                              `mixquantity` = '0.0000',
                              `mixvalue` = '0.0000',
                              `packvalue` = '0.0000',
                              `mixdate` = '".format_save_date($v_productiondate)."',
                              `packdate` = '".format_save_date($v_productiondate)."',
                              `status` = '2',
                              `adduser` = '".$ses_login."',
                              `adddate` = NOW(),
                              `edituser` = '".$ses_login."',
                              `editdate` = NOW(),
                              `inventoryprefix` = '".$arr_old["inventoryprefix"]."',
                              `batchquantity` = '".$batchquantity."',
                              `prodstat` = '1',
                              `formulanoref` = '".$v_formulanumber."',
                              `batchnoref` = '".$v_batchnumber."',
                              `packagingstatus` = '1',
                              `isposted` = '0',
                              `mixingfoh` = '0.00',
                              `packagingfoh` = '0.00',
                              `hasfinishgood` = '0',
                              `warehousecode` = '".$v_warehousecode."',
                              `productreworkcode` = '',
                              `reworkquantity` = '0',
                              `reworkcogsvalue` = '0.000',
                              `usereworkmaterial` = '0',
                              `remarks` = '',
                              `rework_type` = '',
                              `status_batchnumber` = 'Open' 
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan production";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    else
                    {
                        foreach($v_inventorycode as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_kemas_".$val])){ $v_qty_kemas = isset($_POST["v_qty_kemas_".$val]); } else { $v_qty_kemas = $_POST["v_qty_kemas_".$val]; }
                            
                            $v_qty_kemas = save_int($v_qty_kemas);
                             
                            $q = "
                                    INSERT INTO `productionpackaging`
                                    SET 
                                      `productionid` = '".$counter_productionid."',
                                      `packagingdate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$v_batchnumber."',
                                      `inventorycode` = '".$val."',
                                      `quantity` = '".$v_qty_kemas."',
                                      `value` = '0.0000',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW(),
                                      `invstatuspack` = '0',
                                      `mutationqtypack` = '0',
                                      `addquantity` = '0.0000' 
                            "; 
                            
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan productionpackaging";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }    
                            
                            $q = "
                                    INSERT INTO `bincard`
                                    SET 
                                      `inventorycode` = '".$val."',
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `referenceno` = '".$counter_productionid."',
                                      `module` = 'MX',
                                      `qin` = '0.000000',
                                      `qout` = '".$v_qty_kemas."',
                                      `value` = '0.0000',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW()
                            "; 
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }   
                        }
                        
                        
                        foreach($v_matreqnumber as $key=>$val)
                        {
                            if(!isset($_POST["v_prod_batch_".$val])){ $v_prod_batch = isset($_POST["v_prod_batch_".$val]); } else { $v_prod_batch = $_POST["v_prod_batch_".$val]; }
                            
                            $v_prod_batch = save_int($v_prod_batch);
                            
                            if($v_prod_batch)
                            {
                                $q = "
                                        INSERT INTO `productionbatchrequest`
                                        SET 
                                          `productionid` = '".$counter_productionid."',
                                          `batchnumber` = '".$v_batchnumber."',
                                          `matreqnumber` = '".$val."',
                                          `production` = '2',
                                          `prod_batch_packaging` = '".$v_prod_batch."'
                                ";  
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal menyimpan productionbatchrequest";
                                    echo "<script>alert('".$msg."');</script>";
                                    die();
                                } 
                                else
                                {
                                    $q = "
                                            SELECT
                                                SUM(`productionbatchrequest`.prod_batch_packaging) AS jml_prod_batch
                                            FROM
                                                `productionbatchrequest`
                                            WHERE
                                                1
                                                AND `productionbatchrequest`.matreqnumber = '".$val."'
                                    ";
                                    $qry = mysql_query($q);
                                    $row = mysql_fetch_array($qry);
                                    list($jml_prod_batch) = $row;
                                    
                                    $q = "
                                            UPDATE
                                                `matreq`
                                            SET
                                                 batchremain = (quantity - ".$jml_prod_batch.")
                                            WHERE
                                                1
                                                AND matreqnumber = '".$val."'
                                    ";
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal Update Matreq";
                                        echo "<script>alert('".$msg."');</script>";
                                        die();     
                                    }
                                }     
                            }
                        }
                        
                        // insert bincard wip
                        {
                            $q = "
                                    INSERT INTO `bincard_wip`
                                    SET 
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$v_batchnumber."',
                                      `module` = 'PD',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `qin` = '0.0000',
                                      `qout` = '".$qout_wip."',
                                      productionid = '".$counter_productionid."'
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard_wip";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            } 
                        }
                      
                        $msg = "Berhasil menyimpan ".$v_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_batchnumber."', 'packaging');</script>";         
                        die(); 
                    }
                
                } 
            }
            break;
        
        case "edit_packaging" :
            {
                if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                if(!isset($_POST["v_productiondate"])){ $v_productiondate = isset($_POST["v_productiondate"]); } else { $v_productiondate = $_POST["v_productiondate"]; }
                if(!isset($_POST["v_prd_qty_pcs"])){ $v_prd_qty_pcs = isset($_POST["v_prd_qty_pcs"]); } else { $v_prd_qty_pcs = $_POST["v_prd_qty_pcs"]; }
                
                if(!isset($_POST["v_inventorycode"])){ $v_inventorycode = isset($_POST["v_inventorycode"]); } else { $v_inventorycode = $_POST["v_inventorycode"]; }                
                
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }
                
                $v_prd_qty_pcs = save_int($v_prd_qty_pcs);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {     
                    // valiadasi
                    {
                        foreach($v_inventorycode as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_bincard_".$val])){ $v_qty_bincard = isset($_POST["v_qty_bincard_".$val]); } else { $v_qty_bincard = $_POST["v_qty_bincard_".$val]; }        
                            if(!isset($_POST["v_qty_kemas_".$val])){ $v_qty_kemas = isset($_POST["v_qty_kemas_".$val]); } else { $v_qty_kemas = $_POST["v_qty_kemas_".$val]; }        
                            if(!isset($_POST["v_qty_kemas_old_".$val])){ $v_qty_kemas_old = isset($_POST["v_qty_kemas_old_".$val]); } else { $v_qty_kemas_old = $_POST["v_qty_kemas_old_".$val]; }        
                            
                            $v_qty_bincard = save_int($v_qty_bincard)*1;
                            $v_qty_kemas = save_int($v_qty_kemas)*1;
                            $v_qty_kemas_old = save_int($v_qty_kemas_old)*1;
                            
                            $qty_ready = $v_qty_bincard + $v_qty_kemas_old;
                            
                            if($qty_ready<$v_qty_kemas)
                            {
                                $msg = "Stock Tidak mencukupi untuk PCode ".$val.", mohon di cek kembali";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();     
                            }
                        }
                    }
                                
                    $q = "
                            SELECT
                                formula.packagingsize,
                                formula.quantity,
                                production.batchnumber,
                                production.warehousecode
                            FROM
                                production
                                INNER JOIN formula ON
                                    formula.formulanumber = production.formulanoref
                                    AND production.productionid = '".$v_productionid."'
                            WHERE
                                1
                            LIMIT
                                0,1
                    ";
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry);
                    list($packagingsize, $quantity_kg, $batchnumber, $warehousecode) = $row;
                    
                    $batchquantity = ($v_prd_qty_pcs/$packagingsize);
                    $qout_wip = $batchquantity * $quantity_kg;
                    
                    // validasi
                    $q = "
                            UPDATE
                                production
                            SET
                                productiondate = '".format_save_date($v_productiondate)."',
                                batchquantity = '".$batchquantity."',
                                editdate = NOW(),
                                edituser = '".$ses_login."'
                            WHERE
                                productionid = '".$v_productionid."'
                    ";
                    if(!mysql_query($q))      
                    {
                        $msg = "Gagal Update production";
                        echo "<script>alert('".$msg."');</script>";     
                        die();
                    }
                    else
                    {
                        $q = "DELETE FROM bincard_wip WHERE 1 AND productionid = '".$v_productionid."' AND batchnumber = '".$batchnumber."' AND module = 'PD' ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Delete bincard_wip";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                           
                        $q = "
                                INSERT INTO `bincard_wip`
                                SET 
                                  `carddate` = '".format_save_date($v_productiondate)."',
                                  `batchnumber` = '".$batchnumber."',
                                  `module` = 'PD',
                                  `warehousecode` = '".$warehousecode."',
                                  `qin` = '0',
                                  `qout` = '".$qout_wip."',
                                  `productionid` = '".$v_productionid."'
                        "; 
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert bincard_wip";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        } 
                        
                        $q = "DELETE FROM productionpackadditional WHERE 1 AND productionid = '".$v_productionid."'";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Delete productionpackadditional";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                        
                        $q = "DELETE FROM productionpackaging WHERE 1 AND productionid = '".$v_productionid."'";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Delete productionpackaging";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                        
                        $q = "DELETE FROM bincard WHERE 1 AND bincard.referenceno = '".$v_productionid."' AND module = 'MX'";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Delete bincard";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                        
                        foreach($v_inventorycode as $key=>$val)
                        {
                            if(!isset($_POST["v_qty_bincard_".$val])){ $v_qty_bincard = isset($_POST["v_qty_bincard_".$val]); } else { $v_qty_bincard = $_POST["v_qty_bincard_".$val]; }        
                            if(!isset($_POST["v_qty_kemas_".$val])){ $v_qty_kemas = isset($_POST["v_qty_kemas_".$val]); } else { $v_qty_kemas = $_POST["v_qty_kemas_".$val]; }        
                            if(!isset($_POST["v_qty_kemas_old_".$val])){ $v_qty_kemas_old = isset($_POST["v_qty_kemas_old_".$val]); } else { $v_qty_kemas_old = $_POST["v_qty_kemas_old_".$val]; }        
                            
                            $v_qty_bincard = save_int($v_qty_bincard);
                            $v_qty_kemas = save_int($v_qty_kemas);
                            $v_qty_kemas_old = save_int($v_qty_kemas_old);
                            
                            $q = "
                                    INSERT INTO `productionpackaging`
                                    SET 
                                      `productionid` = '".$v_productionid."',
                                      `packagingdate` = '".format_save_date($v_productiondate)."',
                                      `batchnumber` = '".$batchnumber."',
                                      `inventorycode` = '".$val."',
                                      `quantity` = '".$v_qty_kemas."',
                                      `value` = '0',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW()
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Insert productionpackaging";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            } 
                            
                            $q = "
                                    INSERT INTO `bincard`
                                    SET 
                                      `inventorycode` = '".$val."',
                                      `carddate` = '".format_save_date($v_productiondate)."',
                                      `warehousecode` = '".$warehousecode."',
                                      `referenceno` = '".$v_productionid."',
                                      `module` = 'MX',
                                      `qin` = '0.0000',
                                      `qout` = '".$v_qty_kemas."',
                                      `value` = '0.0000',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW()
                            ";
                            if(!mysql_query($q))      
                            {
                                $msg = "Gagal Insert bincard";
                                echo "<script>alert('".$msg."');</script>";     
                                die();
                            }
                        }
                        
                        $msg = "Berhasil menyimpan ".$v_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_batchnumber."', 'packaging');</script>";         
                        die();
                    }
                
                }

            }
            break;
        case "finish_good" :
            {
                if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }    
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }    
                
                $v_batchnumber = save_char($v_batchnumber);
                
                foreach($v_productionid as $key=>$val)
                {
                    if(!isset($_POST["v_productionid_fg_".$val])){ $v_productionid_fg = isset($_POST["v_productionid_fg_".$val]); } else { $v_productionid_fg = $_POST["v_productionid_fg_".$val]; }        
                    if(!isset($_POST["v_qty_".$val])){ $v_qty = isset($_POST["v_qty_".$val]); } else { $v_qty = $_POST["v_qty_".$val]; }        
                    if(!isset($_POST["v_qty_qc_".$val])){ $v_qty_qc = isset($_POST["v_qty_qc_".$val]); } else { $v_qty_qc = $_POST["v_qty_qc_".$val]; }        
                    if(!isset($_POST["v_qc_reject_".$val])){ $v_qc_reject = isset($_POST["v_qc_reject_".$val]); } else { $v_qc_reject = $_POST["v_qc_reject_".$val]; }        
                    
                    $v_productionid_fg = save_int($v_productionid_fg);
                    $v_qty = save_int($v_qty);
                    $v_qty_qc = save_int($v_qty_qc);
                    $v_qc_reject = save_int($v_qc_reject);
                    
                    $qin  = $v_qty + $v_qty_qc + $v_qc_reject; 
                    $qout = $v_qty_qc + $v_qc_reject;
                    
                    $q = "
                            SELECT
                                *
                            FROM
                                production 
                            WHERE
                                1
                                AND production.productionid = '".$val."'
                            LIMIT
                                0,1
                    ";
                    $qry_old = mysql_query($q);
                    $arr_old = mysql_fetch_array($qry_old);
                    
                    $q = "
                            SELECT
                                formula.inventorycode
                            FROM
                                formula
                            WHERE
                                1
                                AND formula.formulanumber = '".$arr_old["formulanoref"]."'
                            LIMIT
                                0,1
                    ";
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry); 
                    list($inventorycode) = $row;
                    
                    if($v_productionid_fg)
                    {
                        $q = "
                                UPDATE `productionfinishgood`
                                SET 
                                  `productiondate` = '".$arr_old["productiondate"]."',
                                  `quantity` = '".$v_qty."',
                                  `quantityqc` = '".$v_qty_qc."',
                                  `editdate` = NOW(),
                                  `edituser` = '".$ses_login."',
                                  `remainquantity` = '".$v_qty."',
                                  `quantity_qc_reject` = '".$v_qc_reject."'
                                WHERE
                                    1
                                    AND `productionid` = '".$v_productionid_fg."'
                                    AND `productionid2` = '".$val."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update productionfinishgood";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        $q = "DELETE FROM bincard WHERE 1 AND referenceno = '".$v_productionid_fg."' AND module = 'FG' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Reset bincard";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        if($qin || $qout)
                        {
                            $q = "
                                    INSERT INTO `bincard`
                                    SET 
                                      `inventorycode` = '".$inventorycode."',
                                      `carddate` = '".$arr_old["productiondate"]."',
                                      `warehousecode` = '".$arr_old["warehousecode"]."',
                                      `referenceno` = '".$v_productionid_fg."',
                                      `module` = 'FG',
                                      `qin` = '".$qin."',
                                      `qout` = '".$qout."',
                                      `value` = '0.0000',
                                      `adduser` = '".$ses_login."',
                                      `adddate` = NOW(),
                                      `edituser` = '".$ses_login."',
                                      `editdate` = NOW()
                            "; 
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Insert Bincard";
                                echo "<script>alert('".$msg."');</script>"; 
                                die();     
                            } 
                        }     
                    }
                    else
                    {
                        $q = "DELETE FROM productionfinishgood WHERE 1 AND productionid2 = '".$val."' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Reset productionfinishgood";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        $q = "DELETE FROM bincard WHERE 1 AND referenceno = '".$val."' AND module = 'FG' ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Reset bincard";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        
                        $counter_productionid = get_counter_int("", "productionfinishgood", "productionid", 100);
                        
                        $q = "
                                INSERT INTO `productionfinishgood`
                                SET `productionid` = '".$counter_productionid."',
                                  `productionid2` = '".$val."',
                                  `productiontypeid` = '1',
                                  `batchnumber` = '".$arr_old["batchnumber"]."',
                                  `inventorycode` = '".$inventorycode."',
                                  `productiondate` = '".$arr_old["productiondate"]."',
                                  `quantity` = '".$v_qty."',
                                  `quantityqc` = '".$v_qty_qc."',
                                  `reportno` = '-',
                                  `mixingwip` = '0',
                                  `packagingwip` = '0',
                                  `adddate` = NOW(),
                                  `adduser` = '".$ses_login."',
                                  `editdate` = NOW(),
                                  `edituser` = '".$ses_login."',
                                  `status` = '1',
                                  `isposted` = '0',
                                  `hppvalue` = '0.0000',
                                  `price_wip` = '0.0000',
                                  `price_packaging` = '0.0000',
                                  `price_packaging_foh` = '0.0000',
                                  `remainquantity` = '".$v_qty."',
                                  `price_rework` = '0.0000',
                                  `quantity_qc_reject` = '".$v_qc_reject."',
                                  `qc_reject_remarks` = ''
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Insert productionfinishgood";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();     
                        }
                        else
                        {
                            if($qin || $qout)
                            {
                                $q = "
                                        INSERT INTO `bincard`
                                        SET 
                                          `inventorycode` = '".$inventorycode."',
                                          `carddate` = '".$arr_old["productiondate"]."',
                                          `warehousecode` = '".$arr_old["warehousecode"]."',
                                          `referenceno` = '".$counter_productionid."',
                                          `module` = 'FG',
                                          `qin` = '".$qin."',
                                          `qout` = '".$qout."',
                                          `value` = '0.0000',
                                          `adduser` = '".$ses_login."',
                                          `adddate` = NOW(),
                                          `edituser` = '".$ses_login."',
                                          `editdate` = NOW()
                                "; 
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal Insert Bincard";
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();     
                                }
                            }       
                        } 
                             
                    }
                    
                }
                
                $msg = "Berhasil menyimpan ".$v_batchnumber;
                echo "<script>alert('".$msg."');</script>"; 
                echo "<script>parent.CallAjax('search', '".$v_batchnumber."', 'finish_good');</script>";         
                die();  
            } 
            break;
            
        case "transfer" :
            {
            	//echo "Hello";die;//TransferOut add
                if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_transfer_date"])){ $v_transfer_date = isset($_POST["v_transfer_date"]); } else { $v_transfer_date = $_POST["v_transfer_date"]; }
                if(!isset($_POST["v_purpose"])){ $v_purpose = isset($_POST["v_purpose"]); } else { $v_purpose = $_POST["v_purpose"]; }           
                if(!isset($_POST["v_amount"])){ $v_amount = isset($_POST["v_amount"]); } else { $v_amount = $_POST["v_amount"]; }           
                if(!isset($_POST["v_remarks"])){ $v_remarks = isset($_POST["v_remarks"]); } else { $v_remarks = $_POST["v_remarks"]; }           
                
                if(!isset($_POST["v_sisa_stock"])){ $v_sisa_stock = isset($_POST["v_sisa_stock"]); } else { $v_sisa_stock = $_POST["v_sisa_stock"]; }           
                
                $v_batchnumber = save_char($v_batchnumber);
                $v_warehousecode = save_char($v_warehousecode);
                $v_transfer_date = format_save_date($v_transfer_date);
                $v_amount = save_int($v_amount);
                $v_sisa_stock = save_int($v_sisa_stock);
                $v_remarks = save_char($v_remarks);
                
                // validasi
                {
                    if($v_warehousecode=="")
                    {
                        $msg = "Warehouse harus dipilih";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();    
                    }
                                    
                    if($v_purpose=="")
                    {
                        $msg = "Purpose harus dipilih";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();    
                    }
                    
                    if($v_amount*1==0)
                    {
                        $msg = "Amount harus diisi";
                        echo "<script>alert('".$msg."');</script>"; 
                        die();    
                    }

                } 
                
                /*if($v_amount*1 > $v_sisa_stock*1)
                {
                    $msg = "Stock Tidak mencukupi, sisa stock ".format_number($v_sisa_stock, 4);
                    echo "<script>alert('".$msg."');</script>"; 
                    die();     
                }*/

                $counter_sid = get_counter_int("", "production_transfer", "sid", 100);
                   
                $q = "
                        INSERT `production_transfer`
                        SET `sid` = '".$counter_sid."',
                          `warehousecode` = '".$v_warehousecode."',
                          `batchnumber` = '".$v_batchnumber."',
                          `purpose` = '".$v_purpose."',
                          `transfer_date` = '".$v_transfer_date."',
                          `amount` = '".$v_amount."',
                          `remarks` = '".$v_remarks."',
                          `productionid` = '".$v_productionid."',
                          `author_user` = '".$ses_login."',
                          `author_date` = NOW(),
                          `edited_user` = '".$ses_login."',
                          `edited_date` = NOW()
                ";
                
                if(!mysql_query($q))
                {
                    $msg = "Gagal menyimpan transfer";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else
                {
                    $q = "
                            INSERT INTO `bincard_wip`
                            SET 
                                  `carddate` = '".$v_transfer_date."',
                                  `batchnumber` = '".$v_batchnumber."',
                                  `module` = 'TF',
                                  `warehousecode` = '".$v_warehousecode."',
                                  `qin` = '0.0000',
                                  `qout` = '".$v_amount."',
                                  `productionid` = '".$counter_sid."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan transfer bincar wip";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    } 
                  
                    $msg = "Berhasil menyimpan ".$v_batchnumber;
                    echo "<script>alert('".$msg."');</script>"; 
                    echo "<script>parent.CallAjax('search', '".$v_batchnumber."', 'transfer_wip');</script>";         
                    die(); 
                }
                
         
            }
            break;
            
         case "edit_transfer" :
            {
            	
                if(!isset($_POST["v_sid"])){ $v_sid = isset($_POST["v_sid"]); } else { $v_sid = $_POST["v_sid"]; }
                if(!isset($_POST["v_productionid"])){ $v_productionid = isset($_POST["v_productionid"]); } else { $v_productionid = $_POST["v_productionid"]; }
                if(!isset($_POST["v_batchnumber"])){ $v_batchnumber = isset($_POST["v_batchnumber"]); } else { $v_batchnumber = $_POST["v_batchnumber"]; }
                if(!isset($_POST["v_warehousecode"])){ $v_warehousecode = isset($_POST["v_warehousecode"]); } else { $v_warehousecode = $_POST["v_warehousecode"]; }
                if(!isset($_POST["v_transfer_date"])){ $v_transfer_date = isset($_POST["v_transfer_date"]); } else { $v_transfer_date = $_POST["v_transfer_date"]; }
                if(!isset($_POST["v_purpose"])){ $v_purpose = isset($_POST["v_purpose"]); } else { $v_purpose = $_POST["v_purpose"]; }           
                if(!isset($_POST["v_amount"])){ $v_amount = isset($_POST["v_amount"]); } else { $v_amount = $_POST["v_amount"]; }           
                if(!isset($_POST["v_remarks"])){ $v_remarks = isset($_POST["v_remarks"]); } else { $v_remarks = $_POST["v_remarks"]; }           
                if(!isset($_POST["v_muttype"])){ $v_muttype = isset($_POST["v_muttype"]); } else { $v_muttype = $_POST["v_muttype"]; }           
                
                
                if(!isset($_POST["v_sisa_stock"])){ $v_sisa_stock = isset($_POST["v_sisa_stock"]); } else { $v_sisa_stock = $_POST["v_sisa_stock"]; }           
                
                $v_sid = save_int($v_sid);
                $v_batchnumber = save_char($v_batchnumber);
                $v_warehousecode = save_char($v_warehousecode);
                $v_transfer_date = format_save_date($v_transfer_date);
                $v_amount = save_int($v_amount);
                $v_sisa_stock = save_int($v_sisa_stock);
                $v_remarks = save_char($v_remarks);
                
                $acc_locked = call_acc_locked("production", format_save_date($v_productiondate), format_save_date($v_productiondate));
                
                if($acc_locked["locked"][format_save_date($v_productiondate)]*1==1)
                {
                    $msg = "Data tanggal ".$v_productiondate." sudah di Acc Locked";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();    
                }
                else
                {
                    
                    // validasi
                    {
                        if($v_warehousecode=="")
                        {
                            $msg = "Warehouse harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                                        
                        if($v_purpose=="")
                        {
                            $msg = "Purpose harus dipilih";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }
                        
                        if($v_amount*1==0)
                        {
                            $msg = "Amount harus diisi";
                            echo "<script>alert('".$msg."');</script>"; 
                            die();    
                        }

                    } 
                    
                    if($v_amount*1 > $v_sisa_stock*1)
                    {
                        $msg = "Stock Tidak mencukupi, sisa stock ".format_number($v_sisa_stock, 4);
                        echo "<script>alert('".$msg."');</script>"; 
                        die();     
                    }

                        
                    $q = "
                            UPDATE `production_transfer`
                            SET 
                              `purpose` = '".$v_purpose."',
                              `transfer_date` = '".$v_transfer_date."',
                              `amount` = '".$v_amount."',
                              `remarks` = '".$v_remarks."',
                              `edited_user` = '".$ses_login."',
                              `edited_date` = NOW()
                            WHERE
                                `sid` = '".$v_sid."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan transfer";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    else
                    {
                        $q = "DELETE FROM `bincard_wip` WHERE `module` = 'TF' AND `productionid` = '".$v_sid."'";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Reset transfer bincar wip";
                            echo "<script>alert('".$msg."');</script>";
                            die();
                        } 
                        
                        if($v_muttype=="in")
                        {
                            $qin = $v_amount;    
                            $qout = 0;
                        }
                        else
                        {
                            $qin = 0;    
                            $qout = $v_amount;
                        }
                        
                        $q = "
                                INSERT INTO `bincard_wip`
                                SET 
                                      `carddate` = '".$v_transfer_date."',
                                      `batchnumber` = '".$v_batchnumber."',
                                      `module` = 'TF',
                                      `warehousecode` = '".$v_warehousecode."',
                                      `qin` = '".$qin."',
                                      `qout` = '".$qout."',
                                      `productionid` = '".$v_sid."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan transfer bincar wip";
                            echo "<script>alert('".$msg."');</script>";
                            die();
                        } 
                      
                        $msg = "Berhasil menyimpan ".$v_batchnumber;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjax('search', '".$v_batchnumber."', 'transfer_wip');</script>";         
                        die(); 
                    }
                
                } 
            }
            break;
            
        case "close_batchnumber" :
            {
				//echo "<pre>";print_r($_POST);echo "</pre>";die;
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }    
                
                $sukses = 0;
                foreach($v_data as $key=>$val)
                {
                    $q = "
                            SELECT
                                SUM(bincard_wip.qin) AS qin,
                                SUM(bincard_wip.qout) AS qout
                            FROM
                                bincard_wip
                            WHERE
                                1
                                AND bincard_wip.batchnumber = '".$val."'
                    ";    
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry);
                    list($qin, $qout) = $row;
                    
                    $q = "
                            SELECT
                                *
                            FROM
                                production 
                            WHERE
                                1
                                AND production.batchnumber = '".$val."'
                                AND production.mixquantity*1 != '0'
                            LIMIT
                                0,1
                    ";
                    $qry_old = mysql_query($q);
                    $arr_old = mysql_fetch_array($qry_old);
                    
                    $selisih = $qin - $qout;
                    
                    $q = "DELETE FROM `bincard_wip` WHERE 1 AND `batchnumber` = '".$val."' AND module = 'WS'";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Reset bincard wip";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }  
                    
                    if($selisih*1>0)
                    {
                        $q = "
                                INSERT INTO `bincard_wip`
                                SET 
                                  `carddate` = NOW(),
                                  `batchnumber` = '".$val."',
                                  `module` = 'WS',
                                  `warehousecode` = '".$arr_old["warehousecode"]."',
                                  `qin` = '0.0000',
                                  `qout` = '".$selisih."',
                                  `productionid` = ''
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan bincard wip";
                            echo "<script>alert('".$msg."');</script>";
                            die();
                        }     
                    }
                    else
                    {
                        if($selisih*1!=0)
                        {
                            $q = "
                                    INSERT INTO `bincard_wip`
                                    SET 
                                      `carddate` = NOW(),
                                      `batchnumber` = '".$val."',
                                      `module` = 'WS',
                                      `warehousecode` = '".$arr_old["warehousecode"]."',
                                      `qin` = '".abs($selisih)."',
                                      `qout` = '0.0000',
                                      `productionid` = ''
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan bincard wip";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }
                        } 
                    } 
                    
                    $q = "
                            UPDATE 
                                production
                            SET
                                status_batchnumber = 'Close',
                                edituser = '".$ses_login."',
                                editdate = NOW()
                            WHERE
                                1
                                AND production.productionid = '".$arr_old["productionid"]."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Update Status Batchnumber";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    else
                    {
                        $sukses++;
                    }
					
					//insert mutasi dan stock mixing
					$x = "
                           SELECT 
							  a.`inventorycode`,
							  a.`quantity` 
							FROM
							  `productionmixing` a 
							WHERE a.`batchnumber` = '".$val."' ;
                    ";    
                    $qryx = mysql_query($x);
                    while($key = mysql_fetch_array($qryx)){
						$PCode = $key['inventorycode'];
						$QtyMix = $key['quantity'];
						
						$x_mutasi = "
                                    INSERT INTO `mutasi`
                                    SET 
										 `NoTransaksi`= '".$val."',
										 `Jenis`= 'O',
										 `KdTransaksi` = 'MX',
										 `Gudang` = '".$arr_old["warehousecode"]."',
										 `GudangTujuan` ='',
										 `Tanggal` = '".$arr_old["productiondate"]."',
										 `KodeBarang` = '".$PCode."',
										 `Qty` ='".$QtyMix."',
										 `Nilai` ='0',
										 `Status` ='1',
										 `Kasir` = '',
										 `Keterangan` = 'Production Form Mixing',
										 `HPP` ='0',
										 `PPN` ='0',
										 `Service_charge`='0'
                            ";
							//echo $x_mutasi;die;
                            if(!mysql_query($x_mutasi))
                            {
                                $msg = "Gagal menyimpan Mutasi Mixing";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }
							
							$arr_exp_mixdate = explode("-", $arr_old["productiondate"]);
                            $tahun    = $arr_exp_mixdate[0];
							$bulan    = $arr_exp_mixdate[1];
							$field = "GKeluar".$bulan;
							
							//pertama ambil nilai di stock untuk ditambahkan dan kemudian di update
							$x_cari_stock = "
							   SELECT a.".$field." AS jml FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$arr_old["warehousecode"]."' AND a.`PCode`='".$PCode."';
							";    
							$qryx_stock = mysql_query($x_cari_stock);
							$keys = mysql_fetch_array($qryx_stock);
							$isi_gkeluar = $keys[0]['jml'];
							$tambah_isi_gkeluar = $isi_gkeluar + $QtyMix;
							
							$x_stock = "
                            UPDATE 
                                stock
                            SET
                                ".$field." = '".$tambah_isi_gkeluar."'
                            WHERE
                                1
                                AND Tahun='".$tahun."' AND KdGudang='".$arr_old["warehousecode"]."' AND PCode='".$PCode."'
							";
							if(!mysql_query($x_stock))
							{
								$msg = "Gagal Update Stock Mixing";
								echo "<script>alert('".$msg."');</script>";
								die();
							}
						
					
					}
					
					
					//insert mutasi dan stock Packging
					$y = "
                           SELECT 
							  a.`inventorycode`,
							  a.`quantity` 
							FROM
							  `productionpackaging` a  
							WHERE a.`batchnumber` = '".$val."' ;
                    ";    
                    $qryy = mysql_query($y);
                    while($key2 = mysql_fetch_array($qryy)){
						$PCode2 = $key2['inventorycode'];
						$QtyPackaging = $key2['quantity'];
						
						$x_packaging = "
                                    INSERT INTO `mutasi`
                                    SET 
										 `NoTransaksi`= '".$val."',
										 `Jenis`= 'O',
										 `KdTransaksi` = 'MX',
										 `Gudang` = '".$arr_old["warehousecode"]."',
										 `GudangTujuan` ='',
										 `Tanggal` = '".$arr_old["productiondate"]."',
										 `KodeBarang` = '".$PCode2."',
										 `Qty` ='".$QtyPackaging."',
										 `Nilai` ='0',
										 `Status` ='1',
										 `Kasir` = '',
										 `Keterangan` = 'Production Packaging',
										 `HPP` ='0',
										 `PPN` ='0',
										 `Service_charge`='0'
                            ";
                            if(!mysql_query($x_packaging))
                            {
                                $msg = "Gagal menyimpan Mutasi Packaging";
                                echo "<script>alert('".$msg."');</script>";
                                die();
                            }
							
							$arr_exp_mixdate2 = explode("-", $arr_old["productiondate"]);
                            $tahun2   = $arr_exp_mixdate2[0];
							$bulan2    = $arr_exp_mixdate2[1];
							$field2 = "GKeluar".$bulan2;
							
							//pertama ambil nilai di stock untuk ditambahkan dan kemudian di update
							$x_cari_stock2 = "
							   SELECT a.".$field2." AS jml FROM `stock` a WHERE a.`Tahun`='".$tahun2."' AND a.`KdGudang`='".$arr_old["warehousecode"]."' AND a.`PCode`='".$PCode2."';
							";    
							$qryx_stock2 = mysql_query($x_cari_stock2);
							$keys2 = mysql_fetch_array($qryx_stock2);
							$isi_gkeluar2 = $keys2[0]['jml'];
							$tambah_isi_gkeluar2 = $isi_gkeluar2 + $QtyPackaging;
							
							$x_stock2 = "
                            UPDATE 
                                stock
                            SET
                                ".$field2." = '".$tambah_isi_gkeluar2."'
                            WHERE
                                1
                                AND Tahun='".$tahun2."' AND KdGudang='".$arr_old["warehousecode"]."' AND PCode='".$PCode2."'
							";
							if(!mysql_query($x_stock2))
							{
								$msg2 = "Gagal Update Stock Packaging";
								echo "<script>alert('".$msg2."');</script>";
								die();
							}
						
					
					}
					
					//insert mutasi dan stock finish good
					$z = "
                           SELECT 
							  a.`inventorycode`,
							  a.`quantity` 
							FROM
							  `productionfinishgood` a    
							WHERE a.`batchnumber` = '".$val."' ;
                    ";    
                    $qryz = mysql_query($z);
                    while($key3 = mysql_fetch_array($qryz)){
						$PCode3 = $key3['inventorycode'];
						$QtyFG = $key3['quantity'];
						
						$x_FG = "
                                    INSERT INTO `mutasi`
                                    SET 
										 `NoTransaksi`= '".$val."',
										 `Jenis`= 'I',
										 `KdTransaksi` = 'FG',
										 `Gudang` = '".$arr_old["warehousecode"]."',
										 `GudangTujuan` ='',
										 `Tanggal` = '".$arr_old["productiondate"]."',
										 `KodeBarang` = '".$PCode3."',
										 `Qty` ='".$QtyFG."',
										 `Nilai` ='0',
										 `Status` ='1',
										 `Kasir` = '',
										 `Keterangan` = 'Production Finish Good',
										 `HPP` ='0',
										 `PPN` ='0',
										 `Service_charge`='0'
                            ";
                            if(!mysql_query($x_FG))
                            {
                                $msg3 = "Gagal menyimpan Mutasi Finish Goods";
                                echo "<script>alert('".$msg3."');</script>";
                                die();
                            }
							
							$arr_exp_mixdate3 = explode("-", $arr_old["productiondate"]);
                            $tahun3   = $arr_exp_mixdate3[0];
							$bulan3   = $arr_exp_mixdate3[1];
							$field3   = "GMasuk".$bulan3;
							
							//pertama ambil nilai di stock untuk ditambahkan dan kemudian di update
							$x_cari_stock3 = "
							   SELECT a.".$field3." AS jml FROM `stock` a WHERE a.`Tahun`='".$tahun3."' AND a.`KdGudang`='".$arr_old["warehousecode"]."' AND a.`PCode`='".$PCode3."';
							";    
							$qryx_stock3 = mysql_query($x_cari_stock3);
							$keys3 = mysql_fetch_array($qryx_stock3);
							$isi_gmasuk3 = $keys3[0]['jml'];
							$tambah_isi_gmasuk3 = $isi_gmasuk3 + $QtyFG;
							
							$x_stock3 = "
                            UPDATE 
                                stock
                            SET
                                ".$field3." = '".$tambah_isi_gmasuk3."'
                            WHERE
                                1
                                AND Tahun='".$tahun3."' AND KdGudang='".$arr_old["warehousecode"]."' AND PCode='".$PCode3."'
							";
							if(!mysql_query($x_stock3))
							{
								$msg3= "Gagal Update Stock Finish Goods";
								echo "<script>alert('".$msg3."');</script>";
								die();
							}
						
					
					}
					
					
                }
                
                $msg = "Berhasil Close Batchnumber ".$sukses." data";
                echo "<script>alert('".$msg."');</script>"; 
                echo "<script>parent.CallAjax('search', '".$full_batchnumber."');</script>";         
                die(); 
                       
            }
            break;    
        case "open_batchnumber" :
            {
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }    
                
                $sukses = 0;
                foreach($v_data as $key=>$val)
                {
                     $q = "
                            SELECT
                                *
                            FROM
                                production 
                            WHERE
                                1
                                AND production.batchnumber = '".$val."'
                                AND production.mixquantity*1 != '0'
                            LIMIT
                                0,1
                    ";
                    $qry_old = mysql_query($q);
                    $arr_old = mysql_fetch_array($qry_old);
                    
                    $q = "DELETE FROM `bincard_wip` WHERE 1 AND `batchnumber` = '".$val."' AND module = 'WS'";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Reset bincard wip";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }  
                    
                    $q = "
                            UPDATE 
                                production
                            SET
                                status_batchnumber = 'Open',
                                edituser = '".$ses_login."',
                                editdate = NOW()
                            WHERE
                                1
                                AND production.productionid = '".$arr_old["productionid"]."'
                    ";
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal Update Status Batchnumber";
                        echo "<script>alert('".$msg."');</script>";
                        die();
                    }
                    else
                    {
                        $sukses++;
                    }
                }
                
                $msg = "Berhasil Open Batchnumber ".$sukses." data";
                echo "<script>alert('".$msg."');</script>"; 
                echo "<script>parent.CallAjax('search');</script>";         
                die(); 
                       
            }
            break; 
           
        /*case "print_setengah_letter" :
            {
                if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
                
                if($v_data=="")
                {
                    $msg = "Data harus dipilih";
                    echo "<script>alert('".$msg."');</script>"; 
                    die();
                }
                
                $arr_epson = sintak_epson();
                
                $total_spasi = 125;
                $jml_detail  = 9;
                $ourFileName = "delivery-order.txt";
                $nama        = "PT. VICTORIA CARE INDONESIA";
                
                header("Content-type: application/txt");
                header("Content-Disposition: attachment; filename=$ourFileName");
                //$content = openFile($ourFileName);
                
                $q = "
                        SELECT
                            warehouse.warehousecode,
                            warehouse.warehousename
                        FROM
                            warehouse
                        WHERE
                            1
                            AND warehouse.warehousecode IN ('WH002','WH005')
                        ORDER BY
                            warehouse.warehousecode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($warehousecode, $warehousename) = $row;
                    
                    $arr_data["list_warehouse"][$warehousecode] = $warehousecode;
                    
                    $arr_data["warehousename"][$warehousecode] = $warehousename;
                }

                $echo  = "";
                foreach($v_data as $key => $val)
                {
                    $dono = $val;
                    
                    $q = "
                            SELECT
                                deliveryorder.dono,
                                deliveryorder.dodate,
                                deliveryorder.contactperson,
                                deliveryorder.warehousecode,
                                customer.customername,
                                deliveryorder.editdate
                            FROM
                                deliveryorder
                                INNER JOIN customer ON
                                    customer.customerid = deliveryorder.customerid
                                    AND deliveryorder.dono = '".$dono."'
                                    AND deliveryorder.status != '2'
                            WHERE
                                1
                            LIMIT
                                0,1
                    ";
                    $qry_curr = mysql_query($q);
                    $arr_curr = mysql_fetch_array($qry_curr);
                    
                    $q = "
                            SELECT
                                inventorymaster.stocktypeid,
                                deliveryorderdetail.inventorycode,
                                inventorymaster.inventoryname,
                                deliveryorderdetail.quantity,
                                inventorymaster.uominitial,
                                deliveryorderdetail.batchnumber,
                                deliveryorderdetail.productionid,
                                inventorymaster.expired,
                                inventorymaster.halal
                            FROM
                                deliveryorderdetail
                                INNER JOIN inventorymaster ON
                                    inventorymaster.inventorycode = deliveryorderdetail.inventorycode
                            WHERE
                                1
                                AND deliveryorderdetail.dono = '".$dono."'
                                AND (deliveryorderdetail.quantity*1) != '0'
                            ORDER BY
                                deliveryorderdetail.sid ASC
                    ";
                    $counter = 1;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($stocktypeid, $inventorycode, $inventoryname, $quantity, $uominitial, $batchnumber, $productionid, $expired, $halal) = $row;
                        
                        $arr_data["list_detail"][$arr_curr["dono"]][$counter] = $counter;
                        
                        $arr_data["detail_stocktypeid"][$arr_curr["dono"]][$counter] = $stocktypeid;
                        $arr_data["detail_inventorycode"][$arr_curr["dono"]][$counter] = $inventorycode;
                        $arr_data["detail_inventoryname"][$arr_curr["dono"]][$counter] = $inventoryname;
                        $arr_data["detail_expired"][$arr_curr["dono"]][$counter] = $expired;
                        $arr_data["detail_quantity"][$arr_curr["dono"]][$counter] = $quantity;
                        $arr_data["detail_uominitial"][$arr_curr["dono"]][$counter] = $uominitial;
                        $arr_data["detail_batchnumber"][$arr_curr["dono"]][$counter] = $batchnumber;
                        $arr_data["detail_productionid"][$arr_curr["dono"]][$counter] = $productionid;
                        $arr_data["detail_halal"][$arr_curr["dono"]][$counter] = $halal;
                        $counter++;
                    }

                    $curr_jml_detail = count($arr_data["list_detail"][$arr_curr["dono"]]);
                    $jml_page = ceil($curr_jml_detail/$jml_detail);
                    
                    $status_halal = false;
                    if(!$status_halal)
                    {
                        $status_halal = true;
                        foreach($arr_data["list_detail"][$arr_curr["dono"]] as $counter)
                        {
                            $halal = $arr_data["detail_halal"][$arr_curr["dono"]][$counter];
                            
                            // kalo ketemu gak halal, langsung break aja
                            if($halal==0)
                            {
                                $status_halal = false;
                                break;
                            }
                        }
                    }
                    
                    if($status_halal)
                    {
                        $nama_cetakan = "SURAT JALAN HALAL";
                        $kode_dokumen = "FH.PR.05";
                        $kode_edisi   = "A";
                        $spasi_tanggal_terbit = 25;
                        $spasi_edisi = 25;
                    }
                    else
                    {
                        $nama_cetakan = "SURAT JALAN";
                        $kode_dokumen = "F.PR.05";
                        $kode_edisi   = "A";
                        $spasi_tanggal_terbit = 25;
                        $spasi_edisi = 22;
                    }    
                    
                    
                    for($i_page=1;$i_page<=$jml_page;$i_page++)
                    {
                        $echo .= $arr_data["esc8CpiOn"];
                        
                        // header
                        {
                            $echo .= $nama;
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($nama));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "";
                            
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen(""));$i++)
                            {
                                $echo .= " ";
                            }

                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen($kode_dokumen));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= $kode_dokumen; 
                            $echo .= "\r\n";    
                        }
                        
                        $echo .= "Kawasan Industri Candi Blok 5A No. 8";
                        
                        $kurang = strlen("Kawasan Industri Candi Blok 5A No. 8");
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen($nama_cetakan)/2) - $kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= $nama_cetakan;
                        
                        for($i=0;$i<$spasi_tanggal_terbit;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $tanggal_terbit      = substr($arr_curr["editdate"],0,10);
                        $arr_tanggal_terbit  = explode("-", $tanggal_terbit);
                        $tanggal_terbit_show = $arr_tanggal_terbit[2]."/".$arr_tanggal_terbit[1]."/".$arr_tanggal_terbit[0];
                        
                        $echo .= "TANGGAL TERBIT : ".$tanggal_terbit_show;
                        
                        $echo .="\r\n";
                        
                        $echo .= "Jl. Gatot Subroto Krapyak";
                        $kurang = strlen("Jl. Gatot Subroto Krapyak");
                        
                        $limit_spasi = ceil(($total_spasi/2)) - (strlen("No : ".$arr_curr["dono"])/2)-$kurang;
                        for($i=0;$i<$limit_spasi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "No : ".$arr_curr["dono"];
                        
                        
                        for($i=0;$i<$spasi_edisi;$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "Edisi / Revisi : ".$kode_edisi." /00";
                        
                        
                        $echo .="\r\n";
                        
                        $echo .= "SEMARANG - INDONESIA";
                        
                        $echo .="\r\n";    
                        
                        $echo .="\r\n";
                        $echo .= $arr_data["esc8CpiOff"];
                        
                        
                        // baris 1
                        {
                            $echo .= "PELANGGAN";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("PELANGGAN"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["customername"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["customername"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "TANGGAL";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= format_show_date($arr_curr["dodate"]); 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["dodate"])));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }

                        // baris 2
                        {
                            $echo .= "CONTACT PERSON";
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("CONTACT PERSON"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            
                            $echo .= $arr_curr["contactperson"];
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_curr["contactperson"]));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $limit_spasi = 25;
                            for($i=0;$i<$limit_spasi;$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "GUDANG ASAL";
                            
                            $limit_spasi = (20-2);
                            for($i=0;$i<($limit_spasi-strlen("GUDANG ASAL"));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= ": ";
                            $echo .= $arr_data["warehousename"][$arr_curr["warehousecode"]]; 
                            
                            $limit_spasi = 30;
                            for($i=0;$i<($limit_spasi-strlen($arr_data["warehousename"][$arr_curr["warehousecode"]]));$i++)
                            {
                                $echo .= " ";
                            }
                            $echo .= "\r\n";    
                        }

                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        $echo .= "\r\n";
                        
                        
                        $echo .= "NO";
                        $limit_spasi = 3;
                        for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $limit_spasi = 14;
                        for($i=0;$i<($limit_spasi-strlen("BANYAKNYA   "));$i++)
                        {
                            $echo .= " ";
                        }
                        $echo .= "BANYAKNYA   ";
                        
                        
                        $echo .= "NAMA BARANG";
                        $limit_spasi = 80;
                        for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "EXP DATE";
                        $limit_spasi = 13;
                        for($i=0;$i<($limit_spasi-strlen("EXP DATE"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        
                        $echo .= "PCODE SSS";
                        $limit_spasi = 15;
                        for($i=0;$i<($limit_spasi-strlen("PCODE SSS"));$i++)
                        {
                            $echo .= " ";
                        }
                        
                        $echo .= "\r\n";
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        $echo .= "\r\n";
                        
                        $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                        $no_end = $no + $jml_detail;
                        
                        for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                        {
                            $stocktypeid = $arr_data["detail_stocktypeid"][$arr_curr["dono"]][$i_detail];
                            $inventorycode = $arr_data["detail_inventorycode"][$arr_curr["dono"]][$i_detail];
                            $inventoryname = $arr_data["detail_inventoryname"][$arr_curr["dono"]][$i_detail];
                            $expired = $arr_data["detail_expired"][$arr_curr["dono"]][$i_detail];
                            $quantity = $arr_data["detail_quantity"][$arr_curr["dono"]][$i_detail];
                            $uominitial = $arr_data["detail_uominitial"][$arr_curr["dono"]][$i_detail];
                            $batchnumber = $arr_data["detail_batchnumber"][$arr_curr["dono"]][$i_detail];
                            $productionid = $arr_data["detail_productionid"][$arr_curr["dono"]][$i_detail];
                            
                            if($productionid*1!=0 && $batchnumber!="" && substr($batchnumber,0,2)!="RA" && substr($batchnumber,0,2)!="ST")
                            {
                                $batchnumber_echo = $batchnumber;
                                
                                $q = "
                                        SELECT
                                            production.mixdate
                                        FROM
                                            productionfinishgood
                                            INNER JOIN production ON
                                                productionfinishgood.productionid2 = production.productionid
                                                AND productionfinishgood.productionid = '".$productionid."'
                                        WHERE
                                            1
                                        LIMIT
                                            0,1
                                ";
                                
                                $qry_exp_date = mysql_query($q);
                                $row_exp_date = mysql_fetch_array($qry_exp_date);
                                
                                $arr_exp_mixdate = explode("-", $row_exp_date["mixdate"]);
                                $mixdate_echo    = $arr_exp_mixdate[2]."/".$arr_exp_mixdate[1]."/".$arr_exp_mixdate[0];
                                
                                $exp_date = date("d/m/Y", mktime(0,0,0, ($arr_exp_mixdate[1]+$expired), $arr_exp_mixdate[2], $arr_exp_mixdate[0]));
                                $exp_date_echo = $exp_date;
                                
                                // dapatin satuan karton
                                //@48pcs = 12Cartons + 21pcs
                                
                                $q = "SELECT value FROM measurementconversion WHERE inventorycode = '".$inventorycode."' AND active = '1' AND uominitial = 'Cartons'";
                                $qry_konversi = mysql_query($q);
                                $row_konversi = mysql_fetch_array($qry_konversi);
                                
                                $konversi_carton = floor($quantity/$row_konversi["value"]);
                                $sisa_pcs        = $quantity - ($konversi_carton*$row_konversi["value"]);
                                
                                if($sisa_pcs*1!=0)
                                {
                                    $sisa_pcs_echo = "+".$sisa_pcs."pcs";
                                }
                                else
                                {
                                    $sisa_pcs_echo = "";
                                }
                                
                                $adjust_konversi = " @".format_number($row_konversi["value"],0)."pcs = ".$konversi_carton."Cartons".$sisa_pcs_echo;
                            }
                            else
                            {
                                $batchnumber_echo = "";
                                $exp_date_echo = "";
                                $adjust_konversi =  "";
                            }

                            if($stocktypeid==1)
                            {
                                $decimal_php = 6;
                            }
                            else
                            {
                                $decimal_php = 0;
                            }
                            
                            $inventoryname = substr($inventoryname, 0, 50);
                            $note = substr($note, 0, 28);
                            
                            // kalo ada isi baru di print
                            if($inventorycode)
                            {
                                $echo .= $no;
                                $limit_spasi = 3;
                                for($i=0;$i<($limit_spasi-strlen($no));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                
                                $limit_spasi = 14;
                                for($i=0;$i<($limit_spasi-strlen(format_number($quantity, $decimal_php)."   "));$i++)
                                {
                                    $echo .= " ";
                                }
                                $echo .= format_number($quantity, $decimal_php)."   ";
                                
                                
                                $echo .= $inventoryname.$adjust_konversi;
                                $limit_spasi = 80;
                                for($i=0;$i<($limit_spasi-strlen($inventoryname.$adjust_konversi));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                
                                $echo .= $exp_date_echo;
                                $limit_spasi = 13;
                                for($i=0;$i<($limit_spasi-strlen($exp_date_echo));$i++)
                                {
                                    $echo .= " ";
                                }
                                
                                $echo .= $batchnumber_echo;
                                $limit_spasi = 15;
                                for($i=0;$i<($limit_spasi-strlen($batchnumber_echo));$i++)
                                {
                                    $echo .= " ";
                                } 
                            }
                            
                            $no++;
                            $echo .= "\r\n";
                        }
                        
                        
                        for($i=1;$i<=$total_spasi;$i++)
                        {
                            $echo .= "-";
                        }
                        
                        if($i_page==$jml_page)
                        {
                            $echo .= "\r\n";
                            //$echo .= "Note : ".$arr_curr["note"];
                            $echo .= "";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            $echo .= "             PENGIRIM             ";
                            $echo .= "          ";
                            $echo .= "                                  ";
                            $echo .= "          ";
                            $echo .= "            PENERIMA           ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "         (              )         ";
                            $echo .= "          ";
                            $echo .= "                                  ";
                            $echo .= "          ";
                            $echo .= "         (              )         ";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                        }
                        else
                        {
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";
                            $echo .= "\r\n";    
                        }
                        
                        $q = "
                                SELECT
                                    COUNT(noreferensi) AS jml_print
                                FROM
                                    log_print
                                WHERE
                                    1
                                    AND noreferensi = '".$dono."' 
                                    AND form_data = 'delivery_order' 
                        ";
                        $qry_jml_print = mysql_query($q);
                        $row_jml_print = mysql_fetch_array($qry_jml_print);
                        
                        if($row_jml_print["jml_print"]*1>0)
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                            
                            $echo .= "COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        else
                        {
                            $limit_spasi = $total_spasi;
                            for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                            {
                                $echo .= " ";
                            }
                        
                            $echo .= "HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s");    
                        }
                        
                        
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                        $echo .= "\r\n";
                    
                    } // end page
                    
                    
                    // update counter print
                    if($ses_login=="hendri1003" || $ses_login=="febri0202"){}
                    else
                    {
                        $q = "
                                INSERT
                                    log_print
                                SET
                                    form_data = 'mutasi_antar_gudang',
                                    noreferensi = '".$mgno."' , 
                                    userid = '".$ses_login."' , 
                                    print_date = NOW() , 
                                    print_page = 'Setengah Letter'    
                        ";
                        if(!mysql_query($q))      
                        {
                            $msg = "Gagal Insert Print Counter";
                            echo "<script>alert('".$msg."');</script>";     
                            die();
                        }
                    }
                }
                
                echo $echo;
            }
            break;*/
    }
}

function menutab($v_batchnumber, $menu){
    // <li ".($menu=='adjustment'?"class='current'":" ")." onclick=\"CallAjax('adjustment', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Adjustment</a></li>
    $str_menu = "
    <style type=\"text/css\">
        #smalltab {
            list-style-type : none;
            margin : 5px 0px 0px 0px;
            font: bold 11px tahoma, sans-serif;
            border-bottom: 1px solid #eeeeff;
        }
        #smalltab li {
            display : inline;
            border-top : 1px #ccccff solid;
            border-left : 1px #ccccff solid;
            border-right : 1px #ccccff solid;
            background-color : #F5FAFF;
            padding : 5px 10px 1px 10px;
        }
        
        #smalltab li a{
            text-decoration : none;
        }
        #smalltab li:hover {
            background-color : #ccccff;
        }
        #smalltab .current {
            background-color : #ccccff;
        }
    </style>

    <div align=\"center\" style=\"padding-top:5px;\"> 
    <ul id=\"smalltab\">
        <li ".($menu=='mixing'?"class='current'":" ")." onclick=\"CallAjax('mixing', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Mixing</a></li>
        <li ".($menu=='packaging'?"class='current'":" ")." onclick=\"CallAjax('packaging', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Packaging</a></li>
        <li ".($menu=='finish_good'?"class='current'":" ")." onclick=\"CallAjax('finish_good', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Finish Good</a></li>
    </ul>
    </div> 
    
";
        return $str_menu;
} 

mysql_close($con);
?>
                                     
<!--

<li ".($menu=='mixing'?"class='current'":" ")." onclick=\"CallAjax('mixing', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Mixing</a></li>
        <li ".($menu=='mixing_wip'?"class='current'":" ")." onclick=\"CallAjax('mixing_wip', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Mixing WIP</a></li>
        <li ".($menu=='transfer_wip'?"class='current'":" ")." onclick=\"CallAjax('transfer_wip', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Transfer WIP</a></li>
        <li ".($menu=='packaging'?"class='current'":" ")." onclick=\"CallAjax('packaging', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Packaging</a></li>
        <li ".($menu=='finish_good'?"class='current'":" ")." onclick=\"CallAjax('finish_good', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Finish Good</a></li>
        <li ".($menu=='sisa_wip'?"class='current'":" ")." onclick=\"CallAjax('sisa_wip', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Sisa WIP</a></li>

-->