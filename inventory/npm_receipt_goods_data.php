<?php
include("header.php");
                               
$modul            = "RECEIPT GOODS";
$file_current     = "npm_receipt_goods.php";
$file_name        = "npm_receipt_goods_data.php";

if($ses_login=="hendri1003" || $ses_login=="febri0202" || $ses_login=="trisno1402" || $ses_login=="henny2905" || $ses_login=="sari1608")
{
    $finance = true;
}
else
{
    $finance = false;
}


function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}

     
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
		$search_keyword        = trim($_GET["search_keyword"]);
        $search_supplier       = trim($_GET["search_supplier"]);
        $search_gudang         = trim($_GET["search_gudang"]);
		$v_date_from           = trim($_GET["v_date_from"]);
		$v_date_to             = trim($_GET["v_date_to"]);
		$v_NoDokumen_curr      = trim($_GET["v_NoDokumen_curr"]);
        //echo $v_NoDokumen_curr;               
		
		$where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "trans_terima_header.NoDokumen";    
            $arr_keyword[1] = "trans_terima_header.PoNo";    
            $arr_keyword[2] = "trans_terima_header.NoSuratJalan";    
            $arr_keyword[3] = "trans_terima_header.Keterangan";      
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        $where .= " AND trans_terima_header.TglDOkumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";
        
        if($search_supplier)
        {
            $where .= " AND trans_terima_header.KdSupplier = '".$search_supplier."'";
        }
        
        if($search_gudang)
        {
            $where .= " AND trans_terima_header.KdGudang = '".$search_gudang."'";
        }
        
        // select query all
        {
            $counter = 1;
			$q="
				SELECT 
				  ".$db["master"].".trans_terima_header.NoDokumen,
				  ".$db["master"].".trans_terima_header.TglDokumen,
                  ".$db["master"].".trans_terima_header.PoNo,
				  ".$db["master"].".supplier.KdSupplier,
				  ".$db["master"].".supplier.Nama AS NamaSupplier,
				  ".$db["master"].".gudang.KdGudang,
				  ".$db["master"].".gudang.Keterangan AS NamaGudang,
				  ".$db["master"].".trans_terima_header.currencycode,
				  ".$db["master"].".trans_terima_header.Total,
				  ".$db["master"].".trans_terima_header.status
				FROM
				  ".$db["master"].".trans_terima_header 
				  INNER JOIN ".$db["master"].".supplier 
				    ON ".$db["master"].".trans_terima_header.KdSupplier = ".$db["master"].".supplier.KdSupplier 
				  INNER JOIN ".$db["master"].".gudang 
				    ON ".$db["master"].".trans_terima_header.KdGudang = ".$db["master"].".gudang.KdGudang 
				WHERE 1 
					".$where."
				ORDER BY 
					".$db["master"].".trans_terima_header.TglDokumen DESC,
					".$db["master"].".trans_terima_header.NoDokumen ASC
			";
            //echo $q;
			$sql=mysql_query($q);
			while($row=mysql_fetch_array($sql))
			{ 
				list(
					$NoDokumen,
					$TglDokumen,
                    $PoNo,
					$KdSupplier,
					$NamaSupplier,
					$KdGudang,
					$NamaGudang,
					$currencycode,
					$Total,
					$status
                )=$row;
				
				$arr_data["list_data"][$counter]=$counter;
				$arr_data["data_NoDokumen"][$counter]=$NoDokumen;
				$arr_data["data_TglDokumen"][$counter]=$TglDokumen;
                $arr_data["data_PoNo"][$counter]=$PoNo;
				$arr_data["data_KdSupplier"][$counter]=$KdSupplier;
				$arr_data["data_NamaSupplier"][$counter]=$NamaSupplier;
				$arr_data["data_KdGudang"][$counter]=$KdGudang;
				$arr_data["data_NamaGudang"][$counter]=$NamaGudang;
				$arr_data["data_currencycode"][$counter]=$currencycode;
				$arr_data["data_Total"][$counter]=$Total;
				$arr_data["data_status"][$counter]=$status;
				
				$counter++;
			}
		} 
        
        if($finance)
        {
            $colspan = 10;
        } 
        else
        {
            $colspan = 8;
        }
		
		
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_receipt_goods_print.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
    	<thead>
			<tr>
	            <th width="30"><strong><center>No</center></strong></th>
	            <th><strong><center>No Dokumen</center></strong></th>
	            <th><strong><center>Tanggal</center></strong></th>
                <th><strong><center>PoNo</center></strong></th>
	            <th><strong><center>Supplier</center></strong></th>
	            <th><strong><center>Gudang</center></strong></th>
                
                <?php 
                    if($finance)
                    {
                ?>     
	            <th width="15"><strong><center>&nbsp;</center></strong></th>      
	            <th><strong><center>Grand Total</center></strong></th>  
                <?php 
                    }
                ?>
	            <th width="80"><strong><center>Status</center></strong></th> 
	            <th width="15"><strong><center><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></center></strong></th>    
			</tr>
			
	        
		</thead>
		<tbody style="color: black;">
		
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="<?php echo $colspan; ?>" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        $photo_echo="";    
        foreach($arr_data["list_data"] as $counter => $val)
        {    
        	$nomor++;
        	
            $NoDokumen = $arr_data["data_NoDokumen"][$counter];
            $TglDokumen = $arr_data["data_TglDokumen"][$counter];
            $PoNo = $arr_data["data_PoNo"][$counter];
            $KdSupplier = $arr_data["data_KdSupplier"][$counter];
            $NamaSupplier = $arr_data["data_NamaSupplier"][$counter];
            $KdGudang = $arr_data["data_KdGudang"][$counter];
            $NamaGudang = $arr_data["data_NamaGudang"][$counter];
            $currencycode = $arr_data["data_currencycode"][$counter];
            $Total = $arr_data["data_Total"][$counter];
            $status = $arr_data["data_status"][$counter];
            
            
            
            if($status==0)
            {
                $status_echo = "Pending";
            }
            else if($status==1)
            {
                $status_echo = "Open";
            }
            else if($status==2)
            {
                $status_echo = "<font color='red'>Void</font>";
            }
            
            $style_color = "";
            if($v_NoDokumen_curr==$NoDokumen)
            {
                $style_color = "background: #CAFDB5";
            }
            		
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><nobr><?php echo $NoDokumen; ?></nobr></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo format_show_date($TglDokumen); ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><nobr><?php echo $PoNo; ?></nobr></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $NamaSupplier; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $NamaGudang; ?></td>  
                
                <?php 
                    if($finance)
                    {
                ?>
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $currencycode; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="right" style=""><?php echo format_number($Total,2); ?></td>  
                <?php 
                    }
                ?>
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $status_echo; ?></td>
                <td align="center">
                    &nbsp;
                    
                    <?php 
                        if($status==1)
                        {
                    ?>
                    <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $NoDokumen; ?>">
                    <?php 
                        }
                    ?>
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot>
            <tr>
                <td colspan="<?php echo $colspan; ?>" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Print">Print</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
	</table> 
	</form>
	</div>  
   	<?php  
    }

	else if($ajax=="add_data")
	{
        $q = "
            SELECT
                ".$db["master"].".aplikasi.TglTrans
            FROM
                ".$db["master"].".aplikasi
            WHERE
                1
        ";
        $qry_aplikasi = mysql_query($q);
        $row_aplikasi = mysql_fetch_array($qry_aplikasi);
        list($TglTrans) = $row_aplikasi;
        
		$q = "
            SELECT
                ".$db["master"].".supplier.KdSupplier,
                ".$db["master"].".supplier.Nama
            FROM
                ".$db["master"].".supplier
            WHERE
                1
            ORDER BY
                ".$db["master"].".supplier.Nama ASC
        ";
        $qry_supplier = mysql_query($q);
        while($row_supplier = mysql_fetch_array($qry_supplier))
        { 
		    list($KdSupplier, $NamaSupplier) = $row_supplier;    
            
            $arr_data["list_supplier"][$KdSupplier] = $KdSupplier;
            $arr_data["NamaSupplier"][$KdSupplier] = $NamaSupplier;
            
        }
        
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
        ";
        $qry_gudang = mysql_query($q);
        while($row_gudang = mysql_fetch_array($qry_gudang))
        { 
            list($KdGudang, $NamaGudang) = $row_gudang;    
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
            
        }
        
        
        ?>

        <div class="col-md-12" align="left">
        
        	<ol class="breadcrumb">
				<li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">Tanggal</td>
                <td width="40%">
                    <input type="text" class="form-control-new datepicker" name="v_TglDokumen" id="v_TglDokumen" size="10" maxlength="10" value="" placeholder="dd/mm/yyyy">
                    <!-- <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglDokumen, 'dd/mm/yyyy');"> -->

                    <script>
                        active_monthGL("v_TglDokumen");
                    </script>
                </td>
                
                <!--<td class="title_table" width="200">Gudang</td>
                <td>
                    <select name="v_KdGudang" id="v_KdGudang" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                $selected = "";
                                if($arr_curr["KdGudang"]==$KdGudang)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>-->
                
                <td class="title_table">Gudang</td>
                <td>
                    <input type="hidden" class="form-control-new" readonly="readonly" size="20"  name="v_KdGudang" id="v_KdGudang">
                    <input type="text" class="form-control-new" readonly="readonly" size="35" name="NamaGudang" id="NamaGudang" onblur="cek('<?php echo $_SERVER[HTTP_HOST];?>')">
               </td>
                
            </tr>  

            <tr>
                <td class="title_table">Supplier</td>
                <td>
                    <select name="v_KdSupplier" id="v_KdSupplier" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_supplier', this.value)">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_supplier"] as $KdSupplier=>$val)
                            {
                                $NamaSupplier = $arr_data["NamaSupplier"][$KdSupplier];
                                ?>
                                <option value="<?php echo $KdSupplier; ?>"><?php echo $NamaSupplier; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            
                
               
               <td class="title_table">No Surat Jalan</td>
                <td><input type="text" class="form-control-new" name="v_NoSuratJalan" id="v_NoSuratJalan" style="width: 200px;" value=""> </td>
                 
            </tr>
                
            <tr>
                <td class="title_table">PoNo</td>
                <td>
                    <input type="text" class="form-control-new" readonly="readonly" size="20" name="v_PoNo" id="v_PoNo">
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_search_po()">&nbsp;&nbsp;<i class="entypo-search"></i></button>
                </td>
                
               <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_Keterangan" id="v_Keterangan" style="width: 200px;" value=""> 
                </td> 
            </tr>
                
           
            
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
                
        	</table>  
        	</form>
        </div>
        <?php
		
	}else if($ajax=="cekTanggalGL"){
        $q = "
            SELECT
                ".$db["master"].".aplikasi.PeriodeGL
            FROM
                ".$db["master"].".aplikasi
            WHERE
                1
        ";
        $qry_aplikasi = mysql_query($q);
        $row_aplikasi = mysql_fetch_array($qry_aplikasi);
        list($PeriodeGL) = $row_aplikasi;

        echo $PeriodeGL;
    }else if($ajax=="edit_data")
    {                                      
		$v_NoDokumen = $_GET["v_NoDokumen"];  
        
        $q = "
                SELECT
                    *
                FROM 
                    ".$db["master"].".trans_terima_header
                WHERE
                    1
                    AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'
                LIMIT
                    0,1
        ";     
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
        //echo "<pre>";print_r($arr_curr);echo "</pre>";
        
        
        $q = "
            SELECT
                ".$db["master"].".supplier.KdSupplier,
                ".$db["master"].".supplier.Nama
            FROM
                ".$db["master"].".supplier
            WHERE
                1
            ORDER BY
                ".$db["master"].".supplier.Nama ASC
        ";
        $qry_supplier = mysql_query($q);
        while($row_supplier = mysql_fetch_array($qry_supplier))
        { 
            list($KdSupplier, $NamaSupplier) = $row_supplier;    
            
            $arr_data["list_supplier"][$KdSupplier] = $KdSupplier;
            $arr_data["NamaSupplier"][$KdSupplier] = $NamaSupplier;
        }
        
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
        ";
        $qry_gudang = mysql_query($q);
        while($row_gudang = mysql_fetch_array($qry_gudang))
        { 
            list($KdGudang, $NamaGudang) = $row_gudang;    
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
            
        }
        
        $q = "
                SELECT
                    ".$db["master"].".trans_order_barang_detail.Sid,    
                    ".$db["master"].".trans_order_barang_detail.PCode,
                    ".$db["master"].".trans_order_barang_detail.Qty,
                    ".$db["master"].".trans_order_barang_detail.Satuan,
                    ".$db["master"].".trans_order_barang_detail.Harga,
                    ".$db["master"].".trans_order_barang_detail.Disc1,
                    ".$db["master"].".trans_order_barang_detail.Disc2,
                    ".$db["master"].".trans_order_barang_detail.Potongan,
                    ".$db["master"].".trans_order_barang_detail.Jumlah,
                    ".$db["master"].".trans_order_barang_detail.PPn,
                    ".$db["master"].".trans_order_barang_detail.Total
                FROM
                    ".$db["master"].".trans_order_barang_detail
                WHERE
                    1
                    AND ".$db["master"].".trans_order_barang_detail.NoDokumen = '".$arr_curr["PoNo"]."'
                ORDER BY
                    ".$db["master"].".trans_order_barang_detail.Sid ASC
        ";
        $qry_po = mysql_query($q);
        while($row_po = mysql_fetch_array($qry_po))
        { 
            list(
                $Sid,    
                $PCode,
                $Qty,
                $Satuan,
                $Harga,
                $Disc1,
                $Disc2,
                $Potongan,
                $Jumlah,
                $PPn,
                $Total 
            ) = $row_po;    
            
            $arr_data["list_data_po"][$Sid] = $Sid;
            
            $arr_data["data_po_PCode"][$Sid] = $PCode;
            $arr_data["data_po_Qty"][$Sid] = $Qty;
            $arr_data["data_po_Satuan"][$Sid] = $Satuan;
            $arr_data["data_po_Harga"][$Sid] = $Harga;
            $arr_data["data_po_Disc1"][$Sid] = $Disc1;
            $arr_data["data_po_Disc2"][$Sid] = $Disc2;
            $arr_data["data_po_Potongan"][$Sid] = $Potongan;
            $arr_data["data_po_Jumlah"][$Sid] = $Jumlah;
            $arr_data["data_po_PPn"][$Sid] = $PPn;
            $arr_data["data_po_Total"][$Sid] = $Total;
            
            $arr_data["list_PCode"][$PCode] = $PCode;
        }
        
        if(count($arr_data["list_PCode"])*1>0)
        {
            $where_pcode = where_array($arr_data["list_PCode"], "PCode", "in");
            
            $q = "
                    SELECT
                        ".$db["master"].".masterbarang.PCode,
                        ".$db["master"].".masterbarang.NamaLengkap,
                        ".$db["master"].".masterbarang.toleransi_terima
                    FROM
                        ".$db["master"].".masterbarang
                    WHERE
                        1
                        ".$where_pcode."
                    ORDER BY
                        ".$db["master"].".masterbarang.PCode ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            { 
                list($PCode, $NamaLengkap, $toleransi_terima) = $row; 
                
                $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
                $arr_data["toleransi_terima"][$PCode] = $toleransi_terima;
            }
            
        }
        
        $q = "
                SELECT
                    ".$db["master"].".satuan.KdSatuan,    
                    ".$db["master"].".satuan.NamaSatuan
                FROM
                    ".$db["master"].".satuan
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".satuan.KdSatuan ASC
        ";
        $qry_pr = mysql_query($q);
        while($row_pr = mysql_fetch_array($qry_pr))
        { 
            list($KdSatuan, $NamaSatuan) = $row_pr;    
            
            $arr_data["list_satuan"][$KdSatuan] = $KdSatuan;
        }
        
        $q = "
                SELECT
                    ".$db["master"].".trans_terima_detail.Sid,    
                    ".$db["master"].".trans_terima_detail.PCode,
                    ".$db["master"].".trans_terima_detail.Qty,
                    ".$db["master"].".trans_terima_detail.Satuan,
                    ".$db["master"].".trans_terima_detail.Harga,
                    ".$db["master"].".trans_terima_detail.Disc1,
                    ".$db["master"].".trans_terima_detail.Disc2,
                    ".$db["master"].".trans_terima_detail.Potongan,
                    ".$db["master"].".trans_terima_detail.Jumlah,
                    ".$db["master"].".trans_terima_detail.PPn,
                    ".$db["master"].".trans_terima_detail.Total
                FROM
                    ".$db["master"].".trans_terima_detail
                WHERE
                    1
                    AND ".$db["master"].".trans_terima_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
                ORDER BY
                    ".$db["master"].".trans_terima_detail.Sid ASC
        ";
        $qry_rg = mysql_query($q);
        while($row_rg = mysql_fetch_array($qry_rg))
        { 
            list(
                $Sid,    
                $PCode,
                $Qty,
                $Satuan,
                $Harga,
                $Disc1,
                $Disc2,
                $Potongan,
                $Jumlah,
                $PPn,
                $Total 
            ) = $row_rg;    
            
            $arr_data["data_rg_Qty"][$PCode] = $Qty;
            $arr_data["data_rg_Satuan"][$PCode] = $Satuan;
            $arr_data["data_rg_Harga"][$PCode] = $Harga;
            $arr_data["data_rg_Disc1"][$PCode] = $Disc1;
            $arr_data["data_rg_Disc2"][$PCode] = $Disc2;
            $arr_data["data_rg_Potongan"][$PCode] = $Potongan;
            $arr_data["data_rg_Jumlah"][$PCode] = $Jumlah;
            $arr_data["data_rg_PPn"][$PCode] = $PPn;
            $arr_data["data_rg_Total"][$PCode] = $Total;
            
            $counter++;
        }
            
		
        ?> 
        <div class="col-md-12" align="left">
        	
        	<ol class="breadcrumb">
				<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			                                           
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value="">    
        <input type="hidden" name="v_NoDokumen" id="v_NoDokumen" value="<?php echo $arr_curr["NoDokumen"]; ?>">   
        <input type="hidden" name="v_PoNo_old" id="v_PoNo_old" value="<?php echo $arr_curr["PoNo"]; ?>">   
        
        
        <table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="200">No Dokumen</td>
                <td colspan="3" style="color: black; font-weight: bold"><?php echo $arr_curr["NoDokumen"]; ?></td>
            </tr>
        
            <tr>
                <td class="title_table" width="200">Tanggal</td>
                <td width="40%">
                    <input type="text" class="form-control-new datepicker" name="v_TglDokumen" id="v_TglDokumen" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["TglDokumen"]); ?>">
                    <!-- <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglDokumen, 'dd/mm/yyyy');"> -->
                    <script>
                        active_monthGL("v_TglDokumen");
                    </script>
                </td>
                
                <td class="title_table" width="200">Gudang</td>
                <td>
                    
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                $selected = "";
                                if($arr_curr["KdGudang"]==$KdGudang)
                                {?>
                                 <input type="hidden" class="form-control-new" value="<?php echo $KdGudang;?>" readonly="readonly" size="20" name="v_KdGudang" id="v_KdGudang">
                                 <input type="text" class="form-control-new" value="<?php echo $NamaGudang;?>" readonly="readonly" size="35" name="NamaGudang" id="NamaGudang">
                                <?php }
                                ?>
                              <?php
                            }
                        ?>
                    
                </td>
                
                
                
            </tr>  

            <tr>
                <td class="title_table">Supplier</td>
                <td>
                    <select name="v_KdSupplier" id="v_KdSupplier" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_supplier', this.value)">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_supplier"] as $KdSupplier=>$val)
                            {
                                $NamaSupplier = $arr_data["NamaSupplier"][$KdSupplier];
                                
                                $selected = "";
                                if($arr_curr["KdSupplier"]==$KdSupplier)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdSupplier; ?>"><?php echo $NamaSupplier; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            
               <td class="title_table">No Surat Jalan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_NoSuratJalan" id="v_NoSuratJalan" style="width: 200px;" value="<?php echo $arr_curr["NoSuratJalan"]; ?>"> 
                </td> 
               
                
            </tr>
                
            <tr>
                <td class="title_table">PoNo</td>
                <td>
                    <input type="text" class="form-control-new" readonly="readonly" size="20" name="v_PoNo" id="v_PoNo" value="<?php echo $arr_curr["PoNo"]; ?>">
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_search_po()">&nbsp;&nbsp;<i class="entypo-search"></i></button>
                </td>
                
                <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_Keterangan" id="v_Keterangan" style="width: 200px;" value="<?php echo $arr_curr["Keterangan"]; ?>"> 
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Status</td>
                <td>
                   <?php 
                    if($arr_curr["Status"]==0)
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option value="0">Pending</option>    
                            <option value="1">Open</option>    
                        </select>
                        <?php
                    }
                    else if($arr_curr["Status"]==1)
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option value="1">Open</option>     
                        </select>
                        <?php
                    }
                    else if($arr_curr["Status"]==2)
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option value="2">Void</option>    
                        </select>
                        <?php
                    }
                    else
                    {
                      if($arr_curr['FlagKonfirmasi']=="T"){
					  	
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option <?php if($arr_curr["Status"]==1) echo "selected='selected'"; ?> value="1">Open</option>    
                            <option <?php if($arr_curr["Status"]==2) echo "selected='selected'"; ?> value="2">Void</option>    
                        </select>
                        <?php 
                        }
                           
                    }
                   ?>
                </td>
                
                <td class="title_table" width="150">&nbsp;</td>
                <td>&nbsp;</td> 
            </tr>
                
               
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                    <?php if($arr_curr['FlagKonfirmasi']=="T"){ ?>
                	<button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan.<i class="entypo-check"></i></button>
                	<?php } ?>
                    <?php 
                        if($arr_curr["Status"]==0 || $arr_curr["Status"]==1)
                        {
                    ?>
                    <?php if($arr_curr['FlagKonfirmasi']=="T"){ ?>
                	<button type="submit" name="btn_delete" id="btn_delete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_delete('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Void">Void<i class="entypo-trash"></i></button>
                    <?php } ?>
                    <?php 
                        }
                        
                        if($arr_curr["Status"]==2)
                        {
                            ?>
                            <?php if($arr_curr['FlagKonfirmasi']=="T"){ ?>
                                <button type="submit" name="btn_unvoid" id="btn_unvoid" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_unvoid('<?php echo $arr_curr["NoDokumen"]; ?>');" value="UnVoid">UnVoid<i class="entypo-trash"></i></button>
                            <?php } ?>
                            <?php
                        }
                    ?>
                    
                    
               </td>
            </tr> 
            
            
            <tr>
                <td colspan="100%">
                    <table width="100%" class="table table-bordered responsive">
                        <thead>
                            <tr>
                                <td width="30">No</td>    
                                <td>PCode</td>    
                                <td>Nama Barang</td> 
                                <td>Satuan RG</td>   
                                <td align="right">Qty PO</td>    
                                <td align="right">Qty</td>   
    
                                <?php 
                                    if($finance)
                                    {
                                ?>    
                                <td>Harga</td>    
                                <td align="right">Disc (%)</td>    
                                <td align="right">Potongan (<?php echo $arr_curr["currencycode"]; ?>)</td>    
                                <td align="right">Subtotal</td>    
                                <?php 
                                    }
                                ?>
                            </tr>
                        </thead>
                        
                        <tbody style="color: black;">
                            <?php
                                $no = 1;
                                $arr_data["total"] = 0; 
                                foreach($arr_data["list_data_po"] as $Sid=>$val)
                                {
                                    $PCode = $arr_data["data_po_PCode"][$Sid];
                                    $po_Qty = $arr_data["data_po_Qty"][$Sid];
                                    $po_Satuan = $arr_data["data_po_Satuan"][$Sid];
                                    $po_Harga = $arr_data["data_po_Harga"][$Sid];
                                    $po_Disc1 = $arr_data["data_po_Disc1"][$Sid];
                                    $po_Disc2 = $arr_data["data_po_Disc2"][$Sid];
                                    $po_Potongan = $arr_data["data_po_Potongan"][$Sid];
                                    $po_PPn = $arr_data["data_po_PPn"][$Sid];
                                    $po_Total = $arr_data["data_po_Total"][$Sid];
									
									$NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                                    $toleransi_terima = $arr_data["toleransi_terima"][$PCode];
                                    
                                    $rg_Qty = $arr_data["data_rg_Qty"][$PCode];
                                    $rg_Satuan = $arr_data["data_rg_Satuan"][$PCode];
                                    $rg_Harga = $arr_data["data_rg_Harga"][$PCode];
                                    $rg_Disc1 = $arr_data["data_rg_Disc1"][$PCode];
                                    $rg_Disc2 = $arr_data["data_rg_Disc2"][$PCode];
                                    $rg_Potongan = $arr_data["data_rg_Potongan"][$PCode];
                                    $rg_Jumlah = $arr_data["data_rg_Jumlah"][$PCode];
                                    $rg_PPn = $arr_data["data_rg_PPn"][$PCode];
                                    $rg_Total = $arr_data["data_rg_Total"][$PCode];
                                    
                                    $subtotal  = ($rg_Qty*$rg_Harga);
                                    $disc1_val = $subtotal*($rg_Disc1/100);
                                    $disc2_val = ($subtotal - $disc1_val)*($rg_Disc2/100);
                                    $subtotal_Total = $subtotal - $disc1_val - $disc2_val - $rg_Potongan;
                            ?>
                            <tr>
                                <td>
                                    <?php echo $no; ?>
                                    <input type="hidden" name="v_sid[]" value="<?php echo $Sid; ?>">
                                    <input type="hidden" name="v_PCode_<?php echo $Sid; ?>" value="<?php echo $PCode; ?>">
                                    
                                    <input type="hidden" name="v_Harga_po_<?php echo $Sid; ?>" value="<?php echo $po_Harga; ?>">
                                    <input type="hidden" name="v_Disc1_po_<?php echo $Sid; ?>" value="<?php echo $po_Disc1; ?>">
                                    <input type="hidden" name="v_Disc2_po_<?php echo $Sid; ?>" value="<?php echo $po_Disc2; ?>">
                                    <input type="hidden" name="v_Potongan_po_<?php echo $Sid; ?>" value="<?php echo $po_Potongan; ?>">
                                    <input type="hidden" name="v_toleransi_terima_<?php echo $Sid; ?>" value="<?php echo $toleransi_terima; ?>">
                                </td>
                                <td><?php echo $PCode; ?></td>
                                <td><?php echo $NamaLengkap; ?></td>
                                <td>
                                    <!--<select name="v_Satuan_<?php echo $Sid; ?>" id="v_Satuan_<?php echo $Sid; ?>" class="form-control-new">-->
                                        <?php 
										
											$q="
												SELECT 
													  a.`SatuanSt` AS KdSatuan, c.`NamaSatuan` 
													FROM
													  ".$db["master"].".masterbarang a INNER JOIN ".$db["master"].".satuan c ON a.`SatuanSt`=c.`KdSatuan`
													WHERE a.`PCode` = '".$PCode."' 
													UNION
													SELECT 
													  b.Satuan_From, d.`NamaSatuan`
													FROM
													  ".$db["master"].".konversi b INNER JOIN ".$db["master"].".satuan d ON b.`Satuan_From`=d.`KdSatuan`
													WHERE b.PCode = '".$PCode."' ;
											";
											
											$qry_pr = mysql_query($q);
											while($row_pr = mysql_fetch_array($qry_pr))
											{ 
												list($KdSatuan, $NamaSatuan) = $row_pr;    
												
												$arr_data["list_satuan_new"][$KdSatuan] = $KdSatuan;
												$arr_data["namasatuan"][$KdSatuan] = $NamaSatuan;
											}
										
                                            foreach($arr_data["list_satuan_new"] as $KdSatuan=>$val)
                                            {
                                                $NamaSatuan = $arr_data["namasatuan"][$KdSatuan];
												//$selected = "";
                                                if($po_Satuan==$KdSatuan)
                                                {
                                                	echo $NamaSatuan;?>
                                                    <!--$selected = "selected='selected'"; --> 
                                                    
                                                    <input type="hidden" class="form-control-new" value="<?php echo $KdSatuan;?>" readonly="readonly" name="v_Satuan_<?php echo $Sid; ?>" id="v_Satuan_<?php echo $Sid; ?>">  
                                                <?php }
                                                ?>
                                                <!--<option <?php echo $selected; ?> value="<?php echo $KdSatuan; ?>"><?php echo $NamaSatuan; ?></option>-->
                                                <?php
                                            }
                                        ?>
                                    <!--</select>-->
                                </td>
                                <td align="right"><?php echo format_number($po_Qty,2)." ".$po_Satuan; ?></td>
                                <td align="right"><input style="text-align: right; width: 60px;" type="text" class="form-control-new" name="v_Qty_<?php echo $Sid; ?>" id="v_Qty_<?php echo $Sid; ?>" value="<?php echo format_number($rg_Qty, 2); ?>" onblur="toFormat2('v_Qty_<?php echo $Sid; ?>')"></td>
                                
                                
                                <?php 
                                    if($finance)
                                    {
                                ?> 
                                <td align="right"><input readonly="readonly" style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_Harga_<?php echo $Sid; ?>" id="v_Harga_<?php echo $Sid; ?>" value="<?php echo format_number($rg_Harga, 2); ?>" onblur="toFormat2('v_Harga_<?php echo $Sid; ?>')"></td>
                                <td align="right">
                                    <input readonly="readonly" style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc1_<?php echo $Sid; ?>" id="v_Disc1_<?php echo $Sid; ?>" value="<?php echo format_number($rg_Disc1, 2); ?>" onblur="toFormat2('v_Disc1_<?php echo $Sid; ?>')">
                                    <input readonly="readonly" style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc2_<?php echo $Sid; ?>" id="v_Disc2_<?php echo $Sid; ?>" value="<?php echo format_number($rg_Disc2, 2); ?>" onblur="toFormat2('v_Disc2_<?php echo $Sid; ?>')">
                                </td>
                                <td align="right"><input readonly="readonly" style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_Potongan_<?php echo $Sid; ?>" id="v_Potongan_<?php echo $Sid; ?>" value="<?php echo format_number($rg_Potongan, 2); ?>" onblur="toFormat2('v_Potongan_<?php echo $Sid; ?>')"></td>
                                <td align="right"><?php echo format_number($subtotal_Total,2); ?></td>
                                <?php 
                                    }
                                ?>
                            </tr>
                            <?php 
                                    $arr_data["total"] += $subtotal_Total;
                                    $no++;
                                }
                                
                                $disc_val = $arr_data["total"]*($arr_curr["Disc"]/100);
                                $ppn_val  = ($arr_data["total"] - $disc_val) * ($arr_curr["PPn"]/100); 
                                $grand_total = ($arr_data["total"] - $disc_val) + $ppn_val;
                            ?>
                        </tbody>
                        
                      
                      <?php 
                            if($finance)
                            {
                        ?> 
                            <tr style="color: black; font-weight: bold;">
                                <td colspan="8" rowspan="4">
                                    Terbilang : <?php echo ucwords(Terbilang($grand_total)); ?> 
                                </td>
                                <td style="text-align: right;">
                                TOTAL <?php echo $arr_curr["currencycode"]; ?>
                                <input type="hidden" name="v_Jumlah" id="v_Jumlah" value="<?php echo $arr_data["total"]; ?>">
                                </td>
                                <td style="text-align: right;"><?php echo format_number($arr_data["total"], 2); ?></td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC <input readonly="readonly" style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" value="<?php echo format_number($arr_curr["Disc"], 2); ?>" onblur="toFormat2('v_DiscHarga')"> (%) <?php echo $arr_curr["currencycode"]; ?></td>
                                <td style="text-align: right;"><?php echo format_number($disc_val, 2); ?></td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN <input readonly="readonly" style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn" id="v_PPn" value="<?php echo format_number($arr_curr["PPn"], 2); ?>" onblur="toFormat2('v_PPn')"> (%) <?php echo $arr_curr["currencycode"]; ?></td>
                                <td style="text-align: right;">
                                    <input type="hidden" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo $ppn_val; ?>">
                                    <?php echo format_number($ppn_val, 2); ?>
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL <?php echo $arr_curr["currencycode"]; ?>
                                    <input type="hidden" name="v_Total" id="v_Total" value="<?php echo $grand_total; ?>">
                                </td>
                                <td style="text-align: right;"><?php echo format_number($grand_total, 2); ?></td>
                            </tr>
                      <?php 
                            }
                      ?>
                    </table>
                
                </td>
            </tr>
            <tr>
                <td>Add User:</td><td colspan='3'><?php echo $arr_curr['AddUser'];?></td>
            </tr>
            <tr>
                <td>Add Date:</td><td colspan='3'><?php echo date('d-m-Y',strtotime($arr_curr['AddDate']));?></td>
            </tr>
            <tr>
                <td>Edit User:</td><td colspan='3'><?php echo $arr_curr['EditUser'];?></td> 
                <!-- test -->
            </tr>
            <tr>
                <td>Edit Date:</td><td colspan='3'><?php echo date('d-m-Y',strtotime($arr_curr['EditDate']));?></td>
            </tr>
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    else if($ajax=="ajax_supplier")
    {
        $v_KdSupplier = $_GET["v_KdSupplier"];
        
        $q = "
                SELECT
                    ".$db["master"].".supplier.TOP
                FROM
                    ".$db["master"].".supplier
                WHERE
                    1
                    AND ".$db["master"].".supplier.KdSupplier = '".$v_KdSupplier."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($TOP) = $row;
        
        echo $TOP;
    }
    
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                
                if(!isset($_POST["v_TglDokumen"])){ $v_TglDokumen = isset($_POST["v_TglDokumen"]); } else { $v_TglDokumen = $_POST["v_TglDokumen"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_PoNo"])){ $v_PoNo = isset($_POST["v_PoNo"]); } else { $v_PoNo = $_POST["v_PoNo"]; } 
                if(!isset($_POST["v_NoSuratJalan"])){ $v_NoSuratJalan = isset($_POST["v_NoSuratJalan"]); } else { $v_NoSuratJalan = $_POST["v_NoSuratJalan"]; } 
                if(!isset($_POST["v_KdSupplier"])){ $v_KdSupplier = isset($_POST["v_KdSupplier"]); } else { $v_KdSupplier = $_POST["v_KdSupplier"]; }          
                if(!isset($_POST["v_Keterangan"])){ $v_Keterangan = isset($_POST["v_Keterangan"]); } else { $v_Keterangan = $_POST["v_Keterangan"]; } 

				$v_TglDokumen    = format_save_date($v_TglDokumen);
				$v_KdGudang      = save_char($v_KdGudang);
				$v_PoNo          = save_char($v_PoNo);
                $v_NoSuratJalan  = save_char($v_NoSuratJalan);
				$v_KdSupplier    = save_char($v_KdSupplier);
                $v_Keterangan    = save_char($v_Keterangan);
                    
                if($v_KdSupplier=="")
                {      
                    $msg = "Supplier harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_KdGudang=="")
                {      
                	$msg = "Gudang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_PoNo=="")
                {   
                	$msg = "PoNo harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                
                // cek data po
                $q = "
                        SELECT 
                            ".$db["master"].".trans_order_barang_header.NoDokumen,
                            ".$db["master"].".trans_order_barang_header.currencycode,
                            ".$db["master"].".trans_order_barang_header.kurs,
                            ".$db["master"].".trans_order_barang_header.PPn,
                            ".$db["master"].".trans_order_barang_header.DiscHarga
                        FROM
                            ".$db["master"].".trans_order_barang_header
                        WHERE
                            1
                            AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_PoNo."'
                        LIMIT
                            0,1
                ";
                $qry = mysql_query($q);
                $row = mysql_fetch_array($qry);
                list($PoNo, $kurs,$currencycode, $PPn, $DiscHarga) = $row;
                if($PoNo=="")
                {   
                    $msg = "PoNo ".$v_PoNo." tidak ditemukan";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                
                  
                // RG00002-05-16
                
                $arr_date = explode("-", $v_TglDokumen);
                $mm   = $arr_date[1];
                $yyyy = $arr_date[0];
                
	            $NoDokumen = get_code_counter($db["master"], "trans_terima_header","NoDokumen", "RG", $mm, $yyyy);
	            
	            // insert po
	            {
		            $q = "
			            INSERT INTO
			                ".$db["master"].".trans_terima_header
			            SET
			                NoDokumen = '".$NoDokumen."',
			                TglDokumen = '".$v_TglDokumen."',
			                KdGudang = '".$v_KdGudang."',
			                PoNo = '".$v_PoNo."',
                            NoSuratJalan = '".$v_NoSuratJalan."',
                            Keterangan = '".$v_Keterangan."',
			                KdSupplier = '".$v_KdSupplier."',
			                currencycode = '".$currencycode."',
			                kurs = '".$kurs."',
                            PPn = '".$PPn."',
                            Disc = '".$DiscHarga."',
                            Rate = '1',
                            Status = '0',
                            AddDate = NOW(),
			                AddUser = '".$ses_login."',
			                EditDate = NOW(),
			                EditUser = '".$ses_login."'
			        ";   
			        if(!mysql_query($q))
			        {
			            $msg = "Gagal menyimpan trans_terima_header";
			            echo "<script>alert('".$msg."');</script>";
			            die(); 
			        }	
				}
				
				$msg = "Berhasil menyimpan ".$NoDokumen;
	            echo "<script>alert('".$msg."');</script>"; 
				echo "<script>parent.CallAjaxForm('search','".$NoDokumen."');</script>";         	
	            
	            die();
				   
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {                                                                                                      
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }    
                if(!isset($_POST["v_NoDokumen"])){ $v_NoDokumen = isset($_POST["v_NoDokumen"]); } else { $v_NoDokumen = $_POST["v_NoDokumen"]; }  
                
                if(!isset($_POST["v_TglDokumen"])){ $v_TglDokumen = isset($_POST["v_TglDokumen"]); } else { $v_TglDokumen = $_POST["v_TglDokumen"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_PoNo"])){ $v_PoNo = isset($_POST["v_PoNo"]); } else { $v_PoNo = $_POST["v_PoNo"]; } 
                if(!isset($_POST["v_PoNo_old"])){ $v_PoNo_old = isset($_POST["v_PoNo_old"]); } else { $v_PoNo_old = $_POST["v_PoNo_old"]; } 
                if(!isset($_POST["v_NoSuratJalan"])){ $v_NoSuratJalan = isset($_POST["v_NoSuratJalan"]); } else { $v_NoSuratJalan = $_POST["v_NoSuratJalan"]; } 
                if(!isset($_POST["v_KdSupplier"])){ $v_KdSupplier = isset($_POST["v_KdSupplier"]); } else { $v_KdSupplier = $_POST["v_KdSupplier"]; }          
                if(!isset($_POST["v_Keterangan"])){ $v_Keterangan = isset($_POST["v_Keterangan"]); } else { $v_Keterangan = $_POST["v_Keterangan"]; } 
                if(!isset($_POST["v_Status"])){ $v_Status = isset($_POST["v_Status"]); } else { $v_Status = $_POST["v_Status"]; } 
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }  
                
                if(!isset($_POST["v_sid"])){ $v_sid = isset($_POST["v_sid"]); } else { $v_sid = $_POST["v_sid"]; } 
                
                $v_NoDokumen     = save_char($v_NoDokumen);
                $v_TglDokumen    = format_save_date($v_TglDokumen);
                $v_KdGudang      = save_char($v_KdGudang);
                $v_PoNo          = save_char($v_PoNo);
                $v_PoNo_old      = save_char($v_PoNo_old);
                $v_NoSuratJalan  = save_char($v_NoSuratJalan);
                $v_KdSupplier    = save_char($v_KdSupplier);
                $v_Keterangan    = save_char($v_Keterangan);
                $v_Status        = save_char($v_Status);
                
                if($v_KdSupplier=="")
                {   
                    $msg = "Supplier harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }   
                else if($v_PoNo=="")
                {   
                    $msg = "PoNo harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }  
                else if($v_KdGudang=="")
                {      
                    $msg = "Gudang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                
            
                // cek data po
                $q = "
                        SELECT 
                            ".$db["master"].".trans_order_barang_header.NoDokumen,
                            ".$db["master"].".trans_order_barang_header.currencycode,
                            ".$db["master"].".trans_order_barang_header.kurs,
                            ".$db["master"].".trans_order_barang_header.PPn,
                            ".$db["master"].".trans_order_barang_header.DiscHarga
                        FROM
                            ".$db["master"].".trans_order_barang_header
                        WHERE
                            1
                            AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_PoNo."'
                        LIMIT
                            0,1
                ";
                $qry = mysql_query($q);
                $row = mysql_fetch_array($qry);
                list($PoNo, $kurs, $currencycode, $PPn, $DiscHarga) = $row;
                if($PoNo=="")
                {   
                    $msg = "PoNo ".$v_PoNo." tidak ditemukan";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                
                $update_po = "";    
                if($v_PoNo!=$v_PoNo_old)
                {
                    $update_po = "
                                currencycode = '".$currencycode."',
                                PPn = '".$PPn."',
                                Disc = '".$DiscHarga."',
                    ";
                }
                
				
                if($v_del==1)   
                {
                	if($btn_delete=="Void")
                	{
                		
                		$z = "
                        SELECT 
                            ".$db["master"].".trans_terima_header.FlagKonfirmasi
                        FROM
                            ".$db["master"].".trans_terima_header
                        WHERE
                            1
                            AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'
                        LIMIT
                            0,1
		                ";
		               
		                $qryz = mysql_query($z);
		                $rowz = mysql_fetch_array($qryz);
		                list($FlagKonfirmasi) = $rowz;
                		if($FlagKonfirmasi=="Y")
                		{   
		                    $msg = "Tidak Dizinkan Void, Karena sudah Konfirmasi terima.";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();
                		}else{
                				//sudah stock opname belum?
			                	$sql="SELECT MAX(DATE(a.`TglDokumen`)) AS tglapprove FROM `opname_header` a WHERE a.`KdGudang`='$v_KdGudang' AND a.`Approval_Status`='1' ;";
			                							
			                	$query = mysql_query($sql);
			                	$cek = mysql_fetch_array($query);
			                	
			                	if($cek['tglapprove'] >= $v_TglDokumen){
								        echo "<script>alert('Sudah Stock Opname.');</script>";
								}else{
									                			
										$q = "
					                            UPDATE
					                                ".$db["master"].".trans_terima_header
					                            SET     
					                                Status = '2',
				                                EditDate = NOW(),
				                                EditUser = '".$ses_login."' 
				                             WHERE 1
				                                AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'   
				                        ";
				                        //echo $q."<hr/>";                  
				                        if(!mysql_query($q))
				                        {
				                            $msg = "Gagal menyimpan trans_terima_header";
				                            echo "<script>alert('".$msg."');</script>";
				                            die(); 
				                        } 
				                        get_mutasi_stock("RG", $v_NoDokumen);
				                                                       
					                    $msg = "Berhasil Void ".$v_NoDokumen;
					                    echo "<script>alert('".$msg."');</script>"; 
					                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
					                    die();
			                    
			                    }
							}
                	
					}
				}
                else  if($v_undel==1)   
                {
                    if($btn_unvoid=="UnVoid")
                    {
                    	
                    	//sudah stock opname belum?
			                	$sql="SELECT MAX(DATE(a.`TglDokumen`)) AS tglapprove FROM `opname_header` a WHERE a.`KdGudang`='$v_KdGudang' AND a.`Approval_Status`='1' ;";
			                							
			                	$query = mysql_query($sql);
			                	$cek = mysql_fetch_array($query);
			                	
			                	if($cek['tglapprove'] >= $v_TglDokumen){
								        echo "<script>alert('Sudah Stock Opname.');</script>";
								}else{
									
			                        $q = "
			                            UPDATE
			                                ".$db["master"].".trans_terima_header
			                            SET     
			                                Status = '1',
			                                EditDate = NOW(),
			                                EditUser = '".$ses_login."' 
			                             WHERE 1
			                                AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'   
			                        ";
			                        //echo $q."<hr/>";                  
			                        if(!mysql_query($q))
			                        {
			                            $msg = "Gagal menyimpan trans_order_barang_header";
			                            echo "<script>alert('".$msg."');</script>";
			                            die(); 
			                        } 
			                        get_mutasi_stock("RG", $v_NoDokumen);
			                               
			                        $msg = "Berhasil UnVoid ".$v_NoDokumen;
			                        echo "<script>alert('".$msg."');</script>"; 
			                        echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
			                        die(); 
                        		}    
                        
                    }
                }
                else  if($v_approve==1)   
                {
                    if($btn_approve=="Approve")
                    {
                        $q = "
                            UPDATE
                                ".$db["master"].".trans_order_barang_header
                            SET     
                                Approval_By = '".$ses_login."',
                                Approval_Date = NOW(),
                                Approval_Status = '1'
                             WHERE 1
                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan trans_order_barang_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
                        $msg = "Berhasil Approve ".$v_NoDokumen;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                        die();
                    }
                }
                else  if($v_reject==1)   
                {
                    if($btn_reject=="Reject")
                    {
                        if($v_Approval_Remarks=="")
                        {
                            $msg = "Alasan Reject harus diisi...";
                            echo "<script>alert('".$msg."');</script>";
                        }
                        else
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".trans_order_barang_header
                                SET     
                                    Approval_By = '".$ses_login."',
                                    Approval_Date = NOW(),
                                    Approval_Status = '2',
                                    Approval_Remarks = '".$v_Approval_Remarks."'
                                 WHERE 1
                                    AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan trans_order_barang_header";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            } 
                                   
                            $msg = "Berhasil Reject ".$v_NoDokumen;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                            die();
                        }
                    }
                }
                else
                {
                    if($btn_save=="Save")
                    {   
						// update po
						{
							$q = "
	                            UPDATE
	                            	".$db["master"].".trans_terima_header
	                            SET     
	                            	TglDokumen = '".$v_TglDokumen."',
                                    KdGudang = '".$v_KdGudang."',
                                    PoNo = '".$v_PoNo."',
                                    NoSuratJalan = '".$v_NoSuratJalan."',
                                    Keterangan = '".$v_Keterangan."',
                                    KdSupplier = '".$v_KdSupplier."',
                                    ".$update_po."
                                    Status = '".$v_Status."',
                                    EditDate = NOW(),
                                    EditUser = '".$ses_login."' 
	                             WHERE 1
	                                AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'   
	                        ";
	                        //echo $q."<hr/>";                  
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan trans_terima_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        }
                            
                            $q = "DELETE FROM ".$db["master"].".trans_terima_detail WHERE 1 AND NoDokumen = '".$v_NoDokumen."' ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal reset trans_terima_detail";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            
                            // validasi
                            foreach($v_sid as $key=>$val_sid)
                            {
                                if(!isset($_POST["v_PCode_".$val_sid])){ $v_PCode = isset($_POST["v_PCode_".$val_sid]); } else { $v_PCode = $_POST["v_PCode_".$val_sid]; }     
                                if(!isset($_POST["v_Qty_".$val_sid])){ $v_Qty = isset($_POST["v_Qty_".$val_sid]); } else { $v_Qty = $_POST["v_Qty_".$val_sid]; }     
                                if(!isset($_POST["v_Satuan_".$val_sid])){ $v_Satuan = isset($_POST["v_Satuan_".$val_sid]); } else { $v_Satuan = $_POST["v_Satuan_".$val_sid]; }     
                                
                                if(!isset($_POST["v_Harga_po_".$val_sid])){ $v_Harga_po = isset($_POST["v_Harga_po_".$val_sid]); } else { $v_Harga_po = $_POST["v_Harga_po_".$val_sid]; }     
                                if(!isset($_POST["v_Disc1_po_".$val_sid])){ $v_Disc1_po = isset($_POST["v_Disc1_po_".$val_sid]); } else { $v_Disc1_po = $_POST["v_Disc1_po_".$val_sid]; }     
                                if(!isset($_POST["v_Disc2_po_".$val_sid])){ $v_Disc2_po = isset($_POST["v_Disc2_po_".$val_sid]); } else { $v_Disc2_po = $_POST["v_Disc2_po_".$val_sid]; }     
                                if(!isset($_POST["v_Potongan_po_".$val_sid])){ $v_Potongan_po = isset($_POST["v_Potongan_po_".$val_sid]); } else { $v_Potongan_po = $_POST["v_Potongan_po_".$val_sid]; }     
                                if(!isset($_POST["v_toleransi_terima_".$val_sid])){ $v_toleransi_terima = isset($_POST["v_toleransi_terima_".$val_sid]); } else { $v_toleransi_terima = $_POST["v_toleransi_terima_".$val_sid]; }     
                                
                                $v_PCode = save_char($v_PCode);
                                $v_Qty = save_int($v_Qty);
                                $v_Satuan = save_char($v_Satuan); 
                                
                                $v_Harga_po = save_int($v_Harga_po);
                                $v_Disc1_po = save_int($v_Disc1_po);
                                $v_Disc2_po = save_int($v_Disc2_po);
                                $v_Potongan_po = save_int($v_Potongan_po);
                                $v_toleransi_terima = save_int($v_toleransi_terima);
                                
                                $subtotal  = ($v_Qty*$v_Harga_po);
                                $disc1_val = $subtotal*($v_Disc1_po/100);
                                $disc2_val = ($subtotal - $disc1_val)*($v_Disc2_po/100);
                                $subtotal_Total = $subtotal - $disc1_val - $disc2_val - $v_Potongan_po;

                                if($v_Qty)
                                {     
                                    // cek toleransi penerimaan
                                    if($v_toleransi_terima*1!=0)
                                    {
                                        $qty_terima = 0;
                                        $q = "
                                                SELECT
                                                    trans_terima_header.NoDokumen,    
                                                    trans_terima_header.TglDokumen,    
                                                    trans_terima_header.NoSuratJalan,    
                                                    trans_terima_header.Keterangan,    
                                                    gudang.KdGudang,    
                                                    gudang.Keterangan AS NamaGudang,    
                                                    trans_terima_detail.PCode,
                                                    trans_terima_detail.Qty,
                                                    trans_terima_detail.Satuan
                                                FROM
                                                    trans_terima_header
                                                    INNER JOIN trans_terima_detail ON
                                                        trans_terima_header.NoDokumen = trans_terima_detail.NoDokumen
                                                        AND trans_terima_header.Status != '2'
                                                        AND trans_terima_header.PoNo = '".$v_PoNo."'
                                                        AND trans_terima_detail.PCode = '".$v_PCode."'
                                                        AND trans_terima_detail.NoDokumen != '".$v_NoDokumen."'
                                                    INNER JOIN gudang ON
                                                        trans_terima_header.KdGudang = gudang.KdGudang
                                                WHERE
                                                    1
                                                ORDER BY
                                                    trans_terima_header.NoDokumen ASC,    
                                                    trans_terima_detail.TglDokumen ASC 
                                        ";
                                        $qry_bpb = mysql_query($q);
                                        while($row_bpb = mysql_fetch_array($qry_bpb))
                                        {
                                             list(
                                                $NoDokumen,    
                                                $TglDokumen,    
                                                $NoSuratJalan,    
                                                $Keterangan,    
                                                $KdGudang,    
                                                $NamaGudang,    
                                                $PCode,
                                                $Qty,
                                                $Satuan  
                                            ) = $row_bpb; 
                                            
                                            $qty_terima += $Qty; 
                                        }
                                        
                                        // qty po
										
                                        $q = "
                                                SELECT
                                                    trans_order_barang_detail.Qty
                                                FROM
                                                    trans_order_barang_detail
                                                WHERE   
                                                    1
                                                    AND trans_order_barang_detail.NoDokumen = '".$v_PoNo."'
                                                    AND trans_order_barang_detail.PCode = '".$v_PCode."'
                                                LIMIT
                                                    0,1
                                        ";
                                        $qry_po = mysql_query($q);
                                        $row_po = mysql_fetch_array($qry_po);
                                        list($Qty_PO) = $row_po;
                                        
                                        $qty_batas_terima = $Qty_PO + ($Qty_PO * ($v_toleransi_terima/100));
                                        $qty_all_terima   = $qty_terima + $v_Qty;
                                        
                                        /*if($qty_all_terima>$qty_batas_terima)
                                        {
                                            $msg = $v_PCode." Gagal menyimpan, karena Qty Terima melebihi batas toleransi ".$v_toleransi_terima."% [".$qty_all_terima."||".$qty_batas_terima."]";
                                            echo "<script>alert('".$msg."');</script>"; 
                                            die();
                                        }*/
                                    }
                                }
                            }
                            
                            
                            foreach($v_sid as $key=>$val_sid)
                            {
                                if(!isset($_POST["v_PCode_".$val_sid])){ $v_PCode = isset($_POST["v_PCode_".$val_sid]); } else { $v_PCode = $_POST["v_PCode_".$val_sid]; }     
                                if(!isset($_POST["v_Qty_".$val_sid])){ $v_Qty = isset($_POST["v_Qty_".$val_sid]); } else { $v_Qty = $_POST["v_Qty_".$val_sid]; }     
                                if(!isset($_POST["v_Satuan_".$val_sid])){ $v_Satuan = isset($_POST["v_Satuan_".$val_sid]); } else { $v_Satuan = $_POST["v_Satuan_".$val_sid]; }     
                                
                                if(!isset($_POST["v_Harga_po_".$val_sid])){ $v_Harga_po = isset($_POST["v_Harga_po_".$val_sid]); } else { $v_Harga_po = $_POST["v_Harga_po_".$val_sid]; }     
                                if(!isset($_POST["v_Disc1_po_".$val_sid])){ $v_Disc1_po = isset($_POST["v_Disc1_po_".$val_sid]); } else { $v_Disc1_po = $_POST["v_Disc1_po_".$val_sid]; }     
                                if(!isset($_POST["v_Disc2_po_".$val_sid])){ $v_Disc2_po = isset($_POST["v_Disc2_po_".$val_sid]); } else { $v_Disc2_po = $_POST["v_Disc2_po_".$val_sid]; }     
                                if(!isset($_POST["v_Potongan_po_".$val_sid])){ $v_Potongan_po = isset($_POST["v_Potongan_po_".$val_sid]); } else { $v_Potongan_po = $_POST["v_Potongan_po_".$val_sid]; }     
                                if(!isset($_POST["v_toleransi_terima_".$val_sid])){ $v_toleransi_terima = isset($_POST["v_toleransi_terima_".$val_sid]); } else { $v_toleransi_terima = $_POST["v_toleransi_terima_".$val_sid]; }     
                                
                                $v_PCode = save_char($v_PCode);
                                $v_Qty = save_int($v_Qty);
                                $v_Satuan = save_char($v_Satuan); 
                                
                                $v_Harga_po = save_int($v_Harga_po);
                                $v_Disc1_po = save_int($v_Disc1_po);
                                $v_Disc2_po = save_int($v_Disc2_po);
                                $v_Potongan_po = save_int($v_Potongan_po);
                                $v_toleransi_terima = save_int($v_toleransi_terima);
                                
                                $subtotal  = ($v_Qty*$v_Harga_po);
                                $disc1_val = $subtotal*($v_Disc1_po/100);
                                $disc2_val = ($subtotal - $disc1_val)*($v_Disc2_po/100);
                                $subtotal_Total = $subtotal - $disc1_val - $disc2_val - $v_Potongan_po;
                                
                                if($v_PPn)
                                {
                                    $Total = $subtotal_Total+(($v_PPn/100)*$subtotal_Total);
                                }
                                else
                                {
                                    $Total = $subtotal_Total;
                                }

                                if($v_Qty)
                                {   
									//ambil harga1c untuk mengisi nilai di mutasi dan stock per PCode
									$harga = "
                                    SELECT a.Harga1c FROM ".$db["master"].".`masterbarang` a WHERE a.`PCode`='".$v_PCode."';
									";
									$qry = mysql_query($harga);
									$row = mysql_fetch_array($qry);
									list($Harga1c) = $row;
									
									$nilai = $Harga1c;
									
									//insert ke mutasi
									$insert_mutasi="
									INSERT INTO `mutasi`
												(
												 `NoTransaksi`,
												 `Jenis`,
												 `KdTransaksi`,
												 `Gudang`,
												 `GudangTujuan`,
												 `Tanggal`,
												 `KodeBarang`,
												 `Qty`,
												 `Nilai`,
												 `Status`)
									VALUES (
											'".$v_NoDokumen."',
											'I',
											'RG',
											'".$v_KdGudang."',
											'".$v_KdGudang."',
											'".$v_TglDokumen."',
											'".$v_PCode."',
											'".$v_Qty."',
											'".$nilai."',
											'1');
									";
									if(!mysql_query($insert_mutasi))
                                    {
                                        $msg = "Gagal Insert mutasi";
                                        echo "<script>alert('".$msg."');</script>";
										
                                        die(); 
                                    }
									
									//insert atau update ke stock
									list($tahun, $bulan, $tanggal) = explode('-',$v_TglDokumen);
									$fieldakhir = "GAkhir" . $bulan;
									$fieldnakhir = "GNAkhir" . $bulan;
									
									$jenis='I';
									if($jenis=='O'){
										$fieldupdate = "GKeluar" . $bulan; 
										$fieldnupdate = "GNKeluar" . $bulan; 
										$vqty = $v_Qty*-1;
										$vnilai = $v_Qty*$nilai*-1;
									}else{
										$fieldupdate = "GMasuk" .$bulan;
										$fieldnupdate = "GNMasuk" .$bulan;
										$vqty = $v_Qty;
										$vnilai = $v_Qty*$nilai;
									}
									
									//cari apakah PCode tersebut sudah ada di table stock?
									$cek= "
									SELECT 
									  * 
									FROM
									  `stock` a 
									WHERE a.tahun = '".$tahun."' 
									  AND a.`KdGudang` = '".$v_KdGudang."' 
									  AND a.`PCode` = '".$v_PCode."' 
									  AND a.`Status` = 'G' ;
									";
									//apakah ada datanya?
									$baris=mysql_query($cek);
									$count = mysql_num_rows($baris); 
									
									if($count>0){
										$sql_update_stock = "Update stock set $fieldupdate = $fieldupdate+$v_Qty, $fieldakhir=$fieldakhir+$vqty,
												$fieldnupdate = $fieldnupdate+$nilai, $fieldnakhir=$fieldnakhir+$vnilai	
												 where Tahun='".$tahun."' and KdGudang='".$v_KdGudang."' and PCode='".$v_PCode."'";
										if(!mysql_query($sql_update_stock))
											{
												//echo $sql_update_stock;
												$msg = "Gagal update Stock";
												echo "<script>alert('".$msg."');</script>";
												die(); 
											}
									}else{
										$sql_insert_stock="
													INSERT INTO `stock`
																(`Tahun`,
																 `KdGudang`,
																 `PCode`,
																 `Status`,
																 ".$fieldupdate.",
																 ".$fieldnupdate.",
																 ".$fieldakhir.",
																 ".$fieldnakhir."
																 )
													VALUES ('".$tahun."',
															'".$v_KdGudang."',
															'".$v_PCode."',
															'G',
															'".$v_Qty."',
															'".$nilai."',
															'".$vqty."',
															'".$vnilai."');
															";
										if(!mysql_query($sql_insert_stock))
											{
												//echo $sql_insert_stock;
												$msg = "Gagal insert Stock";
												echo "<script>alert('".$msg."');</script>";
												die(); 
											}
									}
									
									//cocokan konversi terlebih dahulu
									$konversi = "
                                    SELECT a.amount FROM ".$db["master"].".`konversi` a WHERE a.`PCode`='".$v_PCode."' AND a.`Satuan_From`='".$v_Satuan."';
									";
									
									$qry = mysql_query($konversi);
									$row_ = mysql_num_rows($qry);
									$row = mysql_fetch_array($qry);
									list($amount) = $row;
									
									if($row_*1==0){
										$QtyPcs = $v_Qty;	
									}else{
										//ambil dan tampung amount
									    $QtyPcs = $v_Qty*$amount;	
									}
																		
									$zkl = "
			                        SELECT 
			                            ".$db["master"].".trans_terima_header.kurs
			                        FROM
			                            ".$db["master"].".trans_terima_header
			                        WHERE
			                            1
			                            AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'
			                        LIMIT
			                            0,1
					                ";
					               
					                $qryzkl = mysql_query($zkl);
					                $rowzkl = mysql_fetch_array($qryzkl);
					                list($kurs) = $rowzkl;
		                
		                
									//insert ke trans_terima_detail
                                    $q = "
                                            INSERT INTO ".$db["master"].".`trans_terima_detail`
                                            SET 
                                              `NoDokumen` = '".$v_NoDokumen."',
                                              `TglDokumen` = '".$v_TglDokumen."',
                                              `KdSupplier` = '".$v_KdSupplier."',
                                              `KdGudang` = '".$v_KdGudang."',
                                              `PCode` = '".$v_PCode."',
                                              `Qty` = '".$v_Qty."',
                                              `QtyPcs` = '".$QtyPcs."',
                                              `Satuan` = '".$v_Satuan."',
                                              `Harga` = '".$v_Harga_po*$kurs."',
                                              `Disc1` = '".$v_Disc1_po."',
                                              `Disc2` = '".$v_Disc2_po."',
                                              `Potongan` = '".$v_Potongan_po."',
                                              `Jumlah` = '".$subtotal_Total."',
                                              `PPn` = '".$PPn."',
                                              `Total` = '".$Total."'      
                                    ";
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal Insert trans_terima_detail";
                                        echo "<script>alert('".$msg."');</script>";
                                        die(); 
                                    }
                                    
                                    //update QtyTerima Di trans_order_detail
                                    $qx = "
				                            UPDATE
				                            	".$db["master"].".trans_order_barang_detail
				                            SET 
			                                    QtyPcsTerima = '".$QtyPcs."'
				                             WHERE 1
				                                AND ".$db["master"].".trans_order_barang_detail.NoDokumen = '".$v_PoNo."'
				                                AND ".$db["master"].".trans_order_barang_detail.PCode = '".$v_PCode."'   
				                        ";
				                        //echo $qx."<hr/>";die;                  
				                        if(!mysql_query($qx))
				                        {
				                            $msg = "Gagal menyimpan trans_order_barang_detail";
				                            echo "<script>alert('".$msg."');</script>";
				                            die(); 
				                        } 
                                }
                            }
						}
                        
                        /* update header lagi */
                        {
                            $q = "
                                    SELECT
                                        SUM(".$db["master"].".`trans_terima_detail`.Jumlah) AS Jumlah
                                    FROM
                                        ".$db["master"].".`trans_terima_detail`
                                    WHERE
                                        1
                                        AND ".$db["master"].".`trans_terima_detail`.NoDokumen = '".$v_NoDokumen."'
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($Jumlah) = $row;
                            
                            $Disc_val = 0;
                            if($DiscHarga)
                            {
                                $Disc_val = $Jumlah * ($DiscHarga/100);
                            }
                            
                            
                            $v_NilaiPPn = 0;
                            if($PPn)
                            {
                                $v_NilaiPPn = ($Jumlah - $Disc_val) * ($PPn/100);
                            }
                            
                            $grand_total = ($Jumlah - $Disc_val) + $v_NilaiPPn;
                            
                            $q = "
                                UPDATE
                                    ".$db["master"].".trans_terima_header
                                SET     
                                    Jumlah = '".$Jumlah."',
                                    NilaiPPn = '".$v_NilaiPPn."',
                                    Total = '".$grand_total."'
                                 WHERE 1
                                    AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Update Total Header ".$v_NoDokumen;
                                echo "<script>alert('".$msg."');</script>"; 
                                die();
                            }
                        }
                        get_mutasi_stock("RG", $v_NoDokumen);  

						$msg = "Berhasil menyimpan ".$v_NoDokumen;
		                echo "<script>alert('".$msg."');</script>"; 
		                echo "<script>parent.CallAjaxForm('search','".$v_NoDokumen."');</script>";         
		                die();
                    } 
                }  
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  