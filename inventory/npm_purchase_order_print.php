<?php
    include("header.php");
    
    if(!isset($_POST["action"])){ $action = isset($_POST["action"]); } else { $action = $_POST["action"]; } 
    
    function create_txt_report($paths,$filed,$str)
    {
        $strs=chr(27).chr(64).chr(27).chr(67).chr(33).$str;
         $txt=$paths."/".$filed;
        if(file_exists($txt)){
            unlink($txt);
        }
        $open_it=fopen($txt,"a+");
        fwrite($open_it,$strs);
        fclose($open_it);    
    } 
    
    function sintak_epson()
    {
        $arr_data["escNewLine"]      = chr(10);  // New line (LF line feed)
        $arr_data["escUnerlineOn"]   = chr(27).chr(45).chr(1);  // Unerline On
        $arr_data["escUnerlineOnx2"] = chr(27).chr(45).chr(2);  // Unerline On x 2
        $arr_data["escUnerlineOff"]  = chr(27).chr(45).chr(0);  // Unerline Off
        $arr_data["escBoldOn"]       = chr(27).chr(69).chr(1);  // Bold On
        $arr_data["escBoldOff"]      = chr(27).chr(69).chr(0);  // Bold Off
        $arr_data["escNegativeOn"]   = chr(29).chr(66).chr(1);  // White On Black On'
        $arr_data["escNegativeOff"]  = chr(29).chr(66).chr(0);  // White On Black Off
        $arr_data["esc8CpiOn"]       = chr(29).chr(33).chr(16); // Font Size x2 On
        $arr_data["esc8CpiOff"]      = chr(29).chr(33).chr(0);  // Font Size x2 Off
        $arr_data["esc16Cpi"]        = chr(27).chr(77).chr(48); // Font A  -  Normal Font
        $arr_data["esc20Cpi"]        = chr(27).chr(77).chr(49); // Font B - Small Font
        $arr_data["escReset"]        = chr(27).chr(64); //chr(27) + chr(77) + chr(48); // Reset Printer
        $arr_data["escFeedAndCut"]   = chr(29).chr(86).chr(65); // Partial Cut and feed

        $arr_data["escAlignLeft"]    = chr(27).chr(97).chr(48); // Align Text to the Left
        $arr_data["escAlignCenter"]  = chr(27).chr(97).chr(49); // Align Text to the Center
        $arr_data["escAlignRight"]   = chr(27).chr(97).chr(50); // Align Text to the Right
        
        $arr_data["reset"]  =chr(27).'@'; //reset semua pengaturan printer
        $arr_data["plength"]=chr(27).'C'; //tinggi kertas, contoh $plength.chr(33) ==> tinggi 33 baris.
        $arr_data["lmargin"]=chr(27).'l'; //margin kiri, pemakaian sama dengan $plength.
        $arr_data["cond"]   =chr(15);   //condensed
        $arr_data["ncond"]  =chr(18);   //end condensed
        $arr_data["dwidth"] =chr(27).'!'.chr(16);  //tulisan melebar
        $arr_data["ndwidth"]=chr(27).'!'.chr(1);   //end tulisan melebar
        $arr_data["draft"]  =chr(27).'x'.chr(48);  //font draft
        $arr_data["nlq"]    =chr(27).'x'.chr(49);
        $arr_data["bold"]   =chr(27).'E';   //tulisan bold
        $arr_data["nbold"]  =chr(27).'F';   //end tulisan bold
        $arr_data["uline"]  =chr(27).'!'.chr(129); //garis bawah
        $arr_data["nuline"] =chr(27).'!'.chr(1); //end garis bawah
        $arr_data["dstrik"] =chr(27).'G';    //double strike (tulisan lebih tebal, 2 kali strike)
        $arr_data["ndstrik"]=chr(27).'H';    //end double strike (tulisan lebih tebal, 2 kali strike)
        $arr_data["elite"]  =chr(27).'M';    //tulisan elite
        $arr_data["pica"]   =chr(27).'P';    //tulisan pica
        $arr_data["height"] =chr(27).'!'.chr(16); //tulisan tinggi
        $arr_data["nheight"]=chr(27).'!'.chr(1);  //end tulisan tinggi
        $arr_data["spasi05"]=chr(27)."3".chr(16); //spasi 5 char
        $arr_data["spasi1"] =chr(27)."3".chr(24); //spasi 1 char
        $arr_data["fcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';    // potong kertas full
        $arr_data["pcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';    // potong kertas sebagian
        $arr_data["op_cash"]=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);   //buka cash register
                    
        return $arr_data;                           
    } 
    
    if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
    
    if($v_data=="")
    {
        $msg = "Data harus dipilih";
        echo "<script>alert('".$msg."');</script>"; 
        die();
    } 
    
    $arr_epson = sintak_epson();
    
    if($action=="Print 1/2 Letter")
    {
        //$total_spasi = 135;
		$total_spasi = 100;
        $total_spasi_header = 70;
        
        $jml_detail  = 5;
        $ourFileName = "purchase-order.txt";
        $nama        = "PT. NATURA PESONA MANDIRI"; 
        
        $echo="";
        foreach($v_data as $key => $val)
        {
            $v_NoDokumen = $val;
            unset($arr_data);
            
            $q = "
                    SELECT
                        *
                    FROM 
                        ".$db["master"].".trans_order_barang_header
                    WHERE
                        1
                        AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                    LIMIT
                        0,1
            ";     
            $qry_curr = mysql_query($q);
            $arr_curr = mysql_fetch_array($qry_curr);
            
            $note_header = substr($arr_curr["Keterangan"],0,40);
            
            $q = "
                SELECT
                    ".$db["master"].".supplier.KdSupplier,
                    ".$db["master"].".supplier.Nama,
                    ".$db["master"].".supplier.Contact
                FROM
                    ".$db["master"].".supplier
                WHERE
                    1
                    AND ".$db["master"].".supplier.KdSupplier = '".$arr_curr["KdSupplier"]."'
                ORDER BY
                    ".$db["master"].".supplier.Nama ASC
            ";
            $qry_supplier = mysql_query($q);
            $row_supplier = mysql_fetch_array($qry_supplier);
            $suppliername = $row_supplier["Nama"];
            $Contact = $row_supplier["Contact"];
            
            $counter = 1;
            $q = "
                    SELECT
                        ".$db["master"].".trans_order_barang_detail.Sid,    
                        ".$db["master"].".trans_order_barang_detail.PCode,
                        ".$db["master"].".trans_order_barang_detail.Qty,
                        ".$db["master"].".trans_order_barang_detail.Satuan,
                        ".$db["master"].".trans_order_barang_detail.Harga,
                        ".$db["master"].".trans_order_barang_detail.Disc1,
                        ".$db["master"].".trans_order_barang_detail.Disc2,
                        ".$db["master"].".trans_order_barang_detail.Potongan,
                        ".$db["master"].".trans_order_barang_detail.Jumlah,
                        ".$db["master"].".trans_order_barang_detail.PPn,
                        ".$db["master"].".trans_order_barang_detail.Total
                    FROM
                        ".$db["master"].".trans_order_barang_detail
                    WHERE
                        1
                        AND ".$db["master"].".trans_order_barang_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
                    ORDER BY
                        ".$db["master"].".trans_order_barang_detail.Sid ASC
            ";
          
            $qry_po = mysql_query($q);
            while($row_po = mysql_fetch_array($qry_po))
            { 
                list(
                    $Sid,    
                    $PCode,
                    $Qty,
                    $Satuan,
                    $Harga,
                    $Disc1,
                    $Disc2,
                    $Potongan,
                    $Jumlah,
                    $PPn,
                    $Total 
                ) = $row_po;
                
                $arr_data["list_detail"][$v_NoDokumen][$counter] = $counter;    
                
                $arr_data["data_po_PCode"][$counter] = $PCode;
                $arr_data["data_po_Qty"][$counter] = $Qty;
                $arr_data["data_po_Satuan"][$counter] = $Satuan;
                $arr_data["data_po_Harga"][$counter] = $Harga;
                $arr_data["data_po_Disc1"][$counter] = $Disc1;
                $arr_data["data_po_Disc2"][$counter] = $Disc2;
                $arr_data["data_po_Potongan"][$counter] = $Potongan;
                $arr_data["data_po_Jumlah"][$counter] = $Jumlah;
                $arr_data["data_po_PPn"][$counter] = $PPn;
                $arr_data["data_po_Total"][$counter] = $Total;
                
                $arr_data["list_PCode"][$PCode] = $PCode;
                $counter++;
            }
            
            
            if(count($arr_data["list_PCode"])*1>0)
            {
                $where_pcode = where_array($arr_data["list_PCode"], "PCode", "in");
                
                $q = "
                        SELECT
                            ".$db["master"].".masterbarang.PCode,
                            ".$db["master"].".masterbarang.NamaLengkap
                        FROM
                            ".$db["master"].".masterbarang
                        WHERE
                            1
                            ".$where_pcode."
                        ORDER BY
                            ".$db["master"].".masterbarang.PCode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                { 
                    list($PCode, $NamaLengkap) = $row; 
                    
                    $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
                }
                
            }
            
            
            $curr_jml_detail = count($arr_data["list_detail"][$v_NoDokumen]);
            $jml_page = ceil($curr_jml_detail/$jml_detail);
            
            $nama_dokumen = "PURCHASE ORDER";
            
            $total = 0;
            for($i_page=1;$i_page<=$jml_page;$i_page++)
            {
                // header
                {
                    $echo.=chr(15);
                    $echo.=$nama;
                    //$echo .= chr(27)."G";
                    $echo.="\r\n";    
                    
                    $echo.="Jl. Raya Denpasar Bedugul KM.36 Tabanan";
                    //$echo .= chr(27)."G";
                    $echo.="\r\n";    
                    
                    $echo.="Bali 82191 - Indonesia";
                    //$echo .= chr(27)."G";
                    $echo.="\r\n";    
                    
                    $echo.=" ";
                    //$echo .= chr(27)."G";
                    $echo.="\r\n"; 
                    $echo.=chr(18);   
                }
                
                
                
                $echo.=chr(15);
                $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
                for($i=0;$i<$limit_spasi;$i++)
                {
                    $echo.=" ";
                }
                
                //$echo .= chr(27).chr(119).chr(1);
                $echo.=$nama_dokumen;
                $echo.="\r\n";
                
                
                $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$arr_curr["NoDokumen"])/2);
                for($i=0;$i<$limit_spasi;$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="No : ".$arr_curr["NoDokumen"];
                $echo.="\r\n";
                
                 
                
                // baris 1
                {
                    $echo.="TANGGAL";
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    
                    $echo.=format_show_date($arr_curr["TglDokumen"]);
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TglDokumen"])));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 5;
                    for($i=0;$i<$limit_spasi;$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="SUPPLIER";
                    
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("SUPPLIER"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    $echo.=$suppliername;
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($suppliername));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.="\r\n";    
                }

                // baris 2
                {
                    $echo.="MATA UANG";
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("MATA UANG"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    
                    $echo.=$arr_curr["currencycode"];
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen($arr_curr["currencycode"]));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 5;
                    for($i=0;$i<$limit_spasi;$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="TOP";
                    
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("TOP"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    $echo.=$arr_curr["TOP"]." Hari"; 
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TOP"]." Hari")));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.="\r\n";    
                }
                
                // baris 3
                {
                    $echo.="ESTIMASI";
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("ESTIMASI"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    
                    $echo.=format_show_date($arr_curr["TglTerima"]);
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TglTerima"])));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 5;
                    for($i=0;$i<$limit_spasi;$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="CONTACT";
                    
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("CONTACT"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    $echo.=$Contact; 
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($Contact)));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.="\r\n";    
                }
                
               
                
                $echo.=chr(15);
                for($i=1;$i<=$total_spasi;$i++)
                {
                    $echo.="-";
                }
                $echo .= "\r\n";
                
                
                $echo.="NO";
                $limit_spasi = 3;
                for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="NAMA BARANG";
                $limit_spasi = 40;
                for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                {
                    $echo.=" ";
                }
                
                
                $limit_spasi = 12;
                for($i=0;$i<($limit_spasi-strlen("QTY  "));$i++)
                {
                    $echo.=" ";
                }
                $echo .= "QTY  ";
                
                $echo.="";
                $limit_spasi = 5;
                for($i=0;$i<($limit_spasi-strlen(""));$i++)
                {
                    $echo.=" ";
                }
                
                $limit_spasi = 15;
                for($i=0;$i<($limit_spasi-strlen("HARGA  "));$i++)
                {
                    $echo.=" ";
                }
                $echo .= "HARGA  ";
                
                $limit_spasi = 20;
                for($i=0;$i<($limit_spasi-strlen("SUBTOTAL  "));$i++)
                {
                    $echo.=" ";
                }
                $echo .= "SUBTOTAL  ";
                
              
                
                $echo.="\r\n";
                for($i=1;$i<=$total_spasi;$i++)
                {
                    $echo.="-";
                }
                
                $echo.="\r\n";
                
                $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                $no_end = $no + $jml_detail;
                
                for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                {   
                    $PCode = $arr_data["data_po_PCode"][$i_detail];
                    $NamaLengkap = substr($arr_data["NamaLengkap"][$PCode], 0, 48);
                    $Qty = $arr_data["data_po_Qty"][$i_detail];
                    $Satuan = $arr_data["data_po_Satuan"][$i_detail];
                    $Harga = $arr_data["data_po_Harga"][$i_detail];
                    $Disc1 = $arr_data["data_po_Disc1"][$i_detail];
                    $Disc2 = $arr_data["data_po_Disc2"][$i_detail];
                    $Potongan = $arr_data["data_po_Potongan"][$i_detail];
                    $Jumlah = $arr_data["data_po_Jumlah"][$i_detail];
                    $PPn = $arr_data["data_po_PPn"][$i_detail];
                    $Total = $arr_data["data_po_Total"][$i_detail];

                    //$Harga_echo = $Jumlah/$Qty;
                    $Harga_echo = $Harga;
                    // kalo ada isi baru di print
                    if($NamaLengkap)
                    {
                	    if($Qty*1>0)
                	    {
	                        $echo.=chr(15);
	                        $echo.=$no;
	                        $limit_spasi = 3;
	                        for($i=0;$i<($limit_spasi-strlen($no));$i++)
	                        {
	                            $echo.=" ";
	                        }
	                        
	                        $echo.=$NamaLengkap;
	                        $limit_spasi = 40;
	                        for($i=0;$i<($limit_spasi-strlen($NamaLengkap));$i++)
	                        {
	                            $echo.=" ";
	                        }
	                        
	                        ## QTY
	                        $limit_spasi = 12;
	                        for($i=0;$i<($limit_spasi-strlen(format_number($Qty, 2)."  "));$i++)
	                        {
	                            $echo.=" ";
	                        }
	                        $echo.=format_number($Qty, 2)."  ";
	                        
	                        $echo.=$Satuan;
	                        $limit_spasi = 5;
	                        for($i=0;$i<($limit_spasi-strlen($Satuan));$i++)
	                        {
	                            $echo.=" ";
	                        }
	                        
	                        ## HARGA
	                        $limit_spasi = 15;
	                        for($i=0;$i<($limit_spasi-strlen(format_number($Harga_echo, 2)."  "));$i++)
	                        {
	                            $echo.=" ";
	                        }
	                        $echo.=format_number($Harga_echo, 2)."  ";
	                        
	                        $limit_spasi = 19;
	                        for($i=0;$i<($limit_spasi-strlen(format_number($Jumlah, 2)."  "));$i++)
	                        {
	                            $echo.=" ";
	                        }
	                        $echo.=format_number($Jumlah, 2)."  ";
	                       
	                        $echo.=chr(18);
	                        $total += $Jumlah;
                        }
                    }
                    
                    $no++;
                    $echo.="\r\n";
                }
                $echo.="\r\n";
                
                
                $echo.=chr(15);
                for($i=1;$i<=$total_spasi;$i++)
                {
                    $echo.="=";
                }
                $echo.=chr(18);
                
                
                $echo.=chr(15);
                if($i_page==$jml_page)
                {
                    $disc_val = $total*($arr_curr["DiscHarga"]/100);
                    $ppn_val  = ($total - $disc_val) * ($arr_curr["PPn"]/100); 
                    $grand_total = ($total - $disc_val) + $ppn_val;
                    
                    // TOTAL
                    $echo.="\r\n";
                    
                    $echo.="Note : ".$note_header;
                    
                    $limit_spasi = 68;
                    for($i=0;$i<($limit_spasi-strlen("Note : ".$note_header));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="TOTAL";
                    $limit_spasi = 12;
                    for($i=0;$i<($limit_spasi-strlen("TOTAL"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_number($total, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($total, 2)."  ";
                   
                    
                    // DISC
                    $echo.="\r\n";
                    
                    $echo.="";
                    
                    $limit_spasi = 68;
                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="DISC ".format_number($arr_curr["DiscHarga"],2)."%";
                    $limit_spasi = 12;
                    for($i=0;$i<($limit_spasi-strlen("DISC".format_number($arr_curr["DiscHarga"],2)."%"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_number($disc_val, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($disc_val, 2)."  ";
                    
                    
                    
                    // PPN
                    $echo.="\r\n";
                    
                    $echo.="";
                    
                    $limit_spasi = 68;
                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="PPN ".format_number($arr_curr["PPn"],2)."%";
                    $limit_spasi = 11;
                    for($i=0;$i<($limit_spasi-strlen("PPN".format_number($arr_curr["PPn"],2)."%"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_number($ppn_val, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($ppn_val, 2)."  ";
                    
                   
                    // GRAND TOTAL
                    $echo.="\r\n";
                    
                    $echo.="";
                    
                    $limit_spasi = 68;
                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="GRAND TOTAL";
                    $limit_spasi = 12;
                    for($i=0;$i<($limit_spasi-strlen("GRAND TOTAL"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_number($grand_total, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($grand_total, 2)."  ";
                    
                    
                    
                    
                    $echo.="\r\n"; 
                    $echo.="\r\n";
                    
                    $echo.="       Dibuat Oleh     ";
                    $echo.="          ";
                    $echo.="       Diketahui Oleh      ";
                    $echo.="          ";
                    $echo.="       Disetujui Oleh      ";
                    $echo.="\r\n";    
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="     (             )     ";
                    $echo.="          ";
                    $echo.="     (             )      ";
                    $echo.="          ";
                    $echo.="     (             )      ";
                    $echo.="\r\n";
                    //$echo .= "\r\n";
                    
                    //$echo .= "\r\n";
                }
                else
                {
                    //$echo.="\r\n";
                    //$echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                }
                $echo.=chr(18);
                //$echo .= chr(27);
                
                $q = "
                        SELECT
                            COUNT(noreferensi) AS jml_print
                        FROM
                            ".$db["master"].".log_print
                        WHERE
                            1
                            AND noreferensi = '".$v_NoDokumen."' 
                            AND form_data = 'purchase-order' 
                ";
                $qry_jml_print = mysql_query($q);
                $row_jml_print = mysql_fetch_array($qry_jml_print);
                
                if($row_jml_print["jml_print"]*1>0)
                {
                    //$echo .= $arr_epson["reset"];
                    $limit_spasi = 100;
                    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=chr(15)."COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
                }
                else
                {
                    //$echo .= $arr_epson["reset"];
                    $limit_spasi = 100;
                    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                    {
                        $echo.=" ";
                    }
                
                   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
                   
                }
                
                $echo.="\r\n";
                
                $echo .= "\r\n";
                $echo .= "\r\n";
                $echo .= "\r\n";
            
            } // end page
            
            
            // update counter print
            if($ses_login!="hendri1003")
            {
                $q = "
                        INSERT
                            ".$db["master"].".log_print
                        SET
                            form_data = 'purchase-order',
                            noreferensi = '".$v_NoDokumen."' , 
                            userid = '".$ses_login."' , 
                            print_date = NOW() , 
                            print_page = 'Setengah Letter'    
                ";
                if(!mysql_query($q))      
                {
                    $msg = "Gagal Insert Print Counter";
                    echo "<script>alert('".$msg."');</script>";     
                    die();
                }
            }
        } 
        
        $paths = "cetakan/";
        $name_text_file='purchase-order-'.$ses_login.'.txt';
        create_txt_report($paths,$name_text_file,$echo);
        header("Location: ".$paths."/".$name_text_file); 
    }
    else if($action=="Print Letter")
    {
        //$total_spasi = 135;
        $total_spasi = 100;
        $total_spasi_header = 70;
        
        $jml_detail  = 30;
        $ourFileName = "purchase-order.txt";
        $nama        = "PT. NATURA PESONA MANDIRI"; 
        
        $echo="";
        foreach($v_data as $key => $val)
        {
            $v_NoDokumen = $val;
            unset($arr_data);
            
            $q = "
                    SELECT
                        *
                    FROM 
                        ".$db["master"].".trans_order_barang_header
                    WHERE
                        1
                        AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                    LIMIT
                        0,1
            ";     
            $qry_curr = mysql_query($q);
            $arr_curr = mysql_fetch_array($qry_curr);
            
            $note_header = substr($arr_curr["Keterangan"],0,40);
            
            $q = "
                SELECT
                    ".$db["master"].".supplier.KdSupplier,
                    ".$db["master"].".supplier.Nama,
                    ".$db["master"].".supplier.Contact
                FROM
                    ".$db["master"].".supplier
                WHERE
                    1
                    AND ".$db["master"].".supplier.KdSupplier = '".$arr_curr["KdSupplier"]."'
                ORDER BY
                    ".$db["master"].".supplier.Nama ASC
            ";
            $qry_supplier = mysql_query($q);
            $row_supplier = mysql_fetch_array($qry_supplier);
            $suppliername = $row_supplier["Nama"];
            $Contact = $row_supplier["Contact"];
            
            $counter = 1;
            $q = "
                    SELECT
                        ".$db["master"].".trans_order_barang_detail.Sid,    
                        ".$db["master"].".trans_order_barang_detail.PCode,
                        ".$db["master"].".trans_order_barang_detail.Qty,
                        ".$db["master"].".trans_order_barang_detail.Satuan,
                        ".$db["master"].".trans_order_barang_detail.Harga,
                        ".$db["master"].".trans_order_barang_detail.Disc1,
                        ".$db["master"].".trans_order_barang_detail.Disc2,
                        ".$db["master"].".trans_order_barang_detail.Potongan,
                        ".$db["master"].".trans_order_barang_detail.Jumlah,
                        ".$db["master"].".trans_order_barang_detail.PPn,
                        ".$db["master"].".trans_order_barang_detail.Total
                    FROM
                        ".$db["master"].".trans_order_barang_detail
                    WHERE
                        1
                        AND ".$db["master"].".trans_order_barang_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
                    ORDER BY
                        ".$db["master"].".trans_order_barang_detail.Sid ASC
            ";
          
            $qry_po = mysql_query($q);
            while($row_po = mysql_fetch_array($qry_po))
            { 
                list(
                    $Sid,    
                    $PCode,
                    $Qty,
                    $Satuan,
                    $Harga,
                    $Disc1,
                    $Disc2,
                    $Potongan,
                    $Jumlah,
                    $PPn,
                    $Total 
                ) = $row_po;
                
                $arr_data["list_detail"][$v_NoDokumen][$counter] = $counter;    
                
                $arr_data["data_po_PCode"][$counter] = $PCode;
                $arr_data["data_po_Qty"][$counter] = $Qty;
                $arr_data["data_po_Satuan"][$counter] = $Satuan;
                $arr_data["data_po_Harga"][$counter] = $Harga;
                $arr_data["data_po_Disc1"][$counter] = $Disc1;
                $arr_data["data_po_Disc2"][$counter] = $Disc2;
                $arr_data["data_po_Potongan"][$counter] = $Potongan;
                $arr_data["data_po_Jumlah"][$counter] = $Jumlah;
                $arr_data["data_po_PPn"][$counter] = $PPn;
                $arr_data["data_po_Total"][$counter] = $Total;
                
                $arr_data["list_PCode"][$PCode] = $PCode;
                $counter++;
            }
            
            
            if(count($arr_data["list_PCode"])*1>0)
            {
                $where_pcode = where_array($arr_data["list_PCode"], "PCode", "in");
                
                $q = "
                        SELECT
                            ".$db["master"].".masterbarang.PCode,
                            ".$db["master"].".masterbarang.NamaLengkap
                        FROM
                            ".$db["master"].".masterbarang
                        WHERE
                            1
                            ".$where_pcode."
                        ORDER BY
                            ".$db["master"].".masterbarang.PCode ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                { 
                    list($PCode, $NamaLengkap) = $row; 
                    
                    $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
                }
                
            }
            
            
            $curr_jml_detail = count($arr_data["list_detail"][$v_NoDokumen]);
            $jml_page = ceil($curr_jml_detail/$jml_detail);
            
            $nama_dokumen = "PURCHASE ORDER";
            
            $total = 0;
            for($i_page=1;$i_page<=$jml_page;$i_page++)
            {
                // header
                {
                    $echo.=chr(15);
                    $echo.=$nama;
                    //$echo .= chr(27)."G";
                    $echo.="\r\n";    
                    
                    $echo.="Jl. Raya Denpasar Bedugul KM.36 Tabanan";
                    //$echo .= chr(27)."G";
                    $echo.="\r\n";    
                    
                    $echo.="Bali 82191 - Indonesia";
                    //$echo .= chr(27)."G";
                    $echo.="\r\n";    
                    
                    $echo.=" ";
                    //$echo .= chr(27)."G";
                    $echo.="\r\n"; 
                    $echo.=chr(18);   
                }
                
                
                
                $echo.=chr(18);
                $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
                for($i=0;$i<$limit_spasi;$i++)
                {
                    $echo.=" ";
                }
                
                //$echo .= chr(27).chr(119).chr(1);
                $echo.=$nama_dokumen;
                $echo.="\r\n";
                
                
                $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$arr_curr["NoDokumen"])/2);
                for($i=0;$i<$limit_spasi;$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="No : ".$arr_curr["NoDokumen"];
                $echo.="\r\n";
                
                 
                
                // baris 1
                {
                    $echo.="TANGGAL";
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    
                    $echo.=format_show_date($arr_curr["TglDokumen"]);
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TglDokumen"])));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 5;
                    for($i=0;$i<$limit_spasi;$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="SUPPLIER";
                    
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("SUPPLIER"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    $echo.=$suppliername;
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($suppliername));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.="\r\n";    
                }

                // baris 2
                {
                    $echo.="MATA UANG";
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("MATA UANG"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    
                    $echo.=$arr_curr["currencycode"];
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen($arr_curr["currencycode"]));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 5;
                    for($i=0;$i<$limit_spasi;$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="TOP";
                    
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("TOP"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    $echo.=$arr_curr["TOP"]." Hari"; 
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TOP"]." Hari")));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.="\r\n";    
                }
                
                // baris 3
                {
                    $echo.="ESTIMASI";
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("ESTIMASI"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    
                    $echo.=format_show_date($arr_curr["TglTerima"]);
                    
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TglTerima"])));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 5;
                    for($i=0;$i<$limit_spasi;$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="CONTACT";
                    
                    $limit_spasi = (15-2);
                    for($i=0;$i<($limit_spasi-strlen("CONTACT"));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=": ";
                    $echo.=$Contact; 
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_show_date($Contact)));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.="\r\n";    
                }

                $echo.=chr(15);
                for($i=1;$i<=$total_spasi;$i++)
                {
                    $echo.="-";
                }
                $echo .= "\r\n";
                
                
                $echo.="NO";
                $limit_spasi = 3;
                for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="NAMA BARANG";
                $limit_spasi = 40;
                for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
                {
                    $echo.=" ";
                }
                
                
                $limit_spasi = 12;
                for($i=0;$i<($limit_spasi-strlen("QTY  "));$i++)
                {
                    $echo.=" ";
                }
                $echo .= "QTY  ";
                
                $echo.="";
                $limit_spasi = 5;
                for($i=0;$i<($limit_spasi-strlen(""));$i++)
                {
                    $echo.=" ";
                }
                
                $limit_spasi = 15;
                for($i=0;$i<($limit_spasi-strlen("HARGA  "));$i++)
                {
                    $echo.=" ";
                }
                $echo .= "HARGA  ";
                
                $limit_spasi = 20;
                for($i=0;$i<($limit_spasi-strlen("SUBTOTAL  "));$i++)
                {
                    $echo.=" ";
                }
                $echo .= "SUBTOTAL  ";
                
              
                
                $echo.="\r\n";
                for($i=1;$i<=$total_spasi;$i++)
                {
                    $echo.="-";
                }
                
                $echo.="\r\n";
                
                $no     = (($i_page * $jml_detail) - $jml_detail)+1;
                $no_end = $no + $jml_detail;
                
                for($i_detail=$no;$i_detail<$no_end;$i_detail++)
                {   
                    $PCode = $arr_data["data_po_PCode"][$i_detail];
                    $NamaLengkap = substr($arr_data["NamaLengkap"][$PCode], 0, 48);
                    $Qty = $arr_data["data_po_Qty"][$i_detail];
                    $Satuan = $arr_data["data_po_Satuan"][$i_detail];
                    $Harga = $arr_data["data_po_Harga"][$i_detail];
                    $Disc1 = $arr_data["data_po_Disc1"][$i_detail];
                    $Disc2 = $arr_data["data_po_Disc2"][$i_detail];
                    $Potongan = $arr_data["data_po_Potongan"][$i_detail];
                    $Jumlah = $arr_data["data_po_Jumlah"][$i_detail];
                    $PPn = $arr_data["data_po_PPn"][$i_detail];
                    $Total = $arr_data["data_po_Total"][$i_detail];

                    $Harga_echo = $Jumlah/$Qty;
                    
                    // kalo ada isi baru di print
                    if($NamaLengkap)
                    {
                        if($Qty*1>0)
                        {
                            $echo.=chr(15);
                            $echo.=$no;
                            $limit_spasi = 3;
                            for($i=0;$i<($limit_spasi-strlen($no));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            $echo.=$NamaLengkap;
                            $limit_spasi = 40;
                            for($i=0;$i<($limit_spasi-strlen($NamaLengkap));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            ## QTY
                            $limit_spasi = 12;
                            for($i=0;$i<($limit_spasi-strlen(format_number($Qty, 2)."  "));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=format_number($Qty, 2)."  ";
                            
                            $echo.=$Satuan;
                            $limit_spasi = 5;
                            for($i=0;$i<($limit_spasi-strlen($Satuan));$i++)
                            {
                                $echo.=" ";
                            }
                            
                            ## HARGA
                            $limit_spasi = 15;
                            for($i=0;$i<($limit_spasi-strlen(format_number($Harga_echo, 2)."  "));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=format_number($Harga_echo, 2)."  ";
                            
                            $limit_spasi = 20;
                            for($i=0;$i<($limit_spasi-strlen(format_number($Jumlah, 2)."  "));$i++)
                            {
                                $echo.=" ";
                            }
                            $echo.=format_number($Jumlah, 2)."  ";
                           
                            $echo.=chr(18);
                            $total += $Jumlah;
                        }
                    }
                    
                    $no++;
                    $echo.="\r\n";
                }
                $echo.="\r\n";
                
                
                $echo.=chr(15);
                for($i=1;$i<=$total_spasi;$i++)
                {
                    $echo.="=";
                }
                $echo.=chr(18);
                
                
                $echo.=chr(15);
                if($i_page==$jml_page)
                {
                    $disc_val = $total*($arr_curr["DiscHarga"]/100);
                    $ppn_val  = ($total - $disc_val) * ($arr_curr["PPn"]/100); 
                    $grand_total = ($total - $disc_val) + $ppn_val;
                    
                    // TOTAL
                    $echo.="\r\n";
                    
                    $echo.="Note : ".$note_header;
                    
                    $limit_spasi = 95;
                    for($i=0;$i<($limit_spasi-strlen("Note : ".$note_header));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="TOTAL";
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen("TOTAL"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_number($total, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($total, 2)."  ";
                   
                    
                    // DISC
                    $echo.="\r\n";
                    
                    $echo.="";
                    
                    $limit_spasi = 95;
                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="DISC ".format_number($arr_curr["DiscHarga"],2)."%";
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen("DISC".format_number($arr_curr["DiscHarga"],2)."%"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 19;
                    for($i=0;$i<($limit_spasi-strlen(format_number($disc_val, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($disc_val, 2)."  ";
                    
                    
                    
                    // PPN
                    $echo.="\r\n";
                    
                    $echo.="";
                    
                    $limit_spasi = 95;
                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="PPN ".format_number($arr_curr["PPn"],2)."%";
                    $limit_spasi = 19;
                    for($i=0;$i<($limit_spasi-strlen("PPN".format_number($arr_curr["PPn"],2)."%"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_number($ppn_val, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($ppn_val, 2)."  ";
                    
                   
                    // GRAND TOTAL
                    $echo.="\r\n";
                    
                    $echo.="";
                    
                    $limit_spasi = 95;
                    for($i=0;$i<($limit_spasi-strlen(""));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.="GRAND TOTAL";
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen("GRAND TOTAL"));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(format_number($grand_total, 2)."  "));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($grand_total, 2)."  ";
                    
                    
                    
                    
                    $echo.="\r\n"; 
                    $echo.="\r\n";
                    
                    $echo.="       Dibuat Oleh     ";
                    $echo.="          ";
                    $echo.="       Diketahui Oleh      ";
                    $echo.="          ";
                    $echo.="       Disetujui Oleh      ";
                    $echo.="\r\n";    
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="     (             )     ";
                    $echo.="          ";
                    $echo.="     (             )      ";
                    $echo.="          ";
                    $echo.="     (             )      ";
                    $echo.="\r\n";
                    //$echo .= "\r\n";
                    
                    //$echo .= "\r\n";
                }
                else
                {
                    //$echo.="\r\n";
                    //$echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                    $echo.="\r\n";
                }
                $echo.=chr(18);
                //$echo .= chr(27);
                
                $q = "
                        SELECT
                            COUNT(noreferensi) AS jml_print
                        FROM
                            ".$db["master"].".log_print
                        WHERE
                            1
                            AND noreferensi = '".$v_NoDokumen."' 
                            AND form_data = 'purchase-order' 
                ";
                $qry_jml_print = mysql_query($q);
                $row_jml_print = mysql_fetch_array($qry_jml_print);
                
                if($row_jml_print["jml_print"]*1>0)
                {
                    //$echo .= $arr_epson["reset"];
                    $limit_spasi = 135;
                    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=chr(15)."COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
                }
                else
                {
                    //$echo .= $arr_epson["reset"];
                    $limit_spasi = 135;
                    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                    {
                        $echo.=" ";
                    }
                
                   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
                   
                }
                
                $echo.="\r\n";
                
                $echo .= "\r\n";
                $echo .= "\r\n";
                $echo .= "\r\n";
            
            } // end page
            
            
            // update counter print
            if($ses_login!="hendri1003")
            {
                $q = "
                        INSERT
                            ".$db["master"].".log_print
                        SET
                            form_data = 'purchase-order',
                            noreferensi = '".$v_NoDokumen."' , 
                            userid = '".$ses_login."' , 
                            print_date = NOW() , 
                            print_page = 'Letter'    
                ";
                if(!mysql_query($q))      
                {
                    $msg = "Gagal Insert Print Counter";
                    echo "<script>alert('".$msg."');</script>";     
                    die();
                }
            }
        } 
        
        $paths = "cetakan/";
        $name_text_file='purchase-order-'.$ses_login.'.txt';
        create_txt_report($paths,$name_text_file,$echo);
        header("Location: ".$paths."/".$name_text_file);     
    }
    else if($action=="Close PO")
    {
        foreach($v_data as $key=>$val)
        {
            $q = "
                    UPDATE
                        trans_order_barang_header
                    SET
                        Status = '3'
                    WHERE
                        NoDokumen = '".$val."'
            ";
            if(!mysql_query($q))
            {
                echo "Gagal Update PO ".$val."<br>";
            }
            else
            {
                echo "Berhasil Close PO ".$val."<br>";
            }
        }    
    }
    mysql_close($con);
?>  