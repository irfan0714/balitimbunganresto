<?php
include("header.php");

$modul            = "MOVING CONFIRMATION";

if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }   
if(!isset($_POST["v_mm"])){ $v_mm = isset($_POST["v_mm"]); } else { $v_mm = $_POST["v_mm"]; }   
if(!isset($_POST["v_yyyy"])){ $v_yyyy = isset($_POST["v_yyyy"]); } else { $v_yyyy = $_POST["v_yyyy"]; } 

if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; } 

if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }   
if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   

if($v_mm=="")
{
    $v_mm = date_now("m")*1;
}

if($v_yyyy=="")
{
    $v_yyyy = date_now("Y");
}

//jangan lupa hapus $ses_login
//$ses_login="mechael0101";
$q = "
    SELECT
        ".$db["master"].".gudang.KdGudang,
        ".$db["master"].".gudang.Keterangan
    FROM
        ".$db["master"].".gudang
        INNER JOIN ".$db["master"].".gudang_admin ON
            ".$db["master"].".gudang.KdGudang = ".$db["master"].".gudang_admin.KdGudang
            AND ".$db["master"].".gudang_admin.UserName = '".$ses_login."'
    WHERE
        1
    ORDER BY
        ".$db["master"].".gudang.KdGudang ASC
";
$qry_gudang = mysql_query($q);
while($row_gudang = mysql_fetch_array($qry_gudang))
{ 
    list($KdGudang, $NamaGudang) = $row_gudang;    
    
    $arr_data["list_gudang"][$KdGudang] = $KdGudang;
    $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
} 

$arr_data["list_month"][1] = "Januari";
$arr_data["list_month"][2] = "Febuari";
$arr_data["list_month"][3] = "Maret";
$arr_data["list_month"][4] = "April";
$arr_data["list_month"][5] = "Mei";
$arr_data["list_month"][6] = "Juni";
$arr_data["list_month"][7] = "Juli";
$arr_data["list_month"][8] = "Agustus";
$arr_data["list_month"][9] = "September";
$arr_data["list_month"][10] = "Oktober";
$arr_data["list_month"][11] = "November";
$arr_data["list_month"][12] = "Desember";

$start_year = 2016;
$end_year = date_now("Y");

for($i=$start_year;$i<=$end_year;$i++)
{
    $arr_data["list_year"][$i] = $i;
}

if($btn_save)
{
    foreach($v_data as $key=>$val)
    {
        $q = "
                UPDATE 
                    ".$db["master"].".trans_mutasi_header
                SET
                    MovingConfirmation = '1'
                WHERE
                    1
                    AND NoDokumen = '".$val."'
        ";
        if(!mysql_query($q))
        {
            die("Gagal Update");
        }
        else
        {
            get_mutasi_stock("MC", $val);
        }       
    }
}
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
        
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='search')
				{
                    search_keyword = document.getElementById("search_keyword").value;      
                    search_gudang = document.getElementById("search_gudang").value; 
                    v_date_from = document.getElementById("v_date_from").value; 
                    v_date_to = document.getElementById("v_date_to").value; 
                    
                    variabel += "&search_keyword="+search_keyword;
                    variabel += "&search_gudang="+search_gudang;
                    variabel += "&v_date_from="+v_date_from;
                    variabel += "&v_date_to="+v_date_to;
					
					variabel += "&v_NoDokumen_curr="+param1;

					//alert(param1);
					document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					document.getElementById("col-header").innerHTML   = "";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-search").innerHTML   = xmlhttp.responseText;

							if(param1)
							{
								CallAjaxForm('edit_data' ,param1);
							}
						}

						return false;
					}
					xmlhttp.send(null);
				}

				
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function confirm_delete(name)
		{
			var r = confirm("Anda yakin ingin void "+name+" ? ");
			if(r)
			{
				document.getElementById("v_del").value = '1';
				document.getElementById("theform").submit();
			}
		}
        
        function confirm_unvoid(name)
        {
            var r = confirm("Anda yakin ingin UnVoid "+name+" ? ");
            if(r)
            {
                document.getElementById("v_undel").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve "+name+" ? ");
            if(r)
            {
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_reject(name)
        {
            var r = confirm("Anda yakin ingin Reject "+name+" ? ");
            if(r)
            {
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        
		function CheckAll(param, target)
		{
			var field = document.getElementsByName(target);
			var chkall = document.getElementById(param);
			if (chkall.checked == true)
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = true ;
			}else
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = false ;
			}
		}

		function reform(val)
		{
			var a = val.split(",");
			var b = a.join("");
			//alert(b);
			return b;
		}

		function format(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(0);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format4(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(4);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format2(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(2);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format6(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(6);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function addSeparatorsNF(nStr, inD, outD, sep)
		{
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1)
			{
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr))
			{
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}

		function toFormat4(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format4(document.getElementById(id).value);
		}

		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
		}

		function toFormat6(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format6(document.getElementById(id).value);
		}

		function isanumber(id)
		{
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				document.getElementById(id).value=0;
			}
			else
			{
				document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
			}
		}

		function start_page()
		{
            try
            {
			    document.getElementById("search_keyword").focus();
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
		}

		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
        
        function copy_QtyProgram(PCode)
        {
            QtyProgram = document.getElementById("v_QtyProgram_"+PCode).value;
            
            document.getElementById("v_QtyFisik_"+PCode).value = QtyProgram;    
        }
        
        function calculate(PCode)
        {
            QtyProgram = reform(document.getElementById("v_QtyProgram_"+PCode).value);
            QtyFisik = reform(document.getElementById("v_QtyFisik_"+PCode).value);
            
            selisih = (QtyFisik*1) - (QtyProgram*1);
            
            document.getElementById("td_selisih_"+PCode).innerHTML = format2(selisih);
        }
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Transaksi - Persediaan</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
        <form name='theform' id='theform' method="POST">
		<div class="row">
		       <table class="table table-bordered responsive" align="center">
                
                <tr class="title_table">
                    <td colspan="100%"><?php echo $modul; ?></td>
                </tr>
                
                <tr>
                    <td width="100"><b>Gudang</b></td>
                    <td>
                        <select name="v_KdGudang" id="v_KdGudang" class="form-control-new" style="width: 200px;">
                            <?php 
                                foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                                {
                                    $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                    
                                    $selected = "";
                                    if($KdGudang==$v_KdGudang)
                                    {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                    <?php
                                }
                            ?> 
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td><b>Periode</b></td>
                    <td>
                        <select name="v_mm" id="v_mm" class="form-control-new" style="width: 130px;">
                            <?php 
                                foreach($arr_data["list_month"] as $key=>$val)
                                {
                                    $selected = "";
                                    if($key==$v_mm)
                                    {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                            ?> 
                        </select>  
                        
                        <select name="v_yyyy" id="v_yyyy" class="form-control-new" style="width: 70px;">
                            <?php 
                                foreach($arr_data["list_year"] as $key=>$val)
                                {
                                    $selected = "";
                                    if($val==$v_yyyy)
                                    {
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                            ?> 
                        </select>   
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <button type="submit" name="btn_submit" id="btn_submit" class="btn btn-info btn-icon btn-sm icon-left" value="Submit">Submit<i class="entypo-check"></i></button>
                    </td>
                </tr>
                
               </table>
               <br>
               <?php 
                if($btn_submit || $btn_save)
                {
                	//echo "<script>alert('".$v_KdGudang."');</script>";
                	
                	//cek jika barang sudah SO
                	$sql="SELECT MAX(DATE(a.`TglDokumen`)) AS tglapprove FROM `opname_header` a WHERE a.`KdGudang`='$v_KdGudang' AND a.`Approval_Status`='1' ;";
                
					if($v_mm<10){
						$bln = "0".$v_mm;
					}else{
						$bln = $v_mm;
					}
					
					$tanggal = $v_yyyy."-".$bln."-01";
				
                	$query = mysql_query($sql);
                	$cek = mysql_fetch_array($query);
                	
                	if($cek['tglapprove'] >= $tanggal){
					   echo "<script>alert('Sudah Stock Opname.');</script>";
					}else{
                	
                    $ada_data = 0;
                    $q = "
                            SELECT
                                ".$db["master"].".trans_mutasi_header.NoDokumen,
                                ".$db["master"].".trans_mutasi_header.TglDokumen,
                                ".$db["master"].".trans_mutasi_header.PurposeId,
                                ".$db["master"].".trans_mutasi_header.KdGudang_From,
                                ".$db["master"].".masterbarang.PCode,
                                ".$db["master"].".masterbarang.NamaLengkap,
                                ".$db["master"].".trans_mutasi_detail.Qty,
                                ".$db["master"].".trans_mutasi_detail.Satuan,
                                ".$db["master"].".trans_mutasi_detail.Keterangan
                            FROM
                                ".$db["master"].".trans_mutasi_header
                                INNER JOIN ".$db["master"].".trans_mutasi_detail ON
                                    ".$db["master"].".trans_mutasi_header.NoDokumen = ".$db["master"].".trans_mutasi_detail.NoDokumen
                                    AND ".$db["master"].".trans_mutasi_header.KdGudang_To = '".$v_KdGudang."'
                                    AND ".$db["master"].".trans_mutasi_header.Status = '1'
                                    AND ".$db["master"].".trans_mutasi_header.MovingCOnfirmation = '0'
                                    AND MONTH(".$db["master"].".trans_mutasi_header.TglDokumen) = '".sprintf("%02s", $v_mm)."'
                                    AND YEAR(".$db["master"].".trans_mutasi_header.TglDokumen) = '".$v_yyyy."'
                                INNER JOIN ".$db["master"].".masterbarang ON
                                    ".$db["master"].".trans_mutasi_detail.PCode = ".$db["master"].".masterbarang.PCode
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".trans_mutasi_header.NoDokumen ASC    
                    ";
                    
                    if($ses_login=="mechael0101")
                    {
                        //echo $q;
                    }
											
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $NoDokumen,
                            $TglDokumen,
                            $PurposeId,
                            $KdGudang_From,
                            $PCode,
                            $NamaLengkap,
                            $Qty,
                            $Satuan,
                            $Keterangan
                        ) = $row;
                        
                        $arr_data["list_data"][$NoDokumen][$PCode] = $NoDokumen;
                        
                        $arr_data["data_TglDokumen"][$NoDokumen] = $TglDokumen;
                        $arr_data["data_PurposeId"][$NoDokumen] = $PurposeId;
                        $arr_data["data_KdGudang_From"][$NoDokumen] = $KdGudang_From;
                        $arr_data["data_NamaLengkap"][$NoDokumen][$PCode] = $NamaLengkap;
                        $arr_data["data_Qty"][$NoDokumen][$PCode] = $Qty;
                        $arr_data["data_Satuan"][$NoDokumen][$PCode] = $Satuan;
                        $arr_data["data_Keterangan"][$NoDokumen][$PCode] = $Keterangan;
                        
                        $arr_data["list_PurposeId"][$PurposeId] = $PurposeId;
                        $ada_data++;
                    }
                    
                    $q = "
                        SELECT
                            ".$db["master"].".gudang.KdGudang,
                            ".$db["master"].".gudang.Keterangan
                        FROM
                            ".$db["master"].".gudang
                        WHERE
                            1
                        ORDER BY
                            ".$db["master"].".gudang.KdGudang ASC
                    ";
                    $qry_gudang = mysql_query($q);
                    while($row_gudang = mysql_fetch_array($qry_gudang))
                    { 
                        list($KdGudang, $NamaGudang) = $row_gudang;    
                        
                        $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
                    }
                    
                    if(count($arr_data["list_PurposeId"])*1>0)
                    { 
                        $q = "
                            SELECT
                                ".$db["master"].".intmutpurpose.purposeid,
                                ".$db["master"].".intmutpurpose.purpose
                            FROM
                                ".$db["master"].".intmutpurpose
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".intmutpurpose.purpose ASC
                        ";
                        $qry_gudang = mysql_query($q);
                        while($row_gudang = mysql_fetch_array($qry_gudang))
                        { 
                            list($purposeid, $purpose_name) = $row_gudang;    
                            
                            $arr_data["purpose_name"][$purposeid] = $purpose_name;
                        } 
                    }
                    
                    ?>
                    
                    <table class="table table-bordered responsive">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>No Dokumen</td>
                                <td>Tanggal</td>
                                <td>Dari</td>
                                <td>Tujuan</td>
                                <td>PCODE</td>
                                <td>Nama Barang</td>
                                <td>Qty</td>
                                <td>Satuan</td>
                                <td>Keterangan</td>
                                <td align="center"><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></td>
                            </tr>
                        </thead>
                        
                        <tbody style="color: black;">
                            <?php 
                                $no = 0;
                                
                                if($ada_data*1==0)
                                {
                                    ?>
                                    <tr>
                                        <td colspan="100%" align="center">Tidak ada data</td>
                                    </tr>
                                    <?php
                                } 
                                
                                foreach($arr_data["list_data"] as $NoDokumen=>$val)
                                {
                                    $TglDokumen = $arr_data["data_TglDokumen"][$NoDokumen];
                                    $PurposeId = $arr_data["data_PurposeId"][$NoDokumen];
                                    $KdGudang_From = $arr_data["data_KdGudang_From"][$NoDokumen];
                                    
                                    foreach($arr_data["list_data"][$NoDokumen] as $PCode=>$val)
                                    {
                                        $NamaLengkap = $arr_data["data_NamaLengkap"][$NoDokumen][$PCode];
                                        $Qty = $arr_data["data_Qty"][$NoDokumen][$PCode];
                                        $Satuan = $arr_data["data_Satuan"][$NoDokumen][$PCode];
                                        $Keterangan = $arr_data["data_Keterangan"][$NoDokumen][$PCode];    
                                        
                                        $NoDokumen_echo = "";
                                        $TglDokumen_echo = "";
                                        $no_echo = "";
                                        if($NoDokumen!=$NoDokumen_prev)
                                        {
                                            $no++;
                                            $NoDokumen_echo = $NoDokumen;
                                            $TglDokumen_echo = format_show_date($TglDokumen);
                                            $no_echo = $no;
                                        }
                                        
                                        
                                        
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no_echo; ?></td>
                                        <td><?php echo $NoDokumen_echo; ?></td>
                                        <td align="center"><?php echo $TglDokumen_echo; ?></td>
                                        <td><?php echo $arr_data["NamaGudang"][$KdGudang_From]; ?></td>
                                        <td><?php echo $arr_data["purpose_name"][$PurposeId]; ?></td>
                                        <td><?php echo $PCode; ?></td>
                                        <td><?php echo $NamaLengkap; ?></td>
                                        <td align="right"><?php echo format_number($Qty, 2); ?></td>
                                        <td><?php echo $Satuan; ?></td>
                                        <td><?php echo $Keterangan; ?></td>
                                        <td align="center">
                                            <?php 
                                                if($NoDokumen_echo)
                                                {
                                            ?>
                                            <input type="checkbox" name="v_data[]" value="<?php echo $NoDokumen_echo; ?>">
                                            <?php 
                                                }
                                            ?>
                                        </td>
                                    </tr> 
                                    <?php
                                        $NoDokumen_prev = $NoDokumen;
                                    
                                    }
                                }
                            ?>    
                        </tbody>
                        
                        <?php 
                            if($ada_data)
                            {
                        ?>
                        <tr>
                            <td colspan="100%" align="center"><button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button></td>
                        </tr>
                        <?php 
                            }
                        ?>
                    </table>
                    <?php
                  }
                }
               ?>
       	
       	</div>
		</form>
	
    
<?php 
    if($id)
    {
        $id = save_char($id);
        ?>
        <script>CallAjaxForm('edit_data', '<?php echo $id; ?>');</script>
        <?php
    }

    include("footer.php"); 
?>