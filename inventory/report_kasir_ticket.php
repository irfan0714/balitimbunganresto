<?php               
    include("header.php");
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    
    if(!isset($_POST["v_jenis"])){ $v_jenis = isset($_POST["v_jenis"]); } else { $v_jenis = $_POST["v_jenis"]; }
    if(!isset($_POST["v_tipe"])){ $v_tipe = isset($_POST["v_tipe"]); } else { $v_tipe = $_POST["v_tipe"]; }
    if(!isset($_POST["v_Kasir"])){ $v_Kasir = isset($_POST["v_Kasir"]); } else { $v_Kasir = $_POST["v_Kasir"]; }
    if(!isset($_POST["v_keyword"])){ $v_keyword = isset($_POST["v_keyword"]); } else { $v_keyword = $_POST["v_keyword"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Kasir Ticket";
    
    if($v_date_from=="")
    {
        $v_date_from = date("d/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("d/m/Y");
    }
    
   
    $q = "
            SELECT
                ".$db["master"].".ticket_head.user
            FROM
                ".$db["master"].".ticket_head
            WHERE
                1
                AND ".$db["master"].".ticket_head.user!=''
            GROUP BY
                ".$db["master"].".ticket_head.user
            ORDER BY
                ".$db["master"].".ticket_head.user ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($Kasir) = $row;
        
        $Kasir = strtolower($Kasir);
        
        $arr_data["list_kasir"][$Kasir] = $Kasir;
    }
    
    //$ses_login = "ni1805";
    //echo $ses_UserLevel;
    if($ses_UserLevel==-1 || $ses_UserLevel==13 || $ses_UserLevel==15){
		
	}
    else
    {
        if(strtolower($ses_login)==$arr_data["list_kasir"][strtolower($ses_login)])
        {
            unset($arr_data["list_kasir"]);
            $arr_data["list_kasir"][$ses_login] = $ses_login;
        }
    }
    

    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-kasir-ticket.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  

function pop_edit(notrans,nominal)
{
    windowOpener(400, 600, 'Edit Ticket', 'report_kasir_ticket_pop_edit.php?id='+notrans+'&nominal='+nominal, 'Edit Ticket');
}

function pop_delete(notrans)
{
    windowOpener(400, 600, 'Delete Ticket', 'report_kasir_ticket_pop_delete.php?id='+notrans, 'Delete Ticket');
}

function pop_add()
{
    windowOpener(400, 600, 'Contract Rate Without Ticket', 'report_kasir_ticket_pop_add.php', 'Contract Rate Without Ticket');
}

    </script>
    
    <style>
        .link_pop{
            text-decoration: underline;
            color: black;
        }
        
        .link_pop:hover{
            text-decoration: none;
            color: #222222;
        }
    </style>
    
     <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Reporting</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="POST" name="theform" id="theform">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="100">Tanggal</th>
						<th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
					</tr>
                    
                    <tr>
                        <th>Jenis</th>
                        <th>: 
                                
                                  <select class="form-control-new" name="v_jenis" id="v_jenis" style="width: 200px;">
                                    <option value="">Semua</option>
                                    <option <?php if($v_jenis==1) echo "selected='selected'"; ?> value="1">Lokal</option>
                                    <option <?php if($v_jenis==2) echo "selected='selected'"; ?> value="2">Asing</option>    
                                </select>     
                        </th>
                    </tr>

                    <tr>
                        <th>Tipe</th>
                        <th>: 
                                
                                  <select class="form-control-new" name="v_tipe" id="v_tipe" style="width: 200px;">
                                    <option <?php if($v_tipe==1) echo "selected='selected'"; ?> value="1">Non Swing</option>
                                    <option <?php if($v_tipe==2) echo "selected='selected'"; ?> value="2">Swing</option>    
                                </select>     
                        </th>
                    </tr>
                    
                     <tr>
                        <th>Kasir</th>
                        <th>: 
                                
                                  <select class="form-control-new" name="v_Kasir" id="v_Kasir" style="width: 200px;">
                                  
                                  <?php
                                    if(count($arr_data["list_kasir"])*1>1)
                                    {
                                  ?>
                                    <option value="">Semua</option>
                                  <?php 
                                    }
                                  ?>
                                  
                                    <?php 
                                        foreach($arr_data["list_kasir"] as $Kasir=>$val)
                                        {
                                        ?>
                                        <option <?php if($v_Kasir==$Kasir) echo "selected='selected'"; ?> value="<?php echo $Kasir; ?>"><?php echo $Kasir; ?></option>
                                        <?php        
                                        }
                                    ?>
                                </select>     
                        </th>
                    </tr>
                    
                     
                      <tr>
                        <th>Keyword</th>
                        <th>: 
                            <input type="text" class="form-control-new" name="v_keyword" id="v_keyword" style="width: 200px;" value="<?php echo $v_keyword; ?>">
                        </th>
                     </tr>
                     
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                            
                            &nbsp;&nbsp;&nbsp;
                            <input type="button" onclick="pop_add()" class="btn btn-info btn-icon btn-sm icon-center" name="btn_add" id="btn_add" value="+ Contract Rate Without Ticket">
                        </td>
                    </tr>
				</thead>
				
			</table> 
			<br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    else
                    {
                        $where_date = " AND DATE(`ticket_head`.Tanggal) BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";    
                    }
                    
                    $where_Kasir = "";
                    if($v_Kasir!="")
                    {
                        $where_Kasir = " AND LOWER(ticket_head.user) = '".$v_Kasir."' ";    
                    }
                    
                    $arr_keyword[0] = "ticket_head.noidentitas";    
                    $arr_keyword[1] = "ticket_head.user";    
                    $arr_keyword[2] = "ticket_head.notrans";      
                    
                    $where_keyword = "";
                    if($v_keyword)
                    {
                        $where_search_keyword = search_keyword($v_keyword, $arr_keyword);
                        $where_keyword = $where_search_keyword;
                    }
                    
                    //Yang Lama 15-04-2019
                    // $q = "
                    //         SELECT
                    //             ticket_head.notrans,
                    //             ticket_head.Tanggal,
                    //             ticket_head.qty,
                    //             ticket_head.user,
                    //             ticket_head.noidentitas,
                    //             ticket_head.ttl_amount,
                    //             ticket_head.nilai_tunai-ticket_head.ttl_kembali as nilai_tunai,
                    //             ticket_head.nilai_debit,
                    //             ticket_head.nilai_kredit,
                    //             ticket_head.nilai_voucher,
                    //             ticket_head.edited_user,
                    //             ticket_head.edited_date
                    //         FROM
                    //             ticket_head
                    //         WHERE
                    //             1
                    //             ".$where_date."
                    //             ".$where_Kasir."
                    //             ".$where_keyword."
                    //             AND ticket_head.noidentitas != '1234'
                    //         ORDER BY
                    //             ticket_head.Tanggal ASC,
                    //             ticket_head.notrans ASC    
                    // ";

                    if($v_tipe=="1")
                    {
                    $q = "
                            SELECT
                                ticket_head.notrans,
                                ticket_head.Tanggal,
                                ticket_head.qty,
                                ticket_head.user,
                                ticket_head.noidentitas,
                                ticket_head.ttl_amount,
                                ticket_head.nilai_tunai-ticket_head.ttl_kembali as nilai_tunai,
                                ticket_head.nilai_debit,
                                ticket_head.nilai_kredit,
                                ticket_head.nilai_voucher,
                                kr1.nama AS BankDebet,
                                kr2.nama AS EDCBankDebet,
                                kr3.nama AS BankKredit,
                                kr4.nama AS EDCBankKredit,
                                ticket_head.edited_user,
                                ticket_head.edited_date
                            FROM
                                ticket_head INNER JOIN ticket
                                ON ticket_head.notrans = ticket.notrans
                                LEFT JOIN kartu kr1
                                ON ticket_head.BankDebet = kr1.id
                                LEFT JOIN kartu kr2
                                ON ticket_head.EDCBankDebet = kr2.id
                                LEFT JOIN kartu kr3
                                ON ticket_head.BankKredit = kr3.id
                                LEFT JOIN kartu kr4
                                ON ticket_head.EDCBankKredit = kr4.id
                            WHERE
                                1
                                ".$where_date."
                                ".$where_Kasir."
                                ".$where_keyword."
                                AND ticket_head.noidentitas != '1234'
                                AND ticket.PCode NOT IN ( SELECT a.PCode FROM groupswing a WHERE a.Status='A')
                                GROUP BY ticket_head.`notrans`
                            ORDER BY
                                ticket_head.Tanggal ASC,
                                ticket_head.notrans ASC    
                    ";
                    }else{
                    $q = "
                            SELECT
                                ticket_head.notrans,
                                ticket_head.Tanggal,
                                ticket_head.qty,
                                ticket_head.user,
                                ticket_head.noidentitas,
                                ticket_head.ttl_amount,
                                ticket_head.nilai_tunai-ticket_head.ttl_kembali as nilai_tunai,
                                ticket_head.nilai_debit,
                                ticket_head.nilai_kredit,
                                ticket_head.nilai_voucher,
                                kr1.nama AS BankDebet,
                                kr2.nama AS EDCBankDebet,
                                kr3.nama AS BankKredit,
                                kr4.nama AS EDCBankKredit,
                                ticket_head.edited_user,
                                ticket_head.edited_date
                            FROM
                                ticket_head INNER JOIN ticket
                                ON ticket_head.notrans = ticket.notrans
                                LEFT JOIN kartu kr1
                                ON ticket_head.BankDebet = kr1.id
                                LEFT JOIN kartu kr2
                                ON ticket_head.EDCBankDebet = kr2.id
                                LEFT JOIN kartu kr3
                                ON ticket_head.BankKredit = kr3.id
                                LEFT JOIN kartu kr4
                                ON ticket_head.EDCBankKredit = kr4.id
                            WHERE
                                1
                                ".$where_date."
                                ".$where_Kasir."
                                ".$where_keyword."
                                AND ticket_head.noidentitas != '1234'
                                AND ticket.status='1'
                                AND ticket.PCode IN ( SELECT a.PCode FROM groupswing a WHERE a.Status='A')
                                GROUP BY ticket_head.`notrans`
                            ORDER BY
                                ticket_head.Tanggal ASC,
                                ticket_head.notrans ASC    
                    ";
                    }
                    
                    //echo $q;
                    $counter = 0;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $notrans,
                            $Tanggal,
                            $qty,
                            $user,
                            $noidentitas,
                            $ttl_amount,
                            $nilai_tunai,
                            $nilai_debit,
                            $nilai_kredit,
                            $nilai_voucher,
                            $BankDebet,
                            $EDCBankDebet,
                            $BankKredit,
                            $EDCBankKredit,
                            $edited_user,
                            $edited_date
                        ) = $row;
                        
                        $arr_data["list_data"][$counter] = $counter;
                        $arr_data["data_notrans"][$counter] = $notrans;
                        $arr_data["data_Tanggal"][$counter] = $Tanggal;
                        $arr_data["data_qty"][$counter] = $qty;
                        $arr_data["data_user"][$counter] = $user;
                        $arr_data["data_noidentitas"][$counter] = $noidentitas;
                        $arr_data["data_ttl_amount"][$counter] = $ttl_amount;
                        $arr_data["data_nilai_tunai"][$counter] = $nilai_tunai;
                        $arr_data["data_nilai_debit"][$counter] = $nilai_debit;
                        $arr_data["data_nilai_kredit"][$counter] = $nilai_kredit;
                        $arr_data["data_nilai_voucher"][$counter] = $nilai_voucher;
                        $arr_data["data_bank_debet"][$counter] = $BankDebet;
                        $arr_data["data_edc_bank_debet"][$counter] = $EDCBankDebet;
                        $arr_data["data_bank_kredit"][$counter] = $BankKredit;
                        $arr_data["data_edc_bank_kredit"][$counter] = $EDCBankKredit;
                        $arr_data["data_edited_date"][$counter] = $edited_user;
                        $arr_data["data_edited_user"][$counter] = $edited_date;
                        
                        $arr_data["list_notrans"][$notrans] = $notrans;
                        
                        $counter++;
                    }
                    
                    if(count($arr_data["list_notrans"])*1>0)
                    {
                        $where_notrans = where_array($arr_data["list_notrans"],"ticket.notrans", "in");
                        
                        $q = "
                                SELECT
                                    ticket.notrans,
                                    ticket.jenis
                                FROM
                                    ticket
                                WHERE
                                    1
                                    ".$where_notrans."
                                GROUP BY
                                    ticket.notrans
                                ORDER BY
                                    ticket.notrans ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $notrans,
                                $jenis
                            ) = $row;
                            
                            $arr_data["jenis"][$notrans] = $jenis;
                        } 
                    }
                    
                    foreach($arr_data["list_data"] as $counter=>$val)
                    {
                        $notrans = $arr_data["data_notrans"][$counter];
                        $Tanggal = $arr_data["data_Tanggal"][$counter];
                        $qty = $arr_data["data_qty"][$counter];
                        $user = $arr_data["data_user"][$counter];
                        $ttl_amount = $arr_data["data_ttl_amount"][$counter];
                        $nilai_tunai = $arr_data["data_nilai_tunai"][$counter];
                        $nilai_debit = $arr_data["data_nilai_debit"][$counter];
                        $nilai_kredit = $arr_data["data_nilai_kredit"][$counter];  
                        $nilai_voucher = $arr_data["data_nilai_voucher"][$counter];  
                        $BankDebet = $arr_data["data_bank_debet"][$counter];
                        $EDCBankDebet = $arr_data["data_edc_bank_debet"][$counter];
                        $BankKredit = $arr_data["data_bank_kredit"][$counter];
                        $EDCBankKredit = $arr_data["data_edc_bank_kredit"][$counter];
                        
                        $jenis = $arr_data["jenis"][$notrans];
                        
                        if($v_jenis=="")
                        {
                            $arr_data["list_data_oke"][$counter] = $counter;
                        }
                        else if($v_jenis==1)
                        {
                            if($jenis==1)
                            {
                                $arr_data["list_data_oke"][$counter] = $counter;    
                            }   
                        }
                        else if($v_jenis==2)
                        {
                            if($jenis==2)
                            {
                                $arr_data["list_data_oke"][$counter] = $counter;    
                            }   
                        }
                    }
                    
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="12">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="12">REPORT KASIR TICKET</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="12">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    
                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                            <tr class="title_table">
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">No</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">No Trans</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Tanggal</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Jenis</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Kasir</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">No Identitas</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Qty</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Tunai</td>
                                <td colspan="3" style="vertical-align: middle;text-align: center;">Debit</td>
                                <td colspan="3" style="vertical-align: middle;text-align: center;">Kredit</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Voucher</td>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Total</td>
                                <?php 
                                    if(!$btn_excel)
                                    {
                                ?>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Edit</td>
                                <?php
                                        if($ses_login=="trisno1402" || $ses_login=="tonny1205" || $ses_login=="frangky2311")
                                        {
                                ?>
                                <td rowspan="2" style="vertical-align: middle;text-align: center;">Del</td>
                                <?php 
                                        }
                                    }
                                ?>
                            </tr>
                                
                            <tr class="title_table">
                                <td style="vertical-align: middle;text-align: center;">Rp.</td>
                                <td style="vertical-align: middle;text-align: center;">Kartu</td>
                                <td style="vertical-align: middle;text-align: center;">EDC</td>

                                <td style="vertical-align: middle;text-align: center;">Rp.</td>
                                <td style="vertical-align: middle;text-align: center;">Kartu</td>
                                <td style="vertical-align: middle;text-align: center;">EDC</td>
                            </tr>
                        
                        <tbody style="color: black;">
                            <?php 
                               
                                $no = 1;
                                
                                foreach($arr_data["list_data_oke"] as $counter=>$val)
                                {
                                    $notrans = $arr_data["data_notrans"][$counter];
                                    $Tanggal = $arr_data["data_Tanggal"][$counter];
                                    $qty = $arr_data["data_qty"][$counter];
                                    $user = $arr_data["data_user"][$counter];
                                    $noidentitas = $arr_data["data_noidentitas"][$counter];
                                    $ttl_amount = $arr_data["data_ttl_amount"][$counter];
                                    $nilai_tunai = $arr_data["data_nilai_tunai"][$counter];
                                    $nilai_debit = $arr_data["data_nilai_debit"][$counter];
                                    $nilai_kredit = $arr_data["data_nilai_kredit"][$counter];   
                                    $nilai_voucher = $arr_data["data_nilai_voucher"][$counter];   
                                    $jenis = $arr_data["jenis"][$notrans];
                                    $edited_user = $arr_data["data_edited_date"][$counter];
                                    $edited_date = $arr_data["data_edited_user"][$counter];
                                    $BankDebet = $arr_data["data_bank_debet"][$counter];
                                    $EDCBankDebet = $arr_data["data_edc_bank_debet"][$counter];
                                    $BankKredit = $arr_data["data_bank_kredit"][$counter];
                                    $EDCBankKredit = $arr_data["data_edc_bank_kredit"][$counter];
                                    
                                    $nilai_all = $nilai_tunai + $nilai_debit + $nilai_kredit + $nilai_voucher;
                                    
                                    if($jenis==1)
                                    {
                                        $echo_jenis = "Lokal";
                                    }
                                    else if($jenis==2)
                                    {
                                        $echo_jenis = "Asing";
                                    }
                                    
                                    $bg_color = "";
                                    if($nilai_all*1!=$ttl_amount*1)
                                    {
                                        $bg_color = "bgcolor='#ffff00;'";    
                                    }
                                    
                                    $bg_color_edit = "";
                                    if($edited_user)
                                    {
                                        $bg_color_edit = "bgcolor='#4da6ff;'";
                                    }
                                    
                                    ?>
                                    
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td <?php echo $bg_color_edit; ?>><?php echo $no; ?></td>
                                        <td <?php echo $bg_color_edit; ?> title="<?php echo $edited_user."-".format_show_datetime($edited_date); ?>" ><?php echo $notrans; ?></td>
                                        <td <?php echo $bg_color; ?>><?php echo format_show_datetime($Tanggal); ?></td>
                                        <td <?php echo $bg_color; ?>><?php echo $echo_jenis; ?></td>
                                        <td <?php echo $bg_color; ?>><?php echo $user; ?></td>
                                        <td <?php echo $bg_color; ?>><?php echo $noidentitas; ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo number_format($qty, 0, ",", "."); ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo number_format($nilai_tunai, 0, ",", "."); ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo number_format($nilai_debit, 0, ",", "."); ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo $BankDebet; ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo $EDCBankDebet; ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo number_format($nilai_kredit, 0, ",", "."); ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo $BankKredit; ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo $EDCBankKredit; ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo number_format($nilai_voucher, 0, ",", "."); ?></td>
                                        <td <?php echo $bg_color; ?> style="text-align: right;"><?php echo number_format($ttl_amount, 0, ",", "."); ?></td>
                                        <?php 
                                            if(!$btn_excel)
                                            {
                                                
                                        ?> 
                                        <td style="text-align: center;" onclick="pop_edit('<?php echo $notrans; ?>','<?php echo $ttl_amount;?>')"><img src="<?php echo $base_url; ?>inventory/images/edit2.png" alt=""></td>
                                        
                                        <?php 
                                                if($ses_login=="trisno1402" || $ses_login=="tonny1205" || $ses_login=="frangky2311" || $ses_login=="mechael0101" || $ses_login=="irfan1407" || $ses_login=="yuri0717")
                                                {
                                        ?>
                                        <td style="text-align: center;" onclick="pop_delete('<?php echo $notrans; ?>')"><img src="<?php echo $base_url; ?>inventory/images/cross.gif" alt=""></td>
                                        <?php
                                                } 
                                            }
                                        ?>
                                    </tr>
                                    <?php
                                    
                                    $arr_total["qty"] += $qty;
                                    $arr_total["nilai_tunai"] += $nilai_tunai;
                                    $arr_total["nilai_debit"] += $nilai_debit;
                                    $arr_total["nilai_kredit"] += $nilai_kredit;
                                    $arr_total["nilai_voucher"] += $nilai_voucher;
                                    $arr_total["ttl_amount"] += $ttl_amount;
                                    $no++;
                                }
                            ?>
                        </tbody>
                        
                            <tr style="text-align: right; color: black; font-weight: bold;">
                                <td colspan="6">GRAND TOTAL</td>
                                <td><?php echo number_format($arr_total["qty"], 0, ",", "."); ?></td>
                                <td><?php echo number_format($arr_total["nilai_tunai"], 0, ",", "."); ?></td>
                                <td><?php echo number_format($arr_total["nilai_debit"], 0, ",", "."); ?></td>
                                <td><?php echo "-"; ?></td>
                                <td><?php echo "-"; ?></td>
                                <td><?php echo number_format($arr_total["nilai_kredit"], 0, ",", "."); ?></td>
                                <td><?php echo "-"; ?></td>
                                <td><?php echo "-"; ?></td>
                                <td><?php echo number_format($arr_total["nilai_voucher"], 0, ",", "."); ?></td>
                                <td><?php echo number_format($arr_total["ttl_amount"], 0, ",", "."); ?></td>
                                <?php 
                                    if(!$btn_excel)
                                    {
                                ?>
                                <td>&nbsp;</td>
                                
                                <?php 
                                        if($ses_login=="yulia2107" || $ses_login=="windha" || $ses_login=="hendri1003" || $ses_login=="frangky2311" || $ses_login=="mart2005" || $ses_login=="febri0202")
                                        {
                                ?>
                                <td>&nbsp;</td>
                                <?php 
                                        }
                                    }
                                ?>
                            </tr>   
                    </table>
                    <?php
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
			
		
		</div>
		
		</form>
		
<?php 
                    include("footer.php"); 
                }
?>