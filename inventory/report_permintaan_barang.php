<?php               
    include("header.php");
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Permintaan Barang";
    
    
    if($v_date_from=="")
    {
        $v_date_from = date("d/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("d/m/Y");
    }
    
    $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdGudang, $NamaGudang) = $row;
        
        $arr_data["list_gudang"][$KdGudang] = $KdGudang;
        $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
    }
    
    $q = "
            SELECT
                ".$db["master"].".gudang_admin.KdGudang
            FROM
                ".$db["master"].".gudang_admin
            WHERE
                1
                AND ".$db["master"].".gudang_admin.UserName = '".$ses_login."'
            ORDER BY
                ".$db["master"].".gudang_admin.KdGudang ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdGudang, $NamaGudang) = $row;
        
        $arr_data["list_gudang_admin"][$KdGudang] = $KdGudang;
    }
    
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-persediaan-barang.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  
    </script>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="POST" name="theform" id="theform">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="100">Tanggal</th>
						<th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
					</tr>
                    
                   
                    <tr>
                        <th>Gudang</th>
                        <th>: 
                                
                                  <select class="form-control-new" name="v_KdGudang" id="v_KdGudang" style="width: 200px;">
                                    <?php 
                                        if(count($arr_data["list_gudang"])*1==count($arr_data["list_gudang_admin"])*1)
                                        {
                                    ?>
                                    <option value="">Semua</option>
                                    <?php 
                                        }
                                    ?>
                                    
                                    <?php 
                                        foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                                        {
                                            $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                        ?>
                                        <option <?php if($v_KdGudang==$KdGudang) echo "selected='selected'"; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                        <?php        
                                        }
                                    ?>
                                </select>     
                                  
                        </th>
                    </tr>
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
				</thead>
				
			</table> 
			<br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    $where_gudang = "";
                    if($v_KdGudang!="")
                    {
                        $where_gudang = " AND `gudang`.KdGudang = '".$v_KdGudang."' ";    
                    }
                    
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    else
                    {
                        $where_date = " AND `permintaan_barang_header`.TglDokumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";    
                    }
                    
                    $counter = 1;
                    $q = "
                            SELECT
                                `permintaan_barang_header`.NoDokumen,
                                `permintaan_barang_header`.TglDokumen,
                                `gudang`.KdGudang,
                                `gudang`.Keterangan AS NamaGudang,
                                `divisi`.KdDivisi,
                                `divisi`.NamaDivisi,
                                `permintaan_barang_header`.Keterangan,
                                `masterbarang`.PCode,
                                `masterbarang`.NamaLengkap,
                                `permintaan_barang_detail`.Qty,
                                `permintaan_barang_detail`.Satuan
                            FROM
                                `permintaan_barang_header`
                                INNER JOIN `permintaan_barang_detail` ON
                                    `permintaan_barang_header`.NoDokumen =  `permintaan_barang_detail`.NoDokumen
                                    AND `permintaan_barang_header`.Status = '1'
                                    ".$where_date."
                                INNER JOIN `gudang` ON
                                    `permintaan_barang_header`.KdGudang = `gudang`.KdGudang
                                    ".$where_gudang."    
                                INNER JOIN `divisi` ON
                                    `permintaan_barang_header`.KdDivisi = `divisi`.KdDivisi
                                INNER JOIN `masterbarang` ON
                                    `masterbarang`.PCode = `permintaan_barang_detail`.PCode
                            WHERE
                                1
                            ORDER BY
                                `permintaan_barang_header`.NoDokumen ASC,
                                `permintaan_barang_header`.TglDokumen ASC
                    ";
                    
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $NoDokumen,
                            $TglDokumen,
                            $KdGudang,
                            $NamaGudang,
                            $KdDivisi,
                            $NamaDivisi,
                            $Keterangan,
                            $PCode,
                            $NamaLengkap,
                            $Qty,
                            $Satuan
                        ) = $row;
                        
                        $arr_data["list_data"][$counter] = $counter;
                        
                        $arr_data["data_NoDokumen"][$counter] = $NoDokumen;
                        $arr_data["data_TglDokumen"][$counter] = $TglDokumen;
                        $arr_data["data_KdGudang"][$counter] = $KdGudang;
                        $arr_data["data_NamaGudang"][$counter] = $NamaGudang;
                        $arr_data["data_KdDivisi"][$counter] = $KdDivisi;
                        $arr_data["data_NamaDivisi"][$counter] = $NamaDivisi;
                        $arr_data["data_Keterangan"][$counter] = $Keterangan;
                        $arr_data["data_PCode"][$counter] = $PCode;
                        $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                        $arr_data["data_Qty"][$counter] = $Qty;
                        $arr_data["data_Satuan"][$counter] = $Satuan;
                        
                        $arr_data["list_NoPB"][$NoDokumen] = $NoDokumen;
                        
                        $counter++;
                    }
                    
                    if(count($arr_data["list_NoPB"])*1>0)
                    {
                        $where_NoPB = where_array($arr_data["list_NoPB"], "NoPb", "in");
                        
                        $q = "
                                SELECT
                                    NoPb,
                                    PCode,
                                    SUM(Qty) AS Qty
                                FROM
                                    trans_mutasi_header
                                WHERE
                                    1
                                    AND trans_mutasi_header.Status = '1'
                                    ".$where_NoPB."
                                GROUP BY
                                    NoPb,
                                    PCode
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($NoPB, $PCode, $Qty) = $row;
                            
                            $arr_data["Qty_Mutasi"][$NoPB][$PCode] = $Qty;
                        }
                    }
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="11">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="11">REPORT PERMINTAAN BARANG</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="11">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    
                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NoDokumen</th>
                                <th>Tanggal</th>
                                <th>Gudang</th>
                                <th>Divisi</th>
                                <th>Keterangan</th>
                                <th>PCode</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                                <th>Status</th>
                            </tr>
                            
                        </thead>
                        
                        <tbody style="color: black;">
                        <?php 
                            foreach($arr_data["list_data"] as $counter=>$val)
                            {
                                
                                $NoDokumen = $arr_data["data_NoDokumen"][$counter];
                                $TglDokumen = $arr_data["data_TglDokumen"][$counter];
                                $KdGudang = $arr_data["data_KdGudang"][$counter];
                                $NamaGudang = $arr_data["data_NamaGudang"][$counter];
                                $KdDivisi = $arr_data["data_KdDivisi"][$counter];
                                $NamaDivisi = $arr_data["data_NamaDivisi"][$counter];
                                $Keterangan = $arr_data["data_Keterangan"][$counter];
                                $PCode = $arr_data["data_PCode"][$counter];
                                $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                                $Qty = $arr_data["data_Qty"][$counter];
                                $Satuan = $arr_data["data_Satuan"][$counter];
                                
                                if($arr_data["Qty_Mutasi"][$NoDokumen][$PCode]*1>=$Qty)
                                {
                                    $status="Terkirim";
                                }
                                else
                                {
                                    $status = "Menunggu";     
                                }
                                
                                if($status=="Terkirim")
                                {
                                    $style_status = "background: green; color: white;";
                                }
                                else if($status=="Menunggu")
                                {
                                    $style_status = "background: yellow; color: red;";
                                }
                                
                        ?>
                            <tr style="<?php echo $bg_color; ?>" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                <td><?php echo $counter; ?></td>
                                <td><?php echo $NoDokumen; ?></td>
                                <td><?php echo format_show_date($TglDokumen); ?></td>
                                <td><?php echo $NamaGudang; ?></td>
                                <td><?php echo $NamaDivisi; ?></td>
                                <td><?php echo $Keterangan; ?></td>
                                <td><?php echo $PCode; ?></td>
                                <td><?php echo $NamaLengkap; ?></td>
                                <td align="right"><?php echo format_number($Qty); ?></td>
                                <td><?php echo $Satuan; ?></td>
                                <td style="<?php echo $style_status; ?>"><?php echo $status; ?></td>
                            </tr>
                        <?php 
                                $arr_total["Qty"] += $Qty;
                            }
                        ?>
                        </tbody>
                        
                        <tfoot>
                            <tr style="text-align: right; font-weight: bold; color: black;">
                                <td colspan="8">Grand Total</td>
                                <td align="right"><?php echo format_number($arr_total["Qty"]); ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
			
		
		</div>
		
		</form>
		
<?php 
                    include("footer.php"); 
                }
?>