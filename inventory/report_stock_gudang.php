<?php
include("header.php");

$modul            = "ReportStock";
$file_name        = "report_stock_gudang.php";

$btn_submit = $_POST['btn_submit'];
$btn_excel = $_POST['btn_excel'];
$user = $ses_login;
//echo "USER = ".$user;

$q1 = "SELECT g.KdGudang, g.Keterangan
					FROM gudang g
					INNER JOIN gudang_admin a
						ON g.KdGudang=a.KdGudang AND a.UserName='".$user."'
					WHERE 1
					ORDER BY g.KdGudang";
$qry = mysql_query($q1);
//echo $q1;
$numgudang = mysql_num_rows($qry);

$qry2 = mysql_query("SELECT KdGudang FROM gudang order by KdGudang DESC");
$numgudang2 = mysql_num_rows($qry2);

	if($_REQUEST['search']!=''||$_POST['searchid']!=''){		 	
		$tahunbaru 	= $_POST['tahun'];
		$bulanbaru 	= $_POST['bulan'];
		$gudang 	= $_POST['gudang'];		
		if($numgudang==$numgudang2){
			$vargudang = 1;
		}
		else{
				$vargudang = 2;
		}
		$qryx = mysql_query($q1);
	}
	else{
	
			$qryx = mysql_query($q1);
			$tahunbaru 	= date("Y");
			$bulanbaru 	= date("m");		
			if($numgudang==$numgudang2){
				$vargudang = 1;
			}
			else{
					$hsl2 = mysql_fetch_array($qry2);
					$gudang 	= $hsl2[0];
					$vargudang = 2;
			}			
	}

if ($btn_excel) {
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="ReportStock.xls"');
	
	$class_css = " border='1'";

	$bul = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');	
	$bulx = (int) $bulanbaru;
	
	echo "Report Stock ".$bul[$bulx]." $tahunbaru<br>";
	if($gudang!="SEMUA"){
		$namagudang = cariNama("Keterangan","gudang","KdGudang",$gudang,$where);
		echo "Gudang : $gudang - $namagudang<br>";
	}
	else{
			echo "Gudang : SEMUA<br>";
	}	
	
}
else{	
		$class_css = "class='table table-bordered responsive in_table'";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Gudang</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>
	<script>
		function PopMutasi(param){
			window.open(param,'','scrollbars=yes,width=800,height=600,left=250,top=100');		
		}
	</script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<form method="POST"  name="search" action="" >
			<div class="row">
				<div class="col-md-12">
			
				<input type="hidden" id="searchid" name="searchid" value = "2">
				<table align='center' class="table table-bordered responsive">
			
			<tr bordercolor="#FFFFFF"> 
			  <td class="title_table" style="width: 100px;"> Periode </td>
			  <td> 				
					<?
					echo "<input type='hidden' id='bulan_hide' name='bulan_hide' size='5' value='$bulanbaru'>";
					?>  
				<select size="1" height="1" name ="bulan" id="bulan" class="form-control-new">
				<?
					$bulx = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
					for($bw=1 ; $bw<=12; $bw++){
						if(strlen($bw)==1){
							$bx = "0".$bw;
						}
						else{
							$bx = $bw;
						}
						if((int)$bulanbaru==(int)$bx){
							echo "<option selected value='$bx'>$bulx[$bw]</option>";
						}
						else{
								echo "<option value='$bx'>$bulx[$bw]</option>";		
						}

					}
				?>
				</select>				
				
					<?
					echo "<input type='hidden' id='tahun_hide' name='tahun_hide' size='5' value='$tahunbaru'>";
					?>  
					<select size="1" height="1" name ="tahun" id="tahun" class="form-control-new">
					<?
					$tahunawal = $tahunbaru;
					$tahunakhir = $tahunawal+2;
					for($i=date('Y'); $i>=date('Y')-32; $i-=1){
						echo"<option value='$i'> $i </option>";
						}
					?>
					</select>				
			  </td>
			</tr>
			
			<tr bordercolor="#FFFFFF"> 
			  <td class="title_table" style="width: 100px;"> Gudang </td>
			  <td> 				
				<?
					echo "<input type='hidden' id='gudang_hide' name='gudang_hide' size='5' value='$gudang'>";
				?>  
				<select size="1" height="1" name ="gudang" id="gudang" class="form-control-new">
				<?
					if($vargudang==1){
				?>
						<option select value='SEMUA'>SEMUA GUDANG</option>
				<?
					}
					//ECHO "BLAH";
					while($hsl=mysql_fetch_array($qryx)){
						if($gudang==$hsl[0]){
				?>
							<option selected value='<? echo $hsl[0];?>'><? echo $hsl[1];?></option>
				<?
						}
						else{
				?>
								<option select value='<? echo $hsl[0];?>'><? echo $hsl[1];?></option>
				<?
						}
					}					
				?>
				</select>							
			  </td>
			</tr>			

					<tr bordercolor="#FFFFFF">
						<td>&nbsp;</td>
						<td>
							<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_submit" id="btn_submit"  value="Submit">Submit<i class="entypo-search"></i></button>
							<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_excel" id="btn_excel"  value="Excel">Excel<i class="entypo-search"></i></button>
                        </td>
					</tr>
				</table>
			
				</div>
			</div>
		</form>

<?
}

if($btn_excel || $btn_submit){    
	if($btn_submit){
?>
		<hr>
<?
	}
?>
<table <? echo $class_css;?>>
			<tr class="title_table">
	            <td width="30" rowspan='2' style="vertical-align: middle; text-align: center;">PCode</td>
	            <td rowspan='2' style="vertical-align: middle; text-align: center;">Nama Barang</td>
	            <td colspan='4' align='center'>Saldo</td>
			</tr>
			<tr class="title_table">
	            <td align='center'>Awal</td>
	            <td align='center'>Masuk</td>
                <td align='center'>Keluar</td>    
	            <td align='center'>Akhir</td>                   
			</tr>
<?

	$bulan_now = $bulanbaru;
	$tahun_now = $tahunbaru;

	$gawal 		= "GAwal".$bulan_now;
	$gmasuk 	= "GMasuk".$bulan_now;
	$gkeluar 	= "GKeluar".$bulan_now;
	$gakhir 	= "GAkhir".$bulan_now;
	
	if($gudang=="SEMUA"){
		$wheregudang = "";
		$wheremasterbarang = "";
	}
	else{
			$wheregudang = " AND KdGudang = '$gudang'";
			$wheremasterbarang = " AND KdSubKategori IN (SELECT KdSubKategori FROM gudang_subkategori WHERE KdGudang = '$gudang')";
	}	
	
	$q = "SELECT PCode, 
			SUM(awal) as awal,
			SUM(masuk) as masuk,
			SUM(keluar) as keluar,
			SUM(akhir) as akhir
			FROM(
				SELECT PCode, $gawal as awal, $gmasuk as masuk, $gkeluar as keluar, $gakhir as akhir 
				FROM stock 
				WHERE 1
				AND Tahun='$tahun_now'
				$wheregudang
				ORDER BY PCode
			) as a
			GROUP BY PCode";
			
			if($user=='krisna337'){
				//echo $q;
			}
			
	$qry = mysql_query($q) or die("$q ".mysql_error());
	while($row=mysql_fetch_array($qry)){
		list($PCode,$Awal,$Masuk,$Keluar,$Akhir) = $row;
	
		$arr_data["stock_pcode"][$PCode] = $PCode;
		$arr_data["stock_awal"][$PCode] = $Awal;
		$arr_data["stock_masuk"][$PCode] = $Masuk;
		$arr_data["stock_keluar"][$PCode] = $Keluar;
		$arr_data["stock_akhir"][$PCode] = $Akhir;

	}				

	$q = "SELECT PCode, NamaLengkap 
			FROM masterbarang
			WHERE 1
			$wheremasterbarang
			ORDER BY PCode ASC";
	//echo "$q<hr>";
	if($user=='krisna337'){
				echo $q;
			}
	$qry = mysql_query($q) or die("$q ".mysql_error());
	$num_masterbarang = mysql_num_rows($qry);
	//echo "NUM = ".$num_masterbarang;

	if($num_masterbarang==0){
?>
		<tbody style="color: black;">	
			<tr>
				<td nowrap colspan='6' align='center'>Tidak ada Data</td>
			</tr>
		</tbody>
<?		
	}
	else{
	
		while($hsl=mysql_fetch_array($qry)){
			$pcode 		= $hsl['PCode'];
			$namabarang = $hsl['NamaLengkap'];
		
			$awal = $arr_data["stock_awal"][$pcode];
			$masuk = $arr_data["stock_masuk"][$pcode];
			$keluar = $arr_data["stock_keluar"][$pcode];
			$akhir = $arr_data["stock_akhir"][$pcode];
			
			$link = "report_stock_gudang_detail.php?tahun=$tahun_now&bulan=$bulan_now&gudang=$gudang&pcode=$pcode&namabarang=$namabarang&awal=$awal";
		
?>		
			<tbody style="color: black;">	
				<tr>
					<td nowrap>&nbsp;<? echo $pcode;?></td>
					<td nowrap><span style="cursor:pointer" onclick="PopMutasi('<? echo $link;?>');"><? echo $namabarang;?></span></td>
					<td nowrap align='right'><? echo ubah_format('qty',$awal);?></td>
					<td nowrap align='right'><? echo ubah_format('qty',$masuk);?></td>
					<td nowrap align='right'><? echo ubah_format('qty',$keluar);?></td>
					<td nowrap align='right'><? echo ubah_format('qty',$akhir);?></td>
				</tr>
			</tbody>
<?				
			$tot_awal += $awal;
			$tot_masuk += $masuk;
			$tot_keluar += $keluar;
			$tot_akhir += $akhir;

		}	
?>	
			<tr class="title_table">
				<td colspan='2' nowrap align='center'>TOTAL</td>
				<td nowrap align='right'><? echo ubah_format('qty',$tot_awal);?></td>
				<td nowrap align='right'><? echo ubah_format('qty',$tot_masuk);?></td>
				<td nowrap align='right'><? echo ubah_format('qty',$tot_keluar);?></td>
				<td nowrap align='right'><? echo ubah_format('qty',$tot_akhir);?></td>
			</tr>			
<?
	}
?>
</table>
<?	
}

function cariNama($field,$tabel,$wherefield,$kode,$where){
	$q = mysql_query("select $field from $tabel where $wherefield='$kode' $where");
	$exe = mysql_fetch_array($q);
	
	return $exe[0];
}

function ubah_format($param,$harga){
	if($param=="qty"){
		$s = number_format($harga, 0, ',', '.');
	}
	elseif($param=="netto"){
		$s = number_format($harga, 2, ',', '.');
	}
	return $s;
}

function CekBulan($data){
	if($data=="01"){
		$hasil = "Januari";
	}
	elseif($data=="02"){
		$hasil = "Februari";
	}
	elseif($data=="03"){
		$hasil = "Maret";
	}
	elseif($data=="04"){
		$hasil = "April";
	}
	elseif($data=="05"){
		$hasil = "Mei";
	}
	elseif($data=="06"){
		$hasil = "Juni";
	}
	elseif($data=="07"){
		$hasil = "Juli";
	}
	elseif($data=="08"){
		$hasil = "Agustus";
	}
	elseif($data=="09"){
		$hasil = "September";
	}
	elseif($data=="10"){
		$hasil = "Oktober";
	}
	elseif($data=="11"){
		$hasil = "November";
	}
	elseif($data=="12"){
		$hasil = "Desember";
	}	
	return $hasil;
}

if(!$btn_excel){
	include("footer.php");
}
?>