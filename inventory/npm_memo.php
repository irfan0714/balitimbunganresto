<?php 
include("header.php");

$modul = "Memo";

if(!isset($_GET["id"])){ $id = isset($_GET["id"]); } else { $id = $_GET["id"]; }
if(!isset($_GET["action"])){ $action = isset($_GET["action"]); } else { $action = $_GET["action"]; }


if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }


$q = "SELECT d.KdDivisi, d.NamaDivisi, s.Keterangan, s.Minimum 
		FROM divisi d
		LEFT JOIN (SELECT KdDivisi, Keterangan, Minimum, status FROM memo_struk WHERE status='Y')AS s
			ON d.KdDivisi=s.KdDivisi
		WHERE 1	
		AND d.KdDivisi IN(1,2,3)
		ORDER BY d.KdDivisi";
$qry = mysql_query($q);
while($row=mysql_fetch_array($qry)){
	list($KdDivisi, $NamaDivisi, $Keterangan, $Minimum) = $row;
	
	$arr_data["list_divisi"][$KdDivisi] = $KdDivisi;
	$arr_data["divisi_namadivisi"][$KdDivisi] = $NamaDivisi;
	$arr_data["divisi_keterangan"][$KdDivisi] = $Keterangan;
	$arr_data["divisi_minimum"][$KdDivisi] = $Minimum;
	
}

if($btn_save)
{
    if(!isset($_POST["v_subject"])){ $v_subject = isset($_POST["v_subject"]); } else { $v_subject = $_POST["v_subject"]; }
    if(!isset($_POST["v_content"])){ $v_content = isset($_POST["v_content"]); } else { $v_content = $_POST["v_content"]; }
	$ganti = $_POST["ganti"];
	
    if($action=="")
    {
        $memo_id = get_counter_int("","memo","memo_id",100);
		
		if($ganti=="ya"){
		
			foreach($arr_data["list_divisi"] as $KdDivisi=>$val){
				$minimum  		= $_POST["minimum_".$KdDivisi];
				$ket1 			= $_POST["ket1_".$KdDivisi];
				$ket2 			= $_POST["ket2_".$KdDivisi];
				$ket3 			= $_POST["ket3_".$KdDivisi];
				
				$keterangan = $ket1."||".$ket2."||".$ket3;
				
				if($minimum!=="" and $ket1!="" and $ket2!="" and $ket3!=""){
					$sel = mysql_query("SELECT * FROM memo_struk WHERE KdDivisi='".$KdDivisi."' AND status='Y'");
					$num = mysql_num_rows($sel);
					if($num>0){
						$hsl = mysql_fetch_array($sel);
						$idmem = $hsl['sid'];
						$update = "UPDATE memo_struk SET status='T' WHERE sid = '".$idmem."'";
						//echo $update."<br>";
						mysql_query($update) or die("$update ".mysql_error());
					}
						
						$insert = "INSERT INTO memo_struk(`sid`,`KdDivisi`,`Keterangan`,`Minimum`,`status`)
									VALUES(NULL,'".$KdDivisi."','".$keterangan."','".$minimum."','Y')";
						//echo $insert."<br>";			
						mysql_query($insert) or die("$insert ".mysql_error());			
				}
			}
			
		}
        
        $q = "
                INSERT INTO
                    memo
                SET
                    memo_id = '".$memo_id."',
                    memo_date = NOW(), 
                    subject = '".$v_subject."',
                    content = '".$v_content."',
                    author_date = NOW(),
                    author_user = '".$ses_login."',
                    edited_date = NOW(),
                    edited_user = '".$ses_login."'
        "; 
        if(!mysql_query($q))
        {
            die("Gagal Insert");
        }   
        else
        {
            header("Location: npm_memo.php?action=edit&id=".$memo_id);        
        }
    }
    else if($action=="edit")
    {
	
		if($ganti=="ya"){
		
			foreach($arr_data["list_divisi"] as $KdDivisi=>$val){
				$minimum  		= $_POST["minimum_".$KdDivisi];
				$ket1 			= $_POST["ket1_".$KdDivisi];
				$ket2 			= $_POST["ket2_".$KdDivisi];
				$ket3 			= $_POST["ket3_".$KdDivisi];
				
				$keterangan = $ket1."||".$ket2."||".$ket3;
				
				if($minimum!=="" and $ket1!="" and $ket2!="" and $ket3!=""){
					$sel = mysql_query("SELECT * FROM memo_struk WHERE KdDivisi='".$KdDivisi."' AND status='Y'");
					$num = mysql_num_rows($sel);
					if($num>0){
						$hsl = mysql_fetch_array($sel);
						$idmem = $hsl['sid'];
						$update = "UPDATE memo_struk SET status='T' WHERE sid = '".$idmem."'";
						//echo $update."<br>";
						mysql_query($update) or die("$update ".mysql_error());
					}
						
						$insert = "INSERT INTO memo_struk(`sid`,`KdDivisi`,`Keterangan`,`Minimum`,`status`)
									VALUES(NULL,'".$KdDivisi."','".$keterangan."','".$minimum."','Y')";
						//echo $insert."<br>";			
						mysql_query($insert) or die("$insert ".mysql_error());			
				}
			}
			
		}
	
        $q = "
                UPDATE
                    memo
                SET
                    subject = '".$v_subject."',
                    content = '".$v_content."',
                    edited_date = NOW(),
                    edited_user = '".$ses_login."'
                WHERE
                    1
                    AND memo_id = '".$id."'
        "; 
        if(!mysql_query($q))
        {
            die("Gagal Update");
        }   
        else
        {
            header("Location: npm_memo.php?action=edit&id=".$memo_id);        
        }    
    }
    else if($action=="delete")
    {
        $q = "
                DELETE FROM
                    memo
                WHERE
                    1
                    AND memo_id = '".$id."'
        "; 
        if(!mysql_query($q))
        {
            die("Gagal Delete");
        }   
        else
        {
            ?>
                <script>
                    window.opener.location.reload();
                    window.close();
                </script>
            <?php
        }        
    }
}

$q = "
        SELECT 
            subject,
            content
        FROM
            memo
        WHERE
            1
            AND memo_id = '".$id."'
        LIMIT
            0,1
";
$qry = mysql_query($q);
$row = mysql_fetch_array($qry);
list($subject, $content) = $row;

?>  
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Neon Admin Panel" />
<meta name="author" content="" />
<title><?php echo $modul; ?> - NPM</title>
<link rel="shortcut icon" href="public/images/Logosg.png" >
<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="assets/css/NotoSans.css">
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/neon-core.css">
<link rel="stylesheet" href="assets/css/neon-theme.css">
<link rel="stylesheet" href="assets/css/neon-forms.css">
<link rel="stylesheet" href="assets/css/custom.css">
<link rel="stylesheet" href="assets/css/skins/black.css">
<link rel="stylesheet" href="public/css/style.css">
<link rel="stylesheet" href="assets/css/my.css">

<script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="assets/js/js.js"></script> 

<style>
.title{
    background:#dddddd;
    text-align:center;
    font-weight:bold;
}

.link_menu2{
        text-decoration: underline;
        color: black;
}

.link_menu2:hover{
    text-decoration: none;
}

#wrapper{
    width: 100%;
    height: 450px;
    overflow-x: scroll;
    overflow-y: scroll;
}

.freeze table thead{
    position: fixed;
}

.title_table{
    background: #009490; color: white; font-weight: bold;
}
</style>

      
<script>
function change_onMouseOver (id) {
  document.getElementById(id).style.background = '#EEEEEE';
}

function change_onMouseOut (id) {
  document.getElementById(id).style.background = '#FFFFFF';
}

function start_page()
{
    document.getElementById('v_keyword').focus();
}

function validasi()
{
    if(document.getElementById('v_subject').value=="")
    {
        alert("Subjek harus diisi...");
        document.getElementById('v_subject').focus();
        return false;
    }
}

function targetBlank (url) {
    blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
}

function EnableDisableChecked(no){
    var chkall = document.getElementById("choose");
    var field = document.getElementsByName("choose[]");
    
    //alert(no);
    var curr;
    curr = no;
    
    if (chkall.checked == true){
        field[curr].checked = false ;
    }else if (chkall.checked == true){
        field[curr].checked = true ;
    }        
}

function startPage()
{
    document.getElementById('v_subject').focus();
}
</script>

</head>
<body onLoad="startPage()">
<div class="page-container sidebar-collapsed">
<div class="main-content">
    <form method="post" name="theform_csv" id="theform_csv" enctype="multipart/form-data" onsubmit="return validasi()">
    <table width="100%" class="table table-bordered responsive">
        <tr class="title_table">
            <td colspan="100%">Memo</td>
        </tr>
        
        <tr>
            <td class="title_table" width="100">Subjek</td>
            <td>
                <input type="text" name="v_subject" id="v_subject" value="<?php echo $subject; ?>" style="width: 100%;"> 
            </td>
        </tr>
        
        <tr>
            <td class="title_table">Isi</td>
            <td>
                <textarea cols="100%" rows="10" name="v_content" id="v_content"><?php echo $content; ?></textarea>
            </td>
        </tr>
		
		<tr class="title_table">
            <td colspan="100%">
			Tampilan Cetak Struk, Ganti Cetakan? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type='radio' name='ganti' id='ganti' value='ya'> Ya
			&nbsp;&nbsp;&nbsp;
			<input type='radio' name='ganti' id='ganti' value='tidak' checked> Tidak
			</td>
        </tr>
		<?
		foreach($arr_data["list_divisi"] as $KdDivisi=>$val){
		
			$NamaDivisi = $arr_data["divisi_namadivisi"][$KdDivisi];
			$Keterangan = $arr_data["divisi_keterangan"][$KdDivisi];
			$Minimum = $arr_data["divisi_minimum"][$KdDivisi];		
	
			$exp = explode("||",$Keterangan);
			$ket1 = $exp[0];
			$ket2 = $exp[1];
			$ket3 = $exp[2];
		?>
		<tr>
            <td class="title_table" style="vertical-align: middle;">
				<? echo $NamaDivisi;?>
			</td>
            <td>
				<input type='text' name='minimum_<? echo $KdDivisi;?>' id='minimum_<? echo $KdDivisi;?>' size='15' maxlength='8' value='<? echo $Minimum;?>'><br>
				<input type='text' name='ket1_<? echo $KdDivisi;?>' id='ket1_<? echo $KdDivisi;?>' size='50' maxlength='35' value='<? echo $ket1;?>'><br>
				<input type='text' name='ket2_<? echo $KdDivisi;?>' id='ket2_<? echo $KdDivisi;?>' size='50' maxlength='35' value='<? echo $ket2;?>'><br>
				<input type='text' name='ket3_<? echo $KdDivisi;?>' id='ket3_<? echo $KdDivisi;?>' size='50' maxlength='35' value='<? echo $ket3;?>'>				
			</td>
		</tr>
		<?
		}
		?>
        
        <tr>
            <td colspan="100%" align="center">
                <?php 
                    if($action=="edit" || $action=="")
                    {
                ?>
                <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                <?php 
                    }
                    
                    if($action=="delete")
                    {
                ?>
                <button type="submit" name="btn_save" id="btn_save" class="btn btn-danger btn-icon btn-sm icon-left" value="Hapus">Hapus<i class="entypo-check"></i></button>
                <?php
                    }
                ?>
            </td>
        </tr>
    </table>
    </form>
    
   
</div>
</div>
</body>
</html>
<?
function ubah_format($harga){
	$s = round($harga);
	//$s = number_format($harga, 2, ',', '.');
	return $s;
}

function ubah_awal($harga){
	$s = explode(".",$harga);
	$k = implode($s,"");
	$k = explode(",",$k);
	$s = implode($k,".");
	return $s;
}
?>
