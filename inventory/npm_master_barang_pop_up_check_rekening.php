<?php
include("header.php");

$modul   = "Master Barang - Check Nama Rekening";

if(!isset($_GET["v_NamaRekening"])){ $v_NamaRekening = isset($_GET["v_NamaRekening"]); } else { $v_NamaRekening = $_GET["v_NamaRekening"]; }                                                                                                                                        

if($v_NamaRekening=="")
{
    echo "Nama Rekening harus diisi";
    die();
}

	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
        
        function mouseover(target)
        {
            if(target.bgColor!="#cafdb5")
            {
                if (target.bgColor=='#ccccff')
                target.bgColor='#ccccff';
                else
                target.bgColor='#c1cdd8';
            }
        }

        function mouseout(target)
        {
            if(target.bgColor!="#cafdb5")
            {
                if (target.bgColor=='#ccccff')
                target.bgColor='#ccccff';
                else
                target.bgColor='#FFFFFF';
            }
        }

        function mouseclick(target, idobject, num)
        {
            //var pjg = document.getElementById(idobject + '_sum').innerHTML;
            for(i=0;i<num;i++)
            {
                if (document.getElementById(idobject+'_'+i) != undefined)
                {
                    document.getElementById(idobject+'_'+i).bgColor='#f5faff';
                    if (target.id == idobject+'_'+i)
                    target.bgColor='#ccccff';
                }
            }
        }

        function mouseclick1(target)
        {
            //var pjg = document.getElementById(idobject + '_sum').innerHTML;
            if(target.bgColor!="#cafdb5")
            {
                target.bgColor="#cafdb5";
            }
            else
            {
                target.bgColor="#FFFFFF";
            }
        }
        
        function get_choose(KdRekening, NamaRekening)
        {
        	//window.opener.document.forms[0]["v_NamaRekening"].value = KdRekening;
            //window.opener.document.forms[0]["v_namabarang"+<?php echo $v_nilai; ?>].value = nama_lengkap;
			//window.opener.document.forms[0]["v_satuan"+<?php echo $v_nilai; ?>].value = satuan;
            window.opener.document.getElementById("v_NamaRekening").value  = KdRekening + '-'+NamaRekening;
            //window.opener.document.getElementById("v_qty"+<?php echo $v_nilai; ?>).focus();
            
            self.close() ;
            return; 
        }
        
    </script>
</head>

<body class="page-body skin-black">
<form method="get">
<div class="page-container sidebar-collapsed">
	
	
	<div class="main-content">
    
		
		<div class="row">
		
              
              <br>
              <table class="table table-bordered responsive">
              <thead>
                <tr>
                    <th width="30">No</th>
                    <th><center>No Rekening</center></th>
                    <th><center>Nama Rekening</center></th>
                </tr>
                </thead>
                
                <tbody style="color: black;">
                
                <?php
                    $i = 1;
                    $arr_keyword[0] = "rekening.NamaRekening";
                
                    $where = search_keyword_or($v_NamaRekening, $arr_keyword);
                
                    $sql = "
                            SELECT 
                            	*
                            FROM 
                                ".$db["master"].".rekening 
                            WHERE
                                1
                                ".$where."
                               ORDER BY rekening.KdRekening ASC;
                    "; 
                    //echo $sql;die;
                    $qry = mysql_query($sql);
                    while($row = mysql_fetch_array($qry))
                    {                   
                        list($KdRekening, $NamaRekening ) = $row;
              ?>
                
               <tr onclick="get_choose('<?php echo trim($KdRekening); ?>','<?php echo trim($NamaRekening); ?>')" id="<?php echo $i; ?>">
                    <td><?php echo $i; ?></td>
                    <td align="center"><?php echo $KdRekening; ?></td>
                    <td><?php echo $NamaRekening; ?></td>
                </tr>
              <?php
                        $i++;
                    }
                    
                    if($i==1)
                    {
                        ?>
                            <tr>
                                <td colspan="100%" align="center" style="font-weight: bold;">Data tidak ada</td>
                            </tr>
                        <?php
                    }  
              ?>
                    
                </tbody>
                
               
              </table>
       	
       	</div>
</form>		
		
    
<?php mysql_close(); ?>