<?php
include("header.php");

$modul            = "STOCK OPNAME";
$file_current     = "npm_stock_opname.php";
$file_name        = "npm_stock_opname_data.php";

//$ses_login = "hendri1003";

if($v_date_from=="")
{
    $v_date_from = "01/".date("m/Y");
}

if($v_date_to=="")
{
    $v_date_to = date("t/m/Y");
}

if(!isset($_GET["id"])){ $id = isset($_GET["id"]); } else { $id = $_GET["id"]; }   
 
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
        function import_csv(NoDokumen)
        {
            windowOpener(400, 600, 'Import CSV', 'npm_stock_opname_pop_import_csv.php?v_NoDokumen='+NoDokumen, 'Import CSV')
        }
        
        function generate_excel(NoDokumen)
        {
            windowOpener(400, 600, 'Generate Excel', 'npm_stock_opname_pop_excel.php?v_NoDokumen='+NoDokumen, 'Generate Excel')
        }
    
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='search')
				{
                    search_keyword = document.getElementById("search_keyword").value;      
                    search_gudang = document.getElementById("search_gudang").value; 
                    v_date_from = document.getElementById("v_date_from").value; 
                    v_date_to = document.getElementById("v_date_to").value; 
                    
                    variabel += "&search_keyword="+search_keyword;
                    variabel += "&search_gudang="+search_gudang;
                    variabel += "&v_date_from="+v_date_from;
                    variabel += "&v_date_to="+v_date_to;
					
					variabel += "&v_NoDokumen_curr="+param1;

					//alert(param1);
					document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					document.getElementById("col-header").innerHTML   = "";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-search").innerHTML   = xmlhttp.responseText;

							if(param1)
							{
								CallAjaxForm('edit_data' ,param1);
							}
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='add_data')
				{
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='edit_data')
				{
					variabel += "&v_NoDokumen="+param1;

					//alert(variabel);
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function confirm_delete(name)
		{
			var r = confirm("Anda yakin ingin void "+name+" ? ");
			if(r)
			{
				document.getElementById("v_del").value = '1';
				document.getElementById("theform").submit();
			}
		}
        
        function confirm_unvoid(name)
        {
            var r = confirm("Anda yakin ingin UnVoid "+name+" ? ");
            if(r)
            {
                document.getElementById("v_undel").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve "+name+" ? ");
            if(r)
            {
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_reject(name)
        {
            var r = confirm("Anda yakin ingin Reject "+name+" ? ");
            if(r)
            {
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function muncul_reject()
        {
            document.getElementById("btn_approve").style.display = "none";    
            document.getElementById("btn_confirm_reject").style.display = "none";    
            
            document.getElementById("v_Approval_Remarks").style.display = "";    
            document.getElementById("btn_reject").style.display = "";    
            
            document.getElementById("v_Approval_Remarks").focus();
        }
        

		function CheckAll(param, target)
		{
			var field = document.getElementsByName(target);
			var chkall = document.getElementById(param);
			if (chkall.checked == true)
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = true ;
			}else
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = false ;
			}
		}

		function reform(val)
		{
			var a = val.split(",");
			var b = a.join("");
			//alert(b);
			return b;
		}

		function format(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(0);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format4(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(4);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format2(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(2);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format6(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(6);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function addSeparatorsNF(nStr, inD, outD, sep)
		{
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1)
			{
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr))
			{
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}

		function toFormat4(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format4(document.getElementById(id).value);
		}

		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
		}

		function toFormat6(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format6(document.getElementById(id).value);
		}

		function isanumber(id)
		{
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				document.getElementById(id).value=0;
			}
			else
			{
				document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
			}
		}

		function start_page()
		{
            try
            {
			    document.getElementById("search_keyword").focus();
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
		}

		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
        
        function copy_QtyProgram(PCode)
        {
            QtyProgram = document.getElementById("v_QtyProgram_"+PCode).value;
            
            document.getElementById("v_QtyFisik_"+PCode).value = QtyProgram;    
        }
        
        function calculate(PCode)
        {
            QtyProgram = reform(document.getElementById("v_QtyProgram_"+PCode).value);
            QtyFisik = reform(document.getElementById("v_QtyFisik_"+PCode).value);
            
            selisih = (QtyFisik*1) - (QtyProgram*1);
            
            document.getElementById("td_selisih_"+PCode).innerHTML = format2(selisih);
            document.getElementById("v_selisih_"+PCode).value = selisih;
        }
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Transaksi - Persediaan</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-10">
				Pencarian
                &nbsp;
					<input type="text" class="form-control-new" size="20" maxlength="30" name="search_keyword" id="search_keyword">
                &nbsp;
                Gudang
                <select name="search_gudang" id="search_gudang" class="form-control-new" style="width: 200px;">
                    <option value="">Semua</option>
                    <?php 
                        $q = "
                            SELECT
                                ".$db["master"].".gudang.KdGudang,
                                ".$db["master"].".gudang.Keterangan
                            FROM
                                ".$db["master"].".gudang
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".gudang.KdGudang ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            
                    ?>
                    <option value="<?php echo $row["KdGudang"]; ?>"><?php echo $row["Keterangan"]; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                &nbsp;
                Tanggal
					<input type="text" class="form-control-new datepicker" name="v_date_from" id="v_date_from" size="10" maxlength="10" value="<?php echo $v_date_from; ?>">
                    &nbsp;s/d&nbsp;
                    <input type="text" class="form-control-new datepicker" name="v_date_to" id="v_date_to" size="10" maxlength="10" value="<?php echo $v_date_to; ?>"> 
	    	</div>
	    
			<div class="col-md-2" align="right">
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('search'),show_loading_bar(100)">Cari<i class="entypo-search"></i></button>
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('add_data'),show_loading_bar(100)">Tambah<i class="entypo-plus" ></i></button>
	       	</div>
       	
       	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
    
<?php 
    if($id)
    {
        $id = save_char($id);
        ?>
        <script>CallAjaxForm('edit_data', '<?php echo $id; ?>');</script>
        <?php
    }

    include("footer.php"); 
?>