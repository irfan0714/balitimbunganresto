<?php
include("header.php");

$modul            = "Detail Mutasi";
$file_name        = "report_stock_gudang_detail.php";

$tahun 			= $_GET['tahun'];
$bulan 			= $_GET['bulan'];
$gudang 		= $_GET['gudang'];
$pcode 			= $_GET['pcode'];
$namabarang 	= $_GET['namabarang'];
$awal 			= $_GET['awal'];	

if($gudang=="SEMUA"){
	$wheregudangawal = "";
}
else{
		$wheregudangawal = " AND (Gudang='$gudang' OR GudangTujuan='$gudang')";
}

	$cek = mysql_query("SELECT Gudang,Keterangan FROM(
							SELECT DISTINCT Gudang 
							FROM mutasi 
							WHERE 1
							AND YEAR(Tanggal)='$tahun' 
							AND MONTH(Tanggal)='$bulan'
							AND KodeBarang='$pcode'
							$wheregudangawal
							UNION
							SELECT DISTINCT GudangTujuan AS Gudang
							FROM mutasi 
							WHERE 1
							AND YEAR(Tanggal)='$tahun' 
							AND MONTH(Tanggal)='$bulan'
							AND KodeBarang='$pcode'
							$wheregudangawal
						)AS a
						INNER JOIN gudang g
							ON a.Gudang=g.KdGudang
						WHERE (Gudang!='' OR Gudang IS NOT NULL)
						ORDER BY Gudang ASC");
						
	$num_cek = mysql_num_rows($cek);					
	if($num_cek==0){
		$cek = mysql_query("SELECT KdGudang as Gudang, Keterangan FROM gudang WHERE KdGudang='$gudang'");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - <? echo $pcode." :: ".$namabarang;?></title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>
<body class="page-body skin-black">
<?
while($data=mysql_fetch_array($cek)){
	$gudangxx = $data['Gudang'];
	$namagudangxx = $data['Keterangan'];
	$wheregudang = " AND (m.Gudang='$gudangxx' OR m.GudangTujuan='$gudangxx')";
?>
<br>
<?
echo "Gudang $gudangxx :: $namagudangxx";
?>
<table class="table table-bordered responsive">
		<tr class="title_table">
            <td>No</td>
			<td>Tanggal</td>
            <td>Transaksi</td>
            <td>NoTransaksi</td>    
            <td>Masuk</td>      
			<td>Keluar</td>
			<td>Saldo</td>   
		</tr>
<?
$q = "SELECT 
		m.KodeBarang AS PCode, m.Tanggal, m.Jenis, m.KdTransaksi, t.NamaTransaksi,
		m.Gudang AS GudangAsal, m.GudangTujuan, m.NoTransaksi, 
		m.Qty
	  FROM mutasi m
	  INNER JOIN mutasi_transaksi t
		ON m.KdTransaksi=t.KdTransaksi
	  WHERE 1
		AND YEAR(m.Tanggal)='$tahun' 
		AND MONTH(m.Tanggal)='$bulan'
		$wheregudang
		AND m.KodeBarang='$pcode'
	  ORDER BY 
		m.Tanggal, m.Jenis, m.KdTransaksi, m.Notransaksi ASC";
//echo "<pre>$q</pre>";		
$qry = mysql_query($q);
$no = 1;
$total = $awal;
?>
		<tr>
            <td nowrap>&nbsp;</td>
			<td nowrap>&nbsp;</td>
			<td nowrap><b>SALDO AWAL</b></td>
			<td nowrap>&nbsp;</td>
			<td nowrap>&nbsp;</td>
			<td nowrap>&nbsp;</td>
			<td nowrap align='right'><? echo ubah_format('qty',$total);?></td>
		</tr>
<?
while($hsl=mysql_fetch_array($qry)){
	$tanggal 		= ubah_tanggal($hsl['Tanggal']);
	$jenis 			= $hsl['Jenis'];
	$kdtrans		= $hsl['KdTransaksi'];
	$namatrans		= $hsl['NamaTransaksi'];
	$gudangasal		= $hsl['GudangAsal'];
	$gudangtujuan	= $hsl['GudangTujuan'];
	$notransaksi	= $hsl['NoTransaksi'];
	$qty	 		= $hsl['Qty'];
	
?>
	<tbody style="color: black;">
		<tr>
            <td nowrap><? echo $no;?></td>
			<td nowrap><? echo $tanggal;?></td>
			<td nowrap><? echo $kdtrans." :: ".$namatrans;?></td>
			<td nowrap><? echo $notransaksi;?></td>
			
<?
			if($jenis=="I"){
?>
				<td nowrap align='right'><? echo ubah_format('qty',$qty);?></td>
				<td nowrap>&nbsp;</td>
<?		
				$total += $qty;
				$total_in += $qty;
			}
			elseif($jenis=="O"){
?>
				<td nowrap>&nbsp;</td>
				<td nowrap align='right'><? echo ubah_format('qty',$qty);?></td>
<?	
				$total -= $qty;
				$total_out += $qty;
			}
?>			
			<td nowrap align='right'><? echo $total;?></td>			
		</tr>		
	</tbody>	
<?	
	$no++;
}		
?>
		<tr class="title_table">
            <td nowrap>&nbsp;</td>
			<td nowrap>&nbsp;</td>
			<td nowrap><b>SALDO AKHIR</b></td>
			<td nowrap>&nbsp;</td>
			<td nowrap align='right'><? echo ubah_format('qty',$total_in);?></td>
			<td nowrap align='right'><? echo ubah_format('qty',$total_out);?></td>
			<td nowrap align='right'><? echo ubah_format('qty',$total);?></td>
		</tr>
</table>
<?
}
?>	
</body>	
</html>
<?
function ubah_tanggal($tanggalan){
	list ($tahun, $bulan, $tanggal) = split ('[-]', $tanggalan);
	$tgl = $tanggal."-".$bulan."-".$tahun;
	return $tgl;
}

function ubah_format($param,$harga){
	if($param=="qty"){
		$s = number_format($harga, 0, ',', '.');
	}
	elseif($param=="netto"){
		$s = number_format($harga, 2, ',', '.');
	}
	return $s;
}

function CekBulan($data){
	if($data=="01"){
		$hasil = "Januari";
	}
	elseif($data=="02"){
		$hasil = "Februari";
	}
	elseif($data=="03"){
		$hasil = "Maret";
	}
	elseif($data=="04"){
		$hasil = "April";
	}
	elseif($data=="05"){
		$hasil = "Mei";
	}
	elseif($data=="06"){
		$hasil = "Juni";
	}
	elseif($data=="07"){
		$hasil = "Juli";
	}
	elseif($data=="08"){
		$hasil = "Agustus";
	}
	elseif($data=="09"){
		$hasil = "September";
	}
	elseif($data=="10"){
		$hasil = "Oktober";
	}
	elseif($data=="11"){
		$hasil = "November";
	}
	elseif($data=="12"){
		$hasil = "Desember";
	}	
	return $hasil;
}

?>