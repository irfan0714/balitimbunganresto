<?php

include("header.php");

$modul            = "PURCHASE ORDER";
$file_current     = "npm_purchase_order.php";
$file_name        = "npm_purchase_order_data.php";

//$ses_login = "hendri1003";


if($v_date_from=="")
{
    $v_date_from = "01/".date("m/Y");
}

if($v_date_to=="")
{
    $v_date_to = date("t/m/Y");
}

if(!isset($_GET["id"])){ $id = isset($_GET["id"]); } else { $id = $_GET["id"]; }   
 
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='search')
				{
                    search_keyword = document.getElementById("search_keyword").value;      
                    search_supplier = document.getElementById("search_supplier").value; 
                    search_gudang = document.getElementById("search_gudang").value; 
                    v_date_from = document.getElementById("v_date_from").value; 
                    v_date_to = document.getElementById("v_date_to").value; 
                    search_status = document.getElementById("search_status").value;
                    
                    variabel += "&search_keyword="+search_keyword;
                    variabel += "&search_supplier="+search_supplier;
                    variabel += "&search_gudang="+search_gudang;
                    variabel += "&v_date_from="+v_date_from;
                    variabel += "&v_date_to="+v_date_to;
                    variabel += "&search_status="+search_status;
					
					variabel += "&v_NoDokumen_curr="+param1;

					//alert(param1);
					document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					document.getElementById("col-header").innerHTML   = "";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-search").innerHTML   = xmlhttp.responseText;

							if(param1)
							{
								CallAjaxForm('edit_data' ,param1);
							}
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='add_data')
				{
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='edit_data')
				{
					variabel += "&v_NoDokumen="+param1;

					//alert(variabel);
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_supplier')
				{
					v_KdSupplier = document.getElementById("v_KdSupplier").value;
					
					variabel += "&v_KdSupplier="+v_KdSupplier;

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");
							
							document.getElementById("v_top").value   = arr_data[0];
                            document.getElementById("v_top").focus();
						}

						return false;
					}
					xmlhttp.send(null);
				}
				     

			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function change_pj(nilai)
		{
			if(nilai=="umum")
			{
				document.getElementById('td_type').colSpan = "3";
				document.getElementById('td_sub_type').colSpan = "3";
				document.getElementById('td_empl_name_1').style.display    = 'none';
				document.getElementById('td_empl_name_2').style.display    = 'none';
				document.getElementById('td_jabatan_1').style.display    = 'none';
				document.getElementById('td_jabatan_2').style.display    = 'none';
			}
			else
			{
				document.getElementById('td_type').colSpan = "0";
				document.getElementById('td_sub_type').colSpan = "0";
				document.getElementById('td_empl_name_1').style.display    = '';
				document.getElementById('td_empl_name_2').style.display    = '';
				document.getElementById('td_jabatan_1').style.display    = '';
				document.getElementById('td_jabatan_2').style.display    = '';
			}
			
		}
		
		function cek_qty()
		{
			var v_cek_qty = document.getElementById("v_cek_qty").checked;
			
			//alert(v_cek_qty);
			
			if(v_cek_qty)
			{
				document.getElementById('span_qty').style.display    = '';  
				document.getElementById("v_qty").value = '';
				document.getElementById('v_qty').focus();
			}  
			else
			{
				document.getElementById('span_qty').style.display    = 'none';  
			}
			
		}
		
		function pop_up_employee()
		{
			var variabel;
			var search_cabang_id;
			variabel = "";
			search_cabang_id="";
			
			v_cabang_id = document.getElementById("v_cabang_id").value;    
			
			if(v_cabang_id)
			{
				search_cabang_id = "?search_cabang_id="+v_cabang_id;	
			}
			
			variabel += search_cabang_id;
			  
			windowOpener(600, 800, 'Search Employee', 'npm_fa_master_fixed_asset_pop_up_employee.php'+variabel, 'Search Employee')
		}
		
		function pop_search_pr()
		{
			var variabel;
			variabel = "";
            
            v_KdGudang = document.getElementById("v_KdGudang").value;
            
            if(v_KdGudang=="")
            {
                alert("Gudang harus dipilih...");    
            } 
            else
            {
			    variabel += "?v_KdGudang="+v_KdGudang;
			    
			    windowOpener(400, 600, 'Search PR', 'npm_purchase_order_pop_search_pr.php'+variabel, 'Search PR')
            }
		}
		
		function change_par()
		{
		    var chk_gedung_par = document.getElementById("chk_gedung_par").checked;
		    
		    if(chk_gedung_par)
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = '';
		        document.getElementById('tr_periode_par').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_par').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = 'none';
		        document.getElementById('tr_periode_par').style.display = 'none';
			} 
		}
		
		function change_earthquake()
		{
		    var chk_gedung_earthquake = document.getElementById("chk_gedung_earthquake").checked;
		    
		    if(chk_gedung_earthquake)
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = '';
		        document.getElementById('tr_periode_earthquake').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_earthquake').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = 'none';
		        document.getElementById('tr_periode_earthquake').style.display = 'none';
			} 
		}

		function confirm_delete(name)
		{
			var r = confirm("Anda yakin ingin void "+name+" ? ");
			if(r)
			{
				document.getElementById("v_del").value = '1';
				document.getElementById("theform").submit();
			}
		}
        
        function confirm_unvoid(name)
        {
            var r = confirm("Anda yakin ingin UnVoid "+name+" ? ");
            if(r)
            {
                document.getElementById("v_undel").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve "+name+" ? ");
            if(r)
            {
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_approve_edit(name)
        {
            var r = confirm("Anda yakin ingin Setujui Edit PO "+name+" ? ");
            if(r)
            {
                document.getElementById("v_approve_edit").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_reject(name)
        {
            var r = confirm("Anda yakin ingin Reject "+name+" ? ");
            if(r)
            {
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_request(name)
        {
            var r = confirm("Anda yakin ingin Request Edit PO "+name+" ? ");
            if(r)
            {
                document.getElementById("v_request_edit").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_request2(name)
        {
            var r = confirm("Anda yakin ingin Request Edit PO "+name+" ? ");
            if(r)
            {
                document.getElementById("v_request_edit2").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function muncul_reject()
        {
            document.getElementById("btn_approve").style.display = "none";    
            document.getElementById("btn_confirm_reject").style.display = "none";    
            
            document.getElementById("v_Approval_Remarks").style.display = "";    
            document.getElementById("btn_reject").style.display = "";    
            
            document.getElementById("v_Approval_Remarks").focus();
        }
        
        function muncul_request()
        {   
            document.getElementById("btn_confirm_request").style.display = "none";    
            
            document.getElementById("v_request_Remarks").style.display = "";    
            document.getElementById("btn_request").style.display = "";    
            
            document.getElementById("v_request_Remarks").focus();
        }
        
        function muncul_request2()
        {   
            document.getElementById("btn_confirm_request2").style.display = "none";    
            
            document.getElementById("v_request_Remarks2").style.display = "";    
            document.getElementById("btn_request2").style.display = "";    
            
            document.getElementById("v_request_Remarks2").focus();
        }
        

		function CheckAll(param, target)
		{
			var field = document.getElementsByName(target);
			var chkall = document.getElementById(param);
			if (chkall.checked == true)
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = true ;
			}else
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = false ;
			}
		}

		function reform(val)
		{
			var a = val.split(",");
			var b = a.join("");
			//alert(b);
			return b;
		}

		function format(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(0);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format4(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(4);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format2(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(2);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format6(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(6);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function addSeparatorsNF(nStr, inD, outD, sep)
		{
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1)
			{
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr))
			{
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}

		function toFormat4(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format4(document.getElementById(id).value);
		}

		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
			
			//menghitung
			
		}

		function toFormat6(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format6(document.getElementById(id).value);
		}

		function isanumber(id)
		{
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				document.getElementById(id).value=0;
			}
			else
			{
				document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
			}
		}

		function start_page()
		{
            try
            {
			    document.getElementById("search_keyword").focus();
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
		}

		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
		
		function HitungHarga2(flag, obj) {
		    
            objek = obj.id;
            if (flag == 'harga') {
                id = objek.substr(6, objek.length - 6);				
                
				if($("#v_kon_" + id).val()==""){
				alert("Pilih Satuan Dahulu");
				$("#v_Qty_" + id).val("");
				$("#v_Satuan_" + id).focus();
				return false;
				}
				
				konvers = parseFloat($("#v_kon_" + id).val());
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg_ = parseFloat($("#v_Harga_" + id).val());
				
                if($("#v_Harga_" + id).val()==""){
					hrg=1;
				}else{
					hrg=hrg_;
				}
                disc1_ = parseFloat($("#v_Disc1_" + id).val());
                
                if($("#v_Disc1_" + id).val()==""){
					disc1=0;
				}else{
				   disc1=disc1_;
				}
				
                disc2_ = parseFloat($("#v_Disc2_" + id).val());
                if($("#v_Disc2_" + id).val()==""){
					disc2=0;
				}else{
				   disc2=disc2_;
				}
				
				pot_ = parseFloat($("#v_Potongan_" + id).val());
				if($("#v_Potongan_" + id).val()==""){
					pot=0;
				}else{
					pot=pot_;
				}
				
				//alert(konvers+"-"+qty+"-"+hrg+"-"+disc1+"-"+disc2+"-"+pot);
				//hasil konversi
				jml_konvers = qty*konvers;
				
				//hasil diskon
				nil_dis1 = (disc1/100)*(hrg * jml_konvers);
				nil_dis2 = (((hrg * jml_konvers)-nil_dis1)*(disc2/100));
				
				//hitung
                $("#v_subtotal_" + id).val((((hrg * jml_konvers)-nil_dis1)-nil_dis2)-pot);
                subtot = $("#v_subtotal_" + id).val();
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }
    }
	
	
	function HitungHarga3(flag, obj) {
		    
            objek = obj.id;
            if (flag == 'harga') {
                id = objek.substr(8, objek.length - 8);				
                
				if($("#v_kon_" + id).val()==""){
				alert("Pilih Satuan Dahulu");
				$("#v_Qty_" + id).val("");
				$("#v_Satuan_" + id).focus();
				return false;
				}
				
				konvers = parseFloat($("#v_kon_" + id).val());
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg_ = parseFloat($("#v_Harga_" + id).val());
				
                if($("#v_Harga_" + id).val()==""){
					hrg=1;
				}else{
					hrg=hrg_;
				}
                disc1_ = parseFloat($("#v_Disc1_" + id).val());
                
                if($("#v_Disc1_" + id).val()==""){
					disc1=0;
				}else{
				   disc1=disc1_;
				}
				
                disc2_ = parseFloat($("#v_Disc2_" + id).val());
                if($("#v_Disc2_" + id).val()==""){
					disc2=0;
				}else{
				   disc2=disc2_;
				}
				
				pot_ = parseFloat($("#v_Potongan_" + id).val());
				if($("#v_Potongan_" + id).val()==""){
					pot=0;
				}else{
					pot=pot_;
				}
				
				//alert(konvers+"-"+qty+"-"+hrg+"-"+disc1+"-"+disc2+"-"+pot);
				//hasil konversi
				jml_konvers = qty*konvers;
				
				//hasil diskon
				nil_dis1 = (disc1/100)*(hrg * jml_konvers);
				nil_dis2 = (((hrg * jml_konvers)-nil_dis1)*(disc2/100));
				
				//hitung
                $("#v_subtotal_" + id).val((((hrg * jml_konvers)-nil_dis1)-nil_dis2)-pot);
                subtot = $("#v_subtotal_" + id).val();
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }
    }
	
	function HitungHarga4(flag, obj) {
		    
            objek = obj.id;
            if (flag == 'harga') {
                id = objek.substr(8, objek.length - 8);				
                
				if($("#v_kon_" + id).val()==""){
				alert("Pilih Satuan Dahulu");
				$("#v_Qty_" + id).val("");
				$("#v_Satuan_" + id).focus();
				return false;
				}
				
				konvers = parseFloat($("#v_kon_" + id).val());
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg_ = parseFloat($("#v_Harga_" + id).val());
				
                if($("#v_Harga_" + id).val()==""){
					hrg=1;
				}else{
					hrg=hrg_;
				}
                disc1_ = parseFloat($("#v_Disc1_" + id).val());
                
                if($("#v_Disc1_" + id).val()==""){
					disc1=0;
				}else{
				   disc1=disc1_;
				}
				
                disc2_ = parseFloat($("#v_Disc2_" + id).val());
                if($("#v_Disc2_" + id).val()==""){
					disc2=0;
				}else{
				   disc2=disc2_;
				}
				
				pot_ = parseFloat($("#v_Potongan_" + id).val());
				if($("#v_Potongan_" + id).val()==""){
					pot=0;
				}else{
					pot=pot_;
				}
				
				//alert(konvers+"-"+qty+"-"+hrg+"-"+disc1+"-"+disc2+"-"+pot);
				//hasil konversi
				jml_konvers = qty*konvers;
				
				//hasil diskon
				nil_dis1 = (disc1/100)*(hrg * jml_konvers);
				nil_dis2 = (((hrg * jml_konvers)-nil_dis1)*(disc2/100));
				
				//hitung
                $("#v_subtotal_" + id).val((((hrg * jml_konvers)-nil_dis1)-nil_dis2)-pot);
                subtot = $("#v_subtotal_" + id).val();
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }
    }
	
	function HitungHarga5(flag, obj) {
		    
            objek = obj.id;
            if (flag == 'harga') {
                id = objek.substr(8, objek.length - 8);				
                
				if($("#v_kon_" + id).val()==""){
				alert("Pilih Satuan Dahulu");
				$("#v_Qty_" + id).val("");
				$("#v_Satuan_" + id).focus();
				return false;
				}
				
				konvers = parseFloat($("#v_kon_" + id).val());
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg_ = parseFloat($("#v_Harga_" + id).val());
				
                if($("#v_Harga_" + id).val()==""){
					hrg=1;
				}else{
					hrg=hrg_;
				}
                disc1_ = parseFloat($("#v_Disc1_" + id).val());
                
                if($("#v_Disc1_" + id).val()==""){
					disc1=0;
				}else{
				   disc1=disc1_;
				}
				
                disc2_ = parseFloat($("#v_Disc2_" + id).val());
                if($("#v_Disc2_" + id).val()==""){
					disc2=0;
				}else{
				   disc2=disc2_;
				}
				
				pot_ = parseFloat($("#v_Potongan_" + id).val());
				if($("#v_Potongan_" + id).val()==""){
					pot=0;
				}else{
					pot=pot_;
				}
				
				//alert(konvers+"-"+qty+"-"+hrg+"-"+disc1+"-"+disc2+"-"+pot);
				//hasil konversi
				jml_konvers = qty*konvers;
				
				//hasil diskon
				nil_dis1 = (disc1/100)*(hrg * jml_konvers);
				nil_dis2 = (((hrg * jml_konvers)-nil_dis1)*(disc2/100));
				
				//hitung
                $("#v_subtotal_" + id).val((((hrg * jml_konvers)-nil_dis1)-nil_dis2)-pot);
                subtot = $("#v_subtotal_" + id).val();
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }
    }
	
	
	function HitungHarga6(flag, obj) {
		    
            objek = obj.id;
            if (flag == 'harga') {
                id = objek.substr(11, objek.length - 11);				
                
				if($("#v_kon_" + id).val()==""){
				alert("Pilih Satuan Dahulu");
				$("#v_Qty_" + id).val("");
				$("#v_Satuan_" + id).focus();
				return false;
				}
				
				konvers = parseFloat($("#v_kon_" + id).val());
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg_ = parseFloat($("#v_Harga_" + id).val());
				
                if($("#v_Harga_" + id).val()==""){
					hrg=1;
				}else{
					hrg=hrg_;
				}
                disc1_ = parseFloat($("#v_Disc1_" + id).val());
                
                if($("#v_Disc1_" + id).val()==""){
					disc1=0;
				}else{
				   disc1=disc1_;
				}
				
                disc2_ = parseFloat($("#v_Disc2_" + id).val());
                if($("#v_Disc2_" + id).val()==""){
					disc2=0;
				}else{
				   disc2=disc2_;
				}
				
				pot_ = parseFloat($("#v_Potongan_" + id).val());
				if($("#v_Potongan_" + id).val()==""){
					pot=0;
				}else{
					pot=pot_;
				}
				
				//alert(konvers+"-"+qty+"-"+hrg+"-"+disc1+"-"+disc2+"-"+pot);
				//hasil konversi
				jml_konvers = qty*konvers;
				
				//hasil diskon
				nil_dis1 = (disc1/100)*(hrg * jml_konvers);
				nil_dis2 = (((hrg * jml_konvers)-nil_dis1)*(disc2/100));
				
				//hitung
                $("#v_subtotal_" + id).val((((hrg * jml_konvers)-nil_dis1)-nil_dis2)-pot);
                subtot = $("#v_subtotal_" + id).val();
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }
    }
	
	
	function HitungHarga7() {
		    jumlah = parseFloat($("#v_Jumlah").val());
			diskon_ = parseFloat($("#v_DiscHarga").val());
			
			
			if($("#v_DiscHarga").val()==""){
			diskon=0;
			}else{
			diskon=diskon_;
			}
			
			ppn_ = parseFloat($("#v_PPn").val());
            if($("#v_PPn").val()==""){
			ppn=0;
			}else{
			ppn=ppn_;
			}
			
			jml_diskon = (diskon/100)*jumlah;
			nil_diskon = jml_diskon;
			
			jml_ppn = (ppn/100)*(jumlah - jml_diskon);
			nil_ppn = jml_ppn;
			
			tot = jumlah-nil_diskon+nil_ppn;
			
            $("#v_nil_diskon").val(nil_diskon);
			$("#v_NilaiPPn").val(nil_ppn);
			$("#v_Total").val(tot);
    }
	
	function HitungHarga8() {
		    jumlah = parseFloat($("#v_Jumlah").val());
			diskon_ = parseFloat($("#v_DiscHarga").val());
			
			
			if($("#v_DiscHarga").val()==""){
			diskon=0;
			}else{
			diskon=diskon_;
			}
			
			ppn_ = parseFloat($("#v_PPn").val());
            if($("#v_PPn").val()==""){
			ppn=0;
			}else{
			ppn=ppn_;
			}
			
			jml_diskon = (diskon/100)*jumlah;
			nil_diskon = jml_diskon;
			
			jml_ppn = (ppn/100)*(jumlah - jml_diskon);
			nil_ppn = jml_ppn;
			
			tot = jumlah-nil_diskon+nil_ppn;
			
            $("#v_nil_diskon").val(nil_diskon);
			$("#v_NilaiPPn").val(nil_ppn);
			$("#v_Total").val(tot);
    }
	    
    
        function totalNetto()
		{
		    var lastRow = document.getElementsByName("v_subtotal[]").length;
		    var total = 0;//grand total
		    var stotal = 0;//v_Jumalah atau total atas
		    for (index = 0; index < lastRow; index++)
		    {
		        indexs = index - 1;
		        nama = document.getElementsByName("v_subtotal[]");
		        temp = nama[index].id;
		        temp1 = parseFloat(nama[index].value);
		        stotal += temp1;
		        
		         nama = document.getElementsByName("v_sJumlah[]");
		        temp = nama[index].id;
		        temp1 = parseFloat(nama[index].value);
		        total += temp1;
		    }
		    $("#v_Jumlah").val(Math.round(stotal));
			//alert($("#v_Jumlah").val(Math.round(stotal)));
		    $("#grdTotal").val(Math.round(total));
		    $("#v_Total").val(Math.round(total));

		}
		
		
		function hitung(obj){
		objek = obj.id;
        id = objek.substr(9, objek.length - 9);		
		
		var pcode = $("#v_PCode_"+id).val();
		var satuan_asal = $("#v_sat_asal_"+id).val();
		var satuan_tujuan = $("#v_Satuan_"+id).val();
		//sama tidak satuan PO dengan satuan kontrak supplier
		    if(satuan_asal==satuan_tujuan){
				
				$("#v_kon_"+id).val(1);
				$("#v_Qty_"+id).focus();
				
				}else{
					//alert("beda");
					//alert(pcode+"-"+satuan_asal+"-"+satuan_tujuan);
					/*variabel += "&pcode="+pcode;
				    variabel += "&satuan_asal="+satuan_asal;
                    variabel += "&satuan_tujuan="+satuan_tujuan;
					alert(variabel);*/
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax=konversi&pcode='+pcode+'&satuan_asal='+satuan_asal+'&satuan_tujuan='+satuan_tujuan, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");
							
							//document.getElementById("v_top").value   = arr_data[0];
                            //document.getElementById("v_top").focus();
							$("#v_kon_"+id).val(arr_data[0]);
							$("#v_Qty_"+id).focus();
						}

						return false;
					}
					xmlhttp.send(null);
				
				
				
				
			}
		}
		
		function cekTheform()
					{
						
					    	var yesSubmit = true;
					    	
					        
					        if(yesSubmit)
					        {
								document.getElementById("theform").submit();
								
							}  
						}
			
	</script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Transaksi - Pembelian</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-10">
				Pencarian
                &nbsp;
					<input type="text" class="form-control-new" size="20" maxlength="30" name="search_keyword" id="search_keyword">
                &nbsp;
                Supplier
                <select name="search_supplier" id="search_supplier" class="form-control-new" style="width: 100px;" >
                    <option value="">Semua</option>
                	<?php 
                        $q = "
                            SELECT
                                ".$db["master"].".supplier.KdSupplier,
                                ".$db["master"].".supplier.Nama
                            FROM
                                ".$db["master"].".supplier
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".supplier.Nama ASC
                        ";
                        $qry_supplier = mysql_query($q);
                        while($row_supplier = mysql_fetch_array($qry_supplier))
                        {
                    ?>
                    <option value="<?php echo $row_supplier["KdSupplier"]; ?>"><?php echo $row_supplier["Nama"]; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                &nbsp;
                Gudang
                <select name="search_gudang" id="search_gudang" class="form-control-new" style="width: 100px;">
                    <option value="">Semua</option>
                    <?php 
                        $q = "
                            SELECT
                                ".$db["master"].".gudang.KdGudang,
                                ".$db["master"].".gudang.Keterangan
                            FROM
                                ".$db["master"].".gudang
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".gudang.KdGudang ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            
                    ?>
                    <option value="<?php echo $row["KdGudang"]; ?>"><?php echo $row["Keterangan"]; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                &nbsp;
                Tanggal
					<input type="text" class="form-control-new datepicker" name="v_date_from" id="v_date_from" size="10" maxlength="10" value="<?php echo $v_date_from; ?>">
                    &nbsp;s/d&nbsp;
                    <input type="text" class="form-control-new datepicker" name="v_date_to" id="v_date_to" size="10" maxlength="10" value="<?php echo $v_date_to; ?>"> 
	    	
	    	
	    	&nbsp;
                Status
                <select name="search_status" id="search_status" class="form-control-new" style="width: 150px;" >
                    <option value="">Semua</option>
                    <option value="6">All Waiting Approve</option>
                    <option value="1">Waiting Approve charles3105</option>
                    <option value="2">Waiting Approve trisno1402</option>
                    <option value="3">Waiting Approve henny2905</option>
                    <option value="4">All Approve</option>
                    <option value="7">Approve By charles3105</option>
                    <option value="8">Approve By trisno1402</option>
                    <option value="9">Approve By henny2905</option>
                	<option value="5">Reject</option>
                	<option value="10">Request Edit PO charles3105</option>
                	<option value="11">Request Edit PO trisno1402</option>
                	<option value="12">Request Edit PO henny2905</option>
                </select> 
                &nbsp;
	    	
	    	
	    	
	    	</div>
	    
			<div class="col-md-2" align="right">
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('search'),show_loading_bar(100)">Cari<i class="entypo-search"></i></button>
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('add_data'),show_loading_bar(100)">Tambah<i class="entypo-plus" ></i></button>
	       	</div>
       	
       	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search -->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
    
<?php 
    if($id)
    {
        $id = save_char($id);
        ?>
        <script>CallAjaxForm('edit_data', '<?php echo $id; ?>');</script>
        <?php
    }

    include("footer.php"); 
?>