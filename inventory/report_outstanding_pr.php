<?php               
    include("header.php");
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }
    if(!isset($_POST["v_status"])){ $v_status = isset($_POST["v_status"]); } else { $v_status = $_POST["v_status"]; }
    if(!isset($_POST["v_keyword"])){ $v_keyword = isset($_POST["v_keyword"]); } else { $v_keyword = $_POST["v_keyword"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Outstanding PR";
    
    if($v_date_from=="")
    {
        $v_date_from = "01".date("/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("t/m/Y");
    }
    
   
    $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdGudang, $NamaGudang) = $row;
        
        $arr_data["list_gudang"][$KdGudang] = $KdGudang;
        $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
    }
    
    $q = "
            SELECT
                ".$db["master"].".gudang_admin.KdGudang
            FROM
                ".$db["master"].".gudang_admin
            WHERE
                1
                AND ".$db["master"].".gudang_admin.UserName = '".$ses_login."'
            ORDER BY
                ".$db["master"].".gudang_admin.KdGudang ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdGudang, $NamaGudang) = $row;
        
        $arr_data["list_gudang_admin"][$KdGudang] = $KdGudang;
    }          
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-outstanding_pr.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  

function pop_up_detail(NoPR, PCode)
{
    var variabel;
    variabel = "";
    variabel += "?v_NoPR="+NoPR;
    variabel += "&v_PCode="+PCode;
    
    windowOpener(600, 800, 'Detail Terima', 'report_outstanding_pr_pop_up_po.php'+variabel, 'Detail Terima');
}
    </script>
    
    <style>
        .link_pop{
            text-decoration: underline;
            color: black;
        }
        
        .link_pop:hover{
            text-decoration: none;
            color: #222222;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="POST" name="theform" id="theform">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="100">Tanggal</th>
						<th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
					</tr>
                    
                     <tr>
                        <th>Gudang</th>
                        <th>: 
                                
                                  <select class="form-control-new" name="v_KdGudang" id="v_KdGudang" style="width: 200px;">
                                    <option value="">Semua</option>
                                    
                                    <?php 
                                        foreach($arr_data["list_gudang_admin"] as $KdGudang=>$val)
                                        {
                                            $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                        ?>
                                        <option <?php if($v_KdGudang==$KdGudang) echo "selected='selected'"; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                        <?php        
                                        }
                                    ?>
                                </select>     
                        </th>
                    </tr>
                    
                   
                     
                     <tr>
                        <th>Status</th>
                        <th>: 
                            <select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
                                <option <?php if($v_status=="Pending") echo "selected='selected'"; ?> value="Pending">Pending</option>
                                <option <?php if($v_status=="Semua") echo "selected='selected'"; ?> value="Semua">Semua</option>                                
                                <option <?php if($v_status=="Selesai") echo "selected='selected'"; ?> value="Selesai">Selesai</option>
                            </select>
                        </th>
                     </tr>
                     
                      <tr>
                        <th>Keyword</th>
                        <th>: 
                            <input type="text" class="form-control-new" name="v_keyword" id="v_keyword" style="width: 200px;" value="<?php echo $v_keyword; ?>">
                        </th>
                     </tr>
                     
                      
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
				</thead>
				
			</table> 
			<br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    $where_gudang = "";
                    if($v_KdGudang!="")
                    {
                        $where_gudang = " AND `gudang`.KdGudang = '".$v_KdGudang."' ";    
                    }
                     
                    
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    else
                    {
                        $where_date = " AND `trans_pr_header`.TglDokumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";    
                    }
                    
                    $arr_keyword[0] = "trans_pr_header.NoDokumen";    
                    $arr_keyword[1] = "trans_pr_header.NoPermintaan";    
                    $arr_keyword[2] = "trans_pr_header.Keterangan";      
                    
                    $where_keyword = "";
                    if($v_keyword)
                    {
                        $where_search_keyword = search_keyword($v_keyword, $arr_keyword);
                        $where_keyword = $where_search_keyword;
                    }
                    
                    $q = "
                            SELECT
                                trans_pr_header.NoDokumen AS NoPR,
                                trans_pr_header.NoPermintaan AS NoPB,
                                trans_pr_header.TglDokumen AS TglPR,
                                trans_pr_header.Keterangan AS NotePR,
                                gudang.KdGudang,
                                gudang.Keterangan AS NamaGudang,
                                trans_pr_detail.PCode,
                                masterbarang.NamaLengkap,
                                trans_pr_detail.Qty,
                                trans_pr_detail.Satuan
                            FROM
                                trans_pr_header
                                INNER JOIN trans_pr_detail ON
                                    trans_pr_header.NoDokumen = trans_pr_detail.NoDokumen
                                    AND trans_pr_header.Status*1 != '2'
                                    AND trans_pr_detail.Qty*1 != '0'
                                    ".$where_date."
                                INNER JOIN gudang ON
                                    trans_pr_header.KdGudang = gudang.KdGudang
                                    ".$where_gudang."
                                INNER JOIN masterbarang ON
                                    masterbarang.PCode = trans_pr_detail.PCode
                            WHERE
                                1
                                ".$where_keyword."
                            ORDER BY
                                trans_pr_header.TglDokumen ASC,
                                trans_pr_header.NoDokumen ASC    
                    ";
                    
                    $counter = 0;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $NoPR,
                            $NoPB,
                            $TglPR,
                            $NotePR,
                            $KdGudang,
                            $NamaGudang,
                            $PCode,
                            $NamaLengkap,
                            $Qty,
                            $Satuan      
                        ) = $row;
                        
                        $arr_data["list_data"][$counter] = $counter;
                        $arr_data["data_NoPR"][$counter] = $NoPR;
                        $arr_data["data_NoPB"][$counter] = $NoPB;
                        $arr_data["data_TglPR"][$counter] = $TglPR;
                        $arr_data["data_NotePR"][$counter] = $NotePR;
                        $arr_data["data_KdGudang"][$counter] = $KdGudang;
                        $arr_data["data_NamaGudang"][$counter] = $NamaGudang;
                        $arr_data["data_PCode"][$counter] = $PCode;
                        $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                        $arr_data["data_Qty"][$counter] = $Qty;
                        $arr_data["data_Satuan"][$counter] = $Satuan;
                        
                        $arr_data["list_NoPR"][$NoPR] = $NoPR;
                        
                        $counter++;
                    }
                    
                    if(count($arr_data["list_NoPR"])*1>0)
                    {
                        $where_NoPR = where_array($arr_data["list_NoPR"], "NoPr", "in");
                        
                        $q = "
                                SELECT
                                    trans_order_barang_header.NoPr,    
                                    trans_order_barang_detail.PCode,
                                    SUM(trans_order_barang_detail.Qty) AS Qty_PO
                                FROM
                                    trans_order_barang_header
                                    INNER JOIN trans_order_barang_detail ON
                                        trans_order_barang_header.NoDokumen = trans_order_barang_detail.NoDokumen
                                        AND trans_order_barang_header.Status != '2'
                                        ".$where_NoPR."
                                WHERE
                                    1
                                GROUP BY
                                    trans_order_barang_header.NoPr,    
                                    trans_order_barang_detail.PCode
                                ORDER BY
                                    trans_order_barang_header.NoPr ASC,    
                                    trans_order_barang_detail.PCode ASC   
                        "; 
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $NoPr,
                                $PCode,
                                $Qty_PO
                            ) = $row;
                            
                            $arr_data["Qty_PO"][$NoPr][$PCode] = $Qty_PO;
                        } 
                        
                    }
                    
                    foreach($arr_data["list_data"] as $counter=>$val)
                    {
                        $NoPR = $arr_data["data_NoPR"][$counter];
                        $NoPB = $arr_data["data_NoPB"][$counter];
                        $TglPR = $arr_data["data_TglPR"][$counter];
                        $NotePR = $arr_data["data_NotePR"][$counter];
                        $KdGudang = $arr_data["data_KdGudang"][$counter];
                        $NamaGudang = $arr_data["data_NamaGudang"][$counter];
                        $PCode = $arr_data["data_PCode"][$counter];
                        $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                        $Qty = $arr_data["data_Qty"][$counter];
                        $Satuan = $arr_data["data_Satuan"][$counter];    
                        
                        $Qty_PO = $arr_data["Qty_PO"][$NoPR][$PCode];
                        $Sisa = $Qty-$Qty_PO;
                        
                        if($v_status=="Semua")
                        {
                            $arr_data["list_data_oke"][$counter] = $counter;    
                        }
                        
                        if($v_status=="Pending")
                        {
                            if($Sisa>0)
                            {
                                $arr_data["list_data_oke"][$counter] = $counter;    
                            }
                        }
                        
                        if($v_status=="Selesai")
                        {
                            if($Sisa<=0)
                            {
                                $arr_data["list_data_oke"][$counter] = $counter;    
                            }
                        }
                        
                    }
                    
                    //echo "<pre>";
                    //print_r($arr_data["list_data_oke"]);
                    //echo "</pre>";
                    
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="12">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="12">REPORT OUTSTANDING PR</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="12">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    
                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No PR</th>
                                <th>No PB</th>
                                <th>TGL PR</th>
                                <th>Note PR</th>
                                <th>Gudang</th>
                                <th>PCode</th>
                                <th>Nama Barang</th>
                                <th>Request</th>
                                <th>PO</th>
                                <th>Sisa</th>
                                <th>Satuan</th>
                            </tr>
                            
                        </thead>
                        
                        <tbody style="color: black;">
                            <?php 
                                $NoPR_prev = "";
                                $no = 0;
                                foreach($arr_data["list_data_oke"] as $counter=>$val)
                                {
                                    $NoPR = $arr_data["data_NoPR"][$counter];
                                    $NoPB = $arr_data["data_NoPB"][$counter];
                                    $TglPR = $arr_data["data_TglPR"][$counter];
                                    $NotePR = $arr_data["data_NotePR"][$counter];
                                    $KdGudang = $arr_data["data_KdGudang"][$counter];
                                    $NamaGudang = $arr_data["data_NamaGudang"][$counter];
                                    $PCode = $arr_data["data_PCode"][$counter];
                                    $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                                    $Qty = $arr_data["data_Qty"][$counter];
                                    $Satuan = $arr_data["data_Satuan"][$counter];  
                                    
                                    $no_echo = "";
                                    $NoPR_echo = "";
                                    $NoPB_echo = "";
                                    $TglPR_echo = "";
                                    $NotePR_echo = "";
                                    $gudang_echo = "";
                                    if($NoPR_prev!=$NoPR)
                                    {
                                        $no++;
                                        $no_echo = $no;
                                        $NoPR_echo = $NoPR;
                                        $NoPB_echo = $NoPB;
                                        $TglPR_echo = format_show_date($TglPR);
                                        $NotePR_echo = $NotePR;
                                        $gudang_echo = $NamaGudang;
                                    }
                                    
                                    $Qty_PO = $arr_data["Qty_PO"][$NoPR][$PCode];
                                    $Sisa = $Qty-$Qty_PO;
                                    
                                    ?>
                                    
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no_echo; ?></td>
                                        <td><?php echo $NoPR_echo; ?></td>
                                        <td><?php echo $NoPB_echo; ?></td>
                                        <td><?php echo $TglPR_echo; ?></td>
                                        <td><?php echo $NotePR_echo; ?></td>
                                        <td><?php echo $gudang_echo; ?></td>
                                        <td><?php echo $PCode; ?></td>
                                        <td><?php echo $NamaLengkap; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($Qty, 2, ",", ".", "ind"); ?></td>
                                        <td style="text-align: right;"><?php if(!$btn_excel) { ?><a class="link_pop" href="javascript:void(0)" onclick="pop_up_detail('<?php echo $NoPR; ?>', '<?php echo $PCode; ?>')"><?php } ?><?php echo format_number($Qty_PO, 2,",", ".", "ind"); ?><?php if(!$btn_excel) { ?></a><?php } ?></td>
                                        <td style="text-align: right;"><?php echo format_number($Sisa, 2,",", ".", "ind"); ?></td>
                                        <td><?php echo $Satuan; ?></td>
                                    </tr>
                                    <?php
                                    
                                    $NoPR_prev = $NoPR;
                                }
                            ?>
                        </tbody>
                        
                        <tfoot>
                           
                        </tfoot>
                    </table>
                    <?php
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
			
		
		</div>
		
		</form>
		
<?php 
                    include("footer.php"); 
                }
?>