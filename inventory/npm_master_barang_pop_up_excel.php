<?php
include("header.php");

$search_keyword        = trim($_GET["search_keyword"]);
$search_KdDivisi       = trim($_GET["search_KdDivisi"]);
$search_KdKategori     = trim($_GET["search_KdKategori"]);
$search_order_by       = trim($_GET["search_order_by"]);


$where = "";        
if($search_keyword)
{
    unset($arr_keyword);
    $arr_keyword[0] = "masterbarang.PCode";    
    $arr_keyword[1] = "masterbarang.NamaLengkap";    
    $arr_keyword[2] = "kategori.KdKategori";      
    $arr_keyword[3] = "kategori.NamaKategori";      
    $arr_keyword[4] = "divisi.KdDivisi";      
    $arr_keyword[5] = "divisi.NamaDivisi";      
    $arr_keyword[6] = "masterbarang.Barcode1";    
    
    $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
    $where .= $where_search_keyword;
} 

if($search_KdDivisi!="")
{
    $where .= " AND masterbarang.KdDivisi = '".$search_KdDivisi."'";
}

if($search_KdKategori)
{
    $where .= " AND masterbarang.KdKategori = '".$search_KdKategori."'";
}

if($search_order_by=="Nama Barang")
{                       
    $where_order_by = " ".$db["master"].".masterbarang.NamaLengkap ASC "; 
}
else if($search_order_by=="PCode")
{                       
    $where_order_by = " ".$db["master"].".masterbarang.PCode ASC "; 
}
else if($search_order_by=="Add Date")
{                       
    $where_order_by = " ".$db["master"].".masterbarang.AddDate ASC "; 
}

// select query all
{
    $counter = 1;
    $q="
        SELECT 
          ".$db["master"].".masterbarang.PCode,
          ".$db["master"].".masterbarang.NamaLengkap,
          ".$db["master"].".masterbarang.Barcode1,
          ".$db["master"].".masterbarang.SatuanSt,
          ".$db["master"].".divisi.KdDivisi,
          ".$db["master"].".divisi.NamaDivisi,
          ".$db["master"].".kategori.KdKategori,
          ".$db["master"].".kategori.NamaKategori,
          ".$db["master"].".masterbarang.Approval_By,
          ".$db["master"].".masterbarang.Approval_Date,
          ".$db["master"].".masterbarang.Approval_Status,
          ".$db["master"].".masterbarang.Approval_Remarks,
          ".$db["master"].".masterbarang.AddDate,
          ".$db["master"].".masterbarang.EditDate,
          ".$db["master"].".masterbarang.Harga1c,
          ".$db["master"].".masterbarang.komisi,
          ".$db["master"].".masterbarang.DiscInternal,
          ".$db["master"].".masterbarang.Service_charge,
          ".$db["master"].".masterbarang.PPN,
          ".$db["master"].".masterbarang.Status
        FROM
          ".$db["master"].".masterbarang 
          LEFT JOIN ".$db["master"].".divisi 
            ON ".$db["master"].".masterbarang.KdDivisi = ".$db["master"].".divisi.KdDivisi 
          LEFT JOIN ".$db["master"].".kategori 
            ON ".$db["master"].".masterbarang.KdKategori = ".$db["master"].".kategori.KdKategori
        WHERE 1 
            ".$where."
        ORDER BY 
            ".$where_order_by."
    ";
    $sql=mysql_query($q);
    while($row=mysql_fetch_array($sql))
    { 
        list(
            $PCode,
            $NamaLengkap,
            $Barcode1,
            $SatuanSt,
            $KdDivisi,
            $NamaDivisi,
            $KdKategori,
            $NamaKategori,
            $Approval_By,
            $Approval_Date,
            $Approval_Status,
            $Approval_Remarks,
            $AddDate,
            $EditDate,
            $Harga1c,
            $komisi,
            $DiscInternal,
            $Service_charge,
            $PPN,
            $Status
        )=$row;
        
        if($AddDate=="0000-00-00 00:00:00")
        {
            $AddDate = "";
        }
        
        $arr_data["list_data"][$counter]=$counter;
        $arr_data["data_PCode"][$counter]=$PCode;
        $arr_data["data_NamaLengkap"][$counter]=$NamaLengkap;
        $arr_data["data_Barcode1"][$counter]=$Barcode1;
        $arr_data["data_SatuanSt"][$counter]=$SatuanSt;
        $arr_data["data_KdDivisi"][$counter]=$KdDivisi;
        $arr_data["data_NamaDivisi"][$counter]=$NamaDivisi;
        $arr_data["data_KdKategori"][$counter]=$KdKategori;
        $arr_data["data_NamaKategori"][$counter]=$NamaKategori;
        $arr_data["data_Approval_By"][$counter]=$Approval_By;
        $arr_data["data_Approval_Date"][$counter]=$Approval_Date;
        $arr_data["data_Approval_Status"][$counter]=$Approval_Status;
        $arr_data["data_Approval_Remarks"][$counter]=$Approval_Remarks;
        $arr_data["data_AddDate"][$counter]=$AddDate;
        $arr_data["data_EditDate"][$counter]=$EditDate;
        
        $arr_data["data_Harga1c"][$counter]=$Harga1c;
        $arr_data["data_komisi"][$counter]=$komisi;
        $arr_data["data_DiscInternal"][$counter]=$DiscInternal;
        $arr_data["data_Service_charge"][$counter]=$Service_charge;
        $arr_data["data_PPN"][$counter]=$PPN;
        $arr_data["data_Status"][$counter]=$Status;
        
        $counter++;
    }
}  


$q="SELECT KdDivisi,NamaDivisi FROM divisi WHERE 1 AND divisi.KdDivisi='".$search_KdDivisi."'";
$qry = mysql_query($q);
$row2 = mysql_fetch_array($qry);

$file_name = "master-barang.xls";
header("Content-Disposition".": "."attachment;filename=$file_name");
header("Content-type: application/vnd.ms-excel");


?>

	<table border="1">
        <tr>
            <td colspan="15" style="font-weight: bold;">PT. NATURA PESONA MANDIRI</td>
        </tr>
        
        <tr>
            <td colspan="15" style="font-weight: bold;">Master Barang <?php echo $row2["NamaDivisi"]; ?></td>
        </tr>
        
        <tr>
            <td colspan="15">&nbsp;</td>
        </tr>
        
        <tr style="font-weight: bold;">
            <td width="30">No</td>
            <td>PCode</td>
            <td>Barcode</td>
            <td>Nama Barang</td>
            <td>Satuan</td>
            <td>Divisi</td>
            <td>Kategori</td>     
            <td>Add Date</td>      
            <td>Approval</td>       
            <td>Harga</td>      
            <td>PPN</td>      
            <td>Service Charge</td>   
            <td>Komisi</td>   
            <td>Disc Internal</td> 
            <td>Status</td>   
        </tr>

        <!-- gak bisa commit -->
        
        <?php
        
        $nomor = 0;
        foreach($arr_data["list_data"] as $counter => $val)
        {    
            $nomor++;
            
            $PCode = $arr_data["data_PCode"][$counter];
            $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
            $Barcode1 = $arr_data["data_Barcode1"][$counter];
            $SatuanSt = $arr_data["data_SatuanSt"][$counter];
            $KdDivisi = $arr_data["data_KdDivisi"][$counter];
            $NamaDivisi = $arr_data["data_NamaDivisi"][$counter];
            $KdKategori = $arr_data["data_KdKategori"][$counter];
            $NamaKategori = $arr_data["data_NamaKategori"][$counter];
            $Approval_By = $arr_data["data_Approval_By"][$counter];
            $Approval_Date = $arr_data["data_Approval_Date"][$counter];
            $Approval_Status = $arr_data["data_Approval_Status"][$counter];
            $Approval_Remarks = $arr_data["data_Approval_Remarks"][$counter];
            $AddDate = $arr_data["data_AddDate"][$counter];
            $EditDate = $arr_data["data_EditDate"][$counter];
	        $Harga1c = $arr_data["data_Harga1c"][$counter];
	        $komisi = $arr_data["data_komisi"][$counter];
	        $DiscInternal = $arr_data["data_DiscInternal"][$counter];
	        $Service_charge = $arr_data["data_Service_charge"][$counter];
	        $PPN = $arr_data["data_PPN"][$counter];
	        $Status = $arr_data["data_Status"][$counter];
            
            if($Approval_By=="")
            {
                $approval = "waiting";
            } 
            else
            {
                if($Approval_Status==1)
                {
                    $approval = "<font color='blue'>".$Approval_By." - ".format_show_datetime($Approval_Date)."</font>";
                }
                else if($Approval_Status==2)
                {
                    $approval = "<font color='red'>".$Approval_By." - ".format_show_datetime($Approval_Date)."<br>".$Approval_Remarks."</font>";
                }
            }
            
            
            $style_color = "";
            if($v_PCode_curr==$PCode)
            {
                $style_color = "background: #CAFDB5";
            }

            //$Status='';
            if($Status=="A"){
                $status='Aktif';
            }else{
                $status='NonAktif';
            }
                    
            ?>
            <tr>
                <td align="" style=""><?php echo $nomor; ?></td>
                <td align="" style="mso-number-format:'\@';"><?php echo $PCode; ?></td> 
                <td align="" style="mso-number-format:'\@';"><?php echo $Barcode1; ?></td> 
                <td align="" style=""><?php echo $NamaLengkap; ?></td> 
                <td align="" style=""><?php echo $SatuanSt; ?></td> 
                <td style=""><?php echo $NamaDivisi; ?></td>  
                <td style=""><?php echo $NamaKategori; ?></td>  
                <td style="text-align: center;"><?php echo format_show_datetime($AddDate); ?></td>  
                <td style=""><?php echo $approval; ?></td>  
                <td style="mso-number-format: '\#\,\#\#\#0\.00';" align="right"><?php echo format_number($Harga1c, 2); ?></td>  
                <td style="mso-number-format: '\#\,\#\#\#0\.00';" align="right"><?php echo format_number($PPN, 2); ?></td>  
                <td style="mso-number-format: '\#\,\#\#\#0\.00';" align="right"><?php echo format_number($Service_charge, 2); ?></td>  
                <td style="mso-number-format: '\#\,\#\#\#0\.00';" align="right"><?php echo format_number($komisi, 2); ?></td>  
                <td style="mso-number-format: '\#\,\#\#\#0\.00';" align="right"><?php echo format_number($DiscInternal, 2); ?></td> 
                <td style=""><?php echo $status; ?></td>   
            </tr>
            <?php     
        } 
              
        
    ?>    
    
    </table>
    
<?php mysql_close(); ?>