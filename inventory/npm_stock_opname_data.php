<?php
include("header.php");
                               
$modul            = "STOCK OPNAME";
$file_current     = "npm_stock_opname.php";
$file_name        = "npm_stock_opname_data.php";

//$ses_login = "hendri1003";



function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}
     
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
		$search_keyword        = trim($_GET["search_keyword"]);
        $search_gudang         = trim($_GET["search_gudang"]);
		$v_date_from           = trim($_GET["v_date_from"]);
		$v_date_to             = trim($_GET["v_date_to"]);
		$v_NoDokumen_curr      = trim($_GET["v_NoDokumen_curr"]);
        //echo $v_NoDokumen_curr;               
		
		$where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "opname_header.NoDokumen";    
            $arr_keyword[1] = "opname_header.Keterangan";    
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        $where .= " AND opname_header.TglDOkumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";
        
        if($search_gudang)
        {
            $where .= " AND opname_header.KdGudang = '".$search_gudang."'";
        }
        
        // select query all
        {
            $counter = 1;
			$q="
				SELECT 
				  ".$db["master"].".opname_header.NoDokumen,
				  ".$db["master"].".opname_header.TglDokumen,
				  ".$db["master"].".gudang.KdGudang,
				  ".$db["master"].".gudang.Keterangan AS NamaGudang,
				  ".$db["master"].".opname_header.Status,
				  ".$db["master"].".opname_header.Approval_By,
                  ".$db["master"].".opname_header.Approval_Date,
                  ".$db["master"].".opname_header.Approval_Status,
                  ".$db["master"].".opname_header.Approval_Remarks
				FROM
				  ".$db["master"].".opname_header 
				  INNER JOIN ".$db["master"].".gudang 
				    ON ".$db["master"].".opname_header.KdGudang = ".$db["master"].".gudang.KdGudang 
				WHERE 1 
					".$where."
				ORDER BY 
					".$db["master"].".opname_header.TglDokumen DESC,
					".$db["master"].".opname_header.NoDokumen ASC
			";
			$sql=mysql_query($q);
			while($row=mysql_fetch_array($sql))
			{ 
				list(
					$NoDokumen,
					$TglDokumen,
					$KdGudang,
					$NamaGudang,
					$status,
                    $Approval_By,
                    $Approval_Date,
                    $Approval_Status,
                    $Approval_Remarks
                )=$row;
				
				$arr_data["list_data"][$counter]=$counter;
				$arr_data["data_NoDokumen"][$counter]=$NoDokumen;
				$arr_data["data_TglDokumen"][$counter]=$TglDokumen;
				$arr_data["data_KdGudang"][$counter]=$KdGudang;
				$arr_data["data_NamaGudang"][$counter]=$NamaGudang;
				$arr_data["data_status"][$counter]=$status;
				$arr_data["data_Approval_By"][$counter]=$Approval_By;
                $arr_data["data_Approval_Date"][$counter]=$Approval_Date;
                $arr_data["data_Approval_Status"][$counter]=$Approval_Status;
                $arr_data["data_Approval_Remarks"][$counter]=$Approval_Remarks;
                
				$counter++;
			}
		} 
        
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_receipt_goods_print.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
    	<thead>
			<tr>
	            <th width="30"><strong><center>No</center></strong></th>
	            <th><strong><center>No Dokumen</center></strong></th>
	            <th><strong><center>Tanggal</center></strong></th>
	            <th><strong><center>Gudang</center></strong></th>
                <th><strong><center>Approval</center></strong></th>    
	            <th width="80"><strong><center>Status</center></strong></th>    
                <th width="15"><strong><center><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></center></strong></th>    
			</tr>
			
	        
		</thead>
		<tbody style="color: black;">
		
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        $photo_echo="";    
        foreach($arr_data["list_data"] as $counter => $val)
        {    
        	$nomor++;
        	
            $NoDokumen = $arr_data["data_NoDokumen"][$counter];
            $TglDokumen = $arr_data["data_TglDokumen"][$counter];
            $KdGudang = $arr_data["data_KdGudang"][$counter];
            $NamaGudang = $arr_data["data_NamaGudang"][$counter];
            $status = $arr_data["data_status"][$counter];
            $Approval_By = $arr_data["data_Approval_By"][$counter];
            $Approval_Date = $arr_data["data_Approval_Date"][$counter];
            $Approval_Status = $arr_data["data_Approval_Status"][$counter];
            $Approval_Remarks = $arr_data["data_Approval_Remarks"][$counter];
            
            if($Approval_By=="")
            {
                $approval = "waiting";
            } 
            else
            {
                if($Approval_Status==1)
                {
                    $approval = "<font color='blue'>".$Approval_By." - ".format_show_datetime($Approval_Date)."</font>";
                }
                else if($Approval_Status==2)
                {
                    $approval = "<font color='red'>".$Approval_By." - ".format_show_datetime($Approval_Date)."<br>".$Approval_Remarks."</font>";
                }
            }
            
            if($status==0)
            {
                $status_echo = "Pending";
            }
            else if($status==1)
            {
                $status_echo = "Open";
            }
            else if($status==2)
            {
                $status_echo = "<font color='red'>Void</font>";
            }  
            
            $style_color = "";
            if($v_NoDokumen_curr==$NoDokumen)
            {
                $style_color = "background: #CAFDB5";
            }
            		
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><nobr><?php echo $NoDokumen; ?></nobr></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo format_show_date($TglDokumen); ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $NamaGudang; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $approval; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $status_echo; ?></td>  
                <td align="center">
                    &nbsp;
                    <?php 
                        if($status==1)
                        {
                    ?>
                    <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $NoDokumen; ?>">
                    <?php 
                        }
                    ?>
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot>
            <tr>
                <td colspan="100%" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Print">Print</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
	</table> 
	</form>
	</div>  
   	<?php  
    }

	else if($ajax=="add_data")
	{
        $q = "
            SELECT
                ".$db["master"].".aplikasi.TglTrans
            FROM
                ".$db["master"].".aplikasi
            WHERE
                1
        ";
        $qry_aplikasi = mysql_query($q);
        $row_aplikasi = mysql_fetch_array($qry_aplikasi);
        list($TglTrans) = $row_aplikasi;
        
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
        ";
        $qry_gudang = mysql_query($q);
        while($row_gudang = mysql_fetch_array($qry_gudang))
        { 
            list($KdGudang, $NamaGudang) = $row_gudang;    
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
            
        }
        
        
        ?>
        <div class="col-md-12" align="left">
        
        	<ol class="breadcrumb">
				<li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">Tanggal</td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_TglDokumen" id="v_TglDokumen" size="10" maxlength="10" value="<?php echo format_show_date($TglTrans); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglDokumen, 'dd/mm/yyyy');">
                </td>
                
               
                
            </tr>  

            <tr>
                 <td class="title_table">Gudang</td>
                <td>
                    <select name="v_KdGudang" id="v_KdGudang" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                $selected = "";
                                if($arr_curr["KdGudang"]==$KdGudang)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
                
            <tr>
               <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_Keterangan" id="v_Keterangan" style="width: 200px;" value=""> 
                </td> 
            </tr>
                
           
            
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
            
        	</table>  
        	</form>
        </div>
        <?php
		
	}
    
    else if($ajax=="edit_data")
    {                                      
		$v_NoDokumen = $_GET["v_NoDokumen"];  
        
        $q = "
                SELECT
                    *
                FROM 
                    ".$db["master"].".opname_header
                WHERE
                    1
                    AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'
                LIMIT
                    0,1
        ";     
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
        
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
        ";
        $qry_gudang = mysql_query($q);
        while($row_gudang = mysql_fetch_array($qry_gudang))
        { 
            list($KdGudang, $NamaGudang) = $row_gudang;    
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
            
        }
        
        $ada_data = false;
        $q = "
                SELECT
                    ".$db["master"].".opname_detail.Sid,    
                    ".$db["master"].".opname_detail.PCode,
                    ".$db["master"].".opname_detail.QtyFisik,
                    ".$db["master"].".opname_detail.QtyProgram
                FROM
                    ".$db["master"].".opname_detail
                WHERE
                    1
                    AND ".$db["master"].".opname_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
                ORDER BY
                    ".$db["master"].".opname_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        { 
            list(
                $Sid,    
                $PCode,
                $QtyFisik,
                $QtyProgram
            ) = $row;    
            
            $arr_data["data_QtyFisik"][$PCode] = $QtyFisik;
            $arr_data["data_QtyProgram"][$PCode] = $QtyProgram;
            
            $ada_data = true;
        }
        
        
        $q = "
                SELECT
                    ".$db["master"].".masterbarang.PCode,    
                    ".$db["master"].".masterbarang.NamaLengkap,
                    ".$db["master"].".masterbarang.SatuanSt,
                    tbl_SubKategori.KdSubKategori,
                    tbl_SubKategori.NamaSubKategori
                FROM
                    ".$db["master"].".masterbarang
                    INNER JOIN 
                    (
                        SELECT
                            tbl_uni.KdSubKategori,
                            tbl_uni.NamaSubKategori
                        FROM
                        (
                        SELECT
                            `subkategori`.`KdSubKategori`,
                            `subkategori`.`NamaSubKategori`
                        FROM
                            `subkategori`

                        UNION ALL

                        SELECT
                            `subkategoripos`.`KdSubKategori`,
                            `subkategoripos`.`NamaSubKategori`
                        FROM
                            `subkategoripos`
                        ) AS tbl_uni
                        WHERE
                            1
                        ORDER BY
                            tbl_uni.KdSubKategori ASC     
                    ) AS tbl_SubKategori ON
                        ".$db["master"].".masterbarang.KdSubKategori = tbl_SubKategori.KdSubKategori
                    INNER JOIN ".$db["master"].".gudang_subkategori ON
                        tbl_SubKategori.KdSubKategori = ".$db["master"].".gudang_subkategori.KdSubKategori
                        AND ".$db["master"].".gudang_subkategori.KdGudang = '".$arr_curr["KdGudang"]."'
                WHERE
                    1 AND masterbarang.Status='A'
                ORDER BY
                    tbl_SubKategori.KdSubKategori ASC,
                    ".$db["master"].".masterbarang.NamaLengkap ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        { 
            list(
                $PCode,
                $NamaLengkap,
                $Satuan1,
                $KdSubKategori,
                $NamaSubKategori
            ) = $row;    
            
            $arr_data["SubKategori_PCode"][$KdSubKategori][$PCode] = $PCode;
            $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
            $arr_data["satuan"][$PCode] = $Satuan1;
            
            $arr_data["list_KdSubKategori"][$KdSubKategori] = $KdSubKategori;
            $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
            
            $arr_data["list_pcode"][$PCode] = $PCode;
        }
        
        $arr_stock = get_stock($arr_curr["KdGudang"], $arr_curr["TglDokumen"], $arr_data["list_pcode"]);
        
        ?> 
        <div class="col-md-12" align="left">
        	
        	<ol class="breadcrumb">
				<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			                                           
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value="">    
        <input type="hidden" name="v_NoDokumen" id="v_NoDokumen" value="<?php echo $arr_curr["NoDokumen"]; ?>">   
        
        <table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="200">No Dokumen</td>
                <td style="color: black; font-weight: bold"><?php echo $arr_curr["NoDokumen"]; ?></td>
            </tr>
        
           <tr>
                <td class="title_table" width="200">Tanggal</td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_TglDokumen" id="v_TglDokumen" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["TglDokumen"]); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglDokumen, 'dd/mm/yyyy');">
                </td>
                
                
                
            </tr>  

            <tr>
                <td class="title_table" width="200">Gudang</td>
                <td>
                    <select name="v_KdGudang" id="v_KdGudang" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                $selected = "";
                                if($arr_curr["KdGudang"]==$KdGudang)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
                
            </tr>
                
            <tr>
                
                <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new" name="v_Keterangan" id="v_Keterangan" style="width: 200px;" value="<?php echo $arr_curr["Keterangan"]; ?>"> 
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Status</td>
                <td>
                   <?php 
                    if($arr_curr["Status"]==0)
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option value="0">Pending</option>    
                            <option value="1">Open</option>    
                        </select>
                        <?php
                    }
                    else if($arr_curr["Status"]==1)
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option value="1">Open</option>    
                        </select>
                        <?php
                    }
                    else if($arr_curr["Status"]==2)
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option value="2">Void</option>    
                        </select>
                        <?php
                    }
                    else
                    {
                        ?>
                        <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                            <option <?php if($arr_curr["Status"]==1) echo "selected='selected'"; ?> value="1">Open</option>    
                            <option <?php if($arr_curr["Status"]==2) echo "selected='selected'"; ?> value="2">Void</option>    
                        </select>
                        <?php    
                    }
                   ?>
                </td>
                
                 
            </tr>
                
               
            <tr> 
                <td>&nbsp;</td>                   
                <td>
                <?php 
                    if($arr_curr["Approval_Status"]==0)
                    {
                ?>
                	<button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                	
                    <?php 
                        if($arr_curr["Status"]==0 || $arr_curr["Status"]==1)
                        {
                    ?>
                	<button type="submit" name="btn_delete" id="btn_delete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_delete('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Void">Void<i class="entypo-trash"></i></button>
                    <?php 
                        }
                        
                        if($arr_curr["Status"]==2)
                        {
                            ?>
                    <button type="submit" name="btn_unvoid" id="btn_unvoid" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_unvoid('<?php echo $arr_curr["NoDokumen"]; ?>');" value="UnVoid">UnVoid<i class="entypo-trash"></i></button>
                            <?php
                        }
                    ?>
                <?php 
                    }
                    else
                    {
                        echo "<font color='red'>Button Save tidak Muncul Karena Sudah Proses Approval</font>";
                    }
                ?>
                
                <button type="button" name="btn_excel" id="btn_excel" class="btn btn-info btn-icon btn-sm icon-left" onclick="generate_excel('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Excel">Excel<i class="entypo-check"></i></button>
                    
                    <?php 
                    
                        if($arr_curr["Status"]==1 && $arr_curr["Approval_Status"]==0)
                        {
                            if($ses_login=="dicky0707" || $ses_login=="mart2005" || $ses_login=="sari1608" || $ses_login=="trisno1402" || $ses_login=="william1001" || $ses_login=="febri0202")
                            {
                               ?>
                                <span style="float: right;">
                                <button type="submit" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
                                
                                <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
                                
                                <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
                                <button style="display: none;" type="submit" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
                                </span> 
                                <?php 
                            }
                            else
                            {
                                echo "<span style='float: right; color: blue;'>Approval Waiting William Budiarsa atau Bambang Sutrisno</a>";
                            }
                        }
                    ?>
                    
                    
               </td>
            </tr> 
            
            <?php 
                if($arr_curr["Status"]==0 && $arr_curr["Approval_Status"]==0 || $ses_login=="hendri1003")
                {
            ?>
            <tr>
                <td colspan="100%">
                    <button type="button" name="btn_import_csv" id="btn_import_csv" class="btn btn-info btn-icon btn-sm icon-left" value="Import CSV" onclick="import_csv('<?php echo $arr_curr["NoDokumen"]; ?>')">Import CSV<i class="entypo-check"></i></button>    
                </td>
            </tr>
            <?php 
                }
            ?>
            
            <tr>
                <td colspan="100%">
                    <table width="100%" class="table table-bordered responsive">
                        <thead>
                            <tr>
                                <td width="30">No</td>    
                                <td>PCode</td>    
                                <td>Nama Barang</td>    
                                <td align="right">Qty Fisik</td>    
                                <td align="left">Satuan</td>    
                                <td align="right">Qty Program</td>    
                                <td align="right">Selisih</td>    
                            </tr>
                        </thead>
                        
                        <tbody style="color: black;">
                        <?php
                             
                            foreach($arr_data["list_KdSubKategori"] as $KdSubKategori=>$val)
                            {
                                $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
                                
                                    ?>
                                    <tr>
                                        <td colspan="100%" style="font-weight: bold;"><?php echo $NamaSubKategori; ?></td>
                                    </tr>
                                    <?php
                                    $no = 1;
                                    foreach($arr_data["SubKategori_PCode"][$KdSubKategori] as $PCode=>$val)
                                    {
                                        $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                                        $satuan = $arr_data["satuan"][$PCode];
                                        
                                        $QtyFisik = $arr_data["data_QtyFisik"][$PCode];
                                        $QtyProgram = $arr_data["data_QtyProgram"][$PCode];
            
                                        if(!$ada_data)
                                        {
                                            $QtyProgram = $arr_stock["akhir"][$PCode];
                                        }
                                        
                                        $selisih = $QtyFisik - $QtyProgram;
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="v_PCode[]" value="<?php echo $PCode; ?>">
                                                <input type="hidden" name="v_QtyProgram_<?php echo $PCode; ?>" id="v_QtyProgram_<?php echo $PCode; ?>" value="<?php echo format_number($QtyProgram, 2); ?>">
                                                <?php echo $no; ?>
                                            </td>    
                                            <td><?php echo $PCode; ?></td>    
                                            <td><?php echo $NamaLengkap; ?></td>    
                                            <td align="right"><input type="text" title="Double Klik U/ Salin Qty Program" ondblclick="copy_QtyProgram('<?php echo $PCode; ?>')" class="form-control-new" style="width: 150px; text-align: right;" name="v_QtyFisik_<?php echo $PCode; ?>" id="v_QtyFisik_<?php echo $PCode; ?>" onblur="toFormat2('v_QtyFisik_<?php echo $PCode; ?>'); calculate('<?php echo $PCode; ?>')" onkeyup="calculate('<?php echo $PCode; ?>')" value="<?php echo format_number($QtyFisik, 2); ?>" autocomplete="off"></td>    
                                            <td><?php echo $satuan; ?></td>    
                                            <td align="right"><?php echo format_number($QtyProgram, 2); ?></td>    
                                            <td align="right" id="td_selisih_<?php echo $PCode; ?>">
                                            	<input type="hidden" id="v_selisih_<?php echo $PCode; ?>" name="v_selisih_<?php echo $PCode; ?>" value="<?php echo $selisih; ?>"/>
                                            	<?php echo format_number($selisih, 2); ?>
                                            </td>    
                                        </tr> 
                                        <?php 
                                        $no++;  
                                        
                                        $arr_total["div_QtyFisik"][$KdSubKategori] += $QtyFisik; 
                                        $arr_total["div_QtyProgram"][$KdSubKategori] += $QtyProgram;
                                        $arr_total["div_Selisih"][$KdSubKategori] += $selisih;
                                        
                                        $arr_total["all_QtyFisik"] += $QtyFisik; 
                                        $arr_total["all_QtyProgram"] += $QtyProgram;
                                        $arr_total["all_Selisih"] += $selisih;
                                    }
                                
                                ?>
                                <tr style="text-align: right; font-weight: bold;">
                                    <td colspan="3">Total <?php echo $NamaSubKategori; ?></td>
                                    <td><?php echo format_number($arr_total["div_QtyFisik"][$KdSubKategori],2); ?></td>
                                    <td>&nbsp;</td>
                                    <td><?php echo format_number($arr_total["div_QtyProgram"][$KdSubKategori],2); ?></td>
                                    <td><?php echo format_number($arr_total["div_Selisih"][$KdSubKategori],2); ?></td>
                                </tr>
                                <?php
                              
                            }
                            
                        ?>    
                        </tbody>
                        
                        <tfoot>
                            <tr style="text-align: right; font-weight: bold;">
                                <td colspan="3">GRAND TOTAL</td>
                                <td><?php echo format_number($arr_total["all_QtyFisik"],2); ?></td>
                                <td>&nbsp;</td>
                                <td><?php echo format_number($arr_total["all_QtyProgram"],2); ?></td>
                                <td><?php echo format_number($arr_total["all_Selisih"],2); ?></td>
                            </tr> 
                        </tfoot>
                    </table>
                
                </td>
            </tr>
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                
                if(!isset($_POST["v_TglDokumen"])){ $v_TglDokumen = isset($_POST["v_TglDokumen"]); } else { $v_TglDokumen = $_POST["v_TglDokumen"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_Keterangan"])){ $v_Keterangan = isset($_POST["v_Keterangan"]); } else { $v_Keterangan = $_POST["v_Keterangan"]; } 

				$v_TglDokumen    = format_save_date($v_TglDokumen);
				$v_KdGudang      = save_char($v_KdGudang);
                $v_Keterangan    = save_char($v_Keterangan);
                    
                if($v_KdGudang=="")
                {      
                	$msg = "Gudang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                          
                // RG00002-05-16
                
                $arr_date = explode("-", $v_TglDokumen);
                $mm   = $arr_date[1];
                $yyyy = $arr_date[0];
                
	            $NoDokumen = get_code_counter($db["master"], "opname_header","NoDokumen", "SO", $mm, $yyyy);
	            
	            // insert po
	            {
		            $q = "
			            INSERT INTO
			                ".$db["master"].".opname_header
			            SET
			                NoDokumen = '".$NoDokumen."',
			                TglDokumen = '".$v_TglDokumen."',
			                KdGudang = '".$v_KdGudang."',
                            Keterangan = '".$v_Keterangan."',
                            Status = '0',
                            AddDate = NOW(),
			                AddUser = '".$ses_login."',
			                EditDate = NOW(),
			                EditUser = '".$ses_login."'
			        "; 
                    //echo $q;  
			        if(!mysql_query($q))
			        {
			            $msg = "Gagal menyimpan opname_header";
			            echo "<script>alert('".$msg."');</script>";
			            die(); 
			        }	
				}
				
				$msg = "Berhasil menyimpan ".$NoDokumen;
	            echo "<script>alert('".$msg."');</script>"; 
				echo "<script>parent.CallAjaxForm('search','".$NoDokumen."');</script>";         	
	            
	            die();
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {                                                                                                      
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }    
                if(!isset($_POST["v_NoDokumen"])){ $v_NoDokumen = isset($_POST["v_NoDokumen"]); } else { $v_NoDokumen = $_POST["v_NoDokumen"]; }  
                
                if(!isset($_POST["v_TglDokumen"])){ $v_TglDokumen = isset($_POST["v_TglDokumen"]); } else { $v_TglDokumen = $_POST["v_TglDokumen"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_Keterangan"])){ $v_Keterangan = isset($_POST["v_Keterangan"]); } else { $v_Keterangan = $_POST["v_Keterangan"]; } 
                if(!isset($_POST["v_Status"])){ $v_Status = isset($_POST["v_Status"]); } else { $v_Status = $_POST["v_Status"]; } 
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }  
                
                if(!isset($_POST["v_PCode"])){ $v_PCode = isset($_POST["v_PCode"]); } else { $v_PCode = $_POST["v_PCode"]; } 
                
                $v_NoDokumen     = save_char($v_NoDokumen);
                $v_TglDokumen    = format_save_date($v_TglDokumen);
                $v_KdGudang      = save_char($v_KdGudang);
                $v_Keterangan    = save_char($v_Keterangan);
                $v_Status        = save_char($v_Status);
                
                if($v_KdGudang=="")
                {      
                    $msg = "Gudang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }

                if($v_del==1)   
                {
                	if($btn_delete=="Void")
                	{
                		$q = "
                            UPDATE
                                ".$db["master"].".opname_header
                            SET     
                                Status = '2',
                                EditDate = NOW(),
                                EditUser = '".$ses_login."' 
                             WHERE 1
                                AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan opname_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        //get_mutasi_stock("RG", $v_NoDokumen);
                               
	                    $msg = "Berhasil Void ".$v_NoDokumen;
	                    echo "<script>alert('".$msg."');</script>"; 
	                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
	                    die();
					}
				}
                else  if($v_undel==1)   
                {
                    if($btn_unvoid=="UnVoid")
                    {
                        $q = "
                            UPDATE
                                ".$db["master"].".opname_header
                            SET     
                                Status = '1',
                                EditDate = NOW(),
                                EditUser = '".$ses_login."' 
                             WHERE 1
                                AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan opname_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        //get_mutasi_stock("RG", $v_NoDokumen);
                               
                        $msg = "Berhasil UnVoid ".$v_NoDokumen;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                        die();     
                        
                    }
                }
                else  if($v_approve==1)   
                {
                    if($btn_approve=="Approve")
                    {
                        $q = "
                            UPDATE
                                ".$db["master"].".opname_header
                            SET     
                                Approval_By = '".$ses_login."',
                                Approval_Date = NOW(),
                                Approval_Status = '1'
                             WHERE 1
                                AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan opname_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        else
                        {
                            get_mutasi_stock("SO", $v_NoDokumen);
                            
                            $v_TglDokumen_arr = explode("-",$v_TglDokumen);
                            
                            foreach($v_PCode as $key=>$val_PCode)
                            {
                                if(!isset($_POST["v_QtyFisik_".$val_PCode])){ $v_QtyFisik = isset($_POST["v_QtyFisik_".$val_PCode]); } else { $v_QtyFisik = $_POST["v_QtyFisik_".$val_PCode]; }     
                                if(!isset($_POST["v_QtyProgram_".$val_PCode])){ $v_QtyProgram = isset($_POST["v_QtyProgram_".$val_PCode]); } else { $v_QtyProgram = $_POST["v_QtyProgram_".$val_PCode]; }     
                                if(!isset($_POST["v_selisih_".$val_PCode])){ $v_selisih = isset($_POST["v_selisih_".$val_PCode]); } else { $v_selisih = $_POST["v_selisih_".$val_PCode]; }     
                                
                                $v_QtyFisik   	= save_int($v_QtyFisik);
                                $v_QtyProgram 	= save_int($v_QtyProgram);
                                $v_selisih 		= save_int($v_selisih);

                                if($v_QtyFisik || $v_QtyProgram)
                                {
                                	// tahun,gudang,pcode,bulan,selisih
                                    get_opname_stock($v_TglDokumen_arr[0],$v_KdGudang,$val_PCode,$v_TglDokumen_arr[1],$v_selisih);
                                }
                            }
                                   
                            $msg = "Berhasil Approve ".$v_NoDokumen;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                            die();
                        }
                    }
                }
                else  if($v_reject==1)   
                {
                    if($btn_reject=="Reject")
                    {
                        if($v_Approval_Remarks=="")
                        {
                            $msg = "Alasan Reject harus diisi...";
                            echo "<script>alert('".$msg."');</script>";
                        }
                        else
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".opname_header
                                SET     
                                    Approval_By = '".$ses_login."',
                                    Approval_Date = NOW(),
                                    Approval_Status = '2',
                                    Approval_Remarks = '".$v_Approval_Remarks."'
                                 WHERE 1
                                    AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan opname_header";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            } 
                            else
                            {
                                get_mutasi_stock("SO", $v_NoDokumen);
                                   
                                $msg = "Berhasil Reject ".$v_NoDokumen;
                                echo "<script>alert('".$msg."');</script>"; 
                                echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                                die();    
                            }
                            
                        }
                    }
                }
                else
                {
                    if($btn_save=="Save")
                    {                       	
						// update po
						{
							$q = "
	                            UPDATE
	                            	".$db["master"].".opname_header
	                            SET     
	                            	TglDokumen = '".$v_TglDokumen."',
                                    KdGudang = '".$v_KdGudang."',
                                    Keterangan = '".$v_Keterangan."',
                                    Status = '".$v_Status."',
                                    EditDate = NOW(),
                                    EditUser = '".$ses_login."' 
	                             WHERE 1
	                                AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'   
	                        ";
	                        //echo $q."<hr/>";die;                  
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan opname_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        }
                            
                            $q = "DELETE FROM ".$db["master"].".opname_detail WHERE 1 AND NoDokumen = '".$v_NoDokumen."' ";
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal reset opname_detail";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            
                            foreach($v_PCode as $key=>$val_PCode)
                            {
                                if(!isset($_POST["v_QtyFisik_".$val_PCode])){ $v_QtyFisik = isset($_POST["v_QtyFisik_".$val_PCode]); } else { $v_QtyFisik = $_POST["v_QtyFisik_".$val_PCode]; }     
                                if(!isset($_POST["v_QtyProgram_".$val_PCode])){ $v_QtyProgram = isset($_POST["v_QtyProgram_".$val_PCode]); } else { $v_QtyProgram = $_POST["v_QtyProgram_".$val_PCode]; }     
                                
                                $v_QtyFisik   = save_int($v_QtyFisik);
                                $v_QtyProgram = save_int($v_QtyProgram);

                                if($v_QtyFisik || $v_QtyProgram)
                                {
                                    $q = "
                                            INSERT INTO ".$db["master"].".`opname_detail`
                                            SET 
                                              `NoDokumen` = '".$v_NoDokumen."',
                                              `PCode` = '".$val_PCode."',
                                              `QtyFisik` = '".$v_QtyFisik."',
                                              `QtyProgram` = '".$v_QtyProgram."'
                                    ";
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal Insert opname_detail";
                                        echo "<script>alert('".$msg."');</script>";
                                        die(); 
                                    } 
                                }
                            }
						}
                        

						$msg = "Berhasil menyimpan ".$v_NoDokumen;
		                echo "<script>alert('".$msg."');</script>"; 
		                echo "<script>parent.CallAjaxForm('search','".$v_NoDokumen."');</script>";         
		                die();
                    } 
                }  
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  