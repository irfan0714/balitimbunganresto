<?php               
    include("header.php");
    
    
    if(!isset($_POST["v_type_barang"])){ $v_type_barang = isset($_POST["v_type_barang"]); } else { $v_type_barang = $_POST["v_type_barang"]; }
    if(!isset($_POST["v_KdDivisi"])){ $v_KdDivisi = isset($_POST["v_KdDivisi"]); } else { $v_KdDivisi = $_POST["v_KdDivisi"]; }
    if(!isset($_POST["v_KdKategori"])){ $v_KdKategori = isset($_POST["v_KdKategori"]); } else { $v_KdKategori = $_POST["v_KdKategori"]; }
    if(!isset($_POST["v_market_list_id"])){ $v_market_list_id = isset($_POST["v_market_list_id"]); } else { $v_market_list_id = $_POST["v_market_list_id"]; }
    
    if(!isset($_POST["v_keyword"])){ $v_keyword = isset($_POST["v_keyword"]); } else { $v_keyword = $_POST["v_keyword"]; }
    if(!isset($_POST["v_sort_by"])){ $v_sort_by = isset($_POST["v_sort_by"]); } else { $v_sort_by = $_POST["v_sort_by"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Master Barang";
    
    $q = "
        SELECT
            ".$db["master"].".divisi.KdDivisi,
            ".$db["master"].".divisi.NamaDivisi
        FROM
            ".$db["master"].".divisi
        WHERE
            1
        ORDER BY
            ".$db["master"].".divisi.NamaDivisi ASC
    ";
    $qry_divisi = mysql_query($q);
    while($row_divisi = mysql_fetch_array($qry_divisi))
    {  
        list($KdDivisi, $NamaDivisi) = $row_divisi;
        
        $arr_data["list_divisi"][$KdDivisi] = $KdDivisi;
        $arr_data["NamaDivisi"][$KdDivisi] = $NamaDivisi;
    } 
    
    $q = "
        SELECT
            ".$db["master"].".kategori.KdKategori,
            ".$db["master"].".kategori.NamaKategori
        FROM
            ".$db["master"].".kategori
        WHERE
            1
        ORDER BY
            ".$db["master"].".kategori.NamaKategori ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdKategori, $NamaKategori) = $row;
        
        $arr_data["list_kategori"][$KdKategori] = $KdKategori;
        $arr_data["NamaKategori"][$KdKategori] = $NamaKategori;
    }   
    
    $q = "
        SELECT
            ".$db["master"].".market_list.market_list_id,
            ".$db["master"].".market_list.market_list_name
        FROM
            ".$db["master"].".market_list
        WHERE
            1
        ORDER BY
            ".$db["master"].".market_list.market_list_name ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($market_list_id, $market_list_name) = $row;
        
        $arr_data["list_market_list"][$market_list_id] = $market_list_id;
        $arr_data["market_list_name"][$market_list_id] = $market_list_name;
    }      
    
    if($v_type_barang=="Reguler" || $v_type_barang=="")
    {
        $display_divisi = "";
        $display_kategori = "";
        $display_market_list = "none;";
    }
    else if($v_type_barang=="Market List")
    {
        $display_divisi = "none;";
        $display_kategori = "none;";
        $display_market_list = "";
    }
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report_master_barang.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        //document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  

function change_type_barang(nilai)
{
    if(nilai=="Reguler")
    {
        document.getElementById("tr_divisi").style.display='';
        document.getElementById("tr_kategori").style.display='';
        
        document.getElementById("tr_market_list").style.display='none';
    }
    else if(nilai=="Market List")
    {
        document.getElementById("tr_divisi").style.display='none';
        document.getElementById("tr_kategori").style.display='none';
        
        document.getElementById("tr_market_list").style.display='';
    }
    
}

   
    </script>
    
    <style>
        .link_pop{
            text-decoration: underline;
            color: black;
        }
        
        .link_pop:hover{
            text-decoration: none;
            color: #222222;
        }
        
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="POST" name="theform" id="theform">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
                    <tr class="title_table">
                        <td colspan="100%"><?php echo $modul; ?></td>
                    </tr>
                    
                    
                   <tr>
                        <td class="title_table" width="150">Tipe Barang</td>
                        <td>: 
                            <select class="form-control-new" name="v_type_barang" id="v_type_barang" style="width: 200px;" onchange="change_type_barang(this.value)">
                                <option <?php if($v_type_barang=="Reguler") echo "selected=''"; ?> value="Reguler">Reguler</option>
                                <option <?php if($v_type_barang=="Market List") echo "selected=''"; ?> value="Market List">Market List</option>
                            </select>
                        </td>
                    </tr>
                   
                    <tr id="tr_divisi" style="display: <?php echo $display_divisi; ?>">
                        <td class="title_table">Divisi</td>
                        <td>: 
                                
                                  <select class="form-control-new" name="v_KdDivisi" id="v_KdDivisi" style="width: 200px;">
                                    <option value="">Semua</option>
                                    
                                    <?php 
                                        foreach($arr_data["list_divisi"] as $KdDivisi=>$val)
                                        {
                                            $NamaDivisi = $arr_data["NamaDivisi"][$KdDivisi];
                                            
                                            if($KdDivisi*1==0)
                                            {
                                                $KdDivisi = "GA";
                                            }
                                            
                                            $selected = "";
                                            if($KdDivisi==$v_KdDivisi)
                                            {
                                                $selected = "selected='selected'";    
                                            }
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $KdDivisi; ?>"><?php echo $NamaDivisi; ?></option>
                                        <?php        
                                        }
                                    ?>
                                </select>     
                        </td>
                    </tr>
                    
                     <tr id="tr_kategori" style="display: <?php echo $display_kategori; ?>">
                        <td class="title_table">Kategori</td>
                        <td>:                            
                                  <select class="form-control-new" name="v_KdKategori" id="v_KdKategori" style="width: 200px;">
                                    <option value="">Semua</option>
                                    
                                    <?php 
                                        foreach($arr_data["list_kategori"] as $KdKategori=>$val)
                                        {
                                            $NamaKategori = $arr_data["NamaKategori"][$KdKategori];
                                            
                                            $selected = "";
                                            if($KdKategori==$v_KdKategori)
                                            {
                                                $selected = "selected='selected'";    
                                            }
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $KdKategori; ?>"><?php echo $NamaKategori; ?></option>
                                        <?php        
                                        }
                                    ?>
                                </select>     
                        </td>
                    </tr>
                    
                    <tr id="tr_market_list" style="display: <?php echo $display_market_list; ?>">
                        <td class="title_table">Market List</td>
                        <td>:                            
                                  <select class="form-control-new" name="v_market_list_id" id="v_market_list_id" style="width: 200px;">
                                    <option value="">Semua</option>
                                    
                                    <?php 
                                        foreach($arr_data["list_market_list"] as $market_list_id=>$val)
                                        {
                                            $market_list_name = $arr_data["market_list_name"][$market_list_id];
                                            
                                            $selected = "";
                                            if($market_list_id==$v_market_list_id)
                                            {
                                                $selected = "selected='selected'";    
                                            }
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $market_list_id; ?>"><?php echo $market_list_name; ?></option>
                                        <?php        
                                        }
                                    ?>
                                </select>     
                        </td>
                    </tr>
                    
                      <tr>
                        <td class="title_table">Keyword</td>
                        <td>: 
                            <input type="text" class="form-control-new" name="v_keyword" id="v_keyword" style="width: 200px;" value="<?php echo $v_keyword; ?>">
                        </td>
                     </tr>
                    
                    
                     <tr>
                        <td class="title_table">Urut Berdasarkan</td>
                        <td>: 
                            <select class="form-control-new" name="v_sort_by" id="v_sort_by" style="width: 200px;">
                                <option <?php if($v_sort_by=="PCode") echo "selected='selected'"; ?> value="PCode">PCode</option>
                                <option <?php if($v_sort_by=="NamaLengkap") echo "selected='selected'"; ?> value="NamaLengkap">Nama Barang</option>
                            </select>
                        </td>
                     </tr>
                      
                      
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>

				
			</table> 
			<br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    $where_divisi = "";
                    if($v_KdDivisi!="")
                    {
                        if($v_KdDivisi=="GA")
                        {
                            $v_KdDivisi = "0";
                        }
                        
                        $where_divisi = " AND `masterbarang`.KdDivisi = '".$v_KdDivisi."' ";    
                    }
                    
                    $where_kategori = "";
                    if($v_KdKategori!="")
                    {
                        $where_kategori = " AND `masterbarang`.KdKategori = '".$v_KdKategori."' ";    
                    }
                    
                    $where_market_list = "";
                    if($v_market_list_id!="")
                    {
                        $where_market_list = " AND `market_list_detail`.market_list_id = '".$v_market_list_id."' ";    
                    }
                    
                    $arr_keyword[0] = "masterbarang.PCode";    
                    $arr_keyword[1] = "masterbarang.NamLengkap";    
                    $arr_keyword[2] = "masterbarang.Barcode1";      
                    $arr_keyword[3] = "masterbarang.NamaStruk";      
                    
                    $where_keyword = "";
                    if($v_keyword)
                    {
                        $where_search_keyword = search_keyword($v_keyword, $arr_keyword);
                        $where_keyword = $where_search_keyword;
                    }
                    
                    $counter = 0;
                    $where = "";
                    if($v_type_barang=="Reguler")
                    {
                        $where = $where_divisi.$where_kategori.$where_keyword;
                        $q = "
                                SELECT
                                    masterbarang.PCode,
                                    masterbarang.NamaLengkap,
                                    masterbarang.Barcode1,
                                    masterbarang.KdDivisi,
                                    masterbarang.KdKategori,
                                    masterbarang.Satuan1,
                                    masterbarang.Harga1c
                                FROM
                                    masterbarang
                                WHERE
                                    1
                                    ".$where."
                                ORDER BY
                                    ".$v_sort_by." ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $PCode,
                                $NamaLengkap,
                                $Barcode1,
                                $KdDivisi,
                                $KdKategori,
                                $Satuan1,
                                $Harga1c 
                            ) = $row;
                            
                            $arr_data["list_data"][$counter] = $counter;
                            $arr_data["data_PCode"][$counter] = $PCode;
                            $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                            $arr_data["data_Barcode1"][$counter] = $Barcode1;
                            $arr_data["data_KdDivisi"][$counter] = $KdDivisi;
                            $arr_data["data_KdKategori"][$counter] = $KdKategori;
                            $arr_data["data_Satuan1"][$counter] = $Satuan1;
                            $arr_data["data_Harga1c"][$counter] = $Harga1c;
                            
                            $arr_data["list_pcode"][$PCode] = $PCode;
                            $arr_data["list_divisi"][$KdDivisi] = $KdDivisi;
                            $arr_data["list_kategori"][$KdKategori] = $KdKategori;
                            
                            $counter++;
                        }
                    }
                    else if($v_type_barang=="Market List")
                    {
                        $where = $where_market_list.$where_keyword;
                        
                        $q = "
                                SELECT
                                    masterbarang.PCode,
                                    masterbarang.NamaLengkap,
                                    masterbarang.Barcode1,
                                    masterbarang.KdDivisi,
                                    masterbarang.KdKategori,
                                    masterbarang.Satuan1,
                                    masterbarang.Harga1c
                                FROM
                                    masterbarang
                                    LEFT JOIN market_list_detail ON
                                        masterbarang.PCode = market_list_detail.pcode
                                WHERE
                                    1
                                    ".$where."
                                ORDER BY
                                    ".$v_sort_by." ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $PCode,
                                $NamaLengkap,
                                $Barcode1,
                                $KdDivisi,
                                $KdKategori,
                                $Satuan1,
                                $Harga1c 
                            ) = $row;
                            
                            $arr_data["list_data"][$counter] = $counter;
                            $arr_data["data_PCode"][$counter] = $PCode;
                            $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                            $arr_data["data_Barcode1"][$counter] = $Barcode1;
                            $arr_data["data_KdDivisi"][$counter] = $KdDivisi;
                            $arr_data["data_KdKategori"][$counter] = $KdKategori;
                            $arr_data["data_Satuan1"][$counter] = $Satuan1;
                            $arr_data["data_Harga1c"][$counter] = $Harga1c;
                            
                            $arr_data["list_pcode"][$PCode] = $PCode;
                            $arr_data["list_divisi"][$KdDivisi] = $KdDivisi;
                            $arr_data["list_kategori"][$KdKategori] = $KdKategori;
                            
                            $counter++;
                        }
                    }
                    
                    if(count($arr_data["list_pcode"])*1>0)
                    {
                        $where_pcode = where_array($arr_data["list_pcode"], "pcode", "in");
                        
                        $q = "
                                SELECT
                                    market_list_detail.pcode,
                                    market_list.market_list_id,    
                                    market_list.market_list_name
                                FROM
                                    market_list
                                    INNER JOIN market_list_detail ON
                                        market_list.market_list_id = market_list_detail.market_list_id 
                                WHERE
                                    1
                                    ".$where_pcode."
                                ORDER BY
                                    market_list_detail.pcode ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($pcode, $market_list_id, $market_list_name) = $row;
                            
                            $arr_data["market_list_name"][$pcode] = $market_list_name;
                        }
                    }
                    
                    if(count($arr_data["list_divisi"])*1>0)
                    {
                        $where_divisi = where_array($arr_data["list_divisi"], "KdDivisi", "in");
                        
                        $q = "
                                SELECT
                                    divisi.KdDivisi,    
                                    divisi.NamaDivisi
                                FROM
                                    divisi
                                WHERE
                                    1
                                    ".$where_divisi."
                                ORDER BY
                                    divisi.KdDivisi ASC
                        ";
                        $qry = mysql_query($qry);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($KdDivisi, $NamaDivisi) = $row;
                            
                            $arr_data["NamaDivisi"][$KdDivisi] = $NamaDivisi;
                        }
                    }
                    
                    if(count($arr_data["list_kategori"])*1>0)
                    {
                        $where_kategori = where_array($arr_data["list_kategori"], "KdKategori", "in");
                        
                        $q = "
                                SELECT
                                    kategori.KdKategori,    
                                    kategori.NamaKategori
                                FROM
                                    kategori
                                WHERE
                                    1
                                    ".$where_kategori."
                                ORDER BY
                                    kategori.KdKategori ASC
                        ";
                        $qry = mysql_query($qry);
                        while($row = mysql_fetch_array($qry))
                        {
                            list($KdKategori, $NamaKategori) = $row;
                            
                            $arr_data["NamaKategori"][$KdKategori] = $NamaKategori;
                        }
                    }
                    
                    
                    
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="9">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="9">REPORT MASTER BARANG</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="9">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    
                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                            <tr class="title_table" style="font-weight: bold;">
                                <td style="width: 30px;">No</td>
                                <td>PCode</td>
                                <td>Nama Barang</td>
                                <td>Barcode</td>
                                <td>Market List</td>
                                <td>Divisi</td>
                                <td>Kategori</td>
                                <td>Satuan</td>
                                <td>Harga Jual</td>
                            </tr>
                        
                        <tbody style="color: black;">
                            <?php 
                                $no = 1;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $PCode = $arr_data["data_PCode"][$counter];
                                    $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                                    $Barcode1 = $arr_data["data_Barcode"][$counter];
                                    $KdDivisi = $arr_data["data_KdDivisi"][$counter];
                                    $KdKategori = $arr_data["data_KdKategori"][$counter];
                                    $Satuan1 = $arr_data["data_Satuan1"][$counter];
                                    $Harga1c = $arr_data["data_Harga1c"][$counter];
                                    
                                    $market_list_name= $arr_data["market_list_name"][$PCode];
                                    $NamaDivisi = $arr_data["NamaDivisi"][$KdDivisi];
                                    $NamaKategori = $arr_data["NamaKategori"][$KdKategori];
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no; ?></td>
                                        <td style="mso-number-format: '\@'"><?php echo $PCode; ?></td>
                                        <td><?php echo $NamaLengkap; ?></td>
                                        <td><?php echo $Barcode1; ?></td>
                                        <td><?php echo $market_list_name; ?></td>
                                        <td><?php echo $NamaDivisi; ?></td>
                                        <td><?php echo $NamaKategori; ?></td>
                                        <td><?php echo $Satuan1; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($Harga1c, 2, ",", "."); ?></td>
                                    </tr> 
                                    <?php
                                    
                                    $no++;
                                }
                            ?>  
                        </tbody>
                        
                        <tfoot>
                           
                        </tfoot>
                    </table>
                    <?php
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
			
		
		</div>
		
		</form>
		
<?php 
                    include("footer.php"); 
                }
?>