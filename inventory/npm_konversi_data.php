<?php
include("header.php");
                               
$modul            = "KONVERSI";
$file_current     = "npm_konversi.php";
$file_name        = "npm_konversi_data.php";

//$ses_login = "hendri1003";


function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}
          
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
        $search_keyword        = trim($_GET["search_keyword"]);
        $search_KdDivisi       = trim($_GET["search_KdDivisi"]);
        $search_KdKategori     = trim($_GET["search_KdKategori"]);
        $search_order_by       = trim($_GET["search_order_by"]);
        $v_PCode_curr          = trim($_GET["v_PCode_curr"]);
      
        $where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "masterbarang.PCode";    
            $arr_keyword[1] = "masterbarang.NamaLengkap";    
            $arr_keyword[2] = "kategori.KdKategori";      
            $arr_keyword[3] = "kategori.NamaKategori";      
            $arr_keyword[4] = "divisi.KdDivisi";      
            $arr_keyword[5] = "divisi.NamaDivisi";      
            $arr_keyword[6] = "masterbarang.Barcode1";    
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        if($search_KdDivisi!="")
        {
            $where .= " AND masterbarang.KdDivisi = '".$search_KdDivisi."'";
        }
        
        if($search_KdKategori)
        {
            $where .= " AND masterbarang.KdKategori = '".$search_KdKategori."'";
        }
        
        if($search_order_by=="Nama Barang")
        {                       
            $where_order_by = " ".$db["master"].".masterbarang.NamaLengkap ASC "; 
        }
        else if($search_order_by=="PCode")
        {                       
            $where_order_by = " ".$db["master"].".masterbarang.PCode ASC "; 
        }
        else if($search_order_by=="Add Date")
        {                       
            $where_order_by = " ".$db["master"].".masterbarang.AddDate ASC "; 
        }
        
        if($v_PCode_curr)
        {
            $where = " AND ".$db["master"].".masterbarang.PCode = '".$v_PCode_curr."'";
        }
        
        
        // select query all
        {
            $counter = 1;
            $q="
                SELECT 
                  ".$db["master"].".masterbarang.PCode,
                  ".$db["master"].".masterbarang.NamaLengkap,
                  ".$db["master"].".masterbarang.Barcode1,
                  ".$db["master"].".masterbarang.SatuanSt,
                  ".$db["master"].".divisi.KdDivisi,
                  ".$db["master"].".divisi.NamaDivisi,
                  ".$db["master"].".kategori.KdKategori,
                  ".$db["master"].".kategori.NamaKategori,
                  ".$db["master"].".masterbarang.Approval_By,
                  ".$db["master"].".masterbarang.Approval_Date,
                  ".$db["master"].".masterbarang.Approval_Status,
                  ".$db["master"].".masterbarang.Approval_Remarks,
                  ".$db["master"].".masterbarang.AddDate,
                  ".$db["master"].".masterbarang.EditDate
                FROM
                  ".$db["master"].".masterbarang 
                  LEFT JOIN ".$db["master"].".divisi 
                    ON ".$db["master"].".masterbarang.KdDivisi = ".$db["master"].".divisi.KdDivisi 
                  LEFT JOIN ".$db["master"].".kategori 
                    ON ".$db["master"].".masterbarang.KdKategori = ".$db["master"].".kategori.KdKategori
                WHERE 1 
                    ".$where."
                ORDER BY 
                    ".$where_order_by."
            ";
            $sql=mysql_query($q);
            while($row=mysql_fetch_array($sql))
            { 
                list(
                    $PCode,
                    $NamaLengkap,
                    $Barcode1,
                    $SatuanSt,
                    $KdDivisi,
                    $NamaDivisi,
                    $KdKategori,
                    $NamaKategori,
                    $Approval_By,
                    $Approval_Date,
                    $Approval_Status,
                    $Approval_Remarks,
                    $AddDate,
                    $EditDate
                )=$row;
                
                if($AddDate=="0000-00-00 00:00:00")
                {
                    $AddDate = "";
                }
                
                $arr_data["list_data"][$counter]=$counter;
                $arr_data["data_PCode"][$counter]=$PCode;
                $arr_data["data_NamaLengkap"][$counter]=$NamaLengkap;
                $arr_data["data_Barcode1"][$counter]=$Barcode1;
                $arr_data["data_SatuanSt"][$counter]=$SatuanSt;
                $arr_data["data_KdDivisi"][$counter]=$KdDivisi;
                $arr_data["data_NamaDivisi"][$counter]=$NamaDivisi;
                $arr_data["data_KdKategori"][$counter]=$KdKategori;
                $arr_data["data_NamaKategori"][$counter]=$NamaKategori;
                $arr_data["data_Approval_By"][$counter]=$Approval_By;
                $arr_data["data_Approval_Date"][$counter]=$Approval_Date;
                $arr_data["data_Approval_Status"][$counter]=$Approval_Status;
                $arr_data["data_Approval_Remarks"][$counter]=$Approval_Remarks;
                $arr_data["data_AddDate"][$counter]=$AddDate;
                $arr_data["data_EditDate"][$counter]=$EditDate;
                
                $counter++;
            }
        }  
        
        
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_purchase_order_print.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
            <tr class="title_table">
                <td width="30"><strong><center>No</center></strong></td>
                <td><strong><center>PCode</center></strong></td>
                <td><strong><center>Barcode</center></strong></td>
                <td><strong><center>Nama Barang</center></strong></td>
                <td><strong><center>Satuan</center></strong></td>
                <td><strong><center>Divisi</center></strong></td>
                <td><strong><center>Kategori</center></strong></td>     
                <td><strong><center>Add Date</center></strong></td>      
                <td width="15" style="display: none;"><strong><center><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></center></strong></th>    
            </tr>
        <tbody style="color: black;">
        
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        foreach($arr_data["list_data"] as $counter => $val)
        {    
            $nomor++;
            
            $PCode = $arr_data["data_PCode"][$counter];
            $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
            $Barcode1 = $arr_data["data_Barcode1"][$counter];
            $SatuanSt = $arr_data["data_SatuanSt"][$counter];
            $KdDivisi = $arr_data["data_KdDivisi"][$counter];
            $NamaDivisi = $arr_data["data_NamaDivisi"][$counter];
            $KdKategori = $arr_data["data_KdKategori"][$counter];
            $NamaKategori = $arr_data["data_NamaKategori"][$counter];
            $Approval_By = $arr_data["data_Approval_By"][$counter];
            $Approval_Date = $arr_data["data_Approval_Date"][$counter];
            $Approval_Status = $arr_data["data_Approval_Status"][$counter];
            $Approval_Remarks = $arr_data["data_Approval_Remarks"][$counter];
            $AddDate = $arr_data["data_AddDate"][$counter];
            $EditDate = $arr_data["data_EditDate"][$counter];
            
            if($Approval_By=="")
            {
                $approval = "waiting";
            } 
            else
            {
                if($Approval_Status==1)
                {
                    $approval = "<font color='blue'>".$Approval_By." - ".format_show_datetime($Approval_Date)."</font>";
                }
                else if($Approval_Status==2)
                {
                    $approval = "<font color='red'>".$Approval_By." - ".format_show_datetime($Approval_Date)."<br>".$Approval_Remarks."</font>";
                }
            }
            
            
            $style_color = "";
            if($v_PCode_curr==$PCode)
            {
                $style_color = "background: #CAFDB5";
            }
                    
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $PCode; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $Barcode1; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $NamaLengkap; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $SatuanSt; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style=""><?php echo $NamaDivisi; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style=""><?php echo $NamaKategori; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style="text-align: center;"><?php echo format_show_datetime($AddDate); ?></td>  
                <td align="center" style="display: none;">
                    &nbsp;
                    <?php 
                        if($Approval_Status==1)
                        {
                    ?>
                    <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $PCode; ?>">
                    <?php 
                        }
                    ?>
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot style="display: none;">
            <tr>
                <td colspan="100%" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Print">Print</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
    </table> 
    </form>
    </div>  
       <?php  
    }

    else if($ajax=="edit_data")
    {                                      
        $v_PCode = $_GET["v_PCode"];  
        
        $q = "
                SELECT 
                  ".$db["master"].".masterbarang.PCode,
                  ".$db["master"].".masterbarang.NamaLengkap,
                  ".$db["master"].".masterbarang.SatuanSt,
                  ".$db["master"].".masterbarang.Barcode1,
                  ".$db["master"].".divisi.KdDivisi,
                  ".$db["master"].".divisi.NamaDivisi,
                  ".$db["master"].".subdivisi.KdSubDivisi,
                  ".$db["master"].".subdivisi.NamaSubDivisi,
                  ".$db["master"].".kategori.KdKategori,
                  ".$db["master"].".kategori.NamaKategori,
                  ".$db["master"].".subkategori.NamaSubKategori,
                  ".$db["master"].".masterbarang.Approval_By,
                  ".$db["master"].".masterbarang.Approval_Date,
                  ".$db["master"].".masterbarang.Approval_Status,
                  ".$db["master"].".masterbarang.Approval_Remarks,
                  ".$db["master"].".masterbarang.AddDate,
                  ".$db["master"].".masterbarang.EditDate,
                  ".$db["master"].".masterbarang.AddUser,
                  ".$db["master"].".masterbarang.EditUser,
                  RIGHT(".$db["master"].".masterbarang.KdKategori,2) AS KdKategori_Barang,
                  ".$db["master"].".masterbarang.KdSubKategori AS KdSubKategori_Barang,
                  ".$db["master"].".masterbarang.Harga1c,
                  ".$db["master"].".masterbarang.komisi,
                  ".$db["master"].".masterbarang.DiscInternal,
                  ".$db["master"].".masterbarang.Service_charge,
                  ".$db["master"].".masterbarang.PPN
                FROM
                  ".$db["master"].".masterbarang 
                  LEFT JOIN ".$db["master"].".divisi 
                    ON ".$db["master"].".masterbarang.KdDivisi = ".$db["master"].".divisi.KdDivisi 
                  LEFT JOIN ".$db["master"].".subdivisi 
                    ON ".$db["master"].".masterbarang.KdSubDivisi = ".$db["master"].".subdivisi.KdSubDivisi 
                  LEFT JOIN ".$db["master"].".kategori 
                    ON ".$db["master"].".masterbarang.KdKategori = ".$db["master"].".kategori.KdKategori
                  LEFT JOIN ".$db["master"].".subkategori
                    ON ".$db["master"].".masterbarang.KdSubKategori = ".$db["master"].".subkategori.KdSubKategori
                WHERE 1
                    AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'
                LIMIT
                    0,1
        ";     
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
        
        // black eye
        if($arr_curr["KdSubDivisi"]=="13" || $arr_curr["KdSubDivisi"]=="19")
        {     
            $q = "
                    SELECT
                        ".$db["master"].".subkategoripos.NamaSubKategori
                    FROM
                        ".$db["master"].".subkategoripos
                    WHERE
                        1
                        AND ".$db["master"].".subkategoripos.KdSubKategori = '".$arr_curr["KdSubKategori_Barang"]."'
                    LIMIT
                        0,1
            ";
            $qry = mysql_query($q);
            $row = mysql_fetch_array($qry);
            list($NamaSubKategori) = $row;
            
            $arr_curr["NamaKategori"] = "Black Eye";
            $arr_curr["NamaSubKategori"] = $NamaSubKategori;
            //echo "masuk";    
        }
        
        $q = "
                SELECT
                    ".$db["master"].".satuan.KdSatuan,    
                    ".$db["master"].".satuan.NamaSatuan
                FROM
                    ".$db["master"].".satuan
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".satuan.KdSatuan ASC
        ";
        $qry_pr = mysql_query($q);
        while($row_pr = mysql_fetch_array($qry_pr))
        { 
            list($KdSatuan, $NamaSatuan) = $row_pr;    
            
            $arr_data["list_satuan"][$KdSatuan] = $KdSatuan;
        }
        
        $q = "
                SELECT
                    konversi.sid,
                    konversi.Satuan_From,
                    konversi.Satuan_To,
                    konversi.amount,
                    konversi.AddDate,
                    konversi.AddUser
                FROM
                    konversi
                WHERE
                    konversi.PCode = '".$arr_curr["PCode"]."'
                ORDER BY
                    konversi.amount ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        { 
            list(
                $sid,
                $Satuan_From,
                $Satuan_To,
                $amount,
                $AddDate,
                $AddUser 
            ) = $row;    
            
            $arr_data["list_data"][$sid] = $sid;
            
            $arr_data["Satuan_From"][$sid] = $Satuan_From;
            $arr_data["Satuan_To"][$sid] = $Satuan_To;
            $arr_data["amount"][$sid] = $amount;
            $arr_data["AddDate"][$sid] = $AddDate;
            $arr_data["AddUser"][$sid] = $AddUser;
        }
        
            
        
        ?> 
        <div class="col-md-12" align="left">
            
            <ol class="breadcrumb title_table">
                <li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
                                                       
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value="">    
        <input type="hidden" name="v_PCode" id="v_PCode" value="<?php echo $arr_curr["PCode"]; ?>">   
        <input type="hidden" name="v_SatuanSt_old" id="v_SatuanSt_old" value="<?php echo $arr_curr["SatuanSt"]; ?>">   
        
        <table class="table table-bordered responsive" style="color: black;">
            <tr>
                <td class="title_table" width="200">PCode</td>
                <td style="color: black; font-weight: bold"><?php echo $arr_curr["PCode"]; ?></td>
            </tr>
        
             <tr>
                <td class="title_table" width="200">Nama Barang</td>
                <td><?php echo $arr_curr["NamaLengkap"]; ?></td>
            </tr>
            
             <tr>
                <td class="title_table" width="200">Barcode</td>
                <td><?php echo $arr_curr["Barcode1"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table" width="200">Divisi</td>
                <td><?php echo $arr_curr["NamaDivisi"]." :: ".$arr_curr["NamaSubDivisi"]; ?></td> 
            </tr>  

            <tr>
                <td class="title_table">Kategori</td>
                <td><?php echo $arr_curr["NamaKategori"]." :: ".$arr_curr["NamaSubKategori"]; ?></td> 
            </tr>
                
            <tr>
                <td class="title_table">Satuan</td>      
                <td>
                    <select class="form-control-new" style="width: 100px;" name="v_SatuanSt" id="v_SatuanSt" disabled="true">
                    <?php 
                        foreach($arr_data["list_satuan"] as $KdSatuan=> $val)
                        {
                            $selected = "";
                            if($arr_curr["SatuanSt"]==$KdSatuan)
                            {
                                $selected = "selected='selected'";
                            }
                            ?>
                            <option <?php echo $selected; ?> value="<?php echo $KdSatuan; ?>"><?php echo $KdSatuan; ?></option>
                            <?php   
                        }
                    ?>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>&nbsp;</td>
                <td><button type="submit" name="btn_save_satuan" id="btn_save_satuan" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button></td>
            </tr>
         
               
            <tr> 
                <td colspan="100%">&nbsp;</td>                   
            </tr>
            
            <tr>
                <td colspan="100%">
                    <table class="table table-bordered responsive" style="color: black;">
                        <tr class="title_table">
                            <td width="30">No</td>
                            <td width="100">Dari</td>
                            <td width="100">Menjadi</td>
                            <td width="100">Konversi</td>
                            <td>Dibuat</td>
                            <td width="100">Action</td>
                        </tr>
                        
                        
                        <tr>
                            <td>ADD</td>
                            <td>
                                <select class="form-control-new" style="width: 100px;" name="v_Satuan_From" id="v_Satuan_From">
                                    <option value="">-</option>
                                    <?php 
                                        foreach($arr_data["list_satuan"] as $KdSatuan=>$val)
                                        {
                                    ?>   
                                    <option value="<?php echo $KdSatuan; ?>"><?php echo $KdSatuan; ?></option>
                                    <?php 
                                        }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <?php echo $arr_curr["SatuanSt"]; ?>
                                <input type="hidden" name="v_Satuan_To" id="v_Satuan_To" value="<?php echo $arr_curr["SatuanSt"]; ?>">
                            </td>
                            <td><input type="text" class="form-control-new" name="v_amount" id="v_amount" onblur="toFormat4('v_amount')" value="" style="text-align: right;"></td>
                            <td>&nbsp;</td>
                            <td>
                                <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                            </td>
                        </tr>
                        
                         <tr> 
                            <td colspan="100%">&nbsp;</td>                   
                        </tr>
                        
                        <?php
                            $no = 1; 
                            foreach($arr_data["list_data"] as $sid=>$val)
                            {
                                $Satuan_From = $arr_data["Satuan_From"][$sid];
                                $Satuan_To = $arr_data["Satuan_To"][$sid];
                                $amount = $arr_data["amount"][$sid];
                                $AddDate = $arr_data["AddDate"][$sid];
                                $AddUser = $arr_data["AddUser"][$sid];
                        ?>
                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                            <td><?php echo $no; ?></td>
                            <td><?php echo $Satuan_From; ?></td>
                            <td><?php echo $Satuan_To; ?></td>
                            <td style="text-align: right"><?php echo format_number($amount, 4); ?></td>
                            <td><?php echo format_show_datetime($AddDate)." :: ".$AddUser; ?></td>
                            <!--<td onclick="confirm_delete('<?php echo $sid; ?>')" style="cursor: pointer;">
                                <img src="images/cross.gif"> Hapus
                            </td>-->
                        </tr>
                        <?php 
                                $no++;
                            }
                        ?>
                        
                    </table>
                </td>
            </tr>
            
            
            <tr>
                <td class="title_table" colspan="2">INFORMASI DATA MASTER BARANG</td>      
            </tr> 
            
            <tr>
                <td class="title_table">Dibuat</td>      
                <td><?php echo format_show_datetime($arr_curr["AddDate"])." :: ".$arr_curr["AddUser"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Diedit</td>      
                <td><?php echo format_show_datetime($arr_curr["EditDate"])." :: ".$arr_curr["EditUser"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Approval</td>      
                <td><?php echo $approval; ?></td>
            </tr>
            
            
            
        </table>  
        </form>
        </div>
        <?php
    } 
    else if($ajax=="delete_ajax")
    {
        $v_sid = $_GET["v_sid"];      
        
        $q = "
                SELECT
                    konversi.sid,
                    konversi.PCode
                FROM
                    konversi
                WHERE
                    1
                    AND konversi.sid = '".$v_sid."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($sid, $PCode) = $row;
        
        if($sid)
        {
            $q = "DELETE FROM konversi WHERE sid = '".$sid."'";
            if(!mysql_query($q))
            {
                $msg = "Gagal Hapus SID ".$sid."...";
                echo "<script>alert('".$msg."');</script>";
                die();    
            }
            else
            {
                echo $PCode;    
            }
        }
        else
        {
            $msg = "SID ".$v_sid." tidak ditemukan";
            echo "<script>alert('".$msg."');</script>";
            die();   
        }
    }
   
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){                                                                    
        case "edit_data" :                                                                                                           
            {                                                                                                      
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["btn_save_satuan"])){ $btn_save_satuan = isset($_POST["btn_save_satuan"]); } else { $btn_save_satuan = $_POST["btn_save_satuan"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }    
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }   
                if(!isset($_POST["v_PCode"])){ $v_PCode = isset($_POST["v_PCode"]); } else { $v_PCode = $_POST["v_PCode"]; }  
                
                if(!isset($_POST["v_SatuanSt"])){ $v_SatuanSt  = isset($_POST["v_SatuanSt"]); } else { $v_SatuanSt = $_POST["v_SatuanSt"]; }     
                if(!isset($_POST["v_SatuanSt_old"])){ $v_SatuanSt_old  = isset($_POST["v_SatuanSt_old"]); } else { $v_SatuanSt_old = $_POST["v_SatuanSt_old"]; }     
                if(!isset($_POST["v_Satuan_From"])){ $v_Satuan_From = isset($_POST["v_Satuan_From"]); } else { $v_Satuan_From = $_POST["v_Satuan_From"]; }     
                if(!isset($_POST["v_Satuan_To"])){ $v_Satuan_To = isset($_POST["v_Satuan_To"]); } else { $v_Satuan_To = $_POST["v_Satuan_To"]; }     
                if(!isset($_POST["v_amount"])){ $v_amount = isset($_POST["v_amount"]); } else { $v_amount = $_POST["v_amount"]; }   

                $v_PCode         = save_char($v_PCode);
                $v_Satuan_From   = save_char($v_Satuan_From); 
                $v_Satuan_To     = save_char($v_Satuan_To); 
                $v_amount        = save_int($v_amount); 
                    
                if($btn_save_satuan)
                {
                    if($v_SatuanSt_old!=$v_SatuanSt)
                    {
                        $q = "
                                UPDATE 
                                    masterbarang
                                SET
                                    SatuanSt = '".$v_SatuanSt."'
                                WHERE
                                    PCode = '".$v_PCode."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update Satuan masterbarang ...";
                            echo "<script>alert('".$msg."');</script>";
                            die();     
                        }
                        
                        $q = "
                                UPDATE 
                                    masterbarang_pos
                                SET
                                    Satuan1 = '".$v_SatuanSt."'
                                WHERE
                                    PCode = '".$v_PCode."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update Satuan masterbarang_pos ...";
                            echo "<script>alert('".$msg."');</script>";
                            die();     
                        }
                        
                        $q = "
                                UPDATE 
                                    masterbarang_touch
                                SET
                                    Satuan1 = '".$v_SatuanSt."'
                                WHERE
                                    PCode = '".$v_PCode."'
                        ";
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal Update Satuan masterbarang_touch ...";
                            echo "<script>alert('".$msg."');</script>";
                            die();     
                        }
                        
                        $msg = "Berhasil menyimpan ".$v_PCode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search','".$v_PCode."');</script>";         
                        die();
                    }
                }
                
                if($btn_save)
                {
                    if($v_Satuan_From=="")
                    {
                        $msg = "Dari harus dipilih...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else if($v_Satuan_To=="")
                    {
                        $msg = "Menjadi harus dipilih...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else if($v_amount*1==0)
                    {
                        $msg = "Konversi harus diisi...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }     
                
                
                    // validasi duplicate
                    $q = "
                            SELECT
                                konversi.sid
                            FROM
                                konversi
                            WHERE
                                1
                                AND konversi.Satuan_From = '".$v_Satuan_From."'
                                AND konversi.Satuan_To = '".$v_Satuan_To."'
                                AND konversi.PCode = '".$v_PCode."'
                            LIMIT
                                0,1
                    ";
                    $qry_cek = mysql_query($q);
                    $row_cek = mysql_fetch_array($qry_cek);
                    list($sid_duplicate) = $row_cek;
                    
                    if($sid_duplicate)
                    {
                        $msg = "Gagal menyimpan, Terjadi Duplikasi Data ...";
                        echo "<script>alert('".$msg."');</script>";
                        die();   
                    }
                    
                 
                    if($btn_save=="Save")
                    {   
                        // update masterbarang
                        {
                            $q = "
                                INSERT INTO
                                    ".$db["master"].".konversi
                                SET     
                                    PCode = '".$v_PCode."',
                                    Satuan_From = '".$v_Satuan_From."',
                                    Satuan_To = '".$v_Satuan_To."',
                                    amount = '".$v_amount."',
                                    AddDate = NOW(),
                                    AddUser = '".$ses_login."'
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                        }
                               
                        $msg = "Berhasil menyimpan ".$v_PCode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search','".$v_PCode."');</script>";         
                        die();
                    }
                } 
                
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  