<?php

include("function_general.php");
//include("function_email.php");

function get_code_counter($db_name, $tbl_name, $pk_name, $kode, $mm, $yyyy)
{
    global $db;
    
    $q = "
            SELECT
                SUBSTRING(".$db_name.".".$tbl_name.".".$pk_name.", 3, 5) AS last_counter
            FROM
                ".$db_name.".".$tbl_name."
            WHERE
                1
                AND RIGHT(".$db_name.".".$tbl_name.".".$pk_name.",5) = '".$mm."-".substr($yyyy,2,2)."'
            ORDER BY
                SUBSTRING(".$db_name.".".$tbl_name.".".$pk_name.", 3, 5)*1 DESC
            LIMIT
                0,1
    ";
    //echo $q."<hr>";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    list($last_counter) = $row;
    
    $last_counter = ($last_counter*1)+1;
    
    // PO00002-05-16
    $counter = $kode.sprintf("%05s", $last_counter)."-".$mm."-".substr($yyyy,2,2);
    
    return $counter;
}

function get_stock($KdGudang, $TglDokumen, $arr_pcode)
{
    global $db;
    
    $arr_date = explode("-", $TglDokumen);
    $bulan = $arr_date[1];
    $tahun = $arr_date[0];
    
    $bulan = sprintf("%02s", $bulan);
    $TanggalAwal = $tahun."-".$bulan."-01";
    
    $where_pcode = where_array($arr_pcode, "PCode", "in");
    $where_KdBarang = where_array($arr_pcode, "KodeBarang", "in");
    
    $q = "
            SELECT
                ".$db["master"].".stock.PCode,
                ".$db["master"].".stock.GAwal".$bulan." AS Stock_awal
            FROM
                ".$db["master"].".stock
            WHERE
                1
                ".$where_pcode."
                AND ".$db["master"].".stock.Tahun = '".$tahun."'
                AND ".$db["master"].".stock.KdGudang = '".$KdGudang."'
            ORDER BY
                 ".$db["master"].".stock.PCode
    ";
    $sql=mysql_query($q);
    while($row=mysql_fetch_array($sql))
    { 
        list(
            $PCode,
            $Stock_awal
        ) = $row;
        
        $arr_data["awal"][$PCode] = $Stock_awal; 
    }
    
    $q = "
            SELECT
                ".$db["master"].".mutasi.KodeBarang,
                ".$db["master"].".mutasi.Jenis,
                ".$db["master"].".mutasi.Qty
            FROM
                ".$db["master"].".mutasi
            WHERE
                1
                ".$where_KdBarang."
                AND ".$db["master"].".mutasi.Tanggal BETWEEN '".$TanggalAwal."' AND '".$TglDokumen."'
                AND ".$db["master"].".mutasi.Gudang = '".$KdGudang."'
            ORDER BY
                 ".$db["master"].".mutasi.KodeBarang
    ";
    $sql=mysql_query($q);
    while($row=mysql_fetch_array($sql))
    { 
        list(
            $PCode,
            $Jenis,
            $Qty
        ) = $row;
        
        if($Jenis=="I")
        {
            $arr_data["masuk"][$PCode] += $Qty;     
        }
        else if($Jenis=="O")
        {
            $arr_data["keluar"][$PCode] += $Qty;     
        }
    }
    
    foreach($arr_pcode as $key=>$val_pcode)
    {
        $arr_data["akhir"][$val_pcode] = ($arr_data["awal"][$val_pcode] + $arr_data["masuk"][$val_pcode]) - $arr_data["keluar"][$val_pcode];
    }
    
    return $arr_data;
}

function get_mutasi_stock($type_data, $v_NoDokumen)
{
    global $db;
    
    $q = "
            DELETE FROM
                ".$db["master"].".mutasi
            WHERE
                1
                AND ".$db["master"].".mutasi.KdTransaksi = '".$type_data."'
                AND ".$db["master"].".mutasi.NoTransaksi = '".$v_NoDokumen."'
    ";   
    if(!mysql_query($q))
    {
        $msg = "Gagal Reset Mutasi ".$v_NoDokumen;
        echo "<script>alert('".$msg."');</script>"; 
        die();
    }
    
    if($type_data=="RG")
    {
        $q = "
                SELECT
                    ".$db["master"].".trans_terima_header.NoDokumen,
                    ".$db["master"].".trans_terima_header.TglDokumen,
                    ".$db["master"].".trans_terima_header.KdGudang,
                    ".$db["master"].".trans_terima_detail.PCode,
                    ".$db["master"].".trans_terima_detail.QtyPcs
                    
                FROM
                    ".$db["master"].".trans_terima_header
                    INNER JOIN ".$db["master"].".trans_terima_detail ON
                        ".$db["master"].".trans_terima_header.NoDokumen = ".$db["master"].".trans_terima_detail.NoDokumen
                        AND ".$db["master"].".trans_terima_header.Status = '1'
                        AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".trans_terima_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang, $PCode, $QtyPcs) = $row;
            
            $q = "
                    INSERT INTO ".$db["master"].".`mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = 'I',
                      `KdTransaksi` = '".$type_data."',
                      `Gudang` = '',
                      `GudangTujuan` = '".$KdGudang."',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".$QtyPcs."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            if(!mysql_query($q))
            {
                $msg = "Gagal Insert Mutasi ".$v_NoDokumen;
                echo "<script>alert('".$msg."');</script>"; 
                die();
            } 
        }
    }
    else if($type_data=="SO")
    {
        $q = "
                SELECT
                    ".$db["master"].".opname_header.NoDokumen,
                    ".$db["master"].".opname_header.TglDokumen,
                    ".$db["master"].".opname_header.KdGudang,
                    ".$db["master"].".opname_detail.PCode,
                    ".$db["master"].".opname_detail.QtyFisik,
                    ".$db["master"].".opname_detail.QtyProgram
                FROM
                    ".$db["master"].".opname_header
                    INNER JOIN ".$db["master"].".opname_detail ON
                        ".$db["master"].".opname_header.NoDokumen = ".$db["master"].".opname_detail.NoDokumen
                        AND ".$db["master"].".opname_header.Approval_Status = '1'
                        AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".opname_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang, $PCode, $QtyFisik, $QtyProgram) = $row;
            
            $selisih = $QtyFisik - $QtyProgram;
            
            $GudangTujuan = "";
            $Gudang = "";
            
            if($selisih*1>0)
            {
                $Jenis = "I";
                $GudangTujuan = $KdGudang;
            }
            else
            {
                $Jenis = "O";
                $Gudang = $KdGudang;
            }
            
            $q = "
                    INSERT INTO ".$db["master"].".`mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = '".$Jenis."',
                      `KdTransaksi` = '".$type_data."',
                      `Gudang` = '".$Gudang."',
                      `GudangTujuan` = '".$GudangTujuan."',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".abs($selisih)."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            if(!mysql_query($q))
            {
                $msg = "Gagal Insert Mutasi ".$v_NoDokumen;
                echo "<script>alert('".$msg."');</script>"; 
                die();
            } 
        }    
    }
    else if($type_data=="MC")
    {
        $q = "
                SELECT
                    ".$db["master"].".trans_mutasi_header.NoDokumen,
                    ".$db["master"].".trans_mutasi_header.TglDokumen,
                    ".$db["master"].".trans_mutasi_header.KdGudang_To,
                    ".$db["master"].".trans_mutasi_detail.PCode,
                    ".$db["master"].".trans_mutasi_detail.Qty
                    
                FROM
                    ".$db["master"].".trans_mutasi_header
                    INNER JOIN ".$db["master"].".trans_mutasi_detail ON
                        ".$db["master"].".trans_mutasi_header.NoDokumen = ".$db["master"].".trans_mutasi_detail.NoDokumen
                        AND ".$db["master"].".trans_mutasi_header.Status = '1'
                        -- AND ".$db["master"].".trans_mutasi_header.MovingConfirmation = '1'
                        AND ".$db["master"].".trans_mutasi_header.NoDokumen = '".$v_NoDokumen."'
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".trans_mutasi_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang_To, $PCode, $Qty) = $row;
            
            $q = "
                    INSERT INTO ".$db["master"].".`mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = 'I',
                      `KdTransaksi` = '".$type_data."',
                      `Gudang` = '',
                      `GudangTujuan` = '".$KdGudang_To."',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".$Qty."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            if(!mysql_query($q))
            {
                $msg = "Gagal Insert Mutasi ".$v_NoDokumen;
                echo "<script>alert('".$msg."');</script>"; 
                die();
            } 
        }
    }
}

function get_opname_stock($tahun,$gudang,$pcode,$bulan,$nilai)
{
	global $db;
	
	$where = "";
	if($nilai*1>1)
	{
		$where .= "GNMasuk".$bulan." = '".$nilai."'";	
	}
	else
	{
		$where .= "GNKeluar".$bulan." = '".$nilai."'";	
	}
	
	 $upd = "
        UPDATE
            ".$db["master"].".stock
        SET     
           $where
         WHERE 
         	1
            AND ".$db["master"].".stock.Tahun = '".$tahun."'   
            AND ".$db["master"].".stock.KdGudang = '".$gudang."' 
            AND ".$db["master"].".stock.PCode = '".$pcode."'   
    ";                 
    if(!mysql_query($q))
    {
        $msg = "Gagal update Stock";
        echo "<script>alert('".$msg."');</script>";
        die(); 
    } 
}

function status_kawin($employee_id)
{
    $q = "
            SELECT
                employee_family.sid
            FROM
                employee_family
            WHERE
                1
                AND employee_family.relation_id = '3'
                AND employee_family.sts_cerai != 'cerai'
                AND employee_family.employee_id = '".$employee_id."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    list($kawin) = $row;
    
    if($kawin)
    {
        $status_kawin = "K";
    }
    else
    {
        $status_kawin = "TK";
    }
    
    $q = "
            SELECT
                COUNT(employee_family.sid) AS jml_anak
            FROM
                employee_family
            WHERE
                1
                AND employee_family.relation_id = '4'
                AND employee_family.employee_id = '".$employee_id."'
            LIMIT
                0,1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    list($jml_anak) = $row;
    
    $status_kawin .= ($jml_anak*1);
    
    return $status_kawin;
    
} 

/*function _date_diff($one, $two)
{
    $one = parsedate(format_show_date($one));
    $two = parsedate(format_show_date($two));
    
    $invert = false;
    if ($one > $two) {
        list($one, $two) = array($two, $one);
        $invert = true;
    }

    $key = array("y", "m", "d", "h", "i", "s");
    $a = array_combine($key, array_map("intval", explode(" ", date("Y m d H i s", $one))));
    $b = array_combine($key, array_map("intval", explode(" ", date("Y m d H i s", $two))));

    $result = array();
    $result["y"] = $b["y"] - $a["y"];
    $result["m"] = $b["m"] - $a["m"];
    $result["d"] = $b["d"] - $a["d"];
    $result["h"] = $b["h"] - $a["h"];
    $result["i"] = $b["i"] - $a["i"];
    $result["s"] = $b["s"] - $a["s"];
    $result["invert"] = $invert ? 1 : 0;
    $result["days"] = intval(abs(($one - $two)/86400));

    if ($invert) {
        _date_normalize(&$a, &$result);
    } else {
        _date_normalize(&$b, &$result);
    }

    $return["tahun"] = $result["y"];
    $return["bulan"] = $result["m"];
    $return["hari"] = $result["d"];
    
    return $return;
}*/

function _date_range_limit($start, $end, $adj, $a, $b, $result)
{
    if ($result[$a] < $start) {
        $result[$b] -= intval(($start - $result[$a] - 1) / $adj) + 1;
        $result[$a] += $adj * intval(($start - $result[$a] - 1) / $adj + 1);
    }

    if ($result[$a] >= $end) {
        $result[$b] += intval($result[$a] / $adj);
        $result[$a] -= $adj * intval($result[$a] / $adj);
    }

    return $result;
}

/*function _date_range_limit_days($base, $result)
{
    $days_in_month_leap = array(31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $days_in_month = array(31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    _date_range_limit(1, 13, 12, "m", "y", &$base);

    $year = $base["y"];
    $month = $base["m"];

    if (!$result["invert"]) {
        while ($result["d"] < 0) {
            $month--;
            if ($month < 1) {
                $month += 12;
                $year--;
            }

            $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
            $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];

            $result["d"] += $days;
            $result["m"]--;
        }
    } else {
        while ($result["d"] < 0) {
            $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
            $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];

            $result["d"] += $days;
            $result["m"]--;

            $month++;
            if ($month > 12) {
                $month -= 12;
                $year++;
            }
        }
    }

    return $result;
}*/

/*function _date_normalize($base, $result)
{
    $result = _date_range_limit(0, 60, 60, "s", "i", $result);
    $result = _date_range_limit(0, 60, 60, "i", "h", $result);
    $result = _date_range_limit(0, 24, 24, "h", "d", $result);
    $result = _date_range_limit(0, 12, 12, "m", "y", $result);

    $result = _date_range_limit_days(&$base, &$result);

    $result = _date_range_limit(0, 12, 12, "m", "y", $result);

    return $result;
}*/

function diff_time()
{
    $diff_time = 0;
    
    return $diff_time;
}

function date_now($format)
{
    $now = time()+diff_time();
    
    $date_now = date($format,$now);
    
    return $date_now;
}

function update_employee_join_resign($employee_id)
{
     global $db;
     
     $q = "
            SELECT 
                *
            FROM 
                ".$db["master"].".employee_position
            WHERE
                1
                AND ".$db["master"].".employee_position.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".employee_position.start_date ASC
     ";
     $qry_position = mysql_query($q);
     $temp_start_date = "";
     $temp_end_date   = "";
     while($row_position = mysql_fetch_array($qry_position))
     {
        $start_date = $row_position["start_date"];
        $end_date   = $row_position["end_date"];
         
        if($start_date!="0000-00-00")
        {    
            $temp_start_date .= "'".$start_date."',";
        }
        
        if($end_date!="0000-00-00")
        {    
            $temp_end_date .= "'".$end_date."',";
        }
     } 
     $temp_start_date  = substr($temp_start_date,0,-1);
     $temp_end_date    = substr($temp_end_date,0,-1);
     
     $del_start = "DELETE FROM ".$db["master"].".employee_join WHERE join_date NOT IN (".$temp_start_date.") AND employee_id = '".$employee_id."' ";
     
     mysql_query($del_start);
     
     $q = "
            SELECT 
                *
            FROM 
                ".$db["master"].".employee_position
            WHERE
                1
                AND ".$db["master"].".employee_position.employee_id = '".$employee_id."'
            ORDER BY
                ".$db["master"].".employee_position.start_date ASC
     ";
     $qry_position = mysql_query($q);
     while($row_position = mysql_fetch_array($qry_position))
     {
         $start_date = $row_position["start_date"];
         $end_date   = $row_position["end_date"];
         
         if($start_date!="0000-00-00")
         {
             $q = "
                    SELECT
                        *
                    FROM
                        ".$db["master"].".employee_join 
                    WHERE
                        1
                        AND ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                        AND ".$db["master"].".employee_join.join_date = '".$start_date."'
                    LIMIT
                        0,1
             ";
             $qry_cek_start = mysql_query($q);
             $jml_cek_start = mysql_num_rows($qry_cek_start);
             
             if($jml_cek_start*1==0)
             {
                 $counter_details = get_counter_int($db["master"],"employee_join","sid",100);
                 $ins = "
                            INSERT INTO 
                                ".$db["master"].".employee_join 
                            SET
                                sid = '".$counter_details."',
                                employee_id = '".$employee_id."',
                                join_date = '".$start_date."',
                                remarks = '',
                                author = '".ffclock()."',
                                edited = '".ffclock()."'
                 ";
                 mysql_query($ins);
             }
         }
     }
}

function get_employee_status_join($employee_id, $date)
{
    global $db;
    
    $q = "
            SELECT
                uni.v_status,
                uni.v_date
            FROM
            (
                SELECT
                    'join' as v_status,
                    ".$db["master"].".employee_join.join_date as v_date
                FROM
                    ".$db["master"].".employee_join
                WHERE
                    1
                    AND ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                
                UNION ALL
                
                SELECT
                    'resign' as v_status,
                    ".$db["master"].".employee_resign.resign_date as v_date
                FROM
                    ".$db["master"].".employee_resign
                WHERE
                    1
                    AND ".$db["master"].".employee_resign.employee_id = '".$employee_id."'
            ) as uni
            
            WHERE
                uni.v_date <= '".format_save_date($date)."'
            ORDER BY
                uni.v_date DESC
            LIMIT
                0,1
    ";
    
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    return $row;
} 

function select_atasan($v_employee_id)
{
   	global $db;
    
    $q = "
    	SELECT 
		  setup_atasan_details.*,
		  setup_atasan.employee_id AS employee_id_atasan 
		FROM
		  ".$db["master"].".setup_atasan_details 
		  INNER JOIN ".$db["master"].".setup_atasan 
		    ON ".$db["master"].".setup_atasan_details.setup_atasan_id = ".$db["master"].".setup_atasan.setup_atasan_id 
		WHERE 1 
		  AND ".$db["master"].".setup_atasan_details.employee_id = '".$v_employee_id."'
    ";                                 
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);  
    
    return select_employee($row["employee_id_atasan"],date('d/m/Y'));  
}

function select_employee($v_employee_id,$v_date="")
{
    global $db;
    
    // select resign
    {
		$q_resign="
			SELECT 
			  ".$db["master"].".employee_resign.employee_id,
			  MAX(
			    ".$db["master"].".employee_resign.resign_date
			  ) AS resign_date 
			FROM
			  ".$db["master"].".employee_resign 
			WHERE 1 
			  AND ".$db["master"].".employee_resign.employee_id = '".$v_employee_id."' 
			GROUP BY ".$db["master"].".employee_resign.employee_id 
			ORDER BY ".$db["master"].".employee_resign.employee_id ASC
		";
		$qry_resign = mysql_query($q_resign);
		$row_resign = mysql_fetch_array($qry_resign);	
		
		if($row_resign["employee_id"])
		{
			$where_resign_date = " AND ".$db["master"].".employee_position.start_date > '".$row_resign["resign_date"]."' ";    	
		}
	}
	
	// select join
	{
		$q_join = "
			SELECT 
			  ".$db["master"].".employee_position.employee_id,
			  MIN(
			    ".$db["master"].".employee_position.start_date
			  ) AS join_date 
			FROM
			  ".$db["master"].".employee_position 
			WHERE 1 
			  AND ".$db["master"].".employee_position.employee_id = '".$v_employee_id."' 
			  ".$where_resign_date."
			GROUP BY ".$db["master"].".employee_position.employee_id 
			ORDER BY ".$db["master"].".employee_position.employee_id ASC
		";
		$qry_join = mysql_query($q_join);
		$row_join = mysql_fetch_array($qry_join);	
	}     
	  
    // Selecet field perusahaan, cabang, depo, divisi, jabatan
    {
        $q2 = "
            SELECT
              uni.employee_id,
              uni.company_id,
              uni.company_name,
              uni.company_initial,
              uni.address,
              uni.cabang_id,
              uni.cabang_name,
              uni.depo_id,
              uni.depo_name,
              uni.divisi_id,
              uni.divisi_name,
              uni.departemen_id,
              uni.departemen_name,
              uni.jabatan_id,
              uni.jabatan_name,
              uni.level_id
            FROM
            (
                SELECT 
                  ".$db["master"].".employee_position.employee_id,
                  ".$db["master"].".company.company_id,
                  ".$db["master"].".company.company_name,
                  ".$db["master"].".company.company_initial,
                  ".$db["master"].".company.address,
                  ".$db["master"].".hrd_cabang.cabang_id,
                  ".$db["master"].".hrd_cabang.cabang_name,
                  ".$db["master"].".depo.depo_id,
                  ".$db["master"].".depo.depo_name,
                  ".$db["master"].".hrd_divisi.divisi_id,
                  ".$db["master"].".hrd_divisi.divisi_name,
                  ".$db["master"].".hrd_departemen.departemen_id,
                  ".$db["master"].".hrd_departemen.departemen_name,
                  ".$db["master"].".jabatan.jabatan_id,
                  ".$db["master"].".jabatan.jabatan_name,
                  ".$db["master"].".employee_position.level_id
                FROM
                  ".$db["master"].".employee_position 
                  INNER JOIN ".$db["master"].".company 
                    ON ".$db["master"].".employee_position.company_id = ".$db["master"].".company.company_id 
                  INNER JOIN ".$db["master"].".hrd_divisi 
                    ON ".$db["master"].".employee_position.divisi_id = ".$db["master"].".hrd_divisi.divisi_id 
                  INNER JOIN ".$db["master"].".hrd_departemen 
                    ON ".$db["master"].".employee_position.departemen_id = ".$db["master"].".hrd_departemen.departemen_id 
                  INNER JOIN ".$db["master"].".depo 
                    ON ".$db["master"].".employee_position.depo_id = ".$db["master"].".depo.depo_id 
                  INNER JOIN ".$db["master"].".hrd_cabang 
                    ON ".$db["master"].".depo.cabang_id = ".$db["master"].".hrd_cabang.cabang_id   
                  INNER JOIN ".$db["master"].".jabatan 
                    ON ".$db["master"].".employee_position.jabatan_id = ".$db["master"].".jabatan.jabatan_id 
                WHERE 
                  1
                ORDER BY
                  ".$db["master"].".employee_position.start_date DESC
            )AS uni
            WHERE
                1
                AND uni.employee_id='".$v_employee_id."'
            GROUP BY
                uni.employee_id 
        ";                                                     
        $qry2 = mysql_query($q2);
        $row2 = mysql_fetch_array($qry2);   
    }   
    
    // select lokasi kerja
    {
		$q4="
			SELECT 
			  setup_lokasi_kerja.absensi_cabang_id,
			  absensi_cabang.absensi_cabang_name 
			FROM
			  ".$db["master"].".setup_lokasi_kerja 
			  INNER JOIN ".$db["master"].".absensi_cabang 
			    ON ".$db["master"].".setup_lokasi_kerja.absensi_cabang_id = ".$db["master"].".absensi_cabang.absensi_cabang_id 
			WHERE 1 
			  AND ".$db["master"].".setup_lokasi_kerja.employee_id = '".$v_employee_id."' 
			LIMIT 0, 01
		";
        $qry4 = mysql_query($q4);
        $row4 = mysql_fetch_array($qry4);  
        
        if($row4["absensi_cabang_id"])
        {
			$echo_absensi_cabang_id = $row4["absensi_cabang_id"];
			$echo_absensi_cabang_name = $row4["absensi_cabang_name"];
		}
		else
		{
			$echo_absensi_cabang_id = 0;
			$echo_absensi_cabang_name = "Tidak ada di setup lokasi kerja";
		}
	}
	
    // employee
    {
        $q3 = "
            SELECT 
                ".$db["master"].".employee.employee_id,
                ".$db["master"].".employee.employee_nik,
                ".$db["master"].".employee.employee_code_hrd,
                ".$db["master"].".employee.employee_name,
                ".$db["master"].".employee.email,
                ".$db["master"].".employee.gender,
                ".$db["master"].".employee.work_hour_id,
                ".$db["master"].".employee.work_day_id,
                '".$row2["company_id"]."' AS company_id,
                '".$row2["company_initial"]."' AS company_initial,
                '".$row2["address"]."' AS address,
                '".$row2["company_name"]."' AS company_name,
                '".$row2["cabang_id"]."' AS cabang_id,
                '".$row2["cabang_name"]."' AS cabang_name,
                '".$row2["depo_id"]."' AS depo_id,
                '".$row2["depo_name"]."' AS depo_name,
                '".$row2["divisi_id"]."' AS divisi_id,
                '".$row2["divisi_name"]."' AS divisi_name,
                '".$row2["jabatan_id"]."' AS jabatan_id,
                '".$row2["jabatan_name"]."' AS jabatan_name,
                '".$row2["departemen_id"]."' AS departemen_id,
                '".$row2["departemen_name"]."' AS departemen_name,
                '".$row2["level_id"]."' AS level_id,
                ".$db["master"].".employee.address_1,
                ".$db["master"].".employee.no_ktp,
                '".$row_join["join_date"]."' AS join_date,
                '".$echo_absensi_cabang_id."' AS absensi_cabang_id,
                '".$echo_absensi_cabang_name."' AS absensi_cabang_name
            FROM                                            
                ".$db["master"].".employee
            WHERE
                1
                AND ".$db["master"].".employee.employee_id='".$v_employee_id."'
            ORDER BY                       
                ".$db["master"].".employee.employee_name ASC,
                ".$db["master"].".employee.employee_id ASC,
                ".$db["master"].".employee.employee_nik ASC
        ";         
        $qry3 = mysql_query($q3);
        $row3 = mysql_fetch_array($qry3);  
    }
    
    return $row3; 
}  

function UploadBukti($fupload_name,$dir_folder)
{
    //direktori gambar
    $vdir_upload = $dir_folder."/";
    $vfile_upload = $vdir_upload . $fupload_name;

    //Simpan gambar dalam ukuran sebenarnya
    move_uploaded_file($_FILES["fupload"]["tmp_name"], $vfile_upload);

    //identitas file asli
    $im_src = imagecreatefromjpeg($vfile_upload);
    $src_width = imageSX($im_src);
    $src_height = imageSY($im_src);
                           
    if($src_width==$src_height) 
    {
        $thumb_width = $src_width;
        $thumb_height = $src_height;
    } 
    elseif($src_width<$src_height) 
    {
        $thumb_width = $src_width;
        $thumb_height = $src_width;
    } 
    elseif($src_width>$src_height) 
    {
        $thumb_width = $src_height;
        $thumb_height = $src_height;
    } 
    else 
    {
        $thumb_width = 150;
        $thumb_height = 150;
    }

    $original_aspect = $src_width / $src_height;
    $thumb_aspect = $thumb_width / $thumb_height;

    if ( $original_aspect >= $thumb_aspect ) 
    {
        // If image is wider than thumbnail (in aspect ratio sense)
        $new_height = $thumb_height;
        $new_width = $src_width / ($src_height / $thumb_height);
    }
    else 
    {
        // If the thumbnail is wider than the image
        $new_width = $thumb_width;
        $new_height = $src_height / ($src_width / $thumb_width);
    }

    $im = imagecreatetruecolor( $thumb_width, $thumb_height );

    // Resize and crop
    imagecopyresampled($im,
    $im_src,
    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
    0, 0,
    $new_width, $new_height,
    $src_width, $src_height);
                            
    //Simpan gambar
    imagejpeg($im,$vdir_upload . "thumb_" . $fupload_name);
                              
    //Set ukuran gambar hasil perubahan
    $dst_width2 = 300;
    $dst_height2 = ($dst_width2/$src_width)*$src_height;

    //proses perubahan ukuran
    $im2 = imagecreatetruecolor($dst_width2,$dst_height2);
    imagecopyresampled($im2, $im_src, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

    //Simpan gambar
    imagejpeg($im2,$vdir_upload . "medium_" . $fupload_name);

    //Hapus gambar di memori komputer
    imagedestroy($im_src);
    imagedestroy($im);
    imagedestroy($im2);
}

function vci_join_date($arr_employee_id,$type="")
{
	global $db;

	unset($arr_return);

	$where_employee_resign = where_array($arr_employee_id, "employee_resign.employee_id", "in");
	
	$q = "
            SELECT
                ".$db["master"].".employee_resign.employee_id,
                MAX(".$db["master"].".employee_resign.resign_date) AS resign_date 
            FROM
                ".$db["master"].".employee_resign
            WHERE
                1
                ".$where_employee_resign."
            GROUP BY
                ".$db["master"].".employee_resign.employee_id
            ORDER BY
                ".$db["master"].".employee_resign.employee_id ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($employee_id, $resign_date) = $row;
        
        $arr_data["resign_date"][$employee_id] = $resign_date;
    }
    
    foreach($arr_employee_id as $key=>$val)
	{
		$resign_date = $arr_data["resign_date"][$val];
		
		if($type=="join")
		{
	        $where_resign_date = "";
	        if($resign_date)
	        {
	            $where_resign_date = " AND employee_position.start_date > '".$resign_date."' ";    
	        }	
		}

        $q = "
                SELECT
                    ".$db["master"].".employee_position.employee_id,
                    MIN(".$db["master"].".employee_position.start_date) AS join_date 
                FROM
                    ".$db["master"].".employee_position
                WHERE
                    1
                    AND ".$db["master"].".employee_position.employee_id = '".$val."'
                    ".$where_resign_date."
                GROUP BY
                    ".$db["master"].".employee_position.employee_id
                ORDER BY
                    ".$db["master"].".employee_position.employee_id ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($employee_id, $join_date) = $row;
            
            $arr_data["join_date"][$employee_id] = $join_date;
            $arr_return["join_date_emp"][$employee_id] = $join_date."::".$employee_id;
            $arr_return["join_date"][$employee_id] = $join_date;
        }
	}

	return $arr_return;
}

function masa_kerja($employee_id, $tanggal)
{
    global $db;
    global $ses_login;
    
    // kalo pernah resign, ambil posisi terakhir. kalo gak tetep posisi pertama
    
    $arr_data["list_employee"][$employee_id] = $employee_id;
    
    $arr_join_date = vci_join_date($arr_data["list_employee"],'join');
    
    $start  = $arr_join_date["join_date"][$employee_id];
    $end    = $tanggal;
    
    //if($ses_login=="hendri1003"){ echo $start." - ".$end; }
    
    $masa_kerja = _date_diff($start, $end);
    
    $return = $masa_kerja["tahun"]." Tahun ".$masa_kerja["bulan"]." Bulan ".$masa_kerja["hari"]." Hari";
    
    return $return;
}

function sisa_cuti($employee_id)
{
    global $db;
    
    // kalo pernah resign, ambil posisi terakhir. kalo gak tetep posisi pertama
    
    $q = "
        SELECT
            ".$db["master"].".employee_resign.resign_date
        FROM
            ".$db["master"].".employee_resign
        WHERE                                                   
            ".$db["master"].".employee_resign.employee_id = '".$employee_id."'
        ORDER BY
            ".$db["master"].".employee_resign.resign_date DESC
        LIMIT
            0,1
    ";
    $qry_cek = mysql_query($q);
    $row_cek = mysql_fetch_array($qry_cek);
    
    if($row_cek["resign_date"])
    {
        $q = "
                SELECT
                    ".$db["master"].".employee_join.join_date
                FROM
                    ".$db["master"].".employee_join
                WHERE
                    ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                ORDER BY
                    ".$db["master"].".employee_join.join_date DESC
                LIMIT
                    0,1
            ";
    }
    else
    {
        $q = "
                SELECT
                    ".$db["master"].".employee_join.join_date
                FROM
                    ".$db["master"].".employee_join
                WHERE
                    ".$db["master"].".employee_join.employee_id = '".$employee_id."'
                ORDER BY
                    ".$db["master"].".employee_join.join_date ASC
                LIMIT
                    0,1
            ";    
    }
 
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $start  = $row["join_date"];
    $end    = date("Y-m-d"); 
    $masa_kerja = date_diff_hen($start, $end);
    
    $tahun = date("Y");
    $tahun = "2013";
    
    $q = "
            SELECT
                ".$db["master"].".leave_saldo.amount
            FROM
                ".$db["master"].".leave_saldo
            WHERE
                1
                AND ".$db["master"].".leave_saldo.employee_id = '".$employee_id."'
                AND ".$db["master"].".leave_saldo.saldo_year = '".$tahun."'
            LIMIT
                0,1
    ";
    $qry_sa = mysql_query($q);
    $row_sa = mysql_fetch_array($qry_sa);
    
    $sa = 12;
    if($row_sa["amount"]*1<>0)
    {
        $sa = $row_sa["amount"];
    }
    
    $q = "
            SELECT
                COUNT(".$db["master"].".leave_together_details.leave_date) as jml
            FROM
                ".$db["master"].".leave_together 
                INNER JOIN ".$db["master"].".leave_together_details ON
                    ".$db["master"].".leave_together.leave_together_id = ".$db["master"].".leave_together_details.leave_together_id
                    -- AND ".$db["master"].".leave_together.leave_year = '".$tahun."'
            WHERE
                1
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    
    $cuti_bersama = $row["jml"];
    
    
    // cuti personal
    $q = "
            SELECT
                COUNT(".$db["master"].".leave_details.leave_date) as jml
            FROM
                ".$db["master"].".leave
                INNER JOIN ".$db["master"].".leave_details ON
                    ".$db["master"].".leave.leave_id = ".$db["master"].".leave_details.leave_id
                    AND ".$db["master"].".leave.approval_level_1 = '1'
                    AND ".$db["master"].".leave.status_potong = '1'
                    AND ".$db["master"].".leave.employee_id = '".$employee_id."' 
                    -- AND YEAR(".$db["master"].".leave_details.leave_date) = '".$tahun."'
            WHERE
                1
    ";
    $qry_personal = mysql_query($q);
    $row_personal = mysql_fetch_array($qry_personal);
    
    $cuti_personal = $row_personal["jml"]*1;
    
    $sisa = $sa - ($cuti_bersama + $cuti_personal);
    
    if($masa_kerja["tahun"] < 1)
    {
        $sisa_show = "Anda belum mendapatkan Jatah Cuti";
    }
    else
    {
        // tambah cuti 2014
        $sisa += 12;
        
        $sisa_show = $sisa." Hari";
    }
    
    return $sisa_show;
}

function generate_username($v_employee_name, $v_birthday)
{
    global $db;
    
    $exp_employee_name  = explode(" ", $v_employee_name);
    $jml_spasi          = count($exp_employee_name);
    
    $exp_birthday       = explode("/", $v_birthday);
    $hari               = $exp_birthday[0];
    $bulan              = $exp_birthday[1];
    $tahun              = $exp_birthday[2]; 
    
    $arr_data[1] = $exp_employee_name[0].$hari.$bulan;
    
    if($jml_spasi>=2)
    {
        $arr_data[2] = $exp_employee_name[1].$hari.$bulan;    
    }
    
    if($jml_spasi>=2)
    {
        $arr_data[3] = "";
        for($i=1;$i<=$jml_spasi;$i++)
        {
            $curr = $i-1;
            $arr_data[3] .= substr($exp_employee_name[$curr],0,1);    
        }
        $arr_data[3] .= $hari.$bulan;
    }
    
    $jml_arr_data = count($arr_data);
    
    // cek ke database
    $j = 1;
    for($i=1;$i<=$jml_arr_data;$i++)
    {
        $username = strtolower($arr_data[$i]);
        
        $q = "
            SELECT
                username
            FROM
                ".$db["master"].".employee
            WHERE
                ".$db["master"].".employee.username = '".$username."'
            LIMIT
                0,1
        ";
        $qry = mysql_query($q);
        $ada = mysql_num_rows($qry);
        
        if(!$ada)
        {
           $arr_username[$j] = $username; 
           $j++; 
        }
    }
    
    return $arr_username;
}
  
function generate_password($employee_id, $jml_char)
{
    $bahan = $employee_id.date("d-m-Y H:i:s");
    
    $return = substr(md5($bahan),0,$jml_char);
    
    return $return;    
}

?>