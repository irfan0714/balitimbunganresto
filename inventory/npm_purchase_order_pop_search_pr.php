<?php
include("header.php");

$modul   = "PURCHASE ORDER - Cari PR";

if(!isset($_GET["v_KdGudang"])){ $v_KdGudang = isset($_GET["v_KdGudang"]); } else { $v_KdGudang = $_GET["v_KdGudang"]; }                                                                                                                                        

if(!isset($_GET["v_keyword"])){ $v_keyword = isset($_GET["v_keyword"]); } else { $v_keyword = $_GET["v_keyword"]; }
if(!isset($_GET["p"])){ $p = isset($_GET["p"]); } else { $p = $_GET["p"]; }
if(!isset($_GET["order_by"])){ $order_by = isset($_GET["order_by"]); } else { $order_by = $_GET["order_by"]; }
if(!isset($_GET["order_type"])){ $order_type = isset($_GET["order_type"]); } else { $order_type = $_GET["order_type"]; }

$link_adjust    = "";
$link_adjust   .= "?v_keyword=".$v_keyword;
$link_adjust   .= "&p=".$p;
$link_adjust   .= "&v_KdGudang=".$v_KdGudang;

$link_order_by  = "&order_by=".$order_by."&order_type=".$order_type;

$order_type_change = "asc";
if($order_type=="asc")
{
    $order_type_change = "desc";
}

$order_by_content = "";
if($order_by!="")
{
    $order_by_content = $db["master"].".".$order_by." ".$order_type.",";
} 


$jml_page = 25;
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='search')
				{
                    search_keyword = document.getElementById("search_keyword").value;      
                    search_supplier = document.getElementById("search_supplier").value; 
                    search_gudang = document.getElementById("search_gudang").value; 
                    v_date_from = document.getElementById("v_date_from").value; 
                    v_date_to = document.getElementById("v_date_to").value; 
                    
                    variabel += "&search_keyword="+search_keyword;
                    variabel += "&search_supplier="+search_supplier;
                    variabel += "&search_gudang="+search_gudang;
                    variabel += "&v_date_from="+v_date_from;
                    variabel += "&v_date_to="+v_date_to;
					
					variabel += "&v_NoDokumen="+param1;

					//alert(variabel);
					document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					document.getElementById("col-header").innerHTML   = "";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-search").innerHTML   = xmlhttp.responseText;

							if(param1)
							{
								CallAjaxForm('edit_data' ,param1);
							}
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='add_data')
				{
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='edit_data')
				{
					variabel += "&v_fixed_asset_id="+param1;

					//alert(variabel);
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_proposal')
				{
					v_no_proposal = document.getElementById("v_no_proposal").value;
					
					variabel += "&v_no_proposal="+v_no_proposal;
					variabel += "&v_receipt_no="+param1;

					//alert(variabel);

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");
							
							document.getElementById("td_no_proposal").innerHTML   = arr_data[0];
							document.getElementById("td_tanggal_beli").innerHTML   = arr_data[1];
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_type')
				{
					variabel += "&v_kategory_id="+param1;

					//alert(variabel);
					document.getElementById("show_image_ajax_form").style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("td_type").innerHTML     = xmlhttp.responseText;
							document.getElementById("show_image_ajax_form").style.display = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_sub_type')
				{
					variabel += "&v_type_id="+param1;
					
					//alert(variabel);
					document.getElementById("show_image_ajax_form").style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");

							document.getElementById("td_sub_type").innerHTML   = arr_data[0];
							document.getElementById("td_deskripsi").innerHTML   = arr_data[1];
							document.getElementById("td_tanggal_beli").innerHTML   = arr_data[2];
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_depo')
				{
					variabel += "&v_cabang_id="+param1;
					
					//alert(variabel);
					document.getElementById('show_image_ajax_form').style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("span_depo").innerHTML     = xmlhttp.responseText;
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_ruangan')
				{
					variabel += "&v_depo_id="+param1;
					
					//alert(variabel);
					document.getElementById('show_image_ajax_form').style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("td_ruangan_asset").innerHTML     = xmlhttp.responseText;
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='ajax_chk_asuransi')
				{
                    v_fixed_asset_id = document.getElementById("v_fixed_asset_id").value;  
					chk_gudang_par = document.getElementById("chk_gudang_par").checked;
					chk_gudang_earthquaks = document.getElementById("chk_gudang_earthquaks").checked;
					
					variabel += "&v_fixed_asset_id="+v_fixed_asset_id;
					variabel += "&chk_gudang_par="+chk_gudang_par;
					variabel += "&chk_gudang_earthquaks="+chk_gudang_earthquaks;
					
					//alert(variabel);
					document.getElementById('show_image_ajax_form').style.display = '';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							arr_data = xmlhttp.responseText.split("||");
							
							if(chk_gudang_par)
							{
								document.getElementById("td_no_asuransi_par_1").innerHTML   = arr_data[0];
								document.getElementById("td_no_asuransi_par_2").innerHTML   = arr_data[1];
								document.getElementById("td_periode_asuransi_par_1").innerHTML   = arr_data[2];
								document.getElementById("td_periode_asuransi_par_2").innerHTML   = arr_data[3];
							}
							else 
							{
								document.getElementById("td_no_asuransi_par_1").innerHTML   = "&nbsp;";
								document.getElementById("td_no_asuransi_par_2").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_asuransi_par_1").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_asuransi_par_2").innerHTML   = "&nbsp;";
							}
							
							if(chk_gudang_earthquaks)
							{
								document.getElementById("td_no_asuransi_earthquake_1").innerHTML   = arr_data[4];
								document.getElementById("td_no_asuransi_earthquake_2").innerHTML   = arr_data[5];
								document.getElementById("td_periode_earthquake_1").innerHTML   = arr_data[6];
								document.getElementById("td_periode_earthquake_2").innerHTML   = arr_data[7];
								document.getElementById('tr_no_asuransi_earthquake').style.display = '';
		        				document.getElementById('tr_periode_earthquake').style.display = '';
							}
							else 
							{
								document.getElementById("td_no_asuransi_earthquake_1").innerHTML   = "&nbsp;";
								document.getElementById("td_no_asuransi_earthquake_2").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_earthquake_1").innerHTML   = "&nbsp;";
								document.getElementById("td_periode_earthquake_2").innerHTML   = "&nbsp;";
								document.getElementById('tr_no_asuransi_earthquake').style.display = 'none';
		        				document.getElementById('tr_periode_earthquake').style.display = 'none';
							}
							
							document.getElementById('show_image_ajax_form').style.display    = 'none';
						}

						return false;
					}
					xmlhttp.send(null);
				}

			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function change_pj(nilai)
		{
			if(nilai=="umum")
			{
				document.getElementById('td_type').colSpan = "3";
				document.getElementById('td_sub_type').colSpan = "3";
				document.getElementById('td_empl_name_1').style.display    = 'none';
				document.getElementById('td_empl_name_2').style.display    = 'none';
				document.getElementById('td_jabatan_1').style.display    = 'none';
				document.getElementById('td_jabatan_2').style.display    = 'none';
			}
			else
			{
				document.getElementById('td_type').colSpan = "0";
				document.getElementById('td_sub_type').colSpan = "0";
				document.getElementById('td_empl_name_1').style.display    = '';
				document.getElementById('td_empl_name_2').style.display    = '';
				document.getElementById('td_jabatan_1').style.display    = '';
				document.getElementById('td_jabatan_2').style.display    = '';
			}
			
		}
		
		function cek_qty()
		{
			var v_cek_qty = document.getElementById("v_cek_qty").checked;
			
			//alert(v_cek_qty);
			
			if(v_cek_qty)
			{
				document.getElementById('span_qty').style.display    = '';  
				document.getElementById("v_qty").value = '';
				document.getElementById('v_qty').focus();
			}  
			else
			{
				document.getElementById('span_qty').style.display    = 'none';  
			}
			
		}
		
		function pop_up_employee()
		{
			var variabel;
			var search_cabang_id;
			variabel = "";
			search_cabang_id="";
			
			v_cabang_id = document.getElementById("v_cabang_id").value;    
			
			if(v_cabang_id)
			{
				search_cabang_id = "?search_cabang_id="+v_cabang_id;	
			}
			
			variabel += search_cabang_id;
			  
			windowOpener(600, 800, 'Search Employee', 'npm_fa_master_fixed_asset_pop_up_employee.php'+variabel, 'Search Employee')
		}
		
		function pop_search_pr()
		{
			var variabel;
			variabel = "";
            
            v_KdGudang = document.getElementById("v_KdGudang").value;
            
            if(v_KdGudang=="")
            {
                alert("Gudang harus dipilih...");    
            } 
            else
            {
			    variabel += "?KdGudang="+v_KdGudang;
			    
			    windowOpener(400, 600, 'Search PR', 'npm_purchase_order_pop_search_pr.php'+variabel, 'Search PR')
            }
		}
		
		function change_par()
		{
		    var chk_gedung_par = document.getElementById("chk_gedung_par").checked;
		    
		    if(chk_gedung_par)
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = '';
		        document.getElementById('tr_periode_par').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_par').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = 'none';
		        document.getElementById('tr_periode_par').style.display = 'none';
			} 
		}
		
		function change_earthquake()
		{
		    var chk_gedung_earthquake = document.getElementById("chk_gedung_earthquake").checked;
		    
		    if(chk_gedung_earthquake)
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = '';
		        document.getElementById('tr_periode_earthquake').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_earthquake').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = 'none';
		        document.getElementById('tr_periode_earthquake').style.display = 'none';
			} 
		}

		function confirm_delete(name)
		{
			var r = confirm("Anda yakin ingin menghapus "+name+" ? ");
			if(r)
			{
				document.getElementById("v_del").value = '1';
				document.getElementById("theform").submit();
			}
		}

		function CheckAll(param, target)
		{
			var field = document.getElementsByName(target);
			var chkall = document.getElementById(param);
			if (chkall.checked == true)
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = true ;
			}else
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = false ;
			}
		}

		function reform(val)
		{
			var a = val.split(",");
			var b = a.join("");
			//alert(b);
			return b;
		}

		function format(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(0);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format4(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(4);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format2(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(2);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format6(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(6);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function addSeparatorsNF(nStr, inD, outD, sep)
		{
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1)
			{
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr))
			{
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}

		function toFormat4(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format4(document.getElementById(id).value);
		}

		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
		}

		function toFormat6(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format6(document.getElementById(id).value);
		}

		function isanumber(id)
		{
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				document.getElementById(id).value=0;
			}
			else
			{
				document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
			}
		}

		function start_page()
		{
            try
            {
			    document.getElementById("v_keyword").focus();
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
		}

		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
        
        function get_choose(NoDokumen)
        {   
            window.opener.document.forms["theform"]["v_NoPr"].value = NoDokumen;
            
            self.close() ;
            return;
        }
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">
<form method="get">
<input type="hidden" name="v_KdGudang" value="<?php echo $v_KdGudang; ?>">
<div class="page-container sidebar-collapsed">
	
	
	<div class="main-content">
    
		
		<div class="row">
		
              <div align="center">
                <input class="form-control-new" type="text" style="width: 200px;" name="v_keyword" id="v_keyword" value="<?php echo $v_keyword; ?>">
                <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Cari<i class="entypo-search"></i></button>
              </div>
              <br>
              <table class="table table-bordered responsive">
              <thead>
                <tr>
                    <th width="30">No</th>
                    <th><center>Dokumen</center></th>
                    <th><center>Tanggal</center></th>
                    <th>Divisi</th>
                    <th>Keterangan</th>
                </tr>
                </thead>
                
                <tbody style="color: black;">
                
                <?php
                $keyWord = trim($v_keyword);
                                        
                if($keyWord == '')
                {    
                    $sql = "
                            SELECT 
                                ".$db["master"].".trans_pr_header.NoDokumen,
                                ".$db["master"].".trans_pr_header.TglDokumen,
                                ".$db["master"].".divisi.KdDivisi,
                                ".$db["master"].".divisi.NamaDivisi,
                                ".$db["master"].".trans_pr_header.Keterangan
                            FROM 
                                ".$db["master"].".trans_pr_header
                                INNER JOIN ".$db["master"].".divisi ON
                                    ".$db["master"].".trans_pr_header.KdDivisi = ".$db["master"].".divisi.KdDivisi
                                    AND ".$db["master"].".trans_pr_header.KdGudang = '".$v_KdGudang."'
                            WHERE
                                1
                                AND ".$db["master"].".trans_pr_header.Status = '1'
                            ORDER BY
                                ".$order_by_content." 
                                ".$db["master"].".trans_pr_header.TglDokumen DESC,
                                ".$db["master"].".trans_pr_header.NoDokumen DESC
                    ";                    
                    $query = mysql_query($sql);
                    $max = ceil(mysql_num_rows($query)/$jml_page);
                    $s = $jml_page * $p;
                    $sql = "               
                                SELECT 
                                    ".$db["master"].".trans_pr_header.NoDokumen,
                                    ".$db["master"].".trans_pr_header.TglDokumen,
                                    ".$db["master"].".divisi.KdDivisi,
                                    ".$db["master"].".divisi.NamaDivisi,
                                    ".$db["master"].".trans_pr_header.Keterangan
                                FROM 
                                    ".$db["master"].".trans_pr_header
                                    INNER JOIN ".$db["master"].".divisi ON
                                        ".$db["master"].".trans_pr_header.KdDivisi = ".$db["master"].".divisi.KdDivisi
                                        AND ".$db["master"].".trans_pr_header.KdGudang = '".$v_KdGudang."'
                                WHERE
                                    1 
                                    AND ".$db["master"].".trans_pr_header.Status = '1'
                                ORDER BY
                                    ".$order_by_content." 
                                    ".$db["master"].".trans_pr_header.TglDokumen DESC,
                                    ".$db["master"].".trans_pr_header.NoDokumen DESC 
                                LIMIT ".$s.", ".$jml_page." 
                            ";         
                }
                else
                {
                    unset($arr_keyword);
                    $arr_keyword[0] = "trans_pr_header.NoDokumen";
                    $arr_keyword[1] = "divisi.KdDivisi";
                    $arr_keyword[2] = "divisi.NamaDivisi";
                    $arr_keyword[3] = "trans_pr_header.Keterangan";
                    
                    $search_keyword = search_keyword($v_keyword, $arr_keyword);
                    $where = $search_keyword;
                    
                    $flag=0;
                    for($i=0; $i < strlen($keyWord); $i++)
                    {
                        if($keyWord[$i] == '\'') $flag++;
                        if($keyWord[$i] == '<') $flag++;
                        if($keyWord[$i] == '>') $flag++;
                    }
                    
                    if($flag==0)
                    {
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".trans_pr_header.NoDokumen,
                                        ".$db["master"].".trans_pr_header.TglDokumen,
                                        ".$db["master"].".divisi.KdDivisi,
                                        ".$db["master"].".divisi.NamaDivisi,
                                        ".$db["master"].".trans_pr_header.Keterangan
                                    FROM 
                                        ".$db["master"].".trans_pr_header
                                        INNER JOIN ".$db["master"].".divisi ON
                                            ".$db["master"].".trans_pr_header.KdDivisi = ".$db["master"].".divisi.KdDivisi
                                            AND ".$db["master"].".trans_pr_header.KdGudang = '".$v_KdGudang."'
                                    WHERE
                                        1
                                        AND ".$db["master"].".trans_pr_header.Status = '1'
                                        ".$where."
                                    ORDER BY
                                        ".$order_by_content." 
                                        ".$db["master"].".trans_pr_header.TglDokumen DESC,
                                        ".$db["master"].".trans_pr_header.NoDokumen DESC 
                               ";
                               //echo "<pre>";
                               //echo $sql;
                               //echo "</pre>";
                        $query = mysql_query($sql);
                        $max = ceil(mysql_num_rows($query)/$jml_page);
                        $s = $jml_page * $p;
                        $sql = "
                                    SELECT 
                                        ".$db["master"].".trans_pr_header.NoDokumen,
                                        ".$db["master"].".trans_pr_header.TglDokumen,
                                        ".$db["master"].".divisi.KdDivisi,
                                        ".$db["master"].".divisi.NamaDivisi,
                                        ".$db["master"].".trans_pr_header.Keterangan
                                    FROM 
                                        ".$db["master"].".trans_pr_header
                                        INNER JOIN ".$db["master"].".divisi ON
                                            ".$db["master"].".trans_pr_header.KdDivisi = ".$db["master"].".divisi.KdDivisi
                                            AND ".$db["master"].".trans_pr_header.KdGudang = '".$v_KdGudang."'
                                    WHERE
                                        1
                                        AND ".$db["master"].".trans_pr_header.Status = '1'
                                        ".$where."
                                    ORDER BY
                                        ".$order_by_content." 
                                        ".$db["master"].".trans_pr_header.TglDokumen DESC,
                                        ".$db["master"].".trans_pr_header.NoDokumen DESC 
                                    LIMIT ".$s.", ".$jml_page." 
                              ";
                    }else
                    {
                        $msg = "Your input are not allowed!";
                    }
                }
                
                   //echo "<pre>".$sql."</pre>";
                $query = mysql_query($sql);
                        $sv = 0;
                        $i=0+($jml_page*$p);
                if(!$row = mysql_num_rows($query))
                {
                 echo "<tr>";
                 echo "<td align=\"center\" colspan=\"100%\">";
                 echo "No Data";
                 echo "</td>";
                 echo "</tr>";
                }
                else
                {
                     while ($row = mysql_fetch_array($query))
                     {
                        list($NoDokumen, $TglDokumen, $KdDivisi, $NamaDivisi, $Keterangan) = $row; 
                        
                        $sv++;
                        $i++;
                    
              ?>
                
                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                    <td onclick="get_choose('<?php echo $NoDokumen; ?>')"><?php echo $i; ?></td>
                    <td onclick="get_choose('<?php echo $NoDokumen; ?>')" align="center"><?php echo $NoDokumen; ?></td>
                    <td onclick="get_choose('<?php echo $NoDokumen; ?>')" align="center"><?php echo format_show_date($TglDokumen); ?></td>
                    <td onclick="get_choose('<?php echo $NoDokumen; ?>')"><?php echo $NamaDivisi; ?></td>
                    <td onclick="get_choose('<?php echo $NoDokumen; ?>')"><?php echo $Keterangan; ?></td>
                </tr>
              <?php
                     } 
                }
              ?>
                    
                </tbody>
                
                <tr>
                    <td colspan="100%" align="center"><?php include("paging.php"); ?></td>
                 </tr> 
                
              </table>
       	
       	</div>
</form>		
		
    
<?php include("footer.php"); ?>