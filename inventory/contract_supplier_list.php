<?php
include("header.php");

$modul            = "Contract Supplier List";
$file_name        = "contract_supplier_list.php";

if(!empty($_POST['form1'])){
	$form1=$_POST['form1'];
	$form2=$_POST['form2'];
	$form3=$_POST['form3'];
	$key=$_POST['key'];
	$by=$_POST['by'];
}
else{
	$form1=$_POST['form1'];
	$form2=$_POST['form2'];
	$form3=$_POST['form3'];
	$key=$_POST['key'];
	$by=$_POST['by'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>
	<script>
		function PopMutasi(param){
			window.open(param,'','scrollbars=yes,width=800,height=600,left=250,top=100');		
		}
	</script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>

<table class="table table-bordered responsive">
	<form action="" method="post">
	<input type="hidden" name="form1" id="form1" value="<?=$form1?>" />
	<input type="hidden" name="form2" id="form2" value="<?=$form2?>" />
	<input type="hidden" name="form3" id="form3" value="<?=$form3?>" />
	<tr>
	<td align='center'>Keyword : &nbsp; </td>
	<td>
	<input type="text" class="form-control-new" name="key" id="key" size="30" value="<?=$key?>"> &nbsp;&nbsp;
	<select name="by" id="by" class="form-control-new">
		<option value="NoTransaksi" <? if($by=="h.NoTransaksi"){echo"selected";}?>>NoTransaksi</option>
		<option value="Nama" <? if($by=="s.Nama"){echo"selected";}?>>Nama Supplier</option>
	</select>
	&nbsp;
	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_submit" id="btn_submit"  value="Search">Search<i class="entypo-search"></i></button>
	</td>
	<td align='center'>
	<a href='contract_supplier_edit.php?flag=new'><img src='images/add.gif' border='0' title='Add'>&nbsp;Add</a>
	</td>
	</tr>
	</form>
</table>
<br>
<table align='center' class="table table-bordered responsive" border='1' id='table_list'>
	<tr bordercolor="#FFFFFF">
		<td nowrap class="title_table" style="width: 100px;" align='center'>No Transaksi</td>
		<td nowrap class="title_table" style="width: 100px;" align='center'>Tgl Transaksi</td>
		<td nowrap class="title_table" style="width: 100px;" align='center'>Supplier</td>
		<td nowrap class="title_table" style="width: 100px;" align='center'>Periode</td>
		<td nowrap class="title_table" style="width: 100px;" align='center'>Action</td>
	</tr>		
<?

	$limit=20;
	if(empty($_GET['start'])){
		$start=0;	
	}
	else{
			$start=$_GET['start'];	
	}
	
	$q = "
			SELECT h.NoTransaksi, h.TglTransaksi, h.KdSupplier, s.Nama, h.PeriodeAwal, h.PeriodeAkhir, h.Status
			FROM contract_supplier_header h, supplier s
			WHERE 1
			AND h.Status != '2'
			AND h.KdSupplier=s.KdSupplier	
	";
	if(!empty($key)){
		$q1=mysql_query("$q AND $by like'%$key%' ORDER by h.NoTransaksi DESC limit $start,$limit");
		//$q2=mysql_query("select PCode, NamaLengkap as NamaBarang, HargaBeli from masterbarang where KdSubKategori IN (SELECT KdSubKategori FROM gudang_subkategori WHERE KdGudang NOT IN ($gudang)) AND $by like'%$key%' order by PCode asc ");
		$q2=mysql_query("$q AND $by like'%$key%' ORDER by h.NoTransaksi DESC ");
		$line=mysql_num_rows($q2);
	}
	else{
			$q1=mysql_query("$q ORDER by h.NoTransaksi DESC limit $start,$limit");
			$q2=mysql_query("$q ORDER by h.NoTransaksi DESC ");
			$line=mysql_num_rows($q2);
	}

	while($data=mysql_fetch_object($q1)){
		$x++;

	?>
		<tr>
			<td nowrap>&nbsp;<? echo $data->NoTransaksi;?></td>
			<td nowrap>&nbsp;<? echo ubah_tanggal($data->TglTransaksi);?></td>
			<td nowrap>&nbsp;<? echo $data->KdSupplier." :: ".$data->Nama;?></td>
			<td nowrap>&nbsp;<? echo ubah_tanggal($data->PeriodeAwal)." <b>s/d</b> ".ubah_tanggal($data->PeriodeAkhir);?></td>
			<td nowrap align="center">
			<table width='100' align='center' border='0'>
			<tr>
			<td width='20' align='center'>			
			<a href='contract_supplier_edit.php?flag=view&notransaksi=<? echo $data->NoTransaksi;?>'><img src='images/view.png' border='0' title='View'></a>
			</td>
			<?
			if($data->Status==0){
			?>
				<td width='20' align='center'>
				<a href='contract_supplier_edit.php?flag=edit&notransaksi=<? echo $data->NoTransaksi;?>'><img src='images/edit.png' border='0' title='Edit'></a>
				</td>
				<td width='20' align='center'>
				<a href='contract_supplier_edit.php?flag=delete&notransaksi=<? echo $data->NoTransaksi;?>'><img src='images/delete.gif' border='0' title='Delete'></a>
				</td>
				<td width='20' align='center'>
				<a href='contract_supplier_edit.php?flag=close&notransaksi=<? echo $data->NoTransaksi;?>'><img src='images/save.gif' border='0' title='Close'></a>			
				</td>
			<?
			}
			elseif($data->Status==1){
				echo "<td width='60' align='center'>Close</td>";
			}
			?>
			</tr></table>
			</td>
		</tr>	
	<?
	}
	?>
</table>
<div align='center'>
<?
$jet=new all;
$jet->splitpage2("contract_supplier_list.php?",$line,$limit,"form1=$form1&form2=$form2&form3=$form3&key=$key&by=$by");
?>
</div>
</body>	
</html>

<?
$limit=12;
class all{
	function splitpage2($address,$line,$limit,$hal){
		$i=$line/$limit;
		$i=ceil($i);
		$min=$limit-1;
		?>		
		
		<script language="javascript">
			function go_url(){
			var page=document.getElementById('starts').value;	
			var limits=document.getElementById('limit').value;
			var adds=document.getElementById('address').value;
			var hals=document.getElementById('hal').value;
			var from=parseFloat(page) - 1;
			var sa=parseFloat(from) * parseFloat(limits);
			var startn=Math.ceil(sa);			
			
			window.location=""+adds+"?start="+startn+"&"+hals+"";
			//alert(""+adds+"start="+startn+"&"+hals+"");
			//self.location="http://www.yahoo.com";
			}
		</script>
		<?
		//	echo $_GET['start'];
		
		//if(!empty($_GET['start'])){
		$start=$_GET['start'];	
		$eu = ($start - 0);
		$ini = $eu + $limit;
		$back = $eu - $limit;
		$next = $eu + $limit;
		//}
		
	
		$bagi=$ini/$limit;
		$page=ceil($bagi);
		
		echo"<table><tr>";
		echo"<td>";
		if($back >=0) {
		print "<a href='$address?&start=$back&$hal'><img src='images/first.gif' border='0'></a>";
		}
		else{
		print "<img src='images/firstdisab.gif' border='0'></a>";
		}
		echo"</td>";
		//echo"<form action='err' method='get' name='form1'>";
		echo"<td>";
		echo"<input type='text' name='starts' id='starts' size='1' maxlength='3' dir='LTC' value='$page' onkeydown=\"if(event.which==13){go_url();}\">";
		echo"</td>";
		echo"<input type='hidden' id='address' name='address' value='$address'>";
		echo"<input type='hidden' id='hal' name='hal' value='$hal'>";
		echo"<input type='hidden' id='limit' name='limit' value='$limit'>";
		//echo"</form>";
		echo"<td>";
		if($ini < $line) {
		print "<a href='$address?&start=$next&$hal'><img src='images/last.gif' border='0'></a>";
		}
		else{
		print "<img src='images/lastdisab.gif' border='0'>";
		}
		echo"</td>";
		echo"</tr></table>";	
			
		
	}
}

function ubah_tanggal($tanggalan){
 $tgl = explode("-",$tanggalan);
 $tgl2 = $tgl[2]."/". $tgl[1]."/". $tgl[0];
 return $tgl2;
}	

include("footer.php");
?>