<?php
include("header.php");

$modul   = "Master Barang - Check Nama Barang";

if(!isset($_GET["v_NamaLengkap"])){ $v_NamaLengkap = isset($_GET["v_NamaLengkap"]); } else { $v_NamaLengkap = $_GET["v_NamaLengkap"]; }                                                                                                                                        

if($v_NamaLengkap=="")
{
    echo "Nama Barang harus diisi";
    die();
}

	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
        
        function mouseover(target)
        {
            if(target.bgColor!="#cafdb5")
            {
                if (target.bgColor=='#ccccff')
                target.bgColor='#ccccff';
                else
                target.bgColor='#c1cdd8';
            }
        }

        function mouseout(target)
        {
            if(target.bgColor!="#cafdb5")
            {
                if (target.bgColor=='#ccccff')
                target.bgColor='#ccccff';
                else
                target.bgColor='#FFFFFF';
            }
        }

        function mouseclick(target, idobject, num)
        {
            //var pjg = document.getElementById(idobject + '_sum').innerHTML;
            for(i=0;i<num;i++)
            {
                if (document.getElementById(idobject+'_'+i) != undefined)
                {
                    document.getElementById(idobject+'_'+i).bgColor='#f5faff';
                    if (target.id == idobject+'_'+i)
                    target.bgColor='#ccccff';
                }
            }
        }

        function mouseclick1(target)
        {
            //var pjg = document.getElementById(idobject + '_sum').innerHTML;
            if(target.bgColor!="#cafdb5")
            {
                target.bgColor="#cafdb5";
            }
            else
            {
                target.bgColor="#FFFFFF";
            }
        }
    </script>
</head>

<body class="page-body skin-black">
<form method="get">
<div class="page-container sidebar-collapsed">
	
	
	<div class="main-content">
    
		
		<div class="row">
		
              
              <br>
              <table class="table table-bordered responsive">
              <thead>
                <tr>
                    <th width="30">No</th>
                    <th><center>PCode</center></th>
                    <th><center>Nama Barang</center></th>
                    <th>Divisi</th>
                </tr>
                </thead>
                
                <tbody style="color: black;">
                
                <?php
                    $i = 1;
                    $arr_keyword[0] = "masterbarang.NamaLengkap";
                
                    $where = search_keyword_or($v_NamaLengkap, $arr_keyword);
                
                    $sql = "
                            SELECT 
                                ".$db["master"].".masterbarang.PCode,
                                ".$db["master"].".masterbarang.NamaLengkap,
                                ".$db["master"].".divisi.KdDivisi,
                                ".$db["master"].".divisi.NamaDivisi
                            FROM 
                                ".$db["master"].".masterbarang
                                INNER JOIN ".$db["master"].".divisi ON
                                    ".$db["master"].".masterbarang.KdDivisi = ".$db["master"].".divisi.KdDivisi
                            WHERE
                                1
                                ".$where."
                                AND ".$db["master"].".masterbarang.NamaLengkap != '".$v_NamaLengkap."'
                            ORDER BY
                                ".$db["master"].".masterbarang.NamaLengkap ASC
                    "; 
                    $qry = mysql_query($sql);
                    while($row = mysql_fetch_array($qry))
                    {                   
                        list($PCode, $NamaLengkap, $KdDivisi, $NamaDivisi) = $row;
              ?>
                
                <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                    <td><?php echo $i; ?></td>
                    <td align="center"><?php echo $PCode; ?></td>
                    <td><?php echo $NamaLengkap; ?></td>
                    <td><?php echo $NamaDivisi; ?></td>
                </tr>
              <?php
                        $i++;
                    }
                    
                    if($i==1)
                    {
                        ?>
                            <tr>
                                <td colspan="100%" align="center" style="font-weight: bold;">Data tidak ada</td>
                            </tr>
                        <?php
                    }  
              ?>
                    
                </tbody>
                
               
              </table>
       	
       	</div>
</form>		
		
    
<?php mysql_close(); ?>