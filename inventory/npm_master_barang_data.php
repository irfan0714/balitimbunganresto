<?php
include("header.php");
                               
$modul            = "MASTER BARANG";
$file_current     = "npm_master_barang.php";
$file_name        = "npm_master_barang_data.php";


function get_code_PCode($v_KdSubDivisi, $v_KdKategori, $v_KdSubKategori)
{
    global $db;
    
    //2 sub divisi, 1 digit kategori, digit sub kategori, 4 angka counter 
    
    if($v_KdSubDivisi==13 || $v_KdSubDivisi==19 || $v_KdSubDivisi==20)
    {
        $q = "
                SELECT
                    RIGHT(".$db["master"].".masterbarang.PCode,3) AS last_counter
                FROM
                    ".$db["master"].".masterbarang
                WHERE
                    1
                    AND LEFT(".$db["master"].".masterbarang.PCode, 6) = '".$v_KdSubDivisi.$v_KdSubKategori."'
                ORDER BY
                    ".$db["master"].".masterbarang.PCode*1 DESC
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($last_counter) = $row; 
        
        $counternya = ($last_counter*1) + 1;
        $PCode_oke  = $v_KdSubDivisi.$v_KdSubKategori.sprintf("%03s", $counternya);   
    }
    else
    {
        $strlen_KdSubDivisi   = strlen($v_KdSubDivisi);
        $strlen_KdKategori    = strlen($v_KdKategori);
        $strlen_KdSubKategori = strlen($v_KdSubKategori);
        
        $total_digit = ($strlen_KdSubDivisi*1)+($strlen_KdKategori*1)+($strlen_KdSubKategori*1);
        $digit = $v_KdSubDivisi.$v_KdKategori.$v_KdSubKategori;
        
        $q = "
                SELECT
                    RIGHT(".$db["master"].".masterbarang.PCode,4) AS last_counter
                FROM
                    ".$db["master"].".masterbarang
                WHERE
                    1
                    AND LEFT(".$db["master"].".masterbarang.PCode, ".$total_digit.") = '".$digit."'
                ORDER BY
                    ".$db["master"].".masterbarang.PCode*1 DESC
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($last_counter) = $row;
        //die();
        
        $counternya = ($last_counter*1) + 1;
        $PCode_oke  = $digit.sprintf("%04s", $counternya);
    }
                                                 
    return $PCode_oke;
}

function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}
          
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
        $search_keyword        = trim($_GET["search_keyword"]);
        $search_KdDivisi       = trim($_GET["search_KdDivisi"]);
        $search_KdKategori     = trim($_GET["search_KdKategori"]);
        $search_order_by       = trim($_GET["search_order_by"]);
        $search_approval       = trim($_GET["search_approval"]);
        $v_PCode_curr          = trim($_GET["v_PCode_curr"]);
      
        $where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "masterbarang.PCode";    
            $arr_keyword[1] = "masterbarang.NamaLengkap";    
            $arr_keyword[2] = "kategori.KdKategori";      
            $arr_keyword[3] = "kategori.NamaKategori";      
            $arr_keyword[4] = "divisi.KdDivisi";      
            $arr_keyword[5] = "divisi.NamaDivisi";      
            $arr_keyword[6] = "masterbarang.Barcode1";    
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        if($search_KdDivisi!="")
        {
            $where .= " AND masterbarang.KdDivisi = '".$search_KdDivisi."'";
        }
        
        if($search_KdKategori)
        {
            $where .= " AND masterbarang.KdKategori = '".$search_KdKategori."'";
        }
        
        if($search_order_by=="Nama Barang")
        {                       
            $where_order_by = " ".$db["master"].".masterbarang.NamaLengkap ASC "; 
        }
        else if($search_order_by=="PCode")
        {                       
            $where_order_by = " ".$db["master"].".masterbarang.PCode ASC "; 
        }
        else if($search_order_by=="Add Date")
        {                       
            $where_order_by = " ".$db["master"].".masterbarang.AddDate ASC "; 
        }
        
        if($v_PCode_curr)
        {
            $where = " AND ".$db["master"].".masterbarang.PCode = '".$v_PCode_curr."'";
        }
        
        $where_approval = "";
        if($search_approval)
        {
            $where_approval = " AND ".$db["master"].".masterbarang.Approval_Status = '0'";    
        }
        
        // select query all
        {
            $counter = 1;
            $q="
                SELECT 
                  ".$db["master"].".masterbarang.PCode,
                  ".$db["master"].".masterbarang.NamaLengkap,
                  ".$db["master"].".masterbarang.Barcode1,
                  ".$db["master"].".masterbarang.SatuanSt,
                  ".$db["master"].".divisi.KdDivisi,
                  ".$db["master"].".divisi.NamaDivisi,
                  ".$db["master"].".kategori.KdKategori,
                  ".$db["master"].".kategori.NamaKategori,
                  ".$db["master"].".masterbarang.Approval_By,
                  ".$db["master"].".masterbarang.Approval_Date,
                  ".$db["master"].".masterbarang.Approval_Status,
                  ".$db["master"].".masterbarang.Approval_Remarks,
                  ".$db["master"].".masterbarang.AddDate,
                  ".$db["master"].".masterbarang.EditDate
                FROM
                  ".$db["master"].".masterbarang 
                  LEFT JOIN ".$db["master"].".divisi 
                    ON ".$db["master"].".masterbarang.KdDivisi = ".$db["master"].".divisi.KdDivisi 
                  LEFT JOIN ".$db["master"].".kategori 
                    ON ".$db["master"].".masterbarang.KdKategori = ".$db["master"].".kategori.KdKategori
                WHERE 1 
                    ".$where."
                    ".$where_approval."
                ORDER BY 
                    ".$where_order_by."
            ";
            $sql=mysql_query($q);
            while($row=mysql_fetch_array($sql))
            { 
                list(
                    $PCode,
                    $NamaLengkap,
                    $Barcode1,
                    $SatuanSt,
                    $KdDivisi,
                    $NamaDivisi,
                    $KdKategori,
                    $NamaKategori,
                    $Approval_By,
                    $Approval_Date,
                    $Approval_Status,
                    $Approval_Remarks,
                    $AddDate,
                    $EditDate
                )=$row;
                
                if($AddDate=="0000-00-00 00:00:00")
                {
                    $AddDate = "";
                }
                
                $arr_data["list_data"][$counter]=$counter;
                $arr_data["data_PCode"][$counter]=$PCode;
                $arr_data["data_NamaLengkap"][$counter]=$NamaLengkap;
                $arr_data["data_Barcode1"][$counter]=$Barcode1;
                $arr_data["data_SatuanSt"][$counter]=$SatuanSt;
                $arr_data["data_KdDivisi"][$counter]=$KdDivisi;
                $arr_data["data_NamaDivisi"][$counter]=$NamaDivisi;
                $arr_data["data_KdKategori"][$counter]=$KdKategori;
                $arr_data["data_NamaKategori"][$counter]=$NamaKategori;
                $arr_data["data_Approval_By"][$counter]=$Approval_By;
                $arr_data["data_Approval_Date"][$counter]=$Approval_Date;
                $arr_data["data_Approval_Status"][$counter]=$Approval_Status;
                $arr_data["data_Approval_Remarks"][$counter]=$Approval_Remarks;
                $arr_data["data_AddDate"][$counter]=$AddDate;
                $arr_data["data_EditDate"][$counter]=$EditDate;
                
                $counter++;
            }
        }  
        
        
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_purchase_order_print.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
            <tr class="title_table">
                <td width="30">No</td>
                <td>PCode</td>
                <td>Barcode</td>
                <td>Nama Barang</td>
                <td>Satuan</td>
                <td>Divisi</td>
                <td>Kategori</td>     
                <td>Add Date</td>      
                <td>Approval</td>    
                <td width="15" style="display: none;"><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></td>    
            </tr>
        <tbody style="color: black;">
        
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        foreach($arr_data["list_data"] as $counter => $val)
        {    
            $nomor++;
            
            $PCode = $arr_data["data_PCode"][$counter];
            $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
            $Barcode1 = $arr_data["data_Barcode1"][$counter];
            $SatuanSt = $arr_data["data_SatuanSt"][$counter];
            $KdDivisi = $arr_data["data_KdDivisi"][$counter];
            $NamaDivisi = $arr_data["data_NamaDivisi"][$counter];
            $KdKategori = $arr_data["data_KdKategori"][$counter];
            $NamaKategori = $arr_data["data_NamaKategori"][$counter];
            $Approval_By = $arr_data["data_Approval_By"][$counter];
            $Approval_Date = $arr_data["data_Approval_Date"][$counter];
            $Approval_Status = $arr_data["data_Approval_Status"][$counter];
            $Approval_Remarks = $arr_data["data_Approval_Remarks"][$counter];
            $AddDate = $arr_data["data_AddDate"][$counter];
            $EditDate = $arr_data["data_EditDate"][$counter];
            
            if($Approval_By=="")
            {
                $approval = "waiting";
            } 
            else
            {
                if($Approval_Status==1)
                {
                    $approval = "<font color='blue'>".$Approval_By." - ".format_show_datetime($Approval_Date)."</font>";
                }
                else if($Approval_Status==2)
                {
                    $approval = "<font color='red'>".$Approval_By." - ".format_show_datetime($Approval_Date)."<br>".$Approval_Remarks."</font>";
                }
            }
            
            
            $style_color = "";
            if($v_PCode_curr==$PCode)
            {
                $style_color = "background: #CAFDB5";
            }
                    
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $PCode; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $Barcode1; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $NamaLengkap; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" align="" style=""><?php echo $SatuanSt; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style=""><?php echo $NamaDivisi; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style=""><?php echo $NamaKategori; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style="text-align: center;"><?php echo format_show_datetime($AddDate); ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $PCode; ?>')" style=""><?php echo $approval; ?></td>  
                <td align="center" style="display: none;">
                    &nbsp;
                    <?php 
                        if($Approval_Status==1)
                        {
                    ?>
                    <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $PCode; ?>">
                    <?php 
                        }
                    ?>
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot style="display: none;">
            <tr>
                <td colspan="100%" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Print">Print</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
    </table> 
    </form>
    </div>  
       <?php  
    }

    else if($ajax=="add_data")
    {
        
        $q = "
            SELECT
                ".$db["master"].".divisi.KdDivisi,
                ".$db["master"].".divisi.NamaDivisi
            FROM
                ".$db["master"].".divisi
            WHERE
                1
            ORDER BY
                ".$db["master"].".divisi.NamaDivisi ASC
        ";
        $qry_divisi = mysql_query($q);
        while($row_divisi = mysql_fetch_array($qry_divisi))
        { 
            list($KdDivisi, $NamaDivisi) = $row_divisi;    
            
            $arr_data["list_divisi"][$KdDivisi] = $KdDivisi;
            $arr_data["NamaDivisi"][$KdDivisi] = $NamaDivisi;
            
        }
        
        $q = "
            SELECT
                ".$db["master"].".kategori.KdKategori,
                ".$db["master"].".kategori.NamaKategori
            FROM
                ".$db["master"].".kategori
            WHERE
                1
            ORDER BY
                ".$db["master"].".kategori.KdKategori ASC
        ";
        $qry_kategori = mysql_query($q);
        while($row_kategori = mysql_fetch_array($qry_kategori))
        { 
            list($KdKategori, $NamaKategori) = $row_kategori;    
            
            $arr_data["list_kategori"][$KdKategori] = $KdKategori;
            $arr_data["NamaKategori"][$KdKategori] = $NamaKategori;
            
        }
        
        $q = "
            SELECT
                ".$db["master"].".satuan.KdSatuan,
                ".$db["master"].".satuan.NamaSatuan
            FROM
                ".$db["master"].".satuan
            WHERE
                1
            ORDER BY
                ".$db["master"].".satuan.NamaSatuan ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        { 
            list($KdSatuan) = $row;    
            
            $arr_data["list_satuan"][$KdSatuan] = $KdSatuan;
            
        }
        
      
        ?>
        <div class="col-md-12" align="left">
        
            <ol class="breadcrumb title_table">
                <li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
            
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">Nama Barang</td>
                <td>
                     <input type="text" class="form-control-new" style="width: 400px; text-transform: uppercase;" name="v_NamaLengkap" id="v_NamaLengkap" maxlength="50">
                     
                     <button type="button" name="btn_check" id="btn_check" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check()" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
            
            <!--<tr>
                <td class="title_table" width="200">Barcode</td>
                <td>
                     <input type="text" class="form-control-new" style="width: 400px; text-transform: uppercase;" name="v_Barcode1" id="v_Barcode1" maxlength="50">
                </td>
                
            </tr>-->
            
            <tr>
                <td class="title_table" width="200">Divisi</td>
                <td>
                    <select name="v_KdDivisi" id="v_KdDivisi" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_divisi', this.value)">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_divisi"] as $KdDivisi=>$val)
                            {
                                $NamaDivisi = $arr_data["NamaDivisi"][$KdDivisi];
                                ?>
                                <option value="<?php echo $KdDivisi; ?>"><?php echo $NamaDivisi; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                    
                    <span id="td_KdSubDivisi"></span>
                </td> 
            </tr>  

            <tr>
                <td class="title_table">Kategori</td>
                <td id="td_Kategori">
                    <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori', this.value)">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_kategori"] as $KdKategori=>$val)
                            {
                                $NamaKategori = $arr_data["NamaKategori"][$KdKategori];
                                ?>
                                <option value="<?php echo $KdKategori; ?>"><?php echo $NamaKategori; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                    
                    <span id="td_KdSubKategori"></span>
                </td> 
                
            </tr>
                
            <tr>
                <td class="title_table">Satuan</td>      
                <td>
                     <select name="v_SatuanSt" id="v_SatuanSt" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_satuan"] as $KdSatuan=>$val)
                            {
                                ?>
                                <option value="<?php echo $KdSatuan; ?>"><?php echo $KdSatuan; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
                
               
            </tr>
             
            
            <tr> 
                <td>&nbsp;</td>                   
                <td>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
            
            </table>  
            </form>
        </div>
        <?php
        
    }
    
    else if($ajax=="edit_data")
    {                                      
        $v_PCode = $_GET["v_PCode"];  
        
        $q = "
                SELECT 
                  ".$db["master"].".masterbarang.PCode,
                  ".$db["master"].".masterbarang.NamaLengkap,
                  ".$db["master"].".masterbarang.SatuanSt,
                  ".$db["master"].".masterbarang.Barcode1,
                  ".$db["master"].".divisi.KdDivisi,
                  ".$db["master"].".divisi.NamaDivisi,
                  ".$db["master"].".subdivisi.KdSubDivisi,
                  ".$db["master"].".subdivisi.NamaSubDivisi,
                  ".$db["master"].".kategori.KdKategori,
                  ".$db["master"].".kategori.NamaKategori,
                  ".$db["master"].".subkategori.NamaSubKategori,
                  ".$db["master"].".masterbarang.Approval_By,
                  ".$db["master"].".masterbarang.Approval_Date,
                  ".$db["master"].".masterbarang.Approval_Status,
                  ".$db["master"].".masterbarang.Approval_Remarks,
                  ".$db["master"].".masterbarang.AddDate,
                  ".$db["master"].".masterbarang.EditDate,
                  ".$db["master"].".masterbarang.AddUser,
                  ".$db["master"].".masterbarang.EditUser,
                  RIGHT(".$db["master"].".masterbarang.KdKategori,2) AS KdKategori_Barang,
                  ".$db["master"].".masterbarang.KdSubKategori AS KdSubKategori_Barang,
                  ".$db["master"].".masterbarang.Harga1c,
                  ".$db["master"].".masterbarang.komisi,
                  ".$db["master"].".masterbarang.DiscInternal,
                  ".$db["master"].".masterbarang.Service_charge,
                  ".$db["master"].".masterbarang.PPN,
                  ".$db["master"].".masterbarang.KdRekeningPersediaan,
                  ".$db["master"].".masterbarang.Konsinyasi
                FROM
                  ".$db["master"].".masterbarang 
                  LEFT JOIN ".$db["master"].".divisi 
                    ON ".$db["master"].".masterbarang.KdDivisi = ".$db["master"].".divisi.KdDivisi 
                  LEFT JOIN ".$db["master"].".subdivisi 
                    ON ".$db["master"].".masterbarang.KdSubDivisi = ".$db["master"].".subdivisi.KdSubDivisi 
                  LEFT JOIN ".$db["master"].".kategori 
                    ON ".$db["master"].".masterbarang.KdKategori = ".$db["master"].".kategori.KdKategori
                  LEFT JOIN ".$db["master"].".subkategori
                    ON ".$db["master"].".masterbarang.KdSubKategori = ".$db["master"].".subkategori.KdSubKategori
                WHERE 1
                    AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'
                LIMIT
                    0,1
        ";     
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
        
        // black eye
        if($arr_curr["KdSubDivisi"]=="13" || $arr_curr["KdSubDivisi"]=="19")
        {     
            $q = "
                    SELECT
                        ".$db["master"].".subkategoripos.NamaSubKategori
                    FROM
                        ".$db["master"].".subkategoripos
                    WHERE
                        1
                        AND ".$db["master"].".subkategoripos.KdSubKategori = '".$arr_curr["KdSubKategori_Barang"]."'
                    LIMIT
                        0,1
            ";
            $qry = mysql_query($q);
            $row = mysql_fetch_array($qry);
            list($NamaSubKategori) = $row;
            
            $arr_curr["NamaKategori"] = "Black Eye";
            $arr_curr["NamaSubKategori"] = $NamaSubKategori;
            //echo "masuk";    
        }
        
        if($arr_curr["Approval_By"]=="")
        {
            $approval = "waiting";
        } 
        else
        {
            if($arr_curr["Approval_Status"]==1)
            {
                $approval = "<font color='blue'>".$arr_curr["Approval_By"]." - ".format_show_datetime($arr_curr["Approval_Date"])."</font>";
            }
            else if($arr_curr["Approval_Status"]==2)
            {
                $approval = "<font color='red'>".$arr_curr["Approval_By"]." - ".format_show_datetime($arr_curr["Approval_Date"])."<br>".$arr_curr["Approval_Remarks"]."</font>";
            }
        } 
        
        
        
        
        $q = "
                SELECT
                    ".$db["master"].".satuan.KdSatuan,    
                    ".$db["master"].".satuan.NamaSatuan
                FROM
                    ".$db["master"].".satuan
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".satuan.KdSatuan ASC
        ";
        $qry_pr = mysql_query($q);
        while($row_pr = mysql_fetch_array($qry_pr))
        { 
            list($KdSatuan, $NamaSatuan) = $row_pr;    
            
            $arr_data["list_satuan"][$KdSatuan] = $KdSatuan;
        }
            
        
        ?> 
        <div class="col-md-12" align="left">
            
            <ol class="breadcrumb title_table">
                <li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
                                                       
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value="">    
        <input type="hidden" name="v_PCode" id="v_PCode" value="<?php echo $arr_curr["PCode"]; ?>">   
        
        <table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="200">PCode</td>
                <td style="color: black; font-weight: bold"><?php echo $arr_curr["PCode"]; ?></td>
            </tr>
        
             <tr>
                <td class="title_table" width="200">Nama Barang</td>
                <td>
                     <input type="text" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaLengkap" id="v_NamaLengkap" maxlength="50" value="<?php echo $arr_curr["NamaLengkap"]; ?>">
                     
                     <button type="button" name="btn_check" id="btn_check" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check()" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
            
             <tr>
                <td class="title_table" width="200">Barcode</td>
                <td>
                     <input type="text" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_Barcode1" id="v_Barcode1" maxlength="50" value="<?php echo $arr_curr["Barcode1"]; ?>">
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Divisi</td>
                <td><?php echo $arr_curr["NamaDivisi"]." :: ".$arr_curr["NamaSubDivisi"]; ?></td> 
            </tr>  

            <tr>
                <td class="title_table">Kategori</td>
                <td><?php echo $arr_curr["NamaKategori"]." :: ".$arr_curr["NamaSubKategori"]; ?></td> 
            </tr>
                
            <tr>
                <td class="title_table">Satuan</td>      
                <td>
                     <select name="v_SatuanSt" id="v_SatuanSt" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_satuan"] as $KdSatuan=>$val)
                            {
                                $selected = "";
                                if($KdSatuan==$arr_curr["SatuanSt"])
                                {
                                    $selected = "selected='selected'";    
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdSatuan; ?>"><?php echo $KdSatuan; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <?php
                if($ses_login=="william1001" || $ses_login=="trisno1402" || $ses_login=="mart2005" || $ses_login=="mechael0101" || $ses_login=="tonny1205")
                {
                 
                    ?>
                    
                    <tr>
                        <td colspan="2">&nbsp;</td>      
                    </tr> 
                    
                    <tr>
                        <td class="title_table" colspan="2">FINANCE DATA</td>      
                    </tr> 
                    
                    <tr>
                        <td class="title_table" width="200">Harga</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_Harga1c')" name="v_Harga1c" id="v_Harga1c" value="<?php echo format_number($arr_curr["Harga1c"],2); ?>">
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td class="title_table" width="200">PPN</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_PPN')" name="v_PPN" id="v_PPN" value="<?php echo format_number($arr_curr["PPN"],2); ?>">
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td class="title_table" width="200">Service Charge</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_Service_charge')" name="v_Service_charge" id="v_Service_charge" value="<?php echo format_number($arr_curr["Service_charge"],2); ?>">
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td class="title_table" width="200">Komisi</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_komisi')" name="v_komisi" id="v_komisi" value="<?php echo format_number($arr_curr["komisi"],2); ?>">
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="title_table" width="200">Disc Internal</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_DiscInternal')" name="v_DiscInternal" id="v_DiscInternal" value="<?php echo format_number($arr_curr["DiscInternal"],2); ?>">
                        </td>
                    </tr>
                    
                    <tr>
                <td class="title_table" width="200">Nomor Rekening</td>
                <td>
                     <input type="text" placeholder="Nama Rekening" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaRekening" id="v_NamaRekening" maxlength="50" value="<?php echo $arr_curr["KdRekeningPersediaan"]; ?>">
                     
                     <button type="button" name="btn_check" id="btn_check" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check_rekening()" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
                 
                 
                 <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_Konsinyasi" id="v_Konsinyasi" style="width: 200px;">
		            		<option <?php if($arr_curr["Konsinyasi"]==0){ echo "selected='selected'"; } ?> value="0">Non Konsinyasi</option>
		            		<option <?php if($arr_curr["Konsinyasi"]==1){ echo "selected='selected'"; } ?> value="1">Konsinyasi</option>
		            	</select>
		            </td>
		        </tr>
                    
                      
                    
                    
                    <?php
                }
            ?>
                
               
            <tr> 
                <td>&nbsp;</td>                   
                <td>
                    
                    <?php 
                        if($arr_curr["Approval_Status"]==1 && ( $ses_login=="william1001" || $ses_login=="trisno1402" || $ses_login=="mechael0101" || $ses_login=="tonny1205"))
                        {   
                            ?>
                            <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                            <?php
                        }
                        else
                        {
                               echo "<font color='red'>Button Save tidak muncul karena sudah ada approval</font>";
                        }
                    ?>
                    
                    
                    
                    <?php 
                        if($arr_curr["Approval_Status"]==0)
                        {
                    ?>
                    <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                    <button type="submit" name="btn_delete" id="btn_delete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_delete('<?php echo $arr_curr["PCode"]; ?>');" value="Hapus">Hapus<i class="entypo-trash"></i></button>
                    <?php 
                        }
                    ?>
                    
                    <?php 
                        if($arr_curr["Approval_Status"]==0)
                        {
                            // sari
                            if($ses_login=="william1001" || $ses_login=="trisno1402" || $ses_login=="tonny1205")
                            {
                                ?>
                                <span style="float: right;">
                                <button type="submit" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $arr_curr["PCode"]; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
                                
                                <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
                                
                                <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
                                <button style="display: none;" type="submit" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $arr_curr["PCode"]; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
                                </span> 
                                <?php
                            }
                            else
                            {
                                echo "<span style='float: right; color: blue;'>Approval Waiting Kadek Sariasih</span>";    
                            }
                        }
                        ?>
               </td>
            </tr>
            
            
            <tr>
                <td class="title_table" colspan="2">INFORMASI DATA</td>      
            </tr> 
            
            <tr>
                <td class="title_table">Dibuat</td>      
                <td><?php echo format_show_datetime($arr_curr["AddDate"])." :: ".$arr_curr["AddUser"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Diedit</td>      
                <td><?php echo format_show_datetime($arr_curr["EditDate"])." :: ".$arr_curr["EditUser"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Approval</td>      
                <td><?php echo $approval; ?></td>
            </tr>
            
            
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    else if($ajax=="ajax_divisi")
    {
        $v_KdDivisi = $_GET["v_KdDivisi"];
        
        $q = "
                SELECT
                    ".$db["master"].".subdivisi.KdSubDivisi,
                    ".$db["master"].".subdivisi.NamaSubDivisi
                FROM
                    ".$db["master"].".subdivisi
                WHERE
                    1
                    AND ".$db["master"].".subdivisi.KdDivisi = '".$v_KdDivisi."'
                ORDER BY
                    ".$db["master"].".subdivisi.NamaSubDivisi ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdSubDivisi, $NamaSubDivisi) = $row;    
            
            $arr_data["list_subdivisi"][$KdSubDivisi] = $KdSubDivisi;
            $arr_data["NamaSubDivisi"][$KdSubDivisi] = $NamaSubDivisi;
        }
        
        ?>
            <select name="v_KdSubDivisi" id="v_KdSubDivisi" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_subdivisi', this.value)">  
                <?php 
                    foreach($arr_data["list_subdivisi"] as $KdSubDivisi=>$val)
                    {
                        $NamaSubDivisi = $arr_data["NamaSubDivisi"][$KdSubDivisi];
                ?>
                <option value="<?php echo $KdSubDivisi; ?>"><?php echo $NamaSubDivisi; ?></option>
                <?php 
                    }
                ?>
            </select>
        <?php
        
    }
    
    else if($ajax=="ajax_kategori")
    {
        $v_KdKategori = $_GET["v_KdKategori"];
        
        $q = "
                SELECT
                    ".$db["master"].".subkategori.KdSubKategori,
                    ".$db["master"].".subkategori.NamaSubKategori
                FROM
                    ".$db["master"].".subkategori
                WHERE
                    1
                    AND ".$db["master"].".subkategori.KdKategori = '".$v_KdKategori."'
                ORDER BY
                    ".$db["master"].".subkategori.NamaSubKategori ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdSubKategori, $NamaSubKategori) = $row;    
            
            $arr_data["list_subkategori"][$KdSubKategori] = $KdSubKategori;
            $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
        }
        
        ?>
            <select name="v_KdSubKategori" id="v_KdSubKategori" class="form-control-new" style="width: 200px;">  
                <?php 
                    foreach($arr_data["list_subkategori"] as $KdSubKategori=>$val)
                    {
                        $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
                ?>
                <option value="<?php echo $KdSubKategori; ?>"><?php echo $NamaSubKategori; ?></option>
                <?php 
                    }
                ?>
            </select>
        <?php
        
    }
    
    else if($ajax=="ajax_subdivisi")
    {
        $v_KdSubDivisi = $_GET["v_KdSubDivisi"];
        
        if($v_KdSubDivisi==13 || $v_KdSubDivisi==19 || $v_KdSubDivisi==20)
        {
            // resto
            if($v_KdSubDivisi==19 || $v_KdSubDivisi==20)
            {
                $q = "
                        SELECT
                            ".$db["master"].".subkategoripos.KdSubKategori,
                            ".$db["master"].".subkategoripos.NamaSubKategori
                        FROM
                            ".$db["master"].".subkategoripos
                        WHERE
                            1
                            AND LEFT(".$db["master"].".subkategoripos.KdSubKategori,3) = '010'
                        ORDER BY                                                  
                            ".$db["master"].".subkategoripos.KdSubKategori ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($KdSubKategori, $NamaSubKategori) = $row;    
                    
                    $arr_data["list_subkategori"][$KdSubKategori] = $KdSubKategori;
                    $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
                } 
                if($v_KdSubDivisi==19){
                ?>	
                <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori_pos', this.value)">  
                    <option value="010">Foods</option>
                    <option value="011">Beverages</option>
                </select>
                <?php
                }else{
                ?>	
				<select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori_pos', this.value)">  
                    <option value="020">Foods</option>
                    <option value="021">Beverages</option>
                </select>
                <?php
				}
				?>
                <span id="td_KdSubKategori">
                <select name="v_KdSubKategori" id="v_KdSubKategori" class="form-control-new" style="width: 200px;">  
                    <?php 
                        foreach($arr_data["list_subkategori"] as $KdSubKategori=>$val)
                        {
                            $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
                    ?>
                    <option value="<?php echo $KdSubKategori; ?>"><?php echo $NamaSubKategori; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                </span>  
                <?php
            }  
            else if($v_KdSubDivisi==13)
            {
                $q = "
                        SELECT
                            ".$db["master"].".subkategoripos.KdSubKategori,
                            ".$db["master"].".subkategoripos.NamaSubKategori
                        FROM
                            ".$db["master"].".subkategoripos
                        WHERE
                            1
                            AND LEFT(".$db["master"].".subkategoripos.KdSubKategori,3) = '031'
                        ORDER BY                                                  
                            ".$db["master"].".subkategoripos.KdSubKategori ASC
                ";
                $qry = mysql_query($q);
                while($row = mysql_fetch_array($qry))
                {
                    list($KdSubKategori, $NamaSubKategori) = $row;    
                    
                    $arr_data["list_subkategori"][$KdSubKategori] = $KdSubKategori;
                    $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
                } 
                
                ?>
                <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori_pos', this.value)">  
                    <option value="031">Black Eye</option>
                </select>
                
                <span id="td_KdSubKategori">
                <select name="v_KdSubKategori" id="v_KdSubKategori" class="form-control-new" style="width: 200px;">  
                    <?php 
                        foreach($arr_data["list_subkategori"] as $KdSubKategori=>$val)
                        {
                            $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
                    ?>
                    <option value="<?php echo $KdSubKategori; ?>"><?php echo $NamaSubKategori; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                </span>  
                <?php
            }   
        }
        else
        {
             $q = "
                    SELECT
                        ".$db["master"].".kategori.KdKategori,
                        ".$db["master"].".kategori.NamaKategori
                    FROM
                        ".$db["master"].".kategori
                    WHERE
                        1
                    ORDER BY
                        ".$db["master"].".kategori.KdKategori ASC
                ";
                $qry_kategori = mysql_query($q);
                while($row_kategori = mysql_fetch_array($qry_kategori))
                { 
                    list($KdKategori, $NamaKategori) = $row_kategori;    
                    
                    $arr_data["list_kategori"][$KdKategori] = $KdKategori;
                    $arr_data["NamaKategori"][$KdKategori] = $NamaKategori;
                    
                } 
            
            ?>
            <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori', this.value)">  
                <option value="">-</option>
                <?php 
                    foreach($arr_data["list_kategori"] as $KdKategori=>$val)
                    {
                        $NamaKategori = $arr_data["NamaKategori"][$KdKategori];
                        ?>
                        <option value="<?php echo $KdKategori; ?>"><?php echo $NamaKategori; ?></option>
                        <?php
                    }
                ?>
            </select>
            
            <span id="td_KdSubKategori"></span> 
            <?php
        }
    }
    
    else if($ajax=="ajax_kategori_pos")
    {
        $v_KdKategori = $_GET["v_KdKategori"];
        
        $q = "
                 SELECT
                    ".$db["master"].".subkategoripos.KdSubKategori,
                    ".$db["master"].".subkategoripos.NamaSubKategori
                FROM
                    ".$db["master"].".subkategoripos
                WHERE
                    1
                    AND LEFT(".$db["master"].".subkategoripos.KdSubKategori,3) = '".$v_KdKategori."'
                ORDER BY                                                  
                    ".$db["master"].".subkategoripos.NamaSubKategori ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdSubKategori, $NamaSubKategori) = $row;    
            
            $arr_data["list_subkategori"][$KdSubKategori] = $KdSubKategori;
            $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
        }
        
        ?>
            <select name="v_KdSubKategori" id="v_KdSubKategori" class="form-control-new" style="width: 200px;">  
                <?php 
                    foreach($arr_data["list_subkategori"] as $KdSubKategori=>$val)
                    {
                        $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
                ?>
                <option value="<?php echo $KdSubKategori; ?>"><?php echo $NamaSubKategori; ?></option>
                <?php 
                    }
                ?>
            </select>
        <?php    
    }
    
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                
                if(!isset($_POST["v_PCode"])){ $v_PCode = isset($_POST["v_PCode"]); } else { $v_PCode = $_POST["v_PCode"]; }                                                                                                                                        
                if(!isset($_POST["v_NamaLengkap"])){ $v_NamaLengkap = isset($_POST["v_NamaLengkap"]); } else { $v_NamaLengkap = $_POST["v_NamaLengkap"]; }     
                                if(!isset($_POST["v_NamaRekening"])){ $v_NamaRekening = isset($_POST["v_NamaRekening"]); } else { $v_NamaRekening = $_POST["v_NamaRekening"]; }
                if(!isset($_POST["v_Barcode1"])){ $v_Barcode1 = isset($_POST["v_Barcode1"]); } else { $v_Barcode1 = $_POST["v_Barcode1"]; }     
                if(!isset($_POST["v_KdDivisi"])){ $v_KdDivisi = isset($_POST["v_KdDivisi"]); } else { $v_KdDivisi = $_POST["v_KdDivisi"]; } 
                if(!isset($_POST["v_KdSubDivisi"])){ $v_KdSubDivisi = isset($_POST["v_KdSubDivisi"]); } else { $v_KdSubDivisi = $_POST["v_KdSubDivisi"]; }          
                if(!isset($_POST["v_KdKategori"])){ $v_KdKategori = isset($_POST["v_KdKategori"]); } else { $v_KdKategori = $_POST["v_KdKategori"]; }            
                if(!isset($_POST["v_KdSubKategori"])){ $v_KdSubKategori = isset($_POST["v_KdSubKategori"]); } else { $v_KdSubKategori = $_POST["v_KdSubKategori"]; } 
                if(!isset($_POST["v_SatuanSt"])){ $v_SatuanSt = isset($_POST["v_SatuanSt"]); } else { $v_SatuanSt = $_POST["v_SatuanSt"]; }   

                $v_PCode         = save_char($v_PCode);
                $v_NamaLengkap   = strtoupper(save_char($v_NamaLengkap));
                $v_Barcode1      = strtoupper(save_char($v_Barcode1));
                $v_KdDivisi      = save_char($v_KdDivisi);
                $v_KdSubDivisi   = save_int($v_KdSubDivisi);
                $v_KdKategori    = save_char($v_KdKategori);
                $v_KdSubKategori = save_char($v_KdSubKategori);
                $v_SatuanSt      = save_char($v_SatuanSt);
                
                if($v_NamaLengkap=="")
                {
                    $msg = "Nama Barang harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_KdSubDivisi=="")
                {
                    $msg = "Divisi harus dipilih...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_KdSubKategori=="")
                {
                    $msg = "Kategori harus dipilih...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_SatuanSt=="")
                {
                    $msg = "Satuan harus dipilih...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                    
                // cek duplikat nama
                $q = "
                        SELECT
                            ".$db["master"].".masterbarang.PCode
                        FROM
                            ".$db["master"].".masterbarang
                        WHERE
                            1
                            AND ".$db["master"].".masterbarang.NamaLengkap = '".$v_NamaLengkap."'
                        LIMIT
                            0,1
                ";
                $qry = mysql_query($q);
                $row = mysql_fetch_array($qry);
                list($PCode_exists) = $row;
                
                if($PCode_exists)
                {
                    $msg = "Nama Barang ".$v_NamaLengkap." sudah ada dengan PCode ".$PCode_exists;
                    echo "<script>alert('".$msg."');</script>";
                    die();      
                }
                
                $PCode = get_code_PCode($v_KdSubDivisi, $v_KdKategori, $v_KdSubKategori);
                
                // insert masterbarang
                {
                    $q = "
                        INSERT INTO
                            ".$db["master"].".masterbarang
                        SET
                              `PCode` = '".$PCode."',
                              `NamaLengkap` = '".$v_NamaLengkap."',
                              `NamaStruk` = '".$v_NamaLengkap."',
                              `NamaInitial` = '".$v_NamaLengkap."',
                              `Barcode1` = '".$PCode."',
                              `SatuanSt` = '".$v_SatuanSt."',
                              `KdDivisi` = '".$v_KdDivisi."',
                              `KdSubDivisi` = '".$v_KdSubDivisi."',
                              `KdKategori` = '".$v_KdKategori."',
                              `KdSubKategori` = '".$v_KdSubKategori."',
                              `PersenPajak` = '0.00',
                              `Tipe` = '',
                              `Status` = 'T',
                              `AddDate` = NOW(),
                              `EditDate` = NOW(),
                              `Service_charge` = '0',
                              `JenisPajak` = 'PPN',
                              `HPP` = '0',
                              `StatusPajak` = '0',
                              `FlagReady` = 'Y',
                              `FlagStock` = 'Y',
                              `Printer` = '01',
                              `Jenis` = '1',
                              `komisi` = '0.00',
                              `DiscInternal` = '0.00',
                              AddUser = '".$ses_login."',
                              EditUser = '".$ses_login."'
                    ";   
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan masterbarang";
                        echo "<script>alert('".$msg."');</script>";
                        die(); 
                    }    
                }
                
                // insert masterbarang_touch
                {
                    if(
                    	$v_KdSubDivisi==20 ||
                        $v_KdSubDivisi==19 || 
                        $v_KdSubDivisi==13 ||
                        $v_KdSubDivisi==24 ||
                        $v_KdSubDivisi=="01" || 
                        $v_KdSubDivisi=="02" || 
                        $v_KdSubDivisi=="03" || 
                        $v_KdSubDivisi=="04" || 
                        $v_KdSubDivisi=="05" || 
                        $v_KdSubDivisi=="06" || 
                        $v_KdSubDivisi=="07" || 
                        $v_KdSubDivisi=="08" || 
                        $v_KdSubDivisi=="09" ||
                        $v_KdSubDivisi=="10"
                    )
                    {
                           $q = "
                                INSERT INTO ".$db["master"].".masterbarang_touch
                                SET `PCode` = '".$PCode."',
                                  `NamaLengkap` = '".$v_NamaLengkap."',
                                  `Harga1c` = '0.00',
                                  `Barcode1` = '".$PCode."',
                                  `Service_charge` = '0',
                                  `Komisi` = '0',
                                  `Jenis` = '2',
                                  `RupBar` = '',
                                  `Perhitungan` = '',
                                  `Period1` = '',
                                  `Period2` = '',
                                  `Beban` = '',
                                  `NoRekening` = '',
                                  `Nilai` = '',
                                  `HadiahBarang` = '',
                                  `Opr1` = '',
                                  `Nilai1` = '',
                                  `Opr2` = '',
                                  `Nilai2` = '',
                                  `Campur` = '',
                                  `PPN` = '0',
                                  `HPP` = '',
                                  `KdKategori` = '".$v_KdKategori."',
                                  `KdSubKategori` = '".$v_KdSubKategori."',
                                  `Satuan1` = '".$v_SatuanSt."',
                                  `FlagReady` = 'Y',
                                  `FlagStock` = 'Y',
                                  `Printer` = '01',
                                  `DiscInternal` = '0.00' 
                           "; 
                           if(!mysql_query($q))
                           {
                                $msg = "Gagal menyimpan masterbarang_touch";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                           }      
                           
                           $q = "
                                INSERT INTO ".$db["master"].".masterbarang_pos
                                SET 
                                  `PCode` = '".$PCode."',
                                  `NamaLengkap` = '".$v_NamaLengkap."',
                                  `Harga1c` = '0.00',
                                  `Barcode1` = '".$PCode."',
                                  `Service_charge` = '0',
                                  `Komisi` = '',
                                  `Jenis` = '',
                                  `RupBar` = '',
                                  `Perhitungan` = '',
                                  `Period1` = '',
                                  `Period2` = '',
                                  `Beban` = '',
                                  `NoRekening` = '',
                                  `Nilai` = '',
                                  `HadiahBarang` = '',
                                  `Opr1` = '',
                                  `Nilai1` = '',
                                  `Opr2` = '',
                                  `Nilai2` = '',
                                  `Campur` = '',
                                  `PPN` = '0',
                                  `HPP` = '0',
                                  `KdKategori` = '".$v_KdKategori."',
                                  `KdSubKategori` = '".$v_KdSubKategori."',
                                  `Satuan1` = '".$v_SatuanSt."',
                                  `DiscInternal` = '0.00'  
                           "; 
                           if(!mysql_query($q))
                           {
                                $msg = "Gagal menyimpan masterbarang_pos";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                           }  
                    }
                }
                
                $msg = "Berhasil menyimpan ".$v_NamaLengkap;
                echo "<script>alert('".$msg."');</script>"; 
                echo "<script>parent.CallAjaxForm('search','".$PCode."');</script>";             
                
                die();
                   
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {                                                                                                      
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }    
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }   
                if(!isset($_POST["v_PCode"])){ $v_PCode = isset($_POST["v_PCode"]); } else { $v_PCode = $_POST["v_PCode"]; }  
                
                if(!isset($_POST["v_NamaLengkap"])){ $v_NamaLengkap = isset($_POST["v_NamaLengkap"]); } else { $v_NamaLengkap = $_POST["v_NamaLengkap"]; }             if(!isset($_POST["v_NamaRekening"])){ $v_NamaRekening = isset($_POST["v_NamaRekening"]); } else { $v_NamaRekening = $_POST["v_NamaRekening"]; } 
                if(!isset($_POST["v_Konsinyasi"])){ $v_Konsinyasi = isset($_POST["v_Konsinyasi"]); } else { $v_Konsinyasi = $_POST["v_Konsinyasi"]; } 
                if(!isset($_POST["v_Barcode1"])){ $v_Barcode1 = isset($_POST["v_Barcode1"]); } else { $v_Barcode1 = $_POST["v_Barcode1"]; }     
                if(!isset($_POST["v_SatuanSt"])){ $v_SatuanSt = isset($_POST["v_SatuanSt"]); } else { $v_SatuanSt = $_POST["v_SatuanSt"]; }   
                
                if(!isset($_POST["v_Harga1c"])){ $v_Harga1c = isset($_POST["v_Harga1c"]); } else { $v_Harga1c = $_POST["v_Harga1c"]; }   
                if(!isset($_POST["v_komisi"])){ $v_komisi = isset($_POST["v_komisi"]); } else { $v_komisi = $_POST["v_komisi"]; }   
                if(!isset($_POST["v_DiscInternal"])){ $v_DiscInternal = isset($_POST["v_DiscInternal"]); } else { $v_DiscInternal = $_POST["v_DiscInternal"]; }   
                if(!isset($_POST["v_PPN"])){ $v_PPN = isset($_POST["v_PPN"]); } else { $v_PPN = $_POST["v_PPN"]; }   
                if(!isset($_POST["v_Service_charge"])){ $v_Service_charge = isset($_POST["v_Service_charge"]); } else { $v_Service_charge = $_POST["v_Service_charge"]; }   

                $v_PCode         = save_char($v_PCode);
                $v_NamaLengkap   = strtoupper(save_char($v_NamaLengkap));
                $v_Barcode1      = strtoupper(save_char($v_Barcode1));
                $v_SatuanSt      = save_char($v_SatuanSt); 
                $v_Approval_Remarks = save_char($v_Approval_Remarks); 
                
                $v_Harga1c = save_int($v_Harga1c); 
                $v_komisi = save_int($v_komisi); 
                $v_DiscInternal = save_int($v_DiscInternal); 
                $v_PPN = save_int($v_PPN); 
                $v_Service_charge = save_int($v_Service_charge);
                $v_NamaRekening   = strtoupper(save_char($v_NamaRekening));
                $v_Konsinyasi	= strtoupper(save_char($v_Konsinyasi)); 
                
                    
                if($v_NamaLengkap=="")
                {
                    $msg = "Nama Barang harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_SatuanSt=="")
                {
                    $msg = "Satuan harus dipilih...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                
                
                if($v_del==1)   
                {
                    if($btn_delete=="Hapus")
                    {
                        // cek PB
                        $q = "
                                SELECT
                                    ".$db["master"].".permintaan_barang_detail.PCode
                                FROM
                                    ".$db["master"].".permintaan_barang_detail
                                WHERE
                                    1
                                    AND ".$db["master"].".permintaan_barang_detail.PCode = '".$v_PCode."'
                                LIMIT
                                    0,1
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($PCode_PB) = $row;
                        if($PCode_PB)
                        {
                            $msg = $PCode_PB." Tidak bisa dihapus karena sudah ada Permintaan Barang";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        // cek transaksi_detail
                        $q = "
                                SELECT
                                    ".$db["master"].".transaksi_detail.PCode
                                FROM
                                    ".$db["master"].".transaksi_detail
                                WHERE
                                    1
                                    AND ".$db["master"].".transaksi_detail.PCode = '".$v_PCode."'
                                LIMIT
                                    0,1
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($PCode_Trans) = $row;
                        if($PCode_Trans)
                        {
                            $msg = $PCode_Trans." Tidak bisa dihapus karena sudah ada Transaksi";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".masterbarang
                            WHERE 1
                                AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan masterbarang";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".masterbarang_touch
                            WHERE 1
                                AND ".$db["master"].".masterbarang_touch.PCode = '".$v_PCode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan masterbarang_touch";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".masterbarang_pos
                            WHERE 1
                                AND ".$db["master"].".masterbarang_pos.PCode = '".$v_PCode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan masterbarang_pos";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
                        $msg = "Berhasil Hapus ".$v_PCode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '');</script>";         
                        die();
                    }
                }
                else  if($v_approve==1)   
                {
                    if($btn_approve=="Approve")
                    {
                    	
                    	if($v_NamaRekening=="")
		                {
		                    $msg = "Rekening harus diisi...";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();    
		                }
                    	
                        $q = "
                            UPDATE
                                ".$db["master"].".masterbarang
                            SET     
                                Approval_By = '".$ses_login."',
                                Approval_Date = NOW(),
                                Approval_Status = '1',
                                Status='A'
                             WHERE 1
                                AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan masterbarang";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
                        $msg = "Berhasil Approve ".$v_PCode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '".$v_PCode."');</script>";         
                        die();
                    }
                }
                else  if($v_reject==1)   
                {
                    if($btn_reject=="Reject")
                    {
                        if($v_Approval_Remarks=="")
                        {
                            $msg = "Alasan Reject harus diisi...";
                            echo "<script>alert('".$msg."');</script>";
                        }
                        else
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang
                                SET     
                                    Approval_By = '".$ses_login."',
                                    Approval_Date = NOW(),
                                    Approval_Status = '2',
                                    Status='T',
                                    Approval_Remarks = '".$v_Approval_Remarks."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            } 
                                   
                            $msg = "Berhasil Reject ".$v_PCode;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjaxForm('search', '".$v_PCode."');</script>";         
                            die();
                        }
                    }
                }
                else
                {
                    if($btn_save=="Save")
                    {   
                        // update masterbarang
                        //echo "Hello";die;
                        if($v_NamaRekening=="")
		                {
		                    $msg = "Rekening harus diisi...";
		                    echo "<script>alert('".$msg."');</script>";
		                    die();    
		                }
		                
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang
                                SET     
                                    NamaLengkap = '".$v_NamaLengkap."',
                                    Barcode1 = '".$v_Barcode1."',
                                    SatuanSt = '".$v_SatuanSt."',
                                    EditDate = NOW(),
                                    EditUser = '".$ses_login."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang_touch
                                SET     
                                    NamaLengkap = '".$v_NamaLengkap."',
                                    Barcode1 = '".$v_Barcode1."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang_touch.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang_touch";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang_pos
                                SET     
                                    NamaLengkap = '".$v_NamaLengkap."',
                                    Barcode1 = '".$v_Barcode1."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang_pos.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang_pos";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                        }
                        
                        // update harga
                        // sari
                        if($ses_login=="william1001" || $ses_login=="trisno1402" || $ses_login=="mart2005" || $ses_login=="tonny1205" || $ses_login=="mechael0101") 
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang
                                SET     
                                    Harga1c = '".$v_Harga1c."',
                                    komisi = '".$v_komisi."',
									KomisiLokal = '".$v_komisi."',
                                    DiscInternal = '".$v_DiscInternal."',
                                    PPN = '".$v_PPN."',
                                    Service_charge = '".$v_Service_charge."',
                                    EditDate = NOW(),
                                    EditUser = '".$ses_login."',
                                    KdRekeningPersediaan=SUBSTR('".$v_NamaRekening."',1,8),
                                    Konsinyasi='".$v_Konsinyasi."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>"; die;                 
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang harga";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }    
                            
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang_touch
                                SET     
                                    Harga1c = '".$v_Harga1c."',
                                    Komisi = '".$v_komisi."',
									KomisiLokal = '".$v_komisi."',
                                    DiscInternal = '".$v_DiscInternal."',
                                    PPN = '".$v_PPN."',
                                    Service_charge = '".$v_Service_charge."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang_touch.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang_touch harga";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }    
                            
                            $q = "
                                UPDATE
                                    ".$db["master"].".masterbarang_pos
                                SET     
                                    Harga1c = '".$v_Harga1c."',
                                    Komisi = '".$v_komisi."',
									KomisiLokal = '".$v_komisi."',
                                    DiscInternal = '".$v_DiscInternal."',
                                    PPN = '".$v_PPN."',
                                    Service_charge = '".$v_Service_charge."'
                                 WHERE 1
                                    AND ".$db["master"].".masterbarang_pos.PCode = '".$v_PCode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan masterbarang_pos harga";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }    
                        }
                       
                        $msg = "Berhasil menyimpan ".$v_PCode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search','".$v_PCode."');</script>";         
                        die();
                    } 
                }  
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  