<?php 
include("header.php");

if(!isset($_GET["v_NoDokumen"])){ $v_NoDokumen = isset($_GET["v_NoDokumen"]); } else { $v_NoDokumen = $_GET["v_NoDokumen"]; }

if($v_NoDokumen=="")
{
    echo "No Dokumen Tidak ditemukan";
    die();
}

$file_name = "stock-opname-".$v_NoDokumen.".xls";
header("Content-Disposition".": "."attachment;filename=$file_name");
header("Content-type: application/vnd.ms-excel");

$q = "
        SELECT
            *
        FROM 
            ".$db["master"].".opname_header
        WHERE
            1
            AND ".$db["master"].".opname_header.NoDokumen = '".$v_NoDokumen."'
        LIMIT
            0,1
";     
$qry =  mysql_query($q);
$arr_curr = mysql_fetch_array($qry);

$q = "
    SELECT
        ".$db["master"].".gudang.KdGudang,
        ".$db["master"].".gudang.Keterangan
    FROM
        ".$db["master"].".gudang
    WHERE
        1
    ORDER BY
        ".$db["master"].".gudang.KdGudang ASC
";
$qry_gudang = mysql_query($q);
while($row_gudang = mysql_fetch_array($qry_gudang))
{ 
    list($KdGudang, $NamaGudang) = $row_gudang;    
    
    $arr_data["list_gudang"][$KdGudang] = $KdGudang;
    $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
    
}

$ada_data = false;
$q = "
        SELECT
            ".$db["master"].".opname_detail.Sid,    
            ".$db["master"].".opname_detail.PCode,
            ".$db["master"].".opname_detail.QtyFisik,
            ".$db["master"].".opname_detail.QtyProgram
        FROM
            ".$db["master"].".opname_detail
        WHERE
            1
            AND ".$db["master"].".opname_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
        ORDER BY
            ".$db["master"].".opname_detail.Sid ASC
";
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{ 
    list(
        $Sid,    
        $PCode,
        $QtyFisik,
        $QtyProgram
    ) = $row;    
    
    $arr_data["data_QtyFisik"][$PCode] = $QtyFisik;
    $arr_data["data_QtyProgram"][$PCode] = $QtyProgram;
    
    $ada_data = true;
}


$q = "
        SELECT
            ".$db["master"].".masterbarang.PCode,    
            ".$db["master"].".masterbarang.NamaLengkap,
            ".$db["master"].".masterbarang.SatuanSt,
            tbl_SubKategori.KdSubKategori,
            tbl_SubKategori.NamaSubKategori
        FROM
            ".$db["master"].".masterbarang
            INNER JOIN 
            (
                SELECT
                    tbl_uni.KdSubKategori,
                    tbl_uni.NamaSubKategori
                FROM
                (
                SELECT
                    `subkategori`.`KdSubKategori`,
                    `subkategori`.`NamaSubKategori`
                FROM
                    `subkategori`

                UNION ALL

                SELECT
                    `subkategoripos`.`KdSubKategori`,
                    `subkategoripos`.`NamaSubKategori`
                FROM
                    `subkategoripos`
                ) AS tbl_uni
                WHERE
                    1
                ORDER BY
                    tbl_uni.KdSubKategori ASC     
            ) AS tbl_SubKategori ON
                ".$db["master"].".masterbarang.KdSubKategori = tbl_SubKategori.KdSubKategori
            INNER JOIN ".$db["master"].".gudang_subkategori ON
                tbl_SubKategori.KdSubKategori = ".$db["master"].".gudang_subkategori.KdSubKategori
                AND ".$db["master"].".gudang_subkategori.KdGudang = '".$arr_curr["KdGudang"]."'
        WHERE
            1
        ORDER BY
            tbl_SubKategori.KdSubKategori ASC,
            ".$db["master"].".masterbarang.NamaLengkap ASC
";
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{ 
    list(
        $PCode,
        $NamaLengkap,
        $Satuan1,
        $KdSubKategori,
        $NamaSubKategori
    ) = $row;    
    
    $arr_data["SubKategori_PCode"][$KdSubKategori][$PCode] = $PCode;
    $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
    $arr_data["satuan"][$PCode] = $Satuan1;
    
    $arr_data["list_KdSubKategori"][$KdSubKategori] = $KdSubKategori;
    $arr_data["NamaSubKategori"][$KdSubKategori] = $NamaSubKategori;
    
    $arr_data["list_pcode"][$PCode] = $PCode;
}

$arr_stock = get_stock($arr_curr["KdGudang"], $arr_curr["TglDokumen"], $arr_data["list_pcode"]); 

?>
    <table border="1">
        <tr>
            <td colspan="6" style="font-weight: bold;">PT. NATURA PESONA MANDIRI</td>
        </tr>
        
        <tr>
            <td colspan="6" style="font-weight: bold;">STOCK OPNAME <?php echo $NamaGudang; ?></td>
        </tr>
        
        <tr>
            <td colspan="6" style="font-weight: bold;">Tanggal <?php echo format_show_date($arr_curr["TglDokumen"]); ?></td>
        </tr>
        
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        
        <tr style="font-weight: bold;">
            <td>No</td>
            <td>PCode</td>
            <td>Nama Barang</td>
            <td align="right">Qty Fisik</td>
            <td align="left">Satuan</td>
            <td align="right">Qty Program</td>
            <td align="right">Selisih</td>
        </tr>
        
        <?php
                             
        foreach($arr_data["list_KdSubKategori"] as $KdSubKategori=>$val)
        {
            $NamaSubKategori = $arr_data["NamaSubKategori"][$KdSubKategori];
            
                ?>
                <tr>
                    <td colspan="7" style="font-weight: bold;"><?php echo $NamaSubKategori; ?></td>
                </tr>
                <?php
                $no = 1;
                foreach($arr_data["SubKategori_PCode"][$KdSubKategori] as $PCode=>$val)
                {
                    $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                    $satuan = $arr_data["satuan"][$PCode];
                                        
                    $QtyFisik = $arr_data["data_QtyFisik"][$PCode];
                    $QtyProgram = $arr_data["data_QtyProgram"][$PCode];

                    if(!$ada_data)
                    {
                        $QtyProgram = $arr_stock["akhir"][$PCode];
                    }
                    
                    $selisih = $QtyFisik - $QtyProgram;
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>    
                        <td style="mso-number-format: '@'"><?php echo $PCode; ?></td>    
                        <td><?php echo $NamaLengkap; ?></td>    
                        <td align="right"><?php echo format_number($QtyFisik, 2, ",", ".", "ind"); ?></td>    
                        <td><?php echo $satuan; ?></td>    
                        <td align="right"><?php echo format_number($QtyProgram, 2, ",", ".", "ind"); ?></td>    
                        <td align="right"><?php echo format_number($selisih, 2,",", ".", "ind"); ?></td>    
                    </tr> 
                    <?php 
                    $no++;  
                    
                    $arr_total["div_QtyFisik"][$KdSubKategori] += $QtyFisik; 
                    $arr_total["div_QtyProgram"][$KdSubKategori] += $QtyProgram;
                    $arr_total["div_Selisih"][$KdSubKategori] += $selisih;
                    
                    $arr_total["all_QtyFisik"] += $QtyFisik; 
                    $arr_total["all_QtyProgram"] += $QtyProgram;
                    $arr_total["all_Selisih"] += $selisih;
                }
            
            ?>
            <tr style="text-align: right; font-weight: bold;">
                <td colspan="3">Total <?php echo $NamaSubKategori; ?></td>
                <td><?php echo format_number($arr_total["div_QtyFisik"][$KdSubKategori], 2,",", ".", "ind"); ?></td>
                <td>&nbsp;</td>
                <td><?php echo format_number($arr_total["div_QtyProgram"][$KdSubKategori], 2,",", ".", "ind"); ?></td>
                <td><?php echo format_number($arr_total["div_Selisih"][$KdSubKategori], 2,",", ".", "ind"); ?></td>
            </tr>
            <?php
            
        }
        
    ?>    
    
    <tr style="text-align: right; font-weight: bold;">
        <td colspan="3">GRAND TOTAL</td>
        <td><?php echo format_number($arr_total["all_QtyFisik"],2,",", ".", "ind"); ?></td>
        <td>&nbsp;</td>
        <td><?php echo format_number($arr_total["all_QtyProgram"],2,",", ".", "ind"); ?></td>
        <td><?php echo format_number($arr_total["all_Selisih"],2,",", ".", "ind"); ?></td>
    </tr>   
    </table>
<?php
   

mysql_close();
?>