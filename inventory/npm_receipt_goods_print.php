<?php
    include("header.php");
    
    
    function create_txt_report($paths,$filed,$str)
    {
        $strs=chr(27).chr(64).chr(27).chr(67).chr(33).$str;
         $txt=$paths."/".$filed;
        if(file_exists($txt)){
            unlink($txt);
        }
        $open_it=fopen($txt,"a+");
        fwrite($open_it,$strs);
        fclose($open_it);    
    } 
    
    function sintak_epson()
    {
        $arr_data["escNewLine"]      = chr(10);  // New line (LF line feed)
        $arr_data["escUnerlineOn"]   = chr(27).chr(45).chr(1);  // Unerline On
        $arr_data["escUnerlineOnx2"] = chr(27).chr(45).chr(2);  // Unerline On x 2
        $arr_data["escUnerlineOff"]  = chr(27).chr(45).chr(0);  // Unerline Off
        $arr_data["escBoldOn"]       = chr(27).chr(69).chr(1);  // Bold On
        $arr_data["escBoldOff"]      = chr(27).chr(69).chr(0);  // Bold Off
        $arr_data["escNegativeOn"]   = chr(29).chr(66).chr(1);  // White On Black On'
        $arr_data["escNegativeOff"]  = chr(29).chr(66).chr(0);  // White On Black Off
        $arr_data["esc8CpiOn"]       = chr(29).chr(33).chr(16); // Font Size x2 On
        $arr_data["esc8CpiOff"]      = chr(29).chr(33).chr(0);  // Font Size x2 Off
        $arr_data["esc16Cpi"]        = chr(27).chr(77).chr(48); // Font A  -  Normal Font
        $arr_data["esc20Cpi"]        = chr(27).chr(77).chr(49); // Font B - Small Font
        $arr_data["escReset"]        = chr(27).chr(64); //chr(27) + chr(77) + chr(48); // Reset Printer
        $arr_data["escFeedAndCut"]   = chr(29).chr(86).chr(65); // Partial Cut and feed

        $arr_data["escAlignLeft"]    = chr(27).chr(97).chr(48); // Align Text to the Left
        $arr_data["escAlignCenter"]  = chr(27).chr(97).chr(49); // Align Text to the Center
        $arr_data["escAlignRight"]   = chr(27).chr(97).chr(50); // Align Text to the Right
        
        $arr_data["reset"]  =chr(27).'@'; //reset semua pengaturan printer
        $arr_data["plength"]=chr(27).'C'; //tinggi kertas, contoh $plength.chr(33) ==> tinggi 33 baris.
        $arr_data["lmargin"]=chr(27).'l'; //margin kiri, pemakaian sama dengan $plength.
        $arr_data["cond"]   =chr(15);   //condensed
        $arr_data["ncond"]  =chr(18);   //end condensed
        $arr_data["dwidth"] =chr(27).'!'.chr(16);  //tulisan melebar
        $arr_data["ndwidth"]=chr(27).'!'.chr(1);   //end tulisan melebar
        $arr_data["draft"]  =chr(27).'x'.chr(48);  //font draft
        $arr_data["nlq"]    =chr(27).'x'.chr(49);
        $arr_data["bold"]   =chr(27).'E';   //tulisan bold
        $arr_data["nbold"]  =chr(27).'F';   //end tulisan bold
        $arr_data["uline"]  =chr(27).'!'.chr(129); //garis bawah
        $arr_data["nuline"] =chr(27).'!'.chr(1); //end garis bawah
        $arr_data["dstrik"] =chr(27).'G';    //double strike (tulisan lebih tebal, 2 kali strike)
        $arr_data["ndstrik"]=chr(27).'H';    //end double strike (tulisan lebih tebal, 2 kali strike)
        $arr_data["elite"]  =chr(27).'M';    //tulisan elite
        $arr_data["pica"]   =chr(27).'P';    //tulisan pica
        $arr_data["height"] =chr(27).'!'.chr(16); //tulisan tinggi
        $arr_data["nheight"]=chr(27).'!'.chr(1);  //end tulisan tinggi
        $arr_data["spasi05"]=chr(27)."3".chr(16); //spasi 5 char
        $arr_data["spasi1"] =chr(27)."3".chr(24); //spasi 1 char
        $arr_data["fcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';    // potong kertas full
        $arr_data["pcut"]   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';    // potong kertas sebagian
        $arr_data["op_cash"]=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);   //buka cash register
                    
        return $arr_data;                           
    } 
    
    if(!isset($_POST["v_data"])){ $v_data = isset($_POST["v_data"]); } else { $v_data = $_POST["v_data"]; }
    
    if($v_data=="")
    {
        $msg = "Data harus dipilih";
        echo "<script>alert('".$msg."');</script>"; 
        die();
    } 
    
    $arr_epson = sintak_epson();
    
    $total_spasi = 135;
    $total_spasi_header = 85;
    
    $jml_detail  = 8;
    $ourFileName = "receipt-good.txt";
    $nama        = "PT. NATURA PESONA MANDIRI"; 
    
    $echo="";
    foreach($v_data as $key => $val)
    {
        $v_NoDokumen = $val;
        
        $q = "
                SELECT
                    *
                FROM 
                    ".$db["master"].".trans_terima_header
                WHERE
                    1
                    AND ".$db["master"].".trans_terima_header.NoDokumen = '".$v_NoDokumen."'
                LIMIT
                    0,1
        ";     
        $qry_curr = mysql_query($q);
        $arr_curr = mysql_fetch_array($qry_curr);
        
        $note_header = substr($arr_curr["Keterangan"],0,40);
        
        $q = "
            SELECT
                ".$db["master"].".supplier.KdSupplier,
                ".$db["master"].".supplier.Nama,
                ".$db["master"].".supplier.Contact
            FROM
                ".$db["master"].".supplier
            WHERE
                1
                AND ".$db["master"].".supplier.KdSupplier = '".$arr_curr["KdSupplier"]."'
            ORDER BY
                ".$db["master"].".supplier.Nama ASC
        ";
        $qry_supplier = mysql_query($q);
        $row_supplier = mysql_fetch_array($qry_supplier);
        $suppliername = $row_supplier["Nama"];
        $Contact = $row_supplier["Contact"];
        
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
                AND ".$db["master"].".gudang.KdGudang = '".$arr_curr["KdGudang"]."'
            ORDER BY
                ".$db["master"].".gudang.Keterangan ASC
        ";
        $qry= mysql_query($q);
        $row = mysql_fetch_array($qry);
        $NamaGudang = $row["Keterangan"];
        
        $counter = 1;
        $q = "
                SELECT
                    ".$db["master"].".trans_terima_detail.Sid,    
                    ".$db["master"].".trans_terima_detail.PCode,
                    ".$db["master"].".trans_terima_detail.Qty,
                    ".$db["master"].".trans_terima_detail.Satuan,
                    ".$db["master"].".trans_terima_detail.Harga,
                    ".$db["master"].".trans_terima_detail.Disc1,
                    ".$db["master"].".trans_terima_detail.Disc2,
                    ".$db["master"].".trans_terima_detail.Potongan,
                    ".$db["master"].".trans_terima_detail.Jumlah,
                    ".$db["master"].".trans_terima_detail.PPn,
                    ".$db["master"].".trans_terima_detail.Total
                FROM
                    ".$db["master"].".trans_terima_detail
                WHERE
                    1
                    AND ".$db["master"].".trans_terima_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
                ORDER BY
                    ".$db["master"].".trans_terima_detail.Sid ASC
        ";
      
        $qry_po = mysql_query($q);
        while($row_po = mysql_fetch_array($qry_po))
        { 
            list(
                $Sid,    
                $PCode,
                $Qty,
                $Satuan,
                $Harga,
                $Disc1,
                $Disc2,
                $Potongan,
                $Jumlah,
                $PPn,
                $Total 
            ) = $row_po;
            
            $arr_data["list_detail"][$v_NoDokumen][$counter] = $counter;    
            
            $arr_data["data_rg_PCode"][$counter] = $PCode;
            $arr_data["data_rg_Qty"][$counter] = $Qty;
            $arr_data["data_rg_Satuan"][$counter] = $Satuan;
            $arr_data["data_rg_Harga"][$counter] = $Harga;
            $arr_data["data_rg_Disc1"][$counter] = $Disc1;
            $arr_data["data_rg_Disc2"][$counter] = $Disc2;
            $arr_data["data_rg_Potongan"][$counter] = $Potongan;
            $arr_data["data_rg_Jumlah"][$counter] = $Jumlah;
            $arr_data["data_rg_PPn"][$counter] = $PPn;
            $arr_data["data_rg_Total"][$counter] = $Total;
            
            $arr_data["list_PCode"][$PCode] = $PCode;
            $counter++;
        }
        
        
        if(count($arr_data["list_PCode"])*1>0)
        {
            $where_pcode = where_array($arr_data["list_PCode"], "PCode", "in");
            
            $q = "
                    SELECT
                        ".$db["master"].".masterbarang.PCode,
                        ".$db["master"].".masterbarang.NamaLengkap
                    FROM
                        ".$db["master"].".masterbarang
                    WHERE
                        1
                        ".$where_pcode."
                    ORDER BY
                        ".$db["master"].".masterbarang.PCode ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            { 
                list($PCode, $NamaLengkap) = $row; 
                
                $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
            }      
        }
        
        
        $curr_jml_detail = count($arr_data["list_detail"][$v_NoDokumen]);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "RECEIPT GOOD (".$NamaGudang.")";
        
        $total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            // header
            {                               
                $echo.=$arr_epson["cond"].$nama;   
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Jl. Raya Denpasar Bedugul KM.36 Tabanan";   
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Bali 82191 - Indonesia";   
                $echo.="\r\n";                          
            }
            $echo.="\r\n";
                                         
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }                                         
            
            $echo.=$arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$arr_curr["NoDokumen"])/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."No : ".$arr_curr["NoDokumen"];
            
            $echo.="\r\n";
                              
            // baris 1
            {
                $echo.=$arr_epson["cond"]."TANGGAL";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("TANGGAL"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=format_show_date($arr_curr["TglDokumen"]);
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen(format_show_date($arr_curr["TglDokumen"])));$i++)
                {
                    $echo.=" ";
                }               
                
                $echo.=$arr_epson["cond"]."PONO";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("PONO"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$arr_curr["PoNo"];    
                
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."SUPPLIER";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("SUPPLIER"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$suppliername;
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($suppliername));$i++)
                {
                    $echo.=" ";
                }            
                
                $echo.=$arr_epson["cond"]."NO SURAT JALAN";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("NO SURAT JALAN"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$arr_curr["NoSuratJalan"]; 

                $echo.="\r\n";    
            }
            
        
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCODE";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCODE"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."NAMA BARANG";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("NAMA BARANG"));$i++)
            {
                $echo.=" ";
            }    
            
            $echo.=$arr_epson["cond"]."QTY";
            $limit_spasi = 17;
            for($i=0;$i<($limit_spasi-strlen("QTY  "));$i++)
            {
                $echo.=" ";
            }            
            
            $echo.=$arr_epson["cond"]."SATUAN";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("SATUAN"));$i++)
            {
                $echo.=" ";
            }      
                        
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";            
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {   
                $PCode = $arr_data["data_rg_PCode"][$i_detail];
                $NamaLengkap = substr($arr_data["NamaLengkap"][$PCode], 0, 60);
                $Qty = $arr_data["data_rg_Qty"][$i_detail];
                $Satuan = $arr_data["data_rg_Satuan"][$i_detail];
                $Harga = $arr_data["data_rg_Harga"][$i_detail];
                $Disc1 = $arr_data["data_rg_Disc1"][$i_detail];
                $Disc2 = $arr_data["data_rg_Disc2"][$i_detail];
                $Potongan = $arr_data["data_rg_Potongan"][$i_detail];
                $Jumlah = $arr_data["data_rg_Jumlah"][$i_detail];
                $PPn = $arr_data["data_rg_PPn"][$i_detail];
                $Total = $arr_data["data_rg_Total"][$i_detail];

                $Harga_echo = $Jumlah/$Qty;
                
                // kalo ada isi baru di print
                if($NamaLengkap)
                {
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$PCode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($PCode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$NamaLengkap;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($NamaLengkap));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(format_number($Qty, 2)));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=format_number($Qty, 2);
                    
                    $echo.=$arr_epson["cond"].$Satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }                 
                    
                    $total += $Jumlah;
                }
                                                                                               
                $echo.="\r\n";
                $no++;             
            }            
                             
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="=";
            }                 
            $echo .= "\r\n";
                                 
            $echo .= $spasi_awal;
            if($i_page==$jml_page)
            {
                $echo.="\r\n"; 
                $echo.="\r\n";
                
                $echo.="       Dibuat Oleh     ";
                $echo.="          ";
                $echo.="       Diketahui Oleh      ";
                $echo.="          ";
                $echo.="       Disetujui Oleh      ";
                $echo.="\r\n";    
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="     (             )     ";
                $echo.="          ";
                $echo.="     (             )      ";
                $echo.="          ";
                $echo.="     (             )      ";
                $echo.="\r\n";  
                $echo.="\r\n";         
            }
            else
            {                     
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
            }
            $echo.=$arr_epson["cond"];     
            
            $q = "
                    SELECT
                        COUNT(noreferensi) AS jml_print
                    FROM
                        ".$db["master"].".log_print
                    WHERE
                        1
                        AND noreferensi = '".$v_NoDokumen."' 
                        AND form_data = 'receipt-goods' 
            ";
            $qry_jml_print = mysql_query($q);
            $row_jml_print = mysql_fetch_array($qry_jml_print);
            
            if($row_jml_print["jml_print"]*1>0)
            {
                //$echo .= $arr_epson["reset"];
                $limit_spasi = 135;
                for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=chr(15)."COPIED : ".(($row_jml_print["jml_print"]*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
            }
            else
            {
                //$echo .= $arr_epson["reset"];
                $limit_spasi = 135;
                for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
                {
                    $echo.=" ";
                }
            
               $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
               
            }                 
            
            $echo.="\r\n"; 
            $echo.="\r\n";   
            $echo.="\r\n";           
        
        } // end page
        
        
        // update counter print
        if($ses_login!="hendri1003" && $ses_login!="febri0202")
        {
            $q = "
                    INSERT
                        ".$db["master"].".log_print
                    SET
                        form_data = 'receipt-goods',
                        noreferensi = '".$v_NoDokumen."' , 
                        userid = '".$ses_login."' , 
                        print_date = NOW() , 
                        print_page = 'Setengah Letter'    
            ";
            if(!mysql_query($q))      
            {
                $msg = "Gagal Insert Print Counter";
                echo "<script>alert('".$msg."');</script>";     
                die();
            }
        }
    } 
    
    $paths = "cetakan/";
    $name_text_file='receipt-goods-'.$ses_login.'.txt';
    create_txt_report($paths,$name_text_file,$echo);
    header("Location: ".$paths."/".$name_text_file); 
    mysql_close($con);
?>  