<?php
include("header.php");

$modul   = "PROFORMA INVOICE";

if(!isset($_GET["id"])){ $id = isset($_GET["id"]); } else { $id = $_GET["id"]; }

if($id=="")
{
    die("ID Harus diisi");
}
unset($arr_data);
             
$q = "
        SELECT
            trans_reservasi.NoDokumen,    
            trans_reservasi.TglDokumen,    
            tourtravel.KdTravel,    
            tourtravel.Nama AS NamaTravel,
            tourtravel.Alamat,
            trans_reservasi.Phone,
            trans_reservasi.banquet_event,
            trans_reservasi.ReservasiDate
        FROM
            trans_reservasi
            INNER JOIN tourtravel ON
                trans_reservasi.KdTravel = tourtravel.KdTravel
                AND trans_reservasi.NoDokumen = '".$id."'
        WHERE
            1
        ORDER BY
            trans_reservasi.NoDokumen ASC,    
            trans_reservasi.TglDokumen ASC   
        LIMIT
            0,1
"; 
$counter = 0;
$qry = mysql_query($q);
$row = mysql_fetch_array($qry);
list(
    $NoDokumen,    
    $TglDokumen,    
    $KdTravel,    
    $NamaTravel,
    $Alamat,
    $Phone,
    $banquet_event,
    $ReservasiDate 
) = $row;

$ReservasiDate = substr($ReservasiDate, 0,10);

$counter = 1;
$q = "
        SELECT
            trans_reservasi_detail.sid,    
            trans_reservasi_detail.PCode,    
            trans_reservasi_detail.NamaBarang,    
            trans_reservasi_detail.Qty,
            trans_reservasi_detail.Harga
        FROM
            trans_reservasi_detail
        WHERE
            1
            AND trans_reservasi_detail.NoDokumen = '".$id."'
        ORDER BY
            trans_reservasi_detail.sid ASC
"; 
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{
    list($sid, $PCode, $NamaBarang, $Qty, $Harga) = $row;
    
    $arr_data["list_data"][$counter] = $counter;
    
    if($PCode)
    {
        $arr_data["list_pcode"][$PCode] = $PCode;
    }
    $arr_data["PCode"][$counter] = $PCode;
    $arr_data["NamaBarang"][$counter] = $NamaBarang;
    $arr_data["Qty"][$counter] = $Qty;
    $arr_data["Harga"][$counter] = $Harga;
    
    $counter++;
}

if(count($arr_data["list_pcode"])*1>0)
{
    $where_pcode = where_array($arr_data["list_pcode"], "PCode", "in");
    
    $q = "
            SELECT
                masterbarang.PCode,
                masterbarang.NamaLengkap
            FROM
                masterbarang
            WHERE
                1
                ".$where_pcode."
            ORDER BY
                masterbarang.PCode ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($PCode, $NamaLengkap) = $row; 
        
        $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
    }
}

$max_detail = 15;
                    
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
        
        function start_page()
        {
            
        }
        
		
	</script>
</head>

<body onload="start_page()" style="color: black; font-family: sans-serif; font-size: 11px; margin-top: 100px;">
<table align="center" width="720" border="0" cellpadding="2" cellspacing="2" style="font-family: sans-serif; font-size: 12px;">
    <tr>
        <td colspan="100%" align="center" style="font-size: 16px; font-weight: bold;">PROFORMA INVOICE</td>
    </tr>
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    <tr>
        <td width="50">To</td>
        <td width="5">:</td>
        <td width="250"> <?php echo $NamaTravel; ?></td>
        <td width="100">&nbsp;</td>
        <td width="100" valign="top">Invoice Date</td>
        <td width="5" valign="top">:</td>
        <td valign="top"> <?php echo format_show_date($TglDokumen); ?></td>
    </tr>
    
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td valign="top"><?php echo $Alamat."<br>".$Phone; ?></td>
        <td>&nbsp;</td>
        <td valign="top">Invoice Number</td>
        <td valign="top">:</td>
        <td valign="top"> <?php echo $NoDokumen; ?></td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
    <tr>
        <td colspan="100%" style="font-weight: bold;">Event <?php echo $banquet_event; ?> (<?php echo format_show_date($ReservasiDate)?>)</td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
    
    <tr>
        <td colspan="100%">
            <table width="100%" cellpadding="2" cellspacing="2">
                <tr style="font-weight: bold; font-size: 11px; border-top: 1px solid #000000; border-bottom: 1px solid #000000; ">
                    <td style="text-align: left; padding: 3px;">Description</td>
                    <td style="text-align: right; padding: 3px;" width="100">Qty/Pax</td>
                    <td style="text-align: right; padding: 3px;" width="150">Price/Pax (IDR)</td>
                    <td style="text-align: right; padding: 3px;" width="150">Total Price (IDR)</td>
                </tr>
                
                <?php 
                    $grand_total = 0;
                    for($i=1;$i<=$max_detail;$i++)
                    {
                        $PCode = $arr_data["PCode"][$i];
                        $NamaBarang = $arr_data["NamaBarang"][$i];
                        $NamaLengkap = $arr_data["NamaBarang"][$PCode];
                        $Qty = $arr_data["Qty"][$i];
                        $Harga = $arr_data["Harga"][$i];
                        
                        if($PCode)
                        {
                            $echo_nama = $NamaLengkap;    
                        }
                        else
                        {
                            $echo_nama = $NamaBarang;    
                        }
                        
                        $total = $Qty * $Harga;
                        $grand_total += $total;
                        
                ?>
                <tr style="font-size: 11px; border-top: 1px solid #000000; border-bottom: 1px solid #000000; ">
                    <td style="text-align: left; padding: 3px;"><?php echo $echo_nama; ?>&nbsp;</td>
                    <td style="text-align: right; padding: 3px;"><?php echo format_number($Qty); ?></td>
                    <td style="text-align: right; padding: 3px;"><?php echo format_number($Harga); ?></td>
                    <td style="text-align: right; padding: 3px;"><?php echo format_number($total); ?></td>
                </tr>
                <?php 
                    }
                ?>
                
                <tr style="font-weight: bold; font-size: 11px; border-top: 1px solid #000000; border-bottom: 1px solid #000000; ">
                    <td style="text-align: right; padding: 3px;">TOTAL</td>
                    <td style="text-align: right; padding: 3px;">&nbsp;</td>
                    <td style="text-align: right; padding: 3px;">&nbsp;</td>
                    <td style="text-align: right; padding: 3px;"><?php echo format_number($grand_total); ?></td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
    <tr>
        <td colspan="100%">
            <table width="100%" cellpadding="2" cellspacing="2" style="font-size: 12px;">
                <tr>
                    <td colspan="100%"><b>TERM OF PAYMENT :</b></td>
                </tr>
                <tr>
                    <td colspan="100%">Payment cash at Secret Garden Villages Or Please Kindly wire the Payment to our Bank Account below :</td>
                </tr>
                
                <tr>
                    <td width="150">BANK NAME</td>
                    <td width="5">:</td>
                    <td> <b>BANK MANDIRI (Cab. Tabanan)</b></td>
                </tr>
                
                <tr>
                    <td>ACCOUNT NUMBER</td>
                    <td>:</td>
                    <td> <b>175 0000 260793</b></td>
                </tr>
                
                <tr>
                    <td>ACCOUNT NAME</td>
                    <td>:</td>
                    <td> <b>PT. NATURA PESONA MANDIRI</b></td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
    
    <tr>
        <td colspan="100%">
            <table width="100%" cellpadding="2" cellspacing="2" style="font-size: 12px; font-weight: bold;">
                <tr>
                    <td align="center" width="30%">Prepared by,</td>
                    <td width="35%">&nbsp;</td>
                    <td align="center" width="30%">Acknowledge by,</td>
                </tr>
                
                
                <tr style="height: 60px;">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                
                <tr>
                    <td align="center"><u>Kadek Sariasih</u></td>
                    <td>&nbsp;</td>
                    <td align="center"><u>Dicky Prasetyo</u></td>
                </tr>
                
                <tr>
                    <td align="center">Asst. F&A Manager</td>
                    <td>&nbsp;</td>
                    <td align="center">Senior Operasional Manager</td>
                </tr>
                
            </table>
        </td>
    </tr>
    
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="100%">&nbsp;</td>
    </tr>
    
</table>	                        
</body>
</html>
<?php 
    mysql_close();
?>