<?php               
    include("header.php");
    
    
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    if(!isset($_POST["v_keyword"])){ $v_keyword = isset($_POST["v_keyword"]); } else { $v_keyword = $_POST["v_keyword"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Voucher";
    
    if($v_date_from=="")
    {
        $v_date_from = "01".date("/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("t/m/Y");
    }
    
   
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-voucher.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  


    </script>
    
    <style>
        .link_pop{
            text-decoration: underline;
            color: black;
        }
        
        .link_pop:hover{
            text-decoration: none;
            color: #222222;
        }
    </style>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Reporting</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="POST" name="theform" id="theform">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="100">Tanggal</th>
						<th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
					</tr>
                    
                   
                     
                      <tr>
                        <th>Keyword</th>
                        <th>: 
                            <input type="text" class="form-control-new" name="v_keyword" id="v_keyword" style="width: 200px;" value="<?php echo $v_keyword; ?>">
                        </th>
                     </tr>
                     
                      
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
				</thead>
				
			</table> 
			<br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    $where_divisi = "";
                    if($v_KdDivisi!="")
                    {
                        $where_divisi = " AND `divisi`.KdDivisi = '".$v_KdDivisi."' ";    
                    }
                     
                    
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    else
                    {
                        $where_date = " AND `transaksi_detail_voucher`.Tanggal BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";    
                    }
                    
                    $arr_keyword[0] = "transaksi_detail_voucher.NomorVoucher";    
                    $arr_keyword[1] = "transaksi_header.NoStruk";    
                    $arr_keyword[2] = "transaksi_header.Kasir";      
                    
                    $where_keyword = "";
                    if($v_keyword)
                    {
                        $where_search_keyword = search_keyword($v_keyword, $arr_keyword);
                        $where_keyword = $where_search_keyword;
                    }
                    
                    $q = "
                            SELECT
                                kassa.id_kassa,        
                                kassa.KdDivisi
                            FROM
                                kassa
                            WHERE
                                1
                            ORDER BY
                                kassa.id_kassa
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $id_kassa,
                            $KdDivisi
                        ) = $row;
                        
                        $arr_data["divisi"][$id_kassa] = $KdDivisi;
                    }
                    
                    $q = "
                            SELECT
                                transaksi_detail_voucher.Tanggal,
                                transaksi_header.Waktu,
                                transaksi_header.NoStruk,
                                transaksi_detail_voucher.NomorVoucher,
                                transaksi_header.Kasir,
                                transaksi_header.NoKassa,
                                transaksi_detail_voucher.NilaiVoucher
                            FROM
                                transaksi_detail_voucher
                                INNER JOIN transaksi_header ON
                                    transaksi_detail_voucher.NoStruk = transaksi_header.NoStruk
                                    AND transaksi_header.Status*1 = '1'
                                    ".$where_date."
                                    AND transaksi_detail_voucher.NilaiVoucher*1!='0'
                            WHERE
                                1
                                ".$where_keyword."
                            ORDER BY
                                transaksi_detail_voucher.Tanggal ASC,
                                transaksi_header.Waktu ASC
                    ";
                    
                    $counter = 0;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $Tanggal,
                            $Waktu,
                            $NoStruk,
                            $NomorVoucher,
                            $Kasir,
                            $NoKassa,
                            $NilaiVoucher     
                        ) = $row;
                        
                        $arr_data["list_data"][$counter] = $counter;
                        $arr_data["data_Tanggal"][$counter] = $Tanggal;
                        $arr_data["data_Waktu"][$counter] = $Waktu;
                        $arr_data["data_NoStruk"][$counter] = $NoStruk;
                        $arr_data["data_NomorVoucher"][$counter] = $NomorVoucher;
                        $arr_data["data_Kasir"][$counter] = $Kasir;
                        $arr_data["data_NoKassa"][$counter] = $NoKassa;
                        $arr_data["data_NilaiVoucher"][$counter] = $NilaiVoucher;
                        
                        $KdDivisi = $arr_data["divisi"][$NoKassa];
                        
                        if($KdDivisi==1)
                        {
                            $arr_data["luwus_rice"][$counter] = $NilaiVoucher;
                        }
                        else if($KdDivisi==2)
                        {
                            $arr_data["black_eye"][$counter] = $NilaiVoucher;
                        }
                        else if($KdDivisi==3)
                        {
                            $arr_data["oemah_herborist"][$counter] = $NilaiVoucher;
                        }
                        
                        $counter++;
                    }
                    
                   
                    
                    //echo "<pre>";
                    //print_r($arr_data["list_data_oke"]);
                    //echo "</pre>";
                    
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="10">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="10">REPORT VOUCHER</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="10">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    
                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                            <tr class="title_table">
                                <td>No</td>
                                <td>Tanggal</td>
                                <td>Waktu</td>
                                <td>NoStruk</td>
                                <td>NoVoucher</td>
                                <td>Kasir</td>
                                <td style="text-align: right;">Oemah Herborist</td>
                                <td style="text-align: right;">Luwus Rice</td>
                                <td style="text-align: right;">Black Eye</td>
                                <td style="text-align: right;">Total</td>
                            </tr>
                        
                        <tbody style="color: black;">
                            <?php 
                                $NoPR_prev = "";
                                $no = 1;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $Tanggal = $arr_data["data_Tanggal"][$counter];
                                    $Waktu = $arr_data["data_Waktu"][$counter];
                                    $NoStruk = $arr_data["data_NoStruk"][$counter];
                                    $Kasir = $arr_data["data_Kasir"][$counter];
                                    $NomorVoucher = $arr_data["data_NomorVoucher"][$counter];
                                    $NoKassa = $arr_data["data_NoKassa"][$counter];
                                    $NilaiVoucher = $arr_data["data_NilaiVoucher"][$counter]; 
                                    
                                    $oemah_herborist = $arr_data["oemah_herborist"][$counter];
                                    $luwus_rice = $arr_data["luwus_rice"][$counter];
                                    $black_eye = $arr_data["black_eye"][$counter];
                                    
                                    ?>
                                    
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo format_show_date($Tanggal); ?></td>
                                        <td><?php echo $Waktu; ?></td>
                                        <td><?php echo $NoStruk; ?></td>
                                        <td><?php echo $NomorVoucher; ?></td>
                                        <td><?php echo $Kasir; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($oemah_herborist, 2, ",", ".", "ind"); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($luwus_rice, 2, ",", ".", "ind"); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($black_eye, 2, ",", ".", "ind"); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($NilaiVoucher, 2, ",", ".", "ind"); ?></td>
                                    </tr>
                                    <?php
                                    
                                    $total_oemah_herborist += $oemah_herborist;
                                    $total_luwus_rice += $luwus_rice;
                                    $total_black_eye += $black_eye;
                                    $total_NilaiVoucher += $NilaiVoucher;
                                    $no++;
                                }
                            ?>
                        </tbody>
                        
                        <tfoot>
                            <tr>
                                <td style="text-align: right;" colspan="6">TOTAL</td>
                                <td style="text-align: right;"><?php echo format_number($total_oemah_herborist, 2, ",", ".", "ind"); ?></td>
                                <td style="text-align: right;"><?php echo format_number($total_luwus_rice, 2, ",", ".", "ind"); ?></td>
                                <td style="text-align: right;"><?php echo format_number($total_black_eye, 2, ",", ".", "ind"); ?></td>
                                <td style="text-align: right;"><?php echo format_number($total_NilaiVoucher, 2, ",", ".", "ind"); ?></td>
                            </tr>   
                        </tfoot>
                    </table>
                    <?php
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
			
		
		</div>
		
		</form>
		
<?php 
                    include("footer.php"); 
                }
?>