<?php
    include("header.php");

    if(!isset($_GET["ajax"])){ $ajax = isset($_GET["ajax"]); } else { $ajax = $_GET["ajax"]; }
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    if(!isset($_POST["v_KdSupplier"])){ $v_KdSupplier = isset($_POST["v_KdSupplier"]); } else { $v_KdSupplier = $_POST["v_KdSupplier"]; }
    if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }
    if(!isset($_POST["v_keyword_pcode"])){ $v_keyword_pcode = isset($_POST["v_keyword_pcode"]); } else { $v_keyword_pcode = $_POST["v_keyword_pcode"]; }

    if(!isset($_POST["v_pcode"])){ $v_pcode = isset($_POST["v_pcode"]); } else { $v_pcode = $_POST["v_pcode"]; }
    if(!isset($_POST["v_keyword"])){ $v_keyword = isset($_POST["v_keyword"]); } else { $v_keyword = $_POST["v_keyword"]; }

    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }

    $icon_type_change = "entypo-up-dir";

    $modul = "Report Terima Barang.";

    if($ajax)
    {
        if($ajax=="ajax_keyword_pcode")
        {
            $v_keyword_pcode = save_char($_GET["v_keyword_pcode"]);

            $where_keyword_pcode = "";
            if($v_keyword_pcode)
            {
                $arr_keyword[0] = "masterbarang.PCode";
                $arr_keyword[1] = "masterbarang.NamaLengkap";

                $where_search_keyword = search_keyword($v_keyword_pcode, $arr_keyword);
                $where_keyword_pcode = $where_search_keyword;
            }

            $q = "
                    SELECT
                        ".$db["master"].".masterbarang.PCode,
                        ".$db["master"].".masterbarang.NamaLengkap
                    FROM
                        ".$db["master"].".masterbarang
                    WHERE
                        1
                        ".$where_keyword_pcode."
                    ORDER BY
                        ".$db["master"].".masterbarang.NamaLengkap ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list($PCode, $NamaLengkap) = $row;

                $arr_data["list_pcode"][$PCode] = $PCode;
                $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
            }

            ?>
            <select class="form-control-new" name="v_pcode" id="v_pcode" style="width: 200px;">
                <option value="">Semua</option>
                <?php
                    foreach($arr_data["list_pcode"] as $pcode=>$val)
                    {
                        $NamaLengkap = $arr_data["NamaLengkap"][$pcode];

                        $selected = "";
                        if($v_pcode==$pcode)
                        {
                            $selected = "selected='selected'";
                        }
                        ?>
                            <option <?php echo $selected; ?> value="<?php echo $pcode; ?>"><?php echo $NamaLengkap; ?></option>
                        <?php
                    }
                ?>
            </select>
            <?php


        }
        exit();
    }

    if($v_date_from=="")
    {
        $v_date_from = "01".date("/m/Y");
    }

    if($v_date_to=="")
    {
        $v_date_to = date("t/m/Y");
    }

    $q = "
            SELECT
                ".$db["master"].".supplier.KdSupplier,
                ".$db["master"].".supplier.Nama
            FROM
                ".$db["master"].".supplier
            WHERE
                1
            ORDER BY
                ".$db["master"].".supplier.Nama ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdSupplier, $Nama) = $row;

        $arr_data["list_supplier"][$KdSupplier] = $KdSupplier;
        $arr_data["Nama"][$KdSupplier] = $Nama;
    }

    $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdGudang, $NamaGudang) = $row;

        $arr_data["list_gudang"][$KdGudang] = $KdGudang;
        $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
    }

    $q = "
            SELECT
                ".$db["master"].".gudang_admin.KdGudang
            FROM
                ".$db["master"].".gudang_admin
            WHERE
                1
                AND ".$db["master"].".gudang_admin.UserName = '".$ses_login."'
            ORDER BY
                ".$db["master"].".gudang_admin.KdGudang ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdGudang, $NamaGudang) = $row;

        $arr_data["list_gudang_admin"][$KdGudang] = $KdGudang;
    }

    $where_keyword_pcode = "";
    if($v_keyword_pcode)
    {
        $arr_keyword[0] = "masterbarang.PCode";
        $arr_keyword[1] = "masterbarang.NamaLengkap";

        $where_search_keyword = search_keyword($v_keyword_pcode, $arr_keyword);
        $where_keyword_pcode = $where_search_keyword;
    }

    $q = "
            SELECT
                ".$db["master"].".masterbarang.PCode,
                ".$db["master"].".masterbarang.NamaLengkap
            FROM
                ".$db["master"].".masterbarang
            WHERE
                1
                ".$where_keyword_pcode."
            ORDER BY
                ".$db["master"].".masterbarang.NamaLengkap ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($PCode, $NamaLengkap) = $row;

        $arr_data["list_pcode"][$PCode] = $PCode;
        $arr_data["NamaLengkap"][$PCode] = $PCode." :: ".$NamaLengkap;
    }

    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-terima-barang.xls");
        header("Content-type: application/vnd.ms-excel");
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();
    }

    function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
    {
        try
        {
            if (!tipenya) return false;
            //document.getElementById("show_image_ajax").style.display='block';

            if (param1 == undefined) param1 = '';
            if (param2 == undefined) param2 = '';
            if (param3 == undefined) param3 = '';
            if (param4 == undefined) param4 = '';
            if (param5 == undefined) param5 = '';

            var variabel;
            var arr_data;
            variabel = "";

            if(tipenya=='ajax_keyword_pcode')
            {
                v_keyword_pcode = param1;

                variabel += "&v_keyword_pcode="+v_keyword_pcode;

                //alert(param1);
                document.getElementById("td_pcode").innerHTML = 'Loading...';

                xmlhttp.open('get', 'report_receipt_goods.php?ajax='+tipenya+variabel, true);
                xmlhttp.onreadystatechange = function()
                {
                    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                    {
                        document.getElementById("td_pcode").innerHTML   = xmlhttp.responseText;
                    }

                    return false;
                }
                xmlhttp.send(null);
            }

        }
        catch(err)
        {
            txt  = "There was an error on this page.\n\n";
            txt += "Error description : "+ err.message +"\n\n";
            txt += "Click OK to continue\n\n";
            alert(txt);
        }
    }


function mouseover(target)
{
    if(target.bgColor!="#cafdb5"){
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}

function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }
}

function mouseclick(target, idobject, num)
{

    //var pjg = document.getElementById(idobject + '_sum').innerHTML;
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}

function pop_up_detail(NoPO, PCode)
{
    var variabel;
    variabel = "";
    variabel += "?v_NoPO="+NoPO;
    variabel += "&v_PCode="+PCode;

    windowOpener(600, 800, 'Detail Terima', 'report_outstanding_po_pop_up_terima.php'+variabel, 'Detail Terima');
}
    </script>

    <style>
        .link_pop{
            text-decoration: underline;
            color: black;
        }

        .link_pop:hover{
            text-decoration: none;
            color: #222222;
        }
    </style>

    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>

    <div class="main-content">

		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>

		<hr/>
		<br/>

        <form method="POST" name="theform" id="theform">

		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">

			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="100">Tanggal</th>
						<th>:
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
					</tr>


                    <tr>
                        <th>Supplier</th>
                        <th>:

                                  <select class="form-control-new" name="v_KdSupplier" id="v_KdSupplier" style="width: 300px;">
                                    <option value="">Semua</option>

                                    <?php
                                        foreach($arr_data["list_supplier"] as $KdSupplier=>$val)
                                        {
                                            $Nama = $arr_data["Nama"][$KdSupplier];
                                        ?>
                                        <option <?php if($v_KdSupplier==$KdSupplier) echo "selected='selected'"; ?> value="<?php echo $KdSupplier; ?>"><?php echo $Nama; ?></option>
                                        <?php
                                        }
                                    ?>
                                </select>
                        </th>
                    </tr>

                     <tr>
                        <th>Gudang</th>
                        <th>:

                                  <select class="form-control-new" name="v_KdGudang" id="v_KdGudang" style="width: 300px;">
                                    <option value="">Semua</option>

                                    <?php
                                        foreach($arr_data["list_gudang_admin"] as $KdGudang=>$val)
                                        {
                                            $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                        ?>
                                        <option <?php if($v_KdGudang==$KdGudang) echo "selected='selected'"; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                        <?php
                                        }
                                    ?>
                                </select>
                        </th>
                    </tr>



                     <tr>
                        <th>PCode</th>
                        <th>:
                            <input type="text" onkeyup="CallAjaxForm('ajax_keyword_pcode', this.value)" class="form-control-new" size="10" name="v_keyword_pcode" id="v_keyword_pcode" value="<?php echo $v_keyword_pcode; ?>" placeholder="Keyword">
                            <span id="td_pcode">
                            <select class="form-control-new" name="v_pcode" id="v_pcode" style="width: 200px;">
                                <option value="" selected>Semua</option>
                                <?php
                                    foreach($arr_data["list_pcode"] as $pcode=>$val)
                                    {
                                        $NamaLengkap = $arr_data["NamaLengkap"][$pcode];

                                        $selected = "";
                                        if($v_pcode==$pcode)
                                        {
                                            $selected = "selected='selected'";
                                        }
                                        ?>
                                            <option <?php echo $selected; ?> value="<?php echo $pcode; ?>"><?php echo $NamaLengkap; ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                            </span>
                        </th>
                     </tr>

                      <tr>
                        <th>Keyword</th>
                        <th>:
                            <input type="text" class="form-control-new" name="v_keyword" id="v_keyword" style="width: 300px;" value="<?php echo $v_keyword; ?>">
                        </th>
                     </tr>



                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
				</thead>

			</table>
			<br><br>

            <?php
    }
            ?>

            <?php
                if($btn_submit || $btn_excel)
                {
                    $where_gudang = "";
                    if($v_KdGudang!="")
                    {
                        $where_gudang = " AND `gudang`.KdGudang = '".$v_KdGudang."' ";
                    }

                    $where_supplier = "";
                    if($v_KdSupplier!="")
                    {
                        $where_supplier = " AND `supplier`.KdSupplier = '".$v_KdSupplier."' ";
                    }

                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    else
                    {
                        $where_date = " AND `trans_terima_header`.TglDokumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";
                    }

                    $arr_keyword[0] = "trans_terima_header.NoDokumen";
                    $arr_keyword[1] = "trans_terima_header.NoSuratJalan";
                    $arr_keyword[2] = "trans_terima_header.Keterangan";

                    $where_keyword = "";
                    if($v_keyword)
                    {
                        $where_search_keyword = search_keyword($v_keyword, $arr_keyword);
                        $where_keyword = $where_search_keyword;
                    }

                    $where_keyword_pcode = "";
                    if($v_pcode)
                    {
                        $where_keyword_pcode = " AND trans_terima_detail.PCode = '".$v_pcode."' ";
                    }
                    else
                    {
                        $where_keyword_pcode = "";
                        if($v_keyword_pcode)
                        {
                            $arr_keyword_pcode[0] = "masterbarang.PCode";
                            $arr_keyword_pcode[1] = "masterbarang.NamaLengkap";
                            $where_keyword_pcode = search_keyword($v_keyword_pcode, $arr_keyword_pcode);
                        }
                    }

//trans_terima_detail.Qty*IF(`trans_terima_detail`.`Harga`=0,trans_order_barang_detail.Harga,trans_terima_detail.Harga)/trans_terima_detail.QtyPcs as Harga,
                    $q = "
                            (SELECT
                                trans_terima_header.NoDokumen,
                                trans_terima_header.TglDokumen,
                                trans_terima_header.NoSuratJalan,
                                trans_terima_header.Keterangan,
                                supplier.KdSupplier,
                                supplier.Nama AS NamaSupplier,
                                gudang.KdGudang,
                                gudang.Keterangan AS NamaGudang,
                                trans_terima_header.PoNo,
                                kategori.NamaKategori,
                                trans_terima_detail.PCode,
                                masterbarang.NamaLengkap,
                                trans_terima_detail.QtyPcs as Qty,
                                masterbarang.SatuanSt as Satuan,
                                trans_terima_detail.Qty*IF(`trans_terima_detail`.`Harga`>0,trans_terima_detail.Harga,trans_order_barang_detail.Harga)/trans_terima_detail.QtyPcs as Harga,
                                trans_terima_detail.`Jumlah`*(trans_terima_header.PPn/100) AS PPN,
                                trans_terima_detail.Qty*trans_terima_detail.Harga as Jumlah,
                                invoice_pembelian_header.NoFaktur,
                                invoice_pembelian_header.NoFakturPajak,
                                `trans_order_barang_header`.`kurs`,
                                `trans_order_barang_header`.`currencycode`
                            FROM
                                trans_terima_header
                                INNER JOIN trans_terima_detail ON
                                    trans_terima_header.NoDokumen = trans_terima_detail.NoDokumen
                                    AND trans_terima_header.Status*1 != '2'
                                    AND trans_terima_detail.Qty*1 != '0'
                                    ".$where_date."
                                INNER JOIN `trans_order_barang_detail`
                                    ON `trans_terima_header`.`PoNo` = `trans_order_barang_detail`.`NoDokumen`
                                    AND `trans_terima_detail`.`PCode` = `trans_order_barang_detail`.`PCode`
                                INNER JOIN `trans_order_barang_header`
                                    ON `trans_order_barang_header`.`NoDokumen` = `trans_order_barang_detail`.`NoDokumen`
                                INNER JOIN supplier ON
                                    trans_terima_header.KdSupplier = supplier.KdSupplier
                                    ".$where_supplier."
                                INNER JOIN gudang ON
                                    trans_terima_header.KdGudang = gudang.KdGudang
                                    ".$where_gudang."
                                INNER JOIN masterbarang ON
                                    masterbarang.PCode = trans_terima_detail.PCode
                                    INNER JOIN kategori ON
                                    masterbarang.KdKategori = kategori.KdKategori
                                left join invoice_pembelian_header on trans_terima_header.NoDokumen=invoice_pembelian_header.NoPenerimaan
                            WHERE
                                1
                                ".$where_keyword."
                                ".$where_keyword_pcode."
                            ORDER BY
                                trans_terima_header.TglDokumen ASC,
                                trans_terima_header.NoDokumen ASC)
                                UNION ALL
                            (SELECT
                                trans_terima_header.NoDokumen,
                                trans_terima_header.TglDokumen,
                                trans_terima_header.NoSuratJalan,
                                trans_terima_header.Keterangan,
                                supplier.KdSupplier,
                                supplier.Nama AS NamaSupplier,
                                gudang.KdGudang,
                                gudang.Keterangan AS NamaGudang,
                                trans_terima_header.PoNo,
                                kategori.NamaKategori,
                                trans_terima_detail.PCode,
                                masterbarang.NamaLengkap,
                                trans_terima_detail.QtyPcs as Qty,
                                masterbarang.SatuanSt as Satuan,
                                trans_terima_detail.Qty*trans_terima_detail.Harga/trans_terima_detail.QtyPcs as Harga,
                                trans_terima_detail.`Jumlah`*(trans_terima_header.PPn/100) AS PPN,
                                trans_terima_detail.Qty*trans_terima_detail.Harga as Jumlah,
                                invoice_pembelian_header.NoFaktur,
                                invoice_pembelian_header.NoFakturPajak,
                                trans_terima_header.`kurs`,
                                trans_terima_header.`currencycode`
                            FROM
                                trans_terima_header
                                INNER JOIN trans_terima_detail ON
                                    trans_terima_header.NoDokumen = trans_terima_detail.NoDokumen
                                    AND trans_terima_header.Status*1 != '2'
                                    AND trans_terima_detail.Qty*1 != '0'
                                    ".$where_date."
                                INNER JOIN supplier ON
                                    trans_terima_header.KdSupplier = supplier.KdSupplier
                                    ".$where_supplier."
                                INNER JOIN gudang ON
                                    trans_terima_header.KdGudang = gudang.KdGudang
                                    ".$where_gudang."
                                INNER JOIN masterbarang ON
                                    masterbarang.PCode = trans_terima_detail.PCode
                                     INNER JOIN kategori ON
                                    masterbarang.KdKategori = kategori.KdKategori
                                left join invoice_pembelian_header on trans_terima_header.NoDokumen=invoice_pembelian_header.NoPenerimaan
                            WHERE
                                1
                                ".$where_keyword."
                                ".$where_keyword_pcode."
                                AND trans_terima_header.Keterangan = 'RG Dari VCI'
                            ORDER BY
                                trans_terima_header.TglDokumen ASC,
                                trans_terima_header.NoDokumen ASC)
                    ";
                    //echo "hello ".$q;
                    //trans_terima_detail.Qty*trans_terima_detail.Harga as Jumlah,
                    $counter = 0;
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $NoDokumen,
                            $TglDokumen,
                            $NoSuratJalan,
                            $Keterangan,
                            $KdSupplier,
                            $NamaSupplier,
                            $KdGudang,
                            $NamaGudang,
                            $PoNo,
                            $Kategori,
                            $PCode,
                            $NamaLengkap,
                            $Qty,
                            $Satuan,
                            $Harga,
                            $PPN,
                            $Jumlah,
                            $NoFaktur,
                            $NoFakturPajak,
                            $kurs,
                            $currencycode
                        ) = $row;

                        $arr_data["list_data"][$counter] = $counter;
                        $arr_data["data_NoDokumen"][$counter] = $NoDokumen;
                        $arr_data["data_TglDokumen"][$counter] = $TglDokumen;
                        $arr_data["data_Faktur"][$counter] = $NoFaktur;;
                        $arr_data["data_FakturPajak"][$counter] = $NoFakturPajak;
                        $arr_data["data_Kurs"][$counter] = $kurs;
                        $arr_data["data_Currencycode"][$counter] = $currencycode;
                        $arr_data["data_NoSuratJalan"][$counter] = $NoSuratJalan;
                        $arr_data["data_Keterangan"][$counter] = $Keterangan;
                        $arr_data["data_KdSupplier"][$counter] = $KdSupplier;
                        $arr_data["data_NamaSupplier"][$counter] = $NamaSupplier;
                        $arr_data["data_KdGudang"][$counter] = $KdGudang;
                        $arr_data["data_NamaGudang"][$counter] = $NamaGudang;
                        $arr_data["data_PoNo"][$counter] = $PoNo;
                        $arr_data["data_Kategori"][$counter] = $Kategori;
                        $arr_data["data_PCode"][$counter] = $PCode;
                        $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                        $arr_data["data_Qty"][$counter] = $Qty;
                        $arr_data["data_Satuan"][$counter] = $Satuan;
                        $arr_data["data_Harga"][$counter] = $Harga;
                        $arr_data["data_ppn"][$counter] = $PPN;
                        $arr_data["data_Jumlah"][$counter] = $Jumlah;

                        $arr_data["list_NoPO"][$NoPO] = $NoPO;
                        $counter++;
                    }



                    //echo "<pre>";
                    //print_r($arr_data["list_data_oke"]);
                    //echo "</pre>";


                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }

                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="13">PT. NATURA PESONA MANDIRI</td>
                            </tr>
                            <tr>
                                <td colspan="13">REPORT TERIMA BARANG</td>
                            </tr>

                            <tr>
                                <td colspan="13">&nbsp;</td>
                            </tr>
                        </table>
                        <?php
                    }



                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                            <tr class="title_table" style="font-weight: bold;">
                                <td>No</td>
                                <td>No RG</td>
                                <td>No Faktur</td>
                                <td>Faktur Pajak.</td>
                                <td>Tanggal</td>
                                <td>No SJ</td>
                                <td>Supplier</td>
                                <td>Gudang</td>
                                <td>No PO</td>
                                <td>Cur PO</td>
                                <td>Kurs PO</td>
                                <td>Kategori</td>
                                <td>PCode</td>
                                <td>Nama Barang</td>
                                <td style="text-align: right;">Qty</td>
                                <td>Satuan</td>
                                <td style="text-align: right;">Harga</td>
                                <td style="text-align: right;">DPP</td>
                                <td style="text-align: right;">PPN</td>
                                <td style="text-align: right;">Subtotal</td>
                            </tr>


                        <tbody style="color: black;">
                            <?php
                                $NoDokumen_prev = "";
                                $no = 0;
                                $no_echo = 0;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $NoDokumen = $arr_data["data_NoDokumen"][$counter];
                                    $NoFaktur = $arr_data["data_Faktur"][$counter];
                                    $NoFakturPajak = $arr_data["data_FakturPajak"][$counter];
                                    $kurs = $arr_data["data_Kurs"][$counter];
                                    $currencycode = $arr_data["data_Currencycode"][$counter];
                                    $TglDokumen = $arr_data["data_TglDokumen"][$counter];
                                    $NoSuratJalan = $arr_data["data_NoSuratJalan"][$counter];
                                    $Keterangan = $arr_data["data_Keterangan"][$counter];
                                    $KdSupplier = $arr_data["data_KdSupplier"][$counter];
                                    $NamaSupplier = $arr_data["data_NamaSupplier"][$counter];
                                    $KdGudang = $arr_data["data_KdGudang"][$counter];
                                    $NamaGudang = $arr_data["data_NamaGudang"][$counter];
                                    $PoNo = $arr_data["data_PoNo"][$counter];
                                    $Kategori = $arr_data["data_Kategori"][$counter];
                                    $PCode = $arr_data["data_PCode"][$counter];
                                    $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                                    $Qty = $arr_data["data_Qty"][$counter];
                                    $Satuan = $arr_data["data_Satuan"][$counter];
                                    $Harga = $arr_data["data_Harga"][$counter];
                                    $ppn = $arr_data["data_ppn"][$counter];
                                    $Jumlah = $arr_data["data_Jumlah"][$counter];

                                    $no_echo = "";
                                    $NoDokumen_echo = "";
                                    $NoFaktur_echo = "";
                                    $NoFakturPajak_echo = "";
                                    $Kurs_echo = "";
                                    $Currencycode_echo = "";
                                    $TglDokumen_echo = "";
                                    $NoSuratJalan_echo = "";
                                    $Keterangan_echo = "";
                                    $NamaSupplier_echo = "";
                                    $NamaGudang_echo = "";
                                    $PoNo_echo = "";

                                    if($NoDokumen_prev!=$NoDokumen)
                                    {
                                        $no++;
                                        $no_echo = $no;
                                        $NoDokumen_echo = $NoDokumen;
                                        $NoFaktur_echo = $NoFaktur;
                                        $NoFakturPajak_echo = $NoFakturPajak;
                                        $Kurs_echo = $kurs;
                                        $Currencycode_echo = $currencycode;
                                        $TglDokumen_echo = format_show_date($TglDokumen);
                                        $NoSuratJalan_echo = $NoSuratJalan;
                                        $Keterangan_echo = $Keterangan;
                                        $NamaSupplier_echo = $NamaSupplier;
                                        $NamaGudang_echo = $NamaGudang;
                                        $PoNo_echo = $PoNo;
                                    }

                                    ?>

                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no_echo; ?></td>
                                        <td><?php echo $NoDokumen_echo; ?></td>
                                        <td><?php echo $NoFaktur_echo; ?></td>
                                        <td><?php echo $NoFakturPajak_echo; ?></td>
                                        <td><?php echo $TglDokumen_echo; ?></td>
                                        <td><?php echo $NoSuratJalan_echo; ?></td>
                                        <td><?php echo $NamaSupplier_echo; ?></td>
                                        <td><?php echo $NamaGudang_echo; ?></td>
                                        <td><?php echo $PoNo_echo; ?></td>
                                        <td><?php echo $Currencycode_echo; ?></td>
                                        <td><?php echo $Kurs_echo; ?></td>
                                        <td><?php echo $Kategori; ?></td>
                                        <td style="mso-number-format: '\@'"><?php echo $PCode; ?></td>
                                        <td><?php echo $NamaLengkap; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($Qty, 2, ",", ".", "ind"); ?></td>
                                        <td><?php echo $Satuan; ?></td>
                                        <td style="text-align: right;"><?php echo format_number($Harga, 2, ",", ".", "ind"); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($Harga*$Qty, 2, ",", ".", "ind"); ?></td>
                                        <td style="text-align: right;"><?php echo format_number($ppn, 2, ",", ".", "ind"); ?></td>
                                        <!--<td style="text-align: right;"><?php echo format_number($Jumlah, 2, ",", ".", "ind"); ?></td>-->
                                        <td style="text-align: right;"><?php echo format_number(($Harga*$Qty)+$ppn, 2, ",", ".", "ind"); ?></td>
                                    </tr>
                                    <?php

                                    $NoDokumen_prev = $NoDokumen;
                                }
                            ?>
                        </tbody>

                        <tfoot>

                        </tfoot>
                    </table>
                    <?php
                }


                if(!$btn_excel)
                {

            ?>


		</div>

		</form>

<?php
                    include("footer.php");
                }
?>
