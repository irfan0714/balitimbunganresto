<?php               
    include("header.php");
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }
    
    if(!isset($_POST["v_keyword"])){ $v_keyword = isset($_POST["v_keyword"]); } else { $v_keyword = $_POST["v_keyword"]; }
    if(!isset($_POST["v_sort_by"])){ $v_sort_by = isset($_POST["v_sort_by"]); } else { $v_sort_by = $_POST["v_sort_by"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Stock Opname";
    
    $q = "
        SELECT
            ".$db["master"].".gudang.KdGudang,
            ".$db["master"].".gudang.Keterangan
        FROM
            ".$db["master"].".gudang
        WHERE
            1
        ORDER BY
            ".$db["master"].".gudang.KdGudang ASC
    ";
    $qry_gudang = mysql_query($q);
    while($row_gudang = mysql_fetch_array($qry_gudang))
    {  
        list($KdGudang, $Keterangan) = $row_gudang;
        
        $arr_data["list_Gudang"][$KdGudang] = $KdGudang;
        $arr_data["NamaGudang"][$KdGudang] = $Keterangan;
    } 
    
    if($v_date_from=="")
    {
        $v_date_from = "01/".date("m/Y");
    }

    if($v_date_to=="")
    {
        $v_date_to = date("t/m/Y");
    }
     
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report_stock_opname.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        //document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  
   
    </script>
    
    <style>
        .link_pop{
            text-decoration: underline;
            color: black;
        }
        
        .link_pop:hover{
            text-decoration: none;
            color: #222222;
        }
        
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		<br/>
		
        <form method="POST" name="theform" id="theform">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
                    <tr class="title_table">
                        <td colspan="100%"><?php echo $modul; ?></td>
                    </tr>
                    
                    
                   <tr>
                        <td class="title_table" width="150">Tanggal</td>
                        <td>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </td>
                    </tr>
                   
                    
                    <tr>
                        <td class="title_table">Gudang</td>
                        <td>: 
                            <select class="form-control-new" name="v_KdGudang" id="v_KdGudang" style="width: 200px;">
                                <?php 
                                    foreach($arr_data["list_Gudang"] as $KdGudang=>$val)
                                    {
                                        $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                        
                                        $selected = "";
                                        if($v_KdGudang==$KdGudang)
                                        {
                                            $selected = "selected='selected'";
                                        }
                                        ?>
                                            <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>        
                                        <?php
                                    }
                                ?>
                            </select>
                        </td>
                     </tr>
                    
                    
                      <tr>
                        <td class="title_table">Keyword</td>
                        <td>: 
                            <input type="text" class="form-control-new" name="v_keyword" id="v_keyword" style="width: 200px;" value="<?php echo $v_keyword; ?>">
                        </td>
                     </tr>
                    
                    
                     <tr>
                        <td class="title_table">Urut Berdasarkan</td>
                        <td>: 
                            <select class="form-control-new" name="v_sort_by" id="v_sort_by" style="width: 200px;">
                                <option <?php if($v_sort_by=="PCode") echo "selected='selected'"; ?> value="PCode">PCode</option>
                                <option <?php if($v_sort_by=="NamaLengkap") echo "selected='selected'"; ?> value="NamaLengkap">Nama Barang</option>
                            </select>
                        </td>
                     </tr>
                      
                      
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>

				
			</table> 
			<br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    
                    $where_Gudang = "";
                    if($v_KdGudang!="")
                    {
                        $where_Gudang = " AND `Gudang`.KdGudang = '".$v_KdGudang."' ";    
                    }
                    
                    $arr_keyword[0] = "masterbarang.PCode";    
                    $arr_keyword[1] = "masterbarang.NamLengkap";    
                    $arr_keyword[2] = "masterbarang.Barcode1";      
                    $arr_keyword[3] = "masterbarang.NamaStruk";      
                    
                    $where_keyword = "";
                    if($v_keyword)
                    {
                        $where_search_keyword = search_keyword($v_keyword, $arr_keyword);
                        $where_keyword = $where_search_keyword;
                    }
                    
                    $counter = 0;
                    $q = "
                            SELECT
                                opname_header.NoDokumen,    
                                opname_header.TglDokumen,
                                opname_header.Keterangan,
                                masterbarang.PCode,
                                masterbarang.NamaLengkap,
                                opname_detail.QtyFisik,
                                opname_detail.QtyProgram,
                                masterbarang.Satuan1
                            FROM    
                                opname_header
                                INNER JOIN opname_detail ON
                                    opname_header.NoDokumen = opname_detail.NoDokumen
                                    AND opname_header.TglDokumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                                INNER JOIN masterbarang ON
                                    opname_detail.PCode = masterbarang.PCode
                                INNER JOIN gudang ON
                                    gudang.KdGudang = opname_header.KdGudang
                            WHERE
                                1
                                ".$where_Gudang."
                                ".$where_keyword."
                            ORDER BY
                                opname_header.TglDokumen ASC,
                                opname_header.NoDokumen ASC,
                                ".$v_sort_by." ASC
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list(
                            $NoDokumen,    
                            $TglDokumen,
                            $Keterangan,
                            $PCode,
                            $NamaLengkap,
                            $QtyFisik,
                            $QtyProgram,
                            $Satuan1 
                        ) = $row;
                        
                        $arr_data["list_data"][$counter] = $counter;
                        
                        $arr_data["data_NoDokumen"][$counter] = $NoDokumen;
                        $arr_data["data_TglDokumen"][$counter] = $TglDokumen;
                        $arr_data["data_Keterangan"][$counter] = $Keterangan;
                        $arr_data["data_PCode"][$counter] = $PCode;
                        $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                        $arr_data["data_QtyFisik"][$counter] = $QtyFisik;
                        $arr_data["data_QtyProgram"][$counter] = $QtyProgram;
                        $arr_data["data_Satuan1"][$counter] = $Satuan1;
                        
                        $counter++;
                    }
                    
                    
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="10">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="10">REPORT STOCK OPNAME</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="10">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    
                    ?>
                    <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                            <tr class="title_table" style="font-weight: bold;">
                                <td style="width: 30px;">No</td>
                                <td>No Dokumen</td>
                                <td>Tanggal</td>
                                <td>Keterangan</td>
                                <td>PCode</td>
                                <td>Nama Lengkap</td>
                                <td style="text-align: right;">Qty Fisik</td>
                                <td style="text-align: right;">Qty Program</td>
                                <td style="text-align: right;">Selisih</td>
                                <td>Satuan</td>
                            </tr>
                        
                        <tbody style="color: black;">
                            <?php 
                                $no = 1;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $NoDokumen = $arr_data["data_NoDokumen"][$counter];
                                    $TglDokumen = $arr_data["data_TglDokumen"][$counter];
                                    $Keterangan = $arr_data["data_Keterangan"][$counter];
                                    $PCode = $arr_data["data_PCode"][$counter];
                                    $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                                    $QtyFisik = $arr_data["data_QtyFisik"][$counter];
                                    $QtyProgram = $arr_data["data_QtyProgram"][$counter];
                                    $Satuan1 = $arr_data["data_Satuan1"][$counter]; 
                                    
                                    $diff = $QtyFisik - $QtyProgram;
                                    
                                    ?>
                                    <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $NoDokumen; ?></td>
                                        <td style="text-align: center"><?php echo format_show_date($TglDokumen); ?></td>
                                        <td><?php echo $Keterangan; ?></td>
                                        <td style="mso-number-format: '\@'"><?php echo $PCode; ?></td>
                                        <td><?php echo $NamaLengkap; ?></td>
                                        <td style="text-align: right"><?php echo format_number($QtyFisik, 2, ",", "."); ?></td>
                                        <td style="text-align: right"><?php echo format_number($QtyProgram, 2, ",", "."); ?></td>
                                        <td style="text-align: right"><?php echo format_number($diff, 2, ",", "."); ?></td>
                                        <td><?php echo $Satuan1; ?></td>
                                    </tr> 
                                    <?php
                                    
                                    $arr_total["fisik"] += $QtyFisik;
                                    $arr_total["program"] += $QtyProgram;
                                    $arr_total["diff"] += $diff;
                                    $no++;
                                }
                            ?>  
                        </tbody>
                        
                        <tr style="text-align: right; font-weight: bold; color: black;">
                            <td colspan="6">TOTAL</td>
                            <td style="text-align: right"><?php echo format_number($arr_total["fisik"], 2, ",", "."); ?></td>
                            <td style="text-align: right"><?php echo format_number($arr_total["program"], 2, ",", "."); ?></td>
                            <td style="text-align: right"><?php echo format_number($arr_total["diff"], 2, ",", "."); ?></td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <?php
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
			
		
		</div>
		
		</form>
		
<?php 
                    include("footer.php"); 
                }
?>