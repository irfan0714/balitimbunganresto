<?php               
    include("header.php");
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    if(!isset($_POST["v_user"])){ $v_user = isset($_POST["v_user"]); } else { $v_user = $_POST["v_user"]; }
    if(!isset($_POST["v_KdDivisi"])){ $v_KdDivisi = isset($_POST["v_KdDivisi"]); } else { $v_KdDivisi = $_POST["v_KdDivisi"]; }
    if(!isset($_POST["v_NoStruk"])){ $v_NoStruk = isset($_POST["v_NoStruk"]); } else { $v_NoStruk = $_POST["v_NoStruk"]; }
    if(!isset($_POST["v_group_by"])){ $v_group_by = isset($_POST["v_group_by"]); } else { $v_group_by = $_POST["v_group_by"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Compliment";
    
    
    if($v_date_from=="")
    {
        $v_date_from = date("d/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("d/m/Y");
    }
    
    $q = "
            SELECT
                divisireport.KdDivisiReport,
                divisireport.NamaDivisiReport
            FROM
                divisireport
            WHERE
                1
            ORDER BY
                KdDivisiReport
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdDivisi, $NamaDivisi) = $row;
        
        $arr_data["list_divisi"][$KdDivisi] = $KdDivisi;
        $arr_data["NamaDivisi"][$KdDivisi] = $NamaDivisi;
    }
    
    $q = "
            SELECT
                compliment.nik,
                compliment.keterangan
            FROM    
                compliment
            WHERE
                1
                ".$where_nik."
            ORDER BY
                compliment.keterangan ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($nik, $keterangan) = $row;
        
        $arr_data["list_user"][$nik] = $nik;
        $arr_data["user_nik"][$nik] = $keterangan;
    } 
    
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-compliment.xls");
        header("Content-type: application/vnd.ms-excel");    
    }
    else
    {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  
    </script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
   
   <?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
      <ol class="breadcrumb bc-3">
         <li>
            <a href="index.php">
               <i class="entypo-home"></i>Home
            </a>
         </li>
         <li></li>
         <li class="active"><strong><?php echo $modul; ?></strong></li>
      </ol>
      
      <hr/>
      <br/>
      
        <form method="POST" name="theform" id="theform">
      
      <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
         
         <table class="table table-bordered responsive">
              <thead>
                <tr>
                    <th>Group By</th>
                    <th>: 
                        <select name="v_group_by" id="v_group_by" class="form-control-new" style="width: 200px;">
                            <option <?php if($v_group_by=="Transaksi") echo "selected='selected'"; ?> value="Transaksi">Transaksi</option>
                            <option <?php if($v_group_by=="Detail") echo "selected='selected'"; ?> value="Detail">Detail</option>
                            <option <?php if($v_group_by=="Rekap Barang") echo "selected='selected'"; ?> value="Rekap Barang">Rekap Barang</option>
                        </select>
                    </th>
                </tr>
                
               <tr>
                  <th width="100">Tanggal</th>
                  <th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                  </th>
               </tr>
               
               <tr>
                        <th>User</th>
                        <th>: 
                            <select class="form-control-new" style="width: 200px;" name="v_user" id="v_user">
                                <option value="">Semua</option>
                                <?php 
                                    foreach($arr_data["list_user"] as $nik=>$val)
                                    {
                                        $user_nik = $arr_data["user_nik"][$nik];
                                        
                                        $selected = "";
                                        if($nik==$v_user)
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $nik; ?>"><?php echo $user_nik; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </th>
                    </tr>
                    
                    <tr>
                        <th>Store</th>
                        <th>: 
                            <select class="form-control-new" style="width: 200px;" name="v_KdDivisi" id="v_KdDivisi">
                                <option value="">Semua</option>
                                <?php 
                                    foreach($arr_data["list_divisi"] as $KdDivisi=>$val)
                                    {
                                        $NamaDivisi = $arr_data["NamaDivisi"][$KdDivisi];
                                        
                                        $selected = "";
                                        if($KdDivisi==$v_KdDivisi)
                                        {
                                            $selected = "selected='selected'";
                                        }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdDivisi; ?>"><?php echo $NamaDivisi; ?></option>
                                <?php 
                                    }
                                ?>
                            </select>
                        </th>
                    </tr>
                    
                    <tr>
                        <th>No Struk</th>
                        <th>: <input type="text" class="form-control-new" style="width: 200px;" name="v_NoStruk" id="v_NoStruk" value="<?php echo $v_NoStruk; ?>"></th>
                    </tr>
                    
                    
                 
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
            </thead>
            
         </table> 
         <br><br>
            
            <?php 
    }
            ?>
            
            <?php 
                if($btn_submit || $btn_excel)
                {
                    
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    else
                    {
                        $where_date = " AND `transaksi_header`.Tanggal BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";    
                    }
                    
                    $where_user = "";
                    if($v_user)
                    {
                        $where_user = " AND transaksi_header.KdCustomer = '".$v_user."' ";
                    }
                    
                    $where_KdDivisi = "";
                    if($v_KdDivisi)
                    {
                        $where_KdDivisi = " AND dr.kddivisireport = '".$v_KdDivisi."' ";
                    }
                    
                    $where_NoStruk = "";
                    if($v_NoStruk)
                    {
                        $where_NoStruk = " AND transaksi_header.NoStruk = '".save_char($v_NoStruk)."' ";
                    }

                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                    
                    if($v_group_by=="Transaksi")
                    {
                        $colspan = 7;
                    }
                    else if($v_group_by=="Detail")
                    {
                        $colspan = 12;
                    }
                    else if($v_group_by=="Rekap Barang")
                    {
                        $colspan = 6;
                    }
                    
                    if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="7">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="7">REPORT COMPLIMENT <?php echo $v_group_by; ?></td>   
                            </tr>
                            
                            <tr>
                                <td colspan="7">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    }
                        
                    
                    if($v_group_by=="Transaksi")
                    {
                        $counter = 1;
                        $q = "
                                SELECT
                                    `transaksi_header`.`NoStruk`,
                                    `transaksi_header`.`Tanggal`,
                                    `transaksi_header`.`Waktu`,
                                    `transaksi_header`.`KdCustomer`,
                                    `transaksi_header`.`Kasir`,
                                    `transaksi_header`.`TotalNilai`,
                                    `transaksi_header`.`TotalBayar`,    
                                    `transaksi_header`.`Discount`,
                                    `transaksi_header`.`TAX`,
                                    `transaksi_header`.`Ttl_Charge`,
                                    tbl_detail.total_detail,
                                    tbl_detail.disc_detail,
                                    (tbl_detail.total_detail + `transaksi_header`.`Ttl_Charge` + `transaksi_header`.`TAX`) AS grandtotal_detail
                                FROM
                                    `transaksi_header`
                                    inner JOIN
                                    (
                                        SELECT
                                            `transaksi_detail`.`NoStruk`,
                                            SUM(`transaksi_detail`.`Qty` * `transaksi_detail`.`Harga`)  AS total_detail,
                                            SUM( IFNULL(`transaksi_detail`.`Disc1`,0) + IFNULL(`transaksi_detail`.`Disc2`,0) + IFNULL(`transaksi_detail`.`Disc3`,0) + IFNULL(`transaksi_detail`.`Disc4`,0)) AS disc_detail
                                        FROM
                                            transaksi_detail inner join masterbarang b on transaksi_detail.pcode=b.pcode
                                            inner join subdivisi s on b.kdsubdivisi=s.kdsubdivisi
                                            inner join divisireport dr on s.kddivisireport=dr.kddivisireport
                                            where 1 ".$where_KdDivisi."
                                        GROUP BY
                                            `transaksi_detail`.`NoStruk`
                                    )AS tbl_detail ON
                                        tbl_detail.NoStruk = `transaksi_header`.`NoStruk`
                                        AND `transaksi_header`.DPP*1 = '0'
                                WHERE
                                    1
                                    ".$where_date."
                                    ".$where_user."
                                    ".$where_NoStruk."
                                    AND `transaksi_header`.`TotalBayar` *1 = '0'
                                    AND (`transaksi_header`.`Tunai`*1 + `transaksi_header`.`KKredit`*1 + `transaksi_header`.`KDebit`*1) = '0' 
                                    AND `transaksi_header`.Status = '1'
                                    and `transaksi_header`.Discount > 0
                                ORDER BY
                                    `transaksi_header`.`Tanggal` ASC,
                                    `transaksi_header`.`Waktu` ASC     
                        ";
                        //echo $q;
                        //die();
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $NoStruk,
                                $Tanggal,
                                $Waktu,
                                $KdCustomer,
                                $Kasir,
                                $TotalNilai,
                                $TotalBayar,
                                $Discount,
                                $TAX,
                                $Ttl_Charge,
                                $total_detail,
                                $disc_detail,
                                $grandtotal_detail
                            ) = $row;
                            
                            $arr_data["list_data"][$counter] = $counter;
                            
                            $arr_data["data_NoStruk"][$counter] = $NoStruk;
                            $arr_data["data_Tanggal"][$counter] = $Tanggal;
                            $arr_data["data_Waktu"][$counter] = $Waktu;
                            $arr_data["data_KdCustomer"][$counter] = $KdCustomer;
                            $arr_data["data_Kasir"][$counter] = $Kasir;
                            $arr_data["data_TotalNilai"][$counter] = $TotalNilai;
                            $arr_data["data_TotalBayar"][$counter] = $TotalBayar;
                            $arr_data["data_Discount"][$counter] = $Discount;
                            $arr_data["data_TAX"][$counter] = $TAX;
                            $arr_data["data_Ttl_Charge"][$counter] = $Ttl_Charge;
                            $arr_data["data_total_detail"][$counter] = $total_detail;
                            $arr_data["data_disc_detail"][$counter] = $disc_detail;
                            $arr_data["data_grandtotal_detail"][$counter] = $grandtotal_detail;
                            
                            $counter++;
                        } 
                        
                    ?>
                        <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                                <tr class="title_table">
                                    <td width="30">No</td>
                                    <td>No Struk</td>
                                    <td>Tanggal</td>
                                    <td>Waktu</td>
                                    <td>Kasir</td>
                                    <td>User</td>
                                    <td align="right">Compliment</td>
                                </tr>
                            <tbody style="color: black;">
                            <?php 
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $NoStruk = $arr_data["data_NoStruk"][$counter];
                                    $Tanggal = $arr_data["data_Tanggal"][$counter];
                                    $Waktu = $arr_data["data_Waktu"][$counter];
                                    $KdCustomer = $arr_data["data_KdCustomer"][$counter];
                                    $Kasir = $arr_data["data_Kasir"][$counter];
                                    $TotalNilai = $arr_data["data_TotalNilai"][$counter];
                                    $TotalBayar = $arr_data["data_TotalBayar"][$counter];
                                    $Discount = $arr_data["data_Discount"][$counter];
                                    $TAX = $arr_data["data_TAX"][$counter];
                                    $Ttl_Charge = $arr_data["data_Ttl_Charge"][$counter];
                                    $total_detail = $arr_data["data_total_detail"][$counter];
                                    $disc_detail = $arr_data["data_disc_detail"][$counter];
                                    $grandtotal_detail = $arr_data["data_grandtotal_detail"][$counter];
                                    $user_nik = $arr_data["user_nik"][$KdCustomer];
                                    
                                   
                            ?>
                                <tr style="<?php echo $bg_color; ?>" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $NoStruk; ?></td>
                                    <td><?php echo format_show_date($Tanggal); ?></td>
                                    <td><?php echo $Waktu; ?></td>
                                    <td><?php echo $Kasir; ?></td>
                                    <td><?php echo $user_nik; ?></td>
                                    <td align="right"><?php echo format_number($disc_detail, 0, ",", "."); ?></td>
                                </tr>
                            <?php 
                                    $arr_total["Discount"] += $disc_detail;
                                }
                            ?>
                            </tbody>
                            
                                <tr style="text-align: right; font-weight: bold; color: black;">
                                    <td colspan="6">Grand Total</td>
                                    <td align="right"><?php echo format_number($arr_total["Discount"], 0, ",", "."); ?></td>
                                </tr>
                        </table>
                    <?php
                    }
                    else if($v_group_by=="Detail")
                    {
                        $counter = 1;
                        $q = "
                                SELECT
                                    `transaksi_header`.`NoStruk`,
                                    `transaksi_header`.`Tanggal`,
                                    `transaksi_header`.`Waktu`,
                                    `transaksi_header`.`KdCustomer`,
                                    `transaksi_header`.`Kasir`,
                                    `transaksi_header`.`TotalNilai`,
                                    `transaksi_header`.`TotalBayar`,    
                                    `transaksi_header`.`Discount`,
                                    `transaksi_header`.`TAX`,
                                    `transaksi_header`.`Ttl_Charge`,
                                    masterbarang.PCode,
                                    masterbarang.NamaLengkap,
                                    dr.NamaDivisiReport as NamaDivisi,
                                    masterbarang.PCode,
                                    transaksi_detail.Qty,
                                    transaksi_detail.Harga
                                FROM
                                    `transaksi_header`
                                    INNER JOIN `transaksi_detail` ON
                                        `transaksi_detail`.`NoStruk` = `transaksi_header`.`NoStruk`
                                        ".$where_date." 
                                        AND `transaksi_header`.DPP*1 = '0'
                                    INNER JOIN `masterbarang` ON
                                        `masterbarang`.`PCode` = `transaksi_detail`.`PCode`
                                    inner join subdivisi s on masterbarang.kdsubdivisi=s.kdsubdivisi
                                    inner join divisireport dr on s.kddivisireport=dr.kddivisireport
                                             ".$where_KdDivisi."
                                WHERE
                                    1
                                    ".$where_user."
                                    ".$where_NoStruk."
                                    AND `transaksi_header`.`TotalBayar` *1 = '0'
                                    AND (`transaksi_header`.`Tunai`*1 + `transaksi_header`.`KKredit`*1 + `transaksi_header`.`KDebit`*1) = '0' 
                                    AND `transaksi_header`.Status = '1' 
                                    and `transaksi_header`.Discount > 0
                                ORDER BY
                                    `transaksi_header`.`Tanggal` ASC,
                                    `transaksi_header`.`Waktu` ASC
                        ";
                        //echo $q;
                        //die();
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $NoStruk,
                                $Tanggal,
                                $Waktu,
                                $KdCustomer,
                                $Kasir,
                                $TotalNilai,
                                $TotalBayar,    
                                $Discount,
                                $TAX,
                                $Ttl_Charge,
                                $PCode,
                                $NamaLengkap,
                                $NamaDivisi,
                                $PCode,
                                $Qty,
                                $Harga 
                            ) = $row;
                            
                            $arr_data["list_data"][$counter] = $counter;
                            
                            $arr_data["data_NoStruk"][$counter] = $NoStruk;
                            $arr_data["data_Tanggal"][$counter] = $Tanggal;
                            $arr_data["data_Waktu"][$counter] = $Waktu;
                            $arr_data["data_KdCustomer"][$counter] = $KdCustomer;
                            $arr_data["data_Kasir"][$counter] = $Kasir;
                            $arr_data["data_TotalNilai"][$counter] = $TotalNilai;
                            $arr_data["data_TotalBayar"][$counter] = $TotalBayar;
                            $arr_data["data_Discount"][$counter] = $Discount;
                            $arr_data["data_TAX"][$counter] = $TAX;
                            $arr_data["data_Ttl_Charge"][$counter] = $Ttl_Charge;
                            $arr_data["data_PCode"][$counter] = $PCode;
                            $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                            $arr_data["data_NamaDivisi"][$counter] = $NamaDivisi;
                            $arr_data["data_Qty"][$counter] = $Qty;
                            $arr_data["data_Harga"][$counter] = $Harga;
                            
                            $counter++;
                        } 
                        ?>
                        <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                                <tr class="title_table">
                                    <td width="30">No</td>
                                    <td>No Struk</td>
                                    <td>Tanggal</td>
                                    <td>Waktu</td>
                                    <td>Kasir</td>
                                    <td>User</td>
                                    <td align="right">Compliment</td>
                                    <td>PCode</td>
                                    <td>Nama Lengkap</td>
                                    <td>Nama Divisi</td>
                                    <td align="right">Qty</td>
                                    <td align="right">Harga</td>
                                    <td align="right">Subtotal</td>
                                </tr>
                                <tbody style="color: black;">
                                <?php
                                $NoStruk_prev = ""; 
                                $no = 0;
                                $compliment_struk = 0;
                                foreach($arr_data["list_data"] as $counter=>$val)
                                {
                                    $NoStruk = $arr_data["data_NoStruk"][$counter];
                                    $Tanggal = $arr_data["data_Tanggal"][$counter];
                                    $Waktu = $arr_data["data_Waktu"][$counter];
                                    $KdCustomer = $arr_data["data_KdCustomer"][$counter];
                                    $Kasir = $arr_data["data_Kasir"][$counter];
                                    $TotalNilai = $arr_data["data_TotalNilai"][$counter];
                                    $TotalBayar = $arr_data["data_TotalBayar"][$counter];
                                    $Discount = $arr_data["data_Discount"][$counter];
                                    $TAX = $arr_data["data_TAX"][$counter];
                                    $Ttl_Charge = $arr_data["data_Ttl_Charge"][$counter];
                                    $PCode = $arr_data["data_PCode"][$counter];
                                    $NamaLengkap = $arr_data["data_NamaLengkap"][$counter];
                                    $namadivisi = $arr_data["data_NamaDivisi"][$counter];
                                    $Qty = $arr_data["data_Qty"][$counter];
                                    $Harga = $arr_data["data_Harga"][$counter];
                                    $subtotal = $Qty * $Harga; 
                                    $user_nik = $arr_data["user_nik"][$KdCustomer];
                                    
                                    $no_echo = "";
                                    $NoStruk_echo = "";
                                    $Tanggal_echo = "";
                                    $Waktu_echo = "";
                                    $Kasir_echo = "";
                                    $user_nik_echo = "";
                                    $Discount_echo = ""; 
                                    if($NoStruk_prev!=$NoStruk)
                                    {
                                        $no++;
                                        $no_echo = $no;
                                        $NoStruk_echo = $NoStruk;
                                        $Tanggal_echo = format_show_date($Tanggal);
                                        $Waktu_echo = $Waktu;
                                        $Kasir_echo = $Kasir;
                                        $user_nik_echo = $user_nik;
                                        $Discount_echo = format_number($Discount, 0, ",", ".");
                                        $compliment_struk += $Discount;
                                    }
                                   
                            ?>
                                <tr style="<?php echo $bg_color; ?>" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                    <td><?php echo $no_echo; ?></td>
                                    <td><?php echo $NoStruk_echo; ?></td>
                                    <td><?php echo $Tanggal_echo; ?></td>
                                    <td><?php echo $Waktu_echo; ?></td>
                                    <td><?php echo $Kasir_echo; ?></td>
                                    <td><?php echo $user_nik_echo; ?></td>
                                    <td align="right"><?php echo $Discount_echo; ?></td>
                                    <td><?php echo $PCode; ?></td>
                                    <td><?php echo $NamaLengkap; ?></td>
                                    <td><?php echo $namadivisi; ?></td>
                                    <td align="right"><?php echo format_number($Qty, 0, ",", "."); ?></td>
                                    <td align="right"><?php echo format_number($Harga, 0, ",", "."); ?></td>
                                    <td align="right"><?php echo format_number($subtotal, 0, ",", "."); ?></td>
                                </tr>
                            <?php   
                                    $NoStruk_prev = $NoStruk;
                                    $arr_total["subtotal"] += $subtotal;
                                }
                            ?>
                                </tbody>
                                
                                
                                <tr style="text-align: right; color: black; font-weight: bold;">
                                    <td colspan="6">Total</td>
                                    <td align="right"><?php echo format_number($compliment_struk, 0, ",", "."); ?></td>
                                    <td colspan="5">&nbsp;</td>
                                    <td align="right"><?php echo format_number($arr_total["subtotal"], 0, ",", "."); ?></td>
                                </tr>
                        </table>
                        <?php    
                    }
                    else if($v_group_by=="Rekap Barang")
                    {
                        $counter = 1;
                        $q = "
                                SELECT
                                    `transaksi_header`.`NoStruk`,
                                    `transaksi_header`.`Tanggal`,
                                    `transaksi_header`.`Waktu`,
                                    `transaksi_header`.`KdCustomer`,
                                    `transaksi_header`.`Kasir`,
                                    `transaksi_header`.`TotalNilai`,
                                    `transaksi_header`.`TotalBayar`,    
                                    `transaksi_header`.`Discount`,
                                    `transaksi_header`.`TAX`,
                                    `transaksi_header`.`Ttl_Charge`,
                                    masterbarang.PCode,
                                    masterbarang.NamaLengkap,
                                    masterbarang.PCode,
                                    dr.NamaDivisiReport as NamaDivisi,
                                    transaksi_detail.Qty,
                                    transaksi_detail.Harga
                                FROM
                                    `transaksi_header`
                                    INNER JOIN `transaksi_detail` ON
                                        `transaksi_detail`.`NoStruk` = `transaksi_header`.`NoStruk`
                                        ".$where_date." AND `transaksi_header`.DPP*1 = '0'
                                    INNER JOIN `masterbarang` ON
                                        `masterbarang`.`PCode` = `transaksi_detail`.`PCode`
                                     inner join subdivisi s on masterbarang.kdsubdivisi=s.kdsubdivisi
                                    inner join divisireport dr on s.kddivisireport=dr.kddivisireport
                                             ".$where_KdDivisi."
                                WHERE
                                    1
                                    ".$where_user."
                                    ".$where_NoStruk."
                                    AND `transaksi_header`.`TotalBayar` *1 = '0'
                                    AND (`transaksi_header`.`Tunai`*1 + `transaksi_header`.`KKredit`*1 + `transaksi_header`.`KDebit`*1) = '0' 
                                    AND `transaksi_header`.Status = '1'
                                    and `transaksi_header`.Discount > 0 
                                ORDER BY
                                    masterbarang.NamaLengkap ASC
                        ";
                        
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            list(
                                $NoStruk,
                                $Tanggal,
                                $Waktu,
                                $KdCustomer,
                                $Kasir,
                                $TotalNilai,
                                $TotalBayar,    
                                $Discount,
                                $TAX,
                                $Ttl_Charge,
                                $PCode,
                                $NamaLengkap,
                                $PCode,
                                $NamaDivisi,
                                $Qty,
                                $Harga 
                            ) = $row;
                            
                            $arr_data["list_data"][$counter] = $counter;
                            
                            $arr_data["data_NoStruk"][$counter] = $NoStruk;
                            $arr_data["data_Tanggal"][$counter] = $Tanggal;
                            $arr_data["data_Waktu"][$counter] = $Waktu;
                            $arr_data["data_KdCustomer"][$counter] = $KdCustomer;
                            $arr_data["data_Kasir"][$counter] = $Kasir;
                            $arr_data["data_TotalNilai"][$counter] = $TotalNilai;
                            $arr_data["data_TotalBayar"][$counter] = $TotalBayar;
                            $arr_data["data_Discount"][$counter] = $Discount;
                            $arr_data["data_TAX"][$counter] = $TAX;
                            $arr_data["data_Ttl_Charge"][$counter] = $Ttl_Charge;
                            $arr_data["data_PCode"][$counter] = $PCode;
                            $arr_data["data_NamaLengkap"][$counter] = $NamaLengkap;
                            $arr_data["data_NamaDivisi"][$counter] = $NamaDivisi;
                            $arr_data["data_Qty"][$counter] = $Qty;
                            $arr_data["data_Harga"][$counter] = $Harga;
                            
                            $arr_data["list_PCode"][$PCode] = $PCode;
                            $arr_data["subtotal"][$PCode] += ($Harga*$Qty);
                            $arr_data["qty"][$PCode] += $Qty;
                            
                            $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
                            $arr_data["NamaDivisi"][$PCode] = $NamaDivisi;
                            
                            $counter++;
                        } 
                        ?>
                        <table class="table table-bordered responsive" border="<?php echo $table_border; ?>">
                            <tr class="title_table">
                                <td width="30">No</td>
                                <td>PCode</td>
                                <td>Nama Lengkap</td>
                                <td>Nama Divisi</td>
                                <td align="right">Qty</td>
                                <td align="right">Harga</td>
                                <td align="right">Subtotal</td>
                            </tr>
                            <tbody style="color: black;">    
                                <?php 
                                    $no = 1;
                                    foreach($arr_data["list_PCode"] as $PCode => $val)
                                    {
                                        $subtotal = $arr_data["subtotal"][$PCode];
                                        $Qty = $arr_data["qty"][$PCode];
                                        $Harga = $subtotal/$Qty;
                            
                                        $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                                        $NamaDivisi = $arr_data["NamaDivisi"][$PCode];
                                        ?>
                                        <tr style="<?php echo $bg_color; ?>" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $PCode; ?></td>
                                            <td><?php echo $NamaLengkap; ?></td>
                                            <td><?php echo $NamaDivisi; ?></td>
                                            <td align="right"><?php echo format_number($Qty, 2, ",", "."); ?></td>
                                            <td align="right"><?php echo format_number($Harga, 2, ",", "."); ?></td>
                                            <td align="right"><?php echo format_number($subtotal, 2, ",", "."); ?></td> 
                                        </tr>
                                        <?php
                                        $no++;
                                        $arr_total["subtotal"] += $subtotal;
                                    }
                                    
                                ?>
                            </tbody>
                            
                            <tr style="text-align: right; color: black; font-weight: bold;">
                                <td colspan="6">Total</td>
                                <td><?php echo format_number($arr_total["subtotal"], 2, ",", "."); ?></td>
                            </tr>
                        </table>
                        <?php    
                    }
                }
                
                
                if(!$btn_excel)
                {
                    
            ?>
         
      
      </div>
      
      </form>
      
<?php 
                    include("footer.php"); 
                }
?>