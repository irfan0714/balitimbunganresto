<?php
include("header.php");
include("function_email.php");
                               
$modul            = "Purchase Order";
$file_current     = "npm_purchase_order.php";
$file_name        = "npm_purchase_order_data.php";

//$ses_login = "charles3105";

function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}
          
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
		$search_keyword        = trim($_GET["search_keyword"]);
        $search_supplier       = trim($_GET["search_supplier"]);
        $search_gudang         = trim($_GET["search_gudang"]);
		$v_date_from           = trim($_GET["v_date_from"]);
		$v_date_to             = trim($_GET["v_date_to"]);
		$search_status         = trim($_GET["search_status"]);
		$v_NoDokumen_curr      = trim($_GET["v_NoDokumen_curr"]);
        //echo $v_NoDokumen_curr;               
		
		$where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "trans_order_barang_header.NoDokumen";    
            $arr_keyword[1] = "trans_order_barang_header.NoPr";    
            $arr_keyword[2] = "trans_order_barang_header.Keterangan";      
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        $where .= " AND trans_order_barang_header.TglDOkumen BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."' ";
        
        if($search_supplier)
        {
            $where .= " AND trans_order_barang_header.KdSupplier = '".$search_supplier."'";
        }
        
        if($search_gudang)
        {
            $where .= " AND trans_order_barang_header.KdGudang = '".$search_gudang."'";
        }
        
        if($search_status=="1")
        {   //charles
            $where .= " AND trans_order_barang_header.total <= '5000000' AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='0'";
        }else if($search_status=="2")
        {   //charles
            $where .= " AND trans_order_barang_header.total between '5000001' AND '10000000' AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='0'";
        }else if($search_status=="3")
        {   //charles
            $where .= " AND trans_order_barang_header.total > '10000000' AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='0'";
        }else if($search_status=="4")
        {   //charles
            $where .= " AND trans_order_barang_header.Approval_Status='1'";
        }else if($search_status=="5")
        {   //charles
            $where .= " AND trans_order_barang_header.Approval_Status='2'";
        }else if($search_status=="6")
        {   //charles
            $where .= " AND trans_order_barang_header.Approval_Status='0'";
        }
        else if($search_status=="7")
        {   //charles
            $where .= " AND trans_order_barang_header.Approval_By='charles3105'";
        }else if($search_status=="8")
        {   //charles
            $where .= " AND trans_order_barang_header.Approval_By='trisno1402'";
        }else if($search_status=="9")
        {   //charles
            $where .= " AND trans_order_barang_header.Approval_By='henny2905'";
        }
        else if($search_status=="10")
        {   //charles
            $where .= " AND trans_order_barang_header.Status='9' AND trans_order_barang_header.Approval_By='charles3105'";
        }
        else if($search_status=="11")
        {   //charles
            $where .= " AND trans_order_barang_header.Status='9' AND trans_order_barang_header.Approval_By='trisno1402'";
        }
        else if($search_status=="12")
        {   //charles
            $where .= " AND trans_order_barang_header.Status='9' AND trans_order_barang_header.Approval_By='henny2905'";
        }
        else{
			$where .="";
		}
        
        // select query all
        {
            $counter = 1;
			$q="
				SELECT 
				  ".$db["master"].".trans_order_barang_header.NoDokumen,
				  ".$db["master"].".trans_order_barang_header.TglDokumen,
                  ".$db["master"].".trans_order_barang_header.NoPr,
				  ".$db["master"].".supplier.KdSupplier,
				  ".$db["master"].".supplier.Nama AS NamaSupplier,
				  ".$db["master"].".gudang.KdGudang,
				  ".$db["master"].".gudang.Keterangan AS NamaGudang,
				  ".$db["master"].".trans_order_barang_header.currencycode,
				  ".$db["master"].".trans_order_barang_header.Total,
				  ".$db["master"].".trans_order_barang_header.status,
				  ".$db["master"].".trans_order_barang_header.Approval_by,
                  ".$db["master"].".trans_order_barang_header.Approval_Date,
                  ".$db["master"].".trans_order_barang_header.Approval_Status,
                  ".$db["master"].".trans_order_barang_header.Approval_Remarks
				FROM
				  ".$db["master"].".trans_order_barang_header 
				  INNER JOIN ".$db["master"].".supplier 
				    ON ".$db["master"].".trans_order_barang_header.KdSupplier = ".$db["master"].".supplier.KdSupplier 
				  INNER JOIN ".$db["master"].".gudang 
				    ON ".$db["master"].".trans_order_barang_header.KdGudang = ".$db["master"].".gudang.KdGudang 
				WHERE 1 
					".$where."
				ORDER BY 
					".$db["master"].".trans_order_barang_header.TglDokumen DESC,
					".$db["master"].".trans_order_barang_header.NoDokumen ASC
			";
			//echo $q;
			$sql=mysql_query($q);
			while($row=mysql_fetch_array($sql))
			{ 
				list(
					$NoDokumen,
					$TglDokumen,
                    $NoPr,
					$KdSupplier,
					$NamaSupplier,
					$KdGudang,
					$NamaGudang,
					$currencycode,
					$Total,
					$status,
					$Approval_by,
					$Approval_Date,
					$Approval_Status,
					$Approval_Remarks
                )=$row;
				
				$arr_data["list_data"][$counter]=$counter;
				$arr_data["data_NoDokumen"][$counter]=$NoDokumen;
				$arr_data["data_TglDokumen"][$counter]=$TglDokumen;
                $arr_data["data_NoPr"][$counter]=$NoPr;
				$arr_data["data_KdSupplier"][$counter]=$KdSupplier;
				$arr_data["data_NamaSupplier"][$counter]=$NamaSupplier;
				$arr_data["data_KdGudang"][$counter]=$KdGudang;
				$arr_data["data_NamaGudang"][$counter]=$NamaGudang;
				$arr_data["data_currencycode"][$counter]=$currencycode;
				$arr_data["data_Total"][$counter]=$Total;
				$arr_data["data_status"][$counter]=$status;
				$arr_data["data_Approval_by"][$counter]=$Approval_by;
				$arr_data["data_Approval_Date"][$counter]=$Approval_Date;
				$arr_data["data_Approval_Status"][$counter]=$Approval_Status;
				$arr_data["data_Approval_Remarks"][$counter]=$Approval_Remarks;
				
				$counter++;
			}
		}  
		
		
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_purchase_order_print.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
			<tr class="title_table">
	            <td width="30">No</td>
	            <td>No Dokumen</td>
	            <td>Tanggal</td>
                <td>No PR</td>
	            <td>Supplier</td>
	            <td>Gudang</td>     
	            <td width="15">&nbsp;</td>      
	            <td>Grand Total</td>  
	            <td>Approval</td>    
	            <td width="80">PO Status</td> 
	            <td width="80">RG Status</td>
	            <td width="80">Konfirm Terima</td>      
                <td width="15"><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></td>    
			</tr>
			
	        
		<tbody style="color: black;">
		
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        $photo_echo="";    
        foreach($arr_data["list_data"] as $counter => $val)
        {    
        	$nomor++;
        	
            $NoDokumen = $arr_data["data_NoDokumen"][$counter];
            $TglDokumen = $arr_data["data_TglDokumen"][$counter];
            $NoPr = $arr_data["data_NoPr"][$counter];
            $KdSupplier = $arr_data["data_KdSupplier"][$counter];
            $NamaSupplier = $arr_data["data_NamaSupplier"][$counter];
            $KdGudang = $arr_data["data_KdGudang"][$counter];
            $NamaGudang = $arr_data["data_NamaGudang"][$counter];
            $currencycode = $arr_data["data_currencycode"][$counter];
            $Total = $arr_data["data_Total"][$counter];
            $status = $arr_data["data_status"][$counter];
            $Approval_by = $arr_data["data_Approval_by"][$counter];
            $Approval_Date = $arr_data["data_Approval_Date"][$counter];
            $Approval_Status = $arr_data["data_Approval_Status"][$counter];
            $Approval_Remarks = $arr_data["data_Approval_Remarks"][$counter];
            
            //cek apakah PO sudah ada di RG
            $qs="
				SELECT 
				  ".$db["master"].".trans_terima_header.NoDokumen,
                  ".$db["master"].".trans_terima_header.PoNo
				FROM
				  ".$db["master"].".trans_terima_header 
				WHERE 1
				AND ".$db["master"].".trans_terima_header.PoNo='".$NoDokumen."' 
				AND ".$db["master"].".trans_terima_header.Status<>'2';
			";
			$sqls=mysql_query($qs);
			$rows=mysql_fetch_array($sqls);
			if(!empty($rows)){
				$rg_status = "<nobr><img src='images/accept.png'> ";
			}else{
				$rg_status = "<font color='red'><b>X</b></font>";
			}
			
			$qsa="
				SELECT 
				  ".$db["master"].".trans_terima_header.FlagKonfirmasi
				FROM
				  ".$db["master"].".trans_terima_header 
				WHERE 1
				AND ".$db["master"].".trans_terima_header.PoNo='".$NoDokumen."'
			";
			$sqlsa=mysql_query($qsa);
			$rowsa=mysql_fetch_array($sqlsa);
			if($rowsa['FlagKonfirmasi']=="Y"){
				$rg_status_konfirm = "<nobr><img src='images/accept.png'> ";
			}else{
				$rg_status_konfirm = "<font color='red'><b>X</b></font>";
			}
            
            if($Approval_by=="")
            {
                $approval = "waiting";
            } 
            else
            {
                if($Approval_Status==1)
                {
                    $approval = "<font color='blue'>".$Approval_by." - ".format_show_datetime($Approval_Date)."</font>";
                }
                else if($Approval_Status==2)
                {
                    $approval = "<font color='red'>".$Approval_by." - ".format_show_datetime($Approval_Date)."<br>".$Approval_Remarks."</font>";
                }
            }
            
            if($status==0)
            {
                $status_echo = "Pending";
            }
            else if($status==1)
            {
                $status_echo = "Open";
            }
            else if($status==2)
            {
                $status_echo = "<font color='red'>Void</font>";
            }
            else if($status==3)
            {
                $status_echo = "<font color='black'>Close</font>";
            }else if($status==8)
            {
                $status_echo = "<font color='green'>Tahap Batal RG</font>";
            }else if($status==9)
            {
                $status_echo = "<font color='orange'>Tahap Request Edit</font>";
            }
            
            $style_color = "";
            if($v_NoDokumen_curr==$NoDokumen)
            {
                $style_color = "background: #CAFDB5";
            }
            		
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><nobr><?php echo $NoDokumen; ?></nobr></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo format_show_date($TglDokumen); ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><nobr><?php echo $NoPr; ?></nobr></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $NamaSupplier; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $NamaGudang; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $currencycode; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="right" style=""><?php echo format_number($Total,2); ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $approval; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" style=""><?php echo $status_echo; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo $rg_status; ?></td>  
                <td onclick="CallAjaxForm('edit_data','<?php echo $NoDokumen; ?>')" align="center" style=""><?php echo $rg_status_konfirm; ?></td> 
                <td align="center">
                    &nbsp;
                    <?php 
                        if($Approval_Status==1)
                        {
                    ?>
                            <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $NoDokumen; ?>">
                    <?php 
                        }
                    ?>
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot>
            <tr>
                <td colspan="100%" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Print 1/2 Letter">Print 1/2 Letter</option>      
                        <option value="Print Letter">Print Letter</option>      
                        <option value="Close PO">Close PO</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
	</table> 
	</form>
	</div>  
   	<?php  
    }

	else if($ajax=="add_data")
	{
        $q = "
            SELECT
                ".$db["master"].".aplikasi.TglTrans
            FROM
                ".$db["master"].".aplikasi
            WHERE
                1
        ";
        $qry_aplikasi = mysql_query($q);
        $row_aplikasi = mysql_fetch_array($qry_aplikasi);
        list($TglTrans) = $row_aplikasi;
        
		$q = "
            SELECT
                ".$db["master"].".supplier.KdSupplier,
                ".$db["master"].".supplier.Nama
            FROM
                ".$db["master"].".supplier
            WHERE
                1
            ORDER BY
                ".$db["master"].".supplier.Nama ASC
        ";
        $qry_supplier = mysql_query($q);
        while($row_supplier = mysql_fetch_array($qry_supplier))
        { 
		    list($KdSupplier, $NamaSupplier) = $row_supplier;    
            
            $arr_data["list_supplier"][$KdSupplier] = $KdSupplier;
            $arr_data["NamaSupplier"][$KdSupplier] = $NamaSupplier;
            
        }
        
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
        ";
        $qry_gudang = mysql_query($q);
        while($row_gudang = mysql_fetch_array($qry_gudang))
        { 
            list($KdGudang, $NamaGudang) = $row_gudang;    
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
            
        }
        
        $q = "
            SELECT
                ".$db["master"].".mata_uang.Kd_Uang
            FROM
                ".$db["master"].".mata_uang
            WHERE
                1
            ORDER BY
                ".$db["master"].".mata_uang.id ASC
        ";
        $qry_uang = mysql_query($q);
        while($row_uang = mysql_fetch_array($qry_uang))
        { 
            list($currencycode) = $row_uang;    
            
            $arr_data["list_currency"][$currencycode] = $currencycode;
        }
        ?>
        <div class="col-md-12" align="left">
        
        	<ol class="breadcrumb title_table">
				<li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">Tanggal</td>
                <td width="40%">
                    <input type="text" class="form-control-new datepicker" name="v_TglDokumen" id="v_TglDokumen" size="10" maxlength="10" value="<?php echo format_show_date($TglTrans); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglDokumen, 'dd/mm/yyyy');">
                </td>
                
                <td class="title_table" width="200">Supplier</td>
                <td>
                    <select name="v_KdSupplier" id="v_KdSupplier" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_supplier', this.value)">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_supplier"] as $KdSupplier=>$val)
                            {
                                $NamaSupplier = $arr_data["NamaSupplier"][$KdSupplier];
                                ?>
                                <option value="<?php echo $KdSupplier; ?>"><?php echo $NamaSupplier; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td> 
                
            </tr>  

            <tr>
                <td class="title_table">Gudang</td>
                <td>
                    <select name="v_KdGudang" id="v_KdGudang" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                ?>
                                <option value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td> 
                
                <td class="title_table">TOP (Hari)</td>
                <td>
                    <input type="text" class="form-control-new" style="width: 200px;" name="v_top" id="v_top">
                </td>
                
            </tr>
                
            <tr>
                <td class="title_table">No PR</td>
                <td>
                    <input type="text" class="form-control-new" readonly="readonly" size="20" name="v_NoPr" id="v_NoPr">
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_search_pr()">&nbsp;&nbsp;<i class="entypo-search"></i></button>
                </td>
                
                <td class="title_table">Currency</td>
                <td>
                	<select name="v_currencycode" id="v_currencycode" class="form-control-new" style="width: 200px;">  
                        <?php 
                            foreach($arr_data["list_currency"] as $currencycode=>$val)
                            {
                                ?>
                                <option value="<?php echo $currencycode; ?>"><?php echo $currencycode; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
                
            </tr>
                
            <tr>
                <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new datepicker" name="v_Keterangan" id="v_Keterangan" style="width: 200px;" value=""> 
                </td>
	            
                <td class="title_table" width="150">Estimasi Terima</td>
                <td>
                	<input type="text" class="form-control-new datepicker" name="v_TglTerima" id="v_TglTerima" size="10" maxlength="10" value="">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglTerima, 'dd/mm/yyyy');">
      			</td> 
            </tr>
                
           
            
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                	<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
            
        	</table>  
        	</form>
        </div>
        <?php
		
	}
    
    else if($ajax=="edit_data")
    {                                      
		$v_NoDokumen = $_GET["v_NoDokumen"];  
       
        $q = "
                SELECT
                    *
                FROM 
                    ".$db["master"].".trans_order_barang_header
                WHERE
                    1
                    AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                LIMIT
                    0,1
        ";
		//echo $q."<br>";
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
       
        $q = "
            SELECT
                ".$db["master"].".supplier.KdSupplier,
                ".$db["master"].".supplier.Nama
            FROM
                ".$db["master"].".supplier
            WHERE
                1 AND ".$db["master"].".supplier.KdSupplier='".$arr_curr['KdSupplier']."'
            ORDER BY
                ".$db["master"].".supplier.Nama ASC
        ";
		//penembahan where kdsupplier 30032017 by irfan
        $qry_supplier = mysql_query($q);
        while($row_supplier = mysql_fetch_array($qry_supplier))
        { 
            list($KdSupplier, $NamaSupplier) = $row_supplier;    
            
            $arr_data["list_supplier"][$KdSupplier] = $KdSupplier;
            $arr_data["NamaSupplier"][$KdSupplier] = $NamaSupplier;
            
        }
		
        $q = "
            SELECT
                ".$db["master"].".gudang.KdGudang,
                ".$db["master"].".gudang.Keterangan
            FROM
                ".$db["master"].".gudang
            WHERE
                1
            ORDER BY
                ".$db["master"].".gudang.KdGudang ASC
        ";
        $qry_gudang = mysql_query($q);
        while($row_gudang = mysql_fetch_array($qry_gudang))
        { 
            list($KdGudang, $NamaGudang) = $row_gudang;    
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = $NamaGudang;
            
        }
        
        $q = "
            SELECT
                ".$db["master"].".mata_uang.Kd_Uang
            FROM
                ".$db["master"].".mata_uang
            WHERE
                1
            ORDER BY
                ".$db["master"].".mata_uang.id ASC
        ";
        $qry_uang = mysql_query($q);
        while($row_uang = mysql_fetch_array($qry_uang))
        { 
            list($currencycode) = $row_uang;    
            
            $arr_data["list_currency"][$currencycode] = $currencycode;
        }
        
        $q = "
                SELECT
                    ".$db["master"].".trans_pr_detail.Sid,    
                    ".$db["master"].".trans_pr_detail.PCode,
                    ".$db["master"].".trans_pr_detail.Qty,
                    ".$db["master"].".trans_pr_detail.QtyPcs,
                    ".$db["master"].".trans_pr_detail.Satuan,
                    ".$db["master"].".trans_pr_detail.QtyPO
                FROM
                    ".$db["master"].".trans_pr_detail
                WHERE
                    1
                    AND ".$db["master"].".trans_pr_detail.NoDokumen = '".$arr_curr["NoPr"]."'
                ORDER BY
                    ".$db["master"].".trans_pr_detail.Sid ASC
        ";
		//echo $q;
        $qry_pr = mysql_query($q);
        while($row_pr = mysql_fetch_array($qry_pr))
        { 
            list($Sid, $PCode, $Qty, $QtyPcs, $Satuan, $QtyPO) = $row_pr;    
            
            $arr_data["list_data_pr"][$Sid] = $Sid;
            $arr_data["data_pr_PCode"][$Sid] = $PCode;
            $arr_data["data_pr_Qty"][$Sid] = $Qty;
            $arr_data["data_pr_QtyPcs"][$Sid] = $QtyPcs;
            $arr_data["data_pr_Satuan"][$Sid] = $Satuan;
            $arr_data["data_pr_Qty_PO"][$Sid] = $QtyPO;
            
            $arr_data["list_PCode"][$PCode] = $PCode;
        }
		
		//ini yang mengeksekusi harga di contract supplier
		$q = "
				SELECT  
				  b.PCode,
				  b.HargaContract,
				  a.`PeriodeAwal`,
				  a.`PeriodeAkhir`,
				  b.`Satuan`
				FROM contract_supplier_detail b,  contract_supplier_header a
				WHERE 1
				AND a.NoTransaksi=b.NoTransaksi
				AND a.KdSupplier='$KdSupplier'
				AND a.PeriodeAwal<='".$arr_curr["TglDokumen"]."'
				AND a.PeriodeAkhir>='".$arr_curr["TglDokumen"]."'
				AND a.status='1'";
		//echo $q;		
		$qry_cs = mysql_query($q);
        while($row_cs = mysql_fetch_array($qry_cs))
        { 
            list($PCode, $Harga, $PeriodeAwal, $PeriodeAkhir, $Satuan) = $row_cs;    
            //cek
            $arr_data["data_cs_PCode"][$PCode] = $PCode;
            $arr_data["data_cs_Harga"][$PCode] = $Harga;
            $arr_data["data_cs_PeriodeAwal"][$PCode] = $PeriodeAwal;
			$arr_data["data_cs_PeriodeAkhir"][$PCode] = $PeriodeAkhir;
			$arr_data["data_cs_satuan"][$PCode] = $Satuan;
        }
		
        
        $q = "
                SELECT
                    ".$db["master"].".trans_order_barang_detail.Sid,    
                    ".$db["master"].".trans_order_barang_detail.PCode,
                    ".$db["master"].".trans_order_barang_detail.Qty,
                    ".$db["master"].".trans_order_barang_detail.QtyPcs,
                    ".$db["master"].".trans_order_barang_detail.Satuan,
                    ".$db["master"].".trans_order_barang_detail.Harga,
                    ".$db["master"].".trans_order_barang_detail.Disc1,
                    ".$db["master"].".trans_order_barang_detail.Disc2,
                    ".$db["master"].".trans_order_barang_detail.Potongan,
                    ".$db["master"].".trans_order_barang_detail.Jumlah,
                    ".$db["master"].".trans_order_barang_detail.PPn,
                    ".$db["master"].".trans_order_barang_detail.Total
                FROM
                    ".$db["master"].".trans_order_barang_detail
                WHERE
                    1
                    AND ".$db["master"].".trans_order_barang_detail.NoDokumen = '".$arr_curr["NoDokumen"]."'
                ORDER BY
                    ".$db["master"].".trans_order_barang_detail.Sid ASC
        ";
        //echo "<br><br>".$q;
        $qry_po = mysql_query($q);
        while($row_po = mysql_fetch_array($qry_po))
        { 
            list(
                $Sid,    
                $PCode,
                $Qty,
                $QtyPcs,
                $Satuan,
                $Harga,
                $Disc1,
                $Disc2,
                $Potongan,
                $Jumlah,
                $PPn,
                $Total 
            ) = $row_po;    
            
            $arr_data["data_po_PCode"][$PCode] = $PCode;
            $arr_data["data_po_Qty"][$PCode] = $Qty;
            $arr_data["data_po_QtyPcs"][$PCode] = $QtyPcs;
            $arr_data["data_po_Satuan"][$PCode] = $Satuan;
            $arr_data["data_po_Harga"][$PCode] = $Harga;
            $arr_data["data_po_Disc1"][$PCode] = $Disc1;
            $arr_data["data_po_Disc2"][$PCode] = $Disc2;
            $arr_data["data_po_Potongan"][$PCode] = $Potongan;
            $arr_data["data_po_Jumlah"][$PCode] = $Jumlah;
            $arr_data["data_po_PPn"][$PCode] = $PPn;
            $arr_data["data_po_Total"][$PCode] = $Total;
        }
        
        if(count($arr_data["list_PCode"])*1>0)
        {
            $where_pcode = where_array($arr_data["list_PCode"], "PCode", "in");
            
            $q = "
                    SELECT
                        ".$db["master"].".masterbarang.PCode,
                        ".$db["master"].".masterbarang.NamaLengkap
                    FROM
                        ".$db["master"].".masterbarang
                    WHERE
                        1
                        ".$where_pcode."
                    ORDER BY
                        ".$db["master"].".masterbarang.PCode ASC
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            { 
                list($PCode, $NamaLengkap) = $row; 
                
                $arr_data["NamaLengkap"][$PCode] = $NamaLengkap;
            }
            
        }
        
        $q = "
                SELECT
                    ".$db["master"].".satuan.KdSatuan,    
                    ".$db["master"].".satuan.NamaSatuan
                FROM
                    ".$db["master"].".satuan
                WHERE
                    1
                ORDER BY
                    ".$db["master"].".satuan.KdSatuan ASC
        ";
		
        $qry_pr = mysql_query($q);
        while($row_pr = mysql_fetch_array($qry_pr))
        { 
            list($KdSatuan, $NamaSatuan) = $row_pr;    
            
            $arr_data["list_satuan"][$KdSatuan] = $KdSatuan;
        }
            
		
        ?> 
        <div class="col-md-12" align="left">
        	
        	<ol class="breadcrumb title_table">
				<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
				<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
			</ol>
			                                           
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">
        <input type="hidden" name="v_approve_edit" id="v_approve_edit" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value=""> 
        <input type="hidden" name="v_request_edit" id="v_request_edit" value="">  
        <input type="hidden" name="v_request_edit2" id="v_request_edit2" value=""> 
        <input type="hidden" name="v_NoDokumen" id="v_NoDokumen" value="<?php echo $arr_curr["NoDokumen"]; ?>">   
        <input type="hidden" name="v_Status_old" id="v_Status_old" value="<?php echo $arr_curr["Status"]; ?>">   
        
        <table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="200">No Dokumen</td>
                <td colspan="3" style="color: black; font-weight: bold"><?php echo $arr_curr["NoDokumen"]; ?></td>
            </tr>
        
            <tr>
                <td class="title_table" width="200">Tanggal</td>
                <td width="40%">
                    <input type="text" class="form-control-new datepicker" name="v_TglDokumen" id="v_TglDokumen" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["TglDokumen"]); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglDokumen, 'dd/mm/yyyy');">
                </td>
                
                <td class="title_table" width="200">Supplier</td>
                <td>
                    <select name="v_KdSupplier" id="v_KdSupplier" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_supplier', this.value)">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_supplier"] as $KdSupplier=>$val)
                            {
                                $NamaSupplier = $arr_data["NamaSupplier"][$KdSupplier];
                                
                                $selected = "";
                                if($arr_curr["KdSupplier"]==$KdSupplier)
                                {
                                    $selected = "selected='selected'";
                                }
                                
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdSupplier; ?>"><?php echo $NamaSupplier; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td> 
                
            </tr>  

            <tr>
                <td class="title_table">Gudang</td>
                <td>
                    <select name="v_KdGudang" id="v_KdGudang" class="form-control-new" style="width: 200px;">  
                        <option value="">-</option>
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                $selected = "";
                                if($arr_curr["KdGudang"]==$KdGudang)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td> 
                
                <td class="title_table">TOP (Hari)</td>
                <td>
                    <input type="text" class="form-control-new" style="width: 200px;" name="v_top" id="v_top" value="<?php echo $arr_curr["TOP"]; ?>">
                </td>
                
            </tr>
                
            <tr>
                <td class="title_table">No PR</td>
                <td>
                    <input type="text" class="form-control-new" readonly="readonly" size="20" name="v_NoPr" id="v_NoPr" value="<?php echo $arr_curr["NoPr"]; ?>">
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_search_pr()">&nbsp;&nbsp;<i class="entypo-search"></i></button>
                </td>
                
                <td class="title_table">Currency</td>
                <td>
                    <select name="v_currencycode" id="v_currencycode" class="form-control-new" style="width: 200px;">  
                        <?php 
                            foreach($arr_data["list_currency"] as $currencycode=>$val)
                            {
                                $selected = "";
                                if($arr_curr["currencycode"]==$currencycode)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $currencycode; ?>"><?php echo $currencycode; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
                
            </tr>
                
            <tr>
                <td class="title_table">Keterangan</td>
                <td>
                   <input type="text" class="form-control-new datepicker" name="v_Keterangan" id="v_Keterangan" style="width: 200px;" value="<?php echo $arr_curr["Keterangan"]; ?>"> 
                </td>
                
                <td class="title_table" width="150">Estimasi Terima</td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_TglTerima" id="v_TglTerima" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["TglTerima"]); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_TglTerima, 'dd/mm/yyyy');">
                  </td> 
            </tr>
            
             <?php
            if($arr_curr["Status"]==0)
            {
            ?>
            <tr>
                <td class="title_table">Status</td>
                <td colspan="3">
                    <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                        <option value="">Pilih</option>    
                        <option value="kirim_po">Kirim PO</option>    
                        <option value="void">Void</option>    
                    </select>
                </td>
            </tr>
            <?php				
			}else if($arr_curr["Status"]==1 AND $arr_curr["Approval_By"]=="")
            {
            ?>
            <tr>
                <td class="title_table">Status</td>
                <td colspan="3">
                    <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                        <option value="">Pilih</option>    
                        <option value="kirim_po">Kirim PO</option>    
                        <option value="void">Void</option>    
                    </select>
                </td>
            </tr>
            <?php				
			}
			
			if($arr_curr["Status"]==2)
            {
            ?>
            <tr>
                <td class="title_table">Status</td>
                <td colspan="3">
                    <select name="v_Status" id="v_Status" class="form-control-new" style="width: 200px;">
                        <option value="">Pilih</option>    
                        <option value="unvoid">UnVoid</option>   
                    </select>
                </td>
            </tr>
            <?php				
			}
            ?>                
               
            <tr> 
                <td>&nbsp;</td>                   
                <td colspan="3">
                    <?php 
                        $q = "
                                SELECT
                                    trans_terima_header.NoDokumen
                                FROM
                                    trans_terima_header
                                WHERE
                                    1
                                    AND trans_terima_header.PoNo = '".$arr_curr["NoDokumen"]."'
                                    AND trans_terima_header.Status <> '2'
                                LIMIT
                                    0,1
                        ";
						//echo $q;
                        $qry_cek = mysql_query($q);
                        $row_cek = mysql_fetch_array($qry_cek);
                        list($NoDokumen_RG) = $row_cek;
                        
                        if($NoDokumen_RG)
                        {
                        	
                            echo "<font color='red'>Button Simpan tidak muncul Karena Sudah Ada Penerimaa ".$NoDokumen_RG."</font>"; 
                               
                        }
                        else
                        {
                            if($arr_curr["Status"]*1==0 || $arr_curr["Status"]*1==2)
                            {    
                            	?>
                            	<input type='hidden' name="btn_save" id="btn_save" value="Save">
                	        	<button type="button"  onclick="cekTheform();" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                            	<?php 
                            }
                            else if($arr_curr["Status"]==1 && $arr_curr["Approval_By"]=="")
	                        {?>
	                        	<input type='hidden' name="btn_save" id="btn_save" value="Save">
                	        	<button type="button"  onclick="cekTheform();" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                            	
	                        <?}else{
                            	if($arr_curr["Approval_Status"]*1==0)
                            	{
									echo "<font color='red'>PO Tidak dapat diedit, Karena Sudah Proses Kirim PO</font>";	
								}
								else if($arr_curr["Approval_Status"]*1==1)
								{
									echo "<font color='red'>PO Tidak dapat diedit, Karena Sudah Proses Approval</font>";
									
									if($ses_login=="charles3105" || $ses_login=="trisno1402" || $ses_login=="henny2905")
                                	{
	                                	if($arr_curr["Status"]*1==9)
	                                    {
										?>
	                                    <span style="float: right;">
	                                    <button type="submit" name="btn_approve_edit" id="btn_approve_edit" class="btn btn-danger btn-icon btn-sm icon-left" onclick="confirm_approve_edit('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Approve_edit">Setujui Edit PO<i class="entypo-check"></i></button> </span> 
	                                    <?php
										}
                                	}else{
										if($arr_curr["Status"]*1==9)
	                                    {
	                                	echo "<span style='float: right; color: orange;'><b>Request Edit PO</b></span>";
	                                    }else{
	                                    	?>
	                                    <span style="float: right;">
	                                    <button type="button" name="btn_confirm_request2" id="btn_confirm_request2" class="btn btn-danger btn-icon btn-sm icon-left" value="Request2" onclick="muncul_request2()">Request Edit PO<i class="entypo-check"></i></button>    
	                                    
	                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_request_Remarks2" id="v_request_Remarks2" value="" placeholder="Alasan Request Edit PO">
	                                    <button style="display: none;" type="submit" name="btn_request2" id="btn_request2" class="btn btn-danger btn-icon btn-sm icon-left" onclick="confirm_request2('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Request2">Kirim Request<i class="entypo-check"></i></button>    
	                                    </span> 
	                                    <?php
										
										}
										
								}
                            }
                        }
                        }
                    ?>
                    
                    
                    
                    <?php 
                    	if($arr_curr["Status"]==1 && $arr_curr["Approval_Status"]==0)
                        {
                            
                            if(($arr_curr["Total"]*1)<=5000000)
                            {
                            	
                                // sari
                                if($ses_login=="charles3105" || $ses_login=="mechael0101" || $ses_login=="frangky2311"||$ses_login=="tony1205")
                                {
                                    ?>
                                    <span style="float: right;">
                                    <button type="submit" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
                                    
                                    <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
                                    
                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
                                    <button style="display: none;" type="submit" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
                                    </span> 
                                    <?php
                                }
                                else
                                {
                                    echo "<span style='float: right; color: blue;'>Approval Waiting Kadek charles</span>";    
                                }
                                
                                
                            }
                            else if(($arr_curr["Total"]*1)>5000000 && ($arr_curr["Total"]*1)<=10000000)
                            {
                                // trisno
                                if($ses_login=="trisno1402" || $ses_login=="mechael0101" || $ses_login=="frangky2311"||$ses_login=="tony1205")
                                {
                                   ?>
                                    <span style="float: right;">
                                    <button type="submit" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
                                    
                                    <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
                                    
                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
                                    <button style="display: none;" type="submit" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
                                    </span> 
                                    <?php 
                                }
                                else
                                {
                                    echo "<span style='float: right; color: blue;'>Approval Waiting Bambang Sutrisno</span>";
                                }
                            }
                            else if(($arr_curr["Total"]*1)>10000000)
                            {
                                // henny
                                if($ses_login=="henny2905" || $ses_login=="frangky2311"||$ses_login=="tony1205" ||$ses_login=="mechael0101")
                                {
                                   ?>
                                    <span style="float: right;">
                                    <button type="submit" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
                                    
                                    <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
                                    
                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
                                    <button style="display: none;" type="submit" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $arr_curr["NoDokumen"]; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
                                    </span> 
                                    <?php 
                                }
                                else
                                {
                                    echo "<span style='float: right; color: blue;'>Approval Waiting Henny Soetanto</a>";
                                }
                            }
                    ?>
                    
                		<?php
                        }
                        ?>
               </td>
            </tr> 
            
            
            <tr>
                <td colspan="100%">
                    <table width="100%" class="table table-bordered responsive">
                            <tr class="title_table">
                                <td width="30">No</td>    
                                <td>PCode</td>    
                                <td>Nama Barang</td>    
                                <td align="right">Qty Request</td>    
                                <td align="right">Qty</td>    
                                <td>Satuan</td>    
                                <td>Harga</td>    
                                <td align="right">Disc (%)</td>    
                                <td align="right">Potongan (<?php echo $arr_curr["currencycode"]; ?>)</td>    
                                <td align="right">Subtotal</td>    
                            </tr>
                        
                        <tbody style="color: black;">
                            <?php
                                $no = 1;
                                $arr_data["total"] = 0; 
                                foreach($arr_data["list_data_pr"] as $Sid=>$val)
                                {
                                    $PCode = $arr_data["data_pr_PCode"][$Sid];
                                    $NamaLengkap = $arr_data["NamaLengkap"][$PCode];
                                    $Qty = $arr_data["data_pr_Qty"][$Sid];
                                    $QtyPcs = $arr_data["data_pr_QtyPcs"][$Sid];
                                    $Satuan = $arr_data["data_pr_Satuan"][$Sid];
                                    $QtyPO_diPR = $arr_data["data_pr_Qty_PO"][$Sid];
                                    
                                    $po_Qty = $arr_data["data_po_Qty"][$PCode];
                                    $po_QtyPcs = $arr_data["data_po_QtyPcs"][$PCode];
                                    $po_Satuan = $arr_data["data_po_Satuan"][$PCode];
                                    
                                    //bandingkan QtyPO di PR dengan QtyPO di PO

									if(empty($po_Qty)){
										
										if($QtyPcs  <= $QtyPO_diPR){
										$display = "style='display: none'";
										$Qty_placeholder = $QtyPO_diPR;
										
										}else{
										$display = "style='display:'";
										$Qty_placeholder = $QtyPO_diPR;	
										}
										
									}else{
										$display = "style='display:'";
										}
									
									    $konversi_ = "
											SELECT a.amount AS nil_konversi FROM ".$db["master"].".`konversi` a WHERE a.`PCode`='".$PCode."' AND a.`Satuan_From`='".$po_Satuan."';
											";	
											$qry_ = mysql_query($konversi_);
											$row = mysql_fetch_array($qry_);
											list($nil_konversi) = $row;
									
									
									$satuan_asal = $arr_data["data_cs_satuan"][$PCode];
									if($arr_data["data_cs_Harga"][$PCode]!=""||$arr_data["data_cs_Harga"][$PCode]!=0){
										if($arr_data["data_cs_PeriodeAwal"][$PCode]<=$arr_curr["TglDokumen"] AND $arr_data["data_cs_PeriodeAkhir"][$PCode]>=$arr_curr["TglDokumen"]){
											
											if($arr_data["data_cs_satuan"][$PCode]==$po_Satuan){
												$po_Harga = $arr_data["data_cs_Harga"][$PCode];
												$po_Qty_= $po_Qty;
												$nil_konv=1;
												$read_aja = "1";
											}else{	
												$po_Harga = $arr_data["data_cs_Harga"][$PCode];
												$po_Qty_= $nil_konversi*$po_Qty;
												$nil_konv=$nil_konversi;
												$read_aja = "1";
											}
										}
									}
									else{
										$po_Harga = $arr_data["data_po_Harga"][$PCode];
										$po_Qty_=$po_Qty;
										$nil_konv=1;
										$read_aja = "0";
									}
									
                                    $po_Disc1 = $arr_data["data_po_Disc1"][$PCode];
                                    $po_Disc2 = $arr_data["data_po_Disc2"][$PCode];
                                    $po_Potongan = $arr_data["data_po_Potongan"][$PCode];
                                    $po_Jumlah = $arr_data["data_po_Jumlah"][$PCode];
                                    $po_PPn = $arr_data["data_po_PPn"][$PCode];
                                    $po_Total = $arr_data["data_po_Total"][$PCode];
                                    
                                    $subtotal  = ($po_Qty_*$po_Harga);
                                    $disc1_val = $subtotal*($po_Disc1/100);
                                    $disc2_val = ($subtotal - $disc1_val)*($po_Disc2/100);
                                    $subtotal_Total = $subtotal - $disc1_val - $disc2_val - $po_Potongan;
                            ?>
                            <tr <?php echo $display;?>>
                                <td>
                                    <?php echo $no; ?>
                                    <input type="hidden" name="v_sid[]" value="<?php echo $no; ?>">
                                    <input type="hidden" name="v_PCode_<?php echo $no; ?>" id="v_PCode_<?php echo $no; ?>" value="<?php echo $PCode; ?>">
                                    <input type="hidden" name="v_kon_<?php echo $no; ?>" id="v_kon_<?php echo $no; ?>" value="<?php echo $nil_konv; ?>">
									<input type="hidden" name="v_sat_asal_<?php echo $no; ?>" id="v_sat_asal_<?php echo $no; ?>" value="<?php echo $satuan_asal; ?>">
                                </td>
                                <td><?php echo $PCode; ?></td>
                                <td><?php echo $NamaLengkap; ?></td>
                                <td align="right"><?php echo format_number($Qty,2)." ".$Satuan; ?></td>
                                <td align="right"><input style="text-align: right; width: 60px;" placeholder="<?php echo $Qty_placeholder;?>" type="text" class="form-control-new" name="v_Qty_<?php echo $no; ?>" id="v_Qty_<?php echo $no; ?>" value="<?php echo format_number($po_Qty, 2); ?>" onchange="HitungHarga2('harga', this);"></td>
                                <td>
                                	
                                    <select name="v_Satuan_<?php echo $no; ?>" id="v_Satuan_<?php echo $no; ?>" onchange="hitung(this)" class="form-control-new">
                                        <option> -- Pilih. -- </option>
										<?php 
										
											$q="
												SELECT 
													  a.`SatuanSt` AS KdSatuan, c.`NamaSatuan` 
													FROM
													  ".$db["master"].".masterbarang a INNER JOIN ".$db["master"].".satuan c ON a.`SatuanSt`=c.`KdSatuan`
													WHERE a.`PCode` = '".$PCode."' 
													UNION
													SELECT 
													  b.Satuan_From, d.`NamaSatuan`
													FROM
													  ".$db["master"].".konversi b INNER JOIN ".$db["master"].".satuan d ON b.`Satuan_From`=d.`KdSatuan`
													WHERE b.PCode = '".$PCode."' ;
											";
											
											$qry_pr = mysql_query($q);
											while($row_pr = mysql_fetch_array($qry_pr))
											{ 
												list($KdSatuan, $NamaSatuan) = $row_pr;    
												
												$arr_data["list_satuan_new"][$KdSatuan] = $KdSatuan;
												$arr_data["namasatuan"][$KdSatuan] = $NamaSatuan;
											}
                                            foreach($arr_data["list_satuan_new"] as $KdSatuan=>$val)
                                            {
												$NamaSatuan = $arr_data["namasatuan"][$KdSatuan];
                                                $selected = "";
                                                if($po_Satuan==$KdSatuan)
                                                {
                                                    $selected = "selected='selected'";    
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="<?php echo $KdSatuan; ?>"><?php echo $NamaSatuan; ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_Harga_<?php echo $no; ?>" id="v_Harga_<?php echo $no; ?>" onchange="HitungHarga3('harga', this);" value="<?php echo $po_Harga; ?>" <?php if($read_aja=="1"){echo $readonly="readonly";};?> ></td>
                                <td align="right">
                                    <input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc1_<?php echo $no; ?>" id="v_Disc1_<?php echo $no; ?>" value="<?php echo $po_Disc1; ?>" onchange="HitungHarga4('harga', this);">
                                    <input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc2_<?php echo $no; ?>" id="v_Disc2_<?php echo $no; ?>" value="<?php echo $po_Disc2; ?>" onchange="HitungHarga5('harga', this);">
                                </td>
                                <td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_Potongan_<?php echo $no; ?>" id="v_Potongan_<?php echo $no; ?>" value="<?php echo $po_Potongan; ?>" onchange="HitungHarga6('harga', this);"></td>
                                <!--<td align="right"><?php echo format_number($subtotal_Total,2); ?></td>-->
                                <td align="right"><input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_subtotal[]" id="v_subtotal_<?php echo $no; ?>" dir="rtl" value="<?php echo $subtotal_Total; ?>" ></td>
								<td style="display: none"><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $no; ?>" value="" dir="rtl" class="form-control-new" readonly="readonly"/></td>
                            </tr>
                            <!-- style="display: none" -->
                            <?php 
                                    $arr_data["total"] += $subtotal_Total;
                                    $no++;
                                }
                                
                                $disc_val = $arr_data["total"]*($arr_curr["DiscHarga"]/100);
                                $ppn_val  = ($arr_data["total"] - $disc_val) * ($arr_curr["PPn"]/100); 
                                $grand_total = ($arr_data["total"] - $disc_val) + $ppn_val;
                            ?>
                        </tbody>
                        
                      
                            <tr style="color: black; font-weight: bold;">
                                <td colspan="8" rowspan="4">
                                    Terbilang : <?php echo ucwords(Terbilang($grand_total)); ?> 
                                </td>
                                <td style="text-align: right;">
                                TOTAL
                                <!--<input type="text" name="v_Jumlah" id="v_Jumlah" value="<?php echo $arr_data["total"]; ?>">
                                </td>
                                <td style="text-align: right;"><?php echo format_number($arr_data["total"], 2); ?></td>-->
			
                                </td>
                                <td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_Jumlah" id="v_Jumlah" value="<?php echo $arr_data["total"]; ?>"></td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC <input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" value="<?php echo format_number($arr_curr["DiscHarga"], 2); ?>" onchange="HitungHarga7();"> (%)</td>
                                <!--<td style="text-align: right;"><?php echo format_number($disc_val, 2); ?></td>-->
								<td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_nil_diskon" id="v_nil_diskon" value="<?php echo $disc_val; ?>">
								</td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN <input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn" id="v_PPn" value="<?php echo format_number($arr_curr["PPn"], 2); ?>" onchange="HitungHarga8();"> (%)</td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo $ppn_val; ?>">
                                    <?php// echo format_number($ppn_val, 2); ?>
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                    <!--<input type="text" name="v_Total" id="v_Total" value="<?php echo $grand_total; ?>">
                                </td>
                                <td style="text-align: right;"><?php echo format_number($grand_total, 2); ?></td>-->
								
								</td>
                                <td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_Total" id="v_Total" value="<?php echo $grand_total; ?>"></td>
                            </tr>
                    </table>
                
                </td>
            </tr>
            
            
            <tr class="title_table">
                <td colspan="100%">INFORMASI DATA</td>
            </tr>
            
            <tr style="font-weight: bold; color: black;">
                <td>Dibuat Oleh</td>
                <td><?php echo format_show_datetime($arr_curr["AddDate"])." :: ".$arr_curr["AddUser"];?></td>
            </tr>
            
            <tr style="font-weight: bold; color: black;">
                <td>Diedit Oleh</td>
                <td><?php echo format_show_datetime($arr_curr["EditDate"])." :: ".$arr_curr["EditUser"];?></td>
            </tr>
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    else if($ajax=="ajax_supplier")
    {
        $v_KdSupplier = $_GET["v_KdSupplier"];
        
        $q = "
                SELECT
                    ".$db["master"].".supplier.TOP
                FROM
                    ".$db["master"].".supplier
                WHERE
                    1
                    AND ".$db["master"].".supplier.KdSupplier = '".$v_KdSupplier."'
                LIMIT
                    0,1
        ";
        $qry = mysql_query($q);
        $row = mysql_fetch_array($qry);
        list($TOP) = $row;
        
        echo $TOP;
    }
	
	else if($ajax=="konversi")
    {
        $v_Pcode = $_GET["pcode"];
		$v_satuan_asal = $_GET["satuan_asal"];
		$v_satuan_tujuan = $_GET["satuan_tujuan"];
        		
        $q = "
                SELECT
                    ".$db["master"].".konversi.amount
                FROM
                    ".$db["master"].".konversi
                WHERE
                    1
                    AND ".$db["master"].".konversi.Satuan_From = '".$v_satuan_tujuan."'
					AND ".$db["master"].".konversi.Satuan_To = '".$v_satuan_asal."'
        ";
        $qry = mysql_query($q);
		$cek = mysql_num_rows($qry);
        $row = mysql_fetch_array($qry);
        list($amount) = $row;
        if($cek>0){
        echo $amount;
		}else{
		echo '1';
		}
    }
    
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                
                if(!isset($_POST["v_TglDokumen"])){ $v_TglDokumen = isset($_POST["v_TglDokumen"]); } else { $v_TglDokumen = $_POST["v_TglDokumen"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_NoPr"])){ $v_NoPr = isset($_POST["v_NoPr"]); } else { $v_NoPr = $_POST["v_NoPr"]; } 
                if(!isset($_POST["v_KdSupplier"])){ $v_KdSupplier = isset($_POST["v_KdSupplier"]); } else { $v_KdSupplier = $_POST["v_KdSupplier"]; }          
                if(!isset($_POST["v_top"])){ $v_top = isset($_POST["v_top"]); } else { $v_top = $_POST["v_top"]; }            
                if(!isset($_POST["v_currencycode"])){ $v_currencycode = isset($_POST["v_currencycode"]); } else { $v_currencycode = $_POST["v_currencycode"]; } 
                if(!isset($_POST["v_TglTerima"])){ $v_TglTerima = isset($_POST["v_TglTerima"]); } else { $v_TglTerima = $_POST["v_TglTerima"]; }   
                if(!isset($_POST["v_Keterangan"])){ $v_Keterangan = isset($_POST["v_Keterangan"]); } else { $v_Keterangan = $_POST["v_Keterangan"]; } 

				$v_TglDokumen    = format_save_date($v_TglDokumen);
				$v_TglTerima     = format_save_date($v_TglTerima);
				$v_KdGudang      = save_char($v_KdGudang);
				$v_NoPr          = save_char($v_NoPr);
				$v_KdSupplier    = save_char($v_KdSupplier);
                $v_top           = save_int($v_top);
                $v_currencycode  = save_char($v_currencycode);
                $v_Keterangan    = save_char($v_Keterangan);
                    
                /*if($v_receipt_no=="")
                {    
                	$msg = "No Penerimaan Barang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();                                                                           
                }
                else if($v_no_proposal=="")
                {       
                	$msg = "No Proposal harus diisi";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else */
                if($v_KdGudang=="")
                {      
                	$msg = "Gudang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_NoPr=="")
                {   
                	$msg = "No PR harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_KdSupplier=="")
                {   
                	$msg = "Supplier harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }   
                else if($v_TglTerima=="")
                {   
                	$msg = "Tanggal Terima Harus Diisi, Mimimun harus sama dengan tanggal PO";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                } 
                // PO00002-05-16
                
                $arr_date = explode("-", $v_TglDokumen);
                $mm   = $arr_date[1];
                $yyyy = $arr_date[0];
                
	            $NoDokumen = get_code_counter($db["master"], "trans_order_barang_header","NoDokumen", "PO", $mm, $yyyy);
	            
	            // insert po
	            {
		            $q = "
			            INSERT INTO
			                ".$db["master"].".trans_order_barang_header
			            SET
			                NoDokumen = '".$NoDokumen."',
			                TglDokumen = '".$v_TglDokumen."',
			                NoPr = '".$v_NoPr."',
			                TglTerima = '".$v_TglTerima."',
			                KdSupplier = '".$v_KdSupplier."',
			                Keterangan = '".$v_Keterangan."',
			                KdGudang = '".$v_KdGudang."',
                            TOP = '".$v_top."',
                            currencycode = '".$v_currencycode."',
                            AddDate = NOW(),
			                AddUser = '".$ses_login."',
			                EditDate = NOW(),
			                EditUser = '".$ses_login."'
			        ";   
			        if(!mysql_query($q))
			        {
			            $msg = "Gagal menyimpan trans_order_barang_header";
			            echo "<script>alert('".$msg."');</script>";
			            die(); 
			        }	
				}
				
				$msg = "Berhasil menyimpan";
	            echo "<script>alert('".$msg."');</script>"; 
				echo "<script>parent.CallAjaxForm('search','".$NoDokumen."');</script>";         	
	            
	            die();
				   
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {    
            	                                                                                                 
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; } 
                if(!isset($_POST["v_approve_edit"])){ $v_approve_edit = isset($_POST["v_approve_edit"]); } else { $v_approve_edit = $_POST["v_approve_edit"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }
                if(!isset($_POST["v_request_edit"])){ $v_request_edit = isset($_POST["v_request_edit"]); } else { $v_request_edit = $_POST["v_request_edit"]; }
                if(!isset($_POST["v_request_edit2"])){ $v_request_edit2 = isset($_POST["v_request_edit2"]); } else { $v_request_edit2 = $_POST["v_request_edit2"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; } 
                if(!isset($_POST["btn_approve_edit"])){ $btn_approve_edit = isset($_POST["btn_approve_edit"]); } else { $btn_approve_edit = $_POST["btn_approve_edit"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }  
                if(!isset($_POST["btn_request"])){ $btn_request = isset($_POST["btn_request"]); } else { $btn_request = $_POST["btn_request"]; }
                if(!isset($_POST["btn_request2"])){ $btn_request2 = isset($_POST["btn_request2"]); } else { $btn_request2 = $_POST["btn_request2"]; }   
                if(!isset($_POST["v_NoDokumen"])){ $v_NoDokumen = isset($_POST["v_NoDokumen"]); } else { $v_NoDokumen = $_POST["v_NoDokumen"]; }  
                
                if(!isset($_POST["v_TglDokumen"])){ $v_TglDokumen = isset($_POST["v_TglDokumen"]); } else { $v_TglDokumen = $_POST["v_TglDokumen"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_NoPr"])){ $v_NoPr = isset($_POST["v_NoPr"]); } else { $v_NoPr = $_POST["v_NoPr"]; } 
                if(!isset($_POST["v_KdSupplier"])){ $v_KdSupplier = isset($_POST["v_KdSupplier"]); } else { $v_KdSupplier = $_POST["v_KdSupplier"]; }          
                if(!isset($_POST["v_top"])){ $v_top = isset($_POST["v_top"]); } else { $v_top = $_POST["v_top"]; }            
                if(!isset($_POST["v_currencycode"])){ $v_currencycode = isset($_POST["v_currencycode"]); } else { $v_currencycode = $_POST["v_currencycode"]; } 
                if(!isset($_POST["v_TglTerima"])){ $v_TglTerima = isset($_POST["v_TglTerima"]); } else { $v_TglTerima = $_POST["v_TglTerima"]; }   
                if(!isset($_POST["v_Keterangan"])){ $v_Keterangan = isset($_POST["v_Keterangan"]); } else { $v_Keterangan = $_POST["v_Keterangan"]; } 
                if(!isset($_POST["v_Status"])){ $v_Status = isset($_POST["v_Status"]); } else { $v_Status = $_POST["v_Status"]; } 
                if(!isset($_POST["v_Status_old"])){ $v_Status_old = isset($_POST["v_Status_old"]); } else { $v_Status_old = $_POST["v_Status_old"]; } 
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }
                if(!isset($_POST["v_request_Remarks"])){ $v_request_Remarks = isset($_POST["v_request_Remarks"]); } else { $v_request_Remarks = $_POST["v_request_Remarks"]; } 
                if(!isset($_POST["v_request_Remarks2"])){ $v_request_Remarks2 = isset($_POST["v_request_Remarks2"]); } else { $v_request_Remarks2 = $_POST["v_request_Remarks2"]; }  
                
                if(!isset($_POST["v_Jumlah"])){ $v_Jumlah = isset($_POST["v_Jumlah"]); } else { $v_Jumlah = $_POST["v_Jumlah"]; } 
                if(!isset($_POST["v_PPn"])){ $v_PPn = isset($_POST["v_PPn"]); } else { $v_PPn = $_POST["v_PPn"]; } 
                if(!isset($_POST["v_NilaiPPn"])){ $v_NilaiPPn = isset($_POST["v_NilaiPPn"]); } else { $v_NilaiPPn = $_POST["v_NilaiPPn"]; } 
                if(!isset($_POST["v_DiscHarga"])){ $v_DiscHarga = isset($_POST["v_DiscHarga"]); } else { $v_DiscHarga = $_POST["v_DiscHarga"]; } 
                if(!isset($_POST["v_Total"])){ $v_Total = isset($_POST["v_Total"]); } else { $v_Total = $_POST["v_Total"]; } 
                
                if(!isset($_POST["v_sid"])){ $v_sid = isset($_POST["v_sid"]); } else { $v_sid = $_POST["v_sid"]; } 
                
				//echo "<pre>";print_r($_POST);echo "</pre>";
				//echo "Hallo";die;
                $v_NoDokumen     = save_char($v_NoDokumen);
                $v_TglDokumen    = format_save_date($v_TglDokumen);
                $v_TglTerima     = format_save_date($v_TglTerima);
                $v_KdGudang      = save_char($v_KdGudang);
                $v_NoPr          = save_char($v_NoPr);
                $v_KdSupplier    = save_char($v_KdSupplier);
                $v_top           = save_int($v_top);
                $v_currencycode  = save_char($v_currencycode);
                $v_Keterangan    = save_char($v_Keterangan);
                $v_Status        = save_char($v_Status);
                $v_Status_old    = save_char($v_Status_old);
                
                $v_Jumlah        = save_int($v_Jumlah);
                $v_PPn           = save_int($v_PPn);
                $v_NilaiPPn      = save_int($v_NilaiPPn);
                $v_DiscHarga     = save_int($v_DiscHarga);
                $v_Total         = save_int($v_Total);
                    
                if($v_KdGudang=="")
                {      
                    $msg = "Gudang harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_NoPr=="")
                {   
                    $msg = "No PR harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_KdSupplier=="")
                {   
                    $msg = "Supplier harus dipilih";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                else if($v_TglTerima=="")
                {   
                	$msg = "Tanggal Terima Harus Diisi, Mimimun harus sama dengan tanggal PO";
                    echo "<script>alert('".$msg."');</script>";
                    die();
                }
                
                if($v_del==1)   
                {
                	if($btn_delete=="Void")
                	{
                		$q = "
                            UPDATE
                                ".$db["master"].".trans_order_barang_header
                            SET     
                                Status = '2',
                                EditDate = NOW(),
                                EditUser = '".$ses_login."' 
                             WHERE 1
                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                                          
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan trans_order_barang_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
	                    $msg = "Berhasil Void ".$v_NoDokumen;
	                    echo "<script>alert('".$msg."');</script>"; 
	                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
	                    die();
					}
				}
                else  if($v_undel==1)   
                {
                    if($btn_unvoid=="UnVoid")
                    {
                        $q = "
                            UPDATE
                                ".$db["master"].".trans_order_barang_header
                            SET     
                                Status = '1',
                                EditDate = NOW(),
                                EditUser = '".$ses_login."' 
                             WHERE 1
                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan trans_order_barang_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
                        $msg = "Berhasil UnVoid ".$v_NoDokumen;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                        die();     
                        
                    }
                }
                else  if($v_approve==1)   
                {
                    if($btn_approve=="Approve")
                    {
                        $q = "
                            UPDATE
                                ".$db["master"].".trans_order_barang_header
                            SET     
                                Approval_By = '".$ses_login."',
                                Approval_Date = NOW(),
                                Approval_Status = '1'
                             WHERE 1
                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan trans_order_barang_header";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        // kirim email
                        {
                            $q = "
                                    SELECT
                                        ".$db["master"].".trans_order_barang_header.Approval_By,
                                        ".$db["master"].".trans_order_barang_header.EditUser,
                                        ".$db["master"].".trans_order_barang_header.AddUser
                                    FROM
                                        ".$db["master"].".trans_order_barang_header
                                    WHERE
                                        1
                                        AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                                    LIMIT
                                        0,1
                            ";
                            $qry = mysql_query($q);
                            $row = mysql_fetch_array($qry);
                            list($Approval_By, $EditUser, $AddUser) = $row;
                            
                            $to = "";
                            $to_name = "";
                            $q = "
                                    SELECT
                                        ".$db["master"].".employee.email,        
                                        ".$db["master"].".employee.employee_name
                                    FROM
                                        ".$db["master"].".employee
                                    WHERE
                                        1
                                        AND ".$db["master"].".employee.username IN('".$EditUser."', '".$AddUser."')
                                    ORDER BY
                                        ".$db["master"].".employee.employee_name ASC
                            ";
                            $qry = mysql_query($q);
                            while($row = mysql_fetch_array($qry))
                            {
                                list($email_address, $employee_name) = $row;
                                
                                $to .= $email_address.";";
                                $to_name .= $employee_name.";";
                            }
                            
                            $subject = "Notifikasi PO ".$v_NoDokumen." [APPROVED]";
                            $author  = "Auto System";
                            
                            $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                            
                            $to .= "hnd@vci.co.id;";
                            $to_name .= "Hendri;";
                            
                            $body  = "Dear ".$to_name.", <br><br>";
                            $body .= "Approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)." Sudah di APPROVED oleh <b>".$Approval_By."</b><br><br>";
                            $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat PO Atau Copy Paste Link ini di broswer anda : ".$url;
                            
                            $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);    
                        }    
                           
                        $msg = "Berhasil Approve ".$v_NoDokumen;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                        die();
                    }
                }
                else  if($v_reject==1)   
                {
                    if($btn_reject=="Reject")
                    {
                        if($v_Approval_Remarks=="")
                        {
                            $msg = "Alasan Reject harus diisi...";
                            echo "<script>alert('".$msg."');</script>";
                        }
                        else
                        {
                            
							
							
							$q = "
                                UPDATE
                                    ".$db["master"].".trans_order_barang_header
                                SET     
                                    Approval_By = '".$ses_login."',
                                    Approval_Date = NOW(),
                                    Approval_Status = '2',
                                    Approval_Remarks = '".$v_Approval_Remarks."'
                                 WHERE 1
                                    AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan trans_order_barang_header";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            
                            //kurangi QtyPO di trans_pr_detail
                            $q = "
                                   SELECT 
									  a.`NoDokumen`,
									  a.`NoPr`,
									  b.`PCode`,
									  b.`Qty`
									FROM
									  ".$db["master"].".`trans_order_barang_header` a 
									  INNER JOIN ".$db["master"].".`trans_order_barang_detail` b 
									    ON a.`NoDokumen` = b.`NoDokumen` 
									WHERE a.`NoDokumen` = '".$v_NoDokumen."' ; 
                            	 ";
                            	
                            $qry = mysql_query($q);
                            while($rowx = mysql_fetch_array($qry))
                            {
                                    //Ambil nilai QtyPO di trans_pr_detail
                                    $QtyPO_ditrans_prs = "
                                    SELECT a.QtyPO AS QuantityPO FROM `trans_pr_detail` a WHERE a.`NoDokumen`='".$rowx['NoPr']."' AND a.`PCode`='".$rowx['PCode']."';
									";
									
									$qryss = mysql_query($QtyPO_ditrans_prs);
									$rows = mysql_fetch_array($qryss);
									list($QuantityPO) = $rows;
                               
                               		$QtyPO_new = $QuantityPO - $rowx['Qty'];
                               		
                               		//update juga ke Purchase Request Detail
                                    $sql = "
                                    		UPDATE 
											  `trans_pr_detail` 
											SET
											  `QtyPO` = '".$QtyPO_new."' 
											WHERE `NoDokumen` = '".$rowx['NoPr']."' AND
											  `PCode` = '".$rowx['PCode']."' ;
                                    		";
                                    if(!mysql_query($sql))
                                    {
                                        $msg = "Gagal Update trans_pr_detail";
                                        echo "<script>alert('".$msg."');</script>";
										
                                        die(); 
                                    }
                               		
                               		
                            }
                            
                            // kirim email
                            {
                                $q = "
                                        SELECT
                                            ".$db["master"].".trans_order_barang_header.Approval_By,
                                            ".$db["master"].".trans_order_barang_header.Approval_Remarks,
                                            ".$db["master"].".trans_order_barang_header.EditUser,
                                            ".$db["master"].".trans_order_barang_header.AddUser
                                        FROM
                                            ".$db["master"].".trans_order_barang_header
                                        WHERE
                                            1
                                            AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                                        LIMIT
                                            0,1
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($Approval_By, $Approval_Remarks, $EditUser, $AddUser) = $row;
                                
                                $to = "";
                                $to_name = "";
                                $q = "
                                        SELECT
                                            ".$db["master"].".employee.email,        
                                            ".$db["master"].".employee.employee_name
                                        FROM
                                            ".$db["master"].".employee
                                        WHERE
                                            1
                                            AND ".$db["master"].".employee.username IN('".$EditUser."', '".$AddUser."')
                                        ORDER BY
                                            ".$db["master"].".employee.employee_name ASC
                                ";
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list($email_address, $employee_name) = $row;
                                    
                                    $to .= $email_address.";";
                                    $to_name .= $employee_name.";";
                                }
                                
                                $subject = "Notifikasi PO ".$v_NoDokumen." [REJECTED]";
                                $author  = "Auto System";
                                
                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                                
                                $to .= "hnd@vci.co.id;";
                                $to_name .= "Hendri;";
                                
                                $body  = "Dear ".$to_name.", <br><br>";
                                $body .= "Approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)." Sudah di REJECTED oleh <b>".$Approval_By."</b> karena <b>".$Approval_Remarks."</b><br><br>";
                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat PO Atau Copy Paste Link ini di broswer anda : ".$url;
                                
                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);    
                            } 
                                   
                            $msg = "Berhasil Reject ".$v_NoDokumen;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                            die();
                        }
                    }
                }else  if($v_request_edit2==1)   
                {
                    if($btn_request2=="Request2")
                    {
                        if($v_request_Remarks2=="")
                        {
                            $msg = "Alasan Request Edit PO harus diisi...";
                            echo "<script>alert('".$msg."');</script>";
                        }
                        else
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".trans_order_barang_header
                                SET 
                                	Status = '9',    
                                    Request_By = '".$ses_login."',
                                    Request_Date = NOW(),
                                    Request_Status = '1',
                                    Request_Remarks = '".$v_request_Remarks2."'
                                 WHERE 1
                                    AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan trans_order_barang_header";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                                                        
                            // kirim email
                            {
                                $q = "
                                        SELECT
                                            ".$db["master"].".trans_order_barang_header.Approval_By,
                                            ".$db["master"].".trans_order_barang_header.Request_Remarks,
                                            ".$db["master"].".trans_order_barang_header.EditUser,
                                            ".$db["master"].".trans_order_barang_header.AddUser
                                        FROM
                                            ".$db["master"].".trans_order_barang_header
                                        WHERE
                                            1
                                            AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                                        LIMIT
                                            0,1
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($Approval_By, $Request_Remarks, $EditUser, $AddUser) = $row;
                                
                                $to = "";
                                $to_name = "";
                                $q = "
                                        SELECT
                                            ".$db["master"].".employee.email,        
                                            ".$db["master"].".employee.employee_name
                                        FROM
                                            ".$db["master"].".employee
                                        WHERE
                                            1
                                            AND ".$db["master"].".employee.username IN('".$Approval_By."')
                                        ORDER BY
                                            ".$db["master"].".employee.employee_name ASC
                                ";
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list($email_address, $employee_name) = $row;
                                    
                                    $to .= $email_address.";";
                                    $to_name .= $employee_name.";";
                                }
                                
                                $subject = "Notifikasi PO ".$v_NoDokumen." [REQUEST EDIT PO]";
                                $author  = "Auto System";
                                
                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                                
                                $to .= "irf@vci.co.id;";
                                $to_name .= "";
                                
                                $body  = "Dear ".$to_name.", <br><br>";
                                $body .= "Mohon Setujui Edit PO untuk No. ".$v_NoDokumen." , karena <b>".$Request_Remarks."</b><br><br>";
                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat PO Atau Copy Paste Link ini di broswer anda : ".$url;
                                
                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);    
                            } 
                                   
                            $msg = "Berhasil Request Edit PO ".$v_NoDokumen;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                            die();
                        }
                    }
                }else  if($v_approve_edit==1)   
                {
                    if($btn_approve_edit=="Approve_edit")
                    {
                        
                            $q = "
                                UPDATE
                                    ".$db["master"].".trans_order_barang_header
                                SET 
                                	Status = '0',
                                    Request_Status = '0'
                                 WHERE 1
                                    AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan trans_order_barang_header";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                                                        
                            // kirim email
                            /*{
                                $q = "
                                        SELECT
                                            ".$db["master"].".trans_order_barang_header.Approval_By,
                                            ".$db["master"].".trans_order_barang_header.Approval_Remarks,
                                            ".$db["master"].".trans_order_barang_header.EditUser,
                                            ".$db["master"].".trans_order_barang_header.AddUser
                                        FROM
                                            ".$db["master"].".trans_order_barang_header
                                        WHERE
                                            1
                                            AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
                                        LIMIT
                                            0,1
                                ";
                                $qry = mysql_query($q);
                                $row = mysql_fetch_array($qry);
                                list($Approval_By, $Approval_Remarks, $EditUser, $AddUser) = $row;
                                
                                $to = "";
                                $to_name = "";
                                $q = "
                                        SELECT
                                            ".$db["master"].".employee.email,        
                                            ".$db["master"].".employee.employee_name
                                        FROM
                                            ".$db["master"].".employee
                                        WHERE
                                            1
                                            AND ".$db["master"].".employee.username IN('".$EditUser."', '".$AddUser."')
                                        ORDER BY
                                            ".$db["master"].".employee.employee_name ASC
                                ";
                                $qry = mysql_query($q);
                                while($row = mysql_fetch_array($qry))
                                {
                                    list($email_address, $employee_name) = $row;
                                    
                                    $to .= $email_address.";";
                                    $to_name .= $employee_name.";";
                                }
                                
                                $subject = "Notifikasi PO ".$v_NoDokumen." [REJECTED]";
                                $author  = "Auto System";
                                
                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                                
                                $to .= "hnd@vci.co.id;";
                                $to_name .= "Hendri;";
                                
                                $body  = "Dear ".$to_name.", <br><br>";
                                $body .= "Approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)." Sudah di REJECTED oleh <b>".$Approval_By."</b> karena <b>".$Approval_Remarks."</b><br><br>";
                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat PO Atau Copy Paste Link ini di broswer anda : ".$url;
                                
                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);    
                            } */
                                   
                            $msg = "Berhasil Setujui Edit PO ".$v_NoDokumen;
                            echo "<script>alert('".$msg."');</script>"; 
                            echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
                            die();
                        
                    }
                }
                else
                {
                    if($btn_save=="Save")
                    {
                    	
                    	$save_status = "0";
                    	if($v_Status=="kirim_po")
                    	{
							$save_status = "1";
						}
						else if($v_Status=="void")
                    	{
							$save_status = "2";
						}
						else if($v_Status=="unvoid")
                    	{
							$save_status = "0";
						}
						   
						// update po
						{
                            foreach($v_sid as $key=>$val_sid)
                            {
                                if(!isset($_POST["v_PCode_".$val_sid])){ $v_PCode = isset($_POST["v_PCode_".$val_sid]); } else { $v_PCode = $_POST["v_PCode_".$val_sid]; }     
                                if(!isset($_POST["v_Qty_".$val_sid])){ $v_Qty = isset($_POST["v_Qty_".$val_sid]); } else { $v_Qty = $_POST["v_Qty_".$val_sid]; }     
                                if(!isset($_POST["v_Satuan_".$val_sid])){ $v_Satuan = isset($_POST["v_Satuan_".$val_sid]); } else { $v_Satuan = $_POST["v_Satuan_".$val_sid]; }     
                                if(!isset($_POST["v_Harga_".$val_sid])){ $v_Harga = isset($_POST["v_Harga_".$val_sid]); } else { $v_Harga = $_POST["v_Harga_".$val_sid]; }     
                                if(!isset($_POST["v_Disc1_".$val_sid])){ $v_Disc1 = isset($_POST["v_Disc1_".$val_sid]); } else { $v_Disc1 = $_POST["v_Disc1_".$val_sid]; }     
                                if(!isset($_POST["v_Disc2_".$val_sid])){ $v_Disc2 = isset($_POST["v_Disc2_".$val_sid]); } else { $v_Disc2 = $_POST["v_Disc2_".$val_sid]; }     
                                if(!isset($_POST["v_Potongan_".$val_sid])){ $v_Potongan = isset($_POST["v_Potongan_".$val_sid]); } else { $v_Potongan = $_POST["v_Potongan_".$val_sid]; }     
                                
                                $v_PCode = save_char($v_PCode);
                                $v_Qty = save_int($v_Qty);
                                $v_Satuan = save_char($v_Satuan);
                                $v_Harga = save_int($v_Harga);
                                $v_Disc1 = save_int($v_Disc1);
                                $v_Disc2 = save_int($v_Disc2);
                                $v_Potongan = save_int($v_Potongan);
                                
                                if($v_Qty)
                                {
									//===============================================================================================================================
									
									//Ambil nilai QtyPO di trans_pr_detail
                                    $QtyPO_ditrans_pr = "
                                    SELECT a.QtyPO AS QuantityPO FROM `trans_pr_detail` a WHERE a.`NoDokumen`='".$v_NoPr."' AND a.`PCode`='".$v_PCode."';
									";
									$qrys = mysql_query($QtyPO_ditrans_pr);
									$row = mysql_fetch_array($qrys);
									list($QuantityPO) = $row;
									
									//jika sudah dapat ambil Qty trans_order_detail sebelum di delete
									$QtyPO_ditrans_order = "
                                    SELECT QtyPcs as Qty_order FROM ".$db["master"].".trans_order_barang_detail WHERE 1 AND NoDokumen = '".$v_NoDokumen."' AND PCode='".$v_PCode."'
									";
									
									$qryz = mysql_query($QtyPO_ditrans_order);
									$rowz = mysql_fetch_array($qryz);
									list($Qty_order) = $rowz;
									
									//hapus trans_order_detail
									$q = "DELETE FROM ".$db["master"].".trans_order_barang_detail WHERE 1 AND NoDokumen = '".$v_NoDokumen."' AND PCode='".$v_PCode."' ";
									if(!mysql_query($q))
									{
										$msg = "Gagal reset trans_order_barang_detail";
										echo "<script>alert('".$msg."');</script>";
										die(); 
									}
							
									
									
									//samakan dahulu antara satuan PO dengan Satuan yang ada di Kontrak Supplier
									$q = "
											SELECT  
											  b.PCode,
											  b.HargaContract,
											  a.`PeriodeAwal`,
											  a.`PeriodeAkhir`,
											  b.`Satuan`
											FROM contract_supplier_detail b,  contract_supplier_header a
											WHERE 1
											AND a.NoTransaksi=b.NoTransaksi
											AND a.KdSupplier='".$v_KdSupplier."'
											AND a.PeriodeAwal<='".$v_TglDokumen."'
											AND a.PeriodeAkhir>='".$v_TglDokumen."'
											AND a.status='1' AND b.PCode='".$v_PCode."'";
										
									$qry_cs = mysql_query($q);
									while($row_cs = mysql_fetch_array($qry_cs))
									{ 
										list($PCode, $HargaContract, $PeriodeAwal, $PeriodeAkhir, $Satuan) = $row_cs;    
										//cek
										$arr_data["data_cs_PCode"][$PCode] = $PCode;
										$arr_data["data_cs_Harga"][$PCode] = $HargaContract;
										$arr_data["data_cs_PeriodeAwal"][$PCode] = $PeriodeAwal;
										$arr_data["data_cs_PeriodeAkhir"][$PCode] = $PeriodeAkhir;
										$arr_data["data_cs_satuan"][$PCode] = $Satuan;
									}
									
									//jika dalam masa kontrak harga
									if(isset($arr_data["data_cs_Harga"][$v_PCode])){
										
										if($arr_data["data_cs_PeriodeAwal"][$PCode]<=$v_TglDokumen AND $arr_data["data_cs_PeriodeAkhir"][$PCode]>=$v_TglDokumen){
											
											$konversi_ = "
											SELECT a.amount AS nil_konversi FROM ".$db["master"].".`konversi` a WHERE a.`PCode`='".$v_PCode."' AND a.`Satuan_From`='".$v_Satuan."';
											";
											$qry_ = mysql_query($konversi_);
											$row = mysql_fetch_array($qry_);
											list($nil_konversi) = $row;
											
											if($arr_data["data_cs_satuan"][$PCode]==$v_Satuan){
												//jika satuan sama po dengan kontrak supplier
												$v_Qty_ = $v_Qty;
											}else{
												//jika satuan tidak sama po dengan kontrak supplier
												$v_Qty_ = $v_Qty*$nil_konversi;	
												
											}
										
										}
									}
									//jika tidak
									else{
										
										$v_Qty_ = $v_Qty;
										
									}
									
									
									
									
									
								$subtotal  = ($v_Qty_*$v_Harga);
								
                                $disc1_val = $subtotal*($v_Disc1/100);
                                $disc2_val = ($subtotal - $disc1_val)*($v_Disc2/100);
                                $subtotal_Total = $subtotal - $disc1_val - $disc2_val - $v_Potongan;
								
								
                                
                                if($v_PPn)
                                {
                                    $Total = $subtotal_Total+(($v_PPn/100)*$subtotal_Total);
                                }
                                else
                                {
                                    $Total = $subtotal_Total;
                                }
									
									
									
									//cocokan konversi terlebih dahulu
									$konversi = "
                                    SELECT a.amount FROM ".$db["master"].".`konversi` a WHERE a.`PCode`='".$v_PCode."' AND a.`Satuan_From`='".$v_Satuan."';
									";
									$qry = mysql_query($konversi);
									$row_ = mysql_num_rows($qry);
									$row = mysql_fetch_array($qry);
									list($amount) = $row;
									
									if($row_*1==0){
										//ambil dan tampung amount
										$QtyPcs = $v_Qty;
									}else{
										//ambil dan tampung amount
										$QtyPcs = $v_Qty * $amount;
									}
																		
									
									//insert ke trans_order_barang_detail
                                    $q = "
                                        INSERT INTO ".$db["master"].".`trans_order_barang_detail`
                                        SET 
                                          `NoDokumen` = '".$v_NoDokumen."',
                                          `PCode` = '".$v_PCode."',
                                          `Qty` = '".$v_Qty."',
                                          `QtyPcs` = '".$QtyPcs."',
                                          `Satuan` = '".$v_Satuan."',
                                          `Harga` = '".$v_Harga."',
                                          `Disc1` = '".$v_Disc1."',
                                          `Disc2` = '".$v_Disc2."',
                                          `Potongan` = '".$v_Potongan."',
                                          `Jumlah` = '".$subtotal_Total."',
                                          `PPn` = '".$v_PPn."',
                                          `Total` = '".$Total."'      
                                    ";
									
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal Insert trans_order_barang_detail";
                                        echo "<script>alert('".$msg."');</script>";
										
                                        die(); 
                                    } 
                                    
                                    
                                    //update ke PR detail
									
									$QuantityPO1 = $QuantityPO + $Qty_order ;
									//$QuantityPO2 = $v_Qty + $QuantityPO1;
									$QuantityPO2 = $QtyPcs + $QuantityPO1;
                                    //echo $QuantityPO1." = ".$QuantityPO." + ".$Qty_order."<br>";
                                    //echo $QuantityPO2." = ".$QtyPcs." + ".$QuantityPO1;die;
                                    //update juga ke Purchase Request Detail
                                    $sql = "
                                    		UPDATE 
											  `trans_pr_detail` 
											SET
											  `QtyPO` = '".$QuantityPO2."' 
											WHERE `NoDokumen` = '".$v_NoPr."' AND
											  `PCode` = '".$v_PCode."' ;
                                    		";
                                    if(!mysql_query($sql))
                                    {
                                        $msg = "Gagal Update trans_pr_detail ".$sql;
                                        echo "<script>alert('".$msg."');</script>";
										
                                        die(); 
                                    }
                                    
                                    
                                    $arr_data["jml_detail"] += $v_Total;                                    
                                    
                                }
                            }
                            //
							$q = "
	                            UPDATE
	                            	".$db["master"].".trans_order_barang_header
	                            SET     
	                            	TglDokumen = '".$v_TglDokumen."',
                                    NoPr = '".$v_NoPr."',
                                    TglTerima = '".$v_TglTerima."',
                                    KdSupplier = '".$v_KdSupplier."',
                                    Keterangan = '".$v_Keterangan."',
                                    KdGudang = '".$v_KdGudang."',
                                    TOP = '".$v_top."',
                                    currencycode = '".$v_currencycode."',
                                    
                                    Jumlah = '".$arr_data["jml_detail"]."',
                                    DiscHarga = '".$v_DiscHarga."',
                                    PPn = '".$v_PPn."',
                                    NilaiPPn = '".$v_NilaiPPn."',
                                    Total = '".$v_Total."',
                                    
                                    Status = '".$save_status."',
                                    EditDate = NOW(),
                                    EditUser = '".$ses_login."' 
	                             WHERE 1
	                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
	                        ";                
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan trans_order_barang_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        }
						}
                        
                        /* update header lagi */
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".trans_order_barang_header
                                SET     
                                    Jumlah = '".$v_Jumlah."',
                                    NilaiPPn = '".$v_NilaiPPn."',
                                    Total = '".$v_Total."'
                                 WHERE 1
                                    AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
                            ";
                            
                            
							
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal Update Total Header ".$v_NoDokumen;
                                echo "<script>alert('".$msg."');</script>"; 
                                die();
                            }
                            
                            
                            
                            
                            $q_ = "
                                    SELECT
                                        ".$db["master"].".`trans_order_barang_header`.Total AS grand_total
                                    FROM
                                        ".$db["master"].".`trans_order_barang_header`
                                    WHERE
                                        1
                                        AND ".$db["master"].".`trans_order_barang_header`.NoDokumen = '".$v_NoDokumen."'
                            ";
                            $qry_ = mysql_query($q_);
                            $row = mysql_fetch_array($qry_);
                            list($grand_total) = $row;
                            
                            
                        }  
                        
                     
                        if($v_Status=="kirim_po")
                        {
                        	
                        	if($ses_login=="mechael0101")
                        	{
								$msg = "sudah dikirim cuy ".$grand_total;
                                echo "<script>alert('".$msg."');</script>"; 
                              
							}
							else
							{
	                        	if($grand_total*1<=5000000)    
	                            {
	                            	// sari                              
	                                $subject = "Request Approval PO ".$v_NoDokumen;
	                                $author  = "Auto System";
	                                
	                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
	                                
	                                $to = "crfg@secretgarden.co.id;fyttmp@vci.co.id;";
	                                $to_name = "Charles;Febriyanto;";
	                                
	                                $body  = "Dear Charles, <br><br>";
	                                $body .= "Mohon lakukan approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)."<br><br>";
	                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
	                                
	                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);  
	                            }
	                            else if($grand_total*1>5000000 && $grand_total*1<10000000)    
	                            {
	                                // trisno
	                                $subject = "Request Approval PO ".$v_NoDokumen;
	                                $author  = "Auto System";
	                                
	                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
	                                
	                                $to = "eno@vci.co.id;fyttmp@vci.co.id;";
	                                $to_name = "Trisno;Febriyanto;";
	                                
	                                $body  = "Dear Bambang Sutrisno, <br><br>";
	                                $body .= "Mohon lakukan approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)."<br><br>";
	                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
	                                
	                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);   
	                            }
	                            else if($grand_total*1>=10000000)    
	                            {
	                                // henny
	                                $subject = "Request Approval PO ".$v_NoDokumen;
	                                $author  = "Auto System";
	                                
	                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
	                                
	                                $to = "hst@vci.co.id;fyttmp@vci.co.id;";
	                                $to_name = "Henny;Febriyanto;";
	                                
	                                $body  = "Dear Henny, <br><br>";
	                                $body .= "Mohon lakukan approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)."<br><br>";
	                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
	                                
	                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);
	                            }
							}
											
						}
						else if($v_Status=="void")
                        {
                        	/*$q = "
	                            UPDATE
	                                ".$db["master"].".trans_order_barang_header
	                            SET     
	                                Status = '2',
	                                EditDate = NOW(),
	                                EditUser = '".$ses_login."' 
	                             WHERE 1
	                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
	                        ";
	                        //echo $q."<hr/>";                  
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan trans_order_barang_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        } */
	                        
	                        
	                        
	                        //kurangi QtyPO di trans_pr_detail
                            $qs = "
                                   SELECT 
									  a.`NoDokumen`,
									  a.`NoPr`,
									  b.`PCode`,
									  b.`Qty`
									FROM
									  ".$db["master"].".`trans_order_barang_header` a 
									  INNER JOIN ".$db["master"].".`trans_order_barang_detail` b 
									    ON a.`NoDokumen` = b.`NoDokumen` 
									WHERE a.`NoDokumen` = '".$v_NoDokumen."' ; 
                            	 ";
                            	
                            $qry = mysql_query($qs);
                            while($rowx = mysql_fetch_array($qry))
                            {
                                    //Ambil nilai QtyPO di trans_pr_detail
                                    $QtyPO_ditrans_prs = "
                                    SELECT a.QtyPO AS QuantityPO FROM `trans_pr_detail` a WHERE a.`NoDokumen`='".$rowx['NoPr']."' AND a.`PCode`='".$rowx['PCode']."';
									";
									
									$qryss = mysql_query($QtyPO_ditrans_prs);
									$rows = mysql_fetch_array($qryss);
									list($QuantityPO) = $rows;
                               
                               		$QtyPO_new = $QuantityPO - $rowx['Qty'];
                               		
                               		//update juga ke Purchase Request Detail
                                    $sql = "
                                    		UPDATE 
											  `trans_pr_detail` 
											SET
											  `QtyPO` = '".$QtyPO_new."' 
											WHERE `NoDokumen` = '".$rowx['NoPr']."' AND
											  `PCode` = '".$rowx['PCode']."' ;
                                    		";
                                    if(!mysql_query($sql))
                                    {
                                        $msg = "Gagal Update trans_pr_detail";
                                        echo "<script>alert('".$msg."');</script>";
										
                                        die(); 
                                    }
                               		
                               		
                            }
	                        
	                        
	                        
	                               
		                    $msg = "Berhasil Void ".$v_NoDokumen;
		                    echo "<script>alert('".$msg."');</script>"; 
		                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
		                    die();
                        }
						else if($v_Status=="unvoid")
                        {
                        	$q = "
	                            UPDATE
	                                ".$db["master"].".trans_order_barang_header
	                            SET     
	                                Status = '0',
	                                EditDate = NOW(),
	                                EditUser = '".$ses_login."' 
	                             WHERE 1
	                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'   
	                        ";
	                        //echo $q."<hr/>";                  
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan trans_order_barang_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die(); 
	                        } 
	                        
	                        
	                        //kurangi QtyPO di trans_pr_detail
                            $q = "
                                   SELECT 
									  a.`NoDokumen`,
									  a.`NoPr`,
									  b.`PCode`,
									  b.`Qty`
									FROM
									  ".$db["master"].".`trans_order_barang_header` a 
									  INNER JOIN ".$db["master"].".`trans_order_barang_detail` b 
									    ON a.`NoDokumen` = b.`NoDokumen` 
									WHERE a.`NoDokumen` = '".$v_NoDokumen."' ; 
                            	 ";
                            	
                            $qry = mysql_query($q);
                            while($rowx = mysql_fetch_array($qry))
                            {
                                    //Ambil nilai QtyPO di trans_pr_detail
                                    $QtyPO_ditrans_prs = "
                                    SELECT a.QtyPO AS QuantityPO FROM `trans_pr_detail` a WHERE a.`NoDokumen`='".$rowx['NoPr']."' AND a.`PCode`='".$rowx['PCode']."';
									";
									
									$qryss = mysql_query($QtyPO_ditrans_prs);
									$rows = mysql_fetch_array($qryss);
									list($QuantityPO) = $rows;
                               
                               		$QtyPO_new = $QuantityPO + $rowx['Qty'];
                               		
                               		//update juga ke Purchase Request Detail
                                    $sql = "
                                    		UPDATE 
											  `trans_pr_detail` 
											SET
											  `QtyPO` = '".$QtyPO_new."' 
											WHERE `NoDokumen` = '".$rowx['NoPr']."' AND
											  `PCode` = '".$rowx['PCode']."' ;
                                    		";
                                    if(!mysql_query($sql))
                                    {
                                        $msg = "Gagal Update trans_pr_detail";
                                        echo "<script>alert('".$msg."');</script>";
										
                                        die(); 
                                    }
                               		
                               		
                            }
	                        
	                               
		                    $msg = "Berhasil UnVoid ".$v_NoDokumen;
		                    echo "<script>alert('".$msg."');</script>"; 
		                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";         
		                    die();
                        }
                        
                        // perubahan ke open, kasih tau ke yang approval
                        /*if($v_Status*1==1 && $v_Status_old*1==0)
                        {
                            if($grand_total*1<=5000000)    
                            {
                                // sari                              
                                $subject = "Request Approval PO ".$v_NoDokumen;
                                $author  = "Auto System";
                                
                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                                
                                $to = "sari@secretgarden.co.id;kadeksari01@gmail.com;hnd@vci.co.id;";
                                $to_name = "Kadek Sari;Kadek Sari;Hendri;";
                                
                                $body  = "Dear Kadek Sari, <br><br>";
                                $body .= "Mohon lakukan approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)."<br><br>";
                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
                                
                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);   
                                
                                echo "masuk sini cuy";
                            }
                            else if($grand_total*1>5000000 && $grand_total*1<10000000)    
                            {
                                // trisno
                                $subject = "Request Approval PO ".$v_NoDokumen;
                                $author  = "Auto System";
                                
                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                                
                                $to = "eno@vci.co.id;hnd@vci.co.id;";
                                $to_name = "Trisno;Hendri;";
                                
                                $body  = "Dear Bambang Sutrisno, <br><br>";
                                $body .= "Mohon lakukan approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)."<br><br>";
                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
                                
                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);   
                            }
                            else if($grand_total*1>=10000000)    
                            {
                                // henny
                                $subject = "Request Approval PO ".$v_NoDokumen;
                                $author  = "Auto System";
                                
                                $url = "http://sys.bebektimbungan.com/inventory/npm_purchase_order.php?id=".$v_NoDokumen;
                                
                                $to = "hst@vci.co.id;hnd@vci.co.id;";
                                $to_name = "Henny;Hendri;";
                                
                                $body  = "Dear Henny, <br><br>";
                                $body .= "Mohon lakukan approval untuk PO ".$v_NoDokumen." senilai ".$v_currencycode." ".format_number($grand_total)."<br><br>";
                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
                                
                                $kirim   = send_email_multiple($subject, $body, $to, $to_name, $author);
                            }
                        }*/
                        
						$msg = "Berhasil menyimpan ".$v_NoDokumen;
		                echo "<script>alert('".$msg."');</script>"; 
		                echo "<script>parent.CallAjaxForm('search','".$v_NoDokumen."');</script>";         
		                die();
                    } 
                }  
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  