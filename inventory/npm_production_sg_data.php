<?php
include("header.php");
                               
$modul            = "PRODUCTION SECRET GARDEN";
$file_current     = "npm_production_sg.php";
$file_name        = "npm_production_sg_data.php";

//$ses_login = "admin";     

function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}
          
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
        $search_keyword        = trim($_GET["search_keyword"]);
        $v_date_from           = trim($_GET["v_date_from"]);
        $v_date_to             = trim($_GET["v_date_to"]);   
        $search_purposeid      = trim($_GET["search_purposeid"]);
        $search_KdGudang       = trim($_GET["search_KdGudang"]);
        $v_BatchNumber_curr    = trim($_GET["v_BatchNumber_curr"]);
        
        $where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "production_sg.BatchNumber";    
            $arr_keyword[1] = "production_sg.planning_no";    
            $arr_keyword[2] = "production_sg.remarks";    
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        if($v_BatchNumber_curr)
        {
            $where = " AND production_sg.BatchNumber = '".$v_BatchNumber_curr."'";
        }
        
        $where_purpose = "";
        if($search_purposeid!="")
        {
            $where_purpose = " AND production_sg.purpose_id = '".$search_purposeid."'";
        }
        
        $where_KdGudang = "";
        if($search_KdGudang!="")
        {
            $where_KdGudang = " AND production_sg.KdGudang = '".$search_KdGudang."'";
        }
        
       
        // select query all
        {
            $counter = 1;
            $q="
                SELECT 
                  production_sg.production_id,
                  production_sg.production_date,
                  production_sg.KdGudang,
                  production_sg.BatchNumber,
                  production_sg.purpose_id,
                  production_sg.planning_no,
                  production_sg.remarks,
                  production_sg.status_production
                FROM
                  production_sg 
                WHERE 1 
                    ".$where."
                    ".$where_purpose."
                    ".$where_KdGudang."
                    AND 
                        (
                            production_sg.production_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                        )
                ORDER BY 
                    production_sg.production_date ASC,
                    production_sg.production_id ASC
            ";
            //echo $q;
            $sql=mysql_query($q);
            while($row=mysql_fetch_array($sql))
            { 
                list(
                    $production_id,
                    $production_date,
                    $KdGudang,
                    $BatchNumber,
                    $purpose_id,
                    $planning_no,
                    $remarks,
                    $status_production 
                )=$row;
                
                $arr_data["list_data"][$production_id]=$production_id;
                $arr_data["data_production_date"][$production_id]=$production_date;
                $arr_data["data_KdGudang"][$production_id]=$KdGudang;
                $arr_data["data_BatchNumber"][$production_id]=$BatchNumber;
                $arr_data["data_purpose_id"][$production_id]=$purpose_id;
                $arr_data["data_planning_no"][$production_id]=$planning_no;
                $arr_data["data_remarks"][$production_id]=$remarks;
                $arr_data["data_status_production"][$production_id]=$status_production;
                
                $counter++;
            }
        }  
        
        
        $q = "
                SELECT
                    intmutpurpose.purposeid,
                    intmutpurpose.purpose
                FROM
                    intmutpurpose
                WHERE
                    1
                    AND intmutpurpose.purposeid IN('40','41','42','43')
                ORDER BY
                    intmutpurpose.purpose ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($purposeid, $purpose_name) = $row;
            
            $arr_data["list_purpose"][$purposeid] = $purposeid;
            $arr_data["purpose_name"][$purposeid] = $purpose_name;
        }
         
         
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('03','05')
                ORDER BY
                    gudang.Keterangan ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $NamaGudang) = $row;
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = str_replace("GUDANG", "", $NamaGudang);
        } 
        
        
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_production_sg_data.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
            <tr class="title_table">
                <td rowspan="2" width="30" style="vertical-align: middle;">No</td>
                <td rowspan="2" style="vertical-align: middle;">BatchNumber</td>
                <td rowspan="2" style="vertical-align: middle;"> Tanggal</td>
                <td rowspan="2" style="vertical-align: middle;">Gudang</td>
                <td rowspan="2" style="vertical-align: middle;">Tujuan</td>
                <td rowspan="2" style="vertical-align: middle;">Planning</td>
                <td rowspan="2" style="vertical-align: middle;">Keterangan</td>
                <td colspan="3" style="text-align: center;">TOTAL KG</td>
                <td rowspan="2" style="vertical-align: middle;">Status</td>
                <td width="50" rowspan="2">
                    Action<br/>
                    <input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')">
                </td>
            </tr>
            
            <tr class="title_table" style="text-align: center;">
                <td>Baku</td>
                <td>Jadi</td>
                <td>Waste</td>
            </tr>
            
        <tbody style="color: black;">
        
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        foreach($arr_data["list_data"] as $production_id => $val)
        {    
            $nomor++;
            
            $production_date = $arr_data["data_production_date"][$production_id];
            $KdGudang = $arr_data["data_KdGudang"][$production_id];
            $BatchNumber = $arr_data["data_BatchNumber"][$production_id];
            $purpose_id = $arr_data["data_purpose_id"][$production_id];
            $planning_no = $arr_data["data_planning_no"][$production_id];
            $remarks = $arr_data["data_remarks"][$production_id];
            $status_production = $arr_data["data_status_production"][$production_id]; 
            
            $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
            $purpose_name = $arr_data["purpose_name"][$purpose_id];
            
            $style_color = "";
            if($v_prodplancode_curr==$prodplancode)
            {
                $style_color = "background: #CAFDB5";
            }
                    
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $BatchNumber; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo format_show_date($production_date); ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $NamaGudang; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $purpose_name; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $planning_no; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $remarks; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo "1"; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo "1"; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo "3"; ?></td> 
                <td onclick="CallAjaxForm('baku','<?php echo $BatchNumber; ?>')" align="" style=""><?php echo $status_production; ?></td> 
                <td align="center">
                    &nbsp;
                    
                    <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $BatchNumber; ?>">
                    
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot style="">
            <tr>
                <td colspan="100%" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Close">Close BatchNumber</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
    </table> 
    </form>
    </div>  
       <?php  
    }

    else if($ajax=="add_data")
    {
        
        $q = "
                SELECT
                    intmutpurpose.purposeid,
                    intmutpurpose.purpose
                FROM
                    intmutpurpose
                WHERE
                    1
                    AND intmutpurpose.purposeid IN('40','41','42','43')
                ORDER BY
                    intmutpurpose.purpose ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($purposeid, $purpose_name) = $row;
            
            $arr_data["list_purpose"][$purposeid] = $purposeid;
            $arr_data["purpose_name"][$purposeid] = $purpose_name;
        }
         
         
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('03','05')
                ORDER BY
                    gudang.Keterangan ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $NamaGudang) = $row;
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = str_replace("GUDANG", "", $NamaGudang);
        } 
        
        ?>
        <div class="col-md-12" align="left">
        
            <ol class="breadcrumb title_table">
                <li><strong><i class="entypo-pencil"></i>TAMBAH <?php echo $modul; ?></strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
            
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">

            
            <tr>
                <td class="title_table" width="200">Tanggal <font color="red">(*)</font></td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_production_date" id="v_production_date" size="10" maxlength="10" value="<?php echo date("d/m/Y"); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_production_date, 'dd/mm/yyyy');">
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Gudang <font color="red">(*)</font></td>
                <td>
                    <select class="form-control-new" name="v_KdGudang" id="v_KdGudang" style="width: 200px;">
                        <option value="">-</option>
                        
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                ?>
                                <option value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>     
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">BatchNumber <font color="red">(*)</font></td>
                <td>
                    <input type="text" class="form-control-new" name="v_BatchNumber" id="v_BatchNumber" style="width: 200px;">
                </td>
                
            </tr>
            
            
            <tr>
                <td class="title_table">Tujuan <font color="red">(*)</font></td>
                <td>
                    <select class="form-control-new" name="v_purposeid" id="v_purposeid" style="width: 200px;">
                        <option value="">-</option>
                        
                        <?php 
                            foreach($arr_data["list_purpose"] as $purposeid=>$val)
                            {
                                $purpose_name = $arr_data["purpose_name"][$purposeid];
                                
                                ?>
                                <option value="<?php echo $purposeid; ?>"><?php echo $purpose_name; ?></option>
                                <?php
                            }
                        ?>
                    </select>     
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Planning No <font color="red">(*)</font></td>
                <td>
                    <select class="form-control-new" name="v_planning_no" id="v_planning_no" style="width: 200px;">
                        <option value="">-</option>
                        <option value="No Planning">No Planning</option>
                    </select>     
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Keterangan</td>
                <td>
                    <input type="text" class="form-control-new" name="v_remarks" id="v_remarks" style="width: 200px;">
                </td>
                
            </tr>
            
            <tr> 
                <td>&nbsp;</td>                   
                <td>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
            
            </table>  
            </form>
        </div>
        <?php
        
    }
    
    else if($ajax=="baku")
    {                                      
        $v_BatchNumber = $_GET["v_BatchNumber"];  
        
        $q = "
                SELECT 
                  production_sg.production_id,
                  production_sg.production_date,
                  production_sg.BatchNumber,
                  production_sg.KdGudang,
                  production_sg.purpose_id,
                  production_sg.planning_no,
                  production_sg.remarks,
                  production_sg.author_user,
                  production_sg.author_date,
                  production_sg.edited_user,
                  production_sg.edited_date
                FROM
                  production_sg
                WHERE 1
                    AND production_sg.BatchNumber = '".$v_BatchNumber."'
                LIMIT
                    0,1
        ";     
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
        
         $q = "
                SELECT
                    intmutpurpose.purposeid,
                    intmutpurpose.purpose
                FROM
                    intmutpurpose
                WHERE
                    1
                    AND intmutpurpose.purposeid IN('40','41','42','43')
                ORDER BY
                    intmutpurpose.purpose ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($purposeid, $purpose_name) = $row;
            
            $arr_data["list_purpose"][$purposeid] = $purposeid;
            $arr_data["purpose_name"][$purposeid] = $purpose_name;
        }
         
         
        $q = "
                SELECT
                    gudang.KdGudang,
                    gudang.Keterangan
                FROM
                    gudang
                WHERE
                    1
                    AND gudang.KdGudang IN('03','05')
                ORDER BY
                    gudang.Keterangan ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($KdGudang, $NamaGudang) = $row;
            
            $arr_data["list_gudang"][$KdGudang] = $KdGudang;
            $arr_data["NamaGudang"][$KdGudang] = str_replace("GUDANG", "", $NamaGudang);
        } 
           
        ?> 
        
        <div class="col-md-12" align="left">
            
            <ol class="breadcrumb">
                <li><strong><i class="entypo-pencil"></i>EDIT <?php echo $modul; ?></strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
                                                       
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value="">    
        <input type="hidden" name="v_production_id" id="v_production_id" value="<?php echo $arr_curr["production_id"]; ?>">   
        
        <table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="200">Tanggal <font color="red">(*)</font></td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_production_date" id="v_production_date" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["production_date"]); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_production_date, 'dd/mm/yyyy');">
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Gudang <font color="red">(*)</font></td>
                <td>
                    <select class="form-control-new" name="v_KdGudang" id="v_KdGudang" style="width: 200px;">
                        <?php 
                            foreach($arr_data["list_gudang"] as $KdGudang=>$val)
                            {
                                $NamaGudang = $arr_data["NamaGudang"][$KdGudang];
                                
                                $selected = "";
                                if($KdGudang==$arr_curr["KdGudang"])
                                {
                                    $selected = "selected='selected'";    
                                }
                                
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $KdGudang; ?>"><?php echo $NamaGudang; ?></option>
                                <?php
                            }
                        ?>
                    </select>     
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">BatchNumber <font color="red">(*)</font></td>
                <td>
                    <input type="text" class="form-control-new" name="v_BatchNumber" id="v_BatchNumber" style="width: 200px;" value="<?php echo $arr_curr["BatchNumber"]; ?>">
                </td>
                
            </tr>
            
            
            <tr>
                <td class="title_table">Tujuan <font color="red">(*)</font></td>
                <td>
                    <select class="form-control-new" name="v_purposeid" id="v_purposeid" style="width: 200px;">
                        <?php 
                            foreach($arr_data["list_purpose"] as $purposeid=>$val)
                            {
                                $purpose_name = $arr_data["purpose_name"][$purposeid];
                                
                                $selected = "";
                                if($purposeid==$arr_curr["purpose_id"])
                                {
                                    $selected = "selected='selected'";    
                                }
                                
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $purposeid; ?>"><?php echo $purpose_name; ?></option>
                                <?php
                            }
                        ?>
                    </select>     
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Planning No <font color="red">(*)</font></td>
                <td>
                    <select class="form-control-new" name="v_planning_no" id="v_planning_no" style="width: 200px;">
                        <option value="No Planning">No Planning</option>
                    </select>     
                </td>
                
            </tr>
            
            <tr>
                <td class="title_table">Keterangan</td>
                <td>
                    <input type="text" class="form-control-new" name="v_remarks" id="v_remarks" style="width: 200px;" value="<?php echo $arr_curr["remarks"]; ?>">
                </td>
                
            </tr> 
            
            <tr>
                <td colspan="100%" align="center">
                    <?php echo menutab($v_BatchNumber, 'baku'); ?>  
                </td>
            </tr>
            
            
            
            
            <tr> 
                <td colspan="100%">
                    <table class="table table-bordered responsive" style="color: black;">
                        <tr>
                            <td>ADD</td>
                            <td>
                                <input type="text" class="form-control-new" name="v_PCode_add" id="v_PCode_add" style="width: 150px; border: solid 1px #14141f;">
                                <button type="button" onclick="pop_cari_pcode('<?php echo $arr_curr["production_id"]; ?>')" name="btn_cari_pcode" id="btn_cari_pcode" class="btn btn-info" value="Cari PCode">?</button>
                            </td>
                            <td id="td_NamaLengkap_add">&nbsp;</td>
                            <td align="right"><input type="text" class="form-control-new" name="v_qty_add" id="v_qty_add" value="" style="width: 70px; text-align: right; border: solid 1px #14141f; "></td>
                            <td id="td_Satuan_add">&nbsp;</td>
                            <td align="center"><button type="button" onclick="save_detail('<?php echo $arr_curr["production_id"]; ?>')" name="btn_save_detail" id="btn_save_detail" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button></td>
                        </tr>
                        <tr class="title_table">
                            <td width="30">No</td>
                            <td width="200">PCode</td>
                            <td>Nama Barang</td>
                            <td width="80" align="right">Qty</td>
                            <td width="50">Satuan</td>
                            <td width="80" align="center">
                                Hapus<br>
                                <input type="checkbox" id="chkall_del" onclick="CheckAll('chkall_del', 'v_data_del[]')">
                            </td>
                        </tr>  
                        
                        <tbody style="color: black;">
                            <?php 
                                foreach($arr_data["list_detail"] as $counter=>$val)
                                {
                            ?>
                            <tr>
                                <td><?php echo $counter; ?></td>
                                <td>
                                    <input type="text" class="form-control-new" name="v_PCode_<?php echo $counter; ?>" id="v_PCode_<?php echo $counter; ?>" style="width: 150px; border: solid 1px #14141f;">
                                    <button type="button" onclick="pop_cari_pcode('<?php echo $arr_curr["production_id"]; ?>')" name="btn_cari_pcode" id="btn_cari_pcode" class="btn btn-info" value="Cari PCode">?</button>
                                </td>
                                <td id="td_NamaLengkap_<?php echo $counter; ?>">&nbsp;</td>
                                <td align="right"><input type="text" class="form-control-new" name="v_qty_add" id="v_qty_add" value="" style="width: 70px; text-align: right; border: solid 1px #14141f; "></td>
                                <td id="td_Satuan_<?php echo $counter; ?>">&nbsp;</td>
                                <td align="center"><button type="button" onclick="save_detail('<?php echo $arr_curr["production_id"]; ?>')" name="btn_save_detail" id="btn_save_detail" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Save<i class="entypo-check"></i></button></td>
                            </tr> 
                            <?php 
                                }
                            ?>  
                        </tbody>      
                    </table>
                </td>                   
                
            </tr>
            
            
            <tr>
                <td class="title_table" colspan="2">INFORMASI DATA</td>      
            </tr> 
            
            <tr>
                <td class="title_table">Dibuat</td>      
                <td><?php echo format_show_datetime($arr_curr["author_date"])." :: ".$arr_curr["author_user"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Diedit</td>      
                <td><?php echo format_show_datetime($arr_curr["edited_date"])." :: ".$arr_curr["edited_user"]; ?></td>
            </tr>
             
            
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                
                if(!isset($_POST["v_production_date"])){ $v_production_date = isset($_POST["v_production_date"]); } else { $v_production_date = $_POST["v_production_date"]; }                                                                                                                                        
                if(!isset($_POST["v_KdGudang"])){ $v_KdGudang = isset($_POST["v_KdGudang"]); } else { $v_KdGudang = $_POST["v_KdGudang"]; }     
                if(!isset($_POST["v_BatchNumber"])){ $v_BatchNumber = isset($_POST["v_BatchNumber"]); } else { $v_BatchNumber = $_POST["v_BatchNumber"]; }     
                if(!isset($_POST["v_purposeid"])){ $v_purposeid = isset($_POST["v_purposeid"]); } else { $v_purposeid = $_POST["v_purposeid"]; }     
                if(!isset($_POST["v_planning_no"])){ $v_planning_no = isset($_POST["v_planning_no"]); } else { $v_planning_no = $_POST["v_planning_no"]; }     
                if(!isset($_POST["v_remarks"])){ $v_remarks = isset($_POST["v_remarks"]); } else { $v_remarks = $_POST["v_remarks"]; }     
                
                if(!isset($_POST["v_planning_no"])){ $v_planning_no = isset($_POST["v_planning_no"]); } else { $v_planning_no = $_POST["v_planning_no"]; }
                
                $v_KdGudang = save_char($v_KdGudang);
                $v_BatchNumber = save_char($v_BatchNumber);
                $v_purposeid = save_char($v_purposeid);
                $v_planning_no = save_char($v_planning_no);
                $v_remarks = save_char($v_remarks);

                // validasi
                {
                    if($v_production_date=="")
                    {
                        $msg = "Tanggal harus diisi...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else if($v_KdGudang=="")
                    {
                        $msg = "Gudang harus dipilih...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else if($v_BatchNumber=="")
                    {
                        $msg = "BatchNumber harus diisi...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else if($v_purposeid=="")
                    {
                        $msg = "Tujuan harus diisi...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    else if($v_planning_no=="")
                    {
                        $msg = "Planning No harus diisi...";
                        echo "<script>alert('".$msg."');</script>";
                        die();    
                    }
                    
                    $q = "
                            SELECT
                                production_sg.BatchNumber
                            FROM
                                production_sg
                            WHERE
                                1
                                AND production_sg.BatchNumber = '".$v_BatchNumber."'
                            LIMIT
                                0,1
                    ";
                    $qry = mysql_query($q);
                    $row = mysql_fetch_array($qry);
                    list($BatchNumber_exist) = $row;
                    
                    if($BatchNumber_exist)
                    {
                        $msg = "BatchNumber ".$BatchNumber_exist." sudah ada...";
                        echo "<script>alert('".$msg."');</script>";
                        die();     
                    }
                }
               
                // insert production
                {
                    $q = "
                        INSERT INTO `production_sg`
                        SET `production_id` = '".$production_id."',
                          `production_date` = '".format_save_date($v_production_date)."',
                          `KdGudang` = '".$v_KdGudang."',
                          `BatchNumber` = '".$v_BatchNumber."',
                          `purpose_id` = '".$v_purposeid."',
                          `planning_no` = '".$v_planning_no."',
                          `remarks` = '".$v_remarks."',
                          `author_user` = '".$ses_login."',
                          `author_date` = NOW(),
                          `edited_user` = '".$ses_login."',
                          `edited_date` = NOW()
                    ";   
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan production_sg";
                        echo "<script>alert('".$msg."');</script>";
                        die(); 
                    }    
                }
                
              
                $msg = "Berhasil menyimpan ".$v_BatchNumber;
                echo "<script>alert('".$msg."');</script>"; 
                echo "<script>parent.CallAjaxForm('search','".$v_BatchNumber."');</script>";             
                
                die();
                   
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {                                                                                                      
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }    
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }   
                if(!isset($_POST["v_prodplancode"])){ $v_prodplancode = isset($_POST["v_prodplancode"]); } else { $v_prodplancode = $_POST["v_prodplancode"]; }  
                
                if(!isset($_POST["v_start_date"])){ $v_start_date = isset($_POST["v_start_date"]); } else { $v_start_date = $_POST["v_start_date"]; }                                                                                                                                        
                if(!isset($_POST["v_end_date"])){ $v_end_date = isset($_POST["v_end_date"]); } else { $v_end_date = $_POST["v_end_date"]; }     
                
                if(!isset($_POST["v_pcode"])){ $v_pcode = isset($_POST["v_pcode"]); } else { $v_pcode = $_POST["v_pcode"]; }     
                if(!isset($_POST["v_data_del"])){ $v_data_del = isset($_POST["v_data_del"]); } else { $v_data_del = $_POST["v_data_del"]; }     
                
                    
                if($v_start_date=="")
                {
                    $msg = "Period harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_end_date=="")
                {
                    $msg = "Period harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                
                if($v_del==1)   
                {
                    if($btn_delete=="Hapus")
                    {
                        // cek PB
                        $q = "
                                SELECT
                                    matreq.prodplancode
                                FROM
                                    matreq
                                WHERE
                                    1
                                    matreq.prodplancode = '".$v_prodplancode."'
                                LIMIT
                                    0,1
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($prodplancode_matreq) = $row;
                        if($prodplancode_matreq)
                        {
                            $msg = $prodplancode_matreq." Tidak bisa dihapus karena sudah ada Matreq";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".prodplan
                            WHERE 1
                                AND ".$db["master"].".prodplan.prodplancode = '".$v_prodplancode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan prodplan";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".prodplandetail
                            WHERE 1
                                AND ".$db["master"].".prodplandetail.prodplancode = '".$v_prodplancode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan prodplandetail";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
                        $msg = "Berhasil Hapus ".$v_prodplancode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '');</script>";         
                        die();
                    }
                }
                else
                {
                    if($btn_save=="Save")
                    {   
                        // update masterbarang
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".prodplan
                                SET     
                                    `startingperiod` = '".format_save_date($v_start_date)."',
                                    `endingperiod` = '".format_save_date($v_end_date)."',
                                    `edituser` = '".$ses_login."',
                                    `editdate` = NOW()
                                 WHERE 1
                                    AND ".$db["master"].".prodplan.prodplancode = '".$v_prodplancode."'   
                            ";
                            //echo $q."<hr/>";                  
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan prodplan";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            
                            foreach($v_pcode as $key_pcode=>$val_pcode)
                            {
                                if(!isset($_POST["v_batch_".$val_pcode])){ $v_batch = isset($_POST["v_batch_".$val_pcode]); } else { $v_batch = $_POST["v_batch_".$val_pcode]; }         
                                if(!isset($_POST["v_formula_".$val_pcode])){ $v_formula = isset($_POST["v_formula_".$val_pcode]); } else { $v_formula = $_POST["v_formula_".$val_pcode]; }         
                                
                                $v_batch = save_int($v_batch);
                                
                                if($v_formula)
                                {
                                    $q = "
                                            UPDATE `prodplandetail`
                                            SET 
                                              `quantity` = '".$v_batch."',
                                              `remainquantity` = '".$v_batch."',
                                              `remainquantitypack` = '".$v_batch."',
                                              `edituser` = '".$ses_login."',
                                              `editdate` = NOW(),
                                              `formula` = '".$v_formula."'
                                            WHERE 
                                                `prodplancode` = '".$v_prodplancode."'  
                                                AND `inventorycode` = '".$val_pcode."'  
                                    ";
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal simpan prodplandetail ".$v_prodplancode;
                                        echo "<script>alert('".$msg."');</script>"; 
                                        die();
                                    }
                                }  
                            }
                            
                            foreach($v_data_del as $key_del=>$val_del)
                            {
                                $q = "
                                        DELETE FROM `prodplandetail`
                                        WHERE 
                                            `prodplancode` = '".$v_prodplancode."'  
                                            AND `inventorycode` = '".$val_del."'  
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal hapus prodplandetail ".$v_prodplancode;
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();
                                }    
                            }
                            
                        }
                        
                        $msg = "Berhasil menyimpan ".$v_prodplancode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search','".$v_prodplancode."');</script>";         
                        die();
                    } 
                }  
            } 
            break;   
    }
}         

function menutab($v_batchnumber, $menu){
    $str_menu = "
    <style type=\"text/css\">
        #smalltab {
            list-style-type : none;
            margin : 5px 0px 0px 0px;
            font: bold 11px tahoma, sans-serif;
            border-bottom: 1px solid #eeeeff;
        }
        #smalltab li {
            display : inline;
            border-top : 1px #ccccff solid;
            border-left : 1px #ccccff solid;
            border-right : 1px #ccccff solid;
            background-color : #F5FAFF;
            padding : 5px 10px 1px 10px;
        }
        
        #smalltab li a{
            text-decoration : none;
        }
        #smalltab li:hover {
            background-color : #ccccff;
        }
        #smalltab .current {
            background-color : #ccccff;
        }
    </style>

    <div align=\"center\" style=\"padding-top:5px;\"> 
    <ul id=\"smalltab\">
        <li ".($menu=='baku'?"class='current'":" ")." onclick=\"CallAjax('baku', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Baku</a></li>
        <li ".($menu=='kemas'?"class='current'":" ")." onclick=\"CallAjax('kemas', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Kemas</a></li>
        <li ".($menu=='jadi'?"class='current'":" ")." onclick=\"CallAjax('jadi', '".$v_batchnumber."')\"><a href=\"javascript:void(0)\">Jadi</a></li>
    </ul>
    </div> 
    
";
        return $str_menu;
}                                             

mysql_close($con);
?>  