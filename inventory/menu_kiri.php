<?php $base_url = "http://sys.bebektimbungan.com/"; ?>

<div class="sidebar-menu">
    <header class="logo-env">
        <div class="logo">
            <a href="index.html">
                <img src="../public/images/Logosg.png" width="120" alt="" />
            </a>
        </div>
        <div class="sidebar-collapse">
            <a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                <i class="entypo-menu"></i>
            </a>
        </div>
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <div class="sidebar-user-info">
        <div class="sui-normal">
            <a href="#" class="user-link">
                <span><?php echo date('d-m-Y'); ?></span>
                <strong>
                    <?php echo $ses_login; ?>
                </strong>
            </a> 
                                                     
        </div>
        
        
        
    </div>
    
<?php
    $q = "
            SELECT
                ".$db["master"].".user.UserName,
                ".$db["master"].".user.UserLevel
            FROM
                ".$db["master"].".user
            WHERE
                1
                AND ".$db["master"].".user.UserName = '".$ses_login."'
            
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    list($UserName, $userlevelid) = $row;

     $q = "
            SELECT 
              menu.`nama`,
              menu.`ulid`,
              menu.`root`,
              menu.`url`,
              menu.`urutan`,
              menu.`icon`,
              menu.sts_apps
            FROM
              menu 
              INNER JOIN `userlevelpermissions` 
                ON menu.`nama` = userlevelpermissions.`tablename` 
                AND `menu`.FlagAktif = '1' 
                AND userlevelpermissions.`userlevelid` = '".$userlevelid."' 
                AND (
                  userlevelpermissions.`add` = 'Y' 
                  OR userlevelpermissions.`edit` = 'Y' 
                  OR userlevelpermissions.`delete` = 'Y' 
                  OR userlevelpermissions.`view` = 'Y'
                ) 
            WHERE 1 
            GROUP BY    
              menu.`nama`,
              menu.`ulid`,
              menu.`root`,
              menu.`url`,
              menu.`urutan`,
              menu.`icon`,
              menu.sts_apps 
            ORDER BY 
                urutan ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($nama, $ulid, $root, $url, $urutan, $icon, $sts_apps) = $row;
        
        $arr_data["list_menu_permission"][$nama] = $nama;
    }
    
?>


<ul id="main-menu" class="">
    <?php
    $q = "
            SELECT 
              menu.`nama`,
              menu.`ulid`,
              menu.`root`,
              menu.`url`,
              menu.`urutan`,
              menu.`icon`,
              menu.sts_apps
            FROM
              menu 
            WHERE 1 
            AND menu.`root` = '1'
            AND `menu`.FlagAktif = '1' 
            GROUP BY    
              menu.`nama`,
              menu.`ulid`,
              menu.`root`,
              menu.`url`,
              menu.`urutan`,
              menu.`icon`,
              menu.sts_apps
            ORDER BY 
                urutan ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($nama, $ulid, $root, $url, $urutan, $icon, $sts_apps) = $row;
        
        $url = str_replace("inventory/", "", $url);
        
        if($sts_apps*1!=3 && $url)
        {
            $url = $base_url.$url;    
        }
        
        if($nama=="Logout")
        {
            $url = $base_url."index.php/logout";    
        }
        
            if($arr_data["list_menu_permission"][$nama]==$nama)
            {
            ?>
                <li>
                    <a href="<?=$url;?>">
                        <?php echo (!empty($icon))?"<i class='".$icon."'></i>":"<i class='entypo-window'></i>";?>
                        <span><?php echo $nama;?></span>
                    </a>
                    
                        <ul>
                            <?php
                                $q = "
                                        SELECT 
                                          menu.`nama`,
                                          menu.`ulid`,
                                          menu.`root`,
                                          menu.`url`,
                                          menu.`urutan`,
                                          menu.`icon`,
                                          menu.sts_apps
                                        FROM
                                          menu 
                                        WHERE 1 
                                        AND menu.`root` = '".$ulid."'
                                        AND `menu`.FlagAktif = '1' 
                                        GROUP BY    
                                          menu.`nama`,
                                          menu.`ulid`,
                                          menu.`root`,
                                          menu.`url`,
                                          menu.`urutan`,
                                          menu.`icon`,
                                          menu.sts_apps 
                                        ORDER BY 
                                            urutan ASC
                                ";
                                $qry_menu = mysql_query($q);
                                while($row_menu = mysql_fetch_array($qry_menu))
                                {
                                    list($nama_menu, $ulid_menu, $root_menu, $url_menu, $urutan_menu, $icon_menu, $sts_apps_menu) = $row_menu;
                                    
                                    $url_menu = str_replace("inventory/", "", $url_menu);
        
                                    if($sts_apps_menu*1!=3 && $url_menu)
                                    {
                                        $url_menu = $base_url.$url_menu;    
                                    } 
                                    
                                    if($arr_data["list_menu_permission"][$nama_menu]==$nama_menu)
                                    {
                            ?>
                                    <li><a href="<?=$url_menu?>"><?php echo (!empty($icon_menu))?"<i class='".$icon_menu."'></i>":"<i class='entypo-flow-branch'></i>";?><span><?=$nama_menu;?></span></a>
                                        
                                        <ul>
                                            <?php 
                                            
                                                $q_submenu = "
                                                        SELECT 
                                                          menu.`nama`,
                                                          menu.`ulid`,
                                                          menu.`root`,
                                                          menu.`url`,
                                                          menu.`urutan`,
                                                          menu.`icon`,
                                                          menu.sts_apps
                                                        FROM
                                                          menu 
                                                        WHERE 1 
                                                        AND menu.`root` = '".$nama_menu."'
                                                        AND `menu`.FlagAktif = '1' 
                                                        GROUP BY    
                                                          menu.`nama`,
                                                          menu.`ulid`,
                                                          menu.`root`,
                                                          menu.`url`,
                                                          menu.`urutan`,
                                                          menu.`icon`,
                                                          menu.sts_apps 
                                                        ORDER BY 
                                                            urutan ASC
                                                ";
                                                //echo $q."<hr>";
                                                $qry_submenu = mysql_query($q_submenu);
                                                while($row_submenu = mysql_fetch_array($qry_submenu))
                                                {
                                                    list($nama_submenu, $ulid_submenu, $root_submenu, $url_submenu, $urutan_submenu, $icon_submenu, $sts_apps_submenu) = $row_submenu;
                                                    
                                                    $url_submenu = str_replace("inventory/", "", $url_submenu);
        
                                                    if($sts_apps_submenu*1!=3 && $url_submenu)
                                                    {
                                                        $url_submenu = $base_url.$url_submenu;    
                                                    }  
                                                    
                                                    if($arr_data["list_menu_permission"][$nama_submenu]==$nama_submenu)
                                                    {
                                            ?>
                                            
                                        
                                            <li><a href="<?=$url_submenu?>"><?php echo (!empty($icon_submenu))?"<i class='".$icon_submenu."'></i>":"<i class='entypo-flow-branch'></i>";?><span><?=$nama_submenu;?></span></a></li>
                                            
                                            <?php 
                                                    }
                                                }
                                               
                                                
                                            ?>
                                        </ul>
                                    </li>                                    
                            <?php 
                                    }
                                }
                            ?>
                        </ul>
                </li>
            <?php
            }
        
    }
    ?>
</ul>


</div>
