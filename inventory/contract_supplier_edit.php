<?php
include("header.php");

$modul            = "Contract Supplier";
$file_name        = "contract_supplier_edit.php";

if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }


$flag = $_GET['flag'];
			if($flag=="new"){
				$q = "SELECT TglTrans FROM aplikasi";
				$qry = mysql_query($q);
				$hsl = mysql_fetch_array($qry);
				$tgltransaksi = $hsl[0];
				$notransaksi = "";
				$supplier = "";
				$tgl_terakhir = date('t', strtotime($tgltransaksi));//echo "TES $tgl_terakhir";
				$tgl1 = substr($tgltransaksi,0,4)."-".substr($tgltransaksi,5,2)."-01";//"2016-11-01";
				$tgl2 = substr($tgltransaksi,0,4)."-".substr($tgltransaksi,5,2)."-".$tgl_terakhir;//"2016-11-30";
				$disabled = "disabled";
				$disabled2 = "";
				$classtanggal = "class='form-control-new datepicker'";
				$readonly2 = "";
			}
			else{
					$notransaksi = $_GET['notransaksi'];
					$q = "SELECT * FROM contract_supplier_header WHERE NoTransaksi='".$notransaksi."'";
					$qry = mysql_query($q);
					$hsl = mysql_fetch_array($qry);

					$tgltransaksi 	= $hsl['TglTransaksi'];
					$tgl1 			= $hsl['PeriodeAwal'];
					$tgl2 			= $hsl['PeriodeAkhir'];
					$supplier		= $hsl['KdSupplier'];
					$nokontrak		= $hsl['NoKontrak'];

					mysql_free_result($qry);

					if($flag=="edit"){
						$disabled = "";
						$disabled2 = "";
						$classtanggal = "class='form-control-new datepicker'";
						$readonly2 = "";
					}
					else{
							$disabled = "disabled";
							$disabled2 = "disabled";
							$classtanggal = "class='form-control-new'";
							$readonly2 = "readonly";
					}
			}

			$readonly = "readonly";

	if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=report-kasir-ticket.xls");
        header("Content-type: application/vnd.ms-excel");
    }
    else
    {
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

	<script>
			function ReloadPage(){alert(0);
				var flag = document.getElementById("flag").value;
				if(flag=="edit"){
					alert(1);
					document.getElementById("pcode").focus();
				}
				else{
						alert(2);
				}
			}

			function format(harga){
				 harga=parseFloat(harga);
				 harga=harga.toFixed(2);
				 //alert(harga);
				 s = addSeparatorsNF(harga, '.', ',', '.');
				 return s;
			}

			function reform(val){
				var a = val.split(".");
				var b = a.join("");
				var c = b.split(",");
				var d = c.join(".");
				//alert(d);
				return d;
			}

			function toFormat(obj){
				var id = obj.id;
				//alert(id);
				//alert(document.getElementById(id).value);
				if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
				{
					//alert("That's not a number.")
					document.getElementById(id).value=0;
					//document.getElementById(id).focus();
				}
				document.getElementById(id).value=reform(document.getElementById(id).value);
				document.getElementById(id).value=format(document.getElementById(id).value);
			}

			function ubah_harga(harga){
				harga=parseFloat(harga);
				harga = harga.toFixed(2);
				s = addSeparatorsNF(harga, '.', ',', '.');
				return s;
			}

			function ubah_awal(harga){
				 t = harga.split(".");
				 k = t.join("");
				 s = k.split(",");
				 s = s.join(".");
				 return s;
			}

			function list_pcode(param){
				var supplier = document.getElementById('supplier').value;
				if(supplier==""){
					alert("Anda belum memilih Supplier");
					return false;
				}
				else{
						window.open(param,'','width=600,height=600,left=250,top=100');
				}
			}

			function ExportExcel(id){
				alert(id);

				// $.ajax({
				// 	url: "http://sys.bebektimbungan.com/index.php/exportcontract/ExportContract/",
				// 	data: {notrans:id},
				// 	type: "POST",
				// 	dataType: 'json',
				// 	success: function(res)
				// 	{
				// 		// $('#v_satuan').html(res);
				// 	},
				// 	error: function(e)
				// 	{
				// 		alert(e);
				// 	}
				// });

				// AJAX
				$.ajax({
				type: 'ajax',
				method: 'post',
				url: 'http://sys.bebektimbungan.com/index.php/exportcontract/ExportContract',
				data: {notrans: id},
				async: false,
				dataType: 'json',
				success: function(data1){
					// $("#dokumen").removeClass('form-control');

					// var html2 = '';
					// var i;
					//$(".modal-body #bookId").val( data1.names );
					//
					// for(i=0; i<data1.length; i++){
					//
					// 		html2 +='<option value="'+data1[i].NoDokumen+'">'+data1[i].NoDokumen+'</option>';
					//
					// }
					// $('#dokumen').html(html2);

				},
				error: function(){
					// alert('Could not Show Dokumen');
					swal("Anda Memilih Lainnya", "Silakan Jelaskan Permasalahan Anda Secara Detail!");
				}
			});

			// AJAX


				return false;
			}

			function UpdateItemID(){


	            var PCode = $('#pcode').val();

			  $.ajax({
					url: "http://sys.bebektimbungan.com/index.php/transaksi/delivery_order/satuan/"+PCode+"/",
					data: {pcode:PCode},
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
						$('#v_satuan').html(res);
					},
					error: function(e)
					{
						alert(e);
					}
				});
			}

			function Cek(){
				var supplier 		= document.getElementById("supplier").value;
				var pcode 			= document.getElementById("pcode").value;
				var satuan 			= document.getElementById("v_satuan").value;
				var hargacontract 	= document.getElementById("hargacontract").value;
				var nocontract 	= document.getElementById("nokontrak").value;
				var notransaksi 	= document.getElementById("notransaksi").value;

				if(supplier==""){
					alert("Anda belum memilih Supplier");
					return false;
				}
				else if(pcode!="" && hargacontract!=""){
					search.submit();
				}
				else if(pcode=="" && hargacontract=="" && notransaksi!=""){
					alert("Transaksi berhasil di simpan");
					return false;
				}
				else{
						alert("Detail belum terisi semua");
						return false;
				}

			}

			function keyShortcut(e,obj,parTable) {

				if(window.event) // IE
				{
					var code = e.keyCode;
				}
				else if(e.which) // Netscape/Firefox/Opera
				{
					var code = e.which;
				}
				if (code == 13) { //checks for the escape key
					if(obj.name=="pcode"){
						var supplier = document.getElementById('supplier').value;
						if(supplier==""){
							alert("Anda belum memilih Supplier");
							return false;
						}
						else{
								SearchDetail(1,obj);
						}
					}
					else if(obj.name=="hargacontract"){
						var pcode = document.getElementById('pcode').value;
						var harga = obj.value;
						if(pcode==""){
							alert("Kode Barang belum dimasukkan");
							return false;
						}
						else if(harga==""){
							alert("Harga Contract tidak boleh kosong");
							return false;
						}
						else{
								search.submit();
						}
					}
				}
			}

			function SearchDetail(ids,obj){

				var kode = obj.id;
				if(ids==1){
					var pcode = document.getElementById(kode).value;
					$.post("ajax.php",{proses:'SearchBarang',param:pcode},
						function (output){
							arr=output.split("|");
							if(arr[0]=="ok"){
									$("#pcode").val(arr[1]);
									$("#namabarang").val(arr[2]);
									$("#hargadefault").val(ubah_harga(arr[3]));
									$('#hargacontract').focus();
							}
							else if(arr[0]=="no"){
								alert("Kode Barang tidak terdaftar");
								return false;
							}
						});
				}

			}

			function CekHapus(notransaksi,pcode){
				if (confirm("Anda yakin ingin menghapus baris ini?")){
					$.post("ajax.php",{proses:'HapusBarang',notransaksi:notransaksi,pcode:pcode},
					function (output){
						if(output=="ok"){
							window.location='contract_supplier_edit.php?flag=edit&notransaksi='+notransaksi;
						}
						else{
								alert("baris tersebut gagal di hapus");
								return false;
						}
					});
				}
			}

			function DeleteButton(){
				if (confirm("Anda yakin ingin menghapus transaksi ini?")){
					/*$.post("ajax.php",{proses:'Hapus',notransaksi:notransaksi},
					function (output){
						if(output=="ok"){
							window.location='contract_supplier_list.php';
						}
						else{
								alert("transaksi tersebut gagal di hapus");
								return false;
						}
					});	*/
					search.submit();
				}
			}

			function CloseButton(){
				if (confirm("Anda yakin ingin close transaksi ini?")){
					/*$.post("ajax.php",{proses:'Close',notransaksi:notransaksi},
					function (output){
						if(output=="ok"){
							window.location='contract_supplier_list.php';
						}
						else{
								alert("transaksi tersebut gagal di hapus");
								return false;
						}
					});	*/
					search.submit();
				}
			}

			function BackButton(){
				window.location='contract_supplier_list.php';
			}

		$(document).ready(function(){
			// alert("masuk");
			var popup = '';
			$('#import_form').on('submit', function(event){
			event.preventDefault();
			  $.ajax({
				url:"http://sys.bebektimbungan.com/index.php/exportcontract/import",
				method:"POST",
				data:new FormData(this),
				contentType:false,
				cache:false,
				processData:false,
				success:function(data){
				// window.location='contract_supplier_list.php';
					popup = data;
					// alert("SUKSES");

					$('#file').val('');
					// load_data();
				}
			  })
			// alert("success");
			var r = confirm("Import Sukses Ya..!");
			if (r == true) {
				setTimeout(function() {
    				window.history.back();
				}, 3000);
			  
			    
			} else {
			  window.history.back();
			    
			} 
			});
		});
	</script>

    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>

	<div class="main-content">

		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>

		<hr/>
		<?php if($_GET['flag']=="view"){?>

			<form method="post" action="http://sys.bebektimbungan.com/index.php/exportcontract/ExportContract">
				<input type="hidden" name="notrans" value="<? echo $notransaksi;?>">
				<input type="submit" class="btn btn-info" value="EXPORT TO EXCEL">
			</form>

			
		<?php } ?>

		<!-- <?php if($_GET['flag']=="new"){?>

			<form method="post" id="import_form" action="http://sys.bebektimbungan.com/index.php/exportcontract/ExportContract" enctype="multipart/form-data">
				<label>Select Excel File</label>
				<input type="file" name="file" id="file" required accept=".xls, .xlsx" />	
				<input type="submit" class="btn btn-success" value="IMPORT FROM EXCEL">
			</form>

			
		<?php } ?> -->

	<input type='hidden' name='flag' id='flag' value='<? echo $flag;?>'>
	<form method="POST"  name="search" id="import_form" action="" enctype="multipart/form-data">

		<input type="hidden" id="searchid" name="searchid" value = "2">
		<table align='center' class="table table-bordered responsive" border='1'>
			<?php if($_GET['flag']=="new"){?>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;">File Template</td>
			  <td><a href="assets/template/ImportContract.xls"><button class="btn btn-warning">Download</button> </a></td>
			</tr>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;"> File Excel </td>
			  <td><input type="file" name="file" id="file" required accept=".xls, .xlsx" /></td>
			</tr>
			<?php } ?>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;"> Tgl Transaksi </td>
			  <td><input type='text' class="form-control-new" id='tgltransaksi' name='tgltransaksi' size='20' value='<? echo ubah_tanggal($tgltransaksi);?>' <? echo $readonly;?>></td>
			</tr>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;"> No Transaksi </td>
			  <td><input placeholder="Auto Generate" type='text' class="form-control-new" id='notransaksi' name='notransaksi' size='20' value='<? echo $notransaksi;?>' <? echo $readonly;?>></td>
			</tr>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;"> Supplier </td>
			  <td>
				<select size="1" height="1" name ="supplier" id="supplier" class="form-control-new" <? echo $disabled2;?>>
				<option select value=''>PILIH SUPPLIER</option>
				<?
					$qryx = mysql_query("SELECT KdSupplier, Nama FROM supplier ORDER BY Nama ASC");
					//ECHO "BLAH";
					while($hsl=mysql_fetch_array($qryx)){
						if($supplier==$hsl[0]){
				?>
							<option selected value='<? echo $hsl[0];?>'><? echo $hsl[0]." - ".$hsl[1];?></option>
				<?
						}
						else{
				?>
								<option select value='<? echo $hsl[0];?>'><? echo $hsl[0]." - ".$hsl[1];?></option>
				<?
						}
					}
				?>
				</select>
			  </td>
			</tr>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;"> No Surat Kontrak </td>
			  <td><input type='text' class="form-control-new" id='nokontrak' name='nokontrak' size='20' value='<? echo $nokontrak;?>'></td>
			</tr>
			<tr bordercolor="#FFFFFF">
			  <td class="title_table" style="width: 100px;"> Periode </td>
			  <td>
			  <input type='text' <? echo $classtanggal;?> id='tgl1' name='tgl1' size='20' value='<? echo ubah_tanggal($tgl1);?>' <? echo $readonly;?>>
			  <b>s/d</b>
			  <input type='text' <? echo $classtanggal;?> id='tgl2' name='tgl2' size='20' value='<? echo ubah_tanggal($tgl2);?>' <? echo $readonly;?>>
			  </td>
			</tr>

		</table>
			<?php if($_GET['flag']=="new"){?>

				<input type="submit" class="btn btn-success" value="IMPORT FROM EXCEL">
			<?php } ?>


		<?php if($_GET['flag']=="view"){?>

			<!-- <input type="button" class="btn btn-info" name="btn_export" id="btn_export" onclick="ExportExcel('<? echo $notransaksi;?>')" value="Export To Excel"> -->
			<!-- <input type="submit" class="btn btn-info" name="btn_excel" id="btn_excel" value="Export To Excel"> -->
		<?php } ?>

<?php
//batas excel
}
?>

		<?php $btn_submit=true;
                if($btn_submit || $btn_excel)
        {

        	if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="12">PT. NATURA PESONA MANDIRI</td>
                            </tr>
                            <tr>
                                <td colspan="12">REPORT CONTRACT SUPPLIER</td>
                            </tr>

                            <tr>
                                <td colspan="12">&nbsp;</td>
                            </tr>
                        </table>
                        <table border="1">
                        <?php
                    }else{?>
						<table align='center' class="table table-bordered responsive" border='1' id='table_list'>
					<?php }
        ?>

		<br><br>

			<tr bordercolor="#FFFFFF">
			  <td  class="title_table" style="width: 100px;" align='center'> Kode Barang </td>
			  <td  class="title_table" style="width: 100px;" align='center'> Nama Barang </td>
			  <td  class="title_table" style="width: 100px;" align='center'> Satuan </td>
			  <td  class="title_table" style="width: 100px;" align='center'> Harga Barang </td>
			  <td  class="title_table" style="width: 100px;" align='center'> Harga Contract </td>
			  <?php
			  if(!$btn_excel)
                {
                	?>
			  <td  class="title_table" style="width: 10px;"> &nbsp; </td>
			  <?php
			  }
			  ?>
			</tr>

			<?php

			if(!$btn_excel)
                    {
                    	?>
			<tr bordercolor="#FFFFFF">
			  <td ><input type='text' class="form-control-new" id='pcode' name='pcode' size='25' value='' onblur="UpdateItemID()" onkeydown="keyShortcut(event,this,'table_list');" <? echo $readonly2;?>>
			  &nbsp;
			  <?
				$linkbarang = "pop_list_barang_contract_supplier.php?form1=pcode&form2=namabarang&form3=hargadefault";
			  ?>
			  <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_search" id="btn_search"  value="Cari" onclick="list_pcode('<? echo $linkbarang;?>');" <? echo $disabled2;?>>&nbsp;<i class="entypo-search"></i></button></td>
			  <td ><input type='text' class="form-control-new" id='namabarang' name='namabarang' size='50' value='' <? echo $readonly;?>></td>
			  <td >
			  	<select class="form-control-new" name="v_satuan" id="v_satuan">
				    <option value=""> -- Pilih -- </option>
				</select>
			  </td>
			  <td ><input type='text' class="form-control-new" id='hargadefault' name='hargadefault' size='20' value='' <? echo $readonly;?> dir='rtl'></td>
			  <td ><input type='text' class="form-control-new" id='hargacontract' name='hargacontract' size='20' value='' onkeydown="keyShortcut(event,this,'table_list');" onblur='toFormat(this);' dir='rtl'></td>
			  <td  align='center'>&nbsp;</td>
			</tr>
<?
          }
	if($flag=="new"){
		$q = "SELECT '' AS PCode, '' AS NamaLengkap, '' AS HargaDefault, '' AS HargaContract";
	}
	else{
			$q = "	SELECT
						d.PCode, m.NamaLengkap, Satuan, HargaDefault, HargaContract
					FROM
						contract_supplier_detail d, masterbarang m
					WHERE 1
					AND d.PCode=m.PCode
					AND d.NoTransaksi='".$notransaksi."'
					ORDER BY sid ASC";
	}
	//echo $q;
	$qry = mysql_query($q);
	$x = 1;
	while($hsl=mysql_fetch_array($qry)){
		$pcode 			= $hsl['PCode'];
		$namabarang 	= $hsl['NamaLengkap'];
		$satuan 	    = $hsl['Satuan'];
		$hargadefault 	= $hsl['HargaDefault'];
		$hargacontract 	= $hsl['HargaContract'];
?>
		<tr bordercolor="#FFFFFF">
		  <td ><input type='text' class="form-control-new" id='pcodex<? echo $x;?>' name='pcodex[]' size='37' value='<? echo $pcode;?>' <? echo $readonly;?>></td>
		  <td ><input type='text' class="form-control-new" id='namabarangx<? echo $x;?>' name='namabarangx[]' size='50' value="<? echo $namabarang;?>" <? echo $readonly;?>></td>
		  <td  align='left'><input type='text' class="form-control-new" id='satuan<? echo $x;?>' name='satuan[]' size='20' value="<? echo $satuan;?>" <? echo $readonly;?>></td>

		  <?php
			  if(!$btn_excel)
                {
                	?>
		  <td  align='left'><input type='text' class="form-control-new" id='hargadefaultx<? echo $x;?>' name='hargadefaultx[]' size='20' value="<? echo ubah_format('netto',$hargadefault);?>" <? echo $readonly;?> dir='rtl'></td>
		  <td  align='left'><input type='text' class="form-control-new" id='hargacontractx<? echo $x;?>' name='hargacontractx[]' size='20' value="<? echo ubah_format('netto',$hargacontract);?>" <? echo $readonly;?> dir='rtl'></td>
		 <?php }else{?>
		 	 <td  align='left'><input type='text' class="form-control-new" id='hargadefaultx<? echo $x;?>' name='hargadefaultx[]' size='20' value="<? echo $hargadefault;?>" ></td>
		     <td  align='left'><input type='text' class="form-control-new" id='hargacontractx<? echo $x;?>' name='hargacontractx[]' size='20' value="<? echo $hargacontract;?>" ></td>

		 <?php } ?>

		  <?php
			  if(!$btn_excel)
                {
                	?>
		  <td  align='center'><input type='button' value='Del' onclick="CekHapus('<? echo $notransaksi;?>','<? echo $pcode;?>');" <? echo $disabled;?>></td>
		  <?php } ?>
		</tr>
<?	$x++;
	}

	}

	if(!$btn_excel)
                {
?>
		</table>
		<br>
		<table align='center' class="table table-bordered responsive" border='1'>
			<tr bordercolor="#FFFFFF">
				<td>
				<?
				if($flag=="new"||$flag=="edit"){
				?>
					<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save"  value="Save" onclick='Cek();'>Save</button>
				<?
				}
				elseif($flag=="delete"){
				?>
					<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_delete" id="btn_delete"  value="Delete" onclick='DeleteButton();'>Delete</button>
				<?
				}
				elseif($flag=="close"){
				?>
					<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Close" onclick='CloseButton();'>Close</button>
				<?
				}
				?>
					<button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_back" id="btn_back"  value="Back" onclick='BackButton();'>Back</button>
				</td>
			</tr>
		</table>
	</form>

<?
   }

if($_REQUEST['search']!=''||$_POST['searchid']!=''){

	$date = date("Y-m-d H:i:s");
	$user = $ses_login;

	if($flag=="new"){

		$tgltransaksi 	= ubah_tanggal_save($_POST['tgltransaksi']);
		$supplier 		= $_POST['supplier'];
		$nokontrak 		= $_POST['nokontrak'];
		$tgl1 			= ubah_tanggal_save($_POST['tgl1']);
		$tgl2 			= ubah_tanggal_save($_POST['tgl2']);

		$pcode 			= $_POST['pcode'];
		$satuan 		= $_POST['v_satuan'];
	    $hargadefault	= ubah_awal($_POST['hargadefault']);
		$hargacontract	= ubah_awal($_POST['hargacontract']);

		$tgx = explode("-",$tgltransaksi);
		$notrans_awal = substr($tgx[0],2,2).$tgx[1];

		$q = "SELECT NoTransaksi
				FROM contract_supplier_header
				WHERE NoTransaksi LIKE '$notrans_awal%'
				ORDER BY NoTransaksi DESC
				LIMIT 1";
		$qry = mysql_query($q);
		$num = mysql_num_rows($qry);
		if($num>0){
			$hsl = mysql_fetch_array($qry);
			$notrans_new = $hsl[0] + 1;
		}
		else{
				$notrans_new = $notrans_awal."0001";
		}

		$qh_insert = "INSERT INTO contract_supplier_header(`NoTransaksi`,`TglTransaksi`,`KdSupplier`,NoKontrak,`PeriodeAwal`,`PeriodeAkhir`,`Status`,`AddDate`,`AddUser`)
						VALUES ('$notrans_new','$tgltransaksi','$supplier','$nokontrak','$tgl1','$tgl2','0','$date','$user')";
		$qd_insert = "INSERT INTO contract_supplier_detail(`sid`,`NoTransaksi`,`PCode`,Satuan,`HargaDefault`,`HargaContract`)
						VALUES (NULL,'$notrans_new','$pcode','$satuan','$hargadefault','$hargacontract')";
		//echo $qd_insert;die();
		$ekse1 = mysql_query($qh_insert) or die("$qh_insert ".mysql_error());
		$ekse2 = mysql_query($qd_insert) or die("$qd_insert ".mysql_error());

		if($ekse1 and $ekse2){
				echo "<meta http-equiv='refresh' content='0;URL=contract_supplier_edit.php?flag=edit&&notransaksi=$notrans_new'>";
		}

	}
	elseif($flag=="edit"){
			$notransaksi 	= $_POST['notransaksi'];
			$supplier 		= $_POST['supplier'];
			$tgl1 			= ubah_tanggal_save($_POST['tgl1']);
			$tgl2 			= ubah_tanggal_save($_POST['tgl2']);
			$pcode 			= $_POST['pcode'];
			$satuan 		= $_POST['v_satuan'];
			$hargadefault	= ubah_awal($_POST['hargadefault']);
			$hargacontract	= ubah_awal($_POST['hargacontract']);

			$q = "SELECT * FROM contract_supplier_detail WHERE NoTransaksi = '$notransaksi' AND PCode = '$pcode'";
			$qry = mysql_query($q);
			$num = mysql_num_rows($qry);
			if($num>0){
				echo "<script>
							alert('Kode Barang tersebut sudah ada');
							document.getElementById('pcode').focus();
						</script>";
			}
			else{
					$qh_update = "UPDATE contract_supplier_header SET KdSupplier = '$supplier', PeriodeAwal = '$tgl1', PeriodeAkhir='$tgl2', EditUser = '$user', EditDate = '$date' WHERE NoTransaksi = '$notransaksi'";
					$ekse1 = mysql_query($qh_update) or die("$qh_update ".mysql_error());
					$qd_insert = "INSERT INTO contract_supplier_detail(`sid`,`NoTransaksi`,`PCode`,Satuan,`HargaDefault`,`HargaContract`)
									VALUES (NULL,'$notransaksi','$pcode','$satuan','$hargadefault','$hargacontract')";
					//echo $qd_insert;die();
					$ekse2 = mysql_query($qd_insert) or die("$qd_insert ".mysql_error());

					echo "<meta http-equiv='refresh' content='0;URL=contract_supplier_edit.php?flag=edit&&notransaksi=$notransaksi'>";
			}

	}
	elseif($flag=="close"){
		$notransaksi 	= $_POST['notransaksi'];
		$q = "UPDATE contract_supplier_header SET Status = '1', EditUser = '$user' WHERE NoTransaksi = '$notransaksi'";
		$qry = mysql_query($q);
		if($qry){
			echo "<meta http-equiv='refresh' content='0;URL=contract_supplier_list.php'>";
		}
	}
	elseif($flag=="delete"){
		$notransaksi 	= $_POST['notransaksi'];
		$q = "UPDATE contract_supplier_header SET Status = '2', EditUser = '$user' WHERE NoTransaksi = '$notransaksi'";
		$qry = mysql_query($q);
		if($qry){
			echo "<meta http-equiv='refresh' content='0;URL=contract_supplier_list.php'>";
		}
	}

}


function CekBulan($data){
	if($data=="01"){
		$hasil = "Januari";
	}
	elseif($data=="02"){
		$hasil = "Februari";
	}
	elseif($data=="03"){
		$hasil = "Maret";
	}
	elseif($data=="04"){
		$hasil = "April";
	}
	elseif($data=="05"){
		$hasil = "Mei";
	}
	elseif($data=="06"){
		$hasil = "Juni";
	}
	elseif($data=="07"){
		$hasil = "Juli";
	}
	elseif($data=="08"){
		$hasil = "Agustus";
	}
	elseif($data=="09"){
		$hasil = "September";
	}
	elseif($data=="10"){
		$hasil = "Oktober";
	}
	elseif($data=="11"){
		$hasil = "November";
	}
	elseif($data=="12"){
		$hasil = "Desember";
	}
	return $hasil;
}

function ubah_tanggal($tanggalan){
 $tgl = explode("-",$tanggalan);
 $tgl2 = $tgl[2]."/". $tgl[1]."/". $tgl[0];
 return $tgl2;
}

function ubah_tanggal_save($tanggalan){
 $tgl = explode("/",$tanggalan);
 $tgl2 = $tgl[2]."-". $tgl[1]."-". $tgl[0];
 return $tgl2;
}

function ubah_awal($harga){
	$s = explode(".",$harga);
	$k = implode($s,"");
	$k = explode(",",$k);
	$s = implode($k,".");
	return $s;
}

function ubah_format($param,$harga){
	if($param=="qty"){
		$s = number_format($harga, 0, ',', '.');
	}
	elseif($param=="netto"){
		$s = number_format($harga, 2, ',', '.');
	}
	return $s;
}

if(!$btn_excel)
                {
include("footer.php");
}

?>
