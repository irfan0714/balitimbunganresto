<?php
include("header.php");

$modul            = "Update Mutasi";
$file_name        = "updatemutasi.php";

$btn_save = $_POST['btn_save'];

	if($_REQUEST['search']!=''||$_POST['searchid']!=''){		 	
		$tahunbaru = $_POST['tahun'];
		$bulanbaru = $_POST['bulan'];
	}
	else{
			$tahunbaru = date("Y");
			$bulanbaru = date("m");
	}	
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed">

	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Inventory</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<form method="POST"  name="search" action="" >
			<div class="row">
				<div class="col-md-12">
			
				<input type="hidden" id="searchid" name="searchid" value = "2">
				<table align='center' class="table table-bordered responsive">
			
			<tr bordercolor="#FFFFFF"> 
			  <td class="title_table" style="width: 100px;"> Periode </td>
			  <td> 				
					<?
					echo "<input type='hidden' id='bulan_hide' name='bulan_hide' size='5' value='$bulanbaru'>";
					?>  
				<select size="1" height="1" name ="bulan" id="bulan" class="form-control-new">
				<?
					$bulx = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
					for($bw=1 ; $bw<=12; $bw++){
						if(strlen($bw)==1){
							$bx = "0".$bw;
						}
						else{
							$bx = $bw;
						}
						if((int)$bulanbaru==(int)$bx){
							echo "<option selected value='$bx'>$bulx[$bw]</option>";
						}
						else{
								echo "<option value='$bx'>$bulx[$bw]</option>";		
						}

					}
				?>
				</select>				
				
					<?
					echo "<input type='hidden' id='tahun_hide' name='tahun_hide' size='5' value='$tahunbaru'>";
					?>  
					<select size="1" height="1" name ="tahun" id="tahun" class="form-control-new">
					<?
					$tahunawal = $tahunbaru-1;
					$tahunakhir = $tahunawal+2;
					for($a=$tahunawal;$a<=$tahunakhir;$a++){
						echo "<option value='$a'>$a</option>";
					}
					?>
					</select>				
			  </td>
			</tr>

					<tr bordercolor="#FFFFFF">
						<td>&nbsp;</td>
						<td>
							<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save"  value="Submit">Submit<i class="entypo-search"></i></button>
                        </td>
					</tr>
				</table>
			
				</div>
			</div>
		</form>
		
	

<?

if($_REQUEST['search']!=''||$_POST['searchid']!=''){

	$bulan_now = $bulanbaru;
	$tahun_now = $tahunbaru;
	
	if((int)$bulan_now==1){
		$bulan_prev = 12;
		$tahun_prev = $tahun_now - 1;
	}
	else{
			$bulan_prev = (int)$bulan_now - 1;
			$tahun_prev = $tahun_now;
	}
	
	if(strlen($bulan_prev)==1){
		$bulan_prev = "0".$bulan_prev;
	}
	
	$gawalprev 		= "GAwal".$bulan_prev;
	$gmasukprev 	= "GMasuk".$bulan_prev;
	$gkeluarprev 	= "GKeluar".$bulan_prev;
	$gakhirprev 	= "GAkhir".$bulan_prev;

	$gawal 		= "GAwal".$bulan_now;
	$gmasuk 	= "GMasuk".$bulan_now;
	$gkeluar 	= "GKeluar".$bulan_now;
	$gakhir 	= "GAkhir".$bulan_now;


	$q = "SELECT KdGudang,PCode
			FROM stock
			WHERE Tahun='$tahun_now'";
	//echo "$q<hr>";		
	$qry = mysql_query($q);
	while($hsl=mysql_fetch_array($qry)){
		$gudang = $hsl['KdGudang'];
		$pcode = $hsl['PCode'];
		
			$q1 = "SELECT $gakhirprev FROM stock WHERE Tahun='$tahun_prev' AND KdGudang='$gudang' AND PCode='$pcode'";
			$qry1 = mysql_query($q1);
			$hsl1 = mysql_fetch_array($qry1);
			$qtyprev = $hsl1[0];
			mysql_free_result($qry1);
			//UPDATE stock_simpan_tes SET GAwal05 = WHERE Tahun='05' AND KdGudang='00' AND PCode='00'
			$ekse0 = "UPDATE stock SET $gawal = $qtyprev WHERE Tahun='$tahun_now' AND KdGudang='$gudang' AND PCode='$pcode'";
			//echo "<br>$ekse0<br>";
			mysql_query($ekse0)or die("$ekse0 ".mysql_error());		
		
	}	

	

	$q = "
	SELECT Jenis,Gudang AS GudangAsal, GudangTujuan, KodeBarang, SUM(Qty) AS Qty
	FROM mutasi 
	WHERE MONTH(Tanggal)='$bulan_now' AND YEAR(Tanggal)='$tahun_now'
	GROUP BY Jenis,Gudang,GudangTujuan,KodeBarang
	";
	
	$qry = mysql_query($q);
	
	while($hsl = mysql_fetch_array($qry)){
		$jenis 			= $hsl['Jenis'];
		$gudangasal 	= $hsl['GudangAsal'];
		$gudangtujuan 	= $hsl['GudangTujuan'];
		$pcode 			= $hsl['KodeBarang'];
		$qty 			= $hsl['Qty'];		
		
		if($jenis=="I"){
			$cek = mysql_query("SELECT * FROM stock WHERE Tahun='$tahun_now' AND KdGudang='$gudangtujuan' AND PCode='$pcode'");
			$num = mysql_num_rows($cek);
			if($num==0){
				$ekse1 = "INSERT INTO stock(`Tahun`,`KdGudang`,`PCode`,`Status`,`$gmasuk`) 
							VALUES('$tahun_now','$gudangtujuan','$pcode','G','$qty')";
			}
			else{
					$ekse1 = "UPDATE stock SET $gmasuk = $qty WHERE Tahun='$tahun_now' AND KdGudang='$gudangtujuan' AND PCode='$pcode'";
			}
			//echo "<br>$ekse1";
			mysql_query($ekse1)or die("$ekse1 ".mysql_error());
			
		}
		elseif($jenis=="O"){
			$cek = mysql_query("SELECT * FROM stock WHERE Tahun='$tahun_now' AND KdGudang='$gudangasal' AND PCode='$pcode'");
			$num = mysql_num_rows($cek);
			if($num==0){
				$ekse1 = "INSERT INTO stock (`Tahun`,`KdGudang`,`PCode`,`Status`,`$gkeluar`) 
							VALUES('$tahun_now','$gudangasal','$pcode','G','$qty')";
			}
			else{
					$ekse1 = "UPDATE stock SET $gkeluar = $qty WHERE Tahun='$tahun_now' AND KdGudang='$gudangasal' AND PCode='$pcode'";
			}
			//echo "<br>$ekse1";
			mysql_query($ekse1)or die("$ekse1 ".mysql_error());
			
		}
		
	}
	
		$ekse2 = "UPDATE stock SET $gakhir = ($gawal + $gmasuk - $gkeluar) WHERE Tahun='$tahun_now'";
		mysql_query($ekse2)or die("$ekse2 ".mysql_error());
		echo "<br>";
		echo "<br>";
		echo "<font color='red'><b>Berhasil Update Mutasi</b></font>";
}

function CekBulan($data){
	if($data=="01"){
		$hasil = "Januari";
	}
	elseif($data=="02"){
		$hasil = "Februari";
	}
	elseif($data=="03"){
		$hasil = "Maret";
	}
	elseif($data=="04"){
		$hasil = "April";
	}
	elseif($data=="05"){
		$hasil = "Mei";
	}
	elseif($data=="06"){
		$hasil = "Juni";
	}
	elseif($data=="07"){
		$hasil = "Juli";
	}
	elseif($data=="08"){
		$hasil = "Agustus";
	}
	elseif($data=="09"){
		$hasil = "September";
	}
	elseif($data=="10"){
		$hasil = "Oktober";
	}
	elseif($data=="11"){
		$hasil = "November";
	}
	elseif($data=="12"){
		$hasil = "Desember";
	}	
	return $hasil;
}

include("footer.php");
?>