<?php
include("header.php");

$modul   = "REPORT OUTSTANDING PR - Order";

if(!isset($_GET["v_NoPR"])){ $v_NoPR = isset($_GET["v_NoPR"]); } else { $v_NoPR = $_GET["v_NoPR"]; }
if(!isset($_GET["v_PCode"])){ $v_PCode = isset($_GET["v_PCode"]); } else { $v_PCode = $_GET["v_PCode"]; }

unset($arr_data);
             
$q = "
        SELECT
            trans_order_barang_header.NoDokumen,    
            trans_order_barang_header.TglDokumen,    
            supplier.KdSupplier,    
            supplier.Nama AS NamaSuplier,    
            trans_order_barang_header.Keterangan,    
            gudang.KdGudang,    
            gudang.Keterangan AS NamaGudang,    
            trans_order_barang_detail.PCode,
            trans_order_barang_detail.Qty,
            trans_order_barang_detail.Satuan
        FROM
            trans_order_barang_header
            INNER JOIN trans_order_barang_detail ON
                trans_order_barang_header.NoDokumen = trans_order_barang_detail.NoDokumen
                AND trans_order_barang_header.Status != '2'
                AND trans_order_barang_header.NoPr = '".$v_NoPR."'
                AND trans_order_barang_detail.PCode = '".$v_PCode."'
            INNER JOIN gudang ON
                trans_order_barang_header.KdGudang = gudang.KdGudang
            INNER JOIN supplier ON
                trans_order_barang_header.KdSupplier = supplier.KdSupplier
        WHERE
            1
        ORDER BY
            trans_order_barang_header.NoDokumen ASC,    
            trans_order_barang_header.TglDokumen ASC   
"; 
$counter = 0;
$qry = mysql_query($q);
while($row = mysql_fetch_array($qry))
{
    list(
        $NoDokumen,    
        $TglDokumen,    
        $KdSupplier,    
        $NamaSupplier,    
        $Keterangan,    
        $KdGudang,    
        $NamaGudang,    
        $PCode,
        $Qty,
        $Satuan  
    ) = $row;
    
    $arr_data["list_data"][$counter] = $counter;
    $arr_data["data_NoDokumen"][$counter] = $NoDokumen;
    $arr_data["data_TglDokumen"][$counter] = $TglDokumen;
    $arr_data["data_KdSupplier"][$counter] = $KdSupplier;
    $arr_data["data_NamaSupplier"][$counter] = $NamaSupplier;
    $arr_data["data_Keterangan"][$counter] = $Keterangan;
    $arr_data["data_KdGudang"][$counter] = $KdGudang;
    $arr_data["data_NamaGudang"][$counter] = $NamaGudang;
    $arr_data["data_PCode"][$counter] = $PCode;
    $arr_data["data_Qty"][$counter] = $Qty;
    $arr_data["data_Satuan"][$counter] = $Satuan;
    
    $counter++;
} 

	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
        
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">
<form method="get">
<input type="hidden" name="v_KdSupplier" value="<?php echo $v_KdSupplier; ?>">
<div class="page-container sidebar-collapsed">
	
	
	<div class="main-content">
    
		
		<div class="row">
              <table class="table table-bordered responsive">
              <thead>
                <tr>
                    <th width="30">No</th>
                    <th>No Dokumen</th>
                    <th>Tanggal</th>
                    <th>Supplier</th>
                    <th>Gudang</th>
                    <th>Keterangan</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                </tr>
                </thead>
                
                <tbody style="color: black;">
                
                <?php 
                    $no = 1;
                    foreach($arr_data["list_data"] as $counter=>$val)
                    {
                        $NoDokumen = $arr_data["data_NoDokumen"][$counter];
                        $TglDokumen = $arr_data["data_TglDokumen"][$counter];
                        $KdSupplier = $arr_data["data_KdSupplier"][$counter];
                        $NamaSupplier = $arr_data["data_NamaSupplier"][$counter];
                        $Keterangan = $arr_data["data_Keterangan"][$counter];
                        $KdGudang = $arr_data["data_KdGudang"][$counter];
                        $NamaGudang = $arr_data["data_NamaGudang"][$counter];
                        $PCode = $arr_data["data_PCode"][$counter];
                        $Qty = $arr_data["data_Qty"][$counter];
                        $Satuan = $arr_data["data_Satuan"][$counter];
                        
                        ?>
                        <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                            <td><?php echo $no; ?></td>
                            <td><?php echo $NoDokumen; ?></td>
                            <td><?php echo format_show_date($TglDokumen); ?></td>
                            <td><?php echo $NamaSupplier; ?></td>
                            <td><?php echo $NamaGudang; ?></td>
                            <td><?php echo $Keterangan; ?></td>
                            <td style="text-align: right"><?php echo format_number($Qty, 2, ",", ".", "ind"); ?></td>
                            <td><?php echo $Satuan; ?></td>
                        </tr>
                        <?php
                        
                        $no++;
                    }
                ?>
                </tbody>
                
                
              </table>
       	
       	</div>
</form>		
		
    
<?php include("footer.php"); ?>