<?php
include("header.php");
                               
$modul            = "PRODUCTION PLANNING";
$file_current     = "npm_prodplan.php";
$file_name        = "npm_prodplan_data.php";

//$ses_login = "admin";

function get_prodplancode($v_start_date)
{
    global $db;
    //PP00001/04/16
    $arr_date = explode("/", $v_start_date);
    
    $period = $arr_date[1]."/".substr($arr_date[2],2,2);
    
    $q = "
            SELECT
                 SUBSTRING(prodplan.prodplancode,3,5) AS last_counter
            FROM
                prodplan
            WHERE
                1
                AND RIGHT(prodplan.prodplancode,5) = '".$period."'
            ORDER BY
                prodplan.prodplancode DESC
            LIMIT
                0,1                                      
    ";
    $qry = mysql_query($q);
    $row = mysql_fetch_array($qry);
    list($last_counter) = $row;
    
    $last_counter = ($last_counter*1)+1;
    
    $return = "PP".sprintf("%05s", $last_counter)."/".$period;
    
    return $return;
}
                

function db_connect_lokal()
{
    global $db;
    
    $db_lokal["host"] = "localhost"; 
    $db_lokal["user"] = "root"; 
    $db_lokal["pass"] = "";
    
    mysql_close($db);
    $conn    = mysql_connect($db_lokal["host"], $db_lokal["user"], $db_lokal["pass"]);
} 

function db_connect_type($server,$user,$pass)
{
    $db_type["host"] = $server; 
    $db_type["user"] = $user; 
    $db_type["pass"] = $pass;                                                                
    
    mysql_close();
    $db_type = mysql_connect($db_type["host"], $db_type["user"], $db_type["pass"]);
}
          
$ajax = $_REQUEST["ajax"];       
        
if($ajax)
{
    if($ajax=='search')
    {                                         
        $search_keyword        = trim($_GET["search_keyword"]);
        $v_date_from           = trim($_GET["v_date_from"]);
        $v_date_to             = trim($_GET["v_date_to"]);   
        $v_prodplancode_curr   = trim($_GET["v_prodplancode_curr"]);
        
        $where = "";        
        if($search_keyword)
        {
            unset($arr_keyword);
            $arr_keyword[0] = "prodplan.prodplancode";    
            
            $where_search_keyword = search_keyword($search_keyword, $arr_keyword);
            $where .= $where_search_keyword;
        } 
        
        if($v_prodplancode_curr)
        {
            $where = " AND prodplan.prodplancode = '".$v_prodplancode_curr."'";
        }
        
       
        // select query all
        {
            $counter = 1;
            $q="
                SELECT 
                  prodplan.prodplancode,
                  prodplan.startingperiod,
                  prodplan.endingperiod,
                  prodplan.adduser
                FROM
                  prodplan 
                WHERE 1 
                    ".$where."
                    AND 
                        (
                            prodplan.startingperiod BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                            OR
                            prodplan.endingperiod BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                        )
                ORDER BY 
                    prodplan.prodplancode ASC
            ";
            //echo $q;
            $sql=mysql_query($q);
            while($row=mysql_fetch_array($sql))
            { 
                list(
                    $prodplancode,
                    $startingperiod,
                    $endingperiod,
                    $adduser
                )=$row;
                
                $arr_data["list_data"][$counter]=$counter;
                $arr_data["data_prodplancode"][$counter]=$prodplancode;
                $arr_data["data_startingperiod"][$counter]=$startingperiod;
                $arr_data["data_endingperiod"][$counter]=$endingperiod;
                
                $counter++;
            }
        }  
        
        
        
?>      
    <div class="col-md-12">
    <form method="post" name="theform_list" id="theform_list" target="_blank" action="npm_purchase_order_print.php">
    <input type="hidden" name="ajax_post" id="ajax_post" value="submit">
    <table class="table table-bordered responsive">
        <thead>
            <tr>
                <th width="30"><strong><center>No</center></strong></th>
                <th><strong><center>PP Code</center></strong></th>
                <th><strong><center>Start</center></strong></th>
                <th><strong><center>End</center></strong></th>
                <th width="15" style="display: none;"><strong><center><input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'v_data[]')"></center></strong></th>    
            </tr>
            
        </thead>
        <tbody style="color: black;">
        
        <?php
        $nomor=0;    
         
        $jml = count($arr_data["list_data"]);
        
        $js="";
        for($i=1;$i<=$jml;$i++) {    
            $js.="document.getElementById('row_$i').style.background=''; ";
        }
        
        if(count($arr_data["list_data"])==0)
        {
            ?>
                <tr>
                    <td colspan="100%" align="center">Tidak ada data</td>
                </tr>
            <?php
        }  
               
        $nomor = 0;
        foreach($arr_data["list_data"] as $counter => $val)
        {    
            $nomor++;
            
            $prodplancode = $arr_data["data_prodplancode"][$counter];
            $startingperiod = $arr_data["data_startingperiod"][$counter];
            $endingperiod = $arr_data["data_endingperiod"][$counter];
            
            
            
            $style_color = "";
            if($v_prodplancode_curr==$prodplancode)
            {
                $style_color = "background: #CAFDB5";
            }
                    
            ?>
            <tr id="row_<?php echo $nomor; ?>" onclick="<?php echo $js; ?>document.getElementById('row_<?php echo $nomor; ?>').style.background='#CAFDB5';" onmouseover="mouseover(this)" onmouseout="mouseout(this)" style="cursor:pointer; <?php echo $style_color.$bgcolor; ?>">
                <td onclick="CallAjaxForm('edit_data','<?php echo $prodplancode; ?>')" align="" style=""><?php echo $nomor; ?></td>
                <td onclick="CallAjaxForm('edit_data','<?php echo $prodplancode; ?>')" align="" style=""><?php echo $prodplancode; ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $prodplancode; ?>')" align="" style=""><?php echo format_show_date($startingperiod); ?></td> 
                <td onclick="CallAjaxForm('edit_data','<?php echo $prodplancode; ?>')" align="" style=""><?php echo format_show_date($endingperiod); ?></td> 
                <td align="center" style="display: none;">
                    &nbsp;
                    
                    <input type="checkbox" name="v_data[]" id="v_data_<?php echo $nomor; ?>" value="<?php echo $prodplancode; ?>">
                    
                    &nbsp;
                </td>
            </tr>
            <?php     
        }    
        ?> 
        </tbody>
        
        <tfoot style="display: none;">
            <tr>
                <td colspan="100%" align="right">
                    <select name="action" id="action" class="form-control-new">
                        <option value="Print">Print</option>      
                    </select>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left">Submit<i class="entypo-submit"></i></button>
                </td>
            </tr>
        </tfoot>
    </table> 
    </form>
    </div>  
       <?php  
    }

    else if($ajax=="add_data")
    {
        
      
        ?>
        <div class="col-md-12" align="left">
        
            <ol class="breadcrumb">
                <li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
            
            <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add_data">
            
            <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="200">Period</td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_start_date" id="v_start_date" size="10" maxlength="10" value="">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_start_date, 'dd/mm/yyyy');">
                    &nbsp;s/d&nbsp;
                    <input type="text" class="form-control-new datepicker" name="v_end_date" id="v_end_date" size="10" maxlength="10" value=""> 
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_end_date, 'dd/mm/yyyy');">
                </td>
                
            </tr>
            
            <tr> 
                <td>&nbsp;</td>                   
                <td>
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" name="btn_save" id="btn_save" value="Save">Save<i class="entypo-check"></i></button>
               </td>
            </tr> 
            
            </table>  
            </form>
        </div>
        <?php
        
    }
    
    else if($ajax=="edit_data")
    {                                      
        $v_prodplancode = $_GET["v_prodplancode"];  
        
        $q = "
                SELECT 
                  prodplan.prodplancode,
                  prodplan.startingperiod,
                  prodplan.endingperiod,
                  prodplan.adddate,
                  prodplan.adduser,
                  prodplan.editdate,
                  prodplan.edituser
                FROM
                  prodplan
                WHERE 1
                    AND prodplan.prodplancode = '".$v_prodplancode."'
                LIMIT
                    0,1
        ";     
        $qry =  mysql_query($q);
        $arr_curr = mysql_fetch_array($qry);
        
        $q = "
                SELECT
                    prodplandetail.prodplandetailid,
                    masterbarang.PCode AS inventorycode,
                    masterbarang.NamaLengkap AS inventoryname,
                    prodplandetail.quantity,
                    prodplandetail.formula
                FROM
                    prodplandetail
                    INNER JOIN masterbarang ON
                        prodplandetail.inventorycode = masterbarang.PCode
                        AND prodplandetail.prodplancode = '".$v_prodplancode."'
                WHERE
                    1
                ORDER BY
                    prodplandetail.prodplandetailid ASC
        ";
		//echo $q;
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list(
                $prodplandetailid,
                $inventorycode,
                $inventoryname,
                $quantity,
                $formula
            ) = $row;
            
            $arr_data["list_detail"][$prodplandetailid] = $prodplandetailid;
            $arr_data["inventorycode"][$prodplandetailid] = $inventorycode;
            $arr_data["inventoryname"][$prodplandetailid] = $inventoryname;
            $arr_data["quantity"][$prodplandetailid] = $quantity;
            $arr_data["formula"][$prodplandetailid] = $formula;
            
            $arr_data["list_inventorycode"][$inventorycode] = $inventorycode;
        }
        
        //$where_pcode = where_array($arr_data["list_inventorycode"], "uni.inventorycode", "in");
        $where_pcode = where_array($arr_data["list_inventorycode"], "inventorycode", "in");
        //echo $where_pcode."<br><br>";
        /*$q = "
                SELECT
                    uni_oke2.inventorycode,
                    uni_oke2.formulanumber,
                    uni_oke2.formulaname,
                    uni_oke2.packagingsize,
                    uni_oke2.quantity
                FROM
                (
                SELECT
                    uni_oke.sort_by,
                    uni_oke.inventorycode,
                    uni_oke.formulanumber,
                    uni_oke.formulaname,
                    uni_oke.packagingsize,
                    uni_oke.quantity
                FROM
                (
                SELECT
                    '1' AS sort_by,
                    uni.inventorycode,
                    uni.formulanumber,
                    uni.formulaname,
                    uni.packagingsize,
                    uni.quantity
                FROM
                (
                    SELECT
                        formula.inventorycode,
                        formula.formulanumber,
                        formula.formulaname,
                        formula.packagingsize,
                        formula.quantity,
                        formula.editdate
                    FROM
                        formula
                    WHERE
                        1
                        AND formula.isdefault = '1'
                        AND formula.isactive = '1'
                        AND (formula.approval_status_1 = '1' OR formula.approval_status_2 = '1')
                    ORDER BY
                        formula.editdate DESC
                ) AS uni
                WHERE
                    1
                    ".$where_pcode."
                GROUP BY
                    uni.inventorycode
                
                UNION ALL
                
                SELECT
                    '2' AS sort_by,
                    uni.inventorycode,
                    uni.formulanumber,
                    uni.formulaname,
                    uni.packagingsize,
                    uni.quantity
                FROM
                (
                    SELECT
                        formula.inventorycode,
                        formula.formulanumber,
                        formula.formulaname,
                        formula.packagingsize,
                        formula.quantity,
                        formula.editdate
                    FROM
                        formula
                    WHERE
                        1
                        AND formula.isdefault = '1'
                        AND formula.isactive = '0'
                        AND (formula.approval_status_1 = '1' OR formula.approval_status_2 = '1')
                    ORDER BY
                        formula.editdate DESC
                ) AS uni
                WHERE
                    1
                    ".$where_pcode."
                GROUP BY
                    uni.inventorycode
                    
                
                UNION ALL
                
                SELECT
                    '3' AS sort_by,
                    uni.inventorycode,
                    uni.formulanumber,
                    uni.formulaname,
                    uni.packagingsize,
                    uni.quantity
                FROM
                (
                    SELECT
                        formula.inventorycode,
                        formula.formulanumber,
                        formula.formulaname,
                        formula.packagingsize,
                        formula.quantity,
                        formula.editdate
                    FROM
                        formula
                    WHERE
                        1
                        AND formula.isdefault = '0'
                        AND formula.isactive = '0'
                        AND (formula.approval_status_1 = '1' OR formula.approval_status_2 = '1')
                    ORDER BY
                        -- formula.editdate DESC
                        RIGHT(formula.`formulanumber`, 2) DESC,
                        LEFT(RIGHT(formula.`formulanumber`, 5),2) DESC,
                        RIGHT(LEFT(formula.`formulanumber`,7),5)*1 DESC
                        
                ) AS uni
                WHERE
                    1
                    ".$where_pcode."
                GROUP BY
                    uni.inventorycode
                )AS uni_oke
                WHERE
                    1
                ORDER BY
                    uni_oke.sort_by DESC 
                )as uni_oke2
                WHERE
                    1
                GROUP BY
                    uni_oke2.inventorycode   
        ";*/
        $q="
        SELECT 
		  a.`inventorycode`,
		  a.`formulanumber`,
		  a.`formulaname`,
		  a.`packagingsize`,
		  a.`quantity` 
		FROM
		  `formula` a 
		WHERE 1 ".$where_pcode." ;
        ";     
             
        $qry = mysql_query($q);
		
        while($row = mysql_fetch_array($qry))
        {
            list($inventorycode, $formulanumber, $formulaname, $packagingsize, $quantity_kg) = $row;
            
            $arr_data["list_formula_active"][$inventorycode][$formulanumber] = $formulanumber;     
            
            $arr_data["formula_active_number"][$formulanumber] = $formulanumbers;
            $arr_data["formula_active_name"][$formulanumber] = $formulaname;     
            $arr_data["formula_active_packagingsize"][$formulanumber] = $packagingsize;     
            $arr_data["formula_active_quantity"][$formulanumber] = $quantity_kg;     
        } 
        
        /*echo "<pre>";
        print_r($arr_data["list_formula_active"]);
        echo "</pre>";
        
        echo "<pre>";
        print_r($arr_data["formula_active_name"]);
        echo "</pre>";*/
        
        ?> 
        <div class="col-md-12" align="left">
            
            <ol class="breadcrumb">
                <li><strong><i class="entypo-pencil"></i><?php echo $modul; ?> Form</strong></li>
                <span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
            </ol>
                                                       
        <form name='theform' id='theform' method="POST" target="forsubmit" action="<?php echo $file_name; ?>?ajax_post=submit" enctype="multipart/form-data">
        <input type="hidden" name="action" value="edit_data">  
        <input type="hidden" name="v_del" id="v_del" value="">    
        <input type="hidden" name="v_undel" id="v_undel" value="">    
        <input type="hidden" name="v_approve" id="v_approve" value="">    
        <input type="hidden" name="v_reject" id="v_reject" value="">    
        <input type="hidden" name="v_prodplancode" id="v_prodplancode" value="<?php echo $arr_curr["prodplancode"]; ?>">   
        
        <table class="table table-bordered responsive">
            <tr>
                <td class="title_table" width="200">PP Code</td>
                <td style="color: black; font-weight: bold"><?php echo $arr_curr["prodplancode"]; ?></td>
            </tr>
        
            <tr>
                <td class="title_table" width="200">Period</td>
                <td>
                    <input type="text" class="form-control-new datepicker" name="v_start_date" id="v_start_date" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["startingperiod"]); ?>">
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_start_date, 'dd/mm/yyyy');">
                    &nbsp;s/d&nbsp;
                    <input type="text" class="form-control-new datepicker" name="v_end_date" id="v_end_date" size="10" maxlength="10" value="<?php echo format_show_date($arr_curr["endingperiod"]); ?>"> 
                    <img src="images/cal.gif" onClick="popUpCalendar(this, theform.v_end_date, 'dd/mm/yyyy');">
                </td>
            </tr>     
                        
            <tr> 
                <td>&nbsp;</td>                   
                <td>                    
                    <button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button>
                    
                    <span style="float: right;">
                    <button type="submit" name="btn_delete" id="btn_delete" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_delete('<?php echo $arr_curr["prodplancode"]; ?>');" value="Hapus">Hapus <?php echo $arr_curr["prodplancode"]; ?><i class="entypo-trash"></i></button>
                    </span>
               </td>
            </tr>
            
            <tr>
                <td colspan="100%">
                    <button type="button" onclick="pop_add_pcode('<?php echo $arr_curr["prodplancode"]; ?>')" name="btn_add" id="btn_add" class="btn btn-info btn-icon btn-sm icon-left" value="Tambah PCode">Tambah PCode<i class="entypo-plus"></i></button>
                </td>
            </tr>
            
            <tr> 
                <td colspan="100%">
                    <table class="table table-bordered responsive">
                        <thead>
                        <tr>
                            <td width="30">No</td>
                            <td>PCode</td>
                            <td>Nama Barang</td>
                            <td>Batch</td>
                            <td>Formula</td>
                            <td>Qty KG</td>
                            <td>Pack Size</td>
                            <td align="center">
                                Hapus<br>
                                <input type="checkbox" id="chkall_del" onclick="CheckAll('chkall_del', 'v_data_del[]')">
                            </td>
                        </tr>  
                        </thead> 
                        
                        <tbody style="color: black;">
                            <?php 
                            	$i=0;
                                $no = 1;
                                foreach($arr_data["list_detail"] as $prodplandetailid=>$val)
                                {
                                    $pcode = $arr_data["inventorycode"][$prodplandetailid];
                                    $inventoryname = $arr_data["inventoryname"][$prodplandetailid];
                                    $quantity = $arr_data["quantity"][$prodplandetailid];
                                    $formula = $arr_data["formula"][$prodplandetailid];
                                    
                                    $packagingsize_oke = $arr_data["formula_active_packagingsize"][$formula];     
                                    $quantity_kg_oke = $arr_data["formula_active_quantity"][$formula];     
                            ?>
                            <tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                <td>
                                    <?php echo $no; ?>
                                    
                                    <input type="hidden" name="v_pcode[]" value="<?php echo $pcode; ?>">
                                </td>
                                <input type="hidden" name="v_prodplandetailid_<?php echo $i; ?>" value="<?php echo $prodplandetailid; ?>">    
                                <td><?php echo $pcode; ?></td>
                                <td><?php echo $inventoryname; ?></td>
                                <td align="right"><input type="text" class="form-control-new" name="v_batch_<?php echo $i; ?>" value="<?php echo format_number($quantity, 4); ?>" id="v_batch_<?php echo $pcode; ?>" size="12" style="text-align: right;" onblur="toFormat4('v_batch_<?php echo $pcode; ?>')"></td>
                                
                                <td>
                                    <select class="form-control-new" name="v_formula_<?php echo $i; ?>" id="v_formula_<?php echo $pcode; ?>" style="width: 500px;">
                                         <?php 
                                            
                                            foreach($arr_data["list_formula_active"][$pcode] as $formulanumber=>$val)
                                            {
                                               
                                                $formulaname = $arr_data["formula_active_name"][$formulanumber];     
                                                $packagingsize = $arr_data["formula_active_packagingsize"][$formulanumber];     
                                                $quantity_kg = $arr_data["formula_active_quantity"][$formulanumber];
            
                                                $selected="";
                                                if ($formulanumber == $formula){
													$selected='selected="selected"';
												}    
                                                
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $formulanumber; ?>"><?php echo $formulanumber." - ".$formula." :: ".$formulaname; ?></option>
                                        <?php 
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td align="right"><?php echo format_number($quantity_kg_oke*$quantity,2); ?></td>
                                <td align="right"><?php echo format_number($packagingsize_oke*$quantity,0); ?></td>
                                <td align="center"><input type="checkbox" name="v_data_del[]" id="v_data_del_<?php echo $pcode; ?>" value="<?php echo $pcode; ?>"></td>
                            </tr>
                            <?php
                                    $i++;
                                    $no++; 
                                }
                            ?>
                        </tbody>      
                    </table>
                </td>                   
                
            </tr>
            
            
            <tr>
                <td class="title_table" colspan="2">INFORMASI DATA</td>      
            </tr> 
            
            <tr>
                <td class="title_table">Dibuat</td>      
                <td><?php echo format_show_datetime($arr_curr["adddate"])." :: ".$arr_curr["adduser"]; ?></td>
            </tr>
            
            <tr>
                <td class="title_table">Diedit</td>      
                <td><?php echo format_show_datetime($arr_curr["editdate"])." :: ".$arr_curr["edituser"]; ?></td>
            </tr>
             
            
            
        </table>  
        </form>
        </div>
        <?php
    } 
    
    exit();
}         

$ajax_post = $_REQUEST["ajax_post"];

if($ajax_post=="submit"){
    $msg = "";
    $action = $_REQUEST["action"];
    switch($action){
        case "add_data" :                                           
            {                                     
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                
                if(!isset($_POST["v_start_date"])){ $v_start_date = isset($_POST["v_start_date"]); } else { $v_start_date = $_POST["v_start_date"]; }                                                                                                                                        
                if(!isset($_POST["v_end_date"])){ $v_end_date = isset($_POST["v_end_date"]); } else { $v_end_date = $_POST["v_end_date"]; }     

                if($v_start_date=="")
                {
                    $msg = "Period harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_end_date=="")
                {
                    $msg = "Period harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                
                $prodplancode = get_prodplancode($v_start_date);
                //PP00001/04/10
               
                // insert prodplan
                {
                    $q = "
                        INSERT INTO `prodplan`
                        SET `prodplancode` = '".$prodplancode."',
                          `startingperiod` = '".format_save_date($v_start_date)."',
                          `endingperiod` = '".format_save_date($v_end_date)."',
                          `status` = '0',
                          `adduser` = '".$ses_login."',
                          `adddate` = NOW(),
                          `edituser` = '".$ses_login."',
                          `editdate` = NOW(),
                          `temp` = '0',
                          `requested` = '0',
                          `warehousecode` = '15'
                    ";
                    //echo $q;die;   
                    if(!mysql_query($q))
                    {
                        $msg = "Gagal menyimpan prodplancode";
                        echo "<script>alert('".$msg."');</script>";
                        die(); 
                    }    
                }
                
              
                $msg = "Berhasil menyimpan ".$prodplancode;
                echo "<script>alert('".$msg."');</script>"; 
                echo "<script>parent.CallAjaxForm('search','".$PCode."');</script>";             
                
                die();
                   
            }
            break;                                                                     
        case "edit_data" :                                                                                                           
            {   
            	//echo "<pre>";print_r($_POST);echo "</pre>";die;                                                                                                   
                if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }   
                if(!isset($_POST["v_del"])){ $v_del = isset($_POST["v_del"]); } else { $v_del = $_POST["v_del"]; }  
                if(!isset($_POST["v_undel"])){ $v_undel = isset($_POST["v_undel"]); } else { $v_undel = $_POST["v_undel"]; }  
                if(!isset($_POST["v_approve"])){ $v_approve = isset($_POST["v_approve"]); } else { $v_approve = $_POST["v_approve"]; }  
                if(!isset($_POST["v_reject"])){ $v_reject = isset($_POST["v_reject"]); } else { $v_reject = $_POST["v_reject"]; }  
                if(!isset($_POST["btn_delete"])){ $btn_delete = isset($_POST["btn_delete"]); } else { $btn_delete = $_POST["btn_delete"]; }    
                if(!isset($_POST["btn_unvoid"])){ $btn_unvoid = isset($_POST["btn_unvoid"]); } else { $btn_unvoid = $_POST["btn_unvoid"]; }    
                if(!isset($_POST["btn_approve"])){ $btn_approve = isset($_POST["btn_approve"]); } else { $btn_approve = $_POST["btn_approve"]; }    
                if(!isset($_POST["btn_reject"])){ $btn_reject = isset($_POST["btn_reject"]); } else { $btn_reject = $_POST["btn_reject"]; }    
                if(!isset($_POST["v_Approval_Remarks"])){ $v_Approval_Remarks = isset($_POST["v_Approval_Remarks"]); } else { $v_Approval_Remarks = $_POST["v_Approval_Remarks"]; }   
                if(!isset($_POST["v_prodplancode"])){ $v_prodplancode = isset($_POST["v_prodplancode"]); } else { $v_prodplancode = $_POST["v_prodplancode"]; }  
                
                if(!isset($_POST["v_start_date"])){ $v_start_date = isset($_POST["v_start_date"]); } else { $v_start_date = $_POST["v_start_date"]; }                                                                                                                                        
                if(!isset($_POST["v_end_date"])){ $v_end_date = isset($_POST["v_end_date"]); } else { $v_end_date = $_POST["v_end_date"]; }     
                
                if(!isset($_POST["v_pcode"])){ $v_pcode = isset($_POST["v_pcode"]); } else { $v_pcode = $_POST["v_pcode"]; }
               if(!isset($_POST["v_data_del"])){ $v_data_del = isset($_POST["v_data_del"]); } else { $v_data_del = $_POST["v_data_del"]; }     
                
                    
                if($v_start_date=="")
                {
                    $msg = "Period harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                else if($v_end_date=="")
                {
                    $msg = "Period harus diisi...";
                    echo "<script>alert('".$msg."');</script>";
                    die();    
                }
                
                if($v_del==1)   
                {
                    if($btn_delete=="Hapus")
                    {
                        // cek PB
                        $q = "
                                SELECT
                                    matreq.prodplancode
                                FROM
                                    matreq
                                WHERE
                                    1
                                    matreq.prodplancode = '".$v_prodplancode."'
                                LIMIT
                                    0,1
                        ";
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($prodplancode_matreq) = $row;
                        if($prodplancode_matreq)
                        {
                            $msg = $prodplancode_matreq." Tidak bisa dihapus karena sudah ada Matreq";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".prodplan
                            WHERE 1
                                AND ".$db["master"].".prodplan.prodplancode = '".$v_prodplancode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan prodplan";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                        
                        $q = "
                            DELETE FROM
                                ".$db["master"].".prodplandetail
                            WHERE 1
                                AND ".$db["master"].".prodplandetail.prodplancode = '".$v_prodplancode."'   
                        ";
                        //echo $q."<hr/>";                  
                        if(!mysql_query($q))
                        {
                            $msg = "Gagal menyimpan prodplandetail";
                            echo "<script>alert('".$msg."');</script>";
                            die(); 
                        } 
                               
                        $msg = "Berhasil Hapus ".$v_prodplancode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search', '');</script>";         
                        die();
                    }
                }
                else
                {
                    if($btn_save=="Save")
                    {   
                        // update masterbarang
                        {
                            $q = "
                                UPDATE
                                    ".$db["master"].".prodplan
                                SET     
                                    `startingperiod` = '".format_save_date($v_start_date)."',
                                    `endingperiod` = '".format_save_date($v_end_date)."',
                                    `edituser` = '".$ses_login."',
                                    `editdate` = NOW()
                                 WHERE 1
                                    AND ".$db["master"].".prodplan.prodplancode = '".$v_prodplancode."'   
                            ";
                            
                                             
                            if(!mysql_query($q))
                            {
                                $msg = "Gagal menyimpan prodplan";
                                echo "<script>alert('".$msg."');</script>";
                                die(); 
                            }
                            $i=0;
                            foreach($v_pcode as $key_pcode=>$val_pcode)
                            {
                                if(!isset($_POST["v_batch_".$i])){ $v_batch = isset($_POST["v_batch_".$i]); } else { $v_batch = $_POST["v_batch_".$i]; }         
                                if(!isset($_POST["v_formula_".$i])){ $v_formula = isset($_POST["v_formula_".$i]); } else { $v_formula = $_POST["v_formula_".$i]; }
                                if(!isset($_POST["v_prodplandetailid_".$i])){ $v_prodplandetailid = isset($_POST["v_prodplandetailid_".$i]); } else { $v_prodplandetailid = $_POST["v_prodplandetailid_".$i]; }         
                                
                                $v_batch = save_int($v_batch);
                                if($v_formula)
                                {
                                    $q = "
                                            UPDATE `prodplandetail`
                                            SET 
                                              `quantity` = '".$v_batch."',
                                              `remainquantity` = '".$v_batch."',
                                              `remainquantitypack` = '".$v_batch."',
                                              `edituser` = '".$ses_login."',
                                              `editdate` = NOW(),
                                              `formula` = '".$v_formula."'
                                            WHERE 
                                            	prodplandetailid = '".$v_prodplandetailid."'
                                                AND `prodplancode` = '".$v_prodplancode."'  
                                                AND `inventorycode` = '".$val_pcode."'  
                                    ";
                                       
                                    if(!mysql_query($q))
                                    {
                                        $msg = "Gagal simpan prodplandetail ".$v_prodplancode;
                                        echo "<script>alert('".$msg."');</script>"; 
                                        
                                    }
                                }
                                $i++;  
                            }
                            
                            foreach($v_data_del as $key_del=>$val_del)
                            {
                                $q = "
                                        DELETE FROM `prodplandetail`
                                        WHERE 
                                            `prodplancode` = '".$v_prodplancode."'  
                                            AND `inventorycode` = '".$val_del."'  
                                ";
                                if(!mysql_query($q))
                                {
                                    $msg = "Gagal hapus prodplandetail ".$v_prodplancode;
                                    echo "<script>alert('".$msg."');</script>"; 
                                    die();
                                }    
                            }
                            
                        }
                        
                        $msg = "Berhasil menyimpan ".$v_prodplancode;
                        echo "<script>alert('".$msg."');</script>"; 
                        echo "<script>parent.CallAjaxForm('search','".$v_prodplancode."');</script>";         
                        die();
                    } 
                }  
            } 
            break;   
    }
}                                                      

mysql_close($con);
?>  
