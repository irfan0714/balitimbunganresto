<?php

include("header.php");

$modul            = "MASTER BARANG";
$file_current     = "npm_master_barang.php";
$file_name        = "npm_master_barang_data.php";



if(!isset($_GET["id"])){ $id = isset($_GET["id"]); } else { $id = $_GET["id"]; }   
 
$menu_validation = menu_validation("masterbarang", $ses_login);
        
if(!$menu_validation["v"]){
    die("Anda tidak memiliki Otorisasi");
}	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - Modul Inventory - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTMLand media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script>
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='search')
				{
                    search_keyword = document.getElementById("search_keyword").value;      
                    search_KdDivisi = document.getElementById("search_KdDivisi").value; 
                    search_KdKategori = document.getElementById("search_KdKategori").value; 
                    search_order_by = document.getElementById("search_order_by").value; 
                    search_approval = document.getElementById("search_approval").value; 
                    
                    variabel += "&search_keyword="+search_keyword;
                    variabel += "&search_KdDivisi="+search_KdDivisi;
                    variabel += "&search_KdKategori="+search_KdKategori;
                    variabel += "&search_order_by="+search_order_by;
                    variabel += "&search_approval="+search_approval;
					
					variabel += "&v_PCode_curr="+param1;
                    
                    //alert(search_approval);

					//alert(param1);
					document.getElementById("col-search").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					document.getElementById("col-header").innerHTML   = "";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-search").innerHTML   = xmlhttp.responseText;

							if(param1)
							{
								CallAjaxForm('edit_data' ,param1);
							}
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='add_data')
				{
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';
					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}

				else if(tipenya=='edit_data')
				{
					variabel += "&v_PCode="+param1;

					//alert(variabel);
					document.getElementById("col-header").innerHTML = '<img src=\'images/img-ajax.gif\'>';

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							document.getElementById("col-header").innerHTML     = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}
				
				else if(tipenya=='ajax_divisi')
				{
					v_KdDivisi = document.getElementById("v_KdDivisi").value;
					
					variabel += "&v_KdDivisi="+v_KdDivisi;
                    
                    document.getElementById("td_KdSubDivisi").innerHTML = "Loading...";

					xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
					xmlhttp.onreadystatechange = function()
					{
						if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
						{
							//arr_data = xmlhttp.responseText.split("||");
							
							document.getElementById("td_KdSubDivisi").innerHTML   = xmlhttp.responseText;
						}

						return false;
					}
					xmlhttp.send(null);
				}
                else if(tipenya=='ajax_kategori')
                {
                    v_KdKategori = document.getElementById("v_KdKategori").value;
                    
                    variabel += "&v_KdKategori="+v_KdKategori;
                    
                    document.getElementById("td_KdSubKategori").innerHTML = "Loading...";

                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function()
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {
                            //arr_data = xmlhttp.responseText.split("||");
                            
                            document.getElementById("td_KdSubKategori").innerHTML   = xmlhttp.responseText;
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
                else if(tipenya=='ajax_subdivisi')
                {
                    v_KdSubDivisi = document.getElementById("v_KdSubDivisi").value;
                    
                    variabel += "&v_KdSubDivisi="+v_KdSubDivisi;
                    
                    document.getElementById("td_Kategori").innerHTML = "Loading...";

                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function()
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {
                            //arr_data = xmlhttp.responseText.split("||");
                            
                            document.getElementById("td_Kategori").innerHTML   = xmlhttp.responseText;
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
                
                else if(tipenya=='ajax_kategori_pos')
                {
                    v_KdKategori = document.getElementById("v_KdKategori").value;
                    
                    variabel += "&v_KdKategori="+v_KdKategori;
                    
                    document.getElementById("td_KdSubKategori").innerHTML = "Loading...";

                    xmlhttp.open('get', '<?php echo $file_name; ?>?ajax='+tipenya+variabel, true);
                    xmlhttp.onreadystatechange = function()
                    {
                        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200))
                        {
                            //arr_data = xmlhttp.responseText.split("||");                          
                            document.getElementById("td_KdSubKategori").innerHTML   = xmlhttp.responseText;
                        }

                        return false;
                    }
                    xmlhttp.send(null);
                }
                
                
				     
                     
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function change_pj(nilai)
		{
			if(nilai=="umum")
			{
				document.getElementById('td_type').colSpan = "3";
				document.getElementById('td_sub_type').colSpan = "3";
				document.getElementById('td_empl_name_1').style.display    = 'none';
				document.getElementById('td_empl_name_2').style.display    = 'none';
				document.getElementById('td_jabatan_1').style.display    = 'none';
				document.getElementById('td_jabatan_2').style.display    = 'none';
			}
			else
			{
				document.getElementById('td_type').colSpan = "0";
				document.getElementById('td_sub_type').colSpan = "0";
				document.getElementById('td_empl_name_1').style.display    = '';
				document.getElementById('td_empl_name_2').style.display    = '';
				document.getElementById('td_jabatan_1').style.display    = '';
				document.getElementById('td_jabatan_2').style.display    = '';
			}
			
		}
		
		function cek_qty()
		{
			var v_cek_qty = document.getElementById("v_cek_qty").checked;
			
			//alert(v_cek_qty);
			
			if(v_cek_qty)
			{
				document.getElementById('span_qty').style.display    = '';  
				document.getElementById("v_qty").value = '';
				document.getElementById('v_qty').focus();
			}  
			else
			{
				document.getElementById('span_qty').style.display    = 'none';  
			}
			
		}
		
		function pop_up_employee()
		{
			var variabel;
			var search_cabang_id;
			variabel = "";
			search_cabang_id="";
			
			v_cabang_id = document.getElementById("v_cabang_id").value;    
			
			if(v_cabang_id)
			{
				search_cabang_id = "?search_cabang_id="+v_cabang_id;	
			}
			
			variabel += search_cabang_id;
			  
			windowOpener(600, 800, 'Search Employee', 'npm_fa_master_fixed_asset_pop_up_employee.php'+variabel, 'Search Employee')
		}
		
		function pop_search_pr()
		{
			var variabel;
			variabel = "";
            
            v_KdGudang = document.getElementById("v_KdGudang").value;
            
            if(v_KdGudang=="")
            {
                alert("Gudang harus dipilih...");    
            } 
            else
            {
			    variabel += "?v_KdGudang="+v_KdGudang;
			    
			    windowOpener(400, 600, 'Search PR', 'npm_purchase_order_pop_search_pr.php'+variabel, 'Search PR')
            }
		}
		
		function change_par()
		{
		    var chk_gedung_par = document.getElementById("chk_gedung_par").checked;
		    
		    if(chk_gedung_par)
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = '';
		        document.getElementById('tr_periode_par').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_par').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_par').style.display = 'none';
		        document.getElementById('tr_periode_par').style.display = 'none';
			} 
		}
		
		function change_earthquake()
		{
		    var chk_gedung_earthquake = document.getElementById("chk_gedung_earthquake").checked;
		    
		    if(chk_gedung_earthquake)
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = '';
		        document.getElementById('tr_periode_earthquake').style.display = '';
		        
		        document.getElementById('tr_no_asuransi_earthquake').focus();
		    }
		    else
		    {
		        document.getElementById('tr_no_asuransi_earthquake').style.display = 'none';
		        document.getElementById('tr_periode_earthquake').style.display = 'none';
			} 
		}

		function confirm_delete(name)
		{
			var r = confirm("Anda yakin ingin Hapus "+name+" ? ");
			if(r)
			{
				document.getElementById("v_del").value = '1';
				document.getElementById("theform").submit();
			}
		}
        
        function confirm_unvoid(name)
        {
            var r = confirm("Anda yakin ingin UnVoid "+name+" ? ");
            if(r)
            {
                document.getElementById("v_undel").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve "+name+" ? ");
            if(r)
            {
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_reject(name)
        {
            var r = confirm("Anda yakin ingin Reject "+name+" ? ");
            if(r)
            {
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function pop_up_check()
        {
            try{
                v_NamaLengkap = document.getElementById("v_NamaLengkap").value;
                
                windowOpener(600, 800, 'Check Nama Barang', 'npm_master_barang_pop_up_check.php?v_NamaLengkap='+v_NamaLengkap, 'Check Nama Barang')    
            
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
        }
        
        function pop_up_check_rekening()
        {
            try{
                v_NamaRekening = document.getElementById("v_NamaRekening").value;
                
                windowOpener(600, 600, 'Check Nomor Rekening', 'npm_master_barang_pop_up_check_rekening.php?v_NamaRekening='+v_NamaRekening, 'Check Nama Rekening')    
            
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
        }
        
        function muncul_reject()
        {
            document.getElementById("btn_approve").style.display = "none";    
            document.getElementById("btn_confirm_reject").style.display = "none";    
            
            document.getElementById("v_Approval_Remarks").style.display = "";    
            document.getElementById("btn_reject").style.display = "";    
            
            document.getElementById("v_Approval_Remarks").focus();
        }
        

		function CheckAll(param, target)
		{
			var field = document.getElementsByName(target);
			var chkall = document.getElementById(param);
			if (chkall.checked == true)
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = true ;
			}else
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = false ;
			}
		}

		function reform(val)
		{
			var a = val.split(",");
			var b = a.join("");
			//alert(b);
			return b;
		}

		function format(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(0);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format4(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(4);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format2(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(2);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function format6(harga)
		{
			harga=parseFloat(harga);
			harga=harga.toFixed(6);
			//alert(harga);
			s = addSeparatorsNF(harga, '.', '.', ',');
			return s;
		}

		function addSeparatorsNF(nStr, inD, outD, sep)
		{
			nStr += '';
			var dpos = nStr.indexOf(inD);
			var nStrEnd = '';
			if (dpos != -1)
			{
				nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
				nStr = nStr.substring(0, dpos);
			}
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(nStr))
			{
				nStr = nStr.replace(rgx, '$1' + sep + '$2');
			}
			return nStr + nStrEnd;
		}

		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}

		function toFormat4(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format4(document.getElementById(id).value);
		}

		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
		}

		function toFormat6(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format6(document.getElementById(id).value);
		}

		function isanumber(id)
		{
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				document.getElementById(id).value=0;
			}
			else
			{
				document.getElementById(id).value=parseFloat(reform(document.getElementById(id).value));
			}
		}

		function start_page()
		{
            try
            {
			    document.getElementById("search_keyword").focus();
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
		}

		function mouseover(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#c1cdd8';
			}
		}

		function mouseout(target)
		{
			if(target.bgColor!="#cafdb5")
			{
				if (target.bgColor=='#ccccff')
				target.bgColor='#ccccff';
				else
				target.bgColor='#FFFFFF';
			}
		}

		function mouseclick(target, idobject, num)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			for(i=0;i<num;i++)
			{
				if (document.getElementById(idobject+'_'+i) != undefined)
				{
					document.getElementById(idobject+'_'+i).bgColor='#f5faff';
					if (target.id == idobject+'_'+i)
					target.bgColor='#ccccff';
				}
			}
		}

		function mouseclick1(target)
		{
			//var pjg = document.getElementById(idobject + '_sum').innerHTML;
			if(target.bgColor!="#cafdb5")
			{
				target.bgColor="#cafdb5";
			}
			else
			{
				target.bgColor="#FFFFFF";
			}
		}
		
		function exportexcel()
		{
			var variabel;
			var arr_data;
			variabel = "";
			
            search_keyword = document.getElementById("search_keyword").value;      
            search_KdDivisi = document.getElementById("search_KdDivisi").value; 
            search_KdKategori = document.getElementById("search_KdKategori").value; 
            search_order_by = document.getElementById("search_order_by").value; 
            
            variabel += "search_keyword="+search_keyword;
            variabel += "&search_KdDivisi="+search_KdDivisi;
            variabel += "&search_KdKategori="+search_KdKategori;
            variabel += "&search_order_by="+search_order_by;
            
            windowOpener(600, 800, 'Export To Excel', 'npm_master_barang_pop_up_excel.php?'+variabel, 'Export To Excel') 
			
		}
		
	</script>
    
    <style>
        .title_table{
            background: #009490; color: white; font-weight: bold;
        }
    </style>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed">
	
	<?php include("menu_kiri.php"); ?>
	
	<div class="main-content">
    
		<ol class="breadcrumb bc-3">
			<li>
				<a href="index.php">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			<li>Transaksi - Pembelian</li>
			<li class="active"><strong><?php echo $modul; ?></strong></li>
		</ol>
		
		<hr/>
		
		<div class="row">
		
		<iframe marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=yes id="forsubmit" name="forsubmit" style="width:100%;height:0px"></iframe>
		
			<div class="col-md-8">
				Pencarian
                &nbsp;
					<input type="text" class="form-control-new" size="20" maxlength="30" name="search_keyword" id="search_keyword">
                &nbsp;
                Divisi
                <select name="search_KdDivisi" id="search_KdDivisi" class="form-control-new" style="width: 130px;" >
                	<?php 
                        $q = "
                            SELECT
                                ".$db["master"].".divisi.KdDivisi,
                                ".$db["master"].".divisi.NamaDivisi
                            FROM
                                ".$db["master"].".divisi
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".divisi.NamaDivisi ASC
                        ";
                        $qry_divisi = mysql_query($q);
                        while($row_divisi = mysql_fetch_array($qry_divisi))
                        {
                    ?>
                    <option value="<?php echo $row_divisi["KdDivisi"]; ?>"><?php echo $row_divisi["NamaDivisi"]; ?></option>
                    <?php 
                        }
                    ?>
                    <option value="">Semua</option>
                </select> 
                &nbsp;
                Kategori
                <select name="search_KdKategori" id="search_KdKategori" class="form-control-new" style="width: 130px;">
                    <option value="">Semua</option>
                    <?php 
                        $q = "
                            SELECT
                                ".$db["master"].".kategori.KdKategori,
                                ".$db["master"].".kategori.NamaKategori
                            FROM
                                ".$db["master"].".kategori
                            WHERE
                                1
                            ORDER BY
                                ".$db["master"].".kategori.NamaKategori ASC
                        ";
                        $qry = mysql_query($q);
                        while($row = mysql_fetch_array($qry))
                        {
                            
                    ?>
                    <option value="<?php echo $row["KdKategori"]; ?>"><?php echo $row["NamaKategori"]; ?></option>
                    <?php 
                        }
                    ?>
                </select> 
                &nbsp;
                Urut
                <select name="search_order_by" id="search_order_by" class="form-control-new" style="width: 100px;">
                    <option value="Add Date">Add Date</option>
                    <option value="Nama Barang">Nama Barang</option>
                    <option value="PCode">PCode</option>
                </select>
                
                <?php 
                    if($ses_login=="tonny1205" || $ses_login=="sari1608" || $ses_login=="mart2005")
                    {
                        ?>
                        Status 
                        <select name="search_approval" id="search_approval" class="form-control-new" style="width: 120px;">
                            <option value="0">Semua</option>    
                            <option value="1">Waiting Approval</option>    
                        </select>
                        <?php
                    }
                    else
                    {
                        ?>
                        <input type="hidden" name="search_approval" id="search_approval" value="0">
                        <?php
                    }
                ?>
	    	</div>
	    
			<div class="col-md-4" align="right">
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('search'),show_loading_bar(100)">Cari<i class="entypo-search"></i></button>
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="CallAjaxForm('add_data'),show_loading_bar(100)">Tambah<i class="entypo-plus" ></i></button>
	       		<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="exportexcel(),show_loading_bar(100)">Export Excel<i class="entypo-download"></i></button>
	       	</div>
       	
       	</div>
		
		<hr />
		
		<center>
		<div id="col-search" class="row" style="overflow:auto;height:240px;"></div><!-- untuk search scrollable-->
        <hr/>
    	<div id="col-header" class="row" style="overflow:auto;padding-top:0px;"></div>
		</center>
    
<?php 
    if($id)
    {
        $id = save_char($id);
        ?>
        <script>CallAjaxForm('edit_data', '<?php echo $id; ?>');</script>
        <?php
    }

    include("footer.php"); 
?>