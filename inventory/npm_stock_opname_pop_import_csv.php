<?php 
include("header.php");

$modul = "Stock Opname Import CSV";

if(!isset($_GET["v_NoDokumen"])){ $v_NoDokumen = isset($_GET["v_NoDokumen"]); } else { $v_NoDokumen = $_GET["v_NoDokumen"]; }

if($v_NoDokumen=="")
{
    echo "No Dokumen Tidak ditemukan";
    die();
}

function save_int_csv($nilai)
{
    // 2.000,00
    $return = str_replace(".","", $nilai);
    $return = str_replace(",",".", $return);
           
    return $return;
}

if(!isset($_POST["btn_save"])){ $btn_save = isset($_POST["btn_save"]); } else { $btn_save = $_POST["btn_save"]; }

if($btn_save)
{
    $v_file_tmp    = $_FILES['v_file']['tmp_name'];
    $v_file_name   = strtolower($_FILES['v_file']['name']);
    $v_file_size   = $_FILES['v_file']['size'];
    $v_file_ext    = strtolower($_FILES['v_file']["type"]);
    
    $exp_ext = explode(".", $v_file_name);
    
    if($exp_ext[1]=="csv")
    {
        if($v_file_name)
        {
            if(file_exists("stock-opname-".$v_NoDokumen.".csv"))
            {
                unlink("stock-opname-".$v_NoDokumen.".csv");
            }
            
            copy($v_file_tmp,"stock-opname-".$v_NoDokumen.".csv");    
            chmod("stock-opname-".$v_NoDokumen.".csv", 0777);
            
            $row = 0;
            if (($handle = fopen("stock-opname-".$v_NoDokumen.".csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    
                    for ($c=0; $c < $num; $c++) {
                          //57
                          //788
                        if($row!=0)
                        {
                            $exp_data = explode(";", $data[$c]);
                            
                            //echo $data[$c]."<hr>";
                            if($c==0)
                            {
                                $xls_pcode = $data[$c];
                                $xls_pcode = trim($xls_pcode);
                                $xls_pcode = str_replace(" ", "", $xls_pcode);
                            }                                                 
                            
                            if($c==1)
                            {
                                $xls_qty = $data[$c];
                            } 
                        }
                        
                        if($xls_pcode)
                        {
                            $arr_data["list_pcode"][$xls_pcode] = $xls_pcode;
                            $arr_data["qty"][$xls_pcode] = save_int_csv($xls_qty);
                        }
                        
                    }

                    $row++;
                }
                fclose($handle);
            }

            foreach($arr_data["list_pcode"] as $xls_pcode => $val)
            {
                $xls_pcode = trim($xls_pcode);
                $xls_pcode = str_replace(" ", "", $xls_pcode);
                $xls_qty = $arr_data["qty"][$xls_pcode];

                $q = "
                        SELECT
                            ".$db["master"].".opname_detail.PCode
                        FROM
                            ".$db["master"].".opname_detail
                        WHERE
                            1
                            AND ".$db["master"].".opname_detail.NoDokumen = '".$v_NoDokumen."'
                            AND ".$db["master"].".opname_detail.PCode = '".$xls_pcode."'
                        LIMIT
                            0,1
                ";
                $qry = mysql_query($q);
                $row = mysql_fetch_array($qry);
                list($PCode_exist) = $row;
                
                if($PCode_exist)
                {
                    $q = "
                            UPDATE 
                                ".$db["master"].".opname_detail
                            SET
                                QtyFisik = '".$xls_qty."' 
                            WHERE
                                1
                                AND ".$db["master"].".opname_detail.NoDokumen = '".$v_NoDokumen."'
                                AND ".$db["master"].".opname_detail.PCode = '".$xls_pcode."'
                    ";
                    //echo $q."<hr>";
                    //die();
                    if(!mysql_query($q))
                    {
                        $msg .= "Failed Insert Stock opname_header Details ||".$q."<br>";
                    }  
                }
                else
                {
                    $q = "
                            INSERT INTO 
                                ".$db["master"].".opname_detail
                            SET
                                NoDokumen = '".$v_NoDokumen."',
                                PCode = '".$xls_pcode."',
                                QtyFisik = '".$xls_qty."'
                    "; 
                    if(!mysql_query($q))
                    {
                        $msg .= "Failed Insert Stock opname_header Details ||".$q."<br>";
                    }    
                }               
            }
            
            if(file_exists("stock-opname-".$v_NoDokumen.".csv"))
            {
                unlink("stock-opname-".$v_NoDokumen.".csv");
            }
            
            echo "<script>window.opener.CallAjaxForm('search','".$v_NoDokumen."');</script>";
            echo "<script>window.close();</script>";
            die();
 
        }  
    }
    else
    {
        $msg = "Format Harus CSV";
        echo "<script>alert('".$msg."')</script>";
        echo "<script>window.close();</script>";
        die();
    } 
}

?>  
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Neon Admin Panel" />
<meta name="author" content="" />
<title><?php echo $modul; ?> - NPM</title>
<link rel="shortcut icon" href="public/images/Logosg.png" >
<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="assets/css/NotoSans.css">
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/neon-core.css">
<link rel="stylesheet" href="assets/css/neon-theme.css">
<link rel="stylesheet" href="assets/css/neon-forms.css">
<link rel="stylesheet" href="assets/css/custom.css">
<link rel="stylesheet" href="assets/css/skins/black.css">
<link rel="stylesheet" href="public/css/style.css">
<link rel="stylesheet" href="assets/css/my.css">

<script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="assets/js/js.js"></script> 

<style>
.title{
    background:#dddddd;
    text-align:center;
    font-weight:bold;
}

.link_menu2{
        text-decoration: underline;
        color: black;
}

.link_menu2:hover{
    text-decoration: none;
}

#wrapper{
    width: 100%;
    height: 450px;
    overflow-x: scroll;
    overflow-y: scroll;
}

.freeze table thead{
    position: fixed;
}
</style>

      
<script>
function change_onMouseOver (id) {
  document.getElementById(id).style.background = '#EEEEEE';
}

function change_onMouseOut (id) {
  document.getElementById(id).style.background = '#FFFFFF';
}

function start_page()
{
    document.getElementById('v_keyword').focus();
}

function validasi()
{
    if(document.getElementById('v_file').value=="")
    {
        alert("File harus dipilih...");
        return false;
    }
}

function targetBlank (url) {
    blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
}

function EnableDisableChecked(no){
    var chkall = document.getElementById("choose");
    var field = document.getElementsByName("choose[]");
    
    //alert(no);
    var curr;
    curr = no;
    
    if (chkall.checked == true){
        field[curr].checked = false ;
    }else if (chkall.checked == true){
        field[curr].checked = true ;
    }        
}
</script>

</head>
<body onLoad="">
<div class="page-container sidebar-collapsed">
<div class="main-content">
    <form method="post" name="theform_csv" id="theform_csv" enctype="multipart/form-data" onsubmit="return validasi()">
    <table width="100%" class="table table-bordered responsive">
        <tr class="title_table">
            <td colspan="100%">Import CSV</td>
        </tr>
        
        <tr>
            <td class="title_table" width="100">File</td>
            <td><input type="file" class="form-control-new freeze" name="v_file" id="v_file" accept=".csv"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><button type="submit" name="btn_save" id="btn_save" class="btn btn-info btn-icon btn-sm icon-left" value="Save">Simpan<i class="entypo-check"></i></button></td>
        </tr>
    </table>
    </form>
    
    <div><a href="format-stockopname.csv">Download Format CSV</a></div>
</div>
</div>
</body>
</html>