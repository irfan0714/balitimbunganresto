<?php
class npm_json_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function getlistdo($tahun, $bulan){
		$sql = "SELECT h.`dono` as NoDokumen, h.`dodate` as Tanggal, h.`note` AS Note FROM `deliveryorder` h 
				WHERE YEAR(h.`dodate`)='$tahun' AND MONTH(h.`dodate`)='$bulan' AND h.`customerid`='PT1001' AND h.`status`='1'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getDetailDO($dono){
		
		$sql = "SELECT h.`dono`, h.`dodate`, h.`note`, d.`inventorycode`, b.`NamaLengkap`, d.`QtyPcs` FROM `deliveryorder` h INNER JOIN `deliveryorderdetail` d ON h.`dono`=d.`dono`
				INNER JOIN masterbarang b ON d.`inventorycode`=b.`PCode`
				WHERE h.`dono`='$dono' AND  h.`customerid`='PT1001' AND h.`status`='1';";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getBrand(){
		$sql = "Select KdBrand, NamaBrand from brand";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getSubBrand(){
		$sql = "SELECT KdSubBrand, NamaSubBrand, KdBrand FROM subbrand ORDER BY KdBrand, NamaSubBrand";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}
?>