<?php

class Globalmodel extends CI_Model {
    function __construct(){
        parent::__construct();
    }

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

  //panicbutton
  function GetAll(){
    $id = $this->input->post('user');
    // $this->db->where('id_paket', $id);

      $sql = "SELECT * FROM revisi_detail";


    $qry 	= $this->db->query($sql);
    return $qry->result_array();

  }

  function getrecipe(){
    $id = $this->input->post('user');
    // $this->db->where('id_paket', $id);

    $sql = "SELECT r.`resep_id`, r.`PCode`, r.`Satuan`, r.`Status`, r.`Qty`, r.Jenis, b.NamaLengkap
      FROM resep_header r INNER JOIN masterbarang b ON r.`PCode`=b.`PCode`";


    $qry 	= $this->db->query($sql);
    return $qry->result_array();

  }

  function pilihrecipe(){
    $id = $this->input->post('norecipe');

    $sql  = "SELECT r.`resep_id`, r.`PCode`, b.`NamaLengkap`, r.`Qty`, r.Satuan, r.Harga
          FROM resep_detail r INNER JOIN masterbarang b ON r.`PCode`=b.`PCode`
          WHERE resep_id='$id'";

    $qry 	= $this->db->query($sql);
    return $qry->result_array();
  }

  function GetHistory(){
    $id = $this->input->post('user');
    // $this->db->where('id_paket', $id);

      $sql = "SELECT * FROM revisi_detail WHERE username = '$id'";


    $qry 	= $this->db->query($sql);
    return $qry->result_array();

  }

  function GetHistoryBeli(){
    $id = $this->input->post('pcode');
    // $this->db->where('id_paket', $id);

      $sql = "SELECT
              h.`Tanggal`,
              s.`Nama`,
              m.`PCode`,
              m.`NamaLengkap`,
              d.`Satuan`,
              d.`Harga`
            FROM
              `invoice_pembelian_detail` d
              INNER JOIN `invoice_pembelian_header` h
                ON d.`NoFaktur` = h.`NoFaktur`  
              INNER JOIN `masterbarang` m
                ON d.`KdBarang` = m.`PCode`
              INNER JOIN `supplier` s
                ON s.`KdSupplier` = h.`KdSupplier`
            WHERE d.`KdBarang` = '$id' AND
            `Tanggal` BETWEEN DATE_SUB(NOW(), INTERVAL 1 YEAR) and NOW()
            ORDER BY `Tanggal` DESC"; 


    $qry  = $this->db->query($sql);
    return $qry->result_array();

  }

  function GetStatus(){
    $id = $this->input->post('user');
		// $this->db->where('id_paket', $id);

      $sql = "SELECT * FROM revisi_detail WHERE UserApproval = '$id' AND type = '1'";


		$qry 	= $this->db->query($sql);
    return $qry->result_array();

  }

  function approvereq(){
    $this->load->library('telegramlib');
    $telegram = new telegramlib();

    $id     = $this->input->post('id');
    // $alasan = $this->input->post('alasan');

    $result = '';
  	$sql = "SELECT NoDokumen, Status, JenisRevisi, usertelegram FROM revisi_detail WHERE NoRevisi='$id' ";
  	$rec = $this->db->query($sql);
  	$row = $rec->row_array();

    $nostruk = $row['NoDokumen'];
  	$status = $row['Status'];
  	$type = $row['JenisRevisi'];
  	// $date = date('Y-m-d H:i:s');
  	$usertelegram = $row['usertelegram'];

    $data = array(
      'Status'     => '1',
      'TglApprove' => date('Y-m-d H:i:s'),
      'Respon_by'  => '2'
    );


    $this->db->where("NoRevisi",$id);
    $this->db->update("revisi_detail",$data);

    if($type=='MS'){
			// $sql2 = "update revisi_detail set status = '1', TglApprove ='$date',Respon_by = '1' where NoRevisi='$id'";
			$sql1 = "CALL PendingMutasiAntarGudang('$nostruk')";
		  $this->db->query($sql1);

			$note = 'INFO : Pending Mutasi Antar Gudang No. Dokumen '.$nostruk.' telah Approve';
		}elseif($type=='SO'){

			$sql1 = "CALL OpenStockOpname('$nostruk')";
			$this->db->query($sql1);

			$note = 'INFO : Pending Stock Opname No. Dokumen '.$nostruk.' telah Approve';

		}elseif($type=='DL'){

			$sql1 = "CALL PendingPenerimaanLain('$nostruk')";
		  $this->db->query($sql1);

			$note = 'INFO : Pending Penerimaan Lain No. Dokumen '.$nostruk.' telah Approve';

		}elseif($type=='PL'){

			$sql1 = "CALL PendingPengeluaranLain('$nostruk')";
			$this->db->query($sql1);

			$note = 'INFO : Pending Stock Opname No. Dokumen '.$nostruk.' telah Approve';

		}



    $telegram->chat_id($usertelegram);
    $telegram->text($note);
    $telegram->send();

    return $msg['success'] = true;
  }

  function rejectreq(){
    $this->load->library('telegramlib');
    $telegram = new telegramlib();

    $id     = $this->input->post('id');
    $alasan = $this->input->post('alasan');

    $result = '';
  	$sql = "SELECT NoDokumen, Status, JenisRevisi, usertelegram FROM revisi_detail WHERE NoRevisi='$id' ";
  	$rec = $this->db->query($sql);
  	$row = $rec->row_array();

    $nostruk = $row['NoDokumen'];
  	$status = $row['Status'];
  	$type = $row['JenisRevisi'];
  	// $date = date('Y-m-d H:i:s');
  	$usertelegram = $row['usertelegram'];

    $data = array(
      'Status'     => '2',
      'TglApprove' => date('Y-m-d H:i:s'),
      'Respon_by'  => '2'
    );

    $datainsert = array(
      'NoRevisi' => $id,
      'Status'   => 'reply',
      'Alasan'   => $alasan
    );
    $this->db->insert("revisi_log", $datainsert);
    $this->db->where("NoRevisi",$id);
    $this->db->update("revisi_detail",$data);

    $telegram->chat_id($usertelegram);
    $telegram->text("Request Revisi Anda Di Reject untuk No. Dok ".$nostruk.". Alasan : ".$alasan);
    $telegram->send();

    return $msg['success'] = true;

  }

  function responreq(){
    $this->load->library('telegramlib');
    $telegram = new telegramlib();

    $id     = $this->input->post('id');
    $respon = $this->input->post('status');

    // $result = '';
  	$sql = "SELECT NoDokumen, Status, JenisRevisi, usertelegram FROM revisi_detail WHERE NoRevisi='$id' ";
  	$rec = $this->db->query($sql);
  	$row = $rec->row_array();

    $nostruk = $row['NoDokumen'];
  	$status = $row['Status'];
  	$type = $row['JenisRevisi'];

    $tahap = '';
    if($respon == '0'){
      $tahap = "di Pending";
    }else if ($respon == '2') {
      // code...
      $tahap = "di Reject, karena";
    }elseif ($respon == '3') {
      // code...
      $tahap = "dalam Proses Pengerjaan";
    }elseif ($respon == '4') {
      // code...
      $tahap = "Selesai Dikerjakan";
    }
  	// $date = date('Y-m-d H:i:s');
  	$usertelegram = $row['usertelegram'];

    $data = array(
      'Status'     => $respon,
      'TglApprove' => date('Y-m-d H:i:s'),
      'Respon_by'  => '2'
    );

    // $datainsert = array(
    //   'NoRevisi' => $id,
    //   'Status'   => 'reply',
    //   'Alasan'   => $alasan
    // );
    // $this->db->insert("revisi_log", $datainsert);
    $this->db->where("NoRevisi",$id);
    $this->db->update("revisi_detail",$data);

    $telegram->chat_id($usertelegram);
    $telegram->text("Request Perbaikan Anda untuk No. Req ".$id." sudah ".$tahap);
    $telegram->send();

    return $msg['success'] = true;

  }




  function GetRevisi(){
    $id = $this->input->post('jenis');
		// $this->db->where('id_paket', $id);
    if($id=="DL"){
      $sql = "SELECT * FROM trans_penerimaan_lain WHERE Status = 1 AND TglDokumen BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) and NOW()";
    }elseif ($id == "PL") {
      $sql = "SELECT * FROM trans_pengeluaran_lain WHERE Status = 1 AND TglDokumen BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) and NOW()";
    }elseif ($id == "SO") {
      $sql = "SELECT * FROM opname_header WHERE Status = 1 AND TglDokumen BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) and NOW()";
    }elseif ($id == "MS") {
      $sql = "SELECT * FROM trans_mutasi_header WHERE Status = 1 AND TglDokumen BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) and NOW()";
    }

		$qry 	= $this->db->query($sql);
    return $qry->result_array();

  }

  function SaveRevisi(){
    $this->load->library('telegramlib');
    $telegram = new telegramlib();

    $mylib = new globallib();
    $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
    list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
    $v_norevisi = $mylib->get_code_counter($this->db->database, "revisi_detail","NoRevisi", "NR", $xbulan, $xtahun);

    //Request Name
    $username = $this->session->userdata('username');
    $sql2 ="SELECT e.employee_name, u.TelegramID FROM employee e INNER JOIN user u ON e.employee_nik = u.employee_nik WHERE u.UserName = '$username'";
	  $row = $this->db->query($sql2);
	  $nama = $row->row_array();
	  $namal = '';
    $usertelegram = '';
    // print_r($nama);
    // print_r($sql2);
    // die();
    if(!empty($nama)){
			$namal = $nama["employee_name"];
      $usertelegram = $nama["TelegramID"];
		}else {
      $namal = $this->session->userdata('username');
      $usertelegram = $username;
    }

    //Approval1Name
    $telegramid = $this->input->post('approval');
    $sql3 ="SELECT employee.employee_name as employee_name , user.UserName as UserName FROM employee INNER JOIN user ON employee.employee_nik = user.employee_nik WHERE user.TelegramID = '$telegramid'";
	  $row2 = $this->db->query($sql3);
	  $namap = $row2->row_array();
	  $namalp = '';

    if(!empty($namap)){
			$namalp = $namap["employee_name"];
      $userapp = $namap["UserName"];
		}else {
      $namalp = $telegramid;
      $userapp = $telegramid;

    }

    // is_uploaded_file
    $section = $this->fileupload->do_upload(
    'assets/images/revisi/',
    'filegambar'
  );
  // if logo is uploaded then resize the logo
  if ($section !== false && $section != null) {
    $this->fileupload->do_resize(
      $section,
      800,
      500
    );
  }
    // is_uploaded_file

    $jenis = $this->input->post('jrevisi');
    $data = array(
      'NoRevisi'    => $v_norevisi ,
      'username'    => $this->session->userdata('username'),
      'JenisRevisi' => $this->input->post('jrevisi'),
      'NoDokumen'   => ($jenis=='OR'?$this->input->post('nodok'):$this->input->post('dokumen')),
      'NamaLengkap' => $namal,
      'Approval'    => $namalp,
      'UserApproval'=> $userapp,
      'type'        => ($jenis=='OR'?'2':'1'),
      'idtelegram'  => $telegramid,
      'TglRequest'  => date('Y-m-d H:i:s'),
      'Alasan'      => $this->input->post('alasan'),
      'AddUser'     => $this->session->userdata('username'),
      'usertelegram' => $usertelegram,
      'image'       => (!empty($section)?$section:'noimage')

    );

    $jrevisi = $this->input->post('jrevisi');
    $nrevisi = "";
    if($jrevisi == "MS"){
      $nrevisi = "<pre>Pending Mutasi Antar Gudang</pre>\n\n";
    }elseif ($jrevisi == "DL") {
      $nrevisi = "<pre>Pending Penerimaan Lain</pre>\n\n";
    }elseif ($jrevisi == "PL") {
      $nrevisi = "<pre>Pending Pengeluaran Lain</pre>\n\n";
    }elseif ($jrevisi == "SO") {
      $nrevisi = "<pre>Pending Stock Opname</pre>\n\n";
    }else{
      $nrevisi = "<pre>Permasalahan Sistem Lain</pre>\n\n";
    }

    $rdokumen = ($jenis=='OR'?$this->input->post('nodok'):$this->input->post('dokumen'));
    $tgl =  date("Y-m-d");
    $alasan = $this->input->post('alasan');


    $flag = $this->db->insert('revisi_detail',$data);
    if($flag){


      $data = array(
        'chat_id' => '523261461',
        'parse_mode' => 'html',
        'caption' => 'Gambar Error',
        // 'photo'  => new CURLFile(realpath($section)));
        'photo'  => "@$section");


      $ch = curl_init('https://api.telegram.org/bot501842023:AAFbwVFagbe84-kgig-PGkdgg--tPoxx7Dc/sendPhoto');
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($ch);
      curl_close($ch);


        //kirim juga ke telegream dengan username trisno1402
        //$tele = $this->edit_transaksi_posmodel->getTelegram('trisno1402');
        // $textreq= "<pre>" . str_pad('No. Req.',9,' '). str_pad(': '.$v_norevisi,9,' ') . "</pre>\n";
        // $textstruk= "<pre>" . str_pad('No. Dok',9,' '). str_pad(': '.$rdokumen,9,' ') . "</pre>\n";
        // $texttanggal= "<pre>" . str_pad('Tanggal',9,' '). str_pad(': '.$tgl,9,' ') . "</pre>\n";
        // $textalasaan= "<pre>" . str_pad('Alasan',9,' '). str_pad(': '.$alasan,9,' ') . "</pre>\n";
        // $textajukan= "<pre>" . str_pad('Diajukan',9,' '). str_pad(': '.$namal,9,' ') . "</pre>\n";

        $textreq= "<pre>" . str_pad('No. Req.',9,' '). str_pad(': '.$v_norevisi,9,' ') . "</pre>\n";
        $textstruk= "<pre>" . str_pad('No. Dok',9,' '). str_pad(': '.$rdokumen,9,' ') . "</pre>\n";
        $texttanggal= "<pre>" . str_pad('Tanggal',9,' '). str_pad(': '.$tgl,9,' ') . "</pre>\n";
        $textalasaan= "<pre>" . str_pad('Alasan',9,' '). str_pad(': '.$alasan,9,' ') . "</pre>\n\n";
        $textajukan= "<pre>" . str_pad('Diajukan',9,' '). str_pad(': '.$namal,9,' ') . "</pre>\n";

        // $textreq= "<pre>No. Req.  : " .$v_norevisi."</pre>\n";
        // $textstruk= "<pre>No Dok   : " .$rdokumen."</pre>\n";
        // $texttanggal= "<pre>Tgl    : " .$tgl."</pre>\n";
        // $textalasaan= "<pre>Alasan  : " .$alasan."</pre>\n";
        // $textajukan= "<pre>Diajukan  : " .$namal."</pre>\n" ;

        $vtext = $nrevisi.$textreq.$textstruk.$texttanggal.$textajukan.$textalasaan."Terimakasih";



        $keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujureq#'.$v_norevisi);
        $keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakreq#'.$v_norevisi);


        //kirim ke approval
        // $telegram->chat_id("523261461");
        $telegram->chat_id($telegramid);
        // $telegram->chat_id("300127392");
        $telegram->text($vtext);
        $telegram->keyboard($keyboard);
        $telegram->send_with_inline_keyboard();

        //kirim ke requester
        $telegram->chat_id($usertelegram);
        $telegram->text("Request Revisi Anda Sudah Terkirim. Silakan Tunggu Info Selanjutnya");
        $telegram->send();
      return true;
    }else {
      return false;
    }

  }

  function GetApproval(){
    $sql = "SELECT * FROM user u INNER JOIN employee e ON u.employee_nik = e.employee_nik WHERE u.UserLevel ='-1' AND u.UserName IN('indah0203','trisno1402','i2911')";
    $qry 	= $this->db->query($sql);
    return $qry->result_array();
  }

  function getDataRevisi(){
    $id = $this->input->post('norevisi');
    $sql ="SELECT * FROM revisi_detail WHERE NoRevisi ='$id'";
    $row = $this->db->query($sql);
    $tgltrans = $row->row_array();


    return $tgltrans;
	}
  //panicbutton

	function getTahun(){
    $sql ='SELECT DATE_FORMAT(PeriodeKasBank,"%d-%m-%Y") as TglTrans FROM aplikasi';
    $row = $this->db->query($sql);
    $tgltrans = $row->row_array();


    return $tgltrans;
	}

	function getGudangAdmin($user)
	{
		$sql = "SELECT gudang_admin.KdGudang FROM gudang_admin WHERE 1 AND gudang_admin.UserName = '".$user."'";
		$qry 	= $this->db->query($sql);
        return $qry->result_array();
	}

	function getPermission($str,$id)
	{
//        echo $str."||".$id;
		$sql="select * from userlevelpermissions where tablename=(select nama from menu where url='$str' and UserLevelID='$id') and UserLevelID='$id'";

		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getValidation($menu,$user)
	{
//        echo $str."||".$id;
		$sql="SELECT * FROM `menu_validation` a WHERE a.`user_id`='$user' AND a.`user_form`='$menu'";

		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getGenereteNumberTicket($sql){
		$sql 	="SELECT ".$sql." ";
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
	}
	function getQuery($sql){
		$sql 	="SELECT ".$sql." ";
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
	}
	function queryInsert($table,$data){
		$this->db->insert($table,$data);
        return true;
	}
	function queryUpdate($table,$data,$where){
		$this->db->where($where);
		$this->db->update($table,$data);
        return true;
	}
	function queryDelete($table,$where){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function getName($url)
	{
		$sql="select distinct nama,root,ulid from menu where url='$url';";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getName2($root)
	{
		$sql="select distinct nama,root,ulid from menu where nama='$root';";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getRoot($ulid)
	{
		$sql="select distinct nama,root,ulid from menu where ulid='$ulid' and root='1';";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getLogPrint($nodok,$type="")
	{
		$where_type="";
		if($type)
		{
			$where_type="AND form_data = '".$type."' ";
		}

		$sql = "
			SELECT
			  log_print.sid
			FROM
			  log_print
			WHERE 1
			  AND noreferensi = '".$nodok."'
			  ".$where_type."
		";
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function getEmail()
	{
		$sql="
			SELECT
			  email.email_id,
			  email.host,
			  email.port,
			  email.subject,
			  email.email_address,
			  email.password,
			  email.status
			FROM
			  email
			LIMIT 0, 01
		";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function function_email($menu)
	{
		$sql="
			SELECT
				a.`email_address` AS email,
				a.`email_name` AS employee_name
			FROM
			    `function_email` a
			WHERE a.`func_name` = '$menu'
		";
		$qry 	= $this->db->query($sql);
        return $qry->result_array();
	}

	function getPeriodeKasBank($tahun){
		$sql = "select DATE_FORMAT(PeriodeKasBank,'%Y-%m-%d') as PeriodeKasBank from aplikasi where Tahun='$tahun'";
		$rec = $this->getArrayResult($sql);
		return $rec[0]['PeriodeKasBank'];
	}
}
?>
