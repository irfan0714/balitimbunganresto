<?php
class order_touch_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getList($num,$offset,$id,$with)
	{
		$ipaddres = $this->session->userdata('ip_address');
		$kdkategori = $this->getKategoriKassa($ipaddres);
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause = " and $with like '%$id%'";
			}
			else
			{
				$clause = " and $with = '$id'";
			}
		}
    	$sql = "select NoKassa,NoTrans,date_format(Tanggal,'%d-%m-%Y') as Tanggal,h.Waktu, TotalItem,Kasir,KdPersonal,KdMeja,Tanggal as TglDokumen2
				from trans_order_header h inner join kassa k on h.NoKassa = k.id_kassa
				where k.KdKategori='$kdkategori' and h.status=0
				$clause 
				order by cast(NoTrans as unsigned) desc Limit $offset,$num
			";
			//left join kendaraan on kendaraan.KdKendaraan = h.KdKendaraan
			//left join personal on kendaraan.KdPersonal = personal.KdPersonal
		return $this->getArrayResult($sql);
    }
    
    function num_order_row($id,$with){
    	$ipaddres = $this->session->userdata('ip_address');
		$kdkategori = $this->getKategoriKassa($ipaddres);
     	$clause="";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause = " and $with like '%$id%'";
			}
			else
			{
				$clause = " and $with = '$id'";
			}
		}
		$sql = "select NoKassa,NoTrans,date_format(Tanggal,'%d-%m-%Y') as Tanggal,TotalItem,Kasir,KdPersonal,KdMeja,Tanggal as TglDokumen2
				from trans_order_header h inner join kassa k on h.NoKassa = k.id_kassa
				where k.KdKategori='$kdkategori' and h.status=0 $clause" ;
        return $this->NumResult($sql);
	}
	function getDate(){
    	$sql = "SELECT TglTrans,date_format(TglTrans,'%d-%m-%Y') as TglTrans2 from aplikasi";
        return $this->getRow($sql);
    }
	function FindBar($bar)
	{
		$sql = "SELECT PCode from masterbarang where BarcodeSatuanKecil='$bar'";
		return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	//=================================================================================================
	function getkassa($param1)
	{
    	$sql = "select * from kassa where ip='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
	function getnoipkassa($param1)
	{
    	$sql = "select * from kassa where id_kassa='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getnokassa($param1)
	{
    	$sql = "select * from kassa where ip='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row[0]['id_kassa'];
    }
    
    function getKategoriKassa($param1)
	{
    	$sql = "select * from kassa where ip='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row[0]['KdKategori'];
    }
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
		
    function masterbarang()
	{
    	$sql = "SELECT PCode, NamaLengkap,FlagReady from masterbarang ORDER BY NamaLengkap ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
    
    function getNote()
	{
    	$sql = "SELECT * from note ORDER BY id ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
	
	function getBarang()
	{
    	$sql = "select a.pcode,a.namalengkap,a.flagready,s.kategoriparent as kdkategori,t.subkategoriparent as kdsubkategori,
		        round(a.harga1c) as harga,a.satuan,komisi,printer,disc from 
		        (SELECT pcode,namalengkap,kdkategori,kdsubkategori,harga1c,Satuan1 as satuan,flagready,komisi,printer,DiscInternal as disc FROM masterbarang_touch WHERE Jenis='2' ORDER BY NamaLengkap ASC)a
				inner join
				(
				select kdkategori,kdparent as kategoriparent from kategoripos
				)s 
				on s.kdkategori=a.kdkategori
				inner join
				(
				select kdsubkategori,kdparent as subkategoriparent from subkategoripos
				)t
				on t.kdsubkategori=a.kdsubkategori
				ORDER BY a.namalengkap ASC
				";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getKategori()
	{
    	$sql = "select a.kdparent as kdkategori,s.namakategori from 
		        (SELECT distinct kdparent FROM kategoripos)a
				left join
				(
				select kdkategori,namakategori from kategoripos
				)s 
				on s.kdkategori=a.kdparent order by kdkategori";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getSubKategori()
	{
    	$sql = "select a.kdparent as kdsubkategori,s.kdsubkategori,s.namasubkategori,s.kdkategori from 
		        (SELECT distinct kdparent FROM subkategoripos)a
				left join
				(SELECT kdkategori,kdsubkategori,namasubkategori FROM subkategoripos)s
				on s.kdsubkategori=a.kdparent
				ORDER BY s.kdsubkategori ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getKeterangan()
	{
    	$sql = "SELECT keterangan FROM keterangan";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getUserId()
	{
    	$sql = "SELECT id,username,password FROM user";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getLokasi($kddivisi)
	{
    	$sql = "SELECT kdlokasi,keterangan, IF(KdMeja IS NULL, 0, 1) AS AdaIsi FROM lokasipos
				LEFT JOIN 
				(SELECT DISTINCT KdMeja FROM `trans_order_header` WHERE `status`=0 AND tanggal=CURDATE()) b
				ON kdlokasi=KdMeja
 				WHERE kdtipelokasi='02' AND kddivisi='$kddivisi' ORDER BY Keterangan ASC";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function do_simpan_voucher($no,$nokassa,$tgl,$voucher0,$vouchpakai0,$vouchjenis0) 
	{
        $sql = "INSERT INTO transaksi_detail_voucher(NoKassa,NoStruk,Tanggal,Jenis,NomorVoucher,NilaiVoucher)VALUE
                ('$nokassa','$no','$tgl','$vouchjenis0','$voucher0',$vouchpakai0)"; //tambahin field baru disini
        $qry = $this->db->query($sql);
		if($vouchjenis0<>'3')
		{
			$sql = "UPDATE voucher SET terpakai=terpakai+$vouchpakai0 where novoucher='$voucher0'";
			$qry = $this->db->query($sql);
		}
    }
    
    function do_simpan_photo($notrans,$kassa,$tgl,$kdphoto0) 
	{
        $sql = "INSERT INTO transaksi_detail_photo(NoKassa,NoStruk,Tanggal,NomorPhoto)VALUE
                ('$kassa','$notrans','$tgl','$kdphoto0')";
        $qry = $this->db->query($sql);
    }
	
	function do_simpan_compliment($id_compliment,$bruto)
	{
		$sql = "UPDATE compliment SET terpakai=terpakai+$bruto where nik='$id_compliment'";
		$qry = $this->db->query($sql);
	}
	
	function order_temp($nokassa)
	{
    	$sql = "select st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan, st.NoTrans
				from order_temp st, masterbarang mb 
				where st.PCode = mb.PCode and st.Qty<>'0' and st.NoKassa='$nokassa'
				order by st.NoUrut asc";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
    
    function order_temp_count($nokassa)
    {
    	$sql = "SELECT COUNT(PCode) as total FROM order_temp WHERE Qty<>'0' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
	
	function DeleteRecord($NoUrut,$nokassa)
	{
		if($NoUrut == '')
		{
			$LastRecord = $this->LastRecord(0,$nokassa);
			$AutoID     = $LastRecord[0]['AutoID'];
			
			$sql = "delete from order_temp where AutoID = '$AutoID' and NoKassa='$nokassa'";
			$qry = $this->db->query($sql);
		}
		else
		{
			$sql = "delete from order_temp where NoUrut = '$NoUrut' and NoKassa='$nokassa'";
			$qry = $this->db->query($sql);
		}
    }
	
	function LastRecord($echoFlg,$nokassa)
	{
    	$sql = "select st.AutoID, st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan
				from order_temp st, masterbarang mb
				where st.PCode = mb.PCode and st.NoKassa='$nokassa'
				order by st.NoUrut desc limit 1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
		if($echoFlg == 1){
        	echo 'datajson = '.jsonEncode($row);
		}else{
			return $row;
		}
    }
	
	function EditRecord($NoUrut,$nokassa)
	{
    	$sql = "select st.AutoID, st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan
				from order_temp st, masterbarang mb
				where st.PCode = mb.PCode and st.NoUrut = '$NoUrut' and st.NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
		
		echo 'datajson = '.jsonEncode($row);
    }
    
    function order_temp_cek($kdbrg,$nokassa)
	{
    	$sql = "select PCode as KodeBarang from order_temp where PCode='$kdbrg' and nokassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        
        return $row;
    }
    
    function order_temp_add($jumlah,$kdbrg,$NoTrans,$nokassa,$keterangan)
    {
		$sql = "UPDATE order_temp SET Qty = Qty + round($jumlah,2), Keterangan='$keterangan' WHERE NoTrans='$NoTrans' and PCode='$kdbrg' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
	}
    	
	function save_trans_detail($notrans,$nokassa,$kdcontact,$kdmeja,$kdpersonal)
	{
		$sql = "INSERT INTO trans_order_detail(`NoKassa`,`NoTrans`,`NoUrut`,`Tanggal`,`Waktu`,`Kasir`,`PCode`,`Qty`,`Satuan`,`Keterangan`,`KdStore`,`Status`,`KdPersonal`,`KdContact`)
				SELECT NoKassa,NoTrans,NoUrut,Tanggal,Waktu,Kasir,PCode,Qty,Satuan,Keterangan,KdStore,Status,'$kdpersonal','$kdcontact' FROM order_temp where NoTrans='$notrans' and Qty<>'0' and NoKassa='$nokassa'";//tambahin field baru disini
		$qry = $this->db->query($sql);    
	}
	
	function save_trans_header($notrans,$nokassa,$kdcontact,$kdmeja,$kdpersonal,$idguest,$adddate)
	{
	    //echo "waktu1";
		$sql = "INSERT INTO trans_order_header(`NoKassa`,`NoTrans`,`Tanggal`,`Waktu`,`Kasir`,`TotalItem`,`KdStore`,`Status`,`KdPersonal`,`KdMeja`,`KdContact`,`TotalGuest`,`AddDate`)
				SELECT MIN(NoKassa),MIN(NoTrans),MIN(Tanggal),MIN(Waktu),MIN(Kasir),COUNT(PCode),MIN(KdStore),MIN(Status),'$kdpersonal','$kdmeja','$kdcontact','$idguest','$adddate' FROM order_temp where NoTrans='$notrans' and Qty<>'0' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
		
		//$sql = "update kassa set notrans=notrans+1 where nokassa='$nokassa'";
		//$qry = $this->db->query($sql);
	}
	
	function clear_trans($notrans,$nokassa)
	{
		$sql = "delete from order_temp where NoTrans = '$notrans' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
    }
	
	function save_order_table($kdmeja)
    {
		$sql = "UPDATE Lokasi SET Status=Status+1 WHERE KdLokasi='$kdmeja'";
		//echo $sql;
		$qry = $this->db->query($sql);
	} 
	
	function no_trans($bulan,$tahun,$kategori)
	{
		$this->locktables('counter');
		
		if($kategori=='01')
		{
		$sql = "update counter set NoStruk01=NoStruk01+1 where bulan='$bulan' and tahun='$tahun'";
		}
		if($kategori=='02')
		{
		$sql = "update counter set NoStruk02=NoStruk02+1 where bulan='$bulan' and tahun='$tahun'";
		}
		if($kategori=='03')
		{
		$sql = "update counter set NoStruk03=NoStruk03+1 where bulan='$bulan' and tahun='$tahun'";
		}
		//echo $sql;
    	$qry = $this->db->query($sql);
		
		$this->unlocktables();
		
		if($kategori=='01')
		$sql = "select NoStruk01 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
		if($kategori=='02')
		$sql = "select NoStruk02 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
		if($kategori=='03')
		$sql = "select NoStruk03 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	
	function no_trans_temp()
	{
		$sql = "select NoTrans,NoKassa,Kasir from order_temp LIMIT 1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function customer($pelanggan)
	{
		$sql = "select * from customer where KdCustomer='$pelanggan'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
       echo 'datajson = '.jsonEncode($row);
	}
	
	
	function trans_header()
	{
		$sql = "select * from trans_order_header where Status = '0' order by NoTrans DESC LIMIT 1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	
	function NamaPrinter($id) {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function all_trans($id) {
        $sql = "select h.* from trans_order_header h where h.NoTrans = '$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function det_trans($id, $jenis) {
    	if($jenis==1) // meja
    		$where = " ";
    	elseif($jenis==2) // minuman
    		$where = " and s.`Jenis`= 'B'";
    	elseif($jenis==3) //makanan
    		$where = " and s.`Jenis`= 'F'";
    	elseif($jenis==4) //grill
    		$where = " and s.`Jenis`= 'G'";
    		
        $sql = "SELECT a.PCode,b.NamaLengkap AS NamaStruk,z.Image,a.Berat,z.`NamaInitial`,a.Qty, a.Keterangan
			FROM trans_order_detail a INNER JOIN masterbarang_touch b ON a.PCode=b.Pcode
			INNER JOIN masterbarang z ON a.`PCode`=z.`PCode`
			LEFT JOIN subkategoripos s ON b.`KdSubKategori`=s.`KdSubKategori`
			where a.NoTrans='$id' $where order by s.`Jenis` desc, Waktu ASC";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row;
    }
	
	function ifPCodeBarcode($id){
		$bar = substr($id,0,20);
		$sql = "select b.*,keterangan as NamaSatuan from(
				SELECT PCode,NamaLengkap as NamaInitial,KonversiJualKecil,KonversiBesarKecil,KonversiTengahKecil,
				KdSatuanJual,if(PCode='$id',concat('K','*&^%',KdSatuanKecil,'*&^%',HargaJualKecil),
				if(BarcodeSatuanBesar='$bar',concat('B','*&^%',KdSatuanBesar,'*&^%',HargaJualTengah),
				if(BarcodeSatuanTengah='$bar',concat('T','*&^%',KdSatuanTengah,'*&^%',HargaJualBesar),
				concat('K','*&^%',KdSatuanKecil,'*&^%',HargaJualKecil)))) as Jenis FROM masterbarang 
				Where PCode='$id' or BarcodeSatuanBesar='$bar' or BarcodeSatuanTengah='$bar' or 
				BarcodeSatuanKecil='$bar'
				)b
				left join
				(
				select NamaSatuan,Keterangan from satuan
				) s 
				on s.NamaSatuan=b.KdSatuanJual";		
		//echo $sql;
		return $this->getRow($sql);
	}
	
	function getPCodeDet($kode)
	{
		$sql = "
				select b.*,keterangan as NamaSatuan from(
				SELECT PCode,NamaLengkap as NamaInitial,KonversiJualKecil,KonversiBesarKecil,KonversiTengahKecil,KdSatuanJual 
				FROM masterbarang Where PCode='$kode'
				) b
				left join
				(
				select NamaSatuan,Keterangan from satuan
				) s 
				on s.NamaSatuan=b.KdSatuanJual";
        return $this->getRow($sql);
	}
	
	function getOpenOrder($tgl, $kdmeja){
		$sql = "select NoTrans, KdAgent, TotalGuest from trans_order_header where tanggal='$tgl' and kdmeja='$kdmeja' and status=0 limit 1";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if(count($row)>0)
        	return $row[0];
        else
        	return array('NoTrans'=>'','KdAgen'=>'','TotalGuest'=>'');
	}
	
	
}



