<?php
class pos_touch_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function getList($num,$offset,$id,$with, $ipaddress)
	{
		$date = date('Y-m-d');
		if($offset !=''){
			$offset = $offset;
		}
		else{
			$offset = 0;
		}
		$clause="";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
		$datakassa = $this->getkassa($ipaddress);
		$kategori = $datakassa[0]['KdKategori'];

		$sql = "select h.NoTrans,date_format(h.Tanggal,'%d-%m-%Y') as Tanggal,h.KdMeja, count(h.KdMeja) as `Order`,
		# sum(d.status) as Selesai
		SUM(IF(d.status!=0,1,0)) AS Selesai,
		SUM(d.`MenuBaru`) AS MenuBaru
		from trans_order_header h inner join trans_order_detail d on h.notrans=d.notrans
		inner join kassa k on h.NoKassa = k.id_kassa
		where h.status=0 and k.KdKategori='$kategori' AND h.`Tanggal`=  '$date' and d.Status<>2
		group by h.Tanggal, h.KdMeja
		order by  h.Tanggal, h.KdMeja Limit $offset,$num
		";

		//echo $sql;die;
		//left join kendaraan on kendaraan.KdKendaraan = h.KdKendaraan
		//left join personal on kendaraan.KdPersonal = personal.KdPersonal
		return $this->getArrayResult($sql);
	}

	function num_order_row($id,$with,$ipaddress){
		$clause="";
		$datakassa = $this->getkassa($ipaddress);
		$kategori = $datakassa[0]['KdKategori'];
		if($id!=''){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
		$sql = "SELECT NoTrans FROM trans_order_header h
		inner join kassa k on h.NoKassa = k.id_kassa
		where h.status=0 and k.KdKategori='$kategori'
		$clause";
		return $this->NumResult($sql);
	}
	function getDate(){
		$sql = "SELECT TglTrans,date_format(TglTrans,'%d-%m-%Y') as TglTrans2 from aplikasi";
		return $this->getRow($sql);
	}
	function FindBar($bar)
	{
		$sql = "SELECT PCode from masterbarang where BarcodeSatuanKecil='$bar'";
		return $this->getRow($sql);
	}
	
	function getListYangMauDiDiskonkan($meja,$username,$todays)
	{
		$sql = "SELECT a.*,a.`Harga`*a.`Qty` AS Jumlah FROM transaksi_temp a where a.Meja='$meja' AND a.AddUser='$username' AND a.AddDate='$todays' AND a.GroupBarang IN('2','8') ORDER BY Jumlah DESC";
		return $this->getArrayResult($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}
	//=================================================================================================
	function getkassa($param1)
	{
		$sql = "select * from kassa where ip='$param1'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function voucher($id_voucher) {
		$sql = "SELECT novoucher,keterangan,nominal,rupdisc,jenis,STATUS FROM voucher WHERE novoucher='$id_voucher'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			echo  "salah";
		}else{
			echo 'datajson = ' . json_encode($row);
		}

	}

	function ketentuandisc($pcode) {

        $day= date('N', strtotime(date("Y-m-d")));
        $flag=0;
      
		$tgl = date('Y-m-d');
		$sql = "SELECT d.`Nilai`, h.berlaku
		FROM discountheader h INNER JOIN discountdetail d ON h.`NoTrans`=d.`NoTrans`
		WHERE h.`TglAkhir`>='$tgl' AND h.`TglAwal`<='$tgl' and d.pcode='$pcode' AND h.`Status`=1";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			return 0;
		}else{
            $pisah_hari= explode("#",$row[0]['berlaku']);
            $tanda_pager = substr_count($row[0]['berlaku'], "#");
            for($x=0; $x<($tanda_pager*1); $x++){
                if($day==$pisah_hari[$x])
                   $flag+=1;
            }
            if($flag>0)
                return $row[0]['Nilai'];
            else
                return 0;
		}
	}

	function hargagofood($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT d.Harga FROM groupharga_header h INNER JOIN groupharga_detail d ON h.`GroupHargaID`=d.`GroupHargaID`
		WHERE d.`PCode`='$pcode' AND h.`GroupHargaName`='GOFOOD'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			return 0;
		}else{
			return $row[0]['Harga'];
		}
	}

	function getKdGroupBarang($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT a.KdGroupBarang FROM masterbarang a WHERE a.PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			return 0;
		}else{
			return $row[0]['KdGroupBarang'];
		}
	}
	
	function getDiskonSnack($pcode,$meja) {
		$tgl = date('Y-m-d');
		$sql = "SELECT a.Waktu FROM trans_order_detail a WHERE a.PCode='$pcode' AND a.Tanggal='$tgl' AND a.KdMeja='$meja' AND a.Status='1'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		
		if(empty($row)){
			    $Diskonnya = 0;
		}else{
			    date_default_timezone_set('Asia/Jakarta');
		    	$sekarang = strtotime($row[0]['Waktu']);
		    	$sore = strtotime('15:00:00');
		    	$malam = strtotime('23:30:00');
		    	
		    	if($sekarang>$sore  AND $sekarang<$malam){
					$Diskonnya = 50;
				}else{
					$Diskonnya = 0;
				}
		}
		
		return $Diskonnya;
	}
	
	function getServiceCharge($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT a.Service_charge FROM masterbarang a WHERE a.PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			return 0;
		}else{
			return $row[0]['Service_charge'];
		}
	}
	
	function ambilDiskonnya($pcode,$meja,$username){
		$tgl = date('Y-m-d');
		$sql = "SELECT a.Diskon FROM transaksi_temp a WHERE a.PCode='$pcode' AND a.AddUser='$username' AND a.Meja='$meja' AND a.AddDate='$tgl'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			return 0;
		}else{
			return $row[0]['Diskon'];
		}
	}
	
	function getnoipkassa($param1)
	{
		$sql = "select * from kassa where id_kassa='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function getnokassa($param1)
	{
		$sql = "select * from kassa where ip='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
		$row = $qry->result_array();

		return $row[0]['id_kassa'];
	}

	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();

		return $row;
	}

	function kurs($valas)
	{
		$sql = "SELECT NilaiTukar AS Nilai FROM mata_uang WHERE Kd_Uang='$valas'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();

		return $row;
	}

	function masterbarang()
	{
		$sql = "SELECT PCode, NamaLengkap,FlagReady from masterbarang ORDER BY NamaLengkap ASC";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();

		return $row;
	}

	function getBarang($divisi,$subdivisi)
	{
		$sql = "select a.pcode,a.namalengkap,a.flagready,s.kategoriparent as kdkategori,t.subkategoriparent as kdsubkategori,
		round(a.harga1c) as harga,a.Satuan1 as satuan,a.komisi,a.printer,a.DiscInternal as disc, b.`KdGroupBarang` from
		masterbarang_touch a
		inner join masterbarang b on a.pcode=b.pcode
		left join
		(
			select kdkategori,kdparent as kategoriparent from kategoripos
			)s on s.kdkategori=a.kdkategori
			left join
			(
				select kdsubkategori,kdparent as subkategoriparent from subkategoripos
				)t on t.kdsubkategori=a.kdsubkategori
				where a.Jenis='2' and b.Status='A' and b.KdDivisi ='$divisi' AND b.KdSubDivisi='$subdivisi'
				ORDER BY a.namalengkap ASC
				";
				//echo $sql;
				$qry = $this->db->query($sql);
				$row = $qry->result_array();
				return $row;
			}

			function getKategori()
			{
				$sql = "select a.kdparent as kdkategori,s.namakategori from
				(SELECT distinct kdparent FROM kategoripos)a
				left join
				(
					select kdkategori,namakategori from kategoripos
					)s
					on s.kdkategori=a.kdparent order by kdkategori";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function getSubKategori()
				{
					$sql = "select a.kdparent as kdsubkategori,s.kdsubkategori,s.namasubkategori,s.kdkategori from
					(SELECT distinct kdparent FROM subkategoripos)a
					left join
					(SELECT kdkategori,kdsubkategori,namasubkategori FROM subkategoripos)s
					on s.kdsubkategori=a.kdparent
					ORDER BY s.kdsubkategori ASC";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function getKeterangan()
				{
					$sql = "SELECT keterangan FROM keterangan";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function getUserId()
				{
					$sql = "SELECT id,username,password FROM user";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function getLokasi($kddivisi)
				{
					$sql = "SELECT kdlokasi,keterangan FROM lokasipos WHERE kdtipelokasi='02' and kddivisi='$kddivisi' ORDER BY Keterangan ASC";
					//echo $sql;
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function do_simpan_voucher($no,$nokassa,$tgl,$voucher0,$vouchpakai0,$vouchjenis0)
				{
					//if($vouchjenis0=='5'){
					//$vouchpakai0 = 0;
					//}else{
					$vouchpakai0 = $vouchpakai0;
					//}

					$sql = "INSERT INTO transaksi_detail_voucher(NoKassa,NoStruk,Tanggal,Jenis,NomorVoucher,NilaiVoucher)VALUE
					('$nokassa','$no','$tgl','$vouchjenis0','$voucher0',$vouchpakai0)"; //tambahin field baru disini
					$qry = $this->db->query($sql);
					if($vouchjenis0<>'3')
					{
						$sql = "UPDATE voucher SET terpakai=terpakai+$vouchpakai0 where novoucher='$voucher0'";
						$qry = $this->db->query($sql);
					}
				}

				function do_simpan_photo($notrans,$kassa,$tgl,$kdphoto0)
				{
					$sql = "INSERT INTO transaksi_detail_photo(NoKassa,NoStruk,Tanggal,NomorPhoto)VALUE
					('$kassa','$notrans','$tgl','$kdphoto0')";
					$qry = $this->db->query($sql);
				}

				function do_simpan_compliment($id_compliment,$bruto)
				{
					$sql = "UPDATE compliment SET terpakai=terpakai+$bruto where nik='$id_compliment'";
					$qry = $this->db->query($sql);
				}

				function order_temp($nokassa)
				{
					$sql = "select st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan, st.NoTrans
					from order_temp st, masterbarang mb
					where st.PCode = mb.PCode and st.Qty<>'0' and st.NoKassa='$nokassa'
					order by st.NoUrut asc";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}

				function order_temp_count($nokassa)
				{
					$sql = "SELECT COUNT(PCode) as total FROM order_temp WHERE Qty<>'0' and NoKassa='$nokassa'";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}

				function DeleteRecord($NoUrut,$nokassa)
				{
					if($NoUrut == '')
					{
						$LastRecord = $this->LastRecord(0,$nokassa);
						$AutoID     = $LastRecord[0]['AutoID'];

						$sql = "delete from order_temp where AutoID = '$AutoID' and NoKassa='$nokassa'";
						$qry = $this->db->query($sql);
					}
					else
					{
						$sql = "delete from order_temp where NoUrut = '$NoUrut' and NoKassa='$nokassa'";
						$qry = $this->db->query($sql);
					}
				}

				function LastRecord($echoFlg,$nokassa)
				{
					$sql = "select st.AutoID, st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan
					from order_temp st, masterbarang mb
					where st.PCode = mb.PCode and st.NoKassa='$nokassa'
					order by st.NoUrut desc limit 1";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					if($echoFlg == 1){
						echo 'datajson = '.jsonEncode($row);
					}else{
						return $row;
					}
				}

				function EditRecord($NoUrut,$nokassa)
				{
					$sql = "select st.AutoID, st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan
					from order_temp st, masterbarang mb
					where st.PCode = mb.PCode and st.NoUrut = '$NoUrut' and st.NoKassa='$nokassa'";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					echo 'datajson = '.jsonEncode($row);
				}

				function order_temp_cek($kdbrg,$nokassa)
				{
					$sql = "select PCode as KodeBarang from order_temp where PCode='$kdbrg' and nokassa='$nokassa'";
					$qry = $this->db->query($sql);
					$row = $qry->num_rows();

					return $row;
				}

				function order_temp_add($jumlah,$kdbrg,$NoTrans,$nokassa,$keterangan)
				{
					$sql = "UPDATE order_temp SET Qty = Qty + round($jumlah,2), Keterangan='$keterangan' WHERE NoTrans='$NoTrans' and PCode='$kdbrg' and NoKassa='$nokassa'";
					$qry = $this->db->query($sql);
				}

				function save_trans_detail($notrans,$nokassa,$kdcontact,$kdmeja,$kdpersonal)
				{
					$sql = "INSERT INTO trans_order_detail(`NoKassa`,`NoTrans`,`NoUrut`,`Tanggal`,`Waktu`,`Kasir`,`PCode`,`Qty`,`Satuan`,`Keterangan`,`KdStore`,`Status`,`KdPersonal`,`KdContact`)
					SELECT NoKassa,NoTrans,NoUrut,Tanggal,Waktu,Kasir,PCode,Qty,Satuan,Keterangan,KdStore,Status,'$kdpersonal','$kdcontact' FROM order_temp where NoTrans='$notrans' and Qty<>'0' and NoKassa='$nokassa'";//tambahin field baru disini
					$qry = $this->db->query($sql);
				}

				function save_trans_header($notrans,$nokassa,$kdcontact,$kdmeja,$kdpersonal,$idguest,$adddate)
				{
					//echo "waktu1";
					$sql = "INSERT INTO trans_order_header(`NoKassa`,`NoTrans`,`Tanggal`,`Waktu`,`Kasir`,`TotalItem`,`KdStore`,`Status`,`KdPersonal`,`KdMeja`,`KdContact`,`TotalGuest`,`AddDate`)
					SELECT MIN(NoKassa),MIN(NoTrans),MIN(Tanggal),MIN(Waktu),MIN(Kasir),COUNT(PCode),MIN(KdStore),MIN(Status),'$kdpersonal','$kdmeja','$kdcontact','$idguest','$adddate' FROM order_temp where NoTrans='$notrans' and Qty<>'0' and NoKassa='$nokassa'";
					$qry = $this->db->query($sql);

					//$sql = "update kassa set notrans=notrans+1 where nokassa='$nokassa'";
					//$qry = $this->db->query($sql);
				}

				function clear_trans($notrans,$nokassa)
				{
					$sql = "delete from order_temp where NoTrans = '$notrans' and NoKassa='$nokassa'";
					$qry = $this->db->query($sql);
				}

				function save_order_table($kdmeja)
				{
					$sql = "UPDATE Lokasi SET Status=Status+1 WHERE KdLokasi='$kdmeja'";
					//echo $sql;
					$qry = $this->db->query($sql);
				}

				function no_trans($bulan,$tahun,$kategori)
				{
					$this->locktables('counter');
					//echo $kategori;
					if($kategori=='01')
					{
						$sql = "update counter set NoStruk01=NoStruk01+1 where bulan='$bulan' and tahun='$tahun'";
					}
					if($kategori=='02')
					{
						$sql = "update counter set NoStruk02=NoStruk02+1 where bulan='$bulan' and tahun='$tahun'";
					}
					if($kategori=='03')
					{
						$sql = "update counter set NoStruk03=NoStruk03+1 where bulan='$bulan' and tahun='$tahun'";
					}
					//echo $sql;
					$qry = $this->db->query($sql);

					$this->unlocktables();

					if($kategori=='01')
					$sql = "select NoStruk01 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
					if($kategori=='02')
					$sql = "select NoStruk02 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
					if($kategori=='03')
					$sql = "select NoStruk03 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";

					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}

				function ambil_No($tahun,$bulan, $tgl, $nokassa) {
					$tahun = $tahun-2000;
					$tglhari = substr($tgl, 8, 2)*1;
					$tglhari = $tglhari<10 ? '0'.$tglhari : $tglhari;

					$kassa = $nokassa<'10' ? '0'.$nokassa : $nokassa;
					$nostruk = $tahun.$bulan.$tglhari.$kassa;
					$bulan = $bulan * 1;

					//$sql = "SELECT NoStruk FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
					$sql = "SELECT NoStruk FROM transaksi_header WHERE nokassa='$nokassa' AND tanggal='$tgl'
					AND nostruk LIKE '$nostruk%' order by NoStruk Desc limit 1";

					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					if(count($row) == 0){
						$hasil = $nostruk . '001';
					}else{
						$lastnostruk = substr($row[0]['NoStruk'],-3)*1;
						$newno = $lastnostruk + 1;
						if($newno<10){
							$newno = '00'.$newno;
						}elseif($newno<100){
							$newno = '0'.$newno;
						}
						$hasil = $nostruk.$newno;
					}
					return $hasil;
				}



				function no_trans_temp()
				{
					$sql = "select NoTrans,NoKassa,Kasir from order_temp LIMIT 1";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}

				function customer($pelanggan)
				{
					$sql = "select * from customer where KdCustomer='$pelanggan'";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					echo 'datajson = '.jsonEncode($row);
				}


				function trans_header()
				{
					$sql = "select * from trans_order_header where Status = '0' order by NoTrans DESC LIMIT 1";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}


				function NamaPrinter($id) {
					$sql = "SELECT * from kassa where ip='$id'";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function all_trans($id,$kassa) {
					$sql = "select h.*, k.IsCounter, m.JumlahPoint, COALESCE(gh.NamaGroupDisc,'') as NamaGroupDisc,
					COALESCE(m.NamaMember,'') as NamaMember, tm.NilaiPoint
					from transaksi_header h inner join kassa k on h.NoKassa = k.id_kassa
					left join member m on h.KdMember=m.KdMember
					left join type_member tm on m.KdTipeMember=tm.KdTipeMember
					left join group_disc_header gh on h.NamaCard=gh.KdGroupDisc
					where h.NoStruk = '$id' and h.NoKassa='$kassa' order by NoStruk DESC LIMIT 1";
					// echo $sql;
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					return $row;
				}

				function det_trans($id,$kassa) {
					$sql = "select a.PCode,b.NamaLengkap as NamaStruk,a.Qty,a.Berat,a.Harga,a.Netto,Disc1,Ketentuan1
					from transaksi_detail a, masterbarang_touch b where a.NoStruk='$id' and a.NoKassa='$kassa'
					and a.PCode=b.Pcode order by Waktu ASC";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}

				function det_trans_gofood($id,$kassa) {
					$sql = "select 
					a.PCode,
					b.NamaLengkap AS NamaStruk,
					a.Qty,
					a.Berat,
					ROUND(a.Harga+(a.`Harga`*5/100)+((a.Harga+(a.`Harga`*5/100))*10/100),0) AS Harga,
					a.Qty * ROUND(a.Harga+(a.`Harga`*5/100)+((a.Harga+(a.`Harga`*5/100))*10/100),0) AS Netto,
					Disc1,
					Ketentuan1
					from transaksi_detail a, masterbarang_touch b where a.NoStruk='$id' and a.NoKassa='$kassa'
					and a.PCode=b.Pcode order by Waktu ASC";
				
					$qry = $this->db->query($sql);
					$row = $qry->result_array();

					return $row;
				}
				//npm model
				function all_transnpm($id,$kassa) {
        		$sql = "select h.*, k.IsCounter, m.JumlahPoint, COALESCE(gh.NamaGroupDisc,'') as NamaGroupDisc, 
        			COALESCE(m.NamaMember,'') as NamaMember, tm.NilaiPoint  
        			from transaksi_header h inner join kassa k on h.NoKassa = k.id_kassa 
        			left join member m on h.KdMember=m.KdMember
        			left join type_member tm on m.KdTipeMember=tm.KdTipeMember
        			left join group_disc_header gh on h.NamaCard=gh.KdGroupDisc
        			where h.NoStruk = '$id' and h.NoKassa='$kassa' order by NoStruk DESC LIMIT 1";
        			// echo $sql;
        		$qry = $this->db->query($sql);
        		$row = $qry->result_array();
        		return $row;
   				 }

   			 function det_transnpm($id,$kassa) {
       		 $sql = "select a.PCode,b.NamaLengkap as NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			from transaksi_detail a, masterbarang_touch b where a.NoStruk='$id' and a.NoKassa='$kassa'
			and a.PCode=b.Pcode order by Waktu ASC";
	       	 $qry = $this->db->query($sql);
	       	 $row = $qry->result_array();

	       	 return $row;
    		}

				//npmmodel

				function ifPCodeBarcode($id){
					$bar = substr($id,0,20);
					$sql = "select b.*,keterangan as NamaSatuan from(
						SELECT PCode,NamaLengkap as NamaInitial,KonversiJualKecil,KonversiBesarKecil,KonversiTengahKecil,
						KdSatuanJual,if(PCode='$id',concat('K','*&^%',KdSatuanKecil,'*&^%',HargaJualKecil),
						if(BarcodeSatuanBesar='$bar',concat('B','*&^%',KdSatuanBesar,'*&^%',HargaJualTengah),
						if(BarcodeSatuanTengah='$bar',concat('T','*&^%',KdSatuanTengah,'*&^%',HargaJualBesar),
						concat('K','*&^%',KdSatuanKecil,'*&^%',HargaJualKecil)))) as Jenis FROM masterbarang
						Where PCode='$id' or BarcodeSatuanBesar='$bar' or BarcodeSatuanTengah='$bar' or
						BarcodeSatuanKecil='$bar'
						)b
						left join
						(
							select NamaSatuan,Keterangan from satuan
							) s
							on s.NamaSatuan=b.KdSatuanJual";
							//echo $sql;
							return $this->getRow($sql);
						}

						function getPCodeDet($kode)
						{
							$sql = "
							select b.*,keterangan as NamaSatuan from(
								SELECT PCode,NamaLengkap as NamaInitial,KonversiJualKecil,KonversiBesarKecil,KonversiTengahKecil,KdSatuanJual
								FROM masterbarang Where PCode='$kode'
								) b
								left join
								(
									select NamaSatuan,Keterangan from satuan
									) s
									on s.NamaSatuan=b.KdSatuanJual";
									return $this->getRow($sql);
								}


								function getcontact($kode)
								{
									$sql = "select * FROM contact Where KdContact='$kode'";
									return $this->getRow($sql);
								}

								function getpersonal($kode)
								{
									$sql = "select * FROM personal Where KdPersonal='$kode'";
									return $this->getRow($sql);
								}

								function order_temp_cek_ada($notrans,$nokassa)
								{
									$sql = "select PCode as KodeBarang from order_temp where nokassa='$nokassa' and NoTrans='$notrans' and Qty<>'0'";
									$qry = $this->db->query($sql);
									$row = $qry->num_rows();
									return $row;
								}

								function getprintjob($nokassa)
								{
									$sql = "select a.*,b.jenisprint from kassaprint a, kassa b where a.nokassa='$nokassa' and b.nokassa='$nokassa'";
									$qry = $this->db->query($sql);
									$row = $qry->result_array();
									return $row;
								}

								function getDataOrder($kdmeja){
									$tgl = date('Y-m-d');
									$sql = "SELECT h.KdMeja, AVG(h.TotalGuest) AS TotalGuest, h.KdAgent, d.PCode,
									b.`NamaLengkap`, SUM(d.`Qty`) AS Qty, AVG(b.`Harga1c`) AS Harga, b.Komisi, b.DiscInternal, COALESCE(ds.Nilai,0)  as KDisc, SUM(d.`Berat`) AS Berat
									FROM trans_order_header h INNER JOIN trans_order_detail d ON h.notrans=d.notrans
									INNER JOIN masterbarang_touch b ON d.`PCode`=b.`PCode`
									left join(
										SELECT d.`PCode`, d.`Nilai`
										FROM discountheader h INNER JOIN discountdetail d ON h.`NoTrans`=d.`NoTrans`
										WHERE h.`TglAkhir`>='$tgl' AND h.`TglAwal`<='$tgl' AND h.`Status`=1
									) ds on d.pcode=ds.PCode
									WHERE h.tanggal='$tgl' AND h.status=0 AND h.`KdMeja`='$kdmeja' and d.status=1
									GROUP BY d.`PCode`";
									$qry = $this->db->query($sql);
									$row = $qry->result_array();
									return $row;
								}

								function geHeaderOrder($kdmeja){
									$tgl = date('Y-m-d');
									$sql = "SELECT NoTrans FROM trans_order_header h
									WHERE h.tanggal='$tgl' AND h.status=0 AND h.`KdMeja`='$kdmeja'";
									$qry = $this->db->query($sql);
									$row = $qry->result_array();
									return $row;
								}

								function updateorder($orderid, $nostruk){
									$vorderid = explode("*",$orderid);

									for($i=0;$i<count($vorderid);$i++){
										$noorder = $vorderid[$i];
										$sql = "Update trans_order_header set NoStruk='$nostruk', `status`= 1 where NoTrans='$noorder'";
										$qry = $this->db->query($sql);
									}
								}

								function updatepointmember($kdmember, $nettosales, $bayarpoint){
									$bayarpoint = $bayarpoint*1;
									$point = floor($nettosales /10000);
									$point -= $bayarpoint;
									$sql = "Update member set JumlahPoint = JumlahPoint+'$point' where KdMember='$kdmember' ";
									$qry = $this->db->query($sql);
								}

								function getOpenOrder($tgl, $kdmeja){
									$sql = "select NoTrans, KdAgent, TotalGuest from trans_order_header where tanggal='$tgl' and kdmeja='$kdmeja' and status=0 limit 1";
									$qry = $this->db->query($sql);
									$row = $qry->result_array();
									if(count($row)>0)
									return $row[0];
									else
									return array('NoTrans'=>'','KdAgen'=>'','TotalGuest'=>'');
								}

								function getgroupdisc($namacard, $store, $pcode){
									$sql1 = "SELECT KdGroupBarang FROM masterbarang WHERE pcode='$pcode'";
									$qry = $this->db->query($sql1);
									$row = $qry->result_array();
									$kdgroupbarang = $row[0]['KdGroupBarang'];

									$sql2 = "SELECT d.`PersenDisc`, h.MaxDiscount FROM group_disc_header h INNER JOIN group_disc_detail d ON h.`KdGroupDisc`=d.`KdGroupDisc`
									WHERE d.`KdGroupDisc`='$namacard' AND d.`KdStore`='$store' AND d.`KdGroupBarang`='$kdgroupbarang'
									AND h.BerlakuMulai <= DATE(NOW()) AND h.BerlakuSampai >= DATE(NOW())";

									$qry = $this->db->query($sql2);
									$row = $qry->result_array();
									if(empty($row)){
										return 0;
									}else{
										return $row[0]['PersenDisc']."#".$row[0]['MaxDiscount'];
									}

								}

							}
