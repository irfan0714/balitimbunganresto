<?php

class cek_model extends CI_Model {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function getDetailForPrint($id) {
        $sql = "
                SELECT d.*,NamaLengkap FROM( 
                SELECT NoTransaksi,PCode, Qty, Harga
                FROM finance_komisi_detail WHERE NoTransaksi='$id' ORDER BY PCode
                ) d 
                INNER JOIN masterbarang
                ON masterbarang.`PCode`=d.PCode
		";
//echo $sql;
        return $this->getArrayResult($sql);
    }

    function getHeaderForPrint($id) {
        $sql = "
              SELECT 
                a.NoTransaksi, DATE_FORMAT(a.TglTransaksi, '%d-%m-%Y') AS TglTransaksi, a.KdAgent, a.Total AS TotKomisi, b.TotalNilai AS TotSales, 
                MIN(c.`Waktu`) AS mulai, MAX(c.waktu) AS akhir 
              FROM
                finance_komisi_header a
              INNER JOIN transaksi_header b
              ON a.`KdAgent` = b.`KdAgent`
              INNER JOIN transaksi_detail c
              ON b.`NoStruk` = c.`NoStruk`
              AND a.NoTransaksi = '$id'
              GROUP BY `KdAgent` 
		";
//echo $sql;
         return $this->getRow($sql);
    }

    function getCountDetail($id) {
        $sql = "SELECT * FROM finance_komisi_detail where NoTransaksi='$id'";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getKomisiList($num, $offset, $id, $with) {
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }
        $clause = "";
        if ($id != "") {
            if ($with == "NoTransaksi") {
                $clause = "WHERE $with like '%$id%'";
            } else {
                $clause = "WHERE $with = '$id'";
            }
        }
        $sql = "SELECT NoTransaksi, date_format(TglTransaksi,'%d-%m-%Y') AS TglTransaksi, KdAgent, Nama, Keterangan, Total
                    FROM finance_komisi_header $clause
                    ORDER BY NoTransaksi DESC limit $offset,$num";
//WHERE $clause       
// echo $sql;
        return $this->getArrayResult($sql);
    }

    function getHeader($id) {
        $sql = "SELECT NoTransaksi, date_format(TglTransaksi,'%d-%m-%Y') AS TglTransaksi, KdAgent, Nama, Keterangan, Total
                    FROM finance_komisi_header
                    WHERE NoTransaksi='$id'
                    ORDER BY NoTransaksi DESC";
            //echo $sql;
        return $this->getRow($sql);
    }

    function getDetail($id) {
        $sql = "SELECT a.NoTransaksi, b.PCode, c.NamaLengkap, b.Qty, b.Harga
                    FROM finance_komisi_header a, finance_komisi_detail b, masterbarang c
                    WHERE a.NoTransaksi = b.NoTransaksi AND b.PCode = c.PCode
                                              AND a.NoTransaksi='$id' ORDER BY NoTransaksi DESC, b.AddDate Desc";
        return $this->getArrayResult($sql);
    }

    function num_komisi_row($id, $with) {
        $clause = "";
        if ($id != '') {
            if ($with == "NoTransaksi") {
                $clause = "WHERE $with like '%$id%'";
            } else {
                $clause = "WHERE $with = '$id'";
            }
        }
        $sql = "SELECT NoTransaksi FROM finance_komisi_header $clause";
        return $this->NumResult($sql);
    }

    function getKomisi($NoAgent) {
        $sql = "
                SELECT 
  a.NoStruk,
  a.Tanggal AS TglJual,
  a.Waktu,
  a.KdAgent,
  b.PCode,
  c.NamaLengkap,
  b.Qty,
  b.Harga,
  IFNULL(b.Komisi, 0) AS Komisi,
  IFNULL(
    ROUND(b.Komisi * b.Harga / 100),
    0
  ) AS Nilai 
FROM
  transaksi_header a 
  INNER JOIN transaksi_detail b 
    ON a.`NoStruk` = b.`NoStruk` 
  INNER JOIN masterbarang c 
    ON b.`PCode` = c.`PCode` 
 $NoAgent
ORDER BY a.NoStruk DESC ";
        $qry = $this->db->query($sql); //echo $sql;die();
        $row = $qry->result_array();

        return $row;
    }

    function getNewNo($tahun,$bulan) {
        $sql = "SELECT NoKomisi FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
        return $this->getRow($sql);
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function NamaPrinter($id) {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
        return $this->getRow($sql);
    }

    function getTotalNetto($no) {
        $sql = "SELECT SUM(Netto) FROM transaksi_detail WHERE NoStruk ='$no'";
        return $this->getArrayResult($sql);
    }

    function getWaktu($no) {
        $sql = "SELECT MIN(waktu) AS mulai, MAX(waktu) AS akhir FROM transaksi_detail WHERE NoStruk = '$no'";
        return $this->getArrayResult($sql);
    }

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function ifPCodeBarcode($id) {
        $bar = substr($id, 0, 10);
        $sql = "SELECT KdRekening FROM rekening Where KdRekening='$id'";
        return $this->getRow($sql);
    }

    function getPCode($kode) {
        $sql = "select PCode, NamaLengkap,Barcode1,Harga1c from masterbarang where Barcode1='$kode'";
        return $this->getRow($sql);
    }

}

?>
