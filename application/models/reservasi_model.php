<?php

class Reservasi_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }

    function getReservasiList($limit, $offset, $arrSearch) {
        $mylib = new globallib();

        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }

        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "trans_reservasi.NoDokumen";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }
        }

        $sql = "  
            SELECT 
              *
            FROM
              trans_reservasi
            WHERE 
            	1  
            	" . $where_keyword . "                                    
            ORDER BY 
              CAST(NoDokumen AS UNSIGNED) DESC,
              trans_reservasi.NoDokumen DESC 
            Limit 
              $offset,$limit
        ";
        /* echo $sql;
          echo "<hr/>"; */
        return $this->getArrayResult($sql);
    }

    function num_reservasi_row($arrSearch) {
        $mylib = new globallib();

        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "trans_reservasi.NoDokumen";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }
        }

        $sql = "
            SELECT 
              *
			FROM
			trans_reservasi
            WHERE 
            	1
            	" . $where_keyword . "
		";

        return $this->NumResult($sql);
    }

    function getSearch($id, $module, $user) {
        $sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
        return $this->getRow($sql);
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

    function getDivisi() {
        $sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
        return $this->getArrayResult($sql);
    }

    function getGudang() {
        $sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
        return $this->getArrayResult($sql);
    }

    function getSatuan() {
        $sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
        return $this->getArrayResult($sql);
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function getDetail($id) {
        $sql = "
			SELECT 
			  permintaan_barang_detail.* 
			FROM
			  permintaan_barang_detail 
			WHERE 1 
			  AND permintaan_barang_detail.NoDokumen = '$id' 
			ORDER BY 
			  permintaan_barang_detail.sid DESC
		";

        return $this->getArrayResult($sql);
    }

    function getHeader($id) {
        $sql = "
			SELECT * FROM `trans_reservasi` WHERE NoDokumen='$id'";
        return $this->getArrayResult($sql);
        //return $this->getRow($sql);
    }

    function getHeaderForPrint($id) {
        $sql = "
			SELECT * FROM `trans_reservasi_detail` WHERE NoDokumen='$id'";
        return $this->getArrayResult($sql);
        //return $this->getRow($sql);
    }

    function getTravel($id) {
        $sql = "
			SELECT * FROM `tourtravel` WHERE KdTravel='$id'";
        //return $this->getArrayResult($sql);
        return $this->getRow($sql);
    }

    function getDetailForPrint($id) {
        $sql = "SELECT `trans_reservasi`.*,d.PCode,d.NamaBarang,d.Qty,d.Harga FROM trans_reservasi INNER JOIN `trans_reservasi_detail` d
                ON d.`NoDokumen` = trans_reservasi.`NoDokumen` AND trans_reservasi.`NoDokumen` = '$id'";
        //echo $sql;
        return $this->getArrayResult($sql);
        //return $this->getRow($sql);
    }

    public function get_by_id($id) {
        $sql = "SELECT NoDokumen,dp,Total FROM `trans_reservasi` WHERE NoDokumen='$id'";
        //echo $sql;
        return $this->getRow($sql);
    }

    function cekNodok($id) {
        $sql = "
			SELECT 
  			  trans_pr_header.NoDokumen,
			  trans_pr_header.NoPermintaan, 
			  DATE_FORMAT(trans_pr_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_pr_header.AddUser 
			FROM
			  trans_pr_header 
			WHERE 1 
			  AND trans_pr_header.NoPermintaan = '" . $id . "'
		";

        return $this->getRow($sql);
    }

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>