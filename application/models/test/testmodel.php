<?php
class TestModel extends CI_Model {
	
    function __construct() {
        parent::__construct();
    }
 
    function viewData($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT PCode, NamaLengkap FROM masterbarang ";
		if($key!=='') {
			$pattern = '/%/';
			$ispersen = preg_match($pattern, $key);
			if($ispersen) {
				$sql .=" WHERE $category LIKE '$key'";
			} else {
				$sql .=" WHERE $category LIKE '%$key%'";
			}
		}
		$sql .=" ORDER BY NamaLengkap LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;       
    }
    
    function getNumRowsDataView($key,$category) {
		$sql = "SELECT PCode FROM masterbarang ";

		if($key!=='') {
			$pattern = '/%/';
			$ispersen = preg_match($pattern, $key);
			if($ispersen) {
				$sql .=" WHERE $category LIKE '$key'";
			} else {
				$sql .=" WHERE $category LIKE '%$key%'";
			}
		}
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
    }    
}