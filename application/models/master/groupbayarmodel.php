<?php
class Groupbayarmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getGroupbayarList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "SELECT KdGroupbayar, NamaGroupbayar FROM groupbayar $clause order by KdGroupbayar Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_groupbayar_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdGroupbayar FROM groupbayar $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT KdGroupbayar,NamaGroupbayar from groupbayar Where KdGroupbayar='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdGroupbayar FROM groupbayar Where KdGroupbayar='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		/* $sql = "SELECT KdGroupbayar FROM subgroupbayar Where KdGroupbayar='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num; */
		return 0;
	}
}
?>