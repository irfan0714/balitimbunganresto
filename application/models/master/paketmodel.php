<?php

class Paketmodel extends CI_Model {
var $tabel = 'subdivisi';
    function __construct() {
        parent::__construct();
    }

    function getbarangList() {
        $sql = "
                SELECT id,MPCode,nl,bMaster,DPcode,nmdet,bDetail FROM
                (SELECT id,MPCode,DPcode FROM `barang_paket`) bp
                INNER JOIN
                (SELECT PCode,BarCode1 as bMaster,NamaLengkap AS nl FROM masterbarang)mb ON mb.PCode= bp.MPCode
                INNER JOIN
                (SELECT PCode,Barcode1 as bDetail,NamaLengkap AS nmdet FROM masterbarang)mbd ON mbd.PCode= bp.DPCode
			 ORDER BY MPCode,DPcode";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function getPaket($term) {
        $sql = "SELECT CONCAT(PCode,' ',NamaLengkap) AS PCode, PCode AS PCode1, NamaLengkap, Barcode1 FROM masterbarang WHERE SatuanSt='PKT' AND (PCode LIKE '$term%' OR NamaLengkap LIKE '$term%') order by NamaLengkap";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
		return $row;
    }

    function getBarang($type,$name) {
        $sql = "SELECT PCode, NamaLengkap, Barcode1 FROM masterbarang WHERE ($type LIKE '$name%' OR $type LIKE '$name%') LIMIT 0,20";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function getHeader($id) {
        $sql = "SELECT id,MPCode,nl,bMaster,DPcode,nmdet,bDetail,Qty FROM
                (SELECT id,MPCode,DPcode,Qty FROM `barang_paket`) bp
                INNER JOIN
                (SELECT PCode,BarCode1 as bMaster,NamaLengkap AS nl FROM masterbarang)mb ON mb.PCode= bp.MPCode
                INNER JOIN
                (SELECT PCode,Barcode1 as bDetail,NamaLengkap AS nmdet FROM masterbarang)mbd ON mbd.PCode= bp.DPCode
			 WHERE bp.MPCode='$id'";
        $query = $this->db->query($sql);
        return $query;
    }
    
    function cekPCode($mpcode,$pcode){

    	$sql = "SELECT * FROM `barang_paket` a WHERE a.`MPCode`='".$mpcode."' AND a.`DPcode`='".$pcode."';";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;

    }
    
    function get_id($id) {
        $sql = "SELECT MPCode FROM barang_paket Where MPCode='$id'";
        $query = $this->db->query($sql);
        return $query;
//        $num = $query->num_rows();
//        $query->free_result();
//        return $num;
    }

}

?>