<?php
class Kendaraanmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_kendaraan_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "
			select k.KdKendaraan,NamaKendaraan,NoPolisi,NamaPersonal
			from
			(
			select * from kendaraan $clause order by KdKendaraan Limit $offset,$num
			) as k
			left join
			(
			select KdPersonal,NamaPersonal from personal
			) as personal
			on k.KdPersonal = personal.KdPersonal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_kendaraan_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdKendaraan FROM kendaraan $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    function getMaster(){
    	$sql = "SELECT * from personal order by KdPersonal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function getDetail($id){
    	$sql = "SELECT * from kendaraan Where KdKendaraan='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdKendaraan FROM kendaraan Where KdKendaraan='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdKendaraan from trans_kirim_header Where KdKendaraan='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>