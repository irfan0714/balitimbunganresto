<?php

class Matauangmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getMatauangList($num, $offset, $id, $with) {
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }
        $clause = "";
        if ($id != "") {
            $clause = " where $with like '%$id%'";
        }
        $sql = "SELECT Kd_Uang, Keterangan,NilaiTukar,AddDate,EditDate,FlagAktif FROM mata_uang $clause order by EditDate DESC Limit $offset,$num";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_matauang_row($id, $with) {
        $clause = "";
        if ($id != '') {
            if ($with == "Kd_Uang") {
                $clause = "WHERE $with like '%$id%'";
            } elseif ($with == "Keterangan") {
                $clause = "WHERE $with like '%$id%'";
            }
        }
        $sql = "SELECT Kd_Uang FROM mata_uang $clause";
        return $this->NumResult($sql);

//        $clause = "";
//        if ($id != '') {
//            $clause = " where $with like '%$id%'";
//        }
//        $sql = "SELECT Kd_Uang FROM mata_uang $clause";
//        echo $sql; 
//        $qry = $this->db->query($sql);
//        $num = $qry->num_rows();
//        $qry->free_result();
//        return $num;
    }

    function getDetail($id) {
        $sql = "SELECT Kd_Uang,Keterangan,NilaiTukar,FlagAktif FROM mata_uang Where Kd_Uang='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function get_id($id) {
        $sql = "SELECT Kd_Uang FROM mata_uang Where Kd_Uang='$id'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        $query->free_result();
        return $num;
    }

    function get_flag($id) {
//        $sql = "SELECT Kd_Uang,FlagAktif FROM mata_uang";
//        $qry = $this->db->query($sql);
//        $row = $qry->result_array();
//        $qry->free_result();
//        return $row;
        $query = $this->db->query("SELECT FlagAktif FROM mata_uang WHERE Kd_Uang ='$id'");
        return $query->result();
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>