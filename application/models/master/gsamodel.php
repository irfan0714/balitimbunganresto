<?php
class Gsamodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_gsa_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
		$sql="SELECT * FROM gsa_list LEFT JOIN warna ON gsa_list.warna = warna.id_warna $clause";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getdata($id){
    	$sql="SELECT gsa_list.KdGsa,gsa_list.employee_id, gsa_list.nama,gsa_list.status, gsa_list.warna AS wrn,  warna.warna FROM gsa_list LEFT JOIN warna ON gsa_list.warna = warna.id_warna where gsa_list.KdGsa='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    function num_gsa_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT * FROM gsa_list $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getSupervisor(){
    	$sql = "
    				SELECT 
					  b.`employee_id`,a.`employee_name`
					FROM
					  employee a 
					  INNER JOIN employee_position b 
					    ON a.`employee_id` = b.`employee_id` 
					  WHERE b.`jabatan_id`='30';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getEmployee($employee){
    	$sql = "
    				SELECT 
					  b.`employee_id`,a.`employee_name`
					FROM
					  employee a 
					  INNER JOIN employee_position b 
					    ON a.`employee_id` = b.`employee_id` 
					  WHERE a.`employee_id`='$employee';
    			";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getWarna(){
    	$sql = "
    				SELECT * FROM warna;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
}
?>