<?php
class Membermodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_member_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "select head.KdMember,NamaMember,NamaTipeMember,JoinDate,LastTrans,TotalTrans from(
					SELECT KdMember, NamaMember, KdTipeMember, AddDate as JoinDate 
					FROM member $clause order by KdMember  Limit $offset,$num
				)as head
				left join
				(
				  select KdTipeMember, NamaTipeMember from tipe_member
				)as tipe
				on tipe.KdTipeMember = head.KdTipeMember
				left join
				(
				  select kdmember, max(tanggal) as LastTrans, sum(NettoRupiah-PPnRupiah) as TotalTrans from trans_sales_header
				  where status='0' and NOT ISNULL(kdmember) group by kdmember
				)as member
				on member.kdmember=head.kdmember";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_member_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdMember FROM member $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getTipe(){
    	$sql = "SELECT KdTipeMember, NamaTipeMember from tipe_member order by KdTipeMember";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT KdMember,NamaMember,NickName,Alamat1,Alamat2,Kota,KotaLahir,(select date_format(TglLahir,'%d-%m-%Y')) as TglLahir,Telp,Email,Twitter,KdTipeMember from member Where KdMember='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdMember FROM member Where KdMember='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdMember FROM trans_sales_header Where KdMember='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	
}
?>