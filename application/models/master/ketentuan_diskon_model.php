<?php
class Ketentuan_diskon_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
	
    function getDiskonList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "Ketentuan";
		        $arr_keyword[1] = "NoTrans";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "SELECT 
				  * FROM discountheader WHERE 1 $where_keyword 
				LIMIT $offset,$limit";
        
		return $this->getArrayResult($sql);
    }
    
    function getByKategori(){
                	/*$sql="SELECT NamaLengkap 
						FROM(
							SELECT '1' AS posisi, Kdkategori AS kode, Namakategori AS NamaLengkap FROM kategori WHERE 1 AND Kdkategori IN('01','02','04','05','06','07','09','11')
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,14) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'SCENTED%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,13) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'BATH & SHOWER%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,9) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'BATH BOMB%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,14) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'BEAUTY%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,16) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'BODY EX%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,11) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'BODY BUT%'	
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,10) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'FANCY%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,13) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'FRAGRANCE%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,4) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'JAMU%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,4) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'KOPI%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,5) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'PAKET%'
							UNION
							SELECT '2' AS posisi, '' AS kode, LEFT(NamaLengkap,11) AS NamaLengkap FROM masterbarang_pos WHERE NamaLengkap LIKE 'SOAP%'
							) AS a
						GROUP BY NamaLengkap
						ORDER BY posisi,kode, NamaLengkap";*/
					$sql = "SELECT * FROM kategori a ORDER BY a.`NamaKategori` ASC";
		return $this->getArrayResult($sql);
	}
	
	function getByDivisi(){
    	$sql="SELECT * FROM `divisi` WHERE 1 ORDER BY divisi.`KdDivisi` ASC;";
		return $this->getArrayResult($sql);
	}	
	
	function getNoDisc($notrans_awal){
    	$sql="SELECT NoTrans 
			  FROM discountheader 
			  WHERE NoTrans LIKE '$notrans_awal%' 
			  ORDER BY NoTrans DESC
			  LIMIT 1";
		
		return $this->getRow($sql);
	}
	
	function insertHeaderDiscount($notrans_new,$tgltransaksi,$v_note ,$v_tgl_periode1,$v_tgl_periode2,$minimum,$Status,$excludepromo,$v_pil_input,$v_pil_by, $v_pil_by_kategori,$v_pil_by_divisi,$user)
	{
		$data  =  array('NoTrans'		=>$notrans_new,
				        'TglTrans'		=>$tgltransaksi,
				        'Ketentuan'		=>$v_note,
				        'TglAwal'		=>$v_tgl_periode1,
				        'TglAkhir'		=>$v_tgl_periode2,
				        'Minimum'		=>$minimum,
				        'Status'		=>$Status,
				        'exclude_promo'	=>$excludepromo,
				        'tipe_input'	=>$v_pil_input,
				        'tipe_by'		=>$v_pil_by,
				        'tipe_kategori'	=>$v_pil_by_kategori,
				        'tipe_divisi'	=>$v_pil_by_divisi,
				        'adduser'		=>$user,
				        'adddate'		=>date('Y-m-d'));
		$this->db->insert('discountheader',$data);
	}
	
	function updateHeaderDiscount($notrans_new,$tgltransaksi,$v_note ,$v_tgl_periode1,$v_tgl_periode2,$minimum,$Status,$excludepromo,$v_pil_input,$v_pil_by, $v_pil_by_kategori,$v_pil_by_divisi,$user)
	{
		$data  =  array(
				        'TglTrans'		=>$tgltransaksi,
				        'Ketentuan'		=>$v_note,
				        'TglAwal'		=>$v_tgl_periode1,
				        'TglAkhir'		=>$v_tgl_periode2,
				        'Minimum'		=>$minimum,
				        'Status'		=>$Status,
				        'exclude_promo'	=>$excludepromo,
				        'tipe_input'	=>$v_pil_input,
				        'tipe_by'		=>$v_pil_by,
				        'tipe_kategori'	=>$v_pil_by_kategori,
				        'tipe_divisi'	=>$v_pil_by_divisi,
				        'edituser'		=>$user,
				        'editdate'		=>date('Y-m-d'));
		$this->db->update('discountheader',$data, array('NoTrans'=>$notrans_new));
	}
	
	function insertDetailDiscount($notrans_new,$pcode,$tanda,$v_diskon)
	{
		$data  =  array('NoTrans'		=>$notrans_new,
				        'PCode'			=>$pcode,
				        'Jenis'			=>$tanda,
				        'Nilai'			=>$v_diskon);
		$this->db->insert('discountdetail',$data);
	}
		
	
	function insertDetailBykategoriIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_kategori,$v_tgl_periode1,$v_tgl_periode2)
	{
		$qd_insert = "INSERT INTO discountdetail 
						SELECT '$notrans_new', masterbarang_pos.PCode, '$tanda', '$v_diskon' 
						FROM
						masterbarang_pos INNER JOIN masterbarang ON
						masterbarang_pos.`PCode`= masterbarang.`PCode` 
						WHERE masterbarang_pos.`NamaLengkap` LIKE '%".$v_pil_by_kategori."%' AND  masterbarang_pos.`PCode` NOT IN 
					  (SELECT 
						`discountdetail`.`PCode` 
						 FROM
						 `discountdetail` 
						  INNER JOIN `discountheader` 
							ON `discountdetail`.`NoTrans` = `discountheader`.`NoTrans` 
						WHERE `discountheader`.`TglAwal` <= '".$v_tgl_periode1."' 
						AND `discountheader`.`TglAkhir` >= '".$v_tgl_periode2."')
						";
		
		$this->db->query($qd_insert);
	}
	
	function insertDetailByDivisiIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_divisi,$v_tgl_periode1,$v_tgl_periode2)
	{
	$qd_insert = "INSERT INTO discountdetail 
								  SELECT '$notrans_new', masterbarang_pos.PCode, '$tanda', '$v_diskon' 
								  FROM
								  masterbarang_pos INNER JOIN masterbarang ON
								  masterbarang_pos.`PCode`= masterbarang.`PCode` 
								WHERE masterbarang.`KdDivisi`='".$v_pil_by_divisi."' AND  masterbarang_pos.`PCode` NOT IN 
								  (SELECT 
								    `discountdetail`.`PCode` 
								  FROM
								    `discountdetail` 
								    INNER JOIN `discountheader` 
								      ON `discountdetail`.`NoTrans` = `discountheader`.`NoTrans` 
								  WHERE `discountheader`.`TglAwal` <= '".$v_tgl_periode1."' 
								    AND `discountheader`.`TglAkhir` >= '".$v_tgl_periode2."')
    								";
    	$this->db->query($qd_insert);
	}
	
	function insertDetailBykategoriTanpaIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_kategori)
	{
	    $qd_insert = "INSERT INTO discountdetail 
				      SELECT '$notrans_new', PCode, '$tanda', '$v_diskon' FROM masterbarang_pos WHERE KdKategori LIKE '$v_pil_by_kategori%'";
		$this->db->query($qd_insert);
	}
	
	function insertDetailByDivisiTanpaIncludepromo($notrans_new,$tanda,$v_diskon,$v_pil_by_divisi)
	{
	    $qd_insert = "INSERT INTO discountdetail 
					  SELECT '$notrans_new', masterbarang_pos.PCode, '$tanda', '$v_diskon' FROM masterbarang_pos INNER JOIN `masterbarang` ON `masterbarang_pos`.`PCode`=`masterbarang`.`PCode` WHERE `masterbarang`.`KdDivisi`='".$v_pil_by_divisi."';";
		$this->db->query($qd_insert);
	}
	
	function getTabelHeaderList($notrans){
    	$sql="SELECT *
			  FROM discountheader 
			  WHERE NoTrans = '$notrans' ";
		
		return $this->getRow($sql);
	}
	
	function num_ketentuan_diskon_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `discountheader` a;       
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getTabelDetailList($notrans){
    	$sql="SELECT *
			  FROM discountdetail INNER JOIN masterbarang ON discountdetail.Pcode = masterbarang.PCode
			  WHERE discountdetail.NoTrans = '$notrans' ";
		
		return $this->getArrayResult($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>