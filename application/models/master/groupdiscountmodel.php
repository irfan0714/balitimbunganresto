<?php
class Groupdiscountmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_groupdiscount_List($num, $offset,$id,$with)
	{
	    $db2	= $this->load->database('gl',true);
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
		$sql = "select KdGroupDiscount,NamaGroupDiscount,Ketentuan01,Ketentuan02,Ketentuan03,Ketentuan04,Ketentuan05,Ketentuan06,Ketentuan07
		from groupdiscount $clause order by KdGroupDiscount Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_groupdiscount_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdGroupDiscount FROM groupdiscount $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getTipe(){
    	$sql = "SELECT NamaDisc from discount_header order by NamaDisc";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT KdGroupDiscount,NamaGroupDiscount,Ketentuan01,Ketentuan02,Ketentuan03,Ketentuan04,Ketentuan05,Ketentuan06,Ketentuan07 from groupdiscount Where KdGroupDiscount='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdGroupDiscount FROM groupdiscount Where KdGroupDiscount='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdGroupDiscount FROM groupdiscount Where KdGroupDiscount='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	
}
?>