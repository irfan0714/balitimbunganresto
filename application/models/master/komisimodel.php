<?php
class KomisiModel extends CI_Model {
	
    function __construct() {
        parent::__construct();
    }
 
    function viewData($num, $offset, $key, $category) {
	 	if($offset !='')
            $offset = $offset.',';
        
        $sql = "SELECT KdTravel, Keterangan, Komisi1, Komisi2, Komisi3, Komisi4 FROM komisi_header ";
		if($key!=='') {
			$pattern = '/%/';
			$ispersen = preg_match($pattern, $key);
			if($ispersen) {
				$sql .=" WHERE $category LIKE '$key'";
			} else {
				$sql .=" WHERE $category LIKE '%$key%'";
			}
		}
		$sql .=" ORDER BY Keterangan LIMIT $offset $num";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;       
    }
    
    function getNumRowsDataView($key,$category) {
		$sql = "SELECT KdTravel FROM komisi_header ";

		if($key!=='') {
			$pattern = '/%/';
			$ispersen = preg_match($pattern, $key);
			if($ispersen) {
				$sql .=" WHERE $category LIKE '$key'";
			} else {
				$sql .=" WHERE $category LIKE '%$key%'";
			}
		}
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        return $num;
    }   
		
	function getMasterBarang() {
		$sql = "SELECT PCode, NamaLengkap FROM masterbarang ORDER BY NamaLengkap";
        $query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}
	
	function getMasterTravel() {
		$sql = "SELECT KdTravel, CONCAT(KdTravel,' - ', Nama) AS Nama  FROM tourtravel ORDER BY Nama";
        $query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}
	
	function getKomisiHeader($id) {
		$sql = "SELECT KdTravel, Keterangan, Komisi1, Komisi2, Komisi3, Komisi4 "
			. "FROM komisi_header "
			. "WHERE KdTravel='$id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
		return $result[0];
	}
	
	function getKomisiDetail($id) {
		$sql = "SELECT k.KdTravel, b.NamaLengkap, k.PCode, "
			. "FORMAT(k.Komisi1,0) AS Komisi1, FORMAT(k.Komisi2,0) AS Komisi2, FORMAT(k.Komisi3,0) AS Komisi3, "
			. "FORMAT(k.Komisi4,0) AS Komisi4, FORMAT(k.Komisi4bag,0) AS Komisi4bag, FORMAT(k.Total,0) AS Total "
			. "FROM komisi_detail k, masterbarang b "
			. "WHERE k.PCode=b.PCode AND KdTravel='$id'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}
}