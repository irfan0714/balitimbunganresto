<?php
class Cndnmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_cndn_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "select KdCNDN,NamaCNDN,NamaRekening from(
				  SELECT KdCNDN,NamaCNDN,KdRekening
				  FROM cndn $clause order by KdCNDN Limit $offset,$num
				) as head
				left join
				(
				  select KdRekening, NamaRekening from rekening
				) as tipe
				on tipe.KdRekening = head.KdRekening";
		$sql = "select KdCNDN,NamaCNDN,KdRekening from cndn $clause order by KdCNDN Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_cndn_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdCNDN FROM cndn $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getTipe(){
    	$sql = "SELECT KdRekening, NamaRekening from rekening where tingkat='3' order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT KdCNDN, NamaCNDN, KdRekening from cndn Where KdCNDN='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdCNDN FROM cndn Where KdCNDN='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdCNDN FROM cndn Where KdCNDN='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	
}
?>