<?php
class Barangmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }

    function getbarangList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "
		    select b.PCode,NamaLengkap,NamaStruk,
			NamaDivisi,NamaKategori,NamaBrand,NamaSatuan,Satuan1,Harga1c,Harga1b,PersenPajak,Tipe,Status
			from
			(
			select * from masterbarang $clause order by PCode Limit $offset,$num
			) as b
			left join
			(
			select KdDivisi,NamaDivisi from divisi
			)as divisi
			on divisi.KdDivisi = b.KdDivisi
			left join
			(
			select KdKategori,NamaKategori from kategori
			)as kategori
			on kategori.KdKategori = b.KdKategori
			left join
			(
			select KdBrand,NamaBrand from brand
			)as brand
			on brand.KdBrand = b.KdBrand
			left join
			(
			select KdSatuan,NamaSatuan from satuan
			)as satuan
			on satuan.KdSatuan = b.Satuan1
			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_barang_row($id,$with)
    {
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT PCode FROM masterbarang $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

    function getDivisi()
    {
    	$sql = "select KdDivisi,NamaDivisi from divisi order by KdDivisi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function getSubDivBy($divisi="")
    {
    	$clause="";
    	if($divisi)
    	{
			$clause="where KdDivisi='".$divisi."'";	
		}
		
		$sql = "select KdSubDivisi,NamaSubDivisi from subdivisi ".$clause." order by KdSubDivisi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

    function getKategori()
    {
		$sql = "select KdKategori,NamaKategori from kategori order by KdKategori";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getSubKatBy($kategori="")
    {
    	$clause="";
    	if($kategori)
    	{
			$clause="where KdKategori='".$kategori."'";	
		}
		
		$sql = "select KdSubKategori,NamaSubKategori from subkategori ".$clause." order by KdSubKategori";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getBrand()
    {
		$sql = "select KdBrand,NamaBrand from brand order by KdBrand";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getSubBrandBy($brand="")
    {
    	$clause="";
    	if($brand)
    	{
			$clause="where KdBrand='".$brand."'";	
		}
		
		$sql = "select KdSubBrand,NamaSubBrand from subbrand ".$clause." order by KdSubBrand";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        
        return $row;
	}

	function getSize()
    {
		$sql = "select KdSize,NamaSize from size order by KdSize";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getSubSizeBy($size)
    {
		$sql = "select KdSubSize,Ukuran from subsize where KdSize='$size' order by KdSubSize";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getKelas()
	{
		$sql = "select KdKelas,NamaKelas from kelasproduk order by KdKelas";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getTipeStock()
	{
    	$sql = "SELECT KdTipeStock, NamaTipeStock from tipe_stock order by KdTipeStock";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getStatus()
	{
    	$sql = "SELECT KdStatus, NamaStatus from status order by KdStatus";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getFormula()
	{
    	$sql = "SELECT KdTipeStock, NamaTipeStock from tipe_stock order by KdTipeStock";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

	function getTag()
	{
		$sql = "select KdProductTag,Keterangan from product_tag order by KdProductTag";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getKemasan()
	{
		$sql = "select KdKemasan,NamaKemasan from kemasan order by KdKemasan";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function getSatuan()
	{
		$sql = "select KdSatuan,NamaSatuan from satuan order by NamaSatuan";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

    function getKomisi()
    {
        $sql = "SELECT Kd_Komisi,Nama_Komisi FROM group_komisi order by Kd_Komisi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

	function getNamaSatuan($kode)
	{
		$sql = "select keterangan from satuan where NamaSatuan='$kode'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getParent()
	{
		$sql = "select PCode,NamaLengkap from masterbarang order by PCode";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $row2= array("PCode"=>"","NamaLengkap"=>"No Parent");
        array_unshift($row,$row2);
        $qry->free_result();
        return $row;
	}

	function getPicking()
	{
		$sql = "select KdPicking,Keterangan from picking_method order by KdPicking";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();        
        $qry->free_result();
        return $row;
	}

    function getHeader($id)
    {
    	$sql = "SELECT * from masterbarang Where PCode='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
	function getDetail($id)
	{
    	$sql = "SELECT * from masterbarang_detail Where PCode='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function get_id($id)
    {
		$sql = "SELECT PCode FROM masterbarang Where PCode='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}

	function getidcounter($id)
	{
	    $sql = "Update brand set Counter=Counter+1 where KdBrand='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM brand Where KdBrand='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function CekDelete($id)
	{
		$sql = "SELECT distinct PCode FROM trans_sales_detail Where PCode='$id'
				union
				SELECT distinct PCode FROM trans_order_detail Where PCode='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
}
?>