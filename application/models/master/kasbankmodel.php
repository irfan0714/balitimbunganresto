<?php
class Kasbankmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_kasbank_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
		$sql = "select KdKasBank,NamaKasBank,KdRekening,Jenis,if(Jenis='1','Kas/Bank',if(Jenis='2','Giro','Kosong')) as NamaJenis from kasbank $clause order by KdKasBank Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_kasbank_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdKasBank FROM kasbank $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getTipe(){
    	$sql = "SELECT KdRekening, NamaRekening from rekening where tingkat='3' order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT KdKasBank, NamaKasBank, KdRekening, Jenis, if(Jenis='1','Kas/Bank',if(Jenis='2','Giro','Kosong')) as NamaJenis from kasbank Where KdKasBank='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdKasBank FROM kasbank Where KdKasBank='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdKasBank FROM kasbank Where KdKasBank='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	
}
?>