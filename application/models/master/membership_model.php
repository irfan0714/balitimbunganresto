<?php
class Membership_model extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function gettypetourList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "
              SELECT member.*,type_member.`NamaTypeMember` FROM member INNER JOIN type_member ON member.`KdTipeMember`=type_member.`KdTipeMember` $clause ORDER BY sid DESC LIMIT $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    function getType() {
        $sql = "SELECT * FROM type_member";
        return $this->getArrayResult($sql);
    }
    
    function num_typetour_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "
            SELECT member.*,type_member.`NamaTypeMember` FROM member INNER JOIN type_member ON member.`KdTipeMember`=type_member.`KdTipeMember` $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT *
                FROM member Where KdMember='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdTravel,Nama,Contact,Phone,Fax,Alamat,Email,Kota,Telepon,Aktif,ADDDATE,EditDate,KdTypeTour
                FROM tourtravel Where KdTravel='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    function getidcounter($id)
    {
        $sql = "SELECT KdTravel FROM travellist Where KdTravel='$id'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        IF ($num == 0) {
            $sql = "insert into travellist (KdTravel, Counter) values('$id','0')";
            $qry = $this->db->query($sql);
        }
        $sql = "Update travellist set Counter=Counter+1 where KdTravel='$id'";
        $qry = $this->db->query($sql);
        $sql = "SELECT Counter FROM travellist Where KdTravel='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
}
?>