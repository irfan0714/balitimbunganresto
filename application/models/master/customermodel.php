<?php
class Customermodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getcustomerList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "SELECT KdCustomer,Nama,Alamat,Kota,Telepon,IF(Payment='C','Cash','Kredit') as Payment,TOP,NamaTypecust,PPn FROM customer c,typecust ts where ts.KdTypecust = c.KdTypecust $clause order by KdCustomer Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_customer_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdCustomer FROM customer c $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT *,date_format(TglPkp,'%d-%m-%Y') as Tanggal from customer Where KdCustomer='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdCustomer FROM customer Where KdCustomer='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function getTipeCustomer()
	{
		$sql = "SELECT KdTypecust,NamaTypecust FROM typecust order by KdTypecust";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function getGroupCustomer()
	{
		$sql = "SELECT KdGroupcust,NamaGroupcust FROM groupcust order by KdGroupcust";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function getGroupBayar()
	{
		$sql = "SELECT KdGroupbayar,NamaGroupbayar FROM groupbayar order by KdGroupbayar";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function getArea()
	{
		$sql = "SELECT KdArea,NamaArea FROM area order by KdArea";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function cekDelete($id)
	{   /*
		$sql = "SELECT KdCustomer FROM trans_simpan_header Where KdCustomer='$id'
			union
			SELECT KdCustomer FROM trans_order_barang_header Where KdCustomer='$id'
			union
			SELECT KdCustomer FROM trans_terima_header Where KdCustomer='$id'
			union
			SELECT KdCustomer FROM ketentuan_simpan Where KdCustomer='$id'
			union
			SELECT KdCustomer FROM ketentuan_simpan_detail Where KdCustomer='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		*/
		return 0;
	}
	 function getidcounter($id){
	    $sql = "SELECT KdCustomer FROM customerlist Where KdCustomer='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		IF($num==0)
		{
		   $sql = "insert into customerlist (KdCustomer, Counter) values('$id','0')";
		   $qry = $this->db->query($sql);
		}
	    $sql = "Update customerlist set Counter=Counter+1 where KdCustomer='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM customerlist Where KdCustomer='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>