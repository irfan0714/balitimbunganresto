<?php
class Market_listmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getTabelList($limit,$offset)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
    	$sql = "
    		SELECT 
			  market_list.market_list_id,
			  market_list.market_list_name,
			  IF(tb_pcode.total_pcode,tb_pcode.total_pcode,0) AS total_pcode,
			  IF(tb_pair.total_pair,tb_pair.total_pair,0) AS total_pair 
			FROM
			  market_list 
			  LEFT JOIN 
			    (SELECT 
			      market_list_detail.market_list_id,
			      COUNT(market_list_detail.sid) AS total_pcode 
			    FROM
			      market_list_detail 
			    WHERE 1 
			    GROUP BY market_list_detail.market_list_id) AS tb_pcode 
			    ON market_list.market_list_id = tb_pcode.market_list_id 
			  LEFT JOIN 
			    (SELECT 
			      market_list_pair.market_list_id,
			      COUNT(market_list_pair.sid) AS total_pair 
			    FROM
			      market_list_pair 
			    WHERE 1 
			    GROUP BY market_list_pair.market_list_id) AS tb_pair 
			    ON market_list.market_list_id = tb_pair.market_list_id 
			WHERE 
			  1 
			GROUP BY 
			  market_list.market_list_id 
			ORDER BY 
			  market_list.market_list_name,
			  market_list.market_list_id ASC 
			LIMIT
			  $offset,$limit
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_tabel_row()
    {
		$sql = "
            SELECT 
			  market_list.market_list_id,
			  market_list.market_list_name,
			  COUNT(market_list_pair.sid) AS total_user_level,
			  COUNT(market_list_detail.sid) AS total_pcode 
			FROM
			  market_list 
			  INNER JOIN market_list_pair 
			    ON market_list.market_list_id = market_list_pair.market_list_id 
			  INNER JOIN market_list_detail 
			    ON market_list.market_list_id = market_list_detail.market_list_id 
			WHERE 
				1 
			GROUP BY 
				market_list.market_list_id 
			ORDER BY 
				market_list.market_list_name,
				market_list.market_list_id ASC       
		";
		                  
        return $this->NumResult($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
    
    function getUserLevel()
    {
		$sql = "
			SELECT 
			  userlevels.UserLevelID,
			  userlevels.UserLevelName 
			FROM
			  userlevels 
			WHERE 1 
			ORDER BY userlevels.UserLevelName ASC,
			  userlevels.UserLevelID DESC
		";
		
        return $this->getArrayResult($sql);
	}
	
	function getDetail($id)
	{
		$sql = "
			SELECT 
			  market_list_detail.*,
			  masterbarang.NamaLengkap AS NamaBarang
			FROM
			  market_list_detail 
			  INNER JOIN masterbarang 
			    ON market_list_detail.pcode = masterbarang.PCode 
			WHERE 1 
			  AND market_list_detail.market_list_id = '".$id."' 
			ORDER BY 
			  masterbarang.NamaLengkap ASC,
			  market_list_detail.pcode ASC,
			  market_list_detail.sid DESC
		";
		
        return $this->getArrayResult($sql);
	}

	function getHeader($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  market_list 
			WHERE 1 
			  AND market_list.market_list_id = '".$id."' 
			ORDER BY market_list.market_list_name ASC 
			LIMIT 0, 01	
        ";
        
        return $this->getRow($sql);
	}

	function getPair($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  market_list_pair 
			WHERE 1 
			  AND market_list_pair.market_list_id = '".$id."' 
			ORDER BY market_list_pair.market_list_id ASC 
        ";
        
        return $this->getArrayResult($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  market_list_detail.* 
			FROM
			  market_list_detail 
			WHERE 1 
			  AND market_list_detail.pcode = '$pcode' 
			  AND market_list_detail.market_list_id = '$nodok' 
			LIMIT 1
		";
		
        return $this->getRow($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>