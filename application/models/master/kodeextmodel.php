<?php
class Kodeextmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getkodeextList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
    	$sql = "select date_format(tgl,'%d-%m-%Y') as tgl,kodegrp,namagrp,status from kodeextheader 
				$clause 
				order by kodegrp desc Limit $offset,$num
			";
		return $this->getArrayResult($sql);
    }
    
    function num_kodeext_row($id,$with){
     	$clause="";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
		$sql = "SELECT kodegrp FROM kodeextheader $clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	function getCustomerList(){
    	$sql = "SELECT KdCustomer,NamaCustomer from customer order by KdCustomer";
		return $this->getArrayResult($sql);
    }
	function getSatuanAtas(){
    	$sql = "SELECT KdSatuan,NamaSatuan from satuan order by KdSatuan";
		return $this->getArrayResult($sql);
    }
	function getSatuan($pcode)
	{
		$sql = "SELECT Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
    }
	
	function getCustomer($kode)
	{
		$sql = "SELECT KdCustomer,Nama,Alamat,Kota,Payment,TOP,LimitKredit,LimitFaktur FROM customer WHERE KdCustomer='$kode'";
        return $this->getRow($sql);
	}
	function getPCodeDet($kode)
	{
		$sql = "SELECT b.*,NamaBrand,NamaKategori,NamaSatuanSt FROM(
				SELECT PCode,NamaLengkap,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand,
				SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,  
				(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0
				FROM masterbarang WHERE pcode='$kode'
				) b
				LEFT JOIN
				(
				SELECT KdBrand,NamaBrand FROM brand
				) br
				ON br.KdBrand=b.KdBrand
				LEFT JOIN
				(
				SELECT KdKategori,NamaKategori FROM kategori
				) kt
				ON kt.KdKategori=b.KdKategori
				LEFT JOIN
				(
				SELECT KdSatuan,NamaSatuan AS NamaSatuanSt FROM satuan
				) st	
				ON st.KdSatuan=b.SatuanSt";
        return $this->getRow($sql);
	}
    function ifPCodeBarcode($id){
		$bar = substr($id,0,10);
		$sql = "SELECT PCode,if(PCode='$id','',if(BarcodeSatuanBesar='$bar','B',if(BarcodeSatuanTengah='$bar','T','K'))) as Jenis FROM masterbarang Where PCode='$id' or BarcodeSatuanBesar='$bar' or BarcodeSatuanTengah='$bar' or BarcodeSatuanKecil='$bar'";
		return $this->getRow($sql);
	}
	function cekShare($kode,$kontak)
	{
		$sql = "select h.KdKetentuan
				from ketentuan_simpan_detail d,ketentuan_simpan h
				where PCode='$kode' and h.KdKetentuan=d.KdKetentuan and Pilihan='K' and h.KdCustomer<>'$kontak'
				and SharePCode='N';";
        return $this->NumResult($sql);
	}
	function getNewNo()
	{
		$sql = "SELECT grpext FROM aplikasi";
		return $this->getRow($sql);
	}
	function getCounter($id)
	{
		$sql = "SELECT counter FROM kodeextdetail Where kodegrp='$id' order by Counter desc limit 0,1";
		return $this->getRow($sql);
	}
	function getDetail($id)
	{
		$sql = "
		select d.*,NamaLengkap from(
		SELECT * from kodeextdetail Where kodegrp='$id' order by counter
		) d
		left join
		(
		   select PCode,NamaLengkap from masterbarang
		) masterb
		on masterb.PCode = d.PCode";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "
		select d.PCode,QtyInput,QtyPcs,Satuan,NamaInitial,Harga,NamaSatuan,KonversiSt from(
		SELECT * from kodeextdetail Where NoDokumen='$id' order by Counter
		) d
		left join
		(
			select PCode,NamaStruk as NamaInitial,Satuan1,Satuan2,Satuan3 from masterbarang
		) masterb
		on masterb.PCode = d.PCode
		left join 
		(
			select KdSatuan,NamaSatuan from satuan
		)sak
		on sak.KdSatuan = Satuan";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
		$sql = "select date_format(tgl,'%d-%m-%Y') as tgl,kodegrp,namagrp,status from kodeextheader h 
				where h.kodegrp='$id'";
        return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getidcounter(){
	    $sql = "Update aplikasi set grpext=grpext+1";
		$qry = $this->db->query($sql);
		$sql = "SELECT grpext FROM aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>