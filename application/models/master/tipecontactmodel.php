<?php
class Tipecontactmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function gettipecontactList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "SELECT KdTipeContact, NamaTipeContact FROM tipe_contact $clause order by KdTipeContact Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_tipecontact_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdTipeContact FROM tipe_contact $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    function DefaultTipe()
	{
		$sql = "SELECT DefaultKodeTipeCustomer,DefaultKodeTipeSupplier from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
    function getDetail($id){
    	$sql = "SELECT KdTipeContact,NamaTipeContact from tipe_contact Where KdTipeContact='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdTipeContact FROM tipe_contact Where KdTipeContact='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdTipeContact FROM contact Where KdTipeContact='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
}
?>