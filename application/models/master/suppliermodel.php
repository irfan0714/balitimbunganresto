<?php
class Suppliermodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getsupplierList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "SELECT KdSupplier,Nama,Alamat,Kota,Telepon,IF(Payment='C','Cash','Kredit') as Payment,TOP,PPn, if(StatAktif='Y','Aktif','Non Aktif') as StatAktif, Email, Notes
		    	FROM supplier where 1 $clause order by Nama Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_supplier_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdSupplier FROM supplier c $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getArea()
	{
		$sql = "SELECT KdArea,NamaArea FROM area order by KdArea";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
    
    function getDetail($id){
    	$sql = "SELECT *,date_format(TglPkp,'%d-%m-%Y') as Tanggal from supplier Where KdSupplier='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdSupplier FROM supplier Where KdSupplier='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function getTipeSupplier()
	{
		$sql = "SELECT KdTypesupp,NamaTypesupp FROM typesupp order by KdTypesupp";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function getGroupSupplier()
	{
		$sql = "SELECT KdGroupsupp,NamaGroupsupp FROM groupsupp order by KdGroupsupp";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function getKodeExt()
	{
		$sql = "SELECT kodegrp,namagrp FROM kodeextheader order by kodegrp";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function cekDelete($id)
	{   /*
		$sql = "SELECT KdSupplier FROM trans_simpan_header Where KdSupplier='$id'
			union
			SELECT KdSupplier FROM trans_order_barang_header Where KdSupplier='$id'
			union
			SELECT KdSupplier FROM trans_terima_header Where KdSupplier='$id'
			union
			SELECT KdSupplier FROM ketentuan_simpan Where KdSupplier='$id'
			union
			SELECT KdSupplier FROM ketentuan_simpan_detail Where KdSupplier='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		*/
		return 0;
	}
	 function getidcounter($id){
	    $sql = "SELECT KdSupplier FROM supplierlist Where KdSupplier='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		IF($num==0)
		{
		   $sql = "insert into supplierlist (KdSupplier, Counter) values('$id','0')";
		   $qry = $this->db->query($sql);
		}
	    $sql = "Update supplierlist set Counter=Counter+1 where KdSupplier='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM supplierlist Where KdSupplier='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>