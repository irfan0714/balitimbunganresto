<?php
class Group_harga_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
   
	function getListTemp($user){
		$skrg = date('Y-m-d');
		$sql="SELECT 
				  a.`PCode`,
				  b.`NamaLengkap`,
				  a.Harga 
				FROM
				  groupharga_temp a 
				  INNER JOIN masterbarang b 
				    ON a.`PCode` = b.`PCode` 
				WHERE a.adduser = '$user' 
				  AND a.adddate = '$skrg' 
				  AND a.Harga <> '0' ";
				  
		return $this->getArrayResult($sql);
	}
	
	function getListHeader($id){
		$sql="SELECT 
				  * FROM groupharga_header a
				WHERE a.GroupHargaID='$id' ";
				  
		return $this->getRow($sql);
	}
	
	function getGroupHargaList(){
		$sql="SELECT 
				  * FROM groupharga_header a
				WHERE 1 ";
				  
		return $this->getArrayResult($sql);
	}
	
	function getListDetail($id){
		$sql="SELECT 
				  a.`PCode`,
				  b.`NamaLengkap`,
				  ROUND(a.Harga) AS Harga 
				   FROM groupharga_detail a
				  INNER JOIN masterbarang b
				  ON a.PCode = b.PCode
				WHERE a.GroupHargaID='$id' ";
				  
		return $this->getArrayResult($sql);
	}
	
	function getID($nama){
		$sql="SELECT 
				  * FROM groupharga_header 
			  WHERE GroupHargaName = '$nama'";
				  
		return $this->getRow($sql);
	}
	
	function getCekPCode($id,$PCode){
		$sql="SELECT 
				  * FROM groupharga_detail 
			  WHERE GroupHargaID = '$id' AND PCode='$PCode'";
				  
		return $this->getRow($sql);
	}
    
    //standart function -------------------------------------------------------------------------------------------------
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>