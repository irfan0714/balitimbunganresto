<?php
class Personalmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_personal_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "select KdPersonal,NamaPersonal,NamaTipePersonal from(
					SELECT KdPersonal, NamaPersonal,KdTipePersonal 
					FROM personal $clause order by KdPersonal  Limit $offset,$num
				) as head
				left join
				(
					select KdTipePersonal, NamaTipePersonal from tipe_personal
				) as tipe
				on tipe.KdTipePersonal = head.KdTipePersonal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_personal_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdPersonal FROM personal $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getTipe(){
    	$sql = "SELECT KdTipePersonal, NamaTipePersonal from tipe_personal order by KdTipePersonal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT KdPersonal, NamaPersonal,KdTipePersonal from personal Where KdPersonal='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdPersonal FROM personal Where KdPersonal='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdPersonal FROM kendaraan Where KdPersonal='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	
}
?>