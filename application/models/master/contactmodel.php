<?php
class Contactmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getcontactList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "SELECT KdContact,NamaTipeContact,Nama,Alamat,Kota,Telepon,IF(Payment='C','Cash','Kredit') as Payment,TOP FROM contact c,tipe_contact tc where tc.KdTipeContact = c.KdTipeContact $clause order by KdContact Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_contact_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " and $with like '%$id%'";
		}
		$sql = "SELECT KdContact FROM contact c,tipe_contact tc where tc.KdTipeContact = c.KdTipeContact $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT * from contact Where KdContact='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdContact FROM contact Where KdContact='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function getTipeKontak()
	{
		$sql = "SELECT KdTipeContact,NamaTipeContact FROM tipe_contact order by KdTipeContact";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		$query->free_result();
		return $row;
	}
	function cekDelete($id)
	{   /*
		$sql = "SELECT KdContact FROM trans_simpan_header Where KdContact='$id'
			union
			SELECT KdContact FROM trans_order_barang_header Where KdContact='$id'
			union
			SELECT KdContact FROM trans_terima_header Where KdContact='$id'
			union
			SELECT KdContact FROM ketentuan_simpan Where KdContact='$id'
			union
			SELECT KdContact FROM ketentuan_simpan_detail Where KdContact='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		*/
		return 0;
	}
	 function getidcounter($id){
	    $sql = "SELECT KdContact FROM contactlist Where KdContact='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		IF($num==0)
		{
		   $sql = "insert into contactlist (KdContact, Counter) values('$id','0')";
		   $qry = $this->db->query($sql);
		}
	    $sql = "Update contactlist set Counter=Counter+1 where KdContact='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM contactlist Where KdContact='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>