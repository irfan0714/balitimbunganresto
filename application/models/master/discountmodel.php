<?php
class discountmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getdiscountList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "select NamaDisc,if(Jenis='P','Promosi','Regular') as Jenis,if(`RupBar`='B','Barang',if(`RupBar`='P','Persentase','Rupiah')) as Rupiah,
if(Perhitungan='B','Bertingkat',if(Perhitungan='S','Sejajar',if(Perhitungan='K','Kelipatan','Tidak'))) as Perhitungan,
(select date_format(Period1,'%d-%m-%Y')) as Periode1,(select date_format(Period2,'%d-%m-%Y')) as Periode2,
if(Beban='M','Marketing',if(Beban='S','Sales',if(Beban='L','Lain - Lain',''))) as Beban1,NoRekening as Rek1,
Persen1 as Persen1, if(Beban2='M','Marketing',if(Beban2='S','Sales',if(Beban2='L','Lain - Lain',''))) as Beban2,NoRekening2 as Rek2,
Persen2 as Persen2, if(Beban3='M','Marketing',if(Beban3='S','Sales',if(Beban3='L','Lain - Lain',''))) as Beban3,NoRekening3 as Rek3,
Persen3 as Persen3,Nilai from discount_header $clause order by NamaDisc Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_discount_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT NamaDisc FROM discount_header $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getRekening()
	{
		$sql = "select KdRekening,concat(KdRekening,' - ',NamaRekening) as Keterangan from genled.rekening order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
    
    function getHeader($id){
    	$sql = "SELECT * from discount_header Where NamaDisc='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
	
	function getDetail($id){
    	$sql = "SELECT * from discount_detail Where NamaDisc='$id' Order By Jenis,List,STATUS DESC";
		$sql = "SELECT NamaDisc,Jenis,List,Concat(Jenis,List) as JenisList,Status,Opr1,Nilai1,Opr2,Nilai2,Campur from discount_detail Where NamaDisc='$id' Order By Jenis,List,STATUS DESC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT NamaDisc FROM discount_header Where NamaDisc='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function HadiahExist($id)
	{
		$sql = "SELECT PCode from masterbarang Where PCode='$id'";
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getList($field1,$field2,$tabel)
	{
	 	$sql = "select $field1,$field2 from $tabel order by $field1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;		
	}	
}
?>