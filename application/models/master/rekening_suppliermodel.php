<?php
class Rekening_Suppliermodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getsupplierList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "SELECT r.KdSupplier,s.Nama, r.NoRekening, r.NamaBank, r.NamaPemilik, if(r.StatAktif='Y','Aktif','Non Aktif') as StatAktif 
		    	FROM rekening_supplier r inner join supplier s on r.KdSupplier=s.KdSupplier
		    	where 1 $clause order by s.Nama Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_supplier_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "select r.* FROM rekening_supplier r inner join supplier s on r.KdSupplier=s.KdSupplier $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getSupplier($var = '')
	{
		$where='';
		if($var != ''){
			$where = " where Nama like '%$var%' ";
		}
		$sql = "SELECT KdSupplier, Nama FROM supplier $where order by nama";
    	return $this->getArrayResult($sql);
    }
    
    function getDetail($kdsupplier, $norekening){
		$sql = "select r.KdSupplier, s.Nama, r.NoRekening, r.NamaBank, r.NamaPemilik, r.StatAktif 
				from rekening_supplier r inner join supplier s on r.KdSupplier=s.KdSupplier
				where r.KdSupplier='$kdsupplier' and r.NoRekening='$norekening' ";
		$hasil = $this->getArrayResult($sql);
		return $hasil[0];
	}
    
    function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>