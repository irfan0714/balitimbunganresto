<?php
class Tour_Leadermodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function gettypetourList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "
              SELECT KeteranganTourLeader AS type_leader,tourtravel.Nama AS namatravel,tourleader.*
              FROM tourleader
              LEFT JOIN typetour_leader ON typetour_leader.KdTypeTourLeader = tourleader.KdTypeTourLeader
              LEFT JOIN tourtravel ON tourtravel.KdTravel=tourleader.KdTravel
              $clause order by KdTourLeader Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    function getTypeLeader() {
        $sql = "SELECT KdTypeTourLeader,  KeteranganTourLeader FROM typetour_leader";
        return $this->getArrayResult($sql);
    }
    function getBiroTour() {
        $sql = "SELECT KdTravel,  Nama FROM tourtravel";
        return $this->getArrayResult($sql);
    }
    
    function getTypeTourLeader() {
        $sql = "SELECT KdTypeTourLeader, KeteranganTourLeader FROM typetour_leader";
        return $this->getArrayResult($sql);
    }
        
    function num_typetour_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KeteranganTourLeader AS type_leader,tourtravel.Nama AS namatravel,tourleader.*
              FROM tourleader
              LEFT JOIN typetour_leader ON typetour_leader.KdTypeTourLeader = tourleader.KdTypeTourLeader
              LEFT JOIN tourtravel ON tourtravel.KdTravel=tourleader.KdTravel
              $clause order by KdTourLeader";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT *
                FROM tourleader Where KdTourLeader='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT *
                FROM tourtravel Where KdTourLeader='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    function getidcounter($id)
    {
        $sql = "SELECT KdLeader FROM leaderlist Where KdLeader='$id'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        IF ($num == 0) {
            $sql = "insert into leaderlist (KdLeader, Counter) values('$id','0')";
            $qry = $this->db->query($sql);
        }
        $sql = "Update leaderlist set Counter=Counter+1 where KdLeader='$id'";
        $qry = $this->db->query($sql);
        $sql = "SELECT Counter FROM leaderlist Where KdLeader='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
}
?>