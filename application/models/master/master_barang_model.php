<?php
class Master_barang_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
	function getDivisi()
	{
    	$sql = "SELECT * FROM divisi a ORDER BY a.`NamaDivisi` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function getKategori()
	{
    	$sql = "SELECT * FROM `kategori` a ORDER BY a.`NamaKategori` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function getSubkategoriPos()
	{
    	$sql = "SELECT * FROM `subkategoripos` a ORDER BY a.`NamaSubKategori` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function getSatuan()
	{
    	$sql = "SELECT * FROM `satuan` a ORDER BY a.`NamaSatuan` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    
	function getGroupBarang()
	{
    	$sql = "SELECT * FROM `groupbarang` a ORDER BY a.`NamaGroupBarang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    	
	function getDataSubDivisis($divisi)
	{
    	$sql = "
    			SELECT
                    subdivisi.KdSubDivisi,
                    subdivisi.NamaSubDivisi
                FROM
                    subdivisi
                WHERE
                    1
                    AND subdivisi.KdDivisi = '".$divisi."'
                ORDER BY
                    subdivisi.NamaSubDivisi ASC
    		   ";
        return $this->db->query($sql);
    }	
    
    function getDataSubKategoris($kategori)
	{
    	$sql = "
    			SELECT
                    subkategori.KdSubKategori,
                    subkategori.NamaSubKategori
                FROM
                    subkategori
                WHERE
                    1
                    AND subkategori.KdKategori = '".$kategori."'
                ORDER BY
                    subkategori.NamaSubKategori ASC
    		   ";
        return $this->db->query($sql);
    }
    
    function getDataSubKategoriPos($kategori)
	{
    	$sql = "
    			SELECT
                    subkategoripos.KdSubKategori,
                    subkategoripos.NamaSubKategori
                FROM
                    subkategoripos
                WHERE
                    1
                    AND LEFT(subkategoripos.KdSubKategori,3) = '".$kategori."'
                ORDER BY                                                  
                    subkategoripos.NamaSubKategori ASC
    		   ";
        return $this->db->query($sql);
    }
    
    function getDataKategoriTouch($kdsubkategori)
	{
    	$sql = "
    			SELECT
                    KdKategori
                FROM
                    subkategoripos
                WHERE
                    KdSubKategori='$kdsubkategori'";
        $row =  $this->getArrayResult($sql);
        return $row[0]['KdKategori'];
    }
    
    function getDataKategoris($subdivisi)
	{
    	$sql = "
    			SELECT
                    subkategori.KdSubKategori,
                    subkategori.NamaSubKategori
                FROM
                    subkategori
                WHERE
                    1
                    AND subkategori.KdKategori = '".$kategori."'
                ORDER BY
                    subkategori.NamaSubKategori ASC
    		   ";
        return $this->db->query($sql);
    }	
    
    function getCekDuplicatePcode($pcode){
		$sql = "
				SELECT 
				  masterbarang.PCode,
				  masterbarang.`NamaLengkap`,
				  masterbarang.`Barcode1`
				FROM
				  masterbarang 
				WHERE 1 
				  AND (
				    masterbarang.PCode = '$pcode' 
				  ) 
				LIMIT 0, 1 
				";
	
		return $this->getRow($sql);
	}
	
	function getCekDuplicateBarang($barang){
		$sql = "
				SELECT 
				  masterbarang.PCode,
				  masterbarang.`NamaLengkap`,
				  masterbarang.`Barcode1`
				FROM
				  masterbarang 
				WHERE 1 
				  AND (
				    masterbarang.NamaLengkap = '$barang' 
				  ) 
				LIMIT 0, 1 
				";
	 
		return $this->getRow($sql);
	}
	
	function getCekDuplicateBarcode($barcode){
		$sql = "
				SELECT 
				  masterbarang.PCode,
				  masterbarang.`NamaLengkap`,
				  masterbarang.`Barcode1`
				FROM
				  masterbarang 
				WHERE 1 
				  AND (
				    masterbarang.Barcode1 = '$barcode' 
				  ) 
				LIMIT 0, 1 
				";
	 
		return $this->getRow($sql);
	}
	
	function getSdhApprove($v_PCode)
	{
		$sql = "
				SELECT 
				  masterbarang.Approval_Status,
				  masterbarang.Approval_Status_2
				FROM
				  masterbarang 
				WHERE 
				    masterbarang.PCode = '$v_PCode' 
				";
	 
		return $this->getRow($sql);
	}
	
	function getMasterBarangList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_divisi="";
        $where_kategori = "";
        $where_urut="";
        $where_status="";
        $batas="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "masterbarang.PCode";
				$arr_keyword[1] = "masterbarang.NamaLengkap";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = " AND divisi.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = " AND kategori.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["urut"]!="")
			{
				if($arrSearch["urut"]=="0"){
					$where_urut = " ORDER BY masterbarang.AddDate ASC";
				}else if($arrSearch["urut"]=="1"){
					$where_urut = " ORDER BY masterbarang.NamaLengkap ASC";
				}else if($arrSearch["urut"]=="2"){
					$where_urut = " ORDER BY masterbarang.PCode ASC";
				}	
			}
			
			if($arrSearch["status"]!="")
			{
				if($arrSearch["status"]=='1'){
					$where_status = " AND masterbarang.Approval_Status = 0 and masterbarang.Status_mail=0 ";
                }else if($arrSearch["status"]=='2'){
                	$where_status = " AND masterbarang.Approval_Status = 0 and masterbarang.Status_mail=1 ";
                }else if($arrSearch["status"]=='3'){
                	$where_status = " AND masterbarang.Approval_Status = 1 ";
				}else {
					$where_status = " AND masterbarang.Approval_Status = 2 ";
				}
			}
		}else{
			$batas = " ORDER BY masterbarang.AddDate DESC Limit 0,$limit";
		} 
        
    	$sql = "  
	           SELECT 
                  masterbarang.PCode,
                  masterbarang.NamaLengkap,
                  masterbarang.Barcode1,
                  masterbarang.SatuanSt,
                  divisi.KdDivisi,
                  divisi.NamaDivisi,
                  kategori.KdKategori,
                  kategori.NamaKategori,
                  masterbarang.Approval_By,
                  masterbarang.Approval_By_2,
                  masterbarang.Approval_Date,
                  masterbarang.Approval_Status,
                  masterbarang.Approval_Status_2,
                  masterbarang.Approval_Remarks,
                  masterbarang.Approval_Remarks_2,
                  masterbarang.AddDate,
                  masterbarang.Status,
                  masterbarang.EditDate,
                  masterbarang.status_mail
                FROM
                  masterbarang 
                  LEFT JOIN divisi 
                    ON masterbarang.KdDivisi = divisi.KdDivisi 
                  LEFT JOIN kategori 
                    ON masterbarang.KdKategori = kategori.KdKategori
                WHERE 1
                ".$where_keyword."
                ".$where_divisi."
                ".$where_kategori."
                ".$where_status."
                ".$where_urut."
                ".$batas."
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
	
	function getDetailBarang($pcode){
		$sql="
				SELECT 
                  masterbarang.PCode,
                  masterbarang.NamaLengkap,
                  masterbarang.SatuanSt,
                  masterbarang.Barcode1,
                  divisi.KdDivisi,
                  divisi.NamaDivisi,
                  subdivisi.KdSubDivisi,
                  subdivisi.NamaSubDivisi,
                  kategori.KdKategori,
                  kategori.NamaKategori,
                  subkategori.NamaSubKategori,
                  masterbarang.Approval_By,
                  masterbarang.Approval_By_2,
                  masterbarang.Approval_Date,
                  masterbarang.Approval_Status,
                  masterbarang.Approval_Status_2,
                  masterbarang.Approval_Remarks,
                  masterbarang.Approval_Remarks_2,
                  masterbarang.AddDate,
                  masterbarang.EditDate,
                  masterbarang.AddUser,
                  masterbarang.EditUser,
                  RIGHT(masterbarang.KdKategori,2) AS KdKategori_Barang,
                  masterbarang.KdSubKategori AS KdSubKategori_Barang,
                  masterbarang.Harga1c,
                  masterbarang.komisi,
                  masterbarang.DiscInternal,
                  masterbarang.Service_charge,
                  masterbarang.PPN,
                  masterbarang.Status,
                  masterbarang.KdRekeningPersediaan,
                  masterbarang.Konsinyasi,
				  masterbarang.`status_mail`,
				  masterbarang.`FlagStock`,
				  masterbarang.`FlagPembelian`,
				  masterbarang.`FlagPenjualanPOS`,
				  masterbarang.`FlagPenjualanPOSTouch`,
				  masterbarang.SubKategoriPOSTouch,
				  subkategoripos.NamaSubKategori AS subkategoripos,
				  groupbarang.NamaGroupBarang AS NamaGroupBarang
                FROM
                  masterbarang 
                  LEFT JOIN divisi 
                    ON masterbarang.KdDivisi = divisi.KdDivisi 
                  LEFT JOIN subdivisi 
                    ON masterbarang.KdSubDivisi = subdivisi.KdSubDivisi 
                  LEFT JOIN kategori 
                    ON masterbarang.KdKategori = kategori.KdKategori
                  LEFT JOIN subkategori
                    ON masterbarang.KdSubKategori = subkategori.KdSubKategori
                  LEFT JOIN subkategoripos
                    ON masterbarang.SubKategoriPOSTouch = subkategoripos.KdSubKategori
                  LEFT JOIN groupbarang
                    ON masterbarang.KdGroupBarang = groupbarang.KdGroupBarang
                WHERE 1
                    AND masterbarang.PCode = '".$pcode."'
                LIMIT
                    0,1
			";
		//echo $sql;
		//echo "<hr>";
		return $this->getRow($sql);
	}
	
	function getRekPersediaan($pcode){
		$sql="
				SELECT 
				  a.`KdRekeningPersediaan`,
				  b.`NamaRekening` 
				FROM
				  `masterbarang` a 
				  INNER JOIN rekening b 
				    ON a.`KdRekeningPersediaan` = b.`KdRekening` 
				WHERE a.`PCode` = '$pcode' ;
			";
		//echo $sql;
		//echo "<hr>";
		return $this->getRow($sql);
	}
	
	function getRekHpp($pcode){
		$sql="
				SELECT 
				  a.`KdRekeningHpp`,
				  b.`NamaRekening` 
				FROM
				  `masterbarang` a 
				  INNER JOIN rekening b 
				    ON a.`KdRekeningHpp` = b.`KdRekening` 
				WHERE a.`PCode` = '$pcode' ;
			";
		//echo $sql;
		//echo "<hr>";
		return $this->getRow($sql);
	}
	
	function getRekPenjualan($pcode){
		$sql="
				SELECT 
				  a.`KdRekeningPenjualan`,
				  b.`NamaRekening` 
				FROM
				  `masterbarang` a 
				  INNER JOIN rekening b 
				    ON a.`KdRekeningPenjualan` = b.`KdRekening` 
				WHERE a.`PCode` = '$pcode' ;
			";
		//echo $sql;
		//echo "<hr>";
		return $this->getRow($sql);
	}
	
	function getRekReturn($pcode){
		$sql="
				SELECT 
				  a.`KdRekeningReturn`,
				  b.`NamaRekening` 
				FROM
				  `masterbarang` a 
				  INNER JOIN rekening b 
				    ON a.`KdRekeningReturn` = b.`KdRekening` 
				WHERE a.`PCode` = '$pcode' ;
			";
		//echo $sql;
		//echo "<hr>";
		return $this->getRow($sql);
	}
	
	function getRekDisc($pcode){
		$sql="
				SELECT 
				  a.`KdRekeningDisc`,
				  b.`NamaRekening` 
				FROM
				  `masterbarang` a 
				  INNER JOIN rekening b 
				    ON a.`KdRekeningDisc` = b.`KdRekening` 
				WHERE a.`PCode` = '$pcode' ;
			";
		//echo $sql;
		//echo "<hr>";
		return $this->getRow($sql);
	}
	
	function getCekPB($pcode){
		$sql="
				SELECT
                    permintaan_barang_detail.PCode
                FROM
                    permintaan_barang_detail
                WHERE
                    1
                AND permintaan_barang_detail.PCode = '".$pcode."'
                LIMIT
                0,1
			";
		/*echo $sql;
		echo "<hr>";*/
		return $this->getRow($sql);
	}
	
	function getCekTransaksi($pcode){
		$sql="
			SELECT
                transaksi_detail.PCode
            FROM
                transaksi_detail
            WHERE
                1
            AND transaksi_detail.PCode = '".$v_PCode."'
            LIMIT
            0,1
			";
		/*echo $sql;
		echo "<hr>";*/
		return $this->getRow($sql);
	}
	
	function num_master_barang_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM masterbarang;       
		";
		                  
        return $this->NumResult($sql);
	}
    
    function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function level_approve($level){
		$result = $this->db->get_where('otorisasi_user',array('Tipe'=>$level));

		if($result->num_rows() > 0){
			$val = $result->row();
			$return = $val->UserName;
		}else{
			$return = "Otorisasi Belum Di Setting."; 
		}
		return $return;

	}

	function getEmail($func_name,$username){
		
		$result = $this->db->get_where('function_email',array('func_name'=>$func_name,'username'=>$username));

		if($result->num_rows() > 0){
			$val = $result->row();
			$return = $val;
		}else{
			$return = "Email Belum Di Setting."; 
		}
		return $return;
	}
}
?>