<?php
class Perusahaanmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function getperusahaanList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = "where $with like '%$id%'";
		}
    	$sql = "SELECT KdPerusahaan,Nama FROM perusahaan $clause order by KdPerusahaan Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_perusahaan_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = "where $with like '%$id%'";
		}
		$sql = "SELECT KdPerusahaan FROM perusahaan $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT * from perusahaan Where KdPerusahaan='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdPerusahaan FROM perusahaan Where KdPerusahaan='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "
		SELECT KdPerusahaan FROM trans_order_taking_header Where KdPerusahaan='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function DefaultKode()
	{
		$sql = "SELECT DefaultKodePerusahaan from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>