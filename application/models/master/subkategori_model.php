<?php

class Subkategori_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }

    function getDataList($limit, $offset, $arrSearch) {
        $mylib = new globallib();

        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }

        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "subkategori.NamaSubKategori";
				$arr_keyword[1] = "kategori.NamaKategori";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }
        }

        $sql = "  
            SELECT 
              *,kategori.NamaKategori
            FROM
              subkategori
			LEFT JOIN
			  kategori
			ON subkategori.KdKategori=kategori.KdKategori 
            WHERE 
            	1  
            " . $where_keyword . "                                    
            ORDER BY 
              subkategori.NamaSubKategori DESC 
            Limit 
              $offset,$limit
        ";
        /* echo $sql;
          echo "<hr/>"; */
        return $this->getArrayResult($sql);
    }

    function num_data_row($arrSearch) {
        $mylib = new globallib();

        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "subkategori.NamaSubKategori";
				$arr_keyword[1] = "kategori.NamaKategori";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }
        }

        $sql = "
            SELECT 
              *,kategori.NamaKategori
            FROM
              subkategori
			LEFT JOIN
			  kategori
			ON subkategori.KdKategori=kategori.KdKategori 
            WHERE 
            	1
            " . $where_keyword . "
		";

        return $this->NumResult($sql);
    }
    
    function getDetail($id){
    	$sql = "SELECT KdSubKategori,NamaSubKategori,KdKategori from subkategori Where KdSubKategori='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>