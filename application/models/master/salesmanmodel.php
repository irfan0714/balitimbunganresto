<?php
class Salesmanmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_salesman_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	/*$sql = "select KdSalesman,NamaSalesman,head.KdPersonal,NamaPersonal,NamaTipeSalesman,KdGudang from(
					SELECT KdSalesman,NamaSalesman,KdPersonal,KdTipeSalesman, KdGudang 
					FROM salesman $clause order by KdSalesman  Limit $offset,$num
				) as head
				left join
				(
					select KdPersonal, NamaPersonal from personal
				) as personal
				on personal.KdPersonal = head.KdPersonal
				left join
				(
					select KdTipeSalesman, NamaTipeSalesman from tipe_salesman
				) as tipe
				on tipe.KdTipeSalesman = head.KdTipeSalesman";*/
		$sql="SELECT * FROM salesman INNER JOIN supervisor  ON salesman.KdSupervisor = supervisor.KdSupervisor $clause";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_salesman_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdSalesman FROM salesman $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getTipe(){
    	$sql = "SELECT KdTipeSalesman, NamaTipeSalesman from tipe_salesman order by KdTipeSalesman";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getPersonal(){
    	$sql = "SELECT KdPersonal, NamaPersonal from personal order by KdPersonal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getSupervisor(){
    	$sql = "SELECT * from supervisor order by KdSupervisor";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getGudang(){
    	$sql = "SELECT KdGudang, Keterangan as NamaGudang from gudang order by KdGudang";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT * from salesman Where KdSalesman='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdSalesman FROM salesman Where KdSalesman='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		/* $sql = "SELECT KdSalesman FROM kendaraan Where KdSalesman='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;*/
		return 0;
	}
	
	function getidcounter($id){
	    $sql = "SELECT KdSalesman FROM salesmanlist Where KdSalesman='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		IF($num==0)
		{
		   $sql = "insert into salesmanlist (KdSalesman, Counter) values('$id','0')";
		   $qry = $this->db->query($sql);
		}
	    $sql = "Update salesmanlist set Counter=Counter+1 where KdSalesman='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM salesmanlist Where KdSalesman='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
}
?>