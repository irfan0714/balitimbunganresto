<?php
class Saldoawalmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_saldoawal_List($num,$offset,$id,$with,$tahunbulan)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
		$tahun = substr($tahunbulan,0,4);
		$bulan = substr($tahunbulan,-2);
    	$sql = "SELECT a.*,NamaRekening FROM
(SELECT Departemen,KdRekening,Awal$bulan as SaldoAwal FROM saldo_gl WHERE tahun='$tahun')a
LEFT JOIN
(SELECT kdrekening AS kode,NamaRekening FROM rekening)b
ON a.kdrekening=b.kode $clause
ORDER BY departemen,kdrekening";
				
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
	function getTahunBulan()
	{
		$sql = "select TahunBulanAwal as TahunBulan from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
		return $this->getRow($sql);
	}
	
    function num_saldoawal_row($id,$with,$tahunbulan){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$tahun = substr($tahunbulan,0,4);
		$bulan = substr($tahunbulan,-2);
		$sql = "SELECT a.*,namarekening FROM
(SELECT departemen,kdrekening,Awal$bulan FROM saldo_gl WHERE tahun='$tahun')a
LEFT JOIN
(SELECT kdrekening AS kode,namarekening FROM rekening)b
ON a.kdrekening=b.kode $clause
ORDER BY departemen,kdrekening";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getPosisi(){
    	$sql = "SELECT posisi, namaposisi from posisi order by posisi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getDepartemen(){
    	$sql = "SELECT KdDepartemen, NamaDepartemen from departemen order by KdDepartemen";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getRekening(){
    	$sql = "SELECT kdrekening, namarekening from rekening where posisi in ('01','02') and tingkat='3' order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($departemen,$rekening,$tahunbulan){
		$tahun = substr($tahunbulan,0,4);
		$bulan = substr($tahunbulan,-2);
    	$sql = "SELECT a.*,NamaRekening,NamaDepartemen FROM
(SELECT Departemen,KdRekening,Awal$bulan as SaldoAwal FROM saldo_gl WHERE tahun='$tahun' and departemen='$departemen' and kdrekening='$rekening')a
LEFT JOIN
(SELECT kdrekening AS kode,NamaRekening FROM rekening)b
ON a.kdrekening=b.kode
LEFT JOIN
(SELECT kddepartemen AS kode,NamaDepartemen FROM departemen)c
ON a.Departemen=c.kode
ORDER BY departemen,kdrekening";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function cekdata($departemen,$rekening,$tahun){
		$sql = "SELECT KdRekening FROM saldo_gl where Departemen='$departemen' and KdRekening='$rekening' and Tahun='$tahun'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdSaldoawal FROM JurnalDetail Where KdSaldoawal='$id' limit 1";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
}
?>