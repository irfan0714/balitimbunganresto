<?php
class Rekeningmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_rekening_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "select head.KdRekening,NamaRekening,Posisi,Tingkat,Parent,NamaParent from(
				  SELECT KdRekening,NamaRekening,Posisi,Tingkat,Parent FROM rekening $clause order by KdRekening Limit $offset,$num
				) as head
				left join
				(
				  select KdRekening, NamaRekening as NamaParent from rekening
				) as tipe
				on tipe.KdRekening = head.Parent";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_rekening_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdRekening FROM rekening $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getPosisi(){
    	$sql = "SELECT posisi, namaposisi from posisi order by posisi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getTingkat(){
    	$sql = "SELECT tingkat, namatingkat from tingkat order by tingkat";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getParent(){
    	$sql = "SELECT KdRekening, NamaRekening from rekening where tingkat in ('1','2') order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "select head.KdRekening,NamaRekening,Posisi,Tingkat,Parent as RekParent,NamaParent from(
				  SELECT KdRekening,NamaRekening,Posisi,Tingkat,Parent FROM rekening WHERE KdRekening='$id'
				) as head
				left join
				(
				  select KdRekening, NamaRekening as NamaParent from rekening
				) as tipe
				on tipe.KdRekening = head.Parent";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdRekening FROM rekening Where KdRekening='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		$sql = "SELECT KdRekening FROM jurnaldetail Where KdRekening='$id' limit 1";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	
}
?>