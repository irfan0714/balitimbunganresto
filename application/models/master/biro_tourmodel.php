<?php
class Biro_Tourmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function gettypetourList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "
              SELECT KdTravel,Nama,Contact,Phone,Fax,Alamat,Email,Kota,Telepon,Aktif,ADDDATE,EditDate,KdTypeTour,AddDate,EditDate
              FROM tourtravel $clause order by KdTypeTour Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    function getType() {
        $sql = "SELECT KdTypeTour,  KeteranganTour FROM typetour";
        return $this->getArrayResult($sql);
    }
    
    function num_typetour_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "
            SELECT KdTravel,Nama,Contact,Phone,Fax,Alamat,Email,Kota,Telepon,Aktif,ADDDATE,EditDate,KdTypeTour
            FROM tourtravel $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
    function getDetail($id){
    	$sql = "SELECT KdTravel,Nama,Contact,Phone,Fax,Alamat,Email,Kota,Telepon,Aktif,ADDDATE,EditDate,KdTypeTour
                FROM tourtravel Where KdTravel='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function cekDetail($phone){
    	$sql = "SELECT * FROM `tourtravel` a WHERE a.`Phone`='$phone' OR a.`Phone`='$phone';";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function get_id($id){
		$sql = "SELECT KdTravel,Nama,Contact,Phone,Fax,Alamat,Email,Kota,Telepon,Aktif,ADDDATE,EditDate,KdTypeTour
                FROM tourtravel Where KdTravel='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    function getidcounter($id)
    {
        $sql = "SELECT KdTravel FROM travellist Where KdTravel='$id'";
        $query = $this->db->query($sql);
        $num = $query->num_rows();
        IF ($num == 0) {
            $sql = "insert into travellist (KdTravel, Counter) values('$id','0')";
            $qry = $this->db->query($sql);
        }
        $sql = "Update travellist set Counter=Counter+1 where KdTravel='$id'";
        $qry = $this->db->query($sql);
        $sql = "SELECT Counter FROM travellist Where KdTravel='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
}
?>