<?php
class Formulamodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getformulaList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
    	$sql = "select date_format(tglformula,'%d-%m-%Y') as tglformula,kdformula,namaformula,h.pcode,status,namalengkap as namabarang,qty,namasatuan as satuan from formulaheader h 
				left join masterbarang on masterbarang.PCode = h.pcode
				left join satuan on satuan.kdsatuan = h.satuan
				$clause 
				order by kdformula desc Limit $offset,$num
			";
		return $this->getArrayResult($sql);
    }
    
    function num_formula_row($id,$with){
     	$clause="";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
		$sql = "SELECT kdformula FROM formulaheader $clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	function getCustomerList(){
    	$sql = "SELECT KdCustomer,NamaCustomer from customer order by KdCustomer";
		return $this->getArrayResult($sql);
    }
	function getSatuanAtas(){
    	$sql = "SELECT KdSatuan,NamaSatuan from satuan order by KdSatuan";
		return $this->getArrayResult($sql);
    }
	function getSatuan($pcode)
	{
		$sql = "SELECT Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
    }
	
	function getCustomer($kode)
	{
		$sql = "SELECT KdCustomer,Nama,Alamat,Kota,Payment,TOP,LimitKredit,LimitFaktur FROM customer WHERE KdCustomer='$kode'";
        return $this->getRow($sql);
	}
	
	function getPCodeDet($kode)
	{
		$sql = "
				SELECT b.*,NamaBrand,NamaKategori,NamaSatuanSt FROM(
				SELECT PCode,NamaStruk,SatuanSt,Satuan1,Konv1st,Harga1c,Harga1t,Satuan2,Konv2st,Harga2c,Harga2t,Satuan3,Konv3st,Harga3c,Harga3t,
				KdKategori,KdBrand,
				(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt
				FROM masterbarang WHERE pcode='$kode'
				) b
				LEFT JOIN
				(
				SELECT KdBrand,NamaBrand FROM brand
				) br
				ON br.KdBrand=b.KdBrand
				LEFT JOIN
				(
				SELECT KdKategori,NamaKategori FROM kategori
				) kt
				ON kt.KdKategori=b.KdKategori
				LEFT JOIN
				(
				SELECT KdSatuan,NamaSatuan AS NamaSatuanSt FROM satuan
				) st	
				ON st.KdSatuan=b.SatuanSt";
        return $this->getRow($sql);
	}
    function ifPCodeBarcode($id){
		$bar = substr($id,0,10);
		$sql = "SELECT PCode,if(PCode='$id','',if(BarcodeSatuanBesar='$bar','B',if(BarcodeSatuanTengah='$bar','T','K'))) as Jenis FROM masterbarang Where PCode='$id' or BarcodeSatuanBesar='$bar' or BarcodeSatuanTengah='$bar' or BarcodeSatuanKecil='$bar'";
		return $this->getRow($sql);
	}
	function cekShare($kode,$kontak)
	{
		$sql = "select h.KdKetentuan
				from ketentuan_simpan_detail d,ketentuan_simpan h
				where PCode='$kode' and h.KdKetentuan=d.KdKetentuan and Pilihan='K' and h.KdCustomer<>'$kontak'
				and SharePCode='N';";
        return $this->NumResult($sql);
	}
	function getNewNo()
	{
		$sql = "SELECT NoFormula FROM aplikasi";
		return $this->getRow($sql);
	}
	function getCounter($id,$pcode)
	{
		$sql = "SELECT counter FROM formuladetail Where kdformula='$id' and pcode='$pcode' order by Counter desc limit 0,1";
		return $this->getRow($sql);
	}
	function getDetail($id,$pcode)
	{
		$sql = "
		select d.PCodeDetail as PCode,NamaInitial,Qty,Satuan,NamaSatuan,masterb.PCode as PCodeBarang,
		NamaSatuan as nilsatuan,
		Satuan1,Nama1,Satuan2,Nama2,Satuan3,Nama3
		from(
		SELECT * from formuladetail Where kdformula='$id' and pcode='$pcode' order by counter
		) d
		left join
		(
		   select PCode,NamaStruk as NamaInitial,
				Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3 from masterbarang
		) masterb
		on masterb.PCode = d.PCodeDetail
		left join
		(
		select KdSatuan,NamaSatuan from satuan
		) satu
		on satu.KdSatuan = d.satuan";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "
		select d.PCode,QtyInput,QtyPcs,Satuan,NamaInitial,Harga,NamaSatuan,KonversiSt from(
		SELECT * from formuladetail Where NoDokumen='$id' order by Counter
		) d
		left join
		(
			select PCode,NamaStruk as NamaInitial,Satuan1,Satuan2,Satuan3 from masterbarang
		) masterb
		on masterb.PCode = d.PCode
		left join 
		(
			select KdSatuan,NamaSatuan from satuan
		)sak
		on sak.KdSatuan = Satuan";
        return $this->getArrayResult($sql);
	}
	function getHeader($id,$pcode)
	{
		$sql = "select date_format(tglformula,'%d-%m-%Y') as tglformula,kdformula,namaformula,h.pcode,status,namalengkap as namabarang,qty,satuan as satuan from formulaheader h 
				left join masterbarang on masterbarang.PCode = h.pcode
				left join satuan on satuan.kdsatuan = h.satuan where h.kdformula='$id' and h.pcode='$pcode'";
        return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getidcounter($id){
	    $sql = "SELECT PCode FROM formulalist Where PCode='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		IF($num==0)
		{
		   $sql = "insert into formulalist (PCode, Counter) values('$id','0')";
		   $qry = $this->db->query($sql);
		}
	    $sql = "Update formulalist set Counter=Counter+1 where PCode='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM formulalist Where PCode='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>