<?php
class Authmodel extends CI_Model {

    function __construct(){
        parent::__construct();
    }



    function GetCorp(){

    $sql = "SELECT * FROM corporate WHERE Aktif='Y'";
    $qry  = $this->db->query($sql);
    return $qry->result_array();
    }

	function getValidUser($username,$password){
		$retval	= array();
		$mdpassword=md5($password);
		$sql	= "SELECT * FROM user WHERE UserName=? AND 	Password=? AND Active=?";
		$query	= $this->db->query($sql,array($username, $mdpassword,'Y'));
		$result	= $query->result_array();

		$zql	= "SELECT * FROM aplikasi";
		$kuery	= $this->db->query($zql);
		$main	= $kuery->result_array();

		if($query->num_rows()>0) {

			//versionstart

			$vtahun = date("y");
      $vbulan = date("m");
      $this->db->select("DefaultKdProject,FlagUpdate");
      $this->db->limit(1);
      $this->db->from("aplikasi");
      $query = $this->db->get();
      $rslt = $query->result_array();

      $total_rec = $query->num_rows();

      if($rslt[0]["FlagUpdate"] == 1){

        $lines = explode("\n", file_get_contents(base_url().'/cangelog.txt'));


        $nomor = intval(substr($rslt[0]["DefaultKdProject"],0,1));
        if($nomor == 0){
          $nomor1 = intval(substr($rslt[0]["DefaultKdProject"],0,2))+1;
          $nomor = "0".$nomor1;
        }else{
          $nomor = intval(substr($rslt[0]["DefaultKdProject"],0,2))+1;
        }

        $this->db->where('KdCabang', "00");
        $this->db->update("aplikasi",array("Version" => $vtahun.$vbulan.$nomor,"FlagUpdate" => "0","DefaultKdProject" => $nomor));
        $dataversion = array(
          'version' => $vtahun.$vbulan.$nomor,
          'waktu_update' => date("Y-m-d"),
          'jumlahupdate' => count($lines)

        );

        $this->db->insert("version_header",$dataversion);

        for ($i=0; $i < count($lines) ; $i++) {
          // code...
          $vdata = array(
            'version' => $vtahun.$vbulan.$nomor,
            'updatename' => $lines[$i]
          );

          $this->db->insert('version_detail', $vdata);
        }

      }

			//versionend
      //panicbutton
      $panic = '';
      $user = $result[0]['UserName'];
      $sql4 = "SELECT * FROM otorisasi_user WHERE Tipe ='panicbutton' AND UserName = '$user'";
      $qry = $this->db->query($sql4);

      $permit = $qry->result_array();

      if(!empty($permit)){
        $panic = "Y";
      }else {
        $panic = "T";
      }
      //panicbutton

			$retval	= array(
							array(
				                   'username'  => $result[0]['UserName'],
				                   'userlevel' => $result[0]['UserLevel'],
				                   'userid'    => $result[0]['Id'],
								   'Tanggal_Trans' => $main[0]['TglTrans'],
								   'HeaderColor' => $main[0]['HeaderColor'],
								   'Logo' => $main[0]['Logo'],
								   'PT' => $main[0]['NamaPT'],
								   'Alias' => $main[0]['NamaAlias'],
								   'database' => $main[0]['database'],
									 'version' => $main[0]['Version'],
                   'panicbutton' => $panic,
								   'bulanaktif' => $result[0]['Bulan'],
								   'tahunaktif' => $result[0]['Tahun'],
								   'active' => $result[0]['UserName'],
								   'last_page' => ''
				               )
						);
		}
		return $retval;

	}

}
?>
