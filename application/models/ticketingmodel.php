<?php
class Ticketingmodel extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }
	
	function getAllTicket(){
		$sql = "select * from masterbarang where JenisBarang='TCK' ";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function getTransTicket($getLastIP){
		$sql = "select * from ticket where status='0' and last_ip='$getLastIP' order by noticket ASC";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function editStatusTicket($parameter,$data,$where){
		$this->db->where($where);
		$this->db->update($parameter, $data);
	}
	
	function totalTicketPerhari($date){
		$sql = "SELECT COUNT(noticket) AS total FROM ticket WHERE add_date='$date' AND noidentitas !='1234'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function num_ticket_row($first_date,$last_date){
        $where="where a.noidentitas !='1234' ";
        $orderby=" ORDER BY a.noticket DESC ";
     	if($first_date!=''){
			$where .= " and add_date = '$first_date'";
		}
		if($first_date!='' && $last_date!=''){
			$where .= " and add_date >= '$first_date' and add_date <= '$last_date' ";
		}
		$sql = "SELECT a.noticket FROM ticket a LEFT JOIN ticket_jenis b ON a.jenis=b.idjenis $where $orderby ";
        return $this->NumResult($sql);
	}
	
	function getTicketList($num,$offset,$first_date,$last_date)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        $where="where a.noidentitas !='1234' ";
        $orderby=" ORDER BY a.noticket DESC ";
     	if($first_date!=''){
			$where .= " and add_date = '$first_date'";
		}
		if($first_date!='' && $last_date!=''){
			$where .= " and add_date >= '$first_date' and add_date <= '$last_date' ";
		}
		$sql = "SELECT *,b.NamaJenis FROM ticket a LEFT JOIN ticket_jenis b ON a.jenis=b.idjenis $where $orderby Limit $offset,$num";

		if($this->session->userdata('username') == 'mechael0101'){
			echo $sql; #iso.
		}
		return $this->getArrayResult($sql);
    }
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>