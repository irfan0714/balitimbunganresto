<?php
class Barcodefixedassestmodel extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function getBarang(){
		$sql = "SELECT * FROM masterbarang";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getSubDivisi(){
		$sql = "SELECT * FROM subdivisi";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getProduct($SubDivisi){
		$sql = "SELECT * FROM masterbarang WHERE KdSubDivisi = '".$SubDivisi."' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getBarcode($KdPro){
		$sql = "SELECT NamaLengkap, NamaInitial, Barcode1, Harga1c FROM masterbarang WHERE PCode = '".$KdPro."' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getDepartement(){
		$sql = "SELECT * FROM departemen_fa ORDER BY KdDepartemen ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getLokasi(){
		$sql = "SELECT * FROM lokasi ORDER BY KdLokasi ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getDataBuku(){
		$sql = "SELECT DISTINCT(a.`data_buku`) AS data_buku FROM fixed_assets a WHERE 1 ORDER BY a.`data_buku` ASC;";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getSortDataBuku(){
		$sql = "SELECT DISTINCT(SUBSTR(a.`data_buku`,1,1)) AS sort_data_buku FROM fixed_assets a WHERE 1 ORDER BY a.`data_buku` ASC;";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getDataAssest($Departement,$Lokasi,$DataBuku,$Nama,$limit,$ToDataBuku,$FromDataBuku){

		if($Departement=='0' OR $Departement==''){
		   $Departement=" ";
		}else{
		   $Departement=" AND a.`departement`='$Departement'";
		}

		if($Lokasi=='0' OR $Lokasi==''){
		   $Lokasi=" ";
		}else{
		   $Lokasi=" AND a.`lokasi`='$Lokasi'";
		}

		if($DataBuku=='0' OR $DataBuku==''){
		   $DataBuku=" ";
		}else{
		   $DataBuku=" AND a.`data_buku`='$DataBuku'";
		}

		if($Nama=='0' OR $Nama==''){
		   $Nama=" ";
		}else{
		   $Nama=" AND a.`nofa` LIKE '%$Nama%'";
		}

		if($limit=='0' OR $limit==''){
		   $limit=" ";
		}else{
		   $limit=" LIMIT 0,$limit";
		}
		
		if($ToDataBuku=="" AND $FromDataBuku==""){	
		   $Sort_Data_Buku = "" ;
		}else{		   
		   $Sort_Data_Buku = "AND SUBSTR(a.`data_buku`,1,1) BETWEEN '$ToDataBuku' AND '$FromDataBuku'" ;
		}
		
		$sql = "SELECT a.* FROM fixed_assets a WHERE 1 $Departement $Lokasi $DataBuku $Nama $Sort_Data_Buku ORDER BY SUBSTR(a.`data_buku`,1,1) ASC,SUBSTR(a.`data_buku`,3,3) ASC, a.`nama_assets` ASC;";
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	
	function getDataAssestKiriman($NoAsset,$Lokasi,$Dept){

	    $sql = "SELECT a.* FROM master_fixed_asset a WHERE a.NoAsset='$NoAsset' AND a.Lokasi='$Lokasi' AND a.Departemen='$Dept';";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}
?>