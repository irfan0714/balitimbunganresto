<?php
class Report_pr_non_po_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getArrayGudang($arrdata)
    {
    	$where = "";
    	if(count($arrdata)*1>0)
    	{
			$where= $arrdata;	
		}
		
    	$sql = "
    		SELECT 
			  gudang.KdGudang,
			  gudang.Keterangan 
			FROM
			  gudang 
			WHERE 
			  1
			  ".$where." 
			ORDER BY 
			  gudang.Keterangan ASC
		";
		
		return $this->getArrayResult($sql);
	}
	
	function getArraySupplier()
    {
    	$sql = "
    		SELECT 
			  supplier.KdSupplier,
			  supplier.Nama 
			FROM
			  supplier 
			WHERE 1 
			  AND supplier.StatAktif = 'Y' 
			ORDER BY supplier.Nama ASC,
			  supplier.KdSupplier ASC
		";
		
		return $this->getArrayResult($sql);
	}
	
	function getArrayCurrency()
    {
    	$sql = "
    		SELECT 
			  mata_uang.Kd_Uang, 
			  mata_uang.Keterangan 
			FROM
			  mata_uang 
			WHERE 1 
			ORDER BY mata_uang.id ASC
		";
		
		return $this->getArrayResult($sql);
	}

    function getArrayHeader($start_date,$end_date,$supplier)
    {
    	$mylib = new globallib();
    	
    	$start_date = $mylib->ubah_tanggal($start_date);
        $end_date = $mylib->ubah_tanggal($end_date);
        
        $where = "";
		
        if($supplier!="all")
        {
			//$where .= "AND trans_order_barang_header.KdSupplier='".$supplier."'";
		}
    	
		$sql="
			SELECT
			  b.*,
			  g.`Keterangan` AS NamaGudang,
			  DATE_FORMAT(b.TglDokumen, '%d-%m-%Y') AS TglDokumen_,
			  DATE_FORMAT(b.TglTerima, '%d-%m-%Y') AS TglTerima_,
			  d.`PCode`,
			  e.`NamaLengkap`,
			  d.`QtyPermintaan` AS QtyPB,
			  d.`Qty` AS QtyPR,
			  d.`Satuan`
			FROM
			  `trans_pr_header` b
			  INNER JOIN `trans_pr_detail` d
			    ON b.`NoDokumen` = d.`NoDokumen`
			  INNER JOIN masterbarang e
			    ON d.`PCode` = e.`PCode`
			  INNER JOIN gudang g
			    ON b.`KdGudang` = g.`KdGudang`
			LEFT JOIN
			        trans_order_barang_header c
			ON c.NoPr = b.NoDokumen
			WHERE 1
			AND c.NoPr IS NULL 
			AND b.`TglTerima` BETWEEN '".$start_date."' 
			AND '".$end_date."'  
			AND b.`Status` = '1';
			"; 
		return $this->getArrayResult($sql);	
	}
	
	function getArrayDetail($arrdata)
	{
		$where = "";
		if($arrdata)
		{
			$where = $arrdata;
		}
		
		$sql = "
			SELECT 
			  trans_order_barang_detail.*,
			  masterbarang.NamaLengkap as nama_barang
			FROM
			  trans_order_barang_detail 
			  INNER JOIN masterbarang 
			    ON trans_order_barang_detail.PCode = masterbarang.PCode 
			WHERE 
			  1 
			  ".$where."
			ORDER BY trans_order_barang_detail.Sid DESC
		";
		
		return $this->getArrayResult($sql);	
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>