<?php
class Report_umur_piutangmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getDate()
	{
		$sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getReport($date)
	{
		$sql = "SELECT h.`KdCustomer`, concat(COALESCE(s.Nama,''),COALESCE(t.Nama,'')) as Nama, h.`NoDokumen`, h.TglTransaksi as Tanggal, h.`JatuhTempo`, h.`NilaiTransaksi`, h.`Sisa`+COALESCE(p.Bayar,0) AS SisaPiutang,
					if(h.JatuhTempo>='$date',h.`Sisa`+COALESCE(p.Bayar,0),0) as BelumJT,
					if(DATEDIFF('$date',h.JatuhTempo)>=1 and DATEDIFF('$date',h.JatuhTempo)<31,`Sisa`+COALESCE(p.Bayar,0),0) as Aging1,
					if(DATEDIFF('$date',h.JatuhTempo)>=31 and DATEDIFF('$date',h.JatuhTempo)<61,`Sisa`+COALESCE(p.Bayar,0),0) as Aging31,
					if(DATEDIFF('$date',h.JatuhTempo)>=61 and DATEDIFF('$date',h.JatuhTempo)<91,`Sisa`+COALESCE(p.Bayar,0),0) as Aging61,
					if(DATEDIFF('$date',h.JatuhTempo)>=91,`Sisa`+COALESCE(p.Bayar,0),0) as Aging91,
					DATEDIFF('$date',h.JatuhTempo) as Umur
					FROM 
					piutang h
					LEFT JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
					LEFT JOIN tourtravel t ON h.`KdCustomer`=t.KdTravel
					LEFT JOIN (
					SELECT d.`NoFaktur`, SUM(d.`NilaiBayar`) AS Bayar
					FROM `pelunasan_piutang_header` h INNER JOIN `pelunasan_piutang_detail` d ON h.`NoTransaksi`=d.`NoTransaksi`
					WHERE h.`TglDokumen`>'$date' AND h.`Status`=1 AND d.`NilaiBayar`>0
					GROUP BY d.`NoFaktur`) p ON h.NoDokumen=p.NoFaktur
					WHERE h.TglTransaksi<='$date' AND tipetransaksi <>'P'
					HAVING SisaPiutang<>0
					ORDER BY h.`KdCustomer`, h.`NoDokumen`";
		return $this->getArrayResult($sql);
	}
	
	function getReportRekap($date)
	{
		$sql = "SELECT h.`KdCustomer`, concat(COALESCE(s.Nama,''),COALESCE(t.Nama,'')) as Nama, sum(h.`NilaiTransaksi`) as NilaiTransaksi , sum(h.`Sisa`+COALESCE(p.Bayar,0)) AS SisaPiutang,
					sum(if(h.JatuhTempo>='$date',h.`Sisa`+COALESCE(p.Bayar,0),0)) as BelumJT,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=1 and DATEDIFF('$date',h.JatuhTempo)<31,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging1,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=31 and DATEDIFF('$date',h.JatuhTempo)<61,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging31,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=61 and DATEDIFF('$date',h.JatuhTempo)<91,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging61,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=91,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging91
					FROM 
					piutang h
					left JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
					LEFT JOIN tourtravel t ON h.`KdCustomer`=t.KdTravel
					LEFT JOIN (
					SELECT d.`NoFaktur`, SUM(d.`NilaiBayar`) AS Bayar
					FROM `pelunasan_piutang_header` h INNER JOIN `pelunasan_piutang_detail` d ON h.`NoTransaksi`=d.`NoTransaksi`
					WHERE h.`TglDokumen`>'$date' AND h.`Status`=1 AND d.`NilaiBayar`>0
					GROUP BY d.`NoFaktur`) p ON h.NoDokumen=p.NoFaktur
					WHERE h.TglTransaksi<='$date' AND tipetransaksi <>'P'
					group by h.KdCustomer HAVING SisaPiutang<>0
					ORDER BY s.Nama";
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>