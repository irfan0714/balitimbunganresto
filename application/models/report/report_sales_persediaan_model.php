<?php
class Report_sales_persediaan_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
    
    
    function getReport($start_date,$end_date) 
    {
    	
    	$where_date ="";
    	if($start_date!="" && $end_date!="" && $start_date!="0000-00-00" && $end_date!="0000-00-00")
    	{
			$where_date ="AND transaksi_header.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
		}
		
			$sql =" SELECT 
					  transaksi_detail.`PCode`,
					  b.`NamaLengkap`,
					  SUM(transaksi_detail.`Qty`) AS Qty,
					  sum(if(transaksi_header.Tanggal='$end_date',Qty, 0)) as QtyToday
					FROM
					  transaksi_header 
					  INNER JOIN transaksi_detail 
					    ON transaksi_header.`NoKassa` = transaksi_detail.`NoKassa` 
					    AND transaksi_header.`NoStruk` = transaksi_detail.`NoStruk` 
					  INNER JOIN masterbarang b 
					    ON transaksi_detail.`PCode` = b.`PCode` 
					  INNER JOIN subdivisi s 
					    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
					  INNER JOIN divisireport r 
					    ON s.`KdDivisiReport` = r.`KdDivisiReport`
					  INNER JOIN divisi ON s.`KdDivisi`=divisi.`KdDivisi` 
					WHERE transaksi_header.status = '1' and b.kdsubdivisi='01' 
                    ".$where_date.
					"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , b.`PCode`";
		//echo $sql;
        $result = $this->getArrayResult($sql);
        return $result;
    }
	
	function getStockAwalBulan($bulan, $tahun)
	{
		//$sql = "SELECT a.`PCode`,a.GAwal$bulan AS saldoawal FROM stock a WHERE a.`Tahun`='$tahun' AND a.`KdGudang`='$bulan';" ;
		$sql = "
				SELECT 
				  a.`PCode`,
				  b.`NamaLengkap`,
				  SUM(a.GAwal$bulan) AS saldoawal 
				FROM
				  stock a 
				  INNER JOIN masterbarang b 
				    ON a.`PCode` = b.`PCode` 
				WHERE a.`Tahun` = '$tahun' and b.kdsubdivisi='01' and a.kdgudang='07'
				GROUP BY a.`PCode` 
				ORDER BY a.`PCode` ASC ;
				";
		return $this->getArrayResult($sql);
	}
	
	function getHitungMutasi($tgldari)
	{
		$sql = "
				SELECT
				a.KodeBarang AS PCode, 
				SUM(IF( a.`Jenis`='I', a.`Qty`, 0)) AS jml_in, 
				SUM(IF( a.`Jenis`='O', a.`Qty`, 0)) AS jml_out
				FROM `mutasi` a 
				inner join masterbarang b on a.kodebarang=b.pcode
				WHERE a.`Tanggal` >= '$tgldari' and b.kdsubdivisi='01' and (a.Gudang='07' or a.GudangTujuan='07')
				GROUP BY a.`KodeBarang`,a.Jenis;
				" ;
		return $this->getArrayResult($sql);
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_sales'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>