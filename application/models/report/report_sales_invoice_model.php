<?php
class Report_Sales_Invoice_Model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getCustomer()
	{
		$sql = "select KdCustomer, Nama from customer order by Nama";
		return $this->getArrayResult($sql);
	}
	
	function getReport($v_start_date,$v_end_date,$kdcustomer){
		if($kdcustomer=='')
			$wherecustomer = '';
		else
			$wherecustomer = " And h.customerid='" . $kdcustomer . "'";
		$sql = "SELECT h.`invoiceno`, h.`sidate`, c.`Nama`, d.`inventorycode`, b.`NamaLengkap`,
					d.`dono`, d.`quantity`, d.`sellprice`
					 FROM salesinvoice h INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
					INNER JOIN masterbarang b ON d.`inventorycode`=b.`PCode`
					INNER JOIN customer c ON h.`customerid`=c.`KdCustomer`
					WHERE h.`sidate` BETWEEN '$v_start_date' AND '$v_end_date' $wherecustomer AND h.`status`=1
					ORDER BY h.`invoiceno`, h.`sidate`";
		
		return $this->getArrayResult($sql);
	}
	
	function getReportBarang($v_start_date,$v_end_date,$kdcustomer){
		if($kdcustomer=='')
			$wherecustomer = '';
		else
			$wherecustomer = " And h.customerid='" . $kdcustomer . "'";
		$sql = "SELECT d.`inventorycode`, b.`NamaLengkap`, SUM(d.`quantity`) AS Qty, AVG(d.`sellprice`) AS Price
					 FROM salesinvoice h INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
					INNER JOIN masterbarang b ON d.`inventorycode`=b.`PCode`
					INNER JOIN customer c ON h.`customerid`=c.`KdCustomer`
					WHERE h.`sidate` BETWEEN '$v_start_date' AND '$v_end_date' $wherecustomer AND h.`status`=1
					GROUP BY d.`inventorycode`";
		
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>