<?php
class Report_retur_beli_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getCustomer()
	{
		$sql = "select KdSupplier AS KdCustomer, Nama from supplier order by Nama";
		return $this->getArrayResult($sql);
	}
	
	function getReport($v_start_date,$v_end_date,$kdcustomer){
		if($kdcustomer=='')
			$wherecustomer = '';
		else
			$wherecustomer = " And h.supplierid='" . $kdcustomer . "'";
		    
		    $sql = "
				SELECT 
				  h.`pretdate` AS `dodate`,
				  h.`pretno` AS dono,
				  b.`NamaLengkap`,
				  g.`Keterangan` AS gudang,
				  d.`inventorycode`,
				  d.`quantity`,
				  d.`price` AS `hppvalue`,
				  c.`Nama` AS Pelanggan 
				FROM
				  `purchasereturn` h 
				  INNER JOIN `purchasereturndetail` d 
				    ON h.`pretno` = d.`pretno` 
				  INNER JOIN supplier c 
				    ON h.`supplierid` = c.`KdSupplier`
				  INNER JOIN masterbarang b 
				    ON d.`inventorycode` = b.`PCode`
				  INNER JOIN gudang g 
				    ON h.`warehouse` = g.`KdGudang` 
				WHERE h.`pretdate` between '$v_start_date' and '$v_end_date' $wherecustomer
				  AND h.`status` = 1 
				ORDER BY h.`pretdate`,
				  h.`pretno` 
		        ";
	   
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>