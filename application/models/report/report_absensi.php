<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_absensi extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {
	 	$sql = "SELECT employee.employee_id, employee_name, religion, TIME(MIN(PresensiTime)) AS TimeIn, TIME(MAX(`PresensiTime`)) AS TimeOut, shiftdate as Tanggal, Shift
				FROM shift
				INNER JOIN employee ON employee.`employee_id` = shift.`KdEmployee`
				INNER JOIN absensiemployee ON shift.`KdEmployee`=absensiemployee.`employee_id`
				INNER JOIN employee_position ON employee.employee_id = employee_position.employee_id
				INNER JOIN hrd_departemen ON employee_position.departemen_id = hrd_departemen.departemen_id
				INNER JOIN jabatan ON  jabatan.`jabatan_id` = employee_position.`jabatan_id`
				LEFT JOIN absensitrn ON  absensitrn.`NoAbsen` = absensiemployee.`NoAbsen`
				WHERE  DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY employee_id, departemen_name, Tanggal
				ORDER BY employee_id, departemen_name, Tanggal";

				
		$query = $this->db->query($sql);
        $result = $query->result_array();
        
        $arr = array();
       
        for ($i=0; $i<count($result); $i++){
        	$empid = $result[$i]['employee_id'];
        	$tgl = $result[$i]['Tanggal'];
        	$nama = $result[$i]['employee_name'];
        	$timein = $result[$i]['TimeIn'];
        	$timeout = $result[$i]['TimeOut'];
        	$shift = $result[$i]['Shift'];
			$data = array($timein,$timeout,$shift);
			$arr[$empid][$tgl] = implode('*', $data);
			
		}
					
		return $arr;
    }
    
    function getListEmp($tgldari, $tglsampai) {
	 	$sql = "SELECT employee_name AS NamaKaryawan, `departemen_name`, employee.employee_id, religion, jabatan_name
				FROM shift
				INNER JOIN employee ON employee.`employee_id` = shift.`KdEmployee`
				INNER JOIN absensiemployee ON shift.`KdEmployee`=absensiemployee.`employee_id`
				INNER JOIN employee_position ON employee.employee_id = employee_position.employee_id
				INNER JOIN hrd_departemen ON employee_position.departemen_id = hrd_departemen.departemen_id
				INNER JOIN jabatan ON  jabatan.`jabatan_id` = employee_position.`jabatan_id`
				LEFT JOIN absensitrn ON  absensitrn.`NoAbsen` = absensiemployee.`NoAbsen`
				WHERE  DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY employee_id, departemen_name
				ORDER BY departemen_name, employee_name";
				
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
	
	
	function datediff($tgl2, $tgl1){
		$sql = "SELECT DATEDIFF('$tgl2','$tgl1') as hari";
		$query = $this->db->query($sql);
        $result = $query->result_array();
        return $result[0]['hari'];
	}

}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */