<?php
class Labarugi_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getListTahun()
	{
		$sql = "select distinct Tahun from counter order by Tahun";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        $query->free_result();
        return $row;
	}
	function getDept()
	{
		$sql = "select KdDepartemen,NamaDepartemen from departemen order by KdDepartemen";
		return $this->getArrayResult($sql);
	}
	
	function getDivisi()
	{
		$sql = "select KdDivisi,NamaDivisi from divisi order by KdDivisi";
		return $this->getArrayResult($sql);
	}
	
	function getNamaDivisi($kddivisi)
	{
		$sql = "select NamaDivisi from divisi where KdDivisi='$kddivisi'";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        $query->free_result();
        return $row[0]['NamaDivisi'];
	}
	
	function getRekening()
	{
		$sql = "select KdRekening,CONCAT(KdRekening,'-',NamaRekening) as NamaRekening from rekening where tingkat='3' order by KdRekening";
		return $this->getArrayResult($sql);
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select PeriodeGL from aplikasi order by Tahun desc limit 0,1";
		$row =  $this->getArrayResult($sql);
		return $row[0]['PeriodeGL'];
	}
	function getReport($tahun, $bulan, $divisi)
	{
		$user = $this->session->userdata('username');
		$thnlalu = $tahun-1;
		if($bulan=='01'){
			$blnlalu ='12';	
		}else{
			$blnlalu = $bulan*1-1;
			if($blnlalu<10){
				$blnlalu = '0'.$blnlalu;
			}
		}
		$strbudget = '';
		for($i=1;$i<=($bulan*1);$i++){
			if($i<10) $b='0'.$i;
			$strbudget .= 'Budget'.$b.'+';
		}
		$strbudget .= '0';
		
		if($divisi=='' || $divisi=='No'){
			$where = '';
		}else{
			$where=" and d.KdDivisi='$divisi'";
		}

		if($divisi=='6'){
		$thnlalu = '0000';
		$tahun='0000';
		}
		
		if($bulan=='01'){
			$sql ="Select Parent1, NamaRekening1, Parent2, NamaRekening2, KdRekening, NamaRekening, sum(Awal) as Awal,
				sum(Nilai) as Nilai, sum(Budget) as Budget, sum(BudgetTahunIni) as BudgetTahunIni, sum(NilaiBulanLalu) as NilaiBulanLalu, sum(NilaiTahunLalu) as NilaiTahunLalu from (
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
				r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
				s.`KdRekening`, r.`NamaRekening`, SUM(s.Awal$bulan) AS Awal, 
				SUM(Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan) AS Nilai,
				sum(Budget$bulan) as Budget,
				sum($strbudget) as BudgetTahunIni,
				0 AS NilaiBulanLalu, 0 as NilaiTahunLalu
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$tahun' AND s.KdRekening >= '40000000' $where
				GROUP BY s.`KdRekening` having(Awal<>0 or Nilai<>0 or BudgetTahunIni<>0) 
					UNION ALL
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
				r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
				s.`KdRekening`, r.`NamaRekening`, 0 AS Awal, 0 as Nilai, 
				0, 0,
				SUM(Debet$blnlalu-Kredit$blnlalu+AdjDebet$blnlalu-AdjKredit$blnlalu) as NilaiBulanLalu,
				SUM(Awal$bulan+Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan) AS NilaiTahunLalu
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$thnlalu' AND s.KdRekening >= '40000000' $where
				GROUP BY s.`KdRekening` having(NilaiBulanLalu<>0 or NilaiTahunLalu<>0)
				) t group by KdRekening order by  KdRekening ";	
		}else{
			$sql ="Select Parent1, NamaRekening1, Parent2, NamaRekening2, KdRekening, NamaRekening, sum(Awal) as Awal,
				sum(Nilai) as Nilai, sum(Budget) as Budget, sum(BudgetTahunIni) as BudgetTahunIni, sum(NilaiBulanLalu) as NilaiBulanLalu, sum(NilaiTahunLalu) as NilaiTahunLalu from (
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
				r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
				s.`KdRekening`, r.`NamaRekening`, SUM(s.Awal$bulan) AS Awal, 
				SUM(Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan) AS Nilai,
				sum(Budget$bulan) as Budget,
				sum($strbudget) as BudgetTahunIni,
				SUM(Debet$blnlalu-Kredit$blnlalu+AdjDebet$blnlalu-AdjKredit$blnlalu) AS NilaiBulanLalu, 0 as NilaiTahunLalu
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$tahun' AND s.KdRekening >= '40000000' $where
				GROUP BY s.`KdRekening` having(Awal<>0 or Nilai<>0 or NilaiBulanLalu<>0 or BudgetTahunIni<>0) 
					UNION ALL
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
				r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
				s.`KdRekening`, r.`NamaRekening`, 0 AS Awal, 0 as Nilai, 0,0, 0 as NilaiBulanLalu,
				SUM(Awal$bulan+Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan) AS NilaiTahunLalu
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$thnlalu' AND s.KdRekening >= '40000000' $where
				GROUP BY s.`KdRekening` having(NilaiTahunLalu<>0)
				) t group by KdRekening order by  KdRekening ";	
		}
		
		if($user=="mechael0101" || $user=="yuri0717"  || $user=="yuri0717" || $user=="jamal3108"){
			echo $sql;
		}
				
		return $this->getArrayResult($sql);
	}
	
	function getReportTrend($tahun, $bulan, $divisi)
	{		
		if($divisi=='' || $divisi=='No'){
			$where = '';
		}else{
			$where=" and d.KdDivisi='$divisi'";
		}
			$sql ="SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
					r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
					s.`KdRekening`, r.`NamaRekening`,
					SUM(Debet01-Kredit01+AdjDebet01-AdjKredit01) AS Nilai1,
					SUM(Debet02-Kredit02+AdjDebet02-AdjKredit02) AS Nilai2,
					SUM(Debet03-Kredit03+AdjDebet03-AdjKredit03) AS Nilai3,
					SUM(Debet04-Kredit04+AdjDebet04-AdjKredit04) AS Nilai4,
					SUM(Debet05-Kredit05+AdjDebet05-AdjKredit05) AS Nilai5,
					SUM(Debet06-Kredit06+AdjDebet06-AdjKredit06) AS Nilai6,
					SUM(Debet07-Kredit07+AdjDebet07-AdjKredit07) AS Nilai7,
					SUM(Debet08-Kredit08+AdjDebet08-AdjKredit08) AS Nilai8,
					SUM(Debet09-Kredit09+AdjDebet09-AdjKredit09) AS Nilai9,
					SUM(Debet10-Kredit10+AdjDebet10-AdjKredit10) AS Nilai10,
					SUM(Debet11-Kredit11+AdjDebet11-AdjKredit11) AS Nilai11,
					SUM(Debet12-Kredit12+AdjDebet12-AdjKredit12) AS Nilai12
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$tahun' AND s.KdRekening >= '40000000' $where
				GROUP BY s.`KdRekening` having(Nilai1<>0  or Nilai2<>0 or Nilai3<>0 or Nilai4<>0 or Nilai5<>0 or Nilai6<>0 or Nilai7<>0
				or Nilai8<>0 or Nilai9<>0 or Nilai10<>0 or Nilai11<>0 or Nilai12<>0) "
				;		
		return $this->getArrayResult($sql);
	}
	
	function getReportByDivisi($tahun, $bulan)
	{
		$sql ="SELECT Parent1, NamaRekening1, Parent2, NamaRekening2, KdRekening, NamaRekening, SUM(NilaiDiv1) AS NilaiDiv1, 
				SUM(NilaiDiv2) AS NilaiDiv2, SUM(NilaiDiv3) AS NilaiDiv3, SUM(NilaiDiv5) AS NilaiDiv5 FROM (
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
				r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
				s.`KdRekening`, r.`NamaRekening`,
				SUM(IF(d.`KdDivisi`=1,Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan,0)) AS NilaiDiv1,
				SUM(IF(d.`KdDivisi`=2 or d.`KdDivisi`=6,Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan,0)) AS NilaiDiv2,
				SUM(IF(d.`KdDivisi`=3,Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan,0)) AS NilaiDiv3,
				SUM(IF(d.`KdDivisi`=5,Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan,0)) AS NilaiDiv5
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				INNER JOIN subdivisi d ON s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$tahun' AND s.KdRekening >= '40000000'
				GROUP BY s.`KdRekening`, d.`KdDivisi` HAVING(NilaiDiv1<>0 OR NilaiDiv2<>0 OR NilaiDiv3<>0 OR NilaiDiv5<>0) ORDER BY s.KdRekening) t 
				GROUP BY Parent1, Parent2, KdRekening";
				
		return $this->getArrayResult($sql);
	}
	
	function getNetSales($tahun, $bulan, $divisi)
	{
		if($divisi=='' || $divisi=='No'){
			$where = '';
		}else{
			$where=" and d.KdDivisi='$divisi'";
		}
		
		$sql ="SELECT SUM(s.Awal$bulan+Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan)*-1 AS NetSalesYTD, 
				SUM(Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan)*-1 AS NetSales,
				sum(Debet01-Kredit01+AdjDebet01-AdjKredit01)*-1 as NetSales01,
				sum(Debet02-Kredit02+AdjDebet02-AdjKredit02)*-1 as NetSales02,
				sum(Debet03-Kredit03+AdjDebet03-AdjKredit03)*-1 as NetSales03,
				sum(Debet04-Kredit04+AdjDebet04-AdjKredit04)*-1 as NetSales04,
				sum(Debet05-Kredit05+AdjDebet05-AdjKredit05)*-1 as NetSales05,
				sum(Debet06-Kredit06+AdjDebet06-AdjKredit06)*-1 as NetSales06,
				sum(Debet07-Kredit07+AdjDebet07-AdjKredit07)*-1 as NetSales07,
				sum(Debet08-Kredit08+AdjDebet08-AdjKredit08)*-1 as NetSales08,
				sum(Debet09-Kredit09+AdjDebet09-AdjKredit09)*-1 as NetSales09,
				sum(Debet10-Kredit10+AdjDebet10-AdjKredit10)*-1 as NetSales10,
				sum(Debet11-Kredit11+AdjDebet11-AdjKredit11)*-1 as NetSales11,
				sum(Debet12-Kredit12+AdjDebet12-AdjKredit12)*-1 as NetSales12
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$tahun' AND r2.Parent='40' $where ";
				
		$row = $this->getArrayResult($sql);
		return $row[0];
	}
    
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>