<?php

class report_compliment_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getReport($pilihan,$start_date,$end_date,$divisi,$kasir) 
    {
    	$where_date ="";
    	if($start_date!="" && $end_date!="" && $start_date!="0000-00-00" && $end_date!="0000-00-00")
    	{
			$where_date ="AND Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
		}
		
    	$where_divisi ="";
    	if($divisi)
    	{
            $where_divisi = "AND divisi.KdDivisi = '".$divisi."' "; 
		}
		
        $where_kasir = "";
        if($kasir!="")
        {
            $where_kasir = "AND Kasir = '".$kasir."' ";
        }
        
		$sql = "";
		if($pilihan=="transaksi")
		{
            $sql = "
            	SELECT 
				  divisi.`KdDivisi`,
				  divisi.`NamaDivisi`,
				  NoKassa,
			      NoStruk,
			      Tanggal,
			      Kasir,
			      if(kassa.PriceIncludeTax='T',Discount,Discount/1.1) as Discount
				FROM
				  (SELECT 
				    NoKassa,
				    NoStruk,
				    Tanggal,
				    Kasir,
				    Discount 
				  FROM
				    transaksi_header h1 
				  WHERE 
				    1 
				    ".$where_date." 
				    AND DPP = '0'
				  UNION ALL
					SELECT 
				    NoKassa,
				    NoStruk,
				    Tanggal,
				    Kasir,
				    Discount 
				  FROM
				    transaksi_header_sunset h2 
				  WHERE 
				    1 
				    ".$where_date." 
				    AND DPP = '0') h 
				  LEFT JOIN kassa 
				    ON kassa.`id_kassa` = h.NoKassa 
				  LEFT JOIN divisi 
				    ON divisi.`KdDivisi` = kassa.`KdDivisi`
				    ".$where_divisi."
				WHERE 
				  1
				  ".$where_kasir." 
				ORDER BY 
				  NamaDivisi,
				  Tanggal
            ";
		
		}
		elseif($pilihan=="detail")
		{
			$sql ="";	
		}
		elseif($pilihan=="barang")
		{
			$sql ="";
		
		}
    	
        return $this->getArrayResult($sql);
    }

	function getVoucher($start_date,$end_date,$arr_struck)
	{
		$where_date ="";
    	if($start_date!="" && $start_date!="0000-00-00" && $end_date!="" && $end_date!="0000-00-00")
    	{
			$where_date ="AND transaksi_detail_voucher.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
		}
		
		$where_struck="";
		if(count($arr_struck)>0)
		{
			$where_struck = $arr_struck;
		}
		
		$sql="
			SELECT 
			  transaksi_detail_voucher.NoKassa,
			  transaksi_detail_voucher.NoStruk,
			  DATE_FORMAT(transaksi_detail_voucher.Tanggal, '%d-%m-%Y') AS Tanggal,
			  transaksi_detail_voucher.Jenis,
			  transaksi_detail_voucher.NomorVoucher,
			  transaksi_detail_voucher.NilaiVoucher 
			FROM
			  transaksi_detail_voucher 
			WHERE 
			  1
			  ".$where_date."
			  ".$where_struck." 
			ORDER BY transaksi_detail_voucher.Tanggal DESC
		";
		return $this->getArrayResult($sql);
		
	}
    
    function getType($id="")
    {
    	$where_divisi="";
    	if(count($id)>0)
    	{
			$where_divisi = "AND divisi.KdDivisi IN (".$id.")";
		}
		
		$sql = "SELECT KdDivisi, NamaDivisi FROM divisi WHERE 1 ".$where_divisi." ORDER BY NamaDivisi ASC";
		//echo $sql;die;--
		return $this->getArrayResult($sql);
	}
    
    function getKasir($id="")
    {
    	$where_kasir="";
    	if(count($id)>0)
    	{
			$where_kasir = "AND user.UserLevel IN (".$id.")";
		}
		
		$sql = "
            SELECT 
              user.UserLevel,
              userlevels.UserLevelName,
              user.employee_nik,
              user.UserName,
              employee.employee_name 
            FROM
              user 
              LEFT JOIN userlevels 
                ON user.UserLevel = userlevels.UserLevelID 
              LEFT JOIN employee 
                ON user.employee_nik = employee.employee_nik 
            WHERE 1 
               ".$where_kasir."
            GROUP BY user.employee_nik 
            ORDER BY user.UserName ASC
		";      
		return $this->getArrayResult($sql);
	}
    
    function getKasirFromHeader()
    {		
		$sql = "
            SELECT 
              transaksi_header.Kasir,
              employee.employee_name 
            FROM
              transaksi_header 
              LEFT JOIN employee 
                ON transaksi_header.Kasir = employee.username 
            WHERE 1 
            GROUP BY Kasir 
            ORDER BY Kasir ASC
        ";
		
		return $this->getArrayResult($sql);
	}
    
    function getArrayResult($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) 
    {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) 
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() 
    {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>