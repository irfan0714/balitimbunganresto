<?php
class Report_fluktuatif_harga_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getArrayGudang($arrdata)
    {
    	$where = "";
    	if(count($arrdata)*1>0)
    	{
			$where= $arrdata;	
		}
		
    	$sql = "
    		SELECT 
			  gudang.KdGudang,
			  gudang.Keterangan 
			FROM
			  gudang 
			WHERE 
			  1
			  ".$where." 
			ORDER BY 
			  gudang.Keterangan ASC
		";
		
		return $this->getArrayResult($sql);
	}
	
	function getArraySupplier()
    {
    	$sql = "
    		SELECT 
			  supplier.KdSupplier,
			  supplier.Nama 
			FROM
			  supplier 
			WHERE 1 
			  AND supplier.StatAktif = 'Y' 
			ORDER BY supplier.Nama ASC,
			  supplier.KdSupplier ASC
		";
		
		return $this->getArrayResult($sql);
	}

	function getArrayBarangSupplier($kd_supplier)
	{
		/*
		$sql =" SELECT b.`KdSupplier`, c.`Nama`, a.`PCode`, d.`NamaLengkap` 
				FROM db_natura.`trans_order_barang_detail` a 
				INNER JOIN db_natura.`trans_order_barang_header` b ON a.`NoDokumen` = b.`NoDokumen`
				LEFT JOIN db_natura.`supplier` c ON b.`KdSupplier` = c.`KdSupplier`
				LEFT JOIN db_natura.`masterbarang` d ON a.`PCode` = d.`PCode`
				WHERE b.`KdSupplier` = '$kd_supplier' ORDER BY d.`NamaLengkap` ASC";
		*/

		$sql =" SELECT d.`KdBarang`, m.`NamaLengkap` , m.`PCode`, h.`KdSupplier`
				FROM `invoice_pembelian_detail` d 
				INNER JOIN `invoice_pembelian_header` h ON d.`NoFaktur` = h.`NoFaktur`
				INNER JOIN masterbarang m ON d.`KdBarang` = m.`PCode`
				WHERE h.`KdSupplier` = '$kd_supplier'
				GROUP BY d.`KdBarang` ORDER BY m.`NamaLengkap`";

		return $this->getArrayResult($sql);
	}
	function getArrayCurrency()
    {
    	$sql = "
    		SELECT 
			  mata_uang.Kd_Uang, 
			  mata_uang.Keterangan 
			FROM
			  mata_uang 
			WHERE 1 
			ORDER BY mata_uang.id ASC
		";
		
		return $this->getArrayResult($sql);
	}

    function getArrayHeader($start_date,$end_date,$supplier,$PCode)
    {
    	$mylib = new globallib();
    	
    	$start_date = $mylib->ubah_tanggal($start_date);
        $end_date = $mylib->ubah_tanggal($end_date);	
        
        $where = "";
		
        if($supplier!="all")
        {
			//$where .= "AND trans_order_barang_header.KdSupplier='".$supplier."'";
		}
    	
		$sql="	SELECT DATE_FORMAT(h.`Tanggal`,'%d/%m/%Y') AS Tanggal,h.`NoPO`, s.`Nama`, m.`PCode`, m.`NamaLengkap`,d.`Qty`, d.`Satuan`, d.`Harga` 
				FROM `invoice_pembelian_detail` d 
				INNER JOIN `invoice_pembelian_header` h ON d.`NoFaktur` = h.`NoFaktur` 
				INNER JOIN `masterbarang` m ON d.`KdBarang` = m.`PCode` 
				INNER JOIN `supplier` s ON s.`KdSupplier` = h.`KdSupplier` 
				WHERE h.`KdSupplier` = '$supplier' AND m.`PCode` = '$PCode' 
				AND h.`Tanggal` BETWEEN '$start_date' AND '$end_date'  ORDER BY h.`Tanggal` ASC ";

		return $this->getArrayResult($sql);	
	}
	
	function getArrayDetail($arrdata)
	{
		$where = "";
		if($arrdata)
		{
			$where = $arrdata;
		}
		
		$sql = "
			SELECT 
			  trans_order_barang_detail.*,
			  masterbarang.NamaLengkap as nama_barang
			FROM
			  trans_order_barang_detail 
			  INNER JOIN masterbarang 
			    ON trans_order_barang_detail.PCode = masterbarang.PCode 
			WHERE 
			  1 
			  ".$where."
			ORDER BY trans_order_barang_detail.Sid DESC
		";
		
		return $this->getArrayResult($sql);	
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>