<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_mutasi_antargudang_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	
	function getReport($tgldari, $tglsampai, $gudangasal, $jenis) {
	 	if($gudangasal=='00'){
			$wheregudang = '';
		}else{
			$wheregudang = " and h.KdGudang_From = '$gudangasal'";
		}
		
		if($jenis=='A'){
			$wherejenis = '';
		}else{
			
			$wherejenis = " and h.MovingConfirmation=0";
		}
		$sql ="SELECT h.`NoDokumen`, h.`TglDokumen`, i.purpose , gdari.`Keterangan` AS GudangAsal, gtujuan.`Keterangan` AS GudangTujuan, h.`Keterangan`, 
				h.`MovingConfirmation`, d.`PCode`, b.`NamaLengkap`, d.`Qty`, d.`Satuan`
				FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen`=d.`NoDokumen`
				INNER JOIN gudang gdari ON h.`KdGudang_From`=gdari.`KdGudang`
				INNER JOIN gudang gtujuan ON h.`KdGudang_To`=gtujuan.`KdGudang`
				INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
				INNER JOIN intmutpurpose i ON h.`PurposeId`=  i.purposeid
				WHERE h.`TglDokumen` BETWEEN '$tgldari' AND '$tglsampai' AND h.`Status`=1 $wheregudang $wherejenis";
		//echo $jenis; die();		
		$query = $this->db->query($sql);
        return $query->result_array();
	}
	
	function listgudang($username, $userlevel){
		$sql = "SELECT g.`KdGudang`, g.`Keterangan` FROM gudang g INNER JOIN gudang_admin ga ON g.`KdGudang`=ga.`KdGudang`
					WHERE ga.`UserName`='$username'";
		
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
		if($userlevel==-1 OR $userlevel==5){
			$result[] = array('KdGudang' => '00', 'Keterangan'=>'Semua');
		}
        return $result;
	}
    
}
?>