<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_pi_marketing_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {

		$sql ="SELECT a.`NoDokumen`,a.`RGNo`,g.`PONo`,p.`NoPr`,a.`NoProposal`, a.`TglDokumen`, s.`Nama`, a.`Keterangan` , r.`KdRekening`, r.`NamaRekening`, sd.`NamaSubDivisi` , k.`Deskripsi`, k.`Jumlah` 
		FROM pi_marketing a 
				INNER JOIN pi_marketing_detail_akun k ON a.`NoDokumen`= k.`NoDokumen`
				INNER JOIN rg_marketing g ON g.`NoDokumen`= a.`RGNo`
				INNER JOIN po_marketing p ON p.`NoDokumen`= g.`PONo`
				INNER JOIN rekening r ON k.`KdRekening`= r.`KdRekening`
				INNER JOIN supplier s ON a.`KdSupplier`=s.`KdSupplier`
				INNER JOIN subdivisi sd ON k.`KdSubDivisi`=sd.`KdSubDivisi`
				WHERE a.`TglDokumen` BETWEEN '$tgldari' AND '$tglsampai' AND a.`status` = 1 
				ORDER BY a.`NoDokumen`, a.`TglDokumen` 
			  ";
		//echo $sql;die;		
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}    
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */