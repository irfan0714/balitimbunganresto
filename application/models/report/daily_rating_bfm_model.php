<?php
class daily_rating_bfm_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	
	
	
	function getMakanan($v_date_from,$v_date_to,$jenis ){
		$sql = "SELECT 
				  c.`NamaGroupBarang`,
				  a.`PCode`,
				  b.`NamaLengkap`, 
				  SUM(a.`Qty`) AS Qty,
				  SUM(a.`Netto`) AS Netto,
				  '0' AS Berat,
				  '0' AS NettoBerat
				FROM
				  transaksi_detail a 
				  INNER JOIN masterbarang b 
				    ON a.`PCode` = b.`PCode` 
				  INNER JOIN `groupbarang` c 
				    ON b.`KdGroupBarang` = c.`KdGroupBarang`  
				WHERE a.`Tanggal` BETWEEN '$v_date_from' 
				  AND '$v_date_to' AND a.Status='1' AND b.`KdGroupBarang`='2' GROUP BY a.`PCode` ORDER BY `Netto` DESC,NettoBerat DESC LIMIT 0,10;";
        
        return $this->getArrayResult($sql);
	}
	
	function getMinuman($v_date_from,$v_date_to,$jenis ){
		$sql = "SELECT 
				  c.`NamaGroupBarang`,
				  a.`PCode`,
				  b.`NamaLengkap`, 
				  SUM(a.`Qty`) AS Qty,
				  ROUND(SUM(IF(a.berat>0,(a.Qty*a.`Berat`/100)*a.`Harga`,a.Qty*a.`Harga`))) AS Netto,
				  ROUND(SUM(IF(a.berat>0,a.Qty*a.`Berat`,0))) AS Berat,
				  ROUND(SUM(IF(a.berat>0,(a.Qty*a.`Berat`/100)*a.`Harga`,0))) AS NettoBerat
				FROM
				  transaksi_detail a 
				  INNER JOIN masterbarang b 
				    ON a.`PCode` = b.`PCode` 
				  INNER JOIN `groupbarang` c 
				    ON b.`KdGroupBarang` = c.`KdGroupBarang` 
				WHERE a.`Tanggal` BETWEEN '$v_date_from' 
				  AND '$v_date_to' AND a.Status='1' AND b.`KdGroupBarang`='1' GROUP BY a.`PCode` ORDER BY `Netto` DESC,NettoBerat DESC LIMIT 0,10;";
        
        return $this->getArrayResult($sql);
	}
	
	
	
	
		
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'rating_daily_bfm'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function voucher($v_date){
		$mylib = new globallib();
		$sql = "SELECT novoucher, nominal, terpakai FROM voucher WHERE expdate='".$mylib->ubah_tanggal($v_date)."' and Jenis<>5 order by novoucher";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>