<?php

class Bukubesar_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getListTahun() {
        $sql = "select distinct Tahun from saldo_gl order by Tahun";
        return $this->getArrayResult($sql);
    }
    
     function getListDivisi() {
        $sql = "select KdDivisi, NamaDivisi from divisi order by NamaDivisi";
        return $this->getArrayResult($sql);
    }

    function getDept() {
        $sql = "select KdDepartemen,NamaDepartemen from departemen order by KdDepartemen";
        return $this->getArrayResult($sql);
    }

    function getRekening($filter='') {
    	if($filter ==''){
			$where  = '';
		}else{
			$where = " and (KdRekening like '%$filter%' or NamaRekening like '%$filter%') ";
		}
        $sql = "select KdRekening,CONCAT(KdRekening,'-',NamaRekening) as NamaRekening from rekening 
        		where tingkat='3' $where order by KdRekening";
        		
        return $this->getArrayResult($sql);
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row;
    }

    function getDate() {
        $sql = "select PeriodeGL from aplikasi order by Tahun desc limit 0,1";
        $row = $this->getArrayResult($sql);
        return $row[0]['PeriodeGL'];
    }

    
    function getDetailTrans($tahun, $bulan1, $bulan2, $kddivisi, $kdrekening1, $kdrekening2) {
    	if($kdrekening2!=''){
			$where_rek = " and r.KdRekening between '$kdrekening1' and '$kdrekening2' ";
			$where_rekawal = " and g.KdRekening between '$kdrekening1' and '$kdrekening2' ";
		}elseif($kdrekening1 != ''){
			$where_rek = " and r.KdRekening = '$kdrekening1' ";
			$where_rekawal = " and g.KdRekening = '$kdrekening1' ";
		}else{
			$where_rek = "";
			$where_rekawal = "";
		}
		
        if ($kddivisi == '' or $kddivisi=='No') {
        	$where_div = "";
            $sql = "SELECT r.KdRekening, r.NamaRekening, h.TglTransaksi,h.JenisJurnal,h.KodeJurnal,h.NoReferensi,h.NoTransaksi,
				d.KeteranganDetail,sa.SaldoAwal, d.Debit,d.Kredit ,r.KdRekening,r.NamaRekening, dv.NamaDivisi, s.`NamaSubDivisi`
				FROM (
					SELECT g.`KdRekening`, SUM(Awal$bulan1) AS SaldoAwal FROM saldo_gl g 
					WHERE g.Tahun='$tahun' $where_rekawal
				GROUP BY g.KdRekening) sa 
				LEFT JOIN jurnaldetail d ON sa.KdRekening=d.KdRekening
				INNER JOIN jurnalheader h ON h.KdDepartemen=d.KdDepartemen AND h.noreferensi=d.noreferensi AND h.bulan=d.bulan AND h.tahun=d.tahun
				INNER JOIN rekening r ON sa.KdRekening=r.KdRekening
				INNER JOIN subdivisi s ON d.kdSubdivisi=s.KdSubdivisi
				INNER JOIN divisi dv ON s.KdDivisi=dv.KdDivisi
				WHERE h.Tahun='$tahun' AND h.Bulan BETWEEN '$bulan1' AND '$bulan2'  $where_rek $where_div
				ORDER BY r.KdRekening, h.TglTransaksi,h.JenisJurnal,h.KodeJurnal,h.NoReferensi,h.NoTransaksi";
        } else {
            $where_div = " and dv.KdDivisi = '$kddivisi' ";
            $sql = "SELECT r.KdRekening, r.NamaRekening, h.TglTransaksi,h.JenisJurnal,h.KodeJurnal,h.NoReferensi,h.NoTransaksi,
				d.KeteranganDetail,sa.SaldoAwal, d.Debit,d.Kredit ,r.KdRekening,r.NamaRekening, dv.NamaDivisi, s.`NamaSubDivisi`
				FROM (
					SELECT g.`KdRekening`, g.KdSubDivisi, SUM(Awal$bulan1) AS SaldoAwal FROM saldo_gl g 
					WHERE g.Tahun='$tahun' $where_rekawal
				GROUP BY g.KdRekening, g.KdSubDivisi) sa 
				LEFT JOIN jurnaldetail d ON sa.KdRekening=d.KdRekening and sa.KdSubDivisi=d.KdSubDivisi
				INNER JOIN jurnalheader h ON h.KdDepartemen=d.KdDepartemen AND h.noreferensi=d.noreferensi AND h.bulan=d.bulan AND h.tahun=d.tahun
				INNER JOIN rekening r ON sa.KdRekening=r.KdRekening
				INNER JOIN subdivisi s ON d.kdSubdivisi=s.KdSubdivisi
				INNER JOIN divisi dv ON s.KdDivisi=dv.KdDivisi
				WHERE h.Tahun='$tahun' AND h.Bulan BETWEEN '$bulan1' AND '$bulan2'  $where_rek $where_div
				ORDER BY r.KdRekening, h.TglTransaksi,h.JenisJurnal,h.KodeJurnal,h.NoReferensi,h.NoTransaksi";
        }
        
        		
        return $this->getArrayResult($sql);
    }
    
    
    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
    }

}

?>