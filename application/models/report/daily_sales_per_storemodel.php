<?php
class daily_sales_per_storemodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
    
    function ListStore(){
		$sql = "select KodeStore, NamaStore, IsRestoCafe from store";
		return $this->getArrayResult($sql);
		
	}
		
	function getsales($v_date_from,$v_date_to){
		$sql = "SELECT h.Tanggal, s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header h 
				  INNER JOIN transaksi_detail d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk` 
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore` 
				WHERE  h.`Tanggal` between '$v_date_from' and '$v_date_to'
				  AND h.`Status` = 1 
				GROUP BY h.Tanggal, s.`KodeStore`
				UNION ALL
				SELECT h.Tanggal, s.`KodeStore`, s.`NamaStore`,
				  SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
				FROM
				  transaksi_header_sunset h 
				  INNER JOIN transaksi_detail_sunset d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk` 
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
				  INNER JOIN store s ON k.`KdStore`=s.`KodeStore` 
				WHERE h.`Tanggal` between '$v_date_from' and '$v_date_to'
				  AND h.`Status` = 1 
				GROUP BY h.Tanggal, s.`KodeStore`";
		return $this->getArrayResult($sql);
	}
	
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_daily'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>