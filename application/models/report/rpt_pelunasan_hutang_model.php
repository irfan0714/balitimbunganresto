<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_pelunasan_hutang_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {

		$sql ="
				SELECT 
				  z.`NoTransaksi`,
				  z.`NoPaymentVoucher`,
				  DATE_FORMAT(z.TglDokumen, '%d-%m-%Y') AS Tanggal,
				  j.`Nama`,
				  z.`Keterangan`,
				  p.`NoFaktur`,
				  i.NoPenerimaan AS NoRG,
				  i.`NoPO`,
				  p.`NilaiFaktur`,
				  p.`NilaiBayar` 
				FROM
				  pelunasan_hutang_detail p 
				  LEFT JOIN invoice_pembelian_header i 
				    ON p.`NoFaktur` = i.`NoFaktur` 
				  INNER JOIN `pelunasan_hutang_header` z 
				    ON z.`NoTransaksi` = p.`NoTransaksi` 
				  INNER JOIN supplier j 
				    ON j.`KdSupplier` = z.`KdSupplier` 
				WHERE 1 
				  AND Z.`TglDokumen` BETWEEN '".$tgldari."' 
				  AND '".$tglsampai."' 
				  and z.Status=1
				  AND P.`NilaiBayar` > 0 ORDER BY j.`Nama` ASC;
			  ";
		//echo $sql;die;		
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
    }
    
    function total($tgldari, $tglsampai){
		$sql="
		SELECT 
		  SUM(p.`NilaiFaktur`) AS total_faktur,
		  SUM(p.`NilaiBayar`) AS total_bayar
		FROM
		  pelunasan_hutang_detail p 
		  LEFT JOIN invoice_pembelian_header i 
		    ON p.`NoFaktur` = i.`NoFaktur` 
		  INNER JOIN `pelunasan_hutang_header` z 
		    ON z.`NoTransaksi` = p.`NoTransaksi` 
		  INNER JOIN supplier j 
		    ON j.`KdSupplier` = z.`KdSupplier` 
		WHERE 1 
		    AND Z.`TglDokumen` BETWEEN '".$tgldari."' 
			AND '".$tglsampai."'
			and z.Status=1 
		  AND P.`NilaiBayar` > 0 ORDER BY j.`Nama` ASC;
		";
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}
    
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */