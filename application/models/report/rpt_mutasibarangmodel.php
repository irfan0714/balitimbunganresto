<?php

//purwanto on sept 16
class Rpt_mutasibarangmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }

    function getReport_header($tglawal, $tglakhir) {
        $sql = "SELECT 
                        id_mutasi, DATE_FORMAT(Tanggal,'%d-%m-%Y') AS Tanggal, MPCode, masterbarang.NamaLengkap AS NamaPaket, Qty, Jenis_mutasi, mutasi_paket.Keterangan, gudang.Keterangan AS namagudang 
                      FROM
                        mutasi_paket 
                        LEFT JOIN gudang 
                          ON mutasi_paket.KdGudang = gudang.KdGudang
                        LEFT JOIN masterbarang
                          ON mutasi_paket.MPCode = masterbarang.PCode
                        WHERE DATE_FORMAT(mutasi_paket.Tanggal,'%Y-%m-%d') BETWEEN '".$tglawal."' AND '".$tglakhir."'";
        //echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_Detail($bulan, $tahun, $gudang, $divisi, $tabel_field, $tabel_field_val, $search_by) {
        /*$sql = "
        SELECT a.`KodeBarang`,c.`NamaLengkap`,
        ROUND(SUM(IF( b.`Tahun`='".$tahun."' AND b.`KdGudang`='".$gudang."', b.".$tabel_field.", 0)),0) AS saldo_awal,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='R' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_R,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='RG' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_RG,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='MM' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_MM,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='MC' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_MC,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='SO' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_SO,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='DL' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_DL,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='PL' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_PL,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='MP' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_MP,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='DO' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_DO,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`KdTransaksi`='SR' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS jml_SR,
        SUM(IF( MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' AND a.`Gudang`='".$gudang."', a.`Qty`, 0)) AS saldo_akhir
        FROM `mutasi` a 
        LEFT JOIN `stock` b ON a.`KodeBarang`=b.`PCode` AND a.`Gudang`=b.`KdGudang`
        INNER JOIN `masterbarang` c ON c.`PCode`=a.`KodeBarang`
        WHERE MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' GROUP BY a.`KodeBarang` limit 10;";*/
        
        if($search_by=="gudang"){
		        $sql="
		        		SELECT 
						  KodeBarang AS PCode,
						  NamaLengkap,
						  SatuanSt,
						  KdKategori,
						  NamaKategori,
						  AVG(saldo_awal) AS saldo_awal,
						  AVG(saldo_awal_val) AS saldo_awal_val,
						  SUM(jml_R) AS jml_R,
						  SUM(jml_R_val) AS jml_R_val,
						  SUM(jml_RG) AS jml_RG,
						  SUM(jml_RG_val) AS jml_RG_val,
						  SUM(jml_MM_PL) AS jml_MM_PL,
						  SUM(jml_MM_PL_val) AS jml_MM_PL_val,
						  SUM(jml_MC_DL) AS jml_MC_DL,
						  SUM(jml_MC_DL_val) AS jml_MC_DL_val,
						  SUM(jml_SO) AS jml_SO,
						  SUM(jml_SO_val) AS jml_SO_val,
						  SUM(jml_MX) AS jml_MX,
						  SUM(jml_MX_val) AS jml_MX_val,
						  SUM(jml_MP_IN) AS jml_MP_IN,
						  SUM(jml_MP_IN_val) AS jml_MP_IN_val,
						  SUM(jml_MP_OUT) AS jml_MP_OUT,
						  SUM(jml_MP_OUT_val) AS jml_MP_OUT_val,
						  SUM(jml_FG) AS jml_FG,
						  SUM(jml_FG_val) AS jml_FG_val,
						  SUM(jml_RB) AS jml_RB,
						  SUM(jml_RB_val) AS jml_RB_val,
						  SUM(jml_SR) AS jml_SR,
						  SUM(jml_SR_val) AS jml_SR_val,
						  sum(jml_ADJ)  as jml_ADJ,
						  sum(jml_ADJ_val) as jml_ADJ_val
						FROM
						  (SELECT 
						    b.PCode AS `KodeBarang`,
						    c.`NamaLengkap`,
						    c.SatuanSt,
						    c.`KdKategori`,
						    d.`NamaKategori`,
						    ROUND(SUM( b.".$tabel_field."), 0) AS saldo_awal,
						    ROUND(SUM( b.".$tabel_field_val."), 0) AS saldo_awal_val,
						    SUM(
						      IF(a.`KdTransaksi` = 'R', a.`Qty`, 0)
						    ) AS jml_R,
						    SUM(
						      IF(a.`KdTransaksi` = 'R', a.Nilai, 0)
						    ) AS jml_R_val,
						    SUM(
						      IF(a.`KdTransaksi` = 'RG', a.`Qty`, 0)
						    ) AS jml_RG,
						    SUM(
						      IF(a.`KdTransaksi` = 'RG', a.Nilai, 0)
						    ) AS jml_RG_val,
						    SUM(
						      IF(a.`KdTransaksi` = 'PL', a.`Qty`, 0)
						    ) + SUM(
						      IF(a.`KdTransaksi` = 'MM', a.`Qty`, 0)
						    ) AS jml_MM_PL,
						    SUM(
						      IF(a.`KdTransaksi` = 'PL', a.Nilai, 0)
						    ) + SUM(
						      IF(a.`KdTransaksi` = 'MM', a.Nilai, 0)
						    ) AS jml_MM_PL_val,
						    SUM(
						      IF(
						        a.`KdTransaksi` = 'MP' && a.Jenis = 'I',
						        a.`Qty`,
						        0
						      )
						    ) AS jml_MP_IN,
						    SUM(
						      IF(
						        a.`KdTransaksi` = 'MP' && a.Jenis = 'I',
						        a.Nilai,
						        0
						      )
						    ) AS jml_MP_IN_val,
						    SUM(
						      IF(a.`KdTransaksi` = 'DL', a.`Qty`, 0)
						    ) + SUM(
						      IF(a.`KdTransaksi` = 'MC', a.`Qty`, 0)
						    ) AS jml_MC_DL,
						    SUM(
						      IF(a.`KdTransaksi` = 'DL', a.Nilai, 0)
						    ) + SUM(
						      IF(a.`KdTransaksi` = 'MC', a.Nilai, 0)
						    ) AS jml_MC_DL_val,
						    SUM(
						      IF(
						        a.`KdTransaksi` = 'MP' && a.Jenis = 'O',
						        a.`Qty`,
						        0
						      )
						    ) AS jml_MP_OUT,
						    SUM(
						      IF(
						        a.`KdTransaksi` = 'MP' && a.Jenis = 'O',
						        a.Nilai,
						        0
						      )
						    ) AS jml_MP_OUT_val,
						    SUM(
						      IF(
						        a.`KdTransaksi` = 'SO',
						        IF(a.Jenis = 'I', a.`Qty`, a.Qty * - 1),
						        0
						      )
						    ) AS jml_SO,
						    SUM(
						      IF(
						        a.`KdTransaksi` = 'SO',
						        IF(a.Jenis = 'I', a.Nilai, - 1*a.Nilai),
						        0
						      )
						    ) AS jml_SO_val,
						    SUM(
						      IF(a.`KdTransaksi` = 'MX', a.`Qty`, 0)
						    ) AS jml_MX,
						    SUM(
						      IF(a.`KdTransaksi` = 'MX', a.Nilai, 0)
						    ) AS jml_MX_val,
						    SUM(
						      IF(a.`KdTransaksi` = 'FG', a.`Qty`, 0)
						    ) AS jml_FG,
						    SUM(
						    	IF(a.`KdTransaksi` = 'FG', a.Nilai, 0)
						    ) AS jml_FG_val,
						    SUM(
								IF(a.`KdTransaksi` = 'RB', a.`Qty`, 0)
							) AS jml_RB,
							SUM(
								IF(a.`KdTransaksi` = 'SR', a.`Qty`, 0)
							) AS jml_SR,
							SUM(
								IF(a.`KdTransaksi` = 'RB', a.Nilai, 0)
							) AS jml_RB_val,
							SUM(
								IF(a.`KdTransaksi` = 'SR', a.Nilai, 0)
							) AS jml_SR_val,
						    sum(IF(a.`KdTransaksi` = 'AD', if(a.Jenis='I', a.`Qty`,a.Qty*-1), 0)) as jml_ADJ,
						    sum(IF(a.`KdTransaksi` = 'AD', if(a.Jenis='I',a.`NilaiAdj`,a.NilaiAdj*-1), 0)) as jml_ADJ_val
						    
						  FROM
						    stock b 
						    INNER JOIN masterbarang c 
						      ON b.PCode = c.PCode 
						      AND b.Tahun = '$tahun' 
						      AND b.KdGudang = '$gudang' 
						    LEFT JOIN kategori d 
						      ON d.`KdKategori` = c.`KdKategori` 
						    LEFT JOIN 
						      (SELECT 
						        KodeBarang,
						        IF(Gudang = '', GudangTujuan, Gudang) AS KdGudang,
						        KdTransaksi,
						        SUM(Qty) AS Qty,
						        SUM(Qty*Nilai) AS Nilai,
						        sum(NilaiAdj) as NilaiAdj,
						        Jenis 
						      FROM
						        mutasi 
						      WHERE YEAR(tanggal) = '$tahun' 
						        AND MONTH(tanggal) = '$bulan' 
						        AND Gudang = '$gudang' 
						      GROUP BY KodeBarang,
						        KdGudang,
						        KdTransaksi,
						        Jenis) a 
						      ON b.PCode = a.KodeBarang 
						  GROUP BY b.Pcode,
						    a.KdTransaksi,
						    a.Jenis) tbl 
						GROUP BY KodeBarang 
		             ";
		}else{
			
			$sql="SELECT k.NamaKategori, t1.PCode, bx.`NamaLengkap` ,bx.SatuanSt, bx.KdKategori,
				sum(saldo_awal) AS saldo_awal,
				sum(saldo_awal_val) AS saldo_awal_val,
				  SUM(COALESCE(jml_R,0)) AS jml_R,
				  SUM(COALESCE(jml_R_val,0)) AS jml_R_val,
				  SUM(COALESCE(jml_RG,0)) AS jml_RG,
				  SUM(COALESCE(jml_RG_val,0)) AS jml_RG_val,
				  SUM(COALESCE(jml_MM_PL,0)) AS jml_MM_PL,
				  SUM(COALESCE(jml_MM_PL_val,0)) AS jml_MM_PL_val,
				  SUM(COALESCE(jml_MC_DL,0)) AS jml_MC_DL,
				  SUM(COALESCE(jml_MC_DL_val,0)) AS jml_MC_DL_val,
				  SUM(COALESCE(jml_SO,0)) AS jml_SO,
				  SUM(COALESCE(jml_SO_val,0)) AS jml_SO_val,
				  SUM(COALESCE(jml_MX,0)) AS jml_MX,
				  SUM(COALESCE(jml_MX_val,0)) AS jml_MX_val,
				  SUM(COALESCE(jml_MP_IN,0)) AS jml_MP_IN,
				  SUM(COALESCE(jml_MP_IN_val,0)) AS jml_MP_IN_val,
				  SUM(COALESCE(jml_MP_OUT,0)) AS jml_MP_OUT,
				  SUM(COALESCE(jml_MP_OUT_val,0)) AS jml_MP_OUT_val,
				  SUM(COALESCE(jml_FG,0))AS jml_FG,
				  SUM(COALESCE(jml_FG_val ,0)) AS jml_FG_val,
				  SUM(COALESCE(jml_RB,0)) AS jml_RB,
				  SUM(COALESCE(jml_RB_val,0)) AS jml_RB_val,
				  SUM(COALESCE(jml_SR,0)) AS jml_SR,
				  SUM(COALESCE(jml_SR_val,0)) AS jml_SR_val,  
				  SUM(COALESCE(jml_ADJ,0)) AS jml_ADJ,
				  SUM(COALESCE(jml_ADJ_val,0)) AS jml_ADJ_val  
				  FROM 
				(SELECT s.`PCode`, SUM(s.$tabel_field) AS saldo_awal, sum(s.$tabel_field_val) as saldo_awal_val, 0 as jml_R, 0 as jml_R_val, 0 as jml_RG, 0 as jml_RG_val,
				0 as jml_MM_PL, 0 as jml_MM_PL_val, 0 as jml_MC_DL, 0 as jml_MC_DL_val, 0 as jml_SO, 0 as jml_SO_val, 0 as jml_MX, 0 as jml_MX_val,
				0 as jml_MP_IN, 0 as jml_MP_IN_val, 0 as jml_MP_OUT, 0 as jml_MP_OUT_val, 0 as jml_FG, 0 as jml_FG_val, 0 as jml_RB, 0 as jml_RB_val, 0 as jml_SR, 0 as jml_SR_val,
				0 as jml_ADJ, 0 as jml_ADJ_val 
				FROM stock s INNER JOIN gudang g ON s.`KdGudang`=g.KdGudang
				 inner join subdivisi dv on g.KdSubDivisi=dv.KdSubDivisi
				WHERE s.tahun='$tahun' and dv.`KdDivisi`='$divisi'
				GROUP BY s.`PCode`
				UNION ALL
				SELECT a.KodeBarang, 0, 0, SUM(
				      IF(a.`KdTransaksi` = 'R', a.`Qty`, 0)
				    ) AS jml_R,
				    SUM(
				      IF(a.`KdTransaksi` = 'R', a.`Qty`*a.Nilai, 0)
				    ) AS jml_R_val,
				    SUM(
				      IF(a.`KdTransaksi` = 'RG', a.`Qty`, 0)
				    ) AS jml_RG,
				    SUM(
				      IF(a.`KdTransaksi` = 'RG', a.`Qty`*a.Nilai, 0)
				    ) AS jml_RG_val,
				    SUM(
				      IF(a.`KdTransaksi` = 'PL', a.`Qty`, 0)
				    ) + SUM(
				      IF(a.`KdTransaksi` = 'MM', a.`Qty`, 0)
				    ) AS jml_MM_PL,
				    SUM(
				      IF(a.`KdTransaksi` = 'PL', a.`Qty`*a.Nilai, 0)
				    ) + SUM(
				      IF(a.`KdTransaksi` = 'MM', a.`Qty`*a.Nilai, 0)
				    ) AS jml_MM_PL_val,
				    SUM(
				      IF(a.`KdTransaksi` = 'DL', a.`Qty`, 0)
				    ) + SUM(
				      IF(a.`KdTransaksi` = 'MC', a.`Qty`, 0)
				    ) AS jml_MC_DL,
				    SUM(
				      IF(a.`KdTransaksi` = 'DL', a.`Qty`*a.Nilai, 0)
				    ) + SUM(
				      IF(a.`KdTransaksi` = 'MC', a.`Qty`*a.Nilai, 0)
				    ) AS jml_MC_DL_val,
				    SUM(
				      IF(
				        a.`KdTransaksi` = 'SO',
				        IF(a.Jenis = 'I', a.`Qty`, a.Qty * - 1),
				        0
				      )
				    ) AS jml_SO,
				    SUM(
				      IF(
				        a.`KdTransaksi` = 'SO',
				        IF(a.Jenis = 'I', a.`Qty`*a.Nilai, a.Qty * - 1*a.Nilai),
				        0
				      )
				    ) AS jml_SO_val,
				    SUM(
				      IF(a.`KdTransaksi` = 'MX', a.`Qty`, 0)
				    ) AS jml_MX,
				    SUM(
				      IF(a.`KdTransaksi` = 'MX', a.`Qty`*a.Nilai, 0)
				    ) AS jml_MX_val,				    
				    SUM(
				      IF(
				        a.`KdTransaksi` = 'MP' && a.Jenis = 'I',
				        a.`Qty`,
				        0
				      )
				    ) AS jml_MP_IN,
				    SUM(
				      IF(
				        a.`KdTransaksi` = 'MP' && a.Jenis = 'I',
				        a.`Qty`*a.Nilai,
				        0
				      )
				    ) AS jml_MP_IN_val,				    
				    SUM(
				      IF(
				        a.`KdTransaksi` = 'MP' && a.Jenis = 'O',
				        a.`Qty`,
				        0
				      )
				    ) AS jml_MP_OUT,
				     SUM(
				      IF(
				        a.`KdTransaksi` = 'MP' && a.Jenis = 'O',
				        a.`Qty`*a.Nilai,
				        0
				      )
				    ) AS jml_MP_OUT_val,
				    SUM(
				      IF(a.`KdTransaksi` = 'FG', a.`Qty`, 0)
				    ) AS jml_FG,
				    SUM(
				      IF(a.`KdTransaksi` = 'FG', a.`Qty`*a.Nilai, 0)
				    ) AS jml_FG_val,
					SUM(
						IF(a.`KdTransaksi` = 'RB', a.`Qty`, 0)
					) AS jml_RB,
					SUM(
						IF(a.`KdTransaksi` = 'SR', a.`Qty`, 0)
					) AS jml_SR,
					SUM(
						IF(a.`KdTransaksi` = 'RB', a.Nilai, 0)
					) AS jml_RB_val,
					SUM(
						IF(a.`KdTransaksi` = 'SR', a.Nilai, 0)
					) AS jml_SR_val,
				    SUM(IF(a.`KdTransaksi` = 'AD', IF(a.Jenis='I',a.`Qty`,a.Qty*-1), 0)) AS jml_ADJ,
				    SUM(IF(a.`KdTransaksi` = 'AD', IF(a.Jenis='I',a.`NilaiAdj`,a.NilaiAdj*-1), 0)) AS jml_ADJ_val 
				FROM mutasi a INNER JOIN masterbarang b2 ON a.kodebarang=b2.pcode
				inner join gudang g on a.Gudang = g.KdGudang
				inner join subdivisi dv on g.KdSubDivisi = dv.KdSubDivisi
				WHERE dv.KdDivisi='$divisi' AND YEAR(a.Tanggal)='$tahun' AND MONTH(a.Tanggal)='$bulan'
				GROUP BY a.kodebarang, a.KdTransaksi, a.Jenis) t1
				INNER JOIN masterbarang bx ON t1.pcode=bx.pcode
				left JOIN kategori k ON bx.kdkategori=k.kdkategori
				GROUP BY bx.KdKategori, t1.pcode"; 
		}	
		// $user = $this->session->userdata('username');
		// if($user == 'jamal3108'){
		// 	echo $sql;
		// }
        
        return $this->getArrayResult($sql);
    }

	function getGudang()
	{
		$sql = "select KdGudang,Keterangan from gudang order by KdGudang";
		return $this->getArrayResult($sql);
	}
	
	function getDivisi()
	{
		$sql = "select KdDivisi,NamaDivisi from divisi order by KdDivisi";
		return $this->getArrayResult($sql);
	}

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

}

?>