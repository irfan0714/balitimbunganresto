<?php
class Report_sales_return_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }

	function getCustomer()
	{
		$sql = "select KdCustomer, Nama from customer order by Nama";
		return $this->getArrayResult($sql);
	}

	function getReport($v_start_date,$v_end_date,$kdcustomer){


		if($kdcustomer==''){
      $wherecustomer = '';
      $wheregroup = '';
      $getsale = "b.`Harga1c` AS salesprice,";
      // $sql = $query1;
    }
		else
    {
      if($kdcustomer=='PT1001'){
      	
      	$tgl_acuan = strtotime('2018-01-02')*1;
      	$a = strtotime($v_start_date)*1;
      	$b = strtotime($v_end_date)*1;
  
      	if($a>=$tgl_acuan AND $tgl_acuan>= $b){
      
    			$wheregroup = " And gd.GroupHargaID='6'";
    		}else{
    		
    			$wheregroup = " And gd.GroupHargaID='1'";
    		}
        
        $getsale = "gd.Harga as salesprice, b.`Harga1c` AS salesprice1,";

        // $sql = $query2;
      }elseif ($kdcustomer=='KA1001') {
        $wheregroup = " And gd.GroupHargaID='3'";
        $getsale = "gd.Harga as salesprice, b.`Harga1c` AS salesprice1,";

        // $sql = $query2;
      }elseif ($kdcustomer=='TO0001') {
        $wheregroup = " And gd.GroupHargaID='5'";
        $getsale = "gd.Harga as salesprice, b.`Harga1c` AS salesprice1,";

        // $sql = $query2;
      }else{
        $wheregroup = '';
        $getsale = "b.`Harga1c` AS salesprice,";

        // $sql = $query1;
      }
			$wherecustomer = " And h.customerid='" . $kdcustomer . "'";
    }



		$sql = "SELECT
				  h.`returndate` AS `dodate`,
				  h.`returnno` AS dono,
				  b.`NamaLengkap`,
				  g.`Keterangan` AS gudang,
				  d.`inventorycode`,
				  d.`quantity`,
          $getsale
				  d.`hppprice` AS `hppvalue`,
				  c.`Nama` AS Pelanggan
				FROM
				  salesreturn h
				  INNER JOIN salesreturndetail d
				    ON h.`returnno` = d.`returnno`
          INNER JOIN groupharga_detail gd
            ON d.inventorycode = gd.PCode
				  INNER JOIN customer c
				    ON h.`customerid` = c.`KdCustomer`
				  INNER JOIN masterbarang b
				    ON d.`inventorycode` = b.`PCode`
				  INNER JOIN gudang g
				    ON h.`warehousecode` = g.`KdGudang`
				WHERE h.`returndate`  between '$v_start_date' and '$v_end_date' $wherecustomer $wheregroup AND h.`status`=1
				order by h.`returndate`, h.`returnno`";

        if($this->session->userdata('username')=='mechael0101'){

        echo $sql;
        }
		return $this->getArrayResult($sql);
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>
