<?php
class report_finance_model  extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
	
	function detail($NoTrans,$Tgltrans)
	{
            $sql = "


SELECT DISTINCT
  a.*, rekening.NamaRekening, kasbank.NamaKasBank, c.KdRekening, c.Jumlah, c.Keterangan
FROM
  (SELECT
    NoDokumen, TglDokumen, KdKasBank, NoBukti
  FROM
    trans_payment_header_cbg
  WHERE 1
    $NoTrans $Tgltrans) a
  INNER JOIN
    (SELECT NoDokumen, KdRekening, Jumlah, keterangan FROM trans_payment_detail_cbg) c
    ON a.NoDokumen = c.NoDokumen
  INNER JOIN rekening
    ON rekening.KdRekening = c.KdRekening
  INNER JOIN kasbank
    ON kasbank.`KdKasBank` = a.KdKasBank
ORDER BY NoDokumen DESC

                ";
		$qry = $this->db->query($sql);//echo $sql;//die();
        $row = $qry->result_array();

        return $row;
	}
	function detailRV($NoTrans,$Tgltrans)
	{
            $sql = "
                SELECT DISTINCT a.*, rekening.NamaRekening,kasbank.NamaKasBank,c.KdRekening,c.Jumlah,c.Keterangan
                FROM( SELECT NoDokumen,TglDokumen,KdKasBank,NoBukti FROM trans_receipt_header_cbg
                WHERE 1 $NoTrans $Tgltrans)a
                INNER JOIN
                (SELECT NoDokumen,KdRekening,Jumlah,Keterangan FROM trans_receipt_detail_cbg)c ON a.NoDokumen=c.NoDokumen
                INNER JOIN rekening ON rekening.KdRekening=c.KdRekening
                INNER JOIN kasbank ON kasbank.`KdKasBank`=a.KdKasBank
                ORDER BY NoDokumen DESC
                ";
		$qry = $this->db->query($sql);//echo $sql;//die();
        $row = $qry->result_array();

        return $row;
	}
	function detailRekening($NoTrans,$Tgltrans)
	{
            $sql = "
                SELECT DISTINCT a.*, rekening.NamaRekening,kasbank.NamaKasBank,c.KdRekening,c.Jumlah,c.Keterangan
                FROM( SELECT NoDokumen,TglDokumen,KdKasBank,NoBukti FROM trans_payment_header_cbg
                WHERE 1 $NoTrans $Tgltrans)a
                INNER JOIN
                (SELECT NoDokumen,KdRekening,Jumlah,Keterangan FROM trans_payment_detail_cbg)c ON a.NoDokumen=c.NoDokumen
                INNER JOIN rekening ON rekening.KdRekening=c.KdRekening
                INNER JOIN kasbank ON kasbank.`KdKasBank`=a.KdKasBank
                ORDER BY c.KdRekening ASC,TglDokumen,NoDokumen
                ";
		$qry = $this->db->query($sql);//echo $sql;//die();
        $row = $qry->result_array();

        return $row;
        }
	function detailRekeningRV($NoTrans,$Tgltrans)
	{
            $sql = "
                SELECT DISTINCT a.*, rekening.NamaRekening,kasbank.NamaKasBank,c.KdRekening,c.Jumlah,c.Keterangan
                FROM( SELECT NoDokumen,TglDokumen,KdKasBank,NoBukti FROM trans_receipt_header_cbg
                WHERE 1 $NoTrans $Tgltrans)a
                INNER JOIN
                (SELECT NoDokumen,KdRekening,Jumlah,Keterangan FROM trans_receipt_detail_cbg)c ON a.NoDokumen=c.NoDokumen
                INNER JOIN rekening ON rekening.KdRekening=c.KdRekening
                INNER JOIN kasbank ON kasbank.`KdKasBank`=a.KdKasBank
                ORDER BY c.KdRekening ASC,TglDokumen,NoDokumen
                ";
		$qry = $this->db->query($sql);//echo $sql;//die();
        $row = $qry->result_array();

        return $row;
	}

	function transaksi($NoTrans,$Tgltrans)
	{
            $sql = "SELECT trans_payment_header_cbg.*,kasbank.NamaKasBank FROM
                   (SELECT * FROM trans_payment_header_cbg WHERE 1 $NoTrans $Tgltrans)trans_payment_header_cbg
                    INNER JOIN kasbank ON kasbank.KdKasBank=trans_payment_header_cbg.KdKasBank
                    ORDER BY NoDokumen DESC";
		$qry = $this->db->query($sql);
                $row = $qry->result_array();
            return $row;
	}
	function transaksiRV($NoTrans,$Tgltrans)
	{
            $sql = "SELECT trans_receipt_header_cbg.*,kasbank.NamaKasBank FROM
                   (SELECT * FROM trans_receipt_header_cbg WHERE 1 $NoTrans $Tgltrans)trans_receipt_header_cbg
                    INNER JOIN kasbank ON kasbank.KdKasBank=trans_receipt_header_cbg.KdKasBank
                    ORDER BY NoDokumen DESC";
		$qry = $this->db->query($sql);
                $row = $qry->result_array();
            return $row;
	}

	function RekapRekening($NoTrans,$Tgltrans)
	{
		$sql = "SELECT a.KdRekening,rekening.NamaRekening,SUM(Jumlah) AS Jumlah
                        FROM( SELECT KdRekening,jumlah FROM trans_payment_detail_cbg)a
                        INNER JOIN trans_payment_header_cbg b ON 1 $NoTrans $Tgltrans
                        INNER JOIN rekening  ON rekening.KdRekening=a.KdRekening
                        GROUP BY a.KdRekening
         ";
		$qry = $this->db->query($sql);//echo $sql;die();
        $row = $qry->result_array();

        return $row;
	}
	function RekapRekeningRV($NoTrans,$Tgltrans)
	{
		$sql = "
                        SELECT c.KdRekening,rekening.NamaRekening,c.Jumlah
                        FROM( SELECT KdRekening,SUM(Jumlah) AS jumlah FROM trans_receipt_detail_cbg a, trans_receipt_header_cbg b
                        WHERE 1 $NoTrans $Tgltrans
                        GROUP BY KdRekening)c
                        INNER JOIN rekening ON rekening.KdRekening=c.KdRekening
         ";
		$qry = $this->db->query($sql);//echo $sql;die();
        $row = $qry->result_array();
        
        return $row;
	}
         function getCustomer(){
                $sql = "SELECT KdCustomer,Nama FROM customer ORDER BY KdCustomer";
            return $this->getArrayResult($sql);
        }

        function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
		
}
?>