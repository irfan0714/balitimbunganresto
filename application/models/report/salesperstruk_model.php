<?php
class salesperstruk_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getsales($v_date_from,$v_date_to){
		$sql = "SELECT KodeStore as KdStore, Tanggal, SUM(Guest) AS JmlGuest, COUNT(NoStruk) AS JmlStruk, SUM(Sales) AS TotSales FROM (
					SELECT
					  s.KodeStore,
					  h.`Tanggal`,
					  h.`NoStruk`,
					  SUM(
						IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)
  					  ) AS Sales,
					  AVG(h.TotalGuest) AS Guest
					FROM
					  transaksi_header h 
					  INNER JOIN transaksi_detail d  ON h.`NoStruk` = d.`NoStruk` and h.nokassa=d.nokassa
					  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa 
					  inner join store s on k.KdStore = s.KodeStore 
					WHERE h.`Tanggal` BETWEEN '$v_date_from' 
					  AND '$v_date_to' 
					  AND h.`Status` = 1 
					GROUP BY h.`Tanggal`, s.KodeStore, h.`NoStruk`
					UNION ALL
					SELECT 
					  s.KodeStore,
					  h.`Tanggal`,
					  h.`NoStruk`,
					  SUM(
						IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)
  					  ) AS Sales,
  					  AVG(h.`TotalGuest`) AS Guest
					FROM
					  transaksi_header_sunset h 
					  INNER JOIN transaksi_detail_sunset d 
					    ON h.`NoKassa` = d.`NoKassa` 
					    AND h.`NoStruk` = d.`NoStruk` 
					  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa 
					  inner join store s on k.KdStore=s.KodeStore
					WHERE h.`Tanggal` BETWEEN '$v_date_from' 
					  AND '$v_date_to' 
					  AND h.`Status` = 1 
					GROUP BY h.`Tanggal`, s.KodeStore, h.`NoStruk` 
					) t GROUP BY Tanggal, KodeStore
					ORDER BY Tanggal, KodeStore";
					
		return $this->getArrayResult($sql);
	}
	
	function ListStore(){
		$user = $this->session->userdata('username');
		
		$sql ="Select s.KodeStore as KdStore, s.NamaStore, IsRestoCafe from store s inner join userstore u on s.KodeStore=u.KodeStore and u.UserName='$user'
				order by urutan";
		return $this->getArrayResult($sql);	
	}
	
	function ListAllStore(){
		$user = $this->session->userdata('username');
		
		$sql ="Select s.KodeStore as KdStore, s.NamaStore, IsRestoCafe from store s order by urutan";
		return $this->getArrayResult($sql);	
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_daily'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>