<?php

class report_kasir_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getReport($pilihan,$start_date,$end_date,$nostruk,$divisi,$username,$nostiker) 
    {
    	$where_date ="";
		$where_date_detail ="";
        $where_date_reservasi = "";
    	if($start_date!="" && $end_date!="" && $start_date!="0000-00-00" && $end_date!="0000-00-00")
    	{
			$where_date ="AND transaksi_header.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
			$where_date_detail ="AND transaksi_detail.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";			
            
            $where_date_reservasi ="AND trans_reservasi.TglDokumen BETWEEN '".$start_date."' AND '".$end_date."' ";
		}
		
    	$where_nostruk ="";
		$where_nostruk_detail ="";
        $where_nostruk_reservasi = "";
    	if($nostruk)
    	{
            $where_nostruk = " AND `transaksi_header`.NoStruk = '".$nostruk."' ";
			$where_nostruk_detail = " AND `transaksi_detail`.NoStruk = '".$nostruk."' ";
            
            $where_nostruk_reservasi = " AND `trans_reservasi`.NoDokumen = '".$nostruk."' ";
		}
		
    	$where_divisi ="";
    	if($divisi)
    	{
            $where_divisi = " AND `divisi`.KdDivisi = '".$divisi."' "; 
		}
        
        $where_black_eye_bar = "";
        if($divisi=="Black Eye Bar")
        {
            $where_black_eye_bar = " AND kassa.id_kassa IN ('7','34','36') ";    
            $where_divisi = "";
        }
		
        $where_kasir = "";
		$where_kasir_detail = "";
        $where_kasir_reservasi = "";
        if($username!="")
        {
            $where_kasir = " AND `transaksi_header`.Kasir = '".$username."' ";
			$where_kasir_detail = " AND `transaksi_detail`.Kasir = '".$username."' ";
            $where_kasir_reservasi = " AND `trans_reservasi`.Kasir = '".$username."' ";
        }
        
		$sql = "";
		if($pilihan=="transaksi"){
		            
            $sql ="
                SELECT
                    tbl_uni.NoKassa,
                    tbl_uni.Kasir,
                    tbl_uni.KdDivisi,
                    tbl_uni.NamaDivisi,
  					tbl_uni.SubDivisi,
                    tbl_uni.Tanggal,
                    tbl_uni.Waktu,
                    tbl_uni.NoStruk,
                    tbl_uni.Bruto,
                    tbl_uni.DiscDetail,
                    tbl_uni.Netto,
                    tbl_uni.Tunai,
                    tbl_uni.KDebit,
                    tbl_uni.KKredit,
                    tbl_uni.Voucher,
                    tbl_uni.Discount,
                    tbl_uni.TotalNilai,
                    tbl_uni.TotalBayar,
                    tbl_uni.Kembali,
                    tbl_uni.Total_Charge,
                    tbl_uni.TAX,
					tbl_uni.KdAgent
                FROM
                (
                    
                SELECT
                    `transaksi_header`.NoKassa,
                    `transaksi_header`.Kasir,
                    `divisi`.KdDivisi,
                    `divisi`.NamaDivisi,
    				`kassa`.`SubDivisi`,
                    `transaksi_header`.Tanggal,
                    `transaksi_header`.Waktu,
                    `transaksi_header`.NoStruk,
                    det.Bruto,
                    det.DiscDetail,
                    det.Netto,
                    COALESCE(`transaksi_header`.Tunai, NULL, '0') AS Tunai,
                    COALESCE(`transaksi_header`.KDebit, NULL, '0') AS KDebit,
                    COALESCE(`transaksi_header`.KKredit, NULL, '0') AS KKredit,
                    COALESCE(`transaksi_header`.Voucher, NULL, '0') AS Voucher,
                    COALESCE(`transaksi_header`.Discount, NULL, '0') AS Discount,
                    COALESCE(`transaksi_header`.TotalNilai, NULL, '0') AS TotalNilai,
                    COALESCE(`transaksi_header`.TotalBayar, NULL, '0') AS TotalBayar,
                    COALESCE(`transaksi_header`.Kembali, NULL, '0') AS Kembali,
                    COALESCE(`transaksi_header`.Ttl_Charge, NULL, '0') AS Total_Charge,
                    COALESCE(`transaksi_header`.TAX, NULL, '0') AS TAX,
					transaksi_header.KdAgent
                FROM
                    `transaksi_header`
                INNER JOIN kassa
                    ON transaksi_header.NoKassa = kassa.id_kassa
                INNER JOIN divisi
                    ON kassa.KdDivisi = divisi.KdDivisi
                INNER JOIN (SELECT NoStruk, 
                            SUM(Qty*Harga) AS Bruto, 
                            SUM(IFNULL(Disc1,0)+IFNULL(Disc2,0)+IFNULL(Disc3,0)+IFNULL(Disc4,0)) AS DiscDetail, 
                            SUM(Netto) AS Netto 
                            FROM transaksi_detail where 1 $where_date_detail
                            GROUP BY NoStruk) as det
                    ON transaksi_header.NoStruk = det.NoStruk
                WHERE
                    1
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date."
                    ".$where_black_eye_bar."
                    AND `transaksi_header`.status = '1'
                
                UNION ALL
                
                SELECT
                    '' AS NoKassa,
                    `trans_reservasi`.Kasir,
                    `divisi`.KdDivisi,
                    `divisi`.NamaDivisi,
    				`subdivisi`.KdSubDivisi,
                    `trans_reservasi`.TglDokumen AS Tanggal,
                    '00:00:00' AS Waktu,
                    `trans_reservasi`.NoDokumen AS NoStruk,
                    '0' AS Bruto,
                    `trans_reservasi`.dp AS DiscDetail,
                    `trans_reservasi`.Total AS Netto,
                    COALESCE(`trans_reservasi`.Sisa_Tunai, NULL, '0') AS Tunai,
                    COALESCE(`trans_reservasi`.Sisa_Debit, NULL, '0') AS KDebit,
                    COALESCE(`trans_reservasi`.Sisa_Kredit, NULL, '0') AS KKredit,
                    '0' AS Voucher,
                    '0' AS Discount,
                    COALESCE(`trans_reservasi`.Total, NULL, '0') AS TotalNilai,
                    (`trans_reservasi`.Sisa_Tunai + `trans_reservasi`.Sisa_Debit + `trans_reservasi`.Sisa_Kredit) AS TotalBayar,
                    '0' AS Kembali,
                    '0' AS Total_Charge,
					'0' AS TAX ,
					'0' AS KdAgent
                FROM
                    `trans_reservasi`
                    INNER JOIN `divisi` ON
                        `trans_reservasi`.KdDivisi = `divisi`.KdDivisi
				    INNER JOIN `subdivisi` 
				      ON `divisi`.KdDivisi = `subdivisi`.KdDivisi 
                WHERE
                    1
                    ".$where_kasir_reservasi."
                    ".$where_nostruk_reservasi."
                    ".$where_divisi."
                    ".$where_date_reservasi." 
                ) AS tbl_uni 
                ORDER BY
                    tbl_uni.KdDivisi ASC,
                    tbl_uni.Tanggal ASC,
                    tbl_uni.Waktu ASC,
                    tbl_uni.NoStruk ASC
            ";
           //echo $sql;
		
		}
		elseif($pilihan=="detail"){
		
			$sql ="
				SELECT
					`transaksi_header`.NoKassa,
					`transaksi_header`.Kasir,
					`divisi`.KdDivisi,
					`divisi`.NamaDivisi,
					`transaksi_header`.Tanggal,
					`transaksi_header`.Waktu,
					`transaksi_header`.NoStruk,
					`transaksi_header`.TotalNilai,
					`transaksi_header`.TotalBayar,
					`transaksi_header`.Voucher,
					`transaksi_header`.Discount,
					`transaksi_header`.TotalItem,
					transaksi_detail.PCode, 
					masterbarang.NamaLengkap, 
					transaksi_detail.Qty, 
					transaksi_detail.Harga, 
					(transaksi_detail.Qty * transaksi_detail.Harga) AS Bruto, 
					(IFNULL(transaksi_detail.Disc1,0)+IFNULL(transaksi_detail.Disc2,0)+IFNULL(transaksi_detail.Disc3,0)+IFNULL(transaksi_detail.Disc4,0)) AS DiscDetail, 
					transaksi_detail.Netto,
					transaksi_detail.Service_charge,
					masterbarang.PPN					
				FROM
					`transaksi_detail`
				INNER JOIN transaksi_header
					ON transaksi_header.NoStruk = `transaksi_detail`.NoStruk 
					AND `transaksi_header`.status = '1' 
					".$where_kasir."
					".$where_nostruk."
					".$where_date."	
				INNER JOIN kassa
					ON transaksi_header.NoKassa = kassa.id_kassa
                    ".$where_black_eye_bar."
				INNER JOIN divisi
					ON kassa.KdDivisi = divisi.KdDivisi
					".$where_divisi."
				INNER JOIN masterbarang
					ON `transaksi_detail`.PCode=masterbarang.PCode	
				WHERE
					1
					".$where_kasir_detail."
					".$where_nostruk_detail."
					".$where_date_detail."
					AND `transaksi_header`.status = '1' 
				ORDER BY
					`divisi`.KdDivisi,
					`transaksi_header`.Tanggal ASC,
					`transaksi_header`.Waktu ASC,
					`transaksi_header`.NoStruk ASC,
					transaksi_detail.PCode ASC
					";		
		
		}
		elseif($pilihan=="barang"){
		
			$sql ="
				
				SELECT KdDivisi,NamaDivisi,PCode,NamaLengkap,SUM(Qty) AS Qty,Harga,SUM(Qty*Harga) AS Bruto, 
				SUM(DiscDetail) AS DiscDetail,
				SUM(Netto) AS Netto
				FROM(
						SELECT
						`transaksi_header`.NoKassa,
						`transaksi_header`.Kasir,
						`divisi`.KdDivisi,
						`divisi`.NamaDivisi,
						`transaksi_header`.Tanggal,
						`transaksi_header`.Waktu,
						`transaksi_header`.NoStruk,
						`transaksi_header`.Discount,
						`transaksi_header`.TotalItem,
						transaksi_detail.PCode, 
						masterbarang.NamaLengkap, 
						transaksi_detail.Qty, 
						transaksi_detail.Harga, 
						(transaksi_detail.Qty * transaksi_detail.Harga) AS Bruto, 
						(IFNULL(transaksi_detail.Disc1,0)+IFNULL(transaksi_detail.Disc2,0)+IFNULL(transaksi_detail.Disc3,0)+IFNULL(transaksi_detail.Disc4,0)) AS DiscDetail, 
						transaksi_detail.Netto
					FROM
						`transaksi_detail`
					INNER JOIN transaksi_header
						ON transaksi_header.NoStruk = `transaksi_detail`.NoStruk 
						AND `transaksi_header`.status = '1' 
						".$where_kasir."
						".$where_nostruk."
						".$where_date."	
					INNER JOIN kassa
						ON transaksi_header.NoKassa = kassa.id_kassa
                        ".$where_black_eye_bar."
					INNER JOIN divisi
						ON kassa.KdDivisi = divisi.KdDivisi
						".$where_divisi."
					INNER JOIN masterbarang
						ON `transaksi_detail`.PCode=masterbarang.PCode	
					WHERE
						1
						".$where_kasir_detail."
						".$where_nostruk_detail."
						".$where_date_detail."
						AND `transaksi_header`.status = '1' 
					ORDER BY
						`divisi`.KdDivisi,
						`transaksi_header`.Tanggal ASC,
						`transaksi_header`.Waktu ASC,
						`transaksi_header`.NoStruk ASC					
				) as a	
				GROUP BY a.KdDivisi,a.PCode
				ORDER BY a.KdDivisi,a.PCode ASC";
		
		}
		elseif($pilihan=="dpp"){
		
			$sql ="
				SELECT
					`transaksi_header`.NoKassa,
					`transaksi_header`.Kasir,
					`divisi`.KdDivisi,
					`divisi`.NamaDivisi,
					`subdivisi`.KdSubDivisi,
					`subdivisi`.NamaSubDivisi,
					`transaksi_header`.Tanggal,
					`transaksi_header`.Waktu,
					`transaksi_header`.NoStruk,
					`transaksi_header`.TotalNilai,
					`transaksi_header`.TotalBayar,
					`transaksi_header`.Voucher,
					`transaksi_header`.Discount,
					`transaksi_header`.TotalItem,
					transaksi_detail.PCode, 
					masterbarang.NamaLengkap, 
					transaksi_detail.Qty, 
					transaksi_detail.Harga, 
					(transaksi_detail.Qty * transaksi_detail.Harga) AS Bruto, 
					(IFNULL(transaksi_detail.Disc1,0)+IFNULL(transaksi_detail.Disc2,0)+IFNULL(transaksi_detail.Disc3,0)+IFNULL(transaksi_detail.Disc4,0)) AS DiscDetail, 
					transaksi_detail.Netto,
					transaksi_detail.Service_charge,
					masterbarang.PPN,
					masterbarang.PersenPajak
				FROM
					`transaksi_detail`
				INNER JOIN transaksi_header
					ON transaksi_header.NoStruk = `transaksi_detail`.NoStruk 
					AND `transaksi_header`.status = '1' 
					".$where_kasir."
					".$where_nostruk."
					".$where_date."	
				INNER JOIN kassa
					ON transaksi_header.NoKassa = kassa.id_kassa
                    ".$where_black_eye_bar."
				INNER JOIN divisi
					ON kassa.KdDivisi = divisi.KdDivisi
					".$where_divisi."
				INNER JOIN subdivisi
					ON kassa.SubDivisi = subdivisi.KdSubDivisi
					".$where_divisi."	
				INNER JOIN masterbarang
					ON `transaksi_detail`.PCode=masterbarang.PCode	
				WHERE
					1
					".$where_kasir_detail."
					".$where_nostruk_detail."
					".$where_date_detail."
					AND `transaksi_header`.status = '1' 
				ORDER BY
					`divisi`.KdDivisi,
					`transaksi_header`.Tanggal ASC,
					`transaksi_header`.Waktu ASC,
					`transaksi_header`.NoStruk ASC,
					transaksi_detail.PCode ASC
					";		
				
		}
		elseif($pilihan=="tanggal"){
		            
            $sql ="
                SELECT
                    tbl_uni.NoKassa,
                    tbl_uni.Kasir,
                    tbl_uni.Tanggal,
                    sum(tbl_uni.Bruto) as Bruto,
                    sum(tbl_uni.DiscDetail) as DiscDetail,
                    sum(tbl_uni.Netto) as Netto,
                    sum(tbl_uni.Tunai) as Tunai,
                    sum(tbl_uni.KDebit) as KDebit,
                    sum(tbl_uni.KKredit) as KKredit,
                    sum(tbl_uni.Voucher) as Voucher,
                    sum(tbl_uni.Discount) as Discount,
                    sum(tbl_uni.TotalNilai) as TotalNilai,
                    sum(tbl_uni.TotalBayar) as TotalBayar,
                    sum(tbl_uni.Kembali) as Kembali,
                    sum(tbl_uni.Total_Charge) as Total_Charge,
                    sum(tbl_uni.TAX) as TAX
                FROM
                (
                    
                SELECT
                    `transaksi_header`.NoKassa,
                    `transaksi_header`.Kasir,
                    `divisi`.KdDivisi,
                    `divisi`.NamaDivisi,
    				`kassa`.`SubDivisi`,
                    `transaksi_header`.Tanggal,
                    `transaksi_header`.Waktu,
                    `transaksi_header`.NoStruk,
                    det.Bruto,
                    det.DiscDetail,
                    det.Netto,
                    COALESCE(`transaksi_header`.Tunai, NULL, '0') AS Tunai,
                    COALESCE(`transaksi_header`.KDebit, NULL, '0') AS KDebit,
                    COALESCE(`transaksi_header`.KKredit, NULL, '0') AS KKredit,
                    COALESCE(`transaksi_header`.Voucher, NULL, '0') AS Voucher,
                    COALESCE(`transaksi_header`.Discount, NULL, '0') AS Discount,
                    COALESCE(`transaksi_header`.TotalNilai, NULL, '0') AS TotalNilai,
                    COALESCE(`transaksi_header`.TotalBayar, NULL, '0') AS TotalBayar,
                    COALESCE(`transaksi_header`.Kembali, NULL, '0') AS Kembali,
                    COALESCE(`transaksi_header`.Ttl_Charge, NULL, '0') AS Total_Charge,
                    COALESCE(`transaksi_header`.TAX, NULL, '0') AS TAX,
					transaksi_header.KdAgent
                FROM
                    `transaksi_header`
                INNER JOIN kassa
                    ON transaksi_header.NoKassa = kassa.id_kassa
                INNER JOIN divisi
                    ON kassa.KdDivisi = divisi.KdDivisi
                INNER JOIN (SELECT NoStruk, 
                            SUM(Qty*Harga) AS Bruto, 
                            SUM(IFNULL(Disc1,0)+IFNULL(Disc2,0)+IFNULL(Disc3,0)+IFNULL(Disc4,0)) AS DiscDetail, 
                            SUM(Netto) AS Netto 
                            FROM transaksi_detail
                            GROUP BY NoStruk) as det
                    ON transaksi_header.NoStruk = det.NoStruk
                WHERE
                    1
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date."
                    ".$where_black_eye_bar."
                    AND `transaksi_header`.status = '1'
                
                UNION ALL
                
                SELECT
                    '' AS NoKassa,
                    `trans_reservasi`.Kasir,
                    `divisi`.KdDivisi,
                    `divisi`.NamaDivisi,
    				`subdivisi`.KdSubDivisi,
                    `trans_reservasi`.TglDokumen AS Tanggal,
                    '00:00:00' AS Waktu,
                    `trans_reservasi`.NoDokumen AS NoStruk,
                    '0' AS Bruto,
                    `trans_reservasi`.dp AS DiscDetail,
                    `trans_reservasi`.Total AS Netto,
                    COALESCE(`trans_reservasi`.Sisa_Tunai, NULL, '0') AS Tunai,
                    COALESCE(`trans_reservasi`.Sisa_Debit, NULL, '0') AS KDebit,
                    COALESCE(`trans_reservasi`.Sisa_Kredit, NULL, '0') AS KKredit,
                    '0' AS Voucher,
                    '0' AS Discount,
                    COALESCE(`trans_reservasi`.Total, NULL, '0') AS TotalNilai,
                    (`trans_reservasi`.Sisa_Tunai + `trans_reservasi`.Sisa_Debit + `trans_reservasi`.Sisa_Kredit) AS TotalBayar,
                    '0' AS Kembali,
                    '0' AS Total_Charge,
					'0' AS TAX ,
					'0' AS KdAgent
                FROM
                    `trans_reservasi`
                    INNER JOIN `divisi` ON
                        `trans_reservasi`.KdDivisi = `divisi`.KdDivisi
				    INNER JOIN `subdivisi` 
				      ON `divisi`.KdDivisi = `subdivisi`.KdDivisi 
                WHERE
                    1
                    ".$where_kasir_reservasi."
                    ".$where_nostruk_reservasi."
                    ".$where_divisi."
                    ".$where_date_reservasi." 
                ) AS tbl_uni 
                group by  
                	tbl_uni.Tanggal,
                	tbl_uni.NoKassa,
                    tbl_uni.Kasir
                ORDER BY
                	tbl_uni.Tanggal,
                	tbl_uni.NoKassa,
                    tbl_uni.Kasir
                    
            ";
           //echo $sql;
		
		}
        $result = $this->getArrayResult($sql);
        return $result;
    }

	function getVoucher($start_date,$end_date,$arr_struck)
	{
		$where_date ="";
    	if($start_date!="" && $start_date!="0000-00-00" && $end_date!="" && $end_date!="0000-00-00")
    	{
			$where_date ="AND transaksi_detail_voucher.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
		}
		
		$where_struck="";
		if(count($arr_struck)>0)
		{
			$where_struck = $arr_struck;
		}
		
		$sql="
			SELECT 
			  transaksi_detail_voucher.NoKassa,
			  transaksi_detail_voucher.NoStruk,
			  DATE_FORMAT(transaksi_detail_voucher.Tanggal, '%d-%m-%Y') AS Tanggal,
			  transaksi_detail_voucher.Jenis,
			  transaksi_detail_voucher.NomorVoucher,
			  transaksi_detail_voucher.NilaiVoucher 
			FROM
			  transaksi_detail_voucher 
			WHERE 
			  1
			  ".$where_date."
			  ".$where_struck." 
			ORDER BY transaksi_detail_voucher.Tanggal DESC
		";
		return $this->getArrayResult($sql);
		
	}
    
    function getType($id="")
    {
    	$where_divisi="";
    	if(count($id)>0)
    	{
			$where_divisi = "AND KdDivisi IN (".$id.")";
		}
		
		$sql = "SELECT KdDivisi, NamaDivisi FROM divisi WHERE 1 ".$where_divisi." ORDER BY NamaDivisi ASC";
		
		return $this->getArrayResult($sql);
	}
    
    function getKasir($id="")
    {
    	$where_kasir="";
    	if(count($id)>0)
    	{
			$where_kasir = "AND user.UserLevel IN (".$id.")";
		}
		
		$sql = "
            SELECT 
              user.UserLevel,
              userlevels.UserLevelName,
              user.employee_nik,
              user.UserName,
              employee.employee_name 
            FROM
              user 
              LEFT JOIN userlevels 
                ON user.UserLevel = userlevels.UserLevelID 
              LEFT JOIN employee 
                ON user.employee_nik = employee.employee_nik 
            WHERE 1 
               ".$where_kasir."
            GROUP BY user.employee_nik 
            ORDER BY user.UserName ASC
		";      
		return $this->getArrayResult($sql);
	}
    
    function getKasirFromHeader()
    {		
		$sql = "
            SELECT 
              transaksi_header.Kasir,
              employee.employee_name 
            FROM
              transaksi_header 
              LEFT JOIN employee 
                ON transaksi_header.Kasir = employee.username 
            WHERE 1 
            GROUP BY Kasir 
            ORDER BY Kasir ASC
        ";
		
		return $this->getArrayResult($sql);
	}
    
    function getArrayResult($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) 
    {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) 
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() 
    {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>