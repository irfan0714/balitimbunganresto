<?php
class report_voucher_employee_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function getReport( $start_date, $end_date, $nostruk, $divisi, $nik) {
		$user = $this->session->userdata('username');
		$where_date = "";
		$where_date_detail = "";
		if ($start_date != "" && $end_date != "" && $start_date != "0000-00-00" && $end_date != "0000-00-00") {
			$where_date = "AND h.Tanggal BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";
			$where_date_detail = "AND d.Tanggal BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";

		}

		$where_nostruk = "";
		$where_nostruk_detail = "";
		if ($nostruk) {
			$where_nostruk = " AND `h`.NoStruk = '" . $nostruk . "' ";
			$where_nostruk_detail = " AND `d`.NoStruk = '" . $nostruk . "' ";
		}

		$where_divisi = "";
		if ($divisi) {
			$where_divisi = " AND `divisi`.KdDivisi = '" . $divisi . "' ";
		}

		$where_nik = "";
		if ($nik) {
			$where_nik = " AND `h`.KdCustomer = '" . $nik . "' ";
		}

		$sql = "
			SELECT
				`h`.NoKassa,
				`h`.Kasir,
				`divisi`.KdDivisi,
				`divisi`.NamaDivisi,
				`h`.Tanggal,
				`h`.Waktu,
				`h`.NoStruk,
				`h`.TotalNilai,
				`h`.TotalBayar,
				`h`.`Tunai`,
				`h`.`Point`,
				`h`.`KKredit`,
				`h`.`KDebit`,
				`h`.`GoPay`,
				`h`.`Voucher`,
				`h`.KdCustomer,
				IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1) AS Discount,
				`h`.TotalItem,
				d.PCode,
				masterbarang.NamaLengkap,
				d.Qty,
				d.Berat,
				IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Harga,
				d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Bruto,
				(IFNULL(d.Disc1,0)+IFNULL(d.Disc2,0)+IFNULL(d.Disc3,0)+IFNULL(d.Disc4,0)) AS DiscDetail,
				IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) AS Netto,
				d.Service_charge,
				masterbarang.PPN
			FROM
				transaksi_detail d
			INNER JOIN transaksi_header h
				ON h.NoStruk = `d`.NoStruk
				AND `h`.status = '1'
				" . $where_nostruk . "
				" . $where_date . "
				" . $where_nik . "
			INNER JOIN voucher_employee v 
				ON `h`.KdCustomer = v.nik
			INNER JOIN kassa k
				ON h.NoKassa = k.id_kassa
			INNER JOIN divisi
				ON k.KdDivisi = divisi.KdDivisi
				" . $where_divisi . "
			INNER JOIN masterbarang
				ON `d`.PCode=masterbarang.PCode
			WHERE
				1
				" . $where_nostruk_detail . "
				" . $where_date_detail . "
				AND `h`.status = '1'
			UNION ALL
			SELECT
				`h`.NoKassa,
				`h`.Kasir,
				`divisi`.KdDivisi,
				`divisi`.NamaDivisi,
				`h`.Tanggal,
				`h`.Waktu,
				`h`.NoStruk,
				`h`.TotalNilai,
				`h`.TotalBayar,
				`h`.`Tunai`,
				`h`.`Point`,
				`h`.`KKredit`,
				`h`.`KDebit`,
				`h`.`GoPay`,
				`h`.`Voucher`,
				`h`.KdCustomer,
				IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1) AS Discount,
				`h`.TotalItem,
				d.PCode,
				masterbarang.NamaLengkap,
				d.Qty,
				d.Berat,
				IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Harga,
				d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Bruto,
				(IFNULL(d.Disc1,0)+IFNULL(d.Disc2,0)+IFNULL(d.Disc3,0)+IFNULL(d.Disc4,0)) AS DiscDetail,
				IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) AS Netto,
				d.Service_charge,
				masterbarang.PPN
			FROM
				transaksi_detail_sunset d
			INNER JOIN transaksi_header_sunset h
				ON h.NoStruk = `d`.NoStruk
				AND `h`.status = '1'
				" . $where_nostruk . "
				" . $where_date . "
				" . $where_nik . "
			INNER JOIN voucher_employee v 
				ON `h`.KdCustomer = v.nik
			INNER JOIN kassa k
				ON h.NoKassa = k.id_kassa
			INNER JOIN divisi
				ON k.KdDivisi = divisi.KdDivisi
				" . $where_divisi . "
			INNER JOIN masterbarang
				ON `d`.PCode=masterbarang.PCode
			WHERE
				1
				" . $where_nostruk_detail . "
				" . $where_date_detail . "
				AND `h`.status = '1'
			ORDER BY
				KdCustomer ASC,
				NoStruk ASC,
				PCode ASC";

		$result = $this->getArrayResult($sql);
		return $result;
	}

	function getVoucherDetailEmployee( $start_date, $end_date, $nostruk) {
		$user = $this->session->userdata('username');
		$where_date = "";
		if ($start_date != "" && $end_date != "" && $start_date != "0000-00-00" && $end_date != "0000-00-00") {
			$where_date = "AND v.Tanggal BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";
		}

		$where_nostruk = "";
		if ($nostruk) {
			$where_nostruk = " AND `v`.NoStruk = '" . $nostruk . "' ";
		}

		$sql = "
			SELECT v.NoKassa,v.NoStruk,v.Tanggal,v.Jenis,v.NomorVoucher,SUM(v.NilaiVoucher) AS NilaiVoucher  
			FROM transaksi_detail_voucher v WHERE 1 
				AND v.jenis = '3'  
				" . $where_date . " 
				" . $where_nostruk . " 
				GROUP BY v.NoStruk,v.jenis
			UNION ALL
			SELECT v.NoKassa,v.NoStruk,v.Tanggal,v.Jenis,v.NomorVoucher,SUM(v.NilaiVoucher) AS NilaiVoucher   
			FROM transaksi_detail_voucher_sunset v WHERE 1 
				AND v.jenis = '3' 
				" . $where_date . " 
				" . $where_nostruk . " 
				GROUP BY v.NoStruk,v.jenis
			ORDER BY NoStruk ASC ";

		$result = $this->getArrayResult($sql);
		return $result;
	}


	function getDivisi() {
	
		$sql = "SELECT KdDivisi, NamaDivisi FROM divisi ORDER BY NamaDivisi ASC";
		return $this->getArrayResult($sql);
	}

	function getEmployee() {

		// $sql = "SELECT employee_nik, employee_name FROM employee WHERE employee_nik <> '' ORDER BY employee_name ASC";
		$sql = "SELECT * FROM voucher_employee GROUP BY nik";
		return $this->getArrayResult($sql);
	}

	function getArrayResult($sql) {
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}

	function NumResult($sql) {
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function getRow($sql) {
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}

	function getDate() {
		$sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}

	function getJenis() {
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>
