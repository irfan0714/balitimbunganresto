<?php

class Rpt_komisimodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getTransTicket() {
        $sql = "SELECT *,b.NamaJenis FROM ticket a LEFT JOIN ticket_jenis b ON a.jenis=b.idjenis WHERE a.noidentitas !='1234' ORDER BY a.noticket DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function totalTicketPerhari($date) {
        $sql = "SELECT COUNT(noticket) AS total FROM ticket WHERE add_date='$date' AND noidentitas !='1234'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function num_ticket_row($first_date, $last_date) {
        $where = "where a.noidentitas !='1234' ";
        $orderby = " ORDER BY a.noticket DESC ";
        if ($first_date != '') {
            $where .= " and add_date = '$first_date'";
        }
        if ($first_date != '' && $last_date != '') {
            $where .= " and add_date >= '$first_date' and add_date <= '$last_date' ";
        }
        $sql = "SELECT a.noticket FROM ticket a LEFT JOIN ticket_jenis b ON a.jenis=b.idjenis $where $orderby ";
        return $this->NumResult($sql);
    }

    function getTicketList($num, $offset, $first_date, $last_date) {
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }
        $where = "where a.noidentitas !='1234' ";
        $orderby = " ORDER BY a.noticket DESC ";
        if ($first_date != '') {
            $where .= " and add_date = '$first_date'";
        }
        if ($first_date != '' && $last_date != '') {
            $where .= " and add_date >= '$first_date' and add_date <= '$last_date' ";
        }
        $sql = "SELECT *,b.NamaJenis FROM ticket a LEFT JOIN ticket_jenis b ON a.jenis=b.idjenis $where $orderby Limit $offset,$num";
        return $this->getArrayResult($sql);
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function getReport($sql) {
        return $this->getArrayResult($sql);
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>