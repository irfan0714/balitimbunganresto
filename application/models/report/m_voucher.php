<?php

class m_voucher extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }
	
	function getAllTicket(){
		$sql = "select * from masterbarang where JenisBarang='TCK' ";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function getTransTicket($getLastIP){
		$sql = "select * from ticket where status='0' and last_ip='$getLastIP' order by noticket ASC";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function editStatusTicket($parameter,$data,$where){
		$this->db->where($where);
		$this->db->update($parameter, $data);
	}
	
	function totalTicketPerhari($date){
		$sql = "SELECT COUNT(noticket) AS total FROM ticket WHERE add_date='$date' AND noidentitas !='1234'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getReport($start_date,$end_date) 
    {
    	$sql ="
			SELECT 
			  DATE_FORMAT(t.add_date, '%Y-%m-%d') AS tanggal, 
			  t.jenis, ticket_jenis.NamaJenis, SUM(t.qty) AS total_qty,
			  SUM(t.harga) AS total_harga ,SUM(ttlTerpakai) AS sisa
			FROM
			  (SELECT 
				* 
			  FROM
				ticket
				WHERE add_date BETWEEN '".$start_date."' AND '".$end_date."' AND noidentitas != '1234') t 
			  
			  INNER JOIN ticket_jenis ON t.jenis = ticket_jenis.idjenis   
			  LEFT JOIN 
				(SELECT NoKassa, NoStruk, Tanggal, Jenis, NomorVoucher, SUM(NilaiVoucher) AS ttlTerpakai
				FROM transaksi_detail_voucher GROUP BY NomorVoucher)v
				ON v.NomorVoucher = t.noticket 
			  GROUP BY t.add_date,t.jenis
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }

	function getTicketHead($start_date,$end_date)
	{
		$sql="
			SELECT 
			  DATE(ticket_head.tanggal) AS tglTiket,
			  SUM(ticket_head.nilai_tunai) AS n_tunai,
			  SUM(ticket_head.nilai_debit) AS n_debit,
			  SUM(ticket_head.nilai_kredit) AS n_kredit,
			  SUM(ticket_head.nilai_voucher) AS n_voucher 
			FROM
			  ticket_head 
			WHERE 1 
			  AND DATE(ticket_head.tanggal) BETWEEN '".$start_date."' 
			  AND '".$end_date."' 
			  AND ticket_head.noidentitas != '1234' 
			GROUP BY tglTiket
		";
        return $this->getArrayResult($sql);
		
	}
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>