<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_pemakaian_bahan_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	
	function getReport($tgldari, $tglsampai, $gudang) {
	 	
		$sql ="SELECT a.formulanumber, a.formulaname, a.batchnumber, MixQty, Qty, QtyQC, QtyReject, fgwgt, inventorycode, NamaLengkap, quantity, b.MRNo
				FROM (
					SELECT f.`formulanumber`,f.`formulaname`,p.batchnumber, SUM(f.Quantity*r.Prod_batch) AS MixQty , SUM(fg.`quantity`) AS Qty, SUM(fg.`quantityqc`) AS QtyQC, SUM(fg.quantity_qc_reject) AS QtyReject,
					SUM(f.`finishgood_wgt`*(fg.`quantity`+fg.`quantityqc`+fg.`quantity_qc_reject`)) AS fgwgt
					FROM (
						SELECT p1.formulanumber, p1.productiondate, p1.warehousecode, p1.batchnumber, SUM(p1.mixquantity) AS mixquantity FROM production p1 GROUP BY p1.batchnumber) p
							INNER JOIN formula f ON p.`formulanumber`=f.`formulanumber`
							INNER JOIN (
								SELECT fg1.batchnumber, fg1.inventorycode, SUM(fg1.`quantity`) AS quantity, SUM(fg1.`quantityqc`) AS quantityqc, 
						 			SUM(fg1.`quantity_qc_reject`) AS quantity_qc_reject 
						 			FROM productionfinishgood fg1 GROUP BY fg1.batchnumber
						 		) fg ON p.`batchnumber`=fg.`batchnumber`
							INNER JOIN masterbarang b ON fg.inventorycode=b.pcode
							INNER JOIN (
								select batchnumber, sum(Prod_batch) as Prod_batch 
									from productionbatchrequest where prod_batch>0 group by batchnumber 
								)  r ON p.batchnumber=r.batchnumber 
							WHERE p.productiondate BETWEEN '$tgldari' AND '$tglsampai' AND p.warehousecode='$gudang' 
							GROUP BY f.`formulanumber`,p.batchnumber
					) a
					INNER JOIN (
						SELECT p.`formulanumber`, p.batchnumber, r.matreqnumber AS MRNo, m.`inventorycode`, b.`NamaLengkap`,SUM(m.quantity) AS quantity
							FROM production p 
							INNER JOIN productionbatchrequest r ON p.batchnumber=r.batchnumber AND r.prod_batch>0				
							INNER JOIN `matreqdetail` m ON m.`matreqnumber`=r.`matreqnumber`
							INNER JOIN masterbarang b ON m.inventorycode=b.pcode
							WHERE p.productiondate BETWEEN '$tgldari' AND '$tglsampai' AND p.warehousecode='$gudang' AND b.`KdKategori`=1 AND p.`mixquantity`>0
							GROUP BY p.`formulanumber`, p.batchnumber, r.matreqnumber, m.`inventorycode`
						UNION ALL
						SELECT p.`formulanumber`, p.`batchnumber`, r.matreqnumber AS MRNo, k.`inventorycode`, b.`NamaLengkap`,SUM(k.`quantity`) AS quantity
							FROM production p 
							INNER JOIN productionbatchrequest r ON p.batchnumber=r.batchnumber AND r.prod_batch_packaging>0
							INNER JOIN matreqdetail k ON r.`matreqnumber`=k.matreqnumber
							INNER JOIN masterbarang b ON k.inventorycode=b.pcode
							WHERE p.productiondate BETWEEN '$tgldari' AND '$tglsampai' AND p.warehousecode='$gudang' AND b.`KdKategori`=2 AND p.`mixquantity`=0
							GROUP BY p.`formulanumber`, p.`batchnumber`, r.`matreqnumber`, k.inventorycode
					) b 
				ON a.`formulanumber`=b.formulanumber AND a.batchnumber=b.batchnumber ORDER BY a.formulanumber, a.batchnumber";
		//echo $sql; die();		
		$query = $this->db->query($sql);
        return $query->result_array();
	}
	
	function listgudang($username){
		$sql = "SELECT g.`KdGudang`, g.`Keterangan` FROM gudang g INNER JOIN gudang_admin ga ON g.`KdGudang`=ga.`KdGudang`
					WHERE ga.`UserName`='$username'";
		
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
		
        return $result;
	}
    
}
?>