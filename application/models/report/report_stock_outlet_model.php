<?php
class Report_stock_outlet_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
    
    
    function getPenjualan(){
    	 
		 $sql ="
				SELECT
				  a.`PCode`,
				  b.`NamaLengkap`,
				  SUM(a.`Qty`) AS Qty
				FROM
				  transaksi_detail_sunset a
				  INNER JOIN masterbarang b
				    ON a.`PCode` = b.`PCode`
				WHERE a.`PCode` IN (
				    '190101002',
				    '190101003',
				    '190101037',
				    '733990011',
				    '733990146'
				  )
				  AND a.`Tanggal` = DATE_ADD(CURDATE(), INTERVAL - 1 DAY)
				  AND (
				    a.`NoKassa` = '89'
				    OR a.`NoKassa` = '91'
				  ) GROUP BY a.`PCode` ORDER BY b.`NamaLengkap` ASC ;
				  ";
		 
		return $this->getArrayResult($sql);
	}
	
	function getDataBEO($tgldari,$PCode){
		 $sql ="SELECT
				  b.`PCode`,
				  SUM(b.Qty) AS Qty
				FROM
				  trans_reservasi a
				  INNER JOIN trans_reservasi_detail b
				    ON a.`NoDokumen` = b.`NoDokumen`
				WHERE 1
				  AND a.`place_bts` = '1'
				  AND DATE(a.`ReservasiDate`) = CURDATE()
				  AND b.`PCode` = '$PCode'
				  GROUP BY b.PCode;";
		return $this->getRow($sql);
	}
	
	function getSafetyStock($gudang){
		 $sql ="SELECT a.`safetystock` FROM gudang a WHERE a.`KdGudang`='$gudang';";
		return $this->getRow($sql);
	}
	
	function getSaldoAwalStockBebek($bln,$gudang){
		 $sql ="SELECT a.`GAwal$bln` AS saldo_awal FROM stock a WHERE a.`KdGudang`='$gudang' AND a.`Tahun`='2019' AND a.`PCode`='271840028';";
		return $this->getRow($sql);
	}
	
	function getSaldoAwalStockBumbu($bln,$gudang){
		 $sql ="SELECT a.`GAwal$bln` AS saldo_awal FROM stock a WHERE a.`KdGudang`='$gudang' AND a.`Tahun`='2019' AND a.`PCode`='201880007';";
		return $this->getRow($sql);
	}
	
	function getMutasiBebek($gudang,$awalbulan,$tglsampai){
		 $sql ="SELECT
				  *
				FROM
				  mutasi a
				WHERE a.`KodeBarang` IN ('271840028')
				  AND a.`Tanggal` BETWEEN '$awalbulan'
				  AND '$tglsampai'
				  AND (
				    a.`Gudang` = '$gudang'
				    OR a.`GudangTujuan` = '$gudang'
				  );";
		return $this->getArrayResult($sql);
	}
	
	function getMutasiJualBebek($gudang,$awalbulan,$tglsampai){
		 $sql ="SELECT
				  a.`KodeBarang`,
  				  SUM(a.Qty) AS Qty
				FROM
				  mutasi a
				WHERE a.KodeBarang IN ('733990011','190101003','190101002','190101037')
				  AND a.`Tanggal` BETWEEN '$awalbulan'
				  AND '$tglsampai'
				  AND (
				    a.`Gudang` = '$gudang'
				    OR a.`GudangTujuan` = '$gudang'
				  ) GROUP BY a.KodeBarang;";
		return $this->getArrayResult($sql);
	}
	
	function getMutasiBumbu($gudang,$awalbulan,$tglsampai){
		 $sql ="SELECT
				  *
				FROM
				  mutasi a
				WHERE a.`KodeBarang` IN ('201880007')
				  AND a.`Tanggal` BETWEEN '$awalbulan'
				  AND '$tglsampai'
				  AND (
				    a.`Gudang` = '$gudang'
				    OR a.`GudangTujuan` = '$gudang'
				  );";
		return $this->getArrayResult($sql);
	}
	
	function getResepBebek($PCode){
		 $sql ="SELECT
				  a.*
				FROM
				  resep_detail a
				  INNER JOIN resep_header b
				    ON a.`resep_id` = b.`resep_id`
				WHERE b.`PCode` = '$PCode'
				  AND a.`PCode` = '271840028';";
		return $this->getRow($sql);
	}
	
	function getBumbuBebek($PCode){
		 $sql ="SELECT
				  a.*
				FROM
				  resep_detail a
				  INNER JOIN resep_header b
				    ON a.`resep_id` = b.`resep_id`
				WHERE b.`PCode` = '$PCode'
				  AND a.`PCode` = '201880007';";
		return $this->getRow($sql);
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_stock_outlet'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function voucher($v_date){
		$mylib = new globallib();
		$sql = "SELECT novoucher, nominal, terpakai FROM voucher WHERE expdate='".$mylib->ubah_tanggal($v_date)."' and Jenis<>5 order by novoucher";
        return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>