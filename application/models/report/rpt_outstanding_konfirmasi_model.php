<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_Outstanding_konfirmasi_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	
	function viewData($tgldari, $tglsampai) {

		$sql ="SELECT TglDokumen, NoDokumen, g.`Keterangan`, PONo, s.`Nama` 
				FROM trans_terima_header h INNER JOIN gudang g ON h.`KdGudang`=g.`KdGudang`
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				WHERE h.`TglDokumen` between '$tgldari' and '$tglsampai' AND `STATUS`=1 AND flagkonfirmasi='T'";
		
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
    }
    
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */