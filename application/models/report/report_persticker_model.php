<?php

class report_persticker_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getReport($pilihan,$v_date,$v_nostiker) 
    {
		$sql = "";
		if($pilihan=="transaksi"){
            $sql ="
            	SELECT 'HO' as NamaDivisi, 'Ticket' as SubDivisi,user as Kasir, date(Tanggal) as Tanggal,
            	Time(Tanggal) as Waktu, NoTrans as NoStruk, SUM(ttl_amount) AS Gross,
            	0 as Discount,  SUM(ttl_amount) AS Nett, 0 as ServiceCharge, 0 as Tax,
            	SUM(ttl_amount) AS Sales, 0 as Voucher, rt.Nama
            	FROM ticket_head 
            	LEFT JOIN (
				SELECT NoStiker, Nama FROM (
				   SELECT r.NoStiker, t.`Nama` FROM register r INNER JOIN tourtravel t ON r.kdtravel=t.`KdTravel` 
				      WHERE r.`Tanggal`= '$v_date'
				   UNION ALL
				   SELECT NoStiker, t.Nama FROM trans_reservasi_konfirmasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel` 
				      WHERE r.TglKonfirmasi='$v_date') tx GROUP BY NoStiker	
				 ) rt ON `noidentitas`=rt.NoStiker
            	WHERE DATE(tanggal)='$v_date' AND noidentitas='$v_nostiker'
            	group by NoTrans
            	UNION ALL
                SELECT 
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  transaksi_header.`Kasir`,
				  transaksi_header.`Tanggal`,
				  transaksi_header.`Waktu`,
				  transaksi_header.`NoStruk`,
				  SUM(transaksi_detail.`Qty`*IF(transaksi_detail.Service_charge>0,transaksi_detail.`Harga`,transaksi_detail.`Harga`/1.1)) AS Gross,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Disc1`,transaksi_detail.`Disc1`/1.1)) AS Discount,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)) AS Nett,
				  SUM(transaksi_detail.`Netto` * transaksi_detail.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)+ (IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1) * transaksi_detail.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )
				  ) AS Sales,
				  SUM(transaksi_header.`Voucher`*(IF(
      				transaksi_detail.`Service_charge` > 0,
      				transaksi_detail.`Netto` * 1155 / 1000,
      				transaksi_detail.Netto
    				)/transaksi_header.`TotalNilai`)) AS Voucher, rt.Nama
				FROM
				  transaksi_header 
				  INNER JOIN transaksi_detail 
				    ON transaksi_header.`NoKassa` = transaksi_detail.`NoKassa` 
				    AND transaksi_header.`NoStruk` = transaksi_detail.`NoStruk` 
				  INNER JOIN masterbarang b 
				    ON transaksi_detail.`PCode` = b.`PCode` 
				  INNER JOIN subdivisi s 
				    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  LEFT JOIN (
					SELECT NoStiker, Nama FROM (
					   SELECT r.NoStiker, t.`Nama` FROM register r INNER JOIN tourtravel t ON r.kdtravel=t.`KdTravel` 
					      WHERE r.`Tanggal`= '$v_date'
					   UNION ALL
					   SELECT NoStiker, t.Nama FROM trans_reservasi_konfirmasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel` 
					      WHERE r.TglKonfirmasi='$v_date') tx GROUP BY NoStiker	
					 ) rt ON transaksi_header.KdAgent=rt.NoStiker 
				  WHERE
                    transaksi_header.status = '1' and transaksi_header.Tanggal = '$v_date' and transaksi_header.KdAgent='$v_nostiker'
				GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , transaksi_header.`Tanggal`, transaksi_header.`NoStruk`
				UNION ALL				
				SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` AS SubDivisi,
				  transaksi_header_sunset.`Kasir`,
				  transaksi_header_sunset.`Tanggal`,
				  transaksi_header_sunset.`Waktu`,
				  transaksi_header_sunset.`NoStruk`,
				  SUM(
				    transaksi_detail_sunset.`Qty` * IF(
				      transaksi_detail_sunset.Service_charge > 0,
				      transaksi_detail_sunset.`Harga`,
				      transaksi_detail_sunset.`Harga` / 1.1
				    )
				  ) AS Gross,
				  SUM(
				    IF(
				      transaksi_detail_sunset.Service_charge > 0,
				      transaksi_detail_sunset.`Disc1`,
				      transaksi_detail_sunset.`Disc1` / 1.1
				    )
				  ) AS Discount,
				  SUM(
				    IF(
				      transaksi_detail_sunset.Service_charge > 0,
				      transaksi_detail_sunset.`Netto`,
				      transaksi_detail_sunset.`Netto` / 1.1
				    )
				  ) AS Nett,
				  SUM(
				    transaksi_detail_sunset.`Netto` * transaksi_detail_sunset.Service_charge / 100
				  ) AS ServiceCharge,
				  SUM(
				    (
				      IF(
				        transaksi_detail_sunset.Service_charge > 0,
				        transaksi_detail_sunset.`Netto`,
				        transaksi_detail_sunset.`Netto` / 1.1
				      ) + (
				        IF(
				          transaksi_detail_sunset.Service_charge > 0,
				          transaksi_detail_sunset.`Netto`,
				          transaksi_detail_sunset.`Netto` / 1.1
				        ) * transaksi_detail_sunset.Service_charge / 100
				      )
				    ) * 0.1
				  ) AS Tax,
				  SUM(
				    IF(
				      transaksi_detail_sunset.`Service_charge` > 0,
				      transaksi_detail_sunset.`Netto` * 1155 / 1000,
				      transaksi_detail_sunset.Netto
				    )
				  ) AS Sales,
				  SUM(
				    transaksi_header_sunset.`Voucher` * (
				      IF(
				        transaksi_detail_sunset.`Service_charge` > 0,
				        transaksi_detail_sunset.`Netto` * 1155 / 1000,
				        transaksi_detail_sunset.Netto
				      ) / transaksi_header_sunset.`TotalNilai`
				    )
				  ) AS Voucher,
				  rt.Nama
				FROM
				  transaksi_header_sunset
				  INNER JOIN transaksi_detail_sunset
				    ON transaksi_header_sunset.`NoKassa` = transaksi_detail_sunset.`NoKassa`
				    AND transaksi_header_sunset.`NoStruk` = transaksi_detail_sunset.`NoStruk`
				  INNER JOIN masterbarang b
				    ON transaksi_detail_sunset.`PCode` = b.`PCode`
				  INNER JOIN subdivisi s
				    ON b.`KdSubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi
				    ON s.`KdDivisi` = divisi.`KdDivisi`
				  LEFT JOIN
				    (SELECT
				      NoStiker,
				      Nama
				    FROM
				      (SELECT
				        r.NoStiker,
				        t.`Nama`
				      FROM
				        register r
				        INNER JOIN tourtravel t
				          ON r.kdtravel = t.`KdTravel`
				      WHERE r.`Tanggal` = '$v_date'
				      UNION
				      ALL
				      SELECT
				        NoStiker,
				        t.Nama
				      FROM
				        trans_reservasi_konfirmasi r
				        INNER JOIN tourtravel t
				          ON r.`KdTravel` = t.`KdTravel`
				      WHERE r.TglKonfirmasi = '$v_date') tx
				    GROUP BY NoStiker) rt
				    ON transaksi_header_sunset.KdAgent = rt.NoStiker
				WHERE transaksi_header_sunset.status = '1'
				  AND transaksi_header_sunset.Tanggal = '$v_date'
				  AND transaksi_header_sunset.KdAgent = '$v_nostiker'
				GROUP BY divisi.`KdDivisi`,
				  r.KdDivisiReport,
				  transaksi_header_sunset.`Tanggal`,
				  transaksi_header_sunset.`NoStruk`";
		}
		elseif($pilihan=="barang"){
		
			$sql ="
				SELECT 'HO' as NamaDivisi, 'Ticket' as SubDivisi,'Ticket' as PCode, 'Ticket' as NamaLengkap,Sum(Qty) as Qty,
				AVG(ttl_amount/Qty) as Harga, SUM(ttl_amount) AS Gross,
            	0 as Discount,  SUM(ttl_amount) AS Nett, 0 as Voucher, rt.Nama
            	FROM ticket_head 
            	LEFT JOIN (
				SELECT NoStiker, Nama FROM (
				   SELECT r.NoStiker, t.`Nama` FROM register r INNER JOIN tourtravel t ON r.kdtravel=t.`KdTravel` 
				      WHERE r.`Tanggal`= '$v_date'
				   UNION ALL
				   SELECT NoStiker, t.Nama FROM trans_reservasi_konfirmasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel` 
				      WHERE r.TglKonfirmasi='$v_date') tx GROUP BY NoStiker	
				 ) rt ON `noidentitas`=rt.NoStiker
            	WHERE DATE(tanggal)='$v_date' AND noidentitas='$v_nostiker'
				union all
					SELECT 
					  divisi.`NamaDivisi`,
					  r.`NamaDivisiReport` as SubDivisi,
					  transaksi_detail.`PCode`,
					  b.`NamaLengkap`,
					  SUM(transaksi_detail.`Qty`) AS Qty,
					  AVG(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Harga`,transaksi_detail.`Harga`/1.1)) AS Harga,
					  SUM(transaksi_detail.`Qty`*IF(transaksi_detail.Service_charge>0,transaksi_detail.`Harga`,transaksi_detail.`Harga`/1.1)) AS Gross,
					  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Disc1`,transaksi_detail.`Disc1`/1.1)) AS Discount,
					  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)) AS Nett,
					  SUM(transaksi_header.`Voucher`*(IF(
      					transaksi_detail.`Service_charge` > 0,
      					transaksi_detail.`Netto` * 1155 / 1000,
      					transaksi_detail.Netto
    					)/transaksi_header.`TotalNilai`)) AS Voucher, rt.Nama
					FROM
					  transaksi_header 
					  INNER JOIN transaksi_detail 
					    ON transaksi_header.`NoKassa` = transaksi_detail.`NoKassa` 
					    AND transaksi_header.`NoStruk` = transaksi_detail.`NoStruk` 
					  INNER JOIN masterbarang b 
					    ON transaksi_detail.`PCode` = b.`PCode` 
					  INNER JOIN subdivisi s 
					    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
					  INNER JOIN divisireport r 
					    ON s.`KdDivisiReport` = r.`KdDivisiReport`
					  INNER JOIN divisi ON s.`KdDivisi`=divisi.`KdDivisi` 
					  LEFT JOIN (
						SELECT NoStiker, Nama FROM (
						   SELECT r.NoStiker, t.`Nama` FROM register r INNER JOIN tourtravel t ON r.kdtravel=t.`KdTravel` 
						      WHERE r.`Tanggal`= '$v_date'
						   UNION ALL
						   SELECT NoStiker, t.Nama FROM trans_reservasi_konfirmasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel` 
						      WHERE r.TglKonfirmasi='$v_date') tx GROUP BY NoStiker	
						 ) rt ON transaksi_header.KdAgent=rt.NoStiker
					WHERE transaksi_header.status = '1' and transaksi_header.Tanggal = '$v_date' and transaksi_header.KdAgent='$v_nostiker'
					GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , b.`PCode`";
		}
		if($pilihan=="rekap"){
            $sql ="
            	SELECT 'HO' as NamaDivisi, 'Ticket' as SubDivisi,'' as Kasir, date(Tanggal) as Tanggal,
            	Time(Tanggal) as Waktu, NoTrans as NoStruk, SUM(ttl_amount) AS Gross,
            	0 as Discount,  SUM(ttl_amount) AS Nett, 0 as ServiceCharge, 0 as Tax,
            	SUM(ttl_amount) AS Sales, 0 as Voucher, rt.Nama
            	FROM ticket_head 
            	LEFT JOIN (
				SELECT NoStiker, Nama FROM (
				   SELECT r.NoStiker, t.`Nama` FROM register r INNER JOIN tourtravel t ON r.kdtravel=t.`KdTravel` 
				      WHERE r.`Tanggal`= '$v_date'
				   UNION ALL
				   SELECT NoStiker, t.Nama FROM trans_reservasi_konfirmasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel` 
				      WHERE r.TglKonfirmasi='$v_date') tx GROUP BY NoStiker	
				 ) rt ON `noidentitas`=rt.NoStiker
            	WHERE DATE(tanggal)='$v_date' AND noidentitas='$v_nostiker'
            	group by NoTrans
            	UNION ALL
                SELECT 
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  transaksi_header.`Kasir`,
				  transaksi_header.`Tanggal`,
				  transaksi_header.`Waktu`,
				  transaksi_header.`NoStruk`,
				  SUM(transaksi_detail.`Qty`*IF(transaksi_detail.Service_charge>0,transaksi_detail.`Harga`,transaksi_detail.`Harga`/1.1)) AS Gross,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Disc1`,transaksi_detail.`Disc1`/1.1)) AS Discount,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)) AS Nett,
				  SUM(transaksi_detail.`Netto` * transaksi_detail.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)+ (IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1) * transaksi_detail.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )
				  ) AS Sales,
				  SUM(transaksi_header.`Voucher`*(IF(
      				transaksi_detail.`Service_charge` > 0,
      				transaksi_detail.`Netto` * 1155 / 1000,
      				transaksi_detail.Netto
    				)/transaksi_header.`TotalNilai`)) AS Voucher, rt.Nama
				FROM
				  transaksi_header 
				  INNER JOIN transaksi_detail 
				    ON transaksi_header.`NoKassa` = transaksi_detail.`NoKassa` 
				    AND transaksi_header.`NoStruk` = transaksi_detail.`NoStruk` 
				  INNER JOIN masterbarang b 
				    ON transaksi_detail.`PCode` = b.`PCode` 
				  INNER JOIN subdivisi s 
				    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  LEFT JOIN (
					SELECT NoStiker, Nama FROM (
					   SELECT r.NoStiker, t.`Nama` FROM register r INNER JOIN tourtravel t ON r.kdtravel=t.`KdTravel` 
					      WHERE r.`Tanggal`= '$v_date'
					   UNION ALL
					   SELECT NoStiker, t.Nama FROM trans_reservasi_konfirmasi r INNER JOIN tourtravel t ON r.`KdTravel`=t.`KdTravel` 
					      WHERE r.TglKonfirmasi='$v_date') tx GROUP BY NoStiker	
					 ) rt ON transaksi_header.KdAgent=rt.NoStiker 
				  WHERE
                    transaksi_header.status = '1' and transaksi_header.Tanggal = '$v_date' and transaksi_header.KdAgent='$v_nostiker'
				GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , transaksi_header.`Tanggal`, transaksi_header.`NoStruk`";
		}
		$user = $this->session->userdata('username');
		if($user=='krisna337'){
			echo "<pre>";
			echo $sql;
			echo "</pre>";
		}
        $result = $this->getArrayResult($sql);
        return $result;
    }
    
    function getArrayResult($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) 
    {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) 
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() 
    {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
}

?>