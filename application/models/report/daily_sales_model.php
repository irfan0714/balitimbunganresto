<?php
class daily_sales_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getkassa()
	{
		$sql = "SELECT kassa.id_kassa, divisi.NamaDivisi, kassa.SubDivisi 
        			FROM kassa 
          			INNER JOIN divisi 
            		ON kassa.KdDivisi = divisi.KdDivisi 
        			WHERE 1 
        			ORDER BY kassa.id_kassa ASC" ;
		return $this->getArrayResult($sql);
	}
	
	function gettiket($v_date_from,$v_date_to,$jenis ){
		if($jenis=='N'){
			$wherejenis = " And length(noidentitas) < 4 ";
		}else{
			$wherejenis = 	"";
		}
		
		$sql = "SELECT `ticket`.noticket, `ticket`.add_date, `ticket`.jenis,`ticket`.harga
                	FROM `ticket`
                   	WHERE
                    1
                    AND `ticket`.add_date BETWEEN '$v_date_from' AND '$v_date_to'
                    AND ticket.`noidentitas` != '1234' and status=1 $wherejenis
                	ORDER BY `ticket`.noticket ASC";
        
        return $this->getArrayResult($sql);
	}
	
	function getvoucher($v_date_from,$v_date_to, $jenis ){
		if($jenis=='N'){
			$wherejenis = " And length(h.KdAgent) < 4 ";
		}else{
			$wherejenis = 	"";
		}
		
		$sql = "SELECT d.Tanggal, SUM(d.NilaiVoucher) AS pemakaian_voucher
                	FROM  transaksi_detail_voucher d inner join transaksi_header h on h.nostruk=d.nostruk
                    WHERE
                    1
                    AND d.Tanggal BETWEEN '$v_date_from' AND '$v_date_to' $wherejenis
                    AND h.status='1' and d.Jenis<>5
                	GROUP BY d.Tanggal";
        
        return $this->getArrayResult($sql);
	}
	
	function getsales($v_date_from,$v_date_to, $jenis ){
		if($jenis=='N'){
			$sql = "SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(d.`Service_charge`>0, d.`Netto`*1155/1000, d.Netto)) AS Sales
								FROM transaksi_header h INNER JOIN transaksi_detail d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
								INNER JOIN subdivisi s ON b.`KdSubDivisi`=s.`KdSubDivisi`
								INNER JOIN divisireport r ON s.`KdDivisiReport`=r.`KdDivisiReport`
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1 And length(h.KdAgent) < 4
								GROUP BY h.`Tanggal`, r.KdDivisiReport";
		}else{
			$sql = "SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
								FROM transaksi_header h INNER JOIN transaksi_detail d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
								INNER JOIN subdivisi s ON b.`KdSubDivisi`=s.`KdSubDivisi`
								INNER JOIN kassa k on h.NoKassa = k.id_kassa
								INNER JOIN divisireport r ON s.`KdDivisiReport`=r.`KdDivisiReport`
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1 
								GROUP BY h.`Tanggal`, r.KdDivisiReport
				UNION ALL
				SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
								FROM transaksi_header_sunset h INNER JOIN transaksi_detail_sunset d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN kassa k on h.NoKassa = k.id_kassa
				  				INNER JOIN subdivisi s ON k.`SubDivisi` = s.`KdSubDivisi` 
				  				INNER JOIN divisireport r ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  				INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi` 
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1
								GROUP BY h.`Tanggal`, r.KdDivisiReport";
			
		}
		
		return $this->getArrayResult($sql);
	}
	
	function getkomisi($v_date_from,$v_date_to ){
		$sql = "SELECT
                `finance_komisi_header`.NoTransaksi,
                `finance_komisi_header`.TglTransaksi,
                `finance_komisi_header`.Total
            	FROM
                	`finance_komisi_header`
            	WHERE
	                1
                	AND `finance_komisi_header`.TglTransaksi BETWEEN '$v_date_from' AND '$v_date_to'
            	ORDER BY
                	`finance_komisi_header`.TglTransaksi ASC";
		return $this->getArrayResult($sql);
	}
	
	function getcompliment($v_date_from,$v_date_to, $jenis ){
		if($jenis=='N'){
			$sql = "SELECT 
	              `transaksi_detail`.NoStruk,
	              `transaksi_detail`.Tanggal,
	              sUM(`transaksi_detail`.Qty * `transaksi_detail`.Harga) AS total 
	            FROM
	              `transaksi_detail` 
	              INNER JOIN `transaksi_header` ON
	                `transaksi_detail`.NoStruk = `transaksi_header`.NoStruk
	                and `transaksi_detail`.nokassa = `transaksi_header`.nokassa
	                AND `transaksi_header`.Status = '1'  
	                AND (`transaksi_header`.TotalNilai*1) = '0'  
	                AND `transaksi_header`.DPP*1 = '0'
	                and `transaksi_header`.Discount > 0
	            WHERE 1 
	              AND `transaksi_detail`.Tanggal BETWEEN '$v_date_from'  
	              AND '$v_date_to'  And length(transaksi_header.KdAgent) < 4 
	            GROUP BY
	              `transaksi_detail`.NoStruk,
	              `transaksi_detail`.Tanggal";
		}else{
			$sql = "SELECT 
	              `transaksi_detail`.NoStruk,
	              `transaksi_detail`.Tanggal,
	              sUM(`transaksi_detail`.Qty * `transaksi_detail`.Harga) AS total 
	            FROM
	              `transaksi_detail` 
	              INNER JOIN `transaksi_header` ON
	                `transaksi_detail`.NoStruk = `transaksi_header`.NoStruk
	                and `transaksi_detail`.nokassa = `transaksi_header`.nokassa
	                AND `transaksi_header`.Status = '1'  
	                AND (`transaksi_header`.TotalNilai*1) = '0'  
	                AND `transaksi_header`.DPP*1 = '0'
	                and `transaksi_header`.Discount > 0
	            WHERE 1 
	              AND `transaksi_detail`.Tanggal BETWEEN '$v_date_from'  
	              AND '$v_date_to'  
	            GROUP BY
	              `transaksi_detail`.NoStruk,
	              `transaksi_detail`.Tanggal
	            UNION ALL
	            SELECT 
	              `transaksi_detail_sunset`.NoStruk,
	              `transaksi_detail_sunset`.Tanggal,
	              sUM(`transaksi_detail_sunset`.Qty * `transaksi_detail_sunset`.Harga) AS total 
	            FROM
	              `transaksi_detail_sunset` 
	              INNER JOIN `transaksi_header_sunset` ON
	                `transaksi_detail_sunset`.NoStruk = `transaksi_header_sunset`.NoStruk
	                and `transaksi_detail_sunset`.nokassa = `transaksi_header_sunset`.nokassa
	                AND `transaksi_header_sunset`.Status = '1'  
	                AND (`transaksi_header_sunset`.TotalNilai*1) = '0'  
	                AND `transaksi_header_sunset`.DPP*1 = '0'
	                and `transaksi_header_sunset`.Discount > 0
	            WHERE 1 
	              AND `transaksi_detail_sunset`.Tanggal BETWEEN '$v_date_from' 
	              AND '$v_date_to'  
	            GROUP BY
	              `transaksi_detail_sunset`.NoStruk,
	              `transaksi_detail_sunset`.Tanggal
	            ";
	    	
		}
		          
		return $this->getArrayResult($sql);
	}
	
	function getdistribusikopi($v_date_from, $v_date_to){
		$hasil = array();
		$sql = "SELECT h.`sidate`, SUM(d.`quantity`*d.`nettprice`)*1.1 AS nilai FROM salesinvoice h 
    					INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
    					INNER JOIN deliveryorder o ON d.dono=o.dono
						INNER JOIN gudang g ON o.warehousecode=g.KdGudang
						INNER JOIN subdivisi s ON g.KdSubdivisi=s.KdSubdivisi
						WHERE sidate BETWEEN '$v_date_from' AND '$v_date_to' AND h.`status`=1 AND s.kddivisi='2'
						group by h.sidate";
		
		return $this->getArrayResult($sql);
	}
	
	function getdistribusiOH($v_date_from, $v_date_to){
		$hasil = array();
		$sql = "SELECT h.`sidate`, SUM(d.`quantity`*d.`nettprice`)*1.1 AS nilai FROM salesinvoice h 
    					INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
    					INNER JOIN deliveryorder o ON d.dono=o.dono
						INNER JOIN gudang g ON o.warehousecode=g.KdGudang
						INNER JOIN subdivisi s ON g.KdSubdivisi=s.KdSubdivisi
						WHERE  sidate BETWEEN '$v_date_from' AND '$v_date_to' AND h.`status`=1 AND s.kddivisi='1' and h.autoinvoice=0
						group by h.sidate";
		return $this->getArrayResult($sql);
	}
	
	function getdistribusivci($v_date_from, $v_date_to){
		$hasil=array();				
		$url = 'http://192.168.0.13/sss/update_npm.php?tgl1='.$v_date_from.'&tgl2='.$v_date_to;
		$aContext = array(
	    	'http' => array(
	        	'request_fulluri' => true,
	    	),
	    );
	    
		$cxContext = stream_context_create($aContext);
		$distribusivci = 0;
		try {
	     	$resp = file_get_contents($url, False, $cxContext);
	        $result = json_decode($resp, true);
	        
	        foreach($result as $rec){
				$pcode = $rec['PCode'];
				$Qty = $rec['Qty'];
				$sqlharga = "SELECT Harga FROM groupharga_detail d INNER JOIN mapping_barang_vci m ON d.`PCode`=m.`PCode`
					WHERE GroupHargaID=1 AND m.`PCodeVCI`='$pcode'";
				$row = $this->getArrayResult($sqlharga);
				if(count($row)>0){
					$harga = $row[0]['Harga']*1.1;
				}else{
					$harga = 0;
				}
				$distribusivci += $Qty * $harga;
			}
			$hasil = array(array('sidate'=>$v_date_to,
								'nilai'=>$distribusivci));
		}
		catch (Exception $e) {
	    	//echo $e->getMessage();
	    	echo "Koneksi gagal.";
		}
		//print_r($hasil);
		return $hasil;
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_daily'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function voucher($v_date){
		$mylib = new globallib();
		$sql = "SELECT novoucher, nominal, terpakai FROM voucher WHERE expdate='".$mylib->ubah_tanggal($v_date)."' and Jenis<>5 order by novoucher";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>