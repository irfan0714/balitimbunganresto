<?php
class Report_mutasi_hutang_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getSupplier()
	{
		$sql = "select KdSupplier,Nama as NamaSupplier from supplier order by KdSupplier";
		return $this->getArrayResult($sql);
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select TglTrans as TglTrans,DATE_FORMAT(TglTrans-3,'%Y-%m-%d') as TglTrans2 from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	
	function getdata($tahun, $bulan, $kdrekening){
		$bulan = $bulan*1;
		
		if($bulan==1){
			$bulanlalu=12;
			$tahunlalu=$tahun-1;
		}else{
			$bulanlalu=$bulan-1;
			$tahunlalu=$tahun;	
		}
		
		$sql = "SELECT a.KdSupplier, s.Nama, SUM(SaldoAwal) AS Awal, SUM(Beli) AS Beli, SUM(CN) AS CN, SUM(Bayar) AS Bayar, SUM(DN) AS DN, SUM(SaldoAwal+Beli+CN-Bayar-DN) AS Akhir
				FROM (
				SELECT KdSupplier, SUM(IF(TipeTransaksi IN('o','I','IM','C','CM'),Jumlah*Kurs-SelisihKurs,(Jumlah*Kurs-SelisihKurs)*-1 )) AS SaldoAwal, 0 AS Beli, 0 AS CN, 0 AS Bayar, 0 AS DN
				FROM mutasi_hutang WHERE Tahun='$tahunlalu' AND Bulan='$bulanlalu' AND KdRekening='$kdrekening'
				GROUP BY KdSupplier
				UNION ALL
				SELECT h.`KdSupplier`, 0 AS SaldoAwal, 
				SUM(IF(h.`TipeTransaksi`='I' || h.`TipeTransaksi`='IM', h.`NilaiTransaksi`,0)) AS Beli,
				SUM(IF(h.`TipeTransaksi`='C' || h.`TipeTransaksi`='CM', h.`NilaiTransaksi`,0)) AS CN,
				SUM(IF(h.`TipeTransaksi`='P' || h.`TipeTransaksi`='PM', h.`NilaiTransaksi`,0)) AS Bayar,
				SUM(IF(h.`TipeTransaksi`='D' || h.`TipeTransaksi`='DM', h.`NilaiTransaksi`,0)) AS DN
				FROM hutang h
				WHERE YEAR(h.`Tanggal`)='$tahun' AND MONTH(h.`Tanggal`)='$bulan' AND h.`KdRekening`='$kdrekening'
				GROUP BY h.NoDokumen) a INNER JOIN supplier s ON a.KdSupplier =s.KdSupplier
				GROUP BY a.KdSupplier";	
		
		return $this->getArrayResult($sql);
	}    
	
	function geRekeningHutang(){
		$sql="SELECT h.`KdRekening`, concat(h.KdRekening,'-',r.`NamaRekening`) as NamaRekening FROM hutang h INNER JOIN rekening r ON h.`KdRekening`=r.`KdRekening`
				GROUP BY h.`KdRekening`";
				
		return $this->getArrayResult($sql);
				
	}
	
	function getNamaRekening($kdrekening){
		$sql = "select NamaRekening from rekening where kdrekening='$kdrekening'";
		$row = $this->getArrayResult($sql);
		return $row[0]['NamaRekening'];
		
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>