<?php
class Report_mutasi_piutang_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getCustomer()
	{
		$sql = "select KdCustomer,Nama as NamaCustomer from customer order by KdCustomer";
		return $this->getArrayResult($sql);
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select TglTrans as TglTrans,DATE_FORMAT(TglTrans-3,'%Y-%m-%d') as TglTrans2 from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getNama($customer)
	{
		$sql = "select KdCustomer,Nama as NamaCustomer from customer where KdCustomer='$customer'";
		return $this->getRow($sql);
	}
	function getRekapTrans($tahun,$bulan)
	{
		$sql = "SELECT concat(COALESCE(b.Nama,''),COALESCE(t.Nama,'')) as NamaCustomer,Jumlah as NilaiAwal
		FROM mutasi_piutang a left join customer b on a.KdCustomer = b.KdCustomer
		LEFT JOIN tourtravel t ON a.`KdCustomer`=t.KdTravel
		WHERE Tahun='$tahun' and Bulan='$bulan' 
		and a.TipeTransaksi='O'";
		return $this->getRow($sql);
	}
	function getDetailTrans($tahun,$bulan)
	{
		$sql = "SELECT * from mutasi_piutang WHERE Tahun='$tahun' and Bulan='$bulan' and TipeTransaksi<>'O'
		        ORDER BY NoDokumen ASC, TipeTransaksi DESC";
		return $this->getArrayResult($sql);
	}
    
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>