<?php
class Alljurnalmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getRekening()
	{
    	$sql = "SELECT KdRekening, NamaRekening from rekening where tingkat='3' order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function getDate() {
        $sql = "select PeriodeGL from aplikasi order by Tahun desc limit 0,1";
        $row = $this->getArrayResult($sql);
        return $row[0]['PeriodeGL'];
    }
	function getReport($sql)
	{
		return $this->getArrayResult($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>