<?php
class Kartu_stock_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getListTahun()
	{
		$sql = "select distinct Tahun from counter order by Tahun";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        $query->free_result();
        return $row;
	}
	function getGudang()
	{
		$sql = "select KdGudang,Keterangan from gudang order by KdGudang";
		return $this->getArrayResult($sql);
	}
	function getBarang()
	{
		$sql = "select PCode,NamaLengkap from masterbarang order by PCode";
		return $this->getArrayResult($sql);
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select TglTrans as TglTrans,DATE_FORMAT(TglTrans-3,'%Y-%m-%d') as TglTrans2 from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getRekapTrans($tahun,$bulan,$gudang,$barang)
	{
	    $fieldawal1 = "GAwal".$bulan;
		$fieldawal2 = "GNAwal".$bulan;
		$sql = "SELECT NamaLengkap as NamaBarang,$fieldawal1 as QtyAwal, $fieldawal2 as NilaiAwal
		FROM stock_simpan a, masterbarang b WHERE Tahun='$tahun' and KdGudang='$gudang' and a.PCode='$barang'
		and a.PCode=b.PCode";
		return $this->getRow($sql);
	}
	function getDetailTrans($tahun,$bulan,$gudang,$barang)
	{
		$sql = "SELECT * from mutasi_barang WHERE Year(TglTransaksi)='$tahun' and Month(TglTransaksi)='$bulan' and KdGudang='$gudang' and PCode='$barang'
		        ORDER BY TglTransaksi ASC, JenisMutasi DESC";
		return $this->getArrayResult($sql);
	}
    
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>