<?php
class Report_Cost_Of_Food_Model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getdata($subkategori,$date_awal,$date_akhir, $gudang)
	{
		if($subkategori=='')
			$where = '';
		else
			$where = "and b.`KdSubKategori`='$subkategori'";
			
        $sql = "SELECT s.NamaSubKategori, p.`purpose`, tbl.PCode, b.NamaLengkap, SUM(tbl.QtyPcs) AS Qty, SUM(tbl.QtyPcs*tbl.Harga) AS Nilai, b.SatuanSt as Satuan
				FROM (
				SELECT d.`PCode`, d.`QtyPcs`, d.`Harga`, h.`PurposeId`
				FROM trans_pengeluaran_lain h INNER JOIN trans_pengeluaran_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
				WHERE h.`TglDokumen` BETWEEN '$date_awal' AND '$date_akhir' AND h.`KdGudang`='$gudang' AND h.`Status`=1
				UNION ALL
				SELECT d.`PCode`, d.`QtyPcs`*-1, d.`Harga`, h.`PurposeId`
				FROM trans_penerimaan_lain h INNER JOIN trans_penerimaan_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
				WHERE h.`TglDokumen` BETWEEN '$date_awal' AND '$date_akhir' AND h.`KdGudang`='$gudang' AND h.`Status`=1) tbl
				INNER JOIN masterbarang b ON tbl.PCode=b.`PCode`
				INNER JOIN subkategori s ON b.`KdSubKategori`=s.`KdSubKategori`
				INNER JOIN intmutpurpose p ON tbl.PurposeId=p.`purposeid`
				WHERE 1 $where
				GROUP BY b.KdSubKategori, tbl.Purposeid, tbl.PCode";
				
		//echo $sql;die();
        
		return $this->getArrayResult($sql);
    }

	
	function getgudang($username){
		$sql = "SELECT g.KdGudang, g.Keterangan FROM gudang g INNER JOIN gudang_admin ga ON g.KdGudang=ga.KdGudang
			WHERE ga.UserName='$username'";
		return $this->getArrayResult($sql);
	}
	
	function getsubkategori(){
		$sql = "SELECT KdSubKategori, NamaSubKategori FROM subkategori WHERE kdkategori='1'";
		return $this->getArrayResult($sql);
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>