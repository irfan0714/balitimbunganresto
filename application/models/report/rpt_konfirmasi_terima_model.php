<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpt_Konfirmasi_Terima_Model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	
	function viewData($tgldari, $tglsampai) {

		$sql ="SELECT h.`NoPenerimaan`, h.`NoPO`, h.`NoFaktur`, h.`Tanggal`, h.`NoFakturSupplier`, h.`NoFakturPajak`, s.`Nama`,
				SUM(d.Qty*d.`Harga`) AS DPP, SUM(d.Qty*d.`Harga`*h.PPN/100) AS Tax, SUM(d.`Qty`*d.`Harga`*(1+h.PPN/100)) AS Total
 				FROM invoice_pembelian_header h INNER JOIN invoice_pembelian_detail d ON h.`NoFaktur`=d.`NoFaktur`
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				INNER JOIN masterbarang b ON d.`KdBarang`= b.`PCode`
				WHERE h.`Tanggal` BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY h.`NoFaktur`,h.`NoPenerimaan`";
		
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
    }
    
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */