<?php
class Outstanding_UM_Model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getData()
	{
		$sql = "SELECT e.`employee_name`, u.`NoDokumen`, u.`NoPV`, u.`TglDokumen`, u.`Keterangan`, u.Sisa FROM uang_muka u 
				INNER JOIN employee e ON u.`Employee`=e.`employee_id`
				WHERE sisa>0 ORDER BY e.`employee_name`, u.`TglDokumen`";
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>