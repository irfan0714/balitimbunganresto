<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_produksi_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function getReport($tgldari, $tglsampai,$gudang) {
		$sql ="SELECT p.`productiondate`, f.`formulaname`, p.`batchnumber`,p.`mixquantity` , fg.`inventorycode`, b.namalengkap, fg.`quantity`, fg.`quantityqc`, fg.quantity_qc_reject,
				f.`finishgood_wgt`*(fg.`quantity`+fg.`quantityqc`+fg.`quantity_qc_reject`) AS fgwgt, f.`finishgood_wgt`*(fg.`quantity`+fg.`quantityqc`+fg.`quantity_qc_reject`)/p.mixquantity*100 AS Persentase
				FROM production p
				INNER JOIN formula f ON p.`formulanumber`=f.`formulanumber`
				INNER JOIN (select fg1.batchnumber, fg1.inventorycode, sum(fg1.`quantity`) as quantity, sum(fg1.`quantityqc`) as quantityqc,
				 sum(fg1.quantity_qc_reject) as quantity_qc_reject from productionfinishgood fg1 group by batchnumber) fg ON p.`batchnumber`=fg.`batchnumber` AND p.`status`=1
				INNER JOIN masterbarang b ON fg.inventorycode=b.pcode
				WHERE p.productiondate BETWEEN '$tgldari' AND '$tglsampai' and p.warehousecode='$gudang' ORDER BY p.`productiondate`;";
		$query = $this->db->query($sql);
        return $query->result_array();
	}
	
	function listgudang($username){
		$sql = "SELECT g.`KdGudang`, g.`Keterangan` FROM gudang g INNER JOIN gudang_admin ga ON g.`KdGudang`=ga.`KdGudang`
					WHERE ga.`UserName`='$username'";
		
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
				
        return $result;
	}
    
}
?>