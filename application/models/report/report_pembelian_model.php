<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_pembelian_model extends CI_Model {


	public function __construct()
	{ 
		parent::__construct();
		
	}

	public function viewdata($tgldari, $tglsampai)
	{
		$sql = "SELECT 
					MataUang, supplier.KdSupplier AS KdSupplier, Nama, Tanggal, NoPO, NoPenerimaan, NoFaktur,
					SUM(trans_terima_detail.`Qty` * trans_terima_detail.`Harga`) AS DPP, 
					SUM(`trans_terima_detail`.`PPn`) AS PPN,
					((SUM(trans_terima_detail.`Qty` * trans_terima_detail.`Harga`)) + ((SUM(trans_terima_detail.`Qty` * trans_terima_detail.`Harga`)*SUM(`trans_terima_detail`.`PPn`)/100))) AS Total
				FROM 
					`invoice_pembelian_header`
				INNER JOIN 
					`trans_terima_detail` 
					ON 
						`trans_terima_detail`.`NoDokumen` = `invoice_pembelian_header`.`NoPenerimaan`
				INNER JOIN 
					`trans_terima_header` 
					ON  
						`trans_terima_header`.`NoDokumen` = trans_terima_detail.`NoDokumen`
				INNER JOIN 
					supplier 
					ON 
						supplier.`KdSupplier` = `trans_terima_header`.`KdSupplier`
				WHERE 
					`trans_terima_detail`.`Status`=1 
					AND 
						Tanggal BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY 
					MataUang, Nama, Tanggal, NoPO, NoPenerimaan, NoFaktur
				ORDER BY 
					MataUang"; 
				
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result; 
	}
	//a24

}
/* End of file report_pembelian.php */
/* Location: ./application/models/report_pembelian.php */