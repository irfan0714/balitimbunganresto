<?php
class Report_hutangmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getSupplier()
	{
		$sql = "select KdSupplier,Nama from supplier order by KdSupplier";
		return $this->getArrayResult($sql);
	}
	function getDate()
	{
		$sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getReport($date)
	{
		$sql = "SELECT h.`MataUang`, h.`KdSupplier`, s.`Nama`, h.`NoDokumen`, 
		            CONCAT(COALESCE(i.NoPO,''),COALESCE(rg.PONo,'')) AS NoPO, 
					CONCAT(COALESCE(i.NoPenerimaan,''),COALESCE(rg.NoDokumen,'')) AS NoPenerimaan, 
					i.NoFakturSupplier,
					h.tipetransaksi, h.`Tanggal`, h.`JatuhTempo`, h.`NilaiTransaksi`, h.`Sisa`+COALESCE(p.Bayar,0) AS SisaHutang ,
					concat(h.KdRekening,'-',r.NamaRekening) as KdRekening
					FROM hutang h
					INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
					left join rekening r on h.KdRekening = r.KdRekening
					left join invoice_pembelian_header i on h.NoDokumen=i.NoFaktur
					LEFT JOIN pi_marketing p
						ON h.NoDokumen = p.NoDokumen
					LEFT JOIN rg_marketing rg
						ON rg.NoDokumen = p.RGNo
					LEFT JOIN (
					select h2.NoFaktur, sum(h2.NilaiTransaksi) as Bayar from hutang h2 where h2.Tanggal>'$date' and h2.TipeTransaksi in('P','PM','D','DM','A','DA')
					group by h2.NoFaktur) p ON h.NoDokumen=p.NoFaktur
					WHERE h.tanggal<='$date' AND h.tipetransaksi in('I','IM','C','CM')
					HAVING SisaHutang<>0
					ORDER BY h.KdRekening, h.`KdSupplier`, h.`NoDokumen`";
					//echo $sql;
		return $this->getArrayResult($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>