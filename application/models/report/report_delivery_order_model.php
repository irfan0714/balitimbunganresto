<?php
class Report_Delivery_Order_Model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getCustomer()
	{
		$sql = "select KdCustomer, Nama from customer order by Nama";
		return $this->getArrayResult($sql);
	}
	
	function getReport($v_start_date,$v_end_date,$kdcustomer){
		if($kdcustomer=='')
			$wherecustomer = '';
		else
			$wherecustomer = " And h.customerid='" . $kdcustomer . "'";
		$sql = "SELECT h.`dodate`, h.`dono`, b.`NamaLengkap`, g.`Keterangan` AS gudang, 
				d.`inventorycode`, d.`quantity`,d.`hppvalue`, c.`Nama` AS Pelanggan
				FROM deliveryorder h INNER JOIN deliveryorderdetail d ON h.`dono`=d.`dono`
				INNER JOIN customer c ON h.`customerid`=c.`KdCustomer`
				INNER JOIN masterbarang b ON d.`inventorycode`=b.`PCode`
				INNER JOIN gudang g ON h.`warehousecode`=g.`KdGudang`
				WHERE h.`dodate` between '$v_start_date' and '$v_end_date' $wherecustomer AND h.`status`=1 
				order by h.dodate, h.dono";
		
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>