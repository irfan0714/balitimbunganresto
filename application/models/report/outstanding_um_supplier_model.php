<?php
class Outstanding_UM_Supplier_Model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getData()
	{
		$sql = "SELECT s.`Nama`, u.`NoDokumen`, u.`NoPV`, u.`TglDokumen`, u.`Keterangan`, u.Sisa FROM uang_muka_supplier u 
				INNER JOIN supplier s ON u.`KdSupplier`=s.`KdSupplier`
				WHERE sisa>0 ORDER BY s.`Nama`, u.`TglDokumen`";
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>