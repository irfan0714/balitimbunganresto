<?php
class Report_lainlain_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getHeader($type,$date_awal,$date_akhir, $gudang, $status)
	{
			
        if($type=="Penerimaan")
        {
        	$where_gudang = $gudang != '0' ? " and trans_penerimaan_lain.KdGudang='$gudang'" : '';
        	
        	if($status=='')
				$wherestatus = '';
			else
				$wherestatus = " and trans_penerimaan_lain.status='$status'";
			
        	$sql = "
	    		SELECT 
				  trans_penerimaan_lain.NoDokumen,
				  DATE_FORMAT(trans_penerimaan_lain.TglDokumen,'%d-%m-%Y') AS TglDokumen,
				  trans_penerimaan_lain.KdGudang,
				  gudang.Keterangan AS nama_gudang,
				  trans_penerimaan_lain.PurposeId,
				  intmutpurpose.purpose,
				  trans_penerimaan_lain.Keterangan,
				  DATE_FORMAT(trans_penerimaan_lain.AddDate,'%d-%m-%Y') AS ADDDATE,
				  trans_penerimaan_lain.AddUser,
				  trans_penerimaan_lain.EditDate,
				  trans_penerimaan_lain.EditUser,
				  trans_penerimaan_lain.Status 
				FROM
				  trans_penerimaan_lain 
				  INNER JOIN intmutpurpose 
				    ON trans_penerimaan_lain.PurposeId = intmutpurpose.purposeid 
				  INNER JOIN gudang 
				    ON trans_penerimaan_lain.KdGudang = gudang.KdGudang 
				WHERE 1 
				  AND trans_penerimaan_lain.TglDokumen BETWEEN '".$date_awal."' AND '".$date_akhir."' $where_gudang $wherestatus
				ORDER BY
				  trans_penerimaan_lain.TglDokumen ASC,
				  trans_penerimaan_lain.NoDokumen ASC
	        ";  
			
		}
		else if($type=="Pengeluaran")
        {
        	$where_gudang = $gudang != '0' ? " and trans_pengeluaran_lain.KdGudang='$gudang'" : '';
        	
        	if($status=='')
				$wherestatus = '';
			else
				$wherestatus = " and trans_pengeluaran_lain.status='$status'";
				
        	$sql = "
	    		SELECT 
				  trans_pengeluaran_lain.NoDokumen,
				  DATE_FORMAT(trans_pengeluaran_lain.TglDokumen,'%d-%m-%Y') AS TglDokumen,
				  trans_pengeluaran_lain.KdGudang,
				  gudang.Keterangan AS nama_gudang,
				  trans_pengeluaran_lain.PurposeId,
				  intmutpurpose.purpose,
				  trans_pengeluaran_lain.Keterangan,
				  DATE_FORMAT(trans_pengeluaran_lain.AddDate,'%d-%m-%Y') AS ADDDATE,
				  trans_pengeluaran_lain.AddUser,
				  trans_pengeluaran_lain.EditDate,
				  trans_pengeluaran_lain.EditUser,
				  trans_pengeluaran_lain.Status 
				FROM
				  trans_pengeluaran_lain 
				  INNER JOIN intmutpurpose 
				    ON trans_pengeluaran_lain.PurposeId = intmutpurpose.purposeid 
				  INNER JOIN gudang 
				    ON trans_pengeluaran_lain.KdGudang = gudang.KdGudang 
				WHERE 1 
				  AND trans_pengeluaran_lain.TglDokumen BETWEEN '".$date_awal."' AND '".$date_akhir."' $where_gudang $wherestatus
				ORDER BY
				  trans_pengeluaran_lain.TglDokumen ASC,
				  trans_pengeluaran_lain.NoDokumen ASC
	        "; 
		}
		   
        //echo $sql; die();
        //echo "<hr/>";*/
        
		return $this->getArrayResult($sql);
    }

	function getDetail($type,$arr_nodok)
	{
		if($type=="Penerimaan")
		{
			$sql = "
				SELECT 
				  t.Sid, t.NoDokumen,t.PCode,b.NamaLengkap as NamaBarang,b.SatuanSt as Satuan, t.QtyPcs as Qty, t.Keterangan 
				FROM
				  trans_penerimaan_lain_detail t inner join masterbarang b on t.PCode=b.PCode
				WHERE 1 
				  ".$arr_nodok."
				ORDER BY 
				  t.NoDokumen
			";
			
		}
		else if($type=="Pengeluaran")
		{
			$sql = "
				SELECT 
				  t.Sid, t.NoDokumen,t.PCode,b.NamaLengkap as NamaBarang,b.SatuanSt as Satuan, t.QtyPcs as Qty, t.Keterangan
				FROM
				  trans_pengeluaran_lain_detail t inner join masterbarang b on t.PCode=b.PCode
				WHERE 1 
				  ".$arr_nodok."
				ORDER BY 
				  t.NoDokumen
			";
		}
		
		/*echo $sql;
        echo "<hr/>";*/
        
        return $this->getArrayResult($sql);
	}
	
	function getgudang($username){
		$sql = "SELECT g.KdGudang, g.Keterangan FROM gudang g INNER JOIN gudang_admin ga ON g.KdGudang=ga.KdGudang
			WHERE ga.UserName='$username'";
		return $this->getArrayResult($sql);
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>