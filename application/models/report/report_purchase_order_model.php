<?php
class Report_purchase_order_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getArrayGudang($arrdata)
    {
    	$where = "";
    	if(count($arrdata)*1>0)
    	{
			$where= $arrdata;	
		}
		
    	$sql = "
    		SELECT 
			  gudang.KdGudang,
			  gudang.Keterangan 
			FROM
			  gudang 
			WHERE 
			  1
			  ".$where." 
			ORDER BY 
			  gudang.Keterangan ASC
		";
		
		return $this->getArrayResult($sql);
	}
	
	function getArraySupplier()
    {
    	$sql = "
    		SELECT 
			  supplier.KdSupplier,
			  supplier.Nama 
			FROM
			  supplier 
			WHERE 1 
			  AND supplier.StatAktif = 'Y' 
			ORDER BY supplier.Nama ASC,
			  supplier.KdSupplier ASC
		";
		
		return $this->getArrayResult($sql);
	}
	
	function getArrayCurrency()
    {
    	$sql = "
    		SELECT 
			  mata_uang.Kd_Uang, 
			  mata_uang.Keterangan 
			FROM
			  mata_uang 
			WHERE 1 
			ORDER BY mata_uang.id ASC
		";
		
		return $this->getArrayResult($sql);
	}

    function getArrayHeader($type,$start_date,$end_date,$gudang,$supplier,$currency,$pono,$prno,$status,$orderby)
    {
    	$mylib = new globallib();
    	
    	$start_date = $mylib->ubah_tanggal($start_date);
        $end_date = $mylib->ubah_tanggal($end_date);
        
        $where = "";
        
        if($gudang!="all")
        {
			$where .= "AND trans_order_barang_header.KdGudang='".$gudang."'";
		}
		
        if($supplier!="all")
        {
			$where .= "AND trans_order_barang_header.KdSupplier='".$supplier."'";
		}
		
        if($currency!="all")
        {
			$where .= "AND trans_order_barang_header.currencycode='".$currency."'";
		}
		
        if($pono)
        {
			$where .= "AND trans_order_barang_header.NoDokumen LIKE '%".trim($pono)."%'";
		}
		
        if($prno)
        {
			$where .= "AND trans_order_barang_header.NoPr LIKE '%".trim($prno)."%'";
		}
		
        if($status!="all")
        {
			$where .= "AND trans_order_barang_header.Status ='".$status."'";
		}
		
		if($orderby=="podate")
		{
			$order = "trans_order_barang_header.TglDokumen ASC,trans_order_barang_header.NoDokumen ASC";	
		}
		else if($orderby=="supplier")
		{
			$order = "nama_supplier ASC,trans_order_barang_header.KdSupplier ASC,trans_order_barang_header.TglDokumen ASC";	
		}
		else if($orderby=="currency")
		{
			$order = "trans_order_barang_header.currencycode ASC,trans_order_barang_header.NoDokumen ASC";
		}
    	
		$sql="
			SELECT 
			  trans_order_barang_header.*,
			  supplier.Nama AS nama_supplier,
			  supplier.Contact,
			  gudang.Keterangan AS nama_gudang,
			  mata_uang.Keterangan AS nama_currency 
			FROM
			  trans_order_barang_header 
			  INNER JOIN supplier 
			    ON trans_order_barang_header.KdSupplier = supplier.KdSupplier 
			  INNER JOIN gudang 
			    ON trans_order_barang_header.KdGudang = gudang.KdGudang 
			  INNER JOIN mata_uang 
			    ON trans_order_barang_header.currencycode = mata_uang.Kd_Uang 
			WHERE 
			  1
			  ".$where."
			  AND trans_order_barang_header.TglDokumen BETWEEN '".$start_date."' AND '".$end_date."'
			ORDER BY 
			  ".$order."
		";
		
		return $this->getArrayResult($sql);	
	}
	
	function getArrayDetail($arrdata)
	{
		$where = "";
		if($arrdata)
		{
			$where = $arrdata;
		}
		
		$sql = "
			SELECT 
			  trans_order_barang_detail.*,
			  masterbarang.NamaLengkap as nama_barang
			FROM
			  trans_order_barang_detail 
			  INNER JOIN masterbarang 
			    ON trans_order_barang_detail.PCode = masterbarang.PCode 
			WHERE 
			  1 
			  ".$where."
			ORDER BY trans_order_barang_detail.Sid DESC
		";
		
		return $this->getArrayResult($sql);	
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>