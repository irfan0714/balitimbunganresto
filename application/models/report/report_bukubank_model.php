<?php
class Report_bukubank_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function listbank($username){
		$sql = "SELECT b.`KdKasBank`, b.`NamaKasBank` FROM kasbank b INNER JOIN userkasbank u ON b.`KdKasBank`=u.`KdKasBank`	
				WHERE u.`UserName`='$username' ORDER BY b.`NamaKasBank`";
	
		return $this->getArrayResult($sql);
    }
    
    function getSaldoAwal($tahun, $bulan, $bank){
    	if($bulan==1){
			$bulan=12;
			$tahun -= 1;
		}else{
			$bulan -= 1;
		}
		$sql = "SELECT akhir FROM saldo_bank WHERE tahun='$tahun' AND bulan='$bulan' AND kdkasbank='$bank'";	
		
		return $this->getArrayResult($sql);
	}
	
	function getReport($tahun, $bulan, $bank){
		$sql = "SELECT TglDokumen, NoDokumen, Keterangan, Debet, Kredit FROM (
				SELECT h.`TglDokumen`, h.`NoDokumen`, h.`keterangan`, 0 AS Debet, h.`JumlahPayment` AS Kredit FROM trans_payment_header h 
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`KdKasBank`='$bank' AND h.`Status`<>'B'
				UNION ALL
				SELECT h.`TglDokumen`, h.`NoDokumen`, h.`keterangan`, h.`JumlahReceipt` AS Debet, 0 AS Kredit FROM trans_receipt_header h 
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`KdKasBank`='$bank' AND h.`Status`<>'B'
				) t ORDER BY TglDokumen, NoDokumen";
		
		return $this->getArrayResult($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>