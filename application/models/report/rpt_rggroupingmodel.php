<?php

//purwanto on sept 16
class Rpt_rggroupingmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }

    function getReport_header($tglawal, $tglakhir) {
        $sql = "SELECT 
                        id_mutasi, DATE_FORMAT(Tanggal,'%d-%m-%Y') AS Tanggal, MPCode, masterbarang.NamaLengkap AS NamaPaket, Qty, Jenis_mutasi, mutasi_paket.Keterangan, gudang.Keterangan AS namagudang 
                      FROM
                        mutasi_paket 
                        LEFT JOIN gudang 
                          ON mutasi_paket.KdGudang = gudang.KdGudang
                        LEFT JOIN masterbarang
                          ON mutasi_paket.MPCode = masterbarang.PCode
                        WHERE DATE_FORMAT(mutasi_paket.Tanggal,'%Y-%m-%d') BETWEEN '".$tglawal."' AND '".$tglakhir."'";
        //echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_Detail($bulan, $tahun, $gudang, $divisi, $tabel_field, $search_by) {
        
        
        if($search_by=="gudang"){
		        $sql="
		        		SELECT 
						   d.`Nama`,
						   a.`PCode`,
						   b.`NamaLengkap`,
						   b.SatuanSt,
						   b.KdSubKategori,
						   c.NamaSubKategori,
						   SUM(a.`QtyPcs`) AS Qty,
						   avg(a.Qty*a.`Harga`/a.QtyPcs) as Harga
						FROM
						  `trans_terima_detail` a 
						  INNER JOIN masterbarang b 
							ON a.`PCode` = b.`PCode` 
						  INNER JOIN subkategori c 
							ON b.`KdSubKategori` = c.`KdSubKategori`
						  INNER JOIN supplier d
							ON a.`KdSupplier` = d.`KdSupplier`
						WHERE 
						MONTH(a.`TglDokumen`)='$bulan' 
						AND YEAR(a.`TglDokumen`)='$tahun'
						AND a.`KdGudang`='$gudang'
						GROUP BY a.`PCode`, a.KdSupplier
						ORDER BY b.`NamaLengkap` ASC;
		             ";
		}else{
				$sql="
						SELECT 
						   d.`Nama`,
						   a.`PCode`,
						   b.`NamaLengkap`,
						   b.SatuanSt,
						   b.KdSubKategori,
						   c.NamaSubKategori,
						   SUM(a.`QtyPcs`) AS Qty,
						   avg(a.Qty*a.`Harga`/a.QtyPcs) as Harga
						FROM
						  `trans_terima_detail` a 
						  INNER JOIN masterbarang b 
							ON a.`PCode` = b.`PCode` 
						  INNER JOIN subkategori c 
							ON b.`KdSubKategori` = c.`KdSubKategori`
						  INNER JOIN supplier d
							ON a.`KdSupplier` = d.`KdSupplier`
						WHERE 
						MONTH(a.`TglDokumen`)='$bulan' 
						AND YEAR(a.`TglDokumen`)='$tahun'
						AND b.KdDivisi='$divisi'
						GROUP BY a.`PCode`, a.KdSupplier
						ORDER BY b.`NamaLengkap` ASC;
					";
		}	
        //echo $sql;
        return $this->getArrayResult($sql);
    }

	function getGudang()
	{
		$sql = "select KdGudang,Keterangan from gudang order by KdGudang";
		return $this->getArrayResult($sql);
	}
	
	function getDivisi()
	{
		$sql = "select KdDivisi,NamaDivisi from divisi order by KdDivisi";
		return $this->getArrayResult($sql);
	}

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

}

?>