<?php
class daily_sales_non_guide_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getkassa()
	{
		$sql = "SELECT kassa.id_kassa, divisi.NamaDivisi, kassa.SubDivisi 
        			FROM kassa 
          			INNER JOIN divisi 
            		ON kassa.KdDivisi = divisi.KdDivisi 
        			WHERE 1 
        			ORDER BY kassa.id_kassa ASC" ;
		return $this->getArrayResult($sql);
	}
	
	function getsales($v_date_from,$v_date_to ){
		$sql = "SELECT h.`Tanggal`, b.Jenis , SUM(IF(d.`Service_charge`>0, d.`Netto`*1155/1000, d.Netto)) AS Sales
								FROM transaksi_header h INNER JOIN transaksi_detail d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1
								GROUP BY h.`Tanggal`, b.Jenis";
		return $this->getArrayResult($sql);
	}
	
	function getcompliment($v_date_from,$v_date_to ){
		$sql = "SELECT 
	              `transaksi_detail`.NoStruk,
	              `transaksi_detail`.Tanggal,
	              sUM(`transaksi_detail`.Qty * `transaksi_detail`.Harga) AS total 
	            FROM
	              `transaksi_detail` 
	              INNER JOIN `transaksi_header` ON
	                `transaksi_detail`.NoStruk = `transaksi_header`.NoStruk
	                and `transaksi_detail`.nokassa = `transaksi_header`.nokassa
	                AND `transaksi_header`.Status = '1'  
	                AND (`transaksi_header`.TotalNilai*1) = '0'  
	                AND `transaksi_header`.DPP*1 = '0'
	                and `transaksi_header`.Discount > 0
	            WHERE 1 
	              AND `transaksi_detail`.Tanggal BETWEEN '$v_date_from' 
	              AND '$v_date_to'  
	            GROUP BY
	              `transaksi_detail`.NoStruk,
	              `transaksi_detail`.Tanggal";
	              
		return $this->getArrayResult($sql);
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_daily'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>