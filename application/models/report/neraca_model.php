<?php
class Neraca_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getListTahun()
	{
		$sql = "select distinct Tahun from counter order by Tahun";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        $query->free_result();
        return $row;
	}
	function getDept()
	{
		$sql = "select KdDepartemen,NamaDepartemen from departemen order by KdDepartemen";
		return $this->getArrayResult($sql);
	}
	
	function getRekening()
	{
		$sql = "select KdRekening,CONCAT(KdRekening,'-',NamaRekening) as NamaRekening from rekening where tingkat='3' order by KdRekening";
		return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select PeriodeGL from aplikasi order by Tahun desc limit 0,1";
		$row =  $this->getArrayResult($sql);
		return $row[0]['PeriodeGL'];
	}
	
	function getReportPerbandingan($tahun, $bulan)
	{	
		$thnlalu= $tahun-1;
		
		$sql ="Select Parent1, NamaRekening1, Parent2, NamaRekening2, KdRekening, NamaRekening, 
				SUM(IF(SUBSTR(KdRekening,1,1)=1,NilaiBlnIni,NilaiBlnIni*-1)) AS NilaiBlnIni,
				SUM(IF(SUBSTR(KdRekening,1,1)=1,NilaiBlnLalu,NilaiBlnLalu*-1)) AS NilaiBlnLalu, 
				SUM(IF(SUBSTR(KdRekening,1,1)=1,NilaiThnLalu,NilaiThnLalu*-1)) AS NilaiThnLalu from (
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
						r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
						s.`KdRekening`, r.`NamaRekening`, 
						SUM(Awal$bulan+Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan) AS NilaiBlnIni, SUM(Awal$bulan) as NilaiBlnLalu, 0 as NilaiThnLalu 
						FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
						INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
						INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
						WHERE tahun='$tahun' AND s.KdRekening between '10000000' and '39999999'
						GROUP BY s.`KdRekening` having(NilaiBlnIni<>0 or NilaiBlnLalu)
							UNION ALL
						SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
						r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
						s.`KdRekening`, r.`NamaRekening`, 0 as NilaiBlnIni,0 as  NilaiBlnLalu,
						SUM(Awal12+Debet12-Kredit12+AdjDebet12-AdjKredit12) as NilaiThnLalu 
						FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
						INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
						INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
						WHERE tahun='$thnlalu' AND s.KdRekening between '10000000' and '39999999'
						GROUP BY s.`KdRekening` having(NilaiThnLalu<>0)) t
						group by KdRekening order by KdRekening
						";
			
		return $this->getArrayResult($sql);
	}
	
	function getReportPerbandinganRekap($tahun, $bulan)
	{	
		$thnlalu= $tahun-1;
		
		$sql ="SELECT Posisi as  Parent1, NamaPosisi AS NamaRekening1, Parent1 AS Parent2, NamaRekening1 AS NamaRekening2,Parent2 AS KdRekening, NamaRekening2 AS  NamaRekening,
				SUM(IF(SUBSTR(KdRekening,1,1)=1,NilaiBlnIni,NilaiBlnIni*-1)) AS NilaiBlnIni,
				SUM(IF(SUBSTR(KdRekening,1,1)=1,NilaiBlnLalu,NilaiBlnLalu*-1)) AS NilaiBlnLalu, 
				SUM(IF(SUBSTR(KdRekening,1,1)=1,NilaiThnLalu,NilaiThnLalu*-1)) AS NilaiThnLalu from (
				SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
						r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
						s.`KdRekening`, r.`NamaRekening`, p.Posisi, p.NamaPosisi,
						SUM(Awal$bulan+Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan) AS NilaiBlnIni, SUM(Awal$bulan) as NilaiBlnLalu, 0 as NilaiThnLalu 
						FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
						INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
						INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
						INNER JOIN posisi p on r.Posisi=p.Posisi
						WHERE tahun='$tahun' AND s.KdRekening between '10000000' and '39999999'
						GROUP BY s.`KdRekening` having(NilaiBlnIni<>0 or NilaiBlnLalu)
							UNION ALL
						SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
						r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
						s.`KdRekening`, r.`NamaRekening`, p.Posisi, p.NamaPosisi,
						0 as NilaiBlnIni,0 as  NilaiBlnLalu,
						SUM(Awal12+Debet12-Kredit12+AdjDebet12-AdjKredit12) as NilaiThnLalu 
						FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
						INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
						INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
						INNER JOIN posisi p on r.Posisi=p.Posisi
						WHERE tahun='$thnlalu' AND s.KdRekening between '10000000' and '39999999'
						GROUP BY s.`KdRekening` having(NilaiThnLalu<>0)) t
						group by Parent2 order by KdRekening
						";
			
		return $this->getArrayResult($sql);
	}
	
	function getReportTrend($tahun, $bulan)
	{
		$sql ="SELECT r2.parent AS Parent1, r3.`NamaRekening` AS NamaRekening1, 
						r.`Parent` AS Parent2, r2.`NamaRekening` AS NamaRekening2, 
						s.`KdRekening`, r.`NamaRekening`, 
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal01+Debet01-Kredit01+AdjDebet01-AdjKredit01,(Awal01+Debet01-Kredit01+AdjDebet01-AdjKredit01)*-1)) AS NilaiBlnIni1,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal02+Debet02-Kredit02+AdjDebet02-AdjKredit02, (Awal02+Debet02-Kredit02+AdjDebet02-AdjKredit02)*-1)) AS NilaiBlnIni2,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal03+Debet03-Kredit03+AdjDebet03-AdjKredit03, (Awal03+Debet03-Kredit03+AdjDebet03-AdjKredit03)*-1)) AS NilaiBlnIni3,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal04+Debet04-Kredit04+AdjDebet04-AdjKredit04, (Awal04+Debet04-Kredit04+AdjDebet04-AdjKredit04)*-1)) AS NilaiBlnIni4,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal05+Debet05-Kredit05+AdjDebet05-AdjKredit05, (Awal05+Debet05-Kredit05+AdjDebet05-AdjKredit05)*-1)) AS NilaiBlnIni5,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal06+Debet06-Kredit06+AdjDebet06-AdjKredit06, (Awal06+Debet06-Kredit06+AdjDebet06-AdjKredit06)*-1)) AS NilaiBlnIni6,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal07+Debet07-Kredit07+AdjDebet07-AdjKredit07, (Awal07+Debet07-Kredit07+AdjDebet07-AdjKredit07)*-1)) AS NilaiBlnIni7,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal08+Debet08-Kredit08+AdjDebet08-AdjKredit08, (Awal08+Debet08-Kredit08+AdjDebet08-AdjKredit08)*-1)) AS NilaiBlnIni8,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal09+Debet09-Kredit09+AdjDebet09-AdjKredit09, (Awal09+Debet09-Kredit09+AdjDebet09-AdjKredit09)*-1)) AS NilaiBlnIni9,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal10+Debet10-Kredit10+AdjDebet10-AdjKredit10, (Awal10+Debet10-Kredit10+AdjDebet10-AdjKredit10)*-1)) AS NilaiBlnIni10,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal11+Debet11-Kredit11+AdjDebet11-AdjKredit11, (Awal11+Debet11-Kredit11+AdjDebet11-AdjKredit11)*-1)) AS NilaiBlnIni11,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal12+Debet12-Kredit12+AdjDebet12-AdjKredit12, (Awal12+Debet12-Kredit12+AdjDebet12-AdjKredit12)*-1)) AS NilaiBlnIni12
						FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
						INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
						INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
						inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
						WHERE tahun='$tahun' AND s.KdRekening between '10000000' and '39999999'
						GROUP BY s.`KdRekening` having(NilaiBlnIni1<>0 or NilaiBlnIni2<>0 or NilaiBlnIni3<>0 or NilaiBlnIni4<>0 or NilaiBlnIni5<>0
						or NilaiBlnIni6<>0 or NilaiBlnIni7<>0  or NilaiBlnIni8<>0 or NilaiBlnIni9<>0 or NilaiBlnIni10<>0 or NilaiBlnIni11<>0 or NilaiBlnIni12<>0) order by s.KdRekening
						";
		return $this->getArrayResult($sql);
	}
	
	function getReportTrendRekap($tahun, $bulan)
	{
		$sql ="SELECT p.Posisi AS Parent1, p.`namaposisi` AS NamaRekening1,
						r2.`Parent` AS Parent2, r3.`NamaRekening` AS NamaRekening2, 
						r.Parent AS KdRekening, r2.`NamaRekening`, 
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal01+Debet01-Kredit01+AdjDebet01-AdjKredit01,(Awal01+Debet01-Kredit01+AdjDebet01-AdjKredit01)*-1)) AS NilaiBlnIni1,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal02+Debet02-Kredit02+AdjDebet02-AdjKredit02, (Awal02+Debet02-Kredit02+AdjDebet02-AdjKredit02)*-1)) AS NilaiBlnIni2,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal03+Debet03-Kredit03+AdjDebet03-AdjKredit03, (Awal03+Debet03-Kredit03+AdjDebet03-AdjKredit03)*-1)) AS NilaiBlnIni3,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal04+Debet04-Kredit04+AdjDebet04-AdjKredit04, (Awal04+Debet04-Kredit04+AdjDebet04-AdjKredit04)*-1)) AS NilaiBlnIni4,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal05+Debet05-Kredit05+AdjDebet05-AdjKredit05, (Awal05+Debet05-Kredit05+AdjDebet05-AdjKredit05)*-1)) AS NilaiBlnIni5,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal06+Debet06-Kredit06+AdjDebet06-AdjKredit06, (Awal06+Debet06-Kredit06+AdjDebet06-AdjKredit06)*-1)) AS NilaiBlnIni6,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal07+Debet07-Kredit07+AdjDebet07-AdjKredit07, (Awal07+Debet07-Kredit07+AdjDebet07-AdjKredit07)*-1)) AS NilaiBlnIni7,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal08+Debet08-Kredit08+AdjDebet08-AdjKredit08, (Awal08+Debet08-Kredit08+AdjDebet08-AdjKredit08)*-1)) AS NilaiBlnIni8,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal09+Debet09-Kredit09+AdjDebet09-AdjKredit09, (Awal09+Debet09-Kredit09+AdjDebet09-AdjKredit09)*-1)) AS NilaiBlnIni9,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal10+Debet10-Kredit10+AdjDebet10-AdjKredit10, (Awal10+Debet10-Kredit10+AdjDebet10-AdjKredit10)*-1)) AS NilaiBlnIni10,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal11+Debet11-Kredit11+AdjDebet11-AdjKredit11, (Awal11+Debet11-Kredit11+AdjDebet11-AdjKredit11)*-1)) AS NilaiBlnIni11,
						SUM(IF(SUBSTR(r.KdRekening,1,1)=1,Awal12+Debet12-Kredit12+AdjDebet12-AdjKredit12, (Awal12+Debet12-Kredit12+AdjDebet12-AdjKredit12)*-1)) AS NilaiBlnIni12
						FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
						INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
						INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
						inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
						INNER JOIN posisi p ON r.`Posisi`=p.`posisi`
						WHERE tahun='$tahun' AND s.KdRekening between '10000000' and '39999999'
						GROUP BY r.Parent having(NilaiBlnIni1<>0 or NilaiBlnIni2<>0 or NilaiBlnIni3<>0 or NilaiBlnIni4<>0 or NilaiBlnIni5<>0
						or NilaiBlnIni6<>0 or NilaiBlnIni7<>0  or NilaiBlnIni8<>0 or NilaiBlnIni9<>0 or NilaiBlnIni10<>0 or NilaiBlnIni11<>0 or NilaiBlnIni12<>0) order by s.KdRekening
						";
		return $this->getArrayResult($sql);
	}

	
	function getNetSales($tahun, $bulan, $divisi)
	{
		if($divisi==''){
			$where = '';
		}else{
			$where=" and d.KdDivisi='$divisi'";
		}
		
		$sql ="SELECT SUM(s.Awal$bulan+Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan)*-1 AS NetSalesYTD, 
				SUM(Debet$bulan-Kredit$bulan+AdjDebet$bulan-AdjKredit$bulan)*-1 AS NetSales
				FROM saldo_gl s INNER JOIN rekening r ON s.`KdRekening`=r.`KdRekening`
				INNER JOIN rekening r2 ON r.`Parent`=r2.KdRekening
				INNER JOIN rekening r3 ON r2.`Parent`=r3.KdRekening
				inner join subdivisi d on s.KdSubDivisi=d.KdSubdivisi
				WHERE tahun='$tahun' AND r2.Parent='40' $where ";
				
		$row = $this->getArrayResult($sql);
		return $row[0];
	}
    
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>