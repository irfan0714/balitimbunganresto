<?php

//purwanto on sept 16
class Rpt_mutasipaketmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }

    function getReport_header($tglawal, $tglakhir) {
        $sql = "SELECT 
                        id_mutasi, DATE_FORMAT(Tanggal,'%d-%m-%Y') AS Tanggal, MPCode, masterbarang.NamaLengkap AS NamaPaket, Qty, Jenis_mutasi, mutasi_paket.Keterangan, gudang.Keterangan AS namagudang,
			            NamaDivisiReport as NamaDivisi         
                      FROM
                        mutasi_paket 
                        INNER JOIN gudang 
                          ON mutasi_paket.KdGudang = gudang.KdGudang
                        INNER JOIN masterbarang
                          ON mutasi_paket.MPCode = masterbarang.PCode
                        INNER JOIN subdivisi s on masterbarang.KdSubDivisi=s.KdSubDivisi
                    	INNER JOIN divisireport dr on s.KdDivisiReport=dr.KdDivisiReport
                        WHERE DATE_FORMAT(mutasi_paket.Tanggal,'%Y-%m-%d') BETWEEN '".$tglawal."' AND '".$tglakhir."'";
        //echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_Detail($tglawal, $tglakhir) {
        $sql = "SELECT 
                    mp.id_mutasi, DATE_FORMAT(mp.Tanggal, '%d-%m-%Y') AS Tanggal, mp.MPCode,mp.Qty,mp.Jenis_mutasi,mp.Keterangan,mp.KdGudang,
                    nmheader.NamaLengkap AS NMheader,
                    barang_paket.DPcode,
                    nmdetail.NamaLengkap AS NMDetail,
                    (barang_paket.`Qty` * mp.Qty) AS detailQTY,
                    NamaDivisiReport as NamaDivisi, mp.Qty as headerQty
                    FROM
                      mutasi_paket  mp
                    INNER JOIN barang_paket 
                      ON barang_paket.MPCode = mp.MPCode 
                    INNER JOIN masterbarang nmdetail 
                      ON nmdetail.PCode = barang_paket.DPcode 
                    INNER JOIN masterbarang nmheader 
                      ON nmheader.PCode = mp.MPCode
                    INNER JOIN subdivisi s on nmheader.KdSubDivisi=s.KdSubDivisi
                    INNER JOIN divisireport dr on s.KdDivisiReport=dr.KdDivisiReport
                    WHERE DATE_FORMAT(Tanggal, '%Y-%m-%d') BETWEEN '".$tglawal."' 
                      AND '".$tglakhir."'
                    ORDER BY Tanggal DESC, id_mutasi DESC";
        //echo $sql;
        return $this->getArrayResult($sql);
    }


    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

}

?>