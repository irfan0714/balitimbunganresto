<?php
class Report_penggunaan_diskon_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function getMembers()
    {
    	$sql = "SELECT * FROM type_member ORDER BY NamaTypeMember ASC";
		
		return $this->getArrayResult($sql);
	}
	
	function getNamaMembers()
    {
    	$sql = "SELECT * FROM member ORDER BY NamaMember ASC";
		
		return $this->getArrayResult($sql);
	}
	
	function getEmployee()
    {
    	$sql = "SELECT * FROM employee a INNER JOIN voucher b ON a.`employee_nik` = b.`novoucher` ORDER BY a.`employee_name` ASC;";
		
		return $this->getArrayResult($sql);
	}
	
	function getCards()
    {
    	$sql = "SELECT * FROM group_disc_header ORDER BY NamaGroupDisc ASC;";
		
		return $this->getArrayResult($sql);
	}
	
	function getDate() 
    {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    
    function getReport($pilihan,$v_tipe_cari,$start_date,$end_date,$v_employee,$v_jenis_members,$v_nama_members,$v_cards) 
    {
    	
    	$where_date ="";
		if($start_date!="" && $end_date!="" && $start_date!="0000-00-00" && $end_date!="0000-00-00")
    	{
			$where_date ="AND a.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";		
        }
		
    	$where_employee ="";
    	if($v_employee)
    	{
            $where_employee = " AND a.KdCustomer = '".$v_employee."' ";
		}
		
    	$where_jenis_members ="";
    	if($v_jenis_members)
    	{
            $where_jenis_members = " AND b.KdTipeMember = '".$v_jenis_members."' "; 
		}
		
		$where_nama_members ="";
    	if($v_nama_members)
    	{
            $where_nama_members = " AND b.KdMember = '".$v_nama_members."' "; 
		}
	
		$where_cards ="";
    	if($v_cards)
    	{
            $where_cards = " AND KdGroupDisc = '".$v_cards."' "; 
		}
     
		$sql = "";
		if($pilihan=="karyawan"){
			
			$sql = "SELECT 
					  a.Tanggal,
					  a.NoStruk,
					  a.TotalNilai,
					  a.Ttl_Charge,
					  a.TAX,
					  a.Tunai,
					  a.KKredit,
					  a.KDebit,
					  a.TotalBayar,
					  a.Kembali,
					  ROUND(IF(a.`TotalBayar`>0 AND a.`Discount`>0 AND IFNULL(a.`KdCustomer`,'')!='' AND a.VoucherTravel=0,IF(k.PriceIncludeTax='T',a.`Discount`,a.`Discount`/1.1),0)) AS DiscKaryawan,
					  c.`employee_name`
					FROM
					  transaksi_header a 
					  LEFT JOIN voucher b 
					    ON a.`KdCustomer` = b.`novoucher` 
					  LEFT JOIN employee c 
					    ON a.`KdCustomer` = c.`employee_nik` 
					  LEFT JOIN kassa k ON a.NoKassa= k.id_kassa
					WHERE 1 
					  $where_date 
					  $where_employee
					  AND a.`KdCustomer` <> '' 
					  AND a.VoucherTravel=0
					  AND a.Status='1'
					UNION ALL
					SELECT 
					  a.Tanggal,
					  a.NoStruk,
					  a.TotalNilai,
					  a.Ttl_Charge,
					  a.TAX,
					  a.Tunai,
					  a.KKredit,
					  a.KDebit,
					  a.TotalBayar,
					  a.Kembali,
					  ROUND(IF(a.`TotalBayar`>0 AND a.`Discount`>0 AND IFNULL(a.`KdCustomer`,'')!='' AND a.VoucherTravel=0,IF(k.PriceIncludeTax='T',a.`Discount`,a.`Discount`/1.1),0)) AS DiscKaryawan,
					  c.`employee_name`
					FROM
					  transaksi_header_sunset a 
					  LEFT JOIN voucher b 
					    ON a.`KdCustomer` = b.`novoucher` 
					  LEFT JOIN employee c 
					    ON a.`KdCustomer` = c.`employee_nik` 
					  LEFT JOIN kassa k ON a.NoKassa= k.id_kassa
					WHERE 1
					  $where_date 
					  $where_employee
					  AND a.`KdCustomer` <> '' 
					  AND a.VoucherTravel=0
					  AND a.Status='1'
					;";
				
		}
		elseif($pilihan=="members"){
		    
			$sql="SELECT 
				  a.Tanggal,
				  a.NoStruk,
				  a.TotalNilai,
				  a.Ttl_Charge,
				  a.TAX,
				  a.Tunai,
				  a.KKredit,
				  a.KDebit,
				  a.TotalBayar,
				  a.Kembali,
				  b.NamaMember,
				  b.KdTipeMember,
				  ROUND(IF(a.`TotalBayar`>0 AND a.`Discount`>0 AND IFNULL(a.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',a.`Discount`,a.`Discount`/1.1),0)) AS DiscMember
				FROM
				  transaksi_header a 
				  INNER JOIN member b 
				    ON a.`KdMember` = b.`KdMember` 
				  INNER JOIN type_member c 
				    ON b.`KdTipeMember` = c.`KdTipeMember` 
				  INNER JOIN kassa k 
				    ON a.NoKassa = k.id_kassa 
				WHERE 1 
				$where_date
				$where_jenis_members
				$where_nama_members
				UNION ALL
				SELECT 
				  a.Tanggal,
				  a.NoStruk,
				  a.TotalNilai,
				  a.Ttl_Charge,
				  a.TAX,
				  a.Tunai,
				  a.KKredit,
				  a.KDebit,
				  a.TotalBayar,
				  a.Kembali,
				  b.NamaMember,
				  b.KdTipeMember,
				  ROUND(IF(a.`TotalBayar`>0 AND a.`Discount`>0 AND IFNULL(a.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',a.`Discount`,a.`Discount`/1.1),0)) AS DiscMember 
				FROM
				  transaksi_header_sunset a 
				  INNER JOIN member b 
				    ON a.`KdMember` = b.`KdMember` 
				  INNER JOIN type_member c 
				    ON b.`KdTipeMember` = c.`KdTipeMember` 
				  INNER JOIN kassa k 
				    ON a.NoKassa = k.id_kassa 
				WHERE 1 
				$where_date
				$where_jenis_members
				$where_nama_members";
		}
		elseif($pilihan=="cards"){
		
			$sql = "SELECT 
					  a.Tanggal,
					  a.NoStruk,
					  a.TotalNilai,
					  a.Ttl_Charge,
					  a.TAX,
					  a.Tunai,
					  a.KKredit,
					  a.KDebit,
					  a.TotalBayar,
					  a.Kembali,
					  b.NamaGroupDisc,
					  ROUND(IF(
					    a.`TotalBayar` > 0 
					    AND a.`Discount` > 0 
					    AND IFNULL(a.`NamaCard`, '') != '',
					    IF(
					      k.PriceIncludeTax = 'T',
					      a.`Discount`,
					      a.`Discount` / 1.1
					    ),
					    0
					  )) AS DiscCard 
					FROM
					  transaksi_header a 
					  INNER JOIN group_disc_header b 
					    ON a.`NamaCard` = b.`KdGroupDisc` 
					  INNER JOIN kassa k 
					    ON a.NoKassa = k.id_kassa 
					WHERE 1 
					  $where_date 
					UNION
					ALL 
					SELECT 
					  a.Tanggal,
					  a.NoStruk,
					  a.TotalNilai,
					  a.Ttl_Charge,
					  a.TAX,
					  a.Tunai,
					  a.KKredit,
					  a.KDebit,
					  a.TotalBayar,
					  a.Kembali,
					  b.NamaGroupDisc,
					  ROUND(IF(
					    a.`TotalBayar` > 0 
					    AND a.`Discount` > 0 
					    AND IFNULL(a.`NamaCard`, '') != '',
					    IF(
					      k.PriceIncludeTax = 'T',
					      a.`Discount`,
					      a.`Discount` / 1.1
					    ),
					    0
					  )) AS DiscCard 
					FROM
					  transaksi_header_sunset a 
					  INNER JOIN group_disc_header b 
					    ON a.`NamaCard` = b.`KdGroupDisc` 
					  INNER JOIN kassa k 
					    ON a.NoKassa = k.id_kassa 
					WHERE 1 
					  $where_date;";
		}
		
	    //echo $sql;
        $result = $this->getArrayResult($sql);
        return $result;
    }
    
    
    function getArrayResult($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) 
    {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) 
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    
    

}

?>