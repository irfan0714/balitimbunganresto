<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_cn_dn_piutang_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {

		$sql ="SELECT cntype, h.`cnno`, h.`cndate`, CONCAT(COALESCE(s.Nama,''),COALESCE(t.Nama,'')) AS Nama, h.`note` , r.`KdRekening`, r.`NamaRekening`, sd.`NamaSubDivisi` , d.`description`, SUM(d.`value`) AS value
				FROM creditnote h INNER JOIN creditnotedtl d ON h.`cnno`=d.`cnno`
				INNER JOIN rekening r ON d.`coano`= r.`KdRekening`
				LEFT JOIN customer s ON h.`KdCustomer` = s.`KdCustomer` 
				LEFT JOIN tourtravel t on h.KdCustomer=t.KdTravel
				INNER JOIN subdivisi sd ON d.`KdSubDivisi`=sd.`KdSubDivisi`
				WHERE h.`cndate` BETWEEN '$tgldari' AND '$tglsampai' AND h.`status`=1
				GROUP BY h.`cnno`
				ORDER BY h.`cntype`, h.`cnno`
			  ";
		//echo $sql;die;		
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}    
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */