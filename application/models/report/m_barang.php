<?php

class m_barang extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getDivisi() {
        $sql = "SELECT * FROM divisi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }
	
	function getAllTicket(){
		$sql = "select * from masterbarang where JenisBarang='TCK' ";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function getTransTicket($getLastIP){
		$sql = "select * from ticket where status='0' and last_ip='$getLastIP' order by noticket ASC";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function editStatusTicket($parameter,$data,$where){
		$this->db->where($where);
		$this->db->update($parameter, $data);
	}
	
	function totalTicketPerhari($date){
		$sql = "SELECT COUNT(noticket) AS total FROM ticket WHERE add_date='$date' AND noidentitas !='1234'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getReport($kd) 
    {
		if(empty($kd)){
			$dev = "";
		}else{
			$dev = "AND mb.KdDivisi='".$kd."' ";
		}
    	$sql = "
			SELECT 
			  mb.*,NamaDivisi 
			FROM
			  (SELECT 
				PCode,
				NamaLengkap,
				IF(
				  (KdDivisi = 3 
				  OR KdDivisi = 2)AND LENGTH(KdKategori)!=3  ,
				  ROUND(Harga1c / 1.1, 2),
				  harga1c
				) AS hJ,
				Harga1c,
				Service_charge,
				PPN,
				DiscInternal,
				DiscLokal,
				Komisi as KomisiLokal,
				KdDivisi,
				KdSubDivisi,
				KdKategori,
				Kdsubkategori 
			  FROM
				masterbarang) mb 
				INNER JOIN divisi ON divisi.`KdDivisi`=mb.KdDivisi
			WHERE 1 ".$dev." 
			ORDER BY PCode ASC
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }

	function getTicketHead($start_date,$end_date)
	{
		$sql="
			SELECT 
			  DATE(ticket_head.tanggal) AS tglTiket,
			  SUM(ticket_head.nilai_tunai) AS n_tunai,
			  SUM(ticket_head.nilai_debit) AS n_debit,
			  SUM(ticket_head.nilai_kredit) AS n_kredit,
			  SUM(ticket_head.nilai_voucher) AS n_voucher 
			FROM
			  ticket_head 
			WHERE 1 
			  AND DATE(ticket_head.tanggal) BETWEEN '".$start_date."' 
			  AND '".$end_date."' 
			  AND ticket_head.noidentitas != '1234' 
			GROUP BY tglTiket
		";
        return $this->getArrayResult($sql);
		
	}
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>