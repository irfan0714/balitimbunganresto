<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_insentif_sls_spv_mgr extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {
	 	

		$sql ="
				SELECT 
				  (SUM(ttl_amount) - 0 - 0) AS pencapaian,
				  rt.KdSalesman,
				  rt.KdSupervisor 
				FROM
				  ticket_head 
				  LEFT JOIN 
				    (SELECT 
				      NoStiker,
				      Nama,
				      KdSalesman,
				      NamaSalesman,
				      KdSupervisor 
				    FROM
				      (SELECT 
				        r.NoStiker,
				        t.`Nama`,
				        n.`KdSalesman`,
				        n.`NamaSalesman`,
				        n.`KdSupervisor` 
				      FROM
				        register r 
				        INNER JOIN tourtravel t 
				          ON r.kdtravel = t.`KdTravel` 
				        INNER JOIN `trans_reservasi` m 
				          ON r.`KdTravel` = m.KdTravel 
				        INNER JOIN salesman n 
				          ON m.`Sales_In_Charge` = n.KdSalesman 
				      WHERE r.`Tanggal` BETWEEN '".$tgldari."' 
				        AND '".$tglsampai."' 
				      UNION
				      ALL 
				      SELECT 
				        NoStiker,
				        t.Nama,
				        n.`KdSalesman`,
				        n.`NamaSalesman`,
				        n.`KdSupervisor` 
				      FROM
				        trans_reservasi_konfirmasi r 
				        INNER JOIN tourtravel t 
				          ON r.`KdTravel` = t.`KdTravel` 
				        INNER JOIN `trans_reservasi` m 
				          ON r.`KdTravel` = m.KdTravel 
				        INNER JOIN salesman n 
				          ON m.`Sales_In_Charge` = n.KdSalesman 
				      WHERE r.TglKonfirmasi BETWEEN '".$tgldari."' 
				        AND '".$tglsampai."') tx 
				    GROUP BY NoStiker) rt 
				    ON `noidentitas` = rt.NoStiker 
				WHERE DATE(tanggal) BETWEEN '".$tgldari."' 
				        AND '".$tglsampai."'
				GROUP BY NoTrans 
				UNION
				ALL 
				SELECT 
				  SUM(
				    transaksi_detail.`Qty` * IF(
				      transaksi_detail.Service_charge > 0,
				      transaksi_detail.`Harga`,
				      transaksi_detail.`Harga` / 1.1
				    )
				  ) - SUM(
				    IF(
				      transaksi_detail.Service_charge > 0,
				      transaksi_detail.`Disc1`,
				      transaksi_detail.`Disc1` / 1.1
				    )
				  ) - SUM(
				    transaksi_header.`Voucher` * (
				      IF(
				        transaksi_detail.`Service_charge` > 0,
				        transaksi_detail.`Netto` * 1155 / 1000,
				        transaksi_detail.Netto
				      ) / transaksi_header.`TotalNilai`
				    )
				  ) AS pencapaian,
				  rt.KdSalesman,
				  rt.KdSupervisor
				FROM
				  transaksi_header 
				  INNER JOIN transaksi_detail 
				    ON transaksi_header.`NoKassa` = transaksi_detail.`NoKassa` 
				    AND transaksi_header.`NoStruk` = transaksi_detail.`NoStruk` 
				  INNER JOIN masterbarang b 
				    ON transaksi_detail.`PCode` = b.`PCode` 
				  INNER JOIN subdivisi s 
				    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  INNER JOIN divisi 
				    ON s.`KdDivisi` = divisi.`KdDivisi` 
				  LEFT JOIN 
				    (SELECT 
				      NoStiker,
				      Nama,
				      KdSalesman,
				      NamaSalesman,
				      KdSupervisor 
				    FROM
				      (SELECT 
				        r.NoStiker,
				        t.`Nama`,
				        n.`KdSalesman`,
				        n.`NamaSalesman`,
				        n.`KdSupervisor` 
				      FROM
				        register r 
				        INNER JOIN tourtravel t 
				          ON r.kdtravel = t.`KdTravel` 
				        INNER JOIN `trans_reservasi` m 
				          ON r.`KdTravel` = m.KdTravel 
				        INNER JOIN salesman n 
				          ON m.`Sales_In_Charge` = n.KdSalesman 
				      WHERE r.`Tanggal` BETWEEN '".$tgldari."' 
				        AND '".$tglsampai."' 
				      UNION
				      ALL 
				      SELECT 
				        NoStiker,
				        t.Nama,
				        n.`KdSalesman`,
				        n.`NamaSalesman`,
				        n.`KdSupervisor` 
				      FROM
				        trans_reservasi_konfirmasi r 
				        INNER JOIN tourtravel t 
				          ON r.`KdTravel` = t.`KdTravel` 
				        INNER JOIN `trans_reservasi` m 
				          ON r.`KdTravel` = m.KdTravel 
				        INNER JOIN salesman n 
				          ON m.`Sales_In_Charge` = n.KdSalesman 
				      WHERE r.TglKonfirmasi BETWEEN '".$tgldari."' 
				        AND '".$tglsampai."') tx 
				    GROUP BY NoStiker) rt 
				    ON transaksi_header.KdAgent = rt.NoStiker 
				WHERE transaksi_header.status = '1' 
				  AND transaksi_header.Tanggal BETWEEN '".$tgldari."' 
				        AND '".$tglsampai."'
				GROUP BY divisi.`KdDivisi`,
				  r.KdDivisiReport,
				  transaksi_header.`Tanggal`,
				  transaksi_header.`NoStruk`  
			  ";
		//echo $sql;	
		$query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
        /*$arr = array();
       
        for ($i=0; $i<count($result); $i++){
        	$KdSalesman = $result[$i]['KdSalesman'];
			$pencapaian = $result[$i]['pencapaian'];
			$data = array($pencapaian);
			$arr[$KdSalesman] = implode('*', $data);
			
		}
					
		return $arr;*/
    }
	
	function viewDataKunj($tgldari, $tglsampai) {
	 	

		$sql ="
				SELECT 
				  SUM(`kunjungan_header`.`status`) AS tot_kunj,
				  `kunjungan_header`.`KdSalesman`,
				  salesman.`KdSupervisor`
				FROM
				  `kunjungan_header` 
				  INNER JOIN salesman 
					ON `kunjungan_header`.`KdSalesman` = salesman.`KdSalesman` 
				WHERE `kunjungan_header`.`tgl_kunjungan` BETWEEN '".$tgldari."' 
				  AND '".$tglsampai."' AND `kunjungan_header`.`status`='1'
				  GROUP BY `kunjungan_header`.`KdSalesman`  
			  ";
		//echo $sql;	
		$query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
     
    function getListEmp($bulan, $tahun) {
	 	
		
		$sql ="
				SELECT 
				  d. KdSalesman,
				  d.`NamaSalesman` 
				FROM
				  `transaksi_header` a 
				  INNER JOIN `trans_reservasi_konfirmasi` b 
				    ON a.`KdAgent` = b.`NoStiker` 
				  INNER JOIN `trans_reservasi` c 
				    ON c.`NoDokumen` = b.`NoReservasi` 
				  INNER JOIN salesman d 
				    ON d.`KdSalesman` = c.`Sales_In_Charge` 
				WHERE MONTH(a.`Tanggal`)='".$bulan."' AND YEAR(a.`Tanggal`)='".$tahun."' 
				GROUP BY d.`KdSalesman` ;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
    
    function getListEmpSpv($bulan, $tahun) {
	 	
		
		$sql ="
				SELECT 
				  e.`KdSupervisor`,
				  e.`NamaSupervisor`
				FROM
				  `transaksi_header` a 
				  INNER JOIN `trans_reservasi_konfirmasi` b 
				    ON a.`KdAgent` = b.`NoStiker` 
				  INNER JOIN `trans_reservasi` c 
				    ON c.`NoDokumen` = b.`NoReservasi` 
				  INNER JOIN salesman d 
				    ON d.`KdSalesman` = c.`Sales_In_Charge`
				  INNER JOIN supervisor e
				    ON e.`KdSupervisor` = d.`KdSupervisor`
				WHERE MONTH(a.`Tanggal`) = '".$bulan."' 
				  AND YEAR(a.`Tanggal`) = '".$tahun."'  
				GROUP BY e.`KdSupervisor` ;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
	
	function jmlhari($tgl) {
	 	
		
		$sql ="
				SELECT LAST_DAY('".$tgl."') AS tgl;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
   
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */