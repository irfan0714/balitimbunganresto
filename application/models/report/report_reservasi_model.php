<?php
class report_reservasi_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getReport($pilihan,$start_date,$end_date,$v_nobeo,$v_nosticker,$username)
    {
    	$user = $this->session->userdata('username');
    	$where_date ="";
    	if($start_date!="" && $end_date!="" && $start_date!="0000-00-00" && $end_date!="0000-00-00")
    	{
			$where_date ="AND rk.`TglKonfirmasi` BETWEEN '".$start_date."' AND '".$end_date."' ";
			$where_date_header ="AND h.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
			$where_date_detail ="AND d.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
		}

		$where_v_nobeo ="";
    	if($v_nobeo)
    	{
      		$where_v_nobeo = " AND rk.`NoReservasi` = '".$v_nobeo."' ";
		}

		$where_v_nosticker ="";
    	if($v_nosticker)
    	{
      		$where_v_nosticker = " AND ks.`NoSticker` = '".$v_nosticker."' ";
		}

		$sql = "";
  		if($pilihan=="transaksi"){

					$sql ="
					SELECT
					rk.`NoDokumen`,
					rk.`NoReservasi`,
					ks.`NoSticker`,
					rk.`TglKonfirmasi`,
					tt.`Nama` AS Company,
					r.`GroupName`,
					r.`Nationality`,
					r.`Jam_Kedatangan`,
					(SELECT
						a.nama_aktivitas_beo
					FROM
						aktivitas_beo a
					WHERE a.id_aktivitas_beo = r.`Event1`) AS Activity1,
					(SELECT
						a.nama_aktivitas_beo
					FROM
						aktivitas_beo a
					WHERE a.id_aktivitas_beo = r.`Event2`) AS Activity2,
					(SELECT
						a.nama_aktivitas_beo
					FROM
						aktivitas_beo a
					WHERE a.id_aktivitas_beo = r.`Event3`) AS Activity3,
					(SELECT
						a.nama_aktivitas_beo
					FROM
						aktivitas_beo a
					WHERE a.id_aktivitas_beo = r.`Event4`) AS Activity4,
					(SELECT
						a.nama_aktivitas_beo
					FROM
						aktivitas_beo a
					WHERE a.id_aktivitas_beo = r.`Event5`) AS Activity5,
					r.`Contact`,
					(SELECT
						SUM(
						d.`Qty` * IF(
							k.PriceIncludeTax = 'T',
							d.`Harga`,
							d.`Harga` / 1.1
						)
						)
					FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Gross,
					(SELECT
						SUM(
						IF(
							k.PriceIncludeTax = 'T',
							d.`Disc1`,
							d.`Disc1` / 1.1
						)
						)
					FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Discount,
					(SELECT
						SUM(
						IF(
							k.PriceIncludeTax = 'T',
							d.`Netto`,
							d.`Netto` / 1.1
						)
						)
					FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Nett,
					(SELECT SUM(d.`Netto` * d.Service_charge / 100) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS ServiceCharge,
					(SELECT SUM(
						(
						IF(
							k.PriceIncludeTax = 'T',
							d.`Netto`,
							d.`Netto` / 1.1
						) + (
							IF(
							k.PriceIncludeTax = 'T',
							d.`Netto`,
							d.`Netto` / 1.1
							) * d.Service_charge / 100
						)
						) * 0.1
					)FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Tax,
					(SELECT SUM(
						IF(
						k.PriceIncludeTax = 'T',
						d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
						d.Netto
						)
					)FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Sales,
					(SELECT SUM(
						th.`Tunai` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Tunai,
					(SELECT SUM(
						th.`KKredit` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS KKredit,
					(SELECT SUM(
						th.`KDebit` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS KDebit,
					(SELECT SUM(
						th.`GoPay` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS GoPay,
					(SELECT SUM(
						th.`Point` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS POINT,
					(SELECT SUM(
						th.`Voucher` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Voucher,
					(SELECT SUM(
						th.`Kembali` * (
						IF(
							k.PriceIncludeTax = 'T',
							d.Netto * (110 + (1.1 * d.`Service_charge`)) / 100,
							d.Netto
						) / th.`TotalNilai`
						)
					) FROM
						transaksi_header th
						INNER JOIN transaksi_detail d
						ON th.`NoKassa` = d.`NoKassa`
						AND th.`NoStruk` = d.`NoStruk`
						INNER JOIN kassa k
						ON th.`NoKassa` = k.id_kassa
						INNER JOIN subdivisi s
						ON k.SubDivisi = s.`KdSubDivisi`
						INNER JOIN divisireport r
						ON s.`KdDivisiReport` = r.`KdDivisiReport`
						INNER JOIN divisi
						ON s.`KdDivisi` = divisi.`KdDivisi`
					WHERE th.KdAgent = ks.`NoSticker`
						AND th.`Tanggal` = rk.`TglKonfirmasi`) AS Kembali
					FROM
					trans_reservasi r
					INNER JOIN `trans_reservasi_konfirmasi` rk
						ON r.`NoDokumen` = rk.`NoReservasi`
					INNER JOIN `trans_reservasi_konfirmasi_sticker` ks
						ON rk.`NoDokumen` = ks.`NoDokumen`
					INNER JOIN `tourtravel` tt
						ON r.`KdTravel` = tt.`KdTravel`
					WHERE r.`status` = '1'
					AND (
						r.`Event1` IN ('5', '12')
						OR r.`Event2` IN ('5', '12')
						OR r.`Event3` IN ('5', '12')
						OR r.`Event4` IN ('5', '12')
						OR r.`Event5` IN ('5', '12')
					)
				  ".$where_date."
				  ".$where_v_nobeo."
				  ".$where_v_nosticker.
				  " ORDER BY rk.`TglKonfirmasi` ASC
				  ";
				

		}
		elseif($pilihan=="detail"){

			$sql ="
				SELECT
					rk.`NoReservasi`,
					ks.`NoSticker`,
					rk.`TglKonfirmasi`,
					`s`.NoKassa,
					`s`.Kasir,
					`s`.KdDivisi,
					`s`.NamaDivisi,
					`s`.Tanggal,
					`s`.KdAgent,
					`s`.Waktu,
					`s`.NoStruk,
					`s`.TotalNilai,
					`s`.TotalBayar,
					`s`.Voucher,
					`s`.Discount,
					`s`.TotalItem,
					s.PCode,
					s.NamaLengkap,
					s.Qty,
					`s`.Harga,
					`s`.Bruto,
					s.DiscDetail,
					s.Netto,
					s.Service_charge,
					s.PPN
					FROM
					trans_reservasi r
					INNER JOIN `trans_reservasi_konfirmasi` rk
						ON r.`NoDokumen` = rk.`NoReservasi`
					INNER JOIN `trans_reservasi_konfirmasi_sticker` ks
						ON rk.`NoDokumen` = ks.`NoDokumen`
					INNER JOIN `tourtravel` tt
						ON r.`KdTravel` = tt.`KdTravel`
					INNER JOIN (SELECT
					`h`.NoKassa,
					`h`.Kasir,
					`v`.KdDivisi,
					`v`.NamaDivisi,
					`h`.Tanggal,
					`h`.KdAgent,
					`h`.Waktu,
					`h`.NoStruk,
					`h`.TotalNilai,
					`h`.TotalBayar,
					`h`.Voucher,
					IF(
						k.PriceIncludeTax = 'T',
						d.`Disc1`,
						d.`Disc1` / 1.1
					) AS Discount,
					`h`.TotalItem,
					d.PCode,
					b.NamaLengkap,
					d.Qty,
					IF(
						k.PriceIncludeTax = 'T',
						d.`Harga`,
						d.`Harga` / 1.1
					) AS Harga,
					d.`Qty` * IF(
						k.PriceIncludeTax = 'T',
						d.`Harga`,
						d.`Harga` / 1.1
					) AS Bruto,
					(
						IFNULL(d.Disc1, 0) + IFNULL(d.Disc2, 0) + IFNULL(d.Disc3, 0) + IFNULL(d.Disc4, 0)
					) AS DiscDetail,
					IF(
						k.PriceIncludeTax = 'T',
						d.`Netto`,
						d.`Netto` / 1.1
					) AS Netto,
					d.Service_charge,
					b.PPN
					FROM
					transaksi_detail d
					INNER JOIN transaksi_header h
						ON h.NoStruk = `d`.NoStruk
						AND `h`.status = '1' ".$where_date_header."
					INNER JOIN kassa k
						ON h.NoKassa = k.id_kassa
					INNER JOIN divisi v
						ON k.KdDivisi = v.KdDivisi
					INNER JOIN masterbarang b
						ON `d`.PCode = b.PCode
					WHERE 1 ".$where_date_detail."
					AND `h`.status = '1') s 
					ON s.KdAgent = ks.`NoSticker` AND s.`Tanggal` = rk.`TglKonfirmasi`
					WHERE r.`status` = '1'
					AND (
						r.`Event1` IN ('5', '12')
						OR r.`Event2` IN ('5', '12')
						OR r.`Event3` IN ('5', '12')
						OR r.`Event4` IN ('5', '12')
						OR r.`Event5` IN ('5', '12')
					)
				".$where_date."
				  ".$where_v_nobeo."
				  ".$where_v_nosticker.
				  " ORDER BY rk.`NoReservasi` ASC
				  ";
		}
		elseif($pilihan=="barang"){

			$sql ="
					
				  ";
		}
		if($user=="tonny1205"){
			//echo $sql;
		}
    if($user=="mechael0101"){
      //echo $sql;
    }
		//echo $sql; die();
        $result = $this->getArrayResult($sql);
        return $result;
    }

	function getVoucher($start_date,$end_date,$arr_reservasi)
	{
		$where_date ="";
    	if($start_date!="" && $start_date!="0000-00-00" && $end_date!="" && $end_date!="0000-00-00")
    	{
			$where_date ="AND r.TglDokumen BETWEEN '".$start_date."' AND '".$end_date."' ";
		}

		$where_reservasi="";
		if(count($arr_reservasi)>0)
		{
			$where_reservasi = $arr_reservasi;
		}

		$sql="
			SELECT
			  v.*,r.TglDokumen
			FROM			
			trans_reservasi_detail v
			INNER JOIN 
			trans_reservasi r
			ON v.`NoDokumen` = r.NoDokumen
			WHERE
			  1
			  ".$where_date."
			  ".$where_reservasi."
			ORDER BY v.NoDokumen DESC
		";
		//echo $sql;
		return $this->getArrayResult($sql);

	}

    function getType($id="")
    {
    	$where_divisi="";
    	if(count($id)>0)
    	{
			$where_divisi = "AND KdDivisiReport IN (".$id.")";
		}

		$sql = "SELECT KdDivisiReport as KdDivisi, NamaDivisiReport as NamaDivisi FROM divisireport ORDER BY NamaDivisiReport ASC";

		return $this->getArrayResult($sql);
	}

	function getDivisi($id="")
    {
    	$where_divisi="";
    	if(count($id)>0)
    	{
			$where_divisi = "AND KdDivisi IN (".$id.")";
		}

		$sql = "SELECT KdDivisi, NamaDivisi FROM divisi ORDER BY NamaDivisi ASC";

		return $this->getArrayResult($sql);
	}

    function getKasir($id="")
    {
    	$where_kasir="";
    	if(count($id)>0)
    	{
			$where_kasir = "AND user.UserLevel IN (".$id.")";
		}

		$sql = "
            SELECT
              user.UserLevel,
              userlevels.UserLevelName,
              user.employee_nik,
              user.UserName,
              employee.employee_name
            FROM
              user
              LEFT JOIN userlevels
                ON user.UserLevel = userlevels.UserLevelID
              LEFT JOIN employee
                ON user.employee_nik = employee.employee_nik
            WHERE 1
               ".$where_kasir."
			AND user.Active='Y'
            GROUP BY user.employee_nik
            ORDER BY user.UserName ASC
		";
		return $this->getArrayResult($sql);
	}

    function getKasirFromHeader()
    {
    	$date = date('Y-m-d');
		$sql = "Select Kasir, employee_name from (
            SELECT
              h.Kasir,
              e.employee_name
            FROM
              transaksi_header h
              LEFT JOIN employee e
                ON h.Kasir = e.username
            where h.tanggal between DATE_ADD('$date', INTERVAL -31 DAY) and '$date'
            GROUP BY Kasir
            union all
            SELECT
              h.Kasir,
              e.employee_name
            FROM
              transaksi_header_sunset h
              LEFT JOIN employee e
                ON h.Kasir = e.username
            where h.tanggal between DATE_ADD('$date', INTERVAL -31 DAY) and '$date'
            GROUP BY Kasir
            ) t ORDER BY Kasir ASC
        ";

		return $this->getArrayResult($sql);
	}

    function getArrayResult($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql)
    {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate()
    {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

}

?>
