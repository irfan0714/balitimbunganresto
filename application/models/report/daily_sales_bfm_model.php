<?php
class daily_sales_bfm_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getkassa()
	{
		$sql = "SELECT kassa.id_kassa, divisi.NamaDivisi, kassa.SubDivisi 
        			FROM kassa 
          			INNER JOIN divisi 
            		ON kassa.KdDivisi = divisi.KdDivisi 
        			WHERE 1 
        			ORDER BY kassa.id_kassa ASC" ;
		return $this->getArrayResult($sql);
	}
	
	function getGuest($v_date_from,$v_date_to,$jenis ){
        $sql = "SELECT 
				  a.`Tanggal`,
				  SUM(a.TotalGuest) AS Guest,
				  SUM(a.`Discount`) AS Discount,
				  SUM(a.`Ttl_Charge`) AS Service_Charge,
				  SUM(a.`TAX`) AS TAX 
				FROM
				  transaksi_header a 
				WHERE a.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND a.Status='1' GROUP BY a.`Tanggal`;";
        
        return $this->getArrayResult($sql);
	}
	
	
	function getStruck($v_date_from,$v_date_to,$jenis ){
		$sql = "SELECT a.`Tanggal`,COUNT(a.`NoStruk`) AS Struk  FROM transaksi_header a WHERE a.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND a.Status='1' GROUP BY a.`Tanggal`;";
        
        return $this->getArrayResult($sql);
	}
	
	function getBruto($v_date_from,$v_date_to,$jenis ){
		$sql = "SELECT 
				  a.`Tanggal`,
				  SUM(a.Netto) AS Bruto
				FROM
				  transaksi_detail a 
				WHERE a.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND a.Status='1' GROUP BY a.`Tanggal`;";
        
        return $this->getArrayResult($sql);
	}
	
	
	
	function getsales($v_date_from,$v_date_to, $jenis ){
		if($jenis=='N'){
			$sql = "SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(d.`Service_charge`>0, d.`Netto`*1155/1000, d.Netto)) AS Sales
								FROM transaksi_header h INNER JOIN transaksi_detail d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
								INNER JOIN subdivisi s ON b.`KdSubDivisi`=s.`KdSubDivisi`
								INNER JOIN divisireport r ON s.`KdDivisiReport`=r.`KdDivisiReport`
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1 And length(h.KdAgent) < 4
								GROUP BY h.`Tanggal`, r.KdDivisiReport";
		}else{
			$sql = "SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
								FROM transaksi_header h INNER JOIN transaksi_detail d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
								INNER JOIN subdivisi s ON b.`KdSubDivisi`=s.`KdSubDivisi`
								INNER JOIN kassa k on h.NoKassa = k.id_kassa
								INNER JOIN divisireport r ON s.`KdDivisiReport`=r.`KdDivisiReport`
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1 
								GROUP BY h.`Tanggal`, r.KdDivisiReport
				UNION ALL
				SELECT h.`Tanggal`, r.`KdDivisiReport`, r.`NamaDivisiReport`, SUM(IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)) AS Sales
								FROM transaksi_header_sunset h INNER JOIN transaksi_detail_sunset d ON h.`NoKassa`=d.`NoKassa` AND h.`NoStruk`=d.`NoStruk`
								INNER JOIN kassa k on h.NoKassa = k.id_kassa
				  				INNER JOIN subdivisi s ON k.`SubDivisi` = s.`KdSubDivisi` 
				  				INNER JOIN divisireport r ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  				INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi` 
								WHERE h.`Tanggal` BETWEEN '$v_date_from' AND '$v_date_to' AND h.`Status`=1
								GROUP BY h.`Tanggal`, r.KdDivisiReport";
			
		}
		
		return $this->getArrayResult($sql);
	}
	
		
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'report_daily_bfm'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function voucher($v_date){
		$mylib = new globallib();
		$sql = "SELECT novoucher, nominal, terpakai FROM voucher WHERE expdate='".$mylib->ubah_tanggal($v_date)."' and Jenis<>5 order by novoucher";
        return $this->getArrayResult($sql);
	}
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>