<?php
class Report_food_costmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
    
    
    function getReport($jenis) 
    {
    	$sql = "SELECT h.`resep_id`, h.`PCode`, b.`NamaLengkap`, SUM(d.`Qty`*d.`Harga`) AS HPP, AVG(b.`Harga1c`) AS HargaJual, SUM(d.`Qty`*d.`Harga`/b.harga1c*100) AS FoodCost, LastCalculateDate
				FROM resep_header h INNER JOIN resep_detail d ON h.`resep_id`=d.`resep_id`
				INNER JOIN masterbarang b ON h.`PCode`=b.`PCode`
				WHERE h.`Jenis`='$jenis'
				GROUP BY h.`resep_id`
				ORDER BY h.`resep_id`";
    	
        $result = $this->getArrayResult($sql);
        return $result;
    }
    
    function getheader($id){
		$sql = "SELECT h.`resep_id`, h.`PCode`, b.`NamaLengkap`, h.`Status`, h.`Jenis`, h.`Satuan`, h.`Qty` , h.AddUser, h.AddDate, h.EditUser, h.EditDate, b.Harga1c, h.LastCalculateDate
				FROM resep_header h INNER JOIN masterbarang b ON h.`PCode`=b.`PCode`
				WHERE resep_id='$id'";
					
		return $this->getArrayResult($sql);
	}	
	
	function getdetail($id){
		$sql  = "SELECT r.`resep_id`, r.`PCode`, b.`NamaLengkap`, r.`Qty`, r.Satuan, r.Harga
					FROM resep_detail r INNER JOIN masterbarang b ON r.`PCode`=b.`PCode`
					WHERE resep_id='$id'";
					
		return $this->getArrayResult($sql);
	}
	
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>