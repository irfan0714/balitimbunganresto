<?php
class Report_beo_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getArrayGudang($arrdata)
    {
    	$where = "";
    	if(count($arrdata)*1>0)
    	{
			$where= $arrdata;	
		}
		
    	$sql = "
    		SELECT 
			  gudang.KdGudang,
			  gudang.Keterangan 
			FROM
			  gudang 
			WHERE 
			  1
			  ".$where." 
			ORDER BY 
			  gudang.Keterangan ASC
		";
		
		return $this->getArrayResult($sql);
	}
		
	function getArrayCurrency()
    {
    	$sql = "
    		SELECT 
			  mata_uang.Kd_Uang, 
			  mata_uang.Keterangan 
			FROM
			  mata_uang 
			WHERE 1 
			ORDER BY mata_uang.id ASC
		";
		
		return $this->getArrayResult($sql);
	}

    function getArrayHeader($start_date,$end_date,$travel,$status,$sort,$place)
    {
    	$where = "";
    	$where_place= "";
	    $kdtravel= "";
	    $sortby= "";
        if($status=="0")
        {
			$where .= "";
		}else if($status=="1"){
			$where .= "AND SUBSTR(a.NoDokumen, 1,7)<>'BOOKING'";
		}else{
			$where .= "AND SUBSTR(a.NoDokumen, 1,7)='BOOKING'";
		}
		
		if($travel==""){
			$kdtravel= "";
		}else{
			$kdtravel .= " AND a.KdTravel='".$travel."'";
		}

		if($place==""){
			$where_place= "";
		}elseif($place=="SGV"){
			$where_place .= " AND a.place_bts='0'";
		}elseif($place=="BTS"){
			$where_place .= " AND a.place_bts='1'";
		}elseif($place=="SANITASI"){
			$where_place .= " AND a.place_sanitasi='1'";
		}
    	
    	if($sort==""){
			$sortby= "";
		}else if($sort=="1"){
			$sortby .= " ORDER BY a.NoDokumen ASC";
		}else if($sort=="2"){
			$sortby .= " ORDER BY a.TglDokumen ASC";
		}
    	
		$sql="
			SELECT 
			  a.`NoDokumen`,
			  DATE_FORMAT(DATE(a.reservasiDate), '%d-%m-%Y') AS Tgl_Kedatangan,
			  a.`Jam_Kedatangan`,
			  b.`Nama`,
			  a.`GroupName`,
			  a.`GroupCode`,
			  a.`Nationality`,
			  a.`adultParticipants`,
			  a.`childParticipants`,
			  a.`kidsParticipants`,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants` AS Participants,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants`  AS Tour,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants`  AS Lounch,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants`  AS Dinner,
			  a.`Event1`,
			  a.`Event2`,
			  a.`Event3`,
			  a.`Event4`,
			  a.`Event5`,
			  a.`Contact`,
			  c.`NamaSalesman`,
			  a.`desc_f_b_kitchen` AS Ket1,
			  a.`desc_black_aye` AS Ket2,
			  a.`desc_oemah_herborist` AS Ket3,
			  a.`desc_front_office` AS Ket4,
			  a.`status_konfirmasi`,
			  CASE
					WHEN a.`place_sanitasi` = 1 THEN 'SANITASI'
					WHEN a.`place_bts` = 1 THEN 'BTS'
					ELSE 'SGV'
				END AS place 
			FROM
			  `trans_reservasi` a 
			  INNER JOIN `tourtravel` b 
			    ON a.`KdTravel` = b.`KdTravel` 
			  INNER JOIN salesman c 
			    ON a.`Sales_In_Charge` = c.`KdSalesman` 
			WHERE 1 
			  AND a.`TglDokumen` BETWEEN '".$start_date."' 
			  AND '".$end_date."' 
			  ".$where."
			  ".$kdtravel."
			  ".$where_place."
			  ".$sortby." ;
		";
		 // echo $sql;
		return $this->getArrayResult($sql);	
	}
	
	function getmaildata()
    {
    	$date = date('Y-m-d');
		$sql="
			SELECT 
			  a.`NoDokumen`,
			  DATE_FORMAT(DATE(a.reservasiDate), '%d-%m-%Y') AS Tgl_Kedatangan,
			  a.`Jam_Kedatangan`,
			  b.`Nama`,
			  a.`GroupName`,
			  a.`GroupCode`,
			  a.`Nationality`,
			  a.`adultParticipants`,
			  a.`childParticipants`,
			  a.`kidsParticipants`,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants` AS Participants,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants`  AS Tour,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants`  AS Lounch,
			  a.`adultParticipants` +  a.`childParticipants` +  a.`kidsParticipants`  AS Dinner,
			  a.`Event1`,
			  a.`Event2`,
			  a.`Event3`,
			  a.`Event4`,
			  a.`Event5`,
			  a.`Contact`,
			  c.`NamaSalesman`,
			  a.`desc_f_b_kitchen` AS Ket1,
			  a.`desc_black_aye` AS Ket2,
			  a.`desc_oemah_herborist` AS Ket3,
			  a.`desc_front_office` AS Ket4,
			  a.`status_konfirmasi` 
			FROM
			  `trans_reservasi` a 
			  INNER JOIN `tourtravel` b 
			    ON a.`KdTravel` = b.`KdTravel` 
			  INNER JOIN salesman c 
			    ON a.`Sales_In_Charge` = c.`KdSalesman` 
			WHERE 1
			  AND a.`TglDokumen` > '$date' and a.status<>2 order by a.ReservasiDate
		";
		//echo $sql;
		return $this->getArrayResult($sql);	
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'guestlist'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function getArrayDetail($arrdata)
	{
		$where = "";
		if($arrdata)
		{
			$where = $arrdata;
		}
		
		$sql = "
			SELECT 
			  trans_order_barang_detail.*,
			  masterbarang.NamaLengkap as nama_barang
			FROM
			  trans_order_barang_detail 
			  INNER JOIN masterbarang 
			    ON trans_order_barang_detail.PCode = masterbarang.PCode 
			WHERE 
			  1 
			  ".$where."
			ORDER BY trans_order_barang_detail.Sid DESC
		";
		
		return $this->getArrayResult($sql);	
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function jmlhari($tgl) {
	 	
		
		$sql ="
				SELECT LAST_DAY('".$tgl."') AS tgl;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
    
    function getTourtravel(){
	 	
		
		$sql ="
				SELECT * FROM `tourtravel` a ORDER BY a.`Nama` ASC;
			  ";
		
		return $this->getArrayResult($sql);
		
    }
    
}
?>