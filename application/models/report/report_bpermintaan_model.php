<?php
class Report_bpermintaan_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getNamaGudang($kode)
	{
		$sql = "select KdGudang,Keterangan from gudang where KdGudang='$kode'";
		return $this->getRow($sql);
	}
	function getGudang()
	{
		$sql = "select KdGudang,Keterangan from gudang order by KdGudang";
		return $this->getArrayResult($sql);
	}
	function getBarang()
	{
		$sql = "select PCode,NamaLengkap from masterbarang order by PCode";
		return $this->getArrayResult($sql);
	}
	function getNamaDivisi($kode)
	{
		$sql = "select KdDivisi,NamaDivisi from divisi where KdDivisi='$kode'";
		return $this->getRow($sql);
	}
	function getDivisi()
	{
		$sql = "select KdDivisi,NamaDivisi from divisi order by KdDivisi";
		return $this->getArrayResult($sql);
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select TglTrans as TglTrans,DATE_FORMAT(TglTrans-3,'%Y-%m-%d') as TglTrans2 from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getRekapTrans($wheretgl,$wheredivisi,$wheregudang,$wherebarang,$whereopt1)
	{
		$sql = "SELECT a.*,if(a.Status='0','Pending','Open') as StatusT,b.Keterangan as NamaGudang,c.NamaDivisi as NamaDivisi FROM (SELECT * FROM permintaan_barang_header WHERE
				$wheretgl $wheredivisi $wheregudang $whereopt1 ORDER BY NoDokumen)a
				LEFT JOIN
				(SELECT KdGudang,Keterangan FROM gudang)b
				ON a.KdGudang=b.KdGudang
				LEFT JOIN
				(SELECT KdDivisi,NamaDivisi FROM divisi)c
				ON a.KdDivisi=c.KdDivisi";
		//echo $sql;
		return $this->getArrayResult($sql);
	}
	function getDetailTrans($wheretgl1,$wheredivisi1,$wheregudang1,$wherebarang1,$whereopt11)
	{
		$sql = "SELECT o.*,if(Status='0','Pending','Open') as StatusT,NamaBarang,NamaGudang,NamaDivisi FROM(
				SELECT a.NoDokumen,b.KdDivisi,b.KdGudang,b.TglDokumen,
				DATE_FORMAT(b.TglDokumen,'%d-%m-%Y') AS Tanggal,PCode,
				Qty,Satuan,b.Status,
				b.Keterangan FROM permintaan_barang_detail a, permintaan_barang_header b
				WHERE $wheretgl1 $wheredivisi1 $wheregudang1 $wherebarang1 $whereopt11 and a.NoDokumen=b.NoDokumen
				order by TglDokumen,NoDokumen,sid
				)o
				LEFT JOIN
				(
				SELECT PCode,NamaLengkap as NamaBarang FROM masterbarang
				)d
				ON d.PCode=o.PCode
				LEFT JOIN
				(SELECT KdGudang,Keterangan as NamaGudang FROM gudang)b
				ON o.KdGudang=b.KdGudang
				LEFT JOIN
				(SELECT KdDivisi,NamaDivisi as NamaDivisi FROM divisi)c
				ON o.KdDivisi=c.KdDivisi
				";
		return $this->getArrayResult($sql);
	}
    
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>