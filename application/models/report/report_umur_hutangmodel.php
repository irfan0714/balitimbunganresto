<?php
class Report_umur_hutangmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getSupplier()
	{
		$sql = "select KdSupplier,Nama from supplier order by KdSupplier";
		return $this->getArrayResult($sql);
	}
	function getDate()
	{
		$sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getReport($date)
	{
		$sql = "SELECT h.`MataUang`, h.`KdSupplier`, s.`Nama`, h.`NoDokumen`, i.NoPO, i.NoPenerimaan, i.NoFakturSupplier, h.`Tanggal`, h.`JatuhTempo`, h.`NilaiTransaksi`, h.`Sisa`+COALESCE(p.Bayar,0) AS SisaHutang,
					if(h.JatuhTempo>='$date',h.`Sisa`+COALESCE(p.Bayar,0),0) as BelumJT,
					if(DATEDIFF('$date',h.JatuhTempo)>=1 and DATEDIFF('$date',h.JatuhTempo)<31,`Sisa`+COALESCE(p.Bayar,0),0) as Aging1,
					if(DATEDIFF('$date',h.JatuhTempo)>=31 and DATEDIFF('$date',h.JatuhTempo)<61,`Sisa`+COALESCE(p.Bayar,0),0) as Aging31,
					if(DATEDIFF('$date',h.JatuhTempo)>=61 and DATEDIFF('$date',h.JatuhTempo)<91,`Sisa`+COALESCE(p.Bayar,0),0) as Aging61,
					if(DATEDIFF('$date',h.JatuhTempo)>=91,`Sisa`+COALESCE(p.Bayar,0),0) as Aging91,
					DATEDIFF('$date',h.JatuhTempo) as Umur
					FROM 
					hutang h
					INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
					left join invoice_pembelian_header i on h.NoDokumen=i.NoFaktur
					LEFT JOIN (
					SELECT d.`NoFaktur`, SUM(d.`NilaiBayar`) AS Bayar
					FROM `pelunasan_hutang_header` h INNER JOIN `pelunasan_hutang_detail` d ON h.`NoTransaksi`=d.`NoTransaksi`
					WHERE h.`TglDokumen`>'$date' AND h.`Status`=1 AND d.`NilaiBayar`>0
					GROUP BY d.`NoFaktur`) p ON h.NoDokumen=p.NoFaktur
					WHERE h.tanggal<='$date' AND tipetransaksi <>'P'
					HAVING SisaHutang<>0
					ORDER BY h.`MataUang`, h.`KdSupplier`, h.`NoDokumen`";
		return $this->getArrayResult($sql);
	}
	
	function getReportRekap($date)
	{
		$sql = "SELECT h.`MataUang`, h.`KdSupplier`, s.`Nama`, sum(h.`NilaiTransaksi`) as NilaiTransaksi , sum(h.`Sisa`+COALESCE(p.Bayar,0)) AS SisaHutang,
					sum(if(h.JatuhTempo>='$date',h.`Sisa`+COALESCE(p.Bayar,0),0)) as BelumJT,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=1 and DATEDIFF('$date',h.JatuhTempo)<31,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging1,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=31 and DATEDIFF('$date',h.JatuhTempo)<61,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging31,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=61 and DATEDIFF('$date',h.JatuhTempo)<91,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging61,
					sum(if(DATEDIFF('$date',h.JatuhTempo)>=91,`Sisa`+COALESCE(p.Bayar,0),0)) as Aging91
					FROM 
					hutang h
					INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
					left join invoice_pembelian_header i on h.NoDokumen=i.NoFaktur
					LEFT JOIN (
					SELECT d.`NoFaktur`, SUM(d.`NilaiBayar`) AS Bayar
					FROM `pelunasan_hutang_header` h INNER JOIN `pelunasan_hutang_detail` d ON h.`NoTransaksi`=d.`NoTransaksi`
					WHERE h.`TglDokumen`>'$date' AND h.`Status`=1 AND d.`NilaiBayar`>0
					GROUP BY d.`NoFaktur`) p ON h.NoDokumen=p.NoFaktur
					WHERE h.tanggal<='$date' AND tipetransaksi <>'P'
					group by h.MataUang, h.KdSupplier HAVING SisaHutang<>0
					ORDER BY h.`MataUang`, s.Nama";
		return $this->getArrayResult($sql);
	}
	
	function getrekening(){
		$sql = "SELECT h.KdRekening, r.NamaRekening FROM hutang h LEFT JOIN rekening r ON h.KdRekening=r.`KdRekening`
					GROUP BY h.`KdRekening`";
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>