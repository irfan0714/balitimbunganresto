<?php
class Giromodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getKasBank()
	{
		$sql = "select KdKasBank,NamaKasBank from kasbank order by KdKasBank";
		return $this->getArrayResult($sql);
	}
	function getCostCenter()
	{
		$sql = "select KdCostCenter,NamaCostCenter from costcenter order by KdCostCenter";
		return $this->getArrayResult($sql);
	}
	function getPersonal()
	{
		$sql = "select KdPersonal,NamaPersonal from personal order by KdPersonal";
		return $this->getArrayResult($sql);
	}
	function getRekening()
	{
	    $db2 = $this->load->database('gl',true);
    	$sql = "SELECT KdRekening, NamaRekening from rekening where tingkat='3' order by KdRekening";
		$qry = $db2->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function getDate()
	{
		$sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getReport($sql)
	{
		return $this->getArrayResult($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>