<?php
class Report_rencana_bayarmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getReport($date, $date2)
	{
		$sql = "SELECT kb.`NamaKasBank`, h.`NoRencanaBayar`, DATE_FORMAT(h.`TglDokumen`,'%d-%m-%Y') as TglDokumen, h.`Keterangan`, SUM(d.`NilaiBayar`) AS NilaiFaktur, AVG(h.`NilaiPPH`) AS NilaiPPH,
					AVG(h.`BiayaAdmin`) AS BiayaAdmin, AVG(h.`Pembulatan`) AS Pembulatan, DATE_FORMAT(MAX(u.`JatuhTempo`),'%d-%m-%Y') AS JatuhTempo, s.`Nama`, h.NoRekeningSupplier AS `NoRekening`, rs.`NamaBank`, rs.`NamaPemilik`, h.NoPaymentVoucher, 
					avg(COALESCE(p.JumlahPayment,0)) as JumlahPayment  
					  FROM pelunasan_hutang_header h INNER JOIN pelunasan_hutang_detail d ON h.`NoTransaksi`=d.`NoTransaksi`
					INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
					INNER JOIN hutang u ON d.`NoFaktur`=u.`NoDokumen`
					INNER JOIN kasbank kb ON h.`KdKasBank`=kb.`KdKasBank`
					LEFT JOIN rekening_supplier rs ON h.`KdSupplier`=rs.`KdSupplier` AND h.`NoRekening`=rs.`NoRekening`
					left join trans_payment_header p on h.NoPaymentVoucher=p.NoDokumen
					WHERE h.`TglDokumen` between '$date' and '$date2' AND h.`Status`<=1
					GROUP BY kb.`NamaKasBank`, h.`NoRencanaBayar`";
					
		return $this->getArrayResult($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>