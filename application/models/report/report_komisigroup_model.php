<?php
class Report_komisigroup_model extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	function getKomisiGroup()
	{
		$sql = "select KdTravel,Keterangan from komisi_header order by KdTravel";
		return $this->getArrayResult($sql);
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	function getDate()
	{
		$sql = "select TglTrans as TglTrans,DATE_FORMAT(TglTrans-3,'%Y-%m-%d') as TglTrans2 from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getKomisi($tgl1,$sticker,$group)
	{
		$sql = "SELECT det.PCode,det.NamaLengkap,SUM(det.Qty) AS Qty,SUM(det.ttlnetto) AS Netto,kom.total,
kom.komisi1,kom.komisi2,kom.komisi3,kom.komisi4 FROM (SELECT
   a.NoStruk,
   a.Tanggal AS TglJual,
   a.Waktu,
   a.KdAgent,
   b.PCode,
   c.NamaLengkap,
   b.Qty,
   ROUND(IF(b.Service_charge=5,b.Harga,b.Harga/1.1)) AS Harga,
   (
     ROUND(b.Qty*IF(b.Service_charge=5,b.Harga,b.Harga/1.1)-IF(b.Service_charge=5,b.Netto,b.Netto/1.1))
   ) AS Disc,
   (
     ROUND(IF(b.Service_charge=5,b.Netto,b.Netto/1.1),0)
   ) AS ttlnetto,
   IFNULL(b.Komisi,0) AS Komisi
FROM
   transaksi_header a, transaksi_detail b, masterbarang c WHERE 
   ((a.Tanggal='$tgl1' AND a.KdAgent='$sticker' and a.userdisc='')) 
   AND a.nostruk=b.nostruk AND b.pcode=c.pcode)det
LEFT JOIN  
(SELECT kdtravel,pcode AS dpcode,total,komisi1,komisi2,komisi3,komisi4,komisi4bag FROM komisi_detail)kom
ON kom.kdtravel='$group' AND det.pcode=kom.dpcode
GROUP BY PCode";
		//echo $sql;
		return $this->getArrayResult($sql);
	}
    
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>