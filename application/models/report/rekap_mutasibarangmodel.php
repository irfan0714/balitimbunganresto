<?php

//purwanto on sept 16
class Rekap_mutasibarangmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    
    function getReport_Detail($bulan, $tahun) {
    	$sql = "SELECT 
			      d.NamaDivisi, dv.NamaSubDivisi, dv.KdSubDivisi, b.KdRekeningPersediaan, r.NamaRekening,
			      SUM(IF(a.`KdTransaksi` = 'R', a.`Qty`*a.Nilai, 0)) AS jml_R,
			      SUM(IF(a.`KdTransaksi` = 'RG', a.`Qty`*a.Nilai, 0)) AS jml_RG,
			      SUM(IF(a.`KdTransaksi` = 'PL', a.`Qty`*a.Nilai, 0)) + SUM(IF(a.`KdTransaksi` = 'MM', a.`Qty`*a.Nilai, 0)) AS jml_MM_PL,
			      SUM(IF(a.`KdTransaksi` = 'MP' &&  a.Jenis = 'I',a.`Qty`*a.Nilai, 0)) AS jml_MP_IN,
			      SUM(IF(a.`KdTransaksi` = 'DL', a.`Qty`*a.Nilai, 0)) + SUM(IF(a.`KdTransaksi` = 'MC', a.`Qty`*a.Nilai, 0)) AS jml_MC_DL,
			      SUM(IF(a.`KdTransaksi` = 'MP' && a.Jenis = 'O',a.`Qty`*a.Nilai,0)) AS jml_MP_OUT,
			      SUM(IF(a.`KdTransaksi` = 'SO',IF(a.Jenis = 'I', a.`Qty`*a.Nilai, a.`Qty`*a.Nilai * - 1), 0)) AS jml_SO,
			      SUM(IF(a.`KdTransaksi` = 'MX', a.`Qty`*a.Nilai, 0)) AS jml_MX,
			      SUM(IF(a.`KdTransaksi` = 'FG', a.`Qty`*a.Nilai, 0)) AS jml_FG,
			      SUM(IF(a.`KdTransaksi` = 'RB', a.`Qty`*a.Nilai, 0)) + SUM(IF(a.`KdTransaksi` = 'SR', a.`Qty`*a.Nilai, 0)) AS jml_RB_SR 
			    FROM
			      mutasi a 
			      INNER JOIN masterbarang b ON a.kodebarang = b.pcode 
			      INNER JOIN gudang g ON g.KdGudang = a.Gudang 
			      INNER JOIN subdivisi dv ON g.KdSubDivisi = dv.KdSubDivisi 
			      INNER JOIN divisi d ON dv.KdDivisi=d.KdDivisi
			      INNER JOIN rekening r ON b.KdRekeningPersediaan = r.KdRekening
			    WHERE year(a.tanggal)='$tahun' and month(a.tanggal)='$bulan'
			    GROUP BY d.KdDivisi, dv.KdSubDivisi, b.KdRekeningPersediaan";
        return $this->getArrayResult($sql);
    }
    
    function getSaldoAwal($bulan, $tahun){
    	if($bulan==1){
			$bln=12;
			$tahun=$tahun-1;
		}else{
			$bln=$bulan-1;
		}
		
		if($bln<10){
			$bln='0'.$bln;
		}
    	
    	$sql = "SELECT dv.KdSubDivisi, b.KdRekeningPersediaan, SUM(s.GNAkhir$bln) AS SAwal
    			FROM  stock s
			      INNER JOIN masterbarang b ON s.pcode = b.pcode 
			      INNER JOIN gudang g ON g.KdGudang = s.KdGudang 
			      INNER JOIN subdivisi dv ON g.KdSubDivisi = dv.KdSubDivisi 
			      INNER JOIN divisi d ON dv.KdDivisi=d.KdDivisi
			      INNER JOIN rekening r ON b.KdRekeningPersediaan = r.KdRekening
			    WHERE s.`Tahun`='$tahun'
			    GROUP BY dv.KdSubDivisi, b.KdRekeningPersediaan";
		$hasil = $this->getArrayResult($sql);
		$data=array();
		foreach($hasil as $rec){
			$kdsubdivisi = $rec['KdSubDivisi'];
			$kdrekening = $rec['KdRekeningPersediaan'];
			$sawal = $rec['SAwal'];
			
			$data[$kdsubdivisi][$kdrekening] = $sawal;
		}
		return $data;
	}

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

}

?>