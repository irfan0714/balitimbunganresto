<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_cn_dn_hutang_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {

		$sql ="SELECT dntype, h.`dnno`, h.`dndate`, s.`Nama`, h.`note` , r.`KdRekening`, r.`NamaRekening`, sd.`NamaSubDivisi` , d.`description`, d.`value`
				FROM debitnote h INNER JOIN debitnotedtl d ON h.`dnno`=d.`dnno`
				INNER JOIN rekening r ON d.`coano`= r.`KdRekening`
				INNER JOIN supplier s ON h.`supplierid`=s.`KdSupplier`
				INNER JOIN subdivisi sd ON d.`KdSubDivisi`=sd.`KdSubDivisi`
				WHERE h.`dndate` BETWEEN '$tgldari' AND '$tglsampai' AND h.`status`=1
				ORDER BY h.`dntype`, h.`dnno`
			  ";
		//echo $sql;die;		
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result;
	}    
}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */