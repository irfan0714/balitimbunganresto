<?php

class Report_hutang_supplier_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function viewData()
	{
		$sql = "SELECT 
					hutang.KdSupplier, MataUang, Nama AS NamaSupplier, NoFaktur, Tanggal, JatuhTempo AS TanggalJatuhTempo, Sisa
					FROM 
						hutang
					INNER JOIN 
					supplier 
					ON 
						supplier.`KdSupplier` = hutang.`KdSupplier`
					WHERE sisa>0 AND TipeTransaksi!='P'
						 
					ORDER BY MataUang";
				
		$query = $this->db->query($sql);
        $result = $query->result_array();
		return $result; 
	}
	
	
	
}