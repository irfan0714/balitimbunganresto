<?php

class Rpt_tourleadermodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }

    function getReport($bulan, $tahun) {
        $sql = "SELECT
                  s.NamaSubDivisi,
                  h.NoStruk,
                  h.Tanggal,
                  h.Waktu,
                  h.Kasir,
                  h.TotalItem,
                  h.TotalNilai,
                  h.TotalBayar,
                  h.Kembali,
                  h.Tunai,
                  h.KKredit,
                  h.KDebit,
                  h.Voucher,
                  h.Discount,
                  h.KdAgent,
                  h.Ttl_Charge,
                  h.DPP,
                  h.TAX,
                  h.KdMeja,
                  h.userdisc,
                  h.nilaidisc,
                  r.KdTourLeader,
                  r.Keterangan ,
                  t.Nama,t.Phone
               FROM
                  (SELECT
                    *
                  FROM
                    transaksi_header
                  WHERE MONTH(Tanggal) = '".$bulan."' AND  YEAR(Tanggal) = '".$tahun."') h
                  LEFT JOIN register r
                    ON r.NoStiker = h.KdAgent
                    AND r.Tanggal = h.Tanggal
                   LEFT JOIN  tourleader t ON r.KdTourLeader=t.KdTourLeader
               INNER JOIN kassa k
                    ON k.id_kassa = h.NoKassa
                  INNER JOIN subdivisi s
                    ON s.KdSubDivisi = k.SubDivisi ORDER BY Tanggal, KdAgent  
               LIMIT 0,1000000";
        //echo $sql;
        return $this->getArrayResult($sql);
    }

    
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

    function getJenis() {
        $sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
        return $this->getArrayResult($sql);
    }

}

?>