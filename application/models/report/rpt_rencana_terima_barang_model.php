<?php

//purwanto on sept 16
class Rpt_rencana_terima_barang_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }

    function getReport_header($tglawal, $tglakhir) {
        $sql = "SELECT 
                        id_mutasi, DATE_FORMAT(Tanggal,'%d-%m-%Y') AS Tanggal, MPCode, masterbarang.NamaLengkap AS NamaPaket, Qty, Jenis_mutasi, mutasi_paket.Keterangan, gudang.Keterangan AS namagudang 
                      FROM
                        mutasi_paket 
                        LEFT JOIN gudang 
                          ON mutasi_paket.KdGudang = gudang.KdGudang
                        LEFT JOIN masterbarang
                          ON mutasi_paket.MPCode = masterbarang.PCode
                        WHERE DATE_FORMAT(mutasi_paket.Tanggal,'%Y-%m-%d') BETWEEN '".$tglawal."' AND '".$tglakhir."'";
        //echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_Detail($tgl, $gudang) {
     	$sql = "
    		SELECT 
			  a.`NoDokumen`,
			  DATE_FORMAT(a.`TglDokumen`, '%d-%m-%Y') AS TglDokumen,
			  a.`KdGudang`,
			  c.`Keterangan`,
			  a.`KdSupplier`,
			  b.`Nama`,
			  d.`PCode`,
			  e.`NamaLengkap`,
			  d.`Qty`,
			  d.`Satuan`, 
			  d.QtyPcs,
			  e.SatuanSt
			FROM
			  trans_order_barang_header a 
			  INNER JOIN `trans_order_barang_detail` d 
			    ON a.`NoDokumen` = d.`NoDokumen` 
			  INNER JOIN masterbarang e 
			    ON d.`PCode` = e.`PCode` 
			  INNER JOIN supplier b 
			    ON a.`KdSupplier` = b.`KdSupplier` 
			  INNER JOIN gudang c 
			    ON a.`KdGudang` = c.`KdGudang` 
			WHERE a.TglTerima = '".$tgl."' 
			  AND a.KdGudang = '".$gudang."' 
			ORDER BY
			  a.`KdSupplier`,
			  a.`NoDokumen`,
			  e.`NamaLengkap` ASC ;
		";
     
        //echo $sql;die;
        return $this->getArrayResult($sql);
    }


    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

}

?>