<?php
class report_kasir_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getReport($pilihan,$v_tipe_cari,$start_date,$end_date,$nostruk,$divisi,$username,$nostiker,$v_sticker)
    {
    	$user = $this->session->userdata('username');
    	$where_date ="";
		$where_date_detail ="";
        $where_date_reservasi = "";
    	if($start_date!="" && $end_date!="" && $start_date!="0000-00-00" && $end_date!="0000-00-00")
    	{
			$where_date ="AND h.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
			$where_date_detail ="AND d.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";

            $where_date_reservasi ="AND trans_reservasi.TglDokumen BETWEEN '".$start_date."' AND '".$end_date."' ";
		}

    	$where_nostruk ="";
		$where_nostruk_detail ="";
    	if($nostruk)
    	{
      $where_nostruk = " AND `h`.NoStruk = '".$nostruk."' ";
			$where_nostruk_detail = " AND `d`.NoStruk = '".$nostruk."' ";
		  }
      if($v_sticker)
    	{
      $where_sticker = " AND `h`.KdAgent = '".$v_sticker."' ";
			$where_sticker_detail = " AND `d`.KdAgent = '".$v_sticker."' ";
		  }

    	$where_divisi ="";
    	if($divisi)
    	{
            $where_divisi = " AND `divisi`.KdDivisi = '".$divisi."' ";
		}

        $where_kasir = "";
		$where_kasir_detail = "";
        $where_kasir_reservasi = "";
        if($username!="")
        {
            $where_kasir = " AND `h`.Kasir = '".$username."' ";
			$where_kasir_detail = " AND `d`.Kasir = '".$username."' ";
        }

		$sql = "";
  		if($pilihan=="transaksi"){

			if($nostiker=="T"){

			if($v_tipe_cari=="struk"){
            $sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  h.`Tanggal`,
				  h.`Waktu`,
				  h.`NoStruk`,
				  h.KdAgent,
				  h.TotalGuest,
				  h.TotalNilaiPem as Rounding,
				  -- SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(d.`Qty` * IF(k.PriceIncludeTax = 'T',IF(d.`Berat`<>0,d.`Harga`*(d.`Berat`/100),d.`Harga`),IF(d.`Berat`<>0,(d.`Harga`*(d.`Berat`/100))/1.1,d.`Harga`/1.1)) ) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    ) 
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`) ) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				    SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
						)/h.`TotalNilai`)) AS Kembali,
						kr1.nama AS BankDebet,
						kr2.nama AS EDCBankDebet,
						kr3.nama AS BankKredit,
						kr4.nama AS EDCBankKredit
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
  				  INNER JOIN subdivisi s
    			  ON k.SubDivisi = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
					INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
					LEFT JOIN kartu kr1
						ON h.BankDebet = kr1.id
					LEFT JOIN kartu kr2
						ON h.EDCBankDebet = kr2.id
					LEFT JOIN kartu kr3
						ON h.BankKredit = kr3.id
					LEFT JOIN kartu kr4
						ON h.EDCBankKredit = kr4.id
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date."
				 GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`, h.`NoStruk`
				UNION ALL
				SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  h.`Tanggal`,
				  h.`Waktu`,
				  h.`NoStruk`,
				  h.KdAgent,
				  h.TotalGuest,
				  h.TotalNilaiPem as Rounding,
				  -- SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(d.`Qty` * IF(k.PriceIncludeTax = 'T',IF(d.`Berat`<>0,d.`Harga`*(d.`Berat`/100),d.`Harga`),IF(d.`Berat`<>0,(d.`Harga`*(d.`Berat`/100))/1.1,d.`Harga`/1.1)) ) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    ) + h.TotalNilaiPem
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`) + h.TotalNilaiPem ) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
						)/h.`TotalNilai`)) AS Kembali,
						kr1.nama AS BankDebet,
						kr2.nama AS EDCBankDebet,
						kr3.nama AS BankKredit,
						kr4.nama AS EDCBankKredit
				FROM
				  transaksi_header_sunset h
				  INNER JOIN transaksi_detail_sunset d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k
				    on h.NoKassa = k.id_kassa
				  INNER JOIN subdivisi s
				    ON k.`SubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
					INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
					LEFT JOIN kartu kr1
						ON h.BankDebet = kr1.id
					LEFT JOIN kartu kr2
						ON h.EDCBankDebet = kr2.id
					LEFT JOIN kartu kr3
						ON h.BankKredit = kr3.id
					LEFT JOIN kartu kr4
						ON h.EDCBankKredit = kr4.id
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`, h.`NoStruk`";
				//echo $sql;
		}else if($v_tipe_cari=="kasir"){
			$sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  '' AS `Tanggal`,
				  '' AS `Waktu`,
				  '' AS NoStruk,
				  '' as KdAgent,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
  				  INNER JOIN subdivisi s
    			  ON k.SubDivisi = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Kasir`
				UNION ALL
				SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  '' AS `Tanggal`,
				  '' AS `Waktu`,
				  '' AS NoStruk,
				  '' as KdAgent,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T'>0,d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header_sunset h
				  INNER JOIN transaksi_detail_sunset d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				   INNER JOIN kassa k
				    on h.NoKassa = k.id_kassa
				  INNER JOIN subdivisi s
				    ON k.`SubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Kasir`";
				}

				}else{

					$sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  h.`Tanggal`,
				  h.`Waktu`,
				  IF(h.`KdAgent`='','----',UPPER(h.`KdAgent`)) AS KdAgent,
				  h.`NoStruk`,
				  SUM(transaksi_detail.`Qty`*IF(transaksi_detail.Service_charge>0,transaksi_detail.`Harga`,transaksi_detail.`Harga`/1.1)) AS Gross,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Disc1`,transaksi_detail.`Disc1`/1.1)) AS Discount,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)) AS Nett,
				  SUM(transaksi_detail.`Netto` * transaksi_detail.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)+ (IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1) * transaksi_detail.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`Voucher`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail
				    ON h.`NoKassa` = transaksi_detail.`NoKassa`
				    AND h.`NoStruk` = transaksi_detail.`NoStruk`
				  INNER JOIN masterbarang b
				    ON transaksi_detail.`PCode` = b.`PCode`
				  INNER JOIN subdivisi s
				    ON b.`KdSubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`, h.`NoStruk` ORDER BY h.KdAgent ASC";
				}

		}
    elseif($pilihan=="sticker"){

			if($nostiker=="T"){

			if($v_tipe_cari=="struk"){
            $sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  h.`Tanggal`,
				  h.`Waktu`,
				  h.`NoStruk`,
				  h.KdAgent,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				    SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
  				  INNER JOIN subdivisi s
    			  ON k.SubDivisi = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_sticker."
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				" GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`, h.`NoStruk`
				UNION ALL
				SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  h.`Tanggal`,
				  h.`Waktu`,
				  h.`NoStruk`,
				  h.KdAgent,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header_sunset h
				  INNER JOIN transaksi_detail_sunset d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k
				    on h.NoKassa = k.id_kassa
				  INNER JOIN subdivisi s
				    ON k.`SubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_sticker."
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`, h.`NoStruk`";
		}else if($v_tipe_cari=="kasir"){
			$sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  '' AS `Tanggal`,
				  '' AS `Waktu`,
				  '' AS NoStruk,
				  '' as KdAgent,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
  				  INNER JOIN subdivisi s
    			  ON k.SubDivisi = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_sticker."
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Kasir`
				UNION ALL
				SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  '' AS `Tanggal`,
				  '' AS `Waktu`,
				  '' AS NoStruk,
				  '' as KdAgent,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T'>0,d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header_sunset h
				  INNER JOIN transaksi_detail_sunset d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				   INNER JOIN kassa k
				    on h.NoKassa = k.id_kassa
				  INNER JOIN subdivisi s
				    ON k.`SubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_sticker."
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Kasir`";
				}

				}else{

					$sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Kasir`,
				  h.`Tanggal`,
				  h.`Waktu`,
				  IF(h.`KdAgent`='','----',UPPER(h.`KdAgent`)) AS KdAgent,
				  h.`NoStruk`,
				  SUM(transaksi_detail.`Qty`*IF(transaksi_detail.Service_charge>0,transaksi_detail.`Harga`,transaksi_detail.`Harga`/1.1)) AS Gross,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Disc1`,transaksi_detail.`Disc1`/1.1)) AS Discount,
				  SUM(IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)) AS Nett,
				  SUM(transaksi_detail.`Netto` * transaksi_detail.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1)+ (IF(transaksi_detail.Service_charge>0,transaksi_detail.`Netto`,transaksi_detail.`Netto`/1.1) * transaksi_detail.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`Voucher`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      transaksi_detail.`Service_charge` > 0,
				      transaksi_detail.`Netto` * 1155 / 1000,
				      transaksi_detail.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail
				    ON h.`NoKassa` = transaksi_detail.`NoKassa`
				    AND h.`NoStruk` = transaksi_detail.`NoStruk`
				  INNER JOIN masterbarang b
				    ON transaksi_detail.`PCode` = b.`PCode`
				  INNER JOIN subdivisi s
				    ON b.`KdSubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_sticker."
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`, h.`NoStruk` ORDER BY h.KdAgent ASC";
				}

		}
		elseif($pilihan=="detail"){

			$sql ="
				SELECT
					`h`.NoKassa,
					`h`.Kasir,
					`divisi`.KdDivisi,
					`divisi`.NamaDivisi,
					`h`.Tanggal,
					`h`.Waktu,
					`h`.NoStruk,
					`h`.TotalNilai,
					`h`.TotalBayar,
					`h`.Voucher,
					IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1) AS Discount,
					`h`.TotalItem,
					d.PCode,
					masterbarang.NamaLengkap,
					d.Qty,
					d.Berat,
					IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Harga,
					d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Bruto,
					(IFNULL(d.Disc1,0)+IFNULL(d.Disc2,0)+IFNULL(d.Disc3,0)+IFNULL(d.Disc4,0)) AS DiscDetail,
					IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) AS Netto,
					d.Service_charge,
					masterbarang.PPN
				FROM
					transaksi_detail d
				INNER JOIN transaksi_header h
					ON h.NoStruk = `d`.NoStruk
					AND `h`.status = '1'
					".$where_kasir."
					".$where_nostruk."
					".$where_date."
				INNER JOIN kassa k
					ON h.NoKassa = k.id_kassa
				INNER JOIN divisi
					ON k.KdDivisi = divisi.KdDivisi
					".$where_divisi."
				INNER JOIN masterbarang
					ON `d`.PCode=masterbarang.PCode
				WHERE
					1
					".$where_kasir_detail."
					".$where_nostruk_detail."
					".$where_date_detail."
					AND `h`.status = '1'
				UNION ALL
				SELECT
					`h`.NoKassa,
					`h`.Kasir,
					`divisi`.KdDivisi,
					`divisi`.NamaDivisi,
					`h`.Tanggal,
					`h`.Waktu,
					`h`.NoStruk,
					`h`.TotalNilai,
					`h`.TotalBayar,
					`h`.Voucher,
					IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1) AS Discount,
					`h`.TotalItem,
					d.PCode,
					masterbarang.NamaLengkap,
					d.Qty,
					d.Berat,
					IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Harga,
					d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1) AS Bruto,
					(IFNULL(d.Disc1,0)+IFNULL(d.Disc2,0)+IFNULL(d.Disc3,0)+IFNULL(d.Disc4,0)) AS DiscDetail,
					IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) AS Netto,
					d.Service_charge,
					masterbarang.PPN
				FROM
					transaksi_detail_sunset d
				INNER JOIN transaksi_header_sunset h
					ON h.NoStruk = `d`.NoStruk
					AND `h`.status = '1'
					".$where_kasir."
					".$where_nostruk."
					".$where_date."
				INNER JOIN kassa k
					ON h.NoKassa = k.id_kassa
				INNER JOIN divisi
					ON k.KdDivisi = divisi.KdDivisi
					".$where_divisi."
				INNER JOIN masterbarang
					ON `d`.PCode=masterbarang.PCode
				WHERE
					1
					".$where_kasir_detail."
					".$where_nostruk_detail."
					".$where_date_detail."
					AND `h`.status = '1'
				ORDER BY
					KdDivisi,
					Tanggal ASC,
					Waktu ASC,
					NoStruk ASC,
					PCode ASC";
		}
		elseif($pilihan=="barang"){

			$sql ="SELECT
					  divisi.`NamaDivisi`,
					  r.`NamaDivisiReport` as SubDivisi,
					  d.`PCode`,
					  b.`NamaLengkap`,
					  SUM(if(h.TotalNilai>0,d.`Qty`,0)) AS Qty,
					  SUM(if(h.TotalNilai=0,d.`Qty`,0)) AS QtyCompliment,
					  AVG(IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Harga,
					  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
					  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
					  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett
					FROM
					  transaksi_header h
					  INNER JOIN transaksi_detail d
					    ON h.`NoKassa` = d.`NoKassa`
					    AND h.`NoStruk` = d.`NoStruk`
					INNER JOIN kassa k ON h.`NoKassa`=k.id_kassa
  					INNER JOIN subdivisi s
    					ON k.SubDivisi = s.`KdSubDivisi`
					INNER JOIN masterbarang b
						ON d.`PCode` = b.`PCode`
					INNER JOIN divisireport r
					    ON s.`KdDivisiReport` = r.`KdDivisiReport`
					  INNER JOIN divisi ON s.`KdDivisi`=divisi.KdDivisi
					WHERE 
					h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
					"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , b.`PCode`
					UNION ALL
					SELECT
					  divisi.`NamaDivisi`,
					  r.`NamaDivisiReport` as SubDivisi,
					  d.`PCode`,
					  b.`NamaLengkap`,
					  SUM(if(h.TotalNilai>0,d.`Qty`,0)) AS Qty,
					  SUM(if(h.TotalNilai=0,d.`Qty`,0)) AS QtyCompliment,
					  AVG(IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Harga,
					  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
					  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
					  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett
					FROM
					  transaksi_header_sunset h
					  INNER JOIN transaksi_detail_sunset d
					    ON h.`NoKassa` = d.`NoKassa`
					    AND h.`NoStruk` = d.`NoStruk`
					inner join masterbarang b on d.pcode=b.pcode
					 INNER JOIN kassa k
				    	on h.NoKassa = k.id_kassa
				  	INNER JOIN subdivisi s
					    ON k.`SubDivisi` = s.`KdSubDivisi`
					  INNER JOIN divisireport r
					    ON s.`KdDivisiReport` = r.`KdDivisiReport`
					  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
					WHERE 
					h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
					"GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , b.`PCode`
					";
		}
		elseif($pilihan=="dpp"){

			$sql ="
				SELECT
					`h`.NoKassa,
					`h`.Kasir,
					`divisi`.KdDivisi,
					`divisi`.NamaDivisi,
					`subdivisi`.KdSubDivisi,
					`subdivisi`.NamaSubDivisi,
					`h`.Tanggal,
					`h`.Waktu,
					`h`.NoStruk,
					`h`.TotalNilai,
					`h`.TotalBayar,
					`h`.Voucher,
					`h`.Discount,
					`h`.TotalItem,
					d.PCode,
					masterbarang.NamaLengkap,
					d.Qty,
					d.Harga,
					(d.Qty * d.Harga) AS Bruto,
					(IFNULL(d.Disc1,0)+IFNULL(d.Disc2,0)+IFNULL(d.Disc3,0)+IFNULL(d.Disc4,0)) AS DiscDetail,
					d.Netto,
					d.Service_charge,
					masterbarang.PPN,
					masterbarang.PersenPajak
				FROM
					transaksi_detail d
				INNER JOIN transaksi_header h
					ON h.NoStruk = `d`.NoStruk
					AND `h`.status = '1'
					".$where_kasir."
					".$where_nostruk."
					".$where_date."
				INNER JOIN kassa
					ON h.NoKassa = kassa.id_kassa
				INNER JOIN divisi
					ON kassa.KdDivisi = divisi.KdDivisi
					".$where_divisi."
				INNER JOIN subdivisi
					ON kassa.SubDivisi = subdivisi.KdSubDivisi
					".$where_divisi."
				INNER JOIN masterbarang
					ON `d`.PCode=masterbarang.PCode
				WHERE
					1
					".$where_kasir_detail."
					".$where_nostruk_detail."
					".$where_date_detail."
					AND `h`.status = '1'
				UNION ALL
				SELECT
					`h`.NoKassa,
					`h`.Kasir,
					`divisi`.KdDivisi,
					`divisi`.NamaDivisi,
					`subdivisi`.KdSubDivisi,
					`subdivisi`.NamaSubDivisi,
					`h`.Tanggal,
					`h`.Waktu,
					`h`.NoStruk,
					`h`.TotalNilai,
					`h`.TotalBayar,
					`h`.Voucher,
					`h`.Discount,
					`h`.TotalItem,
					d.PCode,
					masterbarang.NamaLengkap,
					d.Qty,
					d.Harga,
					(d.Qty * d.Harga) AS Bruto,
					(IFNULL(d.Disc1,0)+IFNULL(d.Disc2,0)+IFNULL(d.Disc3,0)+IFNULL(d.Disc4,0)) AS DiscDetail,
					d.Netto,
					d.Service_charge,
					masterbarang.PPN,
					masterbarang.PersenPajak
				FROM
					transaksi_detail_sunset d
				INNER JOIN transaksi_header_sunset h
					ON h.NoStruk = `d`.NoStruk
					AND `h`.status = '1'
					".$where_kasir."
					".$where_nostruk."
					".$where_date."
				INNER JOIN kassa
					ON h.NoKassa = kassa.id_kassa
				INNER JOIN divisi
					ON kassa.KdDivisi = divisi.KdDivisi
					".$where_divisi."
				INNER JOIN subdivisi
					ON kassa.SubDivisi = subdivisi.KdSubDivisi
					".$where_divisi."
				INNER JOIN masterbarang
					ON `d`.PCode=masterbarang.PCode
				WHERE
					1
					".$where_kasir_detail."
					".$where_nostruk_detail."
					".$where_date_detail."
					AND `h`.status = '1'
				ORDER BY
					KdDivisi,
					Tanggal ASC,
					Waktu ASC,
					NoStruk ASC,
					PCode ASC
					";

		}
		elseif($pilihan=="tanggal"){

            $sql ="
                SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Tanggal`,
				    h.`NoStruk`,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header h
				  INNER JOIN transaksi_detail d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN masterbarang b
				    ON d.`PCode` = b.`PCode`
				  inner join kassa k on h.NoKassa = k.id_kassa
				  INNER JOIN subdivisi s
				    ON b.`KdSubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, h.`Tanggal`
				UNION ALL
				SELECT
				  divisi.`NamaDivisi`,
				  r.`NamaDivisiReport` as SubDivisi,
				  h.`Tanggal`,
				    h.`NoStruk`,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Discount,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )
				  ) AS Sales,
				  SUM(h.`Tunai`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Tunai ,
				  SUM(h.`KKredit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KKredit ,
				  SUM(h.`KDebit`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS KDebit ,
				  SUM(h.`GoPay`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS GoPay ,
				  SUM(h.`Point`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Point,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher ,
				   SUM(h.`Kembali`*(IF(
				      k.PriceIncludeTax='T',
				      d.Netto * (110 + (1.1*d.`Service_charge`))/100,
				      d.Netto
				    )/h.`TotalNilai`)) AS Kembali
				FROM
				  transaksi_header_sunset h
				  INNER JOIN transaksi_detail_sunset d
				    ON h.`NoKassa` = d.`NoKassa`
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k
				    on h.NoKassa = k.id_kassa
				  INNER JOIN subdivisi s
				    ON k.`SubDivisi` = s.`KdSubDivisi`
				  INNER JOIN divisireport r
				    ON s.`KdDivisiReport` = r.`KdDivisiReport`
				  INNER JOIN divisi  ON s.`KdDivisi`=divisi.`KdDivisi`
				  WHERE
                    h.status = '1'
                    AND h.`TotalNilai` > 0
                    ".$where_kasir."
                    ".$where_nostruk."
                    ".$where_divisi."
                    ".$where_date.
				"GROUP BY divisi.`KdDivisi`, h.`Tanggal`
            ";
           //GROUP BY divisi.`KdDivisi`, r.KdDivisiReport , h.`Tanggal`
		//echo $sql;
		}

		if($user=="tonny1205"){
			//echo $sql;
		}
    if($user=="mechael0101"){
      echo "=> ".$sql;
    }
		//echo $sql; die();
        $result = $this->getArrayResult($sql);
        return $result;
    }

	function getVoucher($start_date,$end_date,$arr_struck)
	{
		$where_date ="";
    	if($start_date!="" && $start_date!="0000-00-00" && $end_date!="" && $end_date!="0000-00-00")
    	{
			$where_date ="AND v.Tanggal BETWEEN '".$start_date."' AND '".$end_date."' ";
		}

		$where_struck="";
		if(count($arr_struck)>0)
		{
			$where_struck = $arr_struck;
		}

		$sql="
			SELECT
			  v.NoKassa,
			  v.NoStruk,
			  DATE_FORMAT(v.Tanggal, '%d-%m-%Y') AS Tanggal,
			  v.Jenis,
			  v.NomorVoucher,
			  v.NilaiVoucher
			FROM
			  transaksi_detail_voucher v
			WHERE
			  1
			  ".$where_date."
			  ".$where_struck."
			ORDER BY v.Tanggal DESC
		";
		return $this->getArrayResult($sql);

	}

    function getType($id="")
    {
    	$where_divisi="";
    	if(count($id)>0)
    	{
			$where_divisi = "AND KdDivisiReport IN (".$id.")";
		}

		$sql = "SELECT KdDivisiReport as KdDivisi, NamaDivisiReport as NamaDivisi FROM divisireport ORDER BY NamaDivisiReport ASC";

		return $this->getArrayResult($sql);
	}

	function getDivisi($id="")
    {
    	$where_divisi="";
    	if(count($id)>0)
    	{
			$where_divisi = "AND KdDivisi IN (".$id.")";
		}

		$sql = "SELECT KdDivisi, NamaDivisi FROM divisi ORDER BY NamaDivisi ASC";

		return $this->getArrayResult($sql);
	}

    function getKasir($id="")
    {
    	$where_kasir="";
    	if(count($id)>0)
    	{
			$where_kasir = "AND user.UserLevel IN (".$id.")";
		}

		$sql = "
            SELECT
              user.UserLevel,
              userlevels.UserLevelName,
              user.employee_nik,
              user.UserName,
              employee.employee_name
            FROM
              user
              LEFT JOIN userlevels
                ON user.UserLevel = userlevels.UserLevelID
              LEFT JOIN employee
                ON user.employee_nik = employee.employee_nik
            WHERE 1
               ".$where_kasir."
			AND user.Active='Y'
            GROUP BY user.employee_nik
            ORDER BY user.UserName ASC
		";
		return $this->getArrayResult($sql);
	}

    function getKasirFromHeader()
    {
    	$date = date('Y-m-d');
		$sql = "Select Kasir, employee_name from (
            SELECT
              h.Kasir,
              e.employee_name
            FROM
              transaksi_header h
              LEFT JOIN employee e
                ON h.Kasir = e.username
            where h.tanggal between DATE_ADD('$date', INTERVAL -31 DAY) and '$date'
            GROUP BY Kasir
            union all
            SELECT
              h.Kasir,
              e.employee_name
            FROM
              transaksi_header_sunset h
              LEFT JOIN employee e
                ON h.Kasir = e.username
            where h.tanggal between DATE_ADD('$date', INTERVAL -31 DAY) and '$date'
            GROUP BY Kasir
            ) t ORDER BY Kasir ASC
        ";

		return $this->getArrayResult($sql);
	}

    function getArrayResult($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql)
    {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql)
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate()
    {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }

    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}

	function cekOtorisasi($Tipe, $User)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  otorisasi_user a
			WHERE 1  
			  AND UserName='$User'
			  AND a.Tipe = '$Tipe'
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

}

?>
