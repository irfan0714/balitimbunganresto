<?php
class report_pendapatan_kasirmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getData($tgldari, $tglsampai){
		$sql = "SELECT Tanggal, SUM(TotalNilai) AS TotalNilai, SUM(Tunai) AS Tunai,
					  SUM(KKredit) AS KKredit, SUM(KDebit) AS KDebit, SUM(Voucher) AS Voucher,
					  SUM(USD) AS USD, SUM(CNY) AS CNY,
					  SUM(USDRp) AS USDRp, SUM(CNYRp) AS CNYRp
					FROM (
					SELECT 
					  Tanggal, SUM(TotalNilai) AS TotalNilai, SUM(Tunai - Kembali) AS Tunai,
					  SUM(KKredit) AS KKredit, SUM(KDebit) AS KDebit, SUM(Voucher) AS Voucher,
					  SUM(IF(`Valuta` = 'USD', `Valas`, 0)) AS USD, SUM(IF(`Valuta` = 'CNY', `Valas`, 0)) AS CNY,
					  SUM(IF(`Valuta` = 'USD', `Valas`*Kurs, 0)) AS USDRp, SUM(IF(`Valuta` = 'CNY', `Valas`*Kurs, 0)) AS CNYRp
					FROM
					  transaksi_header 
					WHERE STATUS = '1' 
					  AND Tanggal BETWEEN '$tgldari'  AND '$tglsampai' 
					GROUP BY Tanggal 
					UNION ALL
					SELECT DATE(h.tanggal), SUM(h.ttl_amount), SUM(h.nilai_tunai), SUM(h.nilai_kredit), SUM(h.nilai_debit),
					SUM(h.nilai_voucher), 0, 0, 0, 0 
					FROM ticket_head h
					WHERE DATE(h.tanggal) BETWEEN '$tgldari'  AND '$tglsampai' AND h.noidentitas<>'1234'
					GROUP BY DATE(h.tanggal)) t GROUP BY Tanggal order by Tanggal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>