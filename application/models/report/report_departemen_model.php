<?php

class Report_departemen_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	 function viewData($tgldari, $tglsampai, $departemen_name) {
	 	$sql = "SELECT shiftdate as Tanggal, Shift, KdDepartemen, e.employee_name as NamaKaryawan, e.employee_id,
			TIME(MIN(t.`PresensiTime`)) AS TimeIn, TIME(MAX(t.`PresensiTime`)) AS TimeOut, t.NoAbsen, DAYNAME(DATE(shiftdate))AS Hari
				FROM shift s 
				INNER JOIN employee e ON s.`KdEmployee`=e.`employee_id`
				INNER JOIN absensiemployee a ON s.`KdEmployee`=a.`employee_id`
				inner join employee_position ep on e.employee_id = ep.employee_id
				inner join hrd_departemen dp on ep.departemen_id = dp.departemen_id
				LEFT JOIN absensitrn t ON a.`NoAbsen`=t.`NoAbsen` AND s.`ShiftDate`=DATE(t.`PresensiTime`)
				WHERE dp.departemen_name LIKE '%$departemen_name%' and s.ShiftDate between '$tgldari' AND '$tglsampai'
				GROUP BY s.`KdEmployee`, s.`ShiftDate`
				ORDER BY s.`KdEmployee`, s.`ShiftDate`";
				
		$query = $this->db->query($sql);
        $result = $query->result_array();
        
        $arr = array();
       
        for ($i=0; $i<count($result); $i++){
        	$empid = $result[$i]['employee_id'];
        	$tgl = $result[$i]['Tanggal'];
        	$nama = $result[$i]['NamaKaryawan'];
        	$timein = $result[$i]['TimeIn'];
        	$timeout = $result[$i]['TimeOut'];
        	$shift = $result[$i]['Shift'];
        	
			$data = array($timein,$timeout,$shift);
			$arr[$empid][$tgl] = implode('*', $data);
			
		}
					
		return $arr;
    }
    
    function getListEmp($tgldari, $tglsampai, $departemen_name) {
	 	$sql = "SELECT e.employee_name as NamaKaryawan, e.employee_id
				FROM shift s 
				INNER JOIN employee e ON s.`KdEmployee`=e.`employee_id`
				INNER JOIN absensiemployee a ON s.`KdEmployee`=a.`employee_id`
				inner join employee_position ep on e.employee_id = ep.employee_id
				inner join hrd_departemen dp on ep.departemen_id = dp.departemen_id
				LEFT JOIN absensitrn t ON a.`NoAbsen`=t.`NoAbsen` AND s.`ShiftDate`=DATE(t.`PresensiTime`)
				WHERE dp.departemen_name LIKE '%$departemen_name%' and s.ShiftDate between '$tgldari' AND '$tglsampai'
				GROUP BY s.`KdEmployee`
				ORDER BY e.employee_name";
				
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
	
	
	function datediff($tgl2, $tgl1){
		$sql = "SELECT DATEDIFF('$tgl2','$tgl1') as hari";
		$query = $this->db->query($sql);
        $result = $query->result_array();
        return $result[0]['hari'];
	}
	
}

?>