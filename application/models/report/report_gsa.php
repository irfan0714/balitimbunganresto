<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_gsa extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		  
	}
	 function viewData($tgldari, $tglsampai) {
	 	/*$sql = "SELECT employee.employee_id, employee_name, religion, TIME(MIN(PresensiTime)) AS TimeIn, TIME(MAX(`PresensiTime`)) AS TimeOut, shiftdate as Tanggal, Shift
				FROM shift
				INNER JOIN employee ON employee.`employee_id` = shift.`KdEmployee`
				INNER JOIN absensiemployee ON shift.`KdEmployee`=absensiemployee.`employee_id`
				INNER JOIN employee_position ON employee.employee_id = employee_position.employee_id
				INNER JOIN hrd_departemen ON employee_position.departemen_id = hrd_departemen.departemen_id
				INNER JOIN jabatan ON  jabatan.`jabatan_id` = employee_position.`jabatan_id`
				LEFT JOIN absensitrn ON  absensitrn.`NoAbsen` = absensiemployee.`NoAbsen`
				WHERE  DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY employee_id, departemen_name, Tanggal
				ORDER BY employee_id, departemen_name, Tanggal";*/

		$sql ="
				SELECT 
				  gsa.`Tanggal`,
				  gsa.`employee_id`,
				  SUM(gsa.`beauty_tour`) AS beauty_tour,
				  SUM(gsa.`coffee_tour`) AS coffee_tour,
				  SUM(gsa.`chamber`) AS chamber,
				  SUM(gsa.`standby_oh`) AS standby_oh,
				  SUM(gsa.`standby_resto`) AS standby_resto,
				  SUM(gsa.`standby_be`) AS standby_be 
				FROM
				  gsa
				WHERE gsa.`Tanggal` BETWEEN '".$tgldari."' 
				  AND '".$tglsampai."' 
				  GROUP BY gsa.`Tanggal`,
  				  gsa.`employee_id` ;
			  ";
		//echo $sql;die;		
		$query = $this->db->query($sql);
        $result = $query->result_array();
        
        $arr = array();
       
        for ($i=0; $i<count($result); $i++){
        	$empid = $result[$i]['employee_id'];
        	$tgl = $result[$i]['Tanggal'];
        	//$timein = $result[$i]['TimeIn'];
        	//$timeout = $result[$i]['TimeOut'];
        	//$shift = $result[$i]['Shift'];
			//$data = array($timein,$timeout,$shift);
			$beauty_tour = $result[$i]['beauty_tour'];
			$coffee_tour = $result[$i]['coffee_tour'];
			$chamber = $result[$i]['chamber'];
			$standby_oh = $result[$i]['standby_oh'];
			$standby_resto = $result[$i]['standby_resto'];
			$standby_be = $result[$i]['standby_be'];
			$data = array($beauty_tour,$coffee_tour,$chamber,$standby_oh,$standby_resto,$standby_be);
			$arr[$empid][$tgl] = implode('*', $data);
			
		}
					
		return $arr;
    }
    
    function getListEmp($tgldari, $tglsampai) {
	 	/*$sql = "SELECT employee_name AS NamaKaryawan, `departemen_name`, employee.employee_id, religion, jabatan_name
				FROM shift
				INNER JOIN employee ON employee.`employee_id` = shift.`KdEmployee`
				INNER JOIN absensiemployee ON shift.`KdEmployee`=absensiemployee.`employee_id`
				INNER JOIN employee_position ON employee.employee_id = employee_position.employee_id
				INNER JOIN hrd_departemen ON employee_position.departemen_id = hrd_departemen.departemen_id
				INNER JOIN jabatan ON  jabatan.`jabatan_id` = employee_position.`jabatan_id`
				LEFT JOIN absensitrn ON  absensitrn.`NoAbsen` = absensiemployee.`NoAbsen`
				WHERE  DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY employee_id, departemen_name
				ORDER BY departemen_name, employee_name";*/
		
		$sql ="
				SELECT 
				  gsa.`employee_id`,
				  employee.`employee_name`,
				  gsa.`beauty_tour`,
				  gsa.`coffee_tour`,
				  gsa.`chamber`,
				  gsa.`standby_oh`,
				  gsa.`standby_resto`,
				  gsa.`standby_be` 
				FROM
				  gsa 
				  INNER JOIN employee 
				    ON gsa.`employee_id` = employee.employee_id 
				WHERE gsa.`Tanggal` BETWEEN '".$tgldari."' 
				  AND '".$tglsampai."' 
				  GROUP BY gsa.employee_id ORDER BY employee.employee_name ASC;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
    
	function datediff($tgl2, $tgl1){
		$sql = "SELECT DATEDIFF('$tgl2','$tgl1') as hari";
		$query = $this->db->query($sql);
        $result = $query->result_array();
        return $result[0]['hari'];
	}
	
	function jmlhari($tgl) {
	 	
		
		$sql ="
				SELECT LAST_DAY('".$tgl."') AS tgl;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }

}

/* End of file report_absensi.php */
/* Location: ./application/models/report_absensi.php */