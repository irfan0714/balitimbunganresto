<?php
class Report_piutangmodel extends CI_Model {

    function __construct()
	{
        parent::__construct();
    }
	
	function getDate()
	{
		$sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
		return $this->getRow($sql);
	}
	function getReport($date)
	{
		$sql = "SELECT h.`KdCustomer`, concat(COALESCE(s.Nama,''),COALESCE(t.Nama,'')) as Nama, h.`NoDokumen`,h.tipetransaksi, h.`TglTransaksi` AS Tanggal, h.`JatuhTempo`, h.`NilaiTransaksi`, h.`Sisa`+COALESCE(p.Bayar,0) AS SisaHutang FROM  
					piutang h
					LEFT JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
					LEFT JOIN tourtravel t ON h.`KdCustomer`=t.KdTravel
					LEFT JOIN (
					SELECT d.`NoFaktur`, SUM(d.`NilaiBayar`) AS Bayar
					FROM `pelunasan_piutang_header` h INNER JOIN `pelunasan_piutang_detail` d ON h.`NoTransaksi`=d.`NoTransaksi`
					WHERE h.`TglDokumen`>'$date' AND h.`Status`=1 AND d.`NilaiBayar`>0
					GROUP BY d.`NoFaktur`) p ON h.NoDokumen=p.NoFaktur
					WHERE h.TglTransaksi<='$date' AND h.tipetransaksi <>'P'
					HAVING SisaHutang<>0
					ORDER by h.`KdCustomer`, h.`NoDokumen`";
					
		return $this->getArrayResult($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>