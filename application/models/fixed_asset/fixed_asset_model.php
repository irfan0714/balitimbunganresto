<?php
class Fixed_asset_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getTypeAsset(){
		$sql="SELECT * FROM `type_assets` WHERE Normal_Asset='1' ORDER BY KdTypeAsset ASC;";
		return $this->getArrayResult($sql);
	}
	
	function getTypeAssetOtherAsset(){
		$sql="SELECT * FROM `type_assets` WHERE 1 ORDER BY KdTypeAsset ASC;";
		return $this->getArrayResult($sql);
	}
	
	function getEmployee(){
		$sql="SELECT * FROM `employee` ORDER BY employee_name ASC;";
		return $this->getArrayResult($sql);
	}
	
	function getOtorisasi($user){
		$sql="SELECT * FROM `otorisasi_user` a WHERE a.UserName='$user' AND a.Tipe='fixed_asset';";
		$cek = $this->NumResult($sql);
		if($cek>0){
			$hasil = "Y";
		}else{
			$hasil = "N";
		}
		
		return $hasil;
	}
	
	function getItemAsset(){
		$sql="SELECT * FROM `item_asset` ORDER BY abjad_item_asset ASC;";
		return $this->getArrayResult($sql);
	}
	
	function getCodeItemAsset(){
		$sql="SELECT * FROM `code_assets` ORDER BY Code ASC;";
		return $this->getArrayResult($sql);
	}
	
	function num_list_asset_row($arrSearch){
		$mylib = new globallib();

        $sql = "
            SELECT
			  CONCAT(a.`NoAsset`,'.',a.`Departemen`,'.',a.`Lokasi`) AS NoAsset,
			  a.`Departemen`,
			  b.`NamaDepartemen`,
			  a.`Lokasi`,
			  c.`Lokasi`,
			  a.`nama_asset`,
			  a.`Keterangan`,
			  a.`filegambar`,
			  a.`Pegawai`,
			  e.`employee_name`
			FROM
			  `master_fixed_asset` a 
			  INNER JOIN departemen_fa b ON a.`Departemen`=b.`KdDepartemen`
			  INNER JOIN Lokasi c ON a.`Lokasi` = c.`KdLokasi`
			  INNER JOIN employee e ON e.`employee_id` = a.`Pegawai`
			ORDER BY a.`AddDate` ASC;
		";
        return $this->NumResult($sql);
	}
	
	function getListAsset($limit,$offset,$arrSearch){
		$mylib = new globallib();
		
		if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "a.NoAsset";
                $arr_keyword[1] = "a.nama_asset";
                $arr_keyword[2] = "b.NamaDepartemen";
                $arr_keyword[3] = "c.Lokasi";
                $arr_keyword[4] = "e.employee_name";
                $arr_keyword[5] = "a.Keterangan";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }


        }
        
		$sql="
			SELECT
			  a.`NoAsset` AS No_Asset,
			  CONCAT(a.`NoAsset`,'.',a.`Departemen`,'.',a.`Lokasi`) AS NoAsset,
			  a.`Departemen`,
			  b.`NamaDepartemen`,
			  a.`Lokasi` AS id_lokasi,
			  c.`Lokasi`,
			  a.`nama_asset`,
			  a.`Keterangan`,
			  a.`filegambar`,
			  a.`Pegawai`,
			  a.`Other_Asset`,
			  e.`employee_name`,
			  a.`Sts_accounting`
			FROM
			  `master_fixed_asset` a 
			  INNER JOIN departemen_fa b ON a.`Departemen`=b.`KdDepartemen`
			  INNER JOIN Lokasi c ON a.`Lokasi` = c.`KdLokasi`
			  LEFT JOIN employee e ON e.`employee_id` = a.`Pegawai`
			WHERE 1
			" . $where_keyword . "
			ORDER BY a.`AddDate` ASC
			LIMIT $offset,$limit" ;

		return $this->getArrayResult($sql);
	}
	
	function getDataCodeItemAsset($itemasset)
	{
    	$sql = "
    			SELECT * FROM `code_assets` WHERE abjad_item_asset='$itemasset' ORDER BY `Code` ASC;
    		   ";
        return $this->db->query($sql);
    }	
    

    function getDataTypeAsset($typeasset,$code_asset)
	{
    	
    	if($code_asset=="B6" OR $code_asset=="B10" OR $code_asset=="E2"){
			$where = "";
		}else if($code_asset=="B7" OR $code_asset=="B9" OR $code_asset=="E1" OR $code_asset=="E5" OR $code_asset=="G4"){
			$where = " AND a.Pilihan='N'";
		}
		else{
			$where = " AND a.Pilihan='Y'";
		}
		
    	
    	$sql = "
    			SELECT
				  *
				FROM
				  `sub_type_assets` a 
				WHERE a.KdTypeAsset = '$typeasset'
				  AND a.Normal_Asset = '1'
				  $where
				ORDER BY a.`KdSubTypeAsset` ASC;
    		   ";
    	
        return $this->db->query($sql);
    }
    
    function getDataTypeAssetOtherAsset($typeasset)
	{
    	$sql = "
    			SELECT * FROM `sub_type_assets` WHERE KdTypeAsset='$typeasset'  ORDER BY `KdSubTypeAsset` ASC;
    		   ";
        return $this->db->query($sql);
    }
    
    function getGolPajakModel($code_asset)
	{
    	$sql = "
    			SELECT * FROM `code_assets` WHERE Code='$code_asset';
    		   ";
        return $this->getRow($sql);
    }
    
    function getTypeAssetModel($code_asset)
	{
    	$sql = "
    			SELECT
				  a.`KdTypeAsset`,
				  b.`NamaTypeAsset`
				FROM
				  `code_assets` a
				  INNER JOIN type_assets b
				    ON a.`KdTypeAsset` = b.`KdTypeAsset`
				WHERE CODE = '$code_asset';
    		   ";
 
        return $this->db->query($sql);
    }
    
    function getGolPajakOtherAssetModel($KdTypeAsset,$KdSubTypeAsset)
	{
    	$sql = "
    			SELECT
				  *
				FROM
				  `sub_type_assets` a
				WHERE a.`KdTypeAsset` = '$KdTypeAsset'
				  AND a.`KdSubTypeAsset` = '$KdSubTypeAsset'
				  AND a.Other_Asset='1';
    		   ";
        //return $this->getRow($sql);
        return $this->getArrayResult($sql);
    }
    
    function getLokasiFixedAsset(){
		$sql="SELECT * FROM `lokasi` ORDER BY Lokasi ASC;";
		return $this->getArrayResult($sql);
	}
	
	function getDepartemenFixedAsset(){
		$sql="SELECT * FROM `departemen_fa` ORDER BY NamaDepartemen ASC;";
		return $this->getArrayResult($sql);
	}
	
	function getDataAsset($noasset){
    	$sql = "SELECT * FROM master_fixed_asset a WHERE a.NoAsset='$noasset'";
        return $this->getRow($sql);
	}
	
	function getSubTypeAsset(){
    	$sql = "SELECT * FROM sub_type_assets a WHERE 1 ORDER BY NamaSubTypeAsset ASC";
        return $this->getArrayResult($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>