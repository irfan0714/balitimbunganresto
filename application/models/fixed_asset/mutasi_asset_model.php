	<?php
	class Mutasi_asset_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		
		function getBarang(){
			$sql = "SELECT * FROM masterbarang";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getSubDivisi(){
			$sql = "SELECT * FROM subdivisi";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getProduct($SubDivisi){
			$sql = "SELECT * FROM masterbarang WHERE KdSubDivisi = '".$SubDivisi."' ";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getBarcode($KdPro){
			$sql = "SELECT NamaLengkap, NamaInitial, Barcode1, Harga1c FROM masterbarang WHERE PCode = '".$KdPro."' ";
			$query = $this->db->query($sql);
			return $query->result_array();
		}

		function getDepartement(){
			$sql = "SELECT * FROM departemen_fa ORDER BY KdDepartemen ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}

		function getLokasi(){
			$sql = "SELECT * FROM lokasi ORDER BY KdLokasi ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getEmployee(){
			$sql = "SELECT * FROM employee ORDER BY employee_name ASC";
			$query = $this->db->query($sql);
			return $query->result_array();
		}

		function getDataBuku(){
			$sql = "SELECT DISTINCT(a.`data_buku`) AS data_buku FROM fixed_assets a WHERE 1 ORDER BY a.`data_buku` ASC;";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getSortDataBuku(){
			$sql = "SELECT DISTINCT(SUBSTR(a.`data_buku`,1,1)) AS sort_data_buku FROM fixed_assets a WHERE 1 ORDER BY a.`data_buku` ASC;";
			$query = $this->db->query($sql);
			return $query->result_array();
		}

		function getDataAssest(){
	        $sql = "SELECT a.* FROM master_fixed_asset a WHERE 1 ORDER BY a.`nama_asset` ASC;";
			//echo $sql;
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getDataAssestDetail($Departement,$Lokasi,$Nama){

			if($Departement=='0' OR $Departement==''){
			   $Departement="";
			}else{
			   $Departement=" AND a.`Departemen`='$Departement'";
			}

			if($Lokasi=='0' OR $Lokasi==''){
			   $Lokasi="";
			}else{
			   $Lokasi=" AND a.`Lokasi`='$Lokasi'";
			}

			if($Nama=='0' OR $Nama==''){
			   $Nama="";
			}else{
			   $Nama=" AND a.`NoAsset` LIKE '%$Nama%'";
			}

			
			//$sql = "SELECT a.* FROM master_fixed_asset a WHERE 1 $Departement $Lokasi $DataBuku $Nama $Sort_Data_Buku ORDER BY SUBSTR(a.`data_buku`,1,1) ASC,SUBSTR(a.`data_buku`,3,3) ASC, a.`nama_assets` ASC;";
			$sql = "SELECT a.* FROM master_fixed_asset a WHERE 1 $Departement $Lokasi $Nama ORDER BY a.`nama_asset` ASC;";
			//echo $sql;
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		
		function getMutasiAssestDetail($noasset){

			$sql = "SELECT
					  a.*,
					  b.NamaDepartemen,
					  c.Lokasi AS NamaLokasi,
					  d.nama_asset,
					  e.employee_name
					FROM
					  mutasi_fixed_asset a
					  INNER JOIN departemen_fa b
					    ON a.`Departemen` = b.`KdDepartemen`
					  INNER JOIN Lokasi c
					    ON a.`Lokasi` = c.`KdLokasi`
					  INNER JOIN master_fixed_asset d
					  ON a.NoAsset = d.NoAsset
					  LEFT JOIN employee e
					  ON a.pic = e.employee_id
					WHERE 1
					  AND a.NoAsset = '$noasset'
					ORDER BY a.tgl_mutasi DESC;";
			
			$query = $this->db->query($sql);
			return $query->result_array();
		}

	}
	?>