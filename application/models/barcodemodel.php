<?php
class Barcodemodel extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function getBarang(){
		$sql = "SELECT * FROM masterbarang";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getSubDivisi(){
		$sql = "SELECT * FROM subdivisi";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getProduct($SubDivisi){
		$sql = "SELECT * FROM masterbarang WHERE KdSubDivisi = '".$SubDivisi."' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function getBarcode($KdPro){
		$sql = "SELECT NamaLengkap, NamaInitial, Barcode1, Harga1c FROM masterbarang WHERE PCode = '".$KdPro."' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}
?>