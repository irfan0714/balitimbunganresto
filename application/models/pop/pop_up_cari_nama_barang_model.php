<?php
class Pop_up_cari_nama_barang_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getbarangList($barang)
	{
        	
		$sql = "
			                SELECT 
							  masterbarang.PCode,
							  masterbarang.NamaLengkap,
							  divisi.KdDivisi,
							  divisi.NamaDivisi 
							FROM
							  masterbarang 
							  INNER JOIN divisi 
							    ON masterbarang.KdDivisi = divisi.KdDivisi 
							WHERE 1 
							  AND (
							    masterbarang.NamaLengkap LIKE '%$barang%'
							  ) 
							  AND masterbarang.NamaLengkap != '$barang' 
							ORDER BY masterbarang.NamaLengkap ASC ;
		";
		//echo $sql;
        //echo "<hr/>";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>