<?php
class Barangownermodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getbarangList($num, $offset,$id,$with,$owner)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "
				select b.*,NamaBrand,NamaKategori,NamaSatuanSt from(
				SELECT PCode,NamaStruk,SatuanSt,Satuan1,Konv1st,Harga1c,Harga1t,Satuan2,Konv2st,Harga2c,Harga2t,Satuan3,Konv3st,Harga3c,Harga3t,
				KdKategori,KdBrand
				FROM masterbarang Where status='A' and (namalengkap like '%$owner%' or pcode like '%$owner%') $clause Limit $offset,$num
				) b
				left join
				(
				select KdBrand,NamaBrand from brand
				) br
				on br.KdBrand=b.KdBrand
				left join
				(
				select KdKategori,NamaKategori from kategori
				) kt
				on kt.KdKategori=b.KdKategori
				left join
				(
				select KdSatuan,NamaSatuan as NamaSatuanSt from satuan
				) st	
				on st.KdSatuan=b.SatuanSt";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_barang_row($id,$with,$owner){
     	$clause="";
     	if($id!=''){
			$clause = " and $with like '%$id%'";
		}
		$sql = "select b.*,NamaBrand,NamaKategori from(
				SELECT PCode,NamaStruk,Satuan1,Harga1c,Harga1t,Satuan2,Harga2c,Harga2t,Satuan3,Harga3c,Harga3t,
				KdKategori,KdBrand FROM masterbarang Where status='A' and (namalengkap like '%$owner%' or pcode like '%$owner%') $clause
				) b
				left join
				(
				select KdBrand,NamaBrand from brand
				) br
				on br.KdBrand=b.KdBrand
				left join
				(
				select KdKategori,NamaKategori from kategori
				) kt
				on kt.KdKategori=b.KdKategori";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getSatuan($pcode)
	{
		$sql = "SELECT Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				SatuanSt,(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt
				from masterbarang where PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
    }
}
?>