<?php
class Pop_up_pr_marketing_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getProposalList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        /*$arr_keyword[0] = "masterbarang.NamaLengkap";
				$arr_keyword[1] = "masterbarang.NamaStruk";
				$arr_keyword[2] = "masterbarang.NamaInitial";
				$arr_keyword[3] = "masterbarang.Barcode1";
				$arr_keyword[4] = "divisi.NamaDivisi";*/
				$arr_keyword[0] = "pr_marketing.NoDokumen";
				$arr_keyword[1] = "pr_marketing.NoProposal";
				$arr_keyword[2] = "proposal.NamaProposal";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}
		}
        	
		$sql = "
			SELECT * FROM pr_marketing LEFT JOIN proposal ON proposal.NoProposal = pr_marketing.NoProposal 
			WHERE pr_marketing.Status='1' AND pr_marketing.FlgKonfirmPO='0'".$where_keyword."
		";
		//echo $sql;
        //echo "<hr/>";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_proposal_row($arrSearch)
    {		
        $mylib = new globallib();
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "masterbarang.NamaLengkap";
				$arr_keyword[1] = "masterbarang.NamaStruk";
				$arr_keyword[2] = "masterbarang.NamaInitial";
				$arr_keyword[3] = "masterbarang.Barcode1";
				$arr_keyword[4] = "divisi.NamaDivisi";
				$arr_keyword[5] = "subdivisi.NamaSubDivisi";
				$arr_keyword[6] = "kategori.NamaKategori";
				$arr_keyword[7] = "subkategori.NamaSubKategori";
				$arr_keyword[8] = "brand.NamaBrand";
				$arr_keyword[9] = "subbrand.NamaSubBrand";
		        $arr_keyword[10] = "masterbarang.PCode";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}
		}
		
		$sql = "
			SELECT * FROM pr_marketing LEFT JOIN proposal ON proposal.NoProposal = pr_marketing.NoProposal 
		";
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function getSearch($id)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id'";
		return $this->getRow($sql);
	}
	
	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }
    
	function getKategori()
	{
    	$sql = "SELECT KdKategori,NamaKategori FROM kategori ORDER BY kategori.NamaKategori ASC";
		return $this->getArrayResult($sql);
    }
    
	function getBrand()
	{
    	$sql = "SELECT KdBrand,NamaBrand FROM brand ORDER BY brand.NamaBrand ASC";
		return $this->getArrayResult($sql);
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>