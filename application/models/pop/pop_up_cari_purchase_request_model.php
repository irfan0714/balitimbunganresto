<?php
class Pop_up_cari_purchase_request_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getbarangList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        /*$arr_keyword[0] = "masterbarang.NamaLengkap";
				$arr_keyword[1] = "masterbarang.NamaStruk";
				$arr_keyword[2] = "masterbarang.NamaInitial";
				$arr_keyword[3] = "masterbarang.Barcode1";
				$arr_keyword[4] = "divisi.NamaDivisi";
				$arr_keyword[5] = "subdivisi.NamaSubDivisi";
				$arr_keyword[6] = "kategori.NamaKategori";
				$arr_keyword[7] = "subkategori.NamaSubKategori";
				$arr_keyword[8] = "brand.NamaBrand";
				$arr_keyword[9] = "subbrand.NamaSubBrand";
		        $arr_keyword[10] = "masterbarang.PCode";*/
				//$arr_keyword[0] = "a.NoDokumen";
		        
				//$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				//$where_keyword = $search_keyword;
				$where_keyword = "AND a.NoDokumen LIKE '%".$arrSearch["keyword"]."%' OR c.Keterangan LIKE '%".$arrSearch["keyword"]."%'";
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}
		}
        	
		$sql = "
			SELECT 
			      a.*,
				  c.*,
				  a.`Keterangan` AS Ket,
				  d.NamaDivisi  
			FROM
			  `trans_pr_header` a 
			  INNER JOIN gudang c 
			    ON a.`KdGudang` = c.`KdGudang` 
			  INNER JOIN divisi d 
			    ON a.`KdDivisi` = d.`KdDivisi`
			WHERE 1 
				AND a.`Status` = '1'
            	".$where_keyword."
            	".$where_gudang."
            	".$where_customer."    
            	".$where_status."    
            ORDER BY 
              a.`TglDokumen` DESC 
            Limit 
              $offset,$limit
		";
		
		//echo $sql;
        //echo "<hr/>";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    /*function num_barang_row($arrSearch)
    {		
        $mylib = new globallib();
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "masterbarang.NamaLengkap";
				$arr_keyword[1] = "masterbarang.NamaStruk";
				$arr_keyword[2] = "masterbarang.NamaInitial";
				$arr_keyword[3] = "masterbarang.Barcode1";
				$arr_keyword[4] = "divisi.NamaDivisi";
				$arr_keyword[5] = "subdivisi.NamaSubDivisi";
				$arr_keyword[6] = "kategori.NamaKategori";
				$arr_keyword[7] = "subkategori.NamaSubKategori";
				$arr_keyword[8] = "brand.NamaBrand";
				$arr_keyword[9] = "subbrand.NamaSubBrand";
		        $arr_keyword[10] = "masterbarang.PCode";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}
		}
		
		$sql = "
			SELECT 
			  masterbarang.PCode,
			  masterbarang.NamaLengkap,
			  masterbarang.NamaStruk,
			  masterbarang.NamaInitial,
			  masterbarang.Barcode1,
			  masterbarang.KdDivisi,
			  divisi.NamaDivisi,
			  masterbarang.KdSubDivisi,
			  subdivisi.NamaSubDivisi,
			  masterbarang.KdKategori,
			  kategori.NamaKategori,
			  masterbarang.KdSubKategori,
			  subkategori.NamaSubKategori,
			  masterbarang.KdBrand,
			  brand.NamaBrand,
			  masterbarang.KdSubBrand,
			  subbrand.NamaSubBrand 
			FROM
			  masterbarang 
			  LEFT JOIN divisi 
			    ON masterbarang.KdDivisi = divisi.KdDivisi 
			  LEFT JOIN subdivisi 
			    ON masterbarang.KdSubDivisi = subdivisi.KdSubDivisi 
			  LEFT JOIN kategori 
			    ON masterbarang.KdKategori = kategori.KdKategori 
			  LEFT JOIN subkategori 
			    ON masterbarang.KdSubKategori = subkategori.KdSubKategori 
			  LEFT JOIN brand 
			    ON masterbarang.KdBrand = brand.KdBrand 
			  LEFT JOIN subbrand 
			    ON masterbarang.KdSubBrand = subbrand.KdSubBrand 
			WHERE 
				1 
				AND masterbarang.Status = 'A'
				".$where_keyword."
				".$where_divisi."
				".$where_kategori."
				".$where_brand."
		";
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}*/
	
	function num_barang_row($arrSearch)
    {
    	$sql="SELECT * FROM trans_pr_header a WHERE 1 ORDER BY a.NoDokumen ASC";
    	
    	$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function getSearch($id)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id'";
		return $this->getRow($sql);
	}
	
	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }
    
	function getKategori()
	{
    	$sql = "SELECT KdKategori,NamaKategori FROM kategori ORDER BY kategori.NamaKategori ASC";
		return $this->getArrayResult($sql);
    }
    
	function getBrand()
	{
    	$sql = "SELECT KdBrand,NamaBrand FROM brand ORDER BY brand.NamaBrand ASC";
		return $this->getArrayResult($sql);
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>