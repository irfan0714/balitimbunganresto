<?php
class Tordermodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getorderList($num, $offset,$id,$with,$thnbln)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause=" and h.status<>'B' and DATE_FORMAT(h.TglDokumen,'%Y%m')<='$thnbln'";
		if($id!=""){
			$clause = " AND $with like '%$id%' and h.status<>'B' and DATE_FORMAT(h.TglDokumen,'%Y%m')<='$thnbln'";
		}
    	$sql = "
		SELECT h.*,COUNT(d.PCode) as Jmlh,c.Nama
		from trans_terima_detail d,trans_terima_header h, supplier c
		where h.NoDokumen=d.NoDokumen and h.KdSupplier=c.KdSupplier and h.Status<>'B'
		and d.FlagKonfirmasi='T' 
		$clause 
		group by h.NoDokumen
		order by cast(h.NoDokumen as unsigned) desc Limit $offset,$num";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getDetail($id)
	{
		$sql = "
		select brg.*,NamaBrand,NamaKategori from(
			select d.*,if(d.PCode=b.PCode,'pcode','bar') as jenis,NamaLengkap,b.PCode as PCodeBarang
			from trans_terima_detail d,trans_terima_header h,masterbarang b
			where h.NoDokumen='$id' and h.NoDokumen=d.NoDokumen
			and d.FlagKonfirmasi='T' and (b.PCode = d.PCode)
			order by Counter desc
		)brg
		LEFT JOIN
		(
		SELECT KdBrand,NamaBrand FROM brand
		)s
		ON s.KdBrand=brg.KdBrand
		LEFT JOIN
		(
		SELECT KdKategori,NamaKategori FROM kategori
		)k
		ON k.KdKategori=brg.KdKategori";
	
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
    
    function num_order_row($id,$with,$thnbln){
     	$clause=" and h.status<>'B' and DATE_FORMAT(h.TglDokumen,'%Y%m')<='$thnbln'";
     	if($id!=''){
			$clause = " and $with like '%$id%' and h.status<>'B' and DATE_FORMAT(h.TglDokumen,'%Y%m')<='$thnbln'";
		}
		$sql = "SELECT h.NoDokumen
		from trans_terima_header h
		where h.FlagKonfirmasi='T' $clause ";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getSatuan($pcode)
	{
		$sql = "SELECT SatuanBl as Satuan0,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0,
		        Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where PCode='$pcode'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>