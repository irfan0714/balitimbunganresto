<?php
class Pop_new_temporary_item_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

	function getMenuList($sid_detail,$limit,$offset,$arrSearch)
	{
        $mylib = new globallib();

	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }

        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.NamaLengkap";
				$arr_keyword[1] = "a.Barcode1";
		        $arr_keyword[2] = "a.PCode";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}

		/*$sql = "
			SELECT
			  *
			FROM
			  masterbarang_pos
			WHERE
				1
				AND 1
				".$where_keyword."
			ORDER BY
			  masterbarang_pos.NamaLengkap ASC
			LIMIT
			  $offset,$limit
		";*/
		$sql="
		SELECT
*
FROM
trans_order_detail d
INNER JOIN `masterbarang_touch` m
ON d.`PCode` = m.PCode
WHERE d.`NoTrans` = '$sid_detail'
AND m.Harga1c = 0;
				;
			 ";

		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_menu_row($arrSearch)
    {
        $mylib = new globallib();

        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "masterbarang_pos.NamaLengkap";
				$arr_keyword[1] = "masterbarang_pos.Barcode1";
		        $arr_keyword[2] = "masterbarang_pos.PCode";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}

		$sql = "
			SELECT
			  *
			FROM
			  masterbarang_pos
			WHERE
				1
				".$where_keyword."
		";
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function getSearch($id)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id'";
		return $this->getRow($sql);
	}

	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }

	function getKategori()
	{
    	$sql = "SELECT KdKategori,NamaKategori FROM kategori ORDER BY kategori.NamaKategori ASC";
		return $this->getArrayResult($sql);
    }

	function getBrand()
	{
    	$sql = "SELECT KdBrand,NamaBrand FROM brand ORDER BY brand.NamaBrand ASC";
		return $this->getArrayResult($sql);
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>
