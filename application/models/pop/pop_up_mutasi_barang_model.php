<?php
class Pop_up_mutasi_barang_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getmutasiList($pcode,$tglawal,$tglakhir,$gudang)
	{
        	
		$sql = "
			SELECT
			  a.KodeBarang,
			  b.`NamaLengkap`
			FROM
			  `mutasi` a 
			  INNER JOIN masterbarang b 
			    ON a.`KodeBarang` = b.`PCode` 
			WHERE a.`KodeBarang` = '".$pcode."' ;
		";
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
    
    function getDetailList($pcode,$tglawal,$tglakhir,$gudang,$tipe)
	{
        if($tipe=="gdg"){
		$sql = "
			SELECT 
			  a.`NoTransaksi`,
			  a.`Tanggal`,
			  b.`NamaLengkap`,
			  a.KdTransaksi,
			  IF( a.`Jenis`='I', a.`Qty`, 0) AS Masuk,
			  IF( a.`Jenis`='O', a.`Qty`, 0) AS Keluar
			FROM
			  `mutasi` a 
			  INNER JOIN masterbarang b 
			    ON a.`KodeBarang` = b.`PCode` 
			WHERE a.`KodeBarang` = '$pcode' AND a.`Gudang`='$gudang'
			  AND a.`Tanggal` BETWEEN '$tglawal' 
			  AND '$tglakhir'  ORDER BY a.Tanggal,a.Jenis,a.`NoTransaksi` ASC;
		";
		}else{
		$sql = "
			SELECT 
			  a.`NoTransaksi`,
			  a.`Tanggal`,
			  b.`NamaLengkap`,
			  a.KdTransaksi,
			  IF( a.`Jenis`='I', a.`Qty`, 0) AS Masuk,
			  IF( a.`Jenis`='O', a.`Qty`, 0) AS Keluar
			FROM
			  `mutasi` a 
			  INNER JOIN masterbarang b 
			    ON a.`KodeBarang` = b.`PCode` 
			  inner join gudang g on a.Gudang=KdGudang
			  inner join subdivisi dv on g.KdSubdivisi=dv.KdSubDivisi
			WHERE a.`KodeBarang` = '$pcode' AND dv.`KdDivisi`='$gudang'
			  AND a.`Tanggal` BETWEEN '$tglawal' 
			  AND '$tglakhir'  ORDER BY a.Tanggal,a.Jenis,a.`NoTransaksi` ASC;
		";	
		}
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
	 
	function getDetailList2($empid,$thn, $bln)
	{
		$sql = "
			SELECT 
			  gsa.`Tanggal`,
			  gsa_detail.gsa_id,
			  gsa_detail.`NoStiker`,
			  gsa.`employee_id`,
			  employee.`employee_name`,
			  gsa.`beauty_tour`,
			  gsa.`coffee_tour`,
			  gsa.`chamber`,
			  gsa.`standby_oh`,
			  gsa.`standby_resto`,
			  gsa.`standby_be` 
			FROM
			  gsa 
			  INNER JOIN gsa_detail 
				ON gsa.`gsa_id` = gsa_detail.`gsa_id` 
			  INNER JOIN employee 
				ON gsa.`employee_id` = employee.employee_id 
			WHERE YEAR(gsa.`Tanggal`) = '".$thn."' AND MONTH(gsa.`Tanggal`) = '".$bln."'
			  AND gsa_detail.`NoStiker` != '' 
			  AND gsa.`employee_id` = '".$empid."'
			  GROUP BY gsa.`gsa_id` ;
		";
		//echo $sql;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	 function getTotalList($nodok)
	{
		$sql = "
			SELECT 
			  SUM(a.`Harga`*a.`Qty`) AS subtotal 
			FROM
			  `trans_terima_detail` a 
			  INNER JOIN `masterbarang` b 
				ON a.`PCode` = b.`PCode` 
			  INNER JOIN `satuan` c 
				ON a.`satuan` = c.`KdSatuan` 
			WHERE a.`NoDokumen` = '".$nodok."' ;
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	function jmlhari($tgl) {
	 	
		
		$sql ="
				SELECT LAST_DAY('".$tgl."') AS tgl;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
    
    function saldo($pcode,$thn,$gudang,$tabel_field,$tabel_field2){
		$sql="
			SELECT 
			  ROUND(AVG( a.".$tabel_field."),0) AS saldo_awal,
			  ROUND(AVG( a.".$tabel_field2."),0) AS saldo_akhir
			FROM
			  `stock` a 
			WHERE a.`Tahun` = '".$thn."' 
			  AND a.`KdGudang` = '".$gudang."' 
			  AND a.`PCode`='".$pcode."';
			";
			
	    $query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
	}
	
	function saldodvs($pcode,$thn,$divisi,$tabel_field,$tabel_field2){
		$sql="
			SELECT 
			   ROUND(SUM(a.".$tabel_field."),0) AS saldo_awal,
			   ROUND(SUM(a.".$tabel_field2."),0) AS saldo_akhir 
			FROM
			  `stock` a 
			  INNER JOIN masterbarang b 
			    ON a.PCode = b.PCode
			  INNER JOIN gudang d
			    ON a.KdGudang = d.KdGudang
			  INNER JOIN subdivisi e
			    ON d.KdSubDivisi = e.KdSubDivisi
			WHERE a.`Tahun` = '".$thn."'
			  AND e.`KdDivisi` = '".$divisi."' 
			  AND a.`PCode` = '".$pcode."' ;
			";
				
	    $query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
	}
	    
}
?>