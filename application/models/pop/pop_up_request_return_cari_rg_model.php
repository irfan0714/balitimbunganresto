<?php
class Pop_up_request_return_cari_rg_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getbarangList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_supplier = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_terima_header.NoDokumen";
				$arr_keyword[1] = "gudang.Keterangan";
				$arr_keyword[2] = "trans_terima_header.PoNo";
				$arr_keyword[3] = "supplier.Nama";
				$arr_keyword[4] = "trans_terima_header.NoSuratJalan";
				$arr_keyword[5] = "trans_terima_header.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND gudang.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["supplier"]!="")
			{
				$where_supplier = "AND trans_terima_header.KdSupplier = '".$arrSearch["supplier"]."'";	
			}
		}
        	
		$sql = "
			SELECT 
    		  DATE_FORMAT(trans_terima_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_terima_header.NoDokumen,
			  trans_terima_header.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_terima_header.PoNo,
			  trans_terima_header.KdSupplier,
			  supplier.Nama AS nama_supplier,
			  trans_terima_header.NoSuratJalan,
			  trans_terima_header.Keterangan 
			FROM
			  trans_terima_header 
			  INNER JOIN gudang 
			    ON trans_terima_header.KdGudang = gudang.KdGudang 
			  INNER JOIN supplier 
			    ON trans_terima_header.KdSupplier = supplier.KdSupplier 
			WHERE 
			  1
              ".$where_keyword."
              ".$where_gudang."
              ".$where_supplier." 
			ORDER BY 
			  trans_terima_header.TglDokumen DESC,
			  trans_terima_header.NoDokumen DESC
			LIMIT
              $offset,$limit
		";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_barang_row($arrSearch)
    {		
        $mylib = new globallib();
        
        $where_keyword="";
        $where_gudang="";
        $where_supplier = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_terima_header.NoDokumen";
				$arr_keyword[1] = "gudang.Keterangan";
				$arr_keyword[2] = "trans_terima_header.PoNo";
				$arr_keyword[3] = "supplier.Nama";
				$arr_keyword[4] = "trans_terima_header.NoSuratJalan";
				$arr_keyword[5] = "trans_terima_header.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND gudang.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["supplier"]!="")
			{
				$where_supplier = "AND trans_terima_header.KdSupplier = '".$arrSearch["supplier"]."'";	
			}
		}
		
		$sql = "
			SELECT 
    		  DATE_FORMAT(trans_terima_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_terima_header.NoDokumen,
			  trans_terima_header.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_terima_header.PoNo,
			  trans_terima_header.KdSupplier,
			  supplier.Nama AS nama_supplier,
			  trans_terima_header.NoSuratJalan,
			  trans_terima_header.Keterangan 
			FROM
			  trans_terima_header 
			  INNER JOIN gudang 
			    ON trans_terima_header.KdGudang = gudang.KdGudang 
			  INNER JOIN supplier 
			    ON trans_terima_header.KdSupplier = supplier.KdSupplier 
			WHERE 
			  1
              ".$where_keyword."
              ".$where_gudang."
              ".$where_supplier." 
		";
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getGudang()
	{
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
		return $this->getArrayResult($sql);
    }
	
	function getSupplier()
	{
    	$sql = "
    		SELECT 
			  KdSupplier,
			  Nama,
			  Contact,
			  Alamat,
			  Kota,
			  Telepon,
			  AlamatKirim,
			  KotaKirim 
			FROM
			  supplier 
			WHERE 
			  1 
			ORDER BY 
			  supplier.KdSupplier DESC,
			  supplier.Nama ASC
    	";
    	return $this->getArrayResult($sql);
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>