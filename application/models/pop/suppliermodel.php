<?php
class Suppliermodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getsupplierList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	$sql = "SELECT KdSupplier,Nama,Alamat,Kota,Payment,TOP,LimitKredit,LimitFaktur,PPn,KdGroupext FROM supplier $clause order by KdSupplier Limit $offset,$num";
		//echo "1".$sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getsupplierList2($num,$offset,$id,$with1)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($with1!=""){
			$clause = " where nama like '%$with1%' or KdSupplier like '%$with1%'";
		}
    	$sql = "SELECT KdSupplier,Nama,Alamat,Kota,Payment,TOP,LimitKredit,LimitFaktur,PPn,KdGroupext FROM supplier $clause order by KdSupplier Limit $offset,$num";
		//echo "2".$sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_supplier_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT KdSupplier FROM supplier $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function num_supplier_row2($id,$with1){
     	$clause="";
     	if($with1!=""){
			$clause = " where nama like '%$with1%' or KdSupplier like '%$with1%'";
		}
		$sql = "SELECT KdSupplier FROM supplier $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>