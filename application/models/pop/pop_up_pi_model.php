<?php
class Pop_up_pi_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getrgList($nodok)
	{
        	
		$sql = "
			SELECT 
			  a.`NoFaktur`,
			  DATE_FORMAT(a.`Tanggal`, '%d-%m-%Y') AS Tanggal,
			  DATE_FORMAT(a.`JatuhTempo`, '%d-%m-%Y') AS JatuhTempo,
			  b.`Nama`,
			  a.PPN
			FROM
			  `invoice_pembelian_header` a 
			  INNER JOIN supplier b 
				ON a.`KdSupplier` = b.`KdSupplier` 
			WHERE a.`NoFaktur` = '".$nodok."' ;
		";
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
    
    function getDetailList($nodok)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  `invoice_pembelian_detail` a 
			  INNER JOIN `masterbarang` b 
				ON a.`KdBarang` = b.`PCode` 
			  INNER JOIN `satuan` c 
				ON a.`satuan` = c.`KdSatuan` 
			WHERE a.`NoFaktur` = '".$nodok."' ;
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	function getTotalList($nodok)
	{
		$sql = "
			SELECT 
			  SUM(a.Harga * a.Qty) AS subtotal 
			FROM
			  `invoice_pembelian_detail` a 
			  INNER JOIN `masterbarang` b 
				ON a.`KdBarang` = b.`PCode` 
			  INNER JOIN `satuan` c 
				ON a.`satuan` = c.`KdSatuan` 
			WHERE a.`NoFaktur` = '".$nodok."' ;
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
    
}
?>