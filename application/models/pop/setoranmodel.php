<?php
class Setoranmodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getUserList($num, $offset,$id,$with,$tgl)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "
SELECT user.*,a.*,employee.`employee_name` AS Nama
                FROM
                (SELECT employee_nik AS Kode,UserName FROM   `user`
                WHERE UserLevel = '10' )`user`
			INNER JOIN employee ON employee.`employee_nik`=user.kode
                  INNER JOIN
                    (SELECT
                      SUM(TotalNilai) AS ttlNilai, SUM(TotalBayar) AS ttlBayar,
                      SUM(Kembali)AS ttlKembali, SUM(Tunai) AS ttlTunai,
                      SUM(KKredit) AS ttlKKredit,
                      SUM(KDebit) AS ttlKDebit, SUM(Voucher) AS ttlVoucher,Kasir
                    FROM
                      transaksi_header
                    WHERE tanggal = '$tgl' $clause
                    GROUP BY Kasir)a ON a.Kasir = User.`UserName`
                 order by user.UserName  Limit $offset,$num
              ";
		//echo "1".$sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_user_row($id,$with,$tgl){
     	$clause="";
     	if($id!=''){
			$clause = " and $with like '%$id%'";
		}
		$sql = "SELECT user.*,a.*
                FROM
                (SELECT employee_nik,UserName FROM   `user`
                WHERE UserLevel = '10' )`user`
                  INNER JOIN
                    (SELECT
                      SUM(TotalNilai) AS ttlNilai, SUM(TotalBayar) AS ttlBayar,
                      SUM(Kembali)AS ttlKembali, SUM(Tunai) AS ttlTunai,
                      SUM(KKredit) AS ttlKKredit,
                      SUM(KDebit) AS ttlKDebit, SUM(Voucher) AS ttlVoucher,Kasir
                    FROM
                      transaksi_header
                    WHERE tanggal = '$tgl' $clause
                    GROUP BY Kasir)a ON a.Kasir = User.`UserName`
                 order by user.UserName";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>