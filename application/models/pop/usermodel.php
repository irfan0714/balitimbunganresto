<?php
class Usermodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getUserList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
    	$sql = "SELECT employee.employee_nik as Kode,employee_name as Nama  FROM USER
                INNER JOIN employee ON employee.employee_nik=user.employee_nik
                WHERE UserLevel='10'
                $clause order by employee.employee_nik  Limit $offset,$num
              ";
		//echo "1".$sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_user_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " and $with like '%$id%'";
		}
		$sql = "SELECT employee.employee_nik,employee_name FROM USER
                INNER JOIN employee ON employee.employee_nik=user.employee_nik
                WHERE UserLevel='10' $clause order by employee.employee_nik";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>