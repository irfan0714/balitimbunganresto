<?php
class Pop_up_po_marketing_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getPoMarketingList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        /*$arr_keyword[0] = "masterbarang.NamaLengkap";
				$arr_keyword[1] = "masterbarang.NamaStruk";
				$arr_keyword[2] = "masterbarang.NamaInitial";
				$arr_keyword[3] = "masterbarang.Barcode1";
				$arr_keyword[4] = "divisi.NamaDivisi";
				$arr_keyword[5] = "subdivisi.NamaSubDivisi";
				$arr_keyword[6] = "kategori.NamaKategori";
				$arr_keyword[7] = "subkategori.NamaSubKategori";
				$arr_keyword[8] = "brand.NamaBrand";
				$arr_keyword[9] = "subbrand.NamaSubBrand";
		        $arr_keyword[10] = "masterbarang.PCode";*/
				$arr_keyword[0] = "po_marketing.NoDokumen";
				$arr_keyword[1] = "pr_marketing.NoProposal";
				$arr_keyword[2] = "po_marketing.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}
		}
        	
		$sql = "
			SELECT 
			po_marketing.NoDokumen AS NoDokumen_,
			pr_marketing.NoProposal AS NoProposal_,
			po_marketing.Keterangan AS Keterangan_,
			po_marketing.KdSupplier,
			supplier.Nama
			FROM po_marketing INNER JOIN pr_marketing ON po_marketing.NoPr = pr_marketing.NoDokumen
			LEFT JOIN proposal ON proposal.NoProposal = pr_marketing.NoProposal 
			INNER JOIN supplier ON supplier.KdSupplier = po_marketing.KdSupplier
			WHERE 1 AND po_marketing.Status='1' AND po_marketing.`FlagPengiriman`='T'
			".$search_keyword ."
		";
		//echo $sql;
        //echo "<hr/>";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_po_marketing_row($arrSearch)
    {		
        $mylib = new globallib();
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "masterbarang.NamaLengkap";
				$arr_keyword[1] = "masterbarang.NamaStruk";
				$arr_keyword[2] = "masterbarang.NamaInitial";
				$arr_keyword[3] = "masterbarang.Barcode1";
				$arr_keyword[4] = "divisi.NamaDivisi";
				$arr_keyword[5] = "subdivisi.NamaSubDivisi";
				$arr_keyword[6] = "kategori.NamaKategori";
				$arr_keyword[7] = "subkategori.NamaSubKategori";
				$arr_keyword[8] = "brand.NamaBrand";
				$arr_keyword[9] = "subbrand.NamaSubBrand";
		        $arr_keyword[10] = "masterbarang.PCode";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}
		}
		
		$sql = "
			SELECT * FROM po_marketing  
		";
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function getSearch($id)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id'";
		return $this->getRow($sql);
	}
	
	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }
    
	function getKategori()
	{
    	$sql = "SELECT KdKategori,NamaKategori FROM kategori ORDER BY kategori.NamaKategori ASC";
		return $this->getArrayResult($sql);
    }
    
	function getBrand()
	{
    	$sql = "SELECT KdBrand,NamaBrand FROM brand ORDER BY brand.NamaBrand ASC";
		return $this->getArrayResult($sql);
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>