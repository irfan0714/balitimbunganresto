<?php
class discount_ticket_model extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function discount($id_discount,$qty) {
        $sql = "SELECT username,maxdisc FROM user WHERE otodisc='Y'
		AND (password=md5('$id_discount') OR barcode='$id_discount')";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
         if(empty($row)){
            echo  "salah";
        }else{
            echo 'datajson = ' . json_encode($row);
        }
       
    }
	function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function getlistDetaildata($nomor){
		$sql = "
				SELECT * FROM `member` a INNER JOIN type_member b ON a.`KdTipeMember` = b.`KdTipeMember` WHERE a.`KdMember`='$nomor';
				";
		return $this->getRow($sql);
	}
	
	function getlistDetaildataHp($nomor){
		$sql = "
				SELECT * FROM `member` a INNER JOIN type_member b ON a.`KdTipeMember` = b.`KdTipeMember` WHERE a.`NoHP1`='$nomor' OR a.`NoHP2`='$nomor';
				";
		return $this->getRow($sql);
	}
	
	function getDiscountCard($jenis_kartu){
		$sql = "
				SELECT
				* 
				FROM
				  `group_disc_header` a 
				  INNER JOIN `group_disc_detail` b 
				    ON a.`KdGroupDisc` = b.`KdGroupDisc` 
				WHERE a.`BerlakuMulai` <= NOW() 
				  AND a.`BerlakuSampai` >= NOW() 
				  AND a.`KdGroupDisc` = '$jenis_kartu'
				  AND b.`KdGroupBarang`='9';
				";
		return $this->getRow($sql);
	}
	
	function jenis_kartu(){
		$sql = "
				SELECT * FROM `group_disc_header` a WHERE a.`BerlakuMulai`<=NOW() AND a.`BerlakuSampai`>=NOW();
				";
		return $this->getArrayResult($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>