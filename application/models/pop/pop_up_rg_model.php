<?php
class Pop_up_rg_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getrgList($nodok)
	{
        	
		$sql = "
			SELECT 
			  a.`NoDokumen`,
			  DATE_FORMAT(a.`TglDokumen`, '%d-%m-%Y') AS TglDokumen,
			  c.`Keterangan`,
			  b.`Nama` ,
			  a.`Keterangan` AS Keterangan_ ,
			  a.`Jumlah`,
			  a.`PPn`,
			  a.`NilaiPPn`,
			  a.`Total`
			FROM
			  `trans_terima_header` a 
			  INNER JOIN supplier b 
			    ON a.`KdSupplier` = b.`KdSupplier` 
			  INNER JOIN gudang c 
			    ON a.`KdGudang` = c.`KdGudang` 
			  WHERE a.`NoDokumen`='".$nodok."';
		";
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
    
    function getDetailList($nodok)
	{
		$sql = "
			SELECT * FROM `trans_terima_detail` a 
			INNER JOIN `masterbarang` b ON a.`PCode`=b.`PCode` 
			INNER JOIN `satuan` c ON a.`satuan`=c.`KdSatuan` 
			WHERE a.`NoDokumen`='".$nodok."';
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	 function getTotalList($nodok)
	{
		$sql = "
			SELECT 
			  SUM(a.`Harga`*a.`Qty`) AS subtotal 
			FROM
			  `trans_terima_detail` a 
			  INNER JOIN `masterbarang` b 
				ON a.`PCode` = b.`PCode` 
			  INNER JOIN `satuan` c 
				ON a.`satuan` = c.`KdSatuan` 
			WHERE a.`NoDokumen` = '".$nodok."' ;
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }

	function getCustomer()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
    
}
?>