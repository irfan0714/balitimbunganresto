<?php
class HO_ordermodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getorderList($num, $offset,$id,$with,$thnbln)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause=" and FlagUsed='B' and DATE_FORMAT(Tanggal,'%Y%m')<='$thnbln'";
		if($id!=""){
			$clause = " and $with like '%$id%' and FlagUsed='B' and DATE_FORMAT(Tanggal,'%Y%m')<='$thnbln'";
		}
//    	$sql = "
//		SELECT h.*,COUNT(d.PCode) as Jmlh,c.Nama
//		from trans_order_barang_detail d,trans_order_barang_header h, supplier c
//		where h.NoDokumen=d.NoDokumen and h.KdSupplier=c.KdSupplier
//		and h.FlagTutup='T'
//		$clause
//		group by h.NoDokumen
//		order by cast(h.NoDokumen as unsigned) desc Limit $offset,$num";

        $sql="
        SELECT h.NoPengiriman as NoDokumen, Tanggal, Keterangan, h.ADDDATE, h.ADDTIME, FlagUsed, AddDateAplikasi, AddUser, TglDespatch,KdSupplier,nama,Jmlh,TotalHarga
        FROM
        (SELECT NoPengiriman , Tanggal, Keterangan, ADDDATE, ADDTIME, FlagUsed, AddDateAplikasi, AddUser, TglDespatch
        FROM trans_pengirimanheader WHERE 1 $clause )h
        LEFT JOIN supplier ON supplier.`KdSupplier`='HO0001'
        INNER JOIN (SELECT SUM(TotalHarga) AS TotalHarga,COUNT(PCode) AS Jmlh,NoPengiriman FROM trans_pengirimandetail GROUP BY NoPengiriman)d ON d.NoPengiriman=h.NoPengiriman
        ORDER BY h.NoPengiriman Limit $offset,$num
        ";

		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        echo $sql;
        return $row;

    }
	
	function getDetail($id)
	{
		$sql = "
		select brg.*,NamaBrand,NamaKategori from(
			select d.*,if(d.PCode=b.PCode,'pcode','bar') as jenis,NamaLengkap,b.PCode as PCodeBarang
			from trans_order_barang_detail d,trans_order_barang_header h,masterbarang b
			where h.NoDokumen='$id' and h.NoDokumen=d.NoDokumen
			and h.FlagTutup='T' and (b.PCode = d.PCode)
			order by Counter desc
		)brg
		LEFT JOIN
		(
		SELECT KdBrand,NamaBrand FROM brand
		)s
		ON s.KdBrand=brg.KdBrand
		LEFT JOIN
		(
		SELECT KdKategori,NamaKategori FROM kategori
		)k
		ON k.KdKategori=brg.KdKategori";
	
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
    
    function num_order_row($id,$with,$thnbln){
     	$clause=" and h.status<>'B' and DATE_FORMAT(h.TglDokumen,'%Y%m')<='$thnbln'";
     	if($id!=''){
			$clause = " and $with like '%$id%' and h.status<>'B' and DATE_FORMAT(h.TglDokumen,'%Y%m')<='$thnbln'";
		}
		$sql = "SELECT h.NoDokumen
		from trans_order_barang_header h
		where h.FlagTutup='T' $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getSatuan($pcode)
	{
		$sql = "SELECT SatuanBl as Satuan0,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0,
		        Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where PCode='$pcode'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
}
?>