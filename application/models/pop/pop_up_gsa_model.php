<?php
class Pop_up_gsa_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getgsaList($empid)
	{
        	
		$sql = "
			SELECT
			  a.`employee_name`,
			  c.`jabatan_name` 
			FROM
			  employee a 
			  INNER JOIN employee_position b 
				ON a.`employee_id` = b.`employee_id` 
			  INNER JOIN jabatan c 
				ON b.`jabatan_id` = c.`jabatan_id` 
			WHERE a.`employee_id` = '".$empid."' ;
		";
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
    
    function getDetailList($empid,$tgl)
	{
		$sql = "
			SELECT
			  gsa_detail.gsa_id,
			  gsa_detail.`NoStiker`,
			  gsa.`employee_id`,
			  employee.`employee_name`,
			  gsa.`beauty_tour`,
			  gsa.`coffee_tour`,
			  gsa.`chamber`,
			  gsa.`standby_oh`,
			  gsa.`standby_resto`,
			  gsa.`standby_be` 
			FROM
			  gsa 
			  INNER JOIN gsa_detail
			  ON gsa.`gsa_id` = gsa_detail.`gsa_id`
			  INNER JOIN employee 
				ON gsa.`employee_id` = employee.employee_id 
			WHERE gsa.`Tanggal` = '".$tgl."' AND gsa_detail.`NoStiker`!=''
			  AND gsa.`employee_id` = '".$empid."' GROUP BY gsa_detail.gsa_id ORDER BY gsa_detail.gsa_id ASC;
		";
		//echo $sql;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	function getDetailList2($empid,$thn, $bln)
	{
		$sql = "
			SELECT 
			  gsa.`Tanggal`,
			  gsa_detail.gsa_id,
			  gsa_detail.`NoStiker`,
			  gsa.`employee_id`,
			  employee.`employee_name`,
			  gsa.`beauty_tour`,
			  gsa.`coffee_tour`,
			  gsa.`chamber`,
			  gsa.`standby_oh`,
			  gsa.`standby_resto`,
			  gsa.`standby_be` 
			FROM
			  gsa 
			  INNER JOIN gsa_detail 
				ON gsa.`gsa_id` = gsa_detail.`gsa_id` 
			  INNER JOIN employee 
				ON gsa.`employee_id` = employee.employee_id 
			WHERE YEAR(gsa.`Tanggal`) = '".$thn."' AND MONTH(gsa.`Tanggal`) = '".$bln."'
			  AND gsa_detail.`NoStiker` != '' 
			  AND gsa.`employee_id` = '".$empid."'
			  GROUP BY gsa.`gsa_id` ;
		";
		//echo $sql;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	 function getTotalList($nodok)
	{
		$sql = "
			SELECT 
			  SUM(a.`Harga`*a.`Qty`) AS subtotal 
			FROM
			  `trans_terima_detail` a 
			  INNER JOIN `masterbarang` b 
				ON a.`PCode` = b.`PCode` 
			  INNER JOIN `satuan` c 
				ON a.`satuan` = c.`KdSatuan` 
			WHERE a.`NoDokumen` = '".$nodok."' ;
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	    
}
?>