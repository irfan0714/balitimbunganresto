<?php
class Nobuktimodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getnobuktiList($num,$offset,$id,$prow,$kode)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
		   if($prow=='P')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdSupplier like '%$id%')";
		   }
		   if($prow=='H')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdSupplier like '%$id%') and a.KdSupplier like '%$kode%'";
		   }
		   if($prow=='C')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdSupplier like '%$id%') and a.KdSupplier like '%$kode%'";
		   }
		   if($prow=='R')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdCustomer like '%$id%')";
		   }
		   if($prow=='S')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdCustomer like '%$id%') and a.KdCustomer like '%$kode%'";
		   }
		   if($prow=='D')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdCustomer like '%$id%') and a.KdCustomer like '%$kode%'";
		   }
		}else
		{
		   if(($prow=='C')||($prow=='H'))
		   {
			  $clause = " and a.KdSupplier like '%$kode%'";
		   }
		   if(($prow=='D')||($prow=='S'))
		   {
			  $clause = " and a.KdCustomer like '%$kode%'";
		   }
		}
		if(($prow=='P')||($prow=='H'))
		{
    	$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdSupplier as Kode,Nama,TotalHutang as Nilai,
TotalHutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from hutang a, supplier b where Status<>'B' and TotalHutang<>TotalBayar and Jenis in ('F','K') 
and a.KdSupplier=b.KdSupplier $clause order by --NoTransaksi asc Limit $offset,$num";
		}
		if(($prow=='R')||($prow=='S'))
		{
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdCustomer as Kode,Nama,TotalPiutang as Nilai,
TotalPiutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from piutang a, customer b where Status<>'B' and TotalPiutang<>TotalBayar and Jenis in ('F','D') 
and a.KdCustomer=b.KdCustomer $clause order by --NoTransaksi asc Limit $offset,$num";
		}
		if($prow=='C')
		{
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdSupplier as Kode,Nama,TotalHutang as Nilai,
TotalHutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from hutang a, supplier b where Status<>'B' and TotalHutang<>TotalBayar and Jenis in ('R','D') 
and a.KdSupplier=b.KdSupplier $clause order by --NoTransaksi asc Limit $offset,$num";
		}
		if($prow=='D')
		{
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdCustomer as Kode,Nama,TotalPiutang as Nilai,
TotalPiutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from piutang a, customer b where Status<>'B' and TotalPiutang<>TotalBayar and Jenis in ('R','K') 
and a.KdCustomer=b.KdCustomer $clause order by --NoTransaksi asc Limit $offset,$num";
		}
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_nobukti_row($id,$prow,$kode){
     	$clause="";
     	if($id!=''){
		   if($prow=='P')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdSupplier like '%$id%')";
		   }
		   if($prow=='H')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdSupplier like '%$id%') and a.KdSupplier like '%$kode%'";
		   }
		   if($prow=='C')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdSupplier like '%$id%') and a.KdSupplier like '%$kode%'";
		   }
		   if($prow=='R')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdCustomer like '%$id%')";
		   }
		   if($prow=='S')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdCustomer like '%$id%') and a.KdCustomer like '%$kode%'";
		   }
		   if($prow=='D')
		   {
			  $clause = " and (nama like '%$id%' or notransaksi like '%$id%' or a.KdCustomer like '%$id%') and a.KdCustomer like '%$kode%'";
		   }
		}else
		{
		   if(($prow=='C')||($prow=='H'))
		   {
			  $clause = " and a.KdSupplier like '%$kode%'";
		   }
		   if(($prow=='D')||($prow=='S'))
		   {
			  $clause = " and a.KdCustomer like '%$kode%'";
		   }
		}
		
		if(($prow=='P')||($prow=='H'))
		{
    	$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdSupplier as Kode,Nama,TotalHutang as Nilai,
TotalHutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from hutang a, supplier b where Status<>'B' and TotalHutang<>TotalBayar and Jenis in ('F','K') 
and a.KdSupplier=b.KdSupplier $clause";
		}
		if(($prow=='R')||($prow=='S'))
		{
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdCustomer as Kode,Nama,TotalPiutang as Nilai,
TotalPiutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from piutang a, customer b where Status<>'B' and TotalPiutang<>TotalBayar and Jenis in ('F','D') 
and a.KdCustomer=b.KdCustomer $clause";
		}
		if($prow=='C')
		{
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdSupplier as Kode,Nama,TotalHutang as Nilai,
TotalHutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from hutang a, supplier b where Status<>'B' and TotalHutang<>TotalBayar and Jenis in ('R','D') 
and a.KdSupplier=b.KdSupplier $clause";
		}
		if($prow=='D')
		{
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdCustomer as Kode,Nama,TotalPiutang as Nilai,
TotalPiutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening,Jenis from piutang a, customer b where Status<>'B' and TotalPiutang<>TotalBayar and Jenis in ('R','K') 
and a.KdCustomer=b.KdCustomer $clause";
		}
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>