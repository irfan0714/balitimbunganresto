<?php
class Pop_up_detail_stock_opname_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getStockList($pcode,$tglawal,$tglakhir,$gudang)
	{
        	
		$sql = "
			SELECT
			  b.PCode AS KodeBarang,
			  b.`NamaLengkap`
			FROM masterbarang b 
			WHERE b.`PCode` = '".$pcode."' ;
		";
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
    
    function getDetailList($pcode,$bln,$thn,$gudang)
	{
        
		$sql = "
			SELECT 
			  b.`NoDokumen`,
			  b.`PCode`,
			  b.QtyFisik,
			  a.`AddUser` AS Author 
			FROM
			  `opname_header_split` a 
			  INNER JOIN `opname_detail_split` b 
			    ON a.`NoDokumen` = b.`NoDokumen` 
			WHERE a.`KdGudang` = '$gudang' 
			  AND MONTH(a.`TglDokumen`) = '$bln' 
			  AND YEAR(a.`TglDokumen`) = '$thn' 
			  AND b.`PCode`='$pcode';
		";
		
		/*echo $sql;
        echo "<hr/>";*/
		
		$query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
    }
	 
	function getDetailList2($empid,$thn, $bln)
	{
		$sql = "
			SELECT 
			  gsa.`Tanggal`,
			  gsa_detail.gsa_id,
			  gsa_detail.`NoStiker`,
			  gsa.`employee_id`,
			  employee.`employee_name`,
			  gsa.`beauty_tour`,
			  gsa.`coffee_tour`,
			  gsa.`chamber`,
			  gsa.`standby_oh`,
			  gsa.`standby_resto`,
			  gsa.`standby_be` 
			FROM
			  gsa 
			  INNER JOIN gsa_detail 
				ON gsa.`gsa_id` = gsa_detail.`gsa_id` 
			  INNER JOIN employee 
				ON gsa.`employee_id` = employee.employee_id 
			WHERE YEAR(gsa.`Tanggal`) = '".$thn."' AND MONTH(gsa.`Tanggal`) = '".$bln."'
			  AND gsa_detail.`NoStiker` != '' 
			  AND gsa.`employee_id` = '".$empid."'
			  GROUP BY gsa.`gsa_id` ;
		";
		//echo $sql;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	 function getTotalList($nodok)
	{
		$sql = "
			SELECT 
			  SUM(a.`Harga`*a.`Qty`) AS subtotal 
			FROM
			  `trans_terima_detail` a 
			  INNER JOIN `masterbarang` b 
				ON a.`PCode` = b.`PCode` 
			  INNER JOIN `satuan` c 
				ON a.`satuan` = c.`KdSatuan` 
			WHERE a.`NoDokumen` = '".$nodok."' ;
		";
		//echo $sql;die;
        $query = $this->db->query($sql);
        $result = $query->result_array(); 			
		return $result;
	}
	
	function jmlhari($tgl) {
	 	
		
		$sql ="
				SELECT LAST_DAY('".$tgl."') AS tgl;
			  ";
						
		$query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
    }
    
    function saldo($pcode,$thn,$gudang,$tabel_field,$tabel_field2){
		$sql="
			SELECT 
			  ROUND(AVG( a.".$tabel_field."),0) AS saldo_awal,
			  ROUND(AVG( a.".$tabel_field2."),0) AS saldo_akhir
			FROM
			  `stock` a 
			WHERE a.`Tahun` = '".$thn."' 
			  AND a.`KdGudang` = '".$gudang."' 
			  AND a.`PCode`='".$pcode."';
			";
			
	    $query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
	}
	
	function saldodvs($pcode,$thn,$gudang,$tabel_field,$tabel_field2){
		$sql="
			SELECT 
			  ROUND(SUM(a.GAwal07), 0) AS saldo_awal,
			  ROUND(SUM(a.GAkhir07), 0) AS saldo_akhir 
			FROM
			  `stock` a 
			  INNER JOIN masterbarang b 
			    ON a.PCode = b.PCode
			  INNER JOIN gudang d
			    ON a.KdGudang = d.KdGudang
			  INNER JOIN subdivisi e
			    ON d.KdSubDivisi = e.KdSubDivisi
			WHERE a.`Tahun` = '".$thn."'
			  AND e.`KdDivisi` = '".$gudang."' 
			  AND a.`PCode` = '".$pcode."' ;
			";
				
	    $query = $this->db->query($sql);
        $result = $query->result_array();
        			
		return $result;
	}
	    
}
?>