<?php
class Paketmodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getbarangList($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
         
		 $sql = "
			SELECT DISTINCT(MPCode) as PCode,NamaLengkap FROM `barang_paket`
			INNER JOIN masterbarang ON masterbarang.`PCode`=barang_paket.`MPCode`
			 $clause order by `MPCode` Limit $offset,$num
		";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_barang_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT DISTINCT(MPCode) as PCode,NamaLengkap FROM `barang_paket`
			INNER JOIN masterbarang ON masterbarang.`PCode`=barang_paket.`MPCode`
			 $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>