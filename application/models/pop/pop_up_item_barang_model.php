<?php
class Pop_up_item_barang_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function getItemBarang($arrSearch)
	{
        $mylib = new globallib();
        $where_keyword="";
        
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        
				$arr_keyword[0] = "a.PCode";
				$arr_keyword[1] = "a.NamaLengkap";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}		
		}
        	
		$sql = "
		   SELECT * FROM `masterbarang` a WHERE 1  
		    ".$where_keyword." 
			LIMIT 0,50
		";
		//echo $sql;
        //echo "<hr/>";
        return $this->getArrayResult($sql);
    }
        
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>