<?php
class Barangbelimodel extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function getbarangList($num,$offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " and $with like '%$id%'";
		}
		/*
    	$sql = "
				select b.*,NamaBrand,NamaKategori,NamaSatuanSt,NamaSatuan0 from(
				SELECT PCode,NamaLengkap,SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand
				FROM masterbarang Where (namastruk like '%$owner%' or pcode like '%$owner%') $clause Limit $offset,$num
				) b
				left join
				(
				select KdBrand,NamaBrand from brand
				) br
				on br.KdBrand=b.KdBrand
				left join
				(
				select KdKategori,NamaKategori from kategori
				) kt
				on kt.KdKategori=b.KdKategori
				left join
				(
				select KdSatuan,NamaSatuan as NamaSatuanSt from satuan
				) st	
				on st.KdSatuan=b.SatuanSt
				left join
				(
				select KdSatuan,NamaSatuan as NamaSatuan0 from satuan
				) st2	
				on st2.KdSatuan=b.Satuan0";
				*/
		$sql = "SELECT b.*,NamaBrand,NamaKategori,PCodeExt FROM(
				SELECT PCode,NamaLengkap,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand,PersenPajak as PPnB,
				SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,  
				(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0
				FROM masterbarang WHERE status='A' $clause
				) b
				LEFT JOIN
				(
				SELECT KdBrand,NamaBrand FROM brand
				) br
				ON br.KdBrand=b.KdBrand
				LEFT JOIN
				(
				SELECT KdKategori,NamaKategori FROM kategori
				) kt
				ON kt.KdKategori=b.KdKategori
				LEFT JOIN
				(
				SELECT kodegrp,PCode,PCodeExt,NamaExt FROM kodeextdetail 
				) ext				
				ON ext.PCode=b.PCode";
				
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
		//echo $sql;
        return $row;
    }
    
    function num_barang_row($id,$with,$owner,$kdgroupext){
     	$clause="";
     	if($id!=''){
			$clause = " and $with like '%$id%'";
		}/*
		$sql = "select b.*,NamaBrand,NamaKategori,NamaSatuanSt from(
				SELECT PCode,NamaLengkap,SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand
				FROM masterbarang Where (NamaLengkap like '%$owner%' or pcode like '%$owner%') $clause
				) b
				left join
				(
				select KdBrand,NamaBrand from brand
				) br
				on br.KdBrand=b.KdBrand
				left join
				(
				select KdKategori,NamaKategori from kategori
				) kt
				on kt.KdKategori=b.KdKategori
				left join
				(
				select KdSatuan,NamaSatuan as NamaSatuanSt from satuan
				) st	
				on st.KdSatuan=b.SatuanSt
				LEFT JOIN
				(
				SELECT kodegrp,PCode,PCodeExt,NamaExt FROM kodeextdetail where kodegrp='$kdgroupext' and PCode='%$owner%'
				) ext				
				ON ext.PCode=b.PCode";
				*/
		$sql = "SELECT b.*,NamaBrand,NamaKategori,PCodeExt FROM(
				SELECT PCode,NamaLengkap,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand,PersenPajak as PPnB,
				SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,  
				(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0
				FROM masterbarang WHERE status='A' $clause
				) b
				LEFT JOIN
				(
				SELECT KdBrand,NamaBrand FROM brand
				) br
				ON br.KdBrand=b.KdBrand
				LEFT JOIN
				(
				SELECT KdKategori,NamaKategori FROM kategori
				) kt
				ON kt.KdKategori=b.KdKategori
				LEFT JOIN
				(
				SELECT kodegrp,PCode,PCodeExt,NamaExt FROM kodeextdetail where kodegrp='$kdgroupext'
				) ext				
				ON ext.PCode=b.PCode";
				
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	function getSatuan($pcode)
	{
		$sql = "SELECT SatuanBl as Satuan0,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0,
		      Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where TRIM(PCode)='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
    }
}
?>
