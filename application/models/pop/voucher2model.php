<?php
class voucher2model extends CI_Model
{
	function __construct(){
        parent::__construct();
    }

    function voucher($id_voucher,$qty, $tgltransaksi) {
        
        $cek = "SELECT * FROM voucher a WHERE a.novoucher='$id_voucher' AND a.expDate='$tgltransaksi' AND a.jenis='5'";
        $hasil = $this->db->query($cek);
        $ambil = $hasil->result_array();
        
        if(!empty($ambil)){
            $sql = "SELECT novoucher,if(jenis='1','TICKET',keterangan) as keterangan,round(nominal-terpakai) as nominal,rupdisc,jenis,status,$qty as qty,
                expDate as expired FROM voucher WHERE novoucher='$id_voucher' AND expDate='$tgltransaksi'";
        }else{

            #CEK VOUCHER KARYAWAN 
            $sqlCek = "SELECT * FROM voucher WHERE novoucher = '$id_voucher' AND jenis ='3' "; 
            $hasilCek = $this->db->query($sqlCek);
            $cekVoucher = $hasilCek->result_array();

            if(!empty($cekVoucher)){

                $sql =" SELECT voucher.novoucher, IF(voucher.jenis = '1', 'TICKET', voucher.keterangan) AS keterangan, 
                        IF(voucherEmployee.nominal,ROUND(voucherEmployee.nominal - voucherEmployee.terpakai),0) AS nominal, voucher.rupdisc, voucher.jenis, voucher.status, 1 AS qty, voucherEmployee.expDate AS expired
                        FROM
                        voucher
                        LEFT JOIN 
                        (
                            SELECT * FROM voucher_employee  WHERE nik = '$id_voucher'
                            AND status = '1'
                            AND expDate >= '$tgltransaksi' AND expDate <= LAST_DAY(DATE(NOW()))
                        ) voucherEmployee 
                        ON voucher.novoucher = voucherEmployee.nik
                        WHERE voucher.novoucher = '$id_voucher' ";
            }else{

                $sql = "SELECT novoucher,if(jenis='1','TICKET',keterangan) as keterangan,round(nominal-terpakai) as nominal,rupdisc,jenis,status,$qty as qty,
                expDate as expired FROM voucher WHERE novoucher='$id_voucher'  ";
            }
        }

        $qry = $this->db->query($sql);
        $row = $qry->result_array();
         if(empty($row)){
            echo  "salah";
        }else{
            echo 'datajson = ' . json_encode($row);
        }
       
    }
    
    function vouchertrv($id_voucher,$qty, $tgltransaksi) {
    	
    	$cek = "SELECT * FROM voucher a WHERE a.novoucher_travel='$id_voucher' AND a.expDate='$tgltransaksi' AND a.jenis='5'";
    	$hasil = $this->db->query($cek);
        $ambil = $hasil->result_array();
        
        if(!empty($ambil)){
            $sql = "SELECT novoucher,if(jenis='1','TICKET',keterangan) as keterangan,round(nominal-terpakai) as nominal,rupdisc,jenis,status,$qty as qty,
				expDate as expired FROM voucher WHERE novoucher_travel='$id_voucher' AND expDate='$tgltransaksi'";
        }else{
			$sql = "SELECT novoucher,if(jenis='1','TICKET',keterangan) as keterangan,round(nominal-terpakai) as nominal,rupdisc,jenis,status,$qty as qty,
				expDate as expired FROM voucher WHERE novoucher_travel='$id_voucher'";
		}
    	
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
         if(empty($row)){
            echo  "salah";
        }else{
            echo 'datajson = ' . json_encode($row);
        }
       
    }

    function vouchernominal($id_voucher,$qty, $tgltransaksi) {
    	
    	$cek = "SELECT * FROM voucher a WHERE a.novoucher_travel='$id_voucher' AND a.expDate='$tgltransaksi' AND a.jenis='5'";
    	
    	$hasil = $this->db->query($cek);
        $ambil = $hasil->result_array();
       
        if(!empty($ambil)){
			$sql = "SELECT novoucher,if(jenis='1','TICKET',keterangan) as keterangan,round(nominal-terpakai) as nominal,rupdisc,jenis,status,$qty as qty,
				    expDate as expired FROM voucher WHERE novoucher='$id_voucher' AND expDate='$tgltransaksi'";
		}else{
			$sql = "SELECT novoucher,if(jenis='1','TICKET',keterangan) as keterangan,round(nominal-terpakai) as nominal,rupdisc,jenis,status,$qty as qty,
				expDate as expired FROM voucher WHERE novoucher='$id_voucher'";
		}
      
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
       
         if(empty($row)){
            echo  "salah";
        }else{
            echo 'datajson = ' . json_encode($row);
        }
       
    }

    function cekvoucher($id_voucher,$kassa) {
    	
    	$cek = "SELECT a.`KdCustomer`, a.`KdStore` FROM transaksi_header a WHERE a.KdCustomer='$id_voucher' AND a.`KdStore` = '$kassa'";
    	// print $cek;
    	$hasil = $this->db->query($cek);
        $row = $hasil->result_array();
       
         if(!empty($row)){
            // echo 'datajson = ' . json_encode($id_voucher);
            echo  "salah";
         }
       
    }  
    
	function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
}
?>