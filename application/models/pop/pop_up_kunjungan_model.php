<?php
class Pop_up_kunjungan_model extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
	function gettourList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "tourtravel.KdTravel";
				$arr_keyword[1] = "tourtravel.Nama";
				$arr_keyword[2] = "tourtravel.Contact";
				$arr_keyword[3] = "tourtravel.Phone";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			/*if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}*/
		}
        	
		$sql = "
			SELECT 
			*
			FROM
			tourtravel			 
			WHERE 
				1 
				AND tourtravel.Aktif = 'A'
				".$where_keyword."
			ORDER BY 
			  tourtravel.Nama ASC 
			LIMIT $offset,$limit
		";
		/*echo $sql;
        echo "<hr/>";*/
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function num_tour_row($arrSearch)
    {		
        $mylib = new globallib();
        
        $where_keyword="";
        $where_divisi = "";
        $where_kategori="";
        $where_brand="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "tourtravel.KdTravel";
				$arr_keyword[1] = "tourtravel.Nama";
				$arr_keyword[2] = "tourtravel.Contact";
				$arr_keyword[3] = "tourtravel.Phone";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			/*if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND masterbarang.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			
			if($arrSearch["kategori"]!="")
			{
				$where_kategori = "AND masterbarang.KdKategori = '".$arrSearch["kategori"]."'";	
			}
			
			if($arrSearch["brand"]!="")
			{
				$where_brand = "AND masterbarang.KdBrand = '".$arrSearch["brand"]."'";	
			}*/
		}
		
		$sql = "
			SELECT 
			  *
			  FROM 
			  tourtravel
			  WHERE 1 
				AND tourtravel.Aktif = 'A'
				".$where_keyword."
		";
		//echo $sql;
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	function getSearch($id)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id'";
		return $this->getRow($sql);
	}
	
	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }
    
	function getKategori()
	{
    	$sql = "SELECT KdKategori,NamaKategori FROM kategori ORDER BY kategori.NamaKategori ASC";
		return $this->getArrayResult($sql);
    }
    
	function getBrand()
	{
    	$sql = "SELECT KdBrand,NamaBrand FROM brand ORDER BY brand.NamaBrand ASC";
		return $this->getArrayResult($sql);
    }

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>