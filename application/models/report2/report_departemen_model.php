<?php

class Reportmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	 function viewData($tgldari, $tglsampai, $nama) {
		$masterabsen_arr	= $this->_getMasterAbsensi($tgldari, $tglsampai, $nama);
		$absendatang_data	= $this->_getAbsensiDatang($tgldari, $tglsampai);
		$absenkeluar_data	= $this->_getAbsensiKeluar($tgldari, $tglsampai);
		
		$retval				= array();
		foreach ($masterabsen_arr as $masterabsen) 
		{
			$shift		= $masterabsen['Shift'];
			$nama		= $masterabsen['NamaKaryawan'];
			$hari		= $masterabsen['Hari'];
			$tgl		= $masterabsen['Tanggal'];
			$timein		= $absendatang_data[$tgl];
			$timeout	= ($timein==$absenkeluar_data[$tgl])?' - ':$absenkeluar_data[$tgl];
			
			list($jam,$menit,$detik)	= explode(":",$timein);
			if( ($jam>12) AND ($timeout==' - ') ) 
			{
				$timeout	= $timein;
				$timein		= ' - ';
			}
			
			$retval[]	= array(
							'Shift'			=> $shift,
							'NamaKaryawan'	=> $nama,
							'Hari'			=> $hari,
							'Tanggal'		=> $tgl,
							'TimeIn'		=> $timein,
							'TimeOut'		=> $timeout
						);
		}
		return $retval;
    }
	
	function _getAbsensiKeluar($tgldari,$tglsampai) 
	{
		$retval		= array();		
		$sql1		= "SELECT 
							NamaKaryawan, 
							DATE(MAX(PresensiTime)) AS Tanggal, 
							DAYNAME(DATE(PresensiTime)) AS Hari,
							TIME(MAX(PresensiTime)) AS TimeOut
						FROM 
							absensitrn
						WHERE DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
						 GROUP BY  
							DATE(PresensiTime) 
						ORDER BY 
							NamaKaryawan, PresensiTime";
		
        $query1		= $this->db->query($sql1);
        $result_arr	= $query1->result_array();
		
		foreach ($result_arr as $result) {
			$tgl		= $result['Tanggal'];
			$timeout	= $result['TimeOut'];
			
			$retval[$tgl]	= $timeout;
		}		
        return $retval; 
	}
	function _getAbsensiDatang($tgldari,$tglsampai) {
		$retval		= array();		
		$sql1		= "SELECT 
							NamaKaryawan, 
							DAYNAME(DATE(PresensiTime)) AS Hari,
							DATE(MIN(PresensiTime)) AS Tanggal, 
							TIME(MIN(PresensiTime)) AS TimeIn
						FROM 
							absensitrn
						WHERE DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
						 GROUP BY  
							DATE(PresensiTime) 
						ORDER BY 
							NamaKaryawan, PresensiTime";
		
        $query1		= $this->db->query($sql1);
        $result_arr	= $query1->result_array();
		
		foreach ($result_arr as $result) {
			$tgl		= $result['Tanggal'];
			$timein		= $result['TimeIn'];
			$hari		= $result['Hari'];
			
			$retval[$tgl]	= $timein;
		}		
        return $retval; 
	}
	
	function _getMasterAbsensi($tgldari,$tglsampai, $nama) 
	{
		$sql1 = "
				SELECT
					NamaKaryawan, TIME(presensiTime)AS jam, DAYNAME(DATE(presensiTime))AS Hari, DATE(presensitime) AS Tanggal, Shift
				FROM 
					absensitrn
				JOIN 
					employee 
					ON 
						employee.`employee_name` = absensitrn.`NamaKaryawan`
				JOIN 
					shift 
					ON 
						shift.`KdEmployee` = employee.`employee_id`
				WHERE 
					NamaKaryawan='$nama' 
					AND
						DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
					GROUP BY DATE(`PresensiTime`)
					ORDER BY `NamaKaryawan`
					";
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        return $result1; 
	}
	
}

?>