<?php
class Sales_invoice_model2 extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
	
    
    
    function num_sales_invoice_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `salesinvoice` a;       
		";
		                  
        return $this->NumResult($sql);
	}
	    
    function getSalesInvoiceDetailList($user)
	{
         $sql = "  
         SELECT * FROM `salesinvoicedetail_temp` a  
		 INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` WHERE CONCAT(a.`invoiceno`,a.`adduser`)='00000".$user."';
        ";              
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
	function getSalesInvoiceDetailList2($id)
	{
        
    	$sql = "  
         SELECT * FROM `salesinvoicedetail` a  
		 INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` WHERE a.`invoiceno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function getDataSalesInvoiceDetail($user)
	{
        
    	$sql = "  
        SELECT 
		  * 
		FROM
		  `salesinvoicedetail_temp` a 
		  INNER JOIN masterbarang b 
		    ON a.`inventorycode` = b.`PCode` 
		WHERE a.`invoiceno` = '00000' AND a.`adduser`='".$user."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
	
	function hitungSalesInvoiceDetailList($user)
	{
        
    	$sql = "  
        SELECT SUM(a.`quantity`*a.`nettprice`) AS total , SUM(b.`DiscLokal`) AS diskon
		FROM `salesinvoicedetail_temp` a  
		INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` 
		WHERE CONCAT(a.`invoiceno`,a.`adduser`)='00000".$user."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
	function hitungSalesInvoiceDetailList2($id,$user)
	{
        
    	$sql = "  
        SELECT SUM(a.`quantity`*a.`nettprice`) AS total , SUM(b.`DiscLokal`) AS diskon
		FROM `salesinvoicedetail` a  
		INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` 
		WHERE a.`invoiceno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
	
	function getSalesInvoiceList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.invoiceno";
				$arr_keyword[1] = "a.sidate";
				$arr_keyword[2] = "a.duedate";
				$arr_keyword[3] = "b.Nama";
				$arr_keyword[4] = "a.grandtotal";
				$arr_keyword[5] = "a.status";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT 
              a.*,
			  b.*,
			  DATE_FORMAT(a.`sidate`, '%d-%m-%Y') AS sidate_indo
            FROM `salesinvoice` a 
			INNER JOIN customer b ON a.customerid=b.KdCustomer
			WHERE  
            	1  
            	".$where_keyword."
            	".$where_gudang."
            	".$where_customer."    
            	".$where_status."                                   
            ORDER BY
                a.`sidate` desc
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			  a.*,
			  b.*,
			  DATE_FORMAT(a.`sidate`, '%d-%m-%Y') AS sidate_indo,
			  DATE_FORMAT(a.`duedate`, '%d-%m-%Y') AS duedate_indo
			FROM `salesinvoice` a 
			INNER JOIN `customer` b ON a.`customerid`=b.`KdCustomer`
			WHERE 1 AND a.`invoiceno`='".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT COUNT(a.dono) AS jml FROM `salesinvoicedetail` a WHERE a.dono='".$nodok."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail3($user)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail_temp` a WHERE a.`invoiceno`='00000' AND a.adduser='".$user."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function cekGetDetail4($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getDetail_cetak($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a 
	 INNER JOIN `masterbarang` b ON a.`inventorycode`=b.`PCode`
	 WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function hitungSalesInvoice_cetak($id)
	{
        
    	$sql = "  
        SELECT SUM(a.`quantity`*a.`nettprice`) AS total , SUM(b.`DiscLokal`) AS diskon
		FROM `salesinvoicedetail` a  
		INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` 
		WHERE a.`invoiceno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
    function getCustomer()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>