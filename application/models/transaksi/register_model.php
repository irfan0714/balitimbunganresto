<?php

class register_model extends CI_Model {

    //put your code here
    function __construct() {
        parent::__construct();
    }

     function getTourTravel() {
        $sql = "SELECT KdTravel,  Nama FROM tourtravel ORDER BY Nama";
       $query1 = $this->db->query($sql);
        return $query1; 
    }

    function getDetailForPrint($id) {
        $sql = "
                SELECT d.*,NamaLengkap FROM( 
                SELECT NoTransaksi,PCode, Qty, Harga
                FROM finance_komisi_detail WHERE NoTransaksi='$id' ORDER BY PCode
                ) d 
                INNER JOIN masterbarang
                ON masterbarang.`PCode`=d.PCode
		";
//echo $sql;
        return $this->getArrayResult($sql);
    }

    function getHeaderForPrint($id) {
        $sql = "
              SELECT 
                a.NoTransaksi, DATE_FORMAT(a.TglTransaksi, '%d-%m-%Y') AS TglTransaksi, a.KdAgent, a.Total AS TotKomisi, b.TotalNilai AS TotSales, 
                MIN(c.`Waktu`) AS mulai, MAX(c.waktu) AS akhir 
              FROM
                finance_komisi_header a
              INNER JOIN transaksi_header b
              ON a.`KdAgent` = b.`KdAgent`
              INNER JOIN transaksi_detail c
              ON b.`NoStruk` = c.`NoStruk`
              AND a.NoTransaksi = '$id'
              GROUP BY `KdAgent` 
		";
//echo $sql;
         return $this->getRow($sql);
    }

    function getCountDetail($id) {
        $sql = "SELECT * FROM finance_komisi_detail where NoTransaksi='$id'";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }
    
    function cek_otorisasi($user)
	{
    	$sql = "
    	SELECT * FROM otorisasi_user
			WHERE 1
			AND UserName = '".$user."'
			AND Tipe = 'bypass_sticker'
    	";
		return $this->getArrayResult($sql);
    }

	function validasi_user($user,$pass)
	{
    	$sql = "
    	    SELECT * FROM user
			WHERE 1
			AND UserName = '".$user."'
			AND Password = MD5('".$pass."')
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

    function getRegisterList($num, $offset, $id, $with) {
        if ($offset != '') {
            
            $offset = $offset;
        } else {
            
            $offset = 0;
        }

        $where = "";

        if ($id != "") {
        	$opt = $with == 'NoTransaksi' ? "register.KdRegister = '$id'" : ($with == 'Tanggal' ? "register.Tanggal = '$id'" : "tourleader.Nama LIKE '%$id%'");
        	$where = "  AND 
                        (
                        	$opt
                    	)
                    	";
                         /*   -- tourleader.Nama LIKE '%".$id."%'
                            -- OR
                            -- register.KdRegister LIKE '".$id."%'
                            -- OR
                            -- register.Keterangan LIKE '%".$id."%'
                        )
            ";*/

        }

                    $sql = "

            SELECT 
register.*, tourleader.Nama AS nmAgen, register.KdTravel, tvl.`Nama` AS NamaTravel
FROM ( SELECT * FROM register) register
LEFT JOIN tourleader ON tourleader.KdTourLeader = register.KdTourLeader
LEFT JOIN tourtravel ON tourtravel.KdTravel = tourleader.KdTravel
LEFT JOIN (SELECT * FROM tourtravel)AS tvl ON tvl.KdTravel = register.`KdTravel`
WHERE 1 ".$where."
ORDER BY KdRegister
DESC limit $offset,$num";
//WHERE $clause       
 //echo $sql;
        return $this->getArrayResult($sql);
    }

    function getHeader($id) {
        $sql = "SELECT 
				  register.*,
				  tourleader.Nama AS NamaMember,
                  tourleader.Phone,
                  CONCAT(tourleader.Bank, ' ', tourleader.NoRekening, ' a/n ', tourleader.Penerima) AS bankrek,
				  tourtravel.KdTravel,
				  tvl.Nama AS nmTravel  
				FROM
				  (SELECT 
					* 
				  FROM
					register
				  ) AS register 
				  LEFT JOIN tourleader 
					ON tourleader.KdTourLeader = register.KdTourLeader 
				  LEFT JOIN tourtravel 
					ON tourtravel.KdTravel = tourleader.KdTravel 
                    LEFT JOIN (SELECT * FROM tourtravel) tvl ON tvl.KdTravel = register.`KdTravel`
				WHERE KdRegister = '$id' 
				ORDER BY Tanggal DESC ";
            //echo $sql;
        return $this->getRow($sql);
    } 

    function getDetail($id) {
        $sql = "SELECT a.KdRegister, b.PCode, c.NamaLengkap, b.Qty, b.Harga
                    FROM finance_komisi_header a, finance_komisi_detail b, masterbarang c
                    WHERE a.NoTransaksi = b.NoTransaksi AND b.PCode = c.PCode
                                              AND a.NoTransaksi='$id' ORDER BY NoTransaksi DESC, b.AddDate Desc";
        return $this->getArrayResult($sql);
    }

    function num_komisi_row($id, $with) {
        $clause = "";
        if ($with = 'tourleader.Nama') {
            $clause = "WHERE $with like '%$id%'";
        }else{
			$clause = "";
		}
       /* $sql = "SELECT register.*,tourleader.Nama AS nmAgen,tourtravel.KdTravel,tourtravel.Nama AS nmTravel FROM
            (SELECT * FROM register)register
            INNER JOIN tourleader ON tourleader.KdTourLeader=register.KdTourLeader
            INNER JOIN tourtravel ON tourtravel.KdTravel=tourleader.KdTravel $clause
            ORDER BY KdRegister";*/   //querynya hendri


           /* $sql = "SELECT 
						  register.*,
						  tourleader.Nama AS nmMember
						FROM
						  (SELECT 
						    * 
						  FROM
						    register) register 
						  INNER JOIN tourleader 
						    ON tourleader.KdTourLeader = register.KdTourLeader 
							$clause 
						ORDER BY KdRegister";*/

            $sql = "SELECT register.*, tourleader.Nama AS nmMember, tourtravel.`Nama` AS NamaTravel
FROM (SELECT * FROM register) register 
INNER JOIN tourleader ON tourleader.KdTourLeader = register.KdTourLeader 
LEFT JOIN tourtravel ON tourtravel.`KdTravel` = register.`KdTravel`
$clause
ORDER BY KdRegister";

        return $this->NumResult($sql);
    }

   
    function getTourLeader() {
        $sql = "SELECT KdTourLeader,CONCAT(KdTourLeader,' - ',Nama)AS Nama,NoIdentitas,JenisIdentitas FROM tourleader";
        return $this->getArrayResult($sql);
    }

    function getNewNo($tahun,$bulan) {
        $sql = "SELECT NoKomisi FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
        return $this->getRow($sql);
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function NamaPrinter($id) {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
        return $this->getRow($sql);
    }

    function getTotalNetto($no) {
        $sql = "SELECT SUM(Netto) FROM transaksi_detail WHERE NoStruk ='$no'";
        return $this->getArrayResult($sql);
    }

    function getWaktu($no) {
        $sql = "SELECT MIN(waktu) AS mulai, MAX(waktu) AS akhir FROM transaksi_detail WHERE NoStruk = '$no'";
        return $this->getArrayResult($sql);
    }
    
    function cekStiker($nostiker,$tgl)
	{
    	$sql = "
    			SELECT * FROM register a WHERE a.`Tanggal`='$tgl' AND a.`NoStiker`='$nostiker';
    		   ";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
	function cekLimitStiker($nostiker,$tgl)
	{
    	$sql = "
    			SELECT a.`Waktu` FROM ticket a WHERE a.`noidentitas`='$nostiker' AND DATE(a.`Waktu`)='$tgl' ORDER BY a.`noticket` DESC LIMIT 0,1;
    		   ";
		return $this->getRow($sql);
    }

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function ifPCodeBarcode($id) {
        $bar = substr($id, 0, 10);
        $sql = "SELECT KdRekening FROM rekening Where KdRekening='$id'";
        return $this->getRow($sql);
    }

    function getPCodeDet($kode, $field) {
        $sql = "select KdRekening, NamaRekening from rekening where KdRekening='$kode'";
        return $this->getRow($sql);
    }

}

?>
