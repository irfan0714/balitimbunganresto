<?php
class Kunjungan_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getWarehouse()
	{
    	$sql = "SELECT a.warehousecode,a.warehousename FROM warehouse a ORDER BY a.warehousecode ASC";
		return $this->getArrayResult($sql);
    }
	   
    function num_kunjungan_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `kunjungan_header` a;       
		";
		                  
        return $this->NumResult($sql);
	}
		
	function getKunjunganList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_salesman = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.noku";
				$arr_keyword[1] = "a.note";
				$arr_keyword[2] = "b.NamaSalesman";
				$arr_keyword[3] = "a.status";
				$arr_keyword[4] = "b.KdSalesman";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
						
			if($arrSearch["salesman"]!="")
			{
				$where_salesman = "AND b.KdSalesman = '".$arrSearch["salesman"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	/*$sql = "  
            SELECT a.`dono`,a.`dodate`,a.`warehousecode`,c.`Keterangan`,a.`customerid`,b.`Nama`,a.`contactperson`,a.`note`,a.`status` FROM `deliveryorder` a 
		   INNER JOIN `customer` b ON a.`customerid`=b.`KdCustomer`
		   INNER JOIN gudang c ON a.`warehousecode`=c.`KdGudang` 
            WHERE 
            	1  
            	".$where_keyword."
            	".$where_gudang."
            	".$where_customer."    
            	".$where_status."                                   
            ORDER BY 
              a.dono DESC 
            Limit 
              $offset,$limit
        "; */
			$sql="
				SELECT DATE_FORMAT(a.tgl_kunjungan, '%d-%m-%Y') AS tgl,
			    a.*,b.* FROM kunjungan_header a INNER JOIN salesman b ON a.KdSalesman = b.KdSalesman 
			    WHERE 1 AND a.status ='1' 
				".$where_keyword."
            	".$where_salesman."    
            	".$where_status."                                   
            ORDER BY 
              a.noku DESC 
            Limit 
              $offset,$limit
				";
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			  DATE_FORMAT(a.tgl_kunjungan, '%d-%m-%Y') AS dodate,
			  a.*
			FROM
			  kunjungan_header a 
			  INNER JOIN salesman b 
				ON a.`KdSalesman` = b.`KdSalesman` 
			WHERE a.noku = '".$id."' ;
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail($kdtour,$nodok)
	{
		$sql = "
			SELECT 
			  kunjungan_detail.* 
			FROM
			  kunjungan_detail
			WHERE 1 
			  AND kunjungan_detail.noku = '".$nodok."' 
			  AND kunjungan_detail.KdTour = '".$kdtour."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1  
			  AND deliveryorderdetail.dono = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($id)
	{
		$sql = "
			SELECT * FROM kunjungan_detail a 
			INNER JOIN tourtravel b ON a.KdTour=b.KdTravel WHERE a.noku='".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  `deliveryorderdetail` a 
			  INNER JOIN masterbarang b
			  ON a.`inventorycode`=b.`PCode`
			WHERE 1 
			  AND a.`dono` = '".$id."' 
			ORDER BY a.`sid` ASC ;
		";
        return $this->getArrayResult($sql);
	}
    
    
    function getSalesman()
	{
    	$sql = "SELECT * FROM salesman a ORDER BY a.NamaSalesman ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>