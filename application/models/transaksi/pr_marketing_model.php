<?php
class Pr_marketing_model extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }
	
	function getPrDetail($nodok)
	{
    	$sql = "SELECT * FROM pr_marketing_detail WHERE pr_marketing_detail.NoDokumen='".$nodok."' ORDER BY NoUrut ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function get_pr_marketing_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	
		$sql="SELECT 
			  divisi.*,
			  pr_marketing.*,
			  DATE_FORMAT(
			    pr_marketing.TglDokumen,
			    '%d-%m-%Y'
			  ) AS TglDokumen_,
			  DATE_FORMAT(
			    pr_marketing.TglTerima,
			    '%d-%m-%Y'
			  ) AS TglTerima_ 
			FROM
			  pr_marketing 
			  INNER JOIN divisi 
			    ON pr_marketing.KdDivisi = divisi.KdDivisi $clause 
			ORDER BY pr_marketing.TglDokumen DESC 
			LIMIT $offset,$num";

			// echo $sql;
			// die();
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_pr_marketing_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT * FROM pr_marketing INNER JOIN divisi ON pr_marketing.KdDivisi = divisi.KdDivisi $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getNoProposal($NoProposal)
	{
    	$sql = "
    	SELECT 
		  * 
		FROM
		  proposal 
		WHERE `Status_approve1` = '1' 
		  AND `Status_approve2` = '1' 
		  AND `Status_approve3` = '1'
		  AND (
		    NoProposal LIKE '%".$NoProposal."%' 
		    OR NamaProposal LIKE '%".$NoProposal."%'
		  ) 
		ORDER BY NoProposal ASC ;
    	";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function cek_nil_proposal($nopr)
	{
    	$sql = "SELECT * FROM pr_marketing_detail WHERE pr_marketing_detail.NoDokumen='".$nodok."' ORDER BY NoUrut ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getAktivitasList()
	{
    	$sql = "SELECT * FROM aktivitas ORDER BY NamaAktivitas ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    public function get_by_pr($nodok)
	{
		/*$this->db->select('pr_marketing.*,proposal.*,divisi.*');
		$this->db->from('pr_marketing INNER JOIN proposal ON proposal.NoProposal = pr_marketing.NoProposal
									  INNER JOIN divisi ON divisi.KdDivisi = pr_marketing.KdDivisi
		');
		$this->db->where('pr_marketing.NoDokumen',$nodok);
		$query = $this->db->get();
		return $query->row();*/
		
		
		$sql = "
			SELECT 
			pr_marketing.*,
			pr_marketing.`Status` AS Sts,
			proposal.*,
			divisi.*,
			DATE_FORMAT(pr_marketing.TglDokumen, '%d-%m-%Y') AS TglDokumen_,
			DATE_FORMAT(pr_marketing.TglTerima, '%d-%m-%Y') AS TglTerima_
			FROM
			pr_marketing LEFT JOIN proposal ON proposal.NoProposal = pr_marketing.NoProposal
			INNER JOIN divisi ON divisi.KdDivisi = pr_marketing.KdDivisi 
			WHERE pr_marketing.NoDokumen='".$nodok."';
			   ";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
		
		
        
	}
	
	function getHeader($id)
	{
		$sql = "
			SELECT 
			pr_marketing.*,
			proposal.*,
			divisi.*,
			DATE_FORMAT(pr_marketing.TglDokumen, '%d-%m-%Y') AS TglDokumen_,
			DATE_FORMAT(pr_marketing.TglTerima, '%d-%m-%Y') AS TglTerima_
			FROM
			pr_marketing INNER JOIN proposal ON proposal.NoProposal = pr_marketing.NoProposal
			INNER JOIN divisi ON divisi.KdDivisi = pr_marketing.KdDivisi 
			WHERE pr_marketing.NoDokumen='".$id."';
        ";
        
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	
	function getHeaderPDF($id)
	{
		$sql = "
			SELECT 
			  pr_marketing.*,
			  divisi.`NamaDivisi` 
			FROM
			  pr_marketing 
			  INNER JOIN divisi 
			    ON pr_marketing.`KdDivisi` = divisi.`KdDivisi` 
			WHERE pr_marketing.NoDokumen = '".$id."' ;
        ";
        
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	
	function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  pr_marketing_detail 
			  WHERE pr_marketing_detail.NoDokumen= '".$id."';
		";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getDivisi()
	{
    	$sql = "SELECT * FROM divisi a  ORDER BY a.KdDivisi ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getProposal()
	{
    	$sql = "SELECT * FROM proposal WHERE `Status_approve1` = '1' AND `Status_approve2` = '1' AND `Status_approve3` = '1' ORDER BY NoProposal ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	
}
?>