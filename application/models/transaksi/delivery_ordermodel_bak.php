<?php
class delivery_ordermodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getDeliveryOrderList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "DONo";
				$arr_keyword[1] = "Nama";
				$arr_keyword[2] = "Note";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "  
            SELECT DoNo, DODate, Nama, g.`Keterangan` FROM deliveryorder d INNER JOIN customer c ON d.`customerid`=c.`KdCustomer`
			INNER JOIN gudang g ON d.`warehousecode`=g.`KdGudang`
            WHERE 
            	1  
            	".$where_keyword."                                  
            ORDER BY 
              DONo DESC 
            Limit 
              $offset,$limit
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_deliveryorder_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "DONo";
				$arr_keyword[1] = "Nama";
				$arr_keyword[2] = "Note";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "  
            SELECT DoNo, DODate, Nama, g.`Keterangan` FROM deliveryorder d INNER JOIN customer c ON d.`customerid`=c.`KdCustomer`
			INNER JOIN gudang g ON d.`warehousecode`=g.`KdGudang`
            WHERE 
            	1  
            	".$where_keyword."                                  
            ORDER BY 
              DONo DESC 
        ";
		                  
        return $this->NumResult($sql);
	}

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		return $this->getRow($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	
	function getCustomer()
	{
		$sql = "SELECT KdCustomer, Nama FROM customer order by nama";
    	return $this->getArrayResult($sql);
    }
    
    function getWarehouse()
	{
		$sql = "SELECT KdGudang, Keterangan FROM gudang order by Keterangan";
    	return $this->getArrayResult($sql);
    }
    
    function getBarang($searchby, $searchstring){
    	if($searchby=='PCode'){
			$where = "b.PCode like '%" .$searchstring."%'";
		}else{
			$where = "b.NamaLengkap like '%" .$searchstring."%'";
		}
		$sql = "SELECT b.`PCode`, b.`NamaLengkap`, f.`RemainQuantity`, f.`BatchNumber`, f.`productionid`, f.`productionid2`, ProductionDate
				FROM productionfinishgood f INNER JOIN masterbarang b ON f.`inventorycode`=b.`PCode`
				where $where";
		return $this->getArrayResult($sql);
	}
	
    function getNoTransaksi($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoTransaksi FROM pelunasan_hutang_header WHERE NoTransaksi LIKE '%$blnthn' ORDER BY NoTransaksi DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = 'PH00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoTransaksi'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$notransaksi = 'PH' . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
    
    function getDetail($KdSupplier,$MataUang)
	{
		$sql = "SELECT h.`NoFaktur`, i.`NoFakturSupplier`, i.`NoPO`, h.`Tanggal`, h.`Sisa`  
				FROM hutang h LEFT JOIN invoice_pembelian_header i  ON h.`NoFaktur`=i.`NoFaktur`
				WHERE h.KdSupplier ='$KdSupplier' AND h.MataUang='$MataUang' AND h.TipeTransaksi='I' and h.sisa>0 order by h.Tanggal";
		
				
    	return $this->getArrayResult($sql);
    	
    }
    
    function insertHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan,$user, $nopv)
    {
        $this->locktables('pelunasan_hutang_header');

        $data = array(
            'NoTransaksi'	=> $notransaksi,
            'NoPaymentVoucher' => $nopv,
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'KdSupplier' => $kdSupplier,
            'MataUang' => $kd_uang,
            'Kurs' => $kurs,
            'NoBukti' => $nobukti,
            'Keterangan' => $keterangan,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('pelunasan_hutang_header', $data);

        $this->unlocktables();
    }
    
    function insertDetail($notransaksi,$nofaktur,$sisa,$bayar){
    	$mylib = new globallib();
		$this->locktables('pelunasan_hutang_detail');
		$total = 0;
		for($i=0;$i<count($nofaktur); $i++){
		
			$data = array(
				'NoTransaksi' => $notransaksi,
				'NoFaktur' => $nofaktur[$i],
				'NilaiFaktur' => $mylib->save_int($sisa[$i]),
				'NilaiBayar' => $mylib->save_int($bayar[$i])
			);
			$total += $mylib->save_int($bayar[$i], 'ind');
			$this->db->insert('pelunasan_hutang_detail', $data);
		}
		$this->unlocktables();
		return $total;
	}
	
	function insertHutang($tgl,$notransaksi, $nofaktur, $kdsupplier,$kd_uang,$kurs, $bayar){
		$mylib = new globallib();
		$this->locktables('hutang');
		for($i=0;$i<count($nofaktur); $i++){
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				$data = array(
				'NoDokumen' => $notransaksi,
				'NoFaktur' => $nofaktur[$i], 
				'KdSupplier' => $kdsupplier,
				'TipeTransaksi' => 'P',
				'Tanggal' => $tgl,
				'JatuhTempo' => $tgl,
				'NilaiTransaksi' => $vbayar,
				'Sisa' => 0,
				'MataUang' => $kd_uang,
				'Kurs' => $kurs
				);
				$this->db->insert('hutang', $data);
			}
		}
		$this->unlocktables();
	}
	
	function UpdateHutang($nofaktur,$bayar){
		$mylib = new globallib();
		$this->locktables('hutang');
		for($i=0;$i<count($nofaktur); $i++){
			$nodokumen = $nofaktur[$i];
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				
				$sql = "Update hutang set sisa = sisa-$vbayar where NoDokumen='$nodokumen'";
				$qry = $this->db->query($sql);
				
			}
		}
		$this->unlocktables();
	}
	
	function insertMutasiHutang($tgl,$nofaktur, $kdsupplier,$mata_uang,$kurs, $gtotal){
		$mylib = new globallib();
		$this->locktables('mutasi_hutang');
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$data = array(
			'Tahun' => $tahun,
			'Bulan' => $bulan, 
			'NoDokumen' => $nofaktur, 
			'MataUang' => $mata_uang,
			'Kurs' => $kurs,
			'KdSupplier' => $kdsupplier,
			'TipeTransaksi' => 'P',
			'Jumlah' => $gtotal
		);
		$this->db->insert('mutasi_hutang', $data);
		$this->unlocktables();
	}
	
	function getDataHeader($notransaksi){
    	$sql = "SELECT h.`NoTransaksi`, h.`NoPaymentVoucher`, h.`TglDokumen`, k.`NamaKasBank`, s.`Nama` AS NamaSupplier,
				h.`MataUang`, h.`Kurs`, h.`NoBukti`, h.`Keterangan`, h.`AddDate`, h.`AddUser`, h.`EditDate`, h.`EditUser` FROM pelunasan_hutang_header h 
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				INNER JOIN kasbank k ON h.`KdKasBank`=k.`KdKasBank`
				WHERE h.`NoTransaksi`='$notransaksi'";
        $row = $this->getArrayResult($sql);
        return $row[0];
	}
	
	function getDataDetail($notransaksi){
    	$sql = "SELECT p.`NoFaktur`, i.`NoFakturSupplier`, i.`NoPO`, i.`Tanggal`, p.`NilaiFaktur`, p.`NilaiBayar` FROM pelunasan_hutang_detail p
				LEFT JOIN invoice_pembelian_header i ON p.`NoFaktur`=i.`NoFaktur`
				WHERE p.`NoTransaksi`='$notransaksi'";
        return $this->getArrayResult($sql);
	}

	function getNoPV($kdkasbank,$tgl){
		//$this->komisi_model->locktables('keuangan_pvheader');
		$mylib = new globallib();
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$kd_no = $this->getKodeBank($kdkasbank);
		$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoDokumen FROM trans_payment_header WHERE NoDokumen LIKE '$kd_no%$blnthn' ORDER BY NoDokumen DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = $kd_no . '000001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoDokumen'],2,6);
			$nourut = 1000001 + ($nourut*1);
			$nourut = substr($nourut,-6);
			$notransaksi = $kd_no . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
	
	function insertPVHeader($nopv,$tgl,$kdkasbank,$kdSupplier,$keterangan,$total,$nobukti,$user){
    
        $this->locktables('trans_payment_header');

        $data = array(
            'NoDokumen'	=> $nopv,
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'Penerima' => $this->getNamaSupplier($kdSupplier),
            'Keterangan' => $keterangan,
            'JumlahPayment' => $total,
            'Jenis' => 1,
            'Status' => 1,
            'NoBukti' => $nobukti,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('trans_payment_header', $data);

        $this->unlocktables();
    }
    
	function insertPVDetail($nopv,$tgl,$kdSupplier,$bayar,$nobukti,$nofaktur, $user){
		$mylib = new globallib();
		$this->locktables('trans_payment_detail');
		for($i=0;$i<count($nofaktur); $i++){
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => '21020000', // Hutang Dagang Pihak ke 3
				'Jumlah' => $vbayar,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Pelunasan Hutang ' . $this->getNamaSupplier($kdSupplier) . '-' . $nofaktur[$i],
				'Urutan' => $i+1,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_payment_detail', $data);
			}
		}
		$this->unlocktables();
	}
	
	function getKodeBank($kdkasbank){
		$sql = "SELECT KdPembayaran FROM kasbank where KdKasBank='$kdkasbank'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['KdPembayaran'];
	}
	
	function getNamaSupplier($kdsupplier){
		$sql = "SELECT Nama FROM supplier where KdSupplier='$kdsupplier'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['Nama'];
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>