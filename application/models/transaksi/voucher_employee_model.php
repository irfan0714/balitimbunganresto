<?php
class Voucher_employee_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getVoucherEmployee()
	{
		$sql = "
				SELECT 
				  * 
				FROM
				  voucher_employee a 
				WHERE 1 
				  AND MONTH(a.`expDate`) = '".date('m')."' 
				  AND YEAR(a.`expDate`) = '".date('Y')."' 
				ORDER BY a.`nik` ASC 
			   "; 
    	return $this->getArrayResult($sql);
    }

    function searchData($search_keyword,$search_status,$bulan,$tahun)
	{

		$where1 = "";
		if($search_keyword != ""){
			$where1 = ' AND nik LIKE "%'.$search_keyword.'%" OR keterangan LIKE "%'.$search_keyword.'%" OR nominal LIKE "%'.$search_keyword.'%"  ';  
		}

		$where2 = "";
		if($search_status != ""){
			$where2 = ' AND Status = "'.$search_status.'" ';
		}

		$where3 = "";
		if($bulan != ""){
			$where3 = ' AND MONTH(expDate) = "'.$bulan.'" ';
		}

		$where4 = "";
		if($tahun != ""){
			$where4 = ' AND YEAR(expDate) = "'.$tahun.'" ';
		}

		$sql = "SELECT 
				  * 
				FROM
				  voucher_employee a 
				WHERE 1 
				  ".$where2."
				  ".$where3."
				  ".$where4."
				  ".$where1."
				ORDER BY a.`nik` ASC 
			   "; 
    	return $this->getArrayResult($sql);
    }

    public function upload_file($filename){
	    $this->load->library('upload'); 
	    
	    $config['upload_path'] = './upload/excel_voucher_employee/';
	    $config['allowed_types'] = 'xls';
	    $config['max_size']  = '2048';
	    $config['overwrite'] = true;
	    $config['file_name'] = $filename;
	  
	    $this->upload->initialize($config); 
	    if($this->upload->do_upload('file')){ 
	      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	      return $return;
	    }else{
	      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
	      return $return;
	    }
	}

    function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
    
}
?>