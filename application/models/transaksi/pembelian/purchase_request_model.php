<?php
class Purchase_request_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";
        return $this->getRow($sql);
	}

	function ambilQty($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_pr_detail INNER JOIN
			  trans_pr_header ON trans_pr_header.`NoDokumen` = trans_pr_detail.`NoDokumen`
			WHERE 1
			  AND trans_pr_detail.PCode = '".$pcode."'
			  AND trans_pr_detail.NoDokumen = '".$nodok."'
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

	function cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$fieldupdate.",a.".$fieldnupdate.",a.".$fieldakhir.",a.".$fieldnakhir." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$KdGudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

	function ambilDetail($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_pr_detail INNER JOIN
			  trans_pr_header ON trans_pr_header.`NoDokumen` = trans_pr_detail.`NoDokumen`
			WHERE 1
			  AND trans_pr_detail.NoDokumen = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}

    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();

	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }

        $where_keyword="";
        $where_gudang="";
        $where_divisi = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_pr_header.NoDokumen";
				$arr_keyword[1] = "trans_pr_header.Keterangan";
				$arr_keyword[2] = "trans_pr_header.NoPermintaan";
				$arr_keyword[3] = "gudang.Keterangan";
				$arr_keyword[4] = "divisi.NamaDivisi";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_pr_header.KdGudang = '".$arrSearch["gudang"]."'";
			}

			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND trans_pr_header.KdDivisi = '".$arrSearch["divisi"]."'";
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_pr_header.Status = '".$arrSearch["status"]."'";
			}
		}

    	$sql = "
    		SELECT
			  trans_pr_header.NoDokumen,
			  DATE_FORMAT(trans_pr_header.TglDokumen, '%d-%m-%Y') AS Tanggal,
			  trans_pr_header.NoPermintaan,
              DATE_FORMAT(trans_pr_header.TglTerima,'%d-%m-%Y') AS TglTerima,
			  trans_pr_header.Keterangan,
			  trans_pr_header.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_pr_header.KdDivisi,
			  divisi.NamaDivisi,
			  trans_pr_header.Status
			FROM
			  trans_pr_header
			  INNER JOIN gudang
			    ON trans_pr_header.KdGudang = gudang.KdGudang
			  INNER JOIN divisi
			    ON trans_pr_header.KdDivisi = divisi.KdDivisi
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_divisi."
            	".$where_status."
			ORDER BY
			  trans_pr_header.AddDate DESC,
			  trans_pr_header.TglDokumen DESC,
			  trans_pr_header.NoDokumen DESC
            Limit
              $offset,$limit
        ";
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }

    function num_tabel_row($arrSearch)
    {
        $mylib = new globallib();

        $where_keyword="";
        $where_gudang="";
        $where_divisi = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_pr_header.NoDokumen";
				$arr_keyword[1] = "trans_pr_header.Keterangan";
				$arr_keyword[2] = "trans_pr_header.NoPermintaan";
				$arr_keyword[3] = "gudang.Keterangan";
				$arr_keyword[4] = "divisi.NamaDivisi";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_pr_header.KdGudang = '".$arrSearch["gudang"]."'";
			}

			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND trans_pr_header.KdDivisi = '".$arrSearch["divisi"]."'";
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_pr_header.Status = '".$arrSearch["status"]."'";
			}
		}

		$sql = "
			SELECT
			  trans_pr_header.NoDokumen,
			  trans_pr_header.TglDokumen,
			  trans_pr_header.NoPermintaan,
			  trans_pr_header.TglTerima,
			  trans_pr_header.Keterangan,
			  trans_pr_header.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_pr_header.KdDivisi,
			  divisi.NamaDivisi,
			  trans_pr_header.Status
			FROM
			  trans_pr_header
			  INNER JOIN gudang
			    ON trans_pr_header.KdGudang = gudang.KdGudang
			  INNER JOIN divisi
			    ON trans_pr_header.KdDivisi = divisi.KdDivisi
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_divisi."
            	".$where_status."
		";

        return $this->NumResult($sql);
	}

	function getNopb($keyword="")
	{
		$where_keyword = "";
		if($keyword)
		{
			$where_keyword = "AND permintaan_barang_header.NoDokumen LIKE '%".$keyword."%'";
		}

    	$sql = "
    		SELECT
			  permintaan_barang_header.NoDokumen
			FROM
			  permintaan_barang_header
			  LEFT JOIN trans_pr_header
			    ON permintaan_barang_header.NoDokumen = trans_pr_header.NoPermintaan
			WHERE 1
			  AND permintaan_barang_header.Status = '1'
			  AND trans_pr_header.NoPermintaan IS NULL
			  ".$where_keyword."
			GROUP BY
			  permintaan_barang_header.NoDokumen
			ORDER BY
			  permintaan_barang_header.AddDate DESC,
			  permintaan_barang_header.TglDokumen DESC,
			  permintaan_barang_header.NoDokumen ASC
		";

        return $this->getArrayResult($sql);
    }

		function getNoML($keyword="")
		{
			$where_keyword = "";
			if($keyword)
			{
				$where_keyword = "AND market_list_barang_header.NoDokumen LIKE '%".$keyword."%'";
			}

	    	$sql = "
	    		SELECT
				  market_list_barang_header.NoDokumen
				FROM
				  market_list_barang_header
				  LEFT JOIN trans_pr_header
				    ON market_list_barang_header.NoDokumen = trans_pr_header.NoPermintaan
				WHERE 1
				  AND market_list_barang_header.Status = '1'
				  AND trans_pr_header.NoPermintaan IS NULL
				  ".$where_keyword."
				GROUP BY
				  market_list_barang_header.NoDokumen
				ORDER BY
				  market_list_barang_header.AddDate DESC,
				  market_list_barang_header.TglDokumen DESC,
				  market_list_barang_header.NoDokumen ASC
			";

	        return $this->getArrayResult($sql);
	    }

	function getDetailPb($id)
	{
		$sql = "
			SELECT
			  permintaan_barang_header.AddUser,
			  DATE_FORMAT(permintaan_barang_header.AddDate, '%d-%m-%Y') AS Tanggal,
			  DATE_FORMAT(permintaan_barang_header.TglTerima, '%d-%m-%Y') AS TglTerima,
			  permintaan_barang_header.KdDivisi,
			  divisi.NamaDivisi,
			  gudang.Keterangan AS nama_gudang,
			  permintaan_barang_header.Keterangan
			FROM
			  permintaan_barang_header
			  INNER JOIN gudang
			    ON permintaan_barang_header.KdGudang = gudang.KdGudang
			  INNER JOIN divisi
			    ON permintaan_barang_header.KdDivisi = divisi.KdDivisi
			WHERE 1
			  AND NoDokumen = '".$id."'
		";
		return $this->getRow($sql);
	}

	function getDetailML($id)
	{
		$sql = "
			SELECT
			  market_list_barang_header.AddUser,
			  DATE_FORMAT(market_list_barang_header.AddDate, '%d-%m-%Y') AS Tanggal,
			  DATE_FORMAT(market_list_barang_header.TglTerima, '%d-%m-%Y') AS TglTerima,
			  market_list_barang_header.KdDivisi,
			  divisi.NamaDivisi,
			  gudang.Keterangan AS nama_gudang,
			  market_list_barang_header.Keterangan
			FROM
			  market_list_barang_header
			  INNER JOIN gudang
			    ON market_list_barang_header.KdGudang = gudang.KdGudang
			  INNER JOIN divisi
			    ON market_list_barang_header.KdDivisi = divisi.KdDivisi
			WHERE 1
			  AND NoDokumen = '".$id."'
		";
		return $this->getRow($sql);
	}

	function getDetailPCodeML($id)
	{
		$sql="SELECT market_list_barang_detail.*,satuan.NamaSatuan FROM market_list_barang_detail
			INNER JOIN satuan ON satuan.KdSatuan = market_list_barang_detail.Satuan
			WHERE 1 AND NoDokumen = '".$id."' ORDER BY sid ASC";
		return $this->getArrayResult($sql);
	}

	function getDetailPCodePb($id)
	{
		$sql="SELECT permintaan_barang_detail.*,satuan.NamaSatuan FROM permintaan_barang_detail
			INNER JOIN satuan ON satuan.KdSatuan = permintaan_barang_detail.Satuan
			WHERE 1 AND NoDokumen = '".$id."' ORDER BY sid ASC";
		return $this->getArrayResult($sql);
	}

	function getDetailUnion($nodok,$nopermintaan)
	{
		$sql = "
			SELECT
			  *
			FROM
			  (SELECT
			    trans_pr_detail.PCode,
			    trans_pr_detail.NamaBarang,
			    trans_pr_detail.QtyPermintaan,
			    trans_pr_detail.Qty,
			    trans_pr_detail.QtyPcs,
			    trans_pr_detail.Satuan,
				satuan.NamaSatuan,
			    trans_pr_detail.QtyPcsTerima
			  FROM
			    trans_pr_detail INNER JOIN satuan ON satuan.`KdSatuan` = trans_pr_detail.`Satuan`
			  WHERE 1
			    AND trans_pr_detail.NoDokumen = '".$nodok."'
			  UNION
			  ALL
			  SELECT
			    permintaan_barang_detail.PCode,
			    permintaan_barang_detail.NamaBarang,
				satuan.NamaSatuan,
			    '0' AS QtyPermintaan,
			    permintaan_barang_detail.Qty,
			    '0' AS QtyPcs,
			    permintaan_barang_detail.Satuan,
			    '0' AS QtyPcsTerima
			  FROM
			   permintaan_barang_detail INNER JOIN satuan ON satuan.`KdSatuan` = permintaan_barang_detail.`Satuan`
			  WHERE 1
			    AND NoDokumen = '".$nopermintaan."') AS tb_1
			GROUP BY tb_1.PCode
			ORDER BY tb_1.QtyPermintaan * 1 > 0 DESC
		";

		return $this->getArrayResult($sql);
	}

	function getDetailUnionML($nodok,$nopermintaan)
	{
		$sql = "
			SELECT
			  *
			FROM
			  (SELECT
			    trans_pr_detail.PCode,
			    trans_pr_detail.NamaBarang,
			    trans_pr_detail.QtyPermintaan,
			    trans_pr_detail.Qty,
			    trans_pr_detail.QtyPcs,
			    trans_pr_detail.Satuan,
				satuan.NamaSatuan,
			    trans_pr_detail.QtyPcsTerima
			  FROM
			    trans_pr_detail INNER JOIN satuan ON satuan.`KdSatuan` = trans_pr_detail.`Satuan`
			  WHERE 1
			    AND trans_pr_detail.NoDokumen = '".$nodok."'
			  UNION
			  ALL
			  SELECT
			    market_list_barang_detail.PCode,
			    market_list_barang_detail.NamaBarang,
				satuan.NamaSatuan,
			    '0' AS QtyPermintaan,
			    market_list_barang_detail.Qty,
			    '0' AS QtyPcs,
			    market_list_barang_detail.Satuan,
			    '0' AS QtyPcsTerima
			  FROM
			   market_list_barang_detail INNER JOIN satuan ON satuan.`KdSatuan` = market_list_barang_detail.`Satuan`
			  WHERE 1
			    AND NoDokumen = '".$nopermintaan."') AS tb_1
			GROUP BY tb_1.PCode
			ORDER BY tb_1.QtyPermintaan * 1 > 0 DESC
		";

		return $this->getArrayResult($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }

	function getGudang()
	{
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
		return $this->getArrayResult($sql);
    }

	function getGudang2($gudang)
	{
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang where 1 and KdGudang='".$gudang."'order by KdGudang ";
        return $this->getRow($sql);
    }

	function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }

	function getHeader($id)
	{
		$sql = "
			SELECT
			  trans_pr_header.NoDokumen,
			  trans_pr_header.NoPermintaan,
			  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
			  DATE_FORMAT(TglTerima, '%d-%m-%Y') AS TglTerima,
			  trans_pr_header.FlagKonfirmPO,
			  trans_pr_header.KdGudang,
			  gudang.Keterangan AS NamaGudang,
			  trans_pr_header.KdDivisi,
			  divisi.NamaDivisi,
			  trans_pr_header.Status,
			  trans_pr_header.Keterangan,
			  DATE_FORMAT(trans_pr_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_pr_header.AddUser,
			  DATE_FORMAT(trans_pr_header.EditDate, '%d-%m-%Y') AS EditDate,
			  trans_pr_header.EditUser
			FROM
			  trans_pr_header
			  LEFT JOIN gudang
			    ON gudang.KdGudang = trans_pr_header.KdGudang
			  LEFT JOIN divisi
			    ON divisi.KdDivisi = trans_pr_header.KdDivisi
			WHERE 1
			  AND trans_pr_header.NoDokumen = '".$id."'
			LIMIT 1
        ";

        return $this->getRow($sql);
	}

	function getDetail($id)
	{
		$sql = "
			SELECT
			  trans_pr_detail.*
			FROM
			  trans_pr_detail
			WHERE 1
			  AND trans_pr_detail.NoDokumen = '$id'
			ORDER BY
			  trans_pr_detail.sid DESC
		";

        return $this->getArrayResult($sql);
	}

	function cekNodok($id)
	{

		$sql = "
			SELECT
  			  trans_order_barang_header.NoDokumen,
			  trans_order_barang_header.NoPr,
			  DATE_FORMAT(trans_order_barang_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_order_barang_header.AddUser
			FROM
			  trans_order_barang_header
			WHERE 1
			  AND trans_order_barang_header.NoPr = '".$id."'
		";
		return $this->getRow($sql);
	}

	function getStok($field1,$field2,$field3,$field4,$tahun,$gudang,$pcode,$status)
	{
		$sql = "SELECT $field1,$field2,$field3,$field4 from stock_simpan where Tahun='$tahun'
		and KdGudang='$gudang' and PCode='$pcode' and Status='$status'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>
