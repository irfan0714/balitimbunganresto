<?php

class pos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function masterbarang() {
        $sql = "SELECT PCode, NamaLengkap from masterbarang order by NamaLengkap ASC";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
	function GetGudang($id) {
        $sql = "SELECT * FROM kassa WHERE id_kassa='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['KdGudang'];
    }
    
    function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function getGsaList() {
        $sql = "SELECT * FROM `gsa_list` a LEFT JOIN warna b ON a.warna=b.id_warna ORDER BY b.`id_warna` ASC;";
        return $this->getArrayResult($sql);
		
    }
    
    function getKartu() {
        $sql = "SELECT * FROM `kartu` a WHERE a.tipe<>'EDC' ORDER BY nama ASC";
        return $this->getArrayResult($sql);
		
    }
    
    function getEDC() {
        $sql = "SELECT * FROM `kartu` a WHERE a.tipe='EDC' ORDER BY nama ASC";
        return $this->getArrayResult($sql);
		
	}
	
	function ambil_point($id_members) {
        $sql = "SELECT * FROM member a WHERE a.`KdMember`='$id_members'";
        return $this->getArrayResult($sql);	
	}
	
	function nilai_point($id_members) {
        $sql = "SELECT 
				  * 
				FROM
				  member a 
				  INNER JOIN type_member b 
				    ON a.`KdTipeMember` = b.`KdTipeMember` 
				WHERE a.`KdMember` = '$id_members'";
        return $this->getArrayResult($sql);	
	}

    function get_voucher_employee($nik, $TglTrans){
        $sql = "SELECT nik,nominal, nominal - terpakai AS sisa FROM voucher_employee WHERE nik = '$nik' AND status = '1' AND expDate > '$TglTrans' AND expDate = LAST_DAY('$TglTrans')";
        $qry = $this->db->query($sql);
        $sisaVoucher = array();
        if ($qry->num_rows() > 0) {
            $row = $qry->result_array();
            $sisaVoucher = array(
                'nik' => $row[0]['nik'],
                'nominal' => $row[0]['nominal'],
                'sisa' => $row[0]['sisa']
            ); 
        }
        return $sisaVoucher;
    }
	
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
	function getBarangDisplay($PCode){
		$sql = "SELECT NamaLengkap from masterbarang_pos WHERE Barcode1='$PCode'" ;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
    function NamaPrinter($id) {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function sales_temp($user) {
        $sql = "select st.NoUrut, st.KodeBarang, mb.NamaLengkap as NamaStruk, st.Qty, st.Harga, st.Netto, st.NoStruk,st.Disc,
                (st.Service_charge * Netto /100) AS charge,'2'
                    from sales_temp st, masterbarang_pos mb
                    where st.KodeBarang = mb.PCode and st.Qty<>'0' and Kasir='$user'
                    order by st.NoUrut asc";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function sales_temp_count($user) {
        $sql = "SELECT COUNT(KodeBarang) as total FROM sales_temp WHERE Qty<>'0' and Kasir='$user'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row;
    }

    function TotalNetto($user) {
        $sql = "SELECT SUM(Netto) AS TotalNetto,
                SUM(IF(Service_charge IS NULL,0,Service_charge) * Netto /100)AS Charge,
                (SUM(Netto) + (SUM(IF(Service_charge IS NULL,0,Service_charge) * Netto /100)))AS  ttlall
                FROM sales_temp where Kasir='$user'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row;
    }
    
    function TotalQty($user) {
        $sql = "select sum(Qty) as TotalQty from sales_temp where Kasir='$user'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row[0]['TotalQty'];
    }
	
	function cekkomisi($pcode) {
        $sql = "select Komisi from masterbarang_pos where pcode='$pcode'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row[0]['Komisi'];
    }

    function DeleteRecord($kd, $kasir) {
//		if($NoUrut == '')
//		{
//			$LastRecord = $this->LastRecord(0);
//			$AutoID     = $LastRecord[0]['AutoID'];
//
//			$sql = "delete from sales_temp where AutoID = '$AutoID'";
//			$qry = $this->db->query($sql);
//		}
//		else
//		{
//			$sql = "delete from sales_temp where NoUrut = '$NoUrut'";
        $sql = "delete from sales_temp where Kasir='$kasir' AND KodeBarang='$kd'";
        $qry = $this->db->query($sql);
//		}
    }

    function LastRecord($echoFlg, $nm) {
        $sql = "select st.AutoID, st.NoUrut, st.KodeBarang, mb.NamaStruk, st.Qty, st.Harga, st.Netto
				from sales_temp st, masterbarang mb
				where st.KodeBarang = mb.PCode and Kasir='$nm'
				order by st.AutoID desc limit 1";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        if ($echoFlg == 1) {
            echo 'datajson = ' . json_encode($row);
        } else {
            return $row;
        }
    }

    function EditRecord($kd) {
        $sql = "select st.AutoID, st.NoUrut, st.KodeBarang, mb.NamaStruk, st.Qty, st.Harga, st.Netto
				from sales_temp st, masterbarang mb
				where st.KodeBarang = mb.PCode and st.KodeBarang = '$kd'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        echo 'datajson = ' . json_encode($row);
    }

    function do_hitung_bonus($no) {
        $sql = "SELECT SUM(Disc+Disc1+Disc2) as ttl FROM transaksi_detail WHERE NoStruk='$no'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['ttl'];
    }

    function sales_temp_cek($kdbrg, $kasir) {
        $sql = "select KodeBarang from sales_temp where KodeBarang='$kdbrg' and Kasir='$kasir'";
        $qry = $this->db->query($sql);
        $row = $qry->num_rows();
        return $row;
    }

    function ambilDataTemp($struk, $kasir) {
        $sql = "SELECT a.Tanggal,a.NoUrut,a.NoStruk,a.KodeBarang,a.Qty,a.Harga,TTL FROM 
            (SELECT * FROM `sales_temp` WHERE NoStruk='$struk' AND Kasir='$kasir')a
            LEFT JOIN 
            (SELECT NoStruk,SUM(Qty * Harga) AS TTL FROM `sales_temp` 
            	WHERE NoStruk='$struk' AND Kasir='$kasir' Group By NoStruk)b 
            ON b.NoStruk=a.NoStruk ";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function CekBonus($PCode, $tgl) {
        $sql = "SELECT * FROM `discount_header` b, discount_detail d
            WHERE Period1 <= '$tgl' AND '$tgl' <=Period2 AND
            b.`KodeDisc`=d.`KodeDisc` AND d.list='$PCode'";

        $qry = $this->db->query($sql);
        return $row = $qry->result_array();
    }

    function sales_temp_add($jumlah, $kdbrg, $Struk, $EditFlg, $kasir, $dis_potongan) {


        if ($EditFlg != 1) {
            $sql = "UPDATE sales_temp SET Qty = Qty + $jumlah, Netto = ((Qty * Harga) - (Qty * Harga * $dis_potongan)) WHERE NoStruk='$Struk' and KodeBarang='$kdbrg' and Kasir='$kasir'";
        } else {
            $sql = "UPDATE sales_temp SET Qty = $jumlah, Netto = Qty * Harga WHERE NoStruk='$Struk' and KodeBarang='$kdbrg' and Kasir='$kasir'";
        }
        $qry = $this->db->query($sql);
    }

	function do_simpan_voucher($no,$nokassa,$tgl,$voucher0,$vouchpakai0,$vouchjenis0, $voucher_bayar) 
	{
        $sql = "INSERT INTO transaksi_detail_voucher(NoKassa,NoStruk,Tanggal,Jenis,NomorVoucher,NilaiVoucher)VALUE
                ('$nokassa','$no','$tgl','$vouchjenis0','$voucher0',$vouchpakai0)"; //tambahin field baru disini
        $qry = $this->db->query($sql);
        
        if($vouchjenis0<>'3' AND $vouchjenis0<>'5')
		{
			$sql = "UPDATE voucher SET terpakai=terpakai+$vouchpakai0 where novoucher='$voucher0'";
			$qry = $this->db->query($sql);
		}
		
		if($vouchjenis0=='5')
		{
			$sql = "UPDATE voucher SET notrans=$no,terpakai=terpakai+$voucher_bayar where novoucher='$voucher0'";
			$qry = $this->db->query($sql);
		}

        //VOUCHER KARYAWAN
        if($vouchjenis0 == '3'){
            $sql = "UPDATE voucher_employee SET terpakai=terpakai+$vouchpakai0 where nik='$voucher0' AND status = '1' AND expDate >= '$tgl' AND expDate <= LAST_DAY(DATE(NOW()))";
            $qry = $this->db->query($sql);
        }
    }
	
	function do_simpan_compliment($id_compliment,$bruto)
	{
		$sql = "UPDATE compliment SET terpakai=terpakai+$bruto where nik='$id_compliment'";
		$qry = $this->db->query($sql);
	}
	
    function do_simpan_detail($no,$gdg,$NoKassa,$nostore,$tgl,$Jamsekarang,$nm,$pcode,$hrg,$hpp,$Service_charge,$komisi,$RupBar,
	              $PPN,$qty,$Netto,$disc,$kdagent) {
            $sql = "INSERT INTO transaksi_detail(NoKassa,Gudang,NoStruk,Tanggal,Waktu,Kasir,PCode,Qty,Harga,Netto,KdStore,
                    STATUS,Disc1,Keterangan,Ketentuan1,Service_charge,Komisi,PPN,HPP,KdAgent)VALUE
                    ('$NoKassa','$gdg','$no','$tgl','$Jamsekarang','$nm','$pcode',$qty,$hrg,$Netto,'$nostore','0',$disc,'$RupBar','0',$Service_charge,$komisi,$PPN,$hpp,'$kdagent')"; //tambahin field baru disini
            $qry = $this->db->query($sql);
    }

    function do_simpan_mutasi($no, $id, $gdg,$nokassa,$tgl,$Jamsekarang,$nm,$pcode,$hrg,$hpp,$Service_charge,$komisi,$RupBar,
	              $PPN,$qty,$Netto,$disc) {
            $sql = "INSERT INTO mutasi
                    (NoKassa,Gudang,NoTransaksi,`Jenis`,`KdTransaksi` ,`Tanggal`,`KodeBarang`,Qty,Nilai,Kasir,HPP,PPN,Service_charge)
                    VALUE
                    ('$nokassa','$gdg','$no','O','R','$tgl','$pcode',$qty,$hrg,'$id',$hpp,$PPN,$Service_charge)"; //tambahin field baru disini
            $qry = $this->db->query($sql);
    }

	//wscrev
    function do_simpan_header($no, $nm,$gdg, $nokassa,$nostore,$tgl,$Jamsekarang,$totalItem,$pembulatan,$totalnya,$dpp,$tax,$disc,$kdagent,$kd_gsa,$userdisc,$nilaidisc,$id_valas,$id_kurs,$ketr, $id_members, $id_card, $nm_card, $jenis_discount) {
      //  $sql = "INSERT INTO transaksi_header(`NoKassa`,Gudang,`NoStruk`,`Tanggal`,`Waktu`,`Kasir`,`TotalItem`,`TotalNilai`,`KdStore`,`Status`,Discount)
		//	SELECT MIN(NoKassa),'$gdg','$no',MIN(Tanggal),MIN(Waktu),MIN(Kasir),COUNT(KodeBarang),(SUM(Netto)+SUM(Service_charge * Netto /100)),MIN(KdStore),MIN(Status),SUM(Disc) FROM sales_temp where kasir='$id' and Qty<>'0'";
        $sql ="INSERT INTO transaksi_header(`NoKassa`,Gudang,`NoStruk`,`Tanggal`,`Waktu`,`Kasir`,`TotalItem`,`TotalNilaiPem`,`TotalNilai`,`KdStore`,`Status`,Discount,DPP,TAX,KdAgent,KdGsa,userdisc,KdMember,NoCard, NamaCard, nilaidisc,Valuta,Kurs,Keterangan)value
                ('$nokassa','$gdg','$no','$tgl','$Jamsekarang','$nm','$totalItem','$pembulatan','$totalnya','$nostore','0','$disc','$dpp','$tax','$kdagent','$kd_gsa','$userdisc','$id_members','$id_card','$nm_card','$nilaidisc','$id_valas','$id_kurs','$ketr')";
        $qry = $this->db->query($sql);
    }

    function clear_kasir($id) {
        $sql = "delete from sales_temp where kasir = '$id'";
        $qry = $this->db->query($sql);
    }

    function clear_trans($id) {
        $sql = "delete from sales_temp where kasir = '$id'";
        $qry = $this->db->query($sql);
    }

    function bayar_trans($nostruk, $cash, $kredit, $debet, $jenis_voucher, $voucher, $total_bayar, $id_customer, $nama_customer, $gender_customer, $tgl_customer, $KdAgent,$service_charge,$valas, $point,$id_kredit,$id_kredit_edc,$id_debet,$id_debet_edc) {
        if (!empty($point)) {
            $param0 = ", Point='$point'";
        } else {
            $param0 = "";
        }
        
        if (!empty($cash)) {
            $param1 = ", Tunai='$cash'";
        } else {
            $param1 = "";
        }

        if (!empty($kredit)) {
            $param2 = ", KKredit='$kredit'";
        } else {
            $param2 = "";
        }

        if (!empty($debet)) {
            $param3 = ", KDebit='$debet'";
        } else {
            $param3 = "";
        }

        if (!empty($voucher)) {
        	
        	if($jenis_voucher=="5"){
				$param4 = ", VoucherTravel='$voucher'";
			}else{
				$param4 = ", Voucher='$voucher'";
			}
            
        } else {
            $param4 = "";
        }
		
		if (!empty($valas)) {
            $param5 = ", Valas='$valas'";
        } else {
            $param5 = "";
        }

        if (!empty($id_customer)) {
            $param6 = ", KdCustomer='$id_customer'";
        } else {
            $param6 = "";
        }

        if (!empty($id_kredit)) {
            $param7 = ", BankKredit='$id_kredit'";
        } else {
            $param7 = "";
        }

        if (!empty($id_kredit_edc)) {
            $param8 = ", EDCBankKredit='$id_kredit_edc'";
        } else {
            $param8 = "";
        }

        if (!empty($id_debet)) {
            $param9 = ", BankDebet='$id_debet'";
        } else {
            $param9 = "";
        }

        if (!empty($id_debet_edc)) {
            $param10 = ", EDCBankDebet='$id_debet_edc'";
        } else {
            $param10 = "";
        }
		
        if(empty($service_charge)){$service_charge =0;}

        $sql = "update transaksi_header set Ttl_Charge=$service_charge ,
                                            TotalBayar=$total_bayar + $service_charge,
                                            Kembali= $total_bayar - (TotalNilai),
                                            Status='0' $param0 $param1 $param2 $param3 $param4 $param5 $param6 $param7 $param8 $param9 $param10
                where NoStruk = '$nostruk'";
        $qry = $this->db->query($sql);

        $sql2 = "update transaksi_detail set Status='0' where NoStruk = '$nostruk'";
        $qry2 = $this->db->query($sql2);
    }

    function save_trans_bayar($nostruk, $nokassa, $nama_customer, $id_kredit, $kredit, $id_debet, $debet, $id_voucher, $voucher, $cash, $gdg,$Uang,$valas,$id_valas,$id_kurs, $id_members, $id_card, $nm_card, $point) {
        if (!empty($nama_customer)) {
            $nama = $nama_customer;
        } else {
            $nama = "";
        }
        
        if (!empty($point)) {
        	$nomor0 = $id_members;
            $jenis0 = "P";
            $nilai0 = $point;
        } else {
        	$nomor0 = "";
            $jenis0 = "";
            $nilai0 = 0;
        }
        
         if (!empty($id_card)) {
        	$nomor01 = $nm_card;
            $jenis01 = "C";
            $nilai01 = 0;
        } else {
        	$nomor01 = "";
            $jenis01 = "";
            $nilai01 = 0;
        }

        if (!empty($cash)) {
            $jenis1 = "T";
            $nilai1 = $cash;
        } else {
            $jenis1 = "";
            $nilai1 = 0;
        }

        if (!empty($id_kredit) and ! empty($kredit)) {
            $nomor2 = $id_kredit;
            $jenis2 = "K";
            $nilai2 = $kredit;
        } else {
            $nomor2 = "";
            $jenis2 = "";
            $nilai2 = 0;
        }

        if (!empty($id_debet) and ! empty($debet)) {
            $nomor3 = $id_debet;
            $jenis3 = "D";
            $nilai3 = $debet;
        } else {
            $nomor3 = "";
            $jenis3 = "";
            $nilai3 = 0;
        }

        if (!empty($id_voucher)) {
            $nomor4 = $id_voucher;
            $jenis4 = "V";
            $nilai4 = $voucher;
        } else {
            $nomor4 = "";
            $jenis4 = "";
            $nilai4 = 0;
        }
		
		if (!empty($id_valas)) {
            $nomor5 = $id_valas;
            $jenis5 = "L";
            $nilai5 = $valas;
        } else {
            $nomor5 = "";
            $jenis5 = "";
            $nilai5 = 0;
        }

        $jenis = $jenis0 . $jenis1 . $jenis2 . $jenis3 . $jenis4 . $jenis5;

        $sql = "insert into transaksi_detail_bayar
                (Gudang,`NoKassa`,`NoStruk`,`Jenis`,`Kode`,`KdMember`,`NomorKKredit`,`NomorKDebet`,`NomorVoucher`,`Keterangan`,`ExpDate`,`NilaiPoint`,`NilaiTunai`,`NilaiKredit`,`NilaiDebet`,NilaiVoucher,Currency,Valas,Valuta,Kurs)
			 	VALUES ('$gdg','$nokassa','$nostruk','$jenis','','$nomor0','$nomor2','$nomor3','$nomor4','$nama','0000-00-00',$nilai0,$nilai1,$nilai2,$nilai3,$nilai4,'$Uang',$nilai5,'$id_valas',$id_kurs)";
        $qry = $this->db->query($sql);
    }

    function no_struk() {
        $sql = "select max(NoStruk) as NoStruk from transaksi_header";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row;
    }

    function no_struk_temp() {
        $sql = "SELECT MAX(NoStruk) as NoStruk FROM sales_temp";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function customer($pelanggan) {
        $sql = "select * from customer where KdCustomer='$pelanggan'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        echo 'datajson = ' . json_encode($row);
    }

    function voucher($id_voucher) {
        $sql = "SELECT novoucher,keterangan,nominal,rupdisc,jenis,STATUS FROM voucher WHERE novoucher='$id_voucher'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
         if(empty($row)){
            echo  "salah";
        }else{
            echo 'datajson = ' . json_encode($row);
        }
       
    }

    function trans_header() {
        $sql = "select * from transaksi_header where Status = '0' order by NoStruk DESC LIMIT 1";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekPCode($kd) {
        $sql = "select * from masterbarang where PCode='$kd'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekStock($kd, $thn) {
        $sql = "select * from stock_simpan where PCode='$kd' and tahun='$thn'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function cekStockGDG($kd, $thn,$gudang) { // cek stock
        $sql = "select * from stock where PCode='$kd' and tahun='$thn' and KdGudang='$gudang'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function cekidkassa($ip) {
        $sql = "SELECT id_kassa,KdStore FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
		//echo $sql;die();
        //return $row[0]['id_kassa'];
    }

    function ambil_No($tahun,$bulan, $tgl, $nokassa) {
    	$tahun = $tahun-2000;
    	$tglhari = substr($tgl, 8, 2)*1;
    	$tglhari = $tglhari<10 ? '0'.$tglhari : $tglhari;
    	
    	$kassa = $nokassa<'10' ? '0'.$nokassa : $nokassa;
    	$nostruk = $tahun.$bulan.$tglhari.$kassa;
    	$bulan = $bulan * 1;
    	
        //$sql = "SELECT NoStruk FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
        $sql = "SELECT NoStruk FROM transaksi_header WHERE nokassa='$nokassa' AND tanggal='$tgl'
        		AND nostruk LIKE '$nostruk%' order by NoStruk Desc limit 1";
		
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if(count($row) == 0){
			$hasil = $nostruk . '001';
		}else{
			$lastnostruk = substr($row[0]['NoStruk'],-3)*1;
			$newno = $lastnostruk + 1;
			if($newno<10){
				$newno = '00'.$newno;
			}elseif($newno<100){
				$newno = '0'.$newno;
			}
			$hasil = $nostruk.$newno;
        }
       
        return $hasil;
    }
     
	 //wscrev
	 function cekno($nokassa,$ketr, $tgl) {
        $sql = "SELECT NoStruk FROM transaksi_header WHERE tanggal='$tgl' and nokassa='$nokassa' and keterangan='$ketr'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
		$hasil = count($row);
        return $hasil;
    }
	
//    function ambil_No($tgl) {
//        $tahun = substr($tgl, 0, 4);
//        $bulan = substr($tgl, 5, 2);
//        $sql = "Update counter set NoStruk=NoStruk+1 where Tahun='$tahun' and Bulan='$bulan'";
//        $this->db->query($sql);
//        $sql1 = "SELECT NoStruk FROM counter where Tahun='$tahun' and Bulan='$bulan'";
//        //echo "SELECT NoStruk FROM counter where Tahun='$tahun' and Bulan='$bulan'";    
//        return $this->getRow($sql1);
//    }

    function cekBarcode($kd) {
        $sql = "select * from masterbarang where Barcode='$kd'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function laststruk($kassa) {
    	$aplikasi = $this->aplikasi();
    	$date = $aplikasi[0]['TglTrans'];
        $sql = "SELECT MAX(NoStruk) as NoStruk FROM transaksi_header WHERE tanggal='$date' AND nokassa='$kassa'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['NoStruk'];
    }
    
    function getDiscCard($NamaCard,$KdStore, $KodeGroup){
		$sql = "
				SELECT 
				  a.`MaxDiscount`,b.*  
				FROM
				  `group_disc_header` a 
				  INNER JOIN `group_disc_detail` b 
				    ON a.`KdGroupDisc` = b.`KdGroupDisc` 
				WHERE 1 
				  AND a.`KdGroupDisc` = '$NamaCard' 
				  AND b.`KdStore` = '$KdStore' 
				  AND b.`KdGroupBarang` = '$KodeGroup' 
				ORDER BY b.`KdGroupBarang` ASC  ;
				";
        return $this->getRow($sql);
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

    function all_trans($id) {
    	$sql = "select h.*, k.IsCounter, m.JumlahPoint, COALESCE(gh.NamaGroupDisc,'') as NamaGroupDisc, 
    				COALESCE(m.NamaMember,'') as NamaMember, tm.NilaiPoint
    				from transaksi_header h inner join kassa k on h.NoKassa = k.id_kassa 
        			left join member m on h.KdMember=m.KdMember
        			left join type_member tm on m.KdTipeMember=tm.KdTipeMember
        			left join group_disc_header gh on h.NamaCard=gh.KdGroupDisc
        			where h.NoStruk = '$id' order by NoStruk DESC LIMIT 1";
        			
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function det_trans($id) {
        $sql = "select a.PCode,b.NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			from transaksi_detail a, masterbarang b where a.NoStruk='$id' and a.Qty<>0 
			and a.PCode=b.Pcode order by Waktu ASC";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        return $row;
    }
    function DetailItemForSales($PCode,$qty,$tgl)
    {
        if(strlen($PCode)<13){
            $q = "and PCode = '$PCode'";
        }else{
            $q = "and Barcode1 = '$PCode'";
        }
        $sql = "SELECT a.*,COALESCE(b.jenis,'N') AS jenisdisc,COALESCE(b.nilai,0) AS nilaidisc,b.NoTrans,IFNULL(c.jenis,'no') AS khusus, m.Status, m.`KdGroupBarang` FROM 
        (select
        PCode, NamaLengkap, Harga1c, Barcode1, Service_charge, Komisi, Jenis, RupBar,
        Perhitungan, Period1, Period2, Beban, NoRekening, Nilai, HadiahBarang,
         Opr1, Nilai1, Opr2, Nilai2, Campur,PPN,$qty as Qty, Harga1c as Netto,HPP,DiscInternal as Disc,
		 DiscLokal, KomisiLokal From
         masterbarang_pos where 1 $q)a
         inner join masterbarang m on a.pcode=m.pcode 
LEFT JOIN
(SELECT pcode,jenis,nilai,NoTrans FROM discountdetail WHERE notrans IN 
(SELECT notrans FROM discountheader WHERE Status='1' AND '$tgl'>=tglawal AND '$tgl'<=tglakhir))b
ON a.pcode=b.pcode
LEFT JOIN
(SELECT pcode,jenis,nilai,NoTrans FROM discountdetail WHERE jenis='X' AND notrans IN 
(SELECT notrans FROM discountheader WHERE Status='1' AND '$tgl'>=tglawal AND '$tgl'<=tglakhir))c
ON a.pcode=c.pcode";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if(empty($row)){
            $hsl =  "salah";
        }elseif($row[0]['Status']=='T'){
			$hsl =  "non aktif";
        }else{
            $hsl =  $row;
        }
        return $hsl;
    }

    function GetGroupHarga($PCode, $GroupHargaId) {
        $sql = "SELECT Harga AS Harga1c FROM groupharga_detail WHERE GroupHargaID = '" . $GroupHargaId . "' AND PCode = '" . $PCode . "' ";

        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if (empty($row)) {
            $hsl[0] = array('Harga1c' => 0);
        } else {
            $hsl = $row;
        }
        return $hsl;
    }

    function GroupHargaName() {
        $sql = "SELECT * FROM groupharga_header WHERE Status = 'O' AND GroupHargaName IN('TOKOPEDIA','SHOPEE') ";
        $qry = $this->db->query($sql);
        $hsl = $qry->result_array();
        return $hsl;
    }
	
	function DetailValas($pvalas){
        $sql = "SELECT NilaiTukar FROM mata_uang WHERE kd_uang='$pvalas'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();

        if(empty($row)){
            $hsl =  "salah";
        }else{
            $hsl =  $row[0]['NilaiTukar'];
        }
        return $hsl;
    }

}

?>