<?php
class Recipe_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
    
	
    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "b.NamaLengkap";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "SELECT r.`resep_id`, r.`PCode`, r.`Satuan`, r.`Status`, r.`Qty`, r.Jenis, b.NamaLengkap
				FROM resep_header r INNER JOIN masterbarang b ON r.`PCode`=b.`PCode` Where 1 $where_keyword LIMIT $offset,$limit";
        
		return $this->getArrayResult($sql);
    }
    
    function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  d.* 
			FROM
			  resep_detail d 
			WHERE 1 
				AND d.resep_id = '$nodok' 
			  AND d.PCode = '$pcode' 
			LIMIT 1
		";
		
        return $this->getRow($sql);
	}
	
	function getresepid($jenis){
		$sql  = "select max(resep_id) as lastid from resep_header where jenis='$jenis'";
		$row = $this->getArrayResult($sql);
		$lastid = $row[0]['lastid'];
		$lastid = substr($lastid,-3)+0;
		$nextid = $lastid + 1;
		
		if($nextid<100){
			$nextid = $jenis.'0'.$nextid;
		}
		
		return $nextid;
	}
    
    
    function num_tabel_row($arrSearch)
    {
         $mylib = new globallib();
        
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "b.NamaLengkap";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "SELECT r.`resep_id`, r.`PCode`, r.`Satuan`, r.`Status`, r.`Qty`, r.Jenis, b.NamaLengkap
				FROM resep_header r INNER JOIN masterbarang b ON r.`PCode`=b.`PCode` Where 1 $where_keyword";
        
		                  
        return $this->NumResult($sql);
	}
		
	function getheader($id){
		$sql = "SELECT h.`resep_id`, h.`PCode`, b.`NamaLengkap`, h.`Status`, h.`Jenis`, h.`Satuan`, h.`Qty` , h.AddUser, h.AddDate, h.EditUser, h.EditDate, b.Harga1c, h.LastCalculateDate
				FROM resep_header h INNER JOIN masterbarang b ON h.`PCode`=b.`PCode`
				WHERE resep_id='$id'";
					
		return $this->getArrayResult($sql);
	}	
	
	function getdetail($id){
		$sql  = "SELECT r.`resep_id`, r.`PCode`, b.`NamaLengkap`, r.`Qty`, r.Satuan, r.Harga
					FROM resep_detail r INNER JOIN masterbarang b ON r.`PCode`=b.`PCode`
					WHERE resep_id='$id'";
					
		return $this->getArrayResult($sql);
	}
	
	function view_value($username){
		$sql = "select user_id from menu_validation where user_id='$username' and user_form='recipe' ";
		return $this->NumResult($sql);
	}
	
	function getdata($jenis, $resepid){
		if($resepid=='all'){
			$sql = "SELECT d.`resep_id`, d.`PCode`, d.`Qty`, d.`Satuan`, d.`Harga` 
				FROM resep_header h INNER JOIN resep_detail d ON h.`resep_id`=d.`resep_id`
				WHERE h.`Jenis`='$jenis' AND `status`=1";
		}else{
			$sql = "SELECT d.`resep_id`, d.`PCode`, d.`Qty`, d.`Satuan`, d.`Harga` 
				FROM resep_header h INNER JOIN resep_detail d ON h.`resep_id`=d.`resep_id`
				WHERE h.resep_id='$resepid' and h.`Jenis`='$jenis' AND `status`=1";	
		}
		return $this->getArrayResult($sql);
	}
	
	function updateharga($resep_id, $pcode){
		$sql = "SELECT AVG(IF(ISNULL(amount), Harga, Harga/Amount)) AS HargaSatuan 
					FROM trans_terima_header h INNER JOIN trans_terima_detail d ON h.`NoDokumen`=d.`NoDokumen`
					LEFT JOIN konversi k ON d.`PCode`=k.`PCode` AND d.`Satuan`=k.`Satuan_From`
					WHERE h.`TglDokumen` >= (CURDATE() - INTERVAL 90 DAY) AND d.`PCode`='$pcode'";
		
		$result = $this->getArrayResult($sql);
		if(count($result>0)){
			$hargasatuan = $result[0]['HargaSatuan'];
			$sql = "Update resep_detail set Harga='$hargasatuan' where resep_id='$resep_id' and pcode='$pcode' ";
			$qry = $this->db->query($sql);	
			
			$sql = "Update resep_header set LastCalculateDate=Now() where resep_id='$resep_id'";
			$qry = $this->db->query($sql);	
		}
		
		$sql2 = "SELECT coalesce(SUM(d.`Qty`*d.`Harga`/h.`Qty`),0) AS HargaSatuan 
					FROM resep_header h INNER JOIN resep_detail d ON h.`resep_id`=d.`resep_id`
					WHERE h.`PCode`='$pcode' AND `status`=1";
		
		$result = $this->getArrayResult($sql2);
		if(count($result>0)){
			$hargasatuan = $result[0]['HargaSatuan'];
			if($hargasatuan>0){
				$sql = "Update resep_detail set Harga='$hargasatuan' where resep_id='$resep_id' and pcode='$pcode' ";
				$qry = $this->db->query($sql);		
				
				$sql = "Update resep_header set LastCalculateDate=Now() where resep_id='$resep_id'";
				$qry = $this->db->query($sql);	
			}
		}	
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>