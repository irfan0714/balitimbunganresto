<?php
class Budgetmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }
	
	function getHeader($tahun,$kdaktivitas)
	{
		
        $sql="
        	SELECT budget.*,aktivitas.*,budget.EditUser AS EditUser_,budget.EditDate AS EditDate_,
			budget.AddUser AS AddUser_,budget.AddDate AS AddDate_
			FROM budget INNER JOIN aktivitas ON budget.KdAktivitas=aktivitas.KdAktivitas WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
			";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget01($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun,'Januari' AS Bulan, NamaAktivitas,Budget01,Terpakai01,Realisasi01,Budget01-Realisasi01 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget02($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'Februari' AS Bulan, NamaAktivitas,Budget02,Terpakai02,Realisasi02,Budget02-Realisasi02 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget03($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'Maret' AS Bulan, NamaAktivitas,Budget03,Terpakai03,Realisasi03,Budget03-Realisasi03 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget04($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'April' AS Bulan, NamaAktivitas,Budget04,Terpakai04,Realisasi04,Budget04-Realisasi04 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget05($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'Mei' AS Bulan, NamaAktivitas,Budget05,Terpakai05,Realisasi05,Budget05-Realisasi05 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget06($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun,'Juni' AS Bulan, NamaAktivitas,Budget06,Terpakai06,Realisasi06,Budget06-Realisasi06 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget07($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'Juli' AS Bulan, NamaAktivitas,Budget07,Terpakai07,Realisasi07,Budget07-Realisasi07 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget08($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'Agustus' AS Bulan, NamaAktivitas,Budget08,Terpakai08,Realisasi08,Budget08-Realisasi08 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget09($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'September' AS Bulan, NamaAktivitas,Budget09,Terpakai09,Realisasi09,Budget09-Realisasi09 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget10($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'Oktober' AS Bulan, NamaAktivitas,Budget10,Terpakai10,Realisasi10,Budget10-Realisasi10 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget11($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun, 'November' AS Bulan, NamaAktivitas,Budget11,Terpakai11,Realisasi11,Budget11-Realisasi11 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getBudget12($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT Tahun,'Desember' AS Bulan,NamaAktivitas,Budget12,Terpakai12,Realisasi12,Budget12-Realisasi12 AS sisa FROM budget INNER JOIN aktivitas ON budget.`KdAktivitas`=aktivitas.`KdAktivitas` WHERE budget.`Tahun`='".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."';
        	";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getTotalBudget($tahun,$kdaktivitas)
	{
		
        $sql="
		SELECT 
				  budget.`Budget01` + budget.`Budget02` + budget.`Budget03` + budget.`Budget04` + budget.`Budget05` + budget.`Budget06` + budget.`Budget07` + budget.`Budget08` + budget.`Budget09` + budget.`Budget10` + budget.`Budget11` + budget.`Budget12` AS budget,
				  budget.`Terpakai01` + budget.`Terpakai02` + budget.`Terpakai03` + budget.`Terpakai04` + budget.`Terpakai05` + budget.`Terpakai06` + budget.`Terpakai07` + budget.`Terpakai08` + budget.`Terpakai09` + budget.`Terpakai10` + budget.`Terpakai11` + budget.`Terpakai12` AS terpakai, 
				  budget.`Realisasi01` + budget.`Realisasi02` + budget.`Realisasi03` + budget.`Realisasi04` + budget.`Realisasi05` + budget.`Realisasi06` + budget.`Realisasi07` + budget.`Realisasi08` + budget.`Realisasi09` + budget.`Realisasi10` + budget.`Realisasi11` + budget.`Realisasi12` AS realisasi 
				FROM budget  
				WHERE budget.`Tahun` = '".$tahun."' AND budget.`KdAktivitas`='".$kdaktivitas."'
		";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

    function get_budget_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	
		$sql="SELECT * FROM aktivitas INNER JOIN budget ON aktivitas.KdAktivitas = budget.KdAktivitas $clause ORDER BY aktivitas.KdAktivitas DESC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_budget_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT * FROM aktivitas INNER JOIN budget ON aktivitas.KdAktivitas = budget.KdAktivitas $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getAktivitas($KdAktivitas)
	{
    	$sql = "SELECT * FROM aktivitas WHERE KdAktivitas LIKE '%".$KdAktivitas."%' OR NamaAktivitas LIKE '%".$KdAktivitas."%'ORDER BY NamaAktivitas ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getAktivitasList()
	{
    	$sql = "SELECT * FROM aktivitas ORDER BY NamaAktivitas ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    public function get_by_budget($tahun,$aktivitas)
	{
		$this->db->from('aktivitas INNER JOIN budget ON aktivitas.KdAktivitas = budget.KdAktivitas');
		$this->db->where('budget.Tahun',$tahun);
		$this->db->where('budget.KdAktivitas',$aktivitas);
		$query = $this->db->get();
		return $query->row();
        
	}
	
	
	
}
?>