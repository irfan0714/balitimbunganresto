<?php
class rencana_bayarmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getRencanaBayarList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_tgl = "";
        $where_status= "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!=""){
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoRencanaBayar";
				$arr_keyword[1] = "NoPaymentVoucher";
				$arr_keyword[2] = "Nama";
				$arr_keyword[3] = "Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["tgldari"]!="" && $arrSearch["tgldari"]!='0000-00-00'){
				$where_tgl = " and h.TglDokumen between '".$arrSearch["tgldari"]."' and '".$arrSearch["tglsampai"]."' ";
			}
			
			if($arrSearch["status"]!=""){
				$where_status = " and h.Status='".$arrSearch["status"]."' ";
			}
		}
			    
    	$sql = "  
            SELECT h.`NoRencanaBayar` as NoTransaksi,NoPaymentVoucher,  h.`TglDokumen`, s.`Nama`, h.`Keterangan`, h.status
			FROM pelunasan_hutang_header h INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
            WHERE 
            	h.NoRencanaBayar is not null
            	".$where_keyword.$where_tgl.$where_status."                                  
            ORDER BY 
              NoTransaksi DESC 
            Limit 
              $offset,$limit
        ";               
    	  /*echo $sql;
        echo "<hr/>"; */
		return $this->getArrayResult($sql);
    }
    
    function num_rencana_bayar_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        $where_tgl = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoTransaksi";
				$arr_keyword[1] = "NoPaymentVoucher";
				$arr_keyword[2] = "Nama";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["tgldari"]!="" && $arrSearch["tgldari"]!='0000-00-00'){
				$where_tgl = " and h.TglDokumen between '".$arrSearch["tgldari"]."' and '".$arrSearch["tglsampai"]."' ";
			}
			
			if($arrSearch["status"]!=""){
				$where_status = " and h.Status='".$arrSearch["status"]."' ";
			}
		}
		
		$sql = "  
            SELECT h.`NoRencanaBayar`, h.`TglDokumen`, s.`Nama`, h.`Keterangan`
			FROM pelunasan_hutang_header h INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
            WHERE
            	h.NoRencanaBayar is not null
            	".$where_keyword.$where_tgl.$where_status."                                  
            ORDER BY 
              NoTransaksi DESC 
        ";
		                  
        return $this->NumResult($sql);
	}

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		return $this->getRow($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	
	function getSupplier($var = '')
	{
		$where='';
		if($var != ''){
			$where = " where Nama like '%$var%' ";
		}
		$sql = "SELECT KdSupplier, Nama FROM supplier $where order by nama";
    	return $this->getArrayResult($sql);
    }
    
    function getMataUang()
	{
		$sql = "SELECT kd_uang, keterangan FROM mata_uang order by kd_uang";
    	return $this->getArrayResult($sql);
    }
    
    function getKasBank()
	{
		$sql = "SELECT KdKasBank, NamaKasBank FROM kasbank ORDER BY NamaKasBank";
    	return $this->getArrayResult($sql);
    }
    
    function getNoTransaksi($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoRencanaBayar FROM pelunasan_hutang_header WHERE NoRencanaBayar LIKE 'RH%$blnthn' ORDER BY NoRencanaBayar DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = 'RH00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoRencanaBayar'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$notransaksi = 'RH' . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
	
	function getNoTransaksi2($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoRencanaBayar FROM pelunasan_hutang_header WHERE NoRencanaBayar LIKE 'RM%$blnthn' ORDER BY NoRencanaBayar DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = 'RM00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoRencanaBayar'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$notransaksi = 'RM' . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
    
    //jika type hutang datang
    function getDetail($KdSupplier,$MataUang, $tgl)
	{
		$sql = "SELECT * FROM (
				SELECT 
					h.`NoFaktur`,
					i.`NoFakturSupplier`,
					i.`NoPO`,
					i.NoPenerimaan as NoRG,
					h.`JatuhTempo` as Tanggal,
					h.`Sisa`,
					0 as NilaiBayar  
				FROM 
					hutang h 
				LEFT JOIN invoice_pembelian_header i  ON h.`NoFaktur`=i.`NoFaktur`
				WHERE h.tanggal<='$tgl' 
				AND h.KdSupplier ='$KdSupplier' 
				AND h.MataUang='$MataUang' 
				AND h.TipeTransaksi in('I','C','IM','CM')
				AND h.sisa>0 
				AND h.RencanaBayarFlg=0 
				UNION ALL
				SELECT 
				  h2.`NoFaktur`,
				  i2.`NoDokumen` AS NoFakturSupplier,
				  j2.`PONo` AS NoPO,
				  i2.`RGNo` AS NoRG,
				  h2.`JatuhTempo` AS Tanggal,
				  h2.`Sisa`,
				  0 as NilaiBayar 
				FROM
				  hutang h2
				LEFT JOIN pi_marketing i2 ON h2.`NoFaktur` = i2.`NoDokumen`
			  	LEFT JOIN rg_marketing j2
			    ON i2.`RGNo` = j2.`NoDokumen`
				WHERE h2.tanggal<='$tgl' 
				AND h2.KdSupplier ='$KdSupplier' 
				AND h2.MataUang='$MataUang' 
				AND h2.TipeTransaksi in('IM','CM') 
				AND h2.sisa>0 
				AND h2.RencanaBayarFlg=0 ) tbl
				ORDER BY Tanggal ";
		if($this->session->userdata('username') == 'mechael0101') {

			echo $sql;
		}
    	return $this->getArrayResult($sql);	
    }
    
    //jika type hutang lain lain
    function getDetail2($KdSupplier,$MataUang, $tgl)
	{
		$sql = "
				SELECT 
				  h2.`NoFaktur`,
				  i2.`NoDokumen` AS NoFakturSupplier,
				  j2.`PONo` AS NoPO,
				  i2.`RGNo` AS NoRG,
				  h2.`JatuhTempo` AS Tanggal,
				  h2.`Sisa`,
				  0 as NilaiBayar 
				FROM
				  hutang h2
				LEFT JOIN pi_marketing i2 ON h2.`NoFaktur` = i2.`NoDokumen`
			  	LEFT JOIN rg_marketing j2
			    ON i2.`RGNo` = j2.`NoDokumen`
				WHERE h2.tanggal<='$tgl' 
				AND h2.KdSupplier ='$KdSupplier' 
				AND h2.MataUang='$MataUang' 
				AND h2.TipeTransaksi in('IM','CM') 
				AND h2.sisa>0 
				AND h2.RencanaBayarFlg=0 
				ORDER BY h2.JatuhTempo";
		
		//echo $sql;die;
    	return $this->getArrayResult($sql);	
    }
    
    function insertHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan,$user, $vbiayaadmin, $vpembulatan, $vpph, $noreksupplier,$kdrekeningpph)
    {
    	$mylib = new globallib();
        $this->locktables('pelunasan_hutang_header');
		
        $data = array(
        	'NoRencanaBayar' => $notransaksi,
            'NoTransaksi'	=> $notransaksi,
            'NoPaymentVoucher' => '',
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'KdSupplier' => $kdSupplier,
            'MataUang' => $kd_uang,
            'Kurs' => $kurs,
            'NoBukti' => $nobukti,
            'Keterangan' => $keterangan,
            'BiayaAdmin' => $vbiayaadmin,
            'Pembulatan' => $vpembulatan,
            'NilaiPPH' => $vpph,
            'Status' => 3,
            'NoRekeningSupplier' => $noreksupplier,
            'KdRekeningPPH' => $kdrekeningpph,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('pelunasan_hutang_header', $data);

        $this->unlocktables();
    }
    
    function UpdateHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan,$user, $vbiayaadmin, $vpembulatan, $vpph,$flag, $noreksupplier, $kdrekeningpph)
    {
    	$this->locktables('pelunasan_hutang_header');
		
		$date = date('Y-m-d');
		if($flag=='approve'){
			$status=0;
		}elseif($flag=='Void'){
			$status=2;
		}else{
			$status=3;
		}
		$sql = "Update pelunasan_hutang_header set NoBukti='$nobukti', Keterangan='$keterangan', BiayaAdmin='$vbiayaadmin', 
				Pembulatan='$vpembulatan', NilaiPPH='$vpph', EditUser='$user', EditDate='$date', Status='$status',
				KdKasBank='$kdkasbank',TglDokumen='$tgl', NoRekeningSupplier='$noreksupplier', KdRekeningPPH='$kdrekeningpph' 
				where NoRencanaBayar='$notransaksi'";
		$this->db->query($sql);
		
        $this->unlocktables();
    }
    
    function insertDetail($notransaksi,$nofaktur,$sisa,$bayar){
    	$mylib = new globallib();
		$this->locktables('pelunasan_hutang_detail');
		$total = 0;
		$sql = "delete from pelunasan_hutang_detail where NoRencanaBayar='$notransaksi' ";
		$this->db->query($sql);
		
		for($i=0;$i<count($nofaktur); $i++){
			if($mylib->save_int($bayar[$i]>0)){
				$data = array(
					'NoRencanaBayar'=>$notransaksi,
					'NoTransaksi' => $notransaksi,
					'NoFaktur' => $nofaktur[$i],
					'NilaiFaktur' => $mylib->save_int($sisa[$i]),
					'NilaiBayar' => $mylib->save_int($bayar[$i])
				);
				$total += $mylib->save_int($bayar[$i]);
				$this->db->insert('pelunasan_hutang_detail', $data);	
			}
		}
		$this->unlocktables();
		return $total;
	}
	
	function insertDetailUM($notransaksi, $noum, $nilai){
		$mylib = new globallib();
		$this->locktables('pelunasan_hutang_outstandingum_detail');
		$total = 0;
		$sql = "delete from pelunasan_hutang_outstandingum_detail where NoTransaksi='$notransaksi' ";
		$this->db->query($sql);
		
		if($noum != ''){
			for($i=0;$i<count($noum); $i++){
		
				$data = array(
					'NoTransaksi'=>$notransaksi,
					'NoUM' => $noum[$i],
					'Nilai' =>  $mylib->save_int($nilai[$i])
				);
				$this->db->insert('pelunasan_hutang_outstandingum_detail', $data);
			}
		}
		$this->unlocktables();
	}

	function UpdateHutang($nofaktur,$bayar,$flag){
		$mylib = new globallib();
		$this->locktables('hutang');
		for($i=0;$i<count($nofaktur); $i++){
			$nodokumen = $nofaktur[$i];
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0 && $flag != 'Void'){
				$sql = "Update hutang set RencanaBayarFlg = 1 where NoDokumen='$nodokumen'";
				$qry = $this->db->query($sql);
			}else{
				$sql = "Update hutang set RencanaBayarFlg = 0 where NoDokumen='$nodokumen'";
				$qry = $this->db->query($sql);
			}
		}
		$this->unlocktables();
	}
	 
	function getDataHeader($notransaksi){
    	$sql = "SELECT h.`NoRencanaBayar` as NoTransaksi,DATE_FORMAT(h.TglDokumen,'%d-%m-%Y') as TglDokumen, k.`NamaKasBank`, s.`Nama` AS NamaSupplier, h.KdSupplier, KdRekeningPPH, COALESCE(r.NamaRekening,'') as NamaRekening, 
				h.`MataUang`, h.`Kurs`, h.`NoBukti`, h.`Keterangan`, h.`AddDate`, h.`AddUser`, h.`EditDate`, h.`EditUser`, h.BiayaAdmin, h.Pembulatan, NilaiPPH, h.KdKasBank, h.NoRekeningSupplier  
				FROM pelunasan_hutang_header h 
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				INNER JOIN kasbank k ON h.`KdKasBank`=k.`KdKasBank`
				left join rekening r on h.KdRekeningPPH=r.KdRekening
				WHERE h.`NoRencanaBayar`='$notransaksi'";
        $row = $this->getArrayResult($sql);
        return $row[0];
	}
	
	function getDataDetail($notransaksi){
    	$sql = "SELECT p.`NoFaktur`, i.`NoFakturSupplier`, i.NoPenerimaan as NoRG, i.`NoPO`, i.JatuhTempo as Tanggal, p.`NilaiFaktur` as Sisa, p.`NilaiBayar` FROM pelunasan_hutang_detail p
				LEFT JOIN invoice_pembelian_header i ON p.`NoFaktur`=i.`NoFaktur`
				WHERE p.`NoRencanaBayar`='$notransaksi'";
        return $this->getArrayResult($sql);
	}
	
	function getDataDetail2($notransaksi){
    	$sql = "
    			SELECT 
				  p.`NoFaktur`,
				  i.`NoDokumen` AS NoFakturSupplier,
				  i.`RGNo` AS NoRG,
				  j.`PONo` AS NoPO,
				  i.`TglDokumen` AS Tanggal,
				  p.`NilaiFaktur` as Sisa,
				  p.`NilaiBayar` 
				FROM
				  pelunasan_hutang_detail p 
				  LEFT JOIN pi_marketing i 
				    ON p.`NoFaktur` = i.`NoDokumen`
				   LEFT JOIN rg_marketing j
				    ON i.`RGNo` = j.`NoDokumen` 
				WHERE p.`NoRencanaBayar`='$notransaksi'";
				
        return $this->getArrayResult($sql);
	}
	
	function getOutStandingUM($kdsupplier){
		$sql = "SELECT NoDokumen as NoUM, TglDokumen as TglUM, Keterangan as KeteranganUM, Sisa as NilaiUM FROM uang_muka_supplier WHERE kdsupplier='$kdsupplier' AND sisa>0";
		
		return $this->getArrayResult($sql);
	}
	
	function getDataOutStandingUM($notransaksi){
		$sql = "SELECT d.NoTransaksi,d.NoUM, d.Nilai as NilaiUM, u.TglDokumen as TglUM, u.Keterangan as KeteranganUM  
				FROM pelunasan_hutang_outstandingum_detail d inner join uang_muka_supplier u on d.NoUM=u.NoDokumen
				inner join pelunasan_hutang_header h on h.NoTransaksi=d.NoTransaksi
				WHERE h.NoRencanaBayar='$notransaksi'";
		
		return $this->getArrayResult($sql);
	}
	
	function getKodeBank($kdkasbank){
		$sql = "SELECT KdPembayaran FROM kasbank where KdKasBank='$kdkasbank'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['KdPembayaran'];
	}
	
	function getNamaSupplier($kdsupplier){
		$sql = "SELECT Nama FROM supplier where KdSupplier='$kdsupplier'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['Nama'];
	}
	
	function getNoRekSupplier($kdsupplier){
		$sql = "SELECT CONCAT(NamaBank,' ',NoRekening,' a.n ', NamaPemilik) AS NoRekSupplier FROM rekening_supplier where KdSupplier='$kdsupplier' and StatAktif='Y'";
    	$result = $this->getArrayResult($sql);
    	return $result;
	}
	
	function locktables($table)
	{
		//$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		//$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
    function getRekeningPPH(){
		$sql = "Select KdRekening, NamaRekening from rekening where KdRekening in ('21020101','21020102','21020103','21020104','21020105','21020106')";
		return $this->getArrayResult($sql);
	}
}
?>