<?php
class Productionpackaging_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getMatreqdetail()
	{
    	$sql = "
    		
    			SELECT 
				  `production`.`productionid`,
				  `production`.`productiondate`,
				  `production`.`batchnumber`,
				  matreqdetail.`inventorycode`,
				  matreqdetail.`quantity`,
				  matreqdetail.`adddate`,
				  matreqdetail.`editdate`
				FROM
				  `production` 
				  INNER JOIN productionbatchrequest 
				    ON `production`.`productionid` = productionbatchrequest.`productionid` 
				  INNER JOIN matreq 
				    ON productionbatchrequest.matreqnumber = matreq.matreqnumber 
				  INNER JOIN matreqdetail 
				    ON productionbatchrequest.matreqnumber = matreqdetail.matreqnumber 
				   INNER JOIN `masterbarang`
				   ON `masterbarang`.`PCode` = matreqdetail.`inventorycode` AND `masterbarang`.`KdKategori`='2'
				WHERE 1 
				ORDER BY `production`.`productionid` ASC ;
    	
    			";
    	
		return $this->db->query($sql);
    }
    
    
}
?>