<?php
class Stiker_sales_model extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_gsa_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	/*$sql = "select KdSalesman,NamaSalesman,head.KdPersonal,NamaPersonal,NamaTipeSalesman,KdGudang from(
					SELECT KdSalesman,NamaSalesman,KdPersonal,KdTipeSalesman, KdGudang 
					FROM salesman $clause order by KdSalesman  Limit $offset,$num
				) as head
				left join
				(
					select KdPersonal, NamaPersonal from personal
				) as personal
				on personal.KdPersonal = head.KdPersonal
				left join
				(
					select KdTipeSalesman, NamaTipeSalesman from tipe_salesman
				) as tipe
				on tipe.KdTipeSalesman = head.KdTipeSalesman";*/
		$sql="SELECT stiker_sales.*,salesman.*, DATE_FORMAT(
    stiker_sales.Tanggal,
    '%d-%m-%Y'
  ) AS Tanggal_  FROM `stiker_sales` INNER JOIN salesman ON salesman.`KdSalesman` = stiker_sales.`KdSalesman` $clause";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_gsa_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT * FROM salesman $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getSales()
	{
    	$sql = "
    			SELECT * FROM salesman ORDER BY NamaSalesman ASC;
    		   ";
		//echo $sql;die;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetailGSA($id)
	{
    	$sql = "
    			SELECT 
				  * FROM stiker_sales_detail WHERE stk_id='".$id."' ;
    		   ";
		//echo $sql;die;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getTipe(){
    	$sql = "SELECT KdTipeSalesman, NamaTipeSalesman from tipe_salesman order by KdTipeSalesman";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getPersonal(){
    	$sql = "SELECT KdPersonal, NamaPersonal from personal order by KdPersonal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getSupervisor(){
    	$sql = "SELECT * from supervisor order by KdSupervisor";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getGudang(){
    	$sql = "SELECT KdGudang, Keterangan as NamaGudang from gudang order by KdGudang";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDetail($id){
    	$sql = "SELECT stiker_sales.*,DATE_FORMAT(stiker_sales.Tanggal, '%d-%m-%Y') AS Tanggal_ from stiker_sales INNER JOIN salesman ON stiker_sales.KdSalesman = salesman.KdSalesman Where stk_id='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
   
    function get_id($id){
		$sql = "SELECT KdSupervisor FROM supervisor Where KdSupervisor='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;
	}
	function cekDelete($id)
	{
		/* $sql = "SELECT KdSalesman FROM kendaraan Where KdSalesman='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		$query->free_result();
		return $num;*/
		return 0;
	}
	
	function getidcounter($id){
	    $sql = "SELECT KdSupervisor FROM supervisorlist Where KdSupervisor='$id'";
		$query = $this->db->query($sql);
		$num = $query->num_rows();
		IF($num==0)
		{
		   $sql = "insert into supervisorlist (KdSupervisor, Counter) values('$id','0')";
		   $qry = $this->db->query($sql);
		}
	    $sql = "Update supervisorlist set Counter=Counter+1 where KdSupervisor='$id'";
		$qry = $this->db->query($sql);
		$sql = "SELECT Counter FROM supervisorlist Where KdSupervisor='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getCekStikerOH($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  `transaksi_header`.`KdAgent` 
				FROM
				  `transaksi_header` 
				  INNER JOIN kassa 
				    ON `transaksi_header`.`NoKassa` = kassa.`id_kassa` 
				WHERE 1 AND `transaksi_header`.`KdAgent`='".$nostiker."'
				  AND `transaksi_header`.`Tanggal` = '".$tgl."' 
				  AND kassa.`KdDivisi` = '3' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getCekStikerResto($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  `transaksi_header`.`KdAgent` 
				FROM
				  `transaksi_header` 
				  INNER JOIN kassa 
				    ON `transaksi_header`.`NoKassa` = kassa.`id_kassa` 
				WHERE 1 AND `transaksi_header`.`KdAgent`='".$nostiker."'
				  AND `transaksi_header`.`Tanggal` = '".$tgl."' 
				  AND kassa.`KdDivisi` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getCekStikerBe($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  `transaksi_header`.`KdAgent` 
				FROM
				  `transaksi_header` 
				  INNER JOIN kassa 
				    ON `transaksi_header`.`NoKassa` = kassa.`id_kassa` 
				WHERE 1 AND `transaksi_header`.`KdAgent`='".$nostiker."'
				  AND `transaksi_header`.`Tanggal` = '".$tgl."' 
				  AND kassa.`KdDivisi` = '2' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
     function getbeauty($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  * 
				FROM
				  gsa 
				WHERE gsa.`NoStiker` = '".$nostiker."' 
				  AND gsa.`Tanggal` = '".$tgl."' 
				  AND gsa.`beauty_tour` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getcoffee($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  * 
				FROM
				  gsa 
				WHERE gsa.`NoStiker` = '".$nostiker."' 
				  AND gsa.`Tanggal` = '".$tgl."'
				  AND gsa.`coffee_tour` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getchamber($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  * 
				FROM
				  gsa 
				WHERE gsa.`NoStiker` = '".$nostiker."' 
				  AND gsa.`Tanggal` = '".$tgl."'
				  AND gsa.`chamber` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getoh($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  * 
				FROM
				  gsa 
				WHERE gsa.`NoStiker` = '".$nostiker."' 
				  AND gsa.`Tanggal` = '".$tgl."' 
				  AND gsa.`standby_oh` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getresto($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  * 
				FROM
				  gsa 
				WHERE gsa.`NoStiker` = '".$nostiker."' 
				  AND gsa.`Tanggal` = '".$tgl."'
				  AND gsa.`standby_resto` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getbe($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  * 
				FROM
				  gsa 
				WHERE gsa.`NoStiker` = '".$nostiker."' 
				  AND gsa.`Tanggal` = '".$tgl."' 
				  AND gsa.`standby_be` = '1' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getStikerSales($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  b.`stk_id` 
				FROM
				  stiker_sales_detail a 
				  INNER JOIN stiker_sales b 
				    ON a.`stk_id` = b.`stk_id` 
				WHERE a.`NoStiker` = '".$nostiker."' 
				  AND b.`Tanggal` = '".$tgl."' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getTransaksi($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  `transaksi_header`.`KdAgent` 
				FROM
				  `transaksi_header` 
				  INNER JOIN kassa 
				    ON `transaksi_header`.`NoKassa` = kassa.`id_kassa` 
				WHERE 1 AND `transaksi_header`.`KdAgent`='".$nostiker."'
				  AND `transaksi_header`.`Tanggal` = '".$tgl."' 
				  AND kassa.`KdDivisi` IN ('1','2','3');
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getAdaGaBeauty($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  gsa.`beauty_tour` 
				FROM
				  gsa_detail 
				  INNER JOIN gsa 
				    ON gsa_detail.`gsa_id` = gsa.`gsa_id` 
				WHERE gsa_detail.`NoStiker`='".$nostiker."' AND gsa.`Tanggal`='".$tgl."' AND gsa.`beauty_tour`='1';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getAdaGaCoffee($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  gsa.`beauty_tour` 
				FROM
				  gsa_detail 
				  INNER JOIN gsa 
				    ON gsa_detail.`gsa_id` = gsa.`gsa_id` 
				WHERE gsa_detail.`NoStiker`='".$nostiker."' AND gsa.`Tanggal`='".$tgl."' AND gsa.`coffee_tour`='1';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getAdaGaChamber($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  gsa.`beauty_tour` 
				FROM
				  gsa_detail 
				  INNER JOIN gsa 
				    ON gsa_detail.`gsa_id` = gsa.`gsa_id` 
				WHERE gsa_detail.`NoStiker`='".$nostiker."' AND gsa.`Tanggal`='".$tgl."' AND gsa.`chamber`='1';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getAdaGaStdOh($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  gsa.`beauty_tour` 
				FROM
				  gsa_detail 
				  INNER JOIN gsa 
				    ON gsa_detail.`gsa_id` = gsa.`gsa_id` 
				WHERE gsa_detail.`NoStiker`='".$nostiker."' AND gsa.`Tanggal`='".$tgl."' AND gsa.`standby_oh`='1';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getAdaGaStdRes($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  gsa.`beauty_tour` 
				FROM
				  gsa_detail 
				  INNER JOIN gsa 
				    ON gsa_detail.`gsa_id` = gsa.`gsa_id` 
				WHERE gsa_detail.`NoStiker`='".$nostiker."' AND gsa.`Tanggal`='".$tgl."' AND gsa.`standby_resto`='1';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
    
    function getAdaGaStdBe($nostiker,$tgl){
    	$sql = "
    			SELECT 
				  gsa.`beauty_tour` 
				FROM
				  gsa_detail 
				  INNER JOIN gsa 
				    ON gsa_detail.`gsa_id` = gsa.`gsa_id` 
				WHERE gsa_detail.`NoStiker`='".$nostiker."' AND gsa.`Tanggal`='".$tgl."' AND gsa.`standby_be`='1';
    			";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }
	
}
?>