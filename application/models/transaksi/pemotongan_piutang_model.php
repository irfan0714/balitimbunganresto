<?php
class Pemotongan_piutang_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
   	
	function ambilNoFak($v_customer)
	{
		$sql = "
			SELECT * FROM `piutang` a WHERE a.`KdCustomer`='".$v_customer."' AND a.`TipeTransaksi` IN ('I','D') AND a.`Sisa`>0;
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function getCnno()
	{
    	$sql = "SELECT a.cnno AS nama FROM `creditnote` a ;";
    	return $this->getArrayResult($sql);
    }
    
    
    
    function num_pemotongan_piutang_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `cnallocation` a;       
		";
		                  
        return $this->NumResult($sql);
	}
	    
    function getDebitNoteDetailList($user)
	{
         $sql = "  
         SELECT 
		  * 
		FROM
		  `debitnotedtl_temp` a 
		  INNER JOIN rekening b 
			ON a.coano = b.`KdRekening` 
		WHERE CONCAT(a.dnno, a.adduser)='00000".$user."';
        ";              
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
	function getCreditNoteAllocationDetail($id)
	{
        
    	$sql = "  
          SELECT 
          a.*,
		  b.*,
		  c.*,
		  d.`Sisa`,
		  DATE_FORMAT(d.`JatuhTempo`, '%d-%m-%Y') AS JatuhTempo
           FROM `cnallocation` a INNER JOIN `cnallocationdetail` b ON a.`cnallocationno` = b.`cnallocationno` 
			INNER JOIN `customer` c ON a.`customerid`=c.`KdCustomer` 
			INNER JOIN piutang d
		   ON b.`invno`=d.`NoFaktur`
		   WHERE a.`cnallocationno` = '".$id."' ;;
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function getDataSalesInvoiceDetail($user)
	{
        
    	$sql = "  
        SELECT 
		  * 
		FROM
		  `salesinvoicedetail_temp` a 
		  INNER JOIN masterbarang b 
		    ON a.`inventorycode` = b.`PCode` 
		WHERE a.`invoiceno` = '00000' AND a.`adduser`='".$user."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	   
	
	function getCnaList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.dnno";
				$arr_keyword[1] = "a.cndate";
				$arr_keyword[2] = "b.Nama";
				$arr_keyword[3] = "a.status";
				$arr_keyword[4] = "a.note";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT * FROM `cnallocation` a 
			INNER JOIN customer b ON a.`customerid`=b.`KdCustomer` WHERE 1  
            	".$where_keyword."
            	".$where_customer."    
            	".$where_status."                                   
            ORDER BY
                a.`cnallocationno` ASC 
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT * FROM `cnallocation` a 
			INNER JOIN `customer` b ON a.`customerid`=b.`KdCustomer`
			INNER JOIN `cnallocationdetail` c on a.cnallocationno = c.cnallocationno
			WHERE 1 AND a.cnallocationno='".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT COUNT(a.dono) AS jml FROM `salesinvoicedetail` a WHERE a.dono='".$nodok."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail3($user)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail_temp` a WHERE a.`invoiceno`='00000' AND a.adduser='".$user."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function cekGetDetail4($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getDetail_cetak($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a 
	 INNER JOIN `masterbarang` b ON a.`inventorycode`=b.`PCode`
	 WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function hitungSalesInvoice_cetak($id)
	{
        
    	$sql = "  
        SELECT SUM(a.`quantity`*a.`nettprice`) AS total , SUM(b.`DiscLokal`) AS diskon
		FROM `salesinvoicedetail` a  
		INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` 
		WHERE a.`invoiceno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
    function getCustomerList()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	//echo $sql;
        //echo "<hr/>";
    	return $this->getArrayResult($sql);
    }
    
    	
	function getRequirement($KdCustomer)
	{
		$sql = "SELECT * FROM `creditnote` a WHERE a.`customerid`='".$KdCustomer."' ";
		
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}
	
	function getSibling($KdCustomer)
	{
		$sql = "SELECT 
				  a.*,
				  b.*,
				  FORMAT(b.`Sisa`,0) AS sisa_
				FROM
				  `creditnote` a 
				  INNER JOIN piutang b
				  ON a.`cnno`= b.`NoDokumen`
				WHERE 1 AND a.`customerid` ='".$KdCustomer."' ";
		//echo $sql;die;
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}
	
	function getAmbilPiutang($customer, $tgl)
	{
		$sql = "
			SELECT 
			  a.* ,
			  b.*,
			  DATE_FORMAT(a.`JatuhTempo`, '%d-%m-%Y') AS JatuhTempo_,
			  FORMAT(ROUND(a.`NilaiTransaksi`),2) AS NilaiTransaksi1,
			  ROUND(a.`NilaiTransaksi`) AS NilaiTransaksi2,
			  FORMAT(ROUND(a.`sisa`),2) AS sisa1,
			  ROUND(a.`sisa`) AS sisa2
			  FROM `piutang` a 
			INNER JOIN supplier b ON a.`KdSupplier`=b.`KdSupplier`
			
			WHERE a.`KdSupplier`='".$customer."' AND a.`TipeTransaksi` IN ('I','C','IM','CM') AND a.`Sisa`>0
			AND a.Tanggal <= '".$tgl."';
		";
		//echo $sql;die;
        return $this->db->query($sql);
	}
    
    function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function getCustomer()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	//echo $sql;
        //echo "<hr/>";
    	return $this->getArrayResult($sql);
    }
    
    function getTravel()
	{
    	$sql = "SELECT KdTravel as KdCustomer, Nama FROM tourtravel a ORDER BY a.Nama ASC";
    	//echo $sql;
        //echo "<hr/>";
    	return $this->getArrayResult($sql);
    }
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>