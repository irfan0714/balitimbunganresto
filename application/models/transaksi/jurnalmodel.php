<?php
class Jurnalmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }
    
    function getSubdivisi()
	{
    	$sql = "SELECT CONCAT(KdDivisi,KdSubDivisi)as kode,KdSubDivisi,NamaSubDivisi from subdivisi order by NamaSubDivisi";
    	return $this->getArrayResult($sql);
    }

    function getJurnalList($num,$offset,$id,$with,$thnbln)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
//		$clause="where h.Status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
        $clause="where (h.Status<>'B' or h.Status is NULL)";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else if($with=="TglDokumen"){
				$clause .= " and $with = '$id'";
			}
			else if($with=="h.Keterangan"){
				$clause .= " and $with like '%$id%'";
			}
		}
    	$sql = "select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,h.NoTransaksi,h.Keterangan,JumlahDebit,JumlahKredit,project.Keterangan as NamaProject,NamaCostCenter,NamaDepartemen,UserName,h.Jenis
				from trans_jurnal_header h 
				left join departemen on departemen.KdDepartemen = h.KdDepartemen
				left join project on project.KdProject = h.KdProject
				left join costcenter on costcenter.KdCostCenter = h.KdCostCenter
				left join user on user.id = h.AddUser
				$clause 
				order by cast(NoDokumen as unsigned) desc Limit $offset,$num
			";
			
		return $this->getArrayResult($sql);
    }
    
    function num_jurnal_row($id,$with,$thnbln){
//     	$clause="where Status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
        $clause="where (h.Status<>'B' or h.Status is NULL)";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
		$sql = "SELECT h.NoDokumen FROM trans_jurnal_header h $clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans,DefaultKdDepartemen,DefaultKdProject,DefaultKdCostCenter from aplikasi";
        return $this->getRow($sql);
    }
	function getDept(){
    	$sql = "SELECT KdDepartemen,NamaDepartemen from departemen order by KdDepartemen";
		return $this->getArrayResult($sql);
    }
	function getProject(){
    	$sql = "SELECT KdProject,Keterangan as NamaProject from project order by KdProject";
		return $this->getArrayResult($sql);
    }
	function getCostCenter(){
		$sql = "SELECT KdCostCenter,NamaCostCenter FROM costcenter order by KdCostCenter";
		return $this->getArrayResult($sql);
	}
    function findrekening($id){
		$sql = "SELECT kdrekening,namarekening FROM rekening Where kdrekening='$id' and tingkat='3'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getTipe(){
    	$sql = "SELECT KdRekening, NamaRekening from rekening order by KdRekening";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	function getNewNo($tgl)
	{
		$tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
        //$NoJurnal = "Count$bulan";
		$sql = "Update counter set NoJurnal=NoJurnal+1 where Tahun='$tahun' and Bulan='$bulan' ";
		$this->db->query($sql);
		$sql = "SELECT NoJurnal FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	function getolddata($no,$counter)
	{
		$sql = "SELECT KdRekening,Debit,Kredit from trans_jurnal_detail where NoDokumen='$no' and Urutan='$counter'";
		return $this->getRow($sql);
	}
	function getDetail($id)
	{
		$sql = "select d.KdRekening,KdSubDivisi,Debit,Kredit,Keterangan,NamaRekening,Urutan from(
		SELECT * from trans_jurnal_detail Where NoDokumen='$id' and (STATUS <> 'B' OR STATUS IS NULL)
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "select d.KdRekening,Debit,Kredit,Keterangan,NamaRekening,Urutan from(
		SELECT * from trans_jurnal_detail Where NoDokumen='$id' and (Status<>'B' or Status is NULL)
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
		$sql = "select NoDokumen,Tanggal,NamaProject,NamaCostCenter,NamaDepartemen,NoTransaksi,Keterangan,h.KdProject,h.KdCostCenter,h.KdDepartemen,JumlahDebit,JumlahKredit,Jenis
from(
SELECT NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,Keterangan,NoTransaksi,
KdProject,KdCostCenter,KdDepartemen,JumlahDebit,JumlahKredit,Jenis
from trans_jurnal_header where NoDokumen='$id' and (STATUS <> 'B' OR STATUS IS NULL))h
left JOIN
(
select KdProject,Keterangan as NamaProject from project
)project
on project.KdProject = h.KdProject
left JOIN
(
select KdCostCenter,NamaCostCenter from costcenter
)costcenter
on costcenter.KdCostCenter = h.KdCostCenter
left JOIN
(
select KdDepartemen,NamaDepartemen from departemen
)departemen
on departemen.KdDepartemen = h.KdDepartemen;";
        return $this->getRow($sql);
	}
	
	
	function tarik(){
		$sql="SELECT 
			 a.NoDokumen,
			  a.Keterangan 
			FROM
			  `trans_jurnal_detail` a 
			WHERE a.Urutan = '1' 
			  AND a.TglDokumen > '2017-10-30' ;";
		return $this->getArrayResult($sql);
	}
	
	function AmbiltglGL(){
		$sql="SELECT 
			a.PeriodeGL 
			FROM
			  aplikasi a";
		return $this->getRow($sql);
	}
	
	function tglGL($tglperiodegl,$tgl){
		$sql="SELECT 
			IF ('$tglperiodegl' < '$tgl','1','0') nilai,a.PeriodeGL 
			FROM
			  aplikasi a";
		//echo $sql;die;
		return $this->getRow($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>