<?php
class credit_note_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
   
    function cekTabelPiutang($id)
	{
        
    	$sql = "  
		        SELECT * FROM piutang a
				WHERE a.NoDokumen='".$id."';
		        ";    
		return $this->getRow($sql);
    }
    
    function cekRekening($rek)
	{
        
    	$sql = "  
		        SELECT * FROM rekening a
				WHERE a.KdRekening ='".$rek."';
		        ";    
		return $this->getRow($sql);
    }
    
    
    function cekTabelMutasiPiutang($id)
	{
        
    	$sql = "  
		        SELECT * FROM mutasi_piutang a
				WHERE a.NoDokumen='".$id."';
		        ";    
		return $this->getRow($sql);
    }
	
    function cekDatacreditNoteDetail($v_cnno)
	{
		$sql = "
			SELECT 
			  ROUND(SUM(a.value),0) AS amount,
			  ROUND((b.vatpercent/100)*SUM(a.value),0) AS potongan_ppn,
			  ROUND(SUM(a.value) + (b.vatpercent/100)*SUM(a.value),0) AS grandtotal   
			FROM
			  `creditnotedtl` a inner join creditnote b on a.cnno=b.cnno
			WHERE a.`cnno` = '".$v_cnno."' ;
		";
	    return $this->getArrayResult($sql);
	}
	
	function getSubdivisi()
	{
    	$sql = "SELECT a.KdSubdivisi,a.NamaSubDivisi FROM subdivisi a ORDER BY a.NamaSubDivisi ASC";
    	return $this->getArrayResult($sql);
    }
    
    function num_credit_note_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `creditnote` a;       
		";
		                  
        return $this->NumResult($sql);
	}
	    
    function getCreditNoteDetailList($user)
	{
         $sql = "  
         SELECT 
		  * 
		FROM
		  `creditnotedtl_temp` a 
		  INNER JOIN rekening b 
			ON a.coano = b.`KdRekening` 
		WHERE CONCAT(a.cnno, a.adduser)='00000$user';
        ";              
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql);
    }
	
	function getcreditNoteDetailList2($id)
	{
        
    	$sql = "  
          SELECT a.`cndid`,a.`cnno`,a.`coano`,ROUND(a.value,0) AS value,a.`description`, b.`NamaRekening`,a.`KdSubDivisi` FROM `creditnotedtl` a   
		 LEFT JOIN rekening b ON a.`coano`=b.`KdRekening` LEFT JOIN subdivisi c ON a.`KdSubDivisi`=c.`KdSubDivisi` WHERE a.`cnno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function getDataSalesInvoiceDetail($user)
	{
        
    	$sql = "  
        SELECT 
		  * 
		FROM
		  `salesinvoicedetail_temp` a 
		  INNER JOIN masterbarang b 
		    ON a.`inventorycode` = b.`PCode` 
		WHERE a.`invoiceno` = '00000' AND a.`adduser`='".$user."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
		
	function hitungcreditNoteDetailList2($id)
	{
        
    	$sql = "  
        SELECT * FROM creditnote a
		WHERE a.`cnno`='".$id."';
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getRow($sql);
    }
    
    function hitungcreditNoteDetailList3($id)
	{
        
    	$sql = "  
        SELECT SUM(a.value) AS amountdetail FROM `creditnotedtl` a
		WHERE a.`cnno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getRow($sql);
    }
	
	
	function getcreditNoteList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.cnno";
				$arr_keyword[1] = "a.cndate";
				$arr_keyword[2] = "b.Nama";
				$arr_keyword[3] = "a.status";
				$arr_keyword[4] = "a.note";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT 
			a.cnno, a.cndate, a.cntype, a.returnno, a.KdCustomer, a.note, a.isprinted, a.isposted, 
			a.status, a.currencycode, a.taxrate, a.vatpercent, 
			(a.vatpercent / 100) * SUM(d.value) taxamount,
			a.discpercent,
			(a.discpercent / 100) * SUM(d.value) discamount,
			SUM(d.value) cnamount,
			a.cnamountremain, a.KdRekening,
			a.adddate, a.adduser, a.editdate, a.edituser, a.vatremain,
			CONCAT(COALESCE(b.Nama,''),COALESCE(t.Nama,'')) AS Nama, 
			DATE_FORMAT(a.cndate, '%d-%m-%Y') AS cndate_indo	
             FROM `creditnote` a 
			left JOIN `customer` b ON a.`KdCustomer`=b.`KdCustomer`
			left join tourtravel t on a.KdCustomer=t.KdTravel
			LEFT JOIN creditnotedtl d ON a.cnno=d.cnno
			WHERE 1  
            	".$where_keyword."
            	".$where_customer."    
            	".$where_status."   
			GROUP BY a.cnno                                 
            ORDER BY
                a.AddDate DESC 
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			a.*,
			concat(coalesce(b.Nama,''),coalesce(t.Nama,'')) as Nama,
			c.*,
			d.*,
			DATE_FORMAT(a.cndate, '%d-%m-%Y') AS cndate_indo
			 FROM `creditnote` a 
			left JOIN `customer` b ON a.`KdCustomer`=b.`KdCustomer`
			left join tourtravel t on a.KdCustomer=t.KdTravel
			INNER JOIN `creditnotedtl` c on a.cnno = c.cnno
			left JOIN rekening d ON a.KdRekening = d.KdRekening
			WHERE 1 AND a.`cnno`='".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function getValue($id)
	{
		$sql = "
			SELECT SUM(a.value) AS total FROM `creditnotedtl` a WHERE a.`cnno`='".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT COUNT(a.dono) AS jml FROM `salesinvoicedetail` a WHERE a.dono='".$nodok."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail3($user)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail_temp` a WHERE a.`invoiceno`='00000' AND a.adduser='".$user."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function cekGetDetail4($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getDetail_cetak($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a 
	 INNER JOIN `masterbarang` b ON a.`inventorycode`=b.`PCode`
	 WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function hitungSalesInvoice_cetak($id)
	{
        
    	$sql = "  
        SELECT SUM(a.`quantity`*a.`nettprice`) AS total , SUM(b.`DiscLokal`) AS diskon
		FROM `salesinvoicedetail` a  
		INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` 
		WHERE a.`invoiceno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
    function getCustomer()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	//echo $sql;
        //echo "<hr/>";
    	return $this->getArrayResult($sql);
    }
    
    function getTravel()
	{
    	$sql = "SELECT KdTravel as KdCustomer, Nama FROM tourtravel a ORDER BY a.Nama ASC";
    	//echo $sql;
        //echo "<hr/>";
    	return $this->getArrayResult($sql);
    }
    
    function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>