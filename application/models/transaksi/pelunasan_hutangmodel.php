<?php
class pelunasan_hutangmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getPelunasanHutangList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_jenis="";
        $where_tgl="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoTransaksi";
				$arr_keyword[1] = "NoPaymentVoucher";
				$arr_keyword[2] = "Nama";
				$arr_keyword[3] = "Keterangan";
				$arr_keyword[4] = "NoRencanaBayar";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			if($arrSearch["jenis"]==""){
				$where_jenis = " and h.Status<> 3" ;
			}else{
				$where_jenis = " and h.Status=" . $arrSearch['jenis'] ;
			}
			
			if($arrSearch["tgldari"]!="" && $arrSearch["tgldari"]!='0000-00-00'){
				$where_tgl = " and h.TglDokumen between '".$arrSearch["tgldari"]."' and '".$arrSearch["tglsampai"]."' ";
			}
			
		}
        
    	$sql = "  
            SELECT h.`NoTransaksi`, h.`NoPaymentVoucher`, h.`TglDokumen`, s.`Nama`, h.`Keterangan`, h.status, h.NoRencanaBayar
			FROM pelunasan_hutang_header h INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
            WHERE 
            	1
            	".$where_keyword.  $where_jenis . $where_tgl ." 
            ORDER BY 
              NoTransaksi DESC 
            Limit 
              $offset,$limit
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_pelunasan_hutang_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        $where_jenis="";
        $where_tgl = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoTransaksi";
				$arr_keyword[1] = "NoPaymentVoucher";
				$arr_keyword[2] = "Nama";
				$arr_keyword[3] = "Keterangan";
				$arr_keyword[4] = "NoRencanaBayar";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			if($arrSearch["jenis"]==""){
				$where_jenis = " and h.Status<> 3" ;
			}else{
				$where_jenis = " and h.Status=" . $arrSearch['jenis'] ;
			}
			
			if($arrSearch["tgldari"]!="" && $arrSearch["tgldari"]!='0000-00-00'){
				$where_tgl = " and h.TglDokumen between '".$arrSearch["tgldari"]."' and '".$arrSearch["tglsampai"]."' ";
			}
			
		}
		
		$sql = "  
            SELECT h.`NoTransaksi`, h.`NoPaymentVoucher`, h.`TglDokumen`, s.`Nama`, h.`Keterangan`
			FROM pelunasan_hutang_header h INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
            WHERE 
            	1 
            	".$where_keyword. $where_jenis . $where_tgl . "
            ORDER BY 
              NoTransaksi DESC 
        ";
		                  
        return $this->NumResult($sql);
	}

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		return $this->getRow($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	
	function getSupplier($var = '')
	{
		$where='';
		if($var != ''){
			$where = " where Nama like '%$var%' ";
		}
		$sql = "SELECT KdSupplier, Nama FROM supplier $where order by nama";
    	return $this->getArrayResult($sql);
    }
    
    function getMataUang()
	{
		$sql = "SELECT kd_uang, keterangan FROM mata_uang order by kd_uang";
    	return $this->getArrayResult($sql);
    }
    
    function getKasBank()
	{
		$sql = "SELECT KdKasBank, NamaKasBank FROM kasbank ORDER BY NamaKasBank";
    	return $this->getArrayResult($sql);
    }
    
    function getNoTransaksi($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoTransaksi FROM pelunasan_hutang_header WHERE NoTransaksi LIKE 'PH%$blnthn' ORDER BY NoTransaksi DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = 'PH00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoTransaksi'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$notransaksi = 'PH' . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
	
	function getNoTransaksi2($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoTransaksi FROM pelunasan_hutang_header WHERE NoTransaksi LIKE 'PM%$blnthn' ORDER BY NoTransaksi DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = 'PM00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoTransaksi'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$notransaksi = 'PM' . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
    
    //jika type hutang datang
    function getDetail($KdSupplier,$MataUang, $tgl)
	{
		$sql = "SELECT h.`NoFaktur`, i.`NoFakturSupplier`, i.`NoPO`, i.NoPenerimaan as NoRG, h.`JatuhTempo` as Tanggal, h.`Sisa`  
				FROM hutang h LEFT JOIN invoice_pembelian_header i  ON h.`NoFaktur`=i.`NoFaktur`
				WHERE h.tanggal<='$tgl' and h.KdSupplier ='$KdSupplier' AND h.MataUang='$MataUang' AND h.TipeTransaksi in('I','C') and h.sisa>0 order by h.JatuhTempo";
		//echo $sql;die;
    	return $this->getArrayResult($sql);	
    }
    
    //jika type hutang lain lain
    function getDetail2($KdSupplier,$MataUang, $tgl)
	{
		$sql = "
				SELECT 
				  h.`NoFaktur`,
				  i.`NoDokumen` AS NoFakturSupplier,
				  j.`PONo` AS NoPO,
				  i.`RGNo` AS NoRG,
				  h.`JatuhTempo` AS Tanggal,
				  h.`Sisa` 
				FROM
				  hutang h 
				  LEFT JOIN pi_marketing i 
				    ON h.`NoFaktur` = i.`NoDokumen`
			  LEFT JOIN rg_marketing j
			    ON i.`RGNo` = j.`NoDokumen`
				WHERE h.tanggal<='$tgl' and h.KdSupplier ='$KdSupplier' AND h.MataUang='$MataUang' AND h.TipeTransaksi in('IM','CM') and h.sisa>0 order by h.JatuhTempo";
		//echo $sql;die;
    	return $this->getArrayResult($sql);	
    }
    
    function insertHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan,$user, $nopv, $vbiayaadmin, $vpembulatan, $vpph)
    {
    	$mylib = new globallib();
        $this->locktables('pelunasan_hutang_header');
		
        $data = array(
            'NoTransaksi'	=> $notransaksi,
            'NoPaymentVoucher' => $nopv,
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'KdSupplier' => $kdSupplier,
            'MataUang' => $kd_uang,
            'Kurs' => $kurs,
            'NoBukti' => $nobukti,
            'Keterangan' => $keterangan,
            'BiayaAdmin' => $vbiayaadmin,
            'Pembulatan' => $vpembulatan,
            'NilaiPPH' => $vpph,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('pelunasan_hutang_header', $data);

        $this->unlocktables();
    }
    
     function insertDetail($notransaksi,$nofaktur,$sisa,$bayar,$norencanabayar){
    	$mylib = new globallib();
		$this->locktables('pelunasan_hutang_detail');
		$total = 0;
		$this->db->delete('pelunasan_hutang_detail', array('NoRencanaBayar' => $norencanabayar));
		for($i=0;$i<count($nofaktur); $i++){
			if($mylib->save_int($bayar[$i]>0)){
				$data = array(
					'NoRencanaBayar'=>$norencanabayar,
					'NoTransaksi' => $notransaksi,
					'NoFaktur' => $nofaktur[$i],
					'NilaiFaktur' => $mylib->save_int($sisa[$i]),
					'NilaiBayar' => $mylib->save_int($bayar[$i])
				);
				$total += $mylib->save_int($bayar[$i]);
				$this->db->insert('pelunasan_hutang_detail', $data);	
			}
		}
		$this->unlocktables();
		return $total;
	}
	
	function insertHutang($tgl,$notransaksi, $nofaktur, $kdsupplier,$kd_uang,$kurs, $bayar){
		$mylib = new globallib();
		for($i=0;$i<count($nofaktur); $i++){
			$vbayar = $mylib->save_int($bayar[$i]);
			$nodokumen = $nofaktur[$i];
			$kdrekening = $this->getKdRekening($nodokumen);
			if($vbayar*1>0){
				$data = array(
				'NoDokumen' => $notransaksi,
				'NoFaktur' => $nofaktur[$i], 
				'KdSupplier' => $kdsupplier,
				'TipeTransaksi' => 'P',
				'Tanggal' => $tgl,
				'JatuhTempo' => $tgl,
				'NilaiTransaksi' => $vbayar,
				'Sisa' => 0,
				'MataUang' => $kd_uang,
				'Kurs' => $kurs,
				'KdRekening' => $kdrekening
				);
				$this->db->insert('hutang', $data);
				
				$sql = "Update hutang set sisa = sisa-$vbayar, RencanaBayarFlg=0 where NoDokumen='$nodokumen'";
				$qry = $this->db->query($sql);
				
				$this->insertMutasiHutang($tgl,$notransaksi, $kdsupplier,$kd_uang,$kurs, $vbayar,$kdrekening);
			}
		}
	}
	
	function insertMutasiHutang($tgl,$nofaktur, $kdsupplier,$mata_uang,$kurs, $vbayar,$kdrekening){
		$mylib = new globallib();
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		if($this->isExistMutasiHutang($nofaktur,$tahun,$bulan,$kdrekening)){
			$sql = "Update mutasi_hutang set Jumlah=Jumlah+$vbayar where Tahun='$tahun' and Bulan='$bulan' and NoDokumen='$nofaktur'
					and KdRekening='$kdrekening'";
					
			$qry = $this->db->query($sql);		
		}else{
			$data = array(
				'Tahun' => $tahun,
				'Bulan' => $bulan, 
				'NoDokumen' => $nofaktur, 
				'MataUang' => $mata_uang,
				'Kurs' => $kurs,
				'KdSupplier' => $kdsupplier,
				'TipeTransaksi' => 'P',
				'Jumlah' => $vbayar,
				'KdRekening' => $kdrekening
			);
			$this->db->insert('mutasi_hutang', $data);	
		}
	}
	
	function isExistMutasiHutang($nodokumen,$tahun,$bulan,$kdrekening){
		$sql = "select * from mutasi_hutang where Tahun='$tahun' and Bulan='$bulan' and NoDokumen='$nodokumen' and KdRekening='$kdrekening'";
		$row = $this->getArrayResult($sql);
		if(count($row)>0){
			return true;
		}else{
			return false;
		}		
	}
	
	function getDataHeader($notransaksi){
    	$sql = "SELECT h.`NoTransaksi`, h.`NoPaymentVoucher`, DATE_FORMAT(h.TglDokumen,'%d-%m-%Y') as TglDokumen, k.`NamaKasBank`, s.`Nama` AS NamaSupplier, h.KdSupplier, h.KdKasBank, h.KdRekeningPPH, 
				h.`MataUang`, h.`Kurs`, h.`NoBukti`, h.`Keterangan`, h.`AddDate`, h.`AddUser`, h.`EditDate`, h.`EditUser`, h.BiayaAdmin, h.Pembulatan, NilaiPPH, h.NoRekeningSupplier  
				FROM pelunasan_hutang_header h 
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				INNER JOIN kasbank k ON h.`KdKasBank`=k.`KdKasBank`
				WHERE h.`NoTransaksi`='$notransaksi'";
        $row = $this->getArrayResult($sql);
        return $row[0];
	}
	
	function getDataDetail($notransaksi){
    	$sql = "SELECT p.`NoFaktur`, i.`NoFakturSupplier`, i.NoPenerimaan as NoRG, i.`NoPO`, i.`Tanggal`, p.`NilaiFaktur` as Sisa, p.`NilaiBayar` FROM pelunasan_hutang_detail p
				LEFT JOIN invoice_pembelian_header i ON p.`NoFaktur`=i.`NoFaktur`
				WHERE p.`NoTransaksi`='$notransaksi'";
        return $this->getArrayResult($sql);
	}
	
	function getDataDetail2($notransaksi){
    	$sql = "
    			SELECT 
				  p.`NoFaktur`,
				  i.`NoDokumen` AS NoFakturSupplier,
				  i.`RGNo` AS NoRG,
				  j.`PONo` AS NoPO,
				  i.`TglDokumen` AS Tanggal,
				  p.`NilaiFaktur` as Sisa,
				  p.`NilaiBayar` 
				FROM
				  pelunasan_hutang_detail p 
				  LEFT JOIN pi_marketing i 
				    ON p.`NoFaktur` = i.`NoDokumen`
				   LEFT JOIN rg_marketing j
				    ON i.`RGNo` = j.`NoDokumen` 
				WHERE p.`NoTransaksi`='$notransaksi'";
				
        return $this->getArrayResult($sql);
	}

	function getNoPV($kdkasbank,$tgl){
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
		$kd_no = $this->getKodeBank($kdkasbank);
		$notransaksi = $this->get_no_counter( $kd_no,'trans_payment_header', 'NoDokumen', $tahun,$bulan);
		
		return $notransaksi;
	}
	
	function insertPVHeader($nopv,$tgl,$kdkasbank,$kdSupplier,$keterangan,$total,$nobukti,$user,$biayaadmin, $pembulatan,$pph,$noreksupplier){
    	$mylib = new globallib();
        $this->locktables('trans_payment_header');
		$vbiayaadmin = $mylib->save_int($biayaadmin);
		$vpembulatan = $mylib->save_int($pembulatan);
		
        $data = array(
            'NoDokumen'	=> $nopv,
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'Penerima' => $this->getNamaSupplier($kdSupplier),
            'Keterangan' => $keterangan,
            'JumlahPayment' => $total+$vbiayaadmin+$vpembulatan-$pph,
            'Jenis' => 1,
            'Status' => 1,
            'NoRekening' => $noreksupplier,
            'NoBukti' => $nobukti,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('trans_payment_header', $data);

        $this->unlocktables();
    }
    
	function insertPVDetail($nopv,$tgl,$kdSupplier,$bayar,$nobukti,$nofaktur, $user, $vbiayaadmin, $vpembulatan, $vpph,$norencanabayar,$kdrekeningpph){
		$mylib = new globallib();
		$rekening = $this->getInterface();
			
		$this->locktables('trans_payment_detail');
		$urut = 1;
		for($i=0;$i<count($nofaktur); $i++){
			
			$kdrekening = $this->KdRekHutang($nofaktur[$i]);
			if($kdrekening==''){
				if(substr($nofaktur[$i],0,3)=='PIM' OR substr($nofaktur[$i],0,2)=='CM'){
				    $kdrekening = $rekening['HutangLain'];
				}else{
					$kdrekening = $rekening['HutangDagang'];
				}	
			}
			
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $vbayar,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Pelunasan Hutang ' . $this->getNamaSupplier($kdSupplier) . '-' . $nofaktur[$i],
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_payment_detail', $data);
				$urut++;
			}
		}
		if($vbiayaadmin>0){
			$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $rekening['AdminBank'],
				'Jumlah' => $vbiayaadmin,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Biaya Transfer ' . $this->getNamaSupplier($kdSupplier) . '-' . $nobukti,
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_payment_detail', $data);
				$urut++;
		}
		if($vpph>0){
			$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekeningpph,
				'Jumlah' => $vpph*-1,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'PPH 23 ' . $this->getNamaSupplier($kdSupplier) . '-' . $nobukti,
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_payment_detail', $data);
				$urut++;
		}
		if($vpembulatan<>0){
			if($vpembulatan>0){
				$rekselisih = $rekening['RugiSelisihBayar'];
			}else{
				$rekselisih = $rekening['LabaSelisihBayar'];
			}
			
			$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $rekselisih,
				'Jumlah' => $vpembulatan,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Selisih Pembayaran ' . $this->getNamaSupplier($kdSupplier) . '-' . $nobukti,
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_payment_detail', $data);
		}
		$this->unlocktables();
	}
	
	function getOutStandingUM($kdsupplier){
		$sql = "SELECT NoDokumen as NoUM, TglDokumen as TglUM, Keterangan as KeteranganUM, Sisa as NilaiUM FROM uang_muka_supplier WHERE kdsupplier='$kdsupplier' AND sisa>0";
		
		return $this->getArrayResult($sql);
	}
	
	function KdRekHutang($nofaktur){
		$sql = "SELECT KdRekening FROM hutang where NoDokumen='$nofaktur'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['KdRekening'];
	}
	
	function getDataOutStandingUM($notransaksi){
		$sql = "SELECT d.NoTransaksi,d.NoUM, d.Nilai as NilaiUM, u.TglDokumen as TglUM, u.Keterangan as KeteranganUM  FROM pelunasan_hutang_outstandingum_detail d inner join uang_muka_supplier u on d.NoUM=u.NoDokumen
				WHERE d.NoTransaksi='$notransaksi'";
		
		return $this->getArrayResult($sql);
	}
	
	function UpdateHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan,$user, $vbiayaadmin, $vpembulatan, $vpph,$norencanabayar,$nopv, $noreksupplier)
    {
    	$this->locktables('pelunasan_hutang_header');
		
		$date = date('Y-m-d');
		$sql = "Update pelunasan_hutang_header set NoTransaksi='$notransaksi', NoBukti='$nobukti', Keterangan='$keterangan', BiayaAdmin='$vbiayaadmin', 
				KdKasBank='$kdkasbank', Pembulatan='$vpembulatan', NilaiPPH='$vpph', EditUser='$user', EditDate='$date', Status='1',NoPaymentVoucher='$nopv',
				 NoRekeningSupplier='$noreksupplier' 
				where NoRencanaBayar='$norencanabayar'";
		$this->db->query($sql);
		
        $this->unlocktables();
    }
    
    function insertDetailUM($notransaksi, $noum, $nilai,$norencanabayar){
		$mylib = new globallib();
		$this->locktables('pelunasan_hutang_outstandingum_detail');
		$total = 0;
		
		$this->db->delete('pelunasan_hutang_outstandingum_detail', array('NoTransaksi' => $norencanabayar));
		if($noum != ''){
			for($i=0;$i<count($noum); $i++){
				$data = array(
					'NoTransaksi'=>$notransaksi,
					'NoUM' => $noum[$i],
					'Nilai' =>  $mylib->save_int($nilai[$i])
				);
				$this->db->insert('pelunasan_hutang_outstandingum_detail', $data);
			}	
		}
		
		$this->unlocktables();
	}
	
	function getKodeBank($kdkasbank){
		$sql = "SELECT KdPembayaran FROM kasbank where KdKasBank='$kdkasbank'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['KdPembayaran'];
	}
	
	function getNamaSupplier($kdsupplier){
		$sql = "SELECT Nama FROM supplier where KdSupplier='$kdsupplier'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['Nama'];
	}
	
	function getKdRekening($nofaktur){
		$sql = "SELECT KdRekening FROM hutang where NoDokumen='$nofaktur' ";
		
		$row =  $this->getArrayResult($sql);
		return $row[0]['KdRekening'];
	}
	
	function locktables($table)
	{
		//$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		//$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }

	function getNoRekSupplier($kdsupplier){
		$sql = "SELECT CONCAT(NamaBank,' ',NoRekening,' a.n ', NamaPemilik) AS NoRekSupplier FROM rekening_supplier where KdSupplier='$kdsupplier'";
    	$result = $this->getArrayResult($sql);
    	return $result;
	}
	    
    function getInterface(){
		$sql = "select * from interface";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0];
	}
	
	function getRekeningPPH(){
		$sql = "Select KdRekening, NamaRekening from rekening where KdRekening in ('21020101','21020102','21020103','21020104','21020105','21020106')";
		return $this->getArrayResult($sql);
	}
}
?>