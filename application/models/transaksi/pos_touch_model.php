<?php
class pos_touch_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getList($num,$offset,$id,$with, $ipaddress)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
		$datakassa = $this->getkassa($ipaddress);
		$kategori = $datakassa[0]['KdKategori'];
		
    	// $sql = "select date_format(h.Tanggal,'%d-%m-%Y') as Tanggal,h.KdMeja, count(h.KdMeja) as `Order`,
    	//  # sum(d.status) as Selesai
    	//  SUM(IF(d.status!=0,1,0)) AS Selesai
		// 		from trans_order_header h inner join trans_order_detail d on h.notrans=d.notrans
		// 		inner join kassa k on h.NoKassa = k.id_kassa
		// 		where h.status=0 and k.KdKategori='$kategori' AND h.`Tanggal`=  CURDATE() and d.Status<>2
		// 		group by h.Tanggal, h.KdMeja
		// 		order by  h.Tanggal, h.KdMeja Limit $off?set,$num
		// 	";

			$sql="
			     SELECT
				    orderan.NoTrans,
					orderan.Tanggal,
					orderan.KdMeja,
					COUNT(orderan.Order) AS `Order`,
					SUM(orderan.Selesai) AS Selesai,
					OrderFrom
					FROM
					(SELECT
						h.NoTrans AS NoTrans,
						DATE_FORMAT(h.Tanggal, '%d-%m-%Y') AS Tanggal,
						h.KdMeja,
						h.KdMeja AS `Order`,
						IF(d.`Status` != 0, 1, 0) AS Selesai,
						'Order Touch' AS OrderFrom
					FROM
						trans_order_header h
						INNER JOIN trans_order_detail d
						ON h.notrans = d.notrans
						INNER JOIN kassa k
						ON h.NoKassa = k.id_kassa
					WHERE h.`Status` = 0
						AND k.KdKategori = '$kategori'
						AND h.`Tanggal` = CURDATE()
						AND d.`Status` <> 2
					
					UNION
					ALL
					SELECT
						h.NoStruk AS NoTrans,
						DATE_FORMAT(h.Tanggal, '%d-%m-%Y') AS Tanggal,
						h.KdMeja,
						h.KdMeja AS `Order`,
						IF(d.`Status` != 2, 1, 0) AS Selesai,
						'POS Touch' AS OrderFrom
					FROM
						transaksi_header h
						INNER JOIN transaksi_detail d
						ON h.NoStruk = d.NoStruk
						INNER JOIN kassa k
						ON h.NoKassa = k.id_kassa
					WHERE h.`Status` = 0
						AND k.KdKategori = '$kategori'
						AND h.`Tanggal` = CURDATE()
						AND d.`Status` = 0
					) AS orderan
					GROUP BY orderan.Tanggal,
						orderan.KdMeja
					ORDER BY orderan.Tanggal,
					orderan.KdMeja
					LIMIT $offset,$num;
					";
			
			//left join kendaraan on kendaraan.KdKendaraan = h.KdKendaraan
			//left join personal on kendaraan.KdPersonal = personal.KdPersonal
		return $this->getArrayResult($sql);
    }
    
    function num_order_row($id,$with,$ipaddress){
     	$clause="";
     	$datakassa = $this->getkassa($ipaddress);
		$kategori = $datakassa[0]['KdKategori'];
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%'";
			}
			else
			{
				$clause = " where $with = '$id'";
			}
		}
		$sql = "SELECT NoTrans FROM trans_order_header h
				inner join kassa k on h.NoKassa = k.id_kassa
				where h.status=0 and k.KdKategori='$kategori' 
		 $clause";
        return $this->NumResult($sql);
	}
	function getDate(){
    	$sql = "SELECT TglTrans,date_format(TglTrans,'%d-%m-%Y') as TglTrans2 from aplikasi";
        return $this->getRow($sql);
    }
	function FindBar($bar)
	{
		$sql = "SELECT PCode from masterbarang where BarcodeSatuanKecil='$bar'";
		return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	//=================================================================================================
	function getkassa($param1)
	{
    	$sql = "select * from kassa where ip='$param1'";
		// echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function voucher($id_voucher) {
        $sql = "SELECT novoucher,keterangan,nominal,rupdisc,jenis,STATUS FROM voucher WHERE novoucher='$id_voucher'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
         if(empty($row)){
            echo  "salah";
        }else{
            echo 'datajson = ' . json_encode($row);
        }
       
    }
    
//    function ketentuandisc($pcode) {
//    	$tgl = date('Y-m-d');
//        $sql = "SELECT d.`Nilai`
//				FROM discountheader h INNER JOIN discountdetail d ON h.`NoTrans`=d.`NoTrans`
//				WHERE h.`TglAkhir`>='$tgl' AND h.`TglAwal`<='$tgl' and d.pcode='$pcode' AND h.`Status`=1";
//        $qry = $this->db->query($sql);
//        $row = $qry->result_array();
//         if(empty($row)){
//            return 0;
//        }else{
//            return $row[0]['Nilai'];
//        }
//    }

    function ketentuandisc($pcode) {

        $day= date('N', strtotime(date("Y-m-d"))); 
        $flag=0;

        $tgl = date('Y-m-d');
        $sql = "SELECT d.`Nilai`, h.berlaku
		FROM discountheader h INNER JOIN discountdetail d ON h.`NoTrans`=d.`NoTrans`
		WHERE h.`TglAkhir`>='$tgl' AND h.`TglAwal`<='$tgl' and d.pcode='$pcode' AND h.`Status`=1";
        $qry = $this->db->query($sql); 
        $row = $qry->result_array();
       
        if(empty($row)){
            return 0;
        }else{
            $pisah_hari= explode("#",$row[0]['berlaku']);
            $tanda_pager = substr_count($row[0]['berlaku'], "#");
            for($x=0; $x<=($tanda_pager*1); $x++){
                if($day==$pisah_hari[$x])
                    $flag+=1;
            }
            if($flag>0)
                return $row[0]['Nilai'];
            else
                return 0;
        }
    }
    
    function hargagofood($pcode) {
    	$tgl = date('Y-m-d');
        $sql = "SELECT d.Harga FROM groupharga_header h INNER JOIN groupharga_detail d ON h.`GroupHargaID`=d.`GroupHargaID`
				WHERE d.`PCode`='$pcode' AND h.`GroupHargaName`='GOFOOD'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
         if(empty($row)){
            return 0;
        }else{
            return $row[0]['Harga'];
        }
    }

    function hargagrabfood($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT d.Harga FROM groupharga_header h INNER JOIN groupharga_detail d ON h.`GroupHargaID`=d.`GroupHargaID`
				WHERE d.`PCode`='$pcode' AND h.`GroupHargaName`='GRABFOOD'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if (empty($row)) {
			return 0;
		} else {
			return $row[0]['Harga'];
		}
	}

	function hargatokopedia($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT d.Harga FROM groupharga_header h INNER JOIN groupharga_detail d ON h.`GroupHargaID`=d.`GroupHargaID`
				WHERE d.`PCode`='$pcode' AND h.`GroupHargaName`='TOKOPEDIA'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if (empty($row)) {
			return 0;
		} else {
			return $row[0]['Harga'];
		}
	}

	function hargashopee($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT d.Harga FROM groupharga_header h INNER JOIN groupharga_detail d ON h.`GroupHargaID`=d.`GroupHargaID`
				WHERE d.`PCode`='$pcode' AND h.`GroupHargaName`='SHOPEE'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if (empty($row)) {
			return 0;
		} else {
			return $row[0]['Harga'];
		}
	}

	function hargatraveloka($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT d.Harga FROM groupharga_header h INNER JOIN groupharga_detail d ON h.`GroupHargaID`=d.`GroupHargaID`
				WHERE d.`PCode`='$pcode' AND h.`GroupHargaName`='TRAVELOKAEATS'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if (empty($row)) {
			return 0;
		} else {
			return $row[0]['Harga'];
		}
	}
	
	function getnoipkassa($param1)
	{
    	$sql = "select * from kassa where id_kassa='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getnokassa($param1)
	{
    	$sql = "select * from kassa where ip='$param1'";
		//echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row[0]['id_kassa'];
    }
	
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function kurs($valas)
	{
		$sql = "SELECT NilaiTukar AS Nilai FROM mata_uang WHERE Kd_Uang='$valas'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
    function masterbarang()
	{
    	$sql = "SELECT PCode, NamaLengkap,FlagReady from masterbarang ORDER BY NamaLengkap ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
	
	function getBarang($kddivisi)
	{
    	$sql = "SELECT 
				  a.pcode,
				  a.namalengkap,
				  a.flagready,
				  s.kategoriparent AS kdkategori,
				  t.subkategoriparent AS kdsubkategori,
				  ROUND(a.harga1c) AS harga,
				  a.Satuan1 AS satuan,
				  a.komisi,
				  a.printer,
				  a.DiscInternal AS disc 
				FROM
				  masterbarang_touch a 
				  INNER JOIN masterbarang b 
				    ON a.pcode = b.pcode 
				  LEFT JOIN 
				    (SELECT 
				      kdkategori,
				      kdparent AS kategoriparent 
				    FROM
				      kategoripos) s 
				    ON s.kdkategori = a.kdkategori 
				  LEFT JOIN 
				    (SELECT 
				      kdsubkategori,
				      kdparent AS subkategoriparent 
				    FROM
				      subkategoripos) t 
				    ON t.kdsubkategori = a.kdsubkategori 
				WHERE a.Jenis = '2' 
				  AND b.Status = 'A' 
				  AND a.KdKategori = '$kddivisi'
				ORDER BY a.namalengkap ASC ;

				";
		// echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function getBarang_20190917()
	{
    	$sql = "select a.pcode,a.namalengkap,a.flagready,s.kategoriparent as kdkategori,t.subkategoriparent as kdsubkategori,
		        round(a.harga1c) as harga,a.Satuan1 as satuan,a.komisi,a.printer,a.DiscInternal as disc from 
		        masterbarang_touch a
		        inner join masterbarang b on a.pcode=b.pcode
		        left join
				(
				select kdkategori,kdparent as kategoriparent from kategoripos
				)s on s.kdkategori=a.kdkategori
				left join
				(
				select kdsubkategori,kdparent as subkategoriparent from subkategoripos
				)t on t.kdsubkategori=a.kdsubkategori
				where a.Jenis='2' and b.Status='A' and b.KdDivisi in(1,2)
				ORDER BY a.namalengkap ASC
				";
		echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getKategori()
	{
    	$sql = "select a.kdparent as kdkategori,s.namakategori from 
		        (SELECT distinct kdparent FROM kategoripos)a
				left join
				(
				select kdkategori,namakategori from kategoripos
				)s 
				on s.kdkategori=a.kdparent order by kdkategori";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getSubKategori()
	{
    	$sql = "select a.kdparent as kdsubkategori,s.kdsubkategori,s.namasubkategori,s.kdkategori from 
		        (SELECT distinct kdparent FROM subkategoripos)a
				left join
				(SELECT kdkategori,kdsubkategori,namasubkategori FROM subkategoripos)s
				on s.kdsubkategori=a.kdparent
				ORDER BY s.kdsubkategori ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getKeterangan()
	{
    	$sql = "SELECT keterangan FROM keterangan";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getUserId()
	{
    	$sql = "SELECT id,username,password FROM user";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getLokasi($kddivisi)
	{
    	$sql = "SELECT kdlokasi,keterangan FROM lokasipos WHERE kdtipelokasi='01' and kddivisi='$kddivisi' ORDER BY urutan ASC";
		// echo $sql;
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function do_simpan_voucher($no,$nokassa,$tgl,$voucher0,$vouchpakai0,$vouchjenis0) 
	{
		//if($vouchjenis0=='5'){
			//$vouchpakai0 = 0;
		//}else{
			$vouchpakai0 = $vouchpakai0;
		//}
		
        $sql = "INSERT INTO transaksi_detail_voucher(NoKassa,NoStruk,Tanggal,Jenis,NomorVoucher,NilaiVoucher)VALUE
                ('$nokassa','$no','$tgl','$vouchjenis0','$voucher0',$vouchpakai0)"; //tambahin field baru disini
        $qry = $this->db->query($sql);
		if($vouchjenis0<>'3')
		{
			$sql = "UPDATE voucher SET terpakai=terpakai+$vouchpakai0 where novoucher='$voucher0'";
			$qry = $this->db->query($sql);
		}

		//VOUCHER KARYAWAN
		if($vouchjenis0 == '3'){
			$sql = "UPDATE voucher_employee SET terpakai=terpakai+$vouchpakai0 where nik='$voucher0' AND status = '1' AND expDate >= '$tgl' AND expDate <= LAST_DAY(DATE(NOW()))";
			$qry = $this->db->query($sql);
		}
    }
    
    function do_simpan_photo($notrans,$kassa,$tgl,$kdphoto0) 
	{
        $sql = "INSERT INTO transaksi_detail_photo(NoKassa,NoStruk,Tanggal,NomorPhoto)VALUE
                ('$kassa','$notrans','$tgl','$kdphoto0')";
        $qry = $this->db->query($sql);
    }
	
	function do_simpan_compliment($id_compliment,$bruto)
	{
		$sql = "UPDATE compliment SET terpakai=terpakai+$bruto where nik='$id_compliment'";
		$qry = $this->db->query($sql);
	}
	
	function order_temp($nokassa)
	{
    	$sql = "select st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan, st.NoTrans
				from order_temp st, masterbarang mb 
				where st.PCode = mb.PCode and st.Qty<>'0' and st.NoKassa='$nokassa'
				order by st.NoUrut asc";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
    
    function order_temp_count($nokassa)
    {
    	$sql = "SELECT COUNT(PCode) as total FROM order_temp WHERE Qty<>'0' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
    }
	
	function DeleteRecord($NoUrut,$nokassa)
	{
		if($NoUrut == '')
		{
			$LastRecord = $this->LastRecord(0,$nokassa);
			$AutoID     = $LastRecord[0]['AutoID'];
			
			$sql = "delete from order_temp where AutoID = '$AutoID' and NoKassa='$nokassa'";
			$qry = $this->db->query($sql);
		}
		else
		{
			$sql = "delete from order_temp where NoUrut = '$NoUrut' and NoKassa='$nokassa'";
			$qry = $this->db->query($sql);
		}
    }
	
	function LastRecord($echoFlg,$nokassa)
	{
    	$sql = "select st.AutoID, st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan
				from order_temp st, masterbarang mb
				where st.PCode = mb.PCode and st.NoKassa='$nokassa'
				order by st.NoUrut desc limit 1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
		if($echoFlg == 1){
        	echo 'datajson = '.jsonEncode($row);
		}else{
			return $row;
		}
    }
	
	function EditRecord($NoUrut,$nokassa)
	{
    	$sql = "select st.AutoID, st.NoUrut, st.PCode as KodeBarang, mb.NamaStruk, st.Qty, st.Satuan, st.Keterangan
				from order_temp st, masterbarang mb
				where st.PCode = mb.PCode and st.NoUrut = '$NoUrut' and st.NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
		
		echo 'datajson = '.jsonEncode($row);
    }
    
    function order_temp_cek($kdbrg,$nokassa)
	{
    	$sql = "select PCode as KodeBarang from order_temp where PCode='$kdbrg' and nokassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        
        return $row;
    }
    
    function order_temp_add($jumlah,$kdbrg,$NoTrans,$nokassa,$keterangan)
    {
		$sql = "UPDATE order_temp SET Qty = Qty + round($jumlah,2), Keterangan='$keterangan' WHERE NoTrans='$NoTrans' and PCode='$kdbrg' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
	}
    	
	function save_trans_detail($notrans,$nokassa,$kdcontact,$kdmeja,$kdpersonal)
	{
		$sql = "INSERT INTO trans_order_detail(`NoKassa`,`NoTrans`,`NoUrut`,`Tanggal`,`Waktu`,`Kasir`,`PCode`,`Qty`,`Satuan`,`Keterangan`,`KdStore`,`Status`,`KdPersonal`,`KdContact`)
				SELECT NoKassa,NoTrans,NoUrut,Tanggal,Waktu,Kasir,PCode,Qty,Satuan,Keterangan,KdStore,Status,'$kdpersonal','$kdcontact' FROM order_temp where NoTrans='$notrans' and Qty<>'0' and NoKassa='$nokassa'";//tambahin field baru disini
		$qry = $this->db->query($sql);    
	}
	
	function save_trans_header($notrans,$nokassa,$kdcontact,$kdmeja,$kdpersonal,$idguest,$adddate)
	{
	    //echo "waktu1";
		$sql = "INSERT INTO trans_order_header(`NoKassa`,`NoTrans`,`Tanggal`,`Waktu`,`Kasir`,`TotalItem`,`KdStore`,`Status`,`KdPersonal`,`KdMeja`,`KdContact`,`TotalGuest`,`AddDate`)
				SELECT MIN(NoKassa),MIN(NoTrans),MIN(Tanggal),MIN(Waktu),MIN(Kasir),COUNT(PCode),MIN(KdStore),MIN(Status),'$kdpersonal','$kdmeja','$kdcontact','$idguest','$adddate' FROM order_temp where NoTrans='$notrans' and Qty<>'0' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
		
		//$sql = "update kassa set notrans=notrans+1 where nokassa='$nokassa'";
		//$qry = $this->db->query($sql);
	}
	
	function clear_trans($notrans,$nokassa)
	{
		$sql = "delete from order_temp where NoTrans = '$notrans' and NoKassa='$nokassa'";
		$qry = $this->db->query($sql);
    }
	
	function save_order_table($kdmeja)
    {
		$sql = "UPDATE Lokasi SET Status=Status+1 WHERE KdLokasi='$kdmeja'";
		//echo $sql;
		$qry = $this->db->query($sql);
	} 
	
	function no_trans($bulan,$tahun,$kategori)
	{
		$this->locktables('counter');
		//echo $kategori;
		if($kategori=='01')
		{
		$sql = "update counter set NoStruk01=NoStruk01+1 where bulan='$bulan' and tahun='$tahun'";
		}
		if($kategori=='02')
		{
		$sql = "update counter set NoStruk02=NoStruk02+1 where bulan='$bulan' and tahun='$tahun'";
		}
		if($kategori=='03')
		{
		$sql = "update counter set NoStruk03=NoStruk03+1 where bulan='$bulan' and tahun='$tahun'";
		}
		//echo $sql;
    	$qry = $this->db->query($sql);
		
		$this->unlocktables();
		
		if($kategori=='01')
		$sql = "select NoStruk01 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
		if($kategori=='02')
		$sql = "select NoStruk02 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
		if($kategori=='03')
		$sql = "select NoStruk03 as NoTrans from counter where bulan='$bulan' and tahun='$tahun'";
		
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function ambil_No($tahun,$bulan, $tgl, $nokassa) {
    	$tahun = $tahun-2000;
    	$tglhari = substr($tgl, 8, 2)*1;
    	$tglhari = $tglhari<10 ? '0'.$tglhari : $tglhari;
    	
    	$kassa = $nokassa<'10' ? '0'.$nokassa : $nokassa;
    	$nostruk = $tahun.$bulan.$tglhari.$kassa;
    	$bulan = $bulan * 1;
    	
        //$sql = "SELECT NoStruk FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
        $sql = "SELECT NoStruk FROM transaksi_header WHERE nokassa='$nokassa' AND tanggal='$tgl'
        		AND nostruk LIKE '$nostruk%' order by NoStruk Desc limit 1"; 
		
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if(count($row) == 0){
			$hasil = $nostruk . '001';
		}else{
			$lastnostruk = substr($row[0]['NoStruk'],-3)*1;
			$newno = $lastnostruk + 1;
			if($newno<10){
				$newno = '00'.$newno;
			}elseif($newno<100){
				$newno = '0'.$newno;
			}
			$hasil = $nostruk.$newno;
		}
        return $hasil;
    }

	
	
	function no_trans_temp()
	{
		$sql = "select NoTrans,NoKassa,Kasir from order_temp LIMIT 1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	function customer($pelanggan)
	{
		$sql = "select * from customer where KdCustomer='$pelanggan'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
       echo 'datajson = '.jsonEncode($row);
	}
	
	
	function trans_header()
	{
		$sql = "select * from trans_order_header where Status = '0' order by NoTrans DESC LIMIT 1";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        
        return $row;
	}
	
	
	function NamaPrinter($id) {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function all_trans($id,$kassa) {
        $sql = "select h.*, k.IsCounter, m.JumlahPoint, COALESCE(gh.NamaGroupDisc,'') as NamaGroupDisc, 
        			COALESCE(m.NamaMember,'') as NamaMember, tm.NilaiPoint  
        			from transaksi_header h inner join kassa k on h.NoKassa = k.id_kassa 
        			left join member m on h.KdMember=m.KdMember
        			left join type_member tm on m.KdTipeMember=tm.KdTipeMember
        			left join group_disc_header gh on h.NamaCard=gh.KdGroupDisc
        			where h.NoStruk = '$id' and h.NoKassa='$kassa' order by NoStruk DESC LIMIT 1"; 
        $qry = $this->db->query($sql);
		$row = $qry->result_array();
		
		if(empty($row)){
			$sql = "select h.*, k.IsCounter, m.JumlahPoint, COALESCE(gh.NamaGroupDisc,'') as NamaGroupDisc, 
        			COALESCE(m.NamaMember,'') as NamaMember, tm.NilaiPoint  
        			from transaksi_header_sunset h inner join kassa k on h.NoKassa = k.id_kassa 
        			left join member m on h.KdMember=m.KdMember
        			left join type_member tm on m.KdTipeMember=tm.KdTipeMember
        			left join group_disc_header gh on h.NamaCard=gh.KdGroupDisc
        			where h.NoStruk = '$id' and h.NoKassa='$kassa' order by NoStruk DESC LIMIT 1";
			$qry = $this->db->query($sql);
			$row = $qry->result_array();
		}
        return $row;
    }

    function get_point($notrans,$tipe){
    	$sql = "SELECT JumlahPoint AS `point` FROM mutasi_point WHERE NoTransaksi ='$notrans' AND TipeTransaksi ='$tipe' ";
    	$qry = $this->db->query($sql);
    	$point=0;
    	if($qry->num_rows() > 0){
    		$row = $qry->result_array();
	    	$point = $row[0]['point'];
    	}
    	return $point;
    }
    function get_voucher_employee($nik, $TglTrans){
		$sql = "SELECT nik,nominal, nominal - terpakai AS sisa FROM voucher_employee WHERE nik = '$nik' AND status = '1' AND expDate > '$TglTrans' AND expDate <= LAST_DAY('$TglTrans')"; 
		$qry = $this->db->query($sql);
		$sisaVoucher = array();
		if ($qry->num_rows() > 0) {
			$row = $qry->result_array();
			$sisaVoucher = array(
				'nik' => $row[0]['nik'],
				'nominal' => $row[0]['nominal'],
				'sisa' => $row[0]['sisa']
			); 
		}
		return $sisaVoucher;
	}
    function det_trans($id,$kassa) {
        $sql = "select a.PCode,b.NamaLengkap as NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			from transaksi_detail a, masterbarang b where a.NoStruk='$id' and a.NoKassa='$kassa'
			and a.PCode=b.Pcode order by Waktu ASC"; 
        $qry = $this->db->query($sql);
		$row = $qry->result_array();
		
		if(empty($row)){
		$sql = "select a.PCode,b.NamaLengkap as NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			from transaksi_detail_sunset a, masterbarang_touch b where a.NoStruk='$id' and a.NoKassa='$kassa'
			and a.PCode=b.Pcode order by Waktu ASC";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
		}

        return $row;
    }
    
    function det_trans_second($id,$kassa) {
        $sql = "select a.PCode,b.NamaLengkap as NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			from transaksi_detail a, masterbarang_pos b where a.NoStruk='$id' and a.NoKassa='$kassa'
			and a.PCode=b.Pcode order by Waktu ASC";
        $qry = $this->db->query($sql);
		$row = $qry->result_array();
		
		if(empty($row)){
		$sql = "select a.PCode,b.NamaLengkap as NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			from transaksi_detail_sunset a, masterbarang_pos b where a.NoStruk='$id' and a.NoKassa='$kassa'
			and a.PCode=b.Pcode order by Waktu ASC";
        $qry = $this->db->query($sql);
		$row = $qry->result_array();
		}

        return $row;
    }
	
	function ifPCodeBarcode($id){
		$bar = substr($id,0,20);
		$sql = "select b.*,keterangan as NamaSatuan from(
				SELECT PCode,NamaLengkap as NamaInitial,KonversiJualKecil,KonversiBesarKecil,KonversiTengahKecil,
				KdSatuanJual,if(PCode='$id',concat('K','*&^%',KdSatuanKecil,'*&^%',HargaJualKecil),
				if(BarcodeSatuanBesar='$bar',concat('B','*&^%',KdSatuanBesar,'*&^%',HargaJualTengah),
				if(BarcodeSatuanTengah='$bar',concat('T','*&^%',KdSatuanTengah,'*&^%',HargaJualBesar),
				concat('K','*&^%',KdSatuanKecil,'*&^%',HargaJualKecil)))) as Jenis FROM masterbarang 
				Where PCode='$id' or BarcodeSatuanBesar='$bar' or BarcodeSatuanTengah='$bar' or 
				BarcodeSatuanKecil='$bar'
				)b
				left join
				(
				select NamaSatuan,Keterangan from satuan
				) s 
				on s.NamaSatuan=b.KdSatuanJual";		
		//echo $sql;
		return $this->getRow($sql);
	}
	
	function getPCodeDet($kode)
	{
		$sql = "
				select b.*,keterangan as NamaSatuan from(
				SELECT PCode,NamaLengkap as NamaInitial,KonversiJualKecil,KonversiBesarKecil,KonversiTengahKecil,KdSatuanJual 
				FROM masterbarang Where PCode='$kode'
				) b
				left join
				(
				select NamaSatuan,Keterangan from satuan
				) s 
				on s.NamaSatuan=b.KdSatuanJual";
        return $this->getRow($sql);
	}
	
	
	function getcontact($kode)
	{
		$sql = "select * FROM contact Where KdContact='$kode'";
        return $this->getRow($sql);
	}
	
	function getpersonal($kode)
	{
		$sql = "select * FROM personal Where KdPersonal='$kode'";
        return $this->getRow($sql);
	}
	
	function order_temp_cek_ada($notrans,$nokassa)
	{
    	$sql = "select PCode as KodeBarang from order_temp where nokassa='$nokassa' and NoTrans='$notrans' and Qty<>'0'";
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        return $row;
    }
	
	function getprintjob($nokassa)
	{
    	$sql = "select a.*,b.jenisprint from kassaprint a, kassa b where a.nokassa='$nokassa' and b.nokassa='$nokassa'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
	
	function cekDataOrder($kdmeja){
    	$tgl = date('Y-m-d');
		$sql = "SELECT * FROM transaksi_header a WHERE a.Tanggal='$tgl' AND a.KdMeja='$kdmeja' AND a.Status='0'"; 
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
    
    function getDetailOrder($kdmeja){
    	$tgl = date('Y-m-d');
		$sql = "SELECT  h.KdMeja,
						AVG(h.TotalGuest) AS TotalGuest,
						h.KdAgent, 
						d.PCode, 
						b.`NamaLengkap`,
						SUM(d.`Qty`) AS Qty,
						b.Satuan1 AS Satuan,
						AVG(b.`Harga1c`) AS Harga,
						'0' AS Diskon,
						b.Komisi,
						b.DiscInternal,
						COALESCE(ds.Nilai,0) as KDisc
					FROM trans_order_header h INNER JOIN trans_order_detail d ON h.notrans=d.notrans 
					INNER JOIN masterbarang_touch b ON d.`PCode`=b.`PCode`
					left join(
						SELECT d.`PCode`, d.`Nilai`
						FROM discountheader h INNER JOIN discountdetail d ON h.`NoTrans`=d.`NoTrans`
						WHERE h.`TglAkhir`>='$tgl' AND h.`TglAwal`<='$tgl' AND h.`Status`=1
					) ds on d.pcode=ds.PCode
					WHERE h.tanggal='$tgl' AND h.status=0 AND h.`KdMeja`='$kdmeja' AND d.Status= '1'
					GROUP BY d.`PCode`";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
	
	function geHeaderOrder($kdmeja){
    	$tgl = date('Y-m-d');
		$sql = "SELECT NoTrans,'' AS NoStruk, '0' AS nilaidisc, '' AS userdisc,'0' AS Voucher, '0' AS VoucherTravel FROM trans_order_header h 
					WHERE h.tanggal='$tgl' AND h.status=0 AND h.`KdMeja`='$kdmeja'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}

	function getDetailTrans($no_struk){
    	$tgl = date('Y-m-d');
		$sql = "SELECT  h.KdMeja, 
						AVG(h.TotalGuest) AS TotalGuest, 
						h.KdAgent, 
						d.PCode, 
						b.`NamaLengkap`, 
						SUM(d.`Qty`) AS Qty, 
						b.Satuan1 AS Satuan, 
						AVG(b.`Harga1c`) AS Harga,
						d.`Disc1` AS Diskon, 
						b.Komisi, 
						b.DiscInternal, 
						COALESCE(ds.Nilai,0)  as KDisc
					FROM transaksi_header h
					INNER JOIN transaksi_detail d
						ON h.`NoStruk` = d.`NoStruk`
					INNER JOIN masterbarang_touch b
						ON d.`PCode` = b.`PCode`
					left join(
						SELECT d.`PCode`, d.`Nilai`
						FROM discountheader h INNER JOIN discountdetail d ON h.`NoTrans`=d.`NoTrans`
						WHERE h.`TglAkhir`>='$tgl' AND h.`TglAwal`<='$tgl' AND h.`Status`=1
					) ds on d.pcode=ds.PCode
					WHERE h.NoStruk='$no_struk'
					GROUP BY d.`PCode`";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
	
	function getHeaderTrans($no_struk){
    	$tgl = date('Y-m-d');
		$sql = "SELECT NoStruk AS NoTrans,NoStruk,nilaidisc,userdisc,Voucher,VoucherTravel  FROM transaksi_header h 
					WHERE h.NoStruk='$no_struk'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}

	function getNoStruk($kdmeja){
    	$tgl = date('Y-m-d');
		$sql = "SELECT NoStruk FROM transaksi_header h 
					WHERE h.tanggal='$tgl' AND h.status=0 AND h.`KdMeja`='$kdmeja'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}

	function getNoStrukOrderTouch($kdmeja){
    	$tgl = date('Y-m-d');
		$sql = "SELECT '' AS NoStruk FROM transaksi_header h 
					WHERE 1 LIMIT 0,1";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}
	
	function updateorder($orderid, $nostruk){
		$vorderid = explode("*",$orderid);
		
		for($i=0;$i<count($vorderid);$i++){
			$noorder = $vorderid[$i];
			$sql = "Update trans_order_header set NoStruk='$nostruk', `status`= 1 where NoTrans='$noorder'";
			$qry = $this->db->query($sql);
		}
	}
	
	function updatepointmember($kdmember, $nettosales, $bayarpoint,$no_transaksi){

		/* ketentuan point
			minimal belanja 100.000 dapat 5.000 point 
			kelipatan 50.000 dapat 2.500 point
		*/

		$bayarpoint = $bayarpoint*1;
		$point =0;
		if($nettosales >= 100000){
			$nettosales -= 100000;
			$point = 5000;
			$lipat = 50000;
			
			for ($i= 1; $i <= $nettosales; $i++) { 
				if ( $bagi = $i % $lipat == 0 ) {
					 $point += 2500;
				}
			}	
		}

		if($point >0){
			$this->mutasi_point_member($no_transaksi,$kdmember,$point,'I');
		}

		if($bayarpoint > 0){
			$this->mutasi_point_member($no_transaksi,$kdmember,$bayarpoint,'O');
		}
		$point -= $bayarpoint;
		$sql = "UPDATE member SET JumlahPoint = JumlahPoint+'$point' WHERE KdMember='$kdmember' ";

		$qry = $this->db->query($sql);
	}
	
	function mutasi_point_member($no_transaksi,$kd_member,$point,$tipe){
		$sql = "INSERT INTO mutasi_point SET NoTransaksi ='$no_transaksi', KdMember ='$kd_member',JumlahPoint ='$point', TipeTransaksi ='$tipe' ";
		$this->db->query($sql);
	}
	function getOpenOrder_20190911($tgl, $kdmeja){
		$sql = "select NoTrans, KdAgent, TotalGuest from trans_order_header where tanggal='$tgl' and kdmeja='$kdmeja' and status=0 limit 1";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        if(count($row)>0)
        	return $row[0];
        else
        	return array('NoTrans'=>'','KdAgen'=>'','TotalGuest'=>'');
	}

	function getOpenOrder($tgl, $kdmeja){
		$sql = "SELECT NoTrans, KdAgent, TotalGuest FROM trans_order_header WHERE tanggal='$tgl' AND kdmeja='$kdmeja' AND status=0 LIMIT 1";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(count($row)>0){
			return $row[0]+array('Status' =>'order-touch');
		}
		else{

			#Cek table gantung, karena belum commit
			$sql2 = "SELECT NoStruk AS NoTrans,KdAgent,TotalGuest FROM transaksi_header WHERE Tanggal='$tgl' AND KdMeja='$kdmeja' AND `Status`=0 LIMIT 1"; 
			$qry2 = $this->db->query($sql2);
			$row2 = $qry2->result_array();
			if(count($row2)>0){
				return $row2[0]+array('Status' =>'pos-touch');
			}else{
				return array('NoTrans'=>'','KdAgen'=>'','TotalGuest'=>'','Status'=>'');
			}
			
		}
	}

	function getgroupdisc($namacard, $store, $pcode){
		$sql1 = "SELECT KdGroupBarang FROM masterbarang WHERE pcode='$pcode'";
		$qry = $this->db->query($sql1);
        $row = $qry->result_array();
        $kdgroupbarang = $row[0]['KdGroupBarang'];

		$sql2 = "SELECT d.`PersenDisc`, h.MaxDiscount FROM group_disc_header h INNER JOIN group_disc_detail d ON h.`KdGroupDisc`=d.`KdGroupDisc`
				WHERE d.`KdGroupDisc`='$namacard' AND d.`KdStore`='$store' AND d.`KdGroupBarang`='$kdgroupbarang' 
				AND h.BerlakuMulai <= DATE(NOW()) AND h.BerlakuSampai >= DATE(NOW())";
		
		$qry = $this->db->query($sql2);
		$row = $qry->result_array();
		if(empty($row)){
			return 0;
		}else{
			return $row[0]['PersenDisc']."#".$row[0]['MaxDiscount'];
		}
		
	}

	function getServiceCharge($pcode) {
		$tgl = date('Y-m-d');
		$sql = "SELECT a.Service_charge, a.PPN FROM masterbarang a WHERE a.PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		if(empty($row)){
			return array('SC'=>0,'PPN'=>0);
		}else{
			$return = array('SC'=>$row[0]['Service_charge'],'PPN'=>$row[0]['PPN']);
			return $return;
		}
	}
	
}



