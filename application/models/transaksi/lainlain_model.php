<?php

class Lainlain_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getList($num, $offset, $id, $with) {
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
               if ($id != "") {
            if ($with == "NoDokumen") {
                $clause = "and $with like '%$id%'";
            } else {
                $clause = "and $with = '$id'";
            }
        }
        $sql = "
			select NoDokumen,DATE_FORMAT(TglDokumen,'%d-%m-%Y') as Tanggal
			,TglDokumen,if(Tipe='K','Pengeluaran','Penerimaan') as Tipe,
			h.Keterangan
			from trans_lainlain_header h
			where FlagDelete = 'T'
			order by CAST(NoDokumen as unsigned) desc
			Limit $offset,$num
			";
        return $this->getArrayResult($sql);
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans,TglTrans as TglTrans2 from aplikasi LIMIT 0,1";
        return $this->getRow($sql);
    }

    function FindCabang($nodok) {
        $sql = "SELECT KdGudang FROM trans_lainlain_detail WHERE NoDokumen='$nodok'";
        return $this->getRow($sql);
    }

    function getReports($nodok) {

        $csv_terminated = "\n";
        $csv_separator = "#";
        $csv_enclosed = '';
        $csv_escaped = "\"";
        $schema_insert = "";
        $out = '';

        $aResults = $this->db
                ->select('*')
                ->from('trans_lainlain_header')
                ->where('NoDokumen', $nodok)
                ->get();

        $aFields = $aResults->list_fields();


        // Format the data
        foreach ($aResults->result_array() as $aResult) {
            $schema_insert = '';
            foreach ($aFields as $sField) {
                $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $aResult[$sField]) . $csv_enclosed . $csv_separator;
            }
            $out .= 'trans_lainlain_header,' . $schema_insert;
            $out .= $csv_terminated;
        }
        return $out;
    }

    function getReportsDetail($tabel, $kondisi, $nodok, $csv_terminated, $csv_separator, $csv_enclosed, $csv_escaped, $schema_insert, $out, $batasjudul, $kg) {
//        $sql = $this->db->query("select * from $tabel where $kondisi between $sDateStart and $sDateEnd") ;
        $dt = "select * FROM $tabel
               where $tabel.$kondisi ='$nodok'";
        $nil = $this->getRow($dt);
//        print_r($nil);die();

        $wh = "$kondisi ='$nodok'";
        $aResults = $this->db
                ->select('*')
                ->from($tabel)
                ->where($wh)
                ->get();

        $aFields = $aResults->list_fields();


        if (!empty($nil)) {
            // Format the data
            foreach ($aResults->result_array() as $aResult) {
                $schema_insert = '';
                $out .= $kg . $batasjudul . $tabel . $batasjudul;
                foreach ($aFields as $sField) {
                    $schema_insert .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $aResult[$sField]) . $csv_enclosed . $csv_separator;
                }
                $out .= $schema_insert;
                $out .= $csv_terminated;
            }
        } else {
            $out .= $kg . $batasjudul . $tabel . $batasjudul;
            $schema_insert = "data tidak ditemukan";
            $out .= $schema_insert;
            $out .= $csv_terminated;
        }
        return $out;
    }

    function getDetailTrx($tabelD, $tabelH, $kondisi, $nodok, $csv_terminated, $csv_separator, $csv_enclosed, $csv_escaped, $schema_insert, $out, $batasjudul, $kg) {

        $dt = "select $tabelD.* FROM $tabelD
               INNER JOIN $tabelH ON 
               $tabelH.$kondisi=$tabelD.$kondisi 
               AND $tabelH.NoDokumen='$nodok'";
        $nil = $this->getArrayResult($dt);

//        print_r($nil);
        $aList = $this->db
                ->select('*')
                ->from($tabelD)
                ->get();

        $aFields = $aList->list_fields();
        /* header  field */
        $lstD = '';
        $by = '';
//        echo count($aFields);
//        die();
        if (!empty($nil)) {
            for ($x = 0; $x < count($nil); $x++) {
                $out .= $kg . $batasjudul . $tabelD . $batasjudul;
                $by = "";
                for ($y = 0; $y < count($aFields);) {
                    $bts = $y;
                    $lstD .= $aFields[$y] . ",";
                    //                $l = $csv_enclosed . str_replace( $csv_enclosed, $csv_escaped . $csv_enclosed, stripslashes( $sField ) ) . $csv_enclosed;
                    //                $schema_insert .= $l;
                    $by .= $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $nil[$x][$aFields[$y]]) . $csv_enclosed . $csv_separator;
                    //            $by .=$nil->$aFields[$y];

                    $y++;
                }

                $out .= $by;
                $out .= $csv_terminated;
//                     $out .= $csv_terminated;
            }
        } else {
            $by = "data tidak ditemukan";
        }
//        echo $lstD; 
//        $out .= $by;
//        $out .= $csv_terminated;
//        $aFields = $aResults->list_fields();


        return $out;
    }

    function num_row($id, $with) {
        $clause = "";
        if ($id != '') {
            if ($with == "NoDokumen") {
                $clause = " and $with like '%$id%'";
            } else {
                $clause = " and $with = '$id'";
            }
        }
        $sql = "SELECT NoDokumen FROM trans_lainlain_header where FlagDelete = 'T' $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getnoorder($no) {
        $sql = "select * from req_lainlain_header where NoDokumen = '$no'";
        return $this->getArrayResult($sql);
    }

    function getdetOrder($pcode, $noorder) {
        $sql = "SELECT IdD_Req FROM req_lainlain_detail WHERE NoDokumen='$noorder' AND PCode='$pcode'";
        return $this->getArrayResult($sql);
    }

    function getGudang() {
        $sql = "SELECT KdGudang,Keterangan as Nama from gudang order by KdGudang";
        return $this->getArrayResult($sql);
    }

    function getCNDN() {
        $sql = "SELECT KdCNDN,Keterangan as Nama from cndn order by KdCNDN";
        return $this->getArrayResult($sql);
    }

    function getKontak() {
        $sql = "select KdContact,Nama from contact order by KdContact";
        return $this->getArrayResult($sql);
    }

    function cekShare($kode, $kontak) {
        $sql = "select h.KdKetentuan
				from ketentuan_simpan_detail d,ketentuan_simpan h
				where PCode='$kode' and h.KdKetentuan=d.KdKetentuan and Pilihan='K' and h.KdContact<>'$kontak'
				and SharePCode='N';";
        return $this->NumResult($sql);
    }

    function getPCodeDet($kode) {
        /*$sql = "
				select b.*,keterangan as NamaSatuan from(
				SELECT PCode,NamaLengkap as NamaInitial,KdSatuan,HargaJual
				FROM masterbarang Where PCode='$kode'
				) b
				left join
				(
				select NamaSatuan,Keterangan from satuan
				) s 
				on s.NamaSatuan=b.KdSatuan";
         
         */
        $sql = "SELECT PCode, NamaLengkap, Harga0b AS Harga FROM masterbarang WHERE PCode = '$kode'";
        return $this->getRow($sql);
    }

    function getStok($pcode, $tahun, $field, $order, $join) {
        $sql = "
		select PCode,Nama,NoPenerimaan,KonversiBesarKecil,KonversiTengahKecil,$field,AsalData from(
			select head.* from(
			select distinct PCode,KonversiBesarKecil,KonversiTengahKecil,NoPenerimaan,AddDate,AddDateAplikasi,Urutan,AsalData,$field
			from stock_simpan_detail
			where PCode='$pcode'
			and Tahun='$tahun' and (AsalData='O' or AsalData='L')
			) head
			$join
			order by $order
		) stock
		
		where $field>0
		";
        //echo "<pre>$sql</pre>";die;
        return $this->getArrayResult($sql);
    }

    function getNewNo($tgl) {
        $tahun = substr($tgl, 0, 4);
        $bulan = substr($tgl, 5, 2);
        $sql = "Update counter set NoLainLain=NoLainLain+1 where Tahun='$tahun' and Bulan='$bulan'";
        $this->db->query($sql);
        $sql1 = "SELECT NoLainLain FROM counter where Tahun='$tahun' and Bulan='$bulan'";
        //echo "SELECT NoLainLain FROM counter where Tahun='$tahun' and Bulan='$bulan'";    
        return $this->getRow($sql1);
    }

    function getdatalama($no) {
        $sql = "select PCode,QtyPcs from trans_lainlain_detail
		where NoDokumen='$no'";
        return $this->getRow($sql);
    }

    function getPastLocation($no, $pcode) {
        $sql = "select QtyPcs from trans_lainlain_detail
		where NoDokumen='$no' and PCode='$pcode'";
        return $this->getRow($sql);
    }

    function CekStock($fieldmasuk, $fieldakhir, $pcode, $tahun) {
        $sql = "select $fieldmasuk,$fieldakhir from stock 
		where Tahun='$tahun'
		and KodeBarang='$pcode'";
//                print $sql;
        return $this->getRow($sql);
    }

    function cekmutasi($tabel, $no, $pcode, $kdtransaksi) {
        $sql = "select IdMutasi,Qty from $tabel
		where NoTransaksi='$no' and PCode='$pcode' and KdTransaksi='$kdtransaksi'
		";
        return $this->getRow($sql);
    }

    function StockKeluarAwal($fieldkeluar, $fieldakhir, $pcode, $tahun) {
        $sql = "select $fieldkeluar,$fieldakhir from stock
		where Tahun='$tahun'
		and KodeBarang='$pcode'";
        //print $sql;
        return $this->getRow($sql);
    }

    function StockKeluarDetailAwal($no, $pcode, $lokasi, $tahun, $fieldkeluar, $fieldakhir) {
        $sql = "select $fieldkeluar,$fieldakhir from stock_simpan_detail
		where Tahun='$tahun' 
		and NoPenerimaan='$no'
		and PCode='$pcode'";
//                    print $sql;
        return $this->getRow($sql);
    }

    function getHeader($id) {
        $sql = "SELECT NoDokumen,DATE_FORMAT(TglDokumen,'%d-%m-%Y') as TglDokumen,h.Keterangan,h.KdGudang,
		if(Tipe='M','masuk','keluar') as Tipe,if(Tipe='M','PENERIMAAN','PENGELUARAN') as NamaKeterangan
		from trans_lainlain_header h
		Where NoDokumen='$id'
                ";
        return $this->getRow($sql);
    }

    function getDetail($id) {
        $sql = "
		select d.*,NamaInitial from(
		SELECT distinct NoDokumen,PCode,QtyPcs,Harga,Netto
		,KonversiBesarKecil,KonversiTengahKecil,KonversiJualKecil,KdSatuan,KdSatuanJual,TglStockSimpan,TglAplikasiStokSimpan from trans_lainlain_detail 
		Where NoDokumen='$id' order by PCode
		) d
		left join
		(
			select PCode,NamaLengkap as NamaInitial
			from masterbarang
		) masterb
		on masterb.PCode = d.PCode
		left join
		(
		select NamaSatuan from satuan
		) satu
		on satu.NamaSatuan = d.KdSatuanJual";
        return $this->getArrayResult($sql);
    }

    function getDetailForPrint($id) {
        $sql = "
		select d.PCode,QtyPcs,NamaInitial,d.Harga from(
		SELECT distinct PCode,QtyPcs,Harga from trans_lainlain_detail Where NoDokumen='$id' order by PCode
		) d
		left join
		(
			select PCode,NamaLengkap as NamaInitial from masterbarang
		) masterb
		on masterb.PCode = d.PCode
		";
        return $this->getArrayResult($sql);
    }

    function getCountDetail($id) {
        $sql = "SELECT NoDokumen FROM trans_lainlain_detail where NoDokumen='$id'";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getQtyCetak($id) {
        $sql = "SELECT QtyCetak from trans_simpan_cetak where nodokumen='$id' ORDER BY PCode";
        return $this->getArrayResult($sql);
    }

    function getBasedBarcode($id) {

        $sql = "select distinct PCode,h.NoDokumen,QtyPcs,QtyKonversi,KonversiBesarKecil,KonversiTengahKecil 
			from trans_lainlain_detail d,trans_lainlain_header h
			where h.NoDokumen=d.NoDokumen and h.NoDokumen='$id'";

        return $this->getArrayResult($sql);
    }

    function getPCodeName($pcode) {
        $sql = "
		select NamaLengkap,SatuanBesar,SatuanTengah,SatuanKecil from
		(
			select NamaLengkap,KdSatuanBesar,KdSatuanTengah,KdSatuanKecil from masterbarang where PCode='$pcode'
		)m
		left join
		(
		select NamaSatuan,Keterangan as SatuanBesar from satuan
		) sb
		on sb.NamaSatuan=m.KdSatuanBesar
		left join
		(
		select NamaSatuan,Keterangan as SatuanTengah from satuan
		) st
		on st.NamaSatuan=m.KdSatuanTengah
		left join
		(
		select NamaSatuan,Keterangan as SatuanKecil from satuan
		) sk
		on sk.NamaSatuan=m.KdSatuanKecil";
        return $this->getRow($sql);
    }

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
    }

    function getno_dok() {
        $sql = "SELECT NoDokumen FROM trans_lainlain_header ORDER BY NoDokumen DESC LIMIT 1";
        return $this->getRow($sql);
    }

}

?>