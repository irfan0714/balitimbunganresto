<?php
class edit_transaksi_posmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
//
    function getstruk($nostruk)
	{
		//$sql = "SELECT nostruk, tanggal, kasir, totalnilai, `status`, kdagent, statuskomisi FROM transaksi_header WHERE nostruk='$nostruk' and status='1'";
		$sql = "SELECT nostruk, tanggal, kasir, totalnilai, `status`, kdagent, statuskomisi FROM transaksi_header WHERE nostruk='$nostruk'";
		//echo $sql;
		$result = $this->getArrayResult($sql);
		return $result;
	}
	
	function getTourTravel() {
        $sql = "SELECT KdTravel,  Nama FROM tourtravel ORDER BY Nama";
        $result = $this->getArrayResult($sql);
		return $result;
    }
	
	function getregist($noregist)
	{
		$sql = "
				SELECT 
				      a.`KdRegister`,
					  a.`Tanggal`,
					  a.`KdTravel`,
					  b.`Nama` AS nama_travel,
					  c.`Nama` AS nama_tour_leader,
					  a.`statuskomisi`  
				FROM
				  `register` a 
				  INNER JOIN `tourtravel` b 
				    ON a.`KdTravel` = b.`KdTravel` 
				  INNER JOIN `tourleader` c 
				    ON A.`KdTourLeader` = C.`KdTourLeader` 
				WHERE a.`statuskomisi` = '0' 
				  AND a.`KdRegister` = '$noregist' ;
			   ";
		//echo $sql;
		$result = $this->getArrayResult($sql);
		return $result;
	}
	
	function getList($limit,$offset,$arrSearch)
	{
		
		$mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_status="";
        $where_tgl="";

        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "edit_log.NoDokumen";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			
			if($arrSearch["tgl_awal"]!="" OR $arrSearch["tgl_akhir"]!="")
			{
				$where_tgl = "AND edit_log.Created_Date BETWEEN '".$arrSearch["tgl_awal"]."' AND '".$arrSearch["tgl_akhir"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND edit_log.Status_Request = '".$arrSearch["status"]."'";	
			}
			
		} 
		
		$sql = "SELECT * FROM edit_log 
				WHERE 1
				".$where_keyword."
				".$where_status."
				".$where_tgl."
				ORDER BY id_request DESC";
		
		$result = $this->getArrayResult($sql);
		return $result;
	}
	
	function getData($id)
	{
		$sql = "SELECT * FROM edit_log WHERE id_request='$id' ORDER BY id_request DESC";
		return $this->getRow($sql);
	}
	
	function num_edit_transaksi_pos_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `edit_log` a;       
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getTelegram($user)
	{
		$sql = "SELECT a.`TelegramID` FROM user a WHERE a.`UserName`='$user';";
		return $this->getRow($sql);
	}
	
	function hapus($nostruk){
		$sql1 = "update transaksi_header set status=2 where nostruk='$nostruk'";
		$sql2 = "update transaksi_detail set status=2 where nostruk='$nostruk'";
		$sql3 = "delete from transaksi_detail_voucher where nostruk='$nostruk'";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		$this->db->query($sql3);
	}
	
	function update_stiker($nostruk, $nostiker){
		$sql1 = "update transaksi_header set kdagent='$nostiker' where nostruk='$nostruk'";
		$sql2 = "update transaksi_detail set kdagent='$nostiker' where nostruk='$nostruk'";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
	}
	
	function insert_log($data){
		$this->db->insert('edit_log', $data);	
		return $this->db->insert_id() ;
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

}
?>