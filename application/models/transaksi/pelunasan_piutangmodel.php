<?php
class pelunasan_piutangmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getPelunasanHutangList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoTransaksi";
				$arr_keyword[1] = "NoReceiveVoucher";
				$arr_keyword[2] = "Nama";
				$arr_keyword[3] = "Keterangan";
				$arr_keyword[4] = "CNNo";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "  
            SELECT h.`NoTransaksi`, h.`NoReceiveVoucher`, h.`TglDokumen`, concat(COALESCE(s.`Nama`,''),COALESCE(t.Nama,'')) as Nama, h.`Keterangan`, h.CNNo
			FROM pelunasan_piutang_header h left JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
			left join tourtravel t on h.KdCustomer=t.KdTravel
            WHERE 
            	1  
            	".$where_keyword."                                  
            ORDER BY 
              NoTransaksi DESC 
            Limit 
              $offset,$limit
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_pelunasan_piutang_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoTransaksi";
				$arr_keyword[1] = "NoReceiveVoucher";
				$arr_keyword[2] = "Nama";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
		}
		
		$sql = "  
            SELECT h.`NoTransaksi`, h.`NoReceiveVoucher`, h.`TglDokumen`, concat(COALESCE(s.`Nama`,''),COALESCE(t.Nama,'')) as Nama, h.`Keterangan`
			FROM pelunasan_piutang_header h left JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
			left join tourtravel t on h.KdCustomer=t.KdTravel
            WHERE 
            	1  
            	".$where_keyword."                                  
            ORDER BY 
              NoTransaksi DESC 
        ";
		                  
        return $this->NumResult($sql);
	}

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		return $this->getRow($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	
	function getCustomer()
	{
		$sql = "SELECT KdCustomer, Nama FROM customer order by nama";
    	return $this->getArrayResult($sql);
    }
    
    function getTravel()
	{
		$sql = "SELECT KdTravel as KdCustomer, Nama FROM tourtravel order by nama";
    	return $this->getArrayResult($sql);
	}

    function getMataUang()
	{
		$sql = "SELECT kd_uang, keterangan FROM mata_uang order by kd_uang";
    	return $this->getArrayResult($sql);
    }
    
    function getKasBank()
	{
		$sql = "SELECT KdKasBank, NamaKasBank FROM kasbank ORDER BY NamaKasBank";
    	return $this->getArrayResult($sql);
    }
    
    function getNoTransaksi($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoTransaksi FROM pelunasan_piutang_header WHERE NoTransaksi LIKE '%$blnthn' ORDER BY NoTransaksi DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$notransaksi = 'PP00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoTransaksi'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$notransaksi = 'PP' . $nourut .'-'.$blnthn;
		}
		return $notransaksi;
	}
    
    function getDetail($KdCustomer, $type)
	{
		if($type==1){
			$sql = "SELECT h.`NoFaktur`, h.`JatuhTempo` as Tanggal, h.`Sisa`  
				FROM piutang h 
				WHERE h.KdCustomer ='$KdCustomer' AND h.TipeTransaksi in('ID','DD','I') and h.sisa>0 order by h.JatuhTempo";	
		}else{
			$sql = "SELECT h.`NoFaktur`, h.`JatuhTempo` as Tanggal, h.`Sisa`  
				FROM piutang h 
				WHERE h.KdCustomer ='$KdCustomer' AND h.TipeTransaksi in('IR','DR') and h.sisa>0 order by h.JatuhTempo";	
		}
		//print_r($sql);
    	return $this->getArrayResult($sql);
    	
    }
    
    function insertHeader($notransaksi, $tgl, $kdkasbank, $nobukti, $KdCustomer, $keterangan,$user, $norv, $jenis, $cnno, $vbiayaadmin, $vpembulatan, $vpph, $kdrekeningpph)
    {
		$mylib = new globallib();
        $this->locktables('pelunasan_piutang_header');

        $data = array(
            'NoTransaksi'	=> $notransaksi,
            'NoReceiveVoucher' => $norv,
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'KdCustomer' => $KdCustomer,
            'MataUang' => 'IDR',
            'Kurs' => 1,
            'NoBukti' => $nobukti,
            'Keterangan' => $keterangan,
            'BiayaAdmin' => $vbiayaadmin,
            'Pembulatan' => $vpembulatan,
            'NilaiPPH' => $vpph,
            'Jenis' => $jenis,
			'CNNO' => $cnno,
			'KdRekeningPPH'=>$kdrekeningpph,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('pelunasan_piutang_header', $data);

        $this->unlocktables();
    }
    
    function insertDetail($notransaksi,$nofaktur,$sisa,$bayar){
    	$mylib = new globallib();
		$this->locktables('pelunasan_piutang_detail');
		$total = 0;
		for($i=0;$i<count($nofaktur); $i++){
			$data = array(
				'NoTransaksi' => $notransaksi,
				'NoFaktur' => $nofaktur[$i],
				'NilaiFaktur' => $mylib->save_int($sisa[$i]),
				'NilaiBayar' => $mylib->save_int($bayar[$i])
			);
			$total += $mylib->save_int($bayar[$i]);
			$this->db->insert('pelunasan_piutang_detail', $data);
		}
		$this->unlocktables();
		return $total;
	}
	
	function insertPiutang($tgl,$notransaksi, $nofaktur, $KdCustomer,$bayar,$jenis){
		$mylib = new globallib();
		if($jenis=='B')
			$tipetrans='P';
		else
			$tipetrans='CA';
			
		$this->locktables('piutang');
		for($i=0;$i<count($nofaktur); $i++){
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				$data = array(
				'NoDokumen' => $notransaksi,
				'NoFaktur' => $nofaktur[$i], 
				'KdCustomer' => $KdCustomer,
				'TipeTransaksi' => $tipetrans,
				'TglTransaksi' => $tgl,
				'JatuhTempo' => $tgl,
				'NilaiTransaksi' => $vbayar,
				'Sisa' => 0
				);
				$this->db->insert('piutang', $data);
			}
		}
		$this->unlocktables();
	}
	
	function UpdatePiutang($nofaktur,$bayar){
		$mylib = new globallib();
		$this->locktables('hutang');
		for($i=0;$i<count($nofaktur); $i++){
			$nodokumen = $nofaktur[$i];
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				
				$sql = "Update piutang set sisa = sisa-$vbayar where NoDokumen='$nodokumen'";
				$qry = $this->db->query($sql);
				
			}
		}
		$this->unlocktables();
	}
	
	function UpdatePiutangCN($nofaktur,$bayar){
		$this->locktables('hutang');
		
		$sql = "Update piutang set sisa = sisa-$bayar where NoDokumen='$nofaktur'";
		$qry = $this->db->query($sql);
		
		$this->unlocktables();
	}
	
	function insertMutasiPiutang($tgl,$nofaktur, $KdCustomer,$gtotal, $jenis){
		$mylib = new globallib();
		if($jenis=='B')
			$tipetrans='P';
		else
			$tipetrans='CA';
			
		$this->locktables('mutasi_hutang');
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$data = array(
			'Tahun' => $tahun,
			'Bulan' => $bulan, 
			'NoDokumen' => $nofaktur, 
			'KdCustomer' => $KdCustomer,
			'TipeTransaksi' => $tipetrans,
			'Jumlah' => $gtotal
		);
		$this->db->insert('mutasi_piutang', $data);
		$this->unlocktables();
	}
			
	function getDataHeader($notransaksi){
    	$sql = "SELECT h.`NoTransaksi`, h.`NoReceiveVoucher`, h.`TglDokumen`, k.`NamaKasBank`, concat(COALESCE(s.`Nama`,''),COALESCE(t.Nama,'')) AS NamaCustomer,CNNo, h.KdRekeningPPH, 
				h.`MataUang`, h.`Kurs`, h.`NoBukti`, h.`Keterangan`, h.`AddDate`, h.`AddUser`, h.`EditDate`, h.`EditUser`,  h.BiayaAdmin, h.Pembulatan, NilaiPPH, h.NoRekeningSupplier 
				 FROM pelunasan_piutang_header h 
				left JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
				left join tourtravel t on h.KdCustomer=t.KdTravel	
				left JOIN kasbank k ON h.`KdKasBank`=k.`KdKasBank`
				WHERE h.`NoTransaksi`='$notransaksi'";
		//echo $sql;
        $row = $this->getArrayResult($sql);
        return $row[0];
	}
	
	function getDataDetail($notransaksi){
    	$sql = "SELECT p.`NoFaktur`, i.`NoFakturSupplier`, i.`NoPO`, i.`Tanggal`, p.`NilaiFaktur`, p.`NilaiBayar` FROM pelunasan_piutang_detail p
				LEFT JOIN invoice_pembelian_header i ON p.`NoFaktur`=i.`NoFaktur`
				WHERE p.`NoTransaksi`='$notransaksi'";
        return $this->getArrayResult($sql);
	}

	function getNoRV($kdkasbank,$tgl){
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
		$kd_no = $this->getKodeBank($kdkasbank);
		$notransaksi = $this->get_no_counter( $kd_no,'trans_receipt_header', 'NoDokumen', $tahun,$bulan);
		
		return $notransaksi;
	}
	
	function updatecn($cnno,$nilai){
		$sql = "UPDATE creditnote SET cnamountremain=cnamountremain-$nilai WHERE cnno='$cnno'";
		$this->db->query($sql);
	}
	
	function insertRVHeader($nopv,$tgl,$kdkasbank,$KdCustomer,$keterangan,$total,$nobukti,$user, $type,$biayaadmin, $pembulatan,$pph){
		$mylib = new globallib();
        $this->locktables('trans_receipt_header');
		$vbiayaadmin = $mylib->save_int($biayaadmin);
		$vpembulatan = $mylib->save_int($pembulatan);

        $data = array(
            'NoDokumen'	=> $nopv,
            'TglDokumen'	=> $tgl,
            'KdKasBank' => $kdkasbank,
            'TerimaDari' => $this->getNamaCustomer($KdCustomer, $type),
            'Keterangan' => $keterangan,
            'JumlahReceipt' => $total+$vbiayaadmin+$vpembulatan+$pph,
            'Jenis' => 1,
            'Status' => 1,
            'NoBukti' => $nobukti,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('trans_receipt_header', $data);

        $this->unlocktables();
    }
    
	function insertRVDetail($nopv,$tgl,$KdCustomer,$bayar,$nobukti,$nofaktur, $user,$type ,$vbiayaadmin, $vpembulatan, $vpph,$kdrekeningpph){
		$mylib = new globallib();
		$this->locktables('trans_receipt_detail');
		$urut = 1;
		$rekening = $this->getInterface();
		for($i=0;$i<count($nofaktur); $i++){
			$vbayar = $mylib->save_int($bayar[$i]);
			if($vbayar*1>0){
				$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $rekening['PiutangDagang'],
				'Jumlah' => $vbayar,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Pelunasan Piutang ' . $this->getNamaCustomer($KdCustomer,$type) . '-' . $nofaktur[$i],
				'Urutan' => $i+1,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_receipt_detail', $data);
			}
		}
		if($vbiayaadmin>0){
			$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $rekening['AdminBank'],
				'Jumlah' => $vbiayaadmin,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Biaya Transfer ' . $this->getNamaCustomer($KdCustomer,$type) . '-' . $nobukti,
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_receipt_detail', $data);
				$urut++;
		}
		if($vpph>0){
			$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekeningpph,
				'Jumlah' => $vpph*-1,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'PPH 23 ' . $this->getNamaCustomer($KdCustomer,$type) . '-' . $nobukti,
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_receipt_detail', $data);
				$urut++;
		}
		if($vpembulatan<>0){
			if($vpembulatan>0){
				$rekselisih = $rekening['RugiSelisihBayar'];
			}else{
				$rekselisih = $rekening['LabaSelisihBayar'];
			}
			
			$data = array(
				'NoDokumen' => $nopv,
				'TglDokumen' => $tgl,
				'KdRekening' => $rekselisih,
				'Jumlah' => $vpembulatan,
				'KdDepartemen' => '00',
				'KdSubDivisi' => 25, // head office
				'Keterangan' => 'Selisih Pembayaran ' . $this->getNamaCustomer($KdCustomer,$type) . '-' . $nobukti,
				'Urutan' => $urut,
				'Jenis' => 1,
				'NoBukti' => $nobukti,
				'Status' => 1,
				'AddDate' => date('Y-m-d'),
            	'AddUser' => $user,
            	'EditDate' => date('Y-m-d'),
            	'EditUser' => $user
				);
				$this->db->insert('trans_receipt_detail', $data);
		}
		$this->unlocktables();
	}
	
	function getKodeBank($kdkasbank){
		$sql = "SELECT KdPembayaran FROM kasbank where KdKasBank='$kdkasbank'";
    	$result = $this->getArrayResult($sql);
    	return $result[0]['KdPembayaran'];
	}
	
	function getCN($kdcustomer){
		$sql = "select CNNo, CNAmountRemain as Nilai from creditnote where KdCustomer='$kdcustomer' and cntype in('CS','CR') and CNAmountRemain>0";
		return $this->getArrayResult($sql);
	}
	
	function getNilaiCN($cnno){
		$sql = "select CNNo, CNAmountRemain as Nilai from creditnote where cnno='$cnno' ";
		//echo $sql;
		return $this->getArrayResult($sql);
	}
	
	function getNamaCustomer($KdCustomer, $type){
		if($type==1){
			$sql = "SELECT Nama FROM customer where KdCustomer='$KdCustomer'";	
		}else{
			$sql = "SELECT Nama FROM tourtravel where KdTravel='$KdCustomer'";	
		}
		
    	$result = $this->getArrayResult($sql);
    	return $result[0]['Nama'];
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
    function getInterface(){
		$sql = "select * from interface";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0];
	}

	function getRekeningPPH(){
		$sql = "Select KdRekening, NamaRekening from rekening where KdRekening like '%2106%' AND tingkat=3";
		return $this->getArrayResult($sql);
	}
}
?>