<?php
class Rg_marketing_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}

	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }
    
    function getWarehouse()
	{
    	$sql = "SELECT a.warehousecode,a.warehousename FROM warehouse a ORDER BY a.warehousecode ASC";
		return $this->getArrayResult($sql);
    }
	
    function num_rg_marketing_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `rg_marketing` a;       
		";
		                  
        return $this->NumResult($sql);
	}
		
	function getRgMarketingList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_supplier = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.NoDokumen";
				$arr_keyword[1] = "c.Nama";
				$arr_keyword[2] = "a.Status";
				$arr_keyword[3] = "a.Keterangan";
		        $arr_keyword[4] = "a.PONo";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["supplier"]!="")
			{
				$where_supplier = "AND a.KdSupplier = '".$arrSearch["supplier"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT 
			  a.*,
			  c.*,
			  DATE_FORMAT(a.TglDokumen, '%d-%m-%Y') AS TglDokumen_
			FROM
			  `rg_marketing` a  
			  INNER JOIN supplier c 
			    ON a.`KdSupplier` = c.`KdSupplier` 
			WHERE 1
			  
            	".$where_keyword."
            	".$where_supplier."    
            	".$where_status."                                   
            ORDER BY 
              a.NoDokumen DESC 
            Limit 
              $offset,$limit
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		/*$sql = "
			SELECT 
			  a.*,
			  b.*,
			  c.*,
			  d.`cnno`,
			  d.`returnno`,
			  e.`cndid`,
			  e.`KdSubdivisi`,
			  DATE_FORMAT(a.`returndate`, '%d-%m-%Y') AS returndate_indo
			FROM
			  `salesreturn` a 
			  INNER JOIN gudang b 
			    ON a.`warehousecode` = b.`KdGudang` 
			  INNER JOIN customer c 
			    ON a.`customerid` = c.`KdCustomer`
			  INNER JOIN `creditnote` d 
			    ON a.`returnno` = d.`returnno` 
			  INNER JOIN `creditnotedetail` e 
			    ON d.`cnno` = e.`cnno` 
			WHERE a.`returnno` = '".$id."';
        ";*/
        $sql="
        	SELECT 
			`rg_marketing`.*,
			supplier.*,
			DATE_FORMAT(`rg_marketing`.TglDokumen, '%d-%m-%Y') AS TglDokumen
			FROM `rg_marketing`INNER JOIN supplier ON rg_marketing.KdSupplier = supplier.KdSupplier WHERE `rg_marketing`.`NoDokumen`= '".$id."';
        	";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.* 
			FROM
			  deliveryorderdetail
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '".$pcode."' 
			  AND deliveryorderdetail.dono = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  salesreturndetail INNER JOIN
			  salesreturn ON salesreturn.`returnno` = salesreturndetail.`returnno`
			WHERE 1 
			  AND salesreturndetail.inventorycode = '".$pcode."' 
			  AND salesreturndetail.returnno = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  salesreturndetail INNER JOIN
			  salesreturn ON salesreturn.`returnno` = salesreturndetail.`returnno`
			WHERE 1  
			  AND salesreturndetail.returnno = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetCnnoDetail($returnno,$pcode)
	{
		$sql = "
		SELECT * FROM `creditnotedetail` a WHERE a.`cnno`='".$returnno."' AND a.`description` LIKE '%".$pcode."%';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetCnno($returnno)
	{
	
		$sql = "
		SELECT * FROM `creditnote` a WHERE a.`returnno`='".$returnno."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  rg_marketing_detail 
			  WHERE rg_marketing_detail.NoDokumen= '".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function getDetailListTemp($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  rg_marketing_detail_temp 
			  WHERE rg_marketing_detail_temp.NoDokumen= '".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	 function cekDataBarang($pcode)
	{
		$sql = "
			SELECT 
			  a.`Harga1c`,
  			  b.`KdRekeningRetur` 
			FROM
			  `masterbarang` a 
			  INNER JOIN `divisi` b 
			    ON a.`KdDivisi` = b.`KdDivisi` 
			WHERE a.`PCode` = '".$pcode."' ;
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekDataCreditNoteDetail($v_cnno)
	{
		$sql = "
			SELECT 
			  ROUND(SUM(a.value),0) AS amount,
			  ROUND((10/100)*SUM(a.value),0) AS potongan_ppn,
			  ROUND(SUM(a.value) + (10/100)*SUM(a.value),0) AS grandtotal   
			FROM
			  `creditnotedetail` a 
			WHERE a.`cnno` = '".$v_cnno."' ;
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  `rg_marketing_detail` a 
			  
			WHERE
			   a.NoDOKUMEN = '".$id."' 
			ORDER BY a.NoUrut ASC ;
		";
        return $this->getArrayResult($sql);
	}
    
    
    function getSupplier()
	{
    	$sql = "SELECT a.KdSupplier,a.Nama FROM supplier a ORDER BY a.Nama ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getPoMarketingDetail($pono)
	{
		$sql = "
			SELECT * FROM po_marketing_detail WHERE po_marketing_detail.NoDokumen='".$pono."' ORDER BY NoUrut ASC;
		";
		
        return $this->getArrayResult($sql);
	}
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>