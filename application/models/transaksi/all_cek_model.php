<?php
class All_cek_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getMPCode()
	{
    	$sql = "SELECT * FROM barang_paket where 1 GROUP BY MPCode";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getTransOrder($tgl1, $tgl2)
	{
    	$sql = "
    			SELECT 
				  a.`NoPr`,
				  b.`PCode`,
				  b.`QtyPcs`,
				  b.`Satuan` 
				FROM
				  trans_order_barang_header a 
				  INNER JOIN `trans_order_barang_detail` b 
				    ON a.`NoDokumen` = b.`NoDokumen` 
				WHERE a.`TglDokumen` BETWEEN '$tgl1' 
				  AND '$tgl2' ;
    			";
		return $this->getArrayResult($sql);
		//return $this->db->query($sql);
    }
    
    function getPCodeTransaksi()
	{
    	$sql = "SELECT * FROM transaksi_detail where 1";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
	
	
	function getHargaBeli($Pcode)
	{
    	$sql = "SELECT a.HargaBeli FROM masterbarang a WHERE a.Pcode='$Pcode'";
		 return $this->getRow($sql);
    }
    
    function getCek($gudang,$tgl){
		$sql = "SELECT MAX(DATE(a.`TglDokumen`)) AS tglapprove FROM `opname_header` a WHERE a.`KdGudang`='$gudang' and a.approval_status=1;";
		
		return $this->getRow($sql);
	}
	
	function getCekType2($gudang_from,$gudang_to,$tgl){
		$sql = "SELECT MAX(DATE(a.`TglDokumen`)) AS tglapprove FROM `opname_header` a WHERE (a.`KdGudang`='$gudang_to') and a.approval_status=1;";
		
		return $this->getRow($sql);
	}
 

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>