<?php
class Terima_barangmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getTerimaBarangList($num,$offset,$id,$with,$thnbln)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="where h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%' and h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
			}
			else
			{
				$clause = " where $with = '$id' and h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
			}
		}
    	$sql = "
			select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,date_format(TglJto,'%d-%m-%Y') as TglJto,NoOrder,h.Keterangan,Nama,gudang.Keterangan as NamaGudang,
			h.Jumlah,h.PPn,h.Total,h.KdGudang,h.KdSupplier,FlagKonfirmasi from trans_terima_header h 
			left join supplier on supplier.KdSupplier = h.KdSupplier
			left join gudang on gudang.KdGudang = h.KdGudang
			$clause 
			order by cast(NoDokumen as unsigned) desc Limit $offset,$num";
		return $this->getArrayResult($sql);
    }
    
    function num_terima_barang_row($id,$with,$thnbln){
     	$clause="where h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause = " where $with like '%$id%' and h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
			}
			else
			{
				$clause = " where $with = '$id' and h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
			}
		}
		$sql = "SELECT NoDokumen FROM trans_terima_header h $clause";
        return $this->NumResult($sql);
	}

	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	function getSupplier($kode)
	{
		$sql = "SELECT KdSupplier,Nama,Alamat,Kota,Payment,TOP,LimitKredit,LimitFaktur,PPn,KdGroupext FROM supplier WHERE KdSupplier='$kode'";
        return $this->getRow($sql);
	}
	
	function getPCodeDet($kode,$kodeext)
	{
		$sql = "SELECT b.*,NamaBrand,NamaKategori,PCodeExt FROM(
				SELECT PCode,NamaLengkap,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand,PersenPajak as PPnB,
				SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,  
				(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0
				FROM masterbarang WHERE pcode='$kode' and status='A'
				) b
				LEFT JOIN
				(
				SELECT KdBrand,NamaBrand FROM brand
				) br
				ON br.KdBrand=b.KdBrand
				LEFT JOIN
				(
				SELECT KdKategori,NamaKategori FROM kategori
				) kt
				ON kt.KdKategori=b.KdKategori
				LEFT JOIN
				(
				SELECT kodegrp,PCode,PCodeExt,NamaExt FROM kodeextdetail where kodegrp='$kodeext' and PCode='$kode'
				) ext				
				ON ext.PCode=b.PCode";
        return $this->getRow($sql);
	}
	function getGudang(){
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
		return $this->getArrayResult($sql);
    }
	function getSumber($no)
	{
		$sql = "SELECT SumberOrder,NoOrder,NoPengiriman,TglDokumen from trans_terima_header where NoDokumen='$no'";
		return $this->getRow($sql);
	}
	function getStatusOrder($id)
	{
		$sql = "SELECT COUNT(NoDokumen) AS Item,SUM(QtyPcsKirim>=QtyPcs) AS Status,
                SUM(QtyPcsKirim) AS Kirim FROM trans_order_barang_detail WHERE nodokumen='$id'";
		return $this->getRow($sql);
	}
	function getUpdateQtyKirim($id,$pcode,$qtypcs)
	{
		$sql = "UPDATE trans_order_barang_detail SET QtyPcsKirim=QtyPcsKirim+'$qtypcs' WHERE nodokumen='$id' AND pcode='$pcode'";
		$qry = $this->db->query($sql);
	}
	function getSatuan($pcode)
	{
		$sql = "SELECT SatuanBl as Satuan0,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0,
		        Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where PCode='$pcode'";
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
    }
	function getOrderHeader($id)
	{
		$sql = "SELECT h.*,c.Nama
		from trans_order_barang_header h, supplier c
		where h.KdSupplier=c.KdSupplier
		and h.FlagTutup='T' and h.NoDokumen='$id'";
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}
	function getOrderDetail($id)
	{
		$sql = "
		select brg.*,NamaBrand,NamaKategori from(
			select d.*,if(d.PCode=b.PCode,'pcode','bar') as jenis,NamaStruk as NamaLengkap,b.PCode as PCodeBarang
			from trans_order_barang_detail d,trans_order_barang_header h,masterbarang b
			where h.NoDokumen='$id' and h.NoDokumen=d.NoDokumen
			and h.FlagTutup='T' and (b.PCode = d.PCode)
			order by Counter desc
		)brg
		LEFT JOIN
		(
		SELECT KdBrand,NamaBrand FROM brand
		)s
		ON s.KdBrand=brg.KdBrand
		LEFT JOIN
		(
		SELECT KdKategori,NamaKategori FROM kategori
		)k
		ON k.KdKategori=brg.KdKategori";
		return $this->getArrayResult($sql);
	}
	function getNewNo($tgl)
	{
		$tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoTerimaBarang=NoTerimaBarang+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoTerimaBarang FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	function getCounter($id)
	{
		$sql = "SELECT Counter FROM trans_terima_detail Where NoDokumen='$id' order by Counter desc limit 0,1";
		return $this->getRow($sql);
	}
	function getDetail($id)
	{
		$sql = "
		select d.*,masterb.*,s.NamaBrand,k.NamaKategori from(
		SELECT * from trans_terima_detail Where NoDokumen='$id' order by Counter
		) d
		left join
		(
			select PCode,NamaLengkap,harga0b,harga1b,harga2b,harga3b,KonvBlSt as konv0st,konv1st,konv2st,konv3st,
				Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
                SatuanBl as Satuan0,(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0 from masterbarang
		) masterb
		on masterb.PCode = d.PCode
		LEFT JOIN
		(
		SELECT KdBrand,NamaBrand FROM brand
		)s
		ON s.KdBrand=d.KdBrand
		LEFT JOIN
		(
		SELECT KdKategori,NamaKategori FROM kategori
		)k
		ON k.KdKategori=d.KdKategori";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "
		select d.PCode,QtyTerima,QtyPcsTerima,Harga,Satuan,NamaInitial,NamaSatuanJual
		,NamaSatuanKecil,NamaSatuanTengah,NamaSatuanBesar,KonversiBesarKecil,KonversiTengahKecil from(
		SELECT * from trans_terima_detail Where NoDokumen='$id' order by Counter
		) d
		left join
		(
			select PCode,NamaLengkap as NamaInitial,BarcodeSatuanBesar,BarcodeSatuanTengah,BarcodeSatuanKecil
			,KdSatuanBesar,KdSatuanTengah,KdSatuanKecil from masterbarang
		) masterb
		on masterb.PCode = d.PCode or masterb.BarcodeSatuanBesar = substr(d.PCode,1,10) or masterb.BarcodeSatuanTengah = substr(d.PCode,1,10) or masterb.BarcodeSatuanKecil= substr(d.PCode,1,10)
		left join
		(
		select NamaSatuan,Keterangan as NamaSatuanJual from satuan
		) satu
		on satu.NamaSatuan = d.Satuan
		left join 
		(
			select NamaSatuan,Keterangan as NamaSatuanKecil from satuan
		)sak
		on sak.NamaSatuan = KdSatuanKecil
		left join 
		(
			select NamaSatuan,Keterangan as NamaSatuanTengah from satuan
		)sat
		on sat.NamaSatuan = KdSatuanTengah
		left join 
		(
			select NamaSatuan,Keterangan as NamaSatuanBesar from satuan
		)sab
		on sab.NamaSatuan = KdSatuanBesar
		";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
		$sql = "SELECT h.*,date_format(h.TglDokumen,'%d-%m-%Y') as Tgl,date_format(h.TglJto,'%d-%m-%Y') as TgJto,l.Keterangan as NamaLokasi,Nama
		from trans_terima_header h,gudang l,supplier c
		Where NoDokumen='$id' and c.KdSupplier = h.KdSupplier
		and l.KdGudang = h.KdGudang";
        return $this->getRow($sql);
	}
	function getQtyBefore($id,$pcode)
	{
		$sql = "SELECT QtyPcs as QtyPcs, QtyInput as Qty, Hpp FROM trans_terima_detail Where NoDokumen='$id' and PCode='$pcode'";
		$query = $this->db->query($sql);
		$row = $query->row();
		$query->free_result();
		return $row;
	}
	function getStok($field1,$field2,$field3,$field4,$tahun,$gudang,$pcode,$status)
	{
		$sql = "SELECT $field1,$field2,$field3,$field4 from stock_simpan where Tahun='$tahun'
		and KdGudang='$gudang' and PCode='$pcode' and Status='$status'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>