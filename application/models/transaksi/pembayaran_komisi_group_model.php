<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pembayaran_komisi_group_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		
	}

	public function getgroupcombo()
	{
		$qry = "select KdTravel,Keterangan from komisi_header order by KdTravel";
		$query1 = $this->db->query($qry);
        return $query1;
	}

	public function getkas()
	{
		$qry = "SELECT * FROM `kasbank`";
		$query1 = $this->db->query($qry);
		return $query1;
	}

	public function getdata($tgl, $group)
	{
		$qry = "	
				SELECT 
					det.NoStruk, det.PCode, det.KdAgent, kom.KdTravel, det.NamaLengkap, SUM(det.Qty) AS Qty,SUM(det.ttlnetto) AS Netto, kom.total, kom.komisi1, 
					kom.komisi2,kom.komisi3,kom.komisi4 
				FROM (
						SELECT
						   a.NoStruk,
						   a.Tanggal AS TglJual,
						   a.Waktu,
						   a.KdAgent,
						   b.PCode,
						   c.NamaLengkap,
						   b.Qty,
						   ROUND(IF(b.Service_charge=5,b.Harga,b.Harga/1.1)) AS Harga,
						   (
						     ROUND(b.Qty*IF(b.Service_charge=5,b.Harga,b.Harga/1.1)-IF(b.Service_charge=5,b.Netto,b.Netto/1.1))
						   ) AS Disc,
						   (
						     ROUND(IF(b.Service_charge=5,b.Netto,b.Netto/1.1),0)
						   ) AS ttlnetto,
						   IFNULL(b.Komisi,0) AS Komisi
						FROM
						   transaksi_header a, transaksi_detail b, masterbarang c 
						WHERE 
						 		(( a.Tanggal='$tgl' AND a.KdAgent=''))
						   AND 
						   		a.nostruk=b.nostruk AND b.pcode=c.pcode
					)det
				LEFT JOIN  (
							SELECT kdtravel,pcode AS dpcode,total, komisi1, komisi2, komisi3, komisi4, komisi4bag FROM komisi_detail
						)kom
				ON kom.kdtravel='$group' AND det.pcode=kom.dpcode
				GROUP BY PCode";

	
	

		$query = $this->db->query($qry);
        $result = $query->result_array();
		return $result; 

		//return $this->getArrayResult($qry);
	}

	function getRow($qry)
	{
		$qry = $this->db->query($qry);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}


	//front

	function getDetailForPrint($id)
	{
		$sql = "
		SELECT d.*,NamaLengkap FROM(
		SELECT NoTransaksi,PCode, komisi1, komisi2, komisi3, komisi4, (Komisi1+Komisi2+Komisi3+Komisi4)AS Total
		FROM finance_komisi_group_detail WHERE NoTransaksi='$id' ORDER BY PCode
		) d
		INNER JOIN masterbarang
		ON masterbarang.`PCode`=d.PCode
		";
		//echo $sql;
		return $this->getArrayResult($sql);
	}

	function getHeaderForPrint($id)
	{
		
		$sql = "
				SELECT
				        a.NoTransaksi, DATE_FORMAT(a.TanggalTransaksi, '%d-%m-%Y') AS TglTransaksi, a.KdTravel
				        FROM
				        finance_komisi_group_header a
				        WHERE NoTransaksi = '$id'
				        GROUP BY KdTravel";
		
		
		return $this->getRow($sql);
	}
	function getHeaderForPrint2($id)
	{		
		$sql = "SELECT 
		  -- SUM(a.qty * a.harga -a.Potongan ) AS total,
		  SUM(a.hargaPPN) AS total,
		  SUM(((a.qty * a.harga) - a.Potongan) * a.persentase / 100) AS komisi,c.NoKassa,k.SubDivisi,k.KdDivisi,d.NamaDivisi,s.NamaSubDivisi
		FROM
		  (SELECT * FROM finance_komisi_detail WHERE NoTransaksi = '$id') a
		INNER JOIN transaksi_header c ON c.NoStruk=a.NoStruk  
		INNER JOIN kassa k ON k.id_kassa=c.NoKassa
		INNER JOIN divisi d ON d.KdDivisi=k.KdDivisi  
		INNER JOIN subdivisi s ON s.KdSubDivisi=k.SubDivisi
		GROUP BY k.KdDivisi,
		  k.SubDivisi ;";		
		return $this->getArrayResult($sql);
	}

	function getCountDetail($id)
	{
		$sql = "SELECT * FROM finance_komisi_group_detail where NoTransaksi='$id'";
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function getKomisiList($num, $offset, $id, $with)
	{
		$month_cek = $this->aplikasi();
		
		$bln1 = $month_cek[0]['TglTrans'];
		//$bln = substr($bln1,5,2);
		$bln = '09';

	
		if($offset != ''){
			$offset = $offset;
		}
		else
		{
			$offset = 0;
		}

		$clause = "";

		if($id != ""){
			
			if($with == "NoTransaksi"){
				$clause = "AND $with like '%$id%'";
			}
			else
			{
				$clause = "AND $with = '$id'";
			}
		}

		
		$sql = "SELECT NoTransaksi, date_format(TanggalTransaksi,'%d-%m-%Y') AS TglTransaksi, KdTravel, Keterangan, KdKasBank, Penerima, NoBukti, NoDokumen, Komisi1, Komisi2, Komisi3, Komisi4
		FROM finance_komisi_group_header WHERE MONTH(TanggalTransaksi) = '$bln' $clause
		ORDER BY NoTransaksi";
		//WHERE $clause
		//echo $sql;
		return $this->getArrayResult($sql);

	}

	function getHeader($id)
	{
		$qry = "SELECT NoTransaksi, DATE_FORMAT(TanggalTransaksi,'%d-%m-%Y') AS TglTransaksi, KdTravel, Penerima, Keterangan, NoDokumen
				FROM finance_komisi_group_header
				WHERE NoTransaksi='$id'
				ORDER BY NoTransaksi DESC";

		$query = $this->db->query($qry);
        $result = $query->result_array();
		return $result;
	}

	function getDetail($id)
	{
		$sql = "
				SELECT a.NoTransaksi, b.PCode, c.NamaLengkap, b.komisi1, b.komisi2, b.komisi3, b.komisi4
				FROM finance_komisi_group_header a, finance_komisi_group_detail b, masterbarang c
				WHERE a.NoTransaksi = b.NoTransaksi AND b.PCode = c.PCode
				AND a.NoTransaksi='$id' ORDER BY NoTransaksi DESC, a.AddDate DESC";

		return $this->getArrayResult($sql);
	}

	function num_komisi_row($id, $with)
	{
		$clause = "";
		if($id != ''){
			if($with == "NoTransaksi"){
				$clause = "WHERE $with like '%$id%'";
			}
			else
			{
				$clause = "WHERE $with = '$id'";
			}
		}
		$sql = "SELECT NoTransaksi FROM finance_komisi_header $clause";
		return $this->NumResult($sql);
	}

	function getKomisi($field,$tglagen)
	{
	$sql="	
		SELECT 
			  a.NoStruk,
			  a.Tanggal AS TglJual,
			  a.Waktu,
			  a.KdAgent,
			  b.PCode,
			  c.NamaLengkap,
			  b.Qty,
			  b.`Harga` * Qty AS hrgPPN,
			  '0' as komisi1,
			  			  '0' as komisi2,
						  			  '0' as komisi3,
									  			  '0' as komisi4,
			  ROUND(
				IF(
				  b.Service_charge = 5,
				  b.Harga,
				  b.Harga / 1.1
				)
			  ) AS Harga,
			  (
				ROUND(
				  b.Qty * IF(
					b.Service_charge = 5,
					b.Harga,
					b.Harga / 1.1
				  ) - IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				)
			  ) AS Disc,
			  (
				ROUND(
				  IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  ),
				  0
				)
			  ) AS ttlnetto,
			  IFNULL(b.Komisi, 0) AS Komisi,
			  IFNULL(
				ROUND(
				  b.Komisi / 100 * IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				),
				0
			  ) AS Nilai 
			FROM
			  transaksi_header a,
			  transaksi_detail b,
			  masterbarang c 
			WHERE (
				(
				 $tglagen
				)
			  ) 
			  AND a.nostruk = b.nostruk 
			  AND b.pcode = c.pcode 
			  AND a.statuskomisi = '0'
			  ";



//        echo $sql;
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	function getKomisiTravel($field,$tglagen)
	{
	$sql="	
		
		SELECT 
		  det.NoStruk,
		  det.TglJual,
		  det.Waktu,
		  det.KdAgent,
		  det.PCode,
		  det.NamaLengkap,
		  det.Harga,
		  det.Nilai,
  		  det.Disc,
  	      det.ttlnetto,
			det.hrgPPN,
		  SUM(det.Qty) AS Qty,
		  SUM(det.ttlnetto) AS Netto,
		  kom.total AS Komisi,
		  kom.komisi1,
		  kom.komisi2,
		  kom.komisi3,
		  kom.komisi4 
		FROM
		  (SELECT 
		    a.NoStruk,
		    a.Tanggal AS TglJual,
		    a.Waktu,
		    a.KdAgent,
		    b.PCode,
		    c.NamaLengkap,
			b.Harga* Qty AS hrgPPN,
		    b.Qty,
		    ROUND(
		      IF(
		        b.Service_charge = 5,
		        b.Harga,
		        b.Harga / 1.1
		      )
		    ) AS Harga,
		    (
		      ROUND(
		        b.Qty * IF(
		          b.Service_charge = 5,
		          b.Harga,
		          b.Harga / 1.1
		        ) - IF(
		          b.Service_charge = 5,
		          b.Netto,
		          b.Netto / 1.1
		        )
		      )
		    ) AS Disc,
		    (
		      ROUND(
		        IF(
		          b.Service_charge = 5,
		          b.Netto,
		          b.Netto / 1.1
		        ),
		        0
		      )
		    ) AS ttlnetto,
		    IFNULL(b.Komisi, 0) AS Komisi ,
		    IFNULL(
		    ROUND(
		      b.Komisi / 100 * IF(
		        b.Service_charge = 5,
		        b.Netto,
		        b.Netto / 1.1
		      )
		    ),
		    0
		  ) AS Nilai 
		  FROM
		    transaksi_header a,
		    transaksi_detail b,
		    masterbarang c 
		  WHERE (
		      (
		     	$tglagen
		      )
		    ) 
		    AND a.nostruk = b.nostruk 
		    AND b.pcode = c.pcode 
		    AND a.statuskomisi = '0') det 
		  LEFT JOIN 
		    (SELECT 
		      kdtravel,
		      pcode AS dpcode,
		      total,
		      komisi1,
		      komisi2,
		      komisi3,
		      komisi4,
		      komisi4bag 
		    FROM
		      komisi_detail) kom 
		    ON kom.kdtravel = 'JE00067' 
		    AND det.pcode = kom.dpcode 
		GROUP BY PCode ";
//        echo $sql;
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	//melakukan perubahan pada 22/11/2016 dengan mengubah `KdTourLeader`='$field'
	function ambilReq($field,$tgl2)
	{	$sql = "
			SELECT 
			  NoStiker,
			  Tanggal 
			FROM
			  register 
			WHERE statuskomisi = '0' and KdTourLeader='$field'
			  AND Tanggal BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();
		return $row;
	}
	
	function CekAgent($field)
	{	$sql = "
			SELECT DISTINCT k.* FROM register 
			INNER JOIN tourleader ON register.`KdTourLeader`=tourleader.`KdTourLeader`
			INNER JOIN komisi_header k ON k.`KdTravel`=tourleader.`KdTravel`
			WHERE register.`KdTourLeader`='$field'
			  ";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();
		return $row;
	}
	
	
	function Querydata($sql){
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
	}
	
		
	function cekKomisi($stiker,$tgl)
	{
		$sql = "SELECT DISTINCT(NoStruk) FROM `finance_komisi_detail` a
				INNER JOIN `finance_komisi_header` b
				ON a.`NoTransaksi` = b.`NoTransaksi` AND b.`KdAgent` ='$stiker' 
				 AND b.TglTransaksi BETWEEN DATE_SUB('$tgl', INTERVAL 30 DAY) AND '$tgl'
					
				";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();

		return $row;
	}

	function getNewNo($tahun,$bulan)
	{
		$sql = "SELECT NoKomisi FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
		$qry = $this->db->query($sql);
		return $qry->result_array();
		//return $this->getRow($sql);
	}

	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function NamaPrinter($id)
	{
		$sql = "SELECT * from kassa where ip='$id'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function getDate()
	{
		$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
		return $this->getRow($sql);
	}

	function getTotalNetto($no)
	{
		$sql = "SELECT SUM(Netto) FROM transaksi_detail WHERE NoStruk ='$no'";
		return $this->getArrayResult($sql);
	}

	function getWaktu($no)
	{
		$sql = "SELECT MIN(waktu) AS mulai, MAX(waktu) AS akhir FROM transaksi_detail WHERE NoStruk = '$no'";
		return $this->getArrayResult($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}



	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function ifPCodeBarcode($id)
	{
		$bar = substr($id, 0, 10);
		$sql = "SELECT KdRekening FROM rekening Where KdRekening='$id'";
		return $this->getRow($sql);
	}

	function getPCodeDet($kode, $field)
	{
		$sql = "select KdRekening, NamaRekening from rekening where KdRekening='$kode'";
		return $this->getRow($sql);
	}

	

}

/* End of file pembayaran_komisi_group_model.php */
/* Location: ./application/models/pembayaran_komisi_group_model.php */