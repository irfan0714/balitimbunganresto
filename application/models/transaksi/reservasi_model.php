<?php

class Reservasi_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getKodeBank($kd){

    	$sql = "SELECT KdRekening, KdPenerimaan, KdSubDivisi FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    function getSupervisor(){
		$sql = "SELECT KdSupervisor, NamaSupervisor FROM supervisor where status='A' ORDER BY NamaSupervisor";
    	return $this->getArrayResult($sql);
	}
    
    function getNewNo($tgl)
	{
	    $tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoReceipt=NoReceipt+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoReceipt FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
    
    function getKasBank()
	{
		$sql = "SELECT KdKasBank, NamaKasBank FROM kasbank ORDER BY NamaKasBank";
    	return $this->getArrayResult($sql);
    }
    
     function getDataReservasiBeo($id)
	{
		$sql = "SELECT 
				 a.id,
				  a.`NoDokumen`,
				  DATE_FORMAT(DATE(a.TglDokumen), '%d-%m-%Y') AS TglDokumenx,
				  b.`Nama` 
				 FROM `trans_reservasi` a INNER JOIN `tourtravel` b ON a.`KdTravel`=b.`KdTravel` WHERE a.id='$id';";
    	return $this->getRow($sql);
    }

    function getReservasiList($limit, $offset, $arrSearch) {
        $mylib = new globallib();
		//print_r($arrSearch);die;
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }

        $where_keyword = "";
        $where_supervisor = "";
        $where_status = "";
        $where_tanggal = " and a.`TglDokumen` BETWEEN '".$mylib->ubah_tanggal("01-".date("m-Y"))."' AND '".$mylib->ubah_tanggal(date("t-m-Y"))."' ";
        $order_sort= " ORDER BY a.NoDokumen DESC ";
        if (count($arrSearch) * 1 > 0) {
            
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "a.NoDokumen";
                $arr_keyword[1] = "e.`Nama`";
                $arr_keyword[2] = "a.GroupCode";


                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }
            
            if ($arrSearch["supervisor"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "c.KdSupervisor";

                $search_keyword = $mylib->search_keyword($arrSearch["supervisor"], $arr_keyword);
                $where_supervisor = $search_keyword;
            }
            
            if ($arrSearch["status"] != "") {
            	switch ($arrSearch["status"]) {
    				case "P":
        				$where_status = " and SUBSTR(a.NoDokumen, 1,3)='SGV' and a.Status=0 and a.status_approve=0 and a.status_reject=0 ";
        				break;
    				case "W":
    					$where_status = " and a.Status=1 and a.status_approve=0 and a.status_reject=0 ";
    					break;
    				case "A":
    					$where_status = " and a.Status=1 and a.status_approve=1 ";
    					break;
    				case "R":
    					$where_status = " and a.Status=0 and a.status_reject=1 ";
    					break;
    				case "B":
    					$where_status = " and SUBSTR(a.NoDokumen, 1,7)='BOOKING' and a.Status<>'4'";
    					break;
    				case "C":
    					$where_status = " and a.Status='4'";
    					break;
				}
            }
            
            if ($arrSearch["tgl_awal"] != "" or $arrSearch["tgl_akhir"] != "") {
        				$where_tanggal = " and a.`TglDokumen` BETWEEN '".$arrSearch["tgl_awal"]."' AND '".$arrSearch["tgl_akhir"]."' ";
        				
            }
            
            if ($arrSearch["sort"] != "") {
            	if($arrSearch["sort"] =="1"){
					$order_sort = " ORDER BY a.NoDokumen DESC ";
				}else if($arrSearch["sort"] =="2"){
					$order_sort = " ORDER BY a.NoDokumen ASC ";
				}else if($arrSearch["sort"] =="3"){
					$order_sort = " ORDER BY a.TglDokumen ASC ";
				}else if($arrSearch["sort"] =="4"){
					$order_sort = " ORDER BY a.TglDokumen DESC ";
				}  		
            }
            
            
            
        }
	
        $sql = "  
            SELECT 
              a.*,
			  b.*,
			  c.*,
			  d.*,
			  d.UserName AS HdChannel,
              e.`Nama`
            FROM trans_reservasi a 
  				  LEFT JOIN `aktivitas_beo` b 
					ON a.`Event1` = b.`id_aktivitas_beo` 
				  LEFT JOIN salesman c 
					ON a.Sales_In_Charge = c.KdSalesman 
				  LEFT JOIN supervisor d 
					ON d.KdSupervisor = c.KdSupervisor
                    LEFT JOIN tourtravel e
                      ON e.`KdTravel` = a.`KdTravel`
            WHERE 
            	1  
            	" . $where_keyword . " " . $where_supervisor . $where_status . $where_tanggal . $order_sort.                                
            " 
			LIMIT 
              $offset,$limit";
        
        /*
        
              ORDER BY 
              CAST(a.NoDokumen AS UNSIGNED) DESC,
  			  a.id DESC 
        
        */
        
         //echo 'status : '.$arrSearch["status"];
         //echo "<hr/>"; 
         //echo $sql;
        return $this->getArrayResult($sql);
    }

	function getJmlBooking($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS booking FROM trans_reservasi a WHERE SUBSTR(a.NoDokumen, 1,7)='BOOKING' AND status <> '4' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
	function getJmlBeo($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS beo FROM trans_reservasi a WHERE SUBSTR(a.NoDokumen, 1,3)='SGV' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
	function getJmlBeoPending($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS pending FROM trans_reservasi a WHERE SUBSTR(a.NoDokumen, 1,3)='SGV' AND status = '0' AND status_approve='0' AND status_reject='0' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
	function getJmlBeoWaiting($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS waiting FROM trans_reservasi a WHERE SUBSTR(a.NoDokumen, 1,3)='SGV' AND status = '1' AND status_approve='0' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
	function getJmlBeoApprove($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS approved FROM trans_reservasi a WHERE SUBSTR(a.NoDokumen, 1,3)='SGV' AND status = '1' AND status_approve='1' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
	function getJmlBeoReject($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS rejected FROM trans_reservasi a WHERE SUBSTR(a.NoDokumen, 1,3)='SGV' AND status = '0' AND status_reject='1' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
	function getJmlBeoCancel($UserLevel) {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.id) AS canceled FROM trans_reservasi a WHERE status = '4' AND MONTH(a.`TglDokumen`)='$mon' AND YEAR(a.`TglDokumen`)='$yer';";	
		
		return $this->getRow($sql);
	}
	
    function num_reservasi_row($arrSearch) {
        $mylib = new globallib();

        $where_keyword = "";
        $where_supervisor = "";
        $where_status = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "t.NoDokumen";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }
            
            if ($arrSearch["supervisor"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "s.KdSupervisor";

                $search_keyword = $mylib->search_keyword($arrSearch["supervisor"], $arr_keyword);
                $where_supervisor = $search_keyword;
            }
            
            if ($arrSearch["status"] != "") {
            	switch ($arrSearch["status"]) {
    				case "P":
        				$where_status = " and t.Status=0 and t.status_approve=0 and t.status_reject=0 ";
        				break;
    				case "W":
    					break;
    					$where_status = " and t.Status=1 and t.status_approve=0 and t.status_reject=0 ";
    				case "A":
    					$where_status = " and t.Status=1 and t.status_approve=1 ";
    					break;
    				case "R":
    					$where_status = " and t.Status=1 and t.status_reject=1 ";
    					break;
				}
            }
        }

        $sql = "SELECT t.* 
			FROM trans_reservasi t INNER JOIN salesman s ON t.`Sales_In_Charge`=s.`KdSalesman`
            WHERE 1	" . $where_keyword . " " . $where_supervisor . $where_status;

        return $this->NumResult($sql);
    }

    function getSearch($id, $module, $user) {
        $sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
        return $this->getRow($sql);
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

    function getDivisi() {
        $sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
        return $this->getArrayResult($sql);
    }

    function getGudang() {
        $sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
        return $this->getArrayResult($sql);
    }
    
    function getSales() {
        $sql = "SELECT * from salesman";
        return $this->getArrayResult($sql);
    }
	
	function getTravelMail($kdtravel) {
        $sql = "SELECT * FROM `tourtravel` a WHERE a.`KdTravel`='".$kdtravel."';";
        return $this->getRow($sql);
    }
	
	function getSalesMail($kdsales) {
        $sql = "SELECT * from salesman a WHERE a.`KdSalesman`='".$kdsales."';";
        return $this->getRow($sql);
    }
	
	function getAktivitas1Mail($event) {
        $sql = "SELECT * FROM `aktivitas_beo` a WHERE a.`id_aktivitas_beo`='".$event."';";
        return $this->getRow($sql);
    }
	
	function getAktivitas2Mail($event) {
        $sql = "SELECT * FROM `aktivitas_beo` a WHERE a.`id_aktivitas_beo`='".$event."';";
        return $this->getRow($sql);
    }
	
	function getAktivitas3Mail($event) {
        $sql = "SELECT * FROM `aktivitas_beo` a WHERE a.`id_aktivitas_beo`='".$event."';";
        return $this->getRow($sql);
    }
	
	function getAktivitas4Mail($event) {
        $sql = "SELECT * FROM `aktivitas_beo` a WHERE a.`id_aktivitas_beo`='".$event."';";
        return $this->getRow($sql);
    }
	
	function getAktivitas5Mail($event) {
        $sql = "SELECT * FROM `aktivitas_beo` a WHERE a.`id_aktivitas_beo`='".$event."';";
        return $this->getRow($sql);
    }
    
    function getAktivitas() {
        $sql = "SELECT * from aktivitas_beo WHERE 1 ORDER BY `id_aktivitas_beo` ASC";
        return $this->getArrayResult($sql);
    }
    
    function getUangMukaBeos($id) {
        $sql = "SELECT * from uang_muka_beo WHERE id='$id'";
        return $this->getRow($sql);
    }
    
    function getVoucherBeos($id) {
        $sql = "SELECT * from voucher_beo WHERE id='$id'";
        return $this->getRow($sql);
    }
    
    function getDeposit() {
        $sql = "
        		SELECT 
				  * 
				FROM
				  `uang_muka_beo` a 
				  INNER JOIN tourtravel b 
				    ON a.`KdTravel` = b.`KdTravel` 
				WHERE a.`BEO` = '0' 
				ORDER BY a.id DESC ;
        		";
        return $this->getArrayResult($sql);
    }
    
    function getVoucher() {
        $sql = "
        		SELECT 
				  * 
				FROM
				  `voucher_beo` a 
				  INNER JOIN tourtravel b 
				    ON a.`tourtravel` = b.`KdTravel` 
				WHERE 1 AND a.`expDate` > NOW()
				AND a.Jenis = '1'
				ORDER BY a.id DESC 
        		";
        return $this->getArrayResult($sql);
    }
    
    function getAdmSales($user) {
        $sql = "
        		SELECT b.`employee_name` AS Nama, d.`jabatan_name` AS Jabatan FROM `user` a INNER JOIN employee b ON a.`employee_nik` = b.`employee_nik` 
				INNER JOIN employee_position c ON b.`employee_id` = c.`employee_id` 
				INNER JOIN jabatan d ON c.`jabatan_id`=d.`jabatan_id` WHERE a.`UserName`='".$user."';
        	   ";
        
        return $this->getArrayResult($sql);
    }

    function getSatuan() {
        $sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
        return $this->getArrayResult($sql);
    }

    function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function getDetail($id) {
        $sql = "
			SELECT 
			  permintaan_barang_detail.* 
			FROM
			  permintaan_barang_detail 
			WHERE 1 
			  AND permintaan_barang_detail.NoDokumen = '$id' 
			ORDER BY 
			  permintaan_barang_detail.sid DESC
		";

        return $this->getArrayResult($sql);
    }

    function getHeader($id) {
        $sql = "
			SELECT *, DATE_FORMAT(DATE(`trans_reservasi`.`ReservasiDate`), '%d-%m-%Y') AS tgl_reservasi 
			FROM `trans_reservasi` INNER JOIN `aktivitas_beo` ON `aktivitas_beo`.`id_aktivitas_beo`= `trans_reservasi`.`Event1` OR 
			`aktivitas_beo`.`id_aktivitas_beo`= `trans_reservasi`.`Event2` WHERE NoDokumen='$id' LIMIT 0,1";
        return $this->getArrayResult($sql);
        //return $this->getRow($sql);
    }

    function getHeaderForPrint($id) {
        $sql = "
			SELECT * FROM `trans_reservasi_detail` WHERE NoDokumen='$id'";
        return $this->getArrayResult($sql);
        //return $this->getRow($sql);
    }

    function getTravel($id) {
        $sql = "
			SELECT * FROM `tourtravel` WHERE KdTravel='$id'";
        //return $this->getArrayResult($sql);
        return $this->getRow($sql);
    }

    function getDetailForPrint($id) {
        $sql = "SELECT `trans_reservasi`.*,d.PCode,d.NamaBarang,d.Qty,d.Harga FROM trans_reservasi INNER JOIN `trans_reservasi_detail` d
                ON d.`NoDokumen` = trans_reservasi.`NoDokumen` AND trans_reservasi.`NoDokumen` = '$id'";
        //echo $sql;
        return $this->getArrayResult($sql);
        //return $this->getRow($sql);
    }

    public function get_by_id($id) {
        $sql = "SELECT NoDokumen,GroupName, dp,Total, Nama  FROM `trans_reservasi` 
                INNER JOIN `tourtravel` ON trans_reservasi.`KdTravel`= tourtravel.`KdTravel` 
                WHERE  NoDokumen='$id'";
        //echo $sql;
        return $this->getRow($sql);
    }

    function cekNodok($id) {
        $sql = "
			SELECT 
  			  trans_pr_header.NoDokumen,
			  trans_pr_header.NoPermintaan, 
			  DATE_FORMAT(trans_pr_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_pr_header.AddUser 
			FROM
			  trans_pr_header 
			WHERE 1 
			  AND trans_pr_header.NoPermintaan = '" . $id . "'
		";

        return $this->getRow($sql);
    }     
    
    function NamaPrinter($id) 
    {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>