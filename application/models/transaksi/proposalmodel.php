<?php
class Proposalmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
        $this->load->library('globallib');
    }

    function get_proposal_List($num, $offset,$arrSearch)
	{
		$mylib = new globallib();
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		
		$where_keyword="";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		    	$arr_keyword[0] = "proposal.NamaProposal";
		        $arr_keyword[1] = "proposal.NoProposal";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
		} 
    	
		$sql="SELECT proposal.*,divisi.*,employee.*,
			  DATE_FORMAT(proposal.Tanggal, '%d-%m-%Y') AS Tanggal_,
			  DATE_FORMAT(proposal.PeriodeAwal, '%d-%m-%Y') AS PeriodeAwal_,
			  DATE_FORMAT(proposal.PeriodeAkhir, '%d-%m-%Y') AS PeriodeAkhir_,
			  proposal.PIC AS vPIC,
			  proposal.Approval1Name AS Aprv1,
			  proposal.Approval2Name AS Aprv2,
			  proposal.Approval3Name AS Aprv3,
			  proposal.AddUser AS CreatedBy
			  FROM proposal 
		      INNER JOIN divisi ON divisi.KdDivisi = proposal.KdDivisi
		      INNER JOIN employee ON employee.username = proposal.PIC
		$where_keyword ORDER BY proposal.AddDate DESC, proposal.`NoProposal` DESC  LIMIT 0,100";
		//echo $sql."<hr>";
		$user = $this->session->userdata('username');
		if($user=='yuri0717'){
		echo $sql."<hr>";
			// 
		}
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_proposal_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT * FROM proposal  $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getBudgeting($Aktivitas,$var)
	{
    	$sql = "
				SELECT ".$var." AS jml_terpakai FROM `budget` WHERE `KdAktivitas`='".$Aktivitas."'
				;
				";
		//echo $sql;die;
		//return $this->getRow($sql);
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getBudget($id)
	{
    	$sql = "
    			SELECT 
				  budget.`Budget01` + budget.`Budget02` + budget.`Budget03` + budget.`Budget04` + budget.`Budget05` + budget.`Budget06` + budget.`Budget07` + budget.`Budget08` + budget.`Budget09` + budget.`Budget10` + budget.`Budget11` + budget.`Budget12` AS budget,
				  budget.`Terpakai01` + budget.`Terpakai02` + budget.`Terpakai03` + budget.`Terpakai04` + budget.`Terpakai05` + budget.`Terpakai06` + budget.`Terpakai07` + budget.`Terpakai08` + budget.`Terpakai09` + budget.`Terpakai10` + budget.`Terpakai11` + budget.`Terpakai12` AS terpakai, 
				  budget.`Realisasi01` + budget.`Realisasi02` + budget.`Realisasi03` + budget.`Realisasi04` + budget.`Realisasi05` + budget.`Realisasi06` + budget.`Realisasi07` + budget.`Realisasi08` + budget.`Realisasi09` + budget.`Realisasi10` + budget.`Realisasi11` + budget.`Realisasi12` AS realisasi,
				  proposal.`LatarBelakang`,
				  proposal.`Tujuan`,
				  proposal.`NamaProposal`,
				  aktivitas.`NamaAktivitas`,
				  DATE_FORMAT(proposal.`PeriodeAwal`, '%d-%m-%Y') AS Periode_awal,
				  DATE_FORMAT(proposal.`PeriodeAkhir`, '%d-%m-%Y') AS Periode_akhir,
				  proposal.`Mekanisme`
				FROM
				  proposal 
				  INNER JOIN aktivitas 
				    ON proposal.`KdAktivitas` = aktivitas.`KdAktivitas` 
				  INNER JOIN budget 
				    ON budget.`KdAktivitas` = aktivitas.`KdAktivitas` 
				WHERE proposal.`NoProposal` = '".$id."' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getProDetail($id)
	{
    	$sql = "
    			SELECT 
				 SUM(`proposal_detail`.`HargaSatuan`*`proposal_detail`.`Qty`) AS jml_detail
				FROM
				  `proposal_detail`
				WHERE `proposal_detail`.`NoProposal` = '".$id."' ;
    			";
    			
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getdetailproposal($id)
	{
    	$sql = "
    			SELECT 
				  proposal_detail.`Keterangan`,
				  proposal_detail.`Qty`,
				  proposal_detail.`HargaSatuan`,
				  proposal_detail.`HargaSatuan`*proposal_detail.`Qty` AS Jumlah
				FROM
				  `proposal_detail` 
				WHERE `proposal_detail`.`NoProposal` = '".$id."' ;
    			";
    			
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function gettargetproposal($id)
	{
    	$sql = "
    			SELECT 
				  proposal_target.`Keterangan`,
				  proposal_target.`Qty`,
				  proposal_target.`HargaSatuan`,
				  proposal_target.`HargaSatuan`*proposal_target.`Qty` AS Jumlah
				FROM
				  `proposal_target` 
				WHERE `proposal_target`.`NoProposal` = '".$id."' ;
    			";
    			
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function gettotaldetail($id)
	{
    	$sql = "
    			SELECT 
				  SUM(proposal_detail.`HargaSatuan`*proposal_detail.`Qty`) AS Totaldetail
				FROM
				  `proposal_detail` 
				WHERE `proposal_detail`.`NoProposal` = '".$id."' ;
    			";
    			
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
     function gettotaltarget($id)
	{
    	$sql = "
    			SELECT 
				  SUM(proposal_target.`HargaSatuan`*proposal_target.`Qty`) AS Totaltarget
				FROM
				  `proposal_target` 
				WHERE `proposal_target`.`NoProposal` = '".$id."' ;
    			";
    			
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getProTarget($id)
	{
    	$sql = "
    			SELECT 
				 SUM(`proposal_target`.`HargaSatuan`*`proposal_target`.`Qty`) AS jml_target
				FROM
				  `proposal_target`
				WHERE `proposal_target`.`NoProposal` = '".$id."' ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getapprove1($id)
	{
    	$sql = "
    			SELECT 
				  employee.`employee_name`,
				  jabatan.`jabatan_name` 
				FROM
				  employee 
				  INNER JOIN `employee_position` 
				    ON employee.`employee_id` = `employee_position`.`employee_id` 
				  INNER JOIN jabatan 
				    ON jabatan.`jabatan_id` = `employee_position`.`jabatan_id` 
				WHERE employee.`username` IN 
				  (SELECT 
				    proposal.`Approval1Name` 
				  FROM
				    proposal 
				  WHERE proposal.`NoProposal` = '".$id."') ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getapprove2($id)
	{
    	$sql = "
    			SELECT 
				  employee.`employee_name`,
				  jabatan.`jabatan_name` 
				FROM
				  employee 
				  INNER JOIN `employee_position` 
				    ON employee.`employee_id` = `employee_position`.`employee_id` 
				  INNER JOIN jabatan 
				    ON jabatan.`jabatan_id` = `employee_position`.`jabatan_id` 
				WHERE employee.`username` IN 
				  (SELECT 
				    proposal.`Approval2Name` 
				  FROM
				    proposal 
				  WHERE proposal.`NoProposal` = '".$id."') ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getapprove3($id)
	{
    	$sql = "
    			SELECT 
				  employee.`employee_name`,
				  jabatan.`jabatan_name` 
				FROM
				  employee 
				  INNER JOIN `employee_position` 
				    ON employee.`employee_id` = `employee_position`.`employee_id` 
				  INNER JOIN jabatan 
				    ON jabatan.`jabatan_id` = `employee_position`.`jabatan_id` 
				WHERE employee.`username` IN 
				  (SELECT 
				    proposal.`Approval3Name` 
				  FROM
				    proposal 
				  WHERE proposal.`NoProposal` = '".$id."') ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getdibuatoleh($id)
	{
    	$sql = " 
				SELECT 
				  employee.`employee_name`,
				  'test' AS `jabatan_name`  
				FROM employee
				WHERE employee.`username` IN  
								  (SELECT 
								    proposal.`AddUser` 
								  FROM
								    proposal 
								  WHERE proposal.`NoProposal` = '".$id."') ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    /*function getdibuatoleh($user)
	{
    	$sql = "
    			SELECT 
				  employee.`employee_name`,
				  jabatan.`jabatan_name` 
				FROM
				  employee 
				  INNER JOIN `employee_position` 
				    ON employee.`employee_id` = `employee_position`.`employee_id` 
				  INNER JOIN jabatan 
				    ON jabatan.`jabatan_id` = `employee_position`.`jabatan_id` 
				WHERE employee.`username` IN 
				  (SELECT 
				    proposal.`AddUser` 
				  FROM
				    proposal 
				  WHERE proposal.`AddUser` = '".$user."') ;
    			";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }*/
	    
    function getAktivitasList()
	{
    	$sql = "SELECT * FROM aktivitas ORDER BY NamaAktivitas ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getCekApprove($NoProposal)
	{
    	$sql = "SELECT * FROM proposal WHERE `Status_approve1` = '1' AND `Status_approve2` = '1' AND `Status_approve3` = '1' AND proposal.NoProposal='".$NoProposal."'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getNilaiPro($nopo)
	{
    	$sql = "
				SELECT 
				  SUM(cek.jml) AS total_proposal , cek.`KdAktivitas`
				FROM
				  (SELECT 
				    proposal_detail.`HargaSatuan` * proposal_detail.`Qty` AS jml , proposal.`KdAktivitas`
				  FROM proposal 
				    INNER JOIN proposal_detail 
				      ON proposal.`NoProposal` = proposal_detail.`NoProposal` 
				  WHERE proposal.`NoProposal` = '".$nopo."' 
				  UNION
				  SELECT 
				    proposal_target.`HargaSatuan` * proposal_target.`Qty` AS jml , proposal.`KdAktivitas`
				  FROM
				    proposal 
				    INNER JOIN proposal_target 
				      ON proposal.`NoProposal` = proposal_target.`NoProposal` 
				  WHERE proposal.`NoProposal` = '".$nopo."' ) AS cek 
				;
				";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    public function get_by_proposal($nopro)
	{
		
		$sql = "
			SELECT 
			aktivitas.*,
			proposal.*,
			divisi.*,
			employee.*,
			employee.email AS emails,
			DATE_FORMAT(proposal.Tanggal, '%d-%m-%Y') AS Tanggal_,
			DATE_FORMAT(proposal.PeriodeAwal, '%d-%m-%Y') AS PeriodeAwal_,
			DATE_FORMAT(proposal.PeriodeAkhir, '%d-%m-%Y') AS PeriodeAkhir_
			FROM aktivitas INNER JOIN proposal ON aktivitas.KdAktivitas = proposal.KdAktivitas
		                            INNER JOIN divisi ON divisi.KdDivisi = proposal.KdDivisi
		                            INNER JOIN employee ON employee.username = proposal.PIC 
								    WHERE proposal.NoProposal='".$nopro."';
			   ";
		//echo $sql;die;
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
        
	}
		
	
	function getDivisi()
	{
    	$sql = "SELECT * FROM divisi a  ORDER BY a.KdDivisi ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
		
	function cek_aja1($nopro)
	{
    	$sql = "SELECT * FROM proposal WHERE proposal.NoProposal='".$nopro."' AND proposal.Status_approve1='1'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    
    
    function cek_aja3($nopro)
	{
    	$sql = "SELECT * FROM proposal WHERE proposal.NoProposal='".$nopro."' AND proposal.Status_approve3='1'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function cek_aja2($nopro)
	{
    	$sql = "SELECT * FROM proposal WHERE proposal.NoProposal='".$nopro."' AND proposal.Status_approve2='1'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
    
    function getEmployeePic()
	{
    	$sql = "SELECT * FROM employee a WHERE a.username<>'' ORDER BY a.employee_name ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getEmployeeApproval1()
	{
    	$sql = "SELECT * FROM employee a  WHERE a.username<>'' ORDER BY a.employee_name ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getEmployeeApproval2()
	{
    	$sql = "SELECT * FROM employee a  ORDER BY a.employee_name ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getEmployeeApproval3()
	{
    	$sql = "SELECT * FROM employee a  ORDER BY a.employee_name ASC;";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getAktivitas($KdAktivitas)
	{
    	$sql = "SELECT * FROM aktivitas WHERE KdAktivitas LIKE '%".$KdAktivitas."%' OR NamaAktivitas LIKE '%".$KdAktivitas."%' ORDER BY NamaAktivitas ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getSrcEmplPic($var)
	{
    	$sql = "SELECT * FROM employee WHERE employee_id LIKE '%".$var."%' OR employee_name LIKE '%".$var."%' ORDER BY employee_name ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getSrcEmplApr1($var)
	{
    	$sql = "SELECT * FROM employee WHERE employee_id LIKE '%".$var."%' OR employee_name LIKE '%".$var."%' ORDER BY employee_name ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getEmailEmployee($var)
	{
    	$sql = "SELECT email FROM employee WHERE username='$var'";
		return $this->getRow($sql);
    }
    
    function getSrcEmplApr2($var)
	{
    	$sql = "SELECT * FROM employee WHERE employee_id LIKE '%".$var."%' OR employee_name LIKE '%".$var."%' ORDER BY employee_name ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getPrDetail($nodok)
	{
    	$sql = "SELECT * FROM proposal_detail WHERE proposal_detail.NoProposal='".$nodok."' ORDER BY proposal_detail.NoUrut ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function getPrTarget($nodok)
	{
    	$sql = "SELECT * FROM proposal_target WHERE proposal_target.NoProposal='".$nodok."' ORDER BY proposal_target.NoUrut ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    //total canceled
    function getProposalCanceled() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS canceled FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='2';";	
		
		return $this->getRow($sql);
	}
	
	//total rejected
    function getProposalRejected() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS rejected FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='1' AND a.Status_Reject='1';";	
		
		return $this->getRow($sql);
	}
	
	function boleh_lihat($username) {
		
		$sql="SELECT * FROM otorisasi_user a WHERE a.`Tipe`='lihat_proposal' AND a.`UserName`='$username';";	
		
		return $this->getRow($sql);
	}
    
    //total pending
    function getProposalPending() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS pending FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='0';";	
		
		return $this->getRow($sql);
	} 
	
	//total waiting approve 1
    function getProposalWaitingApprove1() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS waiting1 FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='1' AND a.`Status_Approve1`='0' AND a.`Status_Approve2`='0' AND a.`Status_Approve3`='0';";	
		
		return $this->getRow($sql);
	} 
	
	//total waiting approve 2
    function getProposalWaitingApprove2() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS waiting2 FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='1' AND a.`Status_Approve1`='1' AND a.`Status_Approve2`='0' AND a.`Status_Approve3`='0' ;";	
		
		return $this->getRow($sql);
	} 
	
	//total waiting approve 3
    function getProposalWaitingApprove3() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS waiting3 FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='1' AND a.`Status_Approve1`='1' AND a.`Status_Approve2`='1' AND a.`Status_Approve3`='0' ;";	
		
		return $this->getRow($sql);
	} 
	
	//total Approved
    function getProposalWaitingApproved() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="SELECT COUNT(a.NoProposal) AS approved FROM `proposal` a WHERE YEAR(a.`Tanggal`)='$yer' AND MONTH(a.`Tanggal`)='$mon' AND a.`Status`='1' AND a.`Status_Approve1`='1' AND a.`Status_Approve2`='1' AND a.`Status_Approve3`='1' ;";	
		
		return $this->getRow($sql);
	}    
    
    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }
	
	
}
?>