<?php

class Konfirm_terimamodel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }

    function get_konfirm_terima_List($limit, $offset, $arrSearch) {
        $mylib = new globallib();

        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }

        $where_keyword = "";
        $where_gudang = "";
        $where_divisi = "";
        $where_gudang_admin = "";
        $where_status = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "trans_terima_header.NoDokumen";
                $arr_keyword[1] = "trans_terima_header.Keterangan";
                $arr_keyword[2] = "gudang.Keterangan";
                //$arr_keyword[3] = "divisi.NamaDivisi";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }

            if ($arrSearch["gudang"] != "") {
                $where_gudang = "AND trans_terima_header.KdGudang = '" . $arrSearch["gudang"] . "'";
            }

			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND trans_terima_header.KdDivisi = '".$arrSearch["divisi"]."'";	
			}
			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "trans_terima_header.KdGudang", "in");
			}

            if ($arrSearch["status"] != "") {
                $where_status = "AND trans_terima_header.Status = '" . $arrSearch["status"] . "'";
            }
        }

        $sql = "  
            SELECT 
              trans_terima_header.NoDokumen,
              DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
              trans_terima_header.PoNo,
              supplier.Nama,
              trans_terima_header.Keterangan,
              trans_terima_header.KdGudang,
              gudang.Keterangan AS NamaGudang,
              trans_terima_header.Status,
              trans_terima_header.FlagKonfirmasi
            FROM
              trans_terima_header
              LEFT JOIN gudang 
                ON gudang.KdGudang = trans_terima_header.KdGudang 
              LEFT JOIN supplier 
                ON supplier.KdSupplier = trans_terima_header.KdSupplier 
            WHERE 
            	1  
            	" . $where_keyword . "
            	" . $where_gudang . "
            	" . $where_status . "                                   
            ORDER BY TglDokumen DESC,
              CAST(NoDokumen AS UNSIGNED) DESC,
              trans_terima_header.NoDokumen DESC 
            Limit 
              $offset,$limit
        ";
        /* echo $sql;
          echo "<hr/>"; */
        return $this->getArrayResult($sql);
    }
    
    function Querydata($sql){
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
	}

    function num_konfirm_terima_row($arrSearch) {
        $mylib = new globallib();

        $where_keyword = "";
        $where_gudang = "";
        $where_divisi = "";
        $where_gudang_admin = "";
        $where_status = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "trans_terima_header.NoDokumen";
                $arr_keyword[1] = "trans_terima_header.Keterangan";
                $arr_keyword[2] = "gudang.Keterangan";
                //$arr_keyword[3] = "divisi.NamaDivisi";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }

            if ($arrSearch["gudang"] != "") {
                $where_gudang = "AND trans_terima_header.KdGudang = '" . $arrSearch["gudang"] . "'";
            }

            if ($arrSearch["divisi"] != "") {
                $where_divisi = "AND trans_terima_header.KdDivisi = '" . $arrSearch["divisi"] . "'";
            }

            if ($arrSearch["gudang_admin"] != "") {
                $where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "trans_terima_header.KdGudang", "in");
            }

            if ($arrSearch["status"] != "") {
                $where_status = "AND trans_terima_header.Status = '" . $arrSearch["status"] . "'";
            }
        }

        $sql = "
            SELECT 
              trans_terima_header.NoDokumen,
              DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
              trans_terima_header.PoNo,
              supplier.Nama,
              trans_terima_header.Keterangan,
              trans_terima_header.KdGudang,
              gudang.Keterangan AS NamaGudang,
              trans_terima_header.Status
            FROM
              trans_terima_header
              LEFT JOIN gudang 
                ON gudang.KdGudang = trans_terima_header.KdGudang 
              LEFT JOIN supplier 
                ON supplier.KdSupplier = trans_terima_header.KdSupplier 
            WHERE 
            	1
            	" . $where_keyword . "
            	" . $where_gudang . "
            	" . $where_status . "       
		";

        return $this->NumResult($sql);
    }

    function getSearch($id, $module, $user) {
        $sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
        return $this->getRow($sql);
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

    function getDivisi() {
        $sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
        return $this->getArrayResult($sql);
    }

    function getSupplier() {
        $sql = "SELECT KdSupplier,Nama FROM supplier ORDER BY supplier.Nama ASC";
        return $this->getArrayResult($sql);
    }

    function getGudang($arr_gudang = "") {
        $mylib = new globallib();

        $where_gudang_admin = "";
        if (count($arr_gudang) > 0) {
            $where_gudang_admin = $mylib->where_array($arr_gudang, "gudang.KdGudang", "in");
        }

        $sql = "SELECT KdGudang,Keterangan as NamaGudang FROM gudang WHERE 1 " . $where_gudang_admin . " order by KdGudang";
        return $this->getArrayResult($sql);
    }

    public function get_by_id($id) {
        $sql = "SELECT NoDokumen,TglDokumen,PoNo,KdSupplier FROM `trans_terima_header` WHERE NoDokumen='$id'";
        // echo $sql;
        return $this->getRow($sql);
    }

    function getSatuan() {
        $sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
        return $this->getArrayResult($sql);
    }

    function getDetail($id) {
        $sql = "
        SELECT trans_terima_detail.*, NamaLengkap, trans_order_barang_detail.`Harga` AS HargaPO FROM trans_terima_header 
        LEFT JOIN trans_order_barang_header ON trans_order_barang_header.`NoDokumen`= trans_terima_header.PoNo 
        LEFT JOIN trans_terima_detail ON trans_terima_detail.`NoDokumen`= trans_terima_header.NoDokumen 
        LEFT JOIN masterbarang ON masterbarang.PCode = trans_terima_detail.PCode 
        left JOIN trans_order_barang_detail 
        ON trans_order_barang_detail.`NoDokumen` = trans_terima_header.PoNo
        AND trans_order_barang_detail.`PCode` = trans_terima_detail.`PCode`        
			  WHERE 1 
			  AND trans_terima_detail.NoDokumen = '$id' 
			  ORDER BY 
			  trans_terima_detail.sid DESC
    ";
    
        return $this->getArrayResult($sql);
    }

    function getHeader($id) {
        $sql = "
			SELECT 
			  trans_terima_header.NoDokumen,
			  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
			  trans_terima_header.Keterangan,
			  trans_terima_header.KdGudang,
			  trans_terima_header.NoSuratJalan,
			  gudang.Keterangan AS NamaGudang,
			  trans_terima_header.PoNo,
                          supplier.Nama,
			  trans_terima_header.Status,
                          trans_terima_header.FlagKonfirmasi,
			  DATE_FORMAT(trans_terima_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_terima_header.AddUser,
			  DATE_FORMAT(trans_terima_header.EditDate, '%d-%m-%Y') AS EditDate,
			  trans_terima_header.EditUser,
			   trans_terima_header.PPn,
			   trans_terima_header.BiayaTransport
			FROM
			  trans_terima_header 
			  LEFT JOIN gudang 
			    ON gudang.KdGudang = trans_terima_header.KdGudang 
			  LEFT JOIN supplier 
                            ON supplier.KdSupplier = trans_terima_header.KdSupplier 
			WHERE 1 
			  AND trans_terima_header.NoDokumen = '$id' 
			LIMIT 1
        ";

        return $this->getRow($sql);
    }

    function cekNodok($id) {
        $sql = "
			SELECT 
  			  trans_pr_header.NoDokumen,
			  trans_pr_header.NoPermintaan, 
			  DATE_FORMAT(trans_pr_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_pr_header.AddUser 
			FROM
			  trans_pr_header 
			WHERE 1 
			  AND trans_pr_header.NoPermintaan = '" . $id . "'
		";

        return $this->getRow($sql);
    }

    function getMarketList($id) {
        $sql = "
			SELECT 
			  market_list.market_list_name,
			  market_list_detail.pcode,
			  masterbarang.NamaLengkap,
			  masterbarang.SatuanSt 
			FROM
			  market_list_pair 
			  INNER JOIN market_list 
			    ON market_list_pair.market_list_id = market_list.market_list_id 
			  INNER JOIN market_list_detail 
			    ON market_list_pair.market_list_id = market_list_detail.market_list_id 
			  INNER JOIN masterbarang 
			    ON market_list_detail.pcode = masterbarang.PCode 
			WHERE 1 
			  AND market_list_pair.UserLevelID = '" . $id . "' 
			ORDER BY 
			  market_list.market_list_name ASC,
			  masterbarang.NamaLengkap ASC
		";
        return $this->getArrayResult($sql);
    }

    function cekGetDetail($pcode, $nodok) {
        $sql = "
			SELECT 
			  trans_terima_detail.* 
			FROM
			  trans_terima_detail 
			WHERE 1 
			  AND trans_terima_detail.PCode = '$pcode' 
			  AND trans_terima_detail.NoDokumen = '$nodok' 
			LIMIT 1
		";

        return $this->getRow($sql);
    }
    
    function getinterface(){
		$sql = "select * from interface";
		return $this->getArrayResult($sql);
	}
    
    function getjatuhtempo($tgl, $kdsupplier){
		$sql = "SELECT DATE_ADD('$tgl',INTERVAL s.TOP DAY) as TglJt FROM supplier s WHERE kdsupplier='$kdsupplier'";
		$result = $this->getArrayResult($sql);
		return $result[0]['TglJt'];
	}

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function updatefakturpajak($v_no_dokumenf,$v_no_pof,$v_tgl_dokumenfa,$v_EditUserf,$nofpajakf,$tgleditf){
      $sql = "UPDATE invoice_pembelian_header SET NoFakturPajak = '$nofpajakf', EditDate = '$dataedit', EditUser = '$v_EditUserf'  where NoPenerimaan = '$v_no_dokumenf' AND NoPO = '$v_no_pof' AND Tanggal = '$v_tgl_dokumenfa' ";
      $this->db->query($sql);
    }

}

?>