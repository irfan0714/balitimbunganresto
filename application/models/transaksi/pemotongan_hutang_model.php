<?php
class Pemotongan_hutang_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
  
	
	function getDnno()
	{
    	$sql = "SELECT a.dnno AS nama FROM `debitnote` a ;";
    	return $this->getArrayResult($sql);
    }
    
    
    
    function num_pemotongan_hutang_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `dnallocation` a;       
		";
		                  
        return $this->NumResult($sql);
	}
	    
    function getDebitNoteDetailList($user)
	{
         $sql = "  
         SELECT 
		  * 
		FROM
		  `debitnotedtl_temp` a 
		  INNER JOIN rekening b 
			ON a.coano = b.`KdRekening` 
		WHERE CONCAT(a.dnno, a.adduser)='00000".$user."';
        ";              
       
		return $this->getArrayResult($sql);
    }
	
	function getDebitNoteAllocationDetail($id)
	{
        
    	$sql = "  
          SELECT 
          a.*,
		  b.*,
		  c.*,
		  d.`NilaiTransaksi`,
		  d.sisa,
		  DATE_FORMAT(d.`JatuhTempo`, '%d-%m-%Y') AS JatuhTempo
           FROM `dnallocation` a INNER JOIN `dnallocationdetail` b ON a.`dnallocationno` = b.`dnallocationno` 
		INNER JOIN `supplier` c ON a.`supplierid`=c.`KdSupplier`
		INNER JOIN hutang d
		   ON b.`invno`=d.`NoDokumen`
		 WHERE a.`dnallocationno` = '".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function getAmbilHutang($v_supplier, $tgl)
	{
		$sql = "
			SELECT 
			  a.* ,
			  b.*,
			  DATE_FORMAT(a.`JatuhTempo`, '%d-%m-%Y') AS JatuhTempo_,
			  FORMAT(ROUND(a.`NilaiTransaksi`),2) AS NilaiTransaksi1,
			  ROUND(a.`NilaiTransaksi`) AS NilaiTransaksi2,
			  FORMAT(ROUND(a.`sisa`),2) AS sisa1,
			  ROUND(a.`sisa`) AS sisa2
			  FROM `hutang` a 
			INNER JOIN supplier b ON a.`KdSupplier`=b.`KdSupplier`
			
			WHERE a.`KdSupplier`='".$v_supplier."' AND a.`TipeTransaksi` IN ('I','C','IM','CM') AND a.`Sisa`>0
			AND a.Tanggal <= '".$tgl."';
		";
		//echo $sql;die;
        return $this->db->query($sql);
	}
	
	  function getAmountDebit($dnno)
	{
		$sql = "
			SELECT * FROM debitnote where dnno='".$dnno."';
		";
		//echo $sql;die;
        return $this->db->query($sql);
	}
    
    function getDataSalesInvoiceDetail($user)
	{
        
    	$sql = "  
        SELECT 
		  * 
		FROM
		  `salesinvoicedetail_temp` a 
		  INNER JOIN masterbarang b 
		    ON a.`inventorycode` = b.`PCode` 
		WHERE a.`invoiceno` = '00000' AND a.`adduser`='".$user."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	   
	
	function getDnaList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.dnno";
				$arr_keyword[1] = "a.dnadate";
				$arr_keyword[2] = "b.Nama";
				$arr_keyword[3] = "a.status";
				
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT 
			  a.*,
			  b.*,
			  c.*,
			  SUM(c.NilaiTransaksi) AS total_hutang,
			  DATE_FORMAT(a.`dnadate`, '%d-%m-%Y') AS dnadate_indo 
			FROM
			  `dnallocation` a 
			  INNER JOIN supplier b 
				ON a.`supplierid` = b.`KdSupplier`
				INNER JOIN `dnallocationdetail`  d
			  ON d.`dnallocationno`=a.`dnallocationno` 
			  INNER JOIN hutang c 
				ON d.`invno` = c.`NoDokumen` 
			WHERE 1 
            	".$where_keyword."
            	".$where_customer."    
            	".$where_status."                                   
            GROUP BY a.`dnallocationno`
			ORDER BY a.`dnallocationno` ASC 
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			  a.*,
			  b.*,
			  c.*,
			  DATE_FORMAT(a.`dnadate`, '%d-%m-%Y') AS dnadate_indo
			FROM `dnallocation` a 
			INNER JOIN `supplier` b ON a.`supplierid`=b.`KdSupplier`
			INNER JOIN `dnallocationdetail` c on a.dnallocationno = c.dnallocationno
			WHERE 1 AND a.dnallocationno='".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function getDnamountremain($v_invno)
	{
		$sql = "
			SELECT a.`dnamountremain` FROM `debitnote` a WHERE a.`dnno`='".$v_invno."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function getDnamountremain2($v_invno)
	{
		$sql = "
			SELECT a.`dnamount` FROM `debitnote` a WHERE a.`dnno`='".$v_invno."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT COUNT(a.dono) AS jml FROM `salesinvoicedetail` a WHERE a.dono='".$nodok."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetDetail3($user)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail_temp` a WHERE a.`invoiceno`='00000' AND a.adduser='".$user."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	
	function ambil_allocation($dnallocationno)
	{
	 $sql = " 
	        SELECT a.`invno`,a.`allocation` FROM `dnallocationdetail` a WHERE a.`dnallocationno`='".$dnallocationno."';
	        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function ambil_sisa_hutang($nofak)
	{
		$sql = "
			SELECT a.`Sisa` FROM hutang a WHERE a.`NoDokumen`='".$nofak."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function ambil_dnamountremain_debit($nofak)
	{
		$sql = "
			SELECT a.`dnamountremain` FROM `debitnote` a WHERE a.`dnno` ='".$nofak."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function getsissahutang($v_debitno)
	{
		$sql = "
			SELECT * FROM `hutang` a WHERE a.`NoDokumen`='".$v_debitno."';;
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail4($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getDetail_cetak($id)
	{
	 $sql = " 
	 SELECT * FROM `salesinvoicedetail` a 
	 INNER JOIN `masterbarang` b ON a.`inventorycode`=b.`PCode`
	 WHERE a.`invoiceno`='".$id."';
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function hitungSalesInvoice_cetak($id)
	{
        
    	$sql = "  
        SELECT SUM(a.`quantity`*a.`nettprice`) AS total , SUM(b.`DiscLokal`) AS diskon
		FROM `salesinvoicedetail` a  
		INNER JOIN masterbarang b ON a.`inventorycode`=b.`PCode` 
		WHERE a.`invoiceno`='".$id."';
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
	
    function getSupplierList()
	{
    	$sql = "SELECT a.KdSupplier,a.Nama FROM supplier a ORDER BY a.Nama ASC";
    	//echo $sql;
        //echo "<hr/>";
    	return $this->getArrayResult($sql);
    }
    
    function getSupplier()
	{
		$sql = "SELECT a.KdSupplier,a.Nama FROM supplier a ORDER BY a.Nama ASC";
		$qry = $this->db->query($sql);
		$row = $qry->result_array($sql);
		$qry->free_result();
		return $row;
	}
	
	function getRequirement($KdSupplier)
	{
		$sql = "SELECT * FROM `debitnote` a WHERE a.`supplierid`='".$KdSupplier."' ";
		//echo $sql;die;
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}
	
	function getSibling($supplierid)
	{
		$sql = "SELECT 
				  a.*,
				  b.*,
				  FORMAT(b.`Sisa`,0) AS sisa_
				FROM
				  `debitnote` a 
				  INNER JOIN hutang b
				  ON a.`dnno`= b.`NoDokumen`
				WHERE 1 AND a.`supplierid` ='".$supplierid."' 
				 AND b.`Sisa`>0; ";
		//echo $sql;die;
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}
    
    function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function getKdRekening($nofaktur){
		$sql = "SELECT KdRekening FROM hutang where NoDokumen='$nofaktur' ";
		
		$row =  $this->getArrayResult($sql);
		return $row[0]['KdRekening'];
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>