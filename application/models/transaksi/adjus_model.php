<?php
class Adjus_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getRepairJurnal()
	{
    	$sql = "SELECT DISTINCT(a.kodenya) AS kodenya FROM jurnal_bermasalah a";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    
    function getPIHeaderJurnal2017($NoTransaksi)
	{
    	$sql = "SELECT a.`NoReferensi` FROM jurnalheader_per_31122017 a WHERE a.`NoTransaksi`='$NoTransaksi';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    
    function getHeaderJurnal2017($NoTransaksi)
	{
    	$sql = "SELECT a.`NoReferensi` FROM jurnalheader_per_31122017 a WHERE a.`NoTransaksi`='$NoTransaksi';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function cekRV($NoTransaksi)
	{
    	$sql = "SELECT * FROM trans_payment_header a WHERE a.`NoDokumen`='$NoTransaksi';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function cekRG($pcode,$tgl)
	{
		
		 list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		
		 $tgl_awal = $tahun."-".$bulan."-01";
		 $tgl_trans=$tgl;
		 
    	 $sql = "
    			SELECT
				  a.`PCode`,
				  SUM(a.`Qty`) Qty,
				  SUM(a.`QtyPcs`) AS QtyPcs,
				  ROUND(SUM(a.`QtyPcs`)*ROUND(SUM((a.Qty*a.`Harga`)/a.QtyPcs)/COUNT(a.PCode))) AS ValueRataRata,
				  ROUND(SUM((a.Qty*a.`Harga`)/a.QtyPcs)/COUNT(a.PCode)) AS HargaAVG
				FROM
				  trans_terima_detail a
				WHERE a.`PCode` = '$pcode'
				  AND a.`TglDokumen` BETWEEN '$tgl_awal'
				  AND '$tgl_trans' GROUP BY a.`PCode`;
    			";
    			
		return $this->getRow($sql);
    }
    
    function cekMutasi($pcode,$gudang,$tgl)
	{
		
		 list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		
		 $tgl_awal = $tahun."-".$bulan."-01";
		 $tgl_trans=$tgl;
		 
    	 $sql = "
    			SELECT 
				  SUM(a.`Qty`) AS Qty 
				FROM
				  mutasi a 
				WHERE a.`KdTransaksi` = 'DL' 
				AND a.`KodeBarang` = '$pcode' 
				AND (
				    a.`Gudang` = '$gudang' 
				    OR a.`GudangTujuan` = '$gudang'
				  ) AND a.`Tanggal` BETWEEN '$tgl_awal' AND '$tgl_trans'  
				GROUP BY a.`KodeBarang`;
    			";
    			
		return $this->getRow($sql);
    }
    
    function cekMutasiAnehSebelumnya($tgl)
	{
    	$sql = "
    			SELECT 
				  * 
				FROM
				  mutasi a 
				WHERE a.`KodeBarang` = '770001' 
				  AND (
				    a.`Gudang` = '05' 
				    OR a.`GudangTujuan` = '05'
				  ) AND a.Jenis='O' AND a.FlagScanMutasi='0' AND a.`Tanggal` >= '2018-08-01' AND a.`Tanggal` < '$tgl' ORDER BY a.Tanggal ASC;
    			";
    			if($tgl=='2018-08-14'){
					echo $sql;die();
				}
		return $this->getRow($sql);
		 
    }
    
    function cekSaldoAwal($tahun,$pcode)
	{
    	$sql = "
    			SELECT 
				    SUM(a.`GAwal10`) AS Qty_Saldo_Awal,
  					SUM(a.`GNAwal10`) AS Value_Saldo_Awal 
				FROM
				  stock a 
				WHERE 
				a.Tahun='$tahun' AND a.PCode='$pcode';
    			";
		return $this->getRow($sql);
    }
    
    function cekSaldoAja($tahun)
	{
    	//$sql = "SELECT a.Tahun,a.`PCode`,a.`KdGudang` FROM stock a WHERE a.PCode='$pcode' AND a.KdGudang='$gudang' AND a.`Tahun`='$tahun';";
    	$sql = "SELECT a.Tahun,a.`PCode`,a.`KdGudang` FROM stock a WHERE a.`Tahun`='$tahun' AND a.KdGudang<>'';";
		return $this->getArrayResult($sql);
    }
    
    function getHeaderJurnal($NoTransaksi)
	{
    	$sql = "SELECT a.`NoReferensi` FROM jurnalheader a WHERE a.`NoTransaksi`='$NoTransaksi';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getPIDetailJurnal2017($NoReferensi)
	{
    	$sql = "SELECT * FROM jurnaldetail_per_31122017 a WHERE a.`NoReferensi`='$NoReferensi';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getDetailJurnal2017($NoReferensi,$tahun,$bulan)
	{
    	$sql = "SELECT * FROM jurnaldetail_per_31122017 a WHERE a.`NoReferensi`='$NoReferensi' AND a.Tahun='$tahun' AND a.Bulan='$bulan';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getDetailJurnal($NoReferensi)
	{
    	$sql = "SELECT * FROM jurnaldetail a WHERE a.`NoReferensi` IN $NoReferensi;";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getMPCode()
	{
    	$sql = "SELECT * FROM barang_paket where 1 GROUP BY MPCode";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getDataSO($nodok_so)
	{
    	$sql = "SELECT * FROM opname_detail WHERE NoDokumen='$nodok_so'";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getMPCode2($mpcode)
	{
    	$sql = "SELECT * FROM barang_paket where 1 AND MPCode='$mpcode'";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getMPCodePkt()
	{
    	$sql = "SELECT * FROM masterbarang a WHERE a.`SatuanSt`='PKT' AND a.Status='A';";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
    
    function getPCodeTransaksi()
	{
    	$sql = "SELECT * FROM transaksi_detail where 1";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
	
	function getPCodePenerimaan()
	{
    	$sql = "SELECT DISTINCT(a.`PCode`) AS PCode FROM `trans_penerimaan_lain_detail` a WHERE 1;";
		return $this->getArrayResult($sql);
		 //return $this->db->query($sql);
    }
	
	function getDPCode($MPcode)
	{
    	$sql = "SELECT * FROM barang_paket a WHERE a.MPcode='$MPcode'";
    	
		return $this->getArrayResult($sql);
    }
	
	function getHargaBeli($Pcode)
	{
    	$sql = "SELECT a.HargaBeli FROM masterbarang a WHERE a.Pcode='$Pcode'";
		 return $this->getRow($sql);
    }
    
    function getHargaJual($Pcode)
	{
    	$sql = "SELECT a.Harga1c FROM masterbarang a WHERE a.Pcode='$Pcode'";
    	if($Pcode=="098002"){
			//echo $sql;die;
		}
		 return $this->getRow($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}
	
	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }	
    
    
    function num_delivery_order_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT a.`dono`,a.`dodate`,a.`warehousecode`,a.`customerid`,a.`contactperson`,a.`note`,a.`status` FROM `deliveryorder` a;       
		";
		                  
        return $this->NumResult($sql);
	}
		
	function getDeliveryOrderList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.dono";
				$arr_keyword[1] = "a.warehousecode";
				$arr_keyword[2] = "b.Nama";
				$arr_keyword[3] = "a.status";
				$arr_keyword[4] = "a.contactperson";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT a.`dono`,DATE_FORMAT(a.`dodate`, '%d-%m-%Y') AS dodate,a.`warehousecode`,c.`Keterangan`,a.`customerid`,b.`Nama`,a.`contactperson`,a.`note`,a.`status` FROM `deliveryorder` a 
		   INNER JOIN `customer` b ON a.`customerid`=b.`KdCustomer`
		   INNER JOIN gudang c ON a.`warehousecode`=c.`KdGudang` 
            WHERE 
            	1  
            	".$where_keyword."
            	".$where_gudang."
            	".$where_customer."    
            	".$where_status."                                   
            ORDER BY 
              a.dono DESC 
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			  a.`dono`,
			  DATE_FORMAT(a.`dodate`, '%d-%m-%Y') AS dodate,
			  a.`warehousecode`,
			  b.`Keterangan`,
			  a.`customerid`,
			  c.Nama,
			  c.AlamatKirim as Alamat,
			  a.`contactperson`,
			  a.`note`,
			  a.`status`,
			  a.adduser,
			  a.adddate,
			  a.edituser,
			  a.editdate 
			FROM
			  `deliveryorder` a 
			  INNER JOIN gudang b 
			    ON a.`warehousecode` = b.`KdGudang` 
			   INNER JOIN customer c
			    ON a.`customerid`=c.`KdCustomer`
			WHERE a.dono = '".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.* 
			FROM
			  deliveryorderdetail
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1  
			  AND deliveryorderdetail.dono = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a 
			INNER JOIN `masterbarang` b ON a.`inventorycode`=b.`PCode` 
			INNER JOIN `satuan` c ON a.`satuan`=c.`KdSatuan` 
			WHERE a.`dono`='".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  `deliveryorderdetail` a 
			  INNER JOIN masterbarang b
			  ON a.`inventorycode`=b.`PCode`
			  INNER JOIN satuan c
			  ON a.satuan = c.KdSatuan
			WHERE 1 
			  AND a.`dono` = '".$id."' 
			ORDER BY a.`sid` ASC ;
		";
		
        return $this->getArrayResult($sql);
	}
    
    
    function getCustomer()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>