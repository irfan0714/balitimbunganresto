<?php
class AbsensiModel extends CI_Model {
	
    function __construct() {
        parent::__construct();
    }
 
    function viewData($tgldari, $tglsampai) {
		$masterabsen_arr	= $this->_getMasterAbsensi($tgldari, $tglsampai);
		$absendatang_data	= $this->_getAbsensiDatang($tgldari, $tglsampai);
		$absenkeluar_data	= $this->_getAbsensiKeluar($tgldari, $tglsampai);
		
		$retval				= array();
		foreach ($masterabsen_arr as $masterabsen) 
		{
			$noabsen	= $masterabsen['NoAbsen'];
			$nik		= $masterabsen['employee_nik'];
			$nama		= $masterabsen['NamaKaryawan'];
			$tgl		= $masterabsen['Tanggal'];
			$timein		= $absendatang_data[$noabsen][$tgl];
			$timeout	= ($timein==$absenkeluar_data[$noabsen][$tgl])?' - ':$absenkeluar_data[$noabsen][$tgl];
			
			list($jam,$menit,$detik)	= explode(":",$timein);
			if( ($jam>12) AND ($timeout==' - ') ) 
			{
				$timeout	= $timein;
				$timein		= ' - ';
			}
			
			$retval[]	= array(
							'NoAbsen'		=> $noabsen,
							'employee_nik'	=> $nik,
							'NamaKaryawan'	=> $nama,
							'Tanggal'		=> $tgl,
							'TimeIn'		=> $timein,
							'TimeOut'		=> $timeout
						);
		}
		return $retval;
    }
	
	
	
	
	function _getAbsensiKeluar($tgldari,$tglsampai) {
		$retval		= array();		
		$sql1		= "SELECT 
							NoAbsen, 
							NamaKaryawan, 
							DATE(MAX(PresensiTime)) AS Tanggal, 
							TIME(MAX(PresensiTime)) AS TimeOut
						FROM 
							absensitrn
						WHERE DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
						 GROUP BY  
							NoAbsen, DATE(PresensiTime) 
						ORDER BY 
							NamaKaryawan, PresensiTime";
		
        $query1		= $this->db->query($sql1);
        $result_arr	= $query1->result_array();
		
		foreach ($result_arr as $result) {
			$noabsen	= $result['NoAbsen'];
			$tgl		= $result['Tanggal'];
			$timeout	= $result['TimeOut'];
			
			$retval[$noabsen][$tgl]	= $timeout;
		}		
        return $retval; 
	}
	
	function _getAbsensiDatang($tgldari,$tglsampai) {
		$retval		= array();		
		$sql1		= "SELECT 
							NoAbsen, 
							NamaKaryawan, 
							DATE(MIN(PresensiTime)) AS Tanggal, 
							TIME(MIN(PresensiTime)) AS TimeIn
						FROM 
							absensitrn
						WHERE DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
						 GROUP BY  
							NoAbsen, DATE(PresensiTime) 
						ORDER BY 
							NamaKaryawan, PresensiTime";
		
        $query1		= $this->db->query($sql1);
        $result_arr	= $query1->result_array();
		
		foreach ($result_arr as $result) {
			$noabsen	= $result['NoAbsen'];
			$tgl		= $result['Tanggal'];
			$timein		= $result['TimeIn'];
			
			$retval[$noabsen][$tgl]	= $timein;
		}		
        return $retval; 
	}
	
	
	
	/*function _getMasterAbsensi($tgldari,$tglsampai) 
	{
		$sql1 = "SELECT 
					NoAbsen, NamaKaryawan, DATE(PresensiTime) AS Tanggal
				FROM 
					absensitrn
				WHERE DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY  
					NoAbsen, DATE(PresensiTime) 
				ORDER BY 
					DATE(PresensiTime), NamaKaryawan";
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        return $result1; 
	}*/
	
	function _getMasterAbsensi($tgldari,$tglsampai) 
	{
		$sql1 = "SELECT 
			employee_nik, NamaKaryawan, DATE(PresensiTime) AS Tanggal, absensiemployee.NoAbsen
				FROM absensiemployee 
				JOIN employee 
				ON employee.`employee_id` = absensiemployee.`employee_id`
				JOIN absensitrn ON absensitrn.`NoAbsen` = absensiemployee.`NoAbsen`
				WHERE DATE(PresensiTime) BETWEEN '$tgldari' AND '$tglsampai'
				GROUP BY  
					NoAbsen, DATE(PresensiTime) 
				ORDER BY 
					DATE(PresensiTime), NamaKaryawan";
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        return $result1; 
	}
    
}