<?php
class Sales_return_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}

	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }
    
    function getWarehouse()
	{
    	$sql = "SELECT a.warehousecode,a.warehousename FROM warehouse a ORDER BY a.warehousecode ASC";
		return $this->getArrayResult($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
	
	function getSubdivisi()
	{
    	$sql = "SELECT a.KdSubdivisi,a.NamaSubDivisi FROM subdivisi a ORDER BY a.NamaSubDivisi ASC";
    	return $this->getArrayResult($sql);
    }
    
    function num_delivery_order_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT a.`dono`,a.`dodate`,a.`warehousecode`,a.`customerid`,a.`contactperson`,a.`note`,a.`status` FROM `deliveryorder` a;       
		";
		                  
        return $this->NumResult($sql);
	}
		
	function getDeliveryOrderList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.returnno";
				$arr_keyword[1] = "b.Keterangan";
				$arr_keyword[2] = "c.Nama";
				$arr_keyword[3] = "a.status";
				$arr_keyword[4] = "a.note";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT 
			  a.*,
			  b.*,
			  c.*,
			  DATE_FORMAT(a.`returndate`, '%d-%m-%Y') AS returndate_indo
			FROM
			  `salesreturn` a 
			  INNER JOIN gudang b 
			    ON a.`warehousecode` = b.`KdGudang` 
			  INNER JOIN customer c 
			    ON a.`customerid` = c.`KdCustomer` 
			WHERE 1
			  
            	".$where_keyword."
            	".$where_gudang."
            	".$where_customer."    
            	".$where_status."                                   
            ORDER BY 
              a.returnno DESC 
            Limit 
              $offset,$limit
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			  a.*,
			  b.*,
			  c.*,
			  d.`cnno`,
			  d.`returnno`,
			  e.`cndid`,
			  e.`KdSubdivisi`,
			  DATE_FORMAT(a.`returndate`, '%d-%m-%Y') AS returndate_indo
			FROM
			  `salesreturn` a 
			  INNER JOIN gudang b 
			    ON a.`warehousecode` = b.`KdGudang` 
			  INNER JOIN customer c 
			    ON a.`customerid` = c.`KdCustomer`
			  INNER JOIN `creditnote` d 
			    ON a.`returnno` = d.`returnno` 
			  INNER JOIN `creditnotedtl` e 
			    ON d.`cnno` = e.`cnno` 
			WHERE a.`returnno` = '".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.* 
			FROM
			  deliveryorderdetail
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '".$pcode."' 
			  AND deliveryorderdetail.dono = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  salesreturndetail INNER JOIN
			  salesreturn ON salesreturn.`returnno` = salesreturndetail.`returnno`
			WHERE 1 
			  AND salesreturndetail.inventorycode = '".$pcode."' 
			  AND salesreturndetail.returnno = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  salesreturndetail INNER JOIN
			  salesreturn ON salesreturn.`returnno` = salesreturndetail.`returnno`
			WHERE 1  
			  AND salesreturndetail.returnno = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetCnnoDetail($returnno,$pcode)
	{
		$sql = "
		SELECT * FROM `creditnotedtl` a WHERE a.`cnno`='".$returnno."' AND a.`description` LIKE '%".$pcode."%';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetCnno($returnno)
	{
	
		$sql = "
		SELECT * FROM `creditnote` a WHERE a.`returnno`='".$returnno."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($id)
	{
		$sql = "
			SELECT * FROM `salesreturndetail` a 
			INNER JOIN `masterbarang` b ON a.`inventorycode`=b.`PCode` 
			INNER JOIN satuan c ON c.KdSatuan = a.satuan WHERE a.`returnno`='".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	 function cekDataBarang($pcode,$v_customer)
	{
		$sql = "SELECT gd.Harga AS Harga1c, b.`KdRekeningRetur` 
				FROM `groupharga_header` gh INNER JOIN `groupharga_detail` gd ON gh.`GroupHargaID`=gd.`GroupHargaID`
   				INNER JOIN customer c ON c.`KdGroupharga`= gh.GroupHargaID AND c.`KdCustomer`='$v_customer'
   				INNER JOIN `masterbarang` a ON gd.`PCode`=a.`PCode`
  				INNER JOIN `divisi` b ON a.`KdDivisi` = b.`KdDivisi` 
				WHERE a.`PCode` = '$pcode'" ;
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekDataCreditNoteDetail($v_cnno)
	{
		$sql = "
			SELECT 
			  ROUND(SUM(a.value),0) AS amount,
			  ROUND((10/100)*SUM(a.value),0) AS potongan_ppn,
			  ROUND(SUM(a.value) + (10/100)*SUM(a.value),0) AS grandtotal   
			FROM
			  `creditnotedtl` a 
			WHERE a.`cnno` = '".$v_cnno."' ;
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  `salesreturndetail` a 
			  INNER JOIN masterbarang b 
				ON a.`inventorycode` = b.`PCode`
			   INNER JOIN satuan c
			    ON a.`satuan`=c.`KdSatuan`
			WHERE 1 
			  AND a.`returnno` = '".$id."' 
			ORDER BY a.`returndetailid` ASC ;
		";
        return $this->getArrayResult($sql);
	}
    
    
    function getCustomer()
	{
    	$sql = "SELECT a.KdCustomer,a.Nama FROM customer a ORDER BY a.Nama ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>