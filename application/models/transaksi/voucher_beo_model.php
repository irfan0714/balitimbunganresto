<?php
class Voucher_beo_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getKasBank()
	{
		$sql = "SELECT KdKasBank, NamaKasBank FROM kasbank ORDER BY NamaKasBank";
    	return $this->getArrayResult($sql);
    }
    
    function getBeo()
	{
		$sql = "
				SELECT 
				  a.NoDokumen 
				FROM
				  trans_reservasi a 
				WHERE 1 
				  AND MONTH(a.`TglDokumen`) = '".date('m')."' 
				  AND YEAR(a.`TglDokumen`) = '".date('Y')."' 
				  AND SUBSTR(a.NoDokumen, 1, 3) = 'SGV' 
				ORDER BY a.`NoDokumen` ASC ;
			   ";
    	return $this->getArrayResult($sql);
    }
    
    function getTourtravel()
	{
		$sql = "SELECT * FROM tourtravel ORDER BY Nama";
    	return $this->getArrayResult($sql);
    }
    
    function getKodeBank($kd){

    	$sql = "SELECT KdRekening, KdPenerimaan, KdSubDivisi FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    
    function getNewNo($tgl)
	{
	    $tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoReceipt=NoReceipt+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoReceipt FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	
	function getVoucherBeoList($key,$stat,$offset,$limit)
	{
       $mylib = new globallib();
       
        if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
	 	$where_keyword="";
        $where_status="";
        
			if($key!="" OR $key!="0")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.expDate";
				$arr_keyword[1] = "b.Nama";
				$arr_keyword[2] = "a.Keterangan";
				$arr_keyword[3] = "a.NoBukti";
		        
				$search_keyword = $mylib->search_keyword($key, $arr_keyword);
				$where_keyword = $search_keyword;
			}
						
			if($stat!="")
			{
				$where_status = " AND a.status = '".$stat."'";	
			}
        
    	$sql = "  
                                              
	           SELECT 
				  * 
			   FROM
				  `voucher_beo` a 
				  LEFT JOIN `tourtravel` b 
				    on a.`tourtravel` = b.`KdTravel`
				WHERE 1 AND a.Tampil='Y'
				".$where_keyword."
				".$where_status."
				 ORDER BY a.id DESC
	            Limit 
              $offset,$limit
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
	
	public function get_by_id($id) {
        $sql = "
        		SELECT 
				  a.id,
				  DATE_FORMAT(a.`expDate`, '%d-%m-%Y') AS expDate,
				  a.tourtravel,
				  ROUND(a.`nilai`) AS nilai,
				  a.no_voucher,
				  a.no_voucher_travel,
				  a.`NoBukti`,
				  a.`Keterangan`,
				  a.no_receipt,
				  a.BEO
				FROM
				  `voucher_beo` a 
				  INNER JOIN `tourtravel` b 
				    ON a.`tourtravel` = b.`KdTravel` 
				WHERE a.id = '$id' 
        		";
        //echo $sql;
        return $this->getRow($sql);
    }
    
    function getHeader($id)
	{
		$sql = "
			SELECT * FROM `voucher_beo` a INNER JOIN `tourtravel` b ON a.`tourtravel` = b.`KdTravel` WHERE a.`id`='$id';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}

	function updateFlag($id)
	{
		$sql = "
			UPDATE `voucher_beo` SET `push`=1  WHERE `id`='$id';
        ";
		//echo $sql;die;
        // return $this->getRow($sql);
        $this->db->query($sql);

	}

	function updatediVoucher($id,$nilai)
	{
		$sql = "
			UPDATE `voucher` SET `terpakai`='$nilai'  WHERE `novoucher`='$id';
        ";
		//echo $sql;die;
        // return $this->getRow($sql);
        $this->db->query($sql);

	}
	
	function getDetail_cetak($id)
	{
		$sql = "
			SELECT * FROM `voucher_beo` a INNER JOIN `tourtravel` b ON a.`tourtravel` = b.`KdTravel` WHERE a.`id`='$id';
		";
		
        return $this->getArrayResult($sql);
	}
    
    
    function num_voucher_beo_row()
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM voucher_beo a;       
		";
		                  
        return $this->NumResult($sql);
	}
	
    function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>