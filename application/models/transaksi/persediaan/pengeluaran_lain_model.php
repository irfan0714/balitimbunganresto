<?php
class Pengeluaran_lain_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}
	
	function getHarga($pcode)
	{
		$sql = "
				SELECT a.harga1c FROM masterbarang a WHERE a.`PCode`='".$pcode."';
               "; 
			   
        return $this->getRow($sql);
	}
	
	function cekPCode($pcode,$v_no_dokumen)
	{
		$sql = "
				SELECT * FROM mutasi a WHERE a.`NoTransaksi`='$v_no_dokumen' AND a.`KodeBarang`='$pcode';
               "; 
			   
        return $this->getRow($sql);
	}
	
	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }
	
	function getStock_plus($pcode,$gudang,$fieldakhir,$tgl_awal,$tgl_skrg)
	{
    	$sql = "
    			SELECT SUM(cek.Qty) AS Qty FROM ( SELECT 
				  SUM(Qty) AS Qty 
				FROM
				  mutasi 
				WHERE Tanggal BETWEEN '".$tgl_awal."' 
				  AND '".$tgl_skrg."'
				  AND Gudang = '".$gudang."' 
				  AND Jenis = 'I' 
				  AND KodeBarang = '".$pcode."'
				   
				UNION
				 
				SELECT 
				  ".$fieldakhir." AS Qty 
				FROM
				  stock 
				WHERE Tahun = '".date('Y')."' 
				  AND KdGudang = '".$gudang."' 
				  AND PCode = '".$pcode."' ) AS cek ;
    		   ";
    	//echo $sql;die;
        return $this->getRow($sql);
    }
	
	function getStock_min($pcode,$gudang,$tgl_awal,$tgl_skrg)
	{
    	$sql = "
    			SELECT 
				  SUM(Qty) AS Qty 
				FROM
				  mutasi 
				WHERE Tanggal BETWEEN '".$tgl_awal."' 
				  AND '".$tgl_skrg."'
				  AND Gudang = '".$gudang."' 
				  AND Jenis = 'O' 
				  AND KodeBarang = '".$pcode."'
    		   ";
        return $this->getRow($sql);
    }
	
	function ambilDetail($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_pengeluaran_lain_detail INNER JOIN
			  trans_pengeluaran_lain ON trans_pengeluaran_lain.`NoDokumen` = trans_pengeluaran_lain_detail.`NoDokumen`
			WHERE 1  
			  AND trans_pengeluaran_lain_detail.NoDokumen = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function ambilQty($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_pengeluaran_lain_detail INNER JOIN
			  trans_pengeluaran_lain ON trans_pengeluaran_lain.`NoDokumen` = trans_pengeluaran_lain_detail.`NoDokumen`
			WHERE 1 
			  AND trans_pengeluaran_lain_detail.PCode = '".$pcode."' 
			  AND trans_pengeluaran_lain_detail.NoDokumen = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$fieldupdate.",a.".$fieldnupdate.",a.".$fieldakhir.",a.".$fieldnakhir." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$KdGudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_pengeluaran_lain.NoDokumen";
				$arr_keyword[1] = "trans_pengeluaran_lain.Keterangan";
				$arr_keyword[2] = "intmutpurpose.purpose";
				$arr_keyword[3] = "intmutpurpose.purpose";
				$arr_keyword[4] = "gudang.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_pengeluaran_lain.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_pengeluaran_lain.Status = '".$arrSearch["status"]."'";	
			}
		}
        
    	$sql = "  
    		SELECT 
			  trans_pengeluaran_lain.NoDokumen,
			  DATE_FORMAT(trans_pengeluaran_lain.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_pengeluaran_lain.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_pengeluaran_lain.PurposeId,
			  intmutpurpose.purpose,
			  trans_pengeluaran_lain.Keterangan,
			  DATE_FORMAT(trans_pengeluaran_lain.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_pengeluaran_lain.AddUser,
			  trans_pengeluaran_lain.EditDate,
			  trans_pengeluaran_lain.EditUser,
			  trans_pengeluaran_lain.Status  
			FROM
			  trans_pengeluaran_lain 
			  LEFT JOIN gudang 
			    ON trans_pengeluaran_lain.KdGudang = gudang.KdGudang 
			  LEFT JOIN intmutpurpose 
			    ON trans_pengeluaran_lain.PurposeId = intmutpurpose.purposeid 
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_status."  
			ORDER BY 
			  trans_pengeluaran_lain.TglDokumen DESC,
			  trans_pengeluaran_lain.NoDokumen DESC 
			LIMIT
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_tabel_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_pengeluaran_lain.NoDokumen";
				$arr_keyword[1] = "trans_pengeluaran_lain.Keterangan";
				$arr_keyword[2] = "intmutpurpose.purpose";
				$arr_keyword[3] = "intmutpurpose.purpose";
				$arr_keyword[4] = "gudang.Keterangan";
				
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_pengeluaran_lain.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_pengeluaran_lain.Status = '".$arrSearch["status"]."'";	
			}
		}
		
		$sql = "
    		SELECT 
			  trans_pengeluaran_lain.NoDokumen,
			  DATE_FORMAT(trans_pengeluaran_lain.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_pengeluaran_lain.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_pengeluaran_lain.PurposeId,
			  intmutpurpose.purpose,
			  trans_pengeluaran_lain.Keterangan,
			  DATE_FORMAT(trans_pengeluaran_lain.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_pengeluaran_lain.AddUser,
			  trans_pengeluaran_lain.EditDate,
			  trans_pengeluaran_lain.EditUser,
			  trans_pengeluaran_lain.Status  
			FROM
			  trans_pengeluaran_lain 
			  LEFT JOIN gudang 
			    ON trans_pengeluaran_lain.KdGudang = gudang.KdGudang 
			  LEFT JOIN intmutpurpose 
			    ON trans_pengeluaran_lain.PurposeId = intmutpurpose.purposeid 
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang." 
            	".$where_status."  
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getIntMut()
	{
    	$sql = "SELECT purposeid,purpose FROM intmutpurpose WHERE 1 AND intmutpurpose.active = '1' ORDER BY intmutpurpose.purpose ASC";
		return $this->getArrayResult($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
		return $this->getArrayResult($sql);
    }
    
    function getTglTerakhirSO($gudang)
	{
    	$sql = "SELECT MAX(a.`TglDokumen`) AS Tgl_SO_Terakhir FROM opname_header a WHERE a.`KdGudang`='$gudang' AND a.`Status` ='1'";
        return $this->getRow($sql);
    }
	
	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

	function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }

	function getHeader($id)
	{
		$sql = "
			SELECT 
			  trans_pengeluaran_lain.NoDokumen,
			  DATE_FORMAT(trans_pengeluaran_lain.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_pengeluaran_lain.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_pengeluaran_lain.PurposeId,
			  intmutpurpose.purpose,
			  trans_pengeluaran_lain.Keterangan,
			  trans_pengeluaran_lain.Status,
			  DATE_FORMAT(trans_pengeluaran_lain.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_pengeluaran_lain.AddUser,
			  trans_pengeluaran_lain.EditDate,
			  trans_pengeluaran_lain.EditUser,
			  trans_pengeluaran_lain.Proposal
			FROM
			  trans_pengeluaran_lain 
			  LEFT JOIN gudang 
			    ON trans_pengeluaran_lain.KdGudang = gudang.KdGudang 
			  LEFT JOIN intmutpurpose 
			    ON trans_pengeluaran_lain.PurposeId = intmutpurpose.purposeid 
			WHERE 1 
			  AND trans_pengeluaran_lain.NoDokumen = '".$id."' 
			LIMIT 0, 01
        ";
        
        return $this->getRow($sql);
	}

	function getDetail($id)
	{
		$sql = "
			SELECT 
			  trans_pengeluaran_lain_detail.*,
			  satuan.NamaSatuan
			FROM
			  trans_pengeluaran_lain_detail INNER JOIN satuan
			ON trans_pengeluaran_lain_detail.Satuan = satuan.KdSatuan
			WHERE 1 
			  AND trans_pengeluaran_lain_detail.NoDokumen = '$id' 
			ORDER BY 
			  trans_pengeluaran_lain_detail.sid DESC
		";
		
        return $this->getArrayResult($sql);
	}

	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  trans_pengeluaran_lain_detail.* 
			FROM
			  trans_pengeluaran_lain_detail 
			WHERE 1 
			  AND trans_pengeluaran_lain_detail.PCode = '$pcode' 
			  AND trans_pengeluaran_lain_detail.NoDokumen = '$nodok' 
			LIMIT 1
		";
		
        return $this->getRow($sql);
	}
    
    function get_mutasi_stock($no_dokumen)
    {
        $q = "
                DELETE FROM
                    mutasi
                WHERE
                    1
                    AND mutasi.KdTransaksi = 'PL'
                    AND mutasi.NoTransaksi = '".$no_dokumen."'
        ";   
        mysql_query($q);   
        
        $q = "
                SELECT
                    trans_pengeluaran_lain.NoDokumen,
                    trans_pengeluaran_lain.TglDokumen,
                    trans_pengeluaran_lain.KdGudang,
                    trans_pengeluaran_lain_detail.PCode,
                    trans_pengeluaran_lain_detail.Qty
                    
                FROM
                    trans_pengeluaran_lain
                    INNER JOIN trans_pengeluaran_lain_detail ON
                        trans_pengeluaran_lain.NoDokumen = trans_pengeluaran_lain_detail.NoDokumen
                        -- AND trans_pengeluaran_lain.Status = '1'
                        AND trans_pengeluaran_lain.NoDokumen = '".$no_dokumen."'
                WHERE
                    1
                ORDER BY
                    trans_pengeluaran_lain_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang, $PCode, $Qty) = $row;
            
            $q_ins = "
                    INSERT INTO `mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = 'O',
                      `KdTransaksi` = 'PL',
                      `Gudang` = '".$KdGudang."',
                      `GudangTujuan` = '',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".$Qty."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            mysql_query($q_ins);
           
        }
    }
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>