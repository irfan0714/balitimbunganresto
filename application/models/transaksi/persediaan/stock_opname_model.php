<?php
class Stock_opname_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
	
	function getOpnameDetail($id)
	{
    	$sql = "
				SELECT
                    opname_detail.Sid,    
                    opname_detail.PCode,
                    opname_detail.QtyFisik,
                    opname_detail.QtyProgram
                FROM
                    opname_detail
                WHERE
                    1
                    AND opname_detail.NoDokumen = '".$id."'
                ORDER BY
                    opname_detail.Sid ASC
				";
		
		return $this->getArrayResult($sql);
    }
	
	function getStocks($gudang)
	{
    	$sql = "
				SELECT 
				  a.`PCode`,
				  b.`NamaLengkap`,
				  b.`SatuanSt`,
				  b.`KdSubKategori`,
				  c.`NamaSubKategori` 
				FROM
				  stock a 
				  INNER JOIN masterbarang b 
					ON a.`PCode` = b.`PCode` 
				  INNER JOIN subkategori c 
					ON b.`KdSubKategori` = c.`KdSubKategori` 
				WHERE a.`KdGudang` = '".$gudang."'
				  AND b.Konsinyasi = 0
				  AND a.`Tahun` = '".date('Y')."' ;
				";
		
		return $this->getArrayResult($sql);
    }
	
	
    function getWarehouse()
	{
    	$sql = "SELECT a.warehousecode,a.warehousename FROM warehouse a ORDER BY a.warehousecode ASC";
		return $this->getArrayResult($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function cekOtorisasi($user)
	{
		$sql = "
					SELECT * FROM otorisasi_user a WHERE a.Tipe='stock_opname' AND a.UserName='$user';
				";     
        return $this->getRow($sql);
	}
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}
	
	function getStock($tahun,$v_warehouse,$PCode1,$bulan)
	{
		$masuk  = "GMasuk".$bulan;
		$keluar = "GKeluar".$bulan;
		$awal = "GKeluar".$bulan;
		$akhir = "GAkhir".$bulan;
		
		$sql = "SELECT $awal as awal, $masuk as masuk, $keluar as keluar, $akhir as akhir FROM `stock` 
				WHERE `Tahun`='$tahun' AND `KdGudang`='$v_warehouse' AND `PCode`='$PCode1' ";     
        return $this->getArrayResult($sql);
	}
	
	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }	
    
    
    function num_stock_opname_row($arrSearch)
    {
        $mylib = new globallib();
       
        $where_keyword="";
        $where_gudang="";
        $wheretgl = "";;
		
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND opname_header.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			$wheretgl = "AND opname_header.TglDokumen between '".$arrSearch["tgl1"]."' and '".$arrSearch["tgl2"]."'";	
		} 
        
    	$sql = "  
            SELECT 
			  opname_header.NoDokumen,
			  DATE_FORMAT(opname_header.TglDokumen, '%d-%m-%Y') AS TglDokumen,
			  gudang.KdGudang,
			  gudang.Keterangan AS NamaGudang,
			  opname_header.Status,
			  opname_header.Approval_By,
			  opname_header.Approval_Date,
			  opname_header.Approval_Status,
			  opname_header.Approval_Remarks 
			FROM
			  opname_header 
			  INNER JOIN gudang 
			    ON opname_header.KdGudang = gudang.KdGudang 
			WHERE 1 $wheretgl $where_gudang
			  ORDER BY opname_header.TglDokumen DESC,
			  opname_header.NoDokumen ASC 
        ";               
		                  
        return $this->NumResult($sql);
	}
		
	function getStockOpnameList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $wheretgl = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND opname_header.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			$wheretgl = "AND opname_header.TglDokumen between '".$arrSearch["tgl1"]."' and '".$arrSearch["tgl2"]."'";	
		} 
        
    	$sql = "  
            SELECT 
			  opname_header.NoDokumen,
			  DATE_FORMAT(opname_header.TglDokumen, '%d-%m-%Y') AS TglDokumen,
			  gudang.KdGudang,
			  gudang.Keterangan AS NamaGudang,
			  opname_header.Status,
			  opname_header.Approval_By,
			  opname_header.Approval_Date,
			  opname_header.Approval_Status,
			  opname_header.Approval_Remarks 
			FROM
			  opname_header 
			  INNER JOIN gudang 
			    ON opname_header.KdGudang = gudang.KdGudang 
			WHERE 1 $wheretgl $where_gudang
			ORDER BY opname_header.TglDokumen DESC,
			  opname_header.NoDokumen ASC 
            Limit 
              $offset,$limit
        ";   
        /*            
        echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT
                    opname_header.*,
                    opname_header.TglDokumen AS dates,
                    DATE_FORMAT(opname_header.TglDokumen, '%d-%m-%Y') AS TglDokumen
                FROM 
                   opname_header
                WHERE
                    1
                    AND opname_header.NoDokumen = '".$id."'
                LIMIT
                    0,1
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.* 
			FROM
			  deliveryorderdetail
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1  
			  AND deliveryorderdetail.dono = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($gudang)
	{
		$sql = "
			SELECT 
			  a.`PCode`,
			  b.`NamaLengkap`,
			  b.`SatuanSt`,
			  b.`KdSubKategori`,
			  c.`NamaSubKategori` 
			FROM
			  stock a 
			  INNER JOIN masterbarang b 
			    ON a.`PCode` = b.`PCode` 
			  INNER JOIN subkategori c 
			    ON b.`KdSubKategori` = c.`KdSubKategori` 
			WHERE a.`KdGudang` = '".$gudang."' 
			  AND b.Konsinyasi=0
			  AND a.`Tahun` = '".date('Y')."' ;
					";
		//echo $sql;
        return $this->getArrayResult($sql);
	}
	
	
	function getDetailListGabungSo($gudang,$bln,$thn)
	{
		$sql = "
			SELECT 
			  b.`NoDokumen`,
			  b.`PCode`,
			  SUM(b.`QtyFisik`) AS QtyFisik ,
			  b.QtyProgram
			FROM
			  `opname_header_split` a 
			  INNER JOIN `opname_detail_split` b 
			    ON a.`NoDokumen` = b.`NoDokumen` 
			WHERE a.`KdGudang` = '$gudang' 
			  AND MONTH(a.`TglDokumen`) = '$bln'
			  AND YEAR(a.`TglDokumen`) = '$thn'
			GROUP BY b.`PCode` ;
					";
		//echo $sql;
        return $this->getArrayResult($sql);
	}
	
	
	function getDetailList2($id)
	{
		$sql = "
			SELECT 
			  a.`PCode`,
			  b.`NamaLengkap`,
			  a.`QtyFisik`,
			  b.`SatuanSt`,
			  a.`QtyProgram`,
			  b.`KdSubKategori`,
			  c.`NamaSubKategori` 
			FROM
			  opname_detail a 
			  INNER JOIN masterbarang b 
			    ON a.`PCode` = b.`PCode` 
			  INNER JOIN subkategori c 
			    ON b.`KdSubKategori` = c.`KdSubKategori` 
			WHERE a.NoDokumen='".$id."' ;
					";
		//echo $sql;
        return $this->getArrayResult($sql);
	}
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  `deliveryorderdetail` a 
			  INNER JOIN masterbarang b
			  ON a.`inventorycode`=b.`PCode`
			  INNER JOIN satuan c
			  ON a.satuan = c.KdSatuan
			WHERE 1 
			  AND a.`dono` = '".$id."' 
			ORDER BY a.`sid` ASC ;
		";
		
        return $this->getArrayResult($sql);
	}
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>