<?php
class Request_return_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_gudang_admin = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_request_retur_header.NoDokumen";
				$arr_keyword[1] = "trans_request_retur_header.Keterangan";
				$arr_keyword[2] = "trans_request_retur_header.RgNo";
				$arr_keyword[3] = "gudang.Keterangan";
				$arr_keyword[4] = "supplier.nama";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_request_retur_header.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "trans_request_retur_header.KdGudang", "in");
			}
		}
        
    	$sql = "  
    		SELECT 
    		  DATE_FORMAT(trans_request_retur_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_request_retur_header.NoDokumen,
			  trans_request_retur_header.KdSupplier,
			  supplier.Nama AS suppliername,
			  trans_request_retur_header.RgNo,
			  trans_request_retur_header.Keterangan,
			  trans_request_retur_header.KdGudang,
			  gudang.Keterangan AS gudang_nama,
			  trans_request_retur_header.Status,
			  trans_request_retur_header.Approval_By,
			  DATE_FORMAT(trans_request_retur_header.Approval_Date,'%d-%m-%Y') AS Approval_Date,
			  trans_request_retur_header.Approval_Remarks,
			  trans_request_retur_header.AddUser,
			  DATE_FORMAT(trans_request_retur_header.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_request_retur_header.EditUser,
			  trans_request_retur_header.EditDate,
			  trans_request_retur_header.Approval_Status 
			FROM
			  trans_request_retur_header 
			  INNER JOIN trans_terima_header 
			    ON trans_request_retur_header.RgNo = trans_terima_header.NoDokumen 
			  INNER JOIN supplier 
			    ON trans_request_retur_header.KdSupplier = supplier.KdSupplier 
			  INNER JOIN gudang 
			    ON trans_request_retur_header.KdGudang = gudang.KdGudang 
			WHERE 
			  1 
              ".$where_keyword."
              ".$where_gudang."
              ".$where_gudang_admin." 
			ORDER BY 
			  trans_request_retur_header.TglDokumen DESC,
			  trans_request_retur_header.NoDokumen DESC
			LIMIT
              $offset,$limit
        ";               
        //echo $sql;
		return $this->getArrayResult($sql);
    }
    
    function num_tabel_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_gudang_admin = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_request_retur_header.NoDokumen";
				$arr_keyword[1] = "trans_request_retur_header.Keterangan";
				$arr_keyword[2] = "trans_request_retur_header.RgNo";
				$arr_keyword[3] = "gudang.Keterangan";
				$arr_keyword[4] = "supplier.nama";
				
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_request_retur_header.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "trans_request_retur_header.KdGudang", "in");
			}
		}
		
		$sql = "
			SELECT 
    		  DATE_FORMAT(trans_request_retur_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_request_retur_header.TglDokumen,
			  trans_request_retur_header.NoDokumen,
			  trans_request_retur_header.KdSupplier,
			  supplier.Nama,
			  trans_request_retur_header.RgNo,
			  trans_request_retur_header.Keterangan,
			  trans_request_retur_header.KdGudang,
			  gudang.Keterangan,
			  trans_request_retur_header.Status,
			  trans_request_retur_header.Approval_By,
			  DATE_FORMAT(trans_request_retur_header.Approval_Date,'%d-%m-%Y') AS Approval_Date,
			  trans_request_retur_header.Approval_Remarks,
			  trans_request_retur_header.AddUser,
			  DATE_FORMAT(trans_request_retur_header.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_request_retur_header.EditUser,
			  trans_request_retur_header.EditDate,
			  trans_request_retur_header.Approval_Status 
			FROM
			  trans_request_retur_header 
			  INNER JOIN trans_terima_header 
			    ON trans_request_retur_header.RgNo = trans_terima_header.NoDokumen 
			  INNER JOIN supplier 
			    ON trans_request_retur_header.KdSupplier = supplier.KdSupplier 
			  INNER JOIN gudang 
			    ON trans_request_retur_header.KdGudang = gudang.KdGudang 
			WHERE 
			  1 
              ".$where_keyword."
              ".$where_gudang."
              ".$where_gudang_admin." 
		";
		                  
        return $this->NumResult($sql);
	}
	
	function get_mutasi_stock($no_dokumen)
    {
        //echo "dadad";
        $q = "
                DELETE FROM
                    mutasi
                WHERE
                    1
                    AND mutasi.KdTransaksi = 'RR'
                    AND mutasi.NoTransaksi = '".$no_dokumen."'
        ";   
        mysql_query($q);   
        
        $q = "
                SELECT
                    trans_request_retur_header.NoDokumen,
                    trans_request_retur_header.TglDokumen,
                    trans_request_retur_header.KdGudang,
                    trans_request_retur_detail.PCode,
                    trans_request_retur_detail.QtyReturn
                    
                FROM
                    trans_request_retur_header
                    INNER JOIN trans_request_retur_detail ON
                        trans_request_retur_header.NoDokumen = trans_request_retur_detail.NoDokumen
                        -- AND trans_request_retur_header.Status = '1'
                        AND trans_request_retur_header.NoDokumen = '".$no_dokumen."'
                WHERE
                    1
                ORDER BY
                    trans_request_retur_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang_From, $PCode, $Qty) = $row;
            
            $q_ins = "
                    INSERT INTO `mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = 'O',
                      `KdTransaksi` = 'RR',
                      `Gudang` = '".$KdGudang_From."',
                      `GudangTujuan` = '',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".$Qty."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            mysql_query($q_ins);
           
        }
        
        
    }
    
	function getGudang($arr_gudang="")
	{
		$mylib = new globallib();
		
		$where_gudang_admin="";
		if(count($arr_gudang)>0)
		{
			$where_gudang_admin = $mylib->where_array($arr_gudang, "gudang.KdGudang", "in");
		}
		
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang FROM gudang WHERE 1 ".$where_gudang_admin." order by KdGudang";
    	return $this->getArrayResult($sql);
    }
	
	function getSupplier()
	{
    	$sql = "
    		SELECT 
			  KdSupplier,
			  Nama,
			  Contact,
			  Alamat,
			  Kota,
			  Telepon,
			  AlamatKirim,
			  KotaKirim 
			FROM
			  supplier 
			WHERE 
			  1 
			ORDER BY 
			  supplier.KdSupplier DESC,
			  supplier.Nama ASC
    	";
    	return $this->getArrayResult($sql);
    }
	
	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

	function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }

	function getHeader($id)
	{
		$sql = "
			SELECT 
    		  DATE_FORMAT(trans_request_retur_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_request_retur_header.NoDokumen,
			  trans_request_retur_header.KdSupplier,
			  supplier.Nama AS suppliername,
			  trans_request_retur_header.RgNo,
			  trans_terima_header.PoNo,
			  trans_terima_header.NoSuratJalan,
			  trans_request_retur_header.Keterangan,
			  trans_request_retur_header.KdGudang,
			  gudang.Keterangan AS gudang_nama,
			  trans_request_retur_header.Status,
			  trans_request_retur_header.Approval_By,
			  trans_request_retur_header.Approval_Status,
			  DATE_FORMAT(trans_request_retur_header.Approval_Date,'%d-%m-%Y') AS Approval_Date,
			  trans_request_retur_header.Approval_Remarks,
			  trans_request_retur_header.AddUser,
			  DATE_FORMAT(trans_request_retur_header.AddDate,'%d-%m-%Y || %H:%i:%s') AS AddDate,
			  trans_request_retur_header.EditUser,
			  DATE_FORMAT(trans_request_retur_header.EditDate,'%d-%m-%Y || %H:%i:%s') AS EditDate
			FROM
			  trans_request_retur_header 
			  INNER JOIN trans_terima_header 
			    ON trans_request_retur_header.RgNo = trans_terima_header.NoDokumen 
			  INNER JOIN supplier 
			    ON trans_request_retur_header.KdSupplier = supplier.KdSupplier 
			  INNER JOIN gudang 
			    ON trans_request_retur_header.KdGudang = gudang.KdGudang 
			WHERE 
			  1 
              AND trans_request_retur_header.NoDokumen = '".$id."' 
			LIMIT
              0, 01
        ";
        return $this->getRow($sql);
	}

	function getDetail($id)
	{
		$sql = "
			SELECT 
			  trans_request_retur_detail.*,
			  masterbarang.NamaLengkap 
			FROM
			  trans_request_retur_detail 
			  INNER JOIN masterbarang 
			    ON trans_request_retur_detail.PCode = masterbarang.PCode 
			WHERE 1 
			  AND trans_request_retur_detail.NoDokumen = '".$id."' 
			ORDER BY 
			  trans_request_retur_detail.PCode ASC,
			  masterbarang.NamaLengkap ASC
		";
		
        return $this->getArrayResult($sql);
	}
	
	function getDetailUnion($nodok,$rgno)
	{
		$sql = "
			SELECT 
			  tb_1.*,
			  masterbarang.NamaLengkap,
			  trans_terima_detail.Harga,
			  trans_terima_detail.Disc1,
			  trans_terima_detail.Disc2,
			  trans_terima_detail.Potongan,
			  trans_terima_detail.Jumlah,
			  trans_terima_detail.PPn,
			  trans_terima_detail.Total 
			FROM
			  (SELECT 
			    trans_request_retur_detail.NoDokumen,
			    trans_request_retur_header.RgNo,
			    trans_request_retur_detail.PCode,
			    trans_request_retur_detail.Qty,
			    trans_request_retur_detail.QtyPcs,
			    trans_request_retur_detail.QtyReturn,
			    trans_request_retur_detail.Satuan,
			    trans_request_retur_detail.Keterangan 
			  FROM
			    trans_request_retur_detail 
			    INNER JOIN trans_request_retur_header 
			      ON trans_request_retur_detail.NoDokumen = trans_request_retur_header.NoDokumen 
			  WHERE 1 
			    AND trans_request_retur_detail.NoDokumen = '".$nodok."' 
			  UNION
			  SELECT 
			    trans_terima_detail.NoDokumen,
			    trans_terima_detail.NoDokumen AS RgNo,
			    trans_terima_detail.PCode,
			    trans_terima_detail.Qty,
			    trans_terima_detail.QtyPcs,
			    '0' AS QtyReturn,
			    trans_terima_detail.Satuan,
			    NULL AS Keterangan 
			  FROM
			    trans_terima_detail 
			  WHERE 1 
			    AND trans_terima_detail.NoDokumen = '".$rgno."') AS tb_1 
			  INNER JOIN masterbarang 
			    ON tb_1.PCode = masterbarang.PCode 
			  INNER JOIN trans_terima_detail 
			    ON tb_1.RgNo = trans_terima_detail.NoDokumen 
			    AND tb_1.PCode = trans_terima_detail.PCode 
			GROUP BY tb_1.PCode 
			ORDER BY tb_1.QtyReturn * 1 > 0 DESC,
			  tb_1.PCode ASC,
			  masterbarang.NamaLengkap ASC
		";
		return $this->getArrayResult($sql);		
	}

	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  trans_request_retur_detail.* 
			FROM
			  trans_request_retur_detail 
			WHERE 1 
			  AND trans_request_retur_detail.PCode = '$pcode' 
			  AND trans_request_retur_detail.NoDokumen = '$nodok' 
			LIMIT 1
		";
		
        return $this->getRow($sql);
	}
    
	function getDetailRg($id)
	{
		$sql="SELECT * FROM trans_terima_detail WHERE 1 AND NoDokumen = '".$id."' ORDER BY sid ASC";

		return $this->getArrayResult($sql);
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>