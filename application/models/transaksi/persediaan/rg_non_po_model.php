<?php
class Rg_non_po_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        	$arr_keyword[0] = "trans_terima_header.NoDokumen";    
	            $arr_keyword[1] = "trans_terima_header.PoNo";    
	            $arr_keyword[2] = "trans_terima_header.NoSuratJalan";    
	            $arr_keyword[3] = "trans_terima_header.Keterangan"; 
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = " AND trans_terima_header.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_terima_header.Status = '".$arrSearch["status"]."'";	
			}
		}
        
    	$sql = "  
    		SELECT 
				  trans_terima_header.NoDokumen,
				  trans_terima_header.TglDokumen,
          trans_terima_header.PoNo,
				  supplier.KdSupplier,
				  supplier.Nama AS NamaSupplier,
				  gudang.KdGudang,
				  gudang.Keterangan AS nama_gudang,
				  trans_terima_header.currencycode,
				  trans_terima_header.Keterangan,
				  trans_terima_header.Total,
				  trans_terima_header.status AS `Status`
				FROM
				  trans_terima_header 
				  INNER JOIN supplier 
				    ON trans_terima_header.KdSupplier = supplier.KdSupplier 
				  INNER JOIN gudang 
				    ON trans_terima_header.KdGudang = gudang.KdGudang 
				WHERE 1 AND trans_terima_header.PoNo=''
					$where_keyword
					$where_gudang
					$where_status
				ORDER BY 
					trans_terima_header.TglDokumen DESC,
					trans_terima_header.NoDokumen ASC 
				LIMIT
	              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_tabel_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_penerimaan_lain.NoDokumen";
				$arr_keyword[1] = "trans_penerimaan_lain.Keterangan";
				$arr_keyword[2] = "intmutpurpose.purpose";
				$arr_keyword[3] = "intmutpurpose.purpose";
				$arr_keyword[4] = "gudang.Keterangan";
				
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_penerimaan_lain.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_penerimaan_lain.Status = '".$arrSearch["status"]."'";	
			}
		}
		
		$sql = "
    		SELECT 
			  trans_penerimaan_lain.NoDokumen,
			  DATE_FORMAT(trans_penerimaan_lain.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_penerimaan_lain.KdGudang,
			  gudang.Keterangan AS nama_gudang,
			  trans_penerimaan_lain.PurposeId,
			  intmutpurpose.purpose,
			  trans_penerimaan_lain.Keterangan,
			  DATE_FORMAT(trans_penerimaan_lain.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_penerimaan_lain.AddUser,
			  trans_penerimaan_lain.EditDate,
			  trans_penerimaan_lain.EditUser,
			  trans_penerimaan_lain.Status  
			FROM
			  trans_penerimaan_lain 
			  LEFT JOIN gudang 
			    ON trans_penerimaan_lain.KdGudang = gudang.KdGudang 
			  LEFT JOIN intmutpurpose 
			    ON trans_penerimaan_lain.PurposeId = intmutpurpose.purposeid 
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang." 
            	".$where_status."  
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getIntMut()
	{
    	$sql = "SELECT purposeid,purpose FROM intmutpurpose WHERE 1 AND intmutpurpose.active = '1' ORDER BY intmutpurpose.purpose ASC";
		return $this->getArrayResult($sql);
		}
		
		function getsupplier()
	{
    	$sql = "SELECT KdSupplier,Nama FROM supplier ORDER BY Nama ASC";
		return $this->getArrayResult($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
		return $this->getArrayResult($sql);
		}
		
		function getSupplier1()
	{
    	$sql = "SELECT KdSupplier,Nama from supplier order by Nama";
		return $this->getArrayResult($sql);
    }

    function getProposal()
	{
    	$sql = "SELECT proposal.`NoProposal` FROM proposal;";
		return $this->getArrayResult($sql);
    }
	
	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
    
    function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }

    function getIntmutpurpose($tipe)
	{
    	$sql = "
    			SELECT a.`NeedProposal` FROM `intmutpurpose` a WHERE a.`purposeid`='".$tipe."';
    		   ";
        return $this->getRow($sql);
		}
		
		function getHargaq($pcode)
		{
			$sql = "SELECT Harga1c FROM masterbarang WHERE PCode = '$pcode' ";

			return $this->getRow($sql);
			
		}

	function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
    	return $this->getArrayResult($sql);
    }

	function getHeader($id)
	{
		$sql = "
			SELECT 
				trans_terima_header.NoDokumen,
				trans_terima_header.PPn,
				trans_terima_header.Jumlah,
				trans_terima_header.Total,
			  DATE_FORMAT(trans_terima_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_terima_header.KdGudang,
			  gudang.Keterangan AS nama_gudang,
				trans_terima_header.NoSuratJalan,
				trans_terima_header.KdSupplier,
			  trans_terima_header.Keterangan,
			  trans_terima_header.Status,
			  DATE_FORMAT(trans_terima_header.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_terima_header.AddUser,
			  trans_terima_header.EditDate,
			  trans_terima_header.EditUser
			FROM
			trans_terima_header 
			  LEFT JOIN gudang 
			    ON trans_terima_header.KdGudang = gudang.KdGudang 
			  
			WHERE 1 
			  AND trans_terima_header.NoDokumen = '".$id."' 
			LIMIT 0, 01
        ";
        
        return $this->getRow($sql);
	}
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}
	
	function getHarga($pcode)
	{
		$sql = "
				SELECT a.harga1c FROM masterbarang a WHERE a.`PCode`='".$pcode."';
               "; 
        return $this->getRow($sql);
	}
	
	function ambilQty($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_penerimaan_lain_detail INNER JOIN
			  trans_penerimaan_lain ON trans_penerimaan_lain.`NoDokumen` = trans_penerimaan_lain_detail.`NoDokumen`
			WHERE 1 
			  AND trans_penerimaan_lain_detail.PCode = '".$pcode."' 
			  AND trans_penerimaan_lain_detail.NoDokumen = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function ambilDetail($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_terima_detail INNER JOIN
			  trans_terima_header ON trans_terima_header.`NoDokumen` = trans_terima_detail.`NoDokumen`
			WHERE 1  
			  AND trans_terima_detail.NoDokumen = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$fieldupdate.",a.".$fieldnupdate.",a.".$fieldakhir.",a.".$fieldnakhir." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$KdGudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

	function getDetail($id)
	{
		$sql = "
			SELECT 
			  trans_terima_detail.*,
				satuan.NamaSatuan,
				masterbarang.NamaLengkap
			FROM
			trans_terima_detail INNER JOIN satuan
			ON trans_terima_detail.Satuan = satuan.KdSatuan INNER JOIN masterbarang
			ON trans_terima_detail.PCode = masterbarang.PCode
			WHERE 1 
			  AND trans_terima_detail.NoDokumen = '$id' 
			ORDER BY 
			trans_terima_detail.sid DESC
		";
		
        return $this->getArrayResult($sql);
	}

	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  trans_terima_detail.* 
			FROM
			  trans_terima_detail 
			WHERE 1 
			  AND trans_terima_detail.PCode = '$pcode' 
			  AND trans_terima_detail.NoDokumen = '$nodok' 
			LIMIT 1
		";
		
        return $this->getRow($sql);
	}
    
    function get_mutasi_stock($no_dokumen)
    {
        $q = "
                DELETE FROM
                    mutasi
                WHERE
                    1
                    AND mutasi.KdTransaksi = 'DL'
                    AND mutasi.NoTransaksi = '".$no_dokumen."'
        ";   
        mysql_query($q);   
        
        $q = "
                SELECT
                    trans_penerimaan_lain.NoDokumen,
                    trans_penerimaan_lain.TglDokumen,
                    trans_penerimaan_lain.KdGudang,
                    trans_penerimaan_lain_detail.PCode,
                    trans_penerimaan_lain_detail.Qty
                    
                FROM
                    trans_penerimaan_lain
                    INNER JOIN trans_penerimaan_lain_detail ON
                        trans_penerimaan_lain.NoDokumen = trans_penerimaan_lain_detail.NoDokumen
                        -- AND trans_penerimaan_lain.Status = '1'
                        AND trans_penerimaan_lain.NoDokumen = '".$no_dokumen."'
                WHERE
                    1
                ORDER BY
                    trans_penerimaan_lain_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang, $PCode, $Qty) = $row;
            
            $q_ins = "
                    INSERT INTO `mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = 'I',
                      `KdTransaksi` = 'DL',
                      `Gudang` = '".$KdGudang."',
                      `GudangTujuan` = '',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".$Qty."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            mysql_query($q_ins);
           
        }
    }
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>