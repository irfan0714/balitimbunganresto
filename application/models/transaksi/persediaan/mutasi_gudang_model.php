<?php
class Mutasi_gudang_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_gudang_admin = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_mutasi_header.NoDokumen";
				$arr_keyword[1] = "intmutpurpose.purpose";
				$arr_keyword[2] = "gudang_from.Keterangan";
				$arr_keyword[3] = "gudang_to.Keterangan";
				$arr_keyword[4] = "trans_mutasi_header.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_mutasi_header.KdGudang_From = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["tujuan"]!="")
			{
				$where_tujuan = "AND trans_mutasi_header.KdGudang_To = '".$arrSearch["tujuan"]."'";	
			}
			
			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "trans_mutasi_header.KdGudang_From", "in");
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_mutasi_header.Status = '".$arrSearch["status"]."'";	
			}
		}
        
    	$sql = "  
    		SELECT 
			  trans_mutasi_header.NoDokumen,
			  DATE_FORMAT(trans_mutasi_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_mutasi_header.PurposeId,
			  intmutpurpose.purpose,
			  trans_mutasi_header.KdGudang_From,
			  gudang_from.Keterangan AS nama_gudang_from,
			  trans_mutasi_header.KdGudang_To,
			  gudang_to.Keterangan AS nama_gudang_to,
			  trans_mutasi_header.MovingConfirmation,
			  trans_mutasi_header.Keterangan,
			  trans_mutasi_header.Status,
			  trans_mutasi_header.AddDate,
			  trans_mutasi_header.AddUser,
			  trans_mutasi_header.EditDate,
			  trans_mutasi_header.EditUser,
			  trans_mutasi_header.Approval_Date,
			  trans_mutasi_header.Approval_Status,
			  trans_mutasi_header.Approval_Remarks 
			FROM
			  trans_mutasi_header 
			  INNER JOIN intmutpurpose 
			    ON trans_mutasi_header.PurposeId = intmutpurpose.purposeid 
			  INNER JOIN gudang AS gudang_from 
			    ON trans_mutasi_header.KdGudang_From = gudang_from.KdGudang 
			  INNER JOIN gudang AS gudang_to 
			    ON trans_mutasi_header.KdGudang_to = gudang_to.KdGudang 
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_tujuan."   
            	".$where_gudang_admin." 
            	".$where_status."  
			ORDER BY 
			  trans_mutasi_header.TglDokumen DESC,
			  trans_mutasi_header.NoDokumen ASC,
			  trans_mutasi_header.Status * 1 ASC 
			LIMIT
              $offset,$limit
        ";               
       // echo $sql;
        // echo "<hr/>";
		return $this->getArrayResult($sql);
    }
    
    function num_tabel_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        $where_gudang="";
        $where_tujuan = "";
        $where_gudang_admin = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_mutasi_header.NoDokumen";
				$arr_keyword[1] = "intmutpurpose.purpose";
				$arr_keyword[2] = "gudang_from.Keterangan";
				$arr_keyword[3] = "gudang_to.Keterangan";
				$arr_keyword[4] = "trans_mutasi_header.Keterangan";
				
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_mutasi_header.KdGudang_From = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["tujuan"]!="")
			{
				$where_tujuan = "AND trans_mutasi_header.KdGudang_To = '".$arrSearch["tujuan"]."'";	
			}
			
			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "trans_mutasi_header.KdGudang_From", "in");
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND trans_mutasi_header.Status = '".$arrSearch["status"]."'";	
			}
		}
		
		$sql = "
			SELECT 
			  trans_mutasi_header.NoDokumen,
			  DATE_FORMAT(trans_mutasi_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_mutasi_header.PurposeId,
			  intmutpurpose.purpose,
			  trans_mutasi_header.KdGudang_From,
			  gudang_from.Keterangan AS nama_gudang_from,
			  trans_mutasi_header.KdGudang_To,
			  gudang_to.Keterangan AS nama_gudang_to,
			  trans_mutasi_header.Keterangan,
			  trans_mutasi_header.Status,
			  trans_mutasi_header.AddDate,
			  trans_mutasi_header.AddUser,
			  trans_mutasi_header.EditDate,
			  trans_mutasi_header.EditUser,
			  trans_mutasi_header.Approval_Date,
			  trans_mutasi_header.Approval_Status,
			  trans_mutasi_header.Approval_Remarks 
			FROM
			  trans_mutasi_header 
			  INNER JOIN intmutpurpose 
			    ON trans_mutasi_header.PurposeId = intmutpurpose.purposeid 
			  INNER JOIN gudang AS gudang_from 
			    ON trans_mutasi_header.KdGudang_From = gudang_from.KdGudang 
			  INNER JOIN gudang AS gudang_to 
			    ON trans_mutasi_header.KdGudang_to = gudang_to.KdGudang 
			WHERE
				1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_divisi."   
            	".$where_gudang_admin." 
            	".$where_status."  
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getIntMut()
	{
    	$sql = "SELECT purposeid,purpose FROM intmutpurpose WHERE 1 AND intmutpurpose.active = '1' ORDER BY intmutpurpose.purpose ASC";
		return $this->getArrayResult($sql);
    }
    
    function getHarga($pcode)
	{
		$sql = "
				SELECT a.harga1c FROM masterbarang a WHERE a.`PCode`='".$pcode."';
               "; 
        return $this->getRow($sql);
	}
	
	
	function ambilQty($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_mutasi_detail INNER JOIN
			  trans_mutasi_header ON trans_mutasi_header.`NoDokumen` = trans_mutasi_detail.`NoDokumen`
			WHERE 1 
			  AND trans_mutasi_detail.PCode = '".$pcode."' 
			  AND trans_mutasi_detail.NoDokumen = '".$nodok."' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$fieldupdate.",a.".$fieldnupdate.",a.".$fieldakhir.",a.".$fieldnakhir." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$KdGudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function ambilDetail($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  trans_mutasi_detail INNER JOIN
			  trans_mutasi_header ON trans_mutasi_header.`NoDokumen` = trans_mutasi_detail.`NoDokumen`
			WHERE 1  
			  AND trans_mutasi_detail.NoDokumen = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}	
	    
    function get_mutasi_stock($no_dokumen)
    {
        //echo $no_dokumen;die;
        $q = "
                DELETE FROM
                    mutasi
                WHERE
                    1
                    AND mutasi.KdTransaksi = 'MM'
                    AND mutasi.NoTransaksi = '".$no_dokumen."'
        ";   
        mysql_query($q);   
        
        $q = "
                SELECT
                    trans_mutasi_header.NoDokumen,
                    trans_mutasi_header.TglDokumen,
                    trans_mutasi_header.KdGudang_From,
                    trans_mutasi_detail.PCode,
                    trans_mutasi_detail.Qty
                    
                FROM
                    trans_mutasi_header
                    INNER JOIN trans_mutasi_detail ON
                        trans_mutasi_header.NoDokumen = trans_mutasi_detail.NoDokumen
                        -- AND trans_mutasi_header.Status = '1'
                        AND trans_mutasi_header.NoDokumen = '".$no_dokumen."'
                WHERE
                    1
                ORDER BY
                    trans_mutasi_detail.Sid ASC
        ";
        $qry = mysql_query($q);
        while($row = mysql_fetch_array($qry))
        {
            list($NoDokumen, $TglDokumen, $KdGudang_From, $PCode, $Qty) = $row;
            
            $q_ins = "
                    INSERT INTO `mutasi`
                    SET 
                      `NoKassa` = '0',
                      `NoTransaksi` = '".$NoDokumen."',
                      `Jenis` = 'O',
                      `KdTransaksi` = 'MM',
                      `Gudang` = '".$KdGudang_From."',
                      `GudangTujuan` = '',
                      `Tanggal` = '".$TglDokumen."',
                      `KodeBarang` = '".$PCode."',
                      `Qty` = '".$Qty."',
                      `Nilai` = '0',
                      `Status` = '1',
                      `Kasir` = '',
                      `Keterangan` = '',
                      `HPP` = '0',
                      `PPN` = '0',
                      `Service_charge` = '0' 
            ";
            mysql_query($q_ins);
           
        }
        
        
    }
	
	function getGudang($arr_gudang="")
	{
		$mylib = new globallib();
		
		$where_gudang_admin="";
		if(count($arr_gudang)>0)
		{
			$where_gudang_admin = $mylib->where_array($arr_gudang, "gudang.KdGudang", "in");
		}
		
    	$sql = "SELECT KdGudang,Keterangan as NamaGudang FROM gudang WHERE 1 ".$where_gudang_admin." order by KdGudang";
    	return $this->getArrayResult($sql);
    }
	
	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

	function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }

	function getHeader($id)
	{
		$sql = "
			SELECT 
			  trans_mutasi_header.NoDokumen,
			  DATE_FORMAT(trans_mutasi_header.TglDokumen,'%d-%m-%Y') AS TglDokumen,
			  trans_mutasi_header.PurposeId,
			  intmutpurpose.purpose,
			  trans_mutasi_header.KdGudang_From,
			  gudang_asal.Keterangan AS gudang_asal,
			  trans_mutasi_header.KdGudang_To,
			  gudang_tujuan.Keterangan AS gudang_tujuan,
			  trans_mutasi_header.Keterangan,
			  trans_mutasi_header.MovingConfirmation,
			  trans_mutasi_header.Status,
			  DATE_FORMAT(trans_mutasi_header.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_mutasi_header.AddUser,
			  DATE_FORMAT(trans_mutasi_header.EditDate,'%d-%m-%Y') AS EditDate,
			  trans_mutasi_header.EditUser 
			FROM
			  trans_mutasi_header 
			  LEFT JOIN intmutpurpose 
			    ON trans_mutasi_header.PurposeId = intmutpurpose.purposeid 
			  LEFT JOIN gudang AS gudang_asal 
			    ON trans_mutasi_header.KdGudang_From = gudang_asal.KdGudang 
			  LEFT JOIN gudang AS gudang_tujuan 
			    ON trans_mutasi_header.KdGudang_To = gudang_tujuan.KdGudang 
			WHERE 1 
			  AND trans_mutasi_header.NoDokumen = '".$id."' 
			LIMIT 1
        ";
        
        return $this->getRow($sql);
	}

	function getDetail($id)
	{
		$sql = "
			SELECT 
			  t.Sid, t.NoDokumen, t.PCode, b.NamaLengkap as NamaBarang, t.Qty, t.Satuan,t.QtyPcs, t.Harga, t.Keterangan,
			  s.NamaSatuan
			FROM
			  trans_mutasi_detail t INNER JOIN satuan s ON t.Satuan = s.KdSatuan
			  inner join masterbarang b on t.pcode=b.pcode
			WHERE 1 
			  AND t.NoDokumen = '$id' 
			ORDER BY 
			  t.sid DESC
		";
		
        return $this->getArrayResult($sql);
	}

	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  trans_mutasi_detail.* 
			FROM
			  trans_mutasi_detail 
			WHERE 1 
			  AND trans_mutasi_detail.PCode = '$pcode' 
			  AND trans_mutasi_detail.NoDokumen = '$nodok' 
			LIMIT 1
		";
		
        return $this->getRow($sql);
	}
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";  
         
        return $this->getRow($sql);
	}
    
	function getNopb($keyword="")
	{
		$where_keyword = "";
		if($keyword)
		{
			$where_keyword = "AND permintaan_barang_header.NoDokumen LIKE '%".$keyword."%'";
		}
		
    	$sql = "
    		SELECT 
			  permintaan_barang_header.NoDokumen 
			FROM permintaan_barang_header INNER JOIN permintaan_barang_detail 
  ON permintaan_barang_header.`NoDokumen` = permintaan_barang_detail.`NoDokumen` 
			WHERE 1 
			  AND permintaan_barang_header.Status = '1' 
  			  AND permintaan_barang_detail.`Qty` > permintaan_barang_detail.`QtyMutasi`
			  ".$where_keyword."
			GROUP BY 
			  permintaan_barang_header.NoDokumen 
			ORDER BY 
			  permintaan_barang_header.AddDate DESC,
			  permintaan_barang_header.TglDokumen DESC,
			  permintaan_barang_header.NoDokumen ASC
		";
		
        return $this->getArrayResult($sql);
    }
	
	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }
	
	function getDetailPCodePb($id)
	{
		$sql="SELECT permintaan_barang_detail.*,satuan.`NamaSatuan` FROM permintaan_barang_detail INNER JOIN satuan ON satuan.`KdSatuan`= permintaan_barang_detail.`Satuan` WHERE 1 AND NoDokumen = '".$id."' ORDER BY sid ASC";
		
		return $this->getArrayResult($sql);
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>