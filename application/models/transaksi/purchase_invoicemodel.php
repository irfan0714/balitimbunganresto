<?php
class Purchase_Invoicemodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getPurchaseInvoiceList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoFaktur";
				$arr_keyword[1] = "NoFakturSupplier";
				$arr_keyword[2] = "Nama";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	$sql = "  
            SELECT 
               NoFaktur, NoFakturSupplier, Tanggal, NoPO, s.Nama 
            FROM
              invoice_pembelian_header i inner join supplier s on i.KdSupplier = s.KdSupplier
            WHERE 
            	1  
            	".$where_keyword."                                  
            ORDER BY 
              NoFaktur DESC 
            Limit 
              $offset,$limit
        ";               
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function num_purchase_invoice_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "NoFaktur";
				$arr_keyword[1] = "NoFakturSupplier";
				$arr_keyword[2] = "Nama";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
		}
		
		$sql = "  
            SELECT 
               NoFaktur, NoFakturSupplier, Tanggal, NoPO, s.Nama 
            FROM
              invoice_pembelian_header i inner join supplier s on i.KdSupplier = s.KdSupplier
            WHERE 
            	1  
            	".$where_keyword."                                  
            ORDER BY 
              NoFaktur DESC 
        ";
		                  
        return $this->NumResult($sql);
	}

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		return $this->getRow($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	
	function getSupplier()
	{
		$sql = "SELECT KdSupplier, Nama FROM supplier order by nama";
    	return $this->getArrayResult($sql);
    }
    
    function getMataUang()
	{
		$sql = "SELECT kd_uang, keterangan FROM mata_uang order by kd_uang";
    	return $this->getArrayResult($sql);
    }
    
    function getPO($kdsupplier)
	{
		$sql = "SELECT DISTINCT PONo as NoDokumen FROM `trans_terima_header` 
				WHERE flagKonfirmasi='T' AND `Status`=1 AND kdsupplier='$kdsupplier' order by PONo";
    	return $this->getArrayResult($sql);
    }
    
    function getRG($NoPO, $tgl)
	{
		$sql = "SELECT t.NoDokumen, p.`PPn`, p.`currencycode` FROM trans_terima_header t 
				INNER JOIN trans_order_barang_header p ON p.NoDokumen=t.PoNO
				WHERE t.pono='$NoPO' AND YEAR(t.TglDokumen)=Year('$tgl') 
				AND MONTH(t.TglDokumen)=Month('$tgl') AND t.FlagKonfirmasi='T'";
				
    	return $this->getArrayResult($sql);
    }
    
    function getNoFaktur($tgl){
    	list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun - 2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoFaktur FROM invoice_pembelian_header WHERE NoFaktur LIKE '%$blnthn' ORDER BY NoFaktur DESC";
		
		$result =  $this->getArrayResult($sql);
		if(count($result)==0){
			$nofaktur = 'PI00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoFaktur'],2,5);
			$nourut = 100001 + ($nourut*1);
			$nourut = substr($nourut,-5);
			$nofaktur = 'PI' . $nourut .'-'.$blnthn;
		}
		return $nofaktur;
	}
    
    function getDetail($NoPenerimaan)
	{
		$nodokumen = implode("','", $NoPenerimaan);
		$sql = "SELECT h.`NoDokumen` as NoPenerimaan, d.`PCode`, m.`NamaLengkap` AS NamaBarang, d.Qty, po.Jumlah/po.Qty AS Harga, po.PPN, d.Satuan  
				FROM trans_terima_header h INNER JOIN trans_terima_detail d ON h.`NoDokumen`=d.`NoDokumen`
				INNER JOIN trans_order_barang_detail po ON h.`PoNo`=po.`NoDokumen` AND d.`PCode`=po.`PCode`
				INNER JOIN masterbarang m ON m.`PCode`=d.PCode
				WHERE d.NoDokumen IN('$nodokumen') ";
				
    	return $this->getArrayResult($sql);
    }
    
    function insertHeader($nofaktur,$tgl,$jatuhtempo,$nopo,$nofaktursupplier,$nofakturpajak,$kdsupplier,$mata_uang,$kurs,$ppn,$user)
    {
        $this->locktables('invoice_pembelian_header');

        $data = array(
            'NoFaktur'	=> $nofaktur,
            'Tanggal'	=> $tgl,
            'JatuhTempo' => $jatuhtempo,
            'NoPO' => $nopo, 
            'NoFakturSupplier' => $nofaktursupplier,
            'NoFakturPajak' => $nofakturpajak,
            'KdSupplier' => $kdsupplier,
            'MataUang' => $mata_uang,
            'Kurs' => $kurs,
            'PPN' => $ppn,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('invoice_pembelian_header', $data);

        $this->unlocktables();
    }
    
    function insertDetail($nofaktur,$nourut,$pcode,$nopenerimaandtl,$qty,$satuan,$harga, $ppn){
    	$mylib = new globallib();
		$this->locktables('invoice_pembelian_detail');
		for($i=0;$i<count($nourut); $i++){
		
			$data = array(
				'NoFaktur' => $nofaktur,
				'NoUrut' => $nourut[$i],
				'KdBarang' => $pcode[$i],
				'NoPenerimaan' => $nopenerimaandtl[$i],
				'Qty' => $mylib->save_int($qty[$i],'ind'),
				'Satuan' => $satuan[$i],
				'Harga' => $mylib->save_int($harga[$i],'ind')
			);
			$this->db->insert('invoice_pembelian_detail', $data);
		}
	
	
		$this->unlocktables();
	}
	
	function insertHutang($tgl,$nofaktur, $kdsupplier,$mata_uang,$kurs, $gtotal,$jatuhtempo){
		$mylib = new globallib();
		$this->locktables('hutang');
		$data = array(
			'NoDokumen' => $nofaktur,
			'NoFaktur' => $nofaktur, 
			'KdSupplier' => $kdsupplier,
			'TipeTransaksi' => 'I',
			'Tanggal' => $tgl,
			'JatuhTempo' => $jatuhtempo,
			'NilaiTransaksi' => $mylib->save_int($gtotal,'ind'),
			'Sisa' => $mylib->save_int($gtotal,'ind'),
			'MataUang' => $mata_uang,
			'Kurs' => $kurs
		);
		$this->db->insert('hutang', $data);
		$this->unlocktables();
		
	}
	
	function insertMutasiHutang($tgl,$nofaktur, $kdsupplier,$mata_uang,$kurs, $gtotal){
		$mylib = new globallib();
		$this->locktables('mutasi_hutang');
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$data = array(
			'Tahun' => $tahun,
			'Bulan' => $bulan, 
			'NoDokumen' => $nofaktur, 
			'MataUang' => $mata_uang,
			'Kurs' => $kurs,
			'KdSupplier' => $kdsupplier,
			'TipeTransaksi' => 'I',
			'Jumlah' => $mylib->save_int($gtotal,'ind')
		);
		$this->db->insert('mutasi_hutang', $data);
		$this->unlocktables();
	}
	
	function updateRG($nopenerimaan){
		$nodokumen = implode("','", $nopenerimaan);
		$this->locktables('trans_terima_header');
		$sql = "Update trans_terima_header set flagKonfirmasi='Y' where NoDokumen in('$nodokumen')";
        $this->db->query($sql);
        $this->unlocktables();
        return;
	}
	
	function getDataHeader($nofaktur){
    	$sql = "SELECT h.`NoFaktur`, h.`Tanggal`, h.`JatuhTempo`, s.`Nama`, h.`Kurs`, h.`MataUang`, h.`NoFakturPajak`,
    			 h.`NoFakturSupplier`, h.`NoPO`, h.`PPN`, h.AddUser, h.AddDate, h.EditUser, h.EditDate
				FROM invoice_pembelian_header h 
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				WHERE h.`NoFaktur`='$nofaktur'";
        $row = $this->getArrayResult($sql);
        return $row[0];
	}
	
	function getDataDetail($nofaktur){
    	$sql = "SELECT d.`KdBarang`, m.`NamaLengkap`, h.`NoPenerimaan`, d.`NoUrut`, d.`Qty`, d.`Satuan`, d.`Harga`
				FROM invoice_pembelian_detail d INNER JOIN invoice_pembelian_header h ON d.Nofaktur = h.NoFaktur INNER JOIN masterbarang m ON d.`KdBarang`= m.`PCode`
				WHERE d.`NoFaktur`='$nofaktur'";
        return $this->getArrayResult($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>