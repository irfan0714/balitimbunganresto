<?php
class Reservasi_konfirmasi_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getTabelList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_reservasi_konfirmasi.NoDokumen";
				$arr_keyword[1] = "trans_reservasi_konfirmasi.NoReservasi";
				$arr_keyword[2] = "trans_reservasi_konfirmasi.NoStiker";
				$arr_keyword[3] = "trans_reservasi_konfirmasi.NoVoucher";
				$arr_keyword[4] = "trans_reservasi_konfirmasi.TourLeader";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
        
    	/* $sql = "
    		SELECT 
			  trans_reservasi_konfirmasi.NoDokumen,
			  DATE_FORMAT(trans_reservasi_konfirmasi.TglKonfirmasi,'%d-%m-%Y') AS TglKonfirmasi,
			  trans_reservasi_konfirmasi.JamKonfirmasi,
			  trans_reservasi_konfirmasi.NoReservasi,
			  trans_reservasi_konfirmasi.KdTravel,
			  trans_reservasi_konfirmasi.TourLeader,
			  tourleader.Nama as TourLeaderName,
			  trans_reservasi_konfirmasi.JumlahRombongan,
			  trans_reservasi_konfirmasi.NoVoucher,
			  trans_reservasi_konfirmasi.NoStiker,
			  trans_reservasi_konfirmasi.keterangan,
			  trans_reservasi_konfirmasi.status,
			  DATE_FORMAT(trans_reservasi_konfirmasi.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_reservasi_konfirmasi.AddUser,
			  DATE_FORMAT(trans_reservasi_konfirmasi.EditDate,'%d-%m-%Y') AS EditDate,
			  trans_reservasi_konfirmasi.EditUser 
			FROM
			  trans_reservasi_konfirmasi 
			INNER JOIN tourleader
			  ON trans_reservasi_konfirmasi.TourLeader = tourleader.KdTourLeader
			WHERE 
			  1 
              ".$where_keyword."
			ORDER BY trans_reservasi_konfirmasi.TglKonfirmasi,
			  trans_reservasi_konfirmasi.NoDokumen ASC
			LIMIT
              $offset,$limit
        "; */

		$sql = "
    		SELECT 
			  trans_reservasi_konfirmasi.NoDokumen,
			  DATE_FORMAT(trans_reservasi_konfirmasi.TglKonfirmasi,'%d-%m-%Y') AS TglKonfirmasi,
			  trans_reservasi_konfirmasi.JamKonfirmasi,
			  trans_reservasi_konfirmasi.NoReservasi,
			  trans_reservasi_konfirmasi.KdTravel,
			  trans_reservasi_konfirmasi.TourLeader,
			  trans_reservasi_konfirmasi.JumlahRombongan,
			  trans_reservasi_konfirmasi.NoVoucher,
			  trans_reservasi_konfirmasi.NoStiker,
			  trans_reservasi_konfirmasi.keterangan,
			  trans_reservasi_konfirmasi.status,
			  DATE_FORMAT(trans_reservasi_konfirmasi.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_reservasi_konfirmasi.AddUser,
			  DATE_FORMAT(trans_reservasi_konfirmasi.EditDate,'%d-%m-%Y') AS EditDate,
			  trans_reservasi_konfirmasi.EditUser 
			FROM
			  trans_reservasi_konfirmasi 
			WHERE 
			  1 
              ".$where_keyword."
			ORDER BY trans_reservasi_konfirmasi.TglKonfirmasi DESC
			LIMIT
              $offset,$limit
        "; 
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }
    
    function getGSA()
	{
    	$sql = "
    			SELECT 
				  employee.`employee_id`,
  				  employee.`employee_name` 
				FROM
				  `jabatan` 
				  INNER JOIN `employee_position` 
				    ON jabatan.`jabatan_id` = `employee_position`.`jabatan_id` 
				  INNER JOIN employee 
				    ON `employee`.`employee_id` = `employee_position`.`jabatan_id` 
				WHERE `jabatan`.`jabatan_id` = '30' 
				GROUP BY employee.`employee_id` 
				ORDER BY employee.`employee_name` ASC ;
    		   ";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function num_tabel_row($arrSearch)
    {
        $mylib = new globallib();
        
        $where_keyword="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_reservasi_konfirmasi.NoDokumen";
				$arr_keyword[1] = "trans_reservasi_konfirmasi.NoReservasi";
				$arr_keyword[2] = "trans_reservasi_konfirmasi.NoStiker";
				$arr_keyword[3] = "trans_reservasi_konfirmasi.NoVoucher";
				$arr_keyword[4] = "trans_reservasi_konfirmasi.TourLeader";
				
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		}
		
		$sql = "
			SELECT 
			  trans_reservasi_konfirmasi.NoDokumen,
			  DATE_FORMAT(trans_reservasi_konfirmasi.TglKonfirmasi,'%d-%m-%Y') AS TglKonfirmasi,
			  trans_reservasi_konfirmasi.JamKonfirmasi,
			  trans_reservasi_konfirmasi.NoReservasi,
			  trans_reservasi_konfirmasi.KdTravel,
			  trans_reservasi_konfirmasi.TourLeader,
			  trans_reservasi_konfirmasi.JumlahRombongan,
			  trans_reservasi_konfirmasi.NoVoucher,
			  trans_reservasi_konfirmasi.NoStiker,
			  trans_reservasi_konfirmasi.keterangan,
			  trans_reservasi_konfirmasi.status,
			  DATE_FORMAT(trans_reservasi_konfirmasi.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_reservasi_konfirmasi.AddUser,
			  DATE_FORMAT(trans_reservasi_konfirmasi.EditDate,'%d-%m-%Y') AS EditDate,
			  trans_reservasi_konfirmasi.EditUser 
			FROM
			  trans_reservasi_konfirmasi 
			WHERE 
			  1 
              ".$where_keyword."
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
    
    function getNamaTravel($v_tourtravel)
	{
    	$sql = "SELECT * FROM tourtravel a WHERE a.`KdTravel`='$v_tourtravel';";
        return $this->getRow($sql);
    }

	function getDetailKonfirmasi($nodok)
	{
		/*$sql = "
			SELECT 
			  trans_reservasi_konfirmasi.NoDokumen,
			  DATE_FORMAT(trans_reservasi_konfirmasi.TglKonfirmasi,'%d-%m-%Y') AS TglKonfirmasi,
			  trans_reservasi_konfirmasi.JamKonfirmasi,
			  trans_reservasi_konfirmasi.NoReservasi,
			  trans_reservasi_konfirmasi.KdTravel,
			  trans_reservasi_konfirmasi.TourLeader,
			  tourleader.Nama as TourLeaderName,
			  trans_reservasi_konfirmasi.JumlahRombongan,
			  trans_reservasi_konfirmasi.Dewasa,
			  trans_reservasi_konfirmasi.Anak,
			  trans_reservasi_konfirmasi.Batita,
			  trans_reservasi_konfirmasi.NoVoucher,
			  trans_reservasi_konfirmasi.NoStiker,
			  trans_reservasi_konfirmasi.gsa,
			  trans_reservasi_konfirmasi.keterangan,
			  trans_reservasi_konfirmasi.status,
			  DATE_FORMAT(trans_reservasi_konfirmasi.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_reservasi_konfirmasi.AddUser,
			  DATE_FORMAT(trans_reservasi_konfirmasi.EditDate,'%d-%m-%Y') AS EditDate,
			  trans_reservasi_konfirmasi.EditUser 
			FROM
			  trans_reservasi_konfirmasi
			INNER JOIN tourleader
			  ON trans_reservasi_konfirmasi.TourLeader = tourleader.KdTourLeader
			WHERE 1 
			  AND trans_reservasi_konfirmasi.NoDokumen = '".$nodok."' 
			LIMIT 0, 01
		";*/
		
		$sql = "
			SELECT 
			  trans_reservasi_konfirmasi.NoDokumen,
			  DATE_FORMAT(trans_reservasi_konfirmasi.TglKonfirmasi,'%d-%m-%Y') AS TglKonfirmasi,
			  trans_reservasi_konfirmasi.JamKonfirmasi,
			  trans_reservasi_konfirmasi.NoReservasi,
			  trans_reservasi_konfirmasi.KdTravel,
			  trans_reservasi_konfirmasi.TourLeader,
			  trans_reservasi_konfirmasi.JumlahRombongan,
			  trans_reservasi_konfirmasi.JmlTourLeader,
			  trans_reservasi_konfirmasi.JmlTourGuide,
			  trans_reservasi_konfirmasi.Dewasa,
			  trans_reservasi_konfirmasi.Anak,
			  trans_reservasi_konfirmasi.Batita,
			  trans_reservasi_konfirmasi.NoVoucher,
			  trans_reservasi_konfirmasi.NoStiker,
			  trans_reservasi_konfirmasi.gsa,
			  trans_reservasi_konfirmasi.keterangan,
			  trans_reservasi_konfirmasi.status,
			  DATE_FORMAT(trans_reservasi_konfirmasi.AddDate,'%d-%m-%Y') AS AddDate,
			  trans_reservasi_konfirmasi.AddUser,
			  DATE_FORMAT(trans_reservasi_konfirmasi.EditDate,'%d-%m-%Y') AS EditDate,
			  trans_reservasi_konfirmasi.EditUser 
			FROM
			  trans_reservasi_konfirmasi
			WHERE 1 
			  AND trans_reservasi_konfirmasi.NoDokumen = '".$nodok."' 
			LIMIT 0, 01
		";
		
        return $this->getRow($sql);
	}
	
	function getDetailSticker($id)
	{
    	$sql = "
    			SELECT 
				  * FROM trans_reservasi_konfirmasi_sticker WHERE NoDokumen='".$id."' ;
    		   ";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function cekStiker($nostiker,$tgl,$noreservasi)
	{
    	$sql = "
    			SELECT 
				  a.`NoDokumen`,
				  a.`NoReservasi`,
				  a.`TglKonfirmasi`,
				  b.`NoSticker` 
				FROM
				  `trans_reservasi_konfirmasi` a 
				  INNER JOIN `trans_reservasi_konfirmasi_sticker` b 
				    ON a.`NoDokumen` = b.`NoDokumen` 
				WHERE b.`NoSticker` = '$nostiker' 
				  AND a.`TglKonfirmasi` = '$tgl'  ;
    		   ";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function getReservasi($nodok)
    {
        $sql = "
        	SELECT 
			  trans_reservasi.*
			FROM
			  trans_reservasi 
			WHERE 
			  1 
			  AND trans_reservasi.NoDokumen = '".$nodok."'
			LIMIT 0, 01
		"; 
        return $this->getRow($sql);
    }
    
    function getDetailReservasi($nodok)
    {
    	$sql = "SELECT * FROM trans_reservasi_detail WHERE 1 AND trans_reservasi_detail.NoDokumen = '".$nodok."'";           
            
		return $this->getArrayResult($sql);
	}
    
    function getDetailTourTravel($id)
    {
        $sql = "SELECT * FROM tourtravel WHERE 1 AND tourtravel.KdTravel = '".$id."' LIMIT 0,01"; 

        return $this->getRow($sql);
    }
    
    function getNilai($v_noreservasi){
		$sql = "SELECT Total FROM trans_reservasi WHERE NoDokumen='$v_noreservasi'";
		$result = $this->getArrayResult($sql);
		return $result[0]['Total'];
	}
    
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>