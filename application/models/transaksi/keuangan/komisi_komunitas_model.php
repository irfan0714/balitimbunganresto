<?php

class Komisi_komunitas_model extends CI_Model
{

	//put your code here
	function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
	}
	
	
	function getTT()
	{
		$sql = "SELECT * FROM `tourtravel` WHERE Aktif='A' ORDER BY Nama ASC LIMIT 0,100";
		return $this->getArrayResult($sql);
	}
	
	function getCariTT($tl)
	{
    	$sql = "SELECT * FROM `tourtravel` WHERE Aktif='A' AND ( KdTravel LIKE '%".$tl."%' OR Nama LIKE '%".$tl."%') ORDER BY Nama ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
	
	function ambilReq ($tl,$tg_awal,$tg_akhir)
	{	
	    $mylib = new globallib();
		$awal = $mylib->ubah_tanggal($tg_awal);	
		$akhir = $mylib->ubah_tanggal($tg_akhir);		
		$sql = "SELECT 
				  a.`Tanggal`,
				  '' AS NoStiker,
				  b.`KdTourLeader`,
				  c.`Nama`,
				  SUM(a.`TotalNilai`) Omset 
				FROM
				  `transaksi_header` a 
				  INNER JOIN register b 
				    ON a.`KdAgent` = b.`NoStiker` 
				    AND a.`Tanggal` = b.`Tanggal`
				    INNER JOIN tourleader c
				    ON b.`KdTourLeader` = c.`KdTourLeader` 
				WHERE b.`statuskomisi_komunitas` = '0' 
				  AND b.KdTravel = '$tl' 
				  AND a.`Tanggal` BETWEEN '$awal' 
				  AND '$akhir' 
				GROUP BY a.`Tanggal` 
				ORDER BY a.`Tanggal` ASC ;";
		//echo "Cek di register<br>".$sql."<br><br>";die;
		return $this->getArrayResult($sql);
	}
	
	function getNewNo($tahun,$bulan)
	{
		$sql = "SELECT NoKomisi FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
		return $this->getRow($sql);
	}
	
	function Querydata($sql){
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
	}
	
	function getHeader($id)
	{
		$sql = "
				SELECT 
				  finance_komisi_komunitas_header.NoTransaksi,
				  DATE_FORMAT(
					finance_komisi_komunitas_header.AddDate,
					'%d-%m-%Y'
				  ) AS TglTransaksi,
				  DATE_FORMAT(
					finance_komisi_komunitas_header.TglAkhir,
					'%d-%m-%Y'
				  ) AS TglAkhir,
				  finance_komisi_komunitas_header.`KdTravel` AS KdAgent,
				  t.Nama,
				  finance_komisi_komunitas_header.`TotalOmset` AS TotalSales,
				  finance_komisi_komunitas_header.`TotalKomisi` AS Total,
				  finance_komisi_komunitas_header.Keterangan,
				  Pembulatan AS Payment 
				FROM
				  finance_komisi_komunitas_header 
				  INNER JOIN tourtravel t 
					ON finance_komisi_komunitas_header.KdTravel = t.KdTravel 
				WHERE NoTransaksi = '$id' 
				ORDER BY NoTransaksi DESC 
				";
		//echo $sql;
		return $this->getRow($sql);
	}
	
	function getHeaderForPrint2($id)
	{		
		$sql = "SELECT 
				  finance_komisi_komunitas_header.`TotalOmset` AS total,
				  finance_komisi_komunitas_header.`TotalKomisi` AS komisi,
				  '00' AS KdDivisi,
				  '' AS NamaDivisi 
				FROM
				  finance_komisi_komunitas_header 
				WHERE finance_komisi_komunitas_header.NoTransaksi = '$id' ;

				";		
		return $this->getArrayResult($sql);
	}
	
	function getDetailForPrint($id)
	{
		$sql = "SELECT 
				  d.*,
				  DATE_FORMAT(d.Tanggal, '%d-%m-%Y') AS Tgl,
				  '' AS NamaLengkap,
				  '' AS PCode,
				  '' AS Qty,
				  '' AS Harga
				FROM
				  finance_komisi_komunitas_detail d 
				WHERE d.NoTransaksi = '$id' ";
		//echo $sql;
		return $this->getArrayResult($sql);
	}
	
	function getHeaderForExcel($id)
	{
		$sql = "SELECT 
				  finance_komisi_komunitas_header.NoTransaksi,
				  DATE_FORMAT(
					finance_komisi_komunitas_header.AddDate,
					'%d-%m-%Y'
				  ) AS TglTransaksi,
				  finance_komisi_komunitas_header.KdTravel AS KdAgent,
				  t.Nama,
				  finance_komisi_komunitas_header.Keterangan,
				  finance_komisi_komunitas_header.TotalKomisi AS Total,
				  finance_komisi_komunitas_header.TotalOmset AS TotalSales 
				FROM
				  finance_komisi_komunitas_header 
				  INNER JOIN tourtravel t 
					ON finance_komisi_komunitas_header.KdTravel = t.KdTravel 
				WHERE NoTransaksi = '$id' 
				ORDER BY NoTransaksi DESC ";
		//echo $sql;
		return $this->getArrayResult($sql);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

	function getHeaderForPrint($id)
	{
		
		$sql = "SELECT
        a.NoTransaksi, DATE_FORMAT(a.TglTransaksi, '%d-%m-%Y') AS TglTransaksi, a.KdAgent, a.Total AS TotKomisi, a.TotalSales AS TotSales
        FROM
        finance_komisi_header a
        WHERE NoTransaksi = '$id'
        GROUP BY KdAgent";
		
		
		return $this->getRow($sql);
	}
	

	function getCountDetail($id)
	{
		$sql = "SELECT * FROM finance_komisi_detail where NoTransaksi='$id'";
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function getKomisiList($num, $offset, $id, $with)
	{
		$month_cek = $this->aplikasi();
		
		$bln1 = $month_cek[0]['TglTrans'];
		$bln = substr($bln1,5,2);
		if($offset != ''){
			$offset = $offset;
		}
		else
		{
			$offset = 0;
		}
		$clause = "";
		if($id != ""){
			if($with == "NoTransaksi"){
				$clause = "AND $with like '%$id%'";
			}
			else
			{
				$clause = "AND $with = '$id'";
			}
		}
		$sql = "SELECT 
				  NoDokumen,		
		          NoTransaksi, 
				  date_format(TglAwal,'%d-%m-%Y') AS TglAwal,
				  date_format(TglAkhir,'%d-%m-%Y') AS TglAkhir,
				  KdTravel, Keterangan, TotalOmset, TotalKomisi
		FROM finance_komisi_komunitas_header WHERE 1 $clause
		ORDER BY NoTransaksi DESC limit $offset,$num";
		//WHERE $clause
		//echo $sql;
		return $this->getArrayResult($sql);
	}

	
	
	

	function getDetail($id)
	{
		$sql = "SELECT a.NoTransaksi, b.PCode, c.NamaLengkap, b.Qty, b.Harga, Persentase, Potongan, Komisi1, Komisi2, Komisi3, Komisi4 
		FROM finance_komisi_header a, finance_komisi_detail b, masterbarang c
		WHERE a.NoTransaksi = b.NoTransaksi AND b.PCode = c.PCode
		AND a.NoTransaksi='$id' ORDER BY NoTransaksi DESC, b.AddDate Desc";

		return $this->getArrayResult($sql);
	}

	function num_komisi_row($id, $with)
	{
		$clause = "";
		if($id != ''){
			if($with == "NoTransaksi"){
				$clause = "WHERE $with like '%$id%'";
			}
			else
			{
				$clause = "WHERE $with = '$id'";
			}
		}
		$sql = "SELECT NoTransaksi FROM finance_komisi_header $clause";
		return $this->NumResult($sql);
	}

	
	
	function getKomisiTicket($field,$tanggal,$nosticker)
	{
	       
			$sql="
					SELECT DISTINCT
					  d.`notrans`,
					  d.PCode,
					  d.tgl_berlaku,
					  SUM(d.qty) AS quantity,
					  SUM(d.harga/1.1) AS ttlnettos,
					  e.`komisi`,
					  '0' AS komisi1,
					  '0' AS komisi2,
					  '0' AS komisi3,
					  '0' AS komisi4,
					  e.NamaLengkap,
					   d.`harga`/1.1 AS price,
					  SUM(d.`qty` * d.`harga`/1.1) * e.`komisi` / 100 AS komisi_ticket 
					FROM
					   register a  
					  INNER JOIN `ticket_customer` c 
					    ON c.no_identitas = a.NoStiker 
					    AND c.add_date = a.Tanggal 
					  INNER JOIN `ticket` d 
					    ON c.`notrans` = d.`notrans` 
					  INNER JOIN masterbarang e 
					    ON d.`PCode` = e.`PCode` 
					WHERE 1 
					  AND a.statuskomisi = 0 
					  AND d.statuskomisi = 0
					  AND a.Jumlah > 0 
					  AND ($tanggal)
					  AND a.Tanggal >='2017-08-01'
					  AND e.komisi>0
					GROUP BY d.`notrans`,
  					d.PCode ;
				";
		//echo "Dapeting Komisi Ticket <br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	function getKomisiTicketAgent($kdtravel,$tglagen)
	{
	       
			$sql="
					SELECT DISTINCT
					  d.`notrans`,
					  d.PCode,
					  d.tgl_berlaku,
					  SUM(d.qty) AS quantity,
					  SUM(d.harga/1.1) AS ttlnettos,
					  kom.total,
					  kom.komisi1 as komisi,
					  '0' AS komisi1,
					  kom.komisi2,
					  kom.komisi3,
					  kom.komisi4,
					  e.NamaLengkap,
					  d.`harga`/1.1 AS price,
					  SUM(d.`qty` * d.`harga`/1.1) * kom.total / 100 AS komisi_ticket 

					FROM
					   register a  
					  INNER JOIN `ticket_customer` c 
					    ON c.no_identitas = a.NoStiker  
					    AND c.add_date = a.Tanggal 
					  INNER JOIN `ticket` d 
					    ON c.`notrans` = d.`notrans` 
					  INNER JOIN masterbarang e 
					    ON d.`PCode` = e.`PCode` 
					inner JOIN 
					    (SELECT 
					      kdtravel,
					      pcode AS dpcode,
					      total,
					      komisi1,
					      komisi2,
					      komisi3,
					      komisi4,
					      komisi4bag 
					    FROM
					      komisi_detail) kom 
					    ON kom.kdtravel = '$kdtravel' 
					    AND d.PCode = kom.dpcode
					WHERE 1 
					  AND a.statuskomisi = 0 
					  AND d.statuskomisi = 0
					  AND a.Jumlah > 0 
					  AND ($tglagen) 
					  AND a.Tanggal >='2017-08-01'
					GROUP BY d.`notrans`,
  					d.PCode ;
				";
		//echo "Dapeting Komisi Ticket <br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	function getKomisiTravel($kdtravel,$tglagen)
	{
	$sql="	
		
		SELECT 
		  det.NoStruk,
		  det.TglJual,
		  det.Waktu,
		  det.KdAgent,
		  det.PCode,
		  det.NamaLengkap,
		  det.Harga,
		  SUM(((det.Qty*det.Harga)-det.Disc)) * kom.total/100 AS Nilai,
  		  sum(det.Disc) as Disc,
			det.hrgPPN,
		  SUM(det.Qty) AS Qty,
		  SUM(det.ttlnetto) AS ttlnetto,
		  kom.total AS Komisi,
		  kom.komisi1,
		  kom.komisi2,
		  kom.komisi3,
		  kom.komisi4 
		FROM
		  (SELECT 
		    a.NoStruk,
		    a.Tanggal AS TglJual,
		    a.Waktu,
		    a.KdAgent,
		    b.PCode,
		    c.NamaLengkap,
			b.Harga* Qty AS hrgPPN,
		    b.Qty,
		    b.Service_charge,
		    b.Netto,
		    ROUND(
		      IF(
		        b.Service_charge = 5,
		        b.Harga,
		        b.Harga / 1.1
		      )
		    ) AS Harga,
		    (
		      ROUND(
		        b.Qty * IF(
		          b.Service_charge = 5,
		          b.Harga,
		          b.Harga / 1.1
		        ) - IF(
		          b.Service_charge = 5,
		          b.Netto,
		          b.Netto / 1.1
		        )
		      )
		    ) AS Disc,
		    (
		      ROUND(
		        IF(
		          b.Service_charge = 5,
		          b.Netto,
		          b.Netto / 1.1
		        ),
		        0
		      )
		    ) AS ttlnetto,
		    IFNULL(b.Komisi, 0) AS Komisi ,
		    IFNULL(
		    ROUND(
		      b.Komisi / 100 * IF(
		        b.Service_charge = 5,
		        b.Netto,
		        b.Netto / 1.1
		      )
		    ),
		    0
		  ) AS Nilai 
		  FROM
		    transaksi_header a,
		    transaksi_detail b,
		    masterbarang c 
		  WHERE (
		      (
		     	$tglagen
		      )
		    ) 
		    AND a.nostruk = b.nostruk 
		    AND b.pcode = c.pcode 
		    AND a.statuskomisi = '0'
		    and a.userdisc='') det 
		  inner join register r on det.KdAgent=r.NoStiker and r.KdTravel ='$kdtravel' and det.TglJual=r.Tanggal and r.jumlah>0
		  inner JOIN 
		    (SELECT 
		      kdtravel,
		      pcode AS dpcode,
		      total,
		      komisi1,
		      komisi2,
		      komisi3,
		      komisi4,
		      komisi4bag 
		    FROM
		      komisi_detail) kom 
		    ON kom.kdtravel = '$kdtravel' 
		    AND det.pcode = kom.dpcode 
		GROUP BY NoStruk,PCode ";
        //echo "Cek di jika agent ada di komisi header<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	//melakukan perubahan pada 22/11/2016 dengan mengubah `KdTourLeader`='$field'
		
	
		
	function cekKomisi($stiker,$tgl)
	{
		$sql = "SELECT DISTINCT(NoStruk) FROM `finance_komisi_detail` a
				INNER JOIN `finance_komisi_header` b
				ON a.`NoTransaksi` = b.`NoTransaksi` AND b.`KdAgent` ='$stiker' 
				 AND b.TglTransaksi BETWEEN DATE_SUB('$tgl', INTERVAL 31 DAY) AND '$tgl'
					
				";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();

		return $row;
	}

	

	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function NamaPrinter($id)
	{
		$sql = "SELECT * from kassa where ip='$id'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function getDate()
	{
		$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
		return $this->getRow($sql);
	}

	function getTotalNetto($no)
	{
		$sql = "SELECT SUM(Netto) FROM transaksi_detail WHERE NoStruk ='$no'";
		return $this->getArrayResult($sql);
	}

	function getWaktu($no)
	{
		$sql = "SELECT MIN(waktu) AS mulai, MAX(waktu) AS akhir FROM transaksi_detail WHERE NoStruk = '$no'";
		return $this->getArrayResult($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function ifPCodeBarcode($id)
	{
		$bar = substr($id, 0, 10);
		$sql = "SELECT KdRekening FROM rekening Where KdRekening='$id'";
		return $this->getRow($sql);
	}

	function getPCodeDet($kode, $field)
	{
		$sql = "select KdRekening, NamaRekening from rekening where KdRekening='$kode'";
		return $this->getRow($sql);
	}
	
	public function getTrans($id)
	{
		$this->db->from('finance_komisi_komunitas_header');
		$this->db->where('NoTransaksi',$id);
		$query = $this->db->get();
		return $query->row();
        
	}

}

?>
