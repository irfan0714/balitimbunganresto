<?php

class setor_model extends CI_Model {


    //put your code here
    function __construct() {
        parent::__construct();
    }

    function getDetailForPrint($id) {
        $sql = "
                 SELECT a.NoTransaksi, a.TglTransaksi, b.Keterangan, a.TglAwal, a.TglAkhir, a.NamaSetor, a.Nilai, a.Keterangan
                    FROM finance_setor a INNER JOIN 
                    counter b
                    ON a.KdCounter = b.KdCounter AND a.NoTransaksi = $id
		";
//echo $sql;
        return $this->getArrayResult($sql);
    }

    function getSetorList($num, $offset, $id, $counter, $with) {
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }
        $clause = "";
        if ($id != "") {
            if ($with == "NoTransaksi") {
                $clause = "and $with like '%$id%'";
            }else{
                $clause = "and $with = '$id'";
            }
        }else if ($counter != "") {
            $clause = "and $with = '$counter'";
        }
        $sql = "
         SELECT
            *,DATE_FORMAT(TglTransaksi, '%d-%m-%Y') AS TglTransaksi
        FROM
            finance_setor  $clause
        ORDER BY NoTransaksi DESC limit $offset,$num";
      //  echo $sql;
        return $this->getArrayResult($sql);
    }

    function num_setor_row($id, $counter, $with) {
        $clause = "";
        if ($id != '') {
            if ($with == "NoTransaksi") {
                $clause = " where $with like '%$id%'";
            } else {
                $clause = " where $with = '$id'";
            }
        }elseif ($counter != "") {
            $clause = "where $with = '$counter'";
        }
        $sql = "SELECT NoTransaksi FROM finance_setor a $clause";
//        $sql = " SELECT a.NoTransaksi, date_format(a.TglTransaksi,'%d-%m-%Y') AS TglTransaksi , b.Keterangan AS Nama, date_format(a.TglAwal,'%d-%m-%Y') AS TglAwal, date_format(a.TglAkhir,'%d-%m-%Y') AS TglAkhir, a.NamaSetor, a.Nilai, a.Keterangan
//                    FROM finance_setor a INNER JOIN counter b
//                    ON a.KdCounter = b.KdCounter $clause
//                    ORDER BY NoTransaksi DESC";
        return $this->NumResult($sql);
    }

    function getDetail($id) {
        $sql = "SELECT *,NoTransaksi, KdCounter, Jam, Nik, NamaKasir, NilaiSetoran,
             Tunai, KKredit, KDebit, Voucher, Selisih, Keterangan, Terpakai, konf_setoran,
             FlagPosting, NoPosting, ADDDATE, AddUser, EditDate, EditUser,date_format(TglTransaksi,'%d-%m-%Y') AS TglTransaksi
                FROM
                  finance_setor
                where
                  NoTransaksi = $id";
        return $this->getRow($sql);
        
    }

    function get_id($id) {
        $sql = "SELECT NoTransaksi, date_format(TglTransaksi,'%d-%m-%Y') AS TglTransaksi, KdCounter, date_format(TglAwal,'%d-%m-%Y') AS TglAwal, date_format(TglAkhir,'%d-%m-%Y') AS TglAkhir, NamaSetor, Nilai, Keterangan FROM finance_setor WHERE NoTransaksi = $id";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getMaster() {
        $sql = "SELECT KdCounter,Keterangan from counter order by KdCounter";
        $qry = $this->db->query($sql);  
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
     function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
        return $this->getRow($sql);
    }
    
    function getNewNo($tahun) {
        $sql = "SELECT NoSetor FROM setup_no where Tahun='$tahun'";
       // echo $sql; die();
        return $this->getRow($sql);
    }
    
    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>
