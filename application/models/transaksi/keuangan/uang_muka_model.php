<?php
class Uang_muka_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getKasBank($user)
	{
    	$sql = "
    			SELECT 
				  a.`KdKasBank`,
				  a.`NamaKasBank`,
				  a.`KdRekening` 
				FROM
				  `kasbank` a 
				  INNER JOIN `userkasbank` b 
				    ON a.`KdKasBank` = b.`KdKasBank` 
				WHERE b.`UserName` = '".$user."' 
				ORDER BY a.`NamaKasBank` ASC ;
    			";
		return $this->getArrayResult($sql);
    }
	
	
	function getEmployee($employee='')
	{
		if($employee == ''){
			$where = '';
		}else{
			$where = " and a.employee_name like '%$employee%' ";
		}
		
    	$sql = "
    			SELECT 
				  a.employee_id,
				  a.employee_name
				FROM
				  `employee` a 
				WHERE 1 $where
				ORDER BY a.`employee_name` ASC ;
    			";
		return $this->getArrayResult($sql);
    }
    
    
    function getSubDivisi()
	{
    	$sql = "
    			SELECT 
				  * 
				FROM
				  subdivisi a 
				WHERE 1 
				ORDER BY a.`NamaSubDivisi` ASC ;

    			";
		return $this->getArrayResult($sql);
    }
    
    
    function getKodeBank($kd){

    	$sql = "SELECT * FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    function getEmployeeName($employee){

    	$sql = "SELECT employee_name FROM `employee` WHERE employee_id='$employee'";

        return $this->getRow($sql);

    }
    
     function getListUangMuka($id){

    	$sql = "SELECT 
    				a.*,
    				date_format(TglDokumen,'%d-%m-%Y') as Tanggal
    	        FROM uang_muka a WHERE a.NoDokumen ='".$id."'";
        return $this->getRow($sql);

    }
    
    function getRekeningPPH(){
		$sql = "SELECT KdRekening, NamaRekening FROM rekening WHERE KdRekening IN ('21060020','21060030','21060040','21060050','21060010','21060070');";
		return $this->getArrayResult($sql);
	}

    function num_uang_muka_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM uang_muka;       
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getUangMukaList($limit,$offset,$arrSearch)
	{
	   $mylib = new globallib();
	   
	   if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.NoDokumen";
				$arr_keyword[1] = "a.NoReferensi";
				$arr_keyword[2] = "a.NoPV";
				$arr_keyword[3] = "a.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}			
			
		} 	
		
       $sql = "  
           SELECT 
			  a.*,
			  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
			  b.`NamaKasBank`,
			  c.`NamaSubDivisi`,
			  d.`employee_name` 
			FROM
			  uang_muka a 
			  INNER JOIN kasbank b 
			    ON a.`KdKasBank` = b.`KdKasBank` 
			  INNER JOIN subdivisi c 
			    ON a.`KdSubDivisi` = c.`KdSubDivisi` 
			  INNER JOIN employee d 
			    ON a.`Employee` = d.`employee_id` 
			WHERE 1 $where_keyword
			ORDER by TglDokumen desc,  a.NoDokumen  
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getinterface(){
		$sql = "select * from interface";
		$hasil = $this->getArrayResult($sql);
		return $hasil[0];
		
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>