<?php

class Revisi_pembayaranmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function getReport($nostruk) 
    {
    	$sql = "
    		SELECT
                tbl_uni.NoKassa,
                tbl_uni.Kasir,
                tbl_uni.KdDivisi,
                tbl_uni.NamaDivisi,
                tbl_uni.Tanggal,
                tbl_uni.Waktu,
                tbl_uni.NoStruk,
                tbl_uni.Bruto,
                tbl_uni.DiscDetail,
                tbl_uni.Netto,
                tbl_uni.Tunai,
                tbl_uni.KDebit,
                tbl_uni.KKredit,
                tbl_uni.Voucher,
                tbl_uni.Discount,
                tbl_uni.TotalNilai,
                tbl_uni.TotalBayar,
                tbl_uni.Kembali,
                tbl_uni.Total_Charge,
                tbl_uni.TAX
            FROM
            (
                
            SELECT
                `transaksi_header`.NoKassa,
                `transaksi_header`.Kasir,
                `divisi`.KdDivisi,
                `divisi`.NamaDivisi,
                `transaksi_header`.Tanggal,
                `transaksi_header`.Waktu,
                `transaksi_header`.NoStruk,
                det.Bruto,
                det.DiscDetail,
                det.Netto,
                COALESCE(`transaksi_header`.Tunai, NULL, '0') AS Tunai,
                COALESCE(`transaksi_header`.KDebit, NULL, '0') AS KDebit,
                COALESCE(`transaksi_header`.KKredit, NULL, '0') AS KKredit,
                COALESCE(`transaksi_header`.Voucher, NULL, '0') AS Voucher,
                COALESCE(`transaksi_header`.Discount, NULL, '0') AS Discount,
                COALESCE(`transaksi_header`.TotalNilai, NULL, '0') AS TotalNilai,
                COALESCE(`transaksi_header`.TotalBayar, NULL, '0') AS TotalBayar,
                COALESCE(`transaksi_header`.Kembali, NULL, '0') AS Kembali,
                COALESCE(`transaksi_header`.Ttl_Charge, NULL, '0') AS Total_Charge,
                COALESCE(`transaksi_header`.TAX, NULL, '0') AS TAX
            FROM
                `transaksi_header`
            INNER JOIN kassa
                ON transaksi_header.NoKassa = kassa.id_kassa
            INNER JOIN divisi
                ON kassa.KdDivisi = divisi.KdDivisi
            INNER JOIN (SELECT NoStruk, 
                        SUM(Qty*Harga) AS Bruto, 
                        SUM(IFNULL(Disc1,0)+IFNULL(Disc2,0)+IFNULL(Disc3,0)+IFNULL(Disc4,0)) AS DiscDetail, 
                        SUM(Netto) AS Netto 
                        FROM transaksi_detail
                        GROUP BY NoStruk) as det
                ON transaksi_header.NoStruk = det.NoStruk
            WHERE
                1
                AND `transaksi_header`.NoStruk = '".$nostruk."'
                AND `transaksi_header`.status = '1'
            
            UNION ALL
            
            SELECT
                '' AS NoKassa,
                `trans_reservasi`.Kasir,
                `divisi`.KdDivisi,
                `divisi`.NamaDivisi,
                `trans_reservasi`.TglDokumen AS Tanggal,
                '00:00:00' AS Waktu,
                `trans_reservasi`.NoDokumen AS NoStruk,
                '0' AS Bruto,
                `trans_reservasi`.dp AS DiscDetail,
                `trans_reservasi`.Total AS Netto,
                COALESCE(`trans_reservasi`.Sisa_Tunai, NULL, '0') AS Tunai,
                COALESCE(`trans_reservasi`.Sisa_Debit, NULL, '0') AS KDebit,
                COALESCE(`trans_reservasi`.Sisa_Kredit, NULL, '0') AS KKredit,
                '0' AS Voucher,
                '0' AS Discount,
                COALESCE(`trans_reservasi`.Total, NULL, '0') AS TotalNilai,
                (`trans_reservasi`.Sisa_Tunai + `trans_reservasi`.Sisa_Debit + `trans_reservasi`.Sisa_Kredit) AS TotalBayar,
                '0' AS Kembali,
                '0' AS Total_Charge,
                '0' AS TAX
            FROM
                `trans_reservasi`
                INNER JOIN `divisi` ON
                    `trans_reservasi`.KdDivisi = `divisi`.KdDivisi
            WHERE
                1
                AND `trans_reservasi`.NoDokumen = '".$nostruk."'
            ) AS tbl_uni 
            ORDER BY
                tbl_uni.KdDivisi ASC,
                tbl_uni.Tanggal ASC,
                tbl_uni.Waktu ASC,
                tbl_uni.NoStruk ASC
    	";
    	
    	return $this->getRow($sql);
    	
    }
    
    function getVoucher($nostruk) 
    {
    	$sql="
    		SELECT 
			  transaksi_detail_voucher.NoKassa,
			  transaksi_detail_voucher.NoStruk,
			  DATE_FORMAT(transaksi_detail_voucher.Tanggal, '%d-%m-%Y') AS Tanggal,
			  transaksi_detail_voucher.Jenis,
			  transaksi_detail_voucher.NomorVoucher,
			  transaksi_detail_voucher.NilaiVoucher 
			FROM
			  transaksi_detail_voucher 
			WHERE 
			  1
			  AND transaksi_detail_voucher.NoStruk = '".$nostruk."'
			ORDER BY 
			  transaksi_detail_voucher.Tanggal DESC
    	";
    	return $this->getRow($sql);
		
	}
    
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) 
    {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
}

?>