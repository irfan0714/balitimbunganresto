<?php
class Paymentmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getpaymentList($num,$offset,$id,$with,$thnbln)
	{
	
		$user = $this->session->userdata('username');
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        $clause="where(h.status <> 'B' OR h.status IS NULL) ";
//		$clause="where h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			elseif($with='dibayarkepada'){
				$clause .= " and Penerima like '%$id%'";
			}else{
				$clause .= " and $with = '$id'";
			}
		}
    	$sql = "select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,
                NoBukti,Keterangan,JumlahPayment,NamaKasBank,NamaCostCenter,NamaPersonal,user.UserName,
                Penerima,
                h.Jenis,NoGiro,KdBankCair
				from trans_payment_header h 
				inner join kasbank on kasbank.KdKasBank = h.KdKasBank
				left join costcenter on costcenter.KdCostCenter = h.KdCostCenter
				left join personal on personal.KdPersonal = h.KdPersonal
				left join user on user.id = h.AddUser
				INNER JOIN userkasbank uk ON uk.`UserName`='$user'  AND uk.`KdKasBank`=kasbank.`KdKasBank`
				$clause 
				order by TglDokumen desc Limit $offset,$num
			";
		return $this->getArrayResult($sql);
    }
    
    function num_payment_row($id,$with,$thnbln){
        $clause="where (status <> 'B' OR status IS NULL) ";
        $user = $this->session->userdata('username');
//     	$clause="where Status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}elseif($with='dibayarkepada'){
				$clause .= " and Penerima like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
		$sql = "SELECT NoDokumen FROM trans_payment_header h
		INNER JOIN userkasbank uk ON uk.`UserName`='$user'  AND uk.`KdKasBank`=h.`KdKasBank`
		$clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans,DefaultKdDepartemen,DefaultKdProject,DefaultKdCostCenter from aplikasi";
        return $this->getRow($sql);
    }
	function getDivisi(){
    	$sql = "SELECT KdDivisi,NamaDivisi from divisi order by KdDivisi";
		return $this->getArrayResult($sql);
    }

    function getSubdivisi(){
    	$sql = "SELECT CONCAT(KdDivisi,KdSubDivisi)as kode,KdSubDivisi,NamaSubDivisi from subdivisi order by NamaSubDivisi";
		return $this->getArrayResult($sql);
    }
 
    function getKasBank($user){
    	$sql = "SELECT kasbank.KdKasBank,kasbank.NamaKasBank FROM kasbank
		INNER JOIN userkasbank  ON userkasbank.KdKasBank=kasbank.KdKasBank
		WHERE  userkasbank.UserName='$user'
		ORDER BY kasbank.KdRekening
		";
		return $this->getArrayResult($sql);
    }
    function getDept(){
    	$sql = "SELECT KdDepartemen,NamaDepartemen from departemen order by KdDepartemen";
		return $this->getArrayResult($sql);
    }
	function getPersonal(){
    	$sql = "SELECT KdPersonal,NamaPersonal from personal order by KdPersonal";
		return $this->getArrayResult($sql);
    }
	function getCostCenter(){
		$sql = "SELECT KdCostCenter,NamaCostCenter FROM costcenter order by KdCostCenter";
		return $this->getArrayResult($sql);
	}
	function cekgiro($nogiro){
    	$sql = "SELECT * from bukugiro where NoGiro='$nogiro'";
		return $this->getArrayResult($sql);
    }
    function findrekening($id){
		$sql = "SELECT kdrekening,namarekening FROM rekening Where kdrekening='$id' and tingkat='3'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getKodeBank($kd){

    	$sql = "SELECT KdPembayaran FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    function cek($pv){

    	$sql = "SELECT * FROM `pelunasan_hutang_header` a WHERE a.`NoPaymentVoucher`='".$pv."';";

        return $this->getRow($sql);

    }

	function getNewNo($tgl)
	{
		$tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoPayment=NoPayment+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoPayment FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	function getGiro($id)
	{
		$sql = "select Status from bukugiro Where NoTransaksi='$id'";
        return $this->getArrayResult($sql);
	}
	function getDetail($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,concat(d.KdRekening,' - ',NamaRekening) as NamaRekening,KdSubDivisi,KdDepartemen,Urutan from(
		SELECT * from trans_payment_detail Where NoDokumen='$id' and ( STATUS<>'B' OR STATUS IS NULL)
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,NamaRekening,Urutan from(
		SELECT * from trans_payment_detail Where NoDokumen='$id' and (Status<>'B' or Status is NULL)
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
        $sql = "select NoDokumen,Tanggal,NamaKasBank,NamaCostCenter,NamaPersonal,NoBukti,Keterangan,h.KdKasBank,h.KdCostCenter,h.KdPersonal,
            JumlahPayment,Jenis,NoGiro,KdBankCair,TglCair,Penerima, Lampiran1, Lampiran2
            from(
            SELECT NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,NoBukti,Keterangan,Penerima,
            KdKasBank,KdCostCenter,KdPersonal,JumlahPayment,Jenis,NoGiro,KdBankCair,date_format(TglCair,'%d-%m-%Y') as TglCair, Lampiran1, Lampiran2
            from trans_payment_header where NoDokumen='$id' and ( STATUS<>'B' OR STATUS IS NULL))h
            left JOIN
            (
            select KdKasBank,NamaKasBank from kasbank
            )kasbank
            on kasbank.KdKasBank = h.KdKasBank
            left JOIN
            (
            select KdCostCenter,NamaCostCenter from costcenter
            )costcenter
            on costcenter.KdCostCenter = h.KdCostCenter
            left JOIN
            (
            select KdPersonal,NamaPersonal from personal
            )personal
            on personal.KdPersonal = h.KdPersonal;";
            return $this->getRow($sql);
	}
	
	function getHasilCek($id)
	{
        $sql = "
        		SELECT 
				  * 
				FROM
				  `pelunasan_hutang_header` 
				WHERE `pelunasan_hutang_header`.`NoPaymentVoucher` = '".$id."' ;
        	   ";
            return $this->getRow($sql);
	}
	
	function getReferensi($id)
	{
        
    	$sql = "  
		        SELECT * FROM `jurnalheader` a WHERE a.`NoTransaksi`='$id';
		        ";    
		return $this->getRow($sql);
    }
	
	function getLampiran($nodokumen){
		$sql = "select Lampiran1, Lampiran2 from trans_payment_header where NoDokumen='$nodokumen'";
		return $this->getArrayResult($sql);
	}
	
	function getrekening($kdrekening){
		$sql = "Select concat(KdRekening,' - ',NamaRekening) as KdRekening 
				from rekening where Tingkat='3' and kdrekening>='11020901' and kdrekening like '$kdrekening%' order by kdrekening";
		$row=$this->getArrayResult($sql);
		return $row;
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>