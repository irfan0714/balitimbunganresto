<?php

class komisikhusus_model extends CI_Model
{

	//put your code here
	function __construct()
	{
		parent::__construct();
	}

	function getDetailForPrint($id)
	{
		$sql = "SELECT d.*, date_format(TglJual,'%d-%m-%Y') as Tgl, NamaLengkap FROM finance_komisi_khusus_detail d 
		INNER JOIN masterbarang
		ON masterbarang.`PCode`=d.PCode
		WHERE NoTransaksi='$id' ORDER BY PCode";
		//echo $sql;
		return $this->getArrayResult($sql); 
	}

	function dovoid($noregister,$nostruknya,$kodetrans,$nodok){

		// $flag = 0;
		// print_r($noregister);
		// echo "<br>";
		// print_r($nostruknya);
		$dataregister = array(
			'statuskomisi_khusus'=> '0'
		);
		$this->db->update('register', $dataregister, array('KdRegister'=> $noregister));
		// if($this->db->affected_rows() > 0){
		// 	$flag += 1;
		// }
		$datatransaksi = array(
			'statuskomisi_khusus'=> '0'
		);
		$this->db->update('transaksi_header', $datatransaksi, array('NoStruk'=> $nostruknya));
		// if($this->db->affected_rows() > 0){
		// 	$flag += 1;
		// }
		$datakomisi = array(
			'Status'=> '2'
		);
		$this->db->update('finance_komisi_khusus_header', $datakomisi, array('NoTransaksi'=> $kodetrans));
		// if($this->db->affected_rows() > 0){
		// 	$flag += 1;
		// }
		$datapayment = array(
			'Status'=> '2'
		);
		$this->db->update('trans_payment_header', $datapayment, array('NoDokumen'=> $nodok));
		// if($this->db->affected_rows() > 0){
		// 	$flag += 1;
		// }
		print_r($flag);
		// if($flag == 4){
		// 	return true
		// }else{
		// 	return false;
		// }
		return true;
	}

	function getVoid($nostiker,$kdagen)
	{
		// $sql2 = "SELECT * FROM transaksi_header WHERE KdAgent = 'SU0147'";
		$sql = "SELECT a.NoStruk, b.KdRegister FROM transaksi_header a INNER JOIN register b WHERE a.KdAgent = '$nostiker' AND b.NoStiker = '$nostiker' AND a.Tanggal = b.Tanggal AND b.KdTourLeader = '$kdagen'";
		$row = $this->db->query($sql);
		// $komisioh = $row->row_array();
	  return $row->row_array();
	}

	function getHeaderForPrint($id)
	{
		
		$sql = "SELECT
        a.NoTransaksi, DATE_FORMAT(a.TglTransaksi, '%d-%m-%Y') AS TglTransaksi, a.KdAgent, a.Total AS TotKomisi, a.TotalSales AS TotSales
        FROM
        finance_komisi_khusus_header a
        WHERE NoTransaksi = '$id'
        GROUP BY KdAgent";
		
		
		return $this->getRow($sql);
	}
	function getHeaderForPrint2($id)
	{		
		$sql = "SELECT 
		  SUM(a.qty * a.harga -a.Potongan ) AS total,
		  -- SUM(a.hargaPPN) AS total,
		  SUM(((a.qty * a.harga) - a.Potongan) * a.persentase / 100) AS komisi,m.KdDivisi,d.NamaDivisi
		FROM
		  (SELECT * FROM finance_komisi_khusus_detail WHERE NoTransaksi = '$id') a
		INNER JOIN masterbarang m ON m.PCode = a.PCode
		INNER JOIN divisi d ON d.KdDivisi=m.KdDivisi  
		GROUP BY m.KdDivisi ;";		
		return $this->getArrayResult($sql);
	}

	function getCountDetail($id)
	{
		$sql = "SELECT * FROM finance_komisi_khusus_detail where NoTransaksi='$id'";
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function getKomisiList($num, $offset, $id, $with)
	{
		$month_cek = $this->aplikasi();
		
		$bln1 = $month_cek[0]['TglTrans'];
		$bln = substr($bln1,5,2);
		if($offset != ''){
			$offset = $offset;
		}
		else
		{
			$offset = 0;
		}
		$clause = "";
		if($id != ""){
			if($with == "NoTransaksi"){
				$clause = "AND $with like '%$id%'";
			}
			else
			{
				$clause = "AND $with = '$id'";
			}
		}
		$sql = "SELECT 	NoDokumen,NoTransaksi, date_format(TglTransaksi,'%d-%m-%Y') AS TglTransaksi, KdAgent, Nama, Keterangan, Kategori, Total, TotalSales, NoSticker
		FROM finance_komisi_khusus_header WHERE 1 $clause
		ORDER BY NoTransaksi DESC limit $offset,$num";
		//WHERE $clause
		//echo $sql;
		return $this->getArrayResult($sql);
	}

	function getHeader($id)
	{
		$sql = "SELECT finance_komisi_khusus_header.NoTransaksi, date_format(finance_komisi_khusus_header.TglTransaksi,'%d-%m-%Y') AS TglTransaksi, KdAgent, t.Nama, finance_komisi_khusus_header.Keterangan, Total,TotalSales, Pembulatan as Payment
		FROM finance_komisi_khusus_header inner join tourleader t on finance_komisi_khusus_header.KdAgent = t.KdTourLeader
		WHERE NoTransaksi='$id'
		ORDER BY NoTransaksi DESC";
		//echo $sql;
		return $this->getRow($sql);
	}
	
	function getHeaderForExcel($id)
	{
		$sql = "SELECT NoTransaksi, date_format(TglTransaksi,'%d-%m-%Y') AS TglTransaksi, KdAgent, t.Nama, Keterangan, Total,TotalSales
		FROM finance_komisi_khusus_header inner join tourleader t on finance_komisi_khusus_header.KdAgent = t.KdTourLeader
		WHERE NoTransaksi='$id'
		ORDER BY NoTransaksi DESC";
		//echo $sql;
		return $this->getArrayResult($sql);
	}

	function getDetail($id)
	{
		$sql = "SELECT a.NoTransaksi, b.PCode, c.NamaLengkap, b.Qty, b.Harga, Persentase, Potongan, Komisi1, Komisi2, Komisi3, Komisi4 
		FROM finance_komisi_khusus_header a, finance_komisi_khusus_detail b, masterbarang c
		WHERE a.NoTransaksi = b.NoTransaksi AND b.PCode = c.PCode
		AND a.NoTransaksi='$id' ORDER BY NoTransaksi DESC, b.AddDate Desc";

		return $this->getArrayResult($sql);
	}

	function num_komisi_row($id, $with)
	{
		$clause = "";
		if($id != ''){
			if($with == "NoTransaksi"){
				$clause = "WHERE $with like '%$id%'";
			}
			else
			{
				$clause = "WHERE $with = '$id'";
			}
		}
		$sql = "SELECT NoTransaksi FROM finance_komisi_khusus_header $clause";
		return $this->NumResult($sql);
	}

	function getKomisi($field,$tglagen)
	{
		$sql2 ="SELECT KomisiGOH FROM setup";
	$row = $this->db->query($sql2);
	$komisioh = $row->row_array();

	$sql="	
		SELECT 
			  a.NoStruk,
			  a.Tanggal AS TglJual,
			  a.Waktu,
			  a.KdAgent,
			  b.PCode,
			  c.NamaLengkap,
			  b.Qty,
			  b.`Harga` * Qty AS hrgPPN,
			  '0' as komisi1,
			  			  '0' as komisi2,
						  			  '0' as komisi3,
														'0' as komisi4,
															'0' as komisi5,
			  ROUND(
				IF(
				  b.Service_charge = 5,
				  b.Harga,
				  b.Harga / 1.1
				)
			  ) AS Harga,
			  (
				ROUND(
				  b.Qty * IF(
					b.Service_charge = 5,
					b.Harga,
					b.Harga / 1.1
				  ) - IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				)
			  ) AS Disc,
			  (
				ROUND(
				  IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  ),
				  0
				)
			  ) AS ttlnetto,
			  IFNULL(b.Komisi, 0) AS Komisi,
			  IFNULL(
				ROUND(
				  b.Komisi / 100 * IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				),
				0
			  ) AS Nilai 
			FROM
			  transaksi_header a,
			  transaksi_detail b,
			  masterbarang c 
			WHERE (
				(
				 $tglagen
				)
			  ) 
			  AND a.nostruk = b.nostruk 
			  AND b.pcode = c.pcode 
			  AND a.statuskomisi_khusus = '0'
			  AND a.status='1'
				AND b.Status='1'
				AND c.kdsubdivisi IN('01','06') 
				AND c.status='A' 
				AND c.harga1c>0 
				AND c.komisi>0
			  and a.userdisc <> 'office' 
			  ";



		//echo "Cek di jika tidak ada agent ada di komisi header<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}

	function getKomisiOH($field,$tglagen)
	{
		$sql2 ="SELECT KomisiGOH FROM setup";
	$row = $this->db->query($sql2);
	$komisioh = $row->row_array();
	$komisivalue = '';
		if(!empty($komisioh)){
			$komisivalue = $komisioh["KomisiGOH"];
		}
	//print_r($komisivalue);
	
	$sql="	
		SELECT 
			  a.NoStruk,
			  a.Tanggal AS TglJual,
			  a.Waktu,
			  a.KdAgent,
			  b.PCode,
			  c.NamaLengkap,
			  b.Qty,
			  b.`Harga` * Qty AS hrgPPN,
			  '0' as komisi1,
			  			  '0' as komisi2,
						  			  '0' as komisi3,
														'0' as komisi4,
															'0' as komisi5,
			  ROUND(
				IF(
				  b.Service_charge = 5,
				  b.Harga,
				  b.Harga / 1.1
				)
			  ) AS Harga,
			  (
				ROUND(
				  b.Qty * IF(
					b.Service_charge = 5,
					b.Harga,
					b.Harga / 1.1
				  ) - IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				)
			  ) AS Disc,
			  (
				ROUND(
				  IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  ),
				  0
				)
			  ) AS ttlnetto,
			  IFNULL($komisivalue, 0) AS Komisi,
			  IFNULL(
				ROUND(
				  $komisivalue / 100 * IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				),
				0
			  ) AS Nilai 
			FROM
			  transaksi_header a,
			  transaksi_detail b,
			  masterbarang c 
			WHERE (
				(
				 $tglagen
				)
			  ) 
				AND a.nostruk = b.nostruk 
				AND a.Tanggal >= '2018-08-10'
			  AND b.pcode = c.pcode 
			  AND a.statuskomisi_khusus = '0'
			  AND a.status='1'
				AND b.Status='1'
				AND c.status='A'
				AND c.komisi>0
				AND c.harga1c>0 
			  and a.userdisc <> 'office' 
			  AND c.NamaLengkap NOT LIKE '%80K%'
			  ";

		//print_r($sql);

		//echo "Cek di jika tidak ada agent ada di komisi header<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		// print_r($qry);
		$row = $qry->result_array();  
		
		// print_r($row);
		
		if(!$row){
			echo '<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-ban"></i> Data Tidak Ditemukan!!</h4>
			Kode Agen Salah atau Agen Sudah Mengambil Komisi Khususnya. Terimakasih.
		</div>';
		}

		return $row;
	}

	function getKomisiOH30($field,$tanggal)
	{
		$sql2 ="SELECT KomisiGOH FROM setup";
	$row = $this->db->query($sql2);
	$komisioh = $row->row_array();
	$komisivalue = '';
		if(!empty($komisioh)){
			$komisivalue = $komisioh["KomisiGOH"];
		}
	//print_r($komisivalue);
	
	$sql="	
		SELECT 
			  a.NoStruk,
			  a.Tanggal AS TglJual,
			  a.Waktu,
			  a.KdAgent,
			  b.PCode,
			  c.NamaLengkap,
			  b.Qty,
			  b.`Harga` * Qty AS hrgPPN,
			  '0' as komisi1,
			  			  '0' as komisi2,
						  			  '0' as komisi3,
														'0' as komisi4,
															'0' as komisi5,
			  ROUND(
				IF(
				  b.Service_charge = 5,
				  b.Harga,
				  b.Harga / 1.1
				)
			  ) AS Harga,
			  (
				ROUND(
				  b.Qty * IF(
					b.Service_charge = 5,
					b.Harga,
					b.Harga / 1.1
				  ) - IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				)
			  ) AS Disc,
			  (
				ROUND(
				  IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  ),
				  0
				)
			  ) AS ttlnetto,
			  IFNULL($komisivalue, 0) AS Komisi,
			  IFNULL(
				ROUND(
				  $komisivalue / 100 * IF(
					b.Service_charge = 5,
					b.Netto,
					b.Netto / 1.1
				  )
				),
				0
			  ) AS Nilai 
			FROM
			  transaksi_header a,
			  transaksi_detail b,
				masterbarang c,
				register d 
			WHERE (
				(
				 $tanggal
				)
				) 
			  AND a.nostruk = b.nostruk 
			  AND b.pcode = c.pcode 
			  AND a.statuskomisi_khusus = '0'
			  AND a.status='1'
				AND b.Status='1'
				AND c.kdsubdivisi IN('01','06') 
				AND c.status='A' 
				AND c.harga1c>0 
				AND c.komisi>0
				AND d.NoStiker = a.KdAgent
				AND d.Tanggal = a.Tanggal
				AND d.KdTourLeader = '$field'
			  and a.userdisc <> 'office' 
			  AND c.NamaLengkap NOT LIKE '%80K%'
			  ";



		//echo "Cek di jika tidak ada agent ada di komisi header<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();  
		
		// print_r($row);
		if(!$row){
			echo '<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-ban"></i> Data Tidak Ditemukan!</h4>
			Kode Agen Salah atau Agen Sudah Mengambil Komisi Khususnya. Terimakasih.
		</div>';
		}

		return $row;
	}
	
	function getKomisiTicket($field,$tanggal,$nosticker)
	{
	        $sql2 ="SELECT KomisiGOH FROM setup";
			$row = $this->db->query($sql2);
			$komisioh = $row->row_array();
			$komisivalue = '';
				if(!empty($komisioh)){
					$komisivalue = $komisioh["KomisiGOH"];
				}

			$sql="
					SELECT DISTINCT
					  d.`notrans`,
					  d.PCode,
					  d.tgl_berlaku,
					  SUM(d.qty) AS quantity,
					  SUM(d.harga/1.1) AS ttlnettos,
					  $komisivalue AS komisi,
					  '0' AS komisi1,
					  '0' AS komisi2,
					  '0' AS komisi3,
					  '0' AS komisi4,
					  e.NamaLengkap,
					   d.`harga`/1.1 AS price,
					  SUM(d.`qty` * d.`harga`/1.1) * $komisivalue / 100 AS komisi_ticket 
					FROM
					   register a  
					  INNER JOIN `ticket_customer` c 
					    ON c.no_identitas = a.NoStiker 
					    AND c.add_date = a.Tanggal 
					  INNER JOIN `ticket` d 
					    ON c.`notrans` = d.`notrans` 
					  INNER JOIN masterbarang e 
					    ON d.`PCode` = e.`PCode` 
					WHERE 1 
					  AND a.statuskomisi_khusus = 0 
					  AND d.statuskomisi_khusus = 0
					  AND a.Jumlah > 0 
					  AND ($tanggal)
					  AND a.Tanggal >='2018-12-01'
						AND e.komisi>0
						AND d.PCode NOT IN (
							'TCK000009',
							'TCK0000014',
							'TCK000003',
							'TCK0000031',
							'TCK0000032',
							'TCK0000033',
							'TCK0000034',
							'TCK0000035',
							'TCK0000036',
							'TCK0000037')
					GROUP BY d.`notrans`,
  					d.PCode ;
				";
		// echo "Dapeting Komisi Ticket <br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	function getKomisiTicketAgent($kdtravel,$tglagen)
	{
	       
			$sql="
					SELECT DISTINCT
					  d.`notrans`,
					  d.PCode,
					  d.tgl_berlaku,
					  SUM(d.qty) AS quantity,
					  SUM(d.harga/1.1) AS ttlnettos,
					  kom.total,
					  kom.komisi1 as komisi,
					  '0' AS komisi1,
					  kom.komisi2,
					  kom.komisi3,
						kom.komisi4,
						kom.komisi5,
					  e.NamaLengkap,
					  d.`harga`/1.1 AS price,
					  SUM(d.`qty` * d.`harga`/1.1) * kom.total / 100 AS komisi_ticket 

					FROM
					   register a  
					  INNER JOIN `ticket_customer` c 
					    ON c.no_identitas = a.NoStiker  
					    AND c.add_date = a.Tanggal 
					  INNER JOIN `ticket` d 
					    ON c.`notrans` = d.`notrans` 
					  INNER JOIN masterbarang e 
					    ON d.`PCode` = e.`PCode` 
					inner JOIN 
					    (SELECT 
					      kdtravel,
					      pcode AS dpcode,
					      total,
					      komisi1,
					      komisi2,
					      komisi3,
					      komisi4,
								komisi4bag,
								komisi5 
					    FROM
					      komisi_detail) kom 
					    ON kom.kdtravel = '$kdtravel' 
					    AND d.PCode = kom.dpcode
					WHERE 1 
					  AND a.statuskomisi_khusus = 0 
					  AND d.status = 0
					  AND a.Jumlah > 0 
					  AND ($tglagen) 
					  AND a.Tanggal >='2017-08-01'
					GROUP BY d.`notrans`,
  					d.PCode ;
				";
		//echo "Dapeting Komisi Ticket <br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	function getKomisiTravel($kdtravel,$tglagen)
	{
	$sql="	
		
		SELECT 
		  det.NoStruk,
		  det.TglJual,
		  det.Waktu,
		  det.KdAgent,
		  det.PCode,
		  det.NamaLengkap,
		  det.Harga,
		  SUM(((det.Qty*det.Harga)-det.Disc)) * kom.total/100 AS Nilai,
  		  sum(det.Disc) as Disc,
			det.hrgPPN,
		  SUM(det.Qty) AS Qty,
		  SUM(det.ttlnetto) AS ttlnetto,
		  kom.total AS Komisi,
		  kom.komisi1,
		  kom.komisi2,
		  kom.komisi3,
			kom.komisi4,
			kom.komisi5 
		FROM
		  (SELECT 
		    a.NoStruk,
		    a.Tanggal AS TglJual,
		    a.Waktu,
		    a.KdAgent,
		    b.PCode,
		    c.NamaLengkap,
			b.Harga* Qty AS hrgPPN,
		    b.Qty,
		    b.Service_charge,
		    b.Netto,
		    ROUND(
		      IF(
		        b.Service_charge = 5,
		        b.Harga,
		        b.Harga / 1.1
		      )
		    ) AS Harga,
		    (
		      ROUND(
		        b.Qty * IF(
		          b.Service_charge = 5,
		          b.Harga,
		          b.Harga / 1.1
		        ) - IF(
		          b.Service_charge = 5,
		          b.Netto,
		          b.Netto / 1.1
		        )
		      )
		    ) AS Disc,
		    (
		      ROUND(
		        IF(
		          b.Service_charge = 5,
		          b.Netto,
		          b.Netto / 1.1
		        ),
		        0
		      )
		    ) AS ttlnetto,
		    IFNULL(b.Komisi, 0) AS Komisi ,
		    IFNULL(
		    ROUND(
		      b.Komisi / 100 * IF(
		        b.Service_charge = 5,
		        b.Netto,
		        b.Netto / 1.1
		      )
		    ),
		    0
		  ) AS Nilai 
		  FROM
		    transaksi_header a,
		    transaksi_detail b,
		    masterbarang c 
		  WHERE (
		      (
		     	$tglagen
		      )
		    ) 
		    AND a.nostruk = b.nostruk 
		    AND b.pcode = c.pcode 
				AND a.statuskomisi = '0'
				AND c.kdsubdivisi IN('01','06') 
				AND c.status='A' 
				AND c.harga1c>0 
				AND c.komisi>0
		    and a.userdisc<>'office') det 
		  inner join register r on det.KdAgent=r.NoStiker and r.KdTravel ='$kdtravel' and det.TglJual=r.Tanggal and r.jumlah>0
		  inner JOIN 
		    (SELECT 
		      kdtravel,
		      pcode AS dpcode,
		      total,
		      komisi1,
		      komisi2,
		      komisi3,
		      komisi4,
					komisi4bag,
					komisi5 
		    FROM
		      komisi_detail) kom 
		    ON kom.kdtravel = '$kdtravel' 
		    AND det.pcode = kom.dpcode 
		GROUP BY NoStruk,PCode ";
        //echo "Cek di jika agent ada di komisi header<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;die();
		$row = $qry->result_array();   

		return $row;
	}
	
	//melakukan perubahan pada 22/11/2016 dengan mengubah `KdTourLeader`='$field'
	function ambilReq($field,$tgl2,$nosticker)
	{	
		if($nosticker==''){
			$wheresticker = '';
		}else{
			$wheresticker = " and NoStiker='$nosticker' ";
		}
		
		/*$sql = "
			SELECT 
			  NoStiker,
			  Tanggal 
			FROM
			  register 
			WHERE statuskomisi = '0' and KdTourLeader='$field' $wheresticker
			  and jumlah>0
			  AND Tanggal BETWEEN DATE_SUB(NOW(), INTERVAL 31 DAY) AND NOW() group by NoStiker, Tanggal";*/
			  
		$sql = "
			SELECT 
			  NoStiker,
			  Tanggal 
			FROM
			  register a
			WHERE a.statuskomisi_khusus = '0' and a.KdTourLeader='$field' $wheresticker
			  and a.jumlah>0
			  AND Tanggal BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND a.Tanggal >= '2018-08-10' ORDER BY a.Tanggal DESC LIMIT 0,1";
		//echo "Cek di register<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();
		//print_r($row);
		return $row;
	}
	
	function CekAgent($field, $nosticker)
	{	$sql = "
			SELECT DISTINCT k.* FROM register 
			INNER JOIN komisi_header k ON k.`KdTravel`=register.`KdTravel`
			WHERE register.`KdTourLeader`='$field' and register.NoStiker='$nosticker'
			  ";
		//echo "Cek di agent di regiter dan komisi header<br>".$sql."<br><br>";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();
		return $row;
	}

	function ValAgent($field){
		 $sql = "SELECT * FROM register WHERE KdTourLeader='$field'";
		$row = $this->db->query($sql);
		return $row->result_array();
	}
	
	function Querydata($sql){
        $qry 	= $this->db->query($sql);
        return $qry->result_array();
	}
		
	function cekKomisi($stiker,$tgl)
	{
		$sql = "SELECT DISTINCT(NoStruk) FROM `finance_komisi_khusus_detail` a
				INNER JOIN `finance_komisi_khusus_header` b
				ON a.`NoTransaksi` = b.`NoTransaksi` AND b.`KdAgent` ='$stiker' 
				 AND b.TglTransaksi BETWEEN DATE_SUB('$tgl', INTERVAL 31 DAY) AND '$tgl'
					
				";
		$qry = $this->db->query($sql); //echo $sql;
		$row = $qry->result_array();

		return $row;
	}

	function getNewNo($tahun,$bulan)
	{
		$sql = "SELECT NoKomisi FROM counter where Tahun='$tahun' AND Bulan='$bulan'";
		return $this->getRow($sql);
	}

	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function NamaPrinter($id)
	{
		$sql = "SELECT * from kassa where ip='$id'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function getDate()
	{
		$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
		return $this->getRow($sql);
	}

	function getTotalNetto($no)
	{
		$sql = "SELECT SUM(Netto) FROM transaksi_detail WHERE NoStruk ='$no'";
		return $this->getArrayResult($sql);
	}

	function getWaktu($no)
	{
		$sql = "SELECT MIN(waktu) AS mulai, MAX(waktu) AS akhir FROM transaksi_detail WHERE NoStruk = '$no'";
		return $this->getArrayResult($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}

	function ifPCodeBarcode($id)
	{
		$bar = substr($id, 0, 10);
		$sql = "SELECT KdRekening FROM rekening Where KdRekening='$id'";
		return $this->getRow($sql);
	}

	function getPCodeDet($kode, $field)
	{
		$sql = "select KdRekening, NamaRekening from rekening where KdRekening='$kode'";
		return $this->getRow($sql);
	}
	
	public function getTrans($id)
	{
		$this->db->from('finance_komisi_khusus_header');
		$this->db->where('NoTransaksi',$id);
		$query = $this->db->get();
		return $query->row();
        
	}

}

?>
