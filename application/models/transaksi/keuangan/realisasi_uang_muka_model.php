<?php
class Realisasi_uang_muka_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
	
	function getListRealisasiUangMuka($id){

    	$sql = "SELECT 
    				a.*,
    				date_format(TglDokumen,'%d-%m-%Y') as Tanggal
    	        FROM realisasi_uang_muka a WHERE a.NoDokumen ='".$id."'";
        return $this->getRow($sql);

    }
    
    function getListRealisasiUangMukaPdf($id){

    	$sql = "SELECT 
				  a.*,
				  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
				  b.`employee_name`,
				  d.`KdRekening`,
				  d.`NamaKasBank` 
				FROM
				  realisasi_uang_muka a 
				  INNER JOIN employee b 
				    ON a.`Employee` = b.`employee_id` 
				  INNER JOIN `kasbank` d 
				    ON a.`KdKasBank` = d.`KdKasBank`
				 WHERE a.NoDokumen ='".$id."'";
        return $this->getRow($sql);

    }
    
    function cekKdTransaksi($v_no_dokumen, $kdrekening){
    	$sql = " SELECT * FROM `realisasi_uang_muka_payment` a WHERE a.`NoDokumen`='".$v_no_dokumen."' AND a.`KdRekening`='".$kdrekening."';";
		return $this->getRow($sql);
	}
	
	function getRealisasiUangMukaList($limit,$offset,$arrSearch)
	{
		
		$mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
	
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.NoDokumen";
				$arr_keyword[1] = "a.NoReferensi";
				$arr_keyword[2] = "a.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		} 
		
       $sql = "  
           SELECT 
			  a.*,
			  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
			  b.`NamaKasBank`,
			  d.`employee_name` 
			FROM
			  realisasi_uang_muka a 
			  INNER JOIN kasbank b 
			    ON a.`KdKasBank` = b.`KdKasBank` 
			  INNER JOIN employee d 
			    ON a.`Employee` = d.`employee_id` 
			WHERE 1 $where_keyword and a.Status<>'2'
			ORDER BY Year(TglDokumen) desc, month(TglDokumen) desc, a.NoDokumen asc
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getListRealisasiDetail($id){
		$sql = "SELECT 
				  *
				FROM
				  `realisasi_uang_muka_detail` a
				INNER JOIN `uang_muka` b 
					ON a.`NoUM` = b.`NoDokumen`
				WHERE a.`NoDokumen` = '".$id."';";
    	
        return $this->getArrayResult($sql);

    }
    
    function getNoPayReceipt($id){
		$sql = "SELECT NoDokumen FROM `trans_payment_header` a WHERE a.`keterangan` LIKE '%".$id."%';";
    	
        return $this->getArrayResult($sql);

    }
	
	function getListRealisasiDetailPV($id){
		$sql = "SELECT 
				  a.*,
				  d.`NamaRekening`,
				  e.`NamaSubDivisi`,
				  f.`NamaDepartemen` 
				FROM
				  `realisasi_uang_muka_payment` a 
				  INNER JOIN `realisasi_uang_muka` b 
					ON a.`NoDokumen` = b.`NoDokumen` 
				  INNER JOIN rekening d 
					ON a.`KdRekening` = d.`KdRekening` 
				  INNER JOIN subdivisi e 
					ON a.`KdSubDivisi` = e.`KdSubDivisi` 
				  INNER JOIN departemen f 
					ON a.`KdDepartemen` = f.`KdDepartemen` 
				WHERE a.`NoDokumen` = '".$id."' ;";
    	
        return $this->getArrayResult($sql);

    }
    
    function getReferensi($id)
	{
        
    	$sql = "  
		        SELECT * FROM `jurnalheader` a WHERE a.`NoTransaksi`='$id';
		        ";    
		return $this->getRow($sql);
    }
    
    function getKasBank($user)
	{
    	$sql = "
    			SELECT 
				  a.`KdKasBank`,
				  a.`NamaKasBank`,
				  a.`KdRekening` 
				FROM
				  `kasbank` a 
				  INNER JOIN `userkasbank` b 
				    ON a.`KdKasBank` = b.`KdKasBank` 
				WHERE b.`UserName` = '".$user."' 
				ORDER BY a.`NamaKasBank` ASC ;
    			";
		return $this->getArrayResult($sql);
    }
	
	
	function getEmployee($employee='')
	{
		if($employee == ''){
			$where = '';
		}else{
			$where = " and a.employee_name like '%$employee%' ";
		}
		
    	$sql = "
    			SELECT 
				  a.employee_id,
				  a.employee_name
				FROM
				  `employee` a 
				WHERE 1 $where
				ORDER BY a.`employee_name` ASC ;
    			";
		return $this->getArrayResult($sql);
    }
    
    
    function getSubDivisi()
	{
    	$sql = "
    			SELECT 
				  * 
				FROM
				  subdivisi a 
				WHERE 1 
				ORDER BY a.`NamaSubDivisi` ASC ;

    			";
		return $this->getArrayResult($sql);
    }
	
	
	function getDept()
	{
    	$sql = "
    			SELECT 
				  * 
				FROM
				  departemen a 
				WHERE 1 
				ORDER BY a.`KdDepartemen` ASC ;

    			";
		return $this->getArrayResult($sql);
    }
    
    
    function getKodeBank($kd){

    	$sql = "SELECT KdPembayaran FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    function getEmployeeName($employee){

    	$sql = "SELECT employee_name FROM `employee` WHERE employee_id='$employee'";

        return $this->getRow($sql);

    }
    
    function getDataUangMuka($id)
	{
		$sql = "
					SELECT 
					  a.*,
					  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
					  b.`NamaKasBank`,
					  c.`NamaSubDivisi`,
					  d.`employee_name` 
					FROM
					  uang_muka a 
					  INNER JOIN kasbank b 
					    ON a.`KdKasBank` = b.`KdKasBank` 
					  INNER JOIN subdivisi c 
					    ON a.`KdSubDivisi` = c.`KdSubDivisi` 
					  INNER JOIN employee d 
					    ON a.`Employee` = d.`employee_id` 
					WHERE 1 
					AND a.`Employee`='".$id."'
					 AND a.`Sisa` > 0 and a.Status=1
					ORDER BY a.NoDokumen ASC
		       ";
		//echo $sql;
        return $this->db->query($sql);
	}
    
    function num_uang_muka_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM realisasi_uang_muka;       
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getinterface(){
		$sql = "select * from interface";
		$hasil = $this->getArrayResult($sql);
		return $hasil[0];
		
	}
	
	function DeleteDetailPayment($nodokumen){
		$sql = "delete from realisasi_uang_muka_payment where nodokumen='$nodokumen'";
		$this->db->query($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>