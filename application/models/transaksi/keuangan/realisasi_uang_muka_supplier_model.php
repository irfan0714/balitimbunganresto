<?php
class Realisasi_uang_muka_supplier_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
	
	function getListRealisasiUangMuka($id){

    	$sql = "SELECT 
    				a.*,
    				date_format(TglDokumen,'%d-%m-%Y') as Tanggal
    	        FROM realisasi_uang_muka_supplier a WHERE a.NoDokumen ='".$id."'";
        return $this->getRow($sql);

    }
    
    function getTotalBayar($id){

    	$sql = "SELECT 
    				*
    	        FROM realisasi_uang_muka_supplier a WHERE a.NoDokumen ='".$id."'";
        return $this->getRow($sql);

    }
    
    function getListRealisasiUangMukaPdf($id){

    	$sql = "
    			SELECT 
				  a.*,
				  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
				  b.`Nama` NamaSupplier,
				  d.`NamaKasBank`
				FROM
				  `realisasi_uang_muka_supplier` a 
				  INNER JOIN `supplier` b 
				    ON a.KdSupplier = b.KdSupplier
				  INNER JOIN `kasbank` d 
				    ON a.`KdKasBank` = d.`KdKasBank`
    			WHERE a.NoDokumen ='".$id."'";
        return $this->getRow($sql);

    }
   
	function getRealisasiUangMukaList($limit,$offset,$arrSearch)
	{
		
		$mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
	
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.NoDokumen";
				$arr_keyword[1] = "a.NoReferensi";
				$arr_keyword[2] = "a.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
		} 
		
       $sql = "  
           SELECT 
			  a.*,
			  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
			  b.`NamaKasBank`,
			  d.`Nama` 
			FROM
			  realisasi_uang_muka_supplier a 
			  INNER JOIN kasbank b 
			    ON a.`KdKasBank` = b.`KdKasBank` 
			  INNER JOIN supplier d 
			    ON a.`KdSupplier` = d.`KdSupplier` 
			WHERE 1 $where_keyword and a.Status<>'2'
			ORDER BY a.`AddDate` DESC,  
                  a.`NoDokumen` DESC
            Limit 
              $offset,$limit
        ";               
        /*echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
	}
	
	function getListRealisasiDetail($id){
		$sql = "SELECT 
				  *
				FROM
				  `realisasi_uang_muka_supplier_detail` a
				INNER JOIN `uang_muka_supplier` b 
					ON a.`NoUM` = b.`NoDokumen`
				WHERE a.`Realisasi` <> 0 AND a.`NoDokumen` = '".$id."';";
    	
        return $this->getArrayResult($sql);

    }
    
    function getNoPayReceipt($id){
		$sql = "SELECT NoDokumen FROM `trans_payment_header` a WHERE a.`keterangan` LIKE '%".$id."%';";
    	
        return $this->getArrayResult($sql);

    }
	
	function getListRealisasiDetailPV($id){
		/*$sql = "SELECT 
				  a.*,
				  d.`NamaRekening`,
				  e.`NamaSubDivisi`,
				  f.`NamaDepartemen` 
				FROM
				  `realisasi_uang_muka_supplier_payment` a 
				  INNER JOIN `realisasi_uang_muka_supplier` b 
					ON a.`NoDokumen` = b.`NoDokumen` 
				  INNER JOIN rekening d 
					ON a.`KdRekening` = d.`KdRekening` 
				  INNER JOIN subdivisi e 
					ON a.`KdSubDivisi` = e.`KdSubDivisi` 
				  INNER JOIN departemen f 
					ON a.`KdDepartemen` = f.`KdDepartemen` 
				WHERE a.`NoDokumen` = '".$id."' ;";*/
				
		$sql = "SELECT 
				  a.*
				FROM
				  `realisasi_uang_muka_supplier_payment` a 
				  INNER JOIN `realisasi_uang_muka_supplier` b 
				    ON a.`NoDokumen` = b.`NoDokumen`  
				WHERE a.`NoDokumen` = '".$id."' ;";
		$user = $this->session->userdata('username');
        
		//echo $sql;
        return $this->getArrayResult($sql);

    }
    
    function getListRealisasiDetailPVPdf($id){
		$sql = "SELECT 
				  a.*
				FROM
				  `realisasi_uang_muka_supplier_payment` a 
				  INNER JOIN `realisasi_uang_muka_supplier` b 
				    ON a.`NoDokumen` = b.`NoDokumen`  
				WHERE a.`NoDokumen` = '".$id."' ;";
    
        return $this->getArrayResult($sql);

    }
    
    function getKasBank($user)
	{
    	$sql = "
    			SELECT 
				  a.`KdKasBank`,
				  a.`NamaKasBank`,
				  a.`KdRekening` 
				FROM
				  `kasbank` a 
				  INNER JOIN `userkasbank` b 
				    ON a.`KdKasBank` = b.`KdKasBank` 
				WHERE b.`UserName` = '".$user."' 
				ORDER BY a.`NamaKasBank` ASC ;
    			";
		return $this->getArrayResult($sql);
    }
	
	
	function getSupplier($supplier = '')
	{
		if($supplier == ''){
			$where = '';
		}else{
			$where = " and a.Nama like '%$supplier%' ";
		}
    	$sql = "
    			SELECT 
				  a.KdSupplier,
				  a.Nama
				FROM
				  `supplier` a 
				WHERE 1 $where and a.StatAktif = 'Y'
				ORDER BY a.`Nama` ASC ;
    			";
		return $this->getArrayResult($sql);
    }
    
    
    function getSubDivisi()
	{
    	$sql = "
    			SELECT 
				  * 
				FROM
				  subdivisi a 
				WHERE 1 
				ORDER BY a.`NamaSubDivisi` ASC ;

    			";
		return $this->getArrayResult($sql);
    }
	
	
	function getDept()
	{
    	$sql = "
    			SELECT 
				  * 
				FROM
				  departemen a 
				WHERE 1 
				ORDER BY a.`KdDepartemen` ASC ;

    			";
		return $this->getArrayResult($sql);
    }
    
    
    function getKodeBank($kd){

    	$sql = "SELECT KdPembayaran, KdPenerimaan FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    function getSupplierName($supplier){

    	$sql = "SELECT Nama FROM `supplier` WHERE KdSupplier='$supplier'";
		
        //$rec =  $this->getArrayResult($sql);	
        //return $rec[0]['Nama'];
        
        return $this->getRow($sql);

    }
    
    function getDataUangMuka($id)
	{
		$sql = "
					SELECT 
					  a.*,
					  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
					  b.`NamaKasBank`,
					  c.`NamaSubDivisi`,
					  d.`Nama` 
					FROM
					  uang_muka_supplier a 
					  INNER JOIN kasbank b 
					    ON a.`KdKasBank` = b.`KdKasBank` 
					  INNER JOIN subdivisi c 
					    ON a.`KdSubDivisi` = c.`KdSubDivisi` 
					  INNER JOIN supplier d 
					    ON a.`KdSupplier` = d.`KdSupplier` 
					WHERE 1 
					AND a.`KdSupplier`='".$id."'
					 AND a.`Sisa` > 0 and a.Status=1
					ORDER BY a.NoDokumen ASC
		       ";
		//echo $sql;
        return $this->getArrayResult($sql);	
	}
	
	function getDataFaktur($KdSupplier, $tgl)
	{
		$sql = "SELECT h.`NoFaktur`, i.`NoFakturSupplier`, i.`NoPO`, i.NoPenerimaan as NoRG, h.`JatuhTempo` as Tanggal, h.`Sisa` 
				FROM hutang h LEFT JOIN invoice_pembelian_header i  ON h.`NoFaktur`=i.`NoFaktur`
				WHERE h.tanggal<='$tgl' and h.KdSupplier ='$KdSupplier' AND h.MataUang='IDR' AND h.TipeTransaksi in('I','IM','C','CM') and h.sisa>0 order by h.JatuhTempo";
		//echo $sql;
    	return $this->getArrayResult($sql);	
    }
    
    function getDataFakturView($KdSupplier, $tgl, $rus)
	{
    $sql="
    		SELECT 
			  h.`NoFaktur`,
			  i.`NoFakturSupplier`,
			  i.`NoPO`,
			  i.NoPenerimaan AS NoRG,
			  h.`JatuhTempo` AS Tanggal,
			  h.`Sisa`,
			  j.`Bayar`,
  			  j.`NilaiFaktur`
			FROM
			  hutang h 
			  LEFT JOIN invoice_pembelian_header i 
			    ON h.`NoFaktur` = i.`NoFaktur` 
			  INNER JOIN `realisasi_uang_muka_supplier_payment` j
			  ON h.`NoFaktur` = j.`NoFaktur`
			WHERE h.tanggal <= '$tgl' 
			  AND h.KdSupplier = '$KdSupplier' 
			  AND h.MataUang = 'IDR' 
			  AND h.TipeTransaksi IN ('I', 'C','IM','CM') 
			  AND j.`NoDokumen`='$rus'
			ORDER BY h.JatuhTempo DESC;
    	";
    	//echo $sql;
    	return $this->getArrayResult($sql);	
    }
    
    function insertdebitnote($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa, $dnno, $vpph, $vpembulatan ,$interface,$rekhutang,$v_no_dokumen){
    	$user = $this->session->userdata('username');
    	$dnheader = array('dnno' => $dnno,
						'dndate' => $vtgl,
						'dntype' => 'D',
						'supplierid'=> $kdsupplier,
						'KdRekening'=> $rekhutang,
						'note'=> $notedn,
						'status'=>1,
						'currencycode'=>'IDR',
						'taxrate'=>1,
						'vatpercent' => 0,
						'discpercent'=>0,
						'dnamount'=> $v_tot_realisasi,
						'dnamountremain' => 0,
						'RUSNo'=>$v_no_dokumen,
						'adddate' => date('Y-m-d'),
						'adduser' => $user);
		$this->db->insert('debitnote', $dnheader);
		
		$dndetail = array('dnno' => $dnno,
						'coano' => $interface['UMPembelian'],
						'KdSubDivisi' => 25, 
						'value' => $v_tot_realisasi, 
						'description' =>$notedn,
						'adddate' => date('Y-m-d'),
						'adduser' => $user);
		$this->db->insert('debitnotedtl', $dndetail);
		
		if($vpph>0){
			//pembulatan dan PPH
			$dndetailtambahan0 = array('dnno' => $dnno,
									'coano' => $interface['PPH23'],
									'KdSubDivisi' => 25, 
									'value' => $vpph, 
									'description' =>'PPH',
									'adddate' => date('Y-m-d'),
									'adduser' => $user);
			$this->db->insert('debitnotedtl', $dndetailtambahan0);
		}
		
		if($vpembulatan!=0){
			if($vpembulatan>0){
				//pembulatan dan PPH
				$dndetailtambahan1 = array('dnno' => $dnno,
										'coano' => $interface['LabaSelisihBayar'],
										'KdSubDivisi' => 25, 
										'value' => $vpembulatan, 
										'description' =>'Selisih Bayar Plus',
										'adddate' => date('Y-m-d'),
										'adduser' => $user);
				$this->db->insert('debitnotedtl', $dndetailtambahan1);
			}else if($vpembulatan<0){
				//pembulatan dan PPH
				$dndetailtambahan2 = array('dnno' => $dnno,
										'coano' => $interface['RugiSelisihBayar'],
										'KdSubDivisi' => 25, 
										'value' => $vpembulatan, 
										'description' =>'Selisih Bayar Minus',
										'adddate' => date('Y-m-d'),
										'adduser' => $user);
				$this->db->insert('debitnotedtl', $dndetailtambahan2);
			}
		}
		
	}
	
	function inserthutang($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa,$dnno,$interface,$rekhutang){
		list($thn, $bln, $tgl) = explode('-', $vtgl);
		$hutang = array('NoDokumen'=> $dnno,
						'NoFaktur'=> $dnno,
						'KdSupplier'=> $kdsupplier,
						'TipeTransaksi'=> 'D',
						'Tanggal'=> $vtgl,
						'JatuhTempo'=> $vtgl,
						'NilaiTransaksi'=>$v_tot_realisasi,
						'Sisa'=>0,
						'KdRekening'=>$rekhutang,
						'MataUang'=>'IDR',
						'Kurs'=>1);
		$this->db->insert('hutang', $hutang);
		
		$mutasihutang = array('Tahun'=>$thn,
							'Bulan'=>$bln,
							'NoDokumen'=>$dnno,
							'Tanggal'=>$vtgl,
							'KdSupplier'=>$kdsupplier,
							'MataUang'=>'IDR',
							'Kurs'=>1,
							'Jumlah'=>$v_tot_realisasi,
							'SelisihKurs'=>0,
							'KdRekening'=>$rekhutang,
							'TipeTransaksi'=>'D');
		$this->db->insert('mutasi_hutang', $mutasihutang);
	}
	
	function insertallocation($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa,$dnno,$dnallocationno,$v_no_dokumen){
		$user = $this->session->userdata('username');
		$dnallocation = array('dnallocationno' =>$dnallocationno,
							'dnadate' =>$vtgl,
							'dnno' =>$dnno,
							'supplierid' => $kdsupplier,
							'currencycode' => 'IDR',
							'allocationtotal' => $v_tot_realisasi,
							'allocationidrtotal' => $v_tot_realisasi,
							'adddate'=> date('Y-m-d'),
							'adduser'=>$user,
							'status'=>1,
							'isposted'=>0);
		$this->db->insert('dnallocation', $dnallocation);
		
		$totbayar = 0;
		for ($y = 0; $y < count($nofaktur); $y++){
			$vnofaktur = $nofaktur[$y];
			$vsisa = $sisa[$y];
			$vbayar = $bayar[$y];
			if($vbayar>0){
				$dnallocationdtl = array('dnallocationno'=>$dnallocationno,
								'invno'=> $vnofaktur,
								'invamount'=> $vsisa,
								'allocation'=> $vbayar,
								'invamountidr'=>$vsisa,
								'allocationidr' =>$vbayar,
								'adddate' => date('Y-m-d'),
								'adduser'=>$user);
				$this->db->insert('dnallocationdetail', $dnallocationdtl);
				
				$kdrekening = $this->getKdRekeningFaktur($vnofaktur);
				$hutang = array('NoDokumen'=> $dnallocationno,
						'NoFaktur'=> $vnofaktur,
						'KdSupplier'=> $kdsupplier,
						'TipeTransaksi'=> 'A',
						'Tanggal'=> $vtgl,
						'JatuhTempo'=> $vtgl,
						'NilaiTransaksi'=>$vbayar,
						'Sisa'=>0,
						'MataUang'=>'IDR',
						'Kurs'=>1);
				$this->db->insert('hutang', $hutang);	
				
				$sql = "update hutang set Sisa=Sisa-$vbayar where NoDokumen='$vnofaktur'";
				$this->db->query($sql);
				
				$totbayar += $vbayar;
			}
		}
		$sql = "update realisasi_uang_muka_supplier set JmlPV=$totbayar where NoDokumen='$v_no_dokumen'";
		$this->db->query($sql);
	}
	
	function getKdRekeningFaktur($nofaktur){
		$sql = "SELECT KdRekening FROM hutang where NoDokumen='$nofaktur' ";
		
		$row =  $this->getArrayResult($sql);
		return $row[0]['KdRekening'];
	}
	
    function num_uang_muka_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM realisasi_uang_muka_supplier;       
		";
		                  
        return $this->NumResult($sql);
	}
	
	function getInterface(){
		$sql = "Select * from interface";
		$rec = $this->getArrayResult($sql);	
		return $rec[0];
	}
	
	function updateUangMuka($v_no_um,$v_realisasi)
	{
		
		if($v_realisasi>0){
			$sql = "Update uang_muka_supplier set Sisa=0 where NoDokumen='$v_no_um' ";	
			$this->db->query($sql);
		}		
	}
	
	function insertDetailRealisasi($v_no_dokumen,$v_no_um,$v_no_pv,$v_realisasi)
	{
		
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'NoUM'=>$v_no_um,
			 'NoPV' => $v_no_pv,
			 'Realisasi' => $v_realisasi
			);	
		$this->db->insert('realisasi_uang_muka_supplier_detail', $data);
	}
	
	function insertHeaderRealisasi($v_no_dokumen,$v_tgl_dokumen,$v_supplier,$v_no_referensi,$v_kdkasbank,$v_jumlah,$v_tot_pv1,$v_note,$user,$vpph,$vpembulatan)
	{
		$user = $this->session->userdata('username');
				
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'TglDokumen'=>$v_tgl_dokumen,
			 'KdSupplier' => $v_supplier,
			 'NoReferensi' => $v_no_referensi,
             'KdKasBank' => $v_kdkasbank,
             'JmlRealisasi' => $v_jumlah,
             'JmlPV' => $v_tot_pv1,
             'Keterangan'=>$v_note,
             'PPh'=>$vpph,
             'Pembulatan'=>$vpembulatan,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
			);
		$this->db->insert('realisasi_uang_muka_supplier', $data);
	}
	
	function insertreceiptheader($norv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank)
	{
		$user = $this->session->userdata('username');
		
		$data = array(
			'NoDokumen'	=> $norv,
			'TglDokumen' => $tgl,
			'Jenis' => 1,
			'KdKasBank' => $KdKasBank,
            'TerimaDari'      => $Penerima,
			'KdCostCenter' => 0,
			'KdPersonal' => 0,
			'NoGiro' => "",
			'KdBankCair' => "",
			'TglCair' => "",
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahReceipt' => $jumlah,
			'Status' => ' '
		);
		$this->db->insert('trans_receipt_header', $data);
	}
	
	function insertreceiptdetail($NoDokumen,$tgl,$jumlah,$counter,$keterangan,$user,$norek,$subdivisi)
	{
		$user = $this->session->userdata('username');
		
		$data = array(
				'NoDokumen'	    => $NoDokumen,
				'TglDokumen'    => $tgl,
				'KdRekening'    => $norek,
				'Jumlah'        => $jumlah,
                'KdSubdivisi'   => $subdivisi,
                'KdDepartemen'  => "",
				'Keterangan'    => $keterangan,
				'Urutan'	    => $counter,
				'Status'        => "",
				'AddDate'		=> date('Y-m-d'),
				'AddUser'		=> $user
			);
		$this->db->insert('trans_receipt_detail', $data);
		
	}
    
	function insertNewDetailPayment($nodokumen, $vnofaktur,$vsisa,$vbayar)
	{
		
		$data = array(
			 'NoDokumen'=>$nodokumen,	 
			 'NoFaktur'=>$vnofaktur,
             'NilaiFaktur'=>$vsisa,
			 'Bayar' => $vbayar
			);
			
		$this->db->insert('realisasi_uang_muka_supplier_payment', $data);
		
	}
	
	function getRekFaktur($nofaktur){
		$sql = "Select KdRekening from hutang where nodokumen='$nofaktur'";
		$rec = $this->getArrayResult($sql);	
		return $rec[0]['KdRekening'];
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>