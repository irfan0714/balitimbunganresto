<?php
class Aktivitasmodel extends CI_Model {
	
    function __construct()
	{
        parent::__construct();
    }

    function get_aktivitas_List($num, $offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="";
		if($id!=""){
			$clause = " where $with like '%$id%'";
		}
    	
		$sql="SELECT * FROM aktivitas INNER JOIN rekening ON aktivitas.KdRekening = rekening.KdRekening $clause ORDER BY aktivitas.KdAktivitas DESC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function num_aktivitas_row($id,$with){
     	$clause="";
     	if($id!=''){
			$clause = " where $with like '%$id%'";
		}
		$sql = "SELECT * FROM aktivitas INNER JOIN rekening ON aktivitas.KdRekening = rekening.KdRekening $clause";
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	function getRekening($KdRekening)
	{
    	$sql = "SELECT KdRekening, NamaRekening FROM rekening WHERE KdRekening LIKE '%.$KdRekening.%' OR NamaRekening LIKE '%".$KdRekening."%'ORDER BY NamaRekening ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getRekeningList()
	{
    	$sql = "SELECT KdRekening, NamaRekening FROM rekening ORDER BY NamaRekening ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    public function get_by_aktivitas($KdAktivitas)
	{
		$this->db->from('aktivitas INNER JOIN rekening ON aktivitas.KdRekening = rekening.KdRekening');
		$this->db->where('KdAktivitas',$KdAktivitas);
		$query = $this->db->get();
		return $query->row();
		
		/*$sql="SELECT * FROM aktivitas INNER JOIN rekening ON aktivitas.KdRekening = rekening.KdRekening WHERE aktivitas.KdRekening='".$KdRekening."'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;*/
        
	}
	
	
	
}
?>