<?php

class Ticketingmodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
    
    function NamaPrinter($id) {
        $sql = "SELECT * from kassa where ip='$id'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function getCurrency() {
        $sql = "SELECT CONCAT(Kd_Uang,'-',NilaiTukar) AS Kode,Keterangan FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }
	
	function cekidkassa($ip) {
        $sql = "SELECT id_kassa FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['id_kassa'];
    }
    
    function cekkdstore($ip) {
        $sql = "SELECT KdStore FROM kassa WHERE ip='$ip'";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row[0]['KdStore'];
    }
	
	function getAllTicket(){
		$sql = "select * from masterbarang where JenisBarang='TCK' ";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function getTicketList($todayid,$jenis) {
		
		if($jenis=="")
		{
			$jeniz = '1';
		}else{
			$jeniz = $jenis;
		}
			
		$sql="
				SELECT m.*,
			    t.cashback 
				FROM 
				masterbarang m,
				ticketlistconf t
				WHERE 
				m.PCode=t.PCode 
				AND t.DayID=$todayid 
				AND m.JenisBarang='TCK' 
				AND m.`KdSubKategori` = '$jeniz'
				ORDER BY m.kdkategori ASC
			";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	function getTransTicket($getLastIP){
		$sql = "select a.*,b.*,a.`harga` AS harga_ticket, b.Harga1b AS harga_kontrak from ticket a 
		        INNER JOIN masterbarang b ON a.PCode = b.PCode
		        where a.status='0' and a.last_ip='$getLastIP' order by a.noticket ASC";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	
	
	
	function editStatusTicket($parameter,$data,$where){
		$this->db->where($where);
		$this->db->update($parameter, $data);
	}
	
	function totalTicketPerhari($date,$ipkassa){
		
		    if($ipkassa == '192.168.10.61x'){
				$where = " AND PCode IN ('863940001','863940002','96303180001','963940001','963940002','963940003','963940004') ";
			}else{
				$where = " AND PCode NOT IN ('863940001','863940002','96303180001','963940001','963940002','963940003','963940004')";
			}
			
		$sql = "SELECT COUNT(noticket) AS total FROM ticket WHERE add_date='$date' AND noidentitas !='1234' $where";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getReport($start_date,$end_date) 
    {
    	/*$sql ="
    		SELECT 
			  DATE_FORMAT(ticket.add_date, '%Y-%m-%d') AS tanggal,
			  ticket.jenis,
			  ticket_jenis.NamaJenis,
			  SUM(ticket.qty) AS total_qty,
			  SUM(ticket.harga) AS total_harga 
			FROM
			  ticket 
			  INNER JOIN ticket_jenis 
			    ON ticket.jenis = ticket_jenis.idjenis 
			WHERE 1 
			  AND ticket.noidentitas != '1234'
			  AND ticket.status = '1'
			  AND ticket.add_date BETWEEN '".$start_date."' AND '".$end_date."' 
			GROUP BY ticket.add_date,
			  ticket.jenis 
			ORDER BY 
			  ticket.add_date ASC,
			  ticket_jenis.NamaJenis ASC
    	";*/
    	
    	$sql ="
    		SELECT 
			  DATE_FORMAT(ticket.add_date, '%Y-%m-%d') AS tanggal,
			  ticket.jenis,
			  ticket_jenis.NamaJenis,
			  SUM(ticket.qty) AS total_qty,
			  SUM(ticket.harga) AS total_harga 
			FROM
			  ticket 
			  INNER JOIN ticket_jenis 
			    ON ticket.jenis = ticket_jenis.idjenis 
			WHERE 1 
			  AND ticket.noidentitas != '1234'
			  AND ticket.add_date BETWEEN '".$start_date."' AND '".$end_date."' 
			GROUP BY ticket.add_date,
			  ticket.jenis 
			ORDER BY 
			  ticket.add_date ASC,
			  ticket_jenis.NamaJenis ASC
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_ticket($start_date,$end_date) 
    {
    	$sql ="
    		SELECT 
			  a.`PCode`,
			  b.`NamaLengkap`,
			  COUNT(a.`PCode`) AS JML 
			FROM
			  `ticket` a 
			  INNER JOIN masterbarang b 
			    ON a.`PCode` = b.`PCode` 
			WHERE a.`tgl_berlaku` BETWEEN '$start_date' AND '$end_date' 
			GROUP BY a.PCode ;
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_wilayah($start_date,$end_date) 
    {
    	$sql ="
    		SELECT 
			  COUNT(b.`country`) AS JML,c.`country_name`
			FROM
			  ticket a 
			  INNER JOIN ticket_customer b 
			    ON a.`notrans` = b.`notrans` 
			  INNER JOIN `apps_countries` c ON b.`country` = c.`country_code`
			WHERE a.`tgl_berlaku` BETWEEN '$start_date' AND '$end_date' GROUP BY b.`country` ORDER BY COUNT(b.`country`) DESC;
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_kota($start_date,$end_date) 
    {
    	$sql ="
    		SELECT 
			  COUNT(b.`city`) AS JLMS,
			  d.`nama_kota` 
			FROM
			  ticket a 
			  INNER JOIN ticket_customer b 
			    ON a.`notrans` = b.`notrans` 
			  INNER JOIN kota d 
			    ON b.`city` = d.`id_kota` 
			WHERE a.`tgl_berlaku` BETWEEN '$start_date' 
			  AND '$end_date' 
			  AND b.`country` = 'ID' 
			GROUP BY b.`city` 
			ORDER BY COUNT(b.`city`) DESC ;
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }
    
    function getReport_customer($start_date,$end_date) 
    {
    	$sql ="
    		SELECT 
			  b.`customer_name`,
			  COUNT(b.`customer_name`) AS JML 
			FROM
			  ticket_customer b 
			WHERE b.`add_date` BETWEEN '$start_date' 
			  AND '$end_date' 
			GROUP BY b.`customer_name` 
			ORDER BY COUNT(b.`customer_name`) DESC ;
    	";
    	//echo $sql;
        return $this->getArrayResult($sql);
    }
    
    
    
    function getTotalTicket($start_date,$end_date) 
    {
    	$sql ="
    		 SELECT 
			  SUM(ticket.qty) AS total_ticket
			FROM
			  ticket 
			WHERE 1 
			  AND ticket.noidentitas != '1234' 
			  AND ticket.status = '1' 
			  AND ticket.add_date BETWEEN '$start_date' 
			  AND '$end_date'
    	";
    	//echo $sql;
        return $this->getRow($sql);
    }

	function getTicketHead($start_date,$end_date)
	{
		$sql="
			SELECT 
			  DATE(ticket_head.tanggal) AS tglTiket,
			  SUM(ticket_head.nilai_tunai-ticket_head.ttl_kembali) AS n_tunai,
			  SUM(ticket_head.nilai_debit) AS n_debit,
			  SUM(ticket_head.nilai_kredit) AS n_kredit,
			  SUM(ticket_head.nilai_voucher) AS n_voucher
			FROM
			  ticket_head 
			WHERE 1 
			  AND DATE(ticket_head.tanggal) BETWEEN '".$start_date."' 
			  AND '".$end_date."' 
			  AND ticket_head.noidentitas != '1234' 
			GROUP BY tglTiket
		";
        return $this->getArrayResult($sql);
		
	}
    
    function getKartu() {
        $sql = "SELECT * FROM `kartu` a WHERE a.tipe<>'EDC' ORDER BY nama ASC";
        return $this->getArrayResult($sql);
		
    }
    
    function getEDC() {
        $sql = "SELECT * FROM `kartu` a WHERE a.tipe='EDC' ORDER BY nama ASC";
        return $this->getArrayResult($sql);
		
	}
    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getDate() {
        $sql = "select TglTrans from aplikasi order by Tahun desc limit 0,1";
        return $this->getRow($sql);
    }
    
    function getJenis()
	{
		$sql = "select idjenis,NamaJenis from ticket_jenis order by idjenis";
		return $this->getArrayResult($sql);
	}
	
	function getFreeTicket($idmember)
	{
		$sql = "SELECT b.`FreeTicket` FROM member a INNER JOIN type_member b ON a.`KdTipeMember` = b.`KdTipeMember` WHERE a.`KdMember`='$idmember';";
		return $this->getRow($sql);
	}	

	function updatepointmember($kdmember, $nettosales, $bayarpoint,$no_transaksi,$jml_free_ticket=""){
		/* ketentuan point
			minimal belanja 100.000 dapat 5.000 point
			kelipatan 50.000 dapat 2.500 point
		*/
		$bayarpoint = $bayarpoint*1;
		$point =0;
		if($nettosales >= 100000){
			$nettosales -= 100000;
			$point = 5000;
			$lipat = 50000;
			
			for ($i= 1; $i <= $nettosales; $i++) { 
				if ( $bagi = $i % $lipat == 0 ) {
					 $point += 2500;
				}
			}	
		}

		if($point >0){
			$this->mutasi_point_member($no_transaksi,$kdmember,$point,'I');
		}

		if($bayarpoint > 0){
			$this->mutasi_point_member($no_transaksi,$kdmember,$bayarpoint,'O');
		}

		if($jml_free_ticket != ""){
			$this->mutasi_point_member($no_transaksi,$kdmember,'0','I',"Free ".$jml_free_ticket." ticket");
		}
		$point -= $bayarpoint;
		$sql = "UPDATE member SET JumlahPoint = JumlahPoint+'$point' WHERE KdMember='$kdmember' ";

		$qry = $this->db->query($sql);
	}
	
	function mutasi_point_member($no_transaksi,$kd_member,$point,$tipe,$jml_free_ticket=""){
		$sql = "INSERT INTO mutasi_point SET NoTransaksi ='$no_transaksi', KdMember ='$kd_member',JumlahPoint ='$point', TipeTransaksi ='$tipe', Keterangan='$jml_free_ticket' ";
		$this->db->query($sql);
	}
}

?>