<?php
class Purchase_order_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getWarehouse()
	{
    	$sql = "SELECT a.warehousecode,a.warehousename FROM warehouse a ORDER BY a.warehousecode ASC";
		return $this->getArrayResult($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
    
    function getSatuanPcode($pcode)
	{
    	     $sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
		return $this->getArrayResult($sql);
    }
    
    function getHargaKontrak($supplier,$TglDokumen,$PCode)
	{
    	     $sql = "
    			SELECT 
				  b.`HargaContract` 
				FROM
				  `contract_supplier_header` a 
				  INNER JOIN `contract_supplier_detail` b 
				    ON a.`NoTransaksi` = b.`NoTransaksi` 
				WHERE a.`KdSupplier` = '$supplier' 
				  AND b.`PCode` = '$PCode' 
				  AND a.`PeriodeAwal` <= '$TglDokumen' 
				  AND a.`PeriodeAkhir` >= '$TglDokumen' 
				  AND a.`Status` = '1' 
    		   ";
    		   
    	echo $sql;
		return $this->getRow($sql);
    }
    
    function cekApproval($nodok)
	{
    	     $sql = "
    			SELECT a.`Approval_By` FROM `trans_order_barang_header` a WHERE a.`NoDokumen`='$nodok';
    		   ";
		return $this->getRow($sql);
    }
	
	function cekRG($id)
	{
    	     $sql = "
    			SELECT 
					  trans_terima_header.NoDokumen,
	                  trans_terima_header.PoNo
					FROM
					  trans_terima_header 
					WHERE 1
					AND trans_terima_header.PoNo='".$id."' 
					AND trans_terima_header.Status<>'2';
    		   ";
		return $this->getRow($sql);
    }
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               "; 
      
        return $this->getRow($sql);
	}

	function getKonversi_new($pcode,$satuan_from,$satuan_to)
	{
		$sql = "SELECT * FROM konversi WHERE PCode = '$pcode' AND Satuan_From = '$satuan_from' AND Satuan_To = '$satuan_to'";
	
		return $this->getRow($sql); 
	}
	
	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }	
    
    
    function num_purchase_order_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `trans_order_barang_header` a;       
		";
		                  
        return $this->NumResult($sql);
	}
		
	function getPurchaseOrderList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_supplier = "";
        $where_status="";
        $where_tgl="";
        $where_total="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "trans_order_barang_header.NoDokumen";
				$arr_keyword[1] = "trans_order_barang_header.KdGudang";
				$arr_keyword[2] = "supplier.Nama";
				$arr_keyword[3] = "trans_order_barang_header.Status";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND trans_order_barang_header.KdGudang = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["supplier"]!="")
			{
				$where_customer = "AND trans_order_barang_header.KdSupplier = '".$arrSearch["supplier"]."'";	
			}
			
			if($arrSearch["tgl_awal"]!="" OR $arrSearch["tgl_akhir"]!="")
			{
				$where_tgl = "AND trans_order_barang_header.TglDokumen BETWEEN '".$arrSearch["tgl_awal"]."' AND '".$arrSearch["tgl_akhir"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				if($arrSearch["status"]=="0" OR $arrSearch["status"]==""){
				$where_status = " AND trans_order_barang_header.Status='0'";	
				}else if($arrSearch["status"]=="1"){//open
				$where_status = "AND trans_order_barang_header.Status='1'";		
				}else if($arrSearch["status"]=="2"){//void
				$where_status = "AND trans_order_barang_header.Status='2'";		
				}else if($arrSearch["status"]=="3"){//all reject
				$where_status = "AND trans_order_barang_header.Approval_Status='2'";		
				}else if($arrSearch["status"]=="4"){//reject by vicko
				$where_status = "AND trans_order_barang_header.Approval_Status='2' AND trans_order_barang_header.Approval_By='vicko0604'";		
				}else if($arrSearch["status"]=="5"){//reject by wieok
				$where_status = "AND trans_order_barang_header.Approval_Status='2' AND trans_order_barang_header.Approval_By='wieok3110'";		
				}else if($arrSearch["status"]=="6"){//all Waiting Approve
				$where_status = "AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='0'";		
				$where_tgl="";
				}else if($arrSearch["status"]=="7"){//Waiting Approve By vicko
				$where_status = "AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='0' AND trans_order_barang_header.Total<=10000000";		
				$where_tgl="";
				}else if($arrSearch["status"]=="8"){//Waiting Approve By wieok
				$where_status = "AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='0' AND trans_order_barang_header.Total>10000000";		
				$where_tgl="";
				}else if($arrSearch["status"]=="9"){//All Approve
				$where_status = "AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_Status='1'";		
				}else if($arrSearch["status"]=="10"){//Approve By vicko
				$where_status = "AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_By='vicko0604' AND trans_order_barang_header.Approval_Status='1'";		
				}else if($arrSearch["status"]=="11"){//Approve By wieok
				$where_status = "AND trans_order_barang_header.Status='1' AND trans_order_barang_header.Approval_By='wieok3110' AND trans_order_barang_header.Approval_Status='1'";		
				}else if($arrSearch["status"]=="12"){//All Request
				$where_status = "AND trans_order_barang_header.Status='4' AND trans_order_barang_header.Request_Status='1'";		
				}else if($arrSearch["status"]=="13"){//Request By vicko
				$where_status = "AND trans_order_barang_header.Status='4' AND trans_order_barang_header.Request_Status='1' AND trans_order_barang_header.Total<=10000000";		
				}else if($arrSearch["status"]=="14"){//Request By wieok
				$where_status = "AND trans_order_barang_header.Status='4' AND trans_order_barang_header.Request_Status='1' AND trans_order_barang_header.Total>10000000";		
				}
			}
						
			
			if($arrSearch["total"]!="")
			{
				if($arrSearch["total"]=="0"){
					$where_total= "AND trans_order_barang_header.Total <= '5000000'";
				}else if($arrSearch["total"]=="1"){
					$where_total = "AND trans_order_barang_header.Total BETWEEN '5000001' AND '10000000'";
				}else if($arrSearch["total"]=="2"){
					$where_total = "AND trans_order_barang_header.Total > '10000000'";
				}
					
			}
		} 
        
    	$sql = "  
            SELECT 
				  trans_order_barang_header.NoDokumen,
				  DATE_FORMAT(trans_order_barang_header.TglDokumen, '%d-%m-%Y') AS TglDokumen,
                  trans_order_barang_header.NoPr,
				  supplier.KdSupplier,
				  supplier.Nama AS NamaSupplier,
				  gudang.KdGudang,
				  gudang.Keterangan AS NamaGudang,
				  trans_order_barang_header.currencycode,
				  trans_order_barang_header.Total,
				  trans_order_barang_header.Status,
				  trans_order_barang_header.Approval_by,
				  DATE_FORMAT(trans_order_barang_header.Approval_Date, '%d-%m-%Y %h:%i:%s') AS Approval_Date,
                  trans_order_barang_header.Approval_Status,
                  trans_order_barang_header.Approval_Remarks,
                  trans_order_barang_header.AddUser,
                  trans_order_barang_header.Request_Remarks
				FROM
				  trans_order_barang_header 
				  INNER JOIN supplier 
				    ON trans_order_barang_header.KdSupplier = supplier.KdSupplier 
				  INNER JOIN gudang 
				    ON trans_order_barang_header.KdGudang = gudang.KdGudang 
				WHERE 1 
            	".$where_keyword."
            	".$where_gudang."
            	".$where_customer."    
            	".$where_status."
            	".$where_tgl."    
            	".$where_total."                               
            ORDER BY 
					trans_order_barang_header.TglDokumen DESC,
					trans_order_barang_header.NoDokumen ASC
            Limit 
              $offset,$limit
        ";               
        // echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
    
    function getDatax1($tgldari,$tglsampai)
	{
		$sql = "SELECT 
				  * 
				FROM
				  trans_order_barang_header a 
				WHERE a.`Total` < 5000000 
				  AND a.`TglDokumen` BETWEEN '$tgldari' 
				  AND '$tglsampai' 
				  AND a.`Status` = '1' 
				  AND (
				    a.`Approval_By` IS NULL 
				    OR a.`Approval_By` = ''
				  ) 
				  AND a.`Approval_Status` = '0' ;
				";
		
		return $this->getArrayResult($sql); 
	}
	
	function getDatax2($tgldari,$tglsampai)
	{
		$sql = "SELECT 
				  * 
				FROM
				  trans_order_barang_header a 
				WHERE a.`Total` >= 5000000 AND a.Total <10000000
				  AND a.`TglDokumen` BETWEEN '$tgldari' 
				  AND '$tglsampai' 
				  AND a.`Status` = '1' 
				  AND (
				    a.`Approval_By` IS NULL 
				    OR a.`Approval_By` = ''
				  ) 
				  AND a.`Approval_Status` = '0' ;
				";
		
		return $this->getArrayResult($sql); 
	}
	
	function getDatax3($tgldari,$tglsampai)
	{
		$sql = "SELECT 
				  * 
				FROM
				  trans_order_barang_header a 
				WHERE a.`Total` > 10000000 
				  AND a.`TglDokumen` BETWEEN '$tgldari' 
				  AND '$tglsampai' 
				  AND a.`Status` = '1' 
				  AND  a.`Approval_By` <> ''
				  AND a.`Approval_Status` = '9' ;
				";
		
		return $this->getArrayResult($sql); 
	}
	
	function getlistemail1($tipe){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = '$tipe'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function getlistemail2($tipe){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = '$tipe'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function getlistemail3($tipe){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = '$tipe'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	function getlistemail3_cc($tipe){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = '$tipe'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			 a.*,
			 b.KdGudang,b.Keterangan,
			 c.KdSupplier, c.Nama,c.TOP,
			 DATE_FORMAT(a.`TglDokumen`, '%d-%m-%Y') AS TglDokumens,
			 DATE_FORMAT(a.`TglTerima`, '%d-%m-%Y') AS TglTerimas,
			 a.Keterangan AS note,
			 a.Status AS Statuz,
			 a.PPn AS PPn_,
			 a.Approval_Status AS approvstat,
			 a.Approval_Remarks AS ket_approve,
			 DATE_FORMAT(a.Approval_Date, '%d-%m-%Y %h:%i:%s') AS Approval_Dates
			FROM
			  `trans_order_barang_header` a 
			  INNER JOIN gudang b 
			    ON a.`KdGudang` = b.`KdGudang` 
			  INNER JOIN supplier c 
			    ON a.`KdSupplier` = c.`KdSupplier`
			WHERE a.`NoDokumen` = '".$id."' ;
        ";
		if($this->session->userdata('username')=='yuri0717'){

        echo $sql;
        }
        return $this->getRow($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.* 
			FROM
			  deliveryorderdetail
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1  
			  AND deliveryorderdetail.dono = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function getQtyPo($nodok)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  trans_order_barang_detail a
			WHERE 1  
			  AND a.NoDokumen = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function getQtyPoDetail($nodok, $pcode)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  trans_order_barang_detail a
			WHERE 1  
			  AND a.NoDokumen = '$nodok'
			  AND a.PCode = '$pcode'
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function getQtyPoPr($nodok, $pcode)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  trans_pr_detail a
			WHERE 1  
			  AND a.NoDokumen = '$nodok'
			  AND a.PCode = '$pcode'
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function function_email($func_name)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  function_email a
			WHERE 1  
			  AND a.func_name = '$func_name'
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekOtorisasi($Tipe)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  otorisasi_user a
			WHERE 1  
			  AND a.Tipe = '$Tipe'
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function HakOtorisasi($user)
	{
		$sql = "SELECT * FROM `otorisasi_user` a WHERE a.`UserName`='$user' AND a.`Tipe` IN ('po_level_1','po_level_2');";
		// echo $sql;
    return $this->getArrayResult($sql);
	}
	
	function cekApprovalPO($user,$id)
	{
		$sql = "
			SELECT * FROM trans_order_barang_header  a WHERE a.NoDokumen='$id' AND a.`Approval_By`='$user';
		";
		//echo $sql;
        return $this->getRow($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($id)
	{
		$sql = "
			SELECT 
			  a.*,
			  b.*,
			  c.*,
			  f.`Qty` AS QtyPR,
			  f.Satuan AS SatuanPR  
			FROM
			  `trans_order_barang_header` z 
			  INNER JOIN `trans_order_barang_detail` a 
			    ON z.`NoDokumen` = a.`NoDokumen` 
			    INNER JOIN `trans_pr_detail` f 
			    ON z.`NoPr` = f.`NoDokumen` AND f.`PCode` = a.`PCode`
			  INNER JOIN `masterbarang` b 
			    ON a.`PCode` = b.`PCode` 
			  INNER JOIN `satuan` c 
			    ON a.`Satuan` = c.`KdSatuan` 
			WHERE a.`NoDokumen`= '".$id."' GROUP BY f.PCode,f.`Qty`; ;
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  a.*,
			  b.*,
			  c.*,
			  a.`Qty` AS Qtys 
			FROM
			  `trans_order_barang_detail` a 
			  INNER JOIN masterbarang b
			  ON a.PCode=b.`PCode`
			  INNER JOIN satuan c
			  ON a.Satuan = c.KdSatuan
			WHERE 1 
			  AND a.`NoDokumen` = '".$id."' 
			ORDER BY a.`Sid` ASC ;
		";
		if($this->session->userdata('username')=='yuri0717'){

        echo $sql;
        }
        return $this->getArrayResult($sql);
	}
    
    
    function getSupplier()
	{
    	$sql = "SELECT a.`KdSupplier`,a.Nama FROM supplier a ORDER BY a.Nama ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getCurrency()
	{
    	$sql = "SELECT * FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
    	return $this->getArrayResult($sql);
    }
    
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}
	
	function getPRDetail($v_NoPr)
	{
		$sql = "
			SELECT b.`TglDokumen`,a.* FROM trans_pr_detail a INNER JOIN `trans_pr_header` b ON a.`NoDokumen` = b.`NoDokumen` WHERE a.`NoDokumen`='".$v_NoPr."' AND a.`QtyPO` < a.`QtyPcs` ORDER BY a.`PCode` ASC;

		";
		// echo $sql;
        return $this->getArrayResult($sql);
	}
	
	function getDetailListTemp($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  purchase_request_temp 
			  WHERE purchase_request_temp.NoDokumen= '".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function getPending() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS pending 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '0' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.`Approval_Status`='0'
				  AND a.`Request_Status`='0';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getApproved() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS approved 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '1' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.`Approval_Status`='1';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getVoid() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS void 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '2' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getWaiting() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS waiting 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '1' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.`Approval_Status`='0'
			 ";	
		
		return $this->getRow($sql);
	}

    function getReject() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS rejected 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '1' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.`Approval_Status`='2';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getRequest() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS request 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '4' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.Request_Status='1';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getPO5() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS po5 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '1' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.Total <= '5000000';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getPO510() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS po510 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '1' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.Total BETWEEN '5000001' AND '10000000';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function getPO10() {
		
		$mon = date('m');
		$yer = date('Y');
		$sql="
				SELECT 
				  COUNT(a.`NoDokumen`) AS po10 
				FROM
				  `trans_order_barang_header` a 
				WHERE a.`Status` = '1' 
				  AND YEAR(a.`TglDokumen`) = '$yer' 
				  AND MONTH(a.`TglDokumen`) = '$mon' 
				  AND a.Total > '10000000';
			 ";	
		
		return $this->getRow($sql);
	}
	
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}

	
}
?>