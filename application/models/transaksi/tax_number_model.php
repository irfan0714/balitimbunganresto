<?php
class Tax_number_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

	function ListNoFakturPajak($limit,$offset,$arrSearch){
		$mylib = new globallib();
		
		if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }

        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "b.Nama";
                $arr_keyword[1] = "a.NoFaktur";
                $arr_keyword[2] = "a.NoPO";
                $arr_keyword[3] = "a.NoPenerimaan";
                $arr_keyword[4] = "a.NoFakturPajak";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }


        }
        
        
		 $sql = "SELECT
				  a.`NoFaktur`,a.`NoPO`,a.`NoPenerimaan`,b.`Nama`,a.`NoFakturPajak`
				 FROM
				  invoice_pembelian_header a
				 INNER JOIN supplier b
				    ON a.`KdSupplier` = b.`KdSupplier` 
				 WHERE 1
					 " . $where_keyword . "
				 ORDER BY a.`AddDate` DESC 
				 LIMIT $offset,$limit";
		    
		return $this->getArrayResult($sql);
	}
	
	
	function num_list_pajak_row($arrSearch){
		$mylib = new globallib();

        $where_keyword = "";
        if (count($arrSearch) * 1 > 0) {
            if ($arrSearch["keyword"] != "") {
                unset($arr_keyword);
                $arr_keyword[0] = "b.Nama";
                $arr_keyword[1] = "a.NoFaktur";
                $arr_keyword[2] = "a.NoPO";
                $arr_keyword[3] = "a.NoPenerimaan";

                $search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
                $where_keyword = $search_keyword;
            }

        }

        $sql = "
            SELECT
				  a.`NoFaktur`,a.`NoPO`,a.`NoPenerimaan`,b.`Nama`,a.`NoFakturPajak`
				 FROM
				  invoice_pembelian_header a
				 INNER JOIN supplier b
				    ON a.`KdSupplier` = b.`KdSupplier`
            WHERE 
            	1
            	" . $where_keyword . "
            	     
		";

        return $this->NumResult($sql);
	}
	
	function getDataHeader($nofaktur){
    	$sql = "SELECT h.`NoFaktur`, h.`Tanggal`, h.`JatuhTempo`, s.`Nama`, h.`Kurs`, h.`MataUang`, h.`NoFakturPajak`,
    			 h.`NoFakturSupplier`, h.`NoPO`, h.`PPN`, h.AddUser, h.AddDate, h.EditUser, h.EditDate
				FROM invoice_pembelian_header h 
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				WHERE h.`NoFaktur`='$nofaktur'";
        $row = $this->getArrayResult($sql);
        return $row[0];
	}
	
	function getDataDetail($nofaktur){
    	$sql = "SELECT d.`KdBarang`, m.`NamaLengkap`, h.`NoPenerimaan`, d.`NoUrut`, d.`Qty`, d.`Satuan`, d.`Harga`
				FROM invoice_pembelian_detail d INNER JOIN invoice_pembelian_header h ON d.Nofaktur = h.NoFaktur INNER JOIN masterbarang m ON d.`KdBarang`= m.`PCode`
				WHERE d.`NoFaktur`='$nofaktur'";
        return $this->getArrayResult($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>