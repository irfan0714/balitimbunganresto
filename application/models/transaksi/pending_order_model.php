<?php
class Pending_order_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

	function getMeja()
	{
    	$sql = "
    	SELECT DISTINCT b.`KdMeja` FROM `trans_order_detail` a
		INNER JOIN `trans_order_header` b ON a.`NoTrans`=b.`NoTrans`
		INNER JOIN kassa c ON b.`NoKassa`=c.id_kassa
		WHERE b.`Status`='0' AND b.`Tanggal`= CURDATE() ORDER BY b.`KdMeja` ASC;
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

    function getAllMeja()
	{
    	$sql = "
    	SELECT
		  a.`KdLokasi` AS meja
		FROM
		  `lokasipos` a
		WHERE 1
		  AND a.`KdDivisi` <> '02'
		  AND a.`KdLokasi` NOT IN
		  (SELECT DISTINCT
		    b.`KdMeja` AS meja
		  FROM
		    `trans_order_detail` a
		    INNER JOIN `trans_order_header` b
		      ON a.`NoTrans` = b.`NoTrans`
		    INNER JOIN kassa c
		      ON b.`NoKassa` = c.id_kassa
		  WHERE b.`Status` = '0'
		    AND b.`Tanggal` = CURDATE()
		  ORDER BY b.`KdMeja` ASC)
		ORDER BY a.`KdLokasi` ASC ;
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

    function getMejaAktif()
	{
    	$sql = "
    	SELECT DISTINCT b.`KdMeja` FROM `trans_order_detail` a
		INNER JOIN `trans_order_header` b ON a.`NoTrans`=b.`NoTrans`
		INNER JOIN kassa c ON b.`NoKassa`=c.id_kassa
		WHERE b.`Status`='0' AND b.`Tanggal`= CURDATE() ORDER BY b.`KdMeja` ASC;
    	";

		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }



	public function get_by_id($notrans,$pcode)
	{
		$this->db->from('trans_order_detail');
		$this->db->where('Notrans',$notrans);
		$this->db->where('Tanggal',date('Y-m-d'));
		$this->db->where('PCode',$pcode);
		$query = $this->db->get();

		return $query->row();
	}

	function cekAllCancel($notrans)
	{
    	$sql = "
    	SELECT * FROM `trans_order_detail`a WHERE a.`NoTrans`='$notrans' AND ( a.`Status`='0' OR a.`Status`='1' );
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

	function cek_status($NoTrans)
	{
    	$sql = "
    	SELECT
		  *
		FROM
		  `trans_order_detail` a
		WHERE a.`NoTrans` = '".$NoTrans."'
		  AND a.`Tanggal` = '".date('Y-m-d')."'
		  AND a.`Status` = '0' ;
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }


	function cek_otorisasi($user)
	{
    	$sql = "
    	SELECT * FROM otorisasi_user
			WHERE 1
			AND UserName = '".$user."'
			AND Tipe = 'pending_order'
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

	function validasi_user($user,$pass)
	{
    	$sql = "
    	    SELECT * FROM user
			WHERE 1
			AND UserName = '".$user."'
			AND Password = MD5('".$pass."')
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

	function cek_meja($kdmeja)
	{
    	$sql = "
    	    SELECT * FROM `trans_order_header` a WHERE a.`Tanggal`='".date('Y-m-d')."' AND a.`KdMeja`='".$kdmeja."';
    	";
		//echo $sql;
		return $this->getArrayResult($sql);
    }

	function num_tabel_row($arrSearch)
	{
       $mylib = new globallib();

	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }

        $where_keyword="";
        $where_meja="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.invoiceno";
				$arr_keyword[1] = "a.sidate";
				$arr_keyword[2] = "a.duedate";
				$arr_keyword[3] = "b.Nama";
				$arr_keyword[4] = "a.grandtotal";
				$arr_keyword[5] = "a.status";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["meja"]!="")
			{
				$where_meja = "AND b.KdMeja = '".$arrSearch["meja"]."'";
			}

			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";
			}
		}

    	$sql = "
			  SELECT
			  a.NoTrans,
			  b.`KdMeja`,
			  b.`Tanggal`,
			  b.`Waktu` AS waktu_start,
			  a.`Waktu` AS waktu_finish,
			  IF(
			    a.Waktu <> '00:00:00',
			    TIMEDIFF(a.`Waktu`, b.`Waktu`),
			    '00:00:00'
			  ) AS durasi,
			  a.`KdPersonal`,
			  a.`Qty`,
			  a.`Status` AS status_detail,
			  b.`Status` AS status_haader,
			  a.`PCode`,
			  c.`NamaLengkap`,
			  d.`id_kassa`,
			  d.`KdKategori`
			FROM
			  `trans_order_detail` a
			  INNER JOIN `trans_order_header` b
			    ON a.`NoTrans` = b.`NoTrans`
			  INNER JOIN masterbarang c
			    ON a.`PCode` = c.`PCode`
			  INNER JOIN `kassa` d ON b.`NoKassa`=d.`id_kassa`
			WHERE 1 AND b.Status='0' AND b.`Tanggal`= CURDATE()
				".$where_meja."
			ORDER BY a.`Tanggal`,
			  b.`KdMeja` ASC
        ";
        //echo $sql;
        //echo "<hr/>";
		return $this->NumResult($sql);
    }

	/*function getPendingOrderList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();

	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }

        $where_keyword="";
        $where_meja="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.invoiceno";
				$arr_keyword[1] = "a.sidate";
				$arr_keyword[2] = "a.duedate";
				$arr_keyword[3] = "b.Nama";
				$arr_keyword[4] = "a.grandtotal";
				$arr_keyword[5] = "a.status";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["meja"]!="")
			{
				$where_meja = "AND b.KdMeja = '".$arrSearch["meja"]."'";
			}

			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";
			}
		}

    	$sql = "
			  SELECT
			  a.NoTrans,
			  b.`KdMeja`,
			  b.`Tanggal`,
			  b.`Waktu` AS waktu_start,
			  a.`Waktu` AS waktu_finish,
			  IF(
			    a.Waktu <> '00:00:00',
			    TIMEDIFF(a.`Waktu`, b.`Waktu`),
			    '00:00:00'
			  ) AS durasi,
			  a.`KdPersonal`,
			  a.`Qty`,
			  a.`Status` AS status_detail,
			  b.`Status` AS status_haader,
			  a.`PCode`,
			  c.`NamaLengkap`,
			  d.`id_kassa`,
			  d.`KdKategori`
			FROM
			  `trans_order_detail` a
			  INNER JOIN `trans_order_header` b
			    ON a.`NoTrans` = b.`NoTrans`
			  INNER JOIN masterbarang c
			    ON a.`PCode` = c.`PCode`
			  INNER JOIN `kassa` d ON b.`NoKassa`=d.`id_kassa`
			WHERE 1 AND b.Status='0' AND b.`Tanggal`= CURDATE()
				".$where_meja."
			ORDER BY a.`Tanggal`,
			  b.`KdMeja` ASC
            Limit
              $offset,$limit
        ";
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql);
    }*/

	function getPendingOrderList($key,$limit)
	{
       $mylib = new globallib();

       $where_meja="";

       if($key!="")
			{
		    	$where_meja = "AND b.KdMeja = '$key'";
			}


    	$sql = "
			  SELECT
			  a.NoTrans,
			  b.NoTransApps,
			  b.`KdMeja`,
			  b.`Tanggal`,
			  a.`Waktu` AS waktu_start,
			  a.`Waktu` AS waktu_finish,
			  IF(
			    a.Waktu <> '00:00:00',
			    TIMEDIFF(a.`Waktu`, b.`Waktu`),
			    '00:00:00'
			  ) AS durasi,
			  b.`KdPersonal`,
			  a.`Qty`,
			  a.Berat,
			  a.`Status` AS status_detail,
			  b.`Status` AS status_haader,
			  a.`PCode`,
			  c.`NamaLengkap`,
			  d.`id_kassa`,
			  d.`KdKategori`,
				a.Keterangan 
			FROM
			  `trans_order_detail` a
			  INNER JOIN `trans_order_header` b
			    ON a.`NoTrans` = b.`NoTrans`
			  INNER JOIN masterbarang c
			    ON a.`PCode` = c.`PCode`
			  INNER JOIN `kassa` d ON b.`NoKassa`=d.`id_kassa`
			WHERE 1 AND b.Status='0' AND b.`Tanggal`= CURDATE()
				".$where_meja."
			ORDER BY a.`Tanggal`,
			  b.`KdMeja` ASC
            Limit
              0,$limit
        ";
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql);
    }

    function cekPendingOrderList($key,$limit)
	{
       $mylib = new globallib();

        $where_meja="";

        if($key!="")
			{
		    	$where_meja = "AND b.KdMeja = '$key'";
			}

    	$sql = "
            SELECT
              a.NoTrans,
			  b.`KdMeja`,
			  b.`Tanggal`,
			  a.`Waktu`,
			  a.`Kasir`,
			  a.`Qty`,
			  a.`Status` AS status_detail,
			  b.`Status` AS status_haader,
			  a.`PCode`,
			  c.`NamaLengkap`
			FROM
			  `trans_order_detail` a
			  INNER JOIN `trans_order_header` b
			    ON a.`NoTrans` = b.`NoTrans`
			  INNER JOIN masterbarang c
			    ON a.`PCode` = c.`PCode`
			WHERE 1  AND ( a.Status='1' OR a.Status='2') AND b.`Status`='0' AND b.`Tanggal`= CURDATE()
				".$where_meja."
			ORDER BY a.`Tanggal`,
			  a.`Waktu`,
			  b.`KdMeja` ASC
            Limit
              0,$limit
        ";
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql);
    }

	/*function cekPendingOrderList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();

	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }

        $where_keyword="";
        $where_meja="";
        $where_customer = "";
        $where_status="";
		//print_r($arrSearch);die;
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.invoiceno";
				$arr_keyword[1] = "a.sidate";
				$arr_keyword[2] = "a.duedate";
				$arr_keyword[3] = "b.Nama";
				$arr_keyword[4] = "a.grandtotal";
				$arr_keyword[5] = "a.status";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["meja"]!="")
			{
				$where_meja = "AND b.KdMeja = '".$arrSearch["meja"]."'";
			}

			if($arrSearch["customer"]!="")
			{
				$where_customer = "AND a.customerid = '".$arrSearch["customer"]."'";
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND a.status = '".$arrSearch["status"]."'";
			}
		}

    	$sql = "
            SELECT
              a.NoTrans,
			  b.`KdMeja`,
			  b.`Tanggal`,
			  b.`Waktu`,
			  a.`Kasir`,
			  a.`Qty`,
			  a.`Status` AS status_detail,
			  b.`Status` AS status_haader,
			  a.`PCode`,
			  c.`NamaLengkap`
			FROM
			  `trans_order_detail` a
			  INNER JOIN `trans_order_header` b
			    ON a.`NoTrans` = b.`NoTrans`
			  INNER JOIN masterbarang c
			    ON a.`PCode` = c.`PCode`
			WHERE 1  AND ( a.Status='1' OR a.Status='2') AND b.`Status`='0' AND b.`Tanggal`= CURDATE()
				".$where_meja."
			ORDER BY a.`Tanggal`,
			  a.`Waktu`,
			  b.`KdMeja` ASC
            Limit
              $offset,$limit
        ";
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql);
    }*/

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>
