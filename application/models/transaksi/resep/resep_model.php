<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resep_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getMenu()
	{
		$sql = "		
					SELECT resep_id, resep_header.Pcode, NamaLengkap FROM resep_header
					INNER JOIN masterbarang_touch
					ON masterbarang_touch.`PCode` = resep_header.`Pcode`
					ORDER BY NamaLengkap ASC
				";

		$query1 = $this->db->query($sql);
        $result1 = $query1->result_array();
        return $result1; 

	}

	public function getmenusearch($menu)
	{
		$sql = "
				SELECT resep_id, resep_header.Pcode, NamaLengkap FROM resep_header
				INNER JOIN masterbarang_touch
				ON masterbarang_touch.`PCode` = resep_header.`Pcode`
				WHERE NamaLengkap LIKE '%$menu%'
				ORDER BY NamaLengkap asc
				";

		$query1 = $this->db->query($sql);
        $result1 = $query1->result_array();
        return $result1; 
	}

	function getdataheader($id_resep)
	{
		$sql = "
				
SELECT *, namalengkap FROM resep_detail
INNER JOIN masterbarang ON masterbarang.`PCode` = resep_detail.`Pcode`
WHERE resep_id='$id_resep'
				";

		$query1 = $this->db->query($sql);
        $result1 = $query1->result_array();
        return $result1; 

	}

	public function dataSamEdit()
	{
		$sql =  "SELECT resep_id, NamaLengkap, resep_detail.Pcode, IsiPerPack, Qty, Satuan, HargaPerPack
				FROM resep_detail
				INNER JOIN masterbarang ON masterbarang.`PCode` = resep_detail.`Pcode`";

		$query1 = $this->db->query($sql);
        $result1 = $query1->result_array();
        return $result1; 		
	}

	function deleted($id_resep)
	{
		$qr1 = "DELETE FROM resep_detail WHERE resep_id = '$id_resep'";
		
		$this->db->query($qr1);
		
		//echo $qr1;die;
	}
	

	function getnama()
    {
       /* if($offset !=''){
            $offset = $offset;
           
        }
        else{
            $offset = 0;
             
        }
        $clause="";
        if($id!=""){
            $clause = " where $with like '%$id%'";
        }*/
              /*$sql="
                    
                    SELECT resep_header.resep_id AS Kode, NamaLengkap, qtypcs, satuan, harga
                    FROM resep_header
                    INNER JOIN masterbarang ON masterbarang.`PCode` = resep_header.`Pcode` 
                    INNER JOIN trans_order_barang_detail ON masterbarang.`PCode`=`trans_order_barang_detail`.`PCode`
                    ORDER BY resep_header.AddDate DESC";*/


        $sql ="SELECT * FROM `masterbarang_touch`  ORDER BY NamaLengkap asc";


        $query1 = $this->db->query($sql);
        return $query1; 
    }

    function getlast($pcode)
    {
    	$sql="
				SELECT Sid, Qtypcs, satuan, harga 
				FROM
				trans_order_barang_detail
				WHERE Sid =
					(
						SELECT MAX(Sid) FROM trans_order_barang_detail where PCode='$pcode'
					)
				ORDER BY Sid DESC";

		$query1 = $this->db->query($sql);
		        $result1 = $query1->result_array();
		        return $result1;
}

    function getnamamaster($nama)
    {
    	$sql="
    			SELECT * FROM masterbarang 
    			WHERE NamaLengkap LIKE '%$nama%' 
    			ORDER BY NamaLengkap";

    	$query1 = $this->db->query($sql);
        $result1 = $query1->result_array();
        return $result1;
    }

	

	function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
        return $this->getRow($sql);
        
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

     function getHeader($id) {
       /* $sql = "SELECT 
				  register.*,
				  tourleader.Nama AS NamaMember,
                  tourleader.Phone,
                  CONCAT(tourleader.Bank, ' ', tourleader.NoRekening, ' a/n ', tourleader.Penerima) AS bankrek,
				  tourtravel.KdTravel,
				  tvl.Nama AS nmTravel  
				FROM
				  (SELECT 
					* 
				  FROM
					register
				  ) AS register 
				  LEFT JOIN tourleader 
					ON tourleader.KdTourLeader = register.KdTourLeader 
				  LEFT JOIN tourtravel 
					ON tourtravel.KdTravel = tourleader.KdTravel 
                    LEFT JOIN (SELECT * FROM tourtravel) tvl ON tvl.KdTravel = register.`KdTravel`
				WHERE KdRegister = '$id' 
				ORDER BY Tanggal DESC ";*/

			$sql ="	
					SELECT 
						NamaLengkap, NamaSubDivisi, Harga1c,PPN, Service_charge
					FROM 
						`resep_header`
					INNER JOIN masterbarang ON masterbarang.`PCode` = resep_header.`Pcode`
					INNER JOIN `subdivisi` ON `subdivisi`.`KdSubDivisi` = masterbarang.`KdSubDivisi`
					WHERE 
						resep_header.resep_id ='$id' 
					ORDER BY resep_id DESC
					";

           // echo $sql;
           // die;
        
        return $this->getArrayResult($sql);


    } 

    function NamaPrinter($id)
	{
		$sql = "SELECT * from kassa where ip='$id'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}

	function getHeaderForPrint2($id)
	{		
		$sql = "SELECT 
		  -- SUM(a.qty * a.harga -a.Potongan ) AS total,
		  SUM(a.hargaPPN) AS total,
		  SUM(((a.qty * a.harga) - a.Potongan) * a.persentase / 100) AS komisi,c.NoKassa,k.SubDivisi,k.KdDivisi,d.NamaDivisi,s.NamaSubDivisi
		FROM
		  (SELECT * FROM finance_komisi_detail WHERE NoTransaksi = '$id') a
		INNER JOIN transaksi_header c ON c.NoStruk=a.NoStruk  
		INNER JOIN kassa k ON k.id_kassa=c.NoKassa
		INNER JOIN divisi d ON d.KdDivisi=k.KdDivisi  
		INNER JOIN subdivisi s ON s.KdSubDivisi=k.SubDivisi
		GROUP BY k.KdDivisi,
		  k.SubDivisi ;";		
		return $this->getArrayResult($sql);
	}

      function aplikasi() {
        $sql = "select * from aplikasi";
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
    }

     function getDetailForPrint($id) {
     
        $sql= "
				SELECT dtl.*, NamaLengkap FROM
				(
					SELECT resep_id, Pcode, IsiPerPack, Qty, Satuan, HargaPerPack
					FROM resep_detail
					WHERE resep_id='$id'
					ORDER BY Pcode
				) dtl
				INNER JOIN masterbarang ON masterbarang.`PCode` = dtl.Pcode
				";

		return $this->getArrayResult($sql);
    }

    function getCountDetail($id) {
        $sql = "

SELECT * FROM resep_detail 
INNER JOIN masterbarang ON masterbarang.`PCode` = resep_detail.`Pcode` WHERE resep_id='$id'";
      
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num; 
    }

   function get_isi_perpack($pcode, $satuan)
    {
    	$sql = "
SELECT amount, Satuan_To FROM konversi 
INNER JOIN masterbarang ON masterbarang.`PCode` = konversi.`PCode`
WHERE konversi.`PCode` = '$pcode' and konversi.Satuan_From = '$satuan'";


//echo $sql;die;


       // echo $sql; die;

		$query1 = $this->db->query($sql);
        $result1 = $query1->result_array();
        return $result1; 


    }

}

/* End of file resep_model.php */
/* Location: ./application/models/resep_model.php */