<?php
class Invoice_non_reservasi_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
        
    function getDetailInvoice($No)
	{
		$sql = "SELECT 
				  a.*,
				  b.*,
				  c.`NamaLengkap`,
  IF(c.`Service_charge`>0,ROUND((b.`Harga`*b.`Qty`)+((b.`Harga`*b.`Qty`)*5/100)+(((b.`Harga`*b.`Qty`)+(b.`Harga`*b.`Qty`)*5/100)*10/100)),b.`Harga`*b.`Qty`) AS Nettoz 
				FROM
				  `invoice_non_reservasi` a 
				  INNER JOIN `invoice_non_reservasi_detail` b 
				    ON a.`NoDokumen` = b.`NoDokumen` 
				  LEFT JOIN masterbarang c
				  ON b.`PCode` = c.`PCode`
				WHERE a.`NoDokumen` = '$No' ;";
    	return $this->getArrayResult($sql);
    }
    
    function getBiayaTambahan($No)
	{
		$sql = "SELECT 
				  * 
				FROM
				  `invoice_non_reservasi_detail` a 
				WHERE a.`NoDokumen` = '$No'
				AND a.Tambahan='Y'";
    	return $this->getArrayResult($sql);
    }
    
    function getTravelDetail($No)
	{
		$sql = "
				SELECT 
					a.*,
					a.Total_Nilai AS Total_Nilais,
				  b.*
				FROM
				  `invoice_non_reservasi` a 
				  INNER JOIN `invoice_non_reservasi_customer` b 
				    ON a.`NoDokumen` = b.`NoDokumen`
				  WHERE a.`NoDokumen`='$No';
				";
		
    	 return $this->getRow($sql);
    }
    
    function getTravelList($var)
	{
    	$sql = "SELECT * FROM tourtravel WHERE Nama LIKE '%".$var."%' OR KdTravel LIKE '%".$var."%' ORDER BY Nama ASC";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getDataReservasi($var)
	{
    	$sql = "SELECT * FROM trans_reservasi a WHERE a.KdTravel='$var' AND a.status_konfirmasi='1' AND a.status_invoice='0'";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }
    
    function getTglKonfrmBEO($beo)
	{
		$sql = "
				SELECT 
				  a.`NoDokumen`,
				  a.`NoReservasi`,
				  a.`TglKonfirmasi` AS TglTrans,
				  a.`status`,
				  b.`NoSticker` 
				FROM
				  `trans_reservasi_konfirmasi` a 
				  INNER JOIN `trans_reservasi_konfirmasi_sticker` b 
				    ON a.`NoDokumen` = b.`NoDokumen` 
				WHERE a.`NoReservasi` = '$beo' ;
				";
    	return $this->getArrayResult($sql);
    }
    
    function getUangMuka($beo)
	{
		$sql = "
				SELECT * FROM voucher_beo a WHERE a.`BEO`='$beo' AND Jenis='2';
				";
    	return $this->getArrayResult($sql);
    }
    
    function getKodeBank($kd){

    	$sql = "SELECT KdRekening, KdPenerimaan, KdSubDivisi FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    
    function getNewNo($tgl)
	{
	    $tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoReceipt=NoReceipt+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoReceipt FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	
	function getInvoiceReservasiList($key,$stat,$limit)
	{
       $mylib = new globallib();
        
	 	$where_keyword="";
        $where_status="";
        
			if($key!="" OR $key!="0")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.NoDokumen";
		        
				$search_keyword = $mylib->search_keyword($key, $arr_keyword);
				$where_keyword = $search_keyword;
			}
						
			if($stat!="")
			{
				$where_status = " AND a.Status = '".$stat."'";	
			}
        
    	$sql = "  
                                              
	           SELECT 
				  * 
			   FROM
				    `invoice_non_reservasi` a
				     INNER JOIN invoice_non_reservasi_customer b
				     ON a.NoDokumen = b.NoDokumen 
				WHERE 1
				".$where_keyword."
				".$where_status."
				 ORDER BY a.NoDokumen DESC
	            Limit 
              0,$limit
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
	
	public function get_by_id($id) {
        $sql = "
        		SELECT 
				  *
				FROM
				  `invoice_non_reservasi` a 
				  INNER JOIN `invoice_non_reservasi_customer` b
				  ON a.`NoDokumen` = b.`NoDokumen`
				WHERE a.NoDokumen = '$id' 
        		";
        //echo $sql;
        return $this->getRow($sql);
    }
    
    function getHeader($id)
	{
		$sql = "
			SELECT * FROM `voucher_beo` a INNER JOIN `tourtravel` b ON a.`tourtravel` = b.`KdTravel` WHERE a.`id`='$id';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function getDetail_cetak($id)
	{
		$sql = "
			SELECT * FROM `voucher_beo` a INNER JOIN `tourtravel` b ON a.`tourtravel` = b.`KdTravel` WHERE a.`id`='$id';
		";
		
        return $this->getArrayResult($sql);
	}
    
    function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>