<?php
class Uang_muka_beo_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getKasBank()
	{
		$sql = "SELECT KdKasBank, NamaKasBank FROM kasbank ORDER BY NamaKasBank";
    	return $this->getArrayResult($sql);
    }
    
    function getinterface(){
		$sql = "select * from interface";
		$rec = $this->getArrayResult($sql);
		return $rec[0];
	}
    
    function getBeo($kdtravel='')
	{
		if($kdtravel==''){
			$where  = '';
		}else{
			$where = " and a.KdTravel='$kdtravel'";
		}
		$sql = "
				SELECT 
				  a.NoDokumen 
				FROM
				  trans_reservasi a 
				WHERE 1 
				  AND MONTH(a.`TglDokumen`) = '".date('m')."' 
				  AND YEAR(a.`TglDokumen`) = '".date('Y')."' 
				  AND SUBSTR(a.NoDokumen, 1, 3) = 'SGV'  $where
				ORDER BY a.`NoDokumen` ASC ;
			   ";
    	return $this->getArrayResult($sql);
    }
    
    function getTourtravel()
	{
		$sql = "SELECT * FROM tourtravel ORDER BY Nama";
    	return $this->getArrayResult($sql);
    }
    
    function getKodeBank($kd){

    	$sql = "SELECT KdRekening, KdPenerimaan, KdSubDivisi FROM `kasbank` WHERE KdKasBank='$kd'";

        return $this->getRow($sql);

    }
    
    
    function getNewNo($tgl)
	{
	    $tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoReceipt=NoReceipt+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoReceipt FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	
	function getUangMukaBeoList($key,$stat,$offset,$limit)
	{
       $mylib = new globallib();
       
       if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        		
		$where_keyword="";
        $where_status="";
        
			if($key!="" OR $key!="0")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "a.Tanggal";
				$arr_keyword[1] = "b.Nama";
				$arr_keyword[2] = "a.Keterangan";
				$arr_keyword[3] = "a.NoBukti";
				$arr_keyword[4] = "a.No_Receipt";
				$arr_keyword[5] = "a.NoVoucher";
				$arr_keyword[6] = "a.BEO";
		        
				$search_keyword = $mylib->search_keyword($key, $arr_keyword);
				$where_keyword = $search_keyword;
			}
						
			if($stat!="")
			{
				$where_status = " AND a.status = '".$stat."'";	
			}
        
    	$sql = "  
                                              
	           SELECT 
				  * 
			   FROM
				  `uang_muka_beo` a 
				  inner join `tourtravel` b 
				    on a.`KdTravel` = b.`KdTravel` 
				  Left join `kasbank` c 
				    on a.`KdBank` = c.`KdKasBank`
				WHERE 1
				".$where_keyword."
				".$where_status."
				 ORDER BY a.id DESC
	            Limit 
              $offset,$limit
        ";               
        //echo $sql;
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
	
	public function get_by_id($id) {
		//FORMAT(a.`Nilai`,0) AS Nilai,
        $sql = "
        		 SELECT 
				  a.id,
				  DATE_FORMAT(a.`Tanggal`, '%d-%m-%Y') AS Tanggal,
				  DATE_FORMAT(DATE_ADD(d.`ReservasiDate`, INTERVAL 1 WEEK), '%d-%m-%Y') AS Tanggal_beo,
				  a.KdTravel,
				  ROUND(a.`Nilai`) AS Nilai,
				  a.BiayaAdmin,
				  b.Nama,
				  a.`KdBank`,
				  c.`NamaKasBank`,
				  a.`NoBukti`,
				  a.`Keterangan`,
				  a.no_receipt,
				  a.BEO,
				  b.Nama 
				FROM
				  `uang_muka_beo` a 
				  INNER JOIN `tourtravel` b 
				    ON a.`KdTravel` = b.`KdTravel` 
				  INNER JOIN kasbank c 
				    ON a.`KdBank` = c.`KdKasBank` 
				  INNER JOIN trans_reservasi d
				    ON a.`BEO` = d.`NoDokumen`
				WHERE a.id = '$id'
        		";
        //echo $sql;
        return $this->getRow($sql);
    }
    
    function num_uang_muka_beo_row()
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM uang_muka_beo a;       
		";
		                  
        return $this->NumResult($sql);
	}
    
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>