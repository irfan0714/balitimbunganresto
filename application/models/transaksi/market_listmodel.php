<?php
class Market_listmodel extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";
        return $this->getRow($sql);
	}

	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan`
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."'
				UNION
				SELECT
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }

    function cekStatusBarang($pcode)
	{
    	$sql = "
    			SELECT masterbarang.NamaLengkap, masterbarang.`Status` FROM masterbarang WHERE masterbarang.`PCode`='".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }

	function cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
	{
	// $tahun." - ".$gudang." - ".$pcode." - ".$tabel_field;die;
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$fieldupdate.",a.".$fieldnupdate.",a.".$fieldakhir.",a.".$fieldnakhir." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$KdGudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

	function ambilQty($pcode,$nodok)
	{
		$sql = "
			SELECT *
			FROM
			  market_list_barang_detail INNER JOIN
			  market_list_barang_header ON market_list_barang_header.`NoDokumen` = market_list_barang_detail.`NoDokumen`
			WHERE 1
			  AND market_list_barang_detail.PCode = '".$pcode."'
			  AND market_list_barang_detail.NoDokumen = '".$nodok."'
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}

	function ambilDetail($nodok)
	{
		$sql = "
			SELECT *
			FROM
			  market_list_barang_detail INNER JOIN
			  market_list_barang_header ON market_list_barang_detail.`NoDokumen` = market_list_barang_header.`NoDokumen`
			WHERE 1
			  AND market_list_barang_detail.NoDokumen = '".$nodok."'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}

    function getPermintaanBarangList($limit,$offset,$arrSearch)
	{
        $mylib = new globallib();

	 	if($offset !=''){
			$offset = $offset;
		}
        else{
        	$offset = 0;
        }

        $where_keyword="";
        $where_gudang="";
        $where_divisi = "";
        $where_gudang_admin = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "market_list_barang_header.NoDokumen";
				$arr_keyword[1] = "market_list_barang_header.Keterangan";
				$arr_keyword[2] = "gudang.Keterangan";
				$arr_keyword[3] = "divisi.NamaDivisi";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND market_list_barang_header.KdGudang = '".$arrSearch["gudang"]."'";
			}

			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND market_list_barang_header.KdDivisi = '".$arrSearch["divisi"]."'";
			}

			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "market_list_barang_header.KdGudang", "in");
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND market_list_barang_header.Status = '".$arrSearch["status"]."'";
			}
		}

    	$sql = "
            SELECT
              market_list_barang_header.NoDokumen,
              DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
              DATE_FORMAT(TglTerima,'%d-%m-%Y') AS TglTerima,
              market_list_barang_header.Keterangan,
              market_list_barang_header.KdGudang,
              gudang.Keterangan AS NamaGudang,
              market_list_barang_header.KdDivisi,
              divisi.NamaDivisi,
              market_list_barang_header.Status
            FROM
              market_list_barang_header
              LEFT JOIN gudang
                ON gudang.KdGudang = market_list_barang_header.KdGudang
              LEFT JOIN divisi
                ON divisi.KdDivisi = market_list_barang_header.KdDivisi
            WHERE
            	1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_divisi."
            	".$where_gudang_admin."
            	".$where_status."
            ORDER BY
              CAST(NoDokumen AS UNSIGNED) DESC,
              market_list_barang_header.NoDokumen DESC
            Limit
              $offset,$limit
        ";
       /* echo $sql;
        echo "<hr/>";*/
		return $this->getArrayResult($sql);
    }

    function num_market_list_row($arrSearch)
    {
        $mylib = new globallib();

        $where_keyword="";
        $where_gudang="";
        $where_divisi = "";
        $where_gudang_admin = "";
        $where_status="";
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "market_list_barang_header.NoDokumen";
				$arr_keyword[1] = "market_list_barang_header.Keterangan";
				$arr_keyword[2] = "gudang.Keterangan";
				$arr_keyword[3] = "divisi.NamaDivisi";

				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}

			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND market_list_barang_header.KdGudang = '".$arrSearch["gudang"]."'";
			}

			if($arrSearch["divisi"]!="")
			{
				$where_divisi = "AND market_list_barang_header.KdDivisi = '".$arrSearch["divisi"]."'";
			}

			if($arrSearch["gudang_admin"]!="")
			{
				$where_gudang_admin = $mylib->where_array($arrSearch["gudang_admin"], "market_list_barang_header.KdGudang", "in");
			}

			if($arrSearch["status"]!="")
			{
				$where_status = "AND market_list_barang_header.Status = '".$arrSearch["status"]."'";
			}
		}

		$sql = "
            SELECT
              market_list_barang_header.NoDokumen,
              DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
              DATE_FORMAT(TglTerima,'%d-%m-%Y') AS TglTerima,
              market_list_barang_header.Keterangan,
              market_list_barang_header.KdGudang,
              gudang.Keterangan AS NamaGudang,
              market_list_barang_header.KdDivisi,
              divisi.NamaDivisi,
              market_list_barang_header.Status
            FROM
              market_list_barang_header
              LEFT JOIN gudang
                ON gudang.KdGudang = market_list_barang_header.KdGudang
              LEFT JOIN divisi
                ON divisi.KdDivisi = market_list_barang_header.KdDivisi
            WHERE
            	1
            	".$where_keyword."
            	".$where_gudang."
            	".$where_divisi."
            	".$where_gudang_admin."
            	".$where_status."
		";

        return $this->NumResult($sql);
	}

	function getSearch($id,$module,$user)
	{
		$sql = "SELECT * FROM ci_query WHERE id ='$id' AND module='$module' AND AddUser='$user' ";
		return $this->getRow($sql);
	}

	function getDate()
	{
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

	function getDivisi()
	{
    	$sql = "SELECT KdDivisi,NamaDivisi FROM divisi ORDER BY divisi.NamaDivisi ASC";
		return $this->getArrayResult($sql);
    }

	function getGudang($arr_gudang="")
	{
		$mylib = new globallib();

		$where_gudang_admin="";
		if(count($arr_gudang)>0)
		{
			$where_gudang_admin = $mylib->where_array($arr_gudang, "gudang.KdGudang", "in");
		}

    	$sql = "SELECT KdGudang,Keterangan as NamaGudang FROM gudang WHERE 1 ".$where_gudang_admin." order by KdGudang";
    	return $this->getArrayResult($sql);
    }

	function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }

	function getDetail($id)
	{
		$sql = "
			SELECT
			  market_list_barang_detail.* ,
			  satuan.NamaSatuan
			FROM
			  market_list_barang_detail INNER JOIN satuan
			ON market_list_barang_detail.Satuan = satuan.KdSatuan
			WHERE 1
			  AND market_list_barang_detail.NoDokumen = '$id'
			ORDER BY
			  market_list_barang_detail.sid DESC
		";

        return $this->getArrayResult($sql);
	}

	function getHeader($id)
	{
		$sql = "
			SELECT
			  market_list_barang_header.NoDokumen,
			  DATE_FORMAT(TglDokumen, '%d-%m-%Y') AS Tanggal,
			  DATE_FORMAT(TglTerima, '%d-%m-%Y') AS TglTerima,
			  market_list_barang_header.Keterangan,
			  market_list_barang_header.KdGudang,
			  gudang.Keterangan AS NamaGudang,
			  market_list_barang_header.KdDivisi,
			  divisi.NamaDivisi,
			  market_list_barang_header.Status,
			  DATE_FORMAT(market_list_barang_header.AddDate, '%d-%m-%Y') AS AddDate,
			  market_list_barang_header.AddUser,
			  DATE_FORMAT(market_list_barang_header.EditDate, '%d-%m-%Y') AS EditDate,
			  market_list_barang_header.EditUser
			FROM
			  market_list_barang_header
			  LEFT JOIN gudang
			    ON gudang.KdGudang = market_list_barang_header.KdGudang
			  LEFT JOIN divisi
			    ON divisi.KdDivisi = market_list_barang_header.KdDivisi
			WHERE 1
			  AND market_list_barang_header.NoDokumen = '$id'
			LIMIT 1
        ";

        return $this->getRow($sql);
	}

	function cekNodok($id)
	{
		$sql = "
			SELECT
  			  trans_pr_header.NoDokumen,
			  trans_pr_header.NoPermintaan,
			  DATE_FORMAT(trans_pr_header.AddDate, '%d-%m-%Y') AS AddDate,
			  trans_pr_header.AddUser
			FROM
			  trans_pr_header
			WHERE 1
			  AND trans_pr_header.NoPermintaan = '".$id."'
		";

		return $this->getRow($sql);
	}

	function getMarketList($id)
	{
		$sql = "
			SELECT
			  market_list.market_list_name,
			  market_list_detail.pcode,
			  masterbarang.NamaLengkap,
			  masterbarang.SatuanSt
			FROM
			  market_list_pair
			  INNER JOIN market_list
			    ON market_list_pair.market_list_id = market_list.market_list_id
			  INNER JOIN market_list_detail
			    ON market_list_pair.market_list_id = market_list_detail.market_list_id
			  INNER JOIN masterbarang
			    ON market_list_detail.pcode = masterbarang.PCode
			WHERE 1
			  AND market_list_pair.UserLevelID = '".$id."'
			ORDER BY
			  market_list.market_list_name ASC,
			  masterbarang.NamaLengkap ASC
		";
        return $this->getArrayResult($sql);
	}

	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT
			  market_list_barang_detail.*
			FROM
			  market_list_barang_detail
			WHERE 1
			  AND market_list_barang_detail.PCode = '$pcode'
			  AND market_list_barang_detail.NoDokumen = '$nodok'
			LIMIT 1
		";

        return $this->getRow($sql);
	}

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}

	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}

	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>
