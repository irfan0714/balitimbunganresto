<?php
class Po_marketing_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    } 
    
    function getWarehouse()
	{
    	$sql = "SELECT a.warehousecode,a.warehousename FROM warehouse a ORDER BY a.warehousecode ASC";
		return $this->getArrayResult($sql);
    }
	
	function cek_nil_proposal($nopo)
	{
    	$sql = "
				SELECT SUM(cek.jml) AS total_proposal  FROM (
				SELECT 
				  proposal_detail.`HargaSatuan` * proposal_detail.`Qty` AS jml 
				FROM
				  `pr_marketing`
				  INNER JOIN po_marketing
				  ON po_marketing.`NoPr`=pr_marketing.`NoDokumen`
				  INNER JOIN proposal 
					ON `pr_marketing`.`NoProposal` = proposal.`NoProposal`
					INNER JOIN proposal_detail
					ON proposal.`NoProposal` = proposal_detail.`NoProposal` 
				WHERE po_marketing.`NoDokumen` = '".$nopo."' 
				UNION ALL
				SELECT 
				   proposal_target.`HargaSatuan` * proposal_target.`Qty` AS jml
				FROM
				  `pr_marketing` 
				  INNER JOIN po_marketing
				  ON po_marketing.`NoPr`=pr_marketing.`NoDokumen`
				  INNER JOIN proposal 
					ON `pr_marketing`.`NoProposal` = proposal.`NoProposal`
					INNER JOIN proposal_target
					ON proposal.`NoProposal` = proposal_target.`NoProposal` 
				WHERE po_marketing.`NoDokumen` = '".$nopo."' ) AS cek
				;
				";
		return $this->getRow($sql);
    }
	
	function cek_apakah_ada_yang_pakai_proposal_ini($nopo)
	{
    	$sql = "
				SELECT 
				  SUM(po_marketing.`Jumlah`) AS total 
				FROM
				  po_marketing 
				  INNER JOIN pr_marketing 
					ON po_marketing.`NoPr` = pr_marketing.`NoDokumen`
				   WHERE `po_marketing`.`Status` <> '2' AND pr_marketing.`NoProposal` IN
				(   
				   SELECT 
				  pr_marketing.`NoProposal` 
				FROM
				  po_marketing 
				  INNER JOIN pr_marketing 
					ON po_marketing.`NoPr` = pr_marketing.`NoDokumen` 
				WHERE po_marketing.`NoDokumen` = '".$nopo."' 
				);
				";
		return $this->getRow($sql);
    }
	
	function getGudang()
	{
    	$sql = "SELECT a.`KdGudang`,a.`Keterangan` FROM gudang a  ORDER BY a.`KdGudang` ASC;";
		//echo $sql;die;
		return $this->getArrayResult($sql);
    }
	
	function getKonversi($pcode, $Satuan_From)
	{
		$sql = "
				SELECT * FROM `konversi` a WHERE a.`PCode`='".$pcode."' AND a.`Satuan_From`='".$Satuan_From."';
               ";     
        return $this->getRow($sql);
	}
	
	function getSatuanDetail($pcode)
	{
    	$sql = "
    			SELECT 
				  a.`SatuanSt` AS Satuan, c.`NamaSatuan` 
				FROM
				  masterbarang a INNER JOIN satuan c ON a.`SatuanSt`=c.`KdSatuan`
				WHERE a.`PCode` = '".$pcode."' 
				UNION
				SELECT 
				  b.Satuan_From, d.`NamaSatuan`
				FROM
				  konversi b INNER JOIN satuan d ON b.`Satuan_From`=d.`KdSatuan`
				WHERE b.PCode = '".$pcode."' ;
    		   ";
        return $this->db->query($sql);
    }	
    
    
    function num_po_marketing_row($arrSearch)
    {
        $mylib = new globallib();
        
		
		$sql = "
            SELECT * FROM `po_marketing` a;       
		";
		                  
        return $this->NumResult($sql);
	}
		
	function getpo_marketingList($limit,$offset,$arrSearch)
	{
       $mylib = new globallib();
        
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
        
        $where_keyword="";
        $where_gudang="";
        $where_sullier = "";
        $where_status="";
        $where_awal="";
        $where_akhir="";
        $where_order="po_marketing.NoDokumen ASC";


        // echo count($arrSearch)*1;
		// print_r($arrSearch);
        if(count($arrSearch)*1>0)
        {
			if($arrSearch["keyword"]!="")
			{
		    	unset($arr_keyword);
		        $arr_keyword[0] = "po_marketing.NoDokumen";
				$arr_keyword[1] = "po_marketing.NoPr";
				$arr_keyword[2] = "po_marketing.Keterangan";
		        
				$search_keyword = $mylib->search_keyword($arrSearch["keyword"], $arr_keyword);
				$where_keyword = $search_keyword;
			}
			
			if($arrSearch["gudang"]!="")
			{
				$where_gudang = "AND a.warehousecode = '".$arrSearch["gudang"]."'";	
			}
			
			if($arrSearch["supplier"]!="")
			{
				$where_supplier = "AND po_marketing.KdSupplier = '".$arrSearch["supplier"]."'";	
			}
			
			if($arrSearch["status"]!="")
			{
				$where_status = "AND po_marketing.Status = '".$arrSearch["status"]."'";	
			}

			if($arrSearch["tgl_awal"]!="")
			{

				$where_awal = "AND po_marketing.TglDokumen BETWEEN '".$arrSearch["tgl_awal"]."'";
				$where_order = "po_marketing.TglDokumen ASC";
			}

			if($arrSearch["tgl_akhir"]!="")
			{
				$where_akhir = "AND '".$arrSearch["tgl_akhir"]."'";	
			}
		} 
        
    	$sql = "  
            SELECT 
			`po_marketing`.NoDokumen,
			DATE_FORMAT(po_marketing.TglDokumen, '%d-%m-%Y') AS TglDokumen,
			`po_marketing`.NoPr,
			`po_marketing`.Keterangan,
			`po_marketing`.Total,
			`po_marketing`.status,
			`po_marketing`.Approval_By AS Approval_By_,
			DATE_FORMAT(po_marketing.Approval_Date, '%d-%m-%Y') AS Approval_Date_,
			`po_marketing`.Approval_Status AS Approval_Status_,
			`po_marketing`.Approval_Remarks AS Approval_Remarks_,
			supplier.*
			FROM `po_marketing` INNER JOIN pr_marketing ON po_marketing.`NoPr`=pr_marketing.`NoDokumen`
			INNER JOIN supplier ON po_marketing.`KdSupplier`=supplier.`KdSupplier`
			where 1 
            	".$where_keyword."
            	".$where_supplier."    
            	".$where_status."
            	".$where_awal."
            	".$where_akhir."                                   
            ORDER BY 
              ".$where_order."
            Limit 
              $offset,$limit
        ";               
        // echo $sql;
        $user = $this->session->userdata('username');
        if($user == 'mechael0101'){
        // echo $sql;
        		
        }
        //echo "<hr/>";
		return $this->getArrayResult($sql); 
    }
    
    
    function getHeader($id)
	{
		$sql = "
			SELECT 
			  po_marketing.*,
			  po_marketing.`PPn` AS PPn_,
			  pr_marketing.*,
			  pr_marketing_detail.*,
			 
			  supplier.*,
			  mata_uang.*,
			  DATE_FORMAT(po_marketing.TglDokumen, '%d-%m-%Y') AS TglDokumen_,
			  DATE_FORMAT(po_marketing.TglTerima, '%d-%m-%Y') AS TglTerima_,
			  po_marketing.Keterangan AS Keterangan_,
			  po_marketing.`currencycode` AS currencycode_,
			  po_marketing.`TOP` AS top_,
			  po_marketing.`NoDokumen` AS NoDokumen_,
			  po_marketing.`PPn` AS PPn_,
			  po_marketing.`Approval_Status` AS Approval_Status_,
			  po_marketing.`Status` AS Status_
			FROM
			  po_marketing 
			  INNER JOIN pr_marketing 
			    ON po_marketing.`NoPr` = pr_marketing.`NoDokumen` 
			  INNER JOIN pr_marketing_detail
			   ON pr_marketing.`NoDokumen` = pr_marketing_detail.`NoDokumen`
			  
			  INNER JOIN supplier 
			    ON supplier.`KdSupplier` = po_marketing.`KdSupplier`
			  INNER JOIN mata_uang ON mata_uang.`Kd_Uang` = po_marketing.`currencycode`  
			WHERE po_marketing.`NoDokumen`  = '".$id."';
        ";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.* 
			FROM
			  deliveryorderdetail
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function getTop($sup)
	{
		$sql = "
			SELECT
                    supplier.TOP
                FROM
                    supplier
                WHERE
                    1
                    AND supplier.KdSupplier = '".$sup."'
                LIMIT
                    0,1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail2($pcode,$nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1 
			  AND deliveryorderdetail.inventorycode = '$pcode' 
			  AND deliveryorderdetail.dono = '$nodok' 
			LIMIT 1
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	function cekGetDetail3($nodok)
	{
		$sql = "
			SELECT 
			  deliveryorderdetail.`sid`,
			  deliveryorderdetail.`dono`,
			  deliveryorderdetail.`quantity`,
			  deliveryorderdetail.`inventorycode`,
			  deliveryorder.`warehousecode`,
			  deliveryorder.`adddate`
			FROM
			  deliveryorderdetail INNER JOIN
			  deliveryorder ON deliveryorder.`dono` = deliveryorderdetail.`dono`
			WHERE 1  
			  AND deliveryorderdetail.dono = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function cekGetMutasi($NoTransaksi, $Gudang, $Tanggal, $KodeBarang)
	{
		$sql = "
			SELECT * FROM `mutasi` a 
			WHERE a.`NoTransaksi`='".$NoTransaksi."' 
			AND a.`KdTransaksi`='FG' 
			AND a.`Gudang`='".$Gudang."' 
			AND a.`Tanggal`='".$Tanggal."' 
			AND a.`KodeBarang`='".$KodeBarang."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
	
	
	function cekGetStock($tahun,$gudang,$pcode,$tabel_field)
	{
		$sql = "
			SELECT a.`Tahun`,a.`KdGudang`,a.`PCode`,a.".$tabel_field." FROM `stock` a WHERE a.`Tahun`='".$tahun."' AND a.`KdGudang`='".$gudang."' AND a.`PCode`='".$pcode."';
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getDetailList($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  po_marketing_detail 
			  WHERE po_marketing_detail.NoDokumen= '".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	function getDetailListTemp($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  po_marketing_detail_temp 
			  WHERE po_marketing_detail_temp.NoDokumen= '".$id."';
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
    
    function getDetail($id)
	{
		$sql = "
			SELECT * FROM `deliveryorderdetail` a WHERE 1 AND a.`dono`='".$id."' ORDER BY a.`sid` DESC;
		";
        return $this->getArrayResult($sql);
	}
	
	function getPrMarketingDetail($v_NoPr)
	{
		$sql = "
			SELECT * FROM pr_marketing_detail WHERE pr_marketing_detail.NoDokumen='".$v_NoPr."' AND QtyPOM<Qty ORDER BY NoUrut ASC;
		";
        return $this->getArrayResult($sql);
	}
	
	 function getDetail_cetak($id)
	{
		$sql = "
			SELECT 
			  * 
			FROM
			  po_marketing_detail 
			  WHERE po_marketing_detail.NoDokumen= '".$id."';
		";
		
        return $this->getArrayResult($sql);
	}
	
	function getQtyPom($nodok)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  po_marketing_detail a
			WHERE 1  
			  AND a.NoDokumen = '$nodok'
		";
		//echo $sql;die;
        return $this->getArrayResult($sql);
	}
	
	
	function getQtyPoPrm($v_NoPr, $NamaBarangPOM)
	{
		$sql = "
			SELECT 
			  *
			FROM
			  pr_marketing_detail a
			WHERE 1  
			  AND a.NoDokumen = '$v_NoPr'
			  AND a.NamaBarang = '$NamaBarangPOM'
		";
		//echo $sql;die;
        return $this->getRow($sql);
	}
    
    
    function getSupplier()
	{
    	$sql = "SELECT a.KdSupplier,a.Nama FROM supplier a ORDER BY a.Nama ASC";
    	return $this->getArrayResult($sql);
    }
    
    function getCurrency()
	{
    	$sql = "SELECT * FROM mata_uang where FlagAktif='A' ORDER BY id ASC;";
    	return $this->getArrayResult($sql);
    }
    
    function getSatuan()
	{
    	$sql = "SELECT KdSatuan, NamaSatuan FROM satuan ORDER BY satuan.NamaSatuan ASC";
		return $this->getArrayResult($sql);
    }
    
    function cekNodok($id)
	{
		$sql = "
			SELECT * FROM `deliveryorder` a WHERE a.`dono`='".$id."';
		";
		
		return $this->getRow($sql);
	}
	
	

	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}

	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
	
	
}
?>