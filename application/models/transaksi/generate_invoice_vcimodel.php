<?php

class generate_invoice_vcimodel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }
    
    /*
    function ceksudahada($tahun, $bulan){
		$awalbulan = $tahun.'-'.$bulan.'-01';
		$akhirbulan = date("Y-m-t", strtotime($awalbulan));
		$sql = "select * from deliveryorder where dodate='$akhirbulan' and warehousecode='18' and status=1";
		$row = $this->getArrayResult($sql);
		if(count($row)>0)
			return true	;
		else	
			return false;
	}
	*/

	function ceksudahada($v_tglawal, $v_tglakhir){
		$sql = "select * from deliveryorder where warehousecode='18' AND status=1 AND dodate BETWEEN '$v_tglawal' AND '$v_tglakhir' "; 
		$row = $this->getArrayResult($sql);
		if(count($row)>0)
			return true	;
		else	
			return false;
	}
	
    function getharga($pcode){
		$row = array();
		$sqlharga = "SELECT m.PCode, CEIL(Harga) AS Harga, NamaLengkap FROM groupharga_detail d INNER JOIN mapping_barang_vci m ON d.`PCode`=m.`PCode`
					inner join masterbarang b on m.pcode=b.pcode
					WHERE GroupHargaID=1 AND m.`PCodeVCI`='$pcode'";
		$row = $this->getArrayResult($sqlharga);
		return $row;
	}
	
	function gethargabeli($pcode, $tgl){
		$row = array();
		$sqlharga = "SELECT CEIL(d.`HargaContract`) AS HargaContract FROM contract_supplier_header h INNER JOIN contract_supplier_detail d ON h.`NoTransaksi`=d.`NoTransaksi`
					inner join mapping_barang_vci v on d.PCode=v.PCode
					WHERE h.`KdSupplier`='HO0001' AND h.`PeriodeAwal`<='$tgl' 
					AND h.`PeriodeAkhir`>='$tgl' AND v.`PCodeVCI`='$pcode' ORDER BY h.NoTransaksi DESC";
		$row = $this->getArrayResult($sqlharga);
		return $row;
	}
	
	
	function simpan($rgno, $nofaktur,$dono, $invoiceno, $pcode, $qty, $harga, $hargacontract, $tahun, $bulan,$tglakhir ){
		$mylib = new globallib();
		// $awalbulan = $tahun.'-'.$bulan.'-01';
        // $akhirbulan = date("Y-m-t", strtotime($awalbulan));
		$akhirbulan = $tglakhir;
		$user = $this->session->userdata('username');
		$jumlah = 0;
		$jumlahppn = 0;
		$total = 0;
		$ppnjual=0;
		$totaljual=0;
		
		$this->db->trans_start();
		
		for($i=0;$i<count($pcode);$i++){
			$v_qty = $mylib->save_int($qty[$i]);
			$v_harga = $mylib->save_int($harga[$i]);
			$v_hargacontract = $mylib->save_int($hargacontract[$i]);
			$jumlah += $v_qty * $v_hargacontract;
			$jumlahppn += $v_qty * $v_hargacontract*0.1;
			$total += $v_qty * $v_hargacontract*1.1;
		
			$ppnjual += $v_qty*$v_harga*0.1;
			$totaljual += $v_qty*$v_harga*1.1;
			
			//RG
			$data_detail = array('NoDokumen' => $rgno,
							  'TglDokumen' => $akhirbulan,
							  'KdSupplier' => 'HO0001',
							  'KdGudang' => 18,
							  'PCode' => $pcode[$i],
							  'Qty' => $qty[$i],
							  'QtyPcs' => $qty[$i],
							  'Satuan' => 'PCS',
							  'Harga' => $v_hargacontract,
							  'Disc1' => 0,
							  'Disc2' => 0,
							  'Potongan' => 0,
							  'Jumlah' => $v_qty*$v_hargacontract,
							  'PPn' => $v_qty*$v_hargacontract*0.1,
							  'Total' => $v_qty*$v_hargacontract*1.1,
							  'Hpp' => 0,
							  'FlagKonfirmasi' => 'Y',
							  'Status' => 1,
							  'AddDate' => date('Y-m-d'),
							  'AddUser' => $user ,
							  'EditDate' => '',
							  'EditUser' => '0000-00-00');
			$this->db->insert('trans_terima_detail', $data_detail);
			
			//PI
			$data_detail = array('NoFaktur' => $nofaktur,
								  'NoUrut' => $i+1,
								  'KdBarang' => $pcode[$i],
								  'Qty' => $v_qty,
								  'Satuan' => 'PCS',
								  'Harga' => $v_hargacontract,
								  'PPN' => 10);
			$this->db->insert('invoice_pembelian_detail', $data_detail);
			
			//DO
			$data_detail = array('dono'=>$dono,
								  'inventorycode'=>	$pcode[$i],
								  'quantity'=> $v_qty,
								  'Satuan' => 'PCS',
								  'QtyPcs' => $v_qty);
			$this->db->insert('deliveryorderdetail', $data_detail);
			
			//SI
			$data_detail = array('invoiceno'=>$invoiceno,
								  'dono'=>	$dono,
								  'quantity'=> $v_qty,
								  'inventorycode'=> $pcode[$i],
								  'nettprice' => $v_harga,
								  'sellprice' => $v_harga);
			$this->db->insert('salesinvoicedetail', $data_detail);
			
			//Mutasi Stock
			$data_mutasi_rg = array('NoTransaksi' => $rgno,
							  'Jenis' => 'I',
							  'KdTransaksi' => 'RG',
							  'Gudang' => 18,
							  'GudangTujuan' => 18,
							  'Tanggal' => $akhirbulan,
							  'KodeBarang' => $pcode[$i],
							  'Qty' => $v_qty,
							  'Nilai' => $v_hargacontract,
							  'Status' => 1);
			$this->db->insert('mutasi', $data_mutasi_rg);
							  
			$data_mutasi_do = array('NoTransaksi' => $dono,
							  'Jenis' => 'O',
							  'KdTransaksi' => 'R',
							  'Gudang' => 18,
							  'GudangTujuan' => 18,
							  'Tanggal' => $akhirbulan,
							  'KodeBarang' => $pcode[$i],
							  'Qty' => $v_qty,
							  'Nilai' => $v_hargacontract,
							  'Status' => 1);
			$this->db->insert('mutasi', $data_mutasi_do);
		}
		
		//RG
		$data_header = array('NoDokumen' => $rgno,
							'TglDokumen' => $akhirbulan,
							'KdGudang' => 18,
							'SumberOrder' => 'M',
							'PoNo' => '',
							'NoSuratJalan' => '',
							'Keterangan' => 'RG Dari VCI',
							'KdSupplier' => 'HO0001',
							'currencycode' => 'IDR',
							'Jumlah' => $jumlah,
							'PPn' => 10,
							'NilaiPPn' => $jumlahppn,
							'Disc' => 0,
							'Rate' => 1,
							'Total' => $total,
							'FlagKonfirmasi' => 'Y',
							'BiayaTransport' => 0,
							'Status' => 1,
							'AddDate' => date('Y-m-d'),
							'AddUser' => $user,
							'EditDate' => '',
							'EditUser' => '0000-00-00');
							
		$this->db->insert('trans_terima_header', $data_header);
		
		//PI
		$data_header = array('NoFaktur' => $nofaktur,
							  'Tanggal' => $akhirbulan,
							  'JatuhTempo' => $akhirbulan,
							  'NoPO' => '',
							  'NoPenerimaan' => $rgno,
							  'NoFakturSupplier' => '-',
							  'NoFakturPajak' => '',
							  'KdSupplier' => 'HO0001',
							  'MataUang' => 'IDR',
							  'Kurs' => 1,
							  'PPN' => 10,
							  'NoPosting' => '',
							  'BiayaTransport' => 0,
							  'AddDate' => date('Y-m-d'),
							  'AddUser' => $user ,
							  'EditDate' => '',
							  'EditUser' => '0000-00-00');
		$this->db->insert('invoice_pembelian_header', $data_header);
		
		//Hutang
		$data_hutang = array('NoDokumen' =>$nofaktur,
							'NoFaktur'=>$nofaktur,
							'Tanggal'=>$akhirbulan,
							'KdSupplier'=>'HO0001',
							'TipeTransaksi'=>'I',
							'JatuhTempo'=>$akhirbulan,
							'NilaiTransaksi'=>$total,
							'Sisa'=>$total,
							'MataUang' => 'IDR',
							'Kurs' => 1);
		$this->db->insert('hutang', $data_hutang);
		
		//Mutasi Hutang
		$data_mutasi_hutang = array('Tahun' =>$tahun,
									'Bulan'=>$bulan,
									'NoDokumen'=>$nofaktur,
									'Tanggal'=>$akhirbulan,
									'KdSupplier'=>'HO0001',
									'MataUang' => 'IDR',
									'Kurs' => 1,
									'Jumlah'=>$total,
									'TipeTransaksi'=>'I',
									'MataUang'=>'IDR',
									'Kurs'=>1,
									'SelisihKurs'=>0);
		$this->db->insert('mutasi_hutang', $data_mutasi_hutang);
		
		//DO
		$data_header = array('dono'=>$dono,
							 'dodate' => $akhirbulan,
							 'contactperson' => 'Samsul',
							 'warehousecode' => 18,
							 'isprint' => 0,
							 'status' => 1,
							 'note' => '',
							 'hasinvoice' => 1,
							 'customerid' => 'PT1001',
							 'adddate'=>date('Y-m-d'),
							 'adduser'=>$user);
		$this->db->insert('deliveryorder', $data_header);
		
		//SI
		$data_header = array('invoiceno'=>$invoiceno,
							 'sidate' => $akhirbulan,
							 'customerid' => 'PT1001',
							 'duedate' => $akhirbulan,
							 'ppnamount'=> $ppnjual,
							 'status' => 1,
							 'grandtotal'=> $totaljual,
							 'adddate'=>date('Y-m-d'),
							 'adduser'=>$user);
		$this->db->insert('salesinvoice', $data_header);
		
		//Piutang
		$data_piutang = array('NoDokumen' =>$invoiceno,
							'NoFaktur'=>$invoiceno,
							'TglTransaksi'=>$akhirbulan,
							'KdCustomer'=>'PT1001',
							'TipeTransaksi'=>'I',
							'JatuhTempo'=>$akhirbulan,
							'NilaiTransaksi'=>$totaljual,
							'Sisa'=>$totaljual);
		$this->db->insert('piutang', $data_piutang);
		
		//Mutasi Piutang
		$data_mutasi_piutang = array('Tahun' =>$tahun,
									'Bulan'=>$bulan,
									'NoDokumen'=>$invoiceno,
									'Tanggal'=>$akhirbulan,
									'KdCustomer'=>'PT1001',
									'Jumlah'=>$totaljual,
									'TipeTransaksi'=>'I',
									'MataUang'=>'IDR',
									'Kurs'=>1);
		$this->db->insert('mutasi_piutang', $data_mutasi_piutang);
		
		$this->db->trans_complete();
	}
	
	function getlistemail(){
		$sql ="SELECT email_address, email_name
                FROM
                    function_email
                WHERE
                    1
                    AND func_name = 'invoice_vci'
                ORDER BY
                    email_address ASC";
        return $this->getArrayResult($sql);
	}
	
	
    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>