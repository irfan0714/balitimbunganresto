<?php
class ShiftModel extends CI_Model {
	
    function __construct() {
        parent::__construct();
    }
	
	function ambilDept() 
	{  
		$sql1 = "SELECT `departemen_id` AS KdDepartemen, `departemen_name` AS NamaDepartemen FROM `hrd_departemen`";
				
        $query1 = $this->db->query($sql1);
        return $query1; 	
	}
	
	
	function getDataEmployee($kdDep)
	{			
	$qr1 = "SELECT employee_name, employee.employee_id AS KdEmployee,
  			`hrd_departemen`.`departemen_id` AS KdDepartemen
			FROM
  			employee 
  			JOIN employee_position 
    			ON employee_position.`employee_id` = employee.`employee_id` 
  			JOIN `hrd_departemen` 
    			ON `hrd_departemen`.`departemen_id` = employee_position.`departemen_id` 
			WHERE `hrd_departemen`.`departemen_id` = '$kdDep'" ;
					
		$qry		= $this->db->query($qr1);
        return	$qry->result_array();
	}
	
	function getEmployee($kdDep, $tgl)
	{	
		$qr1 = "SELECT 
					employee.employee_id AS KdEmployee, employee_name, hrd_departemen.departemen_id as KdDepartemen,
					(
						SELECT ShiftDate FROM shift
						WHERE KdDepartemen='$kdDep' AND `ShiftDate` = '$tgl' limit 1
					)AS ShiftDate
				FROM 
					employee
				JOIN 
					employee_position 
					ON 
						employee_position.`employee_id` = employee.`employee_id`
				JOIN 
					hrd_departemen 
					ON 
						hrd_departemen.`departemen_id` = employee_position.`departemen_id`
				WHERE 
					hrd_departemen.`departemen_id`='$kdDep'";
					
					
		$qry	= $this->db->query($qr1);
        return	$qry->result_array();
	}
	
	function shift()
	{
		$list_shift = array(
							array(
								'shiftId' => '1',
								'shiftName' => 'shift 1'
							),
							array(
								'shiftId' => '2',
								'shiftName' => 'shift 2'
							),
							array(
								'shiftId' => '3',
								'shiftName' => 'shift 3'
							),
							array(
								'shiftId' => '0',
								'shiftName' => 'OFF'
							)
					);
							
       return	$list_shift;
	}
	
	function getDataShift($tanggal,$KodeDep)
	{
		$qr1 = "
				SELECT 
					Shift, ShiftDate, hrd_departemen.departemen_id as KdDepartemen, KdEmployee
				FROM 
					shift
				JOIN 
					employee 
					ON 
						employee.employee_id = shift.`KdEmployee`
				JOIN 
					hrd_departemen 
					ON 
						hrd_departemen.`departemen_id` = shift.`KdDepartemen`
				WHERE 
					ShiftDate = '$tanggal' AND hrd_departemen.departemen_id = '$KodeDep'";
					
		$qry		= $this->db->query($qr1);
        return	$qry->result_array();
	}
	
	
	function getDataShiftCopy($tanggal,$KodeDep)
	{
		$qr1 = "
				SELECT 
					Shift, ShiftDate, hrd_departemen.departemen_id as KdDepartemen, KdEmployee
				FROM 
					shift
				JOIN 
					employee 
					ON 
						employee.employee_id = shift.`KdEmployee`
				JOIN 
					hrd_departemen 
					ON 
						hrd_departemen.`departemen_id` = shift.`KdDepartemen`
				WHERE 
					ShiftDate = ('$tanggal') < 1 
				AND 
					hrd_departemen.departemen_id = '$KodeDep'
				ORDER BY 
					ShiftDate DESC";
					
		$qry		= $this->db->query($qr1);
       return	$qry->result_array();
	  // echo $qr1;
	}
	
	
	function getDataCompletCopy($tgl, $kdDep)
	{
		$shift				= $this->getDataShiftCopy($tgl,$kdDep);		
		$employee			= $this->getDataEmployee($kdDep);
		
		$datacompletecopy	= array();
		
		foreach($employee as $emp)
		{
			$employee_nama 	= $emp['employee_name'];
			$idkaryawan		= $emp['KdEmployee'];
			$departemenID 	= $emp['KdDepartemen'];
			
			$sf				= '';
			
			foreach($shift as $shf)
			{
				$employee_id_dari_shift 		= $shf['KdEmployee'];
				$shift_dari_shift 				= $shf['Shift'];
				
				if($idkaryawan==$employee_id_dari_shift)
				{
					$sf	 			= $shift_dari_shift;
				}
					
			}
			
			$datacompletecopy[]			= array(
											'employee_name' => $employee_nama,
											'KdEmployee' 	=> $idkaryawan,
											'Shift'			=> $sf,
											'KdDepartemen'	=> $departemenID,
									);
		}
				
				
		return 	$datacompletecopy;
		
	}
	
	
	function getDatacomplete($tgl,$kdDep) 
	{
		
		$shift				= $this->getDataShift($tgl,$kdDep);		
		$employee			= $this->getDataEmployee($kdDep);
		
		$datacomplete	= array();
		
		foreach($employee as $emp)
		{
			$employee_nama 	= $emp['employee_name'];
			$idkaryawan		= $emp['KdEmployee'];
			$departemenID 	= $emp['KdDepartemen'];
			
			$sf				= '';
			
			foreach($shift as $shf)
			{
				$employee_id_dari_shift 		= $shf['KdEmployee'];
				$shift_dari_shift 				= $shf['Shift'];
				
				if($idkaryawan==$employee_id_dari_shift)
				{
					$sf	 			= $shift_dari_shift;
				}
					
			}
			
			$datacomplete[]			= array(
											'employee_name' => $employee_nama,
											'KdEmployee' 	=> $idkaryawan,
											'Shift'			=> $sf,
											'KdDepartemen'	=> $departemenID,
									);
		}
				
				
		return 	$datacomplete;
	}
	
	function deleted($kd, $tanggal)
	{
		$qr1 = "DELETE FROM shift WHERE KdDepartemen = '$kd' AND shiftDate='$tanggal'";
		
		$this->db->query($qr1);
		
		//echo $qr1;
	}
	
	
}