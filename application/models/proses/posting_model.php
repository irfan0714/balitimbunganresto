<?php
class Posting_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getUser()
	{
		$sql = "select * from user where Active='Y' and (userlevel=-1 or userlevel=1) and id<>'00'";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        $query->free_result();
        return $row;
	}
	function getinterface()
	{
		$sql = "SELECT * FROM interface limit 1";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getPeriodeGL()
	{
		$sql = "select PeriodeGL from aplikasi order by Tahun desc limit 0,1";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row[0]['PeriodeGL'];
	}
	
	function getListTahun()
	{
		$sql = "select distinct Tahun from counter order by Tahun";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	
	function getjurnal($tahun,$bulan)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,a.Debit,a.Kredit,a.TglDokumen,a.Keterangan AS KetDetail,
				a.KdSubDivisi as divisidetail,
		        b.NoPosting,b.Keterangan AS KetHeader 
				FROM trans_jurnal_detail a, trans_jurnal_header b
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun' 
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen ORDER BY a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getNewNo($bulan,$tahun)
	{
		$sql = "Update countergl set Count$bulan=Count$bulan+1 where Tahun='$tahun'";
		$this->db->query($sql);
		$sql = "SELECT Count$bulan as counter FROM countergl where Tahun='$tahun'";
		return $this->getRow($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function AkhirSaldoGL($Bulan,$Tahun){
	    $sql = "Select Departemen, KdRekening, KdSubDivisi, awal$Bulan+debet$Bulan-kredit$Bulan+adjdebet$Bulan-adjkredit$Bulan as Saldo from saldo_gl where tahun='$Tahun'";
        $query = $this->db->query($sql);
		return $query->result_array(); 		
	}
	function KosongSaldoAwalGL($Bulan,$Tahun){
	    $sql = "UPDATE saldo_gl set awal$Bulan=0 where tahun='$Tahun'";
        $this->db->query($sql);
	}
	function cekdata($namatable,$where){
         //$returnvalue = 0;
		 //echo $namatable;
         $this->db->select('count(*) as jumlah');
         $this->db->from($namatable);
         $this->db->where($where);
         $result = $this->db->get();
	         foreach ($result->result() as $row) {
	                $returnvalue = $row->jumlah;                                 
	         }                               
		return $returnvalue;
    }
	function addData($parameter,$data){
		$this->db->insert($parameter,$data); 
	}
	function posisirek($KdRekening){
	    $sql 	= "SELECT posisi FROM rekening where KdRekening='$KdRekening'";
        $query 	= $this->db->query($sql);
        $data = $query->row_array();
		if ($data) return $data['posisi'];
	}
	function UpdateSaldoAwal01($Nilai,$Tahun,$KdDepartemen,$KdRekening, $KdSubDivisi){
	    $sql = "UPDATE saldo_gl set awal01=$Nilai where tahun='$Tahun' and Departemen='$KdDepartemen' and KdRekening='$KdRekening' and KdSubDivisi='$KdSubDivisi'";
        $this->db->query($sql);
	}
	function AkhirSaldoRL($Bulan,$Tahun,$KdRekening){
	    $sql = "Select Departemen, KdRekening, KdSubDivisi, awal$Bulan+debet$Bulan-kredit$Bulan+adjdebet$Bulan-adjkredit$Bulan as Saldo from saldo_gl where tahun='$Tahun' and KdRekening='$KdRekening'";
        $query = $this->db->query($sql);
		return $query->result_array(); 		
	}
	function LabaDiTahan(){
	    $sql 	= "SELECT LabaDiTahan FROM interface";
        $query 	= $this->db->query($sql);
        $data = $query->row_array();
		if ($data) return $data['LabaDiTahan'];
	}
	
	function UpdateSaldoRL($Nilai,$Tahun,$KdDepartemen,$KdRekening, $KdSubDivisi){
	    $sql = "UPDATE saldo_gl set awal01=awal01+$Nilai where tahun='$Tahun' and Departemen='$KdDepartemen' and KdRekening='$KdRekening' and KdSubDivisi='$KdSubDivisi'";
        $this->db->query($sql);
	}
	function UpdateSaldoAwal2($Bulan,$Tahun,$BulanAwal){
	    $sql = "UPDATE saldo_gl a,rekening b set awal$Bulan=awal$BulanAwal+debet$BulanAwal-kredit$BulanAwal+adjdebet$BulanAwal-adjkredit$BulanAwal where tahun='$Tahun'
		        AND a.KdRekening=b.KdRekening";
//		         and b.Posisi in ('01','02')";
        $this->db->query($sql);
	}
	function KosongSaldoGL($Bulan,$Tahun){
	    $sql = "UPDATE saldo_gl set debet$Bulan=0, kredit$Bulan=0, adjdebet$Bulan=0, adjkredit$Bulan=0 where tahun='$Tahun'";
        $this->db->query($sql);
	}
	function getDataJur($Bulan,$Tahun){
        $sql = "SELECT kddepartemen as KdDepartemen,
		               kdrekening as KdRekening, KdSubDivisi, 
					   sum(if(jenisjurnal='U',debit,0)) as Debit,
					   sum(if(jenisjurnal='U',kredit,0)) as Kredit,
					   sum(if(jenisjurnal='A',debit,0)) as DebitAdj,
					   sum(if(jenisjurnal='A',kredit,0)) as KreditAdj
                       FROM jurnaldetail where bulan='$Bulan' and tahun='$Tahun' and (isnull(DeleteName) or isnull(DeleteDate)) group by kddepartemen,kdrekening, KdSubDivisi";
        $query = $this->db->query($sql); 
        return $query->result_array(); 		
        	
	}
	function UpdateSaldoGL($KdDepartemen,$Bulan,$Tahun,$KdRekening,$Debit,$Kredit,$DebitAdj,$KreditAdj, $KdSubDivisi){
		$db = 'debet'.$Bulan;
		$cr = 'kredit'.$Bulan;
		$adjdb = 'adjdebet'.$Bulan;
		$adjcr = 'adjkredit'.$Bulan;
	    $sql = "UPDATE saldo_gl set $db=$db+$Debit, $cr=$cr+$Kredit,$adjdb=$adjdb+$DebitAdj, $adjcr=$adjcr+$KreditAdj where tahun='$Tahun' and Departemen='$KdDepartemen' and KdRekening='$KdRekening' and KdSubDivisi='$KdSubDivisi'";
        $this->db->query($sql);
	}
	function LabaPeriode(){
	    $sql 	= "SELECT LabaPeriode FROM interface";
        $query 	= $this->db->query($sql);
        $data = $query->row_array();
		if ($data) return $data['LabaPeriode'];
	}
}
?>