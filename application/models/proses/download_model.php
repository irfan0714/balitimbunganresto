<?php
class tarikdata_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function getlistTerima(){
            $sql    = "
                    SELECT NoDokumen,TglDokumen,NoSuratJalan,Keterangan,
                    FlagKonfirm
                    FROM `trans_despatchheader` 
                    ORDER BY TglDokumen DESC;
                ";
            return $this->getArrayResult($sql);
    }
    function getMutasiCabang(){
            $db2 = $this->load->database('s3smg',true);
            $sql = "SELECT NoDokumen,TglDokumen,NoSuratJalan,Keterangan,
                       NamaUser,AddDate,EditDate
                    FROM mutasigudangheader WHERE YEAR(TglDokumen)='2014' AND 
                    MONTH(TglDokumen) BETWEEN '09' AND '10' 
                    AND KdGudangTujuan='06'AND KdMutasi='MG'
                    order by TglDokumen DESC
                ";
            $query  = $db2->query($sql);
			
        return $query->result_array();
}
 function getPLCabang($tgl){
            $db2 = $this->load->database('s3smg',true);
            $d1 = date('Y-m-d', strtotime(date($tgl) . '- 1 month'));
            $sql    = "
                    SELECT * FROM trans_pengeluaranlainheader WHERE KdTipe='66' AND TglDokumen >='$d1 '
                    order by TglDokumen DESC
                ";
            $query  = $db2->query($sql);
			
        return $query->result_array();
    }
    function CekStock($fieldmasuk,$fieldakhir,$pcode,$tahun)
    {
            $sql = "select $fieldmasuk,$fieldakhir from stock 
            where Tahun='$tahun'
            and KodeBarang='$pcode'";
        return $this->getRow($sql);
    }
    function getPLCabangdetail($tgl){
            $db2 = $this->load->database('s3smg',true);
            $d1 = date('Y-m-d', strtotime(date($tgl) . '- 1 month'));
            $sql    = "SELECT a.* FROM trans_pengeluaranlaindetail a,trans_pengeluaranlainheader h
                            WHERE a.`NoDokumen`=h.`NoDokumen` AND h.KdTipe='66' AND h.TglDokumen >='$d1'
                            ORDER BY h.NoDokumen DESC
                ";
            $query  = $db2->query($sql);
        return $query->result_array();
    }
    function getMutasiCabangdetail(){
            $db2 = $this->load->database('s3smg',true);
            $sql    = "SELECT 
                            mutasidetailgudang.NoDokumen,
                            mutasidetailgudang.PCode,
                            mutasidetailgudang.JmlQty_UnitKecil, 
                            mutasidetailgudang.HKecil,
                            mutasidetailgudang.AddDate,
                            mutasidetailgudang.EditDate
                    FROM 
                        mutasidetailgudang ,mutasigudangheader h
                    WHERE 
			mutasidetailgudang.`NoDokumen`=h.`NoDokumen`AND
                        YEAR(TglDokumen)='2014' AND 
                        MONTH(TglDokumen) BETWEEN '09' AND '10' 
                    AND KdGudangTujuan='06'AND KdMutasi='MG'
                    ORDER BY mutasidetailgudang.NoDokumen DESC
                ";
            $query  = $db2->query($sql);
			
        return $query->result_array();
    }
    function getDetail($id)
	{
		$sql = "SELECT
                        d.*,NamaLengkap
                        FROM (
                        SELECT
                          NoDokumen,
                          PCode,
                          Qty,
                          HKecil,
                          QtyTerima
                        FROM trans_despatchdetail
                        WHERE NoDokumen='$id')d
                        LEFT JOIN
                        masterbarang
                        ON masterbarang.PCode=d.PCode
                ";
		return $this->getArrayResult($sql);
	}
    function getHeader($id)
	{
		$sql = "SELECT NoDokumen,TglDokumen,NoSuratJalan,Keterangan,
                    FlagKonfirm
                    FROM `trans_despatchheader` 
                    WHERE trans_despatchheader.`NoDokumen` = '$id'
                    ORDER BY TglDokumen DESC
                ";
		return $this->getRow($sql);
}


    function do_simpan_terima_detail($no,$id,$gdg)
	{
		$sql = "
                    INSERT INTO trans_terima_detail
                    (Gudang,
                     NoDokumen,
                     PCode,
                     QtyTerima,
                     QtyHargaTerima,
                     ADDDATE,
                     AddUser,
                     EditDate,
                     FlagDelete)
                    SELECT 
                    '$gdg','$no',PCode,QtyTerima,HKecil,ADDDATE,'OTO',EditDate,'T'
                     FROM trans_despatchdetail WHERE NoDokumen='$id'";
		$qry = $this->db->query($sql);    
	}
        
    function do_simpan_mutasi_terima($no,$id,$gdg,$tgl)
	{
		$sql = " INSERT INTO mutasi(
                         NoKassa,Gudang,NoTransaksi,Jenis,KdTransaksi ,Tanggal,KodeBarang,Qty ,Nilai,Kasir )
                         SELECT '1','$gdg','$no','I','T','$tgl',PCode,QtyTerima,HKecil,'OTO' 
                         FROM trans_despatchdetail  WHERE NoDokumen='$id'";
 
		$qry = $this->db->query($sql);    
	}
	
    function do_simpan_terima_header($no,$id,$gdg,$tgl)
	{
		$sql = "INSERT INTO trans_terima_header(
                        Gudang,NoDokumen,TglDokumen,TglTerima,SumberOrder,NoOrder,Keterangan,KdSupplier,ADDDATE,AddUser,FlagDelete,NoPO)
			SELECT '$gdg','$no','$tgl',TglDokumen,'O' AS SumberOrder,'-' AS NoOrder,Keterangan,
                        'HO',ADDDATE,'OTO','T',NoDokumen AS NoPO FROM   trans_despatchheader 
                        WHERE NoDokumen='$id'";
		$qry = $this->db->query($sql);
	}
	function insertOracle($tabel,$data)
	{
		$db2 = $this->load->database('oracle',true);
		$db2->insert($tabel,$data);
	}
	function deleteOracle($tabel,$where)
	{
		$db2 = $this->load->database('oracle',true);
		$db2->delete($tabel,$where);
	}
	function editOracle($tabel,$data)
	{
		$db2 = $this->load->database('oracle',true);
		$db2->update($tabel,$data);
	}
	function editOracleWhere($tabel,$data,$where)
	{
		$db2 = $this->load->database('oracle',true);
		$db2->update($tabel,$data,$where);
	}
	function getProsesFlag()
	{
		$db2 = $this->load->database('oracle',true);
		$sql = "select FLAG from PROSES";
		$query  = $db2->query($sql);
		return $query->row();
	}
	function getUpdateMaster($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "
		select KODE_BRG as KODE,'Barang' as JENIS from V_BARANG_H
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		union
		select KODE_SATUAN as KODE,'Satuan' as JENIS from V_SATUAN
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		union
		select KODE_CUST as KODE, 'Customer' as JENIS from V_CUSTOMER
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		union
		select KODE_SUPP as KODE, 'Supplier' as JENIS from V_SUPPLIER
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		union
		select MERK_BARG as KODE,'Brand' as JENIS from V_MERK_BARANG
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		union
		select JNS_BARG as KODE,'Kategori' as JENIS from V_JNS_BARANG
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		union
		select KODE_KLAS_BRG as KODE,'Kelas' as JENIS from V_KLAS_BARANG
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'
		order by JENIS,KODE";
                $query  = $db2->query($sql);
		return $query->result_array();
	}
	function getUpdateTrans($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select NO_DOK as KODE,KODE_DOK as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from DO_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
        function getupdateRPB($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_DOK as KODE,KODE_DOK as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from RPB_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		//echo $sql;
		$query  = $db2->query($sql);
            return $query->result_array();
	}
        function getRRB($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_DOK as KODE,KODE_DOK as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from RENCANA_RBELI_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
            return $query->result_array();
	}
        function getMutasi($tanggal){/*Request Terima Lain */
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_DOK as KODE,KODE_REG as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from REQ_TRANSF_GUDANG_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
            return $query->result_array();
	}
        function getAdjustment($tanggal){/*Request Terima Lain */
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_DOK as KODE,KODE_REG as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from REQ_KOREKSI_STOCK_GUDG_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
            return $query->result_array();
	}
       
        function getRPL($tanggal){/*Request Terima Lain */
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_DOK as KODE,KODE_REG as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from REQ_TERIMA_LAIN_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
            return $query->result_array();
	}
        function getReqTerima_H($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,RTLH_ID,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK, 
                    KODE_REG,TIPE_TRANS,KODE_GUDG,KET_RTL,FLAG_STATUS,TERIMA_KODE_CUST 
                    from REQ_TERIMA_LAIN_H
                    where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
                    AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
                return $query->result_array();
	}
        function getReqKeluar_H($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,RKLH_ID,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK, 
                    KODE_REG,TIPE_TRANS,KODE_GUDG,KET_RKL,FLAG_STATUS,KELUAR_KODE_CUST 
                    from REQ_KELUAR_LAIN_H
                    where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
                    AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
                return $query->result_array();
	}
         function getMutasi_H($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,RTRAGH_ID,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK,FLAG_STATUS,RTRAGH_ID, 
                    KODE_REG,KODE_GUDG_ASAL,KODE_GUDG_TUJUAN,REFERENSI,KET_RTRAG
                    from REQ_TRANSF_GUDANG_H
                    where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
                    AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
                return $query->result_array();
	}
         function getAdjustment_H($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,RKSGH_ID,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK,FLAG_STATUS, 
                    KODE_REG,KODE_GUDG,KET_RKSG
                    from REQ_KOREKSI_STOCK_GUDG_H
                    where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
                    AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
                return $query->result_array();
	}
          function getRKL($tanggal){/*Request keluar Lain */
		$db2 = $this->load->database('oracle',true);
		$sql = "select NO_DOK as KODE,KODE_REG as JENIS,DECODE(STATUS_TRANSFER,'0','Baru','Edit') as STATUS from REQ_KELUAR_LAIN_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
            return $query->result_array();
	}
       
	function getSJ()
	{
		$sql = "select distinct NoDokumen as KODE,'SJ' as JENIS,if(FlagDelete='Y','Delete','Baru') as STATUS 
		from trans_kirim_detail where Status<>'G' and FlagUpload='T' and FlagKonfirmasi='Y' and (not ISNULL(No_DOK_DO) or NO_DOK_DO!='')";
		return $this->getArrayResult($sql);
	}
	function getRetur($perusahaan)
	{
		$sql = "select NoDokumen as KODE,'RP' as JENIS,if(h.EditDate='0000-00-00' or h.EditDate=h.AddDate,'Baru','Edit') as STATUS 
		from trans_terima_header h,contact c where SumberOrder='R' and c.KdContact=h.KdContact and KdPerusahaan='$perusahaan'  and FlagUpload='T' and FlagKonfirmasi='Y'";
		return $this->getArrayResult($sql);
	}
	function getMasterBarang($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select KODE_BRG,KET_BRG,SKTN_BRG,KODE_KLAS_BRG,
		MERK_BARG,JNS_BARG from V_BARANG_H
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function getMasterBarangD($kode){
		$db2 = $this->load->database('oracle',true);
		$sql	= "SELECT KODE_BRG,KODE_SATUAN,CONT_SATUAN,STOCK_SATUAN FROM V_BARANG_D
		where KODE_BRG='$kode'
		ORDER BY CONT_SATUAN";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function CekBrg($kode)
	{
		$sql = "select PCode from masterbarang where PCode ='$kode'";
		return $this->NumResult($sql);
	}
	function getBarcode()
	{
		$sql = "SELECT PrefixBarBarang,CounterBarBarang from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
        return $this->getRow($sql);
	}
	function getMasterSatuan($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select KODE_SATUAN,KET_SATUAN from V_SATUAN
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function CekSatuan($kode)
	{
		$sql = "select NamaSatuan from satuan where NamaSatuan ='$kode'";
		return $this->NumResult($sql);
	}
	function getMasterBrand($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select MERK_BARG,KET_MERK_BARG from V_MERK_BARANG
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
        function getNoTerima($tahun)
	{
		$sql = "SELECT NoTerima FROM setup_no where Tahun='$tahun'";
		return $this->getRow($sql);
	}
	
	function getGudang()
	{
		$sql = "SELECT KdGU FROM aplikasi";
		return $this->getRow($sql);
	}
	function CekBrand($kode)
	{
		$sql = "select KdBrand from brand where KdBrand ='$kode'";
		return $this->NumResult($sql);
	}
	function getMasterKategori($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select JNS_BARG,KET_JNS_BARG from V_JNS_BARANG
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function CekKategori($kode)
	{
		$sql = "select KdKategori from kategori where KdKategori ='$kode'";
		return $this->NumResult($sql);
	}
	function getMasterKelas($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select KODE_KLAS_BRG,KET_KLAS_BRG from V_KLAS_BARANG
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function CekKelas($kode)
	{
		$sql = "select KdKelas from kelasproduk where KdKelas ='$kode'";
		return $this->NumResult($sql);
	}
	function CekDownload($kode)
	{
		$sql = "SELECT * FROM trans_despatchheader WHERE NoDokumen ='$kode'";
		return $this->NumResult($sql);
    }
    function CekDownloadPL($kode)
	{
		$sql = "SELECT * FROM trans_despatchheader WHERE NoDokumen ='$kode'";
		return $this->NumResult($sql);
	}
	function CekDownloaddetail($kode,$pcode)
	{
		$sql = "SELECT * FROM trans_despatchdetail WHERE NoDokumen ='$kode' and PCode='$pcode'";
		return $this->NumResult($sql);
    }
    function CekDownloaddetailPL($kode,$pcode)
	{
		$sql = "SELECT * FROM trans_pengeluaranlaindetail WHERE NoDokumen ='$kode' and PCode='$pcode'";
		return $this->NumResult($sql);
	}
	function getMasterCustomer($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select KODE_CUST,NAMA_CUST,ALAMAT_CUST,TELEPON,KODE_POS from V_CUSTOMER
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function CekCustomerSupp($kode){
		$sql = "select KdContact from contact where KdContact ='$kode'";
		return $this->NumResult($sql);
	}
	function getMasterSupplier($tanggal)
	{
		$db2 = $this->load->database('oracle',true);
		$sql	= "select KODE_SUPP,NAMA_SUPP,ALAMAT_SUPP,KODE_POS,TELEPON from V_SUPPLIER
		where TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function getTransDOH($tanggal){
		$db2 = $this->load->database('oracle',true);
		$sql	= "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,DOH_ID,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK,SOH_ID,KODE_GUDG,
		KODE_CUST,ALAMAT_KIRIM,KET_DO,FLAG_STATUS,STATUS_TRANSFER,KODE_DOK_SO,NO_DOK_SO from DO_H
		where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
    function getNOPO($kd,$no){
                $db2 = $this->load->database('oracle',true);
		$sql	= "select distinct(NOMOR_PO) from RPB_D where NO_DOK='$no' and KODE_DOK='$kd'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
    function getTransRPB_H($tanggal){
                $db2 = $this->load->database('oracle',true);
		$sql	= "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,KODE_SUPP,RPBH_ID,TIPE,KET_RPB,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK,KODE_GUDG,FLAG_STATUS,STATUS_TRANSFER 
                from RPB_H where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
        function getReturBeli_H($tanggal){//rencana retur beli
                $db2 = $this->load->database('oracle',true);
		$sql	= "select NO_PERUSAHAAN,NO_DOK,KODE_DOK,RCRBELIH_ID,
                KET_RCRBELI,KODE_SUPP,TO_CHAR(TGL_DOK, 'YYYY-MM-DD') AS TGL_DOK,
                KODE_GUDG,FLAG_STATUS,STATUS_TRANSFER 
                from RENCANA_RBELI_H 
                where (STATUS_TRANSFER='0' OR STATUS_TRANSFER='1')
		AND (TO_CHAR(CREATED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal' or TO_CHAR(MODIFIED_ON, 'YYYY-MM-DD HH24:MI:SS') >= '$tanggal')";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function CekDOH($no_dok_do)
	{
		$sql = "select NoDokumen,FlagPengambilan from trans_order_taking_header where NoDokumen ='$no_dok_do'";
		return $this->getRow($sql);
	}
        function CekNoH($kode_dok,$no_dok_rpb)//cek no_dok order barang
	{
		$sql = "select NoDokumen,FlagPenerimaan from trans_order_barang_header where kode_dok_rpb='$kode_dok' and no_dok_rpb ='$no_dok_rpb'";
		return $this->getRow($sql);
	}
        function CekTBRG($no_OB)//cek Terima barang
	{
		$sql = "select NoDokumen,FlagKonfirmasi from trans_terima_header where NoOrder ='$no_OB' and FlagDelete='T'";
		return $this->getRow($sql);
	}
        function CekLainlainH($no_dok,$kode_dok,$tipe)//cek no_dok lain lain
	{
		$sql = "select NoDokumen,FlagPenerimaan from req_lainlain_header where kode_dok_req='$kode_dok' and no_dok_req ='$no_dok' and type = '$tipe'";
                return $this->getRow($sql);
	}
        function CekMutasiH($no_dok,$kode_dok)//cek Mutasi
	{
		$sql = "select * from req_mutasi_header where kode_dok_req='$kode_dok' and no_dok_req ='$no_dok'";
                return $this->getRow($sql);
	}
        function CekAdjustmentH($no_dok,$kode_dok)//cek Mutasi
	{
		$sql = "select * from req_adjustment_header where KdDokReq='$kode_dok' and NoDokReq ='$no_dok'";
                return $this->getRow($sql);
	}
	function getTransDOD($kode)
	{
		$db2 = $this->load->database('oracle',true);
		$sql	= "select NO_DOK,DOH_ID,SOD_ID,DOD_ID,KODE_BRG,KODE_SATUAN,QTY,KODE_DOK from DO_D
		where NO_DOK='$kode'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
        function getTransRPB_D($kodedok,$kode)
	{
		$db2 = $this->load->database('oracle',true);
		$sql	= "select KODE_DOK,NO_DOK,KODE_BRG,KODE_SATUAN,SUM(QTY) AS QTY from RPB_D where KODE_DOK='$kodedok' and NO_DOK='$kode' GROUP BY KODE_DOK,NO_DOK,KODE_BRG,KODE_SATUAN";
		//echo $sql;
		$query  = $db2->query($sql);
		return $query->result_array();
	}
        function getRetur_D($kodedok,$kode)
	{
            $db2 = $this->load->database('oracle',true);
            $sql = "select KODE_DOK,NO_DOK,KODE_BRG,RCRBELIH_ID,RCRBELID_ID,KODE_SATUAN,
                    Round(QTY) as QTY
                from RENCANA_RBELI_D where KODE_DOK='$kodedok' and NO_DOK='$kode'";
                //GROUP BY KODE_DOK,NO_DOK,KODE_BRG,KODE_SATUAN,RCRBELIH_ID";
            $query  = $db2->query($sql);
            return $query->result_array();
	}
	function getTransRPB_D2($kodedok,$kode)
	{
		$db2 = $this->load->database('oracle',true);
		$sql	= "select * from RPB_D where NO_DOK='$kode' and KODE_DOK='$kodedok'";
		$query  = $db2->query($sql);
		return $query->result_array();
	}
	function DetailBarang($pcode)
	{
		$sql = "select KonversiBesarKecil,KonversiTengahKecil,KonversiJualKecil,KdSatuanJual,KdSatuanBesar,KdSatuanTengah,KdSatuanKecil
		from masterbarang where PCode='$pcode'";
		return $this->getRow($sql);
	}
	function getCounter($id)
	{
		$sql = "SELECT Counter FROM trans_order_taking_detail Where NoDokumen='$id' order by Counter desc limit 0,1";
		return $this->getRow($sql);
	}
	function CekDOD($no,$pcode)
	{
		$sql = "select Counter,FlagDelete from trans_order_taking_detail where NoDokumen='$no' and PCode='$pcode'";
		return $this->getRow($sql);
	}
	function getDOD_wms($kode)
	{
		$sql = "select PCode, Counter from trans_order_taking_detail where NoDokumen='$kode'";
		return $this->getArrayResult($sql);
	}
	function getHeaderSJ()
	{
		$sql = "SELECT k.*,KdPerusahaan FROM(
		SELECT DISTINCT NoSuratJalan AS NoDokumen,DATE_FORMAT(h.TglDokumen,'%d-%b-%y') AS TglDokumen,h.Keterangan,d.NoOrder,d.FlagDelete,
		KODE_DOK_DO,NO_DOK_DO,SOH_ID,DOH_ID,KODE_DOK_SO,NO_DOK_SO
		FROM trans_kirim_detail d,trans_kirim_header h,trans_ambil_header ha
		WHERE h.Status<>'G' and FlagUpload='T' AND d.FlagKonfirmasi='Y' AND h.FlagKonfirmasi='Y' AND (NOT ISNULL(No_DOK_DO) OR NO_DOK_DO!='')
		AND ha.NoDokumen=NoPengambilan
		AND h.NoDokumen=d.NoDokumen
		) k
		LEFT JOIN
		(
		SELECT KdPerusahaan,NoDokumen FROM trans_order_taking_header
		) o
		ON o.NoDokumen=k.NoOrder";
		return $this->getArrayResult($sql);
	}
        
     function getLPB()
	{
	$sql = "SELECT k.*,KdPerusahaan FROM(
SELECT DISTINCT h.NoDokumen,DATE_FORMAT(h.TglDokumen,'%d-%b-%y') AS TglDokumen,h.Keterangan,h.NoOrder,d.FlagDelete,
KODE_DOK_RPB,NO_DOK_RPB,RPBH_ID,TIPE
FROM trans_terima_detail d,trans_terima_header h
WHERE FlagUpload='T' AND d.FlagKonfirmasi='Y' AND SumberOrder='O' AND
h.FlagKonfirmasi='Y' AND (NOT ISNULL(No_DOK_RPB) AND NO_DOK_RPB!='')
AND h.NoDokumen=d.NoDokumen
) k
LEFT JOIN
(
SELECT KdPerusahaan,NoDokumen FROM trans_order_taking_header
) o
ON o.NoDokumen=k.NoOrder";
                return $this->getArrayResult($sql);
	}
     function getHeaderLPB()
	{
	$sql = "SELECT k.* FROM(
SELECT DISTINCT h.NoDokumen,DATE_FORMAT(h.TglDokumen,'%d-%b-%y') AS TglDokumen,h.Keterangan,h.KdPerusahaan,h.NoOrder,d.FlagDelete,
KODE_DOK_RPB,NO_DOK_RPB,RPBH_ID,TIPE
FROM trans_terima_detail d,trans_terima_header h
WHERE FlagUpload='T' AND d.FlagKonfirmasi='Y' AND SumberOrder='O' AND 
h.FlagKonfirmasi='Y' AND (NOT ISNULL(NO_DOK_RPB) AND NO_DOK_RPB!='')
AND h.NoDokumen=d.NoDokumen
) k
LEFT JOIN
(
SELECT KdPerusahaan,NoDokumen FROM trans_order_taking_header
) o
ON o.NoDokumen=k.NoOrder";
                return $this->getArrayResult($sql);
	}
        function getHeaderRB()// retur beli
        {
	$sql = "SELECT k.*,KdPerusahaan FROM( 
            SELECT DISTINCT NoSuratJalan AS NoDokumen,
            DATE_FORMAT(h.TglDokumen,'%d-%b-%y') 
            AS TglDokumen,h.Keterangan,d.NoOrder,d.FlagDelete, DOH_ID AS RCRBELIH_ID,KODE_DOK_DO AS KODE_DOK_RCRBELI,NO_DOK_DO 
            AS NO_DOK_RCRBELI,NO_DOK_DO,SOH_ID,KODE_DOK_SO,NO_DOK_SO 
            FROM trans_kirim_detail d,trans_kirim_header h,trans_ambil_header ha 
            WHERE h.Status<>'G' AND FlagUpload='T' AND d.FlagKonfirmasi='Y' 
            AND h.FlagKonfirmasi='Y' AND (NOT ISNULL(No_DOK_DO) OR NO_DOK_DO!='') 
            AND ha.NoDokumen=NoPengambilan AND h.NoDokumen=d.NoDokumen ) k 
            LEFT JOIN ( SELECT KdPerusahaan,NoDokumen,DOH_ID,KODE_DOK_DO,NO_DOK_DO 
            FROM trans_order_taking_header 
            WHERE jenis='RB') o ON o.NoDokumen=k.NoOrder";
            return $this->getArrayResult($sql);
	}
        function getLainlain()
        {
            $sql="SELECT * FROM trans_lainlain_header WHERE FlagUpload='T'";
            return $this->getArrayResult($sql);
        }
        function getHeaderLainlain()
        {
            $sql="SELECT NoDokumen,KdPerusahaan,IdH_Req,NoDokReq,KdDokReq,Tipe,
                DATE_FORMAT(TglDokumen,'%d-%b-%y') AS TglDokumen,Keterangan,FlagDelete  
                FROM trans_lainlain_header WHERE FlagUpload='T'";
            return $this->getArrayResult($sql);
        }
        function getgudangH()
        {
            $sql="SELECT NoDokumen,KdPerusahaan,IdH_Req,NoDokReq,KdDokReq,
                DATE_FORMAT(TglDokumen,'%d-%b-%y') AS TglDokumen,Keterangan,FlagDelete  
                FROM trans_gudang_header WHERE FlagUpload='T'";
            return $this->getArrayResult($sql);
        }
        function getkoreksiH()
        {
            $sql="SELECT NoDokumen,KdPerusahaan,IdH_Req,NoDokReq,KdDokReq,
                DATE_FORMAT(TglDokumen,'%d-%b-%y') AS TglDokumen,Keterangan,FlagDelete   
                FROM trans_koreksi_header WHERE FlagUpload='T'";
            return $this->getArrayResult($sql);
        }
        function getLainlainD($no)
        {
            $sql="SELECT DISTINCT(PCode)AS PCode,IdD_Req,QtyInput,KdSatuan 
                FROM trans_lainlain_detail WHERE NoDokumen='$no'";
            return $this->getArrayResult($sql);
        }
        function getkoreksiD($no)
        {
            $sql="SELECT DISTINCT(PCode)AS PCode,IdD_Req,QtyInput,QtyPcs,SatuanKomp,Satuankoreksi_stock  
                FROM trans_koreksi_detail WHERE NoDokumen='$no'";
            return $this->getArrayResult($sql);
        }
        function getgudangD($no)
        {
            $sql="SELECT DISTINCT(PCode)AS PCode,IdD_Req,QtyInput,QtyDisplay,KdSatuanJual,KdSatuan 
                FROM trans_gudang_detail WHERE NoDokumen='$no'";
            return $this->getArrayResult($sql);
        }
        function getUploadMutasi(){
            $sql="SELECT * FROM trans_gudang_header WHERE FlagUpload='T'";
            return $this->getArrayResult($sql);
        }
        function getUploadAdjustment(){
            $sql="SELECT * FROM trans_koreksi_header WHERE FlagUpload='T'";
            return $this->getArrayResult($sql);
        }
	function getSJDbackup($kode)
	{
		$sql = "SELECT k.*,KdPerusahaan,KonversiBesarKecil,KonversiTengahKecil,KdSatuanBesar,KdSatuanKecil,KdSatuanTengah,SatuanOrder FROM(
SELECT DISTINCT NoSuratJalan AS NoDokumen,DATE_FORMAT(h.TglDokumen,'%d-%b-%y') AS TglDokumen,h.Keterangan,d.NoOrder,d.FlagDelete,d.DOD_ID,
od.Satuan AS SatuanOrder,PCodeBarang,d.Satuan, SUM(d.QtyPcsKonfirm) AS QtyPcs,d.KonversiBesarKecil,d.KonversiTengahKecil 
FROM trans_kirim_detail d,trans_kirim_header h,trans_order_taking_detail od,trans_ambil_header ha
WHERE NoSuratJalan='$kode' AND (NOT ISNULL(d.No_DOK_DO) OR d.NO_DOK_DO!='')
AND od.NoDokumen=d.NoOrder AND od.PCodeBarang=d.PCode AND ha.NoDokumen=d.NoPengambilan
AND d.FlagKonfirmasi='Y' AND h.Status<>'G' AND d.FlagDelete='T'
AND h.NoDokumen=d.NoDokumen group by NoSuratJalan,PCodeBarang
) k
LEFT JOIN
(
SELECT KdPerusahaan,NoDokumen FROM trans_order_taking_header
) o
ON o.NoDokumen=k.NoOrder
LEFT JOIN
(
SELECT PCode,KdSatuanBesar,KdSatuanKecil,KdSatuanTengah
FROM masterbarang
) b
ON b.PCode=k.PCodeBarang";
		return $this->getArrayResult($sql);
	}
	function getSJD($kode)
	{
		$sql = "SELECT k.*,KdPerusahaan,KonversiBesarKecil,KonversiTengahKecil,KdSatuanBesar,KdSatuanKecil,KdSatuanTengah,SatuanOrder FROM(
select d1.*,sum(QtyPcs1) as QtyPcs from (
SELECT DISTINCT NoSuratJalan AS NoDokumen,DATE_FORMAT(h.TglDokumen,'%d-%b-%y') 
AS TglDokumen,h.Keterangan,d.NoOrder,d.FlagDelete,d.DOD_ID,
od.Satuan AS SatuanOrder,PCodeBarang,d.Satuan, d.QtyPcsKonfirm AS QtyPcs1,
d.KonversiBesarKecil,d.KonversiTengahKecil,d.Counter,d.CounterPengambilan
FROM trans_kirim_detail d,trans_kirim_header h,trans_order_taking_detail od,
trans_ambil_header ha
WHERE NoSuratJalan='$kode' AND (NOT ISNULL(d.No_DOK_DO) OR d.NO_DOK_DO!='')
AND od.NoDokumen=d.NoOrder AND od.PCodeBarang=d.PCode AND 
ha.NoDokumen=d.NoPengambilan
AND d.FlagKonfirmasi='Y' AND h.Status<>'G' AND d.FlagDelete='T'
AND h.NoDokumen=d.NoDokumen)d1
group by NoDokumen,PCodeBarang
) k
LEFT JOIN
(
SELECT KdPerusahaan,NoDokumen FROM trans_order_taking_header
) o
ON o.NoDokumen=k.NoOrder
LEFT JOIN
(
SELECT PCode,KdSatuanBesar,KdSatuanKecil,KdSatuanTengah
FROM masterbarang
) b
ON b.PCode=k.PCodeBarang";
		return $this->getArrayResult($sql);
	}
	function getLPBD($kodedok,$kode,$nodok)
	{
		$sql = "SELECT * from Trans_RPB_D where kode_dok='$kodedok' and noterima='$kode' AND NO_DOK='$nodok' order by kode_brg,no_invoice";  //terbalik antara noorder dan noterima
		return $this->getArrayResult($sql);
	}
	function getSisaLPBD($kode,$barang)
	{
		$sql = "SELECT QtyKonfirm from trans_terima_detail where nodokumen='$kode' and PCode='$barang'";
		return $this->getRow($sql);;
	}
    function getReturBeliD($kode)
	{
            $sql = "SELECT k.*,KdPerusahaan,KonversiBesarKecil,KonversiTengahKecil,KdSatuanBesar,KdSatuanKecil,KdSatuanTengah,SatuanOrder FROM(
                select d1.*,sum(QtyPcs1) as QtyPcs from (
                SELECT DISTINCT NoSuratJalan AS NoDokumen,DATE_FORMAT(h.TglDokumen,'%d-%b-%y') 
                AS TglDokumen,h.Keterangan,d.NoOrder,d.FlagDelete,d.DOD_ID,
                od.Satuan AS SatuanOrder,PCodeBarang,d.Satuan, d.QtyPcsKonfirm AS QtyPcs1,
                d.KonversiBesarKecil,d.KonversiTengahKecil,d.Counter
                FROM trans_kirim_detail d,trans_kirim_header h,trans_order_taking_detail od,
                trans_ambil_header ha
                WHERE NoSuratJalan='$kode' AND (NOT ISNULL(d.No_DOK_DO) OR d.NO_DOK_DO!='')
                AND od.NoDokumen=d.NoOrder AND od.PCodeBarang=d.PCode AND 
                ha.NoDokumen=d.NoPengambilan
                AND d.FlagKonfirmasi='Y' AND h.Status<>'G' AND d.FlagDelete='T'
                AND h.NoDokumen=d.NoDokumen)d1
                group by NoDokumen,PCodeBarang
                ) k
                LEFT JOIN
                (
                SELECT KdPerusahaan,NoDokumen FROM trans_order_taking_header
                ) o
                ON o.NoDokumen=k.NoOrder
                LEFT JOIN
                (
                SELECT PCode,KdSatuanBesar,KdSatuanKecil,KdSatuanTengah
                FROM masterbarang
                ) b
                ON b.PCode=k.PCodeBarang";
            return $this->getArrayResult($sql);
	}
	function getNoKirim($nosj)
	{
		$sql = "SELECT DISTINCT d.NoDokumen,d.NoOrder FROM trans_kirim_detail d,trans_ambil_header a 
WHERE d.NoPengambilan = a.NoDokumen AND NoSuratJalan='$nosj' and d.FlagKonfirmasi='Y'";
		return $this->getRow($sql);
	}
	function getHeaderRetur($perusahaan)
	{
		$sql = "select KdPerusahaan,NoDokumen,DATE_FORMAT(TglDokumen,'%d-%b-%y') AS TglDokumen,h.KdContact,h.KdLokasi,FlagDelete,if(h.EditDate='0000-00-00' or h.EditDate=h.AddDate,'Baru','Edit') as STATUS 
		from trans_terima_header h,contact c where SumberOrder='R' and c.KdContact=h.KdContact and KdPerusahaan='$perusahaan'
		and FlagUpload='T' and FlagKonfirmasi='Y'";
		return $this->getArrayResult($sql);
	}
	function getReturD($nodok)
	{
		$sql = "select PCodeBarang,SatuanKonf,QtyKonfirm 
		from trans_terima_detail where NoDokumen='$nodok' and FlagKonfirmasi='Y' and FlagDelete='T'";
		return $this->getArrayResult($sql);
	}
        function getAdjustment_D($kddok,$nodok)// detail terima lain lain
	{
		$db2    = $this->load->database('oracle',true);
		$sql	= "select * from REQ_KOREKSI_STOCK_GUDG_D where NO_DOK = '$nodok' and KODE_DOK = '$kddok' ";
                $query  = $db2->query($sql);
		return $query->result_array();
	}
        function getMutasi_D($kddok,$nodok)// detail terima lain lain
	{
		$db2    = $this->load->database('oracle',true);
		$sql	= "select * from REQ_TRANSF_GUDANG_D where NO_DOK ='$nodok' and KODE_DOK = '$kddok' ";
                $query  = $db2->query($sql);
		return $query->result_array();
	}
        function getlainlainT_D($kddok,$nodok)// detail terima lain lain
	{
		$db2    = $this->load->database('oracle',true);
		$sql	= "select * from REQ_TERIMA_LAIN_D where NO_DOK = '$nodok' and KODE_DOK = '$kddok' ";
                $query  = $db2->query($sql);
		return $query->result_array();
	}
         function getlainlainK_D($kddok,$nodok)// detail terima lain lain
	{
		$db2    = $this->load->database('oracle',true);
		$sql	= "select * from REQ_KELUAR_LAIN_D where NO_DOK = '$nodok' and KODE_DOK = '$kddok' ";
                $query  = $db2->query($sql);
		return $query->result_array();
	}
        function cekDetaillain2($pcode,$id,$type)
	{
		$sql = "select * from req_lainlain_detail where NoDokumen='$id' and PCode='$pcode' and Type = '$type'";
		return $this->getRow($sql);
	}
        function cekDetailMutasi($pcode,$id)
	{
		$sql = "select * from req_mutasi_detail where NoDokumen='$id' and PCode='$pcode'";
		return $this->getRow($sql);
	}
        function getNewNo($perusahaan,$tahun,$field)
	{
		$sql = "SELECT $field FROM setup_per_perusahaan where KdPerusahaan='$perusahaan' and Tahun='$tahun'";
		return $this->getRow($sql);
        }
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->num_rows();
        $qry->free_result();
        return $row;
	}
}
?>