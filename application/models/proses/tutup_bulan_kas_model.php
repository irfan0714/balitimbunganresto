<?php

class tutup_bulan_kas_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getPeriodeKasBank(){
		$sql = "select DATE_FORMAT(PeriodeKasBank,'%d-%m-%Y') as PeriodeKasBank from aplikasi order by Tahun desc limit 0,1";
		$rec = $this->getArrayResult($sql);
		return $rec[0]['PeriodeKasBank'];
	}
	
	function getBulanDepan(){
		$prdsekrang = $this->getPeriodeKasBank();
		list($tgl, $bln, $thn) = explode('-',$prdsekrang);
		$bln = $bln*1 + 1;
		if($bln>12){
			$bln ='1';
			$thn +=1;
		}
		if($bln<10){
			$bln = '0'.$bln;
		}
		$tgldepan = $thn.'-'.$bln.'-'.'01';
		return date("t-m-Y", strtotime($tgldepan));
	}
	
	function updateperiode($tgl){
		$sql = "update aplikasi set PeriodeKasBank='$tgl'";
		$this->db->query($sql);
	}
    
    function updatesaldo($prdsekarang, $prdberikut){
    	list($thnsekarang, $blnsekarang, $tglsekarang) = explode('-',$prdsekarang);
    	list($thnberikut, $blnberikut, $tglberikut) = explode('-',$prdberikut);
    	
    	$blnsekarang=$blnsekarang*1;
    	
    	$sql = "Delete from saldo_bank where tahun='$thnsekarang' and bulan ='$blnsekarang' ";
    	$this->db->query($sql);
    	$this->UpdateSaldoAwal($thnsekarang,$blnsekarang);
    	$this->UpdateMutasi($thnsekarang,$blnsekarang);
	}
	
	function UpdateSaldoAwal($tahun, $bulan){
		$bulanlalu = $bulan;
		$tahunlalu = $tahun;
    	if($bulan==1){
			$bulanlalu=12;
			$tahunlalu -= 1;
		}else{
			$bulanlalu -= 1;
		}
		$sql = "Insert into saldo_bank(Tahun, Bulan, KdKasBank, Awal, Masuk, Keluar, Akhir) 
				SELECT '$tahun', '$bulan', KdKasBank, Akhir,0,0,Akhir FROM saldo_bank WHERE tahun='$tahunlalu' AND bulan='$bulanlalu'";	
		$this->db->query($sql);
	}
	
	function UpdateMutasi($tahun, $bulan){
		$sql = "SELECT KdKasBank, coalesce(sum(Debet),0) as Masuk, coalesce(sum(Kredit),0) as Keluar FROM (
				SELECT h.KdKasBank, 0 AS Debet, sum(h.`JumlahPayment`) AS Kredit FROM trans_payment_header h 
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`Status`<>'B' group by h.KdKasBank
				UNION ALL
				SELECT h.KdKasBank, sum(h.`JumlahReceipt`) AS Debet, 0 AS Kredit FROM trans_receipt_header h 
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`Status`<>'B' group by h.KdKasBank
				) t group by KdKasBank";
		
		$rec =  $this->getArrayResult($sql);
		foreach($rec as $mutasi){
			$kdkasbank = $mutasi['KdKasBank'];
			$masuk = $mutasi['Masuk'];
			$keluar = $mutasi['Keluar'];
			
			$saldo_bank_rec = "Select * from saldo_bank where tahun='$tahun' and Bulan='$bulan' and KdKasBank='$kdkasbank'";
			$hasil =  $this->getArrayResult($saldo_bank_rec);	
			if(!empty($hasil)){
				$sqlupdate = "Update saldo_bank set Masuk=Masuk+$masuk, Keluar=Keluar+$keluar, Akhir=Akhir+$masuk-$keluar 
								where tahun='$tahun' and Bulan='$bulan' and KdKasBank='$kdkasbank'";
			}else{
				$sqlupdate = "Insert into saldo_bank(Tahun, Bulan, KdKasBank, Awal, Masuk, Keluar, Akhir) 
							  values('$tahun', '$bulan', '$kdkasbank', 0,$masuk,$keluar,$masuk-$keluar)";
			}
			
			$this->db->query($sqlupdate);
			
		}
		
	}
	
    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>