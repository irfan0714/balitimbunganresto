<?php
class tarik_data_outlet_model extends CI_Model {

    function __construct(){
        parent::__construct();
		$db2 = $this->load->database('sarinah', TRUE);
    }

	function GetOutlet($kdstore){
		if($kdstore ==''){
			$where = '';
		}else{
			$where = " where ID='".$kdstore."'";
		}
		$sql = "select NamaOutlet,Kassa, DbName, SkipImport from outlet " . $where;
		$qry = $this->db->query($sql);
    	return $qry->result_array();
		
	}
    function TransaksiHeader($kassa,$conn2){
	    $db2 = $this->load->database('sarinah', TRUE);
        $sql    = "Select * from transaksi_header where syncflg<>1";
        $qry = $db2->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$nokassa = $row['NoKassa'];
			$nostruk = $row['NoStruk'];
			
			$data = array('NoKassa' => $row['NoKassa'],
						  'Gudang' => $row['Gudang'],
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Waktu' => $row['Waktu'],
						  'Kasir' => $row['Kasir'],
						  'KdStore' => $row['KdStore'],
						  'TotalItem' => $row['TotalItem'],
						  'TotalNilaiPem' => $row['TotalItem'],
						  'TotalNilai' => $row['TotalNilai'],
						  'TotalBayar' => $row['TotalBayar'],
						  'Kembali' => $row['Kembali'],
						  'Point' => $row['Point'],
						  'Tunai' => $row['Tunai'],
						  'KKredit' => $row['KKredit'],
						  'KDebit' => $row['KDebit'],
						  'GoPay' => $row['GoPay'],
						  'Voucher' => $row['Voucher'],
						  'VoucherTravel' => $row['VoucherTravel'],
						  'Discount' => $row['Discount'],
						  'BankDebet' => $row['BankDebet'],
						  'EDCBankDebet' => $row['EDCBankDebet'],
						  'BankKredit' => $row['BankKredit'],
						  'EDCBankKredit' => $row['EDCBankKredit'],
						  'Status' => $row['Status'],
						  'KdCustomer' => $row['KdCustomer'],
						  'NamaCustomer' => $row['NamaCustomer'],
						  'Gender' => $row['Gender'],
						  'TglLahir' => $row['TglLahir'],
						  'Keterangan' => $row['Keterangan'],
						  'EditDate' => $row['EditDate'],
						  'EditUser' => $row['EditUser'],
						  'KdAgent' => $row['KdAgent'],
						  'KdGsa' => $row['KdGsa'],
						  'Ttl_Charge'=> $row['Ttl_Charge'],
						  'DPP' =>  $row['DPP'],
						  'TAX' => $row['TAX'],
						  'KdMeja' => $row['KdMeja'],
						  'userdisc' => $row['userdisc'],
						  'KdMember' => $row['KdMember'],
						  'NoCard' => $row['NoCard'],
						  'NamaCard' => $row['NamaCard'],
						  'nilaidisc' => $row['nilaidisc'],
						  'statuskomisi' => $row['statuskomisi'],
						  'Valas' => $row['Valas'],
						  'Kurs' => $row['Kurs'],
						  'Valuta' => $row['Valuta'],
						  'TotalGuest'=> $row['TotalGuest']
						);
			
			try {
				$this->db->delete('transaksi_header', array('NoKassa' => $nokassa,'NoStruk'=>$nostruk));
				$this->db->insert('transaksi_header', $data);
				
				if ($this->db->affected_rows() != 1)
				{
					throw new Exception("Gagal insert header kassa " . $row['NoKassa'] . ' struk ' . $row['NoStruk']);
				}else{
					$sql = "UPDATE transaksi_header c  
								set syncflg = 1
								where c.NoKassa='$nokassa' AND c.NoStruk='$nostruk'";
					$qry = $db2->query($sql);
				}
			} catch (Exception $e) {
				echo $e->getMessage();
			}		
		}
		$db2->close();
    }
    
	function TransaksiHeaderPerTgl($date,$kassa,$conn){
    	$sqldb = "Delete from transaksi_header_sunset where tanggal= '$date' and NoKassa in(".$kassa.")";
    	$this->db->query($sqldb);
    	
    	//select dari source
        $sql    = "Select * from transaksi_header where tanggal = '$date' ";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$data = array('NoKassa' => $row['NoKassa'],
						  'Gudang' => $row['Gudang'],
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Waktu' => $row['Waktu'],
						  'Kasir' => $row['Kasir'],
						  'KdStore' => $row['KdStore'],
						  'TotalItem' => $row['TotalItem'],
						  'TotalNilai' => $row['TotalNilai'],
						  'TotalBayar' => $row['TotalBayar'],
						  'Kembali' => $row['Kembali'],
						  'Tunai' => $row['Tunai'],
						  'KKredit' => $row['KKredit'],
						  'KDebit' => $row['KDebit'],
						  'Voucher' => $row['Voucher'],
						  'Point' => $row['Point'],
						  'GoPay' => $row['GoPay'],
						  'VoucherTravel' => $row['VoucherTravel'],
						  'Discount' => $row['Discount'],
						  'Status' => $row['Status'],
						  'KdCustomer' => $row['KdCustomer'],
						  'NamaCustomer' => $row['NamaCustomer'],
						  'Gender' => $row['Gender'],
						  'TglLahir' => $row['TglLahir'],
						  'Keterangan' => $row['Keterangan'],
						  'EditDate' => $row['EditDate'],
						  'EditUser' => $row['EditUser'],
						  'KdAgent' => $row['KdAgent'],
						  'KdGsa' => $row['KdGsa'],
						  'Ttl_Charge'=> $row['Ttl_Charge'],
						  'DPP' =>  $row['DPP'],
						  'TAX' => $row['TAX'],
						  'KdMeja' => $row['KdMeja'],
						  'userdisc' => $row['userdisc'],
						  'nilaidisc' => $row['nilaidisc'],
						  'statuskomisi' => $row['statuskomisi'],
						  'Valas' => $row['Valas'],
						  'Kurs' => $row['Kurs'],
						  'Valuta' => $row['Valuta'],
						  'TotalGuest'=> $row['TotalGuest'],
						  'KdMember'=> $row['KdMember'],
						  'NamaCard'=> $row['NamaCard'],
						  'NoCard'=> $row['NoCard']
						);
						
			//insert ke natura
			$this->db->insert('transaksi_header_sunset', $data);
		}
    }
	
    function TransaksiDetail($kassa,$conn){
    	//select dari source
		$db2 = $this->load->database('sarinah', TRUE);
		$sql1 = "delete from tempSync";
		$qry = $db2->query($sql1);
		
        $sql2 = "insert into tempSync Select NoKassa, NoStruk from transaksi_detail where syncflg<>1 group by NoKassa, NoStruk";
        $qry = $db2->query($sql2);
		
		$sql3 = "select * from tempSync";
		$qry = $db2->query($sql3);
    	$result = $qry->result_array();
    	$qry->free_result();
		foreach($result as $row){
			$nokassa = $row['NoKassa'];
			$nostruk = $row['NoStruk'];
			$this->db->delete('transaksi_detail', array('NoKassa' => $nokassa,'NoStruk'=>$nostruk));
		}
		
		$sql4 = "select d.* from transaksi_detail d inner join tempSync t on d.NoKassa=t.NoKassa and d.NoStruk=t.NoStruk ";
		$qry = $db2->query($sql4);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$nokassa = $row['NoKassa'];
			$id= $row['Id'];
			$data = array(
						  'NoKassa' => $row['NoKassa'],
						  'Gudang' => $row['Gudang'], 
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Waktu' => $row['Waktu'],
						  'Kasir' => $row['Kasir'],
						  'KdStore' => $row['KdStore'], 
						  'PCode' => $row['PCode'],
						  'Qty' => $row['Qty'],				  
						  'Berat' => $row['Berat'],
						  'Harga' => $row['Harga'],
						  'Ketentuan1' => $row['Ketentuan1'],
						  'Disc1' => $row['Disc1'],
						  'Jenis1' => $row['Jenis1'],
						  'Ketentuan2' => $row['Ketentuan2'],
						  'Disc2'=> $row['Disc2'],
						  'Jenis2' => $row['Jenis2'],
						  'Ketentuan3' => $row['Ketentuan3'],
						  'Disc3' => $row['Disc3'],
						  'Jenis3' => $row['Jenis3'],
						  'Ketentuan4' => $row['Ketentuan4'],
						  'Disc4' => $row['Disc4'],
						  'Jenis4' => $row['Jenis4'],
						  'Netto' => $row['Netto'],
						  'Hpp' => $row['Hpp'],
						  'Status' => $row['Status'],
						  'Keterangan' => $row['Keterangan'],
						  'Service_charge' => $row['Service_charge'],
						  'Komisi' => $row['Komisi'],
						  'PPN' => $row['PPN'],
						  'Printer' => $row['Printer'],
						  'KdMeja' => $row['KdMeja'],
						  'KdAgent' => $row['KdAgent'],
						  'id'=>$row['Id']
						);
			try {
				$this->db->insert('transaksi_detail', $data);
				
				if ($this->db->affected_rows() != 1)
				{
					throw new Exception("Gagal insert detaill kassa " . $nokassa . ' Id ' . $id);
				}else{
					$sql = "UPDATE transaksi_detail c  
								set syncflg = 1
								where c.NoKassa='$nokassa' AND c.Id=$id";
					$qry = $db2->query($sql);
				}
			} catch (Exception $e) {
				echo $e->getMessage();
			}		
			
		}
		$db2->close();
    }
	
	function TransaksiDetailPerTgl($date, $kassa,$conn){
    	//hapus di natura
    	
    	$sqldb = "Delete from transaksi_detail_sunset where tanggal = '$date' and NoKassa in(".$kassa.")";
    	$this->db->query($sqldb);
    	
    	//select dari source
        $sql    = "Select * from transaksi_detail where tanggal = '$date' ";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$data = array(
						  'NoKassa' => $row['NoKassa'],
						  'Gudang' => $row['Gudang'], 
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Waktu' => $row['Waktu'],
						  'Kasir' => $row['Kasir'],
						  'KdStore' => $row['KdStore'], 
						  'PCode' => $row['PCode'],
						  'Qty' => $row['Qty'],						  
						  'Berat' => $row['Berat'],
						  'Harga' => $row['Harga'],
						  'Ketentuan1' => $row['Ketentuan1'],
						  'Disc1' => $row['Disc1'],
						  'Jenis1' => $row['Jenis1'],
						  'Ketentuan2' => $row['Ketentuan2'],
						  'Disc2'=> $row['Disc2'],
						  'Jenis2' => $row['Jenis2'],
						  'Ketentuan3' => $row['Ketentuan3'],
						  'Disc3' => $row['Disc3'],
						  'Jenis3' => $row['Jenis3'],
						  'Ketentuan4' => $row['Ketentuan4'],
						  'Disc4' => $row['Disc4'],
						  'Jenis4' => $row['Jenis4'],
						  'Netto' => $row['Netto'],
						  'Hpp' => $row['Hpp'],
						  'Status' => $row['Status'],
						  'Keterangan' => $row['Keterangan'],
						  'Service_charge' => $row['Service_charge'],
						  'Komisi' => $row['Komisi'],
						  'PPN' => $row['PPN'],
						  'Printer' => $row['Printer'],
						  'KdMeja' => $row['KdMeja'],
						  'KdAgent' => $row['KdAgent']
						);
						
			//insert ke natura
			$this->db->insert('transaksi_detail_sunset', $data);
		}
    }
    
    function TransaksiDetailVoucher($kassa,$conn){
    	$db2 = $this->load->database('sarinah', TRUE);
		
		$sql1 = "delete from tempSync";
		$qry = $db2->query($sql1);
		
        $sql2 = "insert into tempSync Select NoKassa, NoStruk from transaksi_detail_voucher where syncflg<>1 group by NoKassa, NoStruk";
        $qry = $db2->query($sql2);
		
		$sql3 = "select * from tempSync";
		$qry = $db2->query($sql3);
    	$result = $qry->result_array();
    	$qry->free_result();
		foreach($result as $row){
			$nokassa = $row['NoKassa'];
			$nostruk = $row['NoStruk'];
			$this->db->delete('transaksi_detail_voucher', array('NoKassa' => $nokassa,'NoStruk'=>$nostruk));
		}
		
		$sql4 = "select d.* from transaksi_detail_voucher d inner join tempSync t on d.NoKassa=t.NoKassa and d.NoStruk=t.NoStruk ";
        $qry = $db2->query($sql4);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$nokassa = $row['NoKassa'];
			$id= $row['Id'];
			$data = array(
						  'NoKassa' => $row['NoKassa'],
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Jenis' => $row['Jenis'],
						  'NomorVoucher' => $row['NomorVoucher'],
						  'NilaiVoucher' => $row['NilaiVoucher'],
						  'Id' => $row['Id']
						);
			
			try {
				$this->db->insert('transaksi_detail_voucher', $data);
				
				if ($this->db->affected_rows() != 1)
				{
					throw new Exception("Gagal insert detail voucher kassa " . $nokassa . ' Id ' . $id);
				}else{
					$sql = "UPDATE transaksi_detail_voucher c  
								set syncflg = 1
								where c.NoKassa='$nokassa' AND c.Id=$id";
					$qry = $db2->query($sql);
				}
			} catch (Exception $e) {
				echo $e->getMessage();
			}		
			
		}
		$db2->close();

    }
	
	function TransaksiDetailVoucherPerTgl($date,$kassa,$conn){
    	//delete dari natura
    	$sqldb = "DELETE d FROM transaksi_header_sunset h INNER JOIN transaksi_detail_voucher_sunset d ON h.nostruk=d.nostruk AND h.nokassa=d.nokassa
 					WHERE h.tanggal = '$date' AND h.NoKassa in(".$kassa.")";
 		$this->db->query($sqldb);
    	
        $sql    = "Select * from transaksi_detail_voucher where tanggal = '$date' ";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$data = array(
						  'NoKassa' => $row['NoKassa'],
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Jenis' => $row['Jenis'],
						  'NomorVoucher' => $row['NomorVoucher'],
						  'NilaiVoucher' => $row['NilaiVoucher']
						);
			$this->db->insert('transaksi_detail_voucher_sunset', $data);
		}
    }
    
    function TransaksiDetailBayar($kassa,$conn){
    	$db2 = $this->load->database('sarinah', TRUE);
		
		$sql1 = "delete from tempSync";
		$qry = $db2->query($sql1);
		
        $sql2 = "insert into tempSync Select NoKassa, NoStruk from transaksi_detail_bayar where syncflg<>1 group by NoKassa, NoStruk";
        $qry = $db2->query($sql2);
		
		$sql3 = "select * from tempSync";
		$qry = $db2->query($sql3);
    	$result = $qry->result_array();
    	$qry->free_result();
		foreach($result as $row){
			$nokassa = $row['NoKassa'];
			$nostruk = $row['NoStruk'];
			$this->db->delete('transaksi_detail_bayar', array('NoKassa' => $nokassa,'NoStruk'=>$nostruk));
		}
		
		$sql4 = "select d.* from transaksi_detail_bayar d inner join tempSync t on d.NoKassa=t.NoKassa and d.NoStruk=t.NoStruk ";
        $qry = $db2->query($sql4);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $rec){
		    $nokassa = $rec['NoKassa'];
			$id = $rec['Id'];
			$data = array(
						  'Gudang' => $rec['Gudang'],
						  'NoKassa' => $rec['NoKassa'],
						  'NoStruk' => $rec['NoStruk'],
						  'Jenis' => $rec['Jenis'],
						  'Kode' => $rec['Kode'],
						  'NomorKKredit' => $rec['NomorKKredit'],
						  'NomorKDebet' => $rec['NomorKDebet'],
						  'NomorVoucher' => $rec['NomorVoucher'],
						  'Status' => $rec['Status'],
						  'Keterangan' => $rec['Keterangan'],
						  'ExpDate' => $rec['ExpDate'],
						  'NilaiTunai' => $rec['NilaiTunai'],
						  'NilaiKredit' => $rec['NilaiKredit'],
						  'NilaiDebet' => $rec['NilaiDebet'],
						  'NilaiVoucher' => $rec['NilaiVoucher'],
						  'NilaiPoint' => $rec['NilaiPoint'],
						  'NilaiGoPay' => $rec['NilaiGoPay'],
						  'Currency' => $rec['Currency'],
						  'Valas' => $rec['Valas'],
						  'Kurs' => $rec['Kurs'],
						  'Valuta' => $rec['Valuta'],
						  'KdMember' => $rec['KdMember'],
						  'Id' => $rec['Id']
						);
			try {
				$this->db->insert('transaksi_detail_bayar', $data);
				
				if ($this->db->affected_rows() != 1)
				{
					throw new Exception("Gagal insert detail bayar kassa " . $nokassa . ' Id ' . $id);
				}else{
					$sql = "UPDATE transaksi_detail_bayar c  
								set syncflg = 1
								where c.NoKassa='$nokassa' AND c.Id=$id";
					$qry = $db2->query($sql);
				}
			} catch (Exception $e) {
				echo $e->getMessage();
			}	
			
		}
		$db2->close();
    }
	
	function TransaksiDetailBayarPerTgl($date, $kassa,$conn){
    	//delete dari natura
    	$sqldb = "DELETE d FROM transaksi_header_sunset h INNER JOIN transaksi_detail_bayar_sunset d ON h.nostruk=d.nostruk AND h.nokassa=d.nokassa
 					WHERE h.tanggal = '$date' AND h.NoKassa in(".$kassa.")";
 		$this->db->query($sqldb);
    	
        $sql    = "Select d.* FROM transaksi_header h INNER JOIN transaksi_detail_bayar d ON h.nostruk=d.nostruk AND h.nokassa=d.nokassa
 					WHERE h.tanggal = '$date' ";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $rec){
			$data = array(
						  'Gudang' => $rec['Gudang'],
						  'NoKassa' => $rec['NoKassa'],
						  'NoStruk' => $rec['NoStruk'],
						  'Jenis' => $rec['Jenis'],
						  'Kode' => $rec['Kode'],
						  'NomorKKredit' => $rec['NomorKKredit'],
						  'NomorKDebet' => $rec['NomorKDebet'],
						  'NomorVoucher' => $rec['NomorVoucher'],
						  'Status' => $rec['Status'],
						  'Keterangan' => $rec['Keterangan'],
						  'ExpDate' => $rec['ExpDate'],
						  'NilaiTunai' => $rec['NilaiTunai'],
						  'NilaiKredit' => $rec['NilaiKredit'],
						  'NilaiDebet' => $rec['NilaiDebet'],
						  'NilaiVoucher' => $rec['NilaiVoucher'],
						  'NilaiPoint' => $rec['NilaiPoint'],
						  'NilaiGoPay' => $rec['NilaiGoPay'],
						  'Currency' => $rec['Currency'],
						  'Valas' => $rec['Valas'],
						  'Kurs' => $rec['Kurs'],
						  'Valuta' => $rec['Valuta'],
						  'KdMember' => $rec['KdMember']
						);
			$this->db->insert('transaksi_detail_bayar_sunset', $data);
		}
    }

    
    function InsertStatus($store, $status){
		$date = date("Y-m-d H:i:s");
		$data = array('Tanggal' => $date,
					'NamaOutlet'=>$store,
					'Status'=> $status
					);
		$this->db->insert('tarik_data_log',$data);
	}

	function TransaksiHeaderPerOutletPerBulan($tahun,$bulan,$kassa,$conn){
    	$sqldb = "DELETE FROM transaksi_header_sunset WHERE YEAR(tanggal) = '$tahun' AND MONTH(tanggal) = '$bulan'  AND NoKassa in(".$kassa.")";
    	$this->db->query($sqldb);
    	
    	//select dari source
        $sql    = "SELECT * FROM transaksi_header WHERE YEAR(tanggal) = '$tahun' AND MONTH(tanggal) = '$bulan' ";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$data = array('NoKassa' => $row['NoKassa'],
						  'Gudang' => $row['Gudang'],
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Waktu' => $row['Waktu'],
						  'Kasir' => $row['Kasir'],
						  'KdStore' => $row['KdStore'],
						  'TotalItem' => $row['TotalItem'],
						  'TotalNilai' => $row['TotalNilai'],
						  'TotalBayar' => $row['TotalBayar'],
						  'Kembali' => $row['Kembali'],
						  'Tunai' => $row['Tunai'],
						  'KKredit' => $row['KKredit'],
						  'KDebit' => $row['KDebit'],
						  'Voucher' => $row['Voucher'],
						  'Point' => $row['Point'],
						  'GoPay' => $row['GoPay'],
						  'VoucherTravel' => $row['VoucherTravel'],
						  'Discount' => $row['Discount'],
						  'Status' => $row['Status'],
						  'KdCustomer' => $row['KdCustomer'],
						  'NamaCustomer' => $row['NamaCustomer'],
						  'Gender' => $row['Gender'],
						  'TglLahir' => $row['TglLahir'],
						  'Keterangan' => $row['Keterangan'],
						  'EditDate' => $row['EditDate'],
						  'EditUser' => $row['EditUser'],
						  'KdAgent' => $row['KdAgent'],
						  'KdGsa' => $row['KdGsa'],
						  'Ttl_Charge'=> $row['Ttl_Charge'],
						  'DPP' =>  $row['DPP'],
						  'TAX' => $row['TAX'],
						  'KdMeja' => $row['KdMeja'],
						  'userdisc' => $row['userdisc'],
						  'nilaidisc' => $row['nilaidisc'],
						  'statuskomisi' => $row['statuskomisi'],
						  'Valas' => $row['Valas'],
						  'Kurs' => $row['Kurs'],
						  'Valuta' => $row['Valuta'],
						  'TotalGuest'=> $row['TotalGuest'],
						  'KdMember'=> $row['KdMember'],
						  'NamaCard'=> $row['NamaCard'],
						  'NoCard'=> $row['NoCard']
						);
						
			//insert ke natura
			$this->db->insert('transaksi_header_sunset', $data);
		}
    }

    function TransaksiDetailPerOutletPerBulan($tahun,$bulan,$kassa,$conn){
    	//hapus di natura
    	$sqldb = "DELETE FROM transaksi_detail_sunset WHERE YEAR(tanggal) = '$tahun' AND MONTH(tanggal) = '$bulan' AND NoKassa in(".$kassa.")";
    	$this->db->query($sqldb);
    	
    	//select dari source
        $sql    = "SELECT * FROM transaksi_detail WHERE YEAR(tanggal) = '$tahun' AND MONTH(tanggal) = '$bulan'";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$data = array(
						  'NoKassa' => $row['NoKassa'],
						  'Gudang' => $row['Gudang'], 
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Waktu' => $row['Waktu'],
						  'Kasir' => $row['Kasir'],
						  'KdStore' => $row['KdStore'], 
						  'PCode' => $row['PCode'],
						  'Qty' => $row['Qty'],				  
						  'Berat' => $row['Berat'],
						  'Harga' => $row['Harga'],
						  'Ketentuan1' => $row['Ketentuan1'],
						  'Disc1' => $row['Disc1'],
						  'Jenis1' => $row['Jenis1'],
						  'Ketentuan2' => $row['Ketentuan2'],
						  'Disc2'=> $row['Disc2'],
						  'Jenis2' => $row['Jenis2'],
						  'Ketentuan3' => $row['Ketentuan3'],
						  'Disc3' => $row['Disc3'],
						  'Jenis3' => $row['Jenis3'],
						  'Ketentuan4' => $row['Ketentuan4'],
						  'Disc4' => $row['Disc4'],
						  'Jenis4' => $row['Jenis4'],
						  'Netto' => $row['Netto'],
						  'Hpp' => $row['Hpp'],
						  'Status' => $row['Status'],
						  'Keterangan' => $row['Keterangan'],
						  'Service_charge' => $row['Service_charge'],
						  'Komisi' => $row['Komisi'],
						  'PPN' => $row['PPN'],
						  'Printer' => $row['Printer'],
						  'KdMeja' => $row['KdMeja'],
						  'KdAgent' => $row['KdAgent']
						);
						
			//insert ke natura
			$this->db->insert('transaksi_detail_sunset', $data);
		}
    }

    function TransaksiDetailVoucherPerOutletPerBulan($tahun,$bulan,$kassa,$conn){
    	//delete dari natura
    	$sqldb = "DELETE d FROM transaksi_header_sunset h INNER JOIN transaksi_detail_voucher_sunset d ON h.nostruk=d.nostruk AND h.nokassa=d.nokassa
 					WHERE YEAR(h.tanggal) = '$tahun' AND MONTH(h.tanggal) = '$bulan' AND h.NoKassa in(".$kassa.")";
 		$this->db->query($sqldb);
    	
        $sql    = "SELECT * FROM transaksi_detail_voucher WHERE YEAR(tanggal) = '$tahun' AND MONTH(tanggal) = '$bulan'";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $row){
			$data = array(
						  'NoKassa' => $row['NoKassa'],
						  'NoStruk' => $row['NoStruk'],
						  'Tanggal' => $row['Tanggal'],
						  'Jenis' => $row['Jenis'],
						  'NomorVoucher' => $row['NomorVoucher'],
						  'NilaiVoucher' => $row['NilaiVoucher']
						);
			$this->db->insert('transaksi_detail_voucher_sunset', $data);
		}
    }

    function TransaksiDetailBayarPerOutletPerBulan($tahun,$bulan,$kassa,$conn){
    	//delete dari natura
    	$sqldb = "DELETE d FROM transaksi_header_sunset h INNER JOIN transaksi_detail_bayar_sunset d ON h.nostruk=d.nostruk AND h.nokassa=d.nokassa
 					WHERE YEAR(h.tanggal) = '$tahun' AND MONTH(h.tanggal) = '$bulan' AND h.NoKassa in(".$kassa.")";
 		$this->db->query($sqldb);
    	
        $sql    = "SELECT d.* FROM transaksi_header h INNER JOIN transaksi_detail_bayar d ON h.nostruk=d.nostruk AND h.nokassa=d.nokassa
 					WHERE YEAR(h.tanggal) = '$tahun' AND MONTH(h.tanggal) = '$bulan'";
        $qry = $conn->query($sql);
    	$result = $qry->result_array();
    	$qry->free_result();
        foreach($result as $rec){
			$data = array(
						  'Gudang' => $rec['Gudang'],
						  'NoKassa' => $rec['NoKassa'],
						  'NoStruk' => $rec['NoStruk'],
						  'Jenis' => $rec['Jenis'],
						  'Kode' => $rec['Kode'],
						  'NomorKKredit' => $rec['NomorKKredit'],
						  'NomorKDebet' => $rec['NomorKDebet'],
						  'NomorVoucher' => $rec['NomorVoucher'],
						  'Status' => $rec['Status'],
						  'Keterangan' => $rec['Keterangan'],
						  'ExpDate' => $rec['ExpDate'],
						  'NilaiTunai' => $rec['NilaiTunai'],
						  'NilaiKredit' => $rec['NilaiKredit'],
						  'NilaiDebet' => $rec['NilaiDebet'],
						  'NilaiVoucher' => $rec['NilaiVoucher'],
						  'NilaiPoint' => $rec['NilaiPoint'],
						  'NilaiGoPay' => $rec['NilaiGoPay'],
						  'Currency' => $rec['Currency'],
						  'Valas' => $rec['Valas'],
						  'Kurs' => $rec['Kurs'],
						  'Valuta' => $rec['Valuta'],
						  'KdMember' => $rec['KdMember']
						);
			$this->db->insert('transaksi_detail_bayar_sunset', $data);
		}
    }
}
?>