<?php
class create_jurnal_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getUser()
	{
		$sql = "select * from user where Active='Y' and (userlevel=-1 or userlevel=1) and id<>'00'";
		$query = $this->db->query($sql);
		$row = $query->result_array();
        $query->free_result();
        return $row;
	}
	function getinterface()
	{
		$sql = "SELECT * FROM interface limit 1";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getPeriodeGL()
	{
		$sql = "select PeriodeGL from aplikasi order by Tahun desc limit 0,1";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row[0]['PeriodeGL'];
	}
	
	function getListTahun()
	{
		$sql = "select distinct Tahun from counter order by Tahun";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
    
	function getpayment($tahun,$bulan)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,Jumlah AS Debit,a.TglDokumen,a.Keterangan AS KetDetail,
		        b.KdKasBank,c.KdRekening as RekeningHeader,
				a.KdSubDivisi as divisidetail,
				c.KdSubDivisi as divisiheader,
				b.NoPosting,b.Keterangan AS KetHeader,
				b.AddUser
				FROM trans_payment_detail a, trans_payment_header b, kasbank c
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun'
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen AND b.KdKasBank=c.KdKasBank ORDER BY
				a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getpaymentCekUlang($tahun,$bulan,$nodok)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,Jumlah AS Debit,a.TglDokumen,a.Keterangan AS KetDetail,
		        b.KdKasBank,c.KdRekening as RekeningHeader,
				a.KdSubDivisi as divisidetail,
				c.KdSubDivisi as divisiheader,
				b.NoPosting,b.Keterangan AS KetHeader 
				FROM trans_payment_detail a, trans_payment_header b, kasbank c
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun' AND a.NoDokumen='$nodok' 
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen AND b.KdKasBank=c.KdKasBank ORDER BY
				a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getpaymentJurnal($tahun,$bulan,$no)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,Jumlah AS Debit,a.TglDokumen,a.Keterangan AS KetDetail,
		        b.KdKasBank,c.KdRekening as RekeningHeader,
				a.KdSubDivisi as divisidetail,
				c.KdSubDivisi as divisiheader,
				b.NoPosting,b.Keterangan AS KetHeader 
				FROM trans_payment_detail a, trans_payment_header b, kasbank c
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun'
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen AND b.KdKasBank=c.KdKasBank AND a.NoDokumen='$no' ORDER BY
				a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}

    function getKdRekening($kd,$posisi)
    {
        $sql = "SELECT * FROM kode_acc_gl WHERE KdTRansaksi = '$kd' and Posisi='$posisi'";
        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
    }

    
   	function getreceipt($tahun,$bulan)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,Jumlah AS Kredit,a.TglDokumen,a.Keterangan AS KetDetail,
		        b.KdKasBank,c.KdRekening as RekeningHeader,
				b.NoPosting,
				a.KdSubDivisi as divisidetail,
				c.KdSubDivisi as divisiheader,
				b.Keterangan AS KetHeader,
				b.AddUser 
				FROM trans_receipt_detail a, trans_receipt_header b, kasbank c
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun' 
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen AND b.KdKasBank=c.KdKasBank ORDER BY a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getreceiptCekUlang($tahun, $bulan, $kodenya)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,Jumlah AS Kredit,a.TglDokumen,a.Keterangan AS KetDetail,
		        b.KdKasBank,c.KdRekening as RekeningHeader,
				b.NoPosting,
				a.KdSubDivisi as divisidetail,
				c.KdSubDivisi as divisiheader,
				b.Keterangan AS KetHeader 
				FROM trans_receipt_detail a, trans_receipt_header b, kasbank c
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun' AND a.NoDokumen='$kodenya'
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen AND b.KdKasBank=c.KdKasBank ORDER BY a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getreceiptJurnal($tahun,$bulan,$no)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,Jumlah AS Kredit,a.TglDokumen,a.Keterangan AS KetDetail,
		        b.KdKasBank,c.KdRekening as RekeningHeader,
				b.NoPosting,
				a.KdSubDivisi as divisidetail,
				c.KdSubDivisi as divisiheader,
				b.Keterangan AS KetHeader 
				FROM trans_receipt_detail a, trans_receipt_header b, kasbank c
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun' 
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen AND b.KdKasBank=c.KdKasBank AND a.`NoDokumen`='$no' ORDER BY a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getjurnal($tahun,$bulan)
	{
		$sql = "SELECT a.NoDokumen,a.KdRekening,a.Debit,a.Kredit,a.TglDokumen,a.Keterangan AS KetDetail,
				a.KdSubDivisi as divisidetail, b.AddUser,
		        b.NoPosting,b.Keterangan AS KetHeader 
				FROM trans_jurnal_detail a, trans_jurnal_header b
                WHERE MONTH(a.TglDokumen)='$bulan' AND YEAR(a.TglDokumen)='$tahun' 
                 and (a.status <> 'B' OR a.status IS NULL)
                and (b.status <> 'B' OR b.status IS NULL)
                AND a.NoDokumen=b.NoDokumen ORDER BY a.NoDokumen,a.Urutan";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getpembelian($tahun,$bulan)
	{
		$sql = "SELECT h.`NoFaktur`, h.`Tanggal`, h.`NoFakturPajak`, CONCAT(b.`NamaLengkap`,' ', d.qty, ' ', d.`Satuan`) AS Keterangan, 
				h.`PPN`, d.`Qty`, d.`Harga`,  g.`KdSubDivisi`, b.`KdRekeningPersediaan`, s.`Nama`, h.NoPosting, h.AddUser
				FROM invoice_pembelian_header h INNER JOIN invoice_pembelian_detail d ON h.`NoFaktur`=d.`NoFaktur`
				inner join trans_terima_header t on h.NoPenerimaan=t.NoDokumen
				inner join gudang g on t.KdGudang=g.KdGudang
				INNER JOIN masterbarang b ON d.`KdBarang`=b.`PCode`
				INNER JOIN supplier s ON h.`KdSupplier`=s.`KdSupplier`
				WHERE Year(h.Tanggal)='$tahun'  and Month(h.Tanggal)='$bulan' and t.Status=1
				ORDER BY h.`NoFaktur`, d.`NoUrut`";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getPembelianNonStok($tahun, $bulan){
		$sql = "SELECT d.`NoDokumen`,h.TglDokumen, d.`KdRekening`, d.`KdSubdivisi`, d.`Deskripsi`, d.`Jumlah`, h.`Keterangan`, h.AddUser
				FROM pi_marketing h INNER JOIN pi_marketing_detail_akun d ON h.`NoDokumen`=d.`NoDokumen`
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`Status`=1
				ORDER BY h.`NoDokumen`, d.`NoUrut`";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getpenjualan($tahun,$bulan)
	{
		$sql = "SELECT 
				  s.`KdDivisi`,
				  b.KdSubDivisi,
				  r.`KdDivisiReport`,
				  r.`NamaDivisiReport`,
				  v.KdRekeningSales, v.KdRekeningDisc,k.JenisPajak,
  				  SUM(h.`TotalNilaiPem`) AS Rounding,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Disc,
				  SUM(IF(h.`Discount`>0 AND (h.`userdisc`='marketing' OR (IFNULL(h.`KdCustomer`,'')!='' AND h.`VoucherTravel`>0)),IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMKT,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='outlet',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOutlet,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='office',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOffice,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`NamaCard`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKK,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMember,
				  SUM(IF(h.`Discount`>0 AND LENGTH(IFNULL(h.`KdCustomer`,''))>1 AND h.VoucherTravel=0,IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKaryawan,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)
				  ) AS Sales,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.`Netto` * 1155 / 1000,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher,
				  sum(d.Qty*d.Hpp) as HPP,
				  b.KdRekeningPersediaan  
				FROM
				  transaksi_header h 
				  INNER JOIN transaksi_detail d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk` 
				  INNER JOIN masterbarang b 
				    ON d.`PCode` = b.`PCode` 
				  INNER JOIN subdivisi s 
				    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  inner join divisi v on b.KdDivisi=v.KdDivisi
				  inner join kassa k on h.NoKassa=k.id_kassa
				WHERE Year(h.Tanggal)= '$tahun' and month(h.Tanggal)='$bulan'
				   AND h.`Status` = 1 and h.TotalNilai>0 
				GROUP BY r.KdDivisiReport 
				UNION ALL
				SELECT 
				  s.`KdDivisi`,
				  s.KdSubDivisi,
				  r.`KdDivisiReport`,
				  r.`NamaDivisiReport`,
				  v.KdRekeningSales, v.KdRekeningDisc,k.JenisPajak,  	
  				  SUM(h.`TotalNilaiPem`) AS Rounding,
				  SUM(d.`Qty` * IF(k.PriceIncludeTax = 'T',IF(d.`Berat`<>0,d.`Harga`*(d.`Berat`/100),d.`Harga`),IF(d.`Berat`<>0,(d.`Harga`*(d.`Berat`/100))/1.1,d.`Harga`/1.1)) ) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Disc,
				  SUM(IF(h.`Discount`>0 AND (h.`userdisc`='marketing' OR (IFNULL(h.`KdCustomer`,'')!='' AND h.`VoucherTravel`>0)),IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMKT,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='outlet',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOutlet,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='office',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOffice,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`NamaCard`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKK,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMember,
				  SUM(IF(h.`Discount`>0 AND LENGTH(IFNULL(h.`KdCustomer`,''))>1 AND h.VoucherTravel=0,IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKaryawan,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
				    IF(k.PriceIncludeTax='T',IF(d.`Service_charge` > 0,d.`Netto` * 1155 / 1000, d.`Netto`*1.1),d.Netto)
				  ) AS Sales,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.`Netto` * 1155 / 1000,
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher,
				    sum(d.Qty*d.Hpp) as HPP,
				    b.KdRekeningPersediaan  
				FROM
				  transaksi_header_sunset h 
				  INNER JOIN transaksi_detail_sunset d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k on h.nokassa=k.id_kassa
				  INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
				  INNER JOIN subdivisi s 
				    ON k.`SubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  inner join divisi v on s.KdDivisi=v.KdDivisi
				WHERE Year(h.Tanggal)= '$tahun' and month(h.Tanggal)='$bulan'
				   AND h.`Status` = 1 and h.TotalNilai>0
				GROUP BY r.KdDivisiReport 
				";		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}

	function getpenjualanSC8_20200910($tahun,$bulan) #ISO
	{
		$sql = "SELECT 
				  s.`KdDivisi`,
				  b.KdSubDivisi,
				  r.`KdDivisiReport`,
				  r.`NamaDivisiReport`,
				  v.KdRekeningSales, v.KdRekeningDisc,k.JenisPajak,
  				  SUM(h.`TotalNilaiPem`) AS Rounding,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Disc,
				  SUM(IF(h.`Discount`>0 AND (h.`userdisc`='marketing' OR (IFNULL(h.`KdCustomer`,'')!='' AND h.`VoucherTravel`>0)),IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMKT,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='outlet',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOutlet,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='office',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOffice,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`NamaCard`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKK,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMember,
				  SUM(IF(h.`Discount`>0 AND LENGTH(IFNULL(h.`KdCustomer`,''))>1 AND h.VoucherTravel=0,IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKaryawan,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
					IF(k.PriceIncludeTax='T',
						IF(d.`Service_charge` > 0,
							d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
						d.`Netto`*1.1),
					d.Netto)
				) AS Sales,
				  SUM(h.`Voucher`*(IF(k.PriceIncludeTax='T',
						IF(d.`Service_charge` > 0,
							d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
						d.`Netto`*1.1),
					d.Netto))/h.`TotalNilai`) AS Voucher,
				  sum(d.Qty*d.Hpp) as HPP,
				  b.KdRekeningPersediaan  
				FROM
				  transaksi_header h 
				  INNER JOIN transaksi_detail d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk` 
				  INNER JOIN masterbarang b 
				    ON d.`PCode` = b.`PCode` 
				  INNER JOIN subdivisi s 
				    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  inner join divisi v on b.KdDivisi=v.KdDivisi
				  inner join kassa k on h.NoKassa=k.id_kassa
				WHERE Year(h.Tanggal)= '$tahun' and month(h.Tanggal)='$bulan'
				   AND h.`Status` = 1 and h.TotalNilai>0 
				GROUP BY r.KdDivisiReport 
				UNION ALL
				SELECT 
				  s.`KdDivisi`,
				  s.KdSubDivisi,
				  r.`KdDivisiReport`,
				  r.`NamaDivisiReport`,
				  v.KdRekeningSales, v.KdRekeningDisc,k.JenisPajak,
  				  SUM(h.`TotalNilaiPem`) AS Rounding,
				  SUM(d.`Qty` * IF(k.PriceIncludeTax = 'T',IF(d.`Berat`<>0,d.`Harga`*(d.`Berat`/100),d.`Harga`),IF(d.`Berat`<>0,(d.`Harga`*(d.`Berat`/100))/1.1,d.`Harga`/1.1)) ) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Disc,
				  SUM(IF(h.`Discount`>0 AND (h.`userdisc`='marketing' OR (IFNULL(h.`KdCustomer`,'')!='' AND h.`VoucherTravel`>0)),IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMKT,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='outlet',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOutlet,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='office',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOffice,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`NamaCard`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKK,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMember,
				  SUM(IF(h.`Discount`>0 AND LENGTH(IFNULL(h.`KdCustomer`,''))>1 AND h.VoucherTravel=0,IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKaryawan,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
					IF(k.PriceIncludeTax='T',
						IF(d.`Service_charge` > 0,
							d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
						d.`Netto`*1.1),
					d.Netto)
				) AS Sales,
				  SUM(h.`Voucher`*(IF(k.PriceIncludeTax='T',
						IF(d.`Service_charge` > 0,
							d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
						d.`Netto`*1.1),
					d.Netto))/h.`TotalNilai`) AS Voucher,
				    sum(d.Qty*d.Hpp) as HPP,
				    b.KdRekeningPersediaan  
				FROM
				  transaksi_header_sunset h 
				  INNER JOIN transaksi_detail_sunset d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k on h.nokassa=k.id_kassa
				  INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
				  INNER JOIN subdivisi s 
				    ON k.`SubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  inner join divisi v on s.KdDivisi=v.KdDivisi
				WHERE Year(h.Tanggal)= '$tahun' and month(h.Tanggal)='$bulan'
				   AND h.`Status` = 1 and h.TotalNilai>0
				GROUP BY r.KdDivisiReport 
				";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}

	function getpenjualanSC8($tahun,$bulan) #ISO test
	{
		$sql = "SELECT KdDivisi,
				       KdSubDivisi,
				       KdDivisiReport,
				       NamaDivisiReport,
				       KdRekeningSales, 
				       KdRekeningDisc,
				       JenisPajak, 
				       Rounding, 
				       Gross,
				       Disc, 
				       DiscMKT,
				       DiscOutlet, 
				       Disc-DiscMKT-DiscOffice-DiscKK-DiscMember-DiscKaryawan AS DiscOutlet2,
				       DiscOffice, 
				       DiscKK, 
				       DiscMember, 
				       DiscKaryawan, 
				       Nett, 
				       ServiceCharge, 
				       Tax, 
				       Sales, 
				       Voucher,
				       HPP,
				       KdRekeningPersediaan FROM(
				SELECT 
				  s.`KdDivisi`,
				  b.KdSubDivisi,
				  r.`KdDivisiReport`,
				  r.`NamaDivisiReport`,
				  v.KdRekeningSales, 
				  v.KdRekeningDisc,
				  k.JenisPajak,
				  SUM(h.`TotalNilaiPem`) AS Rounding,
				  SUM(d.`Qty`*IF(k.PriceIncludeTax='T',d.`Harga`,d.`Harga`/1.1)) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Disc,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='marketing',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMKT,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='outlet',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOutlet,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='office',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOffice,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`NamaCard`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKK,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMember,
				  SUM(IF(h.`Discount`>0 AND LENGTH(IFNULL(h.`KdCustomer`,''))>1 AND h.VoucherTravel=0,IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKaryawan,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
					IF(k.PriceIncludeTax='T',
						IF(d.`Service_charge` > 0,
							d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
						d.`Netto`*1.1),
					d.Netto)
				) AS Sales,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher,
				  SUM(d.Qty*d.Hpp) AS HPP,
				  b.KdRekeningPersediaan  
				FROM
				  transaksi_header h 
				  INNER JOIN transaksi_detail d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk` 
				  INNER JOIN masterbarang b 
				    ON d.`PCode` = b.`PCode` 
				  INNER JOIN subdivisi s 
				    ON b.`KdSubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  INNER JOIN divisi v ON b.KdDivisi=v.KdDivisi
				  INNER JOIN kassa k ON h.NoKassa=k.id_kassa
				WHERE YEAR(h.Tanggal)= '$tahun' AND MONTH(h.Tanggal)='$bulan'
				   AND h.`Status` = 1 AND h.TotalNilai>0 
				GROUP BY r.KdDivisiReport 
				UNION ALL
				SELECT 
				  s.`KdDivisi`,
				  s.KdSubDivisi,
				  r.`KdDivisiReport`,
				  r.`NamaDivisiReport`,
				  v.KdRekeningSales, 
				  v.KdRekeningDisc,
				  k.JenisPajak,
				  SUM(h.`TotalNilaiPem`) AS Rounding,
				  SUM(d.`Qty` * IF(k.PriceIncludeTax = 'T',IF(d.`Berat`<>0,d.`Harga`*(d.`Berat`/100),d.`Harga`),IF(d.`Berat`<>0,(d.`Harga`*(d.`Berat`/100))/1.1,d.`Harga`/1.1)) ) AS Gross,
				  SUM(IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1)) AS Disc,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='marketing',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMKT,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='outlet',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOutlet,
				  SUM(IF(h.`Discount`>0 AND h.`userdisc`='office',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscOffice,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`NamaCard`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKK,
				  SUM(IF(h.`Discount`>0 AND IFNULL(h.`KdMember`,'')!='',IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscMember,
				  SUM(IF(h.`Discount`>0 AND LENGTH(IFNULL(h.`KdCustomer`,''))>1 AND h.VoucherTravel=0,IF(k.PriceIncludeTax='T',d.`Disc1`,d.`Disc1`/1.1),0)) AS DiscKaryawan,
				  SUM(IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)) AS Nett,
				  SUM(d.`Netto` * d.Service_charge / 100) AS ServiceCharge,
				  SUM((IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1)+ (IF(k.PriceIncludeTax='T',d.`Netto`,d.`Netto`/1.1) * d.Service_charge / 100))*0.1) AS Tax,
				  SUM(
					IF(k.PriceIncludeTax='T',
						IF(d.`Service_charge` > 0,
							d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
						d.`Netto`*1.1),
					d.Netto)
				) AS Sales,
				  SUM(h.`Voucher`*(IF(
				      k.PriceIncludeTax='T',
				      d.`Netto` + (d.`Netto`*(d.`Service_charge`/100))+ ((d.`Netto`+(d.`Netto`*(d.`Service_charge`/100)) )*(d.`PPN`/100)),
				      d.Netto
				    )/h.`TotalNilai`)) AS Voucher,
				    SUM(d.Qty*d.Hpp) AS HPP,
				    b.KdRekeningPersediaan  
				FROM
				  transaksi_header_sunset h 
				  INNER JOIN transaksi_detail_sunset d 
				    ON h.`NoKassa` = d.`NoKassa` 
				    AND h.`NoStruk` = d.`NoStruk`
				  INNER JOIN kassa k ON h.nokassa=k.id_kassa
				  INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
				  INNER JOIN subdivisi s 
				    ON k.`SubDivisi` = s.`KdSubDivisi` 
				  INNER JOIN divisireport r 
				    ON s.`KdDivisiReport` = r.`KdDivisiReport` 
				  INNER JOIN divisi v ON s.KdDivisi=v.KdDivisi
				WHERE YEAR(h.Tanggal)= '$tahun' AND MONTH(h.Tanggal)='$bulan'
				   AND h.`Status` = 1 AND h.TotalNilai>0
				GROUP BY r.KdDivisiReport ) tbl";
       // echo $sql;
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getCompliment($tahun,$bulan){
		$sql = "SELECT 
					  s.`KdDivisi`, b.KdSubDivisi, r.`KdDivisiReport`,
					  r.`NamaDivisiReport`,SUM(d.Qty * d.Hpp) AS total, b.KdRekeningPersediaan
					FROM
					  transaksi_detail d 
					  INNER JOIN transaksi_header h ON d.NoStruk = h.NoStruk AND h.Status = '1' AND (h.TotalNilai * 1) = '0' 
					    AND h.DPP * 1 = '0' AND h.Discount > 0 
					  INNER JOIN masterbarang b ON d.`PCode` = b.`PCode` 
					  INNER JOIN subdivisi s ON b.`KdSubDivisi` = s.`KdSubDivisi` 
					  INNER JOIN divisireport r ON s.`KdDivisiReport` = r.`KdDivisiReport` 
					  INNER JOIN divisi v ON b.KdDivisi=v.KdDivisi
					WHERE 1 
					  AND YEAR(h.Tanggal) = '$tahun' 
					  AND MONTH(h.Tanggal) = '$bulan' 
					GROUP BY r.KdDivisiReport
				UNION ALL
				SELECT 
					  s.`KdDivisi`, s.KdSubDivisi, r.`KdDivisiReport`,
					  r.`NamaDivisiReport`,coalesce(SUM(d.Qty * d.Hpp),0) AS total, b.KdRekeningPersediaan
					FROM
					  transaksi_detail_sunset d 
					  INNER JOIN transaksi_header_sunset h ON d.NoStruk = h.NoStruk AND h.Status = '1' AND (h.TotalNilai * 1) = '0' 
					    AND h.DPP * 1 = '0' AND h.Discount > 0 
					  inner join masterbarang b on d.pcode=b.pcode
					  INNER JOIN kassa k on h.nokassa=k.id_kassa
				  	  INNER JOIN subdivisi s ON k.`SubDivisi` = s.`KdSubDivisi` 
					  INNER JOIN divisireport r ON s.`KdDivisiReport` = r.`KdDivisiReport` 
					  INNER JOIN divisi v ON s.KdDivisi=v.KdDivisi
					WHERE 1 
					  AND YEAR(h.Tanggal) = '$tahun' 
					  AND MONTH(h.Tanggal) = '$bulan' 
					GROUP BY r.KdDivisiReport
				 ";
        
        $query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	/*function getpenjualan_tiket($tahun, $bulan){
		$sql = "SELECT sum(harga_asli) as RpTiket, sum(harga_asli-harga) as RpDisc, sum(harga) as NetTiket  
              FROM `ticket`
            WHERE
              1
            AND Year(add_date)='$tahun' and Month(add_date)='$bulan'
						AND ticket.`noidentitas` != '1234' and status=1
						AND PCode NOT IN
						(SELECT
								PCode a
							FROM
								groupswing a
						WHERE a.`Status` = 'A')";
                	
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}*/
	function getpenjualan_tiket($tahun, $bulan){
		$sql = "SELECT 
		SUM(ticket.harga_asli) AS RpTiket,
		SUM(ticket.harga_asli - harga) AS RpDisc,
		SUM(ticket.harga) AS NetTiket 
	  FROM
		ticket_head 
		INNER JOIN ticket 
		  ON ticket_head.notrans = ticket.notrans 
		LEFT JOIN kartu kr1 
		  ON ticket_head.BankDebet = kr1.id 
		LEFT JOIN kartu kr2 
		  ON ticket_head.EDCBankDebet = kr2.id 
		LEFT JOIN kartu kr3 
		  ON ticket_head.BankKredit = kr3.id 
		LEFT JOIN kartu kr4 
		  ON ticket_head.EDCBankKredit = kr4.id 
	  WHERE 1 
            AND Year(ticket_head.Tanggal)='$tahun' and Month(ticket_head.Tanggal)='$bulan'
						AND ticket_head.`noidentitas` != '1234' 
						AND ticket.PCode NOT IN
						(SELECT
								PCode a
							FROM
								groupswing a
						WHERE a.`Status` = 'A')";
                	
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	function getpenjualan_tiket_swing($tahun, $bulan){
		$sql = "SELECT sum(harga_asli) as RpTiket, sum(harga_asli-harga) as RpDisc, sum(harga) as NetTiket  
              FROM `ticket`
            WHERE
              1
            AND Year(add_date)='$tahun' and Month(add_date)='$bulan'
						AND ticket.`noidentitas` != '1234' and status=1
						AND PCode IN
						(SELECT
								PCode a
							FROM
								groupswing a
						WHERE a.`Status` = 'A')";
                	
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getdistribusi($tahun, $bulan){
		$sql = "SELECT h.InvoiceNo, sidate, dv.`KdRekeningSales`, dv.`KdRekeningDist`, b.`KdSubDivisi`,c.Nama, SUM(d.`quantity`*d.`nettprice`) AS Gross, SUM(d.`quantity`*d.`nettprice`*0.1) AS PPN , h.adduser
			FROM salesinvoice h INNER JOIN salesinvoicedetail d ON h.`invoiceno`=d.`invoiceno`
					INNER JOIN masterbarang b ON d.`inventorycode`=b.`PCode`
					INNER JOIN divisi dv ON b.`KdDivisi`=dv.`KdDivisi`
					INNER JOIN customer c ON h.`customerid`=c.`KdCustomer`
					WHERE YEAR(h.`sidate` )='$tahun' AND MONTH(h.`sidate`)='$bulan' AND h.`status`=1
					GROUP BY h.InvoiceNo, sidate, dv.`KdRekeningSales`, b.`KdSubDivisi`";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getpenerimaanlain($tahun, $bulan){
		$sql = "SELECT h.`NoDokumen`, h.TglDokumen, g.`KdSubDivisi`, b.`KdRekeningPersediaan`, (d.`Qty`*d.Harga) AS Nilai, 
					p.`coano`, h.Keterangan , d.Keterangan as KeteranganDetail, h.AddUser
					FROM trans_penerimaan_lain h INNER JOIN trans_penerimaan_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN intmutpurpose p ON h.`PurposeId`=p.`purposeid`
					INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
					inner join gudang g on h.KdGudang=g.KdGudang
					WHERE Year(TglDokumen)='$tahun' and Month(TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang<>'06'
					ORDER BY h.`NoDokumen`, d.`Sid`";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getpengeluaranlain($tahun, $bulan){
		$sql = "SELECT h.`NoDokumen`, h.TglDokumen, g.`KdSubDivisi`, b.`KdRekeningPersediaan`, (d.`Qty`*d.Harga) AS Nilai, 
					p.`coano`, h.Keterangan , d.Keterangan as KeteranganDetail, h.AddUser
					FROM trans_pengeluaran_lain h INNER JOIN trans_pengeluaran_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN intmutpurpose p ON h.`PurposeId`=p.`purposeid`
					INNER JOIN masterbarang b ON d.`PCode`=b.`PCode`
					inner join gudang g on h.KdGudang=g.KdGudang
					WHERE Year(TglDokumen)='$tahun' and Month(TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang<>'06'
					ORDER BY h.`NoDokumen`, d.Sid";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getcndnpembelian($tahun, $bulan){
		$sql = "SELECT h.`dnno`, h.`dndate`, h.`dntype`, h.`note`, d.`coano`, d.`description`, d.`value`, s.`Nama`, d.KdSubDivisi, h.KdRekening, h.VatPercent, h.adduser
				 FROM debitnote h INNER JOIN debitnotedtl d ON h.`dnno`=d.`dnno`
				INNER JOIN supplier s ON h.`supplierid`=s.`KdSupplier`
				WHERE YEAR(h.`dndate`)='$tahun' AND MONTH(h.`dndate`)='$bulan'  AND h.`status`=1 and `value`<>0
				order by h.dnno ";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getcndnpembelianEditJurnal($tahun, $bulan, $dnno){
		$sql = "SELECT h.`dnno`, h.`dndate`, h.`dntype`, h.`note`, d.`coano`, d.`description`, d.`value`, s.`Nama`, d.KdSubDivisi, h.KdRekening, h.VatPercent 
				 FROM debitnote h INNER JOIN debitnotedtl d ON h.`dnno`=d.`dnno`
				INNER JOIN supplier s ON h.`supplierid`=s.`KdSupplier`
				WHERE YEAR(h.`dndate`)='$tahun' AND MONTH(h.`dndate`)='$bulan'  AND h.`status`=1 and `value`<>0 AND h.`dnno`='$dnno'
				order by h.dnno ";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getcndnpenjualan($tahun, $bulan){
		$sql = "SELECT h.`cnno`, h.`cndate`, h.`cntype`, h.`note`, d.`coano`, d.`description`, d.`value`, s.`Nama`, d.KdSubDivisi, h.KdRekening, h.VatPercent,h.adduser,
		 		CONCAT(COALESCE(s.`Nama`,''),COALESCE(t.`Nama`,'')) as Nama 
				 FROM creditnote h INNER JOIN creditnotedtl d ON h.`cnno`=d.`cnno`
				LEFT JOIN customer s ON h.`KdCustomer`=s.`KdCustomer`
				LEFT JOIN tourtravel t on h.KdCustomer=t.KdTravel
				WHERE YEAR(h.`cndate`)='$tahun' AND MONTH(h.`cndate`)='$bulan'  AND h.`status`=1 and `value`<>0
				order by h.cnno ";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getproduksi($tahun, $bulan){
		$sql = "SELECT 'Mix' as Jenis, b.`KdRekeningPersediaan`, b.`KdSubDivisi`, SUM(m.`quantity`*m.`value`) AS Nilai, r.NamaRekening, p.adduser 
					FROM production p INNER JOIN productionmixing m ON p.`productionid`=m.`productionid`
					INNER JOIN masterbarang b ON m.`inventorycode`=b.`PCode`
					INNER JOIN rekening r ON b.`KdRekeningPersediaan`=r.`KdRekening`
					WHERE Year(productiondate)='$tahun' and Month(productiondate)='$bulan'
					GROUP BY b.`KdRekeningPersediaan`, b.`KdSubDivisi`
					UNION ALL
					SELECT 'Pack' as Jenis, b.`KdRekeningPersediaan`, b.`KdSubDivisi`, SUM(m.`quantity`*m.`value`) AS Nilai, r.NamaRekening, p.adduser 
					FROM production p INNER JOIN productionpackaging m ON p.`productionid`=m.`productionid`
					INNER JOIN masterbarang b ON m.`inventorycode`=b.`PCode`
					INNER JOIN rekening r ON b.`KdRekeningPersediaan`=r.`KdRekening`
					WHERE Year(productiondate)='$tahun' and Month(productiondate)='$bulan'
					GROUP BY b.`KdRekeningPersediaan`, b.`KdSubDivisi`";
		
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getrealisasi_UM($tahun, $bulan){
		$sql = "SELECT TglDokumen, NoDokumen, KdRekening, NamaRekening, KdSubDivisi, Keterangan, Debet, Kredit FROM(
					SELECT h.`TglDokumen`, d.`NoDokumen`, d.`KdRekening`, r.`NamaRekening`, d.`KdSubDivisi`, concat(d.`Keterangan`,'-',rdm.NoUMs) as Keterangan, d.`Jumlah` AS debet, 0 AS kredit , h.adduser
					FROM realisasi_uang_muka_payment d INNER JOIN realisasi_uang_muka h ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN rekening r ON d.`KdRekening`=r.`KdRekening`
					inner join(SELECT rd.NoDokumen, GROUP_CONCAT(rd.`NoUM`) as NoUMs FROM realisasi_uang_muka_detail rd 
						WHERE rd.`Realisasi`>0 
						GROUP BY rd.NoDokumen) rdm on h.NoDokumen=rdm.NoDokumen 
					WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' and h.Status=1
					UNION ALL
					SELECT h.`TglDokumen`, h.`NoDokumen`, i.`UMKaryawan` AS KdRekening, r.`NamaRekening` AS NamaRekening, '25' AS KdSubDivisi,CONCAT('Realisasi UM ',e.employee_name,' ',d.`NoUM`) AS Keterangan,
					0 AS Debet, d.`Realisasi` AS Kredit , h.adduser
					FROM realisasi_uang_muka_detail d INNER JOIN realisasi_uang_muka h ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN employee e ON h.`Employee`=e.employee_id
					INNER JOIN interface i
					INNER JOIN rekening r ON i.`UMKaryawan`=r.`KdRekening`
					WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' and h.Status=1
					) t ORDER BY TglDokumen, NoDokumen, Kredit";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	
	function getrealisasi_UM_Jurnal($tahun, $bulan, $no){
		$sql = "SELECT TglDokumen, NoDokumen, KdRekening, NamaRekening, KdSubDivisi, Keterangan, Debet, Kredit FROM(
					SELECT h.`TglDokumen`, d.`NoDokumen`, d.`KdRekening`, r.`NamaRekening`, d.`KdSubDivisi`, concat(h.`Keterangan`,'-',rdm.NoUMs) as Keterangan, d.`Jumlah` AS debet, 0 AS kredit 
					FROM realisasi_uang_muka_payment d INNER JOIN realisasi_uang_muka h ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN rekening r ON d.`KdRekening`=r.`KdRekening`
					inner join(SELECT rd.NoDokumen, GROUP_CONCAT(rd.`NoUM`) as NoUMs FROM realisasi_uang_muka_detail rd 
						WHERE rd.`Realisasi`>0 
						GROUP BY rd.NoDokumen) rdm on h.NoDokumen=rdm.NoDokumen 
					WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' and h.Status=1 AND h.`NoDokumen`='$no'
					UNION ALL
					SELECT h.`TglDokumen`, h.`NoDokumen`, i.`UMKaryawan` AS KdRekening, r.`NamaRekening` AS NamaRekening, '25' AS KdSubDivisi,CONCAT('Realisasi UM ',e.employee_name,' ',d.`NoUM`) AS Keterangan,
					0 AS Debet, d.`Realisasi` AS Kredit
					FROM realisasi_uang_muka_detail d INNER JOIN realisasi_uang_muka h ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN employee e ON h.`Employee`=e.employee_id
					INNER JOIN interface i
					INNER JOIN rekening r ON i.`UMKaryawan`=r.`KdRekening`
					WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' and h.Status=1 AND h.`NoDokumen`='$no'
					) t ORDER BY TglDokumen, NoDokumen, Kredit";
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}

	function getinvoice_sanitasi($tahun, $bulan){
		$sql = "SELECT 
				  i.`Tanggal`,i.`NoDokumen`,i.`Keterangan`,i.`Total_Nilai`,i.`Uang_Muka`,i.`AddUser`,i.`AddDate`,i.`Diskon_Nilai`,i.`Pembulatan`,i.`Total_DPP`,i.`PPN`,i.`PPH`,i.`PPH_Nilai`,i.`Total`,d.`Nilai` AS harga,d.`Keterangan` AS barang , t.`Nama` 
				FROM
				invoice_sanitasi i 
				inner join invoice_sanitasi_detail d 
					on d.`NoDokumen` = i.`NoDokumen` 
				inner join trans_reservasi r 
					on r.`NoDokumen` = i.`NoReservasi` 
				inner join tourtravel t 
					on t.`KdTravel` = r.`KdTravel` 
				WHERE YEAR(i.`Tanggal`) = '$tahun' 
				  AND MONTH(i.`Tanggal`) = '$bulan' 
				  AND i.Status = 1
				ORDER BY i.Tanggal,
				  i.NoDokumen";
                	
		$query = $this->db->query($sql);
		$row = $query->result_array();
		return $row;
	}
	function getNewNo($bulan,$tahun)
	{
		$sql = "Update countergl set Count$bulan=Count$bulan+1 where Tahun='$tahun'";
		$this->db->query($sql);
		$sql = "SELECT Count$bulan as counter FROM countergl where Tahun='$tahun'";
		return $this->getRow($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function cekdata($namatable,$where){
         //$returnvalue = 0;
		 //echo $namatable;
         $this->db->select('count(*) as jumlah');
         $this->db->from($namatable);
         $this->db->where($where);
         $result = $this->db->get();
	         foreach ($result->result() as $row) {
	                $returnvalue = $row->jumlah;                                 
	         }                               
		return $returnvalue;
    }
	function addData($parameter,$data){
		$this->db->insert($parameter,$data); 
	}
}
?>