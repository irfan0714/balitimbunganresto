<?php
class tutup_ticketing_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	function all_trans($tgl,$kasir)
	{
		$sql = "SELECT 
				  Tanggal,
				  CONCAT(USER, '-', employee_name) AS Kasir,
				  COUNT(NoTrans) AS TTLStruk,
				  SUM(Qty) AS TotalItem,
				  SUM(ttl_amount) AS TotalNilai,
				  SUM(ttl_bayar) AS TotalBayar,
				  SUM(ttl_kembali) AS Kembali,
				  SUM(nilai_tunai) AS Tunai,
				  SUM(nilai_kredit) AS KKredit,
				  SUM(nilai_debit) AS KDebit,
				  SUM(nilai_voucher) AS Voucher 
				FROM
				  ticket_head 
				  INNER JOIN employee 
				    ON employee.`username` = ticket_head.`user` 
				WHERE DATE_FORMAT(Tanggal, '%Y-%m-%d') = '".$tgl."' 
				  AND USER = '".$kasir."'
                  AND noidentitas != '1234' 
				GROUP BY USER";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	function total_trans($tgl,$kasir)
	{
		$sql = "SELECT 
				  SUM(IF(tipe = '1', qty, 0)) AS ticket_local,
				  SUM(IF(tipe = '2', qty, 0)) AS ticket_asing,
				  SUM(qty) AS total_ticket 
				FROM
				  ticket 
				WHERE add_date = '".$tgl."' 
				  AND `user` = '".$kasir."'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	function getLastDate()
	{
		$sql = "select TglTrans,Tahun from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
		return $this->getRow($sql);
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}
}
?>