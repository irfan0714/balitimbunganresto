<?php

class tutup_bulan_gl_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getPeriodeGL(){
		$sql = "select DATE_FORMAT(PeriodeGL,'%d-%m-%Y') as PeriodeGL from aplikasi order by Tahun desc limit 0,1";
		$rec = $this->getArrayResult($sql);
		return $rec[0]['PeriodeGL'];
	}
	
	function getBulanDepan(){
		$prdsekrang = $this->getPeriodeGL();
		list($tgl, $bln, $thn) = explode('-',$prdsekrang);
		$bln = $bln*1 + 1;
		if($bln>12){
			$bln ='1';
			$thn +=1;
		}
		if($bln<10){
			$bln = '0'.$bln;
		}
		$tgldepan = $thn.'-'.$bln.'-'.'01';
		return date("t-m-Y", strtotime($tgldepan));
	}
	
	function getBulanLalu(){
		$prdsekrang = $this->getPeriodeGL();
		list($tgl, $bln, $thn) = explode('-',$prdsekrang);
		$bln = $bln*1 - 1;
		if($bln==0){
			$bln ='12';
			$thn -=1;
		}
		if($bln<10){
			$bln = '0'.$bln;
		}
		$tgllalu = $thn.'-'.$bln.'-'.'01';
		return date("t-m-Y", strtotime($tgllalu));
	}
	
	function updateperiode($tgl){
		$sql = "update aplikasi set PeriodeGL='$tgl'";
		$this->db->query($sql);
	}
	
	function OtorisasiUser($userid){
		$sql = "select * from otorisasi_user where Tipe='buka_gl' and UserName='$userid'";
		$rec = $this->getArrayResult($sql);
		if(count($rec)>0)
			return 'Y';
		else
			return 'T';
	}
	
	function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>