<?php

class tutup_bulan_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
    }
    
    function getPeriode(){
		$sql = "select DATE_FORMAT(PeriodeKasBank,'%d-%m-%Y') as PeriodeSekarang from aplikasi order by Tahun desc limit 0,1";
		$rec = $this->getArrayResult($sql);
		return $rec[0]['PeriodeSekarang'];
	}
	
	function getBulanDepan(){
		$prdsekrang = $this->getPeriode();
		list($tgl, $bln, $thn) = explode('-',$prdsekrang);
		$bln = $bln*1 + 1;
		if($bln>12){
			$bln ='1';
			$thn +=1;
		}
		if($bln<10){
			$bln = '0'.$bln;
		}
		$tgldepan = $thn.'-'.$bln.'-'.'01';
		return date("t-m-Y", strtotime($tgldepan));
	}
	
	function getBulanLalu(){
		$prdsekrang = $this->getPeriode();
		list($tgl, $bln, $thn) = explode('-',$prdsekrang);
		$bln = $bln*1 - 1;
		if($bln==0){
			$bln ='12';
			$thn -=1;
		}
		if($bln<10){
			$bln = '0'.$bln;
		}
		$tgllalu = $thn.'-'.$bln.'-'.'01';
		return date("t-m-Y", strtotime($tgllalu));
	}
	
	function updateperiode($tgl){
		$sql = "update aplikasi set PeriodeKasBank='$tgl', PeriodeStock='$tgl'";
		$this->db->query($sql);
	}
	
	function OtorisasiUser($userid){
		$sql = "select * from otorisasi_user where Tipe='buka_tutup_bulan' and UserName='$userid'";
		$rec = $this->getArrayResult($sql);
		if(count($rec)>0)
			return 'Y';
		else
			return 'T';
	}
	
	function updatesaldokas($prdsekarang, $prdberikut){
    	list($thnsekarang, $blnsekarang, $tglsekarang) = explode('-',$prdsekarang);
    	list($thnberikut, $blnberikut, $tglberikut) = explode('-',$prdberikut);
    	
    	$blnsekarang=$blnsekarang*1;
    	
    	$sql = "Delete from saldo_bank where tahun='$thnsekarang' and bulan ='$blnsekarang' ";
    	$this->db->query($sql);
    	$this->UpdateSaldoAwalKas($thnsekarang,$blnsekarang);
    	$this->UpdateMutasiKas($thnsekarang,$blnsekarang);
	}
	
	function UpdateSaldoAwalKas($tahun, $bulan){
		$bulanlalu = $bulan;
		$tahunlalu = $tahun;
    	if($bulan==1){
			$bulanlalu=12;
			$tahunlalu -= 1;
		}else{
			$bulanlalu -= 1;
		}
		$sql = "Insert into saldo_bank(Tahun, Bulan, KdKasBank, Awal, Masuk, Keluar, Akhir) 
				SELECT '$tahun', '$bulan', KdKasBank, Akhir,0,0,Akhir FROM saldo_bank WHERE tahun='$tahunlalu' AND bulan='$bulanlalu'";	
		$this->db->query($sql);
	}
	
	function UpdateMutasiKas($tahun, $bulan){
		$sql = "SELECT KdKasBank, coalesce(sum(Debet),0) as Masuk, coalesce(sum(Kredit),0) as Keluar FROM (
				SELECT h.KdKasBank, 0 AS Debet, sum(h.`JumlahPayment`) AS Kredit FROM trans_payment_header h 
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`Status`<>'B' group by h.KdKasBank
				UNION ALL
				SELECT h.KdKasBank, sum(h.`JumlahReceipt`) AS Debet, 0 AS Kredit FROM trans_receipt_header h 
				WHERE YEAR(h.`TglDokumen`)='$tahun' AND MONTH(h.`TglDokumen`)='$bulan' AND h.`Status`<>'B' group by h.KdKasBank
				) t group by KdKasBank";
		
		$rec =  $this->getArrayResult($sql);
		foreach($rec as $mutasi){
			$kdkasbank = $mutasi['KdKasBank'];
			$masuk = $mutasi['Masuk'];
			$keluar = $mutasi['Keluar'];
			
			$saldo_bank_rec = "Select * from saldo_bank where tahun='$tahun' and Bulan='$bulan' and KdKasBank='$kdkasbank'";
			$hasil =  $this->getArrayResult($saldo_bank_rec);	
			if(!empty($hasil)){
				$sqlupdate = "Update saldo_bank set Masuk=Masuk+$masuk, Keluar=Keluar+$keluar, Akhir=Akhir+$masuk-$keluar 
								where tahun='$tahun' and Bulan='$bulan' and KdKasBank='$kdkasbank'";
			}else{
				$sqlupdate = "Insert into saldo_bank(Tahun, Bulan, KdKasBank, Awal, Masuk, Keluar, Akhir) 
							  values('$tahun', '$bulan', '$kdkasbank', 0,$masuk,$keluar,$masuk-$keluar)";
			}
			$this->db->query($sqlupdate);
		}
	}
	
	function HitungHutang($prdsekarang, $prdberikut){
    	list($thnsekarang, $blnsekarang, $tglsekarang) = explode('-',$prdsekarang);
    	list($thnberikut, $blnberikut, $tglberikut) = explode('-',$prdberikut);
    	
    	$blnsekarang=$blnsekarang*1;		
		$blnberikut = $blnberikut*1;
		
		//hapus dulu saldo awal bulan depan
		$sql = "delete from mutasi_hutang where Tahun='$thnberikut' and Bulan='$blnberikut' and TipeTransaksi in('O','OM')";
		$qry = $this->db->query($sql);
							
		$sql ="insert into mutasi_hutang  (Tahun, Bulan, NoDokumen, Tanggal, MataUang, Kurs, KdSupplier, TipeTransaksi, Jumlah)
			   SELECT '$thnberikut', '$blnberikut', NoDokumen, Tanggal, MataUang, Kurs, KdSupplier,  if(substring(NoDokumen,3,1)='M','OM','O'), sum(Jumlah)
					FROM(
						SELECT NoDokumen, Tanggal, MataUang, Kurs, KdSupplier, Jumlah 
						FROM mutasi_hutang 
						WHERE Tahun='$thnsekarang' AND Bulan='$blnsekarang' AND TipeTransaksi IN('O','I','C','OM','IM','CM')
										UNION ALL
						SELECT apd.NoFaktur, '', ap.MataUang, ap.Kurs, ap.KdSupplier, (apd.NilaiBayar)*-1 
						FROM mutasi_hutang ap INNER JOIN pelunasan_hutang_detail apd ON ap.NoDokumen=apd.NoTransaksi
						inner join pelunasan_hutang_header aph on apd.NoTransaksi=aph.NoTransaksi
						WHERE Tahun='$thnsekarang' AND Bulan='$blnsekarang' AND TipeTransaksi in('P','PM')  and aph.Status=1 
						and Year(aph.TglDokumen)='$thnsekarang' and month(aph.TglDokumen)='$blnsekarang'
										UNION ALL
						SELECT dnad.InvNo, '', ap.MataUang, ap.Kurs, ap.KdSupplier, dnad.Allocation*-1 
						FROM mutasi_hutang ap INNER JOIN dnallocation dna ON ap.NoDokumen=dna.DNNo
						INNER JOIN dnallocationdetail dnad ON dna.dnallocationno = dnad.dnallocationno
						WHERE Tahun='$thnsekarang' AND Bulan='$blnsekarang' AND TipeTransaksi in('D','DM') AND status=1 
						and Year(dna.dnadate)='$thnsekarang' and month(dna.dnadate)='$blnsekarang'
					) tbl
					GROUP BY NoDokumen, MataUang, KdSupplier HAVING SUM(Jumlah)<>0";
		$qry = $this->db->query($sql);
		
	}
	
	function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

}

?>