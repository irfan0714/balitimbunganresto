<?php
class tutup_pos_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	function aplikasi()
	{
		$sql = "select * from aplikasi";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	function FindUser($id)
	{
		$sql = "Select Id,UserName from user where Active='Y' and Id<>'$id'";
		return $this->getArrayResult($sql);
	}
	function FindNextDate($tahun,$interval)
	{
		$sql = "SELECT TglTrans,DATE_FORMAT(DATE_ADD(TglTrans,INTERVAL $interval DAY),'%d-%m-%Y') AS nextdate2,
		DATE_ADD(TglTrans,INTERVAL $interval DAY) AS nextdate FROM aplikasi WHERE Tahun='$tahun'";
		return $this->getRow($sql);
	}
	function cekOrder()
	{
		$sql = "SELECT * FROM sales_temp;";
		return $this->NumResult($sql);
	}
	function all_trans($tgl,$kasir)
	{
		    /*$sql = "SELECT Tanggal,CONCAT (Kasir,'-',employee_name) AS Kasir,COUNT(NoStruk) as TTLStruk,SUM(TotalItem) as TotalItem,SUM(TotalNilai) as TotalNilai,
                    SUM(TotalBayar)as TotalBayar,SUM(Kembali) as Kembali,SUM(Tunai) as Tunai,SUM(KKredit) as KKredit,
					SUM(KDebit)as KDebit,SUM(Voucher)as Voucher  
					FROM transaksi_header 
					INNER JOIN employee ON employee.`username`=transaksi_header.`Kasir`
					WHERE STATUS='1' AND Tanggal='$tgl'  GROUP BY Kasir ";
*/
		$sql = "SELECT DATE_FORMAT(Tanggal, '%Y-%m-%d') as Tanggal,CONCAT (Kasir,'-',COALESCE(employee_name,' ')) AS Kasir,COUNT(NoStruk) AS TTLStruk,SUM(TotalItem) AS TotalItem,SUM(TotalNilai) AS TotalNilai,
                    SUM(TotalBayar)AS TotalBayar,SUM(Kembali) AS Kembali,SUM(Tunai) AS Tunai,SUM(KKredit) AS KKredit,
					SUM(KDebit)AS KDebit,SUM(GoPay)AS GoPay, SUM(Voucher+VoucherTravel)AS Voucher, SUM(IF(`Valuta`='USD',`Valas`,0)) AS USD, SUM(IF(`Valuta`='CNY',`Valas`,0)) AS CNY 
					FROM transaksi_header 
					LEFT JOIN employee ON employee.`username`=transaksi_header.`Kasir`
					WHERE STATUS='1' AND Tanggal='$tgl'  AND Kasir='$kasir' GROUP BY Kasir"; 
		
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		
		return $row;
	}
	
	function all_trans_by_item_header($tgl,$kasir)
	{
		    $sql = "
			SELECT Tanggal,CONCAT (Kasir,'-',COALESCE(employee_name,' ')) AS Kasir,COUNT(NoStruk) AS TTLStruk,SUM(TotalItem) AS TotalItem,SUM(TotalNilai) AS TotalNilai,
                    SUM(TotalBayar)AS TotalBayar,SUM(Kembali) AS Kembali,SUM(Tunai) AS Tunai,SUM(KKredit) AS KKredit,
					SUM(KDebit)AS KDebit,SUM(Voucher)AS Voucher  
					FROM transaksi_header 
					LEFT JOIN employee ON employee.`username`=transaksi_header.`Kasir`
					WHERE STATUS='1' AND Tanggal='$tgl' AND Kasir='$kasir' GROUP BY Kasir
					
					UNION
					
			SELECT Tanggal,CONCAT (USER,'-',COALESCE(employee_name,' ')) AS Kasir,COUNT(NoTrans) AS TTLStruk,SUM(Qty) AS TotalItem,
					SUM(ttl_amount) AS TotalNilai,
                    SUM(ttl_bayar)AS TotalBayar,SUM(ttl_kembali) AS Kembali,SUM(nilai_tunai) AS Tunai,SUM(nilai_kredit) AS KKredit,
					SUM(nilai_debit)AS KDebit,SUM(nilai_voucher)AS Voucher
					 FROM ticket_head 
					 LEFT JOIN employee ON employee.`username`=ticket_head.`user`
					 WHERE DATE_FORMAT(Tanggal, '%Y-%m-%d')='$tgl'  AND user='$kasir' GROUP BY USER";
			
	/*	$sql ="	SELECT DATE_FORMAT(Tanggal, '%d-%m-%Y') AS Tanggal,Kasir,COUNT(NoStruk) as TTLStruk,SUM(TotalItem) as TotalItem,SUM(TotalNilai) as TotalNilai,
                    SUM(TotalBayar)as TotalBayar,SUM(Kembali) as Kembali,SUM(Tunai) as Tunai,SUM(KKredit) as KKredit,
					SUM(KDebit)as KDebit,SUM(Voucher)as Voucher  FROM transaksi_header WHERE STATUS='1' AND Tanggal='$tgl' GROUP BY Kasir "; */
		//echo $sql;
		return $this->getRow($sql);
	}

	function all_trans_by_item($tgl,$kasir)
	{
		$sql = "SELECT a.NoStruk,a.PCode,b.NamaLengkap,a.`Qty`,a.`Harga` FROM transaksi_detail a
				INNER JOIN masterbarang b
					ON a.`PCode` = b.`PCode`
				WHERE Tanggal='$tgl'  ORDER BY PCode";
				//echo $sql;
		return $this->getArrayResult($sql);
	}

	function kurs(){
		$sql ="SELECT SUM(IF(kd_uang='USD',NilaiTukar,0)) AS USD, SUM(IF(kd_uang='CNY',NilaiTukar,0)) AS CNY FROM mata_uang ";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row[0];
	}
	function NamaPrinter($id)
	{
		$sql = "SELECT * from kassa where ip='$id'";
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		return $row;
	}
	function getStokTerima($tahun,$field)
	{
		$sql = "select KodeBarang,Gudang,$field from stock where Tahun='$tahun'";
		return $this->getArrayResult($sql);
	}

	function getStokSimpan($tahun,$field)
	{
		$sql = "select KodeBarang,Gudang,$field from stock where Tahun='$tahun'";
		return $this->getArrayResult($sql);
	}
	function getAllAplikasi($tahun)
	{
		$sql = "select * from aplikasi where Tahun='$tahun'";//echo $sql;die();
		return $this->getRow($sql);
	}
	function getSetupNo($tahun)
	{
		$sql = "select * from setup_no where Tahun='$tahun'";
		return $this->getArrayResult($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->row();
		$qry->free_result();
		return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
		$row = $qry->result_array();
		$qry->free_result();
		return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
		$num = $qry->num_rows();
		$qry->free_result();
		return $num;
	}
	function getLastDate()
	{
		$sql = "select TglTrans,Tahun from aplikasi ORDER BY Tahun DESC LIMIT 0,1";
		return $this->getRow($sql);
	}
}
?>