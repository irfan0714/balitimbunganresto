<?php
class Otorisasi_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getorderList($num, $offset, $id, $with) {
        if ($offset != '') {
            $offset = $offset;
        } else {
            $offset = 0;
        }
        $clause = " where Status<>'B'";
        if ($id != "") {
            if ($with == "NoDokumen") {
                $clause = " where $with like '%$id%' and Status<>'B' and FlagOtorisasi='T'";
            } else {
                $clause = " where $with = '$id' and Status<>'B' and FlagOtorisasi='T'";
            }
        }
        $sql = "select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,h.Keterangan,FlagPengiriman,Nama,gudang.Keterangan as NamaGudang,
		        h.Jumlah,h.PPn,h.Total,FlagPengiriman,FlagTutup,TglKirim,FlagOtorisasi
		        from otorisasi_header h
				left join supplier on supplier.KdSupplier = h.KdSupplier
				left join gudang on gudang.KdGudang = h.KdGudang
				$clause 
				order by cast(NoDokumen as unsigned) desc Limit $offset,$num
			";
        return $this->getArrayResult($sql);
    }

    function getStatusOrder($id) {
        $sql = "SELECT COUNT(NoDokumen) AS Item,SUM(QtyPcsKirim>=QtyPcs) AS Status,
                SUM(QtyPcsKirim) AS Kirim FROM otorisasi_detail WHERE nodokumen='$id'";
        return $this->getRow($sql);
    }

    function num_order_row($id, $with) {
        $clause = " where Status<>'B'";
        if ($id != '') {
            if ($with == "NoDokumen") {
                $clause = " where $with like '%$id%' and Status<>'B'";
            } else {
                $clause = " where $with = '$id' and Status<>'B'";
            }
        }
        $sql = "select NoDokumen from otorisasi_header h
				left join supplier on supplier.KdSupplier = h.KdSupplier
				left join gudang on gudang.KdGudang = h.KdGudang
				$clause 
				order by cast(NoDokumen as unsigned) desc";
        return $this->NumResult($sql);
    }

    function getDate() {
        $sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }

    function getsupplierList() {
        $sql = "SELECT KdSupplier,NamaSupplier from supplier order by KdSupplier";
        return $this->getArrayResult($sql);
    }

    function getGudang() {
        $sql = "SELECT KdGudang,Keterangan as NamaGudang from gudang order by KdGudang";
        return $this->getArrayResult($sql);
    }

    function getSatuan($pcode) {
        $sql = "SELECT SatuanBl as Satuan0,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0
		        Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3
				from masterbarang where PCode='$pcode'";
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getsupplier($kode) {
        $sql = "SELECT KdSupplier,Nama,Alamat,Kota,Payment,TOP,LimitKredit,LimitFaktur,PPn,KdGroupext FROM supplier WHERE KdSupplier='$kode'";
        return $this->getRow($sql);
    }

    function getPCodeDet($kode, $kodeext) {
        $sql = "SELECT b.*,NamaBrand,NamaKategori,PCodeExt FROM(
				SELECT PCode,NamaLengkap,SatuanSt,Satuan1,Konv1st,Harga1b,Satuan2,Konv2st,Harga2b,Satuan3,Konv3st,Harga3b,
				KdKategori,KdBrand,PersenPajak as PPnB,
				SatuanBl as Satuan0,KonvBlSt as Konv0st,Harga0b,  
				(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
				(select NamaSatuan from satuan where KdSatuan=SatuanSt) as NamaSt,
				(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0
				FROM masterbarang WHERE pcode='$kode' and status='A'
				) b
				LEFT JOIN
				(
				SELECT KdBrand,NamaBrand FROM brand
				) br
				ON br.KdBrand=b.KdBrand
				LEFT JOIN
				(
				SELECT KdKategori,NamaKategori FROM kategori
				) kt
				ON kt.KdKategori=b.KdKategori
				LEFT JOIN
				(
				SELECT kodegrp,PCode,PCodeExt,NamaExt FROM kodeextdetail where kodegrp='$kodeext' and PCode='$kode'
				) ext				
				ON ext.PCode=b.PCode";
        return $this->getRow($sql);
    }

    function ifPCodeBarcode($id) {
        $bar = substr($id, 0, 10);
        $sql = "SELECT PCode,if(PCode='$id','',if(BarcodeSatuanBesar='$bar','B',if(BarcodeSatuanTengah='$bar','T','K'))) as Jenis FROM masterbarang Where PCode='$id' or BarcodeSatuanBesar='$bar' or BarcodeSatuanTengah='$bar' or BarcodeSatuanKecil='$bar'";
        return $this->getRow($sql);
    }

    function cekShare($kode, $kontak) {
        $sql = "select h.KdKetentuan
				from ketentuan_simpan_detail d,ketentuan_simpan h
				where PCode='$kode' and h.KdKetentuan=d.KdKetentuan and Pilihan='K' and h.KdSupplier<>'$kontak'
				and SharePCode='N';";
        return $this->NumResult($sql);
    }

    function getNewNo($tgl) {
        $tahun = substr($tgl, 0, 4);
        $bulan = substr($tgl, 5, 2);
        $sql = "Update counter set NoOrderBarang=NoOrderBarang+1 where Tahun='$tahun' and Bulan='$bulan'";
        $this->db->query($sql);
        $sql = "SELECT NoOrderBarang FROM counter where Tahun='$tahun' and Bulan='$bulan'";
        return $this->getRow($sql);
    }

    function getCounter($id) {
        $sql = "SELECT Counter FROM otorisasi_detail Where NoDokumen='$id' order by Counter desc limit 0,1";
        return $this->getRow($sql);
    }

    function getDetail($id) {
        $sql = "
		select d.PCode,NamaKategori,NamaBrand,NamaLengkap,QtyInput,QtyPcs,Satuan,Harga as HargaB,
		Disc1,Disc2,Potongan,KonversiSt,d.namasatuan as NamaSatuan,masterb.PCode as PCodeBarang,
		harga0b,harga1b,harga2b,harga3b,konv0st,konv1st,konv2st,konv3st,konversist as konversi,satuanst,d.namasatuan as nilsatuan,namasatuanst as nilsatuanst,d.kdkategori,d.kdbrand,
		Satuan0,Nama0,Satuan1,Nama1,Satuan2,Nama2,Satuan3,Nama3,PCodeExt,Jumlah as JumlahB,PPn as PPnB,Total as TotalB
		from(
		SELECT * from otorisasi_detail Where NoDokumen='$id' order by Counter
		) d
		left join
		(
		   select PCode,NamaLengkap,harga0b,harga1b,harga2b,harga3b,KonvBlSt as konv0st,konv1st,konv2st,konv3st,
				Satuan1,(select NamaSatuan from satuan where KdSatuan=Satuan1) as Nama1,
				Satuan2,(select NamaSatuan from satuan where KdSatuan=Satuan2) as Nama2,
				Satuan3,(select NamaSatuan from satuan where KdSatuan=Satuan3) as Nama3,
                SatuanBl as Satuan0,(select NamaSatuan from satuan where KdSatuan=SatuanBl) as Nama0 from masterbarang
		) masterb
		on masterb.PCode = d.PCode 
		left join
		(
		select KdSatuan,NamaSatuan from satuan
		) satu
		on satu.KdSatuan = d.Satuan
		left join
		(
		select KdKategori,NamaKategori from kategori
		) dua
		on dua.KdKategori = d.KdKategori
		left join
		(
		select KdBrand,NamaBrand from brand
		) tiga
		on tiga.KdBrand = d.KdBrand";
        return $this->getArrayResult($sql);
    }

    function getDetailForPrint($id) {
        $sql = "
		select d.PCode,QtyInput,QtyPcs,Satuan,NamaInitial,Harga,NamaSatuan,KonversiSt,Potongan,PCodeExt,Jumlah,PPn,Total from(
		SELECT * from otorisasi_detail Where NoDokumen='$id' order by Counter
		) d
		left join
		(
			select PCode,NamaStruk as NamaInitial,Satuan1,Satuan2,Satuan3 from masterbarang
		) masterb
		on masterb.PCode = d.PCode
		left join 
		(
			select KdSatuan,NamaSatuan from satuan
		)sak
		on sak.KdSatuan = Satuan";
        return $this->getArrayResult($sql);
    }

    function getHeader($id) {
        $sql = "SELECT
			NoDokumen,DATE_FORMAT(TglDokumen,'%d-%m-%Y') AS Tanggal,Nama,
			h.KdGudang,h.Keterangan,FlagPengiriman,FlagTutup,DATE_FORMAT(TglKirim,'%d-%m-%Y') AS TglKirim,Kota,DiscHarga,
			h.KdSupplier, h.Jumlah, c.PPn, h.NilaiPPn, h.Total, h.Payment, h.TOP, h.KdGroupext,
			p.Keterangan AS NamaGudang,
			c.Contact AS kontak, c.Alamat AS Alamat, c.Telepon AS TelpSupp
			FROM
			(SELECT * FROM otorisasi_header Where NoDokumen='$id')h
			INNER JOIN supplier c ON c.KdSupplier = h.KdSupplier 
			INNER JOIN gudang p ON p.KdGudang=h.KdGudang";

        return $this->getRow($sql);
    }

    function locktables($table) {
        $this->db->simple_query("LOCK TABLES $table");
    }

    function unlocktables() {
        $this->db->simple_query("UNLOCK TABLES");
    }

    function getRow($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
    }

    function getArrayResult($sql) {
        $qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

    function NumResult($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
    }

    function getalamat() { //wsc
        $sql = "SELECT Alamat1PT as Alamat, TelpPT FROM aplikasi";
        return $this->getRow($sql);
    }

    function getExtCode($kdsupplier, $pcode) { //wsc 211014
        $sql = "SELECT b.`PCodeExt`,b.`NamaExt`,c.`Pcode` FROM `kodeextheader` a, `kodeextdetail` b, `masterbarang` c, `supplier` d WHERE a.`kodegrp`=b.`kodegrp` AND b.`PCode`=c.`PCode` AND b.`kodegrp`=d.`KdGroupext` AND d.`KdSupplier`='$kdsupplier' AND c.`PCode`='$pcode'";
        return $this->getrow($sql);
        //return $this->getArrayResult($sql);
    }

    function getData($sql) {
        //	 echo "<pre>$sql</pre>";die;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getCount($sql) {
        $qry = $this->db->query($sql);
        $num = $qry->num_rows();
        return $num;
    }

}

?>