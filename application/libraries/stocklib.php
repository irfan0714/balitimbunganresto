<?php
/*
Note : stok NPM konversi saldo awal qty dan value di akhir bulan Jun 2017 via data opname Jun 2017
*/
class stocklib {
	var $CI;
	function __construct(){
	    $this->CI =& get_instance();
	 	$this->CI->load->library('session');
		$this->CI->load->model('Globalmodel');
	}

	function update_stock($tgl, $notrans, $gudang, $kdbarang, $qty, $nilai, $kdtransaksi, $jenis){
		$qty = is_null($qty) ? 0 : $qty;
		$nilai = is_null($nilai) ? 0 : $nilai;

		$this->_insert_mutasi($tgl, $notrans, $gudang, $kdbarang, $qty, $nilai,$kdtransaksi, $jenis );
		$this->_update_stock($tgl, $gudang, $kdbarang, $qty, $nilai, $jenis);
	}

	function _insert_mutasi($tgl, $notrans, $gudang, $kdbarang, $qty, $nilai, $kdtransaksi, $jenis){
		if($jenis=='I'){
			$gudangdari = '';
			$gudangtujuan = $gudang;
		}
		else{
			$gudangdari = $gudang;
			$gudangtujuan = '';
		}
		$data = array(
                    'NoTransaksi' => $notrans ,
                    'Jenis' => $jenis,
                    'KdTransaksi' => $kdtransaksi,
                    'Gudang' => $gudang,
                    'GudangTujuan' => $gudang,
                    'Tanggal' => $tgl,
                    'KodeBarang' => $kdbarang,
                    'Qty' => $qty,
                    'Nilai' => $nilai,
                    'Status' =>1
                );

        $this->CI->db->insert('mutasi', $data);
	}

	function _update_stock($tgl, $gudang, $kdbarang, $qty, $nilai, $jenis){
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;
        $nilai = $qty * $nilai;
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $bulan;
			$fieldnupdate = "GNKeluar" . $bulan;
			$vqty = $qty*-1;
			$vnilai = $nilai*-1;
		}else{
			$fieldupdate = "GMasuk" .$bulan;
			$fieldnupdate = "GNMasuk" .$bulan;
			$vqty = $qty;
			$vnilai = $nilai;
		}

		if($kdbarang=="25303180001"){
			$gudang="11";
		}

        $sql = "select $fieldakhir, $fieldnakhir from stock where Tahun='$tahun' and KdGudang='$gudang' and PCode='$kdbarang' and Status='G'";
        $query = $this->CI->db->query($sql);
		$result = $query->result_array();
		if(count($result)>0){
			$sql2 = "Update stock set $fieldupdate=$fieldupdate+$qty,$fieldnupdate=$fieldnupdate+$nilai, $fieldakhir=$fieldakhir+$vqty,$fieldnakhir=$fieldnakhir+$vnilai
						where Tahun='$tahun' and KdGudang='$gudang' and PCode='$kdbarang' and Status='G'";

			$query = $this->CI->db->query($sql2);
		}else{
			$data = array(
					'Tahun' => $tahun,
		            'KdGudang' => $gudang,
		            'PCode' => $kdbarang,
		            'Status' => 'G',
		            $fieldupdate => $qty,
		            $fieldnupdate => $nilai,
		            $fieldakhir => $vqty,
		            $fieldnakhir => $vnilai
		        );
			$this->CI->db->insert('stock', $data);
		}
	}

	function recalculate_stock_bulanan($tahun, $bulan){
		$bulan = $bulan*1;
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$awalbulan = $tahun.'-'.$vbulan.'-01';
		$tglakhir = date('Y-m-t', strtotime($awalbulan));


		$this->_update_saldo_awal($tahun, $vbulan);
		print 'data:Saldo Awal' . PHP_EOL . PHP_EOL;
		
		$this->_delete_mutasi($tahun, $bulan);
		print 'data:Delete Mutasi' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_update_stock_price_awal($tahun, $vbulan);
		print 'data:Saldo Price' . PHP_EOL . PHP_EOL;		
		
		$this->_Pembelian($tahun, $bulan);
		print 'data:Pembelian' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_update_stock_price($tahun, $vbulan);
		print 'data:Saldo Price' . PHP_EOL . PHP_EOL;		
		
		$this->_PenerimaanLain($tahun, $bulan);
		print 'data:Penerimaan Lain ' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_ReturJual($tahun, $bulan);
		print 'data:Retur Penjualan' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_MutasiPaket($tahun, $bulan);
		print 'data:Mutasi Paket' . PHP_EOL . PHP_EOL;
		flush();	
		
		$this->_update_stock_price($tahun, $vbulan);
		print 'data:Saldo Price' . PHP_EOL . PHP_EOL;	
		
		$this->_MutasiGudang($tahun, $bulan);
		print 'data:Mutasi Gudang' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_update_stock_price($tahun, $vbulan);
		print 'data:Saldo Price' . PHP_EOL . PHP_EOL;	
		
		$this->_PengeluaranLain($tahun, $bulan);
		print 'data:Pengeluaran Lain' . PHP_EOL . PHP_EOL;
		flush();	
		
		$this->_Mixing($tahun, $bulan);
		print 'data:Mixing' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_Packaging($tahun, $bulan);
		print 'data:Packaging' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_hitunghpp($tahun,$bulan);
		print 'data:HPP' . PHP_EOL . PHP_EOL;
		flush();
			
		$this->_FinishGoods($tahun, $bulan);
		print 'data:Finish Goods' . PHP_EOL . PHP_EOL;
		flush();	
		
		$this->_update_stock_price($tahun, $vbulan);
		print 'data:Saldo Price' . PHP_EOL . PHP_EOL;		
		
		$this->_MutasiGudangProduksi($tahun, $bulan);
		print 'data: Mutasi Gudang Produksi' . PHP_EOL . PHP_EOL;
		flush();	
		
		$this->_Penjualan($tahun, $bulan);
		print 'data: Penjualan' . PHP_EOL . PHP_EOL;
		flush();	
					
		$this->_DeliveryOrder($tahun, $bulan);
		print 'data: Delivery Order' . PHP_EOL . PHP_EOL;
		flush();		
		
		$this->_ReturBeli($tahun, $bulan);
		print 'data: Retur Beli' . PHP_EOL . PHP_EOL;
		flush();		
			
		$this->_Opname($tahun, $bulan);
		print 'data: Opname' . PHP_EOL . PHP_EOL;
		flush();
		
		$this->_adjustment($tahun,$bulan);
		print 'data:Adjustment ............Done' . PHP_EOL . PHP_EOL;
		flush();
		
	}

	function _update_saldo_awal($tahun, $bulan){
		$vbulan = $bulan*1;
		if($vbulan>1){
			$bulanlalu = $vbulan-1;
			$tahunlalu = $tahun;
		}
		else{
			$bulanlalu = 12;
			$tahunlalu = $tahun-1;
		}
		$bulanlalu = $bulanlalu < 10 ? '0' . $bulanlalu : $bulanlalu;

		$fieldakhirlalu = "GAkhir" . $bulanlalu;
		$fieldnakhirlalu = "GNAkhir" . $bulanlalu;
		$fieldawal = "GAwal" . $bulan;
		$fieldnawal = "GNAwal" . $bulan;
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;

		$fieldgmasuk = "GMasuk" . $bulan;
		$fieldgnmasuk = "GNMasuk" . $bulan;
		$fieldkeluar = "GKeluar" . $bulan;
		$fieldgnkeluar = "GNKeluar" . $bulan;
		
		if($vbulan>1){
			if($tahun=='2017' && $bulan=='07'){
				$sql = "Update stock set GAwal07=0, GNAwal07=0,GMasuk07=0,GNMasuk07=0,
			        GKeluar07=0, GNKeluar07=0, GAkhir07=0,GNAkhir07=0
			         where Tahun='$tahun'";
			    $this->CI->db->query($sql);

			    $sql2 = "UPDATE opname_detail d INNER JOIN opname_header h ON d.`NoDokumen`=h.`NoDokumen`
							INNER JOIN stock t ON h.`KdGudang`=t.`KdGudang` AND d.`PCode`=t.`PCode` AND t.`Tahun`=2017
							SET t.GAwal07 = d.QtyFisik, t.GNAwal07 = d.QtyFisik*d.`HargaSatuan`,
							t.GAkhir07= d.QtyFisik,t.GNAkhir07=d.QtyFisik*d.`HargaSatuan`
							WHERE h.`TglDokumen`='2017-06-30' AND h.`Status`=1 AND h.`Approval_Status`=1";
				$this->CI->db->query($sql2);
			}else{
				$sql = "Update stock set $fieldawal=$fieldakhirlalu, $fieldnawal=$fieldnakhirlalu,$fieldgmasuk=0,$fieldgnmasuk=0,
			        $fieldkeluar=0, $fieldgnkeluar=0, $fieldakhir=$fieldakhirlalu,$fieldnakhir=$fieldnakhirlalu
			         where Tahun='$tahun'";
			    $this->CI->db->query($sql);
			}
		}else{
			$sql1 = "Delete from stock where tahun='$tahun'";
			$sql2 = "insert into stock (Tahun, KdGudang, PCode, Status, $fieldawal, $fieldnawal,$fieldakhir,$fieldnakhir)
					select $tahun, KdGudang, PCode, Status, $fieldakhirlalu, $fieldnakhirlalu, $fieldakhirlalu, $fieldnakhirlalu from stock where Tahun='$tahunlalu'";
			$this->CI->db->query($sql1);
			$this->CI->db->query($sql2);
		}
	}
	
	function _update_stock_price_awal($tahun, $bulan){
		$vbulan = $bulan*1;
		if($vbulan>1){
			$bulanlalu = $vbulan-1;
			$tahunlalu = $tahun;
		}
		else{
			$bulanlalu = 12;
			$tahunlalu = $tahun-1;
		}
		$bulanlalu = $bulanlalu < 10 ? '0' . $bulanlalu : $bulanlalu;

		$fieldakhirlalu = "GAkhir" . $bulanlalu;
		$fieldnakhirlalu = "GNAkhir" . $bulanlalu;
		$fieldawal = "GAwal" . $bulan;
		$fieldnawal = "GNAwal" . $bulan;
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;

		$fieldgmasuk = "GMasuk" . $bulan;
		$fieldgnmasuk = "GNMasuk" . $bulan;
		$fieldkeluar = "GKeluar" . $bulan;
		$fieldgnkeluar = "GNKeluar" . $bulan;
		
		$sql1 = "Delete from stock_price where tahun='$tahun' and bulan='$vbulan'";
		
		$sql2 = "insert into stock_price (Tahun, Bulan, KdDivisi, PCode, Price)
				select $tahun, $vbulan, KdDivisi, PCode, Price from stock_price where Tahun='$tahunlalu' and Bulan='$bulanlalu'";
				
		$this->CI->db->query($sql1);
		$this->CI->db->query($sql2);
	}
	
	function _update_stock_price($tahun, $bulan){
		$vbulan = $bulan*1;
		if($vbulan>1){
			$bulanlalu = $vbulan-1;
			$tahunlalu = $tahun;
		}
		else{
			$bulanlalu = 12;
			$tahunlalu = $tahun-1;
		}
		$bulanlalu = $bulanlalu < 10 ? '0' . $bulanlalu : $bulanlalu;

		$fieldakhirlalu = "GAkhir" . $bulanlalu;
		$fieldnakhirlalu = "GNAkhir" . $bulanlalu;
		$fieldawal = "GAwal" . $bulan;
		$fieldnawal = "GNAwal" . $bulan;
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;

		$fieldgmasuk = "GMasuk" . $bulan;
		$fieldgnmasuk = "GNMasuk" . $bulan;
		$fieldkeluar = "GKeluar" . $bulan;
		$fieldgnkeluar = "GNKeluar" . $bulan;
				
		$sql3 = "insert into stock_price (Tahun, Bulan, KdDivisi, PCode, Price)
				select $tahun, $vbulan, KdDivisi, PCode, Harga from (
				 select KdDivisi, PCode, coalesce(abs(Nilai/Qty),0) as Harga from (
				 select sd.KdDivisi, s.PCode, sum($fieldnakhir) as Nilai, sum($fieldakhir) as Qty  from stock s 
				 inner join gudang g on s.KdGudang = g.KdGudang
				 inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi
				 where Tahun='$tahun' and s.KdGudang not in('06','13','15')
				 group by sd.KdDivisi, PCode) a where Qty<>0) d 
				 ON DUPLICATE KEY UPDATE Price=d.Harga";
		
		$this->CI->db->query($sql3);
	}
	
	function _delete_mutasi($tahun, $bulan){
		$sql = "Delete from mutasi where year(Tanggal)='$tahun' and Month(Tanggal)='$bulan'";
		$this->CI->db->query($sql);
	}

	function _Pembelian($tahun, $bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'I';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}

		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
				SELECT h.`NoDokumen`,'I' as Jenis, 'RG' as KdTransaksi, d.`KdGudang`, d.`KdGudang`, h.`TglDokumen`, PCode, QtyPcs, Harga/(QtyPcs/Qty) as Harga, 0, 1 
				FROM trans_terima_header h INNER JOIN trans_terima_detail d ON h.`NoDokumen`=d.`NoDokumen`
				WHERE year(h.TglDokumen)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1
				ORDER BY h.`NoDokumen`, Sid";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(h.TglDokumen) as Tahun, d.`KdGudang`, PCode, 'G' as Status, sum(QtyPcs) as Qty1, sum(Harga*Qty) as Nilai1, sum(QtyPcs) as Qty2, sum(Harga*Qty) as Nilai2
							FROM trans_terima_header h INNER JOIN trans_terima_detail d ON h.`NoDokumen`=d.`NoDokumen`
							WHERE year(h.TglDokumen)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1
							group by d.KdGudang, PCode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";
		$query = $this->CI->db->query($sqlstock);			
	}

	function _PenerimaanLain($tahun, $bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'I';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "update trans_penerimaan_lain h INNER JOIN trans_penerimaan_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
				inner join gudang g on g.KdGudang = h.KdGudang 
				inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi
				inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.Harga = s.Price
				WHERE year(TglDokumen)='$tahun' and month(TglDokumen)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					select h.NoDokumen,'I','DL',h.KdGudang,h.KdGudang,h.TglDokumen,d.PCode, d.QtyPCs, d.Harga,0,1 
					from trans_penerimaan_lain h INNER JOIN trans_penerimaan_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
					WHERE year(TglDokumen)='$tahun' and month(TglDokumen)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2, if(Qty1>0,Nilai1/Qty1,0) from (
					select year(h.TglDokumen) as Tahun,h.KdGudang, d.PCode, 'G' as Status, sum(d.QtyPCs) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.QtyPCs) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2 
					from trans_penerimaan_lain h INNER JOIN trans_penerimaan_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
					WHERE year(TglDokumen)='$tahun' and month(TglDokumen)='$bulan' AND h.`Status`=1
					group by h.KdGudang, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _MutasiGudang($tahun,$bulan){
		//mutasi selain gudang produksi. Untuk gudang produksi dihitung setelah hitung hpp
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$fieldkeluar = "GKeluar" . $vbulan;
		$fieldnkeluar = "GNKeluar" . $vbulan;
		$fieldmasuk = "GMasuk" .$vbulan;
		$fieldnmasuk = "GNMasuk" .$vbulan;
		
		$sql = "Update
					trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					inner join gudang g on g.KdGudang = h.KdGudang_From 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi
					inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
					set d.Harga=s.Price
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and (h.KdGudang_From<>'05' and h.KdGudang_From<>'01')";
		$query = $this->CI->db->query($sql);
		
		//out
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`,'O','MM',h.`KdGudang_From`, h.`KdGudang_To`, h.TglDokumen, d.`PCode`, d.`QtyPcs`, d.Harga, 0, 1
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and (h.KdGudang_From<>'05' and h.KdGudang_From<>'01')";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldkeluar,$fieldnkeluar,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2, if(Qty1>0,Nilai1/Qty1,0) from (
					SELECT year(h.TglDokumen) as Tahun, KdGudang_From as KdGudang, PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and (h.KdGudang_From<>'05' and h.KdGudang_From<>'01')
					group by KdGudang_From, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldkeluar=$fieldkeluar+a.Qty1 ,$fieldnkeluar=$fieldnkeluar+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
		
		//in
		$sqlmutasi1 = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`,'I','MC',h.`KdGudang_To`, h.`KdGudang_To`, h.TglDokumen, d.`PCode`, d.`QtyPcs`, d.Harga, 0, 1
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and (h.KdGudang_From<>'05' and h.KdGudang_From<>'01')
					and MovingConfirmation=1";
		$query = $this->CI->db->query($sqlmutasi1);
		
		$sqlstock1 = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldmasuk,$fieldnmasuk,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2, if(Qty1>0,Nilai1/Qty1,0) from (
					SELECT year(h.TglDokumen) as Tahun, KdGudang_To as KdGudang, PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and (h.KdGudang_From<>'05' and h.KdGudang_From<>'01') and MovingConfirmation=1
					group by KdGudang_To, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldmasuk=$fieldmasuk+a.Qty1,$fieldnmasuk=$fieldnmasuk+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";
		$query = $this->CI->db->query($sqlstock1);		
	}

	function _MutasiPaket($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$fieldkeluar = "GKeluar" . $vbulan;
		$fieldnkeluar = "GNKeluar" . $vbulan;
		$fieldmasuk = "GMasuk" .$vbulan;
		$fieldnmasuk = "GNMasuk" .$vbulan;
		
		// in		
		$sqlmutasi1 = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
						SELECT m.`id_mutasi`, if(m.Jenis_mutasi='B','I','O'),'MP', m.`KdGudang`,m.`KdGudang`,DATE(Tanggal), b.`MPCode`, avg(m.Qty), sum(b.Qty*s.Price), 0, 1 
						FROM mutasi_paket m
						INNER JOIN barang_paket b ON m.MPCode = b.`MPCode`
						inner join gudang g on g.KdGudang = m.KdGudang 
						inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi
						inner join stock_price s on s.PCode=b.DPCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
						WHERE year(tanggal)='$tahun' and month(tanggal)='$bulan' group by m.id_mutasi";
		$query = $this->CI->db->query($sqlmutasi1);
		
		//out
		$sqlmutasi2 = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
						SELECT m.`id_mutasi`, if(m.Jenis_mutasi='B','O','I'),'MP', m.`KdGudang`,m.`KdGudang`,DATE(Tanggal), b.`DPCode`, b.Qty*m.Qty, s.Price, 0, 1 
						FROM mutasi_paket m
						INNER JOIN barang_paket b ON m.MPCode = b.`MPCode`
						inner join gudang g on g.KdGudang = m.KdGudang 
						inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
						inner join stock_price s on s.PCode=b.DPCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
						WHERE year(tanggal)='$tahun' and month(tanggal)='$bulan'";
		$query = $this->CI->db->query($sqlmutasi2);
		
		//in				
		$sqlstock1 = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldmasuk,$fieldnmasuk,$fieldkeluar,$fieldnkeluar,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, 'G' as Status, if(Jenis_Mutasi='B',Qty,0) as QtyMasuk, if(Jenis_Mutasi='B',NilaiDetail,0) as NilaiMasuk, 
					if(Jenis_Mutasi<>'B',Qty,0) as QtyKeluar, if(Jenis_Mutasi<>'B',NilaiDetail,0) as NilaiKeluar, 
					if(Jenis_Mutasi='B', Qty, -Qty) as QtyAkhir, if(Jenis_Mutasi='B', NilaiDetail, -NilaiDetail) as NilaiAkhir, 
					if(Qty>0, NilaiDetail/Qty,0) from (
					SELECT YEAR(Tanggal) AS Tahun, m.`id_mutasi`, m.Jenis_mutasi, m.`KdGudang`,Tanggal, b.`MPCode` as PCode, avg(m.Qty) as Qty, sum(m.Qty*b.Qty*s.Price) as NilaiDetail 
					FROM mutasi_paket m
					INNER JOIN barang_paket b ON m.MPCode = b.`MPCode` 
					inner join gudang g on g.KdGudang = m.KdGudang 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
					inner join stock_price s on s.PCode=b.DPCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
					WHERE year(tanggal)='$tahun' and month(tanggal)='$bulan' group by m.id_mutasi) a
					ON DUPLICATE KEY UPDATE 
					$fieldmasuk=$fieldmasuk+if(a.Jenis_Mutasi='B',a.Qty,0), $fieldnmasuk=$fieldnmasuk+if(a.Jenis_Mutasi='B',a.NilaiDetail,0),
					$fieldkeluar=$fieldkeluar+if(a.Jenis_Mutasi<>'B',a.Qty,0), $fieldnkeluar=$fieldnkeluar+if(a.Jenis_Mutasi<>'B',a.NilaiDetail,0),
					$fieldakhir=$fieldakhir+if(a.Jenis_Mutasi='B',Qty,-Qty), $fieldnakhir=$fieldnakhir+if(a.Jenis_Mutasi='B',a.NilaiDetail,-a.NilaiDetail)";
		$query = $this->CI->db->query($sqlstock1);		
		
		//out
		$sqlstock2 = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldmasuk,$fieldnmasuk,$fieldkeluar,$fieldnkeluar,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, QtyMasuk, NilaiMasuk, QtyKeluar, NilaiKeluar, QtyAkhir, NilaiAkhir, if(QtyAkhir>0,NilaiAkhir/QtyAkhir, 0) from (
					SELECT YEAR(Tanggal) AS Tahun, m.`KdGudang`,b.`DPCode` AS PCode, 'G' AS STATUS, SUM(IF(Jenis_mutasi<>'B',m.`Qty`*b.Qty,0)) AS QtyMasuk, SUM(IF(Jenis_mutasi<>'B',m.Qty*b.Qty*s.Price,0)) AS NilaiMasuk,
					SUM(IF(Jenis_mutasi='B',m.`Qty`*b.Qty,0)) AS QtyKeluar, SUM(IF(Jenis_mutasi='B',m.Qty*b.Qty*s.Price,0)) AS NilaiKeluar,
					SUM(IF(Jenis_mutasi<>'B',m.`Qty`*b.Qty,m.Qty*b.Qty-1)) AS QtyAkhir, SUM(IF(Jenis_mutasi<>'B',m.Qty*b.Qty*s.Price,m.Qty*b.Qty*s.Price*-1)) AS NilaiAkhir 
					FROM mutasi_paket m
					INNER JOIN barang_paket b ON m.MPCode = b.`MPCode` 
					inner join gudang g on g.KdGudang = m.KdGudang 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi
					INNER JOIN stock_price s ON s.PCode=b.DPCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
					WHERE YEAR(tanggal)='$tahun' AND MONTH(tanggal)='$bulan'
					GROUP BY m.KdGudang,b.DPCode) a
					ON DUPLICATE KEY UPDATE $fieldmasuk=$fieldmasuk+a.QtyMasuk, $fieldnmasuk=$fieldnmasuk+a.NilaiMasuk,
					$fieldkeluar=$fieldkeluar+a.QtyKeluar, $fieldnkeluar=$fieldnkeluar+a.NilaiKeluar,
					$fieldakhir=$fieldakhir+a.QtyMasuk-a.QtyKeluar, $fieldnakhir=$fieldnakhir+a.NilaiMasuk-a.NilaiKeluar";
		$query = $this->CI->db->query($sqlstock2);		
		
		
	}

	function _ReturJual($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'I';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "Update
					salesreturn h INNER JOIN salesreturndetail d ON h.`returnno`=d.`returnno`
					inner join gudang g on g.KdGudang = h.warehousecode 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
					inner join stock_price s on s.PCode=d.InventoryCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.hppprice=s.Price
				WHERE year(h.`returndate`)='$tahun' and month(h.ReturnDate)='$bulan' AND h.`status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`returnno`, 'I', 'SR',h.`warehousecode`,h.`warehousecode`,h.`returndate`,d.`inventorycode`, d.`quantity`, d.`hppprice`,0,1 
					from salesreturn h INNER JOIN salesreturndetail d ON h.`returnno`=d.`returnno`
					WHERE year(h.`returndate`)='$tahun' and month(h.ReturnDate)='$bulan' AND h.`status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2, if(Qty1>0,Nilai1/Qty1,0) from (
					SELECT year(h.returndate) as Tahun,h.`warehousecode` as KdGudang,d.`inventorycode` as PCode,'G' as Status, sum(d.`quantity`) as Qty1, sum(d.quantity*d.`hppprice`) as Nilai1,sum(d.`quantity`) as Qty2, sum(d.quantity*d.`hppprice`) as Nilai2 
					from salesreturn h INNER JOIN salesreturndetail d ON h.`returnno`=d.`returnno`
					WHERE year(h.`returndate`)='$tahun' and month(h.ReturnDate)='$bulan' AND h.`status`=1
					group by h.warehousecode, d.inventorycode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _PengeluaranLain($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'O';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "update trans_pengeluaran_lain h INNER JOIN trans_pengeluaran_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
				inner join gudang g on g.KdGudang = h.KdGudang 
				inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
				inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.Harga = s.Price
				WHERE year(TglDokumen)='$tahun' and month(TglDokumen)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					select h.NoDokumen,'O','PL',h.KdGudang,h.KdGudang,h.TglDokumen,d.PCode, d.QtyPCs, d.Harga,0,1 
					from trans_pengeluaran_lain h INNER JOIN trans_pengeluaran_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
					WHERE year(TglDokumen)='$tahun' and month(TglDokumen)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir,LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2,if(Qty2>0,Nilai2/Qty2,0) from (
					select year(h.TglDokumen) as Tahun,h.KdGudang, d.PCode, 'G' as Status, sum(d.QtyPCs) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.QtyPCs) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2 
					from trans_pengeluaran_lain h INNER JOIN trans_pengeluaran_lain_detail d ON h.`NoDokumen`=d.`NoDokumen`
					WHERE year(TglDokumen)='$tahun' and month(TglDokumen)='$bulan' AND h.`Status`=1
					group by h.KdGudang, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _Mixing($tahun, $bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'O';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "update production h INNER JOIN productionmixing m ON h.`productionid`=m.`productionid` and h.Status=1
				inner join gudang g on g.KdGudang = h.warehousecode 
				inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
				inner join stock_price s on s.PCode=m.inventorycode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set m.value = s.Price
				WHERE year(h.mixdate)='$tahun' and month(h.mixdate)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.batchnumber, 'O', 'MX', h.warehousecode, h.warehousecode, h.`mixdate`, m.`inventorycode`, m.`quantity`, m.`value`, 0, 1 
					FROM production h INNER JOIN productionmixing m ON h.`productionid`=m.`productionid` and h.Status=1
					WHERE year(h.mixdate)='$tahun' and month(h.mixdate)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(h.mixdate) as Tahun, h.warehousecode as KdGudang, m.`inventorycode` as PCode, 'G' as Status, sum(m.`quantity`) as Qty1, sum(m.quantity*m.`value`) as Nilai1, sum(m.`quantity`) as Qty2, sum(m.quantity*m.`value`) as Nilai2 
					FROM production h INNER JOIN productionmixing m ON h.`productionid`=m.`productionid` and h.Status=1
					WHERE year(h.mixdate)='$tahun' and month(h.mixdate)='$bulan' AND h.`Status`=1
					group by h.warehousecode, m.inventorycode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _Packaging($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'O';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "update production h INNER JOIN productionpackaging m ON h.`productionid`=m.`productionid` and h.Status=2
				inner join gudang g on g.KdGudang = h.warehousecode 
				inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
				inner join stock_price s on s.PCode=m.inventorycode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set m.value = s.Price
				WHERE year(m.packagingdate)='$tahun' and month(m.packagingdate)='$bulan'";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.batchnumber, 'O', 'MX', h.warehousecode, h.warehousecode, m.`packagingdate`, m.`inventorycode`, m.`quantity`, m.`value`, 0, 1 
					FROM production h INNER JOIN productionpackaging m ON h.`productionid`=m.`productionid` and h.Status=2
					WHERE year(m.packagingdate)='$tahun' and month(m.packagingdate)='$bulan'";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(m.packagingdate) as Tahun, h.warehousecode as KdGudang, m.`inventorycode` as PCode, 'G' as Status, sum(m.`quantity`) as Qty1, sum(m.quantity*m.`value`) as Nilai1, sum(m.`quantity`) as Qty2, sum(m.quantity*m.`value`) as Nilai2 
					FROM production h INNER JOIN productionpackaging m ON h.`productionid`=m.`productionid` and h.Status=2
					WHERE year(m.packagingdate)='$tahun' and month(m.packagingdate)='$bulan' AND h.`Status`=2
					group by h.warehousecode, m.inventorycode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _FinishGoods($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'I';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		if($bulan>1){
			$bulanlalu = $bulan-1;
			$tahunlalu = $tahun;
		}
		else{
			$bulanlalu = 12;
			$tahunlalu = $tahun-1;
		}
		$bulanlalu = $bulanlalu < 10 ? '0' . $bulanlalu : $bulanlalu;

		$fieldakhirlalu = "GAkhir" . $bulanlalu;
		$fieldnakhirlalu = "GNAkhir" . $bulanlalu;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.batchnumber, 'I', 'FG', h.warehousecode, h.warehousecode, fg.`productiondate`, fg.`inventorycode`, fg.`quantity`, fg.`quantity`* fg.hppvalue, 0, 1 
					FROM productionfinishgood fg INNER JOIN production h ON h.`productionid`=fg.`productionid2`
					WHERE YEAR(fg.`productiondate`)='$tahun' AND MONTH(fg.`productiondate`)='$bulan' and h.Status=2";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir,LastUnitCost)
					SELECT Tahun, KdGudang, PCode, `Status`, Qty1, Nilai1, Qty2, Nilai2, if(Qty1>0,Nilai1/Qty1,0) FROM (
					SELECT YEAR(fg.`productiondate`) AS Tahun, h.warehousecode AS KdGudang, fg.`inventorycode` AS PCode, 'G' AS STATUS, SUM(fg.`quantity`) AS Qty1, SUM(fg.quantity*fg.`hppvalue`) AS Nilai1, SUM(fg.`quantity`) AS Qty2, SUM(fg.quantity*fg.`hppvalue`) AS Nilai2 
					FROM productionfinishgood fg INNER JOIN production h ON h.`productionid`=fg.`productionid2`
					WHERE YEAR(fg.`productiondate`)='$tahun' AND MONTH(fg.`productiondate`)='$bulan' AND h.status='2'
					GROUP BY h.warehousecode, fg.inventorycode) a
					ON DUPLICATE KEY UPDATE LastUnitCost=if($fieldakhir+a.Qty1<>0,abs(($fieldnakhir+a.Nilai1)/($fieldakhir+a.Qty1)),LastUnitCost), $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";

		$query = $this->CI->db->query($sqlstock);
	}

	function _MutasiGudangProduksi($tahun,$bulan){
		//mutasi selain gudang produksi. Untuk gudang produksi dihitung setelah hitung hpp
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$fieldkeluar = "GKeluar" . $vbulan;
		$fieldnkeluar = "GNKeluar" . $vbulan;
		$fieldmasuk = "GMasuk" .$vbulan;
		$fieldnmasuk = "GNMasuk" .$vbulan;
		
		$sql1 = "Update
					trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					inner join gudang g on g.KdGudang = h.KdGudang_To 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
					inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
					set d.Harga=s.Price
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='05'";
		$query = $this->CI->db->query($sql1);
		
		// konfirmasi 05
		$sqlmutasi1 = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`,'I','MC',h.`KdGudang_To`, h.`KdGudang_To`, h.TglDokumen, d.`PCode`, d.`QtyPcs`, d.Harga, 0, 1
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='05' and MovingConfirmation=1";
		$query = $this->CI->db->query($sqlmutasi1);
		
		// mutasi 05
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`,'O','MM',h.`KdGudang_From`, h.`KdGudang_From`, h.TglDokumen, d.`PCode`, d.`QtyPcs`, d.Harga, 0, 1
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='05'";
		$query = $this->CI->db->query($sqlmutasi);
				
		//update konfirmasi 05
		$sqlstock1 = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldmasuk,$fieldnmasuk,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2, if(Qty2>0,Nilai2/Qty2,0) from (
					SELECT year(h.TglDokumen) as Tahun, KdGudang_To as KdGudang, PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='05' and MovingConfirmation=1
					group by KdGudang_From, d.PCode) a
					ON DUPLICATE KEY UPDATE LastUnitCost= if($fieldakhir+a.Qty1<>0,abs(($fieldnakhir+a.Nilai1)/($fieldakhir+a.Qty1)),LastUnitCost),$fieldmasuk=$fieldmasuk+a.Qty1,$fieldnmasuk=$fieldnmasuk+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";
		$query = $this->CI->db->query($sqlstock1);		
		
		// update 05
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldkeluar,$fieldnkeluar,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(h.TglDokumen) as Tahun, KdGudang_From as KdGudang, PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='05' 
					group by KdGudang_From, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldkeluar=$fieldkeluar+a.Qty1,$fieldnkeluar=$fieldnkeluar+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
		
		//update 01
		$sql1 = "Update
					trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen` 
					inner join gudang g on g.KdGudang = h.KdGudang_To 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
					inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
					set d.Harga=s.Price
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='01'";
		$query = $this->CI->db->query($sql1);
		
		//konfirmasi 01
		$sqlmutasi3 = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`,'I','MC',h.`KdGudang_To`, h.`KdGudang_To`, h.TglDokumen, d.`PCode`, d.`QtyPcs`, d.Harga, 0, 1
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='01' and MovingConfirmation=1";
		$query = $this->CI->db->query($sqlmutasi3);
		
		//mutasi 01
		$sqlmutasi2 = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`,'O','MM',h.`KdGudang_From`, h.`KdGudang_From`, h.TglDokumen, d.`PCode`, d.`QtyPcs`, d.Harga, 0, 1
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='01'";
		$query = $this->CI->db->query($sqlmutasi2);
		
		// update konfirmasi 01
		$sqlstock3 = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldmasuk,$fieldnmasuk,$fieldakhir,$fieldnakhir, LastUnitCost)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2, if(Qty2>0,Nilai2/Qty2,0) from (
					SELECT year(h.TglDokumen) as Tahun, KdGudang_To as KdGudang, PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='01' and MovingConfirmation=1
					group by KdGudang_From, d.PCode) a
					ON DUPLICATE KEY UPDATE LastUnitCost= if($fieldakhir+a.Qty1<>0,abs(($fieldnakhir+a.Nilai1)/($fieldakhir+a.Qty1)),LastUnitCost),$fieldmasuk=$fieldmasuk+a.Qty1,$fieldnmasuk=$fieldnmasuk+a.Nilai1, $fieldakhir=$fieldakhir+a.Qty1,$fieldnakhir=$fieldnakhir+a.Nilai1";
		$query = $this->CI->db->query($sqlstock3);		
		
		//update 01
		$sqlstock2 = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldkeluar,$fieldnkeluar,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(h.TglDokumen) as Tahun, KdGudang_From as KdGudang, PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.Harga) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.Harga) as Nilai2
 					FROM trans_mutasi_header h INNER JOIN trans_mutasi_detail d ON h.`NoDokumen` = d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' AND h.`Status`=1 and h.KdGudang_From='01' 
					group by KdGudang_From, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldkeluar=$fieldkeluar+a.Qty1,$fieldnkeluar=$fieldnkeluar+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock2);		
		
	}
	
	function _Penjualan($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'O';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "update transaksi_detail d inner join kassa k on d.NoKassa = k.id_kassa 
					inner join gudang g on g.KdGudang = d.Gudang 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
				inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.HPP = s.Price
				WHERE year(Tanggal)='$tahun' and month(Tanggal)='$bulan' AND d.`Status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					select d.NoStruk,'O','R',if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang) ,if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang) ,d.Tanggal,d.PCode, d.Qty, d.HPP,0,1 
					from transaksi_detail d INNER JOIN kassa k ON d.`NoKassa` = k.`id_kassa`
					WHERE year(Tanggal)='$tahun' and month(Tanggal)='$bulan' AND d.`Status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					select year(d.Tanggal) as Tahun,if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang) as KdGudang , d.PCode, 'G' as Status, sum(d.Qty) as Qty1, sum(d.Qty*d.HPP) as Nilai1, sum(d.Qty) as Qty2, sum(d.Qty*d.HPP) as Nilai2 
					from transaksi_detail d inner join kassa k on d.NoKassa=k.id_kassa
					WHERE year(Tanggal)='$tahun' and month(Tanggal)='$bulan' AND d.`Status`=1
					group by if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang), d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		

		//Penjualan Counter
		$sql = "update transaksi_detail_sunset d inner join kassa k on d.NoKassa = k.id_kassa 
				inner join gudang g on g.KdGudang = d.Gudang 
				inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
				inner join stock_price s on s.PCode=d.PCode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.HPP = s.Price 
				WHERE year(Tanggal)='$tahun' and month(Tanggal)='$bulan' AND d.`Status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					select d.NoStruk,'O','R',if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang) ,if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang) ,d.Tanggal,d.PCode, d.Qty, d.HPP,0,1 
					from transaksi_detail_sunset d INNER JOIN kassa k ON d.`NoKassa` = k.`id_kassa`
					WHERE year(Tanggal)='$tahun' and month(Tanggal)='$bulan' AND d.`Status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					select year(d.Tanggal) as Tahun,if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang) as KdGudang , d.PCode, 'G' as Status, sum(d.Qty) as Qty1, sum(d.Qty*d.HPP) as Nilai1, sum(d.Qty) as Qty2, sum(d.Qty*d.HPP) as Nilai2 
					from transaksi_detail_sunset d INNER JOIN kassa k ON d.`NoKassa` = k.`id_kassa`
					WHERE year(Tanggal)='$tahun' and month(Tanggal)='$bulan' AND d.`Status`=1
					group by if(d.PCode in('200213002','200213003','200213004'),'09',k.KdGudang), d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		

	}

	function _DeliveryOrder($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'O';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "Update deliveryorder h INNER JOIN deliveryorderdetail d ON h.`dono`=d.`dono` 
				inner join gudang g on g.KdGudang = h.warehousecode 
				inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
				inner join stock_price s on s.PCode=d.inventorycode and s.KdDivisi=sd.KdDivisi and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.hppvalue = s.Price 
				WHERE year(h.`dodate`) ='$tahun' and month(h.dodate)='$bulan' AND h.status=1 ";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`dono`, 'O', 'R', h.`warehousecode`, h.`warehousecode`,h.dodate,d.`inventorycode`, d.`QtyPcs`, d.hppvalue, 0, 1 
					FROM deliveryorder h INNER JOIN deliveryorderdetail d ON h.`dono`=d.`dono`
					WHERE year(h.`dodate`)='$tahun' and month(h.dodate)='$bulan' AND h.status=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(h.dodate) as Tahun, h.warehousecode as KdGudang, d.`inventorycode` as PCode, 'G' as Status, sum(d.`QtyPcs`) as Qty1, sum(d.QtyPCs*d.hppvalue) as Nilai1, sum(d.`QtyPcs`) as Qty2, sum(d.QtyPCs*d.hppvalue) as Nilai2 
					FROM deliveryorder h INNER JOIN deliveryorderdetail d ON h.`dono`=d.`dono`
					WHERE year(h.`dodate`)='$tahun' and month(h.dodate)='$bulan' AND  h.status=1
					group by h.warehousecode, d.inventorycode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _ReturBeli($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$jenis = 'O';
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $vbulan;
			$fieldnupdate = "GNKeluar" . $vbulan;
		}else{
			$fieldupdate = "GMasuk" .$vbulan;
			$fieldnupdate = "GNMasuk" .$vbulan;
		}
		
		$sql = "Update
					purchasereturn h INNER JOIN purchasereturndetail d ON h.`pretno`=d.`pretno` 
					inner join gudang g on g.KdGudang = h.warehouse 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi 
					inner join stock_price s on  s.PCode=d.InventoryCode and s.KdDivisi=sd.KdDivisi  and s.Tahun='$tahun' and s.Bulan='$bulan' 
				set d.HPP=s.Price 
				WHERE year(h.`pretdate`)='$tahun' and month(h.pretdate)='$bulan' AND h.`status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`pretno`, 'O', 'RB',h.`warehouse`,h.`warehouse`,h.`pretdate`,d.`inventorycode`, d.`QtyPcs`, d.`HPP`,0,1 
					from purchasereturn h INNER JOIN purchasereturndetail d ON h.`pretno`=d.`pretno`
					WHERE year(h.`pretdate`)='$tahun' and month(h.pretdate)='$bulan' AND h.`status`=1";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, Qty1, Nilai1, Qty2, Nilai2 from (
					SELECT year(h.pretdate) as Tahun,h.`warehouse` as KdGudang,d.`inventorycode` as PCode,'G' as Status, sum(d.`QtyPCs`) as Qty1, sum(d.QtyPCs*d.`HPP`) as Nilai1,sum(d.`QtyPCs`) as Qty2, sum(d.QtyPCS*d.`HPP`) as Nilai2 
					from purchasereturn h INNER JOIN purchasereturndetail d ON h.`pretno`=d.`pretno`
					WHERE year(h.`pretdate`)='$tahun' and month(h.pretdate)='$bulan' AND h.`status`=1
					group by h.warehouse, d.inventorycode) a
					ON DUPLICATE KEY UPDATE $fieldupdate=$fieldupdate+a.Qty1,$fieldnupdate=$fieldnupdate+a.Nilai1, $fieldakhir=$fieldakhir-a.Qty1,$fieldnakhir=$fieldnakhir-a.Nilai1";
		$query = $this->CI->db->query($sqlstock);		
	}

	function _Opname($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$fieldkeluar = "GKeluar" . $vbulan;
		$fieldnkeluar = "GNKeluar" . $vbulan;
		$fieldmasuk = "GMasuk" .$vbulan;
		$fieldnmasuk = "GNMasuk" .$vbulan;
		$tgl = $tahun.'-'.$vbulan.'-01';
		
		$sql0 = "UPDATE opname_header h INNER JOIN opname_detail d ON h.`NoDokumen`=d.`NoDokumen`
					SET d.HargaSatuan=0, d.QtyProgram = 0 
				WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan'";
		$query = $this->CI->db->query($sql0);
				
		if($tgl>='2017-07-01'){
			$sql1 = "UPDATE opname_header h INNER JOIN opname_detail d ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN stock s ON d.`PCode`=s.pcode AND s.Tahun='$tahun' and s.KdGudang=h.KdGudang
					SET d.QtyProgram = $fieldakhir
				WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan'";
			$query = $this->CI->db->query($sql1);	
			
			$sql2 = "UPDATE opname_header h INNER JOIN opname_detail d ON h.`NoDokumen`=d.`NoDokumen` 
					inner join gudang g on g.KdGudang = h.KdGudang 
					inner join subdivisi sd on g.KdSubdivisi=sd.KdSubdivisi
					INNER JOIN stock_price s ON d.`PCode`=s.pcode and s.KdDivisi=sd.KdDivisi AND s.Tahun='$tahun' and s.Bulan='$bulan' 
					SET d.HargaSatuan=s.Price
				WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan'";
				echo $sql2;
			$query = $this->CI->db->query($sql2);	
		}else{
			$sql = "UPDATE opname_header h INNER JOIN opname_detail d ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN stock s ON d.`PCode`=s.pcode AND s.Tahun='$tahun' AND h.KdGudang=s.KdGudang
					SET d.QtyProgram = $fieldakhir
				WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan'";
			$query = $this->CI->db->query($sql);
		}
		

		$sqlmutasi = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
					SELECT h.`NoDokumen`, if(d.QtyFisik>d.QtyProgram,'I','O'),'SO',h.`KdGudang`,h.`KdGudang`,h.`TglDokumen`,d.`PCode`, abs(d.QtyFisik-d.QtyProgram), d.`HargaSatuan`,0,1 
					FROM opname_header h INNER JOIN opname_detail d ON h.`NoDokumen`=d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' 
					AND h.`Approval_Status`=1 AND h.`Status`=1 AND d.`QtyFisik`<>d.`QtyProgram`";
		$query = $this->CI->db->query($sqlmutasi);
		
		$sqlstock = "insert into stock(Tahun, KdGudang, PCode, Status,$fieldmasuk,$fieldnmasuk,$fieldkeluar,$fieldnkeluar,$fieldakhir,$fieldnakhir)
					Select Tahun, KdGudang, PCode, Status, QtyMasuk, NilaiMasuk, QtyKeluar, NilaiKeluar, if(QtyMasuk>QtyKeluar,QtyMasuk-QtyKeluar,QtyKeluar-QtyMasuk) as QtyAkhir, if(QtyMasuk>QtyKeluar,NilaiMasuk-NilaiKeluar,NilaiKeluar-NilaiMasuk) as NilaiAkhir from (
					SELECT year(h.TglDokumen) as Tahun, h.`KdGudang`,d.`PCode`,'G' as Status, sum(if(d.QtyFisik>d.QtyProgram, d.QtyFisik-d.QtyProgram,0)) as QtyMasuk,sum(if(d.QtyFisik>d.QtyProgram, (d.QtyFisik-d.QtyProgram)*HargaSatuan,0)) as NilaiMasuk,
					sum(if(d.QtyFisik<d.QtyProgram, d.QtyProgram-d.QtyFisik,0)) as QtyKeluar,sum(if(d.QtyFisik<d.QtyProgram, (d.QtyProgram-d.QtyFisik)*HargaSatuan,0)) as NilaiKeluar 
					FROM opname_header h INNER JOIN opname_detail d ON h.`NoDokumen`=d.`NoDokumen`
					WHERE year(h.`TglDokumen`)='$tahun' and month(h.TglDokumen)='$bulan' 
					AND h.`Approval_Status`=1 AND h.`Status`=1 AND d.`QtyFisik`<>d.`QtyProgram`
					group by h.KdGudang, d.PCode) a
					ON DUPLICATE KEY UPDATE $fieldmasuk=$fieldmasuk+a.QtyMasuk,$fieldnmasuk=$fieldnmasuk+a.NilaiMasuk, $fieldkeluar=$fieldkeluar+a.QtyKeluar,$fieldnkeluar=$fieldnkeluar+a.NilaiKeluar, 
					$fieldakhir=$fieldakhir+a.QtyMasuk-a.QtyKeluar,$fieldnakhir=$fieldnakhir+a.NilaiMasuk-a.NilaiKeluar";
		$query = $this->CI->db->query($sqlstock);		
		
	}

	function _hitunghpp($tahun,$bulan){
		$qry = "UPDATE productionfinishgood fg INNER JOIN (
					SELECT batchnumber, SUM(Qty) AS Qty, SUM(QtyQC) AS QtyQC, SUM(TotalQty) AS TotalQty, SUM(MixingValue) AS MixingValue, SUM(PackagingValue) AS PackagingValue,
					AVG(QtyFormula) AS QtyFormula, AVG(QtyWIP) QtyWIP, SUM(MixQty) AS MixQty, (SUM(PackagingValue)+SUM(MixingValue)/(SUM(MixQty)/AVG(QtyWIP)*AVG(QtyFormula))*SUM(TotalQty))/SUM(TotalQty) AS UnitCost, formulaname
 					FROM (
					SELECT g.`batchnumber`, AVG(g.`quantity`) AS Qty, AVG(g.`quantityqc`) AS QtyQC, AVG(g.`quantity`+g.`quantityqc`) AS TotalQty, SUM(m.quantity*m.value) AS MixingValue, 0 AS PackagingValue,
					AVG(f.packagingsize) AS QtyFormula, AVG(f.quantity) AS QtyWIP, SUM(m.quantity) AS MixQty, f.formulaname
					FROM productionfinishgood g INNER JOIN productionmixing m ON g.`batchnumber`=m.`batchnumber`
					INNER JOIN production p ON g.batchnumber=p.batchnumber AND p.status=1
					INNER JOIN formula f ON p.formulanumber=f.formulanumber
					WHERE YEAR(g.`productiondate`)='$tahun' AND MONTH(g.`productiondate`)='$bulan'
					GROUP BY g.`batchnumber`
					UNION ALL
					SELECT g.`batchnumber`, 0, 0, 0, 0 AS MixingValue, SUM(m.quantity*m.value) AS PackagingValue,
					AVG(f.packagingsize) AS QtyFormula, AVG(f.quantity) AS QtyWIP, 0 AS MixQty, f.formulaname  
					FROM productionfinishgood g INNER JOIN productionpackaging m ON g.`batchnumber`=m.`batchnumber`
					INNER JOIN production p ON g.batchnumber=p.batchnumber AND p.status=1
					INNER JOIN formula f ON p.formulanumber=f.formulanumber
					WHERE YEAR(g.`productiondate`)='$tahun' AND MONTH(g.`productiondate`)='$bulan' AND YEAR(m.packagingdate)='$tahun' AND MONTH(m.packagingdate)='$bulan'
					GROUP BY g.`batchnumber`
					UNION ALL
					SELECT g.`batchnumber`, 0, 0, 0, 0 AS MixingValue, SUM(m.quantity*m.value) AS PackagingValue,
					AVG(f.packagingsize) AS QtyFormula, AVG(f.quantity) AS QtyWIP, 0 AS MixQty, f.formulaname 
					FROM productionfinishgood g INNER JOIN productionpackadditional m ON g.`batchnumber`=m.`batchnumber`
					INNER JOIN production p ON g.batchnumber=p.batchnumber AND p.status=1
					INNER JOIN formula f ON p.formulanumber=f.formulanumber
					WHERE YEAR(g.`productiondate`)='$tahun' AND MONTH(g.`productiondate`)='$bulan' AND YEAR(m.packagingdate)='$tahun' AND MONTH(m.packagingdate)='$bulan'
					GROUP BY g.`batchnumber`
					) a GROUP BY batchnumber
					) b ON fg.`batchnumber`=b.batchnumber
					SET fg.`hppvalue` = b.UnitCost
					WHERE YEAR(fg.`productiondate`)='$tahun' AND MONTH(fg.`productiondate`)='$bulan'";
		$query = $this->CI->db->query($qry);
	}
	
	function _adjustment($tahun,$bulan){
		$vbulan = $bulan < 10 ? '0'.$bulan : $bulan;
		$fieldakhir = "GAkhir" . $vbulan;
		$fieldnakhir = "GNAkhir" . $vbulan;
		$fieldkeluar = "GKeluar" . $vbulan;
		$fieldnkeluar = "GNKeluar" . $vbulan;
		$fieldmasuk = "GMasuk" .$vbulan;
		$fieldnmasuk = "GNMasuk" .$vbulan;
		
		$sqladj = "insert into stok_adj_detail(NoDokumen,PCode,Nama,QtyData,NilaiData,QtyAdj,NilaiAdj)
					SELECT h.NoDokumen, s.PCode, '', $fieldakhir,$fieldnakhir,if($fieldakhir>0,$fieldakhir,0),if($fieldakhir>0,$fieldnakhir,0) FROM stock s INNER JOIN stok_adj_header h ON s.`Tahun`=YEAR(h.`Tanggal`) AND s.`KdGudang`=h.`KdGudang`
					ON DUPLICATE KEY UPDATE QtyData=$fieldakhir, NilaiData=$fieldnakhir";
		$query = $this->CI->db->query($sqladj);		
		
		$sql = "INSERT INTO mutasi(NoTransaksi, Jenis, KdTransaksi, Gudang, GudangTujuan, Tanggal, KodeBarang, Qty, Nilai, NilaiAdj, `Status`)
				SELECT h.`NoDokumen`, IF( d.`NilaiAdj`>=d.`NilaiData`,'I','O') AS Jenis, 'AD' AS KdTransaksi, h.KdGudang, h.KdGudang,
				h.`Tanggal`, d.PCode, IF(d.`NilaiAdj`>=d.`NilaiData`, d.QtyAdj-d.`QtyData`, d.`QtyData`-d.QtyAdj) AS Qty, 0 AS Nilai,
				IF(d.`NilaiAdj`>=d.`NilaiData`, d.NilaiAdj-d.`NilaiData`, d.`NilaiData`-d.NilaiAdj) AS NilaiAdj, 1
				FROM stok_adj_header h INNER JOIN stok_adj_detail d ON h.`NoDokumen`=d.`NoDokumen`
				WHERE year(h.`Tanggal`)='$tahun' and month(h.Tanggal)='$bulan' AND h.`Status`=1";
		$query = $this->CI->db->query($sql);
		
		$sqlstock = "UPDATE stok_adj_header h INNER JOIN stok_adj_detail d ON h.`NoDokumen`=d.`NoDokumen`
					INNER JOIN stock s ON s.`PCode`=d.`PCode` AND s.`KdGudang`=h.`KdGudang`
					SET LastUnitCost=if($fieldakhir+d.`QtyAdj`-d.`QtyData`>0,($fieldnakhir+d.`NilaiAdj`-d.`NilaiData`)/($fieldakhir+d.`QtyAdj`-d.`QtyData`),LastUnitCost),
					$fieldmasuk=$fieldmasuk+IF(d.QtyAdj>d.`QtyData`,d.`QtyAdj`-d.`QtyData`,0),
	    			$fieldnmasuk=$fieldnmasuk+IF(d.NilaiAdj>d.`NilaiData`,d.`NilaiAdj`-d.`NilaiData`,0),
	    			$fieldkeluar=$fieldkeluar+IF(d.QtyAdj<d.`QtyData`,d.`QtyData`-d.`QtyAdj`,0),
	    			$fieldnkeluar=$fieldnkeluar+IF(d.NilaiAdj<d.`NilaiData`,d.`NilaiData`-d.`NilaiAdj`,0),
	    			$fieldakhir=$fieldakhir+(d.`QtyAdj`-d.`QtyData`),
	    			$fieldnakhir=$fieldnakhir+(d.`NilaiAdj`-d.`NilaiData`) 
					WHERE year(h.Tanggal)='$tahun' and month(h.Tanggal)='$bulan' AND s.`Tahun`=YEAR(h.`Tanggal`)";
		
		$query = $this->CI->db->query($sqlstock);		
	}
	
	function NextDate($tgl){
		$sql = "SELECT DATE_ADD('$tgl', INTERVAL 1 DAY) as NextDate" ;
        $query = $this->CI->db->query($sql);
        $result = $query->result_array();
        return $result[0]['NextDate'];
	}

}
?>
