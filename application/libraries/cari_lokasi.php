<?php
class cari_lokasi{
	var $CI;
	function __construct(){
	    $this->CI =& get_instance();
	 	$this->CI->load->library('session');
		$this->CI->load->model('transaksi/cari_lokasi_model');
	}
	function CariUrutan($arrLokasi)
	{
		$tingkatan = $this->CI->cari_lokasi_model->getTingkat(implode("','",$arrLokasi));
		$simpan_dari = $this->CI->cari_lokasi_model->getDate();
		if($simpan_dari->UrutanPenyimpanan=="B")
		{
			sort($tingkatan);
		}
		if($simpan_dari->UrutanPenyimpanan=="A")
		{
			rsort($tingkatan);
		}
		$urut = $this->CI->cari_lokasi_model->UrutanLokasi(implode("','",$arrLokasi),count($tingkatan),$tingkatan);
		return $urut;
	}
	function FindIn($kdlokasi,$kdlokasi2)
	{
	//	echo $kdlokasi."<br>";
		$tgltrans = $this->CI->session->userdata('Tanggal_Trans');
		$tahun = substr($tgltrans,0,4);
		$bulan = substr($tgltrans,5,2);
		$field = "GAkhir".$bulan;
		$telusuri = $this->CI->cari_lokasi_model->CariTetangga($kdlokasi);
		for($a=0;$a<count($telusuri);$a++)
		{
			if($telusuri[$a]['StatusOnOfAtas']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiAtas']&&$telusuri[$a]['KdLokasiAtas']!="")
			{
				$result = $this->FindIn($telusuri[$a]['KdLokasiAtas'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfBawah']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiBawah']&&$telusuri[$a]['KdLokasiBawah']!="")
			{
				$result = $this->FindIn($telusuri[$a]['KdLokasiBawah'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfTimur']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiTimur']&&$telusuri[$a]['KdLokasiTimur']!="")
			{
				$result = $this->FindIn($telusuri[$a]['KdLokasiTimur'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfUtara']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiUtara']&&$telusuri[$a]['KdLokasiUtara']!="")
			{
				$result = $this->FindIn($telusuri[$a]['KdLokasiUtara'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfBarat']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiBarat']&&$telusuri[$a]['KdLokasiBarat']!="")
			{
				$result = $this->FindIn($telusuri[$a]['KdLokasiBarat'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfSelatan']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiSelatan']&&$telusuri[$a]['KdLokasiSelatan']!="")
			{
				$result = $this->FindIn($telusuri[$a]['KdLokasiSelatan'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfAtas']=="P"&&($telusuri[$a]['StatusInOutAtas']=="I"||$telusuri[$a]['StatusInOutAtas']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($telusuri[$a]['KdLokasiAtas'],$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfBawah']=="P"&&($telusuri[$a]['StatusInOutBawah']=="I"||$telusuri[$a]['StatusInOutBawah']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfTimur']=="P"&&($telusuri[$a]['StatusInOutTimur']=="I"||$telusuri[$a]['StatusInOutTimur']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfUtara']=="P"&&($telusuri[$a]['StatusInOutUtara']=="I"||$telusuri[$a]['StatusInOutUtara']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfBarat']=="P"&&($telusuri[$a]['StatusInOutBarat']=="I"||$telusuri[$a]['StatusInOutBarat']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfSelatan']=="P"&&($telusuri[$a]['StatusInOutSelatan']=="I"||$telusuri[$a]['StatusInOutSelatan']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			else
			{
				if(empty($result))
				{
					$result = "";
				}
				return $result;
			}
		}
	}
	function FindOut($kdlokasi,$kdlokasi2)
	{
	//	echo $kdlokasi."<br>";
		$tgltrans = $this->CI->session->userdata('Tanggal_Trans');
		$tahun = substr($tgltrans,0,4);
		$bulan = substr($tgltrans,5,2);
		$field = "GAkhir".$bulan;
		$telusuri = $this->CI->cari_lokasi_model->CariTetangga($kdlokasi);
		for($a=0;$a<count($telusuri);$a++)
		{
			if($telusuri[$a]['StatusOnOfAtas']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiAtas']&&$telusuri[$a]['KdLokasiAtas']!="")
			{
				$result = $this->FindOut($telusuri[$a]['KdLokasiAtas'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfBawah']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiBawah']&&$telusuri[$a]['KdLokasiBawah']!="")
			{
				$result = $this->FindOut($telusuri[$a]['KdLokasiBawah'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfTimur']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiTimur']&&$telusuri[$a]['KdLokasiTimur']!="")
			{
				$result = $this->FindOut($telusuri[$a]['KdLokasiTimur'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfUtara']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiUtara']&&$telusuri[$a]['KdLokasiUtara']!="")
			{
				$result = $this->FindOut($telusuri[$a]['KdLokasiUtara'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfBarat']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiBarat']&&$telusuri[$a]['KdLokasiBarat']!="")
			{
				$result = $this->FindOut($telusuri[$a]['KdLokasiBarat'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfSelatan']=="N"&&$kdlokasi2!=$telusuri[$a]['KdLokasiSelatan']&&$telusuri[$a]['KdLokasiSelatan']!="")
			{
				$result = $this->FindOut($telusuri[$a]['KdLokasiSelatan'],$kdlokasi);
				if($result!="")
				{
					return $result;
				}
			}
			if($telusuri[$a]['StatusOnOfAtas']=="P"&&($telusuri[$a]['StatusInOutAtas']=="O"||$telusuri[$a]['StatusInOutAtas']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($telusuri[$a]['KdLokasiAtas'],$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfBawah']=="P"&&($telusuri[$a]['StatusInOutBawah']=="O"||$telusuri[$a]['StatusInOutBawah']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfTimur']=="P"&&($telusuri[$a]['StatusInOutTimur']=="O"||$telusuri[$a]['StatusInOutTimur']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfUtara']=="P"&&($telusuri[$a]['StatusInOutUtara']=="O"||$telusuri[$a]['StatusInOutUtara']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfBarat']=="P"&&($telusuri[$a]['StatusInOutBarat']=="O"||$telusuri[$a]['StatusInOutBarat']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			if($telusuri[$a]['StatusOnOfSelatan']=="P"&&($telusuri[$a]['StatusInOutSelatan']=="O"||$telusuri[$a]['StatusInOutSelatan']=="A"))
			{
				$jmlqty = $this->CI->cari_lokasi_model->cariQtyInOut($kdlokasi,$tahun,$field);
				if(!empty($jmlqty)){
					if($jmlqty->$field!=0)
					{
						$result = "full";
					}
					else
					{
						$result = "empty";
					}
				}
				else
				{
					$result = "empty";
				}
				return $result;
			}
			else
			{
				if(empty($result))
				{
					$result = "";
				}
				return $result;
			}
		}
	}
}