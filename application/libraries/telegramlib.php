<?php
class telegramlib {
	var $bot_keyboard = '';
	var $id_chat = '';
	var $id_msg = '';
	var $bot_text = '';
	
	function keyboard($keyboard){
		$this->bot_keyboard = $keyboard;	
	}
	
	function chat_id($chatid){
		$this->id_chat = $chatid;
	}
	
	function msg_id($msgid){
		$this->id_msg = $msgid;
	}
	
	function text($text){
		$this->bot_text = $text;
	}
	
	function send_with_inline_keyboard(){
		if(isset($this->bot_keyboard)){
			$replyMarkup=array(
				"inline_keyboard" => $this->bot_keyboard,
				"one_time_keyboard" => true, 
				"resize_keyboard" => true
			);
			
			$encodedMarkup = json_encode($replyMarkup);
			$data = array(
		        'chat_id' => $this->id_chat,
	    	    'reply_markup' => $encodedMarkup,
	    	    'parse_mode' => 'html',
	        	'text'  => $this->bot_text
	        );
		}else{
			$replyMarkup = array(
	    		'keyboard' => array(array())
			);
	          
			$encodedMarkup = json_encode($replyMarkup);
			$data = array(
	        	'chat_id' => $this->id_chat,
	        	'reply_markup' => $encodedMarkup,
	        	'parse_mode' => 'html',
	        	'text'  => $this->bot_text
	        );
	        // 'reply_to_message_id' => $msgid
		}	
		
	    $this->send_bot($data);
	}
	
	function send_with_keyboard(){
		if(isset($this->bot_keyboard)){
			$replyMarkup=array(
				"keyboard" => $this->bot_keyboard,
				"one_time_keyboard" => true, 
				"resize_keyboard" => true
			);
			
			$encodedMarkup = json_encode($replyMarkup);
			$data = array(
		        'chat_id' => $this->id_chat,
	    	    'reply_markup' => $encodedMarkup,
	    	    'parse_mode' => 'html',
	        	'text'  => $this->bot_text
	        );
		}else{
			$replyMarkup = array(
	    		'keyboard' => array(array())
			);
	          
			$encodedMarkup = json_encode($replyMarkup);
			$data = array(
	        	'chat_id' => $this->id_chat,
	        	'reply_markup' => $encodedMarkup,
	        	'parse_mode' => 'html',
	        	'text'  => $this->bot_text
	        );
	        // 'reply_to_message_id' => $msgid
		}	
	    $this->send_bot($data);
	}
	
	function send(){
		$replyMarkup = array(
    		'keyboard' => array(array())
		);
          
		$encodedMarkup = json_encode($replyMarkup);
		$data = array(
        	'chat_id' => $this->id_chat,
        	'reply_markup' => $encodedMarkup,
        	'parse_mode' => 'html',
        	'text'  => $this->bot_text);
        
	    $this->send_bot($data);
	}
	
	function send_bot($data){
		/*
		$options = array(
    		'http' => array(
        	'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        	'method'  => 'POST',
        	'content' => http_build_query($data),
    		),
    	);
    	$context  = stream_context_create($options);
    	$result = file_get_contents($this->request_url('sendMessage'), false, $context);
    	*/
    	$ch = curl_init($this->request_url('sendMessage'));
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
	}
	
	function request_url($method){
		$TOKEN = "417769536:AAGwPnLiMGGhgNST3Fd3cABwm4bui5XC3ak";
		return "https://api.telegram.org/bot" . $TOKEN . "/". $method;
	}
		
}
?>