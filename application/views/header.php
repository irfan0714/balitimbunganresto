<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo $this->session->userdata('Alias');?></title>
    <link rel="shortcut icon" href="<?= base_url();?>public/images/<?php echo $this->session->userdata('Logo');?>" >

    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">

    <script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>

    <link rel="stylesheet" href="<?= base_url();?>assets/css/select2.css">
    <script src="<?php echo base_url(); ?>assets/js/sweetalert2.all.js"></script>

    <script>
      var ip_address = "Your IP : "+"<?php echo $this->session->userdata('ip_address'); ?>";

      console.log(ip_address);
    </script>

	<style>
	.sticky-header {
	background-color: #222222;
	color: #fff;
	width:100%;
	position:fixed;
	top: 0;
	left: 0;
	margin: 0;
	padding: 10px;
	text-align:left;
	letter-spacing: 3px;
	}
	.content-scroll{
	height:1000px;
	padding:100px 0px 0px 50px;
	}
	.content-scroll p{
	height:50px;
	}
	</style>

</head>
<body class="page-body skin-black" >

<!-- The Modal -->
<div class="modal" id="modal-history1" style="display: none;position: absolute; float: left;left: 50%;top: 60%;transform: translate(-50%, -50%);>
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Riwayat Pembelian</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <table id="riwayatbeli" class="table table-striped table-bordered" style="width:100%">
          <thead>
              <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Supplier</th>
                  <th>PCode</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Harga</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody id="isipembelian">
          </tbody>
        </table>
        <div id="messages"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- Modal History 
<div class="modal fade" id="modal-history1" style="display: none;position: absolute; float: left;left: 50%;top: 60%;transform: translate(-50%, -50%);">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Riwayat Pembelian</h4>
        </div>
        <div class="modal-body">
          <table id="riwayatbeli" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Supplier</th>
                    <th>PCode</th>
                    <th>Nama Barang</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                </tr>
            </thead>
            <tbody id="isipembelian">
            </tbody>
          </table>
          <div id="messages"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
           <button type="button" class="btn btn-primary">Save changes</button> -->
         <!--</div>
      </div>
      /.modal-content -->
     <!--</div>
    /.modal-dialog -->
  <!--</div>
 Modal History -->


  <!-- Modal version -->
    <div class="modal fade" id="modal-version" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 class="modal-title"><i>What's New in System Natura</i></h4>
            </div>
            <div class="modal-body">
              <table class="table table-bordered table-hover" align="center">
                <tr>
                  <th style="text-align:center;font-weight:bold;background-color:#00a65a;color:white">
                    Version
                  </th>
                  <th style="text-align:center;font-weight:bold;background-color:#00a65a;color:white">
                    News Update
                  </th>
                </tr>

                <tbody id="versi">
                </tbody>
                <td colspan="2">Last Update : <span id="versidate"></span> </td>


              </table>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
  <!-- Modal Version -->


<div class="page-container sidebar-collapsed">

    <div class="sidebar-menu">


        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="index.html">
                    <img src="<?= base_url();?>public/images/<?php echo $this->session->userdata('Logo');?>" width="70" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->

            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>



            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>



        <div class="sidebar-user-info">

            <div class="sui-normal">
                <a href="#" class="user-link">
                    <?php $mylib = new globallib(); ?>
                    <span><?=$mylib->ubah_tanggal($this->session->userdata('Tanggal_Trans'));?></span>
                    <strong><?=$this->session->userdata('username');?></strong>
                </a>
            </div>
        </div>

        <?=$this->load->view('slide_kiri');?>

    </div>

    <div class="main-content" >
        <?php $pos = $this->uri->segment(2);
         if($pos=="pos" OR $pos=="ticketing"){
         	//tidak ada breadcrumb
         	}else{?>

	    	<ol class="breadcrumb bc-3">
	    		<li>
					<a href="<?php echo base_url(); ?>start">
						<i class="entypo-home"></i>Home
					</a>
				</li>

				<?php if(!empty($track)){ echo $track; } ?>

			</ol>
		    <table class="table responsive" width="100%"  style="border-radius: 25px; border-collapse: collapse;border: 5px; border-color:#15efec;border-bottom-style: solid;">
		    	<tr>
		    	    <td colspan="100%" align="center" bgcolor="<?php echo $this->session->userdata('HeaderColor');?>"><font color="#ffffff"><b><?php echo $this->session->userdata('PT');?></b></font>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    	</tr>
		    </table>
			<br>
		<?php } ?>
