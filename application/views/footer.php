

		<footer class="main"  id="footer">
    		Copyright All Reserved &copy; 2018 <strong><?php echo $this->session->userdata('PT');?></strong> | Version <a href="javascript:;" class="item-unpaid badge bg-blue" data-toggle="modal" data-target="#modal-version"><?php echo $this->session->userdata('version');?></a>
		</footer>

	</div>

</div>

<script>
$('#footer').on('click', '.item-unpaid', function(){
	
	// alert("TES");
	// $('#versi').hide();
		$.ajax({
				type: 'ajax',
				url: '<?php echo base_url() ?>start/getVersion',
				async: false,
				dataType: 'json',
				success: function(data){
					var html = '';
					var i;
					for(i=0; i<data.length; i++){

						html +='<tr>'+
									'<td style="text-align:center;">'+data[i].version+'</td>'+
									'<td style="font-style:italic;">'+data[i].updatename+'</td>'+
							    '</tr>';
					}
					$('#versi').html(html);
				},
				error: function(){
					alert('Could not get Data from Database');
				}
			});

			// getVersion
			$.ajax({
					type: 'ajax',
					url: '<?php echo base_url() ?>start/getVersionDate',
					async: false,
					dataType: 'json',
					success: function(data1){

						$('#versidate').html(data1[0].waktu_update);
					},
					error: function(){
						alert('Could not get Data from Database');
					}
				});
			//get version

});
</script>

<link rel="stylesheet" href="<?=base_url();?>assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?=base_url();?>assets/js/rickshaw/rickshaw.min.css">

<!-- Bottom Scripts -->
<script src="<?=base_url();?>assets/js/gsap/main-gsap.js"></script>
<script src="<?=base_url();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.js"></script>
<script src="<?=base_url();?>assets/js/joinable.js"></script>
<script src="<?=base_url();?>assets/js/resizeable.js"></script>
<script src="<?=base_url();?>assets/js/neon-api.js"></script>
<script src="<?=base_url();?>assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url();?>assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
<script src="<?=base_url();?>assets/js/jquery.sparkline.min.js"></script>
<script src="<?=base_url();?>assets/js/rickshaw/vendor/d3.v3.js"></script>
<script src="<?=base_url();?>assets/js/rickshaw/rickshaw.min.js"></script>
<script src="<?=base_url();?>assets/js/raphael-min.js"></script>
<script src="<?=base_url();?>assets/js/morris.min.js"></script>
<script src="<?=base_url();?>assets/js/toastr.js"></script>
<script src="<?=base_url();?>assets/js/neon-custom.js"></script>
<script src="<?=base_url();?>assets/js/neon-demo.js"></script>
<script src="<?=base_url();?>assets/js/select2.js"></script>

</body>
</html>
