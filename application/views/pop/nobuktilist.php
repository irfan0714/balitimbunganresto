<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/paging5.css" />
<script language="javascript" src="<?=base_url();?>/public/js/jquery.js"></script>

<form method="POST"  name="search" action="">
<table align='center'>
	<tr>
		<td><input type='text' size'20' name='stSearchingKey' id='stSearchingKey'></td>
		<td>
			<select size="1" height="1" name ="searchby" id ="searchby">
				<option value="NoTransaksi">Nomor</option>
				<option value="Nama">Nama</option>
			</select>
		</td>
		<td><input type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<table align='center' border='1' class='table_class_list' width="700">
	<tr>
		<th width="50">Pilih</th>
		<th width="150">Nomor</th>
		<th width="100">Kode</th>
		<th width="150">Nama</th>
		<th width="100">Nilai</th>
		<th width="100">Sisa</th>
		<th width="100">Terbit</th>
		<th width="100">Tempo</th>
		<th width="100">Rekening</th>
		<th width="100">Jenis</th>
	</tr>
<?php
	if(count($nobuktidata)==0)
	{
?>
	<td nowrap colspan="9" align="center">Tidak Ada Data</td>
<?php
	}
$i=0;
for($a = 0;$a<count($nobuktidata);$a++)
{
 	$i++;
?>
	<input type="hidden" id="detail<?=$i;?>" name="detail<?=$i;?>" value="<?=$row_no.'*_*'.$nobuktidata[$a]['NoTransaksi'].'*_*'.$nobuktidata[$a]['Sisa'].'*_*'.$nobuktidata[$a]['Rekening'].'*_*'.$nobuktidata[$a]['Nama'].'*_*'.$nobuktidata[$a]['Nilai'].'*_*'.$nobuktidata[$a]['TglTransaksi'].'*_*'.$nobuktidata[$a]['Jenis']?>" >
	<tr>
		<td nowrap>
		<? if(($module == 'P')||($module == 'R')||($module == 'S')||($module == 'H')){ ?>
			<a href="" onclick="getCode('<?=$i;?>')">
		<? }elseif(($module == 'C')||($module == 'D')){ ?>
			<a href="" onclick="getCode2('<?=$i;?>')">
		<? } ?>
			<img src="<?=base_url();?>/public/images/pick.png" border="0" alt="Select" Title="Pilih">
		</a></td>
		
		<td nowrap><?=$nobuktidata[$a]['NoTransaksi'];?></td>
		<td nowrap><?=$nobuktidata[$a]['Kode'];?></td>
		<td nowrap><?=$nobuktidata[$a]['Nama'];?></td>
		<td nowrap class="InputAlignRight"><?=number_format($nobuktidata[$a]['Nilai'], 0, ',', '.');?></td>
		<td nowrap class="InputAlignRight"><?=number_format($nobuktidata[$a]['Sisa'], 0, ',', '.');?></td>
		<td nowrap><?=$nobuktidata[$a]['TglTransaksi'];?></td>
		<td nowrap><?=$nobuktidata[$a]['TglJto'];?></td>
		<td nowrap><?=$nobuktidata[$a]['Rekening'];?></td>
		<td nowrap><?=$nobuktidata[$a]['Jenis'];?></td>
	</tr>
<?php
}
?>
</table>
<table align = 'center'>
	<tr><td><?php echo $this->pagination->create_links(); ?></td></tr>
	<tr><td><input type="button" value="Close" onclick = "closing()"></td></tr>
</table>
<?php
$this->load->view('footer'); ?>
<script language="javascript">
	function closing()
	{
	 	window.close();
	}

	function getCode(id)
	{
	 	parameter = $("#detail"+id).val().split("*_*");
		brs = parameter[0];
		window.opener.$("#nobukti"+brs).val(parameter[1]);
		window.opener.$("#tmpnobukti"+brs).val(parameter[1]);
		window.opener.$("#nama"+brs).val(parameter[4]);
		window.opener.$("#kdrekening"+brs).val(parameter[3]);
		window.opener.$("#tmpkdrekening"+brs).val(parameter[3]);
		window.opener.$("#hutang"+brs).val(parameter[2]);
		if(window.opener.$("#jumlah"+brs).val()==0)
	       window.opener.$("#jumlah"+brs).val(parameter[2]);
		window.opener.$("#kdrekening"+brs).focus();
		closing();
	}
	function getCode2(id)
	{
	 	parameter = $("#detail"+id).val().split("*_*");
		window.opener.$("#noreturcndn").val(parameter[1]);
		window.opener.$("#hiddennoreturcndn").val(parameter[1]);
		window.opener.$("#jenis").val(parameter[7]);
		window.opener.$("#tglretur").val(parameter[6]);
		window.opener.$("#jumlahretur").val(parameter[5]);
		window.opener.$("#sisaretur").val(parameter[2]);
		window.opener.$("#ket").focus();
		closing();
	}
</script>