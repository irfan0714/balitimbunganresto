<?php 
	$modul = "Detail PI";
	
	$norg = $this->uri->segment(4);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
    <script>
        
        
        function tutup()
        {
            self.close() ;
            return; 
        }
		
		
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Faktur</td>
	            <td><b><?php echo $header[0]['NoFaktur']; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal</td>
	            <td><b><?php echo $header[0]['Tanggal']; ?></b></td>
	        </tr>
	        
			<tr>
	            <td class="title_table" width="150">Jatuh Tempo</td>
	            <td><b><?php echo $header[0]['JatuhTempo']; ?></b></td>
	        </tr>
			
			<tr>
	            <td class="title_table" width="150">Supplier</td>
	            <td><b><?php echo $header[0]['Nama']; ?></b></td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>  
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
								<th width="100"><center>Harga</center></th>
								<th width="100"><center>Total</center></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i=0;
							foreach($detail_list as $val)
							{
							?>
							<tr>
								<td align="center"><?php echo $val['PCode'];?></td>
								<td><?php echo $val['NamaLengkap'];?></td>
								<td align="right"><?php echo $val['Qty'];?></td>
								<td align="center"><?php echo $val['Satuan'];?></td>
								<td align="right"><?php echo number_format($val['Harga'],0);?></td>
								<td align="right"><?php echo number_format($val['Qty']*$val['Harga'],0);?></td>
							</tr>
							<?php
							$i++;							
							} 
							?>
							
							<tr>
							<td colspan="5" align="right">Sub Total</td>
							<td align="right"><?php echo number_format($total_harga_detail[0]['subtotal'],0); ?></td>
							</tr>
							
							<tr>
							<td colspan="5" align="right">Total PPN</td>
							<td align="right"><?php echo number_format(($header[0]['PPN']/100)*$total_harga_detail[0]['subtotal'],0); ?></td>
							</tr>
							
							<tr>
							<td colspan="5" align="right">Grand Total</td>
							<td align="right"><?php echo number_format($total_harga_detail[0]['subtotal']+(($header[0]['PPN']/100)*$total_harga_detail[0]['subtotal']),0); ?></td>
							</tr>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
			
			<tr>
	            <td colspan="100%" align="center">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="tutup()">Close<i class="entypo-cancel"></i></button>
                </td>
	        </tr>
	        
	    </table>
		
	</div>
</div>
</body>
</html>

            