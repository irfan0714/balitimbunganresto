<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/paging5.css" />
<script language="javascript" src="<?= base_url(); ?>/public/js/jquery.js"></script>

<body onLoad="key('<?= base_url(); ?>');">
    <table align='center' border='0'>
        <tr>
            <td>Bank Kartu</td>
			<td>:</td>
			<td> 
				<select name="bankkartu" style="width:125px;height:30px;font-size:15px" id="bankkartu" onchange="ganti_bank()" style="width:125px;">
                <option value="">-- Kartu --</option>
                <?php
							foreach($kartu as $val)
							{
								?><option value="<?php echo $val["id"]."#".$val["nama"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
				?>
			</select> 
			</td>
            </tr><tr>
			<td>Mesin EDC Bank</td>
			<td>:</td>
			<td> 
				<select name="edcbankkartu" style="width:125px;height:30px;font-size:15px" id="edcbankkartu" onchange="ganti_edc()" style="width:125px;">
                <option value="">-- EDC --</option>
                <?php
							foreach($edc as $val)
							{
								?><option value="<?php echo $val["id"]."#".$val["nama"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
				?>
			</select> 
			</td>
	
        </tr>
    </table>
<br><br>
<table align = 'center'>
    <tr><td><input type="button" value="Close" onclick = "closing()" style="width:100px;height:30px"></td></tr>
</table>
</body>

<script language="javascript">
    function closing()
    {
        window.close();
	}
	
	function ganti_bank(){
		bank = $("#bankkartu").val();
		a = bank.split('#');
		window.opener.$("#kartubankdebet").val(a[0]);
		window.opener.$("#namakartubankdebet").val(a[1]);
		window.opener.debetBank();
	}

	function ganti_edc(){
		bank = $("#edcbankkartu").val();
		a = bank.split('#');
		window.opener.$("#edcbankkartudebet").val(a[0]);
		window.opener.$("#namaedcbankkartudebet").val(a[1]);
		window.opener.EDCdebetBank();
	}
	
</script>