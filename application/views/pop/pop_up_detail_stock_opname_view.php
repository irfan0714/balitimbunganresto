<?php 
	$modul = "Detail Stock Opname";
	
	$pcodes = $this->uri->segment(4);
	//echo $pcodes;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
    <script>
        
        
         function tutup(total)
        {
        	
        	//pcodec = $("#pcodex").val();
            //window.opener.document.getElementById("v_QtyFisik_"+pcodec).value     = total;
            //window.opener.document.getElementById("v_QtyFisik_"+pcodec).focus();
            //window.opener.calculate(pcodec);
            self.close() ;
            return;
           
        }
		
		
		function pickThis(obj)
		{
			    base_url = $("#base_url").val();

			    objek = obj.id;
				id = parseFloat(objek.substr(4,objek.length-4));
				
				nososplit = $("#v_no_dok_so"+id).val();
				pcode = $("#v_pcode"+id).val();
				qty_fisik_new = $("#v_qtyfisik"+id).val();
				
				var r=confirm("Apakah Anda Yakin Ingin Edit Qyt Fisik Barang Tersebut?")
				if (r==true)
				{
					
					$('#save'+id).text('Updateing...');
					
					$.ajax({
			            url: "<?php echo site_url('pop/pop_up_detail_stock_opname/update_qtyfisik') ?>/" + nososplit+"/"+ pcode +"/"+ qty_fisik_new,
			            type: "GET",
			            dataType: "JSON",
			            success: function (data)
			            {
			                  if(data){
			                  	
			                  	
							  	alert("Update Berhasil.");
							  	$('#save'+id).text('Save');
							  	
							  	pcodec = $("#pcodex").val();
							  	total = $("#v_total").val();
					            window.opener.document.getElementById("v_QtyFisik_"+pcodec).value     = total;
					            window.opener.document.getElementById("v_QtyFisik_"+pcodec).focus();
					            window.opener.calculate(pcodec);
					            return;
							  }
						    		
			            }
			            ,
			            error: function (textStatus, errorThrown)
			            {
			                alert('Error get data');
			                $('#save'+id).text('Save');
			                
			            }
			        }
			        );
			        
			        
				}
				else
				{
			  		return false;
				}
			
		}
		
		
		
		function Hitung( obj) {
	
    	
                objek = obj.id;
			
                id = parseFloat(objek.substr(10, objek.length - 10));
				qty = parseFloat($("#v_qtyfisik" + id).val());             
                totalNetto();
           
       }
       
       function totalNetto()
	{
	    var lastRow = document.getElementsByName("v_qtyfisik[]").length;
	    var total = 0;//grand total
	    
	    //alert(lastRow+" - "+total+" - "+stotal);
	    for (index = 0; index < lastRow; index++)
	    {
	        indexs = index - 1;
	        nama = document.getElementsByName("v_qtyfisik[]");
	        temp = nama[index].id;
	        temp1x = nama[index].value;
	        if(temp1x==""){
				temp1=0;
			}else{
				temp1=parseFloat(nama[index].value);
			}
	        total += temp1;
	    }
	    $("#v_total").val(Math.round(total));

	}
		
	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">PCode</td>
	            <td><b><?php echo $header[0]['KodeBarang']; ?>
	            	<input type="hidden" name="pcodex" id="pcodex" value="<?php echo $header[0]['KodeBarang']; ?>">
	            </b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Nama Barang</td>
	            <td><b><?php echo $header[0]['NamaLengkap']; ?></b></td>
	        </tr>
	        
	        
	        <tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
								<th width="100"><center>Author</center></th>
								<th width="100"><center>No. Split SO</center></th>
							    <th width="100"><center>Qty Fisik</center></th>
								<th width="30"><center>&nbsp;</center></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i=1;
							$tot_qty =0;
							
							foreach($detail_list AS $val){
								
							?>
							<tr>
								<td align="center"><?php echo $val['Author'];?></td>
								<td align="center"><?php echo $val['NoDokumen'];?></td>
								<td align="right"><input style="text-align: right;" class="form-control-new" type="text" value="<?php echo $val['QtyFisik'];?>" name="v_qtyfisik[]" id="v_qtyfisik<?=$i;?>" size="10" onkeyup="Hitung(this)"></td>
								<input type="hidden" value="<?php echo $val['NoDokumen'];?>" name="v_no_dok_so" id="v_no_dok_so<?=$i;?>">
								<input type="hidden" value="<?=$header[0]['KodeBarang'];?>" name="v_pcode" id="v_pcode<?=$i;?>">
								<td align="center">
									<button type="button" name="save" id="save<?=$i;?>" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Save" title="" onclick="pickThis(this)" >
		                                Save
		                            </button>
								</td>
								
							</tr>
							<?php
							$tot_qty+=$val['QtyFisik'];
							$i++;							
							} 
							?>
							<tr>
								<td class="title_table" align="left" colspan="2">Total</td>
								<td class="title_table" align="right"><input style="text-align: right;" class="form-control-new" type="text" value="<?php echo $tot_qty;?>" name="v_total" id="v_total" size="10" readonly="readonly"></td>
								<td class="title_table" align="center">&nbsp;</td>
							</tr>
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
			
			<tr>
	            <td colspan="100%" align="center">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="tutup('<?php echo $tot_qty;?>')">Close<i class="entypo-cancel"></i></button>
                </td>
	        </tr>
	    <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">    
	    </table>
		
	</div>
</div>
            
</body>

</html>


        
            