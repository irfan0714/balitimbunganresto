<?php 
	$modul = "Search Item Barang";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

    <script>
        
        function CloseWindow(url)
		{
			window.close();
			//window.opener.location.reload();
		}
		
		function ambil_data(){
	
			url = "<?php echo site_url('pop/pop_up_item_barang/add_temp') ?>";
			$("#loading").css("display",""); 
			$.ajax({
            url: url,
            type: "POST",
            data: $('#theform').serialize(),
            dataType: "JSON",
            success: function (data)
            {
                if (data.status)
                {
                   //alert('Sukses');
                   $("#loading").css("display","none");
                   window.close(); 
                   window.opener.getData();                 
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
               

            }
        });
        
		}
		
	</script>
</head>
<body class="page-body skin-black">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<form method='POST'  name='search' action='<?=base_url();?>index.php/pop/pop_up_item_barang/search'>
			<div class="row">
				<div class="col-md-12">
					<b>Keyword</b>&nbsp;
	               	<input type="text" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" size="20">
	               	&nbsp;
					<span style="float: right;">
						<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
					</span>
				
				</div>
			</div>
		</form>
		
		<hr/>
		
		
	<form id="theform" method='POST'  name='search' action='<?=base_url();?>index.php/pop/pop_up_item_barang/add_temp'>
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead class="title_table">
					<tr>
						<th width="10">No</th>
		                <th><nobr>PCode</nobr></th>
		                <th>Nama Barang</th>
		                <th>Harga</th>
		        	</tr>
				</thead>
				<tbody>
				
				<?php
				if(count($item_barang)==0)
				{
					echo "<tr><td colspan='100%'><center>Tidak Ada Data</center></td></tr>";
				}
				
				$i=1;
				foreach($item_barang as $val)
				{
					$bgcolor = "";
                    if($i%2==0)
                    {
                        $bgcolor = "background:#E7E7E7;";
                    }
                    
					?>
					
						<tr id="<?php echo $i; ?>">
						<td><?php echo $i; ?></td>
						<td><?php echo $val["PCode"]; ?><input style="text-align: right" type="hidden" name="pcode[]" id="pcode<?=$i;?>" class="form-control-new" value="<?=$val["PCode"];?>" size="10"></td>
						<td><?php echo $val["NamaLengkap"]; ?><input style="text-align: right" type="hidden" name="nama_barang[]" id="nama_barang<?=$i;?>" class="form-control-new" value="<?=$val["NamaLengkap"];?>" size="10"></td>
						<td align="right"><input style="text-align: right" type="text" name="harga[]" id="harga<?=$i;?>" class="form-control-new" value="" size="10"></td>
					
					</tr>
					<?php
					$i++;
				}
				?>
             
				</tbody>
			</table> 
		</form>
		<br><br>
		</div>
		
		<div id="loading" style="display: none">
		<center>
		<h4><b>Sedang Proses...</b></h4>	
		</center>
		</div>
		<center>
		<input type="button" class="btn btn-danger btn-md md-new tooltip-primary" name="btn_close" value="Close" onclick="CloseWindow('<?php echo base_url(); ?>')">
		<input type="button" class="btn btn-green btn-md md-new tooltip-primary" name="btn_close" value="Ambil Data" onclick="ambil_data('<?php echo base_url(); ?>')">
		</center>

</div>
</body>
</html>