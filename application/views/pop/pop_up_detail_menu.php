<?php 
	$modul = "Menu";
	$UserLevel = $this->session->userdata('userlevel');
	$session_name = $this->session->userdata('username');
	$session_name;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/js.js"></script>
	<script language="javascript" src="<?=base_url();?>public/js/reservasi.js"></script>

    <!--//[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<script>
function getGrandTotal(elm)
{
	var total = document.getElementById("gtotal").value;
	window.opener.document.forms["theform"]["Total"].value = format(total);
	window.opener.document.forms["theform"]["hidden_total"].value = total;
	window.opener.getTambahanBiaya();
	return; 
}
</script>

<body class="page-body skin-black">

<div class="row">
<?if($booking=="Y"){
	$warna="primary";
	$bacaan = "Simpan Menu Booking";
}else{
	$warna="info";
	$bacaan = "Simpan Menu BEO";
}?>
<?php if($session_name=="bsp1412" || $session_name=="mechael0101" || $session_name=="tonny1205" ){?>
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><h3>Add <?php echo $modul." ".$nodokumen; ?></h3></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/reservasi/save_detail_menu'>
		
	    <table class="table table-bordered responsive">                        
	        <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	        <input type='hidden' name="jml" id="jml" value="1">
	        <input type='hidden' name="nodokumen" id="nodokumen" value="<?=$nodokumen;?>">
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
						<thead>
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Menu</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="50"><center>Diskon (%)</center></th>
							    <th width="50"><center>Harga</center></th>
							    <th width="50"><center>Total</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-<?=$warna;?> btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="addRow()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-<?=$warna;?> btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari Menu" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" id="v_qty<?php echo $no;?>" value="0" onBlur="toFormat('v_qty<?php echo $no;?>');" onKeyUp="getTotal(v_qty<?php echo $no;?>),toFormat('v_qty<?php echo $no;?>')" style="text-align: right;"/>
									<input type="hidden" class="form-control-new" name="v_hidden_qty[]" id="v_hidden_qty<?php echo $no;?>" value="0" style="text-align: right;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_diskon[]" id="v_diskon<?php echo $no;?>" value="0" onBlur="toFormat('v_diskon<?php echo $no;?>');" onKeyUp="getDiskon(v_diskon<?php echo $no;?>),toFormat('v_diskon<?php echo $no;?>')" style="text-align: right;"/>
									<input type="hidden" class="form-control-new" name="v_hidden_diskon[]" id="v_hidden_diskon<?php echo $no;?>" value="0" style="text-align: right;"/>
				                </td>
								<td align="right">
				                	<input type="text" class="form-control-new" name="v_harga[]" id="v_harga<?php echo $no;?>" value="0" onBlur="toFormat('v_harga<?php echo $no;?>');" onKeyUp="getTotal(v_harga<?php echo $no;?>),toFormat('v_harga<?php echo $no;?>')" style="text-align: right;"/>
									<input type="hidden" class="form-control-new" name="v_hidden_harga[]" id="v_hidden_harga<?php echo $no;?>" value="0" style="text-align: right;"/>
				                </td>
								<td align="right">
				                	<input type="text" class="form-control-new" name="v_total[]" id="v_total<?php echo $no;?>" value="0" onBlur="toFormat('v_total<?php echo $no;?>');"style="text-align: right;" readonly />
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-<?=$warna;?> btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
						</tbody>
					</table>
					<table class="table table-bordered responsive">
						<tr>
							<td align="right" colspan="4"><b>Grand Total</b></td>
							<td align="right" width="100">
								<input type="text" class="form-control-new" name="v_grand_total" onblur="toFormat('v_grand_total')" id="v_grand_total" value="0" style="text-align: right;" readonly />
								<input type="hidden" class="form-control-new" name="v_hidden_grand_total" id="v_hidden_grand_total" value="0" style="text-align: right;"/>
							</td>
							<td align="right" width="100"></td>
						</tr>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-<?=$warna;?> btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan"><?=$bacaan;?><i class="entypo-check"></i></button>
		           	<button type="button" class="btn btn-red btn-icon btn-sm icon-left" name="btn_cancel" id="btn_cancel"  value="Batal" onclick="formReset(this);">Batal<i class="entypo-cancel"></i></button>
		           	<?php if($booking=="T"){?>
		           	<button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="getGrandTotal(this), window.opener.getDPBayar(),closeWindow(this)">Keluar<i class="entypo-cancel-circled"></i></button>
	            	<?}else{?>
					<button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="getGrandTotal(this),window.opener.getDPBayar_booking(),closeWindow(this)">Keluar<i class="entypo-cancel-circled"></i></button>	
					<?php }?>
	            </td>
	        </tr>
			
			<tr>
				<td colspan="100%" align="center">
					<table class="table table-bordered responsive">
						<thead>
							<tr>
								<th width="100"><center>PCode</center></th>
								<th width="200"><center>Nama Menu</center></th>               
								<th width="100"><center>Qty</center></th>
								<th width="100"><center>Diskon (%)</center></th>
								<th width="100"><center>Harga</center></th>
								<th width="100"><center>Total</center></th>
								<th width="100"><center>Action</th>
							</tr>
						</thead>
						<?php 
						$gtotal = 0;
						foreach($detailMenu as $rows){
						//$total = $rows['Qty']*$rows['Harga'];	
						//$gtotal = $gtotal + $total;
						/**
						* perhitungan ini sudah di masterbarang nya
						$harga_sudah_pajak_5 = $rows['Harga'] + ($rows['Harga']*(5/100));
						$harga_sudah_pajak_10 = $harga_sudah_pajak_5 + ($harga_sudah_pajak_5*(10/100));
						$harga_fix = $harga_sudah_pajak_10;
						*/
						$harga_fix = $rows['Harga'];
						
							if($rows['Diskon']==0){
								$total = $rows['Qty']*$harga_fix;
							}else{
								$total = ($rows['Qty']*$harga_fix) - (($rows['Qty']*$harga_fix)*$rows['Diskon']/100);
							}
						
						$gtotal = $gtotal + $total;
						
						?>
						<tr>
							<td><?=$rows['PCode'];?></td>
							<td align="center"><?=$rows['NamaBarang'];?></td>
							<td align="center"><?=number_format($rows['Qty']);?></td>
							<td align="center"><?=number_format($rows['Diskon']);?></td>
							<td align="right"><?=number_format($harga_fix);?></td>
							<td align="right"><?=number_format($total);?></td>
							<td align="center">
								<a href="<?php echo base_url(); ?>index.php/pop/pop_up_detail_menu/hapus_trans_detail/<?php echo $rows["sid"]; ?>/<?php echo $nodokumen;?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title=""><i class="entypo-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
						<tr>
							<td colspan="4">Grand Total</td>
							<td align="right"><?=number_format($gtotal);?></td>
							<td></td>
						</tr>
						<input type='hidden' name="gtotal" id="gtotal" value="<?=$gtotal;?>">
					</table>
				</td>
			</tr>
	        
	    </table>
	    
	    </form>
        
	</div>
<?php }else{ ?>
<div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-eye"></i>View <?php echo $modul; ?></strong></li>
		</ol>
		<table class="table table-bordered responsive">
			<thead>
				<tr>
					<th width="100"><center>PCode</center></th>
					<th width="200"><center>Nama Menu</center></th>               
					<th width="100"><center>Qty</center></th>
					<th width="100"><center>Harga</center></th>
					<th width="100"><center>Total</center></th>
				</tr>
			</thead>
			<?php 
			$gtotal = 0;
			foreach($detailMenu as $rows){
			$total = $rows['Qty']*$rows['Harga'];	
			$gtotal = $gtotal + $total;
			?>
			<tr>
				<td><?=$rows['PCode'];?></td>
				<td align="center"><?=$rows['NamaBarang'];?></td>
				<td align="center"><?=number_format($rows['Qty']);?></td>
				<td align="right"><?=number_format($rows['Harga']);?></td>
				<td align="right"><?=number_format($total);?></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="3"></td>
				<td align="center"><b>Grand Total</b></td>
				<td align="right"><?=number_format($gtotal);?></td>
			</tr>
			<tr>
	            <td colspan="100%" align="center">
		           	<button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="getGrandTotal(this), window.opener.getDPBayar(),closeWindow(this)">Keluar<i class="entypo-cancel-circled"></i></button>
	            </td>
	        </tr>
			<input type='hidden' name="gtotal" id="gtotal" value="<?=$gtotal;?>">
		</table>
</div>
<?php } ?>
</div>
</body>
</html>