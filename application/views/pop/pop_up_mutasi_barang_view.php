<?php 
	$modul = "Detail GSA";
	
	$empid = $this->uri->segment(4);
	$tgl = $this->uri->segment(5);
	$pisah = explode("-",$tgl);
	$thn = $pisah[0];
	$bln= $pisah[1];
	$hri = $pisah[2];
	$tgl_=$hri."-".$bln."-".$thn;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
    <script>
        
        
        function tutup()
        {
            self.close() ;
            return; 
        }
		
		
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">PCode</td>
	            <td><b><?php echo $header[0]['KodeBarang']; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Nama Barang</td>
	            <td><b><?php echo $header[0]['NamaLengkap']; ?></b></td>
	        </tr>
	        
	        
	        <tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
       		 				<tr>
								<th colspan="4">Saldo Awal</th>
							    <th align="center"><center><?php echo $saldo[0]['saldo_akhir']; ?></center></th>
							</tr>
							<tr>
								<th width="100"><center>No. Transaksi</center></th>
								<th width="100"><center>Jenis</center></th>
							    <th width="100"><center>Tanggal</center></th>
							    <th width="100"><center>Masuk</center></th>
								<th width="100"><center>Keluar</center></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i=0;
							$tot_masuk =0;
							$tot_keluar=0;
							
							foreach($detail_list AS $val){
								
								$jenistransaksi = '-';
								switch ($val['KdTransaksi']) {
    								case 'RG':
        								$jenistransaksi = 'Pembelian';
        								break;
        							case 'MX':
        								$jenistransaksi = 'Produksi';
        								break;
        							case 'SR':
        								$jenistransaksi = 'Sales Return';
        								break;
        							case 'DL':
        								$jenistransaksi = 'Penerimaan Lain';
        								break;
        							case 'MC':
        								$jenistransaksi = 'Konfirmasi Mutasi';
        								break;
        							case 'MP':
										if($val['Masuk']>0)
											$jenistransaksi = 'Penerimaan Paket';
										else
											$jenistransaksi = 'Pengeluaran Paket';
										break;
        							case 'PL':
        								$jenistransaksi = 'Pengeluaran Lain';
        								break;
        							case 'MM':
        								$jenistransaksi = 'Mutasi Gudang';
        								break;
        							case 'SO':
        								$jenistransaksi = 'Opname';
        								break;
        							case 'FG':
        								$jenistransaksi = 'Hasil Produksi';
        								break;
        							case 'R':
        								$jenistransaksi = 'Sales';
        								break;
        							case 'RB':
        								$jenistransaksi = 'Retur Pembelian';
        								break;
								} 
							?>
							<tr>
								<td align="center"><?php echo $val['NoTransaksi'];?></td>
								<td align="center"><?php echo $jenistransaksi;?></td>
								<td align="center"><?php echo $val['Tanggal'];?></td>
								<td align="right"><?php echo $val['Masuk'];?></td>
								<td align="right"><?php echo $val['Keluar'];?></td>
								
							</tr>
							<?php
							$tot_masuk+=$val['Masuk'];
							$tot_keluar+=$val['Keluar'];
							$i++;							
							} 
							?>
							<tr>
								<td class="title_table" align="left" colspan="3">Total</td>
								<td class="title_table" align="center"><?php echo $tot_masuk;?></td>
								<td class="title_table" align="center"><?php echo $tot_keluar;?></td>
							</tr>
							<tr>
								<td class="title_table" align="left" colspan="4">Saldo Akhir</td>
								<td class="title_table" align="center"><?php echo ($saldo[0]['saldo_akhir']+$tot_masuk)-$tot_keluar;?></td>
							</tr>
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
			
			<tr>
	            <td colspan="100%" align="center">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="tutup()">Close<i class="entypo-cancel"></i></button>
                </td>
	        </tr>
	    <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">    
	    </table>
		
	</div>
</div>
</body>

</html>

            