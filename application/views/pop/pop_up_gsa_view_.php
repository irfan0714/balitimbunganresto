<?php 
	$modul = "Detail GSA";
	
	$empid = $this->uri->segment(4);
	$thn = $this->uri->segment(5);
	$bln = $this->uri->segment(6);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
    <script>
        
        
        function tutup()
        {
            self.close() ;
            return; 
        }
		
		
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<table class="table table-bordered responsive">
			
			<tr>
	            <td class="title_table" width="150">Nama</td>
	            <td><b><?php echo $header[0]['employee_name']; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Jabatan</td>
	            <td><b><?php echo $header[0]['jabatan_name']; ?></b></td>
	        </tr>
	        
	        
	        <tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
								<th width="100"><center>Tanggal</center></th>
								<th width="100"><center>No. GSA</center></th>
							    <th width="100"><center>No. Stiker</center></th>
							    <th width="100"><center>Beauty Tour</center></th>
								<th width="100"><center>Coffee Tour</center></th>
								<th width="100"><center>Chamber</center></th>
								<th width="100"><center>Standby OH</center></th>
								<th width="100"><center>Standby Resto</center></th>
								<th width="100"><center>Standby BE</center></th>
								<th width="100"><center>Jumlah</center></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$tot=0;
							$i=0;
							foreach($detail_list2 as $val)
							{
							
							if($val['beauty_tour']=='1'){
							$persen1 = 0.50;
							}else{
							$persen1 = 0.00;
							}
							
							if($val['coffee_tour']=='1'){
							$persen2 = 0.50;
							}else{
							$persen2 = 0.00;
							}

							
							if($val['chamber']=='1'){
							$persen3 = 0.50;
							}else{
							$persen3 = 0.00;
							}
							
							if($val['standby_oh']=='1'){
							$persen_1 = 0.25;
							}else{
							$persen_1 = 0.00;
							}
							
							if($val['standby_resto']=='1'){
							$persen_2 = 0.25;
							}else{
							$persen_2 = 0.00;
							}
							
							if($val['standby_be']=='1'){
							$persen_3 = 0.25;
							}else{
							$persen_3 = 0.00;
							}
							
							
							$jmlh = $persen1+$persen2+$persen3+$persen_1+$persen_2+$persen_3;
							?>
							<tr>
							    <td align="center"><?php echo $val['Tanggal'];?></td>
								<td align="center"><?php echo $val['gsa_id'];?></td>
								<td align="center"><?php echo $val['NoStiker'];?></td>
								<td align="center"><?php echo $persen1;?></td>
								<td align="center"><?php echo $persen2;?></td>
								<td align="center"><?php echo $persen3;?></td>
								<td align="center"><?php echo $persen_1;?></td>
								<td align="center"><?php echo $persen_2;?></td>
								<td align="center"><?php echo $persen_3;?></td>
								<td align="center"><?php echo $jmlh;?></td>
								
							</tr>
							<?php
							$tot+=$jmlh;
							$i++;							
							} 
							?>
							
							<tr>
							<td colspan="9" align="right">Total</td>
							<td align="center"><?php echo $tot; ?></td>
							</tr>
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
			
			<tr>
	            <td colspan="100%" align="center">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="tutup()">Close<i class="entypo-cancel"></i></button>
                </td>
	        </tr>
	    <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">    
	    </table>
		
	</div>
</div>
</body>

</html>

            