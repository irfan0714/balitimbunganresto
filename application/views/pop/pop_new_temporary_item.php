<?php
	$modul = "Search Kode Barang";

	$v_nilai = $this->uri->segment(4);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>

			 function ganti_harga(baseurl,pcode){

				 harga_baru = $('#harga_baru'+pcode).val();
				 $.ajax({
						url: baseurl+"index.php/pop/pop_new_temporary_item/simpan/",
						data: {pcode:pcode,harga1c:harga_baru},
						type: "POST",
						dataType: 'json',
						success: function(res)
						{
							alert('Sukses pcode '+pcode+' dengan Harga IDR '+harga_baru);
							window.opener.location.reload();
							return false;
						},
						error: function(e)
						{
							alert(e);
						}
				 });

			 }

	</script>
</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">

    <div class="main-content">

		<form method="POST"  name="search" action='<?=base_url();?>index.php/pop/pop_up_permintaan_menu/search'>
			<input type="hidden" name="v_nilai" id="v_nilai" value="<?php echo $v_nilai; ?>"/>
			<div class="row">
				<div class="col-md-12">
					<b>Keyword</b>&nbsp;
	               	<input type="text" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" size="20">
	               	&nbsp;

					<span style="float: right;">
						<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
					</span>

				</div>
			</div>
		</form>

		<hr/>

		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">

			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th width="10">No</th>
		                <th>Kode Menu</th>
		                <th>Nama Menu</th>
		                <th>Harga</th>
										<th th width="50">Action</th>
		        	</tr>
				</thead>
				<tbody>

				<?php
				if(count($barangdata)==0)
				{
					echo "<tr><td colspan='100%'><center>Tidak Ada Data</center></td></tr>";
				}

				$i=1;
				foreach($barangdata as $val)
				{


					$bgcolor = "";
                    if($i%2==0)
                    {
                        $bgcolor = "background:#E7E7E7;";
                    }

                    if($val['Service_charge']>0){
						$hrga = $val['Harga1c'] + ($val['Harga1c']*5/100) + (($val['Harga1c'] + ($val['Harga1c']*5/100))*10/100);
					}else{
						$hrga = $val['Harga1c'];
					}

                    /*$serv_incas = $val["Harga1c"] + ($val["Harga1c"]*5/100);
                    $ppn = $serv_incas + ($serv_incas*10/100);
                    $hrga = $ppn;*/

					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $val["PCode"]; ?></td>
						<td><?php echo $val["NamaLengkap"]; ?></td>
						<td width="80"><input type="text" style="text-align:right;" class="form-control-new" name="harga_baru" id="harga_baru<?=$val["PCode"];?>" value="<?php echo $hrga; ?>" size="50"/></td>
						<td id="action<?=$i;?>" onclick="ganti_harga('<?=base_url(); ?>','<?=$val["PCode"];?>')"><b>Change</b></td>
					</tr>
					<?php
					$i++;
				}
				?>

				</tbody>
			</table>

			<div class="row">
				<div class="col-xs-6 col-left">
					<div id="table-2_info" class="dataTables_info">&nbsp;</div>
				</div>
				<div class="col-xs-6 col-right">
					<div class="dataTables_paginate paging_bootstrap">
						<?php echo $pagination; ?>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>
</body>
</html>
