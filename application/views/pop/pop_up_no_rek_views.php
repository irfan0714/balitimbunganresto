<?php 
	$modul = "Search Nama Rekening";
	
	$v_urut = $this->uri->segment(4);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
    <script>
        
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
        
        function get_choose(NoRek,NmRek)
        {
            window.opener.document.forms[0]["v_NamaRekening"+<?php echo $v_urut; ?>].value = NoRek + ' - '+NmRek;
			window.opener.document.forms[0]["v_KdRekening"+<?php echo $v_urut; ?>].value = NoRek;
            self.close() ;
            return; 
        }
		
		
		
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead class="title_table">
					<tr>
						<th width="10">No</th>
		                <th><center>No Rekening</center></th>
		                <th><center>Nama Rekening</center></th>
		        	</tr>
				</thead>
				<tbody>
				
				<?php
				if(count($data)==0)
				{
					echo "<tr><td colspan='100%'><center>Tidak Ada Data</center></td></tr>";
				}
				
				$i=1;
				foreach($data as $val)
				{
					$bgcolor = "";
                    if($i%2==0)
                    {
                        $bgcolor = "background:#E7E7E7;";
                    }
                    
					?>
					<tr onclick="get_choose('<?php echo trim($val["KdRekening"]); ?>','<?php echo trim($val["NamaRekening"]); ?>')" id="<?php echo $i; ?>">
						<td><?php echo $i; ?></td>
						<td><?php echo $val["KdRekening"]; ?></td>
						<td><?php echo $val["NamaRekening"]; ?></td>
					</tr>
					<?php
					$i++;
				}
				?>
             
				</tbody>
			</table> 

			<div class="row">
				<div class="col-xs-6 col-left">
					<div id="table-2_info" class="dataTables_info">&nbsp;</div>
				</div>
				<div class="col-xs-6 col-right">
					<div class="dataTables_paginate paging_bootstrap">
						<?php echo $pagination; ?>
					</div>
				</div>
			</div>	
		
		</div>
		
	</div>
</div>
</body>
</html>

            