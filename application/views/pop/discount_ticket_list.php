
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>GL NPM</title>
    <link rel="shortcut icon" href="<?= base_url();?>public/images/Logosg.png" >
    
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
    
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/paging5.css" />
	<script language="javascript" src="<?= base_url(); ?>/public/js/jquery.js"></script>    
    <script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>

</head>

<body onLoad="key('<?= base_url(); ?>');">
<div class="panel-body">
	<div class="row">
        <div class="col-md-4">
            <b>Jenis Discount</b>&nbsp;&nbsp;&nbsp;
            <select class="form-control-new" name="jenis_discount" id="jenis_discount" onchange="ganti_jenis_discount()" style="width:125px;">
                <!--<option value="special">Special</option>-->                
                <option value="card">Card</option>
                <option value="members">Member Free Ticket</option>
			</select>  
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <input type="button" class="btn btn-success" value="Close" onclick = "closing()" style="width:100px;height:30px">
        </div>
      
    </div>
	<br>
	
    <table class="table responsive" id="discount_khusus" style="display: none;">
        <tr>
            <td>Kode Discount : <input class="form-control-new" type='password' size='20' name='KodeDiscount' id='KodeDiscount' size="20" maxlength="20"
                           onKeyDown="getCodeDiscount(event, '<?= base_url(); ?>', 'insert')" style="width:125px;height:30px;font-size:15px"></td>
		    <td>Total Belanja :
			<input type="text" class="InputAlignRight" readonly id="tbelanja" name="tbelanja" size="20" style="width:125px;height:30px;border:none;font-size:25px" value=0></td>
			<td>Total Discount :
			<input type="text" class="InputAlignRight" readonly id="tdiscount" name="tdiscount" size="20" style="width:125px;height:30px;border:none;font-size:25px" value=0></td>
	
        </tr>
    </table>
    
    <table class="table responsive" id="discount_anggota" style="display: none;">
            <tr>
	            <td class="title_table">Nomor Anggota</td>
	            <td><input type="text" class="form-control-new" value="" name="no_anggota" id="no_anggota" maxlength="255" size="30" onblur="cek_anggota()" onkeydown="cek(event, '1', '<?= base_url(); ?>')"></td>
	        </tr>
	        <tr>
	            <td class="title_table">Nomor Handphone</td>
	            <td><input type="text" class="form-control-new" value="" name="no_hp" id="no_hp" maxlength="255" size="30" onblur="cek_hp_anggota()" onkeydown="cek_hp(event, '1', '<?= base_url(); ?>')"></td>
	        </tr>
	        <tr>
	            <td class="title_table">Nama Anggota</td>
	            <td><input readonly="readonly" type="text" class="form-control-new" value="" name="nama_anggota" id="nama_anggota" maxlength="255" size="30"></td>
	        </tr>
	        <tr>
	            <td class="title_table">Jumlah Free Ticket</td>
	            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="jml_free_ticket" id="jml_free_ticket" maxlength="255" size="30"></td>
	        </tr>	          
    </table>
    
    <table class="table responsive" id="discount_kartu" style="display: ;">
            
            <tr>
	            <td class="title_table">Nomor Kartu</td>
	            <td><input type="text" class="form-control-new" value="" name="no_card" id="no_card" maxlength="255" size="30"></td>
	        </tr>
	        <tr>
	            <td class="title_table">Nama</td>
	            <td><input type="text" class="form-control-new" value="" name="nama_user_card" id="nama_user_card" maxlength="255" size="30"></td>
	        </tr>
	        <tr>
	            <td class="title_table">Jenis Kartu</td>
	            <td>
	            	<select class="form-control-new" name="jenis_card" id="jenis_card" onblur="hitung_disc_card()" onchange="hitung_disc_card()" style="width:180px;">
		                <option value="">-- Pilih Kartu --</option>
		                <?php
	            		foreach($jenis_kartu as $val)
	            		{
							?><option value="<?php echo $val['KdGroupDisc']; ?>"><?php echo $val['NamaGroupDisc']; ?></option><?php
						}
	            		?>
					</select>
					<span id="span_loading" style=" display: none;"><img src="<?= base_url();?>public/images/ajax-image.gif"/></span>
	            </td>
	        </tr>
	        
	        <tr style="display: none;">
	            <td class="title_table">Total Belanja</td>
	            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_sales_card" id="total_sales_card" maxlength="255" size="30"></td>
	        </tr>
	        <tr style="display: ;">
	            <td class="title_table" style="display: ;">% Discount Card</td>
	            <td style="display: ;"><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="persen_discount_card" id="persen_discount_card" maxlength="255" size="30"></td>
	        </tr>
	        <tr style="display: none;">
	            <td class="title_table" style="display: none;">Total Discount</td>
	            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_discount_card" id="total_discount_card" maxlength="255" size="30"></td>
	        </tr>
	        <tr style="display: none;">
	            <td class="title_table" style="display: none;">Sisa</td>
	            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="sisa_card" id="sisa_card" maxlength="255" size="30">
	            	<span id="checklist2" style="display: none;"><img src="<?= base_url();?>public/images/accept.png"/></span>
	            </td>
	        </tr>
    </table>
    
<br>
<table id="tombol_hitung" class="table responsive" style="display: none;">
    <tr><td align="center"><input type="button" class="btn btn-info" value="Hitung" onclick = "hitung_lagi()" style="width:100px;height:30px"></td></tr>
</table>

</div>
</body>
</html>

<script language="javascript">
    function closing()
    {
        window.close();
    }
	
	function key(url) {
        document.getElementById('KodeDiscount').focus();
		total_biaya = window.opener.$("#total_biaya").val();
		$("#tbelanja").val(total_biaya);
		window.opener.$("#pilihan").val("discount");
		
		iduserdiscount = window.opener.$("#iduserdiscount").val();
		id_discount = window.opener.$("#id_discount").val();
	}
	
	function getCodeDiscount(e,url,action)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			//alert("masuk");
			if(action == 'insert')
			{
				VCode = document.getElementById('KodeDiscount').value;
													//alert(PCode);
				//document.getElementById('temp_pos2').style.display = "block";
				//document.getElementById('temp_pos1').style.display = "none";
				vcode0 = VCode.replace('*','~');
				//alert(vcode0);
				
				if (vcode0 !== "") {
					$.ajax({
						type: "POST",
						url: url+"index.php/pop/discount/Detaildiscount/"+vcode0,
						success: function(msg){
							//alert(msg);alert(vcode0);

							if(msg=="salah"){
								alert("Kode Discount Salah !!!");
							}else{
								var jsdata = msg;
								eval(jsdata);
								vuserdiscount = datajson[0].username;
								vmaxdisc = datajson[0].maxdisc;
								
								nilaidiscount = window.opener.$("#id_discount").val();
								window.opener.$("#jenis_discount").val($("#jenis_discount").val());			
								
								discount = window.opener.$("#discount_bayar").val();
								total_biaya = window.opener.$("#total_biaya").val();
								$("#tdiscount").val(discount);
								$("#tbelanja").val(total_biaya);
								//alert(nilaidiscount);
								//alert(vmaxdisc);
								//alert(nilaidiscount>vmaxdisc);
								nilaidiscount = parseFloat(nilaidiscount);
								vmaxdisc = parseFloat(vmaxdisc);
								
								batal = 0;
								if(nilaidiscount>vmaxdisc)
								{
								   batal = 1;
								   alert("Discount melebihi plafond");
							    }
								if(batal==0)
								{
								    window.opener.HitungNetto();
								    discount = window.opener.$("#discount_bayar").val();
									total_biaya = window.opener.$("#total_biaya").val();
									$("#tdiscount").val(discount);
									$("#tbelanja").val(total_biaya);
									window.opener.$("#iduserdiscount").val(vuserdiscount);	
									window.opener.$("#iddiscountasli").val(nilaidiscount);
								}else
								{
								   document.getElementById('KodeDiscount').value = "";
								   document.getElementById('KodeDiscount').focus();
								}
							}
						}
					});
				}
			}
		}
	}
	
	function ganti_jenis_discount(){
		
		jenis = $("#jenis_discount").val();
		if(jenis=="special"){
			$("#discount_khusus").css("display","");
			$("#discount_anggota").css("display","none");
			$("#discount_kartu").css("display","none");
			document.getElementById('KodeDiscount').focus();
		}else if(jenis=="members"){
			$("#discount_khusus").css("display","none");
			$("#discount_anggota").css("display","");
			$("#discount_kartu").css("display","none");
			$('[name="total_sales"]').val($("#tbelanja").val());
			document.getElementById('no_anggota').focus();
		}else if(jenis=="card"){
			$("#discount_khusus").css("display","none");
			$("#discount_kartu").css("display","");
			$("#discount_anggota").css("display","none");
			$('[name="total_sales_card"]').val($("#tbelanja").val());
			document.getElementById('no_card').focus();
		}
		
		
	}
	
	function cek(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			nomor = $("#no_anggota").val();
			$.ajax({
	            url: "<?php echo site_url('pop/discount_ticket/ajax_edit') ?>/" + nomor,
	            type: "GET",
	            dataType: "JSON",
	            success: function(res)
	            {
	            	  if(res=="NO"){
	            	  		  alert("Kartu Tidak Terdaftar.");	
	            	  		  $("#no_anggota").val('');		
	            	  		  $("#no_hp").val('');
	            	  		  $("#nama_anggota").val('');
	            	  		  $("#jml_free_ticket").val('');					  	
					  	      $("#no_anggota").focus();
					  	      return false;
					  }else{
					  		  
					  	      window.opener.$("#id_discount").val(0);
					  	      $('[name="no_hp"]').val(res.NoHP1);
			            	  $('[name="nama_anggota"]').val(res.NamaMember);
			            	  $('[name="jml_free_ticket"]').val(res.FreeTicket);
			            	  window.opener.$("#member").val(res.KdMember);
			            	  window.opener.$("#id_member").val(res.KdMember);
			            	  window.opener.cekFreeTicket('<?php echo base_url();?>');
			            	  	
	                   }
	            }
	        }
	        );
		}
	}
	
	function cek_hp(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			nomor = $("#no_hp").val();
			$.ajax({
	            url: "<?php echo site_url('pop/discount_ticket/ajax_edit_hp') ?>/" + nomor,
	            type: "GET",
	            dataType: "JSON",
	            success: function(res)
	            {
	            	  if(res=="NO"){
	            	  		  alert("Kartu Tidak Terdaftar.");	
	            	  		  $("#no_anggota").val('');		
	            	  		  $("#no_hp").val('');
	            	  		  $("#nama_anggota").val('');
	            	  		  $("#jml_free_ticket").val('');					  	
					  	      $("#no_anggota").focus();
					  	      return false;
					  }else{
					  		  
					  	      window.opener.$("#id_discount").val(0);
					  	      $('[name="no_anggota"]').val(res.KdMember);
			            	  $('[name="nama_anggota"]').val(res.NamaMember);
			            	  $('[name="jml_free_ticket"]').val(res.FreeTicket);
			            	  window.opener.$("#member").val(res.KdMember);
			            	  window.opener.$("#id_member").val(res.KdMember);
			            	  window.opener.cekFreeTicket('<?php echo base_url();?>');
			            	  	
	                   }
	            }
	        }
	        );
		}
	}
	
	function cek_anggota() {
		    nomor = $("#no_anggota").val();
			$.ajax({
	            url: "<?php echo site_url('pop/discount_ticket/ajax_edit') ?>/" + nomor,
	            type: "GET",
	            dataType: "JSON",
	            success: function(res)
	            {
	            	  if(res=="NO"){						  	
					  	      alert("Kartu Tidak Terdaftar.");	
	            	  		  $("#no_anggota").val('');		
	            	  		  $("#no_hp").val('');
	            	  		  $("#nama_anggota").val('');
	            	  		  $("#jml_free_ticket").val('');					  	
					  	      $("#no_anggota").focus();
					  	      return false;
					  }else{
					  		  
					  	      window.opener.$("#id_discount").val(0);
					  	      $('[name="no_hp"]').val(res.NoHP1);
			            	  $('[name="nama_anggota"]').val(res.NamaMember);
			            	  $('[name="jml_free_ticket"]').val(res.FreeTicket);
			            	  window.opener.$("#member").val(res.KdMember);
			            	  window.opener.$("#id_member").val(res.KdMember);
			            	  window.opener.cekFreeTicket('<?php echo base_url();?>');
			            	  	
	                   }
	            }
	        }
	        );
		
	}
	
	function cek_hp_anggota() {
		    nomor = $("#no_hp").val();
			$.ajax({
	            url: "<?php echo site_url('pop/discount_ticket/ajax_edit_hp') ?>/" + nomor,
	            type: "GET",
	            dataType: "JSON",
	            success: function(res)
	            {
	            	  if(res=="NO"){						  	
					  	      alert("Kartu Tidak Terdaftar.");	
	            	  		  $("#no_anggota").val('');		
	            	  		  $("#no_hp").val('');
	            	  		  $("#nama_anggota").val('');
	            	  		  $("#jml_free_ticket").val('');					  	
					  	      $("#no_anggota").focus();
					  	      return false;
					  }else{
					  		  
					  	      window.opener.$("#id_discount").val(0);
					  	      $('[name="no_anggota"]').val(res.KdMember);
			            	  $('[name="nama_anggota"]').val(res.NamaMember);
			            	  $('[name="jml_free_ticket"]').val(res.FreeTicket);
			            	  window.opener.$("#member").val(res.KdMember);
			            	  window.opener.$("#id_member").val(res.KdMember);
			            	  window.opener.cekFreeTicket('<?php echo base_url();?>');
			            	  	
	                   }
	            }
	        }
	        );
		
	}
	
	function hitung_point(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			jml_point = parseFloat($("#jml_point").val());
			potongan_discount = parseFloat($("#potongan_discount").val());
			if(jml_point>=potongan_discount && jml_point>0){
				window.opener.$("#bayar_point").val(potongan_discount);	
			    window.opener.SetKembali();
			    $("#checklist").css("display","");
			}else{
				alert('Jumlah Point Bayar Melebihi Jumlah Point Yang Ada.');
				$("#potongan_discount").val('');
				$("#potongan_discount").focus();
				return false;
			}
			
			
		}		
	}
	
	function hitung_point2() {
		
		    jml_point = parseFloat($("#jml_point").val());
			potongan_discount = parseFloat($("#potongan_discount").val());
			if(jml_point>=potongan_discount && jml_point>0){
				window.opener.$("#bayar_point").val(potongan_discount);	
			    window.opener.SetKembali();
			    $("#checklist").css("display","");
			}else{
				alert('Jumlah Point Bayar Melebihi Jumlah Point Yang Ada.');
				$("#potongan_discount").val('');
				$("#potongan_discount").focus();
				return false;
			}
				
	}
	
	function hitung_disc_card(){
		 jenis_card = $("#jenis_card").val();
		 no_card = $("#no_card").val();
		 nama_user_card = $("#nama_user_card").val();
		 total_belanja = window.opener.$("#TotalNettoHidde").val();
		 
		 if(no_card==""){
		 	alert("No Kartu Belum Di Input.");
		 	$('[name="jenis_card"]').val('');
		 	$("#no_card").focus();
		 	return false;
		 }else if(nama_user_card==""){
		 	alert("Nama Pengguna Kartu Belum Di Input.");
		 	$('[name="jenis_card"]').val('');
		 	$("#nama_user_card").focus();
		 	return false;
		 }else{
		 	//titik
			$.ajax({
	            url: "<?php echo site_url('pop/discount_ticket/card') ?>/" + jenis_card,
	            type: "GET",
	            dataType: "JSON",
	            success: function(res)
	            {
	            	if(res=="NO"){
					   alert("Kartu Tidak Berlaku.");
					   $("#no_card").val('');
					   $("#nama_user_card").val('');					   
					   $("#no_card").focus();
					   
					   $('[name="persen_discount_card"]').val(0);
						window.opener.$("#id_discount").val(0);
						window.opener.hitung_ticket()
						      
					   return false;
					}else{
						// console.log(total_belanja);
						if((res.KdGroupDisc=="9" || res.KdGroupDisc=="3") && (parseInt(total_belanja)*1)<(parseInt(res.MinimumBelanja)*1)){ 
						  		alert("Minimum Total Belanja Menggunakan Kartu Ini Adalah Rp."+res.MinimumBelanja);
							   $("#no_card").val('');
							   $("#nama_user_card").val('');					   
							   $("#no_card").focus();
						  		return false;
						}else{
						      $('[name="persen_discount_card"]').val(res.PersenDisc);
						      window.opener.$("#id_discount").val(res.PersenDisc);
						      window.opener.hitung_ticket();
						}
						      
					}
	            }
	        });
	        }
	}
	
	
</script>