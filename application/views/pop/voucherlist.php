<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/paging5.css" />
<script language="javascript" src="<?= base_url(); ?>/public/js/jquery.js"></script>

<body onLoad="key('<?= base_url(); ?>');">
    <table align='center'>
    	<tr>
    		<td nowrap colspan="100%">Jenis voucher : 
    					
								<input type="radio" id="pilihan1" name="pilihan1" value="voc_intr" onClick="oncheked1()" >
								Internal
                           
                        
								<input type="radio" id="pilihan2" name="pilihan2" value="voc_tour" onClick="oncheked2()" >
								Travel
								
								
								<input type="radio" id="pilihan3" name="pilihan3" value="voc_nominal" onClick="oncheked3()" >
								Nominal
								
								&nbsp;&nbsp;&nbsp;
								<span id="cari_travel" style="display: none">
								<input type="text" class="form-control-new" style="width: 15%;" maxlength="30" name="v_keyword_travel" id="v_keyword_travel" placeholder="Cari Travel" onkeyup="cari_travel('<?php echo base_url(); ?>')">
	                  			<select class="form-control-new" name="v_travel" id="v_travel" style="width: 30%;" onchange="get_voucher_travel('<?= base_url() ?>')">
				            		<option value="">- Pilih Travel -</option>
				            		<?php
				            		foreach($kdtravel as $val)
				            		{
										?><option value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
									}
				            		?>
				            	</select>
				            	
				            	<span id="td_beo"></span>
				            	
				            	<span id="cari_voucher" style="display: none">
								<select class="form-control-new" name="v_cari_voucher" id="v_cari_voucher" style="width: 15%;" onchange="ambil_voucher_travel('<?= base_url() ?>')">
				            		<option value="">- Pilih Voucher -</option>
				            	</select>
				            	</span>
				            	
			            	    </span>
			            	    
                            </td>
    	</tr>
        <tr>
            <td id="voucher_internal">Voucher : <input type='text' size='20' name='KodeVoucher' id='KodeVoucher' size="20" placeholder="Voucher Internal" maxlength="20"
                           onKeyDown="getCodeVoucher(event, '<?= base_url(); ?>', 'insert')" style="width:125px;height:30px;font-size:15px"></td>
                           
            <td id="voucher_travel" style="display: none;">Voucher : <input type='text' size='20' name='KodeVoucherTrv' id='KodeVoucherTrv' placeholder="Voucher Travel" size="20" maxlength="20"
                           onKeyDown="getCodeVoucherTrv(event, '<?= base_url(); ?>', 'insert')" style="width:125px;height:30px;font-size:15px"></td>
                           
        	<td id="voucher_nominal" style="display: none;">Nominal : <input type='text' size='20' name='NominalVoucher' id='NominalVoucher' placeholder="Nominal Voucher" size="20" maxlength="20"
                           onKeyDown="getNominalVoucher(event, '<?= base_url(); ?>', 'insert')" style="width:125px;height:30px;font-size:15px"></td>
                           
		    <td>Total Belanja :
			<input type="text" class="InputAlignRight" readonly id="tbelanja" name="tbelanja" size="20" style="width:125px;height:30px;border:none;font-size:25px"></td>
			<td>Total Voucher :
			<input type="text" class="InputAlignRight" readonly id="tvoucher" name="tvoucher" size="20" style="width:125px;height:30px;border:none;font-size:25px" value=0></td>
	
        </tr>
		<tr>
        	<td id="cekvoucher" style="display: none;">Voucher : <input type='text' size='20' name='CekVoucher' id='CekVoucher' placeholder="Cek Voucher" size="20" maxlength="20" onKeyDown="getNominalVoucher(event, '<?= base_url(); ?>', 'insert')" style="width:125px;height:30px;font-size:15px">


			<input type="hidden" name="kassa" id="kassa" value="<?=$NoKassa?>">
        	</td>
        </tr>
    </table>
<br>
<td>
	<table align='center' border='1' class='table_class_list' width='100%'>
	<thead>
    <tr>
		<th width="5%">No</th>
        <th width="20%">Voucher</th>
        <th width="30%">Keterangan</th>
		<th width="20%">Nilai</th>
		<th width="20%">Terpakai</th>
		<th width="5%">Jenis</th>
    </tr>
	</thead>
	</table>
	<table width='100%' align='center' class='table_class_listpos' border="0" name="vdetail1" id="vdetail1"> 
		<tr id="baris1">
			<td height="23" width="5%">
				<div id="lfieldvoucher1"></div>
			</td>
			<td width="20%">
				<div id="lvoucher1"></div>
			</td>
			<td width="30%">
				<div id="lvouchket1"></div>
			</td>
			<td align="right" width="20%">
				<div id="lvouchsaldo1"></div>
			</td>
			<td align="right" width="20%">
				<div id="lvouchpakai1"></div>
			</td>
			<td align="right" width="5%">
				<div id="lvouchjenis1"></div>
			</td>
		</tr>
	</table>
</td>
</table>
<table align = 'center'>
    <tr><td><input type="button" value="Close" onclick = "closing()" style="width:125px;height:70px"></td></tr>
</table>
</body>

<script language="javascript">
    function closing()
    {
        window.close();
    }
	
	function key(url) {
        document.getElementById('KodeVoucher').focus();
		total_biaya = window.opener.$("#total_biaya").val();
		$("#tbelanja").val(total_biaya);
		window.opener.$("#pilihan").val("voucher");
		
		list0voucher = window.opener.$("#listvoucher").val();
		list0vouchket = window.opener.$("#listvouchket").val();
		list0vouchsaldo = window.opener.$("#listvouchsaldo").val();
		list0vouchpakai = window.opener.$("#listvouchpakai").val();
		list0jenis = window.opener.$("#listjenis").val();
		voucher0 = list0voucher.split("##");
		vouchket0 = list0vouchket.split("##");
		vouchsaldo0 = list0vouchsaldo.split("##");
		vouchpakai0 = list0vouchpakai.split("##");
		jenis0 = list0jenis.split("##");
		
		var lax = voucher0.length-1;
		//alert(lastRow);
		for(ix=0;ix<lax;ix++)
		{
			vnovoucher = voucher0[ix];
			vnominal = vouchsaldo0[ix];
			vketerangan = vouchket0[ix];
			vjenisvoucher = jenis0[ix];
			vrupdisc = "";
			vqty = 1;
			barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty);
		}
	}
	
	function getCodeVoucher(e,url,action)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			if(action == 'insert')
			{
				VCode = document.getElementById('KodeVoucher').value;
													//alert(PCode);
				//document.getElementById('temp_pos2').style.display = "block";
				//document.getElementById('temp_pos1').style.display = "none";
				vcode0 = VCode.replace('*','~');
				//alert(pcode0);
				
				if (vcode0 !== "") {
					$.ajax({
						type: "POST",
						url: url+"index.php/pop/voucher/DetailVoucher/"+vcode0,
						success: function(msg){
							//alert(msg);alert(PCode);

							if(msg=="salah"){
								alert("Kode Voucher Salah !!!");
								document.getElementById('KodeVoucher').value = "";
								document.getElementById('KodeVoucher').focus();
							}else{
								var jsdata = msg;
								eval(jsdata);
								vnovoucher = datajson[0].novoucher;
								vnominal = datajson[0].nominal;
								vketerangan = datajson[0].keterangan;
								vjenisvoucher = datajson[0].jenis;
								vrupdisc = datajson[0].rupdisc;
								vqty = datajson[0].qty;
								vexpired = datajson[0].expired;
								
								tgltrans = window.opener.$("#tgltrans").val();
								//alert(tgltrans);
								//alert(vexpired);
								//alert(tgltrans>vexpired);
								batal = 0;
								if(tgltrans>vexpired)
								{
								   batal = 1;
								   alert("Voucher expired/tidak berlaku");
							    }
								//if((vnominal==0)&&(vjenisvoucher!='3'))
								//{
								//   batal = 1;
								//   alert("Voucher sudah digunakan");
								//}
								if(batal==0)
								{
								   barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty);
								}else
								{
								   document.getElementById('KodeVoucher').value = "";
								   document.getElementById('KodeVoucher').focus();
								}
							}
						}
					});
				}
			}
		}
	}	
	
	function getCodeVoucherTrv(e,url,action)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			if(action == 'insert')
			{
				VCode = document.getElementById('KodeVoucherTrv').value;
													//alert(PCode);
				//document.getElementById('temp_pos2').style.display = "block";
				//document.getElementById('temp_pos1').style.display = "none";
				vcode0 = VCode.replace('*','~');
				//alert(pcode0);
				
				if (vcode0 !== "") {
					$.ajax({
						type: "POST",
						url: url+"index.php/pop/voucher/DetailVoucherTrv/"+vcode0,
						success: function(msg){
							//alert(msg);alert(PCode);

							if(msg=="salah"){
								alert("Kode Voucher Salah !!!");
								document.getElementById('KodeVoucherTrv').value = "";
								document.getElementById('KodeVoucherTrv').focus();
							}else{
								var jsdata = msg;
								eval(jsdata);
								vnovoucher = datajson[0].novoucher;
								vnominal = datajson[0].nominal;
								vketerangan = datajson[0].keterangan;
								vjenisvoucher = datajson[0].jenis;
								vrupdisc = datajson[0].rupdisc;
								vqty = datajson[0].qty;
								vexpired = datajson[0].expired;
								
								tgltrans = window.opener.$("#tgltrans").val();
								//alert(tgltrans);
								//alert(vexpired);
								//alert(tgltrans>vexpired);
								batal = 0;
								if(tgltrans>vexpired)
								{
								   batal = 1;
								   alert("Voucher expired/tidak berlaku");
							    }
								//if((vnominal==0)&&(vjenisvoucher!='3'))
								//{
								//   batal = 1;
								//   alert("Voucher sudah digunakan");
								//}
								if(batal==0)
								{
								   barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty);
								}else
								{
								   document.getElementById('KodeVoucherTrv').value = "";
								   document.getElementById('KodeVoucherTrv').focus();
								}
							}
						}
					});
				}
			}
		}
	}
	
	function getNominalVoucher(e,url,action)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{



			vcode0 = document.getElementById('CekVoucher').value;
			kassa = document.getElementById('kassa').value;
			
			//alert(kassa) ;
			$.ajax({
			type: "POST",
			// url: url+"index.php/pop/voucher/CekVoucherTerpakai?id_voucher="+vcode0+"&kassa="+kassa,
			url: url+"index.php/pop/voucher/CekVoucherTerpakai/",
			data:{'id_voucher':vcode0,
				'kassa':kassa},
			// dataType: 'json',
			cache:false,
			success: function(data){
				//alert(vcode0); alert(kassa);
				//alert(data);
				if(data=="salah"){
					alert("Voucher Sudah Terpakai !!!") ;
				}else{
					// var jsdata = msgg;
					// eval(jsdata);
					KdCustomer = vcode0;
					// alert(kdCustomer);
					if (KdCustomer !== "") {
							$.ajax({
								type: "POST",
								url: url+"index.php/pop/voucher/DetailVoucherNominal/"+KdCustomer,
								success: function(msg){
									//alert(msg);alert(PCode);
									if (vcode0=='-'){
										vnovoucher = "-";
										vketerangan ="-";
										vjenisvoucher ="-";
										vrupdisc ="-";
										vqty ="-";
										vnominal = document.getElementById('NominalVoucher').value;
										//alert(vnovoucher);alert(vnominal);
										barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty);
										return;
									}
									if(msg=="salah"){
										alert("Kode Voucher Salah !!!");
										document.getElementById('KodeVoucherTrv').value = "";
										document.getElementById('KodeVoucherTrv').focus();
									}else{
										var jsdata = msg;
										eval(jsdata);
										vnovoucher = datajson[0].novoucher;
										vnominal = document.getElementById('NominalVoucher').value;
										vketerangan = datajson[0].keterangan;
										vjenisvoucher = datajson[0].jenis;
										vrupdisc = datajson[0].rupdisc;
										vqty = datajson[0].qty;
										vexpired = datajson[0].expired;
										
										tgltrans = window.opener.$("#tgltrans").val();
										//alert(tgltrans);
										//alert(vexpired);
										//alert(tgltrans>vexpired);
										batal = 0;
										if(tgltrans>vexpired)
										{
										   batal = 1;
										   alert("Voucher expired/tidak berlaku");
									    }

										if(batal==0)
										{
										   barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty);
										}else
										{
										   document.getElementById('KodeVoucherTrv').value = "";
										   document.getElementById('KodeVoucherTrv').focus();
										}
									}
								}
							});
						}else{
					    vnovoucher = "-";
					    vketerangan ="-";
					    vjenisvoucher ="-";
					    vrupdisc ="-";
					    vqty ="-";
						vnominal = document.getElementById('NominalVoucher').value;
						//alert(vnovoucher);alert(vnominal);
						barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty);
					}
				}
				// alert(url);
			}
			});
								
		} 
	}
	
    function barisvoucher(url,vnovoucher,vnominal,vketerangan,vjenisvoucher,vrupdisc,vqty){
		
		//====================================================
		
		//var tbl = document.getElementById("detail1");
        //var lastRow = tbl.rows.length;
		var tbl = document.getElementById("vdetail1");
        var lastRow = tbl.rows.length;
		
		if(vjenisvoucher=='3')
		{
		   window.opener.$("#jenisvoucher").val(vjenisvoucher);
		   window.opener.$("#id_voucher").val(vnovoucher);

		   //potong diskon karyawan
		   window.opener.HitungNetto();
			total_biaya1 = window.opener.$("#total_biaya").val();
			$("#tbelanja").val(total_biaya1);

		}else
		{
		   if(vjenisvoucher=='1')
		   {
		     window.opener.$("#jenistiket").val(vjenisvoucher);
		   	 //window.opener.$("#id_voucher").val(vnovoucher);
		   }else{
		   window.opener.$("#id_voucher").val(vnovoucher);
		   }
		}
		jenistiket = window.opener.$("#jenistiket").val();
		
		tvoucher = parseFloat($("#tvoucher").val());
		tbelanja = parseFloat($("#tbelanja").val());
		selisih = tbelanja - tvoucher;
		if(vnominal>selisih)
		   vterpakai = selisih;
	    else
		   vterpakai = vnominal;
		
		//alert(lastRow);
		datadouble = false;
		for(index=1;index<=lastRow;index++)
		{
			indexs = index; 
			if($("#lvoucher"+indexs).val()==vnovoucher)
			{
			   datadouble = true;
			   break;
			}
			
		}

		//alert(indexs);
		//alert(datadouble);
		if(datadouble)
		{
			if(vqty<0)
			{  
				document.getElementById('lvouchpakai'+indexs).innerHTML = 0;
				$("#lvouchpakai" + indexs).val(0);
				if(vjenisvoucher=='3')
				{
	     		   window.opener.$("#jenisvoucher").val('');
				   window.opener.$("#id_voucher").val('');
				}
			}else
			{
				if($("#lvouchpakai" + indexs).val()==0)
				{
			    document.getElementById('lvouchpakai'+indexs).innerHTML = vterpakai;
				$("#lvouchpakai" + indexs).val(vterpakai);
				}
			}
		}else
		{
			indexs = lastRow;
			//alert($("#lvoucher1").val());
			if(!(($("#lvoucher1").val()=="")||($("#lvoucher1").val()===null)))
			{
				indexs = lastRow+1;
				cloneRow(indexs);
				//alert("masuk");
				//alert(indexs);
			}
			
			$("#lfieldvoucher" + indexs).val(indexs);
			$("#lvoucher" + indexs).val(vnovoucher);
			$("#lvouchket" + indexs).val(vketerangan);
			$("#lvouchsaldo" + indexs).val(vnominal);
			$("#lvouchpakai" + indexs).val(vterpakai);
			$("#lvouchjenis" + indexs).val(vjenisvoucher);

			document.getElementById('lfieldvoucher'+indexs).innerHTML  = indexs;
            document.getElementById('lvoucher'+indexs).innerHTML  = vnovoucher;
            document.getElementById('lvouchket'+indexs).innerHTML = vketerangan;
            //if(vjenisvoucher!="5"){
            document.getElementById('lvouchsaldo'+indexs).innerHTML = vnominal;
            //}
            document.getElementById('lvouchpakai'+indexs).innerHTML =  vterpakai;
            document.getElementById('lvouchjenis'+indexs).innerHTML = vjenisvoucher;
		}
		
		var tbl = document.getElementById("vdetail1");
        var lastRow = tbl.rows.length;
		netto = 0;
		totaldisc = 0;
		var listvoucher="";
		var listvouchket="";
		var listvouchsaldo="";
		var listvouchpakai="";
		var listjenis="";
		for(index=1;index<=lastRow;index++)
		{
			netto = netto + parseFloat($("#lvouchpakai" + index).val());
			//totaldisc = totaldisc + parseFloat($("#ldisc" + index).val());

			listvoucher = listvoucher + $("#lvoucher" + index).val() + "##";
			listvouchket = listvouchket + $("#lvouchket" + index).val() + "##";
			listvouchsaldo = listvouchsaldo + $("#lvouchsaldo" + index).val() + "##";
			listvouchpakai = listvouchpakai + $("#lvouchpakai" + index).val() + "##";
			listjenis = listjenis + $("#lvouchjenis" + index).val() + "##";
			
			//alert($("#ljumlahnetto" + index).val());
			//alert(index);
		}
		window.opener.$("#listvoucher").val(listvoucher);
		window.opener.$("#listvouchket").val(listvouchket);
		window.opener.$("#listvouchsaldo").val(listvouchsaldo);
		window.opener.$("#listvouchpakai").val(listvouchpakai);
		window.opener.$("#listjenis").val(listjenis);
		window.opener.$("#jenis_voucher").val(vjenisvoucher);
		
		/*if(vjenisvoucher!="5"){
		window.opener.$("#voucher_bayar").val(netto);
		$("#tvoucher").val(netto);	
		}
		window.opener.HitungNetto();*/
		
		
		//if(vjenisvoucher=="5"){
		//	window.opener.$("#voucher_bayar").val(total_biaya);
		//	$("#tvoucher").val(total_biaya);	
		//	document.getElementById('lvouchsaldo'+indexs).innerHTML = total_biaya;
		//}else{
			window.opener.$("#voucher_bayar").val(netto);
		    $("#tvoucher").val(netto);
		//}
		
		window.opener.HitungNetto();

		total_biaya = window.opener.$("#total_biaya").val();
		
		$("#tbelanja").val(total_biaya);
		
		document.getElementById('KodeVoucher').value = "";
        document.getElementById('KodeVoucher').focus();
    }
	
	function cloneRow(nbaru) {		
        nomor = nbaru-1;
		var clonedRow = $("#vdetail1 tr:last").clone(true);
		
        $("#lfieldvoucher" + nomor, clonedRow).attr({"id": "lfieldvoucher" + nbaru});
        $("#lvoucher" + nomor, clonedRow).attr({"id": "lvoucher" + nbaru});
        $("#lvouchket" + nomor, clonedRow).attr({"id": "lvouchket" + nbaru});
        $("#lvouchsaldo" + nomor, clonedRow).attr({"id": "lvouchsaldo" + nbaru});
        $("#lvouchpakai" + nomor, clonedRow).attr({"id": "lvouchpakai" + nbaru});
        $("#lvouchjenis" + nomor, clonedRow).attr({"id": "lvouchjenis" + nbaru});
		
		$("#vdetail1").append(clonedRow);
	    $("#vdetail1 tr:last" ).attr( "id", "baris" +nbaru ); // change id of last row
	
        //clone.id = "newID"+nbaru; // change id or other attributes/contents
        //row.appendChild(clone); // add new row to end of table
    }
    
	function oncheked1()
	{
		document.getElementById('pilihan2').checked = false;
		document.getElementById('pilihan3').checked = false;
		document.getElementById("voucher_internal").style.display = "";
		document.getElementById("voucher_travel").style.display = "none";
		document.getElementById("cari_travel").style.display = "none";
		document.getElementById("voucher_nominal").style.display = "none";
		document.getElementById("cekvoucher").style.display = "none";
	}
	
	function oncheked2()
	{
		document.getElementById('pilihan1').checked = false;
		document.getElementById('pilihan3').checked = false;
		document.getElementById("voucher_internal").style.display = "none";
		document.getElementById("voucher_travel").style.display = "";
		document.getElementById("cari_travel").style.display = "";
		document.getElementById("voucher_nominal").style.display = "none";
		document.getElementById("cekvoucher").style.display = "none";
	}
	
	function oncheked3()
	{
		document.getElementById('pilihan1').checked = false;
		document.getElementById('pilihan2').checked = false;
		document.getElementById("voucher_internal").style.display = "none";
		document.getElementById("voucher_travel").style.display = "none";
		document.getElementById("cari_travel").style.display = "none";
		document.getElementById("voucher_nominal").style.display = "";
		document.getElementById("cekvoucher").style.display = "";
	}
	
	function get_voucher_travel(url)
				{
					v_travel = document.getElementById("v_travel").value;
					//alert(v_travel);
					document.getElementById("v_cari_voucher").style.display = "none";
					document.getElementById("td_beo").innerHTML = "Loading...";
				     
						  $.ajax({
								url: url+"index.php/pop/voucher/getVoucherTravel/",
								data: {trv:v_travel},
								type: "POST",
								dataType: 'html',					
								success: function(res,data)
								{
									document.getElementById("v_cari_voucher").style.display = "";
									document.getElementById("cari_voucher").style.display = "";
									$('#v_cari_voucher').html(res);
									document.getElementById("td_beo").innerHTML = "";
								},
								error: function(e) 
								{
									alert(e);
								} 
					   });
				}
				
				
				function ambil_voucher_travel(url){
					voucher_travel = $("#v_cari_voucher").val();
					$("#KodeVoucherTrv").val(voucher_travel);
					$("#KodeVoucherTrv").focus();
				}
				
				function cari_travel(url)
				{
					var act=$("#v_keyword_travel").val();
					$.ajax({
							url: url+"index.php/pop/voucher/ajax_travel/",
							data: {id:act},
							type: "POST",
							dataType: 'html',					
							success: function(res)
							{
								
								$('#v_travel').html(res);
							},
							error: function(e) 
							{
								alert(e);
							} 
						}); 
					
					   	
		   		}
	
	$(document).ready(function()
		{
			document.getElementById('pilihan1').checked = true;
				
		});
	
</script>