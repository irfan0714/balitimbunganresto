<?php 
	$modul = "Detail Voucher";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		
			<h2>No Struk : <?php echo $nostruk; ?></h2>
			
			<table class="table table-bordered responsive">
	        	<thead>
					<tr>
						<th rowspan="2"  style="text-align: center;" width="10">No</th>
		                <th rowspan="2"  style="text-align: center;">Tanggal</th>
		                <th rowspan="2"  style="text-align: center;">Jenis</th>
		                <th colspan="2" style="text-align: center;">Voucher</th>
		        	</tr>
		        	<tr>
		                <th  style="text-align: center;">Nomor</th>
		                <th  style="text-align: center;">Nominal (Rp)</th>
		        	</tr>
				</thead>
				<tbody>
				
				<?php
				if(count($detail_voucher)==0)
				{
					echo "<tr><td colspan='100%'><center>Tidak Ada Data</center></td></tr>";
				}
				
				$i=1;
				foreach($detail_voucher as $val)
				{
					$bgcolor = "";
                    if($i%2==0)
                    {
                        $bgcolor = "background:#f5f5f6;";
                    }
                    
                    if($val["Jenis"]==1)
                    {
						$echo_jenis = "Lokal";
					}
					else if($val["Jenis"]==2)
					{	
						$echo_jenis = "Asing";
					}
					else if($val["Jenis"]==3)
					{	
						$echo_jenis = "Karwayan";
					}
                    
					?>
					<tr style="<?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
						<td><?php echo $i; ?></td>
						<td align="center"><?php echo $val["Tanggal"]; ?></td>
						<td align="center"><?php echo $echo_jenis; ?></td>
						<td><?php echo $val["NomorVoucher"]; ?></td>
						<td align="right"><?php echo number_format($val["NilaiVoucher"]); ?></td>
					</tr>
					<?php
					$i++;
					
					$arr_data["g_total"] += $val["NilaiVoucher"];
				}
				?>
				
				<tr>
					<td colspan="4" align="right"><b>Grand Total</b></td>
					<td align="right"><b><?php echo number_format($arr_data["g_total"]); ?></b></td>
				</tr>
             
				</tbody>
			</table> 
		
		</div>
		
	</div>
</div>
</body>
</html>