<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/paging5.css" />
<script language="javascript" src="<?=base_url();?>/public/js/jquery.js"></script>

<form method="POST"  name="search" action="">
<table align='center'>
	<tr>
		<td><input type='text' size'20' name='stSearchingKey' id='stSearchingKey'></td>
		<td>
			<select size="1" height="1" name ="searchby" id ="searchby">
				<option value="NamaRekening">Nama Rekening</option>
				<option value="KdRekening">Rekening</option>
			</select>
		</td>
		<td><input type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<table align='center' border='1' class='table_class_list' width="700">
	<tr>
		<th width="50">Pilih</th>
		<th width="150">Rekening</th>
		<th width="350">Nama Rekening</th>
	</tr>
<?php
	if(count($rekeningdata)==0)
	{
?>
	<td nowrap colspan="4" align="center">Tidak Ada Data</td>
<?php
	}
$i=0;
for($a = 0;$a<count($rekeningdata);$a++)
{
 	$i++;
?>
	<input type="hidden" id="detail<?=$i;?>" name="detail<?=$i;?>" value="<?=$row_no.'*_*'.$rekeningdata[$a]['KdRekening'].'*_*'.$rekeningdata[$a]['NamaRekening']?>" >
	<tr>
		<td nowrap>
		<? if($module == 'JRN'){ ?>
			<a href="" onclick="getCodeJurnal('<?=$i;?>')">
		<? }else {?>
			<a href="" onclick="getCode('<?=$i;?>')">
		<? } ?>
			<img src="<?=base_url();?>/public/images/pick.png" border="0" alt="Select" Title="Pilih">
		</a></td>
		<td nowrap><?=$rekeningdata[$a]['KdRekening'];?></td>
		<td nowrap><?=$rekeningdata[$a]['NamaRekening'];?></td>
	</tr>
<?php
}
?>
</table>
<table align = 'center'>
	<tr><td><?php echo $this->pagination->create_links(); ?></td></tr>
	<tr><td><input type="button" value="Close" onclick = "closing()"></td></tr>
</table>

<script language="javascript">
	function closing()
	{
	 	window.close();
	}

	function getCode(id)
	{
	 	parameter = $("#detail"+id).val().split("*_*");
		brs = parameter[0];
		window.opener.$("#kdrekening"+brs).val(parameter[1]);
		window.opener.$("#tmpkdrekening"+brs).val(parameter[1]);
		window.opener.$("#namarekening"+brs).val(parameter[2]);
		window.opener.$("#jumlah"+brs).focus();
		closing();
	}
	
	function getCodeJurnal(id)
	{
	 	parameter = $("#detail"+id).val().split("*_*");
		brs = parameter[0];
		window.opener.$("#kdrekening"+brs).val(parameter[1]);
		window.opener.$("#tmpkdrekening"+brs).val(parameter[1]);
		window.opener.$("#namarekening"+brs).val(parameter[2]);
		window.opener.$("#debit"+brs).focus();
		closing();
	}
</script>