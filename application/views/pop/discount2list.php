<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>DISKON PAGE</title>
    <link rel="shortcut icon" href="<?= base_url();?>public/images/Logosg.png" >
    
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
    
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/paging5.css" />
	<script language="javascript" src="<?= base_url(); ?>/public/js/jquery.js"></script>    
    <script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>

</head>

<body onLoad="key('<?= base_url(); ?>');">

<?php
$tgl = date('Y-m-d');
	if($tgl>='2018-12-01' AND $tgl<='2018-12-31'){
		$ya = "1";
	}elseif ($tgl>='2019-01-01') {
        $ya = "2";
    }else{
		$ya = "0";
	}
?>
<div class="panel-body">
	<div class="row">
        <div class="col-md-4">
            <b>Jenis Discount</b>&nbsp;&nbsp;&nbsp;
            <select class="form-control-new" name="jenis_discount" id="jenis_discount" onchange="ganti_jenis_discount()" style="width:125px;">
                <option value="special">Special</option>
                <option value="members">Members</option>
                <option value="card">Card</option>
                <option value="promo">Promo Opening</option>
			</select>  
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <input type="button" class="btn btn-success" value="Close" onclick = "closing()" style="width:100px;height:30px">
        </div>
      
    </div>
	<br>
	
    <table class="table responsive" id="discount_khusus">
        <tr>
            <td>Kode Discount : <input type='password' size='20' name='KodeDiscount' id='KodeDiscount' size="20" maxlength="20"
                           onKeyDown="getCodeDiscount(event, '<?= base_url(); ?>', 'insert')" style="width:125px;height:30px;font-size:15px"></td>
		    <td>Total Belanja :
			<input type="text" class="InputAlignRight" readonly id="tbelanja" name="tbelanja" size="20" style="width:125px;height:30px;border:none;font-size:25px" value=0></td>
			<td>Total Discount :
			<input type="text" class="InputAlignRight" readonly id="tdiscount" name="tdiscount" size="20" style="width:125px;height:30px;border:none;font-size:25px" value=0></td>
	
        </tr>
    </table>
    
    <table class="table responsive" id="discount_anggota" style="display: none;">
        <tr>
            <td class="title_table">Nomor Anggota</td>
       		<td><input type="text" class="form-control-new" value="" name="no_anggota" id="no_anggota" maxlength="255" size="30" onkeydown="member_keypress(event, '1', '<?= base_url(); ?>')" onblur="cekmember()"></td>
        </tr>
        <tr>
            <td class="title_table">Nomor Handphone</td>
       		<td><input type="text" class="form-control-new" value="" name="no_hp" id="no_hp" maxlength="255" size="30" onkeydown="member_keypress_hp(event, '1', '<?= base_url(); ?>')" onblur="cekmember_hp()"></td>
        </tr>
        <tr>
            <td class="title_table">Nama Anggota</td>
            <td><input readonly="readonly" type="text" class="form-control-new" value="" name="nama_anggota" id="nama_anggota" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Jumlah Point</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="jml_point" id="jml_point" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Total Belanja</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_sales" id="total_sales" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">% Discount Member</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="persen_discount" id="persen_discount" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Total Discount</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_discount" id="total_discount" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Sisa</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="sisa" id="sisa" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Potong Point</td>
            <td><input style="text-align: right;" type="text" class="form-control-new" value="0" name="potong_point" id="potong_point" maxlength="255" size="30" onkeydown="bayar_point(event, '1', '<?= base_url(); ?>')" onblur="hitung_point()">
            	<span id="checklist" style="display: none;"><img src="<?= base_url();?>public/images/accept.png"/></span>
            </td>
        </tr>
    </table>
    
    <table class="table responsive" id="discount_kartu" style="display: none;">
        <tr>
            <td class="title_table">Nomor Kartu</td>
            <td><input type="text" class="form-control-new" value="" name="no_card" id="no_card" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Nama</td>
            <td><input type="text" class="form-control-new" value="" name="nama_user_card" id="nama_user_card" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Jenis Kartu</td>
            <td>
            	<select class="form-control-new" name="jenis_card" id="jenis_card" onchange="hitung_disc_card()" style="width:180px;">
	                <option value="">-- Pilih Kartu --</option>
	                <?php
            		foreach($jenis_kartu as $val)
            		{
						?><option value="<?php echo $val['KdGroupDisc']; ?>"><?php echo $val['NamaGroupDisc']; ?></option><?php
					}
            		?>
				</select>
				<span id="span_loading" style=" display: none;"><img src="<?= base_url();?>public/images/ajax-image.gif"/></span>
            </td>
        </tr>
        
        <tr>
            <td class="title_table">Total Belanja</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_sales_card" id="total_sales_card" maxlength="255" size="30"></td>
        </tr>
        <tr style="display: none;">
            <td class="title_table" style="display: none;">% Discount Card</td>
            <td style="display: none;"><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="persen_discount_card" id="persen_discount_card" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Total Discount</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_discount_card" id="total_discount_card" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Sisa</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="sisa_card" id="sisa_card" maxlength="255" size="30">
            	<span id="checklist2" style="display: none;"><img src="<?= base_url();?>public/images/accept.png"/></span>
            </td>
        </tr>
    </table>
    
    <table class="table responsive" id="discount_promo" style="display: none;">
    <?php $total_sales_belum_tax_and_service = $this->uri->segment(4);?>
    <input type="hidden" id="total_sales_belum_tax_and_service" value="<?php echo $total_sales_belum_tax_and_service;?>">
     <input type="hidden" id="masuk_desember" value="<?php echo $ya;?>">
        
        <tr>
            <td class="title_table">Total Belanja Sebelum TAX & Service<b style="color: red;">**</b></td>
            <td><b>Rp. <?php echo $total_sales_belum_tax_and_service;?></b></td>

        </tr>
        <tr>
            <td class="title_table">Nomor KTP</td>
            <td><input type="text" class="form-control-new" value="" name="no_ktp" id="no_ktp" maxlength="255" size="30" onkeydown="ktp_keypress(event, '1', '<?= base_url(); ?>')">
            	<span id="checklist97" style="display: none;"><img src="<?= base_url();?>public/images/accept.png"/></span>
            	<span id="checklist98" style="display: none;"><img src="<?= base_url();?>public/images/cancel.png"/></span>
            	<span id="checklist99" style="display: none;"><img src="<?= base_url();?>public/images/ajax-image.gif"/></span>
            </td>
        </tr>
        <tr>
            <td class="title_table">
                Tanggal Lahir 
            </td>
            <td>
                <span id="lahir"></span>
            </td>
        </tr>
        <tr>
        	<td class="title_table">
        		Umur 
        	</td>
        	<td>
        		<span id="umur"></span>
        	</td>
        </tr>
        <tr>
        	<td class="title_table">
        		Jenis Kelamin 
        	</td>
        	<td>
        		<span id="kelamin"></span>
        	</td>
        </tr>
        <tr>
            <td class="title_table">Nama</td>
            <td><input type="text" class="form-control-new" value="" name="nama_user_ktp" id="nama_user_ktp" maxlength="255" size="30" onkeydown="input_diskon_promo(event)">
            	<span id="checklist100" style="display: none;"><img src="<?= base_url();?>public/images/ajax-image.gif"/></span>
            </td>
        </tr>  
        <tr style="display: ;">
            <td class="title_table" style="display: ;">Discount KTP (%)</td>
            <td style="display: ;"><input style="text-align: right;"  type="text" class="form-control-new" value="" onkeydown="" name="persen_discount_ktp" id="persen_discount_ktp" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Total Belanja</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_sales_ktp" id="total_sales_ktp" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td class="title_table">Discount</td>
            <td><input readonly="readonly" style="text-align: right;" type="text" class="form-control-new" value="" name="total_discount_ktp" id="total_discount_ktp" maxlength="255" size="30"></td>
        </tr>
        <tr>
            <td ><b style="color: red;">** Merupakan Acuan Penentuan Disc. Usia. Mohon dipahami.<br>&nbsp;&nbsp;&nbsp;&nbsp;info : Per 1 s.d 31 Jan 2019, Diskon Umur Max 50 Tahun.</b></td>
            <!-- <td style="color:red;"></td> -->
        </tr>
    </table>
    
<br>
<table id="tombol_hitung" class="table responsive" style="display: none;">
    <tr><td align="center"><input type="button" class="btn btn-info" value="Hitung" onclick = "hitung_lagi()" style="width:100px;height:30px"></td></tr>
</table>

<table id="info" class="table responsive" style="display: none;">
	<tr><td align="center" colspan="100%" id="checking" style=" display: none;"> <span id="span_loading" ><img src="<?= base_url() ?>public/images/ajax-image.gif"/></span></td></tr>
    <tr><td align="left">Min. Transaksi</td><td align="left" id="minimumbelanja">0</td></tr>
    <tr><td align="left">Max. Discount</td><td align="left" id="maxdiscount">0</td></tr>
    <tr><td align="left">Hari</td><td align="left">Sabtu & Minggu</td></tr>
    <tr><td align="left">Kuota Transaksi</td><td align="left" id="kuota">0</td></tr>
    <tr><td align="left">Kuota Terpakai</td><td align="left" id="terpakai_kuota">0</td></tr>
    <tr><td align="left">Sudah Terpakai Di Hari Yang Sama</td><td align="left" id="digunakan">0</td></tr>
</table>
</div>
</body>


<script language="javascript">
    
    function closing()
    {
        window.close();
    }
	
	function key(url) {
        document.getElementById('KodeDiscount').focus();
		total_biaya = window.opener.$("#total_biaya").val();
		$("#tbelanja").val(total_biaya);
		window.opener.$("#pilihan").val("discount");
		
		iduserdiscount = window.opener.$("#iduserdiscount").val();
		idpersendiscount = window.opener.$("#idpersendiscount").val();
	}
	
	function getCodeDiscount(e,url,action)
	{
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			//alert("masuk");
			if(action == 'insert')
			{
				VCode = document.getElementById('KodeDiscount').value;
													//alert(PCode);
				//document.getElementById('temp_pos2').style.display = "block";
				//document.getElementById('temp_pos1').style.display = "none";
				vcode0 = VCode.replace('*','~');
				//alert(vcode0);
				
				if (vcode0 !== "") {
					$.ajax({
						type: "POST",
						url: url+"index.php/pop/discount2/Detaildiscount/"+vcode0,
						success: function(msg){
							//alert(msg);alert(vcode0);

							if(msg=="salah"){
								alert("Kode Discount Salah !!!");
								
							}else{
								var jsdata = msg;
								eval(jsdata);
								vuserdiscount = datajson[0].username;
								vmaxdisc = datajson[0].maxdisc;
								
								nilaidiscount = window.opener.$("#idpersendiscount").val();			
								
								//alert(nilaidiscount);
								//alert(vmaxdisc);
								//alert(nilaidiscount>vmaxdisc);
								nilaidiscount = parseFloat(nilaidiscount);
								vmaxdisc = parseFloat(vmaxdisc);
								batal = 0;
								if(nilaidiscount>vmaxdisc)
								{
								   batal = 1;
								   alert("Discount melebihi plafond");
							    }
								if(batal==0)
								{
									diskon_diskon = $("#KodeDiscount").val();
									window.opener.$("#diskon_diskon").val(diskon_diskon);
									
									window.opener.$("#iduserdiscount").val(vuserdiscount);	
									window.opener.$("#iddiscountasli").val(nilaidiscount);
								    window.opener.hitungnetto();
									window.opener.setPayment();
									discount = window.opener.$("#idnilaidiscount").val();
								    discount = window.opener.$("#idnilaidiscount").val();
									total_biaya = window.opener.$("#nettosales").val();
									$("#tdiscount").val(discount);
									$("#tbelanja").val(total_biaya);		
								}else
								{
								   document.getElementById('KodeDiscount').value = "";
								   document.getElementById('KodeDiscount').focus();
								}
							}
						}
					});
				}
			}
		}
	}
	
	function ganti_jenis_discount(){
		jenis = $("#jenis_discount").val();
		if(jenis=="special"){
			$("#discount_khusus").css("display","");
			$("#discount_anggota").css("display","none");
			$("#discount_kartu").css("display","none");
			$("#discount_promo").css("display","none");
			document.getElementById('KodeDiscount').focus();
		}else if(jenis=="members"){
			$("#discount_khusus").css("display","none");
			$("#discount_anggota").css("display","");
			$("#discount_kartu").css("display","none");
			$("#discount_promo").css("display","none");
			$('[name="total_sales"]').val($("#tbelanja").val());
			document.getElementById('no_anggota').focus();
		}else if(jenis=="card"){
			$("#discount_khusus").css("display","none");
			$("#discount_kartu").css("display","");
			$("#discount_anggota").css("display","none");
			$("#discount_promo").css("display","none");
			$('[name="total_sales_card"]').val($("#tbelanja").val());
			document.getElementById('no_card').focus();
		}else if(jenis=="promo"){
			$("#discount_khusus").css("display","none");
			$("#discount_kartu").css("display","none");
			$("#discount_anggota").css("display","none");
			$("#discount_promo").css("display","");
			$('[name="total_sales_ktp"]').val($("#tbelanja").val());
			document.getElementById('no_ktp').focus();
		}
	}
	
	function member_keypress(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			cekmember();
		}
	}
	
	function cekmember() {
		nomor = $("#no_anggota").val();
		$.ajax({
        	url: "<?php echo site_url('pop/discount2/ajax_edit') ?>/" + nomor,
        	type: "GET",
        	dataType: "JSON",
        	success: function(res){
        		if(res=="NO"){
			  		alert('Member Tersebut Tidak Ada.');
			  	    $("#no_anggota").val('');
			  	    $('[name="nama_anggota"]').val('');
			  	    $('[name="jml_point"]').val('');
			  	    $('[name="persen_discount"]').val('');
			  	    $('[name="total_discount"]').val('');
			  	    $('[name="sisa"]').val('');
			  	    $('[name="potong_point"]').val('0');
			  	    
			  	    //balikan nilai total biaya 
			  	    window.opener.$("#iduserdiscount").val('');	
					window.opener.$("#idpersendiscount").val(0);
					window.opener.$("#iddiscountasli").val(res.PersenDisc);
	        	  	window.opener.$("#jenis_discount").val('');
	        	  	window.opener.$("#kdmember").val('');
	        	  	window.opener.$("#idbayar").val('');
				    window.opener.hitungnetto();
					window.opener.setPayment();
			  	    
			  	    discount = window.opener.$("#discount_bayar").val();
				    total_biaya = window.opener.$("#total_biaya").val();
					$("#tdiscount").val(discount);
					$("#tbelanja").val(total_biaya);
					 	
			  	    $("#no_anggota").focus();
			  	    return false;
				}else{
		  		 	 //jika mau ganti dengan member lain maka di reset dulu
		  		  	// start --------------------------------------------------------
		  		  	$('[name="nama_anggota"]').val('');
		  	      	$('[name="jml_point"]').val('');
		  	      	$('[name="persen_discount"]').val('');
		  	      	$('[name="total_discount"]').val('');
		  	      	$('[name="sisa"]').val('');
		  	      	$('[name="potong_point"]').val('0');
		  	      	
		  	      	//balikan nilai total biaya 
					window.opener.$("#iduserdiscount").val('member');	
					window.opener.$("#idpersendiscount").val(res.PersenDisc);
					window.opener.$("#iddiscountasli").val(res.PersenDisc);
	        	  	window.opener.$("#jenis_discount").val($("#jenis_discount").val());
	        	  	window.opener.$("#kdmember").val(res.KdMember);
	        	  	window.opener.$("#idbayar").val("P");
				    window.opener.hitungnetto();
					window.opener.setPayment();
				    discount = window.opener.$("#idnilaidiscount").val();
					total_biaya = window.opener.$("#nettosales").val();
					brutto = window.opener.$("#brutosales").val();
					$("#tdiscount").val(discount);
					$("#tbelanja").val(brutto);	
		  	     	 //end  --------------------------------------------------------------
		  	        $('[name="no_hp"]').val(res.NoHP1);
	        	 	$('[name="nama_anggota"]').val(res.NamaMember);
	        	  	$('[name="jml_point"]').val(res.JumlahPoint);
	        	  	$('[name="total_sales"]').val(brutto);
	        	  	ttl_sales = parseFloat(brutto); 
	        	  	$('[name="persen_discount"]').val(res.PersenDisc);
					ttl_disc = (ttl_sales*1)*res.PersenDisc/100;		
					$('[name="total_discount"]').val(Math.round(ttl_disc));
					$('[name="sisa"]').val(Math.round(ttl_sales-ttl_disc));		
	            }
	        }
		});
	}
	
	function member_keypress_hp(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			cekmember_hp();
		}
	}
	
	function cekmember_hp() {
		nomor = $("#no_hp").val();
		$.ajax({
        	url: "<?php echo site_url('pop/discount2/ajax_edit_hp') ?>/" + nomor,
        	type: "GET",
        	dataType: "JSON",
        	success: function(res){
        		if(res=="NO"){
			  		alert('Member Tersebut Tidak Ada.');
			  	    $("#no_anggota").val('');
			  	    $("#no_hp").val('');
			  	    $('[name="nama_anggota"]').val('');
			  	    $('[name="jml_point"]').val('');
			  	    $('[name="persen_discount"]').val('');
			  	    $('[name="total_discount"]').val('');
			  	    $('[name="sisa"]').val('');
			  	    $('[name="potong_point"]').val('0');
			  	    
			  	    //balikan nilai total biaya 
			  	    window.opener.$("#iduserdiscount").val('');	
					window.opener.$("#idpersendiscount").val(0);
					window.opener.$("#iddiscountasli").val(res.PersenDisc);
	        	  	window.opener.$("#jenis_discount").val('');
	        	  	window.opener.$("#kdmember").val('');
	        	  	window.opener.$("#idbayar").val('');
				    window.opener.hitungnetto();
					window.opener.setPayment();
			  	    
			  	    discount = window.opener.$("#discount_bayar").val();
				    total_biaya = window.opener.$("#total_biaya").val();
					$("#tdiscount").val(discount);
					$("#tbelanja").val(total_biaya);
					 	
			  	    $("#no_anggota").focus();
			  	    return false;
				}else{
		  		 	 //jika mau ganti dengan member lain maka di reset dulu
		  		  	// start --------------------------------------------------------
		  		  	$('[name="nama_anggota"]').val('');
		  	      	$('[name="jml_point"]').val('');
		  	      	$('[name="persen_discount"]').val('');
		  	      	$('[name="total_discount"]').val('');
		  	      	$('[name="sisa"]').val('');
		  	      	$('[name="potong_point"]').val('0');
		  	      	
		  	      	//balikan nilai total biaya 
					window.opener.$("#iduserdiscount").val('member');	
					window.opener.$("#idpersendiscount").val(res.PersenDisc);
					window.opener.$("#iddiscountasli").val(res.PersenDisc);
	        	  	window.opener.$("#jenis_discount").val($("#jenis_discount").val());
	        	  	window.opener.$("#kdmember").val(res.KdMember);
	        	  	window.opener.$("#idbayar").val("P");
				    window.opener.hitungnetto();
					window.opener.setPayment();
				    discount = window.opener.$("#idnilaidiscount").val();
					total_biaya = window.opener.$("#nettosales").val();
					brutto = window.opener.$("#brutosales").val();
					$("#tdiscount").val(discount);
					$("#tbelanja").val(brutto);	
		  	     	 //end  --------------------------------------------------------------
		  	      	$('[name="no_anggota"]').val(res.KdMember);
	        	 	$('[name="nama_anggota"]').val(res.NamaMember);
	        	  	$('[name="jml_point"]').val(res.JumlahPoint);
	        	  	$('[name="total_sales"]').val(brutto);
	        	  	ttl_sales = parseFloat(brutto); 
	        	  	$('[name="persen_discount"]').val(res.PersenDisc);
					ttl_disc = (ttl_sales*1)*res.PersenDisc/100;		
					$('[name="total_discount"]').val(Math.round(ttl_disc));
					$('[name="sisa"]').val(Math.round(ttl_sales-ttl_disc));		
	            }
	        }
		});
	}
	
	function input_diskon_promo(e) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
            hitungDiskonPromo();
			//$("#persen_discount_ktp").focus();
		}
	}
	
	function hitung_diskon_promo(e) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			hitungDiskonPromo();
		}
	}
	
	function hitungDiskonPromo(){
		$("#checklist100").css("display","");
		$persentase = $("#persen_discount_ktp").val();
		total_sales_belum_tax_and_service = $("#total_sales_belum_tax_and_service").val();
		window.opener.$("#diskon_diskon").val('promo');
		
		window.opener.$("#nocard").val($("#no_ktp").val());
	    window.opener.$("#namacard").val($("#nama_user_ktp").val());
	    window.opener.$("#iduserdiscount").val($("#persen_discount_ktp").val());
	    window.opener.$("#iddiscountasli").val($("#persen_discount_ktp").val());
	    
	    if(parseInt(total_sales_belum_tax_and_service)>1000000){
			window.opener.sumFood();
			window.opener.HitungLagiUntukDiskonPromo();
		}
		
		window.opener.hitungnetto();
		window.opener.setPayment();
		
		totals_diskon = window.opener.$("#idnilaidiscount").val();
	    nettosales = window.opener.$("#nettosales").val();
	    $("#total_sales_ktp").val(nettosales);
		$("#total_discount_ktp").val(totals_diskon);
		$("#checklist100").css("display","none");
		alert('Selesai');
	}
	
	function ktp_keypress(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			var noktp = $('#no_ktp').val();
            //bapak dluan pak, saya cari functon lengnta dlu hehe
            var cekleng =  noktp.length;
            if(parseInt(cekleng) < 16){
                alert("Cek Nomer KTP, Nomer KTP Salah!");
                
                return false;
            }else{

                var jk = parseInt(noktp.substr(6,1));
            var kelamin = '';
            var tanggal = '';
            var bulan = '';
            var tahun = '';
            if(jk < 4){

                kelamin     = "L";
                tanggal     = noktp.substr(6,2);
                bulan       = noktp.substr(8,2);
                tahun       = noktp.substr(10,2);

            }else{
                kelamin     = "P";
                tanggal     = parseInt(noktp.substr(6,2))-40;
                bulan       = noktp.substr(8,2);
                tahun       = noktp.substr(10,2);
            }
            var targetDate = new Date();
            targetDate.setDate(targetDate.getDate());
            var nowyear = targetDate.getFullYear();
            var snowyear = nowyear.toString();

            // alert(nowyear);
            var lahir = '';
            var nowyear2 = parseInt(snowyear.substr(1,2));
            if(nowyear2 < parseInt(tahun)){
            lahir = '19'+tahun+'-'+bulan+'-'+tanggal;

            }else{
            lahir = '20'+tahun+'-'+bulan+'-'+tanggal;

            }

            
            dob = new Date(lahir);
            var today = new Date();
            var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
            $('#lahir').html(tanggal+'-'+bulan+'-19'+tahun);
            
            var masuk_desember = parseInt($('#masuk_desember').val());
            // kalo masuk_desember = 2 artinya dia masuk januari 2019
            
            if(masuk_desember==1){
				if(age<=60){
				ages = age;
				}else{
					alert('Umur '+age+' tahun, Maka Disc. Max 60%.');
					ages = 60;
				}
			}else if(masuk_desember==2){
                if(age<=50){
                ages = age;
                }else{
                    alert('Umur '+age+' tahun, Maka Disc. Max 50%.');
                    ages = 50;
                }
            }else{
				ages = age;
			}
            

            $('#umur').html(age+' Tahun');
            $('#kelamin').html(kelamin);
            $('#persen_discount_ktp').val(ages);
            cekktp();

            }
			
		}
	}
	
	function cekktp() {
		ktp = $("#no_ktp").val();
		$("#checklist99").css("display","");
		$.ajax({
        	url: "<?php echo site_url('pop/discount2/ajax_cek_ktp') ?>/" + ktp,
        	type: "GET",
        	dataType: "JSON",
        	success: function(res){
        		if(res=="NO"){
        			$("#checklist97").css("display","");
        			$("#checklist98").css("display","none");
        			$("#checklist99").css("display","none");
        			$("#nama_user_ktp").focus();
				}else{
					$("#checklist98").css("display","");
					$("#checklist97").css("display","none");
        			$("#checklist99").css("display","none");
					alert('Maaf, No. KTP tersebut sudah melakukan transaksi Hari ini.');
					$("#no_ktp").val('');
					//$("#no_ktp").focus();
        			window.location.reload();
				}
	        }
		});
	}
	
	function bayar_point(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			hitung_point();	
		}		
	}
	
	function hitung_point() {
		jml_point = parseFloat($("#jml_point").val())+0;
		potong_point = parseFloat($("#potong_point").val())+0;
		if(jml_point>=potong_point && jml_point>0){
			window.opener.$("#idbayarpoint").val(potong_point);	
		    window.opener.setPayment();
		    $("#checklist").css("display","");
		}else{
			alert('Jumlah Point '+potong_point+' Bayar Melebihi Jumlah Point Yang Ada '+jml_point);
			$("#potong_point").val('0');
			$("#potong_point").focus();
			return false;
		}
	}
	
	function hitung_disc_card(){
		jenis_card = $("#jenis_card").val();
		kode_store = window.opener.$("#store").val();
		no_card = $("#no_card").val();
		nama_user_card = $("#nama_user_card").val();
		brutto = window.opener.$("#brutosales").val();
		$("#total_sales_card").val(brutto); 
        total_sales_card = brutto;
		table_this = window.opener.$("#table").html();
        if(table_this == 'BTA'){
            table = table_this; 
        }else{
            arr_table = table_this.split("<br>");
            table = arr_table[1];
        }
        

		     if(jenis_card=="3"){
		 	    $("#info").css("display","");
		 	    $("#checking").css("display","");
			 }else{
			 	$("#info").css("display","none");
			 }
			 		 
		if(no_card==""){
		 	alert("No Kartu Belum Di Input.");
		 	$('[name="jenis_card"]').val('');
		 	$("#no_card").focus();
		 	return false;
		}else if(nama_user_card==""){
		 	alert("Nama Pengguna Kartu Belum Di Input.");
		 	$('[name="jenis_card"]').val('');
		 	$("#nama_user_card").focus();
		 	return false;
		}else{
		 	
			$.ajax({
	            url: "<?php echo site_url('pop/discount2/card') ?>/" + jenis_card+"/"+kode_store+"/"+table,
	            type: "GET",
	            dataType: "JSON",
	            success: function(res)
	            {
	            	if(res=="NO"){
					   alert("Kartu Tidak Berlaku.");
					   $("#no_card").val('');
					   $("#no_card").focus();
					   return false;
					}else{
                        //titik
                        if(res.KdGroupDisc=="9" && res.TakeAway==1){
                           alert("Promo Hanya Berlaku Makan Ditempat.");
                           $("#no_card").val('');
                           $("#no_card").focus();
                           return false;
                        }else if((parseInt(total_sales_card)*1)<(parseInt(res.MinimumBelanja)*1)){  //iso
						  		alert("Minimum Total Belanja Menggunakan Kartu Ini Adalah Rp."+res.MinimumBelanja);
						  		$("#no_card").val('');
						  		$("#nama_user_card").val('');	
						  		$("#jenis_card").val('');
						  		 $("#info").css("display","none");
						  		$("#checking").css("display","none");
						  		$("#no_card").focus();
						  		return false;
						}else{
						
				      	//$('[name="persen_discount_card"]').val(res.PersenDisc);
				     	ttl_sales = $("#tbelanja").val(); 
	            	  	window.opener.$("#id_discount").val(0);
	            	  	window.opener.$("#jenis_discount").val($("#jenis_discount").val());
	            	  	window.opener.$("#nocard").val($("#no_card").val());
	            	  	window.opener.$("#namacard").val(res.KdGroupDisc);
	            	  	window.opener.$("#idpersendiscount").val(0);
						window.opener.$("#iddiscountasli").val(0);
	            	  	//window.opener.getDiscCard();
	            	  	//window.opener.hitungnetto();
	            	  	
        	  	          if(jenis_card!="3"){
    	            	     window.opener.hitungnetto();
    	            	  }
    	            	  
    	            	  if(jenis_card=="3"){
    	            	  $.ajax({
    			            url: "<?php echo site_url('pop/discount/kuota_card') ?>/" + jenis_card+"/"+no_card,
    			            type: "GET",
    			            async: false,
    			            dataType: "JSON",
    			            success: function(res)
    			            {
    			            	document.getElementById('minimumbelanja').innerHTML = res.MinimumBelanja;
    			            	document.getElementById('kuota').innerHTML = res.Kuota;
    			            	document.getElementById('maxdiscount').innerHTML = res.maxdiscount;
    			            	
    			            	if(parseInt(res.terpakai_kuota)>parseInt(res.Kuota)){
    								alert("Kartu Tidak Dapat Digunakan Karena Melebihi Kuota.");
    								document.getElementById('terpakai_kuota').innerHTML = res.terpakai_kuota;
    								$("#checking").css("display","none");
    								return false;
    							}else{
    								document.getElementById('terpakai_kuota').innerHTML = res.terpakai_kuota;
    							}
    							
    							
    							if(res.sudah_digunakan=="Y"){
    			            		alert("Sudah Di Gunakan di "+res.namastore);
    								document.getElementById('digunakan').innerHTML = "Sudah Di Gunakan di "+res.namastore;
    								$("#checking").css("display","none");
    								return false;
    							}else{
    							
    								document.getElementById('digunakan').innerHTML = "Belum Di Gunakan Di Store Manapun.";	
    							}
    							
    							if(res.sudah_digunakan=="T" && parseInt(res.terpakai_kuota)<=parseInt(res.Kuota)){
    								window.opener.hitungnetto();
    							}
    			            }
    			            });
    	            	    }
	            	  	
	            	  	
	            	  	window.opener.setPayment();
	            	  	totals_diskon = window.opener.$("#idnilaidiscount").val();
	            	  	nettosales = window.opener.$("#nettosales").val();
						$("#total_discount_card").val(totals_diskon);
						$("#total_discount_card").val(totals_diskon);
						
						$("#sisa_card").val(nettosales);
						
						}
					}
					totals_diskon = window.opener.$("#idnilaidiscount").val();
					nettosales = window.opener.$("#nettosales").val();
					$("#total_discount_card").val(totals_diskon);
					$("#sisa_card").val(nettosales);
	            },
	            async: false
	        });
	    }
	    
	        $("#checking").css("display","none");
	        //setTimeout("hitung_lagi()", 5000);
	}
	
	function hitung_lagi(){
		//window.opener.HitungNetto();
		//$("#span_loading").css("display","none");
		//$("#checklist2").css("display","");
		totals_diskon = window.opener.$("#tot_diskon").val();
		$("#total_discount_card").val(totals_diskon);
		ttl = $("#total_sales_card").val();
		sisax = parseInt(ttl) - parseInt(totals_diskon);
		$("#sisa_card").val(sisax);
	}
</script>