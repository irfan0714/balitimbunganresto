<?php 
	$modul = "Search Kode Barang";
	
	$search_divisi = $this->uri->segment(4);
	$v_nilai = $this->uri->segment(6);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
    <script>
        
		function start_page()
		{
			document.getElementById("v_keyword").focus();	
		}
        
        function get_choose(pcode, nama_lengkap)
        {
           
            //window.opener.document.forms[0]["pcode"+<?php echo $v_nilai; ?>].value = pcode;
            //window.opener.document.forms[0]["v_namabarang"+<?php echo $v_nilai; ?>].value = nama_lengkap;
			//window.opener.document.forms[0]["v_satuan"+<?php echo $v_nilai; ?>].value = satuan;
            //---window.opener.document.getElementById("show_employee_code_hrd_"+<?php echo $v_nilai; ?>).innerHTML      = nama_lengkap;
            //window.opener.document.getElementById("v_qty"+<?php echo $v_nilai; ?>).focus();
            
            window.opener.document.forms[0]["v_coano"+<?php echo $v_nilai; ?>].value = pcode;
            window.opener.document.forms[0]["v_namarekening"+<?php echo $v_nilai; ?>].value = nama_lengkap;
            
            self.close() ;
            
            /*
            window.close();
			window.opener.location.reload();
			*/
            return; 
        }
        
        function CloseWindow(url)
		{
			window.close();
			window.opener.location.reload();
		}
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<form method="POST"  name="search" action='<?=base_url();?>index.php/pop/pop_up_credit_note/search'>
			<input type="hidden" name="v_nilai" id="v_nilai" value="<?php echo $v_nilai; ?>"/>
			<input type="hidden" name="search_divisi" id="search_divisi" value=""/>
			<input type="hidden" name="search_kategori" id="search_kategori" value=""/>
			              <?php
							if($this->uri->segment(4)==undefined){
							$cn_number='undefined';
							}else{
							$cn_number = $this->uri->segment(4);
							}
							?>
			<input type="hidden" name="cn_number" id="cn_number" value="<?php echo $cn_number;?>"/>
			<div class="row">
				<div class="col-md-12">
					<b>Keyword</b>&nbsp;
	               	<input type="text" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" size="20">
	               	&nbsp;
					<!--<b>Divisi</b>&nbsp;
					<select class="form-control-new" name="search_divisi" id="search_divisi">
        				<option value="">All</option>
						<?php
	            		foreach($mdivisi as $val)
	            		{
							$selected="";
		        			if($search_divisi!="")
		        			{
		        				if($val["KdDivisi"]==$search_divisi)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?>  value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	                </select>-->
	                &nbsp;
					<!--<b>Kategori</b>&nbsp;
					<select class="form-control-new" name="search_kategori" id="search_kategori">
        				<option value="">All</option>
						<?php
	            		foreach($mkategori as $val)
	            		{
							$selected="";
		        			if($search_kategori)
		        			{
		        				if($val["KdKategori"]==$search_kategori)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?>  value="<?php echo $val["KdKategori"]; ?>"><?php echo $val["NamaKategori"]; ?></option><?php
						}
	            		?>
	                </select>
	                &nbsp;
	                <b>Brand</b>&nbsp;
					<select class="form-control-new" name="search_brand" id="search_brand">
        				<option value="">All</option>
						<?php
	            		foreach($mbrand as $val)
	            		{
							$selected="";
		        			if($search_brand)
		        			{
		        				if($val["KdBrand"]==$search_brand)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?>  value="<?php echo $val["KdBrand"]; ?>"><?php echo $val["NamaBrand"]; ?></option><?php
						}
	            		?>
	                </select>-->
	                &nbsp;
				
					<span style="float: right;">
						<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
					</span>
				
				</div>
			</div>
		</form>
		
		<hr/>
		
		
	<form method="POST"  name="search" action='<?=base_url();?>index.php/pop/pop_up_credit_note/add_rekening'>
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead class="title_table">
					<tr>
						<th width="10">No</th>
		                <th><nobr>No Rekening</nobr></th>
		                <th>Nama Rekening</th>
		                <!--<th>Action</th>-->
		        	</tr>
				</thead>
				<tbody>
				
				<?php
				if(count($rekening)==0)
				{
					echo "<tr><td colspan='100%'><center>Tidak Ada Data</center></td></tr>";
				}
				
				$i=1;
				foreach($rekening as $val)
				{
					$bgcolor = "";
                    if($i%2==0)
                    {
                        $bgcolor = "background:#E7E7E7;";
                    }
                    
					?>
					<tr onclick="get_choose('<?php echo trim($val["KdRekening"]); ?>','<?php echo trim($val["NamaRekening"]); ?>')" id="<?php echo $i; ?>">
						
						
						<td><?php echo $i; ?></td>
						<td><?php echo $val["KdRekening"]; ?></td>
						<td><?php echo $val["NamaRekening"]; ?></td>
						<?php
							if($this->uri->segment(4)==undefined){
							$cn_number='00000';
							}else{
							$cn_number = $this->uri->segment(4);
							}
							?>
							<input type="hidden" name="cnno" id="cnno" value="<?php echo $cn_number;?>" >
						<!--<td>
						<center>
							<?php
							if($this->uri->segment(4)==undefined){
							$si_number='00000';
							}else{
							$si_number = $this->uri->segment(4);
							}
							?>
							<a href="<?php echo base_url(); ?>index.php/pop/pop_up_credit_note/ambil_data/<?php echo $val["KdRekening"]; ?>/<?php echo $si_number;?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-download"></i></a>
						</center>
						</td>
						
						<td><center><input type='checkbox' name='ambil[]' value='<?php echo $val["KdRekening"];?>'></center></td>
					-->
					</tr>
					<?php
					$i++;
				}
				?>
             
				</tbody>
			</table> 

			<div class="row">
				<div class="col-xs-6 col-left">
					<div id="table-2_info" class="dataTables_info">&nbsp;</div>
				</div>
				<div class="col-xs-6 col-right">
					<div class="dataTables_paginate paging_bootstrap">
						<?php echo $pagination; ?>
					</div>
				</div>
			</div>
			<span style="float: right; display: none;">
						<button type="submit" class="btn btn-orange btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Ambil<i class="entypo-plus"></i></button>
					</span>
		</form>
		<br><br>
		</div>
		<center>
		<input type="button" class="btn btn-danger btn-md md-new tooltip-primary" name="btn_close" value="Close" onclick="CloseWindow('<?php echo base_url(); ?>')">
		<input type="button" class="btn btn-green btn-md md-new tooltip-primary" name="btn_close" value="Selesai" onclick="CloseWindow('<?php echo base_url(); ?>')">
		</center>
	</div>
</div>


<?php
		if($this->session->flashdata('msg'))
		{
		  //$msg = $this->session->flashdata('msg');
		  
		  ?><!--<div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div>-->
		  <script>
				var r=confirm("Apakah Anda Ingin Ambil Data Rekening Kembali ?")
				if (r==true)
				{
					window.location = url+"index.php/pop/pop_up_credit_note/index/0/1/";	
				}
				else
				{
					window.close();
			        window.opener.location.reload();
				}
		</script>
		  <?php
		}
		?>


</body>
</html>