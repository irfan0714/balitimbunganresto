<?php 
	$modul = "Search Kode Barang";
	
	$search_gudang = $this->uri->segment(4);
	$v_nilai = $this->uri->segment(5);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
		function start_page()
		{
			document.getElementById("search_keyword").focus();	
		}
        
        function get_choose(pcode, nama_lengkap)
        {
            window.opener.document.forms["theform"]["pcode"+<?php echo $v_nilai; ?>].value            = pcode;
            window.opener.document.forms["theform"]["v_namabarang"+<?php echo $v_nilai; ?>].value            = nama_lengkap;
            window.opener.document.getElementById("pcode"+<?php echo $v_nilai; ?>).focus();
			
            self.close() ;
            return; 
        }
	</script>
</head>

<body class="page-body skin-black" onload="start_page()">

<div class="page-container sidebar-collapsed" style="padding-left: 0px;">
    
    <div class="main-content">
		
		<form method="POST"  name="search" action='<?=base_url();?>index.php/pop/pop_up_penerimaan_lain/search'>
			<input type="hidden" name="v_nilai" id="v_nilai" value="<?php echo $v_nilai; ?>"/>
			<div class="row">
				<div class="col-md-12">
					<b>Keyword</b>&nbsp;
	               	<input type="text" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" size="20">
	               	&nbsp;
					<b>Gudang</b>&nbsp;
					<select class="form-control-new" name="search_gudang" id="search_gudang">
	            		<option value="">All</option>
	            		<?php
	            		foreach($mgudang as $val)
	            		{
							$selected="";
		        			if($search_gudang)
		        			{
		        				if($val["KdGudang"]==$search_gudang)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?> value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>  
	                &nbsp;
					<b>Divisi</b>&nbsp;
					<select class="form-control-new" name="search_divisi" id="search_divisi">
        				<option value="">All</option>
						<?php
	            		foreach($mdivisi as $val)
	            		{
							$selected="";
		        			if($search_divisi)
		        			{
		        				if($val["KdDivisi"]==$search_divisi)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?>  value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	                </select>
	                &nbsp;
					<b>Kategori</b>&nbsp;
					<select class="form-control-new" name="search_kategori" id="search_kategori">
        				<option value="">All</option>
						<?php
	            		foreach($mkategori as $val)
	            		{
							$selected="";
		        			if($search_kategori)
		        			{
		        				if($val["KdKategori"]==$search_kategori)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?>  value="<?php echo $val["KdKategori"]; ?>"><?php echo $val["NamaKategori"]; ?></option><?php
						}
	            		?>
	                </select>
	                &nbsp;
	                <b>Brand</b>&nbsp;
					<select class="form-control-new" name="search_brand" id="search_brand">
        				<option value="">All</option>
						<?php
	            		foreach($mbrand as $val)
	            		{
							$selected="";
		        			if($search_brand)
		        			{
		        				if($val["KdBrand"]==$search_brand)
		        				{
									$selected='selected="selected"';	
								}
							}
							
							?><option <?php echo $selected; ?>  value="<?php echo $val["KdBrand"]; ?>"><?php echo $val["NamaBrand"]; ?></option><?php
						}
	            		?>
	                </select>
	                &nbsp;
				
					<span style="float: right;">
						<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
					</span>
				
				</div>
			</div>
		</form>
		
		<hr/>
		
		<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
			
			<table class="table table-bordered responsive">
	        	<thead class="title_table">
					<tr>
						<th width="10">No</th>
		                <th>Kode Barang</th>
		                <th>Nama Barang</th>
		                <th>Barcode</th>
		                <th>Divisi</th>
		                <th>Sub Divisi</th>
		                <th>Kategori</th>
		                <th>Sub Kategori</th>
		                <th>Brand</th>
		                <th>Sub Brand</th>
		                <th>Gudang</th>
		        	</tr>
				</thead>
				<tbody>
				
				<?php
				if(count($barangdata)==0)
				{
					echo "<tr><td colspan='100%'><center>Tidak Ada Data</center></td></tr>";
				}
				
				$i=1;
				foreach($barangdata as $val)
				{
					$bgcolor = "";
                    if($i%2==0)
                    {
                        $bgcolor = "background:#E7E7E7;";
                    }
                    
					?>
					<tr onclick="get_choose('<?php echo $val["PCode"]; ?>','<?php echo $val["NamaLengkap"]; ?>')" title="<?php echo $val["NamaLengkap"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $i; ?>')" onmouseout="change_onMouseOut('<?php echo $i; ?>')" id="<?php echo $i; ?>">
						<td><?php echo $i; ?></td>
						<td><?php echo $val["PCode"]; ?></td>
						<td><?php echo $val["NamaLengkap"]; ?></td>
						<td><?php echo $val["Barcode1"]; ?></td>
						<td><?php echo $val["NamaDivisi"]; ?></td>
						<td><?php echo $val["NamaSubDivisi"]; ?></td>
						<td><?php echo $val["NamaKategori"]; ?></td>
						<td><?php echo $val["NamaSubKategori"]; ?></td>
						<td><?php echo $val["NamaBrand"]; ?></td>
						<td><?php echo $val["NamaSubBrand"]; ?></td>
						<td><?php echo $val["nama_gudang"]; ?></td>
					</tr>
					<?php
					$i++;
				}
				?>
             
				</tbody>
			</table> 

			<div class="row">
				<div class="col-xs-6 col-left">
					<div id="table-2_info" class="dataTables_info">&nbsp;</div>
				</div>
				<div class="col-xs-6 col-right">
					<div class="dataTables_paginate paging_bootstrap">
						<?php echo $pagination; ?>
					</div>
				</div>
			</div>	
		
		</div>
		
	</div>
</div>
</body>
</html>