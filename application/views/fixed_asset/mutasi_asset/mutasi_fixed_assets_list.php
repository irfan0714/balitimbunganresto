<?php
$this->load->view('header');
?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<link rel="stylesheet" href="<?=base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">

<link rel="stylesheet" href="<?=base_url();?>public/css_calendar/eventCalendar.css">
<link rel="stylesheet" href="<?=base_url();?>public/css_calendar/eventCalendar_theme_responsive.css">
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>-->

<script type="text/javascript">
//kosong
</script>

	<body>

		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-gradient" data-collapsed="0">
				<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
					<div class="panel-heading">
						<div class="panel-title">
						Filter Mutasi Asset Assets
						</div>
						
						<div class="panel-options">
							<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
							<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
						</div>
					</div>
					
					<div class="panel-body">
					
						<form method='post' action='<?= base_url(); ?>index.php/fixed_asset/mutasi_asset/<?=$action?>' role="form" class="form-horizontal form-groups-bordered">
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Departement</label>
								
								<div class="col-sm-5">
									
									<select name="Departement" id="Departement" class="select2" data-allow-clear="true" data-placeholder="Pilih Departement...">
										<option></option>
										<optgroup label="">
											<?php foreach($Departement as $rowkat){ ?>
											<option value="<?=$rowkat['KdDepartemen'];?>"><?=$rowkat['KdDepartemen'].' - '.$rowkat['NamaDepartemen'];?></option>
											<? } ?>
										</optgroup>
									</select>
									
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Lokasi</label>
								
								<div class="col-sm-5">
									
									<select name="Lokasi" id="Lokasi" class="select2" data-allow-clear="true" data-placeholder="Pilih Lokasi...">
										<option></option>
										<optgroup label="">
											<?php foreach($Lokasi AS $rowkat){ ?>
											<option value="<?=$rowkat['KdLokasi'];?>"><?=$rowkat['KdLokasi'].' - '.$rowkat['Lokasi'];?></option>
											<? } ?>
										</optgroup>
									</select>
									
								</div>
							</div>
							
							<div class="form-group" style="display: none">
								<label class="col-sm-2 control-label">Sort By Data Buku</label>
								
								<div class="col-sm-2">
									
									<select name="SortFromDataBuku" id="SortFromDataBuku" class="select2" data-allow-clear="true" data-placeholder="From">
										<option></option>
										<optgroup label="">
											<?php foreach($Sort_Data_Buku as $rowkat){ ?>
											<option value="<?=$rowkat['sort_data_buku'];?>"><?=$rowkat['sort_data_buku'];?></option>
											<? } ?>
										</optgroup>
									</select>
									
								</div>
								
								<div class="col-sm-2" >
									
									<select name="SortToDataBuku" id="SortToDataBuku" class="select2" data-allow-clear="true" data-placeholder="To">
										<option></option>
										<optgroup label="">
											<?php foreach($Sort_Data_Buku as $rowkat){ ?>
											<option value="<?=$rowkat['sort_data_buku'];?>"><?=$rowkat['sort_data_buku'];?></option>
											<? } ?>
										</optgroup>
									</select>
									
								</div>
							</div>

							<div class="form-group" style="display: none">
								<label class="col-sm-2 control-label">Data Buku</label>
								
								<div class="col-sm-5">
									
									<select name="DataBuku" id="DataBuku" class="select2" data-allow-clear="true" data-placeholder="Pilih Data Buku...">
										<option></option>
										<optgroup label="">
											<?php foreach($Data_Buku as $rowkat){ ?>
											<option value="<?=$rowkat['data_buku'];?>"><?=$rowkat['data_buku'];?></option>
											<? } ?>
										</optgroup>
									</select>
									
								</div>
							</div>
							
							<div class="form-group" style="display:block" id="lblproduct">
								<label class="col-sm-2 control-label">Nama Assets</label>
								
								<div class="col-sm-5">
									
									<!--<div id="dv_product">
									</div>-->
									<select name="Nama" id="Nama" class="select2" data-allow-clear="true" data-placeholder="Pilih Nama Assets...">
										<option></option>
										<optgroup label="">
											<?php foreach($Detail as $rowpro){ ?>
											<option value="<?=$rowpro['NoAsset'];?>"><?php echo $rowpro['nama_asset'];?></option>
											<? } ?>
										</optgroup>
									</select>
									
								</div>
							</div>
							
							<div class="form-group" style="display: none">
								<label class="col-sm-2 control-label">Jumlah</label>
								
								<div class="col-sm-5">
									
									<input type="text" name="jml" class="form-control" id="field-1" style="width:100px" value="1">
									
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label"> </label>
								<div class="col-sm-6">
									<button class="btn btn-green" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>View</button>
								</div>
							</div>
							
						</form>
					
					</div>
					
				</div>
			
			</div>
		</div>
		
		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-gradient" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
						List Mutasi Asset Assets
						</div>
						
						<div class="panel-options">
							<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
							<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
						</div>
					</div>
					
					<div class="panel-body">
					
					<table class="table table-bordered responsive">
			        <thead class="title_table">
			            <tr>
			                <th width="30"><center>No</center></th>
					        <th width="200"><center>Nomor Asset</center></th>
					        <th width="400"><center>Nama Assset</center></th>
					        <th width="200"><center>Brand</center></th>
					        <th><center>Keterangan</center></th>
			        	</tr>
			            
			        </thead>
			        <tbody>
			         <?php
			            if (count($Asset) == 0) {
			                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			            }
			            $no = 1;
			            $bgcolor = "";
		                if ($no % 2 == 0) {
		                    $bgcolor = "background:#f7f7f7;";
		                }
	                
			            
	                    foreach ($Asset as $val) {
	                    	
	                    ?>
	                    	<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk detail <?php echo $val["NoAsset"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
		                    <td><?php echo $no; ?></td>
		                    <td onclick="editing('<?php echo $val["NoAsset"]; ?>')" align="center"><?php echo $val["NoAsset"]; ?></td>
		                    <td onclick="editing('<?php echo $val["NoAsset"]; ?>')" align="center"><?php echo $val["nama_asset"]; ?></td>
		                    <td onclick="editing('<?php echo $val["NoAsset"]; ?>')" align="center"><?php echo $val["brand"]; ?></td>
		                    <td onclick="editing('<?php echo $val["NoAsset"]; ?>')" align="center"><?php echo $val["Keterangan"]; ?></td>
		                    </tr>
	                    <?php
	                    $no++;
	                    }
			            
			         ?>
			        </tbody>
			        </table>
					
					</div>
					
				</div>
			
			</div>
		</div>

	</body>

<link rel="stylesheet" href="<?=base_url();?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?=base_url();?>assets/js/select2/select2.css">
<script src="<?=base_url();?>assets/js/select2/select2.min.js"></script>
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?=base_url();?>public/js_calendar/moment.js" type="text/javascript"></script>
<script src="<?=base_url();?>public/js_calendar/jquery.eventCalendar.js" type="text/javascript"></script>
<!--<script src="<?=base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>-->
<?php $this->load->view('footer'); ?>

<script>
function editing(noasset){
var url=$('#base_url').val();

	window.location = url+"index.php/fixed_asset/mutasi_asset/view_asset/"+noasset;

	
}</script>