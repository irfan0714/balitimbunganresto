<?php
$this->load->view('header');
?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<link rel="stylesheet" href="<?=base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">

<link rel="stylesheet" href="<?=base_url();?>public/css_calendar/eventCalendar.css">
<link rel="stylesheet" href="<?=base_url();?>public/css_calendar/eventCalendar_theme_responsive.css">
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>-->

<script type="text/javascript">
//kosong
</script>

<body>



	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-gradient" data-collapsed="0">
			<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
				<div class="panel-heading">
					<div class="panel-title">
					Form Mutasi Asset Assets
					</div>
					
					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
						<!--<<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->
					</div>
				</div>
				
				<div class="panel-body">
				
					<form method='post' action='<?= base_url(); ?>index.php/fixed_asset/mutasi_asset/<?=$action?>' role="form" class="form-horizontal form-groups-bordered">
						
						<div class="form-group">
							<label class="col-sm-2 control-label">No.Asset</label>
							<div class="col-sm-5">
								<label class="control-label"><b><?= $this->uri->segment(4);?></b></label>
								<input type='hidden' name="NoAsset" id="NoAsset" value="<?= $this->uri->segment(4);?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Nama Asset</label>
							
							<div class="col-sm-5">
								<label class="control-label"><b><?= $History[0]["nama_asset"];?></b></label>
							</div>
						</div>
						
						<div class="form-group" style="display: ">
							<label class="col-sm-2 control-label">Tanggal Mutasi</label>
							
							<div class="col-sm-2">
								<input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" name="tanggal_mutasi" id="tanggal_mutasi" size="10" maxlength="10">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Departemen</label>
							
							<div class="col-sm-5">
								
								<select name="Departement" id="Departement" class="select2" data-allow-clear="true" data-placeholder="Pilih Departement...">
									<option></option>
									<optgroup label="">
										<?php foreach($Departement as $rowkat){ ?>
										<option value="<?=$rowkat['KdDepartemen'];?>"><?=$rowkat['KdDepartemen'].' - '.$rowkat['NamaDepartemen'];?></option>
										<? } ?>
									</optgroup>
								</select>
								
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">Lokasi</label>
							
							<div class="col-sm-5">
								
								<select name="Lokasi" id="Lokasi" class="select2" data-allow-clear="true" data-placeholder="Pilih Lokasi...">
									<option></option>
									<optgroup label="">
										<?php foreach($Lokasi AS $rowkat){ ?>
										<option value="<?=$rowkat['KdLokasi'];?>"><?=$rowkat['KdLokasi'].' - '.$rowkat['Lokasi'];?></option>
										<? } ?>
									</optgroup>
								</select>
								
							</div>
						</div>
						
						<div class="form-group" style="display: ">
							<label class="col-sm-2 control-label">Qty</label>
							
							<div class="col-sm-1">
								<input type="text" class="form-control input-sm" style="font-size: 20px;text-align: right;height:30px;margin-bottom:5px;" value="" name="qty" id="qty">
							</div>
						</div>
						
						<div class="form-group" style="display: ">
							<label class="col-sm-2 control-label">Keterangan</label>
							
							<div class="col-sm-10">
								<input type="text" class="form-control input-sm-8" style="font-size: 20px;text-align: left;height:30px;margin-bottom:5px;" value="" name="ket" id="ket" size="50">
							</div>
						</div>
						
						<div class="form-group" style="display: ">
							<label class="col-sm-2 control-label">Upload Doc. <input type="hidden" name="sts_photo" id="sts_photo" value="0"/></label>
							
							<div class="col-sm-10">
						
								<input class="form-control input-sm-4" type="file" name="filegambar" id="filegambar" style="width:250px"> 
								<br>
								<button type="button" class="btn btn-info btn-sm" id='tambahgambar'>Upload</button>
								<button style="display: none;" type="button" class="btn btn-danger btn-sm" id='tutup_images' onclick="tutup()">Tutup Gambar</button>
            	                <button style="display: none;" type="button" class="btn btn-success btn-sm" id='buka_lagi_images' onclick="buka()">Buka Gambar</button>
				            	   <script>
								        $(document).on('click','#tambahgambar',function(e){
								            e.preventDefault();
								            var file_data = $('#filegambar').prop('files')[0];
								            var form_data = new FormData();
								 			
								            form_data.append('file', file_data);
								            $.ajax({
								                url: '<?php echo site_url("fixed_asset/mutasi_asset/uploadgambar") ?>', // point to server-side PHP script
								                dataType: 'json',  // what to expect back from the PHP script, if anything
								                cache: false,
								                contentType: false,
								                processData: false,
								                data: form_data,
								                type: 'post',
								                success: function(data,status){
								                    //alert(php_script_response); // display response from the PHP script, if any
								                    if (data.status!='error') {
								                    	//untuk mereset imagesnya
								                        //$('#filegambar').val('');
								                        alert(data.msg);
								                        
								                        //tampil isi asset
								                        $('#sts_photo').val('1');
								                        $("#isi_asset").css("display","");
								                        
								                        //load imagesnya
								                        imagesnya = data.nama_file;
								                        var urls = $("#base_url").val();
								                        $.ajax({
																url: urls+"index.php/fixed_asset/mutasi_asset/viewImages/",
																data: {img:imagesnya},
																type: "POST",
																dataType: 'html',					
																success: function(res)
																{
																	$("#tutup_images").css("display","");
																	$("#td_bukaImages").css("display","");
																	$('#td_viewImages').html(res);
																},
																error: function(e) 
																{
																	alert(e);
																} 
													      });
								                        
								                    }else{
								                        alert(data.msg);
								                    }
								                }
								            });
								        })
								    </script>
					
							</div>
						</div>
						
						<div class="form-group" style="display:" id="td_bukaImages">
							<label class="col-sm-2 control-label">&nbsp;</label>
							
							<div class="col-sm-10">
								<span id="td_viewImages">
									<!-- load images dari controller -->
							    </span>
							</div>
							
						</div>
						
						<div class="form-group" style="display:block" id="lblproduct">
							<label class="col-sm-2 control-label">PIC</label>
							
							<div class="col-sm-5">
								
								<!--<div id="dvduct">
								</div>-->
								<select name="PIC" id="PIC" class="select2" data-allow-clear="true" data-placeholder="Pilih Nama PIC...">
									<option></option>
									<optgroup label="">
										<?php foreach($Employee as $val){ ?>
										<option value="<?=$val['employee_id'];?>"><?php echo $val['employee_name'];?></option>
										<? } ?>
									</optgroup>
								</select>
								
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label"> </label>
							<div class="col-sm-6">
								<button class="btn btn-green" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Mutasi</button>
							</div>
						</div>
						
					</form>
				
				</div>
				
			</div>
		
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-gradient" data-collapsed="0">
			
				<div class="panel-heading">
					<div class="panel-title">
					History Mutasi Asset Assets : <?= $History[0]["NoAsset"]." - ".$History[0]["nama_asset"];?>
					</div>
					
					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
						<!--<<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->
					</div>
				</div>
				
				<div class="panel-body">
				
				<table class="table table-bordered responsive">
		        <thead class="title_table">
		            <tr>
		                <th width="30"><center>No</center></th>
		                <th width="50"><center>Tanggal</center></th>
				        <th width="125"><center>Nomor Asset</center></th>
				        <th width="400"><center>Nama Assset</center></th>
				        <th width="120"><center>Departemen</center></th>
				        <th width="120"><center>Lokasi</center></th>
				        <th><center>Keterangan</center></th>
				        <th width="150"><center>PIC</center></th>		
				        <th width="75"><center>Dokumen</center></th>		        
				        <th width="30"><center>Action</center></th>
		        	</tr>
		            
		        </thead>
		        <tbody>
		         <?php
		            if (count($History) == 0) {
		                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
		            }
		            $no = 1;
		            $bgcolor = "";
	                if ($no % 2 == 0) {
	                    $bgcolor = "background:#f7f7f7;";
	                }
                
		            
                    foreach ($History as $val) {
                    	
                    ?>
                    	<tr style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
	                    <td><?php echo $no; ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="center"><?= $val["tgl_mutasi"]; ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="left"><?php echo $val["NoAsset"].".".$val["Departemen"].".".$val["Lokasi"]; ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="left"><?php echo $val["nama_asset"] ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="left"><?php echo $val["NamaDepartemen"]; ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="left"><?php echo $val["NamaLokasi"]; ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="left"><?php echo $val["Keterangan"]; ?></td>
	                    <td onclick="views('<?php echo $val["NoAsset"]; ?>')" align="left"><?= $val["employee_name"]; ?></td>
	                    <td align="center">
	                    <?php if(!empty($val["file_dokumen"])){;?>
	                    	<button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="View Dokumen" title="" onclick="view_dok('<?= $val["file_dokumen"]; ?>')" >
                                <i class="entypo-eye"></i>
                            </button>
                        <?php };?>
	                    </td>
	                    <td align="center">
	                    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?= $val["NoAsset"]; ?>','<?= $val["Departemen"]; ?>','<?= $val["Lokasi"]; ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
	                    </td>
	                    </tr>
                    <?php
                    $no++;
                    }
		            
		         ?>
		        </tbody>
		        </table>
				
				</div>
				
			</div>
		
		</div>
	</div>



</body>


          <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>Loading...</h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<link rel="stylesheet" href="<?=base_url();?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?=base_url();?>assets/js/select2/select2.css">
<script src="<?=base_url();?>assets/js/select2/select2.min.js"></script>
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?=base_url();?>public/js_calendar/moment.js" type="text/javascript"></script>
<script src="<?=base_url();?>public/js_calendar/jquery.eventCalendar.js" type="text/javascript"></script>
<!--<script src="<?=base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>-->
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
    function views(noasset){
	    var url=$('#base_url').val();
		window.location = url+"index.php/fixed_asset/fixed_asset/edit_asset/"+noasset;
	}
	
	function deleteTrans(noasset,dep,lok){
	    var url=$('#base_url').val();
		window.location = url+"index.php/fixed_asset/mutasi_asset/delete_asset/"+noasset+"/"+dep+"/"+lok;
	}
	
	function tutup(){
			$("#td_tutupImages").css("display","");
			$("#td_bukaImages").css("display","none");
			$("#tutup_images").css("display","none");
			$("#buka_lagi_images").css("display","");
		}
		
		function buka(){
			$("#td_tutupImages").css("display","");
			$("#td_bukaImages").css("display","");
			$("#tutup_images").css("display","");
			$("#buka_lagi_images").css("display","none");
		}
		
		function view_dok(filegambar){
			base_url = $("#base_url").val();
		    url = base_url+"index.php/pop/pop_up_images/index/"+filegambar;
		    windowOpener(525, 1000, 'Images', url, 'Images')
		}
</script>