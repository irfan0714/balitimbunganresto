<?php 

$this->load->view('header'); 

$modul = "Fixed Asset";

?>
	
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/fixed_asset/fixed_asset/save_data'>
		<input type="hidden" id="otorisasi" name="otorisasi" value="<?php echo $otorisasi;?>"/>
		<h4><font color="#848484"><b>Klasifikasi Asset</b></font></h4>
	    <table class="table table-bordered responsive">   
	    	
	    	<tr>
                <td class="title_table" width="200"><h4><font color="#ffffff"><b>Nomor Asset</b></font></h4></td>
                <td colspan="100%"><h4><b><?=$data->NoAsset.".".$data->Departemen.".".$data->Lokasi;?></b></h4>
                <input type="hidden" id="No_Asset" name="No_Asset" value="<?=$data->NoAsset;?>"/>
                </td>
                     
            </tr>
                                 
	        <tr>
                <td class="title_table" width="200">Item Asset</td>
                <td colspan="100%">
                    <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_ItemAsset" id="v_ItemAsset" value="<?=$data->ItemOtherAsset;?>" maxlength="50"> 
                </td> 
            </tr> 
            
            <tr>
                <td class="title_table" width="200">Type Asset</td>
                <td width="500">
                    <select name="v_TypeAsset" id="v_TypeAsset" class="form-control-new" style="border-color: #aeb0b2;width: 200px;" onchange="CallAjaxForm('ajax_type_asset', this.value)">  
                        <option value=""> -- Type Asset -- </option>
                        <?php 
                            foreach($type_asset as $val)
                            {
                            	$selected="";
								if($data->TypeAsset==$val["KdTypeAsset"])
								{
									$selected='selected="selected"';
								}
                                ?>
                                <option <?=$selected;?> value="<?php echo $val['KdTypeAsset']; ?>"><?php echo $val['NamaTypeAsset']; ?></option>
                                <?php
                            }
                        ?>
                    </select>                    
                    <span id="span_loading_TypeAsset" style="border-color: #aeb0b2;display: none;"><img src="../../../../public/images/ajax-image.gif"/></span>
                </td> 
                
                <td class="title_table" width="150" >SubType Asset</td>
	            <td colspan="3" id="td_SubTypeAsset"> 
	            	   <select class="form-control-new" name="v_SubTypeAsset" id="v_SubTypeAsset" style="border-color: #aeb0b2;width: 200px;">
		            		<option value="">-- SubType Asset --</option>
		            		<?php 
                            foreach($sub_type_asset as $val)
                            {
                            	$selected="";
								if($data->SubTypeAsset==$val["KdSubTypeAsset"] AND $data->TypeAsset==$val["KdTypeAsset"])
								{
									$selected='selected="selected"';
								}
                                ?>
                                <option <?=$selected;?> value="<?php echo $val['KdSubTypeAsset']; ?>"><?php echo $val['NamaSubTypeAsset']; ?></option>
                                <?php
                            }
                        	?>
		            	</select>
	            </td>
                
            </tr>
            
	    </table>
	    
	    <h4><font color="#848484"><b>Deskripsi Asset</b></font></h4>
	    <table class="table table-bordered responsive">            
	        <tr>
                <td class="title_table" width="200">Nama Asset</td>
                <td width="500">
                     <input value="<?= $data->nama_asset;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_nama_asset" id="v_nama_asset" maxlength="50"> 
                </td> 
                
                <td class="title_table" width="150" >Brand</td>
	            <td colspan="3">
	            	   <input value="<?= $data->brand;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_brand" id="v_brand" maxlength="50">
	            </td>
                
            </tr> 
            
            <tr>
                <td class="title_table" width="200">Volume</td>
                <td width="500">
                     <input value="<?= $data->Volume;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Volume" id="v_Volume" maxlength="50"> 
                </td> 
                
                <td class="title_table" width="150" >Size</td>
	            <td colspan="3">
	            	   <input value="<?= $data->Size;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Size" id="v_Size" maxlength="50">
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Qty & Satuan</td>
                <td width="500">
                     <input value="<?= $data->Qty;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Qty" id="v_Qty" maxlength="50">
                     &nbsp;
                     <select class="form-control-new" name="v_Satuan" id="v_Satuan" style="border-color: #aeb0b2;width: 70px;">
		            		<option <?php if($data->Satuan=="Unit"){ echo "selected='selected'"; } ?> value="Unit">Unit</option>
		            		<option <?php if($data->Satuan=="Set"){ echo "selected='selected'"; } ?> value="Set">Set</option>
		            	</select>  
                </td> 
                
                <td class="title_table" width="150" >Kondisi</td>
	            <td colspan="3">
	            	   <select class="form-control-new" name="v_Kondisi" id="v_Kondisi" style="border-color: #aeb0b2;width: 70px;">
		            		<option <?php if($data->Kondisi=="Baik"){ echo "selected='selected'"; } ?> value="Baik">Baik</option>
		            		<option <?php if($data->Kondisi=="Rusak"){ echo "selected='selected'"; } ?> value="Rusak">Rusak</option>
		            	</select>
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Keterangan</td>
                <td width="500">
                       <input value="<?= $data->Keterangan;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_Keterangan" id="v_Keterangan" maxlength="50">
                </td> 
                
                <td class="title_table" width="150" >Photo <input type="hidden" name="sts_photo" id="sts_photo" value="<?php if($data->filegambar!=""){echo "1";}else{echo "0";};?>"/></td>
	            <td width="300">
	            	   <input class="form-control" type="file" name="filegambar" id="filegambar" style="width:250px">  
	            </td>   
	            
	            <td>
	            	<button type="button" class="btn btn-info btn-sm" id='tambahgambar'>Upload</button>
	            	   
	            	   <script>
					        $(document).on('click','#tambahgambar',function(e){
					            e.preventDefault();
					            var file_data = $('#filegambar').prop('files')[0];
					            var form_data = new FormData();
					 			
					            form_data.append('file', file_data);
					            $.ajax({
					                url: '<?php echo site_url("fixed_asset/fixed_asset/uploadgambar") ?>', // point to server-side PHP script
					                dataType: 'json',  // what to expect back from the PHP script, if anything
					                cache: false,
					                contentType: false,
					                processData: false,
					                data: form_data,
					                type: 'post',
					                success: function(data,status){
					                    //alert(php_script_response); // display response from the PHP script, if any
					                    if (data.status!='error') {
					                    	//untuk mereset imagesnya
					                        //$('#filegambar').val('');
					                        alert(data.msg);
					                        
					                        //update
					                        $('#sts_photo').val('1');
					                        
					                        //load imagesnya
					                        imagesnya = data.nama_file;
					                        var urls = $("#base_url").val();
					                        $.ajax({
													url: urls+"index.php/fixed_asset/fixed_asset/viewImages/",
													data: {img:imagesnya},
													type: "POST",
													dataType: 'html',					
													success: function(res)
													{
														$("#td_tutupImages").css("display","");
														$("#td_bukaImages").css("display","");
														$('#td_viewImages').html(res);
													},
													error: function(e) 
													{
														alert(e);
													} 
										      });
					                        
					                    }else{
					                        alert(data.msg);
					                    }
					                }
					            });
					        })
					    </script>
	            </td>             
            </tr>
            
            <tr id="td_tutupImages" style="display: ;">
            <td colspan="100%" align="right">
            	 <button style="display: ;" type="button" class="btn btn-danger btn-sm" id='tutup_images' onclick="tutup()">Tutup Gambar</button>
            	 <button style="display: none;" type="button" class="btn btn-success btn-sm" id='buka_lagi_images' onclick="buka()">Buka Gambar</button>
            </td>
            </tr>
            <tr id="td_bukaImages" style="display: ;">
            <td colspan="100%" align="center" id="td_viewImages">
            	 <span id='span_loading_TypeAsset' style='border-color: #aeb0b2;display: ;'><img src='../../../../public/fixed_assets/<?=$data->filegambar;?>' width='500' height='350'/></span>
            </td>
            </tr>
            
	    </table>	    
	    
	    <h4><font color="#848484"><b>Penempatan</b></font></h4>
	    <table class="table table-bordered responsive">                        
	        <tr>
                <td class="title_table" width="200">Lokasi</td>
                <td width="500">
                     
                     <select class="form-control-new" name="v_Lokasi" id="v_Lokasi" style="border-color: #aeb0b2; width: 200px;">
	            		<option value="">-- Pilih Lokasi --</option>
	            		<?php
	            		foreach($lokasi as $val)
	            		{
							    $selected="";
								if($data->Lokasi==$val["KdLokasi"])
								{
									$selected='selected="selected"';
								}
                                ?>
                            <option <?= $selected ;?> value="<?php echo $val["KdLokasi"]; ?>"><?php echo $val["Lokasi"]; ?></option><?php
						}
	            		?>
	            	</select>
	            	 
                </td> 
                
                <td class="title_table" width="150" >Departemen</td>
	            <td colspan="3">
	            	<select class="form-control-new" name="v_Departemen" id="v_Departemen" style="border-color: #aeb0b2; width: 200px;">
	            		<option value="">-- Pilih Departemen --</option>
	            		<?php
	            		foreach($departemen as $val)
	            		{
							    $selected="";
								if($data->Departemen==$val["KdDepartemen"])
								{
									$selected='selected="selected"';
								}
                                ?>
                            <option <?= $selected ;?> value="<?php echo $val["KdDepartemen"]; ?>"><?php echo $val["NamaDepartemen"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Yang Bertanggungjawab</td>
                <td colspan="100%">
                       <select class="form-control-new" name="v_PenanggungJawab" id="v_PenanggungJawab" style="border-color: #aeb0b2;width: 100px;">
		            		<option <?php if($data->PenanggungJawab==""){ echo "selected='selected'"; } ?>  value="">-- Pilih Tipe --</option>
		            		<option <?php if($data->PenanggungJawab=="personal"){ echo "selected='selected'"; } ?> value="personal">Personal</option>
		            		<option <?php if($data->PenanggungJawab=="umum"){ echo "selected='selected'"; } ?> value="umum">Umum</option>
		            	</select>
		            	&nbsp;
              			
                       <select class="form-control-new" name="v_Pegawai" id="v_Pegawai" style="border-color: #aeb0b2;width: 290px;">
		            		<option value="">-- Pilih Pegawai --</option>
		            		<?php
		            		foreach($employee as $val)
		            		{
								$selected="";
								if($data->Pegawai==$val["employee_id"])
								{
									$selected='selected="selected"';
								}
                                ?>
                                <option <?= $selected ;?> value="<?php echo $val["employee_id"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
							}
		            		?>
		            	</select>
		            	
                </td>              
            </tr>
            
	    </table>
	    
	    <div id="otorisasi_akunting" style="display: none;">
	    <ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Accounting <?php echo $modul; ?></strong></li>
		</ol>
	    <!--<h4><font color="#848484"><b>Akunting</b></font></h4>-->
	    <table class="table table-bordered responsive">                        
	        <tr>
                <td class="title_table" width="200">Tanggal Beli</td>
                <td width="500">
                     <input style="border-color: #aeb0b2;" type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_beli" id="v_tgl_beli" size="10" maxlength="10"> 
                </td> 
                
                <td class="title_table" width="150" >Nominal</td>
	            <td colspan="3">
	            	 <input value="<?= $data->Nominal;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 200px; text-align: right; text-transform: uppercase;" name="v_Nominal" id="v_Nominal">
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">No.Proposal</td>
                <td width="500">
                     <input value="<?= $data->NoProposal;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 200px; text-align: left; text-transform: uppercase;" name="v_NoProposal" id="v_NoProposal"> 
                </td> 
                
                <td class="title_table" width="150" >Metode Penyusutan</td>
	            <td colspan="3" id="td_go_pajak">
	            	 <input readonly value="<?= $data->MetodePenyusutan;?>" type="text" class="form-control-new" style="border-color: #aeb0b2; width: 80px; text-align: right; text-transform: uppercase;" name="v_MetodePenyusutan" id="v_MetodePenyusutan">  
                	 <span id="span_loading_gol_pajak1" style="border-color: #aeb0b2;display: none;"><img src="../../../../public/images/ajax-image.gif"/></span>
	            </td>
                
            </tr>
            
	    </table>
	    </div>
	    
	    <table class="table table-bordered responsive"> 
		           	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit_other">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/fixed_asset/fixed_asset/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    
	    </form> 
        
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
		$(document).ready(function()
		{
			var pic = $("#v_PenanggungJawab").val();
				if(pic=="personal"){
					$("#v_Pegawai").css("display","");
				}else{
					$("#v_Pegawai").css("display","none");
				}	
				
			var oto = $("#otorisasi").val();
				if(oto=="Y"){
					document.getElementById("otorisasi_akunting").style.display = "";
				}else{
					document.getElementById("otorisasi_akunting").style.display = "none";
				}
		});
		
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='ajax_item_asset')
				{
					v_ItemAsset = document.getElementById("v_ItemAsset").value;
					
					$("#span_loading_CodeItemAsset").css("display","");

					 var url = $("#base_url").val();
				     
						  $.ajax({
								url: url+"index.php/fixed_asset/fixed_asset/getCodeItem/",
								data: {itemasset:v_ItemAsset},
								type: "POST",
								dataType: 'html',					
								success: function(res)
								{
									$("#span_loading_CodeItemAsset").css("display","none");
									$('#td_CodeItemAsset').html(res);
								},
								error: function(e) 
								{
									alert(e);
								} 
					      });
				}
                else if(tipenya=='ajax_type_asset')
                {
					   v_TypeAsset = document.getElementById("v_TypeAsset").value;
					
						$("#span_loading_TypeAsset").css("display","");

						 var url = $("#base_url").val();
					     
							  $.ajax({
									url: url+"index.php/fixed_asset/fixed_asset/getTypeItem/",
									data: {typeasset:v_TypeAsset},
									type: "POST",
									dataType: 'html',					
									success: function(res)
									{
										$("#span_loading_TypeAsset").css("display","none");
										$('#td_SubTypeAsset').html(res);
									},
									error: function(e) 
									{
										alert(e);
									} 
						   });
                }
                     
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function gol_pajak(){
			 v_CodeAsset = document.getElementById("v_CodeAsset").value;
			 $("#span_loading_accept").css("display","none");
			 $("#span_loading_gol_pajak1").css("display","");

						 var url = $("#base_url").val();
					     
							  $.ajax({
									url: url+"index.php/fixed_asset/fixed_asset/getGolPajak/",
									data: {code_asset:v_CodeAsset},
									type: "POST",
									dataType: 'html',					
									success: function(res)
									{
										$("#span_loading_gol_pajak1").css("display","none");
										$("#span_loading_accept").css("display","");
										$('#td_go_pajak').html(res);
									},
									error: function(e) 
									{
										alert(e);
									} 
						   });
		}
		
		function tutup(){
			$("#td_tutupImages").css("display","");
			$("#td_bukaImages").css("display","none");
			$("#tutup_images").css("display","none");
			$("#buka_lagi_images").css("display","");
		}
		
		function buka(){
			$("#td_tutupImages").css("display","");
			$("#td_bukaImages").css("display","");
			$("#tutup_images").css("display","");
			$("#buka_lagi_images").css("display","none");
		}
		
		function cekTheform()
		{		
				if($("#v_ItemAsset").val()==""){
					alert("Item Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_CodeAsset").val()==""){
					alert("Code Item Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_TypeAsset").val()==""){
					alert("Type Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_SubTypeAsset").val()==""){
					alert("Type Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_nama_asset").val()==""){
					alert("Nama Asset Harus Diisi...");
					$('#v_nama_asset').focus();
					return false;
				}
				
				if($("#v_brand").val()==""){
					alert("Brand Harus Diisi...");
					$('#v_brand').focus();
					return false;
				}
				
				if($("#v_Volume").val()==""){
					alert("Volume Harus Diisi...");
					$('#v_Volume').focus();
					return false;
				}
				
				if($("#v_Size").val()==""){
					alert("Size Harus Diisi...");
					$('#v_Size').focus();
					return false;
				}
				
				if($("#v_Qty").val()==""){
					alert("Qty Harus Diisi...");
					$('#v_Qty').focus();
					return false;
				}
				
				if($("#v_Satuan").val()==""){
					alert("Satuan Harus Dipilih...");
					return false;
				}
				
				if($("#v_Kondisi").val()==""){
					alert("Kondisi Harus Dipilih...");
					return false;
				}
				
				if($("#v_Keterangan").val()==""){
					alert("Keterangan Harus Diisi...");
					$('#v_Keterangan').focus();
					return false;
				}
				
				if($("#v_Nominal").val()==""){
					alert("Nominal Harus Diisi...");
					$('#v_Nominal').focus();
					return false;
				}
				
				if($("#v_Proposal").val()==""){
					alert("Nominal Harus Diisi...");
					$('#v_Proposal').focus();
					return false;
				}
				
				if($("#v_Lokasi").val()==""){
					alert("Lokasi Harus Dipilih...");
					return false;
				}
				
				if($("#v_Departemen").val()==""){
					alert("Departemen Harus Dipilih...");
					return false;
				}
				
				/*if($("#v_Pegawai").val()==""){
					alert("Pegawai Harus Dipilih...");
					return false;
				}*/
				
				if($("#sts_photo").val()=="0"){
					
					$(document).on('click','#btn_save',function(e){
					            e.preventDefault();
					            var file_data = $('#filegambar').prop('files')[0];
					            var form_data = new FormData();
					 			
					            form_data.append('file', file_data);
					            $.ajax({
					                url: '<?php echo site_url("fixed_asset/fixed_asset/uploadgambar") ?>', // point to server-side PHP script
					                dataType: 'json',  // what to expect back from the PHP script, if anything
					                cache: false,
					                contentType: false,
					                processData: false,
					                data: form_data,
					                type: 'post',
					                success: function(data,status){
					                    //alert(php_script_response); // display response from the PHP script, if any
					                    if (data.status!='error') {
					                    	
					                        //alert(data.msg);
					                        $('#sts_photo').val('1');
					                        document.getElementById("theform").submit();
					                        
					                    }else{
					                        alert(data.msg);
					                    }
					                }
					            });
					        })
					
					return false;
				}
											
			    var yesSubmit = true;
		    	
		        if(yesSubmit)
		        {
					document.getElementById("theform").submit();	
				}  
			
		}
		
</script>