<?php 

$this->load->view('header'); 

$modul = "Fixed Asset";

?>
	
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name=theform' id='theform' action='<?=base_url();?>index.php/fixed_asset/fixed_asset/save_data'>
		<input type="hidden" id="otorisasi" name="otorisasi" value="<?php echo $otorisasi;?>"/>
		<table class="table table-bordered responsive">    
	                            
	        <tr>
                <td class="title_table" width="200">No.Proposal</td>
                <td colspan="100%">
                     <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 200px; text-align: left; text-transform: uppercase;" name="v_NoProposal" id="v_NoProposal"> 
                </td>
                
            </tr> 
            
            <tr>
                <td class="title_table" width="200">Departement Proposal</td>
                <td width="500">
                    <select class="form-control-new" name="v_Departemen_proposal" id="v_Departemen_proposal" style="border-color: #aeb0b2; width: 200px;" onchange="pilih_departemen()">
	            		<option value="">-- Pilih Departemen --</option>
	            		<?php
	            		foreach($departemen as $val)
	            		{
							?><option value="<?php echo $val["KdDepartemen"]; ?>"><?php echo $val["NamaDepartemen"]; ?></option><?php
						}
	            		?>
	            	</select>
                </td> 
                
               <td class="title_table" width="150" >Image Proposal <input type="hidden" name="sts_photo_pro" id="sts_photo_pro" value="0"/></td>
	            <td width="300">
	            	   <input class="form-control" type="file" name="filegambar_pro" id="filegambar_pro" style="width:250px">  
	            </td>   
	            
	            <td>
	            	<button type="button" class="btn btn-info btn-sm" id='tambahgambar_pro'>Upload</button>
	            	   
	            	   <script>
					        $(document).on('click','#tambahgambar_pro',function(e){
					            e.preventDefault();
					            var file_data = $('#filegambar_pro').prop('files')[0];
					            var form_data = new FormData();
					 			
					            form_data.append('file', file_data);
					            $.ajax({
					                url: '<?php echo site_url("fixed_asset/fixed_asset/uploadgambar_pro") ?>', // point to server-side PHP script
					                dataType: 'json',  // what to expect back from the PHP script, if anything
					                cache: false,
					                contentType: false,
					                processData: false,
					                data: form_data,
					                type: 'post',
					                success: function(data,status){
					                    //alert(php_script_response); // display response from the PHP script, if any
					                    if (data.status!='error') {
					                    	//untuk mereset imagesnya
					                        //$('#filegambar').val('');
					                        alert(data.msg);
					                        
					                        //tampil isi asset
					                        $('#sts_photo_pro').val('1');
					                        $("#isi_asset").css("display","");
					                        
					                        //load imagesnya
					                        imagesnya = data.nama_file;
					                        var urls = $("#base_url").val();
					                        $.ajax({
													url: urls+"index.php/fixed_asset/fixed_asset/viewImages_pro/",
													data: {img:imagesnya},
													type: "POST",
													dataType: 'html',					
													success: function(res)
													{
														$("#td_tutupImages_pro").css("display","");
														$("#td_bukaImages_pro").css("display","");
														$('#td_viewImages_pro').html(res);
													},
													error: function(e) 
													{
														alert(e);
													} 
										      });
					                        
					                    }else{
					                        alert(data.msg);
					                    }
					                }
					            });
					        })
					    </script>
	            </td>
                
            </tr>
            
            <tr id="td_tutupImages_pro" style="display: none;">
            <td colspan="100%" align="right">
            	 <button style="display: ;" type="button" class="btn btn-danger btn-sm" id='tutup_images_pro' onclick="tutup_pro()">Tutup Gambar</button>
            	 <button style="display: none;" type="button" class="btn btn-success btn-sm" id='buka_lagi_images_pro' onclick="buka_pro()">Buka Gambar</button>
            </td>
            </tr>
            <tr id="td_bukaImages_pro" style="display: none;">
            <td colspan="100%" align="center" id="td_viewImages_pro">
            	 <!-- diambil dari load controller -->
            </td>
            </tr> 
	    </table>
	    
	    <div id="isi_asset" style="display: none;">
		<h4><font color="#848484"><b>Klasifikasi Asset</b></font></h4>
	    <table class="table table-bordered responsive">                        
	        <tr>
                <td class="title_table" width="200">Item Asset</td>
                <td colspan="100%">
                    <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_ItemAsset" id="v_ItemAsset" maxlength="50"> 
                </td> 
            </tr> 
            
            <tr>
                <td class="title_table" width="200">Type Asset</td>
                <td width="500">
                    <select name="v_TypeAsset" id="v_TypeAsset" class="form-control-new" style="border-color: #aeb0b2;width: 200px;" onchange="CallAjaxForm('ajax_type_asset_other_asset', this.value)">  
                        <option value=""> -- Type Asset -- </option>
                        <?php 
                            foreach($type_asset as $val)
                            {
                                ?>
                                <option value="<?php echo $val['KdTypeAsset']; ?>"><?php echo $val['NamaTypeAsset']; ?></option>
                                <?php
                            }
                        ?>
                    </select>                    
                    <span id="span_loading_TypeAsset" style="border-color: #aeb0b2;display: none;"><img src="../../../../public/images/ajax-image.gif"/></span>
                </td> 
                
                <td class="title_table" width="150" >SubType Asset</td>
	            <td colspan="3" id="td_SubTypeAsset"> 
	            	   <select class="form-control-new" name="v_SubTypeAsset" id="v_SubTypeAsset" style="border-color: #aeb0b2;width: 200px;">
		            		<option value="">-- SubType Asset --</option>
		            	</select>
	            </td>
                
            </tr>
            
	    </table>
	    
	    <h4><font color="#848484"><b>Deskripsi Asset</b></font></h4>
	    <table class="table table-bordered responsive">            
	        <tr>
                <td class="title_table" width="200">Nama Asset</td>
                <td width="500">
                     <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_nama_asset" id="v_nama_asset" maxlength="50"> 
                </td> 
                
                <td class="title_table" width="150" >Brand</td>
	            <td colspan="3">
	            	   <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_brand" id="v_brand" maxlength="50">
	            </td>
                
            </tr> 
            
            <tr>
                <td class="title_table" width="200">Volume</td>
                <td width="500">
                     <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Volume" id="v_Volume" maxlength="50"> 
                </td> 
                
                <td class="title_table" width="150" >Size</td>
	            <td colspan="3">
	            	   <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Size" id="v_Size" maxlength="50">
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Qty & Satuan</td>
                <td width="500">
                     <input type="text" class="form-control-new"  style="text-align:right; border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Qty" id="v_Qty" maxlength="50">
                     &nbsp;
                       <select class="form-control-new" onchange="set()" name="v_Satuan" id="v_Satuan" style="border-color: #aeb0b2;width: 90px;">
		            		<option value="">-- pilih --</option>
		            		<option value="Set">Set</option>
		            		<option value="Unit">Unit</option>
		            	</select> 
		             <span id="v_Detail_setting" style="display: none;">
		             &nbsp;
		             <input type="text" class="form-control-new" style="text-align:right; border-color: #aeb0b2; width: 70px;  text-transform: uppercase;" name="v_Qty_Detail_Set" id="v_Qty_Detail_Set" value="1" maxlength="50">
                     &nbsp;
                       <select class="form-control-new" name="v_Detail_set" id="v_Detail_set" style="border-color: #aeb0b2;width: 90px;">
		            		<option value="Set">Unit</option>
		            	</select> 
		            </span>	
                </td>  
                
                <td class="title_table" width="150" >Kondisi</td>
	            <td colspan="3">
	            	   <select class="form-control-new" name="v_Kondisi" id="v_Kondisi" style="border-color: #aeb0b2;width: 70px;">
		            		<option value="Baik">Baik</option>
		            		<option value="Rusak">Rusak</option>
		            	</select>
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Keterangan</td>
                <td width="500">
                       <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 400px;  text-transform: uppercase;" name="v_Keterangan" id="v_Keterangan" maxlength="50">
                </td> 
                
                <td class="title_table" width="150" >Photo <input type="hidden" name="sts_photo" id="sts_photo" value="0"/></td>
	            <td width="300">
	            	   <input class="form-control" type="file" name="filegambar" id="filegambar" style="width:250px">  
	            </td>   
	            
	            <td>
	            	<button type="button" class="btn btn-info btn-sm" id='tambahgambar'>Upload</button>
	            	   
	            	   <script>
					        $(document).on('click','#tambahgambar',function(e){
					            e.preventDefault();
					            var file_data = $('#filegambar').prop('files')[0];
					            var form_data = new FormData();
					 			
					            form_data.append('file', file_data);
					            $.ajax({
					                url: '<?php echo site_url("fixed_asset/fixed_asset/uploadgambar") ?>', // point to server-side PHP script
					                dataType: 'json',  // what to expect back from the PHP script, if anything
					                cache: false,
					                contentType: false,
					                processData: false,
					                data: form_data,
					                type: 'post',
					                success: function(data,status){
					                    //alert(php_script_response); // display response from the PHP script, if any
					                    if (data.status!='error') {
					                    	//untuk mereset imagesnya
					                        //$('#filegambar').val('');
					                        alert(data.msg);
					                        
					                        //update
					                        $('#sts_photo').val('1');
					                        
					                        //load imagesnya
					                        imagesnya = data.nama_file;
					                        var urls = $("#base_url").val();
					                        $.ajax({
													url: urls+"index.php/fixed_asset/fixed_asset/viewImages/",
													data: {img:imagesnya},
													type: "POST",
													dataType: 'html',					
													success: function(res)
													{
														$("#td_tutupImages").css("display","");
														$("#td_bukaImages").css("display","");
														$('#td_viewImages').html(res);
													},
													error: function(e) 
													{
														alert(e);
													} 
										      });
					                        
					                    }else{
					                        alert(data.msg);
					                    }
					                }
					            });
					        })
					    </script>
	            </td>             
            </tr>
            
            <tr id="td_tutupImages" style="display: none;">
            <td colspan="100%" align="right">
            	 <button style="display: ;" type="button" class="btn btn-danger btn-sm" id='tutup_images' onclick="tutup()">Tutup Gambar</button>
            	 <button style="display: none;" type="button" class="btn btn-success btn-sm" id='buka_lagi_images' onclick="buka()">Buka Gambar</button>
            </td>
            </tr>
            <tr id="td_bukaImages" style="display: none;">
            <td colspan="100%" align="center" id="td_viewImages">
            	 <!-- diambil dari load controller -->
            </td>
            </tr>
            
	    </table>
	    
	    <h4><font color="#848484"><b>Penempatan</b></font></h4>
	    <table class="table table-bordered responsive">                        
	        <tr>
                <td class="title_table" width="200">Lokasi</td>
                <td width="500">
                     
                     <select class="form-control-new" name="v_Lokasi" id="v_Lokasi" style="border-color: #aeb0b2; width: 200px;">
	            		<option value="">-- Pilih Lokasi --</option>
	            		<?php
	            		foreach($lokasi as $val)
	            		{
							?><option value="<?php echo $val["KdLokasi"]; ?>"><?php echo $val["Lokasi"]; ?></option><?php
						}
	            		?>
	            	</select>
	            	 
                </td> 
                
                <td class="title_table" width="150" >Departemen</td>
	            <td colspan="3" id="sync_dept">
	            	<select class="form-control-new" name="v_Departemen" id="v_Departemen" style="border-color: #aeb0b2; width: 200px;">
	            		<option value="">-- Pilih Departemen --</option>
	            		<?php
	            		foreach($departemen as $val)
	            		{
							?><option value="<?php echo $val["KdDepartemen"]; ?>"><?php echo $val["NamaDepartemen"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
                
            </tr>
            
            <tr>
                <td class="title_table" width="200">Yang Bertanggungjawab</td>
                <td colspan="100%">
                       <select class="form-control-new" name="v_PenanggungJawab" id="v_PenanggungJawab" onchange="pic()" style="border-color: #aeb0b2;width: 100px;">
		            		<option value="">-- Pilih Tipe --</option>
		            		<option value="personal">Personal</option>
		            		<option value="umum">Umum</option>
		            	</select>
		            	&nbsp;
              			
                       <select class="form-control-new" name="v_Pegawai" id="v_Pegawai" style="border-color: #aeb0b2;width: 290px;display:none;">
		            		<option value="">-- Pilih Pegawai --</option>
		            		<?php
		            		foreach($employee as $val)
		            		{
								?><option value="<?php echo $val["employee_id"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
							}
		            		?>
		            	</select>
		            	
                </td>               
            </tr>
            
	    </table>
	    
	    <h4><font color="#848484"><b>Akunting</b></font></h4>
	    <table class="table table-bordered responsive"> 
            <tr>
                <td class="title_table" width="200" >Metode Penyusutan</td>
	            <td colspan="100%" id="td_go_pajak">
	            	 <input readonly type="text" class="form-control-new" style="border-color: #aeb0b2; width: 80px; text-align: right; text-transform: uppercase;" name="v_MetodePenyusutan" id="v_MetodePenyusutan">  
                	 <span id="span_loading_gol_pajak1" style="border-color: #aeb0b2;display: none;"><img src="../../../../public/images/ajax-image.gif"/></span>
	            </td>
                
            </tr>
            
	    </table>
	    
	    <div id="otorisasi_akunting" style="display: ;">
	    <ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Accounting <?php echo $modul; ?></strong></li>
		</ol>
	    <!--<h4><font color="#848484"><b>Akunting</b></font></h4>-->
	    <table class="table table-bordered responsive">                        
	        <tr>
                <td class="title_table" width="200">Tanggal Beli</td>
                <td width="500">
                     <input style="border-color: #aeb0b2;" type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_beli" id="v_tgl_beli" size="10" maxlength="10"> 
                </td> 
                
                <td class="title_table" width="150" >Nominal</td>
	            <td colspan="3">
	            	 <input type="text" class="form-control-new" style="border-color: #aeb0b2; width: 200px; text-align: right; text-transform: uppercase;" name="v_Nominal" id="v_Nominal">
	            </td>
                
            </tr>
            
	    </table>
	    </div>
	    
	    <table class="table table-bordered responsive"> 
		           	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add_other">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/fixed_asset/fixed_asset/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    </div>
	    
	    </form> 
        
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

            <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <center>
                            <img src="<?= base_url() ?>public/images/dot_loader.gif" />
                        </center>
                        </div>
                    </div>
                </div>
            </div>
            
<script>

		$(document).ready(function()
		{
			oto = $("#otorisasi").val();
			if(oto=="Y"){
				document.getElementById("otorisasi_akunting").style.display = "";
			}else{
				document.getElementById("otorisasi_akunting").style.display = "none";
			}
				
				
		});
		
		function pic(){
				var pic = $("#v_PenanggungJawab").val();
				if(pic=="personal"){
					$("#v_Pegawai").css("display","");
				}else{
					$("#v_Pegawai").css("display","none");
				}
		}

		function set(){
				var pic = $("#v_Satuan").val();
				if(pic=="Set"){
					$("#v_Detail_setting").css("display","");
				}else{
					$("#v_Detail_setting").css("display","none");
				}
		}

		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='ajax_item_asset')
				{
					v_ItemAsset = document.getElementById("v_ItemAsset").value;
					
					$("#span_loading_CodeItemAsset").css("display","");

					 var url = $("#base_url").val();
				     
						  $.ajax({
								url: url+"index.php/fixed_asset/fixed_asset/getCodeItem/",
								data: {itemasset:v_ItemAsset},
								type: "POST",
								dataType: 'html',					
								success: function(res)
								{
									$("#span_loading_CodeItemAsset").css("display","none");
									$('#td_CodeItemAsset').html(res);
								},
								error: function(e) 
								{
									alert(e);
								} 
					      });
				}
                else if(tipenya=='ajax_type_asset')
                {
					   v_TypeAsset = document.getElementById("v_TypeAsset").value;
					
						$("#span_loading_TypeAsset").css("display","");

						 var url = $("#base_url").val();
					     
							  $.ajax({
									url: url+"index.php/fixed_asset/fixed_asset/getTypeItem/",
									data: {typeasset:v_TypeAsset},
									type: "POST",
									dataType: 'html',					
									success: function(res)
									{
										$("#span_loading_TypeAsset").css("display","none");
										$('#td_SubTypeAsset').html(res);
									},
									error: function(e) 
									{
										alert(e);
									} 
						   });
                }else if(tipenya=='ajax_type_asset_other_asset')
                {
					   v_TypeAsset = document.getElementById("v_TypeAsset").value;
					
						$("#span_loading_TypeAsset").css("display","");

						 var url = $("#base_url").val();
					     
							  $.ajax({
									url: url+"index.php/fixed_asset/fixed_asset/getTypeItemOtherAsset/",
									data: {typeasset:v_TypeAsset},
									type: "POST",
									dataType: 'html',					
									success: function(res)
									{
										$("#span_loading_TypeAsset").css("display","none");
										$('#td_SubTypeAsset').html(res);
									},
									error: function(e) 
									{
										alert(e);
									} 
						   });
                }
                     
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function gol_pajak_other_asset(){
			 v_SubTypeAsset = document.getElementById("v_SubTypeAsset").value;
			 v_KdTypeAsset = document.getElementById("v_TypeAsset").value;
			 
			 //alert(v_KdTypeAsset+"//"+v_SubTypeAsset);
			 $("#span_loading_accept").css("display","none");
			 $("#span_loading_gol_pajak1").css("display","");

						 var url = $("#base_url").val();
					     
							  $.ajax({
									url: url+"index.php/fixed_asset/fixed_asset/getGolPajakOtherAsset/",
									data: {type_asset:v_KdTypeAsset,sub_type_asset:v_SubTypeAsset},
									type: "POST",
									dataType: 'html',					
									success: function(res)
									{
										$("#span_loading_gol_pajak1").css("display","none");
										$("#span_loading_accept").css("display","");
										$('#td_go_pajak').html(res);
									},
									error: function(e) 
									{
										alert(e);
									} 
						   });
		}
		
		function tutup(){
			$("#td_tutupImages").css("display","");
			$("#td_bukaImages").css("display","none");
			$("#tutup_images").css("display","none");
			$("#buka_lagi_images").css("display","");
		}
		
		function buka(){
			$("#td_tutupImages").css("display","");
			$("#td_bukaImages").css("display","");
			$("#tutup_images").css("display","");
			$("#buka_lagi_images").css("display","none");
		}
		
		function tutup_pro(){
			$("#td_tutupImages_pro").css("display","");
			$("#td_bukaImages_pro").css("display","none");
			$("#tutup_images_pro").css("display","none");
			$("#buka_lagi_images_pro").css("display","");
		}
		
		function buka_pro(){
			$("#td_tutupImages_pro").css("display","");
			$("#td_bukaImages_pro").css("display","");
			$("#tutup_images_pro").css("display","");
			$("#buka_lagi_images_pro").css("display","none");
		}
		
		function cekTheform()
		{		
				if($("#v_ItemAsset").val()==""){
					alert("Item Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_CodeAsset").val()==""){
					alert("Code Item Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_TypeAsset").val()==""){
					alert("Type Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_SubTypeAsset").val()==""){
					alert("Type Asset Harus DipIlih...");
					return false;
				}
				
				if($("#v_nama_asset").val()==""){
					alert("Nama Asset Harus Diisi...");
					$('#v_nama_asset').focus();
					return false;
				}
				
				if($("#v_brand").val()==""){
					alert("Brand Harus Diisi...");
					$('#v_brand').focus();
					return false;
				}
				
				if($("#v_Volume").val()==""){
					alert("Volume Harus Diisi...");
					$('#v_Volume').focus();
					return false;
				}
				
				if($("#v_Size").val()==""){
					alert("Size Harus Diisi...");
					$('#v_Size').focus();
					return false;
				}
				
				if($("#v_Qty").val()==""){
					alert("Qty Harus Diisi...");
					$('#v_Qty').focus();
					return false;
				}
				
				if($("#v_Satuan").val()==""){
					alert("Satuan Harus Dipilih...");
					return false;
				}
				
				if($("#v_Kondisi").val()==""){
					alert("Kondisi Harus Dipilih...");
					return false;
				}
				
				if($("#v_Keterangan").val()==""){
					alert("Keterangan Harus Diisi...");
					$('#v_Keterangan').focus();
					return false;
				}
				
				/*if($("#v_Nominal").val()==""){
					alert("Nominal Harus Diisi...");
					$('#v_Nominal').focus();
					return false;
				}*/
				
				if($("#v_Proposal").val()==""){
					alert("Nominal Harus Diisi...");
					$('#v_Proposal').focus();
					return false;
				}
				
				if($("#v_Lokasi").val()==""){
					alert("Lokasi Harus Dipilih...");
					return false;
				}
				
				if($("#v_Departemen").val()==""){
					alert("Departemen Harus Dipilih...");
					return false;
				}
				
				if($("#v_Pegawai").val()=="" && $("#v_PenanggungJawab").val()=="personal"){
					alert("Pegawai Harus Dipilih...");
					return false;
				}
				
				if($("#sts_photo").val()=="0"){
					
					$(document).on('click','#btn_save',function(e){
					            e.preventDefault();
					            var file_data = $('#filegambar').prop('files')[0];
					            var form_data = new FormData();
					 			
					            form_data.append('file', file_data);
					            $.ajax({
					                url: '<?php echo site_url("fixed_asset/fixed_asset/uploadgambar") ?>', // point to server-side PHP script
					                dataType: 'json',  // what to expect back from the PHP script, if anything
					                cache: false,
					                contentType: false,
					                processData: false,
					                data: form_data,
					                type: 'post',
					                success: function(data,status){
					                    //alert(php_script_response); // display response from the PHP script, if any
					                    if (data.status!='error') {
					                    	
					                        //alert(data.msg);
					                        $('#sts_photo').val('1');
					                        document.getElementById("theform").submit();
					                        
					                    }else{
					                        alert(data.msg);
					                    }
					                }
					            });
					        })
					
					return false;
				}
											
			    var yesSubmit = true;
		    	
		        if(yesSubmit)
		        {
					document.getElementById("theform").submit();	
				}  
			
		}
		
		function pilih_departemen(){
			 dept_pro = document.getElementById("v_Departemen_proposal").value;
             var url = $("#base_url").val();
					     
							  $.ajax({
									url: url+"index.php/fixed_asset/fixed_asset/sychDepart/",
									data: {dept:dept_pro},
									type: "POST",
									dataType: 'html',					
									success: function(res)
									{
										$('#sync_dept').html(res);
									},
									error: function(e) 
									{
										alert(e);
									} 
						   });
		}
		
</script>