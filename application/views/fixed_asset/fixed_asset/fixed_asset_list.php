<?php 
$this->load->view('header'); 
$page = $this->uri->segment(4);
?>

<head>
</head>


   <form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/fixed_asset/fixed_asset/search'>
    <div class="row">
        <div class="col-md-8">
             <b>Search</b>&nbsp;
            <input type="text" style="border-color: #aeb0b2;" size="50" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?=$cari;?>" />
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-danger btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search Asset<i class="entypo-search"></i></button>
        	<a href="<?php echo base_url() . "index.php/fixed_asset/fixed_asset/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Add Specific Asset<i class="entypo-plus"></i></a>
            <a href="<?php echo base_url() . "index.php/fixed_asset/fixed_asset/add_new_other/"; ?>" class="btn btn-success btn-icon btn-sm icon-left" title="" >Add Other Asset<i class="entypo-plus"></i></a>
        </div>
    </div>
	</form>
<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><a href="<?php echo base_url() . "index.php/fixed_asset/fixed_asset/preview/".$msg['nofaktur']; ?>" ><h5><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div></h5></a><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="150"><center>No.Asset</center></th>
		        <th ><center>Nama Asset</center></th>
		        <th width="150"><center>Departemen</center></th>
		        <th width="150"><center>Lokasi</center></th>
		        <th width="150"><center>Penanggungjawab</center></th>
		        <th><center>Keterangan</center></th>
		        <th width="50"><center>Accouting</center></th>
		        <th width="50"><center>Cetak</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
				
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }
                
                
                
                ?>
                <tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk detail <?php echo $val["No_Asset"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="center"><?php echo $val["NoAsset"]; ?></td>
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="center"><?php echo $val["nama_asset"]; ?></td>
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="left"><?php echo $val["NamaDepartemen"]; ?></td>
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="left"><?php echo $val["Lokasi"]; ?></td>
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="left"><?php echo $val["employee_name"]; ?></td>
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="left"><?php echo $val["Keterangan"]; ?></td> 
                    <td onclick="editing('<?php echo $val["No_Asset"]; ?>','<?php echo $val["Other_Asset"]; ?>')" align="left"><?php echo $val["Sts_accounting"]; ?></td>
                    <td align="center">
                    	<?php if($val["Sts_accounting"]=="Y"){?>
                    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrint('<?= $val["No_Asset"]; ?>','<?= $val["id_lokasi"]; ?>','<?= $val["Departemen"]; ?>', '<?= base_url(); ?>');">
			                <i class="entypo-export"></i>
			            </button>
                    	<?php } ?>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<?php $this->load->view('footer'); ?>
<script>

function editing(noasset,other){
var url=$('#base_url').val();
if(other=="0"){
	window.location = url+"index.php/fixed_asset/fixed_asset/edit_asset/"+noasset;
}else{
	window.location = url+"index.php/fixed_asset/fixed_asset/edit_other_asset/"+noasset;
}
	
}


function PopUpPrint(kode,lokasi, dept,  baseurl)
    {
        url = "index.php/barcode_fixed_assets/cetak_barcode_kiriman/" + escape(kode)+"/"+lokasi+"/"+dept;
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=800,height=500,top=100,left=350');
    }


</script>