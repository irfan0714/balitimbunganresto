<!-- Modal REvisi -->
<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Sistem Revisi Otomatis</h4>
        </div>
        <div class="modal-body">
          <form role="form" method="POST" id="requestform" name="requestform" action="<?=base_url();?>index.php/panicbutton/SaveRevisi">
            <div class="box-body">

              <input type="hidden" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen">
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Jenis Revisi</label>
                <select class="form-control" name="jrevisi" id="jrevisi" onchange="GetRevisi();">
                  <option value="">Pilih Jenis Request</option>
                  <option value="MS">Pending MS</option>
                  <option value="SO">Pending SO</option>
                  <option value="PL">Pending PL</option>
                  <option value="DL">Pending DL</option>
                  <option value="OR">Lainnya</option>

                </select>



              </div>
              <div class="form-group" id="divdokumen">
                <label for="exampleInputEmail1">Pilih Dokumen Revisi</label>
                <select class="form-control" tabindex="2" name="dokumen" id="dokumen">

                </select>

              </div>
              <div class="form-group hide" id="divnodok">
                <label for="exampleInputEmail1">No Dokumen(Isikan 0 jika tidak ada)</label>
                <input type="text" name="nodok" class="form-control">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Pilih Approval</label>
                <select class="form-control" id="approval" name="approval">


                </select>

              </div>
              <div class="form-group">
                <label for="exampleInputFile">Alasan Revisi</label>
                <textarea class="form-control" name="alasan"></textarea>
              </div>
              <div class="form-group hide" id="revgambar">
                <label for="exampleInputFile">Gambar (Optional)</label>
                <input type="file" name="filegambar" id="filegambar" style="width:100%">
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="cekbox"> Memang benar saya yang ingin meminta revisi.
                </label>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <center><input type="submit" class="btn btn-primary" id="btnSave" value="Request"></center>
            </div>
          </form>
          <br>
          <br>
          <div id="messages"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- Modal Revisi -->

<!-- Modal Status -->
<div class="modal fade" id="modal-status" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">DATA REQUEST YANG MASUK</h4>
        </div>
        <div class="modal-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
              <tr>
                  <th>No Revisi</th>
                  <th>No Dokumen</th>
                  <th>Tanggal</th>
                  <th>Status</th>
                  <th>Action</th>

              </tr>
          </thead>
          <tbody id="isistatusnya">

          </tbody>

          </table>
          <div id="messages"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- Modal Status -->

<!-- Modal History -->
<div class="modal fade" id="modal-history" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Riwayat Laporan Revisi Anda</h4>
        </div>
        <div class="modal-body">


          <!--  TAB-->
          <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_11" data-toggle="tab" aria-expanded="true">Normal Request</a></li>
            <li class=""><a href="#tab_22" data-toggle="tab" aria-expanded="false">Problem Request</a></li>



          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_11">
              <table id="thistory" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No Revisi</th>
                      <th>Type</th>
                      <th>No Dokumen</th>
                      <th>Tanggal</th>
                      <th>Status</th>
                      <th>Respon By</th>

                  </tr>
              </thead>
              <tbody id="isihistory">

              </tbody>

              </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_22">
              <table id="thistory2" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No Revisi</th>
                      <th>Type</th>
                      <th>No Dokumen</th>
                      <th>Tanggal</th>
                      <th>Status</th>

                  </tr>
              </thead>
              <tbody id="isihistory2">

              </tbody>

              </table>
            </div>
            <!-- /.tab-pane -->

            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
          <!-- TAB -->



          <div id="messages"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- Modal History -->

<!-- Modal History -->
<div class="modal fade" id="modal-list" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Detail Riwayat Laporan Revisi</h4>
        </div>
        <div class="modal-body">

          <!--  TAB-->
          <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_111" data-toggle="tab" aria-expanded="true">Normal Request</a></li>
            <li class=""><a href="#tab_222" data-toggle="tab" aria-expanded="false">Problem Request</a></li>



          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_111">
              <table id="tall" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No Revisi</th>
                      <th>No Dokumen</th>
                      <th>Tanggal</th>
                      <th>Status</th>
                      <th>Respon by</th>


                  </tr>
              </thead>
              <tbody id="isiall">

              </tbody>

              </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_222">
              <table id="tall2" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No Revisi</th>
                      <th>No Dokumen</th>
                      <th>Tanggal</th>
                      <th>Status</th>
                      <th>Action</th>

                  </tr>
              </thead>
              <tbody id="isiall2">

              </tbody>

              </table>
            </div>
            <!-- /.tab-pane -->

            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
          <!-- TAB -->


          <div id="messages"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- Modal History -->
