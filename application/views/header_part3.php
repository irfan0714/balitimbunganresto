
<body class="page-body skin-black" >

<div class="page-container sidebar-collapsed">

    <div class="sidebar-menu">


        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="index.html">
                    <img src="<?= base_url();?>public/images/Logosg.png" width="120" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->

            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>



            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>



        <div class="sidebar-user-info">

            <div class="sui-normal">
                <a href="#" class="user-link">
                    <?php $mylib = new globallib(); ?>
                    <span><?=$mylib->ubah_tanggal($this->session->userdata('Tanggal_Trans'));?></span>
                    <strong><?=$this->session->userdata('username');?></strong>
                </a>
            </div>
        </div>

        <?= $this->load->view('slide_kiri');?>

    </div>

    <div class="main-content">
    
    	<ol class="breadcrumb bc-3">
    		<li>
				<a href="<?php echo base_url(); ?>start">
					<i class="entypo-home"></i>Home
				</a>
			</li>
			
			<?php if(!empty($track)){ echo $track; } ?>
			
		</ol>
		
		<hr/>
        