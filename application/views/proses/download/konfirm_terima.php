<?php
$this->load->view('header');
$this->load->view('space');
$gantikursor = "onkeydown=\"changeCursor(event,'lainlain',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<!--<script language="javascript" src="<?=base_url();?>public/js/lainlain.js"></script>-->
<!--<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>-->
<!--<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />-->
<style type="text/css">
    .InputAlignRight
{
	text-align: right;
}
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<SCRIPT language="javascript">
    function Inputhrg(obj)
    {
        objek = obj.id;
	id = parseFloat(objek.substr(8,objek.length-8));

        qty = $("#qtyinput"+id).val();
	hrg = $("#hrg"+id).val();
        ttl = qty * hrg;
        nm = number_formatjs(ttl, 2, ',', '.');
        $("#netto"+id).val(nm);
    }
    function goBack()
    {
        window.history.back()
    }

    function saveAll()
    {
        $("#lainlain").submit();
    }
</SCRIPT>
<body onload="firstLoad('lainlain');">
<form method='post' name="lainlain" id="lainlain" action='<?=base_url();?>index.php/proses/download_terima/save_konfirm' onsubmit="return false">
    <table align = 'center' >
<!--   =============== form header ==========================================================   -->
        <tr>
            <td>
		<fieldset class="disableMe">
                    <legend class="legendStyle">Konfirm Penerimaan Omah</legend>
                    <table class="table_class_list">
                    	<?php
			$mylib = new globallib();
                        echo $mylib->textaja("No Dokumen",$header->NoDokumen);
                        echo $mylib->textaja("Tanggal Dokumen",$mylib->ubah_format_tanggal($header->TglDokumen));
                        echo $mylib->textaja("No Surat Jalan",$header->NoSuratJalan);
                        echo $mylib->textaja("Keterangan",$header->Keterangan);
			?>
			</table>
			</fieldset>
			</td>
		</tr>
<!--                ======================== End header ======================-->

<!--                +++++++++++++++++++++++= Form Detail =++++++++++++++++++++-->
		<tr>
                    <td>
                        <fieldset class = "disableMe">
                            <legend class="legendStyle">Detail</legend>
                            <div id="Layer1" style="display:none">
                                <p align="center">
                                    <img src='<?=base_url();?>public/images/ajax-loader.gif'>
                                </p>
                            </div>


                            <table class="table_class_list" id="detail">
                                <thead>
                                    <tr id="baris0">
                                        <td>KdBarang</td>
                                        <td>NamaBarang</td>
                                        <td>Qty</td>
                                        <td>Qty Terima</td>
                                        <td>Nilai</td>
                                        <td>Total Nilai</td>
                                    </tr>
                                </thead>
                                 <tbody>
                                <?php
                                    $brs = 1;
                                    for($a=0;$a<count($detail);$a++){
                                     $pcode = $detail[$a]['PCode'];
					$nama = $detail[$a]['NamaLengkap'];
					$qty = $detail[$a]['Qty'];
                                    ?>
                                    <tr id="tr<?=$brs?>" >
                                        <td nowrap><?=$detail[$a]['PCode']?></td>
                                        <td nowrap><?=$detail[$a]['NamaLengkap']?></td>
                                        <td nowrap align="right"><?=$detail[$a]['Qty']?></td>
                                        <td nowrap><input type="text" id="qtyinput<?=$brs?>" name="qtyinput[]" style="text-align:right;" size="8" onkeyup="Inputhrg(this)" value="<?=$detail[$a]['Qty']?>"></td>
                                        <td nowrap align="right">
                                            <?=number_format($detail[$a]['HKecil'], 2, ",",".")?>
                                            
                                        </td>
                                        <td nowrap>
                                            <input type="text" id="netto<?=$brs?>" name="netto[]" size="8" readonly="readonly" style="text-align:right;"
                                            value="<?php echo number_format(($detail[$a]['HKecil'])*($detail[$a]['Qty']),2,",",".")?>">
                                        </td>


<!--                                        ===================== file hidden ===============================-->

    <input type="hidden" id="pcode<?=$brs?>" name="pcode[]" value="<?=$pcode?>">
    <input type="hidden" id="hrg<?=$brs?>" name="hrg[]" value="<?=$detail[$a]['HKecil']?>">

        </td>
                                    </tr>
                                       <?php
                                        $brs++;
                                       }?>
                                </tbody>   
                            </table>
                        </fieldset>
                    </td>
                </tr>
		<tr>
                    <td nowrap>
                        <input type='hidden' id="totalbaris" name="totalbaris" value="1">
                        <input type='hidden' id="transaksi" name="transaksi" value="no">
                        <input type='hidden' id="noterima" name="noterima" value="<?=$header->NoDokumen?>">
                        <input type='hidden' id="flag" name="flag" value="edit">
                        <input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
                        <input type='button' value='Save' onclick="saveAll()"/>
<!--                        <input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/lainlain/" />-->
                        <input type="button" value="Back" onclick="goBack()" />
                    </td>
		</tr>
	</table>
</form>