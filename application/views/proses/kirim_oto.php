<?php
$this->load->view('header');
$this->load->view('space');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";

?>
<script language="javascript">
    
    function doThis()
    {
	x=confirm("Anda akan mengirim backup data otomatis ? \r\n pastikan terkoneksi ke internet?");
	if(x){
		$("#Layer1").attr("style","display:");
		$("#backup").submit();
	}
    }
</script>
<style type="text/css">

</style>

<body>
<div id="Layer1" style="display:none">
  <p align="center">
  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
</p>
</div>
<form method="post" name="backup" id="backup" action="<?=base_url()?>index.php/proses/kirim_otomatis/doThis">
<table align = 'center' width="50%">
	<tr>
		<td align="center">
		<fieldset class="disableMe">
		<legend class="legendStyle">Kirim Backup Otomatis</legend>
		<table style="font-family: georgia, 'times new roman', serif:border:0;font-size:13px;font-weight : bold;">
		<tr>
			<td nowrap colspan="2" id="btnmsg" align="center">
			<?php
			if($msg==""){ 
			$mylib = new globallib();
		?>
                        Pilih Tanggal 
                        <input class="form-control-new datepicker" type="text" id="tglawal" name="tglawal" size="10" value="<?php echo date('d-m-Y'); ?>"> s/d
                        <input class="form-control-new datepicker" type="text" id="tglakhir" name="tglakhir" size="10" value="<?php echo date('d-m-Y'); ?>"><p>
			            <br><input class="form-control-new" type="button" id="btn" value="Kirim" onclick="doThis();">
		<?php
			}
			else
			{
			?>
				<span id="message"><?=$msg?></span>
			<?php
			}
			?>
			</td>
			<td>
			<input type="hidden" value="<?=base_url()?>" id="baseurl" name="baseurl">
			</td>
		</tr>
		</fieldset>
		</td>
	</tr>
</table>
</form>
</body>
<?php
$this->load->view('footer'); ?>
<script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.css"/>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>