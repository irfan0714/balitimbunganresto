<?php
$this->load->view('header'); 
?>
<script>
function Konfirmasi()
{
    tgl = $("#tanggaltrans").val();
    thnbln = tgl.substr(0,4)+tgl.substr(5,2);
	blnthn = tgl.substr(5,2)+"-"+tgl.substr(0,4);
    if(document.getElementById("tahun").value=="")
	{
		alert("Pilih tahun dahulu");
	}else
	{
		if(document.getElementById("bulan").value=="")
		{
			 alert("Pilih bulan dahulu");
		}
		else
		{
		    bulan = $("#bulan").val();
		    tahun = $("#tahun").val();
		    thnbln2 = tahun+bulan;
		    if(thnbln<thnbln2)
			{
			   alert("Bulan dan tahun aktif tidak boleh lebih besar dari "+blnthn);
			}else
			{	
				ss = confirm("Ubah bulan dan tahun aktif menjadi "+bulan+"-"+tahun+"?");
				if(ss)
				{
					document.getElementById("bulanaktif").submit();
				}
			}
		}
	}

}
</script>
<body>
<br>
<form method='post' name="bulanaktif" id="bulanaktif" action='<?=base_url();?>index.php/proses/bulanaktif/save'>
<table cellspacing="3" cellpadding="3" align = 'center' border='0'>
<tr>
	<td nowrap>Tahun</td>
	<td nowrap>:</td>
	<td nowrap>
	<select size="1" id="tahun" name="tahun">
	<option value="">--Select--</option>
	<?php
	for($a = 0;$a<count($tahun);$a++){
		$select = "";
		if($tahunaktif==$tahun[$a]['Tahun']){
			$select = "selected";
		}
	?>
	<option <?=$select;?> value= "<?=$tahun[$a]['Tahun']?>"><?=$tahun[$a]['Tahun']?></option>
	<?php
	}
	?>
	</select>
</tr>

<tr>
    <td nowrap>Bulan</td>
	<td nowrap>:</td>
	<td nowrap>
	<select size="1" id="bulan" name="bulan">
	<option value="">--Select--</option>
	<?php
	for($a = 0;$a<count($bulan);$a++){
		$select = "";
		if($bulanaktif==$bulan[$a]){
			$select = "selected";
		}
	?>
	<option <?=$select;?> value= "<?=$bulan[$a]?>"><?=$bulan[$a]?></option>
	<?php
	}
	?>
	</select>
	</td>
</tr>
</table>
<br>
<table align="center">
	<tr align="center">
	<td><input type="hidden" name="tanggaltrans" id="tanggaltrans" value=<?=$tanggaltrans?>></td>
	<td><input type="button" value="Ganti" onclick="Konfirmasi()"></td>
	<td>&nbsp;</td>
	</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>