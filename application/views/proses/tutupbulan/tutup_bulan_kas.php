<?php 
$this->load->view('header');
?>

<div class="row">
    <div class="col-md-12" align="left">
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Tutup Bulan Kas/Bank</strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action="<?= base_url(); ?>index.php/proses/tutup_bulan_kas/simpan/">
		
	    <table class="table table-bordered responsive">   
	        <tr>
	            <td class="title_table" width="150">Periode Sekarang</td>
	            <td>
	            	<input type="text" size="10" value='<?=$periodesekarang;?>' readonly="readonly" id="periodesekarang" name="periodesekarang" /></td>
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Periode Berikut</td>
	            <td>
	            	<input type="text" size="10" value='<?=$periodeberikut;?>' readonly="readonly" id="periodeberikut" name="periodeberikut" /></td>
	            </td>
	        </tr>
	        <tr>
            	<td>&nbsp;</td>
            	<td>
                	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_proses" id="btn_proses" value="Submit" ">
            	</td>
        	</tr>
	    </table>
	</form>	     
	</div>
</div>
<?php $this->load->view('footer'); ?>
