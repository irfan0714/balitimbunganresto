<?php 
$this->load->view('header');
?>
<script>
	function submit_this(ptype){
		
		$('#pleaseWaitDialog').modal('show');
		tahun = $("#tahun").val();
		bulan = $("#bulan").val();	
		if(typeof(EventSource) !== "undefined") {
			sc = "<?= base_url(); ?>" + "index.php/proses/hitungstock/proses/" + tahun + "/" + bulan;
		    var source = new EventSource(sc);
		    source.onmessage = function(event) {
		    	if(event.data == 'Selesai'){
		        	this.close();
				}
				document.getElementById("result").innerHTML = event.data + "<br>";	
	    	};
		} else {
	    	document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
		}
		
		
	}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Hitung Stock</strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform">
		
		    <table class="table table-bordered responsive">   
		    	<tr>
		            <td class="title_table" width="150">Tahun</td>
		            <td>
		            	<input type="text" size="10" value='<?=$tahun?>' id="tahun" name="tahun" /></td>
		            </td>
		        </tr>
		        <tr>
		            <td class="title_table" width="150">Bulan</td>
		            <td>
		            	<select class="form-control-new" name="bulan" id="bulan" >
		            		<option value="">- Pilih Bulan -</option>
		            		<option value="1">Januari</option>
		            		<option value="2">Februari</option>
		            		<option value="3">Maret</option>
		            		<option value="4">April</option>
		            		<option value="5">Mei</option>
		            		<option value="6">Juni</option>
		            		<option value="7">July</option>
		            		<option value="8">Agustus</option>
		            		<option value="9">September</option>
		            		<option value="10">Oktober</option>
		            		<option value="11">November</option>
		            		<option value="12">Desember</option>
		            	</select>
		            </td>
		        </tr>
		        <tr>
	            	<td>&nbsp;</td>
	            	<td>
	            		<button class="btn btn-primary" type="button" onclick="submit_this();" id="submit">	<i class="entypo-drive"></i>Proses</button>
	                	
	            	</td>
	        	</tr>
		    </table>
		</form>
		
		<div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <h1>Mohon Tunggu, Sedang Proses...</h1>
	                </div>
	                <div class="modal-body">
	                    <div class="progress progress-striped active">
	                        <div class="progress-bar" style="width: 100%;"></div>
	                    </div>
	                    <div id="result"></div>
	                </div>
	            </div>
	        </div>
    	</div>	     
    	
	</div>
</div>
<?php $this->load->view('footer'); ?>
