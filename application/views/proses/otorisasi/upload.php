<html>
<head>
<title>Upload</title>
</head>

<body>
<?php
		$this->load->view('auth/header.php');
		$this->load->view('auth/menu.php');
		$this->load->view('js/datepicker');
?>
<script src="<?php echo base_url();?>/appinclude/tool.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>appinclude/jquery.alerts/jquery.alerts.js"></script>
<link href="<?php echo base_url();?>appinclude/jquery.alerts/jquery.alerts.css" rel="stylesheet" type="text/css" />

<script language = "javascript">
		$(document).ready(function(){
		
		   $("#btn-1").click(function() {
				var par= $(this).val();
				kirim(par);
		   });
	
		   
		   function kirim(par){
				var checked = $("input[name='ck']:checked").length;
				if (checked == 0){
					if(par=="Otorisasi"){
						jAlert('Anda Harus Memilih No Faktur Yang Akan Di Otorisasi', 'Warning');
					}
					
					return false;
				}else{
						var pilihan	= [];
						var pil		= [];
						var KdCabang	= $('#KdCabang').val();		
						var TglTransaksi= $('#TglTransaksi').val();						
						$.each($("input[name='ck']:checked"), function(i) {
									
									var brs	    	= $(this).val(brs);
									var kete		= $('#Ket'+brs).val();
									var bntu		= $('#bntu'+brs).val();
									var hlp			= $('#hlp'+brs).val();
									var tnd			= $('#KdTanda'+brs).val();

									pilihan[i]  	= bntu+'~'+tnd+'~'+kete;	
									pil[i]			= hlp+'<br>';								
									});
									
						
							jConfirm('Apakah Anda Akan Melakukan Proses Otorisasi' + ': <br> '+pil+' ', 'Konfirmasi', function(r) {
								if(r==true){
									$('#form').submit(location.href = '<?=base_url().'index.php/utility/otorisasi_offline/langsung/'?>'+pilihan+'/'+checked+'/'+'Y'+'/'+'F'+'/'+KdCabang+'/'+TglTransaksi);
								}
							});
															
					
				}
					   
		   }		   	   
			
		});

</script>
<?=$msg?>
<form enctype="multipart/form-data" id = "myform" method="post" action="<?=base_url()?>index.php/utility/otorisasi_offline/<?=$act?>/">
<?php 
	if($act=="do_upload"){
?>
<input type="file" name="userfile" id="userfile" size="20" />
<br /><br />
<input type="submit" value="Upload" />

</form>
<?php 
	}else{
		if(!empty($list)){
?>
<input type="hidden" name="KdCabang" id="KdCabang" value="<?=$KdCabang?>" />
<input type="hidden" name="TglTransaksi" id="TglTransaksi" value="<?=$TglTransaksi?>" />
		<fieldset  style=" background-color:#E1DFD2;"><legend  style=" background-color:transparent;">DAFTAR PENGAJUAN OTORISASI FAKTUR OFFLINE</legend>
		<div> <input type="button" id="btn-1" name="btn-1" value="Proses"></div>
			<table border=0  width="80%"  class="ewtable" align="left">
				<thead>
				<tr>
					<td align="left" class="ewtableheader" nowrap><b><font face="Arial" size="2">&nbsp;&nbsp;</font></b></td>
					<td align="left" class="ewtableheader" nowrap><b><font face="Arial" size="2">&nbsp;&nbsp;</font></b></td>
					<td align="left" class="ewtableheader" nowrap><b><font face="Arial" size="2">&nbsp;History&nbsp;</font></b></td>			
					<td class="ewtableheader" nowrap>Otorisasi</td>
					<td class="ewtableheader" nowrap>Jenis</td>
					<td class="ewtableheader" nowrap>Cabang</td>	
					
					<td class="ewtableheader" nowrap>No Transaksi</td>		
					<td class="ewtableheader" nowrap>Tanggal</td>
					<td class="ewtableheader" nowrap>Kode Outlet</td>
					<td class="ewtableheader" nowrap>Kode Sales</td>
					
					<td class="ewtableheader" nowrap>Netto</td>		
					<td class="ewtableheader" nowrap>Total Piutang</td>	
					<td class="ewtableheader" nowrap>Limit Kredit</td>
					<td class="ewtableheader" nowrap>Gudang</td>
					<td class="ewtableheader" nowrap>Tipe Otorisasi</td>
					<td align="left" class="ewtableheader" nowrap><b><font face="Arial" size="2">&nbsp;Keterangan &nbsp;</font></b></td>				
				</tr>
				</thead>
				<tbody>
					<?php 
						$no=1;
						for($i=0;$i<count($list);$i++){
							$Jenis	= $list[$i]['Jenis'];
							if($Jenis=="F"){
								$jns="Faktur";
							}else{
								$jns="Retur";
							}
							$from	= $list[$i]['KdDepo'];
							if($from=="00"){
								$dari	= "Cabang";
							}else{
								$dari	= "Depo";
							}
							$tipe	= $list[$i]['TipeOtorisasi'];
							if($tipe=="L"){
								$tpe	= "Over Limit";
							}elseif($tipe=="P"){
								$tpe	= "Over Due";
							}else{
								$tpe	= "";
							}	
						
					?>						
				<tr>
				<td align="right"  class="" nowrap><b><font face="Arial" size="2">&nbsp;<input type="hidden" size="1" name="bntu" id="bntu<?=$i?>" value="<?=$list[$i]['KdCabang']."~".$list[$i]['Jenis']."~".$list[$i]['NoTransaksi']."~".$list[$i]['KdOutlet']."~".$list[$i]['TglTrans']."~".$list[$i]['Netto']."~".$list[$i]['LimitK']."~".$list[$i]['NilaiPiutang']."~".$list[$i]['TotalPiutang']."~".$list[$i]['KdDepo']."~".$list[$i]['KdSales']."~".$list[$i]['TipeOtorisasi']?>"><input type='checkbox' name='ck' id='ck<?=$i?>' value='<?=$i?>'>&nbsp;</font></b></td>
				<td align="center" class="" nowrap><b><font face="Arial" size="2">&nbsp;<img src="<?=base_url();?>/appinclude/images/dtl.png" width="16" height="16" border="0" onClick="PopupWindow('<?php echo base_url();?>index.php/utility/otorisasi_offline/ViewDetail/<?=$list[$i]['KdCabang']?>/<?=$list[$i]['Jenis']?>/<?=$list[$i]['NoTransaksi']?>',600,600);" title = 'Detail'>&nbsp;</font></b></td>
				<td align="center" class="" nowrap><b><font face="Arial" size="2">&nbsp;<input type="hidden" size="1" name="hlp" id="hlp<?=$i?>" value="<?=$list[$i]['KdCabang']."~".$list[$i]['Jenis']."~".$list[$i]['NoTransaksi']."~".$list[$i]['Netto']?>"><img src="<?=base_url();?>/appinclude/images/applet3-428.png" width="16" height="16" border="0" onClick="PopupWindow('<?php echo base_url();?>index.php/utility/otorisasi/ViewPopup/<?=$list[$i]['KdCabang']?>/<?=$list[$i]['KdOutlet']?>',600,600);" title = 'History Outlet'>&nbsp;</font></b></td>	
				<td align="left" class="" nowrap><?=form_dropdownDB_init2("KdTanda",$approve,"KdTanda","NmTanda","","","Pilih","id=\"KdTanda".$i."\" ");?></td>
					<td><?=$jns?></td>
					<td><?=getNamaCabang($KdCabang)?></td>
					
					<td><?=$list[$i]['NoTransaksi']?></td>
					<td><?=$list[$i]['TglTrans']?></td>
					<td><?=$list[$i]['KdOutlet']?></td>		
					<td><?=$list[$i]['KdSales']?></td>
					
					<td><?=ubah_format($list[$i]['Netto'])?></td>
					<td><?=ubah_format($list[$i]['TotalPiutang'])?></td>
					<td><?=ubah_format($list[$i]['LimitK'])?></td>		
					
					<td><?=$dari?></td>
					<td><?=$tpe?></td>
					<td align="left"   class="" nowrap><b><font face="Arial" size="2">&nbsp;<input type="text" size="50" name="Ket<?=$i?>" id="Ket<?=$i?>" value="">&nbsp;</font></b></td>
				</tr>
				<?php $no++;
					}
				?>				
				</tbody>
			</table>		
		</fieldset>
	<?php 
		}
	?>
</form>
<?php 
	}
?>





</body>
</html>