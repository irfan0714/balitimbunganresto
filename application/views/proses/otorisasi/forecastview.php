<html>
<head>
<?php
	$this->load->view('js/datepicker');
?>
<script src="<?php echo base_url();?>/appinclude/tool.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>appinclude/css/paging5.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php
	$this->load->view('auth/header');
	$this->load->view('auth/menu.php');
?>

<form id="form" method="post" action="<?=base_url()?>index.php/utility/forecast/view">
   	<fieldset style=" background-color:#E1DFD2; " ><legend style="background-color:transparent;">FORECAST ORDER</legend>
	<table border=1 cellpadding="3" cellspacing="3" width="100%" class="" >


	<tr><td colspan="2">
		<table >
	    <tr class="">
			<td nowrap><b><font face="Arial" size="2">&nbsp;Bulan:</font></b></td>
	      	<td>
			<select name = "bulan" id="bulan" size="1">
			<?php
				for($count = 1;$count<13;$count++)
				{
					$selected = "";
					if(strlen($count)==1){ $count = "0".$count; }
					if($count==$bulanaktif) { $selected = "selected"; }
			?>
				<option <?=$selected;?> value="<?=$count;?>"><?=$count;?></option>
			<?php
				}
			?>
			</select>
			</td>
			<td nowrap><b><font face="Arial" size="2">&nbsp;Tahun:</font></b></td>
	      	
			<td>
			<select name = "tahun" id="tahun" size="1">
			<?php
				for($count = 0;$count<count($rowYear);$count++)
				{
					$selected = "";
					if($rowYear[$count]['Tahun'] == $tahun) { $selected = "selected"; }
			?>
				<option <?=$selected;?> value="<?=$rowYear[$count]['Tahun'];?>"><?=$rowYear[$count]['Tahun'];?></option>
			<?php
				}
			?>
			</select>
			</td>
			
			<td>&nbsp;</td>
			<td ><input type="submit" name="Print" value="Print" class='button' ></td>		
	       </tr>
		</table>
    </td></tr>
	</table>
	</fieldset>
</form>
<?php
if(!empty($list)){

$this->load->view('utility/forecast/forecastreport');
}
$this->load->view('auth/footer'); ?>
</body>
</html>