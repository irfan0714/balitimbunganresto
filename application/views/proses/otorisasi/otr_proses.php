<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">
<script language="javascript">
    function submit_form()
    {
        var opt = document.getElementsByName('rbtotorisasi');
        var flag = 0;
        for (var i = 0; i < opt.length; i++)
        {
            if (opt[i].checked) {
                var checked_value = opt[i].value;
                if (checked_value == 'R') {
                    if (document.getElementById('ketreject').value == '') {
                        alert('Masukan Alasan ditolak');
                        document.getElementById('ketreject').focus();
                    } else {
                        alert('Otorisasi Berhasil di Reject');
                        document.getElementById('formotorisasi').submit();
                        window.close();
                        window.opener.location.reload();
                        
                    }
                } else {
                    alert('Otorisasi Berhasil');
                    document.getElementById('formotorisasi').submit();
                    window.close();
                    window.opener.location.reload();
                }
                break;
            }
        }
        // 
    }

    function PopUpPrint(kode, baseurl)
    {
        url = "transaksi/order_barang/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }

    function showKetReject() {
        var opt = document.getElementsByName('rbtotorisasi');
        var flag = 0;
        for (var i = 0; i < opt.length; i++)
        {
            if (opt[i].checked) {
                var checked_value = opt[i].value;
                if (checked_value == 'R') {
                    document.getElementById('grpketreject').style.display = "";
                    document.getElementById('ketreject').focus();
                } else {
                    document.getElementById('grpketreject').style.display = "none";
                }
                break;
            }
        }

    }
</script>

<div class="content">
    <div class="container-fluid">
        <br> </br>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">
                            <i class="fa fa-lock"></i>
                            Proses Otorisasi
                        </h1>
                    </div>
                    <div class="panel-body">
                        <form method="POST" name="formotorisasi" id="formotorisasi" action="<?= base_url(); ?>index.php/proses/otorisasi/save_otorisasi" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="lblnodok"><b> No Dokumen</b></label>
                                <div class="col-sm-5">
                                    <input name="nodok" class="col-sm-3 form-control" type="text" id="nodok" value="<?= $header->NoDokumen; ?>" readonly="readonly" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="tglsub"><b> Tanggal</b></label>
                                <div class="col-sm-5">
                                    <input class="col-sm-3 form-control" id="tgl" value="<?= $header->Tanggal; ?>" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="gdgsub"><b> Gudang</b></label>
                                <div class="col-sm-5">
                                    <input class="col-sm-3 form-control" id="gudang" value="<?= $header->NamaGudang; ?>" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="suppsub"><b> Supplier</b></label>
                                <div class="col-sm-5">
                                    <input class="col-sm-3 form-control" id="supp" value="<?= $header->Nama; ?>" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="jml"><b> Jumlah</b></label>
                                <div class="col-sm-5">
                                    <input class="col-sm-3 form-control" id="jumlah" value="<?= number_format($header->Jumlah, 0, ',', '.'); ?>" readonly="readonly" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="ppnsub"> <b>PPN</b></label>
                                <div class="col-sm-5">
                                    <input class="col-sm-3 form-control" id="ppn1" value="<?=  number_format($header->NilaiPPn, 0, ',', '.'); ?>" readonly="readonly" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="totsub"><b> Total</b></label>
                                <div class="col-sm-5">
                                    <input class="col-sm-3 form-control" id="totsub1" value="<?=  number_format($header->Total, 0, ',', '.'); ?>" readonly="readonly"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="otosub"><b> Otorisasi</b></label>
                                <div class="col-sm-5">
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="rbtotorisasi" id="rbtotorisasiY" value="Y" onclick="showKetReject();"></input>
                                            YA
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="rbtotorisasi" id="rbtotorisasiT" value="R" onclick="showKetReject();"></input>
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="grpketreject" style="display: none" >
                                <div class="col-sm-offset-3 col-sm-5">
                                    <input type="text" class="col-sm-3 form-control" id="ketreject" name="ketreject" placeholder="alasan"/>
                                </div>
                                <?php echo form_error('ketreject'); ?>
                            </div>

                            <br>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">

                                    <input type="button" class="btn btn-default" name="button" id="button" value="Save" onclick="submit_form()"  />
                                    <input type="button" class="btn" value="Close" onclick="window.close()" />

                                </div>
                            </div>

                            <!--                    </fieldset>-->
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>


</div>

