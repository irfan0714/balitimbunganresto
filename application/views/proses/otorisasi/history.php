<?php
if($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="history.xls"');
}
?>
<html>
<head>
<?php
if(empty($excel)){ ?>
<link href="<?php echo base_url();?>appinclude/css/paging4.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>appinclude/css/s3.css" rel="stylesheet" type="text/css" />
<link rel="<?php echo base_url();?>appinclude/css/CSS/mm_health_nutr.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>appinclude/css/dhtml-vert.css" type="text/css" />
<?php } ?>
</head>
<body>

<table id="ewlistmain" align="center">
<tr><td><br></td></tr>
<tr>
<td><strong><font face="Arial" size="2"> History Outlet <?=$outlet;?></font></strong>
<?php
if(empty($excel)){ ?>
<a href="<?php echo base_url();?>index.php/utility/otorisasi/ViewPopup/<?=$cabang?>/<?=$outlet?>/excel"><img src="<?php echo base_url();?>appinclude/images/ic_excel.gif"></a><?php } ?></td>
</tr>
<tr><td>
    <table width="100%" border="1" style="border-collapse: collapse" bordercolor="#111111">
    <tr bgcolor="F8f79A">
    	<td align=left nowrap colspan="6"><b><font face="Arial" size="2">Faktur</font></b></td>
	</tr>
	<tr>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">NoFaktur</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">TglFaktur</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">JTO</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">TotalPiutang</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">TotalBayar</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">Sisa</font></b></td>
	</tr>
	<?php
		$total1 = 0;
		$total2 = 0;
		$sisa1 = 0;
		if(!empty($Faktur))
		{
			for($a=0;$a<count($Faktur);$a++)
			{
				$sisa = (float)$Faktur[$a]['TotalPiutang'] - (float)$Faktur[$a]['TotalBayar'];
				$total1 = (float)$total1 + (float)$Faktur[$a]['TotalPiutang'];
				$total2 = (float)$total2 + (float)$Faktur[$a]['TotalBayar'];
				$sisa1 = (float)$sisa1 + (float)$sisa;
		?>
			<tr bgcolor="D6DFF7">
			<td align=left nowrap><font face="Arial" size="2"><?=$Faktur[$a]['NoTrans']?></font></td>
			<td align=left nowrap><font face="Arial" size="2"><?=ubah_tanggal($Faktur[$a]['TglTrans'])?></font></td>
			<td align=left nowrap><font face="Arial" size="2"><?=ubah_tanggal($Faktur[$a]['TglJto'])?></font></td>
			<td align=right nowrap><font face="Arial" size="2"><?=number_format($Faktur[$a]['TotalPiutang'], 2, ',', '.')?></font></td>
			<td align=right nowrap><font face="Arial" size="2"><?=number_format($Faktur[$a]['TotalBayar'], 2, ',', '.')?></font></td>
			<td align=right nowrap><font face="Arial" size="2"><?=number_format($sisa, 2, ',', '.')?></font></td>
			</tr>
		<?php
			}
		}
		else
		{
		?>
			<tr bgcolor="D6DFF7">
			<td align=center nowrap colspan="6"><font face="Arial" size="2"><b>Tidak Ada Data</b></font></td>
			</tr>
		<?php
		}
	?>
	<tr bgcolor="FFB6A5">
	<td align=left nowrap colspan="3"><font face="Arial" size="2"><b>Total</b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($total1, 2, ',', '.')?></b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($total2, 2, ',', '.')?></b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($sisa1, 2, ',', '.')?></b></font></td>
	</tr>
	<tr>
	<td align=left nowrap colspan="6">&nbsp;</td>
	</tr>
	<tr bgcolor="F8f79A">
    	<td align=left nowrap colspan="6"><b><font face="Arial" size="2">Retur</font></b></td>
	</tr>
	<tr>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">NoRetur</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">TglRetur</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">JTO</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">TotalPiutang</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">TotalBayar</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">Sisa</font></b></td>
	</tr>
	<?php
		$total1 = 0;
		$total2 = 0;
		$sisa2 = 0;
		if(!empty($Retur))
		{
			for($a=0;$a<count($Retur);$a++)
			{
				$sisa = (float)$Retur[$a]['TotalPiutang'] - (float)$Retur[$a]['TotalBayar'];
				$total1 = (float)$total1 + (float)$Retur[$a]['TotalPiutang'];
				$total2 = (float)$total2 + (float)$Retur[$a]['TotalBayar'];
				$sisa2 = (float)$sisa2 + (float)$sisa;
		?>
			<tr bgcolor="D6DFF7">
			<td align=left nowrap><font face="Arial" size="2"><?=$Retur[$a]['NoTrans']?></font></td>
			<td align=left nowrap><font face="Arial" size="2"><?=ubah_tanggal($Retur[$a]['TglTrans'])?></font></td>
			<td align=left nowrap><font face="Arial" size="2"><?=ubah_tanggal($Retur[$a]['TglJto'])?></font></td>
			<td align=right nowrap><font face="Arial" size="2"><?=number_format($Retur[$a]['TotalPiutang'], 2, ',', '.')?></font></td>
			<td align=right nowrap><font face="Arial" size="2"><?=number_format($Retur[$a]['TotalBayar'], 2, ',', '.')?></font></td>
			<td align=right nowrap><font face="Arial" size="2"><?=number_format($sisa, 2, ',', '.')?></font></td>
			</tr>
		<?php
			}
		}
		else
		{
		?>
			<tr bgcolor="D6DFF7">
			<td align=center nowrap colspan="6"><font face="Arial" size="2"><b>Tidak Ada Data</b></font></td>
			</tr>
		<?php
		}
	?>
	<tr bgcolor="FFB6A5">
	<td align=left nowrap colspan="3"><font face="Arial" size="2"><b>Total</b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($total1, 2, ',', '.')?></b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($total2, 2, ',', '.')?></b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($sisa2, 2, ',', '.')?></b></font></td>
	</tr>
	<tr>
	<td align=left nowrap colspan="6">&nbsp;</td>
	</tr>
	<tr bgcolor="F8f79A">
		<td align=left nowrap  colspan="6"><b><font face="Arial" size="2">Giro</font></b></td>
    </tr>
	<tr>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">NoGiro</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">TglTerima</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">JTO</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">NilaiGiro</font></b></td>
		<td align=left nowrap colspan="2">&nbsp;</td>
	</tr>
	<?php
	$total1 = 0;
	if(!empty($Giro))
	{
		for($a=0;$a<count($Giro);$a++)
		{
			$total1 = (float)$total1 + (float)$Giro[$a]['NilaiGiro'];
	?>
		<tr bgcolor="D6DFF7">
		<td align=left nowrap><font face="Arial" size="2"><?=$Giro[$a]['NoGiro']?></font></td>
		<td align=left nowrap><font face="Arial" size="2"><?=ubah_tanggal($Giro[$a]['TglTerima'])?></font></td>
		<td align=left nowrap><font face="Arial" size="2"><?=ubah_tanggal($Giro[$a]['TglJTo'])?></font></td>
		<td align=right nowrap><font face="Arial" size="2"><?=number_format($Giro[$a]['NilaiGiro'], 2, ',', '.')?></font></td>
		<td align=left nowrap colspan="2" bgcolor="E1DFD2">&nbsp;</td>
		</tr>
	<?php
		}
	}
	else
	{
	?>
		<tr bgcolor="D6DFF7">
		<td align=center nowrap colspan="4"><font face="Arial" size="2"><b>Tidak Ada Data</b></font></td>
		<td align=left nowrap colspan="2" bgcolor="E1DFD2">&nbsp;</td>
		</tr>
	<?php
	}
	$grand = (float)$sisa1 - (float)$sisa2 + (float)$total1;
	?>
	<tr bgcolor="FFB6A5">
	<td align=left nowrap colspan="3"><font face="Arial" size="2"><b>Total</b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($total1, 2, ',', '.')?></b></font></td>
	<td align=left nowrap colspan="2" bgcolor="E1DFD2">&nbsp;</td>
	</tr>
	<tr>
	<td align=left nowrap colspan="6">&nbsp;</td>
	</tr>
	 <tr bgcolor="F8f79A">
    	<td align=left nowrap colspan="3"><b><font face="Arial" size="2">Faktur - Retur + Giro</font></b></td>
		<td align=right nowrap colspan="3"><font face="Arial" size="2"><b><?=number_format($grand, 2, ',', '.')?></b></font></td>
	</tr>
	<tr>
	<td align=left nowrap colspan="6">&nbsp;</td>
	</tr>
	<tr bgcolor="F8f79A">
		<td align=left nowrap  colspan="6"><b><font face="Arial" size="2">Pembayaran 2 Bulan Terakhir</font></b></td>
    </tr>
	<tr>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">NoFaktur</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">TglFaktur</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">TglBayar</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">Cara</font></b></td>
		<td align=left nowrap class="ewtableheader"><b><font face="Arial" size="2">Keterangan</font></b></td>
		<td align=right nowrap class="ewtableheader"><b><font face="Arial" size="2">Bayar + PO + Materai + Disc</font></b></td>
	</tr>
	<?php
	$total1 = 0;
	if(!empty($bayar))
	{
		for($a=0;$a<count($bayar);$a++)
		{
			$total1 = (float)$total1 + (float)$bayar[$a]['Bayar'];
	?>
		<tr bgcolor="D6DFF7">
		<td align=left nowrap><font face="Arial" size="2"><?=$bayar[$a]['NoPiutang']?></font></td>
		<td align=left nowrap><font face="Arial" size="2"><?=$bayar[$a]['TglFaktur']?></font></td>
		<td align=left nowrap><font face="Arial" size="2"><?=$bayar[$a]['TglTransaksi']?></font></td>
		<td align=left nowrap><font face="Arial" size="2"><?=$bayar[$a]['Tipe']?></font></td>
		<td align=left nowrap><font face="Arial" size="2"><?=$bayar[$a]['Ket']?></font></td>
		<td align=right nowrap><font face="Arial" size="2"><?=number_format($bayar[$a]['Bayar'], 2, ',', '.')?></font></td>
		</tr>
	<?php
		}
	}
	else
	{
	?>
		<tr bgcolor="D6DFF7">
		<td align=center nowrap colspan="6"><font face="Arial" size="2"><b>Tidak Ada Data</b></font></td>
		</tr>
	<?php
	}
	?>
	<tr bgcolor="FFB6A5">
	<td align=left nowrap colspan="5"><font face="Arial" size="2"><b>Total</b></font></td>
	<td align=right nowrap><font face="Arial" size="2"><b><?=number_format($total1, 2, ',', '.')?></b></font></td>
	</tr>
    </table>
</td></tr>
</table>
</body>
</html>
<?php
function ubah_tanggal($tanggalan){
 list ($tahun, $bulan, $tanggal) = split ('[-]', $tanggalan);
 $tgl = $tanggal."-".$bulan."-".$tahun;
 return $tgl;
}
?>