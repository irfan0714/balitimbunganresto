<?php
	if ($excel == "excel"){
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="log_otorisasi_faktur.xls"');
		$colorbg	= " ";
		$btnsubmit	= " ";
		$sparator	= ".";
		$digit		= "";
		$deci		= "";			
	}else{
		$sparator	= ".";
		$digit		= "";
		$deci		= "";		
		$colorbg	= " class='gradeX' ";
		$btnsubmit	= " <div align='right'><input name='submit'' type='submit' value='EXPORT TO EXCEL' ></div><br>";
?>
	<head>

	<title>Log Otorisasi <?=$jns?></title>
	<script src="<?php echo base_url();?>/appinclude/tool.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>appinclude/media/css/demo_page.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo base_url();?>appinclude/media/css/tableku.css" type="text/css" />		
		<script type="text/javascript" language="javascript" src="<?php echo base_url();?>appinclude/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="<?php echo base_url();?>appinclude/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				//$('#example1').dataTable();
				$('#example1').dataTable({"sPaginationType": "full_numbers"}); 
			} );
		</script>
	</head>
<?php
	$this->load->view('auth/header');
	$this->load->view('auth/menu.php');
	}

?>

<p>
<form id="form" action="<?=base_url()?>index.php/laporan/otorisasi/index" method="POST">
<body >
			
		<hr><div><strong><font face="Arial" size="2"> <?echo ucwords("Log Otorisasi ".$jns);?></font></strong></div><hr>
		<?//echo $btnsubmit; ?>
		
		<div id="container">

			<div id="demo">
					<table cellpadding="0" cellspacing="0" border="0" class="display" id="example1">
						<thead>
							<tr align="left">
								<th align="left" class="ewtableheader" nowrap>Tanggal Otorisasi</th>
								<th align="left" class="ewtableheader" nowrap>Kode Cabang </th>
								<th align="left" class="ewtableheader" nowrap>No <?=$jns?></th>
								<th align="left" class="ewtableheader" nowrap>Tanggal <?=$jns?></th>
								<th align="left" class="ewtableheader" nowrap>Kode Outlet</th>
								<th align="left" class="ewtableheader" nowrap>Nama Outlet</th>
								<th align="left" class="ewtableheader" nowrap>Keterangan</th>
								<th align="left" class="ewtableheader" nowrap>Nilai <?=$jns?></th>
								<th align="left" class="ewtableheader" nowrap>Total Piutang</th>
								<th align="left" class="ewtableheader" nowrap>Limit Kredit</th>	
								<th align="left" class="ewtableheader" nowrap>History Outlet</th>									
							</tr>
						</thead>
						<tbody>
						<?php 
				   		 	for($i=0;$i<count($list);$i++){
				   		 ?>
							<tr  class='gradeA'>
								<td nowrap><?=$list[$i]['DateOtorisasi']?></td>
								<td nowrap><?=getnamacabang($list[$i]['KdCabang'])?></td>
								<td nowrap><?=$list[$i]['NoTransaksi']?></td>
								<td nowrap><?=$list[$i]['TglTrans']?></td>								
								<td nowrap><?=$list[$i]['KdOutlet']?></td>
								<td nowrap><?=$list[$i]['Nama']?></td>
								<td nowrap><?=$list[$i]['Ket']?></td>
								<td nowrap align="right"><?=number_format($list[$i]['NilaiPiutang'],$digit,$deci,$sparator)?></td>
								<td nowrap align="right"><?=number_format($list[$i]['TotalPiutang'],$digit,$deci,$sparator)?></td>
								<td nowrap align="right"><?=number_format($list[$i]['LimitK'],$digit,$deci,$sparator)?></td>
								<td nowrap align="right"><img src="<?=base_url();?>/appinclude/images/applet3-428.png" width="16" height="16" border="0" onClick="PopupWindow('<?php echo base_url();?>index.php/utility/otorisasi/ViewPopup/<?=$list[$i]['KdCabang']?>/<?=$list[$i]['KdOutlet']?>',600,550);" title = 'History'></td>	
							</tr>
						<?php
				   		 	}
				   		 ?>
							
						</tbody>
						<tfoot>
							<tr>
								<th colspan="23">&nbsp;</th>
							</tr>
						</tfoot>
					</table>
			</div>
	
			
		</div>
	</body>
	</form>
</html>