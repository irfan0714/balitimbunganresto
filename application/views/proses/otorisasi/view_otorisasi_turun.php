<?php
	if ($excel != "excel"){
	$this->load->view('js/TextValidation');
	$this->load->view('js/sprydata');
	$this->load->view('js/datepicker');
?>

		<script src="<?php echo base_url();?>/appinclude/tool.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#TglPerubahan").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url();?>/appinclude/images/tgl.gif' });					
				$('table.search-table').tableSearch({
					searchText:'Search Table',
					searchPlaceHolder:'Input Value'
				});
			});			
		</script>

		<script type="text/javascript">
				function eksekusi(par) {
					
					var cbg				= $('#KdCabang').val();
					var idtransaksi		= $('#idtransaksi').val();
					var TglPerubahan	= $('#TglPerubahan').val();			
					if(par=="Print"){
						var BtnVal		= "";
					}else{
						var BtnVal		= "excel";
					}
					var kirim 	   		= idtransaksi+ '|' + TglPerubahan+ '|' + cbg+ '|' + BtnVal;			
					$("#fieldparameter").val(kirim);
					$("#form").submit();
				}
		</script>

		<style>
			#formContainer {
				width: 30%;
			}
			input {
				border: 1px solid #666666;
			}


			label {
				display: block;
			}

			#Col1, #Col2 {
				width: 20%;
			}

			#Col2 {
				float: right;
			}
			form {
				margin: 0px;
			}
		</style>
		
		<style>
		table.search-table
		{ width: 100%;
		background-color: #EAE1E1;
		border: 1px #000000 solid;
		border-collapse: collapse;
		border-spacing: 0px; }





		td.Contact
		{ border-bottom: 1px #079140 solid;
		font-family: Verdana, sans-serif, Arial;
		font-weight: normal;
		font-size: .7em;
		color: #000000;
		background-color: #FFFFFF;
		padding-top: 4px;
		padding-bottom: 4px;
		padding-left: 8px;
		padding-right: 0px; }
		</style>
</head>
<body>

<?php
	$this->load->view('auth/header');
	$this->load->view('auth/menu.php');
?>

   	<fieldset style=" background-color:#E1DFD2; " ><legend style="background-color:transparent;">FORM OTORISASI PENURUNAN LIMIT</legend>
		<div id="formContainer">
			<div>
				<label>No.Transaksi</label>
				<input type="text" id="idtransaksi" name="idtransaksi" value="<?=$idtransaksi?>" readonly></br></br>
			</div>		
			<div>
				<label>Tgl.Perubahan</label>
				<input type="text" id="TglPerubahan" name="TglPerubahan" value="<?=$TglPerubahan?>" ></br></br>
			</div>			
			<div>
				<label>Cabang</label>
				<?=form_dropdownDB_init('KdCabang',$Cabang,'KdCabang','NamaCabang',$KdCabang,'','Pilih','id="KdCabang"');?></br></br>
			</div>
			<div>
				<?php
					$Pilihan_sls = $list[0]['pilsls'];
					if($Pilihan_sls=="A"){
						$str_P1	= "checked";
						$str_P2	= "";
						$str_P3	= "";
						$jum_avg	= "3";
					}elseif($Pilihan_sls=="B"){
						$str_P1	= "";
						$str_P2	= "checked";
						$str_P3	= "";
						$jum_avg	= "6";
					}else{
						$str_P1	= "";
						$str_P2	= "";
						$str_P3	= "checked";
						$jum_avg	= "12";
					}
				?>						
				<label>Tampilkan Sales Per:</label>
				<input type="radio" name="pilsls" id="pilsls" value="A" <?=$str_P1;?> >&nbsp;3 Bulan&nbsp;<input type="radio" name="pilsls" id="pilsls" value="B" <?=$str_P2;?> >&nbsp;6 Bulan&nbsp;<input type="radio" name="pilsls" id="pilsls" value="C" <?=$str_P3;?> >&nbsp;12 Bulan&nbsp;</br></br>
			</div>		
			<div>
			
				<?php
					$Kategori = $list[0]['Type'];
					if($Kategori=="K"){
						$str_k	= "checked";
						$str_l	= "";
					}else{
						$str_k	= "";
						$str_l	= "checked";					
					}
				?>				
				<label>Rata-Rata Sales</label>
				<input type="radio" name="ktgr" id="ktgr" value="K" <?=$str_k;?> >&nbsp;Kurang Dari Limit Kredit&nbsp;<input type="radio" name="ktgr" id="ktgr" value="L" <?=$str_l;?> >&nbsp;Lebih Besar Dari Limit Kredit&nbsp;</br></br>
			</div>				
		</div>
		<div>
			<?php 
				if($action=="view"){
			?>
			<input type="button" id="Print" name="Print" value="Preview" class='button' onclick="eksekusi('Print')">
			<?php
				}else{
			?>
			<input type="submit" id="Print" name="Print" value="Save" class='button' >
			<?php
				}
			?>
			
			<input name='excel' id='excel' type='button' value='excel' ONCLICK =parent.location="<?=base_url()?>index.php/utility/otorisasiturunlimit/excel/<?=$idtransaksi?>/" >
			<input name='btnback' id='btnback' type='button' value='back' ONCLICK =parent.location="<?=base_url()?>index.php/utility/otorisasiturunlimit/" >
			
			
		</div>
		<input type="hidden" name="fieldparameter" id="fieldparameter">
	</fieldset>
	<br><br>
	<?php
	}
	if(!empty($list)){
		//$this->load->view('utility/otorisasi/otorisasi_turun_report');
?>		
<div align='left'>
		<div id="container">

			<div id="demo">
					<table cellpadding="0" cellspacing="0" border="0" class="search-table" >
						<thead>
							<tr align="left">
								<td align="left" class="ewtableheader" nowrap>No</td>
								<td align="left" class="ewtableheader" nowrap>Cabang</th>
								<td align="left" class="ewtableheader" nowrap>Kode Outlet</td>
								<td align="left" class="ewtableheader" nowrap>Nama Outlet </td>	
								<?php 
									for($a=0;$a<$jum_avg;$a++){
								?>
								<td align="right" class="ewtableheader" nowrap>Sales <?=$a+1;?> </td>	
								<?php
									}
								?>
								<td align="right" class="ewtableheader" nowrap>Limit Lama</td>								
								<td align="right" class="ewtableheader" nowrap>Limit Baru</td>
								
							</tr>
						</thead>
						<tbody>
						<?php 
							$no		= 0;
				   		 	for($i=0;$i<count($list);$i++){
							$no		= $no + 1;
				   		 ?>
							<tr  class='gradeA'>
								<td nowrap class='Contact'><?=$no?></td>
								<td nowrap class='Contact'><?=getNamaCabang($list[$i]['KdCabang'])?></td>
								<td nowrap class='Contact'><?=$list[$i]['KdOutlet']?><input type="hidden" name="Outlet[]" id="Outlet<?=$no;?>" size="15" value="<?=$list[$i]['KdOutlet'];?>"  ></td>
								<td nowrap class='Contact'><?=$list[$i]['Nama']?><input type="hidden" name="Limit[]" id="Limit<?=$no;?>" size="15" value="<?=$list[$i]['LimitK'];?>"  ></td>
								<?php 
									$nmr	= 0;
									for($a=0;$a<$jum_avg;$a++){ 
										$nmr	= $nmr + 1;
										if(strlen($nmr)==1){
											$num	= "0".$nmr;
										}else{
											$num	= $nmr;
										}
								?>
								<td nowrap align="right" class='Contact'><?=number_format($list[$i]['sales_'.$num],"","",".")?></td>
								<?php
									}
								?>														
								<td nowrap align="right" class='Contact'><?=number_format($list[$i]['Limit_lama'],"","",".")?></td>
								<td nowrap align="right" class='Contact'><input type="text" name="tambahan[]" id="tambahan<?=$no;?>" size="15" value="<?=number_format($list[$i]['Limit_baru'],"","",".")?>"  onkeyup="FormatCurrency(this);" style="text-align: right;" ></td>
								
							</tr>
						<?php
				   		 	}
				   		 ?>
							
						</tbody>
					</table>
			</div>
	
			
		</div>

<?php		
	}
	
$this->load->view('auth/footer'); ?>
</body>
</html>


