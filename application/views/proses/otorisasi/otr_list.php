<?php $this->load->view('header'); ?><!--
<script language="javascript" src="<?= base_url(); ?>public/js/jquery.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css" />
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
    function gantiSearch()
    {
        if ($("#searchby").val() == "NoDokumen")
        {
            $("#normaltext").css("display", "");
            $("#datetext").css("display", "none");
            $("#date1").datepicker("destroy");
            $("#date1").val("");
        }
        else
        {
            $("#datetext").css("display", "");
            $("#normaltext").css("display", "none");
            $("#stSearchingKey").val("");
            $('#date1').Zebra_DatePicker({format: 'd-m-Y'});
            //$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>/public/images/calendar.png' });
        }
    }
    function deleteTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menghapus Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/transaksi/order_barang/delete_order", {
                kode: nodok},
            function (data) {
                window.location = url + "index.php/transaksi/order_barang";
            });
        }
    }
    function tutupTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menutup Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/transaksi/order_barang/tutup_order", {
                kode: nodok},
            function (data) {
                window.location = url + "index.php/transaksi/order_barang";
            });
        }
    }
    function bukaTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Membuka Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/transaksi/order_barang/buka_order", {
                kode: nodok},
            function (data) {
                alert(data);
                if (data == "sudahtutup")
                    alert("Tidak bisa dibuka lagi, barang sudah diterima semua");
                window.location = url + "index.php/transaksi/order_barang";
            });
        }
    }
    function PopUpOtorisasi(kode, baseurl)
    {
        url = "proses/otorisasi/view_otorisasi/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }
    function PopUpPrint(kode, baseurl)
    {
        url = "proses/otorisasi/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }
</script>
<form method="POST"  name="search" action="">



    <br>

    <table align = 'center' border='1' class='table_class_list'>
        <tr>
            <?php
            if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                ?>
                <th></th>
                <?php
            }
            $mylib = new globallib();
            echo $mylib->write_header($header);
            ?>
        </tr>
        <?php
        if (count($data) == 0) {
            ?>
            <td nowrap colspan="<?php echo count($header) + 1; ?>" align="center">Tidak Ada Data</td>
            <?php
        }
        for ($a = 0; $a < count($data); $a++) {
//print_r($data);die();
            ?>
            <tr>
                <?php
                if ($link->edit == "Y" || $link->delete == "Y") {
                    ?>
                    <td nowrap>
                        <?php
                        if ($link->view == "Y") {
                            ?>
                            <img src='<?= base_url(); ?>public/images/zoom.png' border = '0' title = 'Print' onclick="PopUpPrint('<?= $data[$a]['NoDokumen']; ?>', '<?= base_url(); ?>');"/></a>
                            <?php
                        }
                        if ($data[$a]['FlagPengiriman'] == "Y") {
                            ?>
                            <img src='<?= base_url(); ?>public/images/accept.png' border='0' title="terkirim"/>
                            <?php
                        }
                        if ($data[$a]['FlagOtorisasi'] == "T") {
                            ?>
                            <img src='<?= base_url(); ?>public/images/pick.png' border='0' title="otorisasi" onclick="PopUpOtorisasi('<?= $data[$a]['NoDokumen']; ?>', '<?= base_url(); ?>');"/>
                            <?php
                        }
                        ?>
                    </td>
                <?php } ?>
                <td nowrap><?= $data[$a]['NoDokumen']; ?></td>
                <td nowrap><?= $data[$a]['Tanggal']; ?></td>
                <td nowrap><?= stripslashes($data[$a]['NamaGudang']); ?></td>
                <td nowrap><?= stripslashes($data[$a]['Nama']); ?></td>
                <td nowrap><?= stripslashes($data[$a]['Keterangan']); ?></td>
                <td nowrap class="InputAlignRight"><?= number_format($data[$a]['Jumlah'], 0, ',', '.'); ?></td>
                <td nowrap class="InputAlignRight"><?= number_format($data[$a]['PPn'], 0, ',', '.'); ?></td>
                <td nowrap class="InputAlignRight"><?= number_format($data[$a]['Total'], 0, ',', '.'); ?></td>
                <td nowrap colspan="2">
                    <?php
                                if ($data[$a]['FlagOtorisasi'] == "Y") {
                                    echo "<font color=green>Sudah Approve</font>";
                                } elseif ($data[$a]['FlagOtorisasi'] == "T") {
                                    echo "<font color=red>Belum Approve</font>";
                                } else {
                                    echo "<font color=red>Reject</font>";
                                }
                                ?>
                </td>
            <tr>
                <?php
            }
            ?>
    </table>
    <table align = 'center'  >
        <tr>
            <td>
                <?php echo $this->pagination->create_links(); ?>
            </td>
        </tr>
        <?php
        if ($link->add == "Y") {
            ?>
            <tr>
<!--                <td nowrap colspan="3">
                    <input type="button" value="proses" onclick="prosesOTR();">
                </td>-->
            <?php } ?>
    </table>
</form>
<?php $this->load->view('footer'); ?>