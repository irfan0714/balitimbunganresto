<?php
	if ($excel == "excel"){
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="log_otorisasi_faktur.xls"');
		$colorbg	= " ";
		$btnsubmit	= " ";
		$sparator	= ".";
		$digit		= "";
		$deci		= "";			
	}else{
		$sparator	= ".";
		$digit		= "";
		$deci		= "";		
		$colorbg	= " class='gradeX' ";
		$btnsubmit	= " <div align='right'><input name='submit'' type='submit' value='EXPORT TO EXCEL' ></div><br>";
?>
	<head>

	<title>Log Otorisasi Gudang</title>
	<script src="<?php echo base_url();?>/appinclude/tool.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>appinclude/media/css/demo_page.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo base_url();?>appinclude/media/css/tableku.css" type="text/css" />		
		<script type="text/javascript" language="javascript" src="<?php echo base_url();?>appinclude/media/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="<?php echo base_url();?>appinclude/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				//$('#example1').dataTable();
				$('#example1').dataTable({"sPaginationType": "full_numbers"}); 
			} );
		</script>
	</head>
<?php
	$this->load->view('auth/header');
	$this->load->view('auth/menu.php');
	}

?>

<p>
<form id="form" action="<?=base_url()?>index.php/utility/logotorisasigudang/index" method="POST">
<body >
			
		<hr><div><strong><font face="Arial" size="2"> <?echo ucwords("Log Otorisasi Gudang");?></font></strong></div><hr>
		<?//echo $btnsubmit; ?>
		
		<div id="container">
			<?php if(!empty($list)){ ?>
			<div id="demo">
					<table cellpadding="0" cellspacing="0" border="0" class="display" id="example1">
						<thead>
							<tr align="left">
								
								<th align="left" class="ewtableheader" nowrap>Tanggal Otorisasi</th>
								<th align="left" class="ewtableheader" nowrap>Kode Cabang </th>
								<th align="left" class="ewtableheader" nowrap>No Dokumen</th>
								<th align="left" class="ewtableheader" nowrap>Tanggal </th>
								<th align="left" class="ewtableheader" nowrap>Kode Outlet</th>
								<th align="left" class="ewtableheader" nowrap>Nama Outlet</th>
								<th align="left" class="ewtableheader" nowrap>Keterangan</th>
								<th align="left" class="ewtableheader" nowrap>Total</th>
								<th align="left" class="ewtableheader" nowrap>Status</th>
								<th align="left" class="ewtableheader" nowrap>Detail</th>									
							</tr>
						</thead>
						<tbody>
						<?php 
				   		 	for($i=0;$i<count($list);$i++){
								$flag	= $list[$i]['FlagOtorisasi'];
								if($flag=="A"){
									$flagsts	= "Ya";
								}elseif($flag=="C"){
									$flagsts	= "Tidak";
								}elseif($flag=="O"){
									$flagsts	= "Ya (Offline)";
								}else{
									$flagsts	= "Tidak (Offline)";
								}
							
				   		 ?>
							<tr  class='gradeA'>
								
								<td nowrap><?=luardate($list[$i]['TglOtorisasi'])?></td>
								<td nowrap><?=getnamacabang($list[$i]['KdCabang'])?></td>
								<td nowrap><?=$list[$i]['NoDokumen']?></td>
								<td nowrap><?=luardate($list[$i]['TglDokumen'])?></td>								
								<td nowrap><?=$list[$i]['KdOutlet']?></td>
								<td nowrap><?=$list[$i]['nmoutlet']?></td>
								<td nowrap><?=$list[$i]['Keterangan']?></td>
								<td nowrap align="right"><?=number_format($list[$i]['Total'],$digit,$deci,$sparator)?></td>
								<td nowrap><?=$flagsts;?></td>
								<td nowrap align="right"><img src="<?=base_url();?>/appinclude/images/applet3-428.png" width="16" height="16" border="0" onClick="PopupWindow('<?php echo base_url();?>index.php/utility/otorisasigudang/ViewDetail/<?=$list[$i]['KdCabang']?>/<?=$list[$i]['NoDokumen']?>/<?=$list[$i]['KdPengeluaran']?>',600,600);" title = 'Detail'></td>	
							</tr>
						<?php
				   		 	}
				   		 ?>
							
						</tbody>
						<tfoot>
							<tr>
								<th colspan="13">&nbsp;</th>
							</tr>
						</tfoot>
					</table>
			</div>
			<?php } ?>
			
		</div>
	</body>
	</form>
</html>