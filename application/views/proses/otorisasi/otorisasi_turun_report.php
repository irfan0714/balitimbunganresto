﻿<?php
	if ($excel == "excel"){
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="log_otorisasi_faktur.xls"');
		$colorbg	= " ";
		$btnsubmit	= " ";
		$sparator	= ".";
		$digit		= "";
		$deci		= "";			
	}else{
		$sparator	= ".";
		$digit		= "";
		$deci		= "";		
		$colorbg	= " class='gradeX' ";
		$btnsubmit	= " ";
	}
?>

	<head>
<style>
table.search-table
{ width: 100%;
background-color: #EAE1E1;
border: 1px #000000 solid;
border-collapse: collapse;
border-spacing: 0px; }





td.Contact
{ border-bottom: 1px #079140 solid;
font-family: Verdana, sans-serif, Arial;
font-weight: normal;
font-size: .7em;
color: #000000;
background-color: #FFFFFF;
padding-top: 4px;
padding-bottom: 4px;
padding-left: 8px;
padding-right: 0px; }
</style>
	<title>Penurunan Limit</title>
	<script src="<?php echo base_url();?>/appinclude/tool.js" type="text/javascript"></script>
	
		<script type="text/javascript" src="<?php echo base_url();?>appinclude/js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>appinclude/js/html-table-search.js"></script>
		<!--<script type="text/javascript" language="javascript" src="<?php echo base_url();?>appinclude/media/js/jquery.js"></script>-->
		
		<script type="text/javascript">
			$(document).ready(function(){
				$('table.search-table').tableSearch({
					searchText:'Search Table',
					searchPlaceHolder:'Input Value'
				});
			});
		</script>
		
	<script>

		function hilang_spasi(string) {
            return string.split(' ').join('');
        }	

		function RemCommas(nStr){
			var string 	= nStr;
			var str = string.replace(/,/g,"");
			return str;
		}
	    
	</script>		
	</head>

<p>

<body >
			
		<form id="form" method="post" action="<?=base_url()?>index.php/utility/otorisasiturunlimit/<?=$action?>">
		<div align='left'>
		<div id="container">

			<div id="demo">
					<table cellpadding="0" cellspacing="0" border="0" class="search-table">
						<thead>
							<tr align="left" >
								<td align="left" class='ewtableheader' nowrap>No</td>
								<td align="left" class='ewtableheader' nowrap>Cabang</td>
								<td align="left" class='ewtableheader' nowrap>Kode Outlet</td>
								<td align="left" class='ewtableheader' nowrap>Nama Outlet </td>								
								<?php 
									$pisah_cap	= explode("^",$lbl_cap);
									for($j=0;$j<count($pisah_cap);$j++){
										$bul	= substr(indMonthName(substr($pisah_cap[$j],0,2)),0,3);
										$thn	= substr($pisah_cap[$j],2,4);
								?>
								<td align="left" class='ewtableheader' nowrap><?=$bul."-".$thn;?></td>
								<?php } ?>																
								<td align="center" class='ewtableheader' nowrap>Avrage</td>
								
								<td align="right" class='ewtableheader' nowrap>Limit Lama</td>								
								<td align="right" class='ewtableheader' nowrap>Limit Baru</td>
							</tr>
						</thead>
						<tbody>
						<?php 
							$no		= 0;
				   		 	for($i=0;$i<count($list);$i++){
							$no		= $no + 1;
				   		 ?>
							<tr  >
								<td nowrap class='Contact'><?=$no?></td>
								<td nowrap class='Contact'><?=getNamaCabang($list[$i]['KdCabang'])?></td>
								<td nowrap class='Contact'><?=$list[$i]['KdOutlet']?></td>
								<td nowrap class='Contact'><?=$list[$i]['Nama']?><input type="hidden" name="Limit[]" id="Limit<?=$no;?>" size="15" value="<?=$list[$i]['LimitK'];?>"  ></td>
								<?php 
									$nmr		= 0;
									$vargab	= "";
									for($jj=0;$jj<count($pisah_cap);$jj++){
									$nmr	= $nmr + 1;
									if($nmr=="1"){
										$vargab	.= $list[$i]['nto'.$nmr];
									}else{
										$vargab	.= "~".$list[$i]['nto'.$nmr];
									}
									
								?>
									<td nowrap class='Contact' align="right"><?=number_format($list[$i]['nto'.$nmr],"0","0",".") ?></td>
								<?php }
			
								?>
									<input type="hidden" id="var_sls<?=$i;?>" name="var_sls[]"  value="<?=$vargab;?>" size="25">
									<td nowrap class='Contact' align="right" bgcolor="#F9FC9F"><input readonly type="text" name="avg[]" id="avg<?=$no;?>" size="15" value="<?=number_format($list[$i]['avr'],"0","0",".") ?>"  onkeyup="FormatCurrency(this);" style="text-align: right;"  ></td>								
									<td nowrap align="right" class='Contact'><input type="hidden" name="Outlet[]" id="Outlet<?=$no;?>" size="15" value="<?=$list[$i]['KdOutlet'];?>"  ><?=number_format($list[$i]['LimitK'],$digit,$deci,$sparator)?></td>
									<td nowrap align="right" class='Contact'><input type="text" name="tambahan[]" id="tambahan<?=$no;?>" size="15" value=""  onkeyup="FormatCurrency(this);" style="text-align: right;" ></td>	
							</tr>
						<?php
				   		 	}
				   		 ?>
							
						</tbody>

					</table>
			</div>
	
			
		</div>
		</form>
	</body>
	
</html>