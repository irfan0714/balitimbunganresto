<?php
$this->load->view('header'); 
?>
<script>
function Konfirmasi()
{
    tgl = $("#tanggaltrans").val();
    thnbln = tgl.substr(0,4)+tgl.substr(5,2);
	blnthn = tgl.substr(5,2)+"-"+tgl.substr(0,4);
    if(document.getElementById("tahun").value=="")
	{
		alert("Pilih tahun dahulu");
	}else
	{
		if(document.getElementById("bulan").value=="")
		{
			 alert("Pilih bulan dahulu");
		}
		else
		{
		    bulan = $("#bulan").val();
		    tahun = $("#tahun").val();
		    thnbln2 = tahun+bulan;
		    	
			ss = confirm("Buat Jurnal bulan "+bulan+"-"+tahun+"?");
			if(ss)
			{
				document.getElementById("posting").submit();
			}
		}
	}

}
</script>
<body>
<br>
<form method='post' name="posting" id="posting" action='<?=base_url();?>index.php/proses/create_jurnal/proses'>
<table cellspacing="3" cellpadding="3" align="center" border='0'>
<tr nowrap><td>Create Jurnal :</td></tr>
<tr>
	<td nowrap>Tahun</td>
	<td nowrap>:</td>
	<td nowrap>
		<input size="10" id="tahun" name="tahun" value="<?=$tahunaktif;?>" readonly>
	</td>
</tr>

<tr>
    <td nowrap>Bulan</td>
	<td nowrap>:</td>
	<td nowrap>
		<input size="10" id="bulan" name="bulan" value="<?=$bulanaktif;?>" readonly>
	</td>
</tr>
<tr>
    <td nowrap>Module</td>
	<td nowrap>:</td>
	<td nowrap>
		<select class="form-control-new" name="module" id="module" >
			<option value="All">Semua</option>
			<option value="PV">Pembayaran</option>
			<option value="RV">Penerimaan</option>
			<option value="PI">Pembelian</option>
			<option value="PIM">Pembelian Non Stok</option>
			<option value="R">Penjualan</option>
			<option value="SI">Distribusi</option>
			<option value="RJ">CN/DN Penjualan</option>
			<option value="RB">CN/DN Pembelian</option>
			<option value="RUM">Realisasi UM</option>
			<option value="DL">Penerimaan Lain</option>
			<option value="PL">Pengeluaran Lain</option>
			<option value="LL">Produksi</option>
			<option value="ADJ">Adjustment</option>
			<option value="IS">Jasa Sanitasi</option>
		</select>
	</td>
</tr>
</table>
<br>
<table align="center">
	<tr align="center">
	<td><input type="hidden" name="tanggaltrans" id="tanggaltrans" value=<?=$tanggaltrans?>></td>
	<td><input type="button" value="Posting" onclick="Konfirmasi()"></td>
	<td>&nbsp;</td>
	</tr>
	<tr align="center">
	    <td>&nbsp;</td>
		<td nowrap><?=$msg?></td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>