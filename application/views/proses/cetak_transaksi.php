<?php
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

  
$tgl_oke  = $header[0]['Tanggal'];
list($kasirnya, $nama) = explode('-',$header[0]['Kasir']);
  
$ftext = ''; 
$alamatPT=$store[0]['Alamat1PT'];
$tgl=$header[0]['Tanggal'];
$tgl_1=explode("-",$tgl);
$tgl_tampil=$tgl_1[2]."/".$tgl_1[1]."/".$tgl_1[0];

printer_dos($ftext, $reset.$elite);
printer_dos($ftext, $dwidth.str_pad($store[0]['NamaPT'],37," ",STR_PAD_BOTH).$ndwidth."\r\n");
printer_dos($ftext, str_pad($store[0]['Alamat1PT'],39," ",STR_PAD_BOTH)."\r\n");
printer_dos($ftext, str_pad($store[0]['Alamat2PT'],39," ",STR_PAD_BOTH)."\r\n");
printer_dos($ftext, str_pad($store[0]['TelpPT'],39," ",STR_PAD_BOTH)."\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "Kasir : ".$header[0]['Kasir']."\r\n");
printer_dos($ftext, "Tanggal Transaksi : ".$tgl_tampil."\r\n");
printer_dos($ftext, "========================================\r\n");
printer_dos($ftext, str_pad("Total Struk ",24).":".str_pad($header[0]['TTLStruk'],15," ",STR_PAD_LEFT)."\r\n");
printer_dos($ftext, "----------------------------------------\r\n");
printer_dos($ftext, str_pad("Total Pendapatan ",24).":".str_pad(number_format($header[0]["TotalNilai"], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
printer_dos($ftext, "----------------------------------------\r\n");
printer_dos($ftext, str_pad("Rincian ",40," ",STR_PAD_RIGHT)."\r\n");
if($header[0]['Tunai']<>0)
printer_dos($ftext, str_pad("Tunai Rp",24).":".str_pad(number_format($header[0]['Tunai']-$header[0]['Kembali'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
if($header[0]['USD']<>0){
	$usd = $kurs['USD'] * $header[0]['USD'];
	printer_dos($ftext, str_pad("USD (".$header[0]['USD']." x ". $kurs['USD'].")",24).":".str_pad(number_format($usd, 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");	
}
if($header[0]['CNY']<>0){
	$cny = $kurs['CNY'] * $header[0]['CNY'];
	printer_dos($ftext, str_pad("CNY (".$header[0]['CNY']." x ". $kurs['CNY'].")",24).":".str_pad(number_format($cny, 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
}
if($header[0]['KKredit']<>0)
printer_dos($ftext, str_pad("Kartu Kredit ",24).":".str_pad(number_format($header[0]['KKredit'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
if($header[0]['KDebit']<>0)
printer_dos($ftext, str_pad("Kartu Debit ",24).":".str_pad(number_format($header[0]['KDebit'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
if($header[0]['GoPay']<>0)
printer_dos($ftext, str_pad("GoPay ",24).":".str_pad(number_format($header[0]['GoPay'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
if($header[0]['Voucher']<>0)
printer_dos($ftext, str_pad("Voucher ",24).":".str_pad(number_format($header[0]['Voucher'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
printer_dos($ftext, "========================================\r\n");
printer_dos($ftext, "             ===Terima kasih===          \r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, $fcut);
printer_dos($ftext, $op_cash);
//printer_close($ftext);
?>