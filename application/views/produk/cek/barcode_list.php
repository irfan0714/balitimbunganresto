<?php
$this->load->view('header');
$searchby = $this->input->post('searchby');
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<!--<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>-->
<script language="javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script language="javascript">
   
    function carikode()
    {
		url = "<?=base_url();?>";
		kode = document.getElementById('kode').value;
		alert(kode);
		 $.post(url + "index.php/produk/cek_barcode/getPCode", {
                        PCode: kode},
                    function (msg) {
							if(msg=="Salah"){
								alert("Barcode Salah")
							}else{
								dt = msg.split("##");
								document.getElementById('pcode').value = dt[0];
								document.getElementById('nama').value = dt[1];
								document.getElementById('hrg').value = dt[3];
							}
						}
						);
       
    }
</script>
<body >
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    Cek Barcode 
                </div>
            </div>
            <div class="panel-body">

               <div class="form-group">
                        <label class="col-sm-1"> Kode Barcode </label>
                        <div class="col-sm-5">
                            <input name="ket" type="kode" class="form-control" id="kode" value="" size="35" maxlength="35"   />
                        </div>
                    </div>
<br>
				 <div class="form-group">
                    <div class="form-group" style="display:none">
                        <label class="col-sm-1"> No </label>
                        <div class="col-sm-2">
                            <input name="nodok" type="text" class="form-control" id="nodok" value="" size="10" maxlength="10"   />
                        </div>
                    </div>

				<div class="form-group">
					<label class="col-sm- control-label"> </label>
                        <div class="col-sm-7">
                            <button class="btn btn-primary" type="submit" onclick="carikode();"><i class="entypo-drive"></i>Cek</button>
						</div>

				</div>
				<br />
				<div class="form-group">
                        <label class="col-sm-3"> Kode Barang </label>
                        <div class="col-sm-5">
                            <input name="pcode" type="text" class="form-control" id="pcode" value="" size="35" maxlength="35"   />
                        </div>
                </div>
				<br />
				<div class="form-group">
                        <label class="col-sm-3"> Nama Barang </label>
                        <div class="col-sm-5">
                            <input name="nama" type="text" class="form-control" id="nama" value="" size="35" maxlength="35"   />
                        </div>
                </div>
				<br />
				<div class="form-group">
                        <label class="col-sm-3"> Harga Jual </label>
                        <div class="col-sm-5">
                            <input name="hrg" type="text" class="form-control" id="hrg" value="" size="35" maxlength="35"   />
                        </div>
                </div>

				</div>

                
            </div>
        </div>
    </div>
</body>
<?php $this->load->view('footer'); ?>
