<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Report Penggunaa Diskon";
?>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_penggunaan_diskon/search_report/" onsubmit="return false">
		<input type="hidden" id="tmp_nama_kary" value="<?php echo $tmp_nama_kary;?>"/>
		<input type="hidden" id="tmp_jns_mem" value="<?php echo $tmp_jns_mem;?>"/>
		<input type="hidden" id="tmp_nama_mem" value="<?php echo $tmp_nama_mem;?>"/>
		<input type="hidden" id="tmp_jns_cards" value="<?php echo $tmp_jns_cards;?>"/>
	    <table class="table table-bordered responsive">                        
	        			
	        <tr>
	            <td class="title_table" width="150">Periode</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        
	        <tr>
	            <td class="title_table">Jenis Pencarian</td>
	            <td> 
	            	<select class="form-control-new" name="v_tipe_cari" id="v_tipe_cari" style="width: 200px;" onchange="jenis_cari()">
	            		<option <?php if($v_tipe_cari=="karyawan"){ echo "selected='selected'"; } ?> value="karyawan">Diskon Karyawan</option>
	            		<option <?php if($v_tipe_cari=="members"){ echo "selected='selected'"; } ?> value="members">Members</option>
	            		<option <?php if($v_tipe_cari=="cards"){ echo "selected='selected'"; } ?> value="cards">Cards</option>
	            	</select>   
	            </td>
	        </tr>
	        
	        
	        <tr id="nama_kary" style="display: ;">
	            <td class="title_table">Nama Karyawan</td>
	            <td> 
	            	<select class="form-control-new" name="v_employee" id="v_employee" style="width: 200px;">
	            		
	            		<option value="">All</option>
	            		<?
	            		foreach($employee as $val)
	            		{
	            			$selected="";
	            			if($v_employee==$val["employee_nik"])
	            			{
								$selected = "selected='selected'";
							}
	            			
							?><option <?php echo $selected ?> value="<?php echo $val["employee_nik"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        
	        <tr id="jns_mem" style="display: none;">
	            <td class="title_table">Jenis Members</td>
	            <td> 
	            	<select class="form-control-new" name="v_jenis_members" id="v_jenis_members" style="width: 200px;">
	            		
	            		<option value="">All</option>
	            		<?
	            		foreach($members as $val)
	            		{
	            			$selected="";
	            			if($v_jenis_members==$val["KdTipeMember"])
	            			{
								$selected = "selected='selected'";
							}
	            			
							?><option <?php echo $selected ?> value="<?php echo $val["KdTipeMember"]; ?>"><?php echo $val["NamaTypeMember"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr id="nama_mem" style="display: none;">
	            <td class="title_table">Nama Members</td>
	            <td> 
	            	<select class="form-control-new" name="v_nama_members" id="v_nama_members" style="width: 200px;">
	            		
	            		<option value="">All</option>
	            		<?
	            		foreach($namamembers as $val)
	            		{
	            			$selected="";
	            			if($v_kdmembers==$val["KdMember"])
	            			{
								$selected = "selected='selected'";
							}
	            			
							?><option <?php echo $selected ?> value="<?php echo $val["KdMember"]; ?>"><?php echo $val["NamaMember"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        
	        <tr id="jns_cards" style="display: none;">
	            <td class="title_table">Jenis Cards</td>
	            <td> 
	            	<select class="form-control-new" name="v_cards" id="v_cards" style="width: 200px;">
	            		
	            		<option value="">All</option>
	            		<?php
	            		foreach($cards as $val)
	            		{
	            			$selected="";
	            			if($v_kdgroupdisc==$val["KdGroupDisc"])
	            			{
								$selected = "selected='selected'";
							}
	            			
							?><option <?php echo $selected ?> value="<?php echo $val["KdGroupDisc"]; ?>"><?php echo $val["NamaGroupDisc"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
			
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT) 
{
	if($v_pilihan=="karyawan"){
		 $this->load->view("report/penggunaan_diskon/tampil_diskon_karyawan");
	}
	elseif($v_pilihan=="members"){
		 $this->load->view("report/penggunaan_diskon/tampil_diskon_members");
	}
	elseif($v_pilihan=="cards"){
		 $this->load->view("report/penggunaan_diskon/tampil_diskon_cards");
	}
   
}

$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
    
    $(document).ready(function()
		{
			    tmp_nama_kary = $("#tmp_nama_kary").val();
			    tmp_jns_mem = $("#tmp_jns_mem").val();
			    tmp_nama_mem = $("#tmp_nama_mem").val();
			    tmp_jns_cards = $("#tmp_jns_cards").val();
			    
			    if(tmp_nama_kary=="1"){
					document.getElementById("nama_kary").style.display = "";
					document.getElementById("jns_mem").style.display = "none";
					document.getElementById("nama_mem").style.display = "none";
					document.getElementById("jns_cards").style.display = "none";
				}else if(tmp_jns_mem=="1" && tmp_nama_mem=="1"){
					document.getElementById("nama_kary").style.display = "none";
					document.getElementById("jns_mem").style.display = "";
					document.getElementById("nama_mem").style.display = "";
					document.getElementById("jns_cards").style.display = "none";
				}else if(tmp_jns_cards=="1"){
					document.getElementById("nama_kary").style.display = "none";
					document.getElementById("jns_mem").style.display = "none";
					document.getElementById("nama_mem").style.display = "none";
					document.getElementById("jns_cards").style.display = "";
				}
				
		});
    
	function jenis_cari(){
		 
		jenis = document.getElementById("v_tipe_cari").value;
		
		if(jenis=="karyawan"){
			document.getElementById("nama_kary").style.display = "";
			document.getElementById("jns_mem").style.display = "none";
			document.getElementById("nama_mem").style.display = "none";
			document.getElementById("jns_cards").style.display = "none";
		}else if(jenis=="members"){
			document.getElementById("nama_kary").style.display = "none";
			document.getElementById("jns_mem").style.display = "";
			document.getElementById("nama_mem").style.display = "";
			document.getElementById("jns_cards").style.display = "none";
		}else if(jenis=="cards"){
			document.getElementById("nama_kary").style.display = "none";
			document.getElementById("jns_mem").style.display = "none";
			document.getElementById("nama_mem").style.display = "none";
			document.getElementById("jns_cards").style.display = "";
		}
	}
</script>