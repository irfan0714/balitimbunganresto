<?php
$mylib = new globallib();
$username = $this->session->userdata('username');

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_penggunaan_diskon/search_report/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Report_Penggunaan_Diskon.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td  width="30" style="vertical-align: middle; text-align: center;">No</td>
                    <td  style="vertical-align: middle; text-align: center;">Jenis Cards</td>
                    <td  style="vertical-align: middle; text-align: center;">Tanggal</td>
                    <td  style="vertical-align: middle; text-align: center;">No Struk</td>
                    <td  style="vertical-align: middle; text-align: center;">Total Nilai</td>
                    <td  style="vertical-align: middle; text-align: center;">Disc</td>
                    <td  style="vertical-align: middle; text-align: center;">Service Charge</td>
					<td  style="vertical-align: middle; text-align: center;">Tax</td>
					<td  style="vertical-align: middle; text-align: center;">Tunai</td>
					<td  style="vertical-align: middle; text-align: center;">Kredit</td>
					<td  style="vertical-align: middle; text-align: center;">Debit</td>
					<td  style="vertical-align: middle; text-align: center;">Total Bayar</td>
                    <td  style="vertical-align: middle; text-align: center;">Kembali</td>
                </tr>
			
			<?php
			    $no=1;
			        $total_nilai = 0;
			    	$disc_card = 0;
			    	$ttl_charge = 0;
			    	$tax= 0;
			    	$tunai = 0;
			    	$kkredit = 0;
			    	$kdebit = 0;
			    	$totalbayar = 0;
			    	$kembali = 0;
			    for($x=0;$x<count($hasil);$x++){
			    	if($hasil[$x]['NamaGroupDisc']==""){
						$nama = "NoName";
					}else{
						$nama = $hasil[$x]['NamaGroupDisc'];
					}
			    	?>
			        <tr>
					<td  width="30" style="vertical-align: middle; text-align: center;"><?php echo $no;?></td>
                    <td  style="vertical-align: middle; text-align: left;"><?php echo $nama;?></td>
                    <td  style="vertical-align: middle; text-align: center;"><?php echo $mylib->ubah_tanggal($hasil[$x]['Tanggal']);?></td>
                    <td  style="vertical-align: middle; text-align: center;"><?php echo $hasil[$x]['NoStruk'];?></td>
                    <td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['TotalNilai'];?></td>
                    <td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['DiscCard'];?></td>
                    <td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['Ttl_Charge'];?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['TAX'];?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['Tunai'];?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['KKredit'];?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['KDebit'];?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['TotalBayar'];?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $hasil[$x]['Kembali'];?></td>
                    </tr>
				<?php   $total_nilai += $hasil[$x]['TotalNilai'];
				    	$disc_card += $hasil[$x]['DiscCard'];
				    	$ttl_charge += $hasil[$x]['Ttl_Charge'];
				    	$tax+= $hasil[$x]['TAX'];
				    	$tunai += $hasil[$x]['Tunai'];
				    	$kkredit += $hasil[$x]['KKredit'];
				    	$kdebit += $hasil[$x]['KDebit'];
				    	$totalbayar += $hasil[$x]['TotalBayar'];
				    	$kembali += $hasil[$x]['Kembali'];
				    	$no++;
				    	}
			?>
			        <tr>
					<td  width="30" colspan="4" style="vertical-align: middle; text-align: center;">Total</td>
                    <td  style="vertical-align: middle; text-align: right;"><?php echo $total_nilai;?></td>
                    <td  style="vertical-align: middle; text-align: right;"><?php echo $disc_card;?></td>
                    <td  style="vertical-align: middle; text-align: right;"><?php echo $ttl_charge;?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $tax;?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $tunai;?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $kkredit;?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $kdebit;?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $totalbayar;?></td>
					<td  style="vertical-align: middle; text-align: right;"><?php echo $kembali;?></td>
                    </tr
		</table>
	</div>
	
</form>
	</div>
</div>
