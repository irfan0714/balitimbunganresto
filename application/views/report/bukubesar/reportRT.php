<p>
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/bukubesar/cari/"
      onsubmit="return false">
          <?php
          $mylib = new globallib();
          if ($excel == "excel") {
              header('Content-Type: application/vnd.ms-excel');
              header('Content-Disposition: attachment; filename="reportbukubesar.xls"');
          }
          if ($excel != "excel") {
              ?>
        <div style="margin-left:10px">
            <input name='submit' type='submit' value='export to excel' class="btn btn-success btn-sm"
                   onclick="$('#excel').val('excel');
                   document.getElementById('search').submit()">
<!--                               $('#search').submit()">-->
        </div>
        <?php
    }
    ?>
	<br>
	<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    <table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px"
           width="90%">
        <tr>
            <td nowrap colspan="<? count($judul) ?>"><strong><font face="Arial" size="2">Laporan Buku
                    Besar</font></strong></td>
        </tr>
        <?php
        for ($a = 0; $a < count($judul); $a++) {
            ?>
            <tr>
                <td nowrap><strong><font face="Arial" size="2"><?= $judul[$a] ?></font></strong></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br><br><br><br>
    <p></p>
    <table bgcolor="#CCCCCC" border="1" class="table table-bordered responsive table-hover">
        <tr>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Tanggal</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Jurnal</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Nomor Referensi</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Nomor Transaksi</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Keterangan</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Divisi</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Sub Divisi</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Debit</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Kredit</b></font></th>
            <th  bgcolor="#009490" style="vertical-align: middle; text-align: center;"><font color="white"><b>Saldo</b></font></th>
        </tr>
        <?php
        if (count($hasil) > 0) {
        	$prev_kdrekening = '';
        	$line = 1;
        	$saldoakhir = 0;
            foreach($hasil as $row){
            	if($line % 2 == 0){
					$bgcolor= '#fffff';
				}else{
					$bgcolor= '#e0e5e5';
				}
            	$kdrekening  = $row['KdRekening'];
            	$namarekening =$row['NamaRekening'];
            	$tgltransaksi = $row['TglTransaksi'];
            	$kodejurnal = $row['KodeJurnal'];
            	$noreferensi = $row['NoReferensi'];
            	$notransaksi = $row['NoTransaksi'];
            	$keterangan = $row['KeteranganDetail'];
            	$namadivisi = $row['NamaDivisi'];
            	$namasubdivisi = $row['NamaSubDivisi'];
            	$saldowal = $row['SaldoAwal'];
            	$debit = $row['Debit'];
            	$kredit = $row['Kredit'];
            	if($kdrekening != $prev_kdrekening){
            		if($prev_kdrekening != ''){
            		?>
						<tr>
							<td colspan="7" align='center' bgcolor="#42f4e8"><b><font size="3">Saldo Akhir &nbsp;</font></b></td>
				            <td bgcolor="#42f4e8">&nbsp;</td>
				            <td bgcolor="#42f4e8">&nbsp;</td>
				            <td nowrap align='right' bgcolor="#42f4e8"><b><font size="3"><?= $mylib->ubah_format($saldoakhir,0);?></font></b></td>
				        </tr>
				        <tr>
				        	<td colspan="10" bgcolor="#42f4e8">&nbsp;</td>
				        </tr>
				    <?php
				    	$line++;
					}
					$saldoakhir=$saldowal;
            		?>
            		<tr>
	                    <td bgcolor='yellow' colspan="10"><b><font face="Arial" size="3">
	                            <?= $row['KdRekening'] . ' - ' . $row['NamaRekening']; ?>   </font></b>
	                    </td>
                	</tr>
					<tr>
						<td colspan="7" align='center' bgcolor="#42f4e8"><b><font size="3">Saldo Awal  &nbsp;</font></b></td>
			            <td bgcolor="#42f4e8">&nbsp;</td>
			            <td bgcolor="#42f4e8">&nbsp;</td>
			            <td nowrap align='right' bgcolor="#42f4e8"><b><font size="3"><?= $mylib->ubah_format($saldowal,0);?></font></b></td>
			        </tr>
				<?php
					$line++;
					$prev_kdrekening = $kdrekening;
				}
				?>
				<tr style="cursor:pointer;" data-toggle="tooltip" data-placement="top" data-original-title="Klik Untuk Detail" onclick="click_detail('<?php echo $notransaksi; ?>','<?= $kodejurnal; ?>')">
                    <td bgcolor="<?=$bgcolor;?>" width="5%" align="center"><?=$mylib->ubah_format_tanggal($tgltransaksi)?></td>
                    <td bgcolor="<?=$bgcolor;?>" width="5%"><?= $kodejurnal; ?></td>
                    <td bgcolor="<?=$bgcolor;?>" width="5%"><?= $noreferensi; ?></td>
                    <td bgcolor="<?=$bgcolor;?>" width="8%"><?= $notransaksi; ?></td>
                    <td bgcolor="<?=$bgcolor;?>" width="30%"><?= $keterangan; ?></td>
                    <td bgcolor="<?=$bgcolor;?>" width="10%"><?= $namadivisi; ?></td>
                    <td bgcolor="<?=$bgcolor;?>" width="10%"><?= $namasubdivisi; ?></td>
                    <td align='right' width="8%" bgcolor="<?=$bgcolor;?>"><?= $mylib->ubah_format($debit,0) ?></td>
                    <td align='right' width="8%" bgcolor="<?=$bgcolor;?>"><?= $mylib->ubah_format($kredit,0) ?></td>
                    <td align='right' width="8%" bgcolor="<?=$bgcolor;?>">&nbsp;</td>
                </tr>
            <?php
            	$line++;
            	$saldoakhir += $debit-$kredit;
            }
            ?>
            <tr>
				<td colspan="7" align='center' bgcolor="#42f4e8"><b><font size="3">Saldo Akhir &nbsp;</font></b></td>
	            <td bgcolor="#42f4e8">&nbsp;</td>
	            <td bgcolor="#42f4e8">&nbsp;</td>
	            <td nowrap align='right' bgcolor="#42f4e8"><b><font size="3"><?= $mylib->ubah_format($saldoakhir,0);?></font></b></td>
	        </tr>
            <?php
        } else {
            ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="10"><b>Tidak ada data</b></td>
            </tr>
            <?php
        }
        ?>
    </table>
</form>

<script>
	function click_detail(id,jenis){
		pisah_id = id.substring(0,2);
		
		var urls=$('#base_url').val();
		if(jenis=="CB"){
			urlx= urls+"index.php/transaksi/debit_note/jurnal_edit_debit_note/"+id;
		}else if(jenis=="PV"){
			urlx= urls+"index.php/keuangan/payment/jurnal_edit_payment/"+id;
		}else if(jenis=="RV"){
			urlx= urls+"index.php/keuangan/receipt/jurnal_edit_receipt/"+id;
		}else if(jenis=="RUM"){
			urlx= urls+"index.php/keuangan/realisasi_uang_muka/jurnal_edit_realisasi_uang_muka/"+id;
		}else if(jenis=="PIM"){
			urlx= urls+"index.php/transaksi/pi_marketing/edit_jurnal_pi_marketing/"+id;
		}
		
		windowOpener(500, 1000, 'Detail Jurnal', urlx, 'Detail Jurnal')	
	}
</script>