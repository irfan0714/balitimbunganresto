<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Buku Besar";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
    
    function cari_rekening(p){
    	
    	if(p==1){
			var kdrekening = $('#v_kdrekening1').val();	
		}else{
			var kdrekening = $('#v_kdrekening2').val();	
		}
    	
    	$.ajax({
	         url: "<?=base_url();?>index.php/report/bukubesar/ajax_rekening/",
	         type: "POST",
	         async: true,
	         data: {KdRekening: kdrekening},
	         success: function(res)
				{
					
					if(p==1){
						$('#KdRekening1').html(res);
					}else{
						$('#KdRekening2').html(res);
					}
				},
				error: function(e) 
				{
					alert(e);
				} 
      	});
	}
    
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/bukubesar/cari/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Period</td>
	            <td> 
	            	<select id="bulan1" name="bulan1" class="form-control-new">
					<?php
					for($a = 0;$a<count($listbulan);$a++){
						$select = "";
						if($bulanaktif1==$listbulan[$a]){
							$select = "selected=true";
						}
					?>
						<option <?=$select;?> value= "<?=$listbulan[$a]?>"><?=$listbulan[$a]?></option>
					<?php
					}
					?>
					</select>
					&nbsp;&nbsp;s/d&nbsp;&nbsp;
					<select id="bulan2" name="bulan2" class="form-control-new">
					<?php
					for($a = 0;$a<count($listbulan);$a++){
						$select = "";
						if($bulanaktif2==$listbulan[$a]){
							$select = "selected=true";
						}
					?>
						<option <?=$select;?> value= "<?=$listbulan[$a]?>"><?=$listbulan[$a]?></option>
					<?php
					}
					?>
					</select>
					&nbsp;&nbsp;
					<select size="1" id="tahun1" name="tahun1" class="form-control-new">
					<?php
					for($a = 0;$a<count($listtahun);$a++){
						$select = "";
						if($tahunaktif==$listtahun[$a]['Tahun']){
							$select = "selected";
						}
					?>
						<option <?=$select;?> value= "<?=$listtahun[$a]['Tahun']?>"><?=$listtahun[$a]['Tahun']?></option>
					<?php
					}
					?>
					</select>
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Divisi</td>
	        	<td>
	        		<select id="divisi" name="divisi" class="form-control-new">
	        			<option value="">Semua</option>
					<?php
					for($a = 0;$a<count($listdivisi);$a++){
							if($divisi==$listdivisi[$a]['KdDivisi'])
								$varselect ='selected=true';
							else
								$varselect ='';
					?>
						<option <?=$varselect;?> value= "<?=$listdivisi[$a]['KdDivisi']?>"><?=$listdivisi[$a]['NamaDivisi']?></option>
					<?php
					}
					?>
					</select>
	        	</td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Dari Rekening</td>
	        	<td>
	        		<input type="text" class="form-control-new" style="width: 15%;" maxlength="30" name="v_kdrekening1" id="v_kdrekening1" placeholder="Cari Rekening" onkeyup="cari_rekening('1')">
          			<select width="200" class="form-control-new" name="KdRekening1" id="KdRekening1">
	            		<option value="">- Pilih Rekening -</option>
	            		<?php
	            		foreach($listrekening as $val)
	            		{
	            			if($rekeningaktif1==$val['KdRekening']){
								$selected = "selected=true";
							}else{
								
								$selected = "";
							}
							?><option <?=$selected;?> value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option><?php
						}
	            		?>
	            	</select>
				</td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Sampai Rekening</td>
	        	<td>
	        		<input type="text" class="form-control-new" style="width: 15%;" maxlength="30" name="v_kdrekening1" id="v_kdrekening2" placeholder="Cari Rekening" onkeyup="cari_rekening('2')">
          			<select width="200" class="form-control-new" name="KdRekening2" id="KdRekening2">
	            		<option value="">- Pilih Rekening -</option>
	            		<?php
	            		foreach($listrekening as $val)
	            		{
	            			if($rekeningaktif2==$val['KdRekening']){
								$selected = "selected=true";
							}else{
								
								$selected = "";
							}
							?><option <?=$selected;?> value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option><?php
						}
	            		?>
	            	</select>
				</td>
	        </tr>
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Submit<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT) 
{
	$this->load->view("report/bukubesar/reportRT");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>