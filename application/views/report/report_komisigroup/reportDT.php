<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_komisigroup/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportkomisigroup.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');$('#search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Komisi Group</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
		<th align="center" bgcolor="#f3f7bb">PCode</th>
		<th align="center" bgcolor="#f3f7bb">Nama Barang</th>
		<th align="center" bgcolor="#f3f7bb">Qty</th>
		<th align="center" bgcolor="#f3f7bb">Netto</th>
		<th align="center" bgcolor="#f3f7bb">Office</th>
		<th align="center" bgcolor="#f3f7bb">Guide</th>
		<th align="center" bgcolor="#f3f7bb">Tour Leader</th>
		<th align="center" bgcolor="#f3f7bb">Driver</th>
		<th align="center" bgcolor="#f3f7bb">Total Komisi</th>
	</tr>
<?php
	if(count($hasil)>0){
		$total1 = 0;
		$total2 = 0;
		$total3 = 0;
		$total4 = 0;
		$total5 = 0;
		$total6 = 0;
		for($s=0;$s<count($hasil);$s++)
		{
			$total1 += (float)$hasil[$s]['Netto'];
			$total2 += (float)$hasil[$s]['komisi1']/100*$hasil[$s]['Netto'];
			$total3 += (float)$hasil[$s]['komisi2']/100*$hasil[$s]['Netto'];
			$total4 += (float)$hasil[$s]['komisi3']/100*$hasil[$s]['Netto'];
			$total5 += (float)$hasil[$s]['komisi4']/100*$hasil[$s]['Netto'];
			$total6 += (float)$hasil[$s]['total']/100*$hasil[$s]['Netto'];
	?>
			<tr>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['PCode']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaLengkap']?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['Qty'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['Netto'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['komisi1']/100*$hasil[$s]['Netto'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['komisi2']/100*$hasil[$s]['Netto'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['komisi3']/100*$hasil[$s]['Netto'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['komisi4']/100*$hasil[$s]['Netto'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['total']/100*$hasil[$s]['Netto'])?></td>
			</tr>
	<?php
		}
	?>
		<tr>
			<td nowrap align='center' bgcolor='#f7d7bb' colspan="3"><b>Total</b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total1)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total2)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total3)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total4)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total5)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total6)?></b></td>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="9"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>