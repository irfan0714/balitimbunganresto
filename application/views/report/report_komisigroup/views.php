<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">

function loading(){
//	base_url = $("#baseurl").val();
//	$('#tgl1').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
}
function submitThis()
{
	$("#excel").val("");
	$("#print").val("");
	userlevel= $("#userlevel").val();
	tgl1 = $("#tgl1").val();
	yymmdd1 = tgl1.split('-');
	
	$("#search").submit();
}

</script>
<body onload="loading()">
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_komisigroup/cari/" onsubmit="return false">
<table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<?php
				echo $reportlib->write_textbox("Tanggal","tgl1",$tgl1,"15","15","","text","","1");			
				echo $reportlib->write_textbox("Sticker","nosticker",$nosticker,"4","4","","text","","1");
				echo $reportlib->write_plain_combo("Group","group",$listgroup,$group,"KdTravel","Keterangan",3);
				?>
			</table>
		</td>
	</tr>
	<tr></tr>
	<tr></tr>
	<tr bordercolor="#FFFFFF">
		<td colspan="2" align="center"><input type="button" value="Search (*)" onclick="submitThis()">
		<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
		<input type='hidden' value='<?=$userlevel?>' id="userlevel" name="userlevel">
		<input type='hidden' value='<?=$excel?>' id="excel" name="excel">
		<input type='hidden' value='<?=$print?>' id="print" name="print">
		</td>
	</tr>
</table>
</form>
</body>
<?php
if($tampilkanDT)
{
	$this->load->view("report/report_komisigroup/reportDT");
}
$this->load->view('footer'); ?>