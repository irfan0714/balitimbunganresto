<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
	function pop_up_pemakaian_voucher(v_date)
	{
		url = '<?= base_url()?>/index.php/report/daily_sales/voucher/';
	    try{
	        windowOpener(600, 800, 'Pemakaian Voucher', url+v_date, 'Pemakaian Voucher') ;   
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="dailysales.xls"');
    }
   
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    $arr_data  = array();
	$arr_total = array();
	
    for($i=$mylib->parsedate($v_date_from);$i<=$mylib->parsedate($v_date_to);$i=$i+86400)
    {
        $arr_data["list_date"][$i] = $i;
    }
    
    
    for($i=0;$i<count($ticket);$i++){
        $noticket = $ticket[$i]['noticket'];
        $add_date = $ticket[$i]['add_date'];
        $jenis = $ticket[$i]['jenis'];
        $harga = $ticket[$i]['harga'];  
          
        if($jenis==1){
            $arr_data['lokal'][$add_date]++;                   
        } elseif($jenis==2){
            $arr_data['asia'][$add_date]++;               
        }else{
			$arr_data['barat'][$add_date]++;               
		}
        
        $arr_data['all_ticket'][$add_date]++;
                                              
        $arr_data['harga_tiket'][$add_date] += $harga;
    }
    
    for($i=0;$i<count($voucher);$i++){
    	$Tanggal = $voucher[$i]['Tanggal'];
        $pemakaian_voucher = $voucher[$i]['pemakaian_voucher']; 
        
        $arr_data["pemakaian_voucher_ticket"][$Tanggal] = $pemakaian_voucher;
    }
    
    for($i=0;$i<count($salesrec);$i++){
        $Tanggal = $salesrec[$i]['Tanggal'];
        $KdDivisiReport = $salesrec[$i]['KdDivisiReport'];
        $NamaDivisReport = $salesrec[$i]['NamaDivisiReport'];
        $Sales = $salesrec[$i]['Sales'];
        
        $arr_data["total_TotalNilai"][$Tanggal] += $Sales;
        
        switch($KdDivisiReport){
			case 10:
			 	$arr_data["total_OH"][$Tanggal] += $Sales;
				break;
			case 20:
				$arr_data["total_The_Luwus"][$Tanggal] += $Sales; 
				break;
			case 21:
				$arr_data["total_Rice_View"][$Tanggal] += $Sales; 
				break;
			case 22:
				$arr_data["total_Juice_Corner"][$Tanggal] += $Sales; 
				break;
			case 23:
				$arr_data["total_Mini_Zoo"][$Tanggal] += $Sales; 
				break;
			case 24:
				$arr_data["total_Burger_Corner"][$Tanggal] += $Sales; 
				break;
			case 30:
				$arr_data["total_BE_Store"][$Tanggal] += $Sales;    
				break;
			case 31:
				$arr_data["total_BE_Bar"][$Tanggal] += $Sales;    
				break;	   
			case 32:
				$arr_data["total_BE_Class"][$Tanggal] += $Sales;    
				break;	   
			case 33:
				$arr_data["total_Gelato"][$Tanggal] += $Sales;    
				break;	   
			case 34:
				$arr_data["total_BE_Store"][$Tanggal] += $Sales;    
				break;	   
			case 35:
				$arr_data["total_BE_Bar"][$Tanggal] += $Sales;    
				break;	   
			case 36:
				$arr_data["total_BE_Sunset"][$Tanggal] += $Sales;    
			case 38:
				$arr_data["total_BES_Sunset"][$Tanggal] += $Sales;    
				break;	   
			case 39:
				$arr_data["total_BEC_TanahLot"][$Tanggal] += $Sales;    
				break;	   
			case 40:
				$arr_data["total_BES_TanahLot"][$Tanggal] += $Sales;    
				break;	   
			case 42:
				$arr_data["total_BEC_Discovery"][$Tanggal] += $Sales;    
				break;	   
			case 44:
				$arr_data["total_BES_Discovery"][$Tanggal] += $Sales;    
				break;	   
			case 43:
				$arr_data["total_BEC_Palem"][$Tanggal] += $Sales;    
				break;
			case 47:
				$arr_data["total_Bebek_Timbungan_Sunset"][$Tanggal] += $Sales; 
				break;
			default:
				$arr_data["total_Ngaco"][$Tanggal] += $Sales;   
				break;
		}
    }
    
    for($i=0;$i<count($komisi);$i++){
        $NoTransaksi = $komisi[$i]['NoTransaksi'];    
        $TglTransaksi = $komisi[$i]['TglTransaksi'];
        $Total = $komisi[$i]['Total'];
        
        $arr_data["total_komisi"][$TglTransaksi] += $Total;
    }
	//print_r($arr_data["total_komisi"]);
	
    for($i=0;$i<count($compliment);$i++){
        $NoStruk = $compliment[$i]['NoStruk'];    
        $Tanggal = $compliment[$i]['Tanggal'];    
        $Total = $compliment[$i]['total'];    
        
        $arr_data["total_compliment"][$Tanggal] += $Total;  
    }
    
    for($i=0;$i<count($distribusikopi);$i++){
        $Tanggal = $distribusikopi[$i]['sidate'];    
        $Total = $distribusikopi[$i]['nilai'];    
        
        $arr_data["total_distribusi_kopi"][$Tanggal] += $Total;  
    }
    
    for($i=0;$i<count($distribusioh);$i++){
        $Tanggal = $distribusioh[$i]['sidate'];    
        $Total = $distribusioh[$i]['nilai'];    
        
        $arr_data["total_distribusi_oh"][$Tanggal] += $Total;  
    }
    
    for($i=0;$i<count($distribusivci);$i++){
        $Tanggal = $distribusivci[$i]['sidate'];    
        $Total = $distribusivci[$i]['nilai'];    
        
        $arr_data["total_distribusi_oh"][$Tanggal] += $Total;  
    }
    
    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT DAILY '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
		        <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
		        <th rowspan="2" style="vertical-align: middle;">Hari</th>
		        <th style="vertical-align: middle; text-align: center;" colspan="5">Tiket</th>
		        <th style="vertical-align: middle; text-align: center;" colspan="20">Sales</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: right;">Komisi</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: right;">Pemakaian Voucher</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: right;">Compliment</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: right;">Omzet</th>
		    </tr>
	    
		    <tr>
		        <th style="text-align: right;">Lokal</th>
		        <th style="text-align: right;">Domestik</th>
		        <th style="text-align: right;">Asing</th>
		        <th style="text-align: right;">Jumlah Ticket</th>
		        <th style="text-align: right;">Total (Rp)</th>
		    
		        <th style="text-align: right;">OH</th>
		        <th style="text-align: right;">Dist OH</th>
		        <th style="text-align: right;">The Luwus</th>
		        <th style="text-align: right;">Rice View</th>
		        <th style="text-align: right;">BTS</th>
		        <th style="text-align: right;">Juice</th>
		        <th style="text-align: right;">Burger</th>
		        <th style="text-align: right;">Mecanda</th>
		        <th style="text-align: right;">BE Store</th>
		        <th style="text-align: right;">BE Bar</th>
		        <th style="text-align: right;">BE Class</th>
		        <th style="text-align: right;">Gelato</th>
		        <th style="text-align: right;">BEC Sunset</th>
		        <th style="text-align: right;">BES Sunset</th>
		        <th style="text-align: right;">BEC Tanah Lot</th>
		        <th style="text-align: right;">BES Tanah Lot</th>
		        <th style="text-align: right;">BEC Discovery</th>
		        <th style="text-align: right;">BES Discovery</th>
		        <th style="text-align: right;">BEC Palem</th>
		        <th style="text-align: right;">Dist Kopi</th>
		        <th style="text-align: right;">Total (Rp)</th>
		    </tr>
		</thead>

		<tbody>
		';

    $no = 1; 
    foreach($arr_data["list_date"] as $date=>$val)
    {
        $pemakaian_voucher_ticket = $arr_data["pemakaian_voucher_ticket"][date("Y-m-d", $date)];
        $TotalNilai = $arr_data["total_TotalNilai"][date("Y-m-d", $date)];
        
        $total_OH = $arr_data["total_OH"][date("Y-m-d", $date)];
        $total_distribusi_OH = $arr_data["total_distribusi_oh"][date("Y-m-d", $date)];
        $total_The_Luwus = $arr_data["total_The_Luwus"][date("Y-m-d", $date)];
        $total_Rice_View = $arr_data["total_Rice_View"][date("Y-m-d", $date)];
        $total_Bebek_Timbungan_Sunset =  $arr_data["total_Bebek_Timbungan_Sunset"][date("Y-m-d", $date)];
        $total_Juice_Corner = $arr_data["total_Juice_Corner"][date("Y-m-d", $date)];
        $total_Mini_Zoo = $arr_data["total_Mini_Zoo"][date("Y-m-d", $date)];
		$total_Burger_Corner = $arr_data["total_Burger_Corner"][date("Y-m-d", $date)];
        $total_BE_Bar = $arr_data["total_BE_Bar"][date("Y-m-d", $date)];
        $total_BE_Store = $arr_data["total_BE_Store"][date("Y-m-d", $date)];
        $total_BE_Class = $arr_data["total_BE_Class"][date("Y-m-d", $date)];
        $total_Gelato = $arr_data["total_Gelato"][date("Y-m-d", $date)];
        $total_BE_Sunset = $arr_data["total_BE_Sunset"][date("Y-m-d", $date)];
        $total_BES_Sunset = $arr_data["total_BES_Sunset"][date("Y-m-d", $date)];
        $total_BEC_TanahLot = $arr_data["total_BEC_TanahLot"][date("Y-m-d", $date)];
        $total_BES_TanahLot = $arr_data["total_BES_TanahLot"][date("Y-m-d", $date)];
        $total_BEC_Discovery = $arr_data["total_BEC_Discovery"][date("Y-m-d", $date)];
        $total_BES_Discovery = $arr_data["total_BES_Discovery"][date("Y-m-d", $date)];
        $total_BEC_Palem = $arr_data["total_BEC_Palem"][date("Y-m-d", $date)];
        $total_distribusi_kopi = $arr_data["total_distribusi_kopi"][date("Y-m-d", $date)];
		$TotalNilai += $total_distribusi_kopi+$total_distribusi_OH;
		//$total_LR = 0;
  
        $lokal = $arr_data["lokal"][date("Y-m-d", $date)];
        $asia = $arr_data["asia"][date("Y-m-d", $date)];
        $barat = $arr_data["barat"][date("Y-m-d", $date)];
        $all_ticket = $arr_data["all_ticket"][date("Y-m-d", $date)];
        $harga_tiket = $arr_data["harga_tiket"][date("Y-m-d", $date)];
        //$pemakaian_voucher_ticket = 0;
        $nett_ticket = $harga_tiket;
        
        
        $komisi = $arr_data["total_komisi"][date("Y-m-d", $date)];
        $compliment = $arr_data["total_compliment"][date("Y-m-d", $date)];
        
        $omzet = ($TotalNilai) + $nett_ticket - $pemakaian_voucher_ticket;
        
        $bg_hari ="";
        if(date("l", $date)=="Saturday" || date("l", $date)=="Sunday")
        {
            $bg_hari = "background: #ff99cc;";
        }

    	$echo .= '
    	<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td>'.date("d/m/Y", $date).'</td>
	        <td style="'.$bg_hari.'">'.date("l", $date).'</td>
	        <td align="right">'.$mylib->format_number($lokal, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($asia, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($barat, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($all_ticket, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($nett_ticket, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_OH, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_distribusi_OH, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_The_Luwus, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_Rice_View, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_Bebek_Timbungan_Sunset, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_Juice_Corner, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_Burger_Corner, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_Mini_Zoo, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BE_Store, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BE_Bar, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BE_Class, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_Gelato, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BE_Sunset, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BES_Sunset, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BEC_TanahLot, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BES_TanahLot, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BEC_Discovery, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BES_Discovery, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BEC_Palem, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_distribusi_kopi, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($TotalNilai, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($komisi, 0, "", "", "ind").'</td>
	        <td align="right"><a class="link_pop" href="javascript:void(0)" onclick="pop_up_pemakaian_voucher(&quot;'.date("d/m/Y", $date).'&quot;)">'.$mylib->format_number($pemakaian_voucher_ticket, 0, "", "", "ind").'</a></td>
	        <td align="right">'.$mylib->format_number($compliment, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($omzet, 0, "", "", "ind").'</td>
	    </tr>
    	';
        
        $arr_total["lokal"] += $lokal;
        $arr_total["asia"] += $asia;
        $arr_total["barat"] += $barat;
        $arr_total["all_ticket"] += $all_ticket;
        $arr_total["nett_ticket"] += $nett_ticket;
        $arr_total["OH"] += $total_OH;
        $arr_total["The_Luwus"] += $total_The_Luwus;
        $arr_total["Rice_View"] += $total_Rice_View;
        $arr_total["Bebek_Timbungan_Sunset"] += $total_Bebek_Timbungan_Sunset;
        $arr_total["Juice_Corner"] += $total_Juice_Corner;
        $arr_total["Burger_Corner"] += $total_Burger_Corner;
        $arr_total["Mini_Zoo"] += $total_Mini_Zoo;
        $arr_total["BE_Store"] += $total_BE_Store;
        $arr_total["BE_Bar"] += $total_BE_Bar;
        $arr_total["BE_Class"] += $total_BE_Class;
        $arr_total["Gelato"] += $total_Gelato;
        $arr_total["Sunset"] += $total_BE_Sunset;
        $arr_total["BES_Sunset"] += $total_BES_Sunset;
        $arr_total["BEC_TanahLot"] += $total_BEC_TanahLot;
        $arr_total["BES_TanahLot"] += $total_BES_TanahLot;
        $arr_total["BEC_Discovery"] += $total_BEC_Discovery;
        $arr_total["BES_Discovery"] += $total_BES_Discovery;
        $arr_total["BEC_Palem"] += $total_BEC_Palem;
        $arr_total["distribusi_kopi"] += $total_distribusi_kopi;
        $arr_total["distribusi_OH"] += $total_distribusi_OH;
        $arr_total["Nilai"] += $TotalNilai;
        $arr_total["Komisi"] += $komisi;
        $arr_total["pemakaian_voucher"] += $pemakaian_voucher_ticket;
        $arr_total["Compliment"] += $compliment;
        $arr_total["Omzet"] += $omzet;

        $no++;
    }
    
	$echo .= '
		</tbody>';
	    
	$echo .= '
         <tfoot>
            <tr style="text-align: right; font-weight: bold; color: black;">
                <td colspan="3">Grand Total</td>
                <td align="right">'.$mylib->format_number($arr_total["lokal"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["asia"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["barat"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["all_ticket"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["nett_ticket"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["OH"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["distribusi_OH"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["The_Luwus"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Rice_View"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Bebek_Timbungan_Sunset"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Juice_Corner"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Burger_Corner"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Mini_Zoo"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BE_Store"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BE_Bar"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BE_Class"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Gelato"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Sunset"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BES_Sunset"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BEC_TanahLot"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BES_TanahLot"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BEC_Discovery"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BES_Discovery"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BEC_Palem"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["distribusi_kopi"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Nilai"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Komisi"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["pemakaian_voucher"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Compliment"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Omzet"], 0, "", "", "ind").'</td>
            </tr>
        </tfoot> 
	    ';

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Report Daily ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>