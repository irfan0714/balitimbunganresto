<?php
$mylib = new globallib();
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>GL NPM</title>
    <link rel="shortcut icon" href="<?= base_url();?>public/images/Logosg.png" >
    
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
    
    <script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>

</head>
<body class="page-body skin-black" >
<form>
<div class="page-container sidebar-collapsed">
	
	
	<div class="main-content">
		<div class="row">
              <table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30">No</td>
                    <td>No Voucher</td>
                    <td>Nominal</td>
                    <td>Pemakaian</td>
                </tr>
                
                <tbody style="color: black;">
                
                <?php           
                    $no = 1;
                    $totnominal = 0;
                    $totterpakai = 0;
                    for($i=0;$i<count($voucher);$i++){
                    	$novoucher = $voucher[$i]['novoucher'];
                    	$nominal = $voucher[$i]['nominal'];
                    	$terpakai = $voucher[$i]['terpakai'];
                        
                        $bgcolor = "";
                        if($nominal!=$terpakai)
                        {
                            $bgcolor = "yellow";
                        }
                        
                        ?>
                        <tr>
                            <td bgcolor="<?php echo $bgcolor; ?>"><?php echo $no; ?></td>
                            <td bgcolor="<?php echo $bgcolor; ?>"><?php echo $novoucher; ?></td>
                            <td bgcolor="<?php echo $bgcolor; ?>" style="text-align: right"><?php echo $mylib->format_number($nominal, 0, ",", ".", "ind"); ?></td>
                            <td bgcolor="<?php echo $bgcolor; ?>" style="text-align: right"><?php echo $mylib->format_number($terpakai, 0, ",", ".", "ind"); ?></td>
                        </tr>
                        <?php
                        
                        $totnominal += $nominal;
                        $totterpakai += $terpakai;
                        $no++;
                    }
                ?>
                </tbody>
                
                <tr style="text-align: right;">
                    <td colspan="2">TOTAL</td>
                    <td style="text-align: right"><?php echo $mylib->format_number($totnominal, 0, "", "", "ind"); ?></td>
                    <td style="text-align: right"><?php echo $mylib->format_number($totterpakai, 0, ",", ".", "ind"); ?></td>
                </tr>
                
                
              </table>
       	
       	</div>
</form>		
</body>
</html>