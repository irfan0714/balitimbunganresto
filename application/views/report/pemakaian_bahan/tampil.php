<?php
$mylib = new globallib();

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_pemakaian_bahan/search_report/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="pemakaianbahan.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">No. Formula</td>
                <td style="vertical-align: middle; text-align: center;">Nama Formula</td>
                <td style="vertical-align: middle; text-align: center;">Batchnumber</td>
                <td style="vertical-align: middle; text-align: center;">Rosting Qty</td>
                <td style="vertical-align: middle; text-align: center;">Qty</td>
                <td style="vertical-align: middle; text-align: center;">QC</td>
                <td style="vertical-align: middle; text-align: center;">Reject</td></td>
                <td style="vertical-align: middle; text-align: center;">Berat</td></td>
                <td style="vertical-align: middle; text-align: center;">Waste</td></td>
                <td style="vertical-align: middle; text-align: center;">%</td></td>
                <td style="vertical-align: middle; text-align: center;">PCode</td></td>
                <td style="vertical-align: middle; text-align: center;">NamaBarang</td></td>
                <td style="vertical-align: middle; text-align: center;">MR No</td>
                <td style="vertical-align: middle; text-align: center;">Qty</td></td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$prev_formula = '';			
			$prev_batchnumber = '';		
			for($i=0;$i<count($hasil);$i++)
			{		
				$formulanumber = $hasil[$i]['formulanumber'];
				$formulaname = $hasil[$i]['formulaname'];
				$batchnumber = $hasil[$i]['batchnumber'];
				$mrno = $hasil[$i]['MRNo'];
				$MixQty = $hasil[$i]['MixQty'];
				$Qty = $hasil[$i]['Qty'];
				$QtyQC = $hasil[$i]['QtyQC'];
				$fgwgt = $hasil[$i]['fgwgt'];
				$QtyReject = $hasil[$i]['QtyReject'];
				$persentase = $fgwgt/$MixQty*100;
				$inventorycode = $hasil[$i]['inventorycode'];
				$namalengkap = $hasil[$i]['NamaLengkap'];
				$quantity = $hasil[$i]['quantity'];
				$waste = $MixQty - $fgwgt;
                
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
				<?php
					if($prev_formula == $formulanumber && $prev_batchnumber==$batchnumber){
						$formulanumber_echo = '';
						$formulaname_echo = '';
						$batchnumber_echo = '';
						$MixQty_echo = '';
						$Qty_echo = '';
						$QtyQC_echo = '';
						$QtyReject_echo = '';
						$persentase_echo = '';
						$fgwgt_echo = '';
						$waste_echo ='';
					}else{
						$formulanumber_echo = $formulanumber;
						$batchnumber_echo = $batchnumber;
						$formulaname_echo = $formulaname;
						$MixQty_echo = ubah_format($MixQty);
						$Qty_echo = ubah_format($Qty);
						$QtyQC_echo = ubah_format($QtyQC);
						$QtyReject_echo = ubah_format($QtyReject);
						$persentase_echo = ubah_format($persentase);
						$fgwgt_echo = ubah_format($fgwgt);
						$waste_echo = ubah_format($waste);
					}
				?>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>"><?php echo $formulanumber_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>"><?php echo $formulaname_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>"><?php echo $batchnumber_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $MixQty_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $Qty_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $QtyQC_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $QtyReject_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $fgwgt_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $waste_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo $persentase_echo; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>"><?php echo $inventorycode; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>"><?php echo $namalengkap; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>"><?php echo $mrno; ?></td>
					<td nowrap title="<?php echo $formulanumber." :: ".$formulaname; ?>" style="text-align: right;"><?php echo ubah_format($quantity); ?></td>
				</tr>
			<?php
				$prev_formula = $formulanumber;
				$prev_batchnumber = $batchnumber;
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
