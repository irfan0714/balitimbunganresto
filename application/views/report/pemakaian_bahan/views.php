<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Pemakaian Bahan";
?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_pemakaian_bahan/search_report/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        <tr>
	            <td class="title_table" width="150">Tanggal</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date1; ?>" name="v_date1" id="v_date1" size="10" maxlength="10">
					s/d	            	
					<input type="text" class="form-control-new datepicker" value="<?php echo $v_date2; ?>" name="v_date2" id="v_date2" size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Gudang</td>
	        	<td>
	        		<select id="gudang" name="gudang">
                    	<option value=''>--Pilih--</option>
                        <?php
                        for ($a = 0; $a < count($listgudang); $a++) {
                            $select = "";
                            if ($gudang == $listgudang[$a]['KdGudang']) {
                                $select = "selected";
                            }
                            ?>
                            <option <?= $select; ?> value="<?= $listgudang[$a]['KdGudang'] ?>"><?= $listgudang[$a]['Keterangan'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
	        	</td>
	        </tr>
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if ($tampilkan){
	$this->load->view("report/pemakaian_bahan/tampil", $data);
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>