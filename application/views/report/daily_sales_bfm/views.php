<?php
$this->load->view('header');
$reportlib = new report_lib();

if($jenis=='A')
	$modul = "Penjualan Harian Bali Fish Market";
else
	$modul = "Penjualan Harian Non Stiker"

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/daily_sales_bfm/cari/">       
        	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            	<table class="table table-bordered responsive">
                	<tr>
                    	<td width="100">Tanggal</td>
                    	<td>: 
                        	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                        	s/d
                        	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                    	</td>
                	</tr>
                	<tr style="display: none;">
                		<td>
                			Jenis
                		</td>
                		<td>:
                    		<select id="jenis" name="jenis">
                    			<option <?= $jenis=='A' ? 'Selected' : '' ;?> value="A">Semua</option>
                    			<option <?= $jenis=='N' ? 'Selected' : '' ;?> value="N">Non Stiker</option>
							</select>
						</td>
                	</tr>
                	<tr>
                    	<td>&nbsp;</td>
                    	<td>
                        	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                        	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                    	</td>
                	</tr>
            	</table> 
            </div>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT) 
{
   	$this->load->view("report/daily_sales_bfm/reportRT");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>