<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
	function pop_up_pemakaian_voucher(v_date)
	{
		url = '<?= base_url()?>/index.php/report/daily_sales/voucher/';
	    try{
	        windowOpener(600, 800, 'Pemakaian Voucher', url+v_date, 'Pemakaian Voucher') ;   
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="dailysales.xls"');
    }
   
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    $arr_data  = array();
	$arr_total = array();
	
    for($i=$mylib->parsedate($v_date_from);$i<=$mylib->parsedate($v_date_to);$i=$i+86400)
    {
        $arr_data["list_date"][$i] = $i;
    }
    
    
    for($i=0;$i<count($guest);$i++){
        $Tanggal = $guest[$i]['Tanggal'];  
        $JmlGuest = $guest[$i]['Guest'];
        $JmlDiscount = $guest[$i]['Discount'];
        $JmlService_Charge = $guest[$i]['Service_Charge'];
        $JmlTAX = $guest[$i]['TAX'];
        
        $arr_data["total_guest"][$Tanggal] += $JmlGuest; 
        $arr_data["total_discount"][$Tanggal] += $JmlDiscount; 
        $arr_data["total_service"][$Tanggal] += $JmlService_Charge; 
        $arr_data["total_tax"][$Tanggal] += $JmlTAX; 
        
    }
    
    for($i=0;$i<count($struck);$i++){
        $Tanggal = $struck[$i]['Tanggal'];  
        $JmlStruk = $struck[$i]['Struk'];
        $arr_data["total_struk"][$Tanggal] += $JmlStruk; 
    }
    
    for($i=0;$i<count($bruto);$i++){
        $Tanggal = $bruto[$i]['Tanggal'];  
        $JmlBruto = $bruto[$i]['Bruto'];
        $arr_data["total_bruto"][$Tanggal] += $JmlBruto; 
    }
    
    for($i=0;$i<count($salesrec);$i++){
        $Tanggal = $salesrec[$i]['Tanggal'];
        $KdDivisiReport = $salesrec[$i]['KdDivisiReport'];
        $NamaDivisReport = $salesrec[$i]['NamaDivisiReport'];
        $Sales = $salesrec[$i]['Sales'];
        
        $arr_data["total_TotalNilai"][$Tanggal] += $Sales;
        
        switch($KdDivisiReport){
			case 10:
			 	$arr_data["total_Netto"][$Tanggal] += $Sales;
				break;
		}
    }
	
    
    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold; border-collapse: collapse;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT DAILY '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
		        <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
		        <th rowspan="2" style="vertical-align: middle;">Hari</th>
		        <th rowspan="2" style="vertical-align: middle;">Guest</th>
		        <th rowspan="2" style="vertical-align: middle;">Struk</th>
		        <th rowspan="2" style="vertical-align: middle;">Bruto</th>
		        <th rowspan="2" style="vertical-align: middle;">Discount</th>
		        <th rowspan="2" style="vertical-align: middle;">Netto</th>
		        <th rowspan="2" style="vertical-align: middle;">Service Charge</th>
		        <th rowspan="2" style="vertical-align: middle;">Tax</th>
		        <th rowspan="2" style="vertical-align: middle;">Total</th>
		        <th rowspan="2" style="vertical-align: middle;">Rata2 Per Struk</th>
		        <th rowspan="2" style="vertical-align: middle;">Rata2 Per Pax Guest</th>
		    </tr>
		</thead>

		<tbody>
		';

    $no = 1; 
    foreach($arr_data["list_date"] as $date=>$val)
    {
        $total_Netto = $arr_data["total_Netto"][date("Y-m-d", $date)];
        
		//$total_LR = 0;
        
        $Total_Guest = $arr_data["total_guest"][date("Y-m-d", $date)];
        $Total_Struk = $arr_data["total_struk"][date("Y-m-d", $date)];
        $Total_Discount = $arr_data["total_discount"][date("Y-m-d", $date)];
        $Total_Service = $arr_data["total_service"][date("Y-m-d", $date)];
        $Total_Tax = $arr_data["total_tax"][date("Y-m-d", $date)];
        $Total_Bruto = $arr_data["total_bruto"][date("Y-m-d", $date)];
        //$pemakaian_voucher_ticket = 0;
        
        $bg_hari ="";
        if(date("l", $date)=="Saturday" || date("l", $date)=="Sunday")
        {
            $bg_hari = "background: #a0e2e0;";
        }

    	$echo .= '
    	<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td>'.date("d/m/Y", $date).'</td>
	        <td style="'.$bg_hari.'">'.date("l", $date).'</td>
	        <td align="right">'.$mylib->format_number($Total_Guest, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($Total_Struk, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($Total_Bruto+$Total_Discount, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($Total_Discount, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($Total_Bruto, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($Total_Service, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($Total_Tax, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number(($Total_Bruto)+$Total_Service+$Total_Tax, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number((($Total_Bruto)+$Total_Service+$Total_Tax)/$Total_Struk, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number((($Total_Bruto)+$Total_Service+$Total_Tax)/$Total_Guest, 0, "", "", "ind").'</td>
	    </tr>
    	';
        
        $arr_total["SpendingStruk"] += (($Total_Bruto)+$Total_Service+$Total_Tax)/$Total_Struk;
        $arr_total["SpendingGuest"] += (($Total_Bruto)+$Total_Service+$Total_Tax)/$Total_Guest;
        $arr_total["Guest"] += $Total_Guest;
        $arr_total["Struk"] += $Total_Struk;
        $arr_total["Discount"] += $Total_Discount;
        $arr_total["Service"] += $Total_Service;
        $arr_total["Tax"] += $Total_Tax;
        $arr_total["Bruto"] += $Total_Bruto+$Total_Discount;   
        $arr_total["Netto"] += $Total_Bruto;
        $arr_total["Total"] += ($Total_Bruto)+$Total_Service+$Total_Tax;

        $no++;
    }
    
	$echo .= '
		</tbody>';
	    
	$echo .= '
         <tfoot>
            <tr style="text-align: right; font-weight: bold; color: black;">
                <td colspan="3">Grand Total</td>
                <td align="right">'.$mylib->format_number($arr_total["Guest"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Struk"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Bruto"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Discount"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Netto"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Service"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Tax"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Total"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["SpendingStruk"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["SpendingGuest"], 0, "", "", "ind").'</td>
            </tr>
        </tfoot> 
	    ';

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Report Daily ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>