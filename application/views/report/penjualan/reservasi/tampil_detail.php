<?php

unset($arr_data);
$arr_data = array();
$arr_data["list_reservasi"] = array();

foreach($hasil as $val)
{//
	$arr_data["list_reservasi"][$val["NoReservasi"]]=$val["NoReservasi"];	
	$arr_data["TglKonfirmasi"][$val["NoReservasi"]]=$val["TglKonfirmasi"];
	$arr_data["NoSticker"][$val["NoReservasi"]]=$val["NoSticker"];
	
	$arr_data["totalnilai"][$val["NoReservasi"]]=$val["TotalNilai"];
	$arr_data["totalbayar"][$val["NoReservasi"]]=$val["TotalBayar"];
	$arr_data["voucher"][$val["NoReservasi"]]=$val["Voucher"];
	$arr_data["disc"][$val["NoReservasi"]]=$val["Discount"];
	$arr_data["item"][$val["NoReservasi"]]=$val["TotalItem"];
	
	$arr_data["list_reservasi_detail"][$val["NoReservasi"]][$val["PCode"]]=$val["PCode"];
	
	$arr_data["NamaLengkap"][$val["NoReservasi"]][$val["PCode"]]=$val["NamaLengkap"];
	$arr_data["qty"][$val["NoReservasi"]][$val["PCode"]]=$val["Qty"];
	$arr_data["harga"][$val["NoReservasi"]][$val["PCode"]]=$val["Harga"];
	$arr_data["bruto"][$val["NoReservasi"]][$val["PCode"]]=$val["Bruto"];
	$arr_data["discountdetail"][$val["NoReservasi"]][$val["PCode"]]=$val["DiscDetail"];
	$arr_data["netto"][$val["NoReservasi"]][$val["PCode"]]=$val["Netto"];
	$arr_data["service_charge"][$val["NoReservasi"]][$val["PCode"]]=$val["Service_charge"];
	$arr_data["ppn"][$val["NoReservasi"]][$val["PCode"]]=$val["PPN"];
}
?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_reservasi/search_report/" onsubmit="return false"/>
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportReservasi_detail.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" style="text-align: center;">No</td>
                    <td style="text-align: center;">NoReservasi</td>
                    <td style="text-align: center;">Tanggal</td>
                    <td style="text-align: center;">NoSticker</td>
                    
                    <td style="text-align: center;">PCode</td>
                    <td style="text-align: center;">Nama</td>
					<td style="text-align: center;">Qty</td>
                    <td style="text-align: center;">Harga</td>
                    <td style="text-align: center;">Bruto</td>
					<td style="text-align: center;">Disc</td>
					<td style="text-align: center;">Netto</td>
					<td style="text-align: center;">Charge</td>
					<td style="text-align: center;">Tax</td>
					<td style="text-align: center;">Total</td>
                </tr>            
			<tbody>
			
			<?php
			if(count($arr_data["list_reservasi"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			
			$tempnoreservasi = "";
			
			foreach($arr_data["list_reservasi"] as $noreservasi => $val)
			{		
				//echo $noreservasi;
				$TglDokumen = $arr_data["TglKonfirmasi"][$noreservasi];
				$NoSticker = $arr_data["NoSticker"][$noreservasi];

				$totalnilai = $arr_data["totalnilai"][$nostruk];
				$totalbayar = $arr_data["totalbayar"][$nostruk];
				$voucher = $arr_data["voucher"][$nostruk];
				$disc = $arr_data["disc"][$nostruk];
				$totalitem = $arr_data["item"][$nostruk];
			
				$no=1;
				$nomor=1;
				foreach($arr_data["list_reservasi_detail"][$noreservasi] as $pcode => $val)
				{
					//echo $noreservasi;
					$namalengkap = $arr_data["NamaLengkap"][$noreservasi][$pcode];
					$qty = $arr_data["qty"][$noreservasi][$pcode];
					$harga = $arr_data["harga"][$noreservasi][$pcode];
					$bruto = $arr_data["bruto"][$noreservasi][$pcode];
					$discountdetail = $arr_data["discountdetail"][$noreservasi][$pcode];
					$netto = $arr_data["netto"][$noreservasi][$pcode];
					
					$service_charge = $arr_data["service_charge"][$noreservasi][$pcode];
					$ppn = $arr_data["ppn"][$noreservasi][$pcode];
					
					$charge = $netto * ($service_charge/100);
					$tax = round(($netto * ($ppn/100)) + ($charge * ($ppn/100)));
					
					if($service_charge>0){
						$total = $netto + $charge + $tax;
					}
					else{
							$total = $netto + $tax;
					}
					$disc_all = $bruto - $netto;
					
				if($tempnoreservasi!="" and $tempnoreservasi!=$noreservasi){
	
				?>
					<tr bgcolor='#e6e6e6'>
						<td colspan="6" align="center"><b>Total</b>&nbsp;</td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
						<td align="right"><b><?php echo ubah_format($arr_data["t_disc"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_netto"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_charge"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tax"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_total"]); ?></b></td>
					</tr>		
				<?
					$arr_data["t_qty"] = 0;
					//$arr_data["t_harga"] = 0;
					$arr_data["t_bruto"] = 0;
					$arr_data["t_disc"] = 0;
					$arr_data["t_netto"] = 0;
					$arr_data["t_charge"] = 0;
					$arr_data["t_tax"] = 0;
					$arr_data["t_total"] = 0;
				}		
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $TglDokumen." :: ".$noreservasi; ?>"><?php echo $no; ?></td>
					<td nowrap title="<?php echo $TglDokumen." :: ".$noreservasi; ?>"><?php if($tempnoreservasi!=$noreservasi){echo $noreservasi; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $TglDokumen." :: ".$noreservasi; ?>" align="center"><?php if($tempnoreservasi!=$noreservasi){echo $mylib->ubah_tanggal($TglDokumen); }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $TglDokumen." :: ".$noreservasi; ?>" align="center"><?php if($tempnoreservasi!=$noreservasi){echo $NoSticker; }else{ echo "&nbsp;";} ?></td>
					
					<td nowrap title="PCode = <?php echo $pcode; ?>" align="left" style="mso-number-format: '\@'"><?php echo $pcode; ?></td>
					<td nowrap title="Nama = <?php echo $namalengkap; ?>" align="left"><?php echo $namalengkap; ?></td>
					<td nowrap title="Qty = <?php echo ubah_format2($qty); ?>" align="right"><?php echo ubah_format2($qty); ?></td>
					<td nowrap title="Harga = <?php echo ubah_format($harga); ?>" align="right"><?php echo ubah_format($harga); ?></td>
					<td nowrap title="Bruto = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Disc = <?php echo ubah_format($disc_all); ?>" align="right"><?php echo ubah_format($disc_all); ?></td>					
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>				
					<td nowrap title="Charge = <?php echo ubah_format($charge); ?>" align="right"><?php echo ubah_format($charge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($tax); ?>" align="right"><?php echo ubah_format($tax); ?></td>
					<td nowrap title="Total = <?php echo ubah_format($total); ?>" align="right"><?php echo ubah_format($total); ?></td>					
				</tr>
				<?php
				
				$arr_data["t_qty"] += $qty;
				//$arr_data["t_harga"] += $harga;
				$arr_data["t_bruto"] += $bruto;
				$arr_data["t_disc"] += $disc_all;
				$arr_data["t_netto"] += $netto;
				$arr_data["t_charge"] += $charge;
				$arr_data["t_tax"] += $tax;
				$arr_data["t_total"] += $total;
	
				$arr_data["gt_qty"] += $qty;
				//$arr_data["gt_harga"] += $harga;
				$arr_data["gt_bruto"] += $bruto;
				$arr_data["gt_disc"] += $disc_all;
				$arr_data["gt_netto"] += $netto;
				$arr_data["gt_charge"] += $charge;
				$arr_data["gt_tax"] += $tax;
				$arr_data["gt_total"] += $total;
				

				$no++;
				$tempnoreservasi = $noreservasi;				
				}
			}
			?>
				<tr bgcolor='#e6e6e6'>
					<td colspan="6" align="center"><b>Total</b>&nbsp;</td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($arr_data["t_disc"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_netto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_charge"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_tax"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_total"]); ?></b></td>
				</tr>	
			<?php
			if(count($arr_data["list_reservasi"])>0)
			{
			?>
				<tr class="title_table">
					<td colspan="6" align="center"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_bruto"]); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_disc"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_netto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_charge"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_tax"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_total"]); ?></b></td>
				</tr>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}

function ubah_format2($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>