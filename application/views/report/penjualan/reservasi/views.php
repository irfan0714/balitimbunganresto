<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Penjualan Reservasi";

if($v_pilihan=="transaksi"){
	$check1 = "checked";
	$check2 = "";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}
elseif($v_pilihan=="detail"){
	$check1 = "";
	$check2 = "checked";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}

elseif($v_pilihan=="barang"){
	$check1 = "";
	$check2 = "";
	$check3 = "checked";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}
else{
	$check1 = "checked";
	$check2 = "";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}

if($nostiker=="Y"){
	$yups = "checked";
}

foreach($mkasir as $val)
{
	$arr_data["kasir"][$val["UserName"]] = $val["UserName"];
    $arr_data["employee_name"][$val["UserName"]] = $val["employee_name"];
}

foreach($mreservasifromheader as $val)
{
	$arr_data["kasir"][$val["Kasir"]] = $val["Kasir"];
    $arr_data["employee_name"][$val["Kasir"]] = $val["employee_name"];
}

foreach($arr_data["kasir"] as $kasir => $val)
{
	if($userlevel==-1 || $userlevel==13 || $userlevel==15 )
	{
		$arr_data["all_kasir"][$kasir]=$kasir;
	}
	else
	{
		$arr_data["all_kasir"][$username] = $kasir;
	}
}

//echo "NAMA : $username";

?>

<script>

    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }

</script>

<div class="row">
    <div class="col-md-12" align="left">

    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>

		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_reservasi/search_report/" onsubmit="return false">

	    <table class="table table-bordered responsive">

			<tr>
	        	<td class="title_table" width="150">Rekap Per-</td>
	        	<td>
                    <div class="radio" style="float:left; width:100px; margin-top:0px;" >
                        <label><input type="radio" name="v_pilihan" id="v_pilihan"  value='transaksi' <? echo $check1;?> /> Transaksi</label>
                    </div>
                    <div class="radio" style="float:left;  width:100px; margin-top:0px;">
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='detail' <? echo $check2;?> /> Detail</label>
					 </div>
                     <!--<div class="radio" style="float:left;  width:100px; margin-top:0px;">
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='barang' <? echo $check3;?> /> Barang</label>
	        	     </div>	-->				 
                </td>
	        </tr>

	        <tr>
	            <td class="title_table" width="150">Periode</td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>

	        <tr>
	        	<td class="title_table" width="150">No BEO</td>
	        	<td>
	        		<input type="text" class="form-control-new" value="<?php echo $v_nobeo; ?>" name="v_nobeo" id="v_nobeo" size="34">
	        	</td>
	        </tr>

			<tr>
	        	<td class="title_table" width="150">No Sticker</td>
	        	<td>
	        		<input type="text" class="form-control-new" value="<?php echo $v_nosticker; ?>" name="v_nosticker" id="v_nosticker" size="34">
	        	</td>
	        </tr>

	        

	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>

	    </table>
	    </form>
	</div>
</div>

<?php
//if ($tampilkanDT)
//{
	/*
	echo $v_pilihan;echo "<br>";
echo $excel;echo "<br>";
echo $nostiker;echo "<br>";
				echo "v_pilihan";
				die;
	*/

	if($v_pilihan=="transaksi"){
		$this->load->view("report/penjualan/reservasi/tampil", $data);
	}
	elseif($v_pilihan=="detail"){
		$this->load->view("report/penjualan/reservasi/tampil_detail");
	}
	elseif($v_pilihan=="barang"){
		$this->load->view("report/penjualan/reservasi/tampil_barang");
	}

//}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
