<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Penjualan Per Sticker";

if($v_pilihan=="barang"){
	$check2 = "checked";
	$check1 = "";
}
else{
	$check2 = "";
	$check1 = "checked";
}
?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_persticker/search_report/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        
			<tr>
	        	<td class="title_table" width="150">Rekap Per-</td>
	        	<td>     
                    <div class="radio" style="float:left; width:100px; margin-top:0px;" >
                        <label><input type="radio" name="v_pilihan" id="v_pilihan"  value='transaksi' <? echo $check1;?> /> Transaksi</label>
                    </div> 
                     <div class="radio" style="float:left;  width:100px; margin-top:0px;"> 
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='barang' <? echo $check2;?> /> Barang</label>       
	        	    </div>
	        	    
                </td>
	        </tr>
			
	        <tr>
	            <td class="title_table" width="150">Tanggal</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date; ?>" name="v_date" id="v_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	        	<td class="title_table" width="150">No Sticker</td>
	        	<td>
	        		<input type="text" class="form-control-new" value="<?php echo $v_nostiker; ?>" name="v_nostiker" id="v_nostiker" size="10">	
	        	</td>
	        </tr>
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT){
	if($v_pilihan=="transaksi"){
		$this->load->view("report/penjualan/sticker/tampil", $data);
	}
	elseif($v_pilihan=="barang"){
		$this->load->view("report/penjualan/sticker/tampil_barang");
	}
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>