<?php
$mylib = new globallib();

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_persticker/search_report/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_transaksi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
            <tr class="title_table">
                <td width="30" style="vertical-align: middle; text-align: center;">No</td>
                <td style="vertical-align: middle; text-align: center;">Divisi</td>
                <td style="vertical-align: middle; text-align: center;">Sub Divisi</td>
                <td style="vertical-align: middle; text-align: center;">Kasir</td>
                <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;">Waktu</td>
                <td style="vertical-align: middle; text-align: center;">No Struk</td>
                
                <td style="vertical-align: middle; text-align: center;">Gross</td>
                <td style="vertical-align: middle; text-align: center;">Disc</td>
                <td style="vertical-align: middle; text-align: center;">Voucher</td>
                <td style="vertical-align: middle; text-align: center;">Netto</td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$subdivbruto = 0;
			$subdivdiscount = 0;
			$subdivnetto = 0;
			$subdivoucher = 0;
			
			$divbruto = 0;
			$divdiscount = 0;
			$divnetto = 0;
			$divvoucher = 0;
			
			$gtbruto = 0;
			$gtdiscount = 0;
			$gtnetto = 0;
			$gtvoucher = 0;
			
			$tempsubdivisi='';
			$tempdivisi='';
			for($i=0;$i<count($hasil);$i++)
			{		
				$nostruk = $hasil[$i]["NoStruk"];
				$kasir = $hasil[$i]["Kasir"];
				$namadivisi = $hasil[$i]["NamaDivisi"];
				$subdivisi = $hasil[$i]["SubDivisi"];
				$tanggal = $hasil[$i]["Tanggal"];
				$waktu = $hasil[$i]["Waktu"];
				$bruto = $hasil[$i]["Gross"];
				$discount = $hasil[$i]["Discount"];
				$voucher = $hasil[$i]["Voucher"];
				$netto = $bruto-$discount-$voucher;
				
                if($tempsubdivisi != '' and $tempsubdivisi != $subdivisi){
                	$subdivnetto = $subdivbruto-$subdivdiscount-$subdivvoucher;
				?>
					<tr class="title_table">
						<td nowrap colspan="7" align="center"><?php echo 'Total SubDivisi ' . $tempsubdivisi; ?></td>
						<td nowrap title="Total = <?php echo ubah_format($subdivbruto); ?>" align="right"><?php echo ubah_format($subdivbruto); ?></td>
						<td nowrap title="Discount = <?php echo ubah_format($subdivdiscount); ?>" align="right"><?php echo ubah_format($subdivdiscount); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($subdivvoucher); ?>" align="right"><?php echo ubah_format($subdivvoucher); ?></td>
						<td nowrap title="Netto = <?php echo ubah_format($subdivnetto); ?>" align="right"><?php echo ubah_format($subdivnetto); ?></td>
					</tr>
				<?php
					$subdivbruto = 0;
					$subdivdiscount = 0;
					$subdivvoucher = 0;
				}
				if($tempdivisi != '' and $tempdivisi != $namadivisi){
					$divnetto = $divbruto-$divdiscount-$divvoucher;
				?>
					<tr class="title_table">
						<td nowrap colspan="7" align="center"><?php echo 'Total Divisi ' . $tempdivisi; ?></td>
						<td nowrap title="Total = <?php echo ubah_format($divbruto); ?>" align="right"><?php echo ubah_format($divbruto); ?></td>
						<td nowrap title="Discount = <?php echo ubah_format($divdiscount); ?>" align="right"><?php echo ubah_format($divdiscount); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($divvoucher); ?>" align="right"><?php echo ubah_format($divvoucher); ?></td>
						<td nowrap title="Netto = <?php echo ubah_format($divnetto); ?>" align="right"><?php echo ubah_format($divnetto); ?></td>
					</tr>
				<?
					$divbruto = 0;
					$divdiscount = 0;
					$divvoucher=0;
				}
				?>
				
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo ($i+1); ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $namadivisi; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $subdivisi; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $kasir; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $mylib->ubah_tanggal($tanggal); ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $waktu; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $nostruk; ?></td>
					
					<td nowrap title="Total = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Discount = <?php echo ubah_format($discount); ?>" align="right"><?php echo ubah_format($discount); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($voucher); ?>" align="right"><?php echo ubah_format($voucher); ?></td>
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>
				</tr>
				<?php
	
				$subdivbruto += $bruto;
				$subdivdiscount += $discount;
				$subdivvoucher += $voucher;
				
				$divbruto += $bruto;
				$divdiscount += $discount;
				$divvoucher += $voucher;
				
				$gtbruto += $bruto;
				$gtdiscount += $discount;
				$gtvoucher += $voucher;
				
				$tempdivisi = $namadivisi;
				$tempsubdivisi = $subdivisi;
			}
			?>
			
			<?php
			if(count($hasil)>0)
			{
				$subdivnetto = $subdivbruto-$subdivdiscount-$subdivvoucher;
				$divnetto = $divbruto-$divdiscount-$divvoucher;
				$gtnetto = $gtbruto-$gtdiscount-$gtvoucher;
				?>
				<tr class="title_table">
					<td nowrap colspan="7" align="center"><?php echo 'Total SubDivisi ' . $tempsubdivisi; ?></td>
					<td nowrap title="Total = <?php echo ubah_format($subdivbruto); ?>" align="right"><?php echo ubah_format($subdivbruto); ?></td>
					<td nowrap title="Discount = <?php echo ubah_format($subdivdiscount); ?>" align="right"><?php echo ubah_format($subdivdiscount); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($subdivvoucher); ?>" align="right"><?php echo ubah_format($subdivvoucher); ?></td>
					<td nowrap title="Netto = <?php echo ubah_format($subdivnetto); ?>" align="right"><?php echo ubah_format($subdivnetto); ?></td>
				</tr>
				<tr class="title_table">
						<td nowrap colspan="7" align="center"><?php echo 'Total Divisi ' . $tempdivisi; ?></td>
						<td nowrap title="Total = <?php echo ubah_format($divbruto); ?>" align="right"><?php echo ubah_format($divbruto); ?></td>
						<td nowrap title="Discount = <?php echo ubah_format($divdiscount); ?>" align="right"><?php echo ubah_format($divdiscount); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($divvoucher); ?>" align="right"><?php echo ubah_format($divvoucher); ?></td>
						<td nowrap title="Netto = <?php echo ubah_format($divnetto); ?>" align="right"><?php echo ubah_format($divnetto); ?></td>
				</tr>
				<tr class="title_table">
					<td nowrap colspan="7" align="center"><?php echo 'Grand Total '; ?></td>
					<td nowrap title="Total = <?php echo ubah_format($gtbruto); ?>" align="right"><?php echo ubah_format($gtbruto); ?></td>
					<td nowrap title="Disc = <?php echo ubah_format($gtdiscount); ?>" align="right"><?php echo ubah_format($gtdiscount); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($gtvoucher); ?>" align="right"><?php echo ubah_format($gtvoucher); ?></td>
					<td nowrap title="Netto = <?php echo ubah_format($gtnetto); ?>" align="right"><?php echo ubah_format($gtnetto); ?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
<script text='javascript'>
function VoidTrans(param){
	//if (confirm("Anda yakin ingin menghapus transaksi ini?")){
		window.open(param,'','width=400,height=300,left=400,top=250');
	//}
}
</script>