<?php
unset($arr_data);
$arr_data = array();
$arr_data["list_transaksi"] = array();
//print_r($hasil);
foreach($hasil as $val)
{
	$arr_data["list_transaksi"][$val["NoStruk"]]=$val["NoStruk"];
	$arr_data["nokassa"][$val["NoStruk"]]=$val["NoKassa"];
	$arr_data["kasir"][$val["NoStruk"]]=$val["Kasir"];
	$arr_data["kddivisi"][$val["NoStruk"]]=$val["KdDivisi"];
	$arr_data["namadivisi"][$val["NoStruk"]]=$val["NamaDivisi"];
	$arr_data["kdsubdivisi"][$val["NoStruk"]]=$val["KdSubDivisi"];
	$arr_data["namasubdivisi"][$val["NoStruk"]]=$val["NamaSubDivisi"];
	$arr_data["tanggal"][$val["NoStruk"]]=$val["Tanggal"];
	$arr_data["waktu"][$val["NoStruk"]]=$val["Waktu"];
	
	$arr_data["totalnilai"][$val["NoStruk"]]=$val["TotalNilai"];
	$arr_data["totalbayar"][$val["NoStruk"]]=$val["TotalBayar"];
	$arr_data["voucher"][$val["NoStruk"]]=$val["Voucher"];
	$arr_data["disc"][$val["NoStruk"]]=$val["Discount"];
	$arr_data["item"][$val["NoStruk"]]=$val["TotalItem"];
	
	$arr_data["list_transaksi_detail"][$val["NoStruk"]][$val["PCode"]]=$val["PCode"];
	
	//$arr_data["pcode"][$val["NoStruk"]]=$val["PCode"];
	$arr_data["namalengkap"][$val["NoStruk"]][$val["PCode"]]=$val["NamaLengkap"];
	$arr_data["qty"][$val["NoStruk"]][$val["PCode"]]=$val["Qty"];
	$arr_data["harga"][$val["NoStruk"]][$val["PCode"]]=$val["Harga"];
	$arr_data["bruto"][$val["NoStruk"]][$val["PCode"]]=$val["Bruto"];
	$arr_data["discountdetail"][$val["NoStruk"]][$val["PCode"]]=$val["DiscDetail"];
	$arr_data["netto"][$val["NoStruk"]][$val["PCode"]]=$val["Netto"];
	$arr_data["service_charge"][$val["NoStruk"]][$val["PCode"]]=$val["Service_charge"];
	$arr_data["ppn"][$val["NoStruk"]][$val["PCode"]]=$val["PPN"];
	$arr_data["persenpajak"][$val["NoStruk"]][$val["PCode"]]=$val["PersenPajak"];
}

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_kasir/search_report/" onsubmit="return false"/>
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_detail.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" style="text-align: center;">No</td>
                    <td style="text-align: center;">Divisi</td>
					<td style="text-align: center;">SubDivisi</td>
                    <td style="text-align: center;">Tanggal</td>
                    <td style="text-align: center;">Waktu</td>
                    <td style="text-align: center;">No Struk</td>
                    
                    <td style="text-align: center;">PCode</td>
                    <td style="text-align: center;">Nama</td>
					<td style="text-align: center;">Qty</td>
                    <td style="text-align: center;">Harga</td>
                    <td style="text-align: center;">Bruto</td>
					<td style="text-align: center;">Disc</td>
					<td style="text-align: center;">Voucher</td>
					<td style="text-align: center;">Compliment</td>
					<td style="text-align: center;">DPP</td>
					<td style="text-align: center;">Service Charge</td>
					<td style="text-align: center;">PPN</td>
					<td style="text-align: center;">PB1</td>
					<td style="text-align: center;">HET</td>
                </tr>            
			<tbody>
			
			<?php
			if(count($arr_data["list_transaksi"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}			
			
			$tempnostruk = "";
			foreach($arr_data["list_transaksi"] as $nostruk => $val)
			{		
				
				$nokassa = $arr_data["nokassa"][$nostruk];
				//$kasir = $arr_data["kasir"][$nostruk];
				$kddivisi = $arr_data["kddivisi"][$nostruk];
				$namadivisi = $arr_data["namadivisi"][$nostruk];
				$kdsubdivisi = $arr_data["kdsubdivisi"][$nostruk];
				$namasubdivisi = $arr_data["namasubdivisi"][$nostruk];
				$tanggal = $arr_data["tanggal"][$nostruk];
				$waktu = $arr_data["waktu"][$nostruk];
				
				$totalnilai = $arr_data["totalnilai"][$nostruk];
				$totalbayar = $arr_data["totalbayar"][$nostruk];
				$voucher = $arr_data["voucher"][$nostruk];
				$disc = $arr_data["disc"][$nostruk];
				$totalitem = $arr_data["item"][$nostruk];
				
				$compli = "no";
				if($disc>0 and $totalnilai==0){
						$compli = "yes";
				}
				
				$no=1;
				$nomor=1;
				foreach($arr_data["list_transaksi_detail"][$nostruk] as $pcode=>$val)
				{
				
				$namalengkap = $arr_data["namalengkap"][$nostruk][$pcode];
				$qty = $arr_data["qty"][$nostruk][$pcode];
				$harga = $arr_data["harga"][$nostruk][$pcode];
				$discountdetail = $arr_data["discountdetail"][$nostruk][$pcode];
				$persenpajak = $arr_data["persenpajak"][$nostruk][$pcode];
				$service_charge = $arr_data["service_charge"][$nostruk][$pcode];
				if($kddivisi==3 || ($kddivisi==2 and $nokassa!=36)){
					$harga = ($harga / 1.1);
					$bruto = $qty * $harga;
					$voucher = 0;
					$compliment = 0;
					if($compli=="yes"){
						$compliment = $bruto;
					}
					$dpp = ($bruto - $discountdetail - $voucher - $compliment);
					$sc = 0;//($dpp * ($service_charge/100));
					$ppn = ($dpp * 0.1);
					$pb1 = 0;
				}
				else{
						$bruto = $qty * $harga;
						$voucher = 0;
						$compliment = 0;
						if($compli=="yes"){
							$compliment = $bruto;
						}
						$dpp = ($bruto - $discountdetail - $voucher - $compliment);
						$sc = ($dpp * (5/100));
						$ppn = 0;
						$pb1 = ($dpp + $sc) * 0.1;
				}
				
				
				$het = $dpp + $sc + $ppn + $pb1;
				
				if($tempnostruk!="" and $tempnostruk!=$nostruk){
				?>
					<tr bgcolor='#e6e6e6'>
						<td colspan="8" align="center"><b>Total</b>&nbsp;</td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
						<td align="right"><b><?php echo ubah_format($arr_data["t_discountdetail"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_voucher"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_compliment"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_dpp"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_sc"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_ppn"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_pb1"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_het"]); ?></b></td>
					</tr>		
				<?
					$arr_data["t_qty"] = 0;
					$arr_data["t_bruto"] = 0;
					$arr_data["t_discountdetail"] = 0;
					$arr_data["t_voucher"] = 0;
					$arr_data["t_compliment"] = 0;
					$arr_data["t_dpp"] = 0;
					$arr_data["t_sc"] = 0;
					$arr_data["t_ppn"] = 0;
					$arr_data["t_pb1"] = 0;
					$arr_data["t_het"] = 0;
				}				
				
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $tanggal." :: ".$namadivisi." :: ".$namasubdivisi." :: ".$nostruk; ?>"><?php echo $no; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$namasubdivisi." :: ".$nostruk; ?>"><?php if($tempnostruk!=$nostruk){echo $namadivisi; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$namasubdivisi." :: ".$nostruk; ?>"><?php if($tempnostruk!=$nostruk){echo $namasubdivisi; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$namasubdivisi." :: ".$nostruk; ?>" align="center"><?php if($tempnostruk!=$nostruk){echo $mylib->ubah_tanggal($tanggal); }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$namasubdivisi." :: ".$nostruk; ?>" align="center"><?php if($tempnostruk!=$nostruk){echo $waktu; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$namasubdivisi." :: ".$nostruk; ?>" align="center"><?php if($tempnostruk!=$nostruk){echo $nostruk; }else{ echo "&nbsp;";} ?></td>
					
					<td nowrap title="PCode = <?php echo $pcode; ?>" align="left" style="mso-number-format: '\@'"><?php echo $pcode; ?></td>
					<td nowrap title="Nama = <?php echo $namalengkap; ?>" align="left"><?php echo $namalengkap; ?></td>
					<td nowrap title="Qty = <?php echo ubah_format2($qty); ?>" align="right"><?php echo ubah_format2($qty); ?></td>
					<td nowrap title="Harga = <?php echo ubah_format($harga); ?>" align="right"><?php echo ubah_format($harga); ?></td>
					<td nowrap title="Bruto = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					
					<td nowrap title="Disc = <?php echo ubah_format($discountdetail); ?>" align="right"><?php echo ubah_format($discountdetail); ?></td>					
					<td nowrap title="Voucher = <?php echo ubah_format($voucher); ?>" align="right"><?php echo ubah_format($voucher); ?></td>				
					<td nowrap title="Compliment = <?php echo ubah_format($compliment); ?>" align="right"><?php echo ubah_format($compliment); ?></td>
					<td nowrap title="DPP = <?php echo ubah_format($dpp); ?>" align="right"><?php echo ubah_format($dpp); ?></td>
					<td nowrap title="Service Charge = <?php echo ubah_format($sc); ?>" align="right"><?php echo ubah_format($sc); ?></td>
					<td nowrap title="PPN = <?php echo ubah_format($ppn); ?>" align="right"><?php echo ubah_format($ppn); ?></td>
					<td nowrap title="PB1 = <?php echo ubah_format($pb1); ?>" align="right"><?php echo ubah_format($pb1); ?></td>
					<td nowrap title="HET = <?php echo ubah_format($het); ?>" align="right"><?php echo ubah_format($het); ?></td>
				</tr>
				<?php
	
				$arr_data["t_qty"] += $qty;
				//$arr_data["t_harga"] += $harga;
				$arr_data["t_bruto"] += $bruto;
				$arr_data["t_discountdetail"] += $discountdetail;
				$arr_data["t_voucher"] += $voucher;
				$arr_data["t_compliment"] += $compliment;
				$arr_data["t_dpp"] += $dpp;
				$arr_data["t_sc"] += $sc;
				$arr_data["t_ppn"] += $ppn;
				$arr_data["t_pb1"] += $pb1;
				$arr_data["t_het"] += $het;
	
				$arr_data["gt_qty"] += $qty;
				//$arr_data["gt_harga"] += $harga;
				$arr_data["gt_bruto"] += $bruto;
				$arr_data["gt_discountdetail"] += $discountdetail;
				$arr_data["gt_voucher"] += $voucher;
				$arr_data["gt_compliment"] += $compliment;
				$arr_data["gt_dpp"] += $dpp;
				$arr_data["gt_sc"] += $sc;
				$arr_data["gt_ppn"] += $ppn;
				$arr_data["gt_pb1"] += $pb1;
				$arr_data["gt_het"] += $het;
				
				$no++;
				$tempnostruk = $nostruk;				
				}
			}
			?>
				<tr bgcolor='#e6e6e6'>
						<td colspan="8" align="center"><b>Total</b>&nbsp;</td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
						<td align="right"><b><?php echo ubah_format($arr_data["t_discountdetail"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_voucher"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_compliment"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_dpp"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_sc"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_ppn"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_pb1"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_het"]); ?></b></td>
					</tr>	
			<?php
			if(count($arr_data["list_transaksi"])>0)
			{
			?>
				<tr class="title_table">
					<td colspan="8" align="center"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
						<td align="right"><b><?php echo ubah_format($arr_data["gt_discountdetail"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_voucher"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_compliment"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_dpp"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_sc"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_ppn"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_pb1"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["gt_het"]); ?></b></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>

<?
function where_array($list_array, $kolom, $type="in")
{
    $where = "";
    if($type=="in")
    {
        foreach($list_array as $key=>$val)
        if($where == "")
        {
            $where .= " AND (".$kolom." = '".$val."' ";
        }    
        else
        {
            $where .= " OR ".$kolom." = '".$val."' ";    
        }
        
        if($where)
        {
            $where .= ")";
        }
    }
    else if($type=="not in")
    {
        foreach($list_array as $key=>$val)
        if($where == "")
        {
            $where .= " AND (".$kolom." != '".$val."' ";
        }    
        else
        {
            $where .= " OR ".$kolom." != '".$val."' ";
        }
        
        if($where)
        {
            $where .= ")";
        }    
    }
    
    return $where;
}

function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}

function ubah_format2($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>