<?php

$mylib = new globallib();

unset($arr_data);

$arr_data = array();
$arr_data["list_transaksi"] = array();

//echo "<pre>";print_r($hasil);echo "</pre>";die;
foreach($hasil as $val)
{
	$arr_data["list_transaksi"][$val["NoStruk"]]=$val["NoStruk"];
	$arr_data["nokassa"][$val["NoStruk"]]=$val["NoKassa"];
	$arr_data["kasir"][$val["NoStruk"]]=$val["Kasir"];
	$arr_data["kddivisi"][$val["NoStruk"]]=$val["KdDivisi"];
	$arr_data["namadivisi"][$val["NoStruk"]]=$val["NamaDivisi"];
	$arr_data["subdivisi"][$val["NoStruk"]]=$val["SubDivisi"];
	$arr_data["tanggal"][$val["NoStruk"]]=$val["Tanggal"];
	$arr_data["waktu"][$val["NoStruk"]]=$val["Waktu"];
	$arr_data["tunai"][$val["NoStruk"]]=$val["Tunai"];
	$arr_data["kdebit"][$val["NoStruk"]]=$val["KDebit"];
	$arr_data["kkredit"][$val["NoStruk"]]=$val["KKredit"];
	$arr_data["voucher"][$val["NoStruk"]]=$val["Voucher"];
	$arr_data["discount"][$val["NoStruk"]]=$val["Discount"];
	$arr_data["totalnilai"][$val["NoStruk"]]=$val["TotalNilai"];
	$arr_data["totalbayar"][$val["NoStruk"]]=$val["TotalBayar"];
	$arr_data["kembali"][$val["NoStruk"]]=$val["Kembali"];
	$arr_data["tax"][$val["NoStruk"]]=$val["Tax"];
	$arr_data["sales"][$val["NoStruk"]]=$val["Sales"];
	$arr_data["charge"][$val["NoStruk"]]=$val["ServiceCharge"];
	$arr_data["modal"][$val["NoStruk"]]=$val["Modal"];
	
	$arr_data["totaldetail"][$val["NoStruk"]]=$val["Gross"];
	$arr_data["discountdetail"][$val["NoStruk"]]=$val["Discount"];
	$arr_data["netto"][$val["NoStruk"]]=$val["Nett"];    
}

$arr_data["list_voucher"]= array();
foreach($hasil_voucher as $val)
{
	$arr_data["list_voucher"][$val["Tanggal"]][$val["NoStruk"]] += $val["NilaiVoucher"];
}

$username = $this->session->userdata('username');
$q = "SELECT * FROM `otorisasi_user` WHERE Tipe='kasir' AND UserName='$username'";
$qry = mysql_query($q);
$num_hasil = mysql_num_rows($qry);

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<script>
	function PopUpVoucher(id)
    {
		base_url = "<?php echo base_url(); ?>";
		url = base_url+"index.php/report/report_kasir/pop_up_detail_voucher/index/"+id+"/";
		windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_kasir/search_report/" onsubmit="return false"/>
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_transaksi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" rowspan="2" style="vertical-align: middle; text-align: center;">No</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Divisi</td>
                    <!--<td rowspan="2" style="vertical-align: middle; text-align: center;">Kasir</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">SubDivisi</td>-->
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Tanggal</td>
                    <!--<td rowspan="2" style="vertical-align: middle; text-align: center;">Waktu</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">No Struk</td>-->
                    
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Total</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Disc</td>
					<td rowspan="2" style="vertical-align: middle; text-align: center;">Disc/Compliement</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Netto</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Charge</td>
					<td rowspan="2" style="vertical-align: middle; text-align: center;">Tax</td>
					<td rowspan="2" style="vertical-align: middle; text-align: center;">GrandTotal</td>
					
                    <td colspan="5" style="text-align: center;">Pembayaran</td>
                    
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Total Bayar</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Kembali</td>
					<?
					if($num_hasil>0){
					?>
						<td rowspan="2" style="vertical-align: middle; text-align: center;">Action</td>
					<?
					}
					?>
                </tr>
                
                <tr class="title_table">
                    <td style="text-align: center;">NetTunai</td>
					<td style="text-align: center;">Tunai</td>
                    <td style="text-align: center;">Kredit</td>
                    <td style="text-align: center;">Debit</td>
                    <td style="text-align: center;">Voucher</td>
                </tr>
			<tbody>
			
			<?php
			if(count($arr_data["list_transaksi"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$arr_data["gt_bruto"] ="";
			$arr_data["gt_discountdetail"]  ="";
			$arr_data["gt_discount"]  ="";
			$arr_data["gt_netto"]  ="";
			$arr_data["gt_charge"] ="";
			$arr_data["gt_tax"] ="";
			$arr_data["gt_grandtotal"] ="";
			
			$arr_data["gt_nettunai"] ="";
			$arr_data["gt_tunai"] ="";
			$arr_data["gt_kredit"] ="";
			$arr_data["gt_debit"]  ="";
			$arr_data["gt_voucher"]="";
			
			$arr_data["gt_totalbayar"] ="";
			$arr_data["gt_kembali"] ="";
			$no=1;
			foreach($arr_data["list_transaksi"] as $nostruk => $val)
			{		
				$nokassa = $arr_data["nokassa"][$nostruk];
				$kasir = $arr_data["kasir"][$nostruk];
				$kddivisi = $arr_data["kddivisi"][$nostruk];
				$namadivisi = $arr_data["namadivisi"][$nostruk];
				$subdivisi = $arr_data["subdivisi"][$nostruk];
				$tanggal = $arr_data["tanggal"][$nostruk];
				$waktu = $arr_data["waktu"][$nostruk];
				$tunai = $arr_data["tunai"][$nostruk];
				$kdebit = $arr_data["kdebit"][$nostruk];
				$kkredit = $arr_data["kkredit"][$nostruk];
				$voucher = $arr_data["voucher"][$nostruk];
				$discount = $arr_data["discount"][$nostruk];
				$totalnilai = $arr_data["totalnilai"][$nostruk];
				$totalbayar = $arr_data["totalbayar"][$nostruk];
				$kembali = $arr_data["kembali"][$nostruk];
				$tax = $arr_data["tax"][$nostruk];
				$totalcharge = $arr_data["charge"][$nostruk];			
				$nettunai = $tunai - $kembali;
				
				$bruto = $arr_data["totaldetail"][$nostruk];
				$discountdetail = $arr_data["discountdetail"][$nostruk];
				$netto = $arr_data["netto"][$nostruk];		
				
				$nilai_voucher = $arr_data["list_voucher"][$mylib->ubah_tanggal($tanggal)][$nostruk];	
				
				$arr_date = explode("-",$tanggal);
				
				if($arr_date[1]*1<=5)
				{
					$echo_voucher = $voucher;	
				}
				else
				{
					$echo_voucher = $nilai_voucher;		
				}
						
				//netto = Bruto - DiscHeader - DiscDetail;
				$pembayaran = $tunai + $kdebit + $kkredit + $echo_voucher;
				if($kddivisi!=3){
					$grandtotal = $netto + $totalcharge + $tax;	
				}
				else{
						if($totalcharge>0){
							$grandtotal = $netto + $totalcharge + $tax;
						}
						else{
								$grandtotal = $netto;
						}
				}
				
				if($totalcharge==0){
					$totalcharge = 0;
					$tax = 0;
				}
                
                //$grandtotal = $totalnilai;
				
				$jm_pembayaran = (($tunai*1)+($kkredit*1)+($kdebit*1)+($echo_voucher*1))-($kembali*1);
				
                /*
				$bg_salah="";
				if(($jm_pembayaran*1)!=($grandtotal*1))
				{
					$bg_salah = "background-color: #eef15f;";
					
				}
                */
				
				$echo_grandtotal = $totalnilai;
                
                $bg_salah="";
                if(($jm_pembayaran*1)!=($echo_grandtotal*1))
                {
                    //$bg_salah = "background-color: #eef15f;";
                    
                }
                
                /*
				if($kddivisi=2 && $subdivisi!=15)
				{
					$bg_salah="";
					
					$echo_grandtotal = $grandtotal - ($totalcharge + $tax);
				}
                */
                
                ///$echo_grandtotal = $totalnilai;
					
				
				if($tempsubdivisi != '' and $tempsubdivisi != $subdivisi){
                	$subdivnettunai = $subdivtunai - $subdivkembali;
                	$subdivtotalbayar = $subdivtunai+$subdivkdebit+$subdivkkredit+$subdivvoucher;
				?>
					<tr class="title_table">
						<td nowrap colspan="7" align="center"><?php echo 'Total SubDivisi ' . $tempsubdivisi; ?></td>
						<td nowrap title="Total = <?php echo ubah_format($subdivbruto); ?>" align="right"><?php echo ubah_format($subdivbruto); ?></td>
						<td nowrap title="Disc/Compliement = <?php echo ubah_format($subdivdiscount); ?>" align="right"><?php echo ubah_format($subdivdiscount); ?></td>
						<td nowrap title="Netto = <?php echo ubah_format($subdivnetto); ?>" align="right"><?php echo ubah_format($subdivnetto); ?></td>
						
						<td nowrap title="Charge = <?php echo ubah_format($subdivtotalcharge); ?>" align="right"><?php echo ubah_format($subdivtotalcharge); ?></td>
						<td nowrap title="Tax = <?php echo ubah_format($subdivtax); ?>" align="right"><?php echo ubah_format($subdivtax); ?></td>					
						<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($subdivsales); ?>" align="right"><?php echo ubah_format($subdivsales); ?></td>
						
						<td nowrap title="NetTunai = <?php echo ubah_format($subdivnettunai); ?>" align="right"><?php echo ubah_format($subdivnettunai); ?></td>
						<td nowrap title="Tunai = <?php echo ubah_format($subdivtunai); ?>" align="right" ><?php echo ubah_format($subdivtunai); ?></td>
						<td nowrap title="Kredit = <?php echo ubah_format($subdivkkredit); ?>" align="right"><?php echo ubah_format($subdivkkredit); ?></td>
						<td nowrap title="Debit = <?php echo ubah_format($subdivkdebit); ?>" align="right"><?php echo ubah_format($subdivkdebit); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($subdivvoucher); ?>" align="right"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($subdivvoucher); ?></a><?}else{ echo ubah_format($subdivvoucher);}?></td>
						<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($subdivtotalbayar); ?>" align="right"><?php echo ubah_format($subdivtotalbayar); ?></td>
						<td nowrap title="Kembali = <?php echo ubah_format($subdivkembali); ?>" align="right"><?php echo ubah_format($subdivkembali); ?></td>
					</tr>
				<?php
					$subdivbruto = 0;
					$subdivdiscount = 0;
					$subdivnetto = 0;
					$subdivtax = 0;
					$subdivsales = 0;
					$subdivtunai = 0;
					$subdivkkredit = 0;
					$subdivkdebit = 0;
					$subdivvoucher = 0;
					$subdivkembali = 0;
					$subdivtotalcharge = 0;
				}
				if($tempdivisi != '' and $tempdivisi != $namadivisi){
                	$divnettunai = $divtunai - $divkembali;
                	$divtotalbayar = $divtunai+$divkdebit+$divkkredit+$divvoucher;
				?>
					<tr class="title_table">
						<td nowrap colspan="7" align="center"><?php echo 'Total Divisi ' . $tempdivisi; ?></td>
						<td nowrap title="Total = <?php echo ubah_format($divbruto); ?>" align="right"><?php echo ubah_format($divbruto); ?></td>
						<td nowrap title="Disc/Compliement = <?php echo ubah_format($divdiscount); ?>" align="right"><?php echo ubah_format($divdiscount); ?></td>
						<td nowrap title="Netto = <?php echo ubah_format($divnetto); ?>" align="right"><?php echo ubah_format($divnetto); ?></td>
						
						<td nowrap title="Charge = <?php echo ubah_format($divtotalcharge); ?>" align="right"><?php echo ubah_format($divtotalcharge); ?></td>
						<td nowrap title="Tax = <?php echo ubah_format($divtax); ?>" align="right"><?php echo ubah_format($divtax); ?></td>					
						<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($divsales); ?>" align="right"><?php echo ubah_format($divsales); ?></td>
						
						<td nowrap title="NetTunai = <?php echo ubah_format($divnettunai); ?>" align="right"><?php echo ubah_format($divnettunai); ?></td>
						<td nowrap title="Tunai = <?php echo ubah_format($divtunai); ?>" align="right" ><?php echo ubah_format($divtunai); ?></td>
						<td nowrap title="Kredit = <?php echo ubah_format($divkkredit); ?>" align="right"><?php echo ubah_format($divkkredit); ?></td>
						<td nowrap title="Debit = <?php echo ubah_format($divkdebit); ?>" align="right"><?php echo ubah_format($divkdebit); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($divvoucher); ?>" align="right" style="border-right-color: #009999;"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($divvoucher); ?></a><?}else{ echo ubah_format($divvoucher);}?></td>
						<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($divtotalbayar); ?>" align="right"><?php echo ubah_format($divtotalbayar); ?></td>
						<td nowrap title="Kembali = <?php echo ubah_format($divkembali); ?>" align="right"><?php echo ubah_format($divkembali); ?></td>
					</tr>
				<?
					$divbruto = 0;
					$divdiscount = 0;
					$divnetto = 0;
					$divtax = 0;
					$divsales = 0;
					$divtunai = 0;
					$divkkredit = 0;
					$divkdebit = 0;
					$divvoucher = 0;
					$divkembali = 0;
					$divtotalcharge = 0;
				}
				
				
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="<?php echo $bg_salah; ?>">
					<td nowrap align="center" title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $no; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $namadivisi; ?></td>
					<!--<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $kasir; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $subdivisi; ?></td>-->
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $mylib->ubah_tanggal($tanggal); ?></td>
					<!--<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $waktu; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $nostruk; ?></td>-->
					
					<td nowrap title="Total = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Disc = <?php echo ubah_format($discountdetail); ?>" align="right"><?php echo ubah_format($discountdetail); ?></td>
					<td nowrap title="Disc/Compliement = <?php echo ubah_format($discount); ?>" align="right"><?php echo ubah_format($discount); ?></td>
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>
					
					<td nowrap title="Charge = <?php echo ubah_format($totalcharge); ?>" align="right"><?php echo ubah_format($totalcharge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($tax); ?>" align="right"><?php echo ubah_format($tax); ?></td>					
					<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($echo_grandtotal); ?>" align="right"><?php echo ubah_format($echo_grandtotal); ?></td>
					
					<td nowrap title="NetTunai = <?php echo ubah_format($nettunai); ?>" align="right"><?php echo ubah_format($nettunai); ?></td>
					<td nowrap title="Tunai = <?php echo ubah_format($tunai); ?>" align="right" ><?php echo ubah_format($tunai); ?></td>
					<td nowrap title="Kredit = <?php echo ubah_format($kkredit); ?>" align="right"><?php echo ubah_format($kkredit); ?></td>
					<td nowrap title="Debit = <?php echo ubah_format($kdebit); ?>" align="right"><?php echo ubah_format($kdebit); ?></td>
					<?php
					if($arr_date[1]*1<=5)
					{
						?>
						<td nowrap title="Voucher = <?php echo ubah_format($voucher); ?>" align="right" style="border-right-color: #009999;"><?php echo ubah_format($voucher); ?></td>
						<?php
					}
					else
					{
						?>
						<td nowrap title="Voucher = <?php echo ubah_format($nilai_voucher); ?>" align="right" style="border-right-color: #009999;"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($nilai_voucher); ?></a><?}else{ echo ubah_format($nilai_voucher);}?></td>
						<?php
					}
					?>
					<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($totalbayar); ?>" align="right"><?php echo ubah_format($totalbayar); ?></td>
					<td nowrap title="Kembali = <?php echo ubah_format($kembali); ?>" align="right"><?php echo ubah_format($kembali); ?></td>
					<?
					if($num_hasil>0){
						$link = base_url()."/inventory/form_otorisasi.php?kdtransaksi=R&tipe=kasir&notransaksi=$nostruk";
					?>
						<td rowspan title='Action' align='center'><img src='<?= base_url(); ?>public/images/flag_orange.gif' border='0' title='Void' onclick="VoidTrans('<? echo $link;?>');"></img></td>
					<?
					}
					?>
				</tr>
				<?php
	
				$arr_data["gt_bruto"] += $bruto;
				$arr_data["gt_discountdetail"] += $discountdetail;
				$arr_data["gt_discount"] += $discount;
				$arr_data["gt_netto"] += $netto;
				$arr_data["gt_charge"] += $totalcharge;
				$arr_data["gt_tax"] += $tax;
				$arr_data["gt_grandtotal"] += $echo_grandtotal;
				
				$arr_data["gt_nettunai"] += $nettunai;
				$arr_data["gt_tunai"] += $tunai;
				$arr_data["gt_kredit"] += $kkredit;
				$arr_data["gt_debit"] += $kdebit;
				$arr_data["gt_voucher"] += $echo_voucher;
				
				$arr_data["gt_totalbayar"] += $totalbayar;
				$arr_data["gt_kembali"] += $kembali;
				
				$no++;
			}
			?>
			
			<?php
			if(count($arr_data["list_transaksi"])>0)
			{
			?>
				<tr class="title_table">
					<td colspan="3" align="right"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_bruto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_discountdetail"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_discount"]); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_netto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_charge"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_tax"]); ?></b></td>
					<td align="right" style="border-right-color: #73e8e4;"><b><?php echo ubah_format($arr_data["gt_grandtotal"]); ?></b></td>
					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_nettunai"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_tunai"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_kredit"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_debit"]); ?></b></td>
					<td style="border-right-color: #ff7575;" align="right"><b><?php echo ubah_format($arr_data["gt_voucher"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_totalbayar"]); ?></b></td>
					<td align="right" style="border-right-color: #c1f966;" ><b><?php echo ubah_format($arr_data["gt_kembali"]); ?></b></td>				
					<?
					if($num_hasil>0){
					?>
					<td>&nbsp;</td>
					<?
					}
					?>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
<script text='javascript'>
function VoidTrans(param){
	//if (confirm("Anda yakin ingin menghapus transaksi ini?")){
		window.open(param,'','width=400,height=300,left=400,top=250');
	//}
}
</script>