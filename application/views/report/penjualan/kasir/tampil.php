<?php
$mylib = new globallib();
//echo $v_tipe_cari;
unset($arr_data);

$arr_data = array();
$arr_data["list_voucher"]= array();
foreach($hasil_voucher as $val)
{
	$arr_data["list_voucher"][$val["Tanggal"]][$val["NoStruk"]] += $val["NilaiVoucher"];
}

$username = $this->session->userdata('username');
$q = "SELECT * FROM `otorisasi_user` WHERE Tipe='kasir' AND UserName='$username'";
$qry = mysql_query($q);
$num_hasil = mysql_num_rows($qry);

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<script>
	function PopUpVoucher(id)
    {
		base_url = "<?php echo base_url(); ?>";
		url = base_url+"index.php/report/report_kasir/pop_up_detail_voucher/index/"+id+"/";
		windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_kasir/search_report/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_transaksi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" rowspan="3" style="vertical-align: middle; text-align: center;">No</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Divisi</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Sub Divisi</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Kasir</td>
                    <?php if($v_tipe_cari=="struk"){?>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Tanggal</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Waktu</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">No Struk</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Pax</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">No Stiker</td>
                    <?php } ?>
                    <?php if($otorisasi_diskon_view == 'Y') { ?>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Total</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Disc</td>
	                <?php } ?>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Netto</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Charge</td>
					<td rowspan="3" style="vertical-align: middle; text-align: center;">Tax</td>
					<td rowspan="3" style="vertical-align: middle; text-align: center;">Rounding</td>
					<td rowspan="3" style="vertical-align: middle; text-align: center;">GrandTotal</td>
					
                    <td colspan="11" style="text-align: center;">Pembayaran</td>
                    
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Total Bayar</td>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;">Kembali</td>
					<?
					if($num_hasil>0){
					?>
						<td rowspan="3" style="vertical-align: middle; text-align: center;">Action</td>
					<?
					}
					?>
                </tr>
                
                <tr class="title_table">
                    <td rowspan="2" style="text-align: center;">NetTunai</td>
					<td rowspan="2" style="text-align: center;">Tunai</td>
                    <td colspan="3" style="text-align: center;">Kredit</td>
                    <td colspan="3" style="text-align: center;">Debit</td>
                    <td rowspan="2" style="text-align: center;">GoPay</td>
                    <td rowspan="2" style="text-align: center;">Point</td>
                    <td rowspan="2" style="text-align: center;">Voucher</td>
                </tr>

				<tr class="title_table">
                    <td style="text-align: center;">Rp.</td>
					<td style="text-align: center;">Kartu</td>
                    <td style="text-align: center;">EDC</td>
                    <td style="text-align: center;">Rp.</td>
                    <td style="text-align: center;">Kartu</td>
                    <td style="text-align: center;">EDC</td>
                </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$subdivbruto = 0;
			$subdivdiscount = 0;
			$subdivnetto = 0;
			$subdivtax = 0;
			$subdivrounding = 0;
			$subdivsales = 0;
			$subdivtunai=0;
			$subdivkkredit = 0;
			$subdivkdebit=0;
			$subdivvoucher=0;
			$subdivkembali=0;
			$subdivtotalcharge = 0;
			$subdivgopay=0;
			$subdivpoint=0;
			
			$divbruto = 0;
			$divdiscount = 0;
			$divnetto = 0;
			$divtax = 0;
			$divrounding = 0;
			$divsales = 0;
			$divtunai=0;
			$divkkredit = 0;
			$divkdebit=0;
			$divvoucher=0;
			$divkembali=0;
			$divtotalcharge=0;
			$divgopay=0;
			$divpoint=0;
			
			$gtbruto = 0;
			$gtdiscount = 0;
			$gtnetto = 0;
			$gttax = 0;
			$gtrounding = 0;
			$gtsales = 0;
			$gttunai=0;
			$gtkkredit = 0;
			$gtkdebit=0;
			$gtvoucher=0;
			$gtkembali=0;
			$gttotalcharge=0;
			$gtgopay=0;
			$gtpoint=0;
			
			$tempsubdivisi='';
			$tempdivisi='';
			for($i=0;$i<count($hasil);$i++)
			{		
				$nostruk = $hasil[$i]["NoStruk"];
				$nostiker = $hasil[$i]["KdAgent"];
				$pax = $hasil[$i]["TotalGuest"];
				$kasir = $hasil[$i]["Kasir"];
				$namadivisi = $hasil[$i]["NamaDivisi"];
				$subdivisi = $hasil[$i]["SubDivisi"];
				$tanggal = $hasil[$i]["Tanggal"];
				$waktu = $hasil[$i]["Waktu"];
				$tunaix = $hasil[$i]["Tunai"];
				$kdebit = $hasil[$i]["KDebit"];

				if($kdebit>0){
					$krt_debt = $hasil[$i]["BankDebet"];
					$edc_debt = $hasil[$i]["EDCBankDebet"];
				}else{
					$krt_debt = "-";
					$edc_debt = "-";
				}

				$kkredit = $hasil[$i]["KKredit"];
				if($kkredit>0){
					$krt_kre = $hasil[$i]["BankKredit"];
					$edc_kre = $hasil[$i]["EDCBankKredit"];
				}else{
					$krt_kre = "-";
					$edc_kre = "-";
				}

				$voucher = $hasil[$i]["Voucher"];
				$gopay = $hasil[$i]["GoPay"];
				$point = $hasil[$i]["Point"];
				$discount = $hasil[$i]["Discount"];
				$bruto = $hasil[$i]["Gross"];
				$totalbayar = $tunaix+$kdebit+$kkredit+$voucher;
				$kembali = $hasil[$i]["Kembali"];
				$tax = $hasil[$i]["Tax"];
				$rounding = $hasil[$i]["Rounding"];
				$totalcharge = $hasil[$i]["ServiceCharge"];
				$netto = $hasil[$i]["Nett"];
				$salesx = $hasil[$i]["Sales"];
				$nettunaix = $tunaix - $kembali;

			
				$pembayaran = $tunai+$kdebit+$kkredit+$voucher+$gopay+$point;
				$grandtotal = $netto + $totalcharge + $tax;	

				if ($rounding >= 50)
				{
					$sales = $salesx + $rounding;
					$nettunai = $nettunaix + $rounding;
					$tunai = $tunaix + $rounding;
					//$grandtotal = $grandtotalx + $rounding; 
				}else{
					$sales = $salesx - $rounding;
					$nettunai = $nettunaix - $rounding;
					$tunai = $tunaix - $rounding;
					//$grandtotal = $grandtotalx - $rounding; 
				}
				
				$jm_pembayaran = (($tunai*1)+($kkredit*1)+($kdebit*1)+($voucher*1)+($gopay*1)+($point*1))-($kembali*1);
				
                $bg_salah="";
                if(abs(($jm_pembayaran*1)-($grandtotal*1))>1)
                {
                    //$bg_salah = "background-color: #eef15f;";
                    
                }
                if($tempsubdivisi != '' and $tempsubdivisi != $subdivisi){
                	$subdivnettunai = $subdivtunai - $subdivkembali;
                	$subdivtotalbayar = $subdivtunai+$subdivkdebit+$subdivkkredit+$subdivvoucher+$subdivgopay+$subdivpoint;
				?>
					<tr class="title_table">
						<td nowrap colspan="9" align="center"><?php echo 'Total SubDivisi ' . $tempsubdivisi; ?></td>
	                    <?php if($otorisasi_diskon_view == 'Y') { ?>
						<td nowrap title="Total = <?php echo ubah_format($subdivbruto); ?>" align="right"><?php echo ubah_format($subdivbruto); ?></td>
						<td nowrap title="Disc/Compliement = <?php echo ubah_format($subdivdiscount); ?>" align="right"><?php echo ubah_format($subdivdiscount); ?></td>
						<?php } ?>
						<td nowrap title="Netto = <?php echo ubah_format($subdivnetto); ?>" align="right"><?php echo ubah_format($subdivnetto); ?></td>
						
						<td nowrap title="Charge = <?php echo ubah_format($subdivtotalcharge); ?>" align="right"><?php echo ubah_format($subdivtotalcharge); ?></td>
						<td nowrap title="Tax = <?php echo ubah_format($subdivtax); ?>" align="right"><?php echo ubah_format($subdivtax); ?></td>	
						<td nowrap title="Rounding = <?php echo ubah_format($subdivrounding); ?>" align="right"><?php echo ubah_format($subdivrounding); ?></td>			
						<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($subdivsales); ?>" align="right"><?php echo ubah_format($subdivsales); ?></td>
						
						<td nowrap title="NetTunai = <?php echo ubah_format($subdivnettunai); ?>" align="right"><?php echo ubah_format($subdivnettunai); ?></td>
						<td nowrap title="Tunai = <?php echo ubah_format($subdivtunai); ?>" align="right" ><?php echo ubah_format($subdivtunai); ?></td>
						
						<td nowrap title="Kredit = <?php echo ubah_format($subdivkkredit); ?>" align="right"><?php echo ubah_format($subdivkkredit); ?></td>
						<td nowrap align="left"><?php echo '-'; ?></td>
						<td nowrap align="left"><?php echo '-'; ?></td>
						
						<td nowrap title="Debit = <?php echo ubah_format($subdivkdebit); ?>" align="right"><?php echo ubah_format($subdivkdebit); ?></td>
						<td nowrap align="left"><?php echo '-'; ?></td>
						<td nowrap align="left"><?php echo '-'; ?></td>
						
						<td nowrap title="GoPay = <?php echo ubah_format($subdivgopay); ?>" align="right"><?php echo ubah_format($subdivgopay); ?></td>
						<td nowrap title="Point = <?php echo ubah_format($subdivpoint); ?>" align="right"><?php echo ubah_format($subdivpoint); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($subdivvoucher); ?>" align="right"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($subdivvoucher); ?></a><?}else{ echo ubah_format($subdivvoucher);}?></td>
						<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($subdivtotalbayar); ?>" align="right"><?php echo ubah_format($subdivtotalbayar); ?></td>
						<td nowrap title="Kembali = <?php echo ubah_format($subdivkembali); ?>" align="right"><?php echo ubah_format($subdivkembali); ?></td>
					</tr>
				<?php
					$subdivbruto = 0;
					$subdivdiscount = 0;
					$subdivnetto = 0;
					$subdivtax = 0;
					$subdivrounding = 0;
					$subdivsales = 0;
					$subdivtunai = 0;
					$subdivkkredit = 0;
					$subdivkdebit = 0;
					$subdivvoucher = 0;
					$subdivkembali = 0;
					$subdivtotalcharge = 0;
					$subdivgopay=0;
					$subdivpoint=0;
				}
				if($tempdivisi != '' and $tempdivisi != $namadivisi){
                	$divnettunai = $divtunai - $divkembali;
                	$divtotalbayar = $divtunai+$divkdebit+$divkkredit+$divvoucher+$divgopay+$divpoint;
				?>
					<tr class="title_table">
						<td nowrap colspan="9" align="center"><?php echo 'Total Divisi ' . $tempdivisi; ?></td>
	                    <?php if($otorisasi_diskon_view == 'Y') { ?>
						<td nowrap title="Total = <?php echo ubah_format($divbruto); ?>" align="right"><?php echo ubah_format($divbruto); ?></td>
						<td nowrap title="Disc/Compliement = <?php echo ubah_format($divdiscount); ?>" align="right"><?php echo ubah_format($divdiscount); ?></td>
						<?php } ?>
						<td nowrap title="Netto = <?php echo ubah_format($divnetto); ?>" align="right"><?php echo ubah_format($divnetto); ?></td>
						
						<td nowrap title="Charge = <?php echo ubah_format($divtotalcharge); ?>" align="right"><?php echo ubah_format($divtotalcharge); ?></td>
						<td nowrap title="Tax = <?php echo ubah_format($divtax); ?>" align="right"><?php echo ubah_format($divtax); ?></td>
						<td nowrap title="Tax = <?php echo ubah_format($divrounding); ?>" align="right"><?php echo ubah_format($divrounding); ?></td>						
						<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($divsales); ?>" align="right"><?php echo ubah_format($divsales); ?></td>
						
						<td nowrap title="NetTunai = <?php echo ubah_format($divnettunai); ?>" align="right"><?php echo ubah_format($divnettunai); ?></td>
						<td nowrap title="Tunai = <?php echo ubah_format($divtunai); ?>" align="right" ><?php echo ubah_format($divtunai); ?></td>
						
						<td nowrap title="Kredit = <?php echo ubah_format($divkkredit); ?>" align="right"><?php echo ubah_format($divkkredit); ?></td>
						<td nowrap align="left"><?php echo ''; ?></td>
						<td nowrap align="left"><?php echo ''; ?></td>

						<td nowrap title="Debit = <?php echo ubah_format($divkdebit); ?>" align="right"><?php echo ubah_format($divkdebit); ?></td>
						<td nowrap align="left"><?php echo ''; ?></td>
						<td nowrap align="left"><?php echo ''; ?></td>

						<td nowrap title="GoPay = <?php echo ubah_format($divgopay); ?>" align="right"><?php echo ubah_format($divgopay); ?></td>
						<td nowrap title="Point = <?php echo ubah_format($divpoint); ?>" align="right"><?php echo ubah_format($divpoint); ?></td>
						<td nowrap title="Voucher = <?php echo ubah_format($divvoucher); ?>" align="right" style="border-right-color: #009999;"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($divvoucher); ?></a><?}else{ echo ubah_format($divvoucher);}?></td>
						<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($divtotalbayar); ?>" align="right"><?php echo ubah_format($divtotalbayar); ?></td>
						<td nowrap title="Kembali = <?php echo ubah_format($divkembali); ?>" align="right"><?php echo ubah_format($divkembali); ?></td>
					</tr>
				<?
					$divbruto = 0;
					$divdiscount = 0;
					$divnetto = 0;
					$divtax = 0;
					$divrounding = 0;
					$divsales = 0;
					$divtunai = 0;
					$divkkredit = 0;
					$divkdebit = 0;
					$divvoucher = 0;
					$divkembali = 0;
					$divtotalcharge = 0;
					$divgopay=0;
					$divpoint=0;
				}
				?>
				
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="<?php echo $bg_salah; ?>">
					<td nowrap align="center" title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo ($i+1); ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $namadivisi; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $subdivisi; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $kasir; ?></td>
					<?php if($v_tipe_cari=="struk"){?>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $mylib->ubah_tanggal($tanggal); ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $waktu; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $nostruk; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $pax; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $nostiker; ?></td>
					<?php } ?>
                    <?php if($otorisasi_diskon_view == 'Y') { ?>
					<td nowrap title="Total = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Disc/Compliement = <?php echo ubah_format($discount); ?>" align="right"><?php echo ubah_format($discount); ?></td>
					<?php } ?>
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>
					
					<td nowrap title="Charge = <?php echo ubah_format($totalcharge); ?>" align="right"><?php echo ubah_format($totalcharge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($tax); ?>" align="right"><?php echo ubah_format($tax); ?></td>
					<td nowrap title="Rounding = <?php echo ubah_format($rounding); ?>" align="right"><?php echo ubah_format($rounding); ?></td>								
					<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($sales); ?>" align="right"><?php echo ubah_format($sales); ?></td>
					
					<td nowrap title="NetTunai = <?php echo ubah_format($nettunai); ?>" align="right"><?php echo ubah_format($nettunai); ?></td>
					<td nowrap title="Tunai = <?php echo ubah_format($tunai); ?>" align="right" ><?php echo ubah_format($tunai); ?></td>
					
					<td nowrap title="Kredit = <?php echo ubah_format($kkredit); ?>" align="right"><?php echo ubah_format($kkredit); ?></td>
					<td nowrap align="center"><?php echo $krt_kre; ?></td>
					<td nowrap align="center"><?php echo $edc_kre; ?></td>

					<td nowrap title="Debit = <?php echo ubah_format($kdebit); ?>" align="right"><?php echo ubah_format($kdebit); ?></td>
					<td nowrap align="center"><?php echo $krt_debt; ?></td>
					<td nowrap align="center"><?php echo $krt_debt; ?></td>

					<td nowrap title="GoPay = <?php echo ubah_format($gopay); ?>" align="right"><?php echo ubah_format($gopay); ?></td>
					<td nowrap title="Point = <?php echo ubah_format($point); ?>" align="right"><?php echo ubah_format($point); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($voucher); ?>" align="right" style="border-right-color: #009999;"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($voucher); ?></a><?}else{ echo ubah_format($voucher);}?></td>
					<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($totalbayar); ?>" align="right"><?php echo ubah_format($totalbayar); ?></td>
					<td nowrap title="Kembali = <?php echo ubah_format($kembali); ?>" align="right"><?php echo ubah_format($kembali); ?></td>
					<?
					if($num_hasil>0){
						$link = base_url()."/inventory/form_otorisasi.php?kdtransaksi=R&tipe=kasir&notransaksi=$nostruk";
					?>
						<td rowspan title='Action' align='center'><img src='<?= base_url(); ?>public/images/flag_orange.gif' border='0' title='Void' onclick="VoidTrans('<? echo $link;?>');"></img></td>
					<?
					}
					?>
				</tr>
				<?php
	
				$subdivbruto += $bruto;
				$subdivdiscount += $discount;
				$subdivnetto += $netto;
				$subdivtax += $tax;
				$subdivrounding += $rounding;
				$subdivsales += $sales;
				$subdivtunai += $tunai;
				$subdivkkredit += $kkredit;
				$subdivkdebit += $kdebit;
				$subdivvoucher += $voucher;
				$subdivkembali += $kembali;
				$subdivtotalcharge += $totalcharge;
				$subdivgopay += $gopay;
				$subdivpoint += $point;
				
				$divbruto += $bruto;
				$divdiscount += $discount;
				$divnetto += $netto;
				$divtax += $tax;
				$divrounding += $rounding;
				$divsales += $sales;
				$divtunai += $tunai;
				$divkkredit += $kkredit;
				$divkdebit += $kdebit;
				$divvoucher += $voucher;
				$divkembali += $kembali;
				$divtotalcharge += $totalcharge;
				$divpoint += $point;
				$divgopay += $gopay;
				
				$gtbruto += $bruto;
				$gtdiscount += $discount;
				$gtnetto += $netto;
				$gttax += $tax;
				$gtrounding += $rounding;
				$gtsales += $sales;
				$gttunai += $tunai;
				$gtkkredit += $kkredit;
				$gtkdebit += $kdebit;
				$gtvoucher += $voucher;
				$gtkembali += $kembali;
				$gttotalcharge += $totalcharge;
				$gtgopay += $gopay;
				$gtpoint += $point;
				
				$tempdivisi = $namadivisi;
				$tempsubdivisi = $subdivisi;
			}
			?>
			
			<?php
			if(count($hasil)>0)
			{
				$subdivnettunai = $subdivtunai - $subdivkembali;
                $subdivtotalbayar = $subdivtunai+$subdivkdebit+$subdivkkredit+$subdivvoucher+$subdivgopay+$subdivpoint;
                $divnettunai = $divtunai - $divkembali;
                $divtotalbayar = $divtunai+$divkdebit+$divkkredit+$divvoucher+$divgopay+$divpoint;
                $gtnettunai = $gttunai - $gtkembali;
                $gttotalbayar = $gttunai+$gtkdebit+$gtkkredit+$gtvoucher+$gtgopay+$gtpoint;
				?>
				<tr class="title_table">
				<?php if($v_tipe_cari=="struk"){
					$jarak = "9";
					}else{
					$jarak = "4";	
					}?>
					<td nowrap colspan="<?=$jarak;?>" align="center"><?php echo 'Sub Total ' . $tempsubdivisi; ?></td>
                    <?php if($otorisasi_diskon_view == 'Y') { ?>
					<td nowrap title="Total = <?php echo ubah_format($subdivbruto); ?>" align="right"><?php echo ubah_format($subdivbruto); ?></td>
					<td nowrap title="Disc/Compliement = <?php echo ubah_format($subdivdiscount); ?>" align="right"><?php echo ubah_format($subdivdiscount); ?></td>
					<?php } ?>
					<td nowrap title="Netto = <?php echo ubah_format($subdivnetto); ?>" align="right"><?php echo ubah_format($subdivnetto); ?></td>
					
					<td nowrap title="Charge = <?php echo ubah_format($subdivtotalcharge); ?>" align="right"><?php echo ubah_format($subdivtotalcharge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($subdivtax); ?>" align="right"><?php echo ubah_format($subdivtax); ?></td>	
					<td nowrap title="Rounding = <?php echo ubah_format($subdivrounding); ?>" align="right"><?php echo ubah_format($subdivrounding); ?></td>						
					<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($subdivsales); ?>" align="right"><?php echo ubah_format($subdivsales); ?></td>
					
					<td nowrap title="NetTunai = <?php echo ubah_format($subdivnettunai); ?>" align="right"><?php echo ubah_format($subdivnettunai); ?></td>
					<td nowrap title="Tunai = <?php echo ubah_format($subdivtunai); ?>" align="right" ><?php echo ubah_format($subdivtunai); ?></td>
					
					<td nowrap title="Kredit = <?php echo ubah_format($subdivkkredit); ?>" align="right"><?php echo ubah_format($subdivkkredit); ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>

					<td nowrap title="Debit = <?php echo ubah_format($subdivkdebit); ?>" align="right"><?php echo ubah_format($subdivkdebit); ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>

					<td nowrap title="Gopay = <?php echo ubah_format($subdivgopay); ?>" align="right"><?php echo ubah_format($subdivgopay); ?></td>
					<td nowrap title="Point = <?php echo ubah_format($subdivpoint); ?>" align="right"><?php echo ubah_format($subdivpoint); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($subdivvoucher); ?>" align="right"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($subdivvoucher); ?></a><?}else{ echo ubah_format($subdivvoucher);}?></td>
					<td nowrap title="Total Bayar = <?php echo ubah_format($subdivtotalbayar); ?>" align="right"><?php echo ubah_format($subdivtotalbayar); ?></td>
					<td nowrap title="Kembali = <?php echo ubah_format($subdivkembali); ?>" align="right"><?php echo ubah_format($subdivkembali); ?></td>
				</tr>
				<tr class="title_table">
				<?php if($v_tipe_cari=="struk"){
					$jarak = "9";
					}else{
					$jarak = "4";	
					}?>
					<td nowrap colspan="<?=$jarak;?>" align="center"><?php echo 'Total ' . $tempdivisi; ?></td>
                    <?php if($otorisasi_diskon_view == 'Y') { ?>
					<td nowrap title="Total = <?php echo ubah_format($divbruto); ?>" align="right"><?php echo ubah_format($divbruto); ?></td>
					<td nowrap title="Disc/Compliement = <?php echo ubah_format($divdiscount); ?>" align="right"><?php echo ubah_format($divdiscount); ?></td>
					<?php } ?>
					<td nowrap title="Netto = <?php echo ubah_format($divnetto); ?>" align="right"><?php echo ubah_format($divnetto); ?></td>
					
					<td nowrap title="Charge = <?php echo ubah_format($divtotalcharge); ?>" align="right"><?php echo ubah_format($divtotalcharge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($divtax); ?>" align="right"><?php echo ubah_format($divtax); ?></td>	
					<td nowrap title="Rounding = <?php echo ubah_format($divrounding); ?>" align="right"><?php echo ubah_format($divrounding); ?></td>						
					<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($divsales); ?>" align="right"><?php echo ubah_format($divsales); ?></td>
					
					<td nowrap title="NetTunai = <?php echo ubah_format($divnettunai); ?>" align="right"><?php echo ubah_format($divnettunai); ?></td>
					<td nowrap title="Tunai = <?php echo ubah_format($divtunai); ?>" align="right" ><?php echo ubah_format($divtunai); ?></td>
					
					<td nowrap title="Kredit = <?php echo ubah_format($divkkredit); ?>" align="right"><?php echo ubah_format($divkkredit); ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>

					<td nowrap title="Debit = <?php echo ubah_format($divkdebit); ?>" align="right"><?php echo ubah_format($divkdebit); ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>

					<td nowrap title="GoPay = <?php echo ubah_format($divgopay); ?>" align="right"><?php echo ubah_format($divgopay); ?></td>
					<td nowrap title="Point = <?php echo ubah_format($divpoint); ?>" align="right"><?php echo ubah_format($divpoint); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($divvoucher); ?>" align="right"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($divvoucher); ?></a><?}else{ echo ubah_format($divvoucher);}?></td>
					<td nowrap title="Total Bayar = <?php echo ubah_format($divtotalbayar); ?>" align="right"><?php echo ubah_format($divtotalbayar); ?></td>
					<td nowrap title="Kembali = <?php echo ubah_format($divkembali); ?>" align="right"><?php echo ubah_format($divkembali); ?></td>
				</tr>
				<tr class="title_table">
				<?php if($v_tipe_cari=="struk"){
					$jarak = "9";
					}else{
					$jarak = "4";	
					}?>
				
					<td nowrap colspan="<?=$jarak;?>" align="center"><?php echo 'Grand Total '; ?></td>
                    <?php if($otorisasi_diskon_view == 'Y') { ?>
					<td nowrap title="Total = <?php echo ubah_format($gtbruto); ?>" align="right"><?php echo ubah_format($gtbruto); ?></td>
					<td nowrap title="Disc/Compliement = <?php echo ubah_format($gtdiscount); ?>" align="right"><?php echo ubah_format($gtdiscount); ?></td>
					<?php } ?>
					<td nowrap title="Netto = <?php echo ubah_format($gtnetto); ?>" align="right"><?php echo ubah_format($gtnetto); ?></td>
					
					<td nowrap title="Charge = <?php echo ubah_format($gttotalcharge); ?>" align="right"><?php echo ubah_format($gttotalcharge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($gttax); ?>" align="right"><?php echo ubah_format($gttax); ?></td>
					<td nowrap title="Rounding = <?php echo ubah_format($gtrounding); ?>" align="right"><?php echo ubah_format($gtrounding); ?></td>							
					<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($gtsales); ?>" align="right"><?php echo ubah_format($gtsales); ?></td>
					
					<td nowrap title="NetTunai = <?php echo ubah_format($gtnettunai); ?>" align="right"><?php echo ubah_format($gtnettunai); ?></td>
					<td nowrap title="Tunai = <?php echo ubah_format($gttunai); ?>" align="right" ><?php echo ubah_format($gttunai); ?></td>
					
					<td nowrap title="Kredit = <?php echo ubah_format($gtkkredit); ?>" align="right"><?php echo ubah_format($gtkkredit); ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>

					<td nowrap title="Debit = <?php echo ubah_format($gtkdebit); ?>" align="right"><?php echo ubah_format($gtkdebit); ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>
					<td nowrap align="left"><?php echo ''; ?></td>

					<td nowrap title="GoPay = <?php echo ubah_format($gtgopay); ?>" align="right"><?php echo ubah_format($gtgopay); ?></td>
					<td nowrap title="Point = <?php echo ubah_format($gtpoint); ?>" align="right"><?php echo ubah_format($gtpoint); ?></td>
					<td nowrap title="Voucher = <?php echo ubah_format($gtvoucher); ?>" align="right"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($gtvoucher); ?></a><?}else{ echo ubah_format($gtvoucher);}?></td>
					<td nowrap title="Total Bayar = <?php echo ubah_format($gttotalbayar); ?>" align="right"><?php echo ubah_format($gttotalbayar); ?></td>
					<td nowrap title="Kembali = <?php echo ubah_format($gtkembali); ?>" align="right"><?php echo ubah_format($gtkembali); ?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
<script text='javascript'>
function VoidTrans(param){
	//if (confirm("Anda yakin ingin menghapus transaksi ini?")){
		window.open(param,'','width=400,height=300,left=400,top=250');
	//}
}
</script>