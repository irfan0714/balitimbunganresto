<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Penjualan kasir";

if($v_pilihan=="transaksi"){
	$check1 = "checked";
	$check2 = "";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}
elseif($v_pilihan=="detail"){
	$check1 = "";
	$check2 = "checked";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}

elseif($v_pilihan=="barang"){
	$check1 = "";
	$check2 = "";
	$check3 = "checked";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}
elseif($v_pilihan=="dpp"){
	$check1 = "";
	$check2 = "";
	$check3 = "";
	$check4 = "checked";
	$check5 = "";
	$check6 = "";

}
elseif($v_pilihan=="tanggal"){
	$check1 = "";
	$check2 = "";
	$check3 = "";
	$check4 = "";
	$check5 = "checked";
	$check6 = "";

}
elseif($v_pilihan=="sticker"){
	$check1 = "";
	$check2 = "";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "checked";

}
else{
	$check1 = "checked";
	$check2 = "";
	$check3 = "";
	$check4 = "";
	$check5 = "";
	$check6 = "";

}

if($nostiker=="Y"){
	$yups = "checked";
}

foreach($mkasir as $val)
{
	$arr_data["kasir"][$val["UserName"]] = $val["UserName"];
    $arr_data["employee_name"][$val["UserName"]] = $val["employee_name"];
}

foreach($mkasirfromheader as $val)
{
	$arr_data["kasir"][$val["Kasir"]] = $val["Kasir"];
    $arr_data["employee_name"][$val["Kasir"]] = $val["employee_name"];
}

foreach($arr_data["kasir"] as $kasir => $val)
{
	if($userlevel==-1 || $userlevel==13 || $userlevel==15 || $userlevel==29 )
	{
		$arr_data["all_kasir"][$kasir]=$kasir;
	}
	else
	{
		$arr_data["all_kasir"][$username] = $kasir;
	}
}

//echo "NAMA : $username";

?>

<script>

		function ShowSticker(){
			var a = $("#stk").css("display");
			if(a=="none"){
				$("#stk").css("display","none1");
				// alert("SHOW");
			}else{
				$("#stk").css("display","none");
			}
		}

    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }

    function PilihReport(jenis){
    	$('select').children().remove();
		if(jenis=='barang'){
			var url = $("#base_url").val();
			$.ajax({
					url: url+"index.php/report/report_kasir/subdivisi/",
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
						$('#v_divisi').html(res);
					},
					error: function(e)
					{
						alert(e);
					}
			});
		}else{
			var url = $("#base_url").val();
			$.ajax({
					url: url+"index.php/report/report_kasir/divisi/",
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
						$('#v_divisi').html(res);
					},
					error: function(e)
					{
						alert(e);
					}
			});
		}
	}
</script>

<div class="row">
    <div class="col-md-12" align="left">

    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>

		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_kasir/search_report/" onsubmit="return false">

	    <table class="table table-bordered responsive">

			<tr>
	        	<td class="title_table" width="150">Rekap Per-</td>
	        	<td>
                    <div class="radio" style="float:left; width:100px; margin-top:0px;" >
                        <label><input type="radio" name="v_pilihan" id="v_pilihan"  value='transaksi' <? echo $check1;?> /> Transaksi</label>
                    </div>
                    <div class="radio" style="float:left;  width:100px; margin-top:0px;">
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='detail' <? echo $check2;?> /> Detail</label>
					 </div>
                     <div class="radio" style="float:left;  width:100px; margin-top:0px;">
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='barang' <? echo $check3;?> /> Barang</label>
	        	     </div>
					 <?
					 if($username=="trisno1402"||$username=="frangky2311"||$username=="william1001"||$username=="wieok3110"||$username=="tonny1205"){
					 ?>
					 <div class="radio" style="float:left;  width:100px; margin-top:0px;">
					    <label><input type="radio" name="v_pilihan" id="v_pilihan"  value='dpp' <? echo $check4;?> /> Detail DPP</label>
					 </div>
					 <?
					 }
					 ?>
					 <div class="radio" style="float:left;  width:100px; margin-top:0px;">
					    <label><input type="radio" name="v_pilihan" id="v_pilihan"  value='tanggal' <? echo $check5;?> /> Tanggal</label>
	        	     </div>
								 <div class="radio" style="float:left; width:100px; margin-top:0px;" >
										 <label><input type="radio" name="v_pilihan" id="v_pilihan" onclick="ShowSticker();" value='sticker' <? echo $check6;?> /> Sticker</label>
								 </div>
                </td>
	        </tr>

	        <tr>
	            <td class="title_table" width="150">Periode</td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>

	        <tr>
	        	<td class="title_table" width="150">No Struk</td>
	        	<td>
	        		<input type="text" class="form-control-new" value="<?php echo $v_nostruk; ?>" name="v_nostruk" id="v_nostruk" size="34">
	        	</td>
	        </tr>
					<tr id="stk" style="display:none;">
					 <td class="title_table" width="150">No Sticker</td>
					 <td>
						 <input type="text" class="form-control-new" value="<?php echo $v_sticker; ?>" name="v_sticker" id="v_sticker" size="34">
					 </td>
				 </tr>

	        <tr>
	            <td class="title_table">Divisi</td>
	            <td>
	            	<select class="form-control-new" name="v_divisi" id="v_divisi" style="width: 200px;">

	            		<?php
	            		if($userlevel==-1 || $userlevel==13 || $userlevel==15 || $userlevel==16 || $userlevel==29)
						{
	            		?>
	            		<option value="">All</option>
	            		<?php
	            		}

	            		foreach($mtype as $val)
	            		{
	            			$selected="";
	            			if($v_divisi==$val["KdDivisi"])
	            			{
								$selected = "selected='selected'";
							}

							?><option <?php echo $selected ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">Kasir</td>
	            <td>
	            	<select class="form-control-new" name="v_username" id="v_username" style="width: 200px;">
	            		<option value="">All</option>
	            		<?php
	            		foreach($arr_data["all_kasir"] as $kasir => $val)
	            		{
                            $employee_name = $arr_data["employee_name"][$kasir];

	            			$selected="";
	            			if($v_username==$kasir)
	            			{
								$selected = "selected='selected'";
							}

							?><option <?php echo $selected ?> value="<?php echo $kasir; ?>"><?php echo $kasir." :: ".$employee_name; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">Tipe Pencarian</td>
	            <td>
	            	<select class="form-control-new" name="v_tipe_cari" id="v_tipe_cari" style="width: 200px;">
	            		<option <?php if($v_tipe_cari=="struk"){ echo "selected='selected'"; } ?> value="struk">Struk</option>
	            		<option <?php if($v_tipe_cari=="kasir"){ echo "selected='selected'"; } ?> value="kasir">Kasir</option>
	            	</select>
	            </td>
	        </tr>

			<tr>
				<td class="title_table"> &nbsp;</td>
				<td>
	            	<div class="checkbox">
								<label>
									<input type="checkbox" id="nostiker" name="nostiker" <? echo $yups;?>>No Stiker / Per Rombongan
								</label>
							</div>

	            </td>
			</tr>


	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>

	    </table>
	    </form>
	</div>
</div>

<?php
if ($tampilkanDT)
{
	/*
	echo $v_pilihan;echo "<br>";
echo $excel;echo "<br>";
echo $nostiker;echo "<br>";
				echo "v_pilihan";
				die;
	*/

	if($v_pilihan=="transaksi"){
		 		if($nostiker =='Y'){
	                $this->load->view("report/penjualan/kasir/tampilAgen", $data);
				}else{
					$this->load->view("report/penjualan/kasir/tampil", $data);
				}
	}
	elseif($v_pilihan=="detail"){
		 $this->load->view("report/penjualan/kasir/tampil_detail");
	}
	elseif($v_pilihan=="barang"){
		 $this->load->view("report/penjualan/kasir/tampil_barang");
	}
	elseif($v_pilihan=="dpp"){
		 $this->load->view("report/penjualan/kasir/tampil_dpp");
	}
	elseif($v_pilihan=="tanggal"){
		 $this->load->view("report/penjualan/kasir/tampil_tanggal");
	}
	elseif($v_pilihan=="sticker"){
		if($nostiker =='Y'){
							 $this->load->view("report/penjualan/kasir/tampilAgen", $data);
		 }else{
			 $this->load->view("report/penjualan/kasir/tampil", $data);
		 }
	}

}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
