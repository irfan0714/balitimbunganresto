<?php
$this->load->view('header');
$reportlib = new report_lib();
?>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
    <script language="javascript">
        function submitThis() {
            if ($("#bulan1").val() == "" || $("#bulan2").val() == "" || $("#tahun2").val() == "") {
                alert("Periode harus di pilih.");
            }
            else {
                $("#excel").val("");
                $("#print").val("");
                document.getElementById("search").submit();
            }
        }

    </script>
    <body>
    <p>

    <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/neracasaldo/cari/"
          onsubmit="return false">
        <table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
            <tr>
                <td>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tr>
                            <td nowrap>Periode</td>
                            <td nowrap>:</td>
                            <td nowrap>
                                <select size="1" id="bulan1" name="bulan1">
                                    <?php
                                    for ($a = 0; $a < count($bulan1); $a++) {
                                        $select = "";
                                        if ($bulanaktif == $bulan1[$a]) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?> value="<?= $bulan1[$a] ?>"><?= $bulan1[$a] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                            <td nowrap>s/d</td>
                            <td>
                                <select size="1" id="bulan2" name="bulan2">
                                    <?php
                                    for ($a = 0; $a < count($bulan2); $a++) {
                                        $select = "";
                                        if ($bulanaktif2 == $bulan2[$a]) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?> value="<?= $bulan2[$a] ?>"><?= $bulan2[$a] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <select size="1" id="tahun2" name="tahun2">
                                    <?php
                                    for ($a = 0; $a < count($tahun2); $a++) {
                                        $select = "";
                                        if ($tahunaktif == $tahun2[$a]['Tahun']) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?>
                                            value="<?= $tahun2[$a]['Tahun'] ?>"><?= $tahun2[$a]['Tahun'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        echo $reportlib->write_plain_combo("Dept", "dept", $listdept, $dept, "KdDepartemen", "NamaDepartemen", 3);
                        ?>
                        <tr>
                            <td nowrap>Cetak Tingkat</td>
                            <td nowrap>:</td>
                            <td nowrap>
                                <select size="1" id="tingkat" name="tingkat">
                                    <?php
                                    for ($a = 0; $a < count($listtingkat); $a++) {
                                        $select = "";
                                        if ($tingkat == $listtingkat[$a]) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?>
                                            value="<?= $listtingkat[$a] ?>"><?= $listtingkat[$a] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap>Cetak Mutasi</td>
                            <td nowrap>:</td>
                            <td nowrap>
                                <select size="1" id="mutasi" name="mutasi">
                                    <?php
                                    for ($a = 0; $a < count($listmutasi); $a++) {
                                        $select = "";
                                        if ($mutasi == $listmutasi[$a]) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?>
                                            value="<?= $listmutasi[$a] ?>"><?= $listmutasi[$a] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap>Cetak Budget</td>
                            <td nowrap>:</td>
                            <td nowrap>
                                <select size="1" id="budget" name="budget">
                                    <?php
                                    for ($a = 0; $a < count($listbudget); $a++) {
                                        $select = "";
                                        if ($budget == $listbudget[$a]) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?>
                                            value="<?= $listbudget[$a] ?>"><?= $listbudget[$a] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr bordercolor="#FFFFFF">
                <td colspan="2" align="center"><input type="button" value="Search (*)" onclick="submitThis()">
                    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                    <input type='hidden' value='<?= $userlevel ?>' id="userlevel" name="userlevel">
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
                </td>
            </tr>
        </table>
    </form>
    </body>
<?php
if ($tampilkanRT) {
    $this->load->view("report/neracasaldo/reportRT");
}
$this->load->view('footer'); ?>