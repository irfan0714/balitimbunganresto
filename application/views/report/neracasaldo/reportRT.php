<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/neracasaldo/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportneracasaldo.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');document.getElementById('search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Neraca Saldo</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
	    <th align="center" bgcolor="#f3f7bb">Rekening</th>
		<th align="center" bgcolor="#f3f7bb">Nama Rekening</th>
		<?php
		if($mutasi=="Ya")
		{?>
		<th align="center" bgcolor="#f3f7bb">Saldo Awal</th>
		<th align="center" bgcolor="#f3f7bb">Debit</th>
		<th align="center" bgcolor="#f3f7bb">Kredit</th>
		<?php
		}
		?>
		<th align="center" bgcolor="#f3f7bb">Saldo Akhir</th>
		<?php
		if($budget=="Ya")
		{?>
		<th align="center" bgcolor="#f3f7bb">Budget</th>
		<?php
		}
		?>
<?php
	if(count($hasil1)>0){
	    $total1 = 0;
		$total2 = 0;
		$total3 = 0;
		$total4 = 0;
		$total5 = 0;
		$parent0 = "";
		$parent1 = "";
		$posisi  = "";
		$namaposisi = "";
		$namaparent0 = "";
		$namaparent1 = "";
		$subtotal11 = 0;$subtotal12 = 0;$subtotal13 = 0;$subtotal14 = 0;$subtotal15 = 0;
		$subtotal21 = 0;$subtotal22 = 0;$subtotal23 = 0;$subtotal24 = 0;$subtotal25 = 0;
		$subposisi1 = 0;$subposisi2 = 0;$subposisi3 = 0;$subposisi4 = 0;$subposisi5 = 0;
		$nilbudget = 0;
		for($s=0;$s<count($hasil1);$s++)
		{
			$akhir = (float)$hasil1[$s]['awal']+(float)$hasil1[$s]['debit']-(float)$hasil1[$s]['kredit'];
			$total1 += (float)$hasil1[$s]['awal'];
			$total2 += (float)$hasil1[$s]['debit'];
			$total3 += (float)$hasil1[$s]['kredit'];
			$total4 += $akhir;
			if($hasil1[$s]['posisi']>"02")
			{
				$nilbudget = (float)$hasil1[$s]['budget'];
			
			}else
			{
				$nilbudget = (float)$hasil1[$s]['budget0'];
			}
			$total5 += $nilbudget;
			if($parent1<>$hasil1[$s]['parent1'])
			{
			   if($parent1<>""&&$tingkat<>"Satu")
			   {
				   ?>
				     <tr>
						<td nowrap bgcolor="#EEEEE1"><?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$parent1?></td>
						<td nowrap bgcolor="#EEEEE1"><?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;". $namaparent1?></td>
						<?php
    						if($mutasi=="Ya")
						{?>
						<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal21)?></td>
						<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal22)?></td>
						<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal23)?></td>
						<?php } ?>
						<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal24)?></td>
						<?php
						if($budget=="Ya")
						{?>
						<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal25)?></td>
						<?php
						}
						?>
					 </tr>
				   <?php
			   }
			   $subtotal21 = 0;$subtotal22 = 0;$subtotal23 = 0;$subtotal24 = 0;$subtotal25 = 0;
			   $parent1 = $hasil1[$s]['parent1'];
			   $namaparent1 = $hasil1[$s]['namaparent1'];
			}
			if($parent0<>$hasil1[$s]['parent0'])
			{
			   if($parent0<>"")
			   {
				   ?>
				     <tr>
						<td nowrap bgcolor="#C8C8C8"><?=$parent0?></td>
						<td nowrap bgcolor="#C8C8C8"><?=$namaparent0?></td>
						<?php
    						if($mutasi=="Ya")
							{?>
						<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal11)?></td>
						<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal12)?></td>
						<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal13)?></td>
						<?php } ?>
						<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal14)?></td>
						<?php
						if($budget=="Ya")
						{?>
						<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal15)?></td>
						<?php
						}
						?>
					 </tr>
				   <?php
			   }
			   $subtotal11 = 0;$subtotal12 = 0;$subtotal13 = 0;$subtotal14 = 0;$subtotal15 = 0;
			   $parent0 = $hasil1[$s]['parent0'];
			   $namaparent0 = "TOTAL ".$hasil1[$s]['namaparent0'];
			}
			/*
			if($posisi<>$hasil1[$s]['posisi'])
			{
			   if($posisi<>"")
			   {
				   ?>
				     <tr>
						<td nowrap align='center' bgcolor='#1CCCCC' colspan="2"><b><?=$namaposisi?></b></td>
						<?php
    						if($mutasi=="Ya")
							{?>
						<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi1)?></td>
						<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi2)?></td>
						<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi3)?></td>
						<?php } ?>
						<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi4)?></td>
						<?php
						if($budget=="Ya")
						{?>
						<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi5)?></td>
						<?php
						}
						?>
					 </tr>
				   <?php
			   }
			   $subposisi1 = 0;$subposisi2 = 0;$subposisi3 = 0;$subposisi4 = 0;$subposisi5 = 0;
			   $posisi = $hasil1[$s]['posisi'];
			   IF ($posisi=="01") $namaposisi = "TOTAL AKTIVA";
			   IF ($posisi=="02") $namaposisi = "TOTAL PASIVA";
			   IF ($posisi=="03") $namaposisi = "TOTAL MODAL";
			   IF ($posisi=="04") $namaposisi = "TOTAL PENJUALAN HPP";
			   IF ($posisi=="05") $namaposisi = "TOTAL BIAYA";
			}
			*/
			$subtotal11 += (float)$hasil1[$s]['awal'];
			$subtotal12 += (float)$hasil1[$s]['debit'];
			$subtotal13 += (float)$hasil1[$s]['kredit'];
			$subtotal14 += $akhir;
    		$subtotal21 += (float)$hasil1[$s]['awal'];
			$subtotal22 += (float)$hasil1[$s]['debit'];
			$subtotal23 += (float)$hasil1[$s]['kredit'];
			$subtotal24 += $akhir;
			$subposisi1 += (float)$hasil1[$s]['awal'];
			$subposisi2 += (float)$hasil1[$s]['debit'];
			$subposisi3 += (float)$hasil1[$s]['kredit'];
			$subposisi4 += $akhir;

			$subtotal15 += $nilbudget;
			$subtotal25 += $nilbudget;
			$subposisi5 += $nilbudget;

			if($tingkat=="Tiga")
			{
	?>
			<tr>
				<td nowrap bgcolor="#FFFFFF"><?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$hasil1[$s]['kdrekening']?></td>
				<td nowrap bgcolor="#FFFFFF"><?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$hasil1[$s]['namarekening']?></td>
				<?php
					if($mutasi=="Ya")
					{?>
				<td nowrap align='right' bgcolor="#FFFFFF"><?=$mylib->ubah_format($hasil1[$s]['awal'])?></td>
				<td nowrap align='right' bgcolor="#FFFFFF"><?=$mylib->ubah_format($hasil1[$s]['debit'])?></td>
				<td nowrap align='right' bgcolor="#FFFFFF"><?=$mylib->ubah_format($hasil1[$s]['kredit'])?></td>
				<?php } ?>
				<td nowrap align='right' bgcolor="#FFFFFF"><?=$mylib->ubah_format($akhir)?></td>
				<?php
				if($budget=="Ya")
				{?>
				<td nowrap align='right' bgcolor="#FFFFFF"><?=$mylib->ubah_format($nilbudget)?></td>
				<?php
				}
				?>
			</tr>
	<?php
			}
	    }
		if($parent1<>""&&$tingkat<>"Satu")
	    {
		   ?>
			 <tr>
				<td nowrap bgcolor="#EEEEE1"><?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$parent1?></td>
				<td nowrap bgcolor="#EEEEE1"><?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$namaparent1?></td>
				<?php
					if($mutasi=="Ya")
					{?>
				<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal21)?></td>
				<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal22)?></td>
				<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal23)?></td>
				<?php } ?>
				<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal24)?></td>
				<?php
				if($budget=="Ya")
				{?>
				<td nowrap align='right' bgcolor="#EEEEE1"><?=$mylib->ubah_format($subtotal25)?></td>
				<?php
				}
				?>
			 </tr>
		   <?php
	    }
		if($parent0<>"")
	    {
		   ?>
			 <tr>
				<td nowrap bgcolor="#C8C8C8"><?=$parent0?></td>
				<td nowrap bgcolor="#C8C8C8"><?=$namaparent0?></td>
				<?php
					if($mutasi=="Ya")
					{?>
				<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal11)?></td>
				<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal12)?></td>
				<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal13)?></td>
				<?php } ?>
				<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal14)?></td>
				<?php
				if($budget=="Ya")
				{?>
				<td nowrap align='right' bgcolor="#C8C8C8"><?=$mylib->ubah_format($subtotal15)?></td>
				<?php
				}
				?>
			 </tr>
		   <?php
	    }
		if($posisi<>"")
	    {
		   ?>
			 <tr>
			    <td nowrap align='center' bgcolor='#1CCCCC' colspan="2"><b><?=$namaposisi?></b></td>
				<?php
					if($mutasi=="Ya")
					{?>
				<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi1)?></td>
				<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi2)?></td>
				<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi3)?></td>
				<?php } ?>
				<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi4)?></td>
				<?php
				if($budget=="Ya")
				{?>
				<td nowrap align='right' bgcolor="#1CCCCC"><?=$mylib->ubah_format($subposisi5)?></td>
				<?php
				}
				?>
			 </tr>
		   <?php
	    }
	    
		?>
		<tr>
			<td nowrap align='center' bgcolor='#1CCCCC' colspan="2"><b>TOTAL</b></td>
			<?php
				if($mutasi=="Ya")
				{?>
			<td nowrap align='right' bgcolor='#1CCCCC'><b><?=$mylib->ubah_format($total1)?></b></td>
			<td nowrap align='right' bgcolor='#1CCCCC'><b><?=$mylib->ubah_format($total2)?></b></td>
			<td nowrap align='right' bgcolor='#1CCCCC'><b><?=$mylib->ubah_format($total3)?></b></td>
			<?php } ?>
			<td nowrap align='right' bgcolor='#1CCCCC'><b><?=$mylib->ubah_format($total4)?></b></td>
			<?php
			if($budget=="Ya")
			{?>
			<td nowrap align='right' bgcolor='#1CCCCC'><b><?=$mylib->ubah_format($total5)?></b></td>
			<?php
			}
			?>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="<?=$colspan?>"><b>Tidak ada data</b></td>
	</tr>
	<?php
	}
	?>
</table>
</form>