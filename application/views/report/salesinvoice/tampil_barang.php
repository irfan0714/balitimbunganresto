<?php
$mylib = new globallib();

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportinvoice.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
                <tr class="title_table">
                    <td style="vertical-align: middle; text-align: center;">PCode</td>
                    <td style="vertical-align: middle; text-align: center;">Nama Barang</td>
                    <td style="vertical-align: middle; text-align: center;">Qty</td>
                    <td style="vertical-align: middle; text-align: center;">Harga</td>
                    <td style="vertical-align: middle; text-align: center;">DPP</td>
                    <td style="vertical-align: middle; text-align: center;">PPN</td>
                    <td style="vertical-align: middle; text-align: center;">Total</td>
                </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}else{
				$tdpp = 0;
				$tppn = 0;
				$tvtotal = 0;
				for($i=0;$i<count($hasil);$i++){
					$pcode = $hasil[$i]['inventorycode'];
					$namabarang = $hasil[$i]['NamaLengkap'];
					$quantity = $hasil[$i]['Qty'];
					$sellprice = $hasil[$i]['Price'];
					$dpp = $quantity * $hasil[$i]['Price'];
					$ppn = $dpp * 0.1;
					$vtotal = $dpp + $ppn;
					$tdpp += $dpp;
					$tppn += $ppn;
					$tvtotal += $vtotal;
					?>
					<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
						<td nowrap align="left" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=$pcode; ?></td>
						<td nowrap align="left" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=$namabarang; ?></td>
						<td nowrap align="right" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=ubah_format($quantity); ?></td>
						<td nowrap align="right" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=ubah_format($sellprice); ?></td>
						<td nowrap align="right" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=ubah_format($dpp); ?></td>
						<td nowrap align="right" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=ubah_format($ppn); ?></td>
						<td nowrap align="right" title="<?php echo $pcode." :: ".$namabarang; ?>"><?=ubah_format($vtotal); ?></td>
					</tr>
				<?php
				}
				?>
				<td nowrap align="center" colspan="4">TOTAL</td>
				<td nowrap align="right"><?=ubah_format($tdpp); ?></td>
				<td nowrap align="right"><?=ubah_format($tppn); ?></td>
				<td nowrap align="right"><?=ubah_format($tvtotal); ?></td>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>