<?php
$this->load->view('header');
$reportlib = new report_lib();
$mylib = new globallib();

$modul = "Sales Invoice";
if($v_pilihan=="barang"){
	
	$check1 = "";
	$check2 = "checked";
}
else{
	$check1 = "checked";
	$check2 = "";
}

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_sales_invoice/search_report/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	    	<tr>
	    		<td class="title_table" width="150">Jenis Laporan</td>
	        	<td>     
                    <div class="radio" style="float:left; width:100px; margin-top:0px;" >
                        <label><input type="radio" name="v_pilihan" id="v_pilihan" value='transaksi' <? echo $check1;?> /> Transaksi</label>
                    </div>     
                     <div class="radio" style="float:left;  width:100px; margin-top:0px;"> 
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='barang' <? echo $check2;?> /> Barang</label>       
	        	     </div>
                </td>
            </tr>
	        <tr>
	            <td class="title_table" width="150">Periode</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <?=$mylib->write_combo4('Pelanggan','KdCustomer',$customer,'','KdCustomer','Nama','','','');?>
	        </tr>
			
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT){
	if($v_pilihan=="transaksi")
		$this->load->view("report/salesinvoice/tampil");
	else
		$this->load->view("report/salesinvoice/tampil_barang");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>