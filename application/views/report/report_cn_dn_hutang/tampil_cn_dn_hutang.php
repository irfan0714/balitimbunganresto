<br>
<table class="table table-bordered" border="1">
	<thead class="title_table">
		<tr>
			<td align="center">Type</td>
			<td align="center">No Dokumen</td>
			<td align="center">Tanggal</td>
			<td align="center">Supplier</td>
			<td align="center">Keterangan</td>
			<td align="center">Rekening</td>
			<td align="center">Nama Rekening</td>
			<td align="center">Sub Divisi</td>
			<td align="center">Rincian</td>
			<td align="center">Nilai</td>
		</tr>		
	</thead>
	<tbody>
	<?php foreach($viewdata AS $val){?>
			<tr>
			<td align="center"><?php echo $val['dntype'];?></td>
			<td align="center"><?php echo $val['dnno'];?></td>
			<td align="center"><?php echo $val['dndate'];?></td>
			<td align="left"><?php echo $val['Nama'];?></td>
			<td align="left"><?php echo $val['note'];?></td>
			<td align="left"><?php echo $val['KdRekening'];?></td>
			<td align="left"><?php echo $val['NamaRekening'];?></td>
			<td align="left"><?php echo $val['NamaSubDivisi'];?></td>
			<td align="left"><?php echo $val['description'];?></td>
			<td align="right"><?php echo ubah_format($val['value']);?></td>
			</tr>
	<?php } ?>
	</tbody> 
</table>
<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
