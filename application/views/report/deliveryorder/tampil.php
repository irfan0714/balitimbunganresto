<?php
$mylib = new globallib();
?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_transaksi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
                <tr class="title_table">
                    <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                    <td style="vertical-align: middle; text-align: center;">No DO</td>
                    <td style="vertical-align: middle; text-align: center;">Pelanggan</td>
                    <td style="vertical-align: middle; text-align: center;">Gudang</td>
                    <td style="vertical-align: middle; text-align: center;">PCode</td>
                    <td style="vertical-align: middle; text-align: center;">Nama Barang</td>
                    <td style="vertical-align: middle; text-align: center;">Qty</td>
                </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}else{
				for($i=0;$i<count($hasil);$i++){
						$dodate = $hasil[$i]['dodate'];
						$dono = $hasil[$i]['dono'];
						$namabarang = $hasil[$i]['NamaLengkap'];
						$gudang = $hasil[$i]['gudang'];
						$pcode = $hasil[$i]['inventorycode'];
						$qty = $hasil[$i]['quantity'];
						$pelanggan = $hasil[$i]['Pelanggan'];
					?>
					<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
						<td nowrap align="center" title="<?php echo $dodate." :: ".$dono; ?>"><?=$dodate; ?></td>
						<td nowrap align="center" title="<?php echo $dodate." :: ".$dono; ?>"><?=$dono; ?></td>
						<td nowrap align="left" title="<?php echo $dodate." :: ".$dono; ?>"><?=$pelanggan; ?></td>
						<td nowrap align="left" title="<?php echo $dodate." :: ".$dono; ?>"><?=$gudang; ?></td>
						<td nowrap align="left" title="<?php echo $dodate." :: ".$dono; ?>"><?=$pcode; ?></td>
						<td nowrap align="left" title="<?php echo $dodate." :: ".$dono; ?>"><?=$namabarang; ?></td>
						<td nowrap align="right" title="<?php echo $dodate." :: ".$dono; ?>"><?=ubah_format($qty); ?></td>
					</tr>
				<?php
				}
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>