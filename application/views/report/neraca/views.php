<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Neraca";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/neraca/cari/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Period</td>
	            <td> 
	            	<select id="bulan1" name="bulan1">
					<?php
					for($a = 0;$a<count($bulan1);$a++){
						$select = "";
						if($bulanaktif==$bulan1[$a]){
							$select = "selected";
						}
					?>
						<option <?=$select;?> value= "<?=$bulan1[$a]?>"><?=$bulan1[$a]?></option>
					<?php
					}
					?>
					</select>
					<select size="1" id="tahun1" name="tahun1">
					<?php
					for($a = 0;$a<count($tahun1);$a++){
						$select = "";
						if($tahunaktif==$tahun1[$a]['Tahun']){
							$select = "selected";
						}
					?>
						<option <?=$select;?> value= "<?=$tahun1[$a]['Tahun']?>"><?=$tahun1[$a]['Tahun']?></option>
					<?php
					}
					?>
					</select>
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Type</td>
	        	<td>
	        		<select size="1" id="type" name="type">
	        			<option <?= ($type1=='Perbandingan')? 'selected=true' : '';?> value='Perbandingan'>Perbandingan</option>
	        			<option <?= ($type1=='Trend')? 'selected=true' : '';?>value='Trend'>Trend</option>
	        		</select>
	        	</td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Rekap/Detail</td>
	        	<td>
	        		<select size="1" id="rekapdetail" name="rekapdetail">
	        			<option <?= ($rekapdetail1=='R')? 'selected=true' : '';?> value='R'>Rekap</option>
	        			<option <?= ($rekapdetail1=='D')? 'selected=true' : '';?>value='D'>Detail</option>
	        		</select>
	        	</td>
	        </tr>
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Submit<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT) 
{
	if($type1=='Perbandingan')
   		$this->load->view("report/neraca/reportperbandingan");
   	else{
   		$this->load->view("report/neraca/reporttrend");
	}
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>