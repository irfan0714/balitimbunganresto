<script>
	function get_choose(bln1, bln2, thn, kddivisi, kdrekening1, kdrekening2,tipe)
	{
		if(tipe=='D'){
			base_url = $("#base_url").val();
			
			url = base_url+"index.php/pop/pop_up_bukubesar/index/"+bln1+"/"+bln2+"/"+thn+"/"+kddivisi+"/"+kdrekening1+"/"+kdrekening2+"/";
			windowOpener(600, 600, 'Detail Mutasi Barang', url, 'Detail Mutasi Barang')	
		}
	}	
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/neraca/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="neraca.xls"');
    }
    $border = false;
    if ($excel != "excel") {
    	$border=true;
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>
   
   <ol class="breadcrumb">
    	<?php
		for($i=0;$i<count($judul);$i++){
			?>
			<li><strong><?php echo $judul[$i]; ?></strong></li>
		<?php
		}
		?>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered table-bordered responsive" border="1">
	    	<thead>
				<tr>
					<th width="30%" colspan="4" rowspan="2" ><center>Rekening</center></th>
					<th width="20%" rowspan="2"><center>Bulan Ini</center></th>
					<th width="25%" colspan="2"><center>Bulan Lalu</center></th>
					<th width="25%" colspan="2"><center>Tahun Lalu</center></th>
				</tr>
				<tr>
					<th><center>Aktual</center></th>
					<th><center>Var</center></th>
					<th><center>Aktual</center></th>
					<th><center>Var</center></th>
				</tr>
			</thead>
		
			<tbody>
				<?php
				if(count($data)==0)
				{
					echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}
				$prev_parent1 = '';
				$prev_parent2 = '';
				$tot_bln_ini1 = 0;
				$tot_bln_lalu1 = 0;
				$tot_thn_lalu1= 0;
				$tot_bln_ini2 = 0;
				$tot_bln_lalu2 = 0;
				$tot_thn_lalu2= 0;
				$tot_group_ini =0;
				$tot_group_lalu=0;
				$tot_group_thnlalu=0;
				
				for($i=0;$i<count($data);$i++){
					$parent1 = $data[$i]['Parent1'];
					$namarekening1 = $data[$i]['NamaRekening1'];
					$parent2 = $data[$i]['Parent2'];
					$namarekening2 = $data[$i]['NamaRekening2'];
					$kdrekening = $data[$i]['KdRekening'];
					$namarekening = $data[$i]['NamaRekening'];
					$nilaiblnini  = $data[$i]['NilaiBlnIni'];
					$nilaiblnlalu  = $data[$i]['NilaiBlnLalu'];
					$nilaithnlalu  = $data[$i]['NilaiThnLalu'];
					
					$isheader = false;
					if($parent1 != $prev_parent1){
						$isheader=true;
						if($prev_parent1 != ''){
						?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2-$tot_bln_lalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2-$tot_thn_lalu2);?></b></td>
							</tr>
							<tr>
								<td colspan="4"><b><?='TOTAL '. $prev_namarekening1;?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini1-$tot_bln_lalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini1-$tot_thn_lalu1);?></b></td>
							</tr>
						<?php
							if($parent1=='21' || $parent1=='31' ){ 
								switch ($parent1) {
    								case '21':
							        	$tot_text = 'TOTAL AKTIVA';
							        	break;
							    	case '31':
								        $tot_text = 'TOTAL PASIVA';
							        	break;
								}
								
							?>
								<tr>
									<td class="text text-center" colspan="4"><strong><?=$tot_text;?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_group_ini);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_group_lalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_group_ini-$tot_group_lalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_group_thnlalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_group_ini-$tot_group_thnlalu);?></strong></td>
								</tr>
							<?php	
								$tot_group_ini = 0;
								$tot_group_lalu = 0;
								$tot_group_thnlalu = 0;
							}
							$tot_bln_ini1 = 0;
							$tot_bln_lalu1 = 0;
							$tot_thn_lalu1 = 0;
							
							$tot_bln_ini2 = 0;
							$tot_bln_lalu2 = 0;
							$tot_thn_lalu2 = 0;
						}
					?>
						<tr>
							<td colspan="4"><?=$namarekening1;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="3"><?=$namarekening2;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					<?php	
					}else{ // $parent1 != $prev_parent1
						if($parent2 != $prev_parent2){
							$isheader = true;
							if($prev_parent2 != ''){
							?>
								<tr>
									<td>&nbsp;</td>
									<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2-$tot_bln_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2-$tot_thn_lalu2);?></b></td>
								</tr>
							<?php
								$tot_bln_ini2 = 0;
								$tot_bln_lalu2 = 0;
								$tot_thn_lalu2 = 0;
							}
							?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><?=$namarekening2;?></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<?php	
						} // End of $parent2 != $prev_parent2
					}
					//if ($isheader==false){ // End of $parent1 != $prev_parent1
					?>
					<tr onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','No','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$rekapdetail1;?>')">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?=$kdrekening;?></td>
						<td><?=$namarekening;?></td>
						<td align="right"><?=$mylib->ubah_format($nilaiblnini);?></td>
						<td align="right"><?=$mylib->ubah_format($nilaiblnlalu);?></td>
						<td align="right"><?=$mylib->ubah_format($nilaiblnini-$nilaiblnlalu);?></td>
						<td align="right"><?=$mylib->ubah_format($nilaithnlalu);?></td>
						<td align="right"><?=$mylib->ubah_format($nilaiblnini-$nilaithnlalu);?></td>
					</tr>
				<?php
					//}
					$prev_parent1 = $parent1;
					$prev_parent2 = $parent2;
					$prev_namarekening1 = $namarekening1;
					$prev_namarekening2 = $namarekening2;
					
					$tot_bln_ini1 += $nilaiblnini;
					$tot_bln_ini2 += $nilaiblnini;
					$tot_bln_lalu1 += $nilaiblnlalu;
					$tot_bln_lalu2 += $nilaiblnlalu;
					$tot_thn_lalu1 += $nilaithnlalu;
					$tot_thn_lalu2 += $nilaithnlalu;
					
					$tot_group_ini += $nilaiblnini;
					$tot_group_lalu += $nilaiblnlalu;
					$tot_group_thnlalu += $nilaithnlalu;
				} // End Of For
				?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu2);?></b></td>
					<td align="right"<b>><?=$mylib->ubah_format($tot_bln_ini2-$tot_bln_lalu2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini2-$tot_thn_lalu2);?></b></td>
				</tr>
				<tr>
					<td colspan="4"><b><?='TOTAL '. $prev_namarekening1;?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini1-$tot_bln_lalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_ini1-$tot_thn_lalu1);?></b></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
</div>
</div>