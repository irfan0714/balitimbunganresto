<script>
	function get_choose(bln1, bln2, thn, kddivisi, kdrekening1, kdrekening2,tipe)
	{
		if(tipe=='D'){
			base_url = $("#base_url").val();
			
			url = base_url+"index.php/pop/pop_up_bukubesar/index/"+bln1+"/"+bln2+"/"+thn+"/"+kddivisi+"/"+kdrekening1+"/"+kdrekening2+"/";
			windowOpener(600, 600, 'Detail Mutasi Barang', url, 'Detail Mutasi Barang')	
		}
	}	
</script>

<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/neraca/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="neracatrend.xls"');
    }
    $bulanaktif = $bulanaktif*1;
    
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>
   
   <ol class="breadcrumb">
    	<?php
		for($i=0;$i<count($judul);$i++){
			?>
			<li><strong><?php echo $judul[$i]; ?></strong></li>
		<?php
		}
		?>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
	    	<thead>
				<tr>
					<th colspan="4" ><center>Rekening</center></th>
					<?php
					for($i=1;$i<=$bulanaktif;$i++){
						?>
						<th><center><?=$namabulan[$i-1];?></center></th>
					<?php
						$tot_bln1[$i] = 0;
						$tot_bln2[$i] = 0;
						$tot_group[$i] = 0;
					}
					?>
				</tr>
			</thead>
		
			<tbody>
				<?php
				if(count($data)==0)
				{
					echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}
				$prev_parent1 = '';
				$prev_parent2 = '';
				
				for($i=0;$i<count($data);$i++){
					$parent1 = $data[$i]['Parent1'];
					$namarekening1 = $data[$i]['NamaRekening1'];
					$parent2 = $data[$i]['Parent2'];
					$namarekening2 = $data[$i]['NamaRekening2'];
					$kdrekening = $data[$i]['KdRekening'];
					$namarekening = $data[$i]['NamaRekening'];
					for($bln=1;$bln<=$bulanaktif;$bln++){
						$nilaiblnini[$bln]  = $data[$i]['NilaiBlnIni'.$bln];	
					}
										
					$isheader = false;
					if($parent1 != $prev_parent1){
						$isheader=true;
						if($prev_parent1 != ''){
						?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
								<?php
								for($bln=1;$bln<=$bulanaktif;$bln++){
								?>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln2[$bln]);?></b></td>
								<?php
								}
								?>
							</tr>
							<tr>
								<td colspan="4"><b><?='TOTAL '. $prev_namarekening1;?></b></td>
								<?php
								for($bln=1;$bln<=$bulanaktif;$bln++){
								?>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln1[$bln]);?></b></td>
								<?php
								}
								?>
							</tr>
						<?php
							if($parent1=='21' || $parent1=='31' ){ 
								switch ($parent1) {
    								case '21':
							        	$tot_text = 'TOTAL AKTIVA';
							        	break;
							    	case '31':
								        $tot_text = 'TOTAL PASIVA';
							        	break;
								}
								
							?>
								<tr>
									<td class="text text-center" colspan="4"><strong><?=$tot_text;?></strong></td>
									<?php
									for($bln=1;$bln<=$bulanaktif;$bln++){
									?>
										<td align="right"><b><?=$mylib->ubah_format($tot_group[$bln]);?></b></td>
									<?php
										$tot_group[$bln] = 0;
									}
									?>	
								</tr>
							<?php	
							}
							for($bln=1;$bln<=$bulanaktif;$bln++){
								$tot_bln1[$bln] = 0;
								$tot_bln2[$bln] = 0;
							}
						}
					?>
						<tr>
							<td colspan="4"><?=$namarekening1;?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="3"><?=$namarekening2;?></td>
						</tr>
					<?php	
					}else{ // $parent1 != $prev_parent1
						if($parent2 != $prev_parent2){
							$isheader = true;
							if($prev_parent2 != ''){
							?>
								<tr>
									<td>&nbsp;</td>
									<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
									<?php
									for($bln=1;$bln<=$bulanaktif;$bln++){
										?>
										<td align="right"><b><?=$mylib->ubah_format($tot_bln2[$bln]);?></b></td>
									<?php
										$tot_bln2[$bln] = 0;
									}
									?>
								</tr>
							<?php
							}
							?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><?=$namarekening2;?></td>
							</tr>
							<?php	
						} // End of $parent2 != $prev_parent2
					}
					//if ($isheader==false){ // End of $parent1 != $prev_parent1
					?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?=$kdrekening;?></td>
						<td><?=$namarekening;?></td>
						<?php
						for($bln=1;$bln<=$bulanaktif;$bln++){
							if($bln<10){
								$bulan='0'.$bln;
							}else{
								$bulan=$bln;
							}
							?>
							<td onclick="get_choose('<?=$bulan;?>','<?=$bulan;?>','<?=$tahunaktif;?>','No','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$rekapdetail1;?>')"  align="right"><?=$mylib->ubah_format($nilaiblnini[$bln]);?></td>
						<?php
						}
						?>
					</tr>
				<?php
					//}
					$prev_parent1 = $parent1;
					$prev_parent2 = $parent2;
					$prev_namarekening1 = $namarekening1;
					$prev_namarekening2 = $namarekening2;
					
					for($bln=1;$bln<=$bulanaktif;$bln++){
						$tot_bln1[$bln] += $nilaiblnini[$bln];	
						$tot_bln2[$bln] += $nilaiblnini[$bln];	
						$tot_group[$bln] += $nilaiblnini[$bln];	
					}
				} // End Of For
				?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
					<?php
					for($bln=1;$bln<=$bulanaktif;$bln++){
					?>
						<td align="right"><b><?=$mylib->ubah_format($tot_bln2[$bln]);?></b></td>		
					<?php
					}
					?>
				</tr>
				<tr>
					<td colspan="4"><b><?='TOTAL '. $prev_namarekening1;?></b></td>
					<?php
					for($bln=1;$bln<=$bulanaktif;$bln++){
					?>
						<td align="right"><b><?=$mylib->ubah_format($tot_bln1[$bln]);?></b></td>		
					<?php
					}
					?>
				</tr>
			</tbody>
		</table>
	</div>
</form>
</div>
</div>