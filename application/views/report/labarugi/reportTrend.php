<script>
	function get_choose(bln1, bln2, thn, kddivisi, kdrekening1, kdrekening2,tipe)
	{
		base_url = $("#base_url").val();
		
		url = base_url+"index.php/pop/pop_up_bukubesar/index/"+bln1+"/"+bln2+"/"+thn+"/"+kddivisi+"/"+kdrekening1+"/"+kdrekening2+"/";
		windowOpener(600, 600, 'Detail Mutasi Barang', url, 'Detail Mutasi Barang')	
	}	
</script>

<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/labarugi/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="reportdetailticket.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>
   
   <ol class="breadcrumb">
    	<?php
		for($i=0;$i<count($judul);$i++){
			?>
			<li><strong><?php echo $judul[$i]; ?></strong></li>
		<?php
		}
		?>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered table-bordered responsive" border="1">
	    	<thead>
				<tr>
					<th rowspan="2" width="40%" colspan="4"><center>Rekening</center></th>
					<?php
					$bulanaktif = $bulanaktif * 1;
					for($bln=1;$bln<=$bulanaktif;$bln++){
					?>
						<th colspan="2"><center><?=$namabulan[$bln-1];?></center></th>
					<?php	
						$tot_bln1[$bln] = 0;
						$tot_bln2[$bln] = 0;
						$tot_group[$bln] = 0;
						$penyusutan[$bln] = 0;
						$lru_actual[$bln] = 0;
					}
					?>
					<th colspan="2"><center>Total</center></th>
				</tr>
				<tr>
					<?php
					for($bln=1;$bln<=$bulanaktif;$bln++){
					?>
						<th><center>Actual</center></th>
						<th><center>%</center></th>
					<?php
					}
					?>
					<th><center>Actual</center></th>
					<th><center>%</center></th>
				</tr>
			</thead>
		
			<tbody>
				<?php
				if(count($data)==0)
				{
					echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}
				$prev_parent1 = '';
				$prev_parent2 = '';
				$nilaiblnini = '';
				
				$norek_penyusutan =array('42050115','42050120','42050125','42050130','42050135','42050140','42050145');
				for($i=0;$i<count($data);$i++){
					$parent1 = $data[$i]['Parent1'];
					$namarekening1 = $data[$i]['NamaRekening1'];
					$parent2 = $data[$i]['Parent2'];
					$namarekening2 = $data[$i]['NamaRekening2'];
					$kdrekening = $data[$i]['KdRekening'];
					$namarekening = $data[$i]['NamaRekening'];
					for($bln=1;$bln<=$bulanaktif;$bln++){
						if($parent1=='40' || $parent1=='43'){
							$nilaiblnini[$bln]  = $data[$i]['Nilai'.$bln]*-1;		
						}else{
							$nilaiblnini[$bln]  = $data[$i]['Nilai'.$bln];		
						}
					}
					
					if(in_array($kdrekening,$norek_penyusutan)){
						for($bln=1;$bln<=$bulanaktif;$bln++){
							$penyusutan[$bln] +=  $nilaiblnini[$bln];
						}
					}
					
					$isheader = false;
					if($parent1 != $prev_parent1){
						$isheader=true;
						if($prev_parent1 != ''){
						?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><b><?='TOTAL'. $prev_namarekening2;?></b></td>
								<?php
								$totline = 0;
								for($bln=1;$bln<=$bulanaktif;$bln++){
									if($bln<10){
										$bulan='0'.$bln;
									}else{
										$bulan=$bln;
									}
									
									$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_bln2[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
								?>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln2[$bln]);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persensales);?></b></td>
								<?php	
									$totline += $tot_bln2[$bln];
								}
								$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
								?>
								<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
							</tr>
							<tr>
								<td colspan="4"><b><?='TOTAL'. $prev_namarekening1;?></b></td>
								<?php
								$totline=0;
								for($bln=1;$bln<=$bulanaktif;$bln++){
									if($bln<10){
										$bulan='0'.$bln;
									}else{
										$bulan=$bln;
									}
									$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_bln1[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
								?>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln1[$bln]);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persensales);?></b></td>
								<?php	
									$totline += $tot_bln1[$bln];
								}
								$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
								?>
								<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
							</tr>
						<?php
							if($parent1=='42' || $parent1=='43' || $parent1=='45'){ // laba rugi kotor
								switch ($parent1) {
    								case '42':
							        	$labarugi = 'LABA RUGI KOTOR';
							        	break;
							    	case '43':
								        $labarugi = 'LABA RUGI USAHA';
								        for($bln=1;$bln<=$bulanaktif;$bln++){
								        	$lru_actual[$bln] = $tot_group[$bln];
										}
							        	break;
							        case '45':
								        $labarugi = 'LABA RUGI SEBELUM PAJAK';
							        	break;
								}
								
							?>
								<tr>
									<td class="text text-center" colspan="4"><strong><?=$labarugi;?></strong></td>
									<?php
									$totline=0;
									for($bln=1;$bln<=$bulanaktif;$bln++){
										if($bln<10){
											$bulan='0'.$bln;
										}else{
											$bulan=$bln;
										}
										$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_group[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
									?>
										<td align="right"><b><?=$mylib->ubah_format($tot_group[$bln]);?></b></td>
										<td align="right"><b><?=$mylib->ubah_format($persensales);?></b></td>
									<?php	
									$totline += $tot_group[$bln];
									}
									$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
									?>
									<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
								</tr>
							<?php	
							}
							for($bln=1;$bln<=$bulanaktif;$bln++){
								$tot_bln1[$bln] = 0;
								$tot_bln2[$bln] = 0;
							}
						}
					?>
						<tr>
							<td colspan="4"><?=$namarekening1;?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="3"><?=$namarekening2;?></td>
						</tr>
					<?php	
					}else{ // $parent1 != $prev_parent1
						if($parent2 != $prev_parent2){
							$isheader = true;
							if($prev_parent2 != ''){
							?>
								<tr>
									<td>&nbsp;</td>
									<td colspan="3"><b><?='TOTAL'. $prev_namarekening2;?></b></td>
									<?php	
									$totline = 0;
									for($bln=1;$bln<=$bulanaktif;$bln++){
										if($bln<10){
											$bulan='0'.$bln;
										}else{
											$bulan=$bln;
										}
										$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_bln2[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
									?>
										<td align="right"><b><?=$mylib->ubah_format($tot_bln2[$bln]);?></b></td>
										<td align="right"><b><?=$mylib->ubah_format($persensales);?></b></td>
									<?php	
									$totline += $tot_bln2[$bln];
									}
									$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
									?>
									<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
								</tr>
							<?php
								for($bln=1;$bln<=$bulanaktif;$bln++){
									$tot_bln2[$bln] = 0;
								}
							}
							?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><?=$namarekening2;?></td>
							</tr>
							<?php	
						} // End of $parent2 != $prev_parent2
					}
					//if ($isheader==false){ // End of $parent1 != $prev_parent1
					?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?=$kdrekening;?></td>
						<td><?=$namarekening;?></td>
						<?php
						$totline=0;
						for($bln=1;$bln<=$bulanaktif;$bln++){
							if($bln<10){
								$bulan='0'.$bln;
							}else{
								$bulan = $bln;
							}
							$persensales = $netsales['NetSales'.$bulan]!=0 ? $nilaiblnini[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
						?>
							<td onclick="get_choose('<?=$bulan;?>','<?=$bulan;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($nilaiblnini[$bln]);?></td>
							<td onclick="get_choose('<?=$bulan;?>','<?=$bulan;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persensales);?></td>
						<?php
							$totline += $nilaiblnini[$bln];
						}
						$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
						?>
						<td align="right"><?=$mylib->ubah_format($totline);?></td>
						<td align="right"><?=$mylib->ubah_format($persenline);?></td>
					</tr>
				<?php
					$prev_parent1 = $parent1;
					$prev_parent2 = $parent2;
					$prev_namarekening1 = $namarekening1;
					$prev_namarekening2 = $namarekening2;
					if($parent1=='40' || $parent1=='43'){
						for($bln=1;$bln<=$bulanaktif;$bln++){
							$tot_group[$bln] += $nilaiblnini[$bln];
						};
					}else{
						for($bln=1;$bln<=$bulanaktif;$bln++){
							$tot_group[$bln] -= $nilaiblnini[$bln];
						};
					}
					for($bln=1;$bln<=$bulanaktif;$bln++){
						$tot_bln1[$bln] += $nilaiblnini[$bln];
						$tot_bln2[$bln] += $nilaiblnini[$bln];
					}
				} // End Of For
				
				?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3"><b><?='TOTAL  '. $prev_namarekening2;?></b></td>
					<?php
					$totline=0;
					for($bln=1;$bln<=$bulanaktif;$bln++){
						if($bln<10){
							$bulan='0'.$bln;
						}else{
							$bulan=$bln;
						}
						$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_bln2[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
					?>
						<td align="right"><b><?=$mylib->ubah_format($tot_bln2[$bln]);?></b></td>
						<td align="right"><b><?=$mylib->ubah_format($persensales);?></b></td>
					<?php
						$totline += $tot_bln2[$bln];
					}
					$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
					?>
					<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
				</tr>
				<tr>
					<td colspan="4"><b><?='TOTAL '. $prev_namarekening1;?></b></td>
					<?php
					$totline=0;
					for($bln=1;$bln<=$bulanaktif;$bln++){
						if($bln<10){
							$bulan='0'.$bln;
						}else{
							$bulan=$bln;
						}
						$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_bln1[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
					?>
						<td align="right"><b><?=$mylib->ubah_format($tot_bln1[$bln]);?></b></td>
						<td align="right"><b><?=$mylib->ubah_format($persensales);?></b></td>
					<?php
						$totline += $tot_bln1[$bln];
					}
					$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
					?>
					<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
				</tr>
				<?php
				if($parent1=='44' || $parent1=='45'){ // laba rugi kotor
					switch ($parent1) {
						case '44':
				        	$labarugi = 'LABA RUGI SEBELUM PAJAK';
				        	break;
				    	case '45':
					        $labarugi = 'LABA RUGI SETELAH PAJAK';
				        	break;
					}
					
				?>
					<tr>
						<td class="text text-center" colspan="4"><strong><?=$labarugi;?></strong></td>
						<?php
						$totline=0;
						for($bln=1;$bln<=$bulanaktif;$bln++){
							if($bln<10){
								$bulan='0'.$bln;
							}else{
								$bulan=$bln;
							}
							$persensales = $netsales['NetSales'.$bulan]!=0 ? $tot_group[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
						?>
							<td align="right"><strong><?=$mylib->ubah_format($tot_group[$bln]);?></strong></td>
							<td align="right"><strong><?=$mylib->ubah_format($persensales);?></strong></td>
						<?php
							$totline += $tot_group[$bln];
						}
						$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
						?>
						<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
						<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
					</tr>
				<?php
				}	
				?>
				<tr></tr>
				<tr>
					<td class="text text-center" colspan="4"><strong>Total Beban Penyusutan dan Amortisasi</strong></td>
					<?php
					$totline=0;
					for($bln=1;$bln<=$bulanaktif;$bln++){
						if($bln<10){
							$bulan='0'.$bln;
						}else{
							$bulan=$bln;
						}
						$persensales = $netsales['NetSales'.$bulan]!=0 ? $penyusutan[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
					?>
						<td align="right"><strong><?=$mylib->ubah_format($penyusutan[$bln]);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persensales);?></strong></td>
					<?php
						$totline += $penyusutan[$bln];
					}
					$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
					?>
					<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
				</tr>
				<tr>
					<td class="text text-center" colspan="4"><strong>EBITDA</strong></td>
					<?php
					$totline=0;
					for($bln=1;$bln<=$bulanaktif;$bln++){
						if($bln<10){
							$bulan='0'.$bln;
						}else{
							$bulan=$bln;
						}
						$persensales = $netsales['NetSales'.$bulan]!=0 ? $lru_actual[$bln]/$netsales['NetSales'.$bulan]*100 : 0 ;
					?>
						<td align="right"><strong><?=$mylib->ubah_format($lru_actual[$bln]+$penyusutan[$bln]);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persensales);?></strong></td>
					<?php
						$totline += $lru_actual[$bln]+$penyusutan[$bln];
					}
					$persenline = $netsales['NetSalesYTD']!=0 ? $totline/$netsales['NetSalesYTD']*100 : 0 ;
					?>
					<td align="right"><b><?=$mylib->ubah_format($totline);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenline);?></b></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
</div>
</div>