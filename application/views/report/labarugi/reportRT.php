<script>
	function get_choose(bln1, bln2, thn, kddivisi, kdrekening1, kdrekening2,tipe)
	{
		base_url = $("#base_url").val();
		
		url = base_url+"index.php/pop/pop_up_bukubesar/index/"+bln1+"/"+bln2+"/"+thn+"/"+kddivisi+"/"+kdrekening1+"/"+kdrekening2+"/";
		windowOpener(600, 600, 'Detail Mutasi Barang', url, 'Detail Mutasi Barang')	
	}	
</script>

<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/labarugi/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="reportdetailticket.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>
   
   <ol class="breadcrumb">
    	<?php
		for($i=0;$i<count($judul);$i++){
			?>
			<li><strong><?php echo $judul[$i]; ?></strong></li>
		<?php
		}
		?>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered table-bordered responsive" border="1">
	    	<thead>
				<tr>
					<th width="40%" colspan="4" rowspan="2" ><center>Rekening</center></th>
					<th width="30%" colspan="6"><center>Bulan Ini</center></th>
					<th width="30%" colspan="6"><center>Tahun Ini</center></th>
				</tr>
				<tr>
					<th><center>Aktual</center></th>
					<th><center>Bulan Lalu</center></th>
					<th><center>%</center></th>
					<th><center>Budget</center></th>
					<th><center>%Budget</center></th>
					<th><center>%Sales</center></th>
					<th><center>Aktual</center></th>
					<th><center>Tahun Lalu</center></th>
					<th><center>%</center></th>
					<th><center>Budget</center></th>
					<th><center>%Budget</center></th>
					<th><center>%Sales</center></th>
				</tr>
			</thead>
		
			<tbody>
				<?php
				if(count($data)==0)
				{
					echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}
				$prev_parent1 = '';
				$prev_parent2 = '';
				$tot_parent1 = 0;
				$tot_bln_lalu1 = 0;
				$tot_budget1 = 0;
				$tot_parent2 = 0;
				$tot_bln_lalu2 = 0;
				$tot_budget2 = 0;
				$tot_parent_ytd1 = 0;
				$tot_parent_ytd2 = 0;
				$tot_budget_ytd1 = 0;
				$tot_budget_ytd2 = 0;
				$tot_thn_lalu1 = 0;
				$tot_thn_lalu2 = 0;
				$tot_labarugi = 0;
				$tot_labarugi_bln_lalu=0;
				$tot_labarugi_thn_lalu=0;
				$tot_bud_labarugi = 0;
				$tot_labarugi_ytd = 0;
				$tot_bud_labarugi_ytd = 0;
				$tot_penyusutan_bulanini = 0;
				$tot_penyusutan_bulan_lalu = 0;
				$tot_penyusutan_budget_bulanini=0;
				$tot_penyusutan_tahun_ini = 0;
				$tot_penyusutan_tahun_lalu = 0;
				$tot_penyusutan_budget_tahunini=0;
				
				$norek_penyusutan =array('42050115','42050120','42050125','42050130','42050135','42050140','42050145');
				
				$lru_bulan_ini = 0;
				$lru_bln_lalu=0;
				$lru_bud_ini = 0;
				$lru_tahun_ini =0;
				$lru_tahun_lalu=0;
				$lru_bud_tahun = 0;
					
				for($i=0;$i<count($data);$i++){
					$parent1 = $data[$i]['Parent1'];
					$namarekening1 = $data[$i]['NamaRekening1'];
					$parent2 = $data[$i]['Parent2'];
					$namarekening2 = $data[$i]['NamaRekening2'];
					$kdrekening = $data[$i]['KdRekening'];
					$namarekening = $data[$i]['NamaRekening'];
					
					if($parent1=='40' || $parent1=='43'){
						$awal  = $data[$i]['Awal']*-1;
						$nilai  = $data[$i]['Nilai']*-1;
						$budget  = $data[$i]['Budget'];
						$budgettahunini  = $data[$i]['BudgetTahunIni'];
						$nilaibulanlalu  = $data[$i]['NilaiBulanLalu']*-1;
						$nilaitahunlalu  = $data[$i]['NilaiTahunLalu']*-1;
					}else{
						$awal  = $data[$i]['Awal'];
						$nilai  = $data[$i]['Nilai'];
						$budget  = $data[$i]['Budget'];
						$budgettahunini  = $data[$i]['BudgetTahunIni'];
						$nilaibulanlalu  = $data[$i]['NilaiBulanLalu'];
						$nilaitahunlalu  = $data[$i]['NilaiTahunLalu'];
					}
					if(in_array($kdrekening,$norek_penyusutan)){
						$tot_penyusutan_bulanini += $nilai;
						$tot_penyusutan_bulan_lalu += $nilaibulanlalu;
						$tot_penyusutan_budget_bulanini += $budget;
						$tot_penyusutan_tahun_ini += $awal+$nilai;
						$tot_penyusutan_tahun_lalu += $nilaitahunlalu;
						$tot_penyusutan_budget_tahunini += $budgettahunini;
					}
				
					$persenbudget = $budget!=0 ? $nilai / $budget * 100 : 0;
					$persenbulanlalu = $nilaibulanlalu!=0 ? $nilai / $nilaibulanlalu * 100 : 0;
					$persentahunlalu = $nilaitahunlalu!=0 ? ($nilai+$awal) / $nilaitahunlalu * 100 : 0;
					$persensales = $netsales['NetSales']!=0 ? $nilai/$netsales['NetSales']*100 : 0 ;
					$persenbudgetytd = $budgettahunini !=0 ? ($nilai+$awal) / $budgettahunini * 100 : 0;
					$persensalesytd = $netsales['NetSalesYTD']!=0 ? ($nilai+$awal)/$netsales['NetSalesYTD']*100 : 0 ;
					$persen_labarugi_bud = $tot_bud_labarugi != 0 ? $tot_labarugi/$tot_bud_labarugi*100 : 0;
					$persen_labarugi_bln_lalu = $tot_labarugi_bln_lalu != 0 ? $tot_labarugi/$tot_labarugi_bln_lalu*100 : 0;
					$persen_labarugi_thn_lalu = $tot_labarugi_thn_lalu != 0 ? $tot_labarugi_ytd/$tot_labarugi_thn_lalu*100 : 0;
					$persen_labarugi_sales = $netsales['NetSales']!=0 ? $tot_labarugi/$netsales['NetSales']*100 : 0 ;
					$persen_labarugi_budytd = $tot_bud_labarugi_ytd != 0 ? $tot_labarugi_ytd/$tot_bud_labarugi_ytd*100 : 0;
					$persen_labarugi_salesytd = $netsales['NetSalesYTD']!=0 ? $tot_labarugi_ytd/$netsales['NetSalesYTD']*100 : 0 ;
					
					$isheader = false;
					if($parent1 != $prev_parent1){
						$isheader=true;
						if($prev_parent1 != ''){
							$persenbudget1 = $tot_budget1!=0 ? $tot_parent1 / $tot_budget1 * 100 : 0;
							$persenbulanlalu1 = $tot_bln_lalu1!=0 ? $tot_parent1 / $tot_bln_lalu1 * 100 : 0;
							$persentahunlalu1 = $tot_thn_lalu1!=0 ? $tot_parent_ytd1 / $tot_thn_lalu1 * 100 : 0;
							$persensales1 = $netsales['NetSales']!=0 ? $tot_parent1/$netsales['NetSales']*100 : 0 ;
							$persenbudgetytd1 = $tot_budget_ytd1!=0 ? $tot_parent_ytd1 / $tot_budget_ytd1 * 100 : 0;
							$persensalesytd1 = $netsales['NetSalesYTD']!=0 ? $tot_parent_ytd1/$netsales['NetSalesYTD']*100 : 0 ;
							
							$persenbudget2 = $tot_budget2!=0 ? $tot_parent2 / $tot_budget2 * 100 : 0;
							$persenbulanlalu2 = $tot_bln_lalu2!=0 ? $tot_parent2 / $tot_bln_lalu2 * 100 : 0;
							$persentahunlalu2 = $tot_thn_lalu2!=0 ? $tot_parent_ytd2 / $tot_thn_lalu2 * 100 : 0;
							$persensales2 = $netsales['NetSales']!=0 ? $tot_parent2/$netsales['NetSales']*100 : 0 ;
							$persenbudgetytd2 = $tot_budget_ytd2!=0 ? $tot_parent_ytd2 / $tot_budget_ytd2 * 100 : 0;
							$persensalesytd2 = $netsales['NetSalesYTD']!=0 ? $tot_parent_ytd2/$netsales['NetSalesYTD']*100 : 0 ;
						?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_parent2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenbulanlalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_budget2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenbudget2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persensales2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_parent_ytd2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persentahunlalu2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_budget_ytd2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenbudgetytd2);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persensalesytd2);?></b></td>
							</tr>
							<tr>
								<td colspan="4"><b><?='TOTAL '. $prev_namarekening1;?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_parent1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenbulanlalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_budget1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenbudget1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persensales1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_parent_ytd1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persentahunlalu1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($tot_budget_ytd1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persenbudgetytd1);?></b></td>
								<td align="right"><b><?=$mylib->ubah_format($persensalesytd1);?></b></td>
							</tr>
						<?php
							if($parent1=='42' || $parent1=='43' || $parent1=='45'){ // laba rugi kotor
								switch ($parent1) {
    								case '42':
							        	$labarugi = 'LABA RUGI KOTOR';
							        	break;
							    	case '43':
								        $labarugi = 'LABA RUGI USAHA';
								        $lru_bulan_ini = $tot_labarugi;
										$lru_bln_lalu = $tot_labarugi_bln_lalu;
										$lru_bud_ini = $tot_bud_labarugi;
										$lru_tahun_ini = $tot_labarugi_ytd;
										$lru_tahun_lalu = $tot_labarugi_thn_lalu;
										$lru_bud_tahun = $tot_bud_labarugi_ytd;
							        	break;
							        case '45':
								        $labarugi = 'LABA RUGI SEBELUM PAJAK';
							        	break;
								}
								
							?>
								<tr>
									<td class="text text-center" colspan="4"><strong><?=$labarugi;?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi_bln_lalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_bln_lalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_bud_labarugi);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_bud);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi_ytd);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi_thn_lalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_thn_lalu);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_bud_labarugi_ytd);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_budytd);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_salesytd);?></strong></td>
								</tr>
							<?php	
							}
							$tot_parent1 = 0;
							$tot_bln_lalu1=0;
							$tot_budget1 = 0;
							$tot_parent_ytd1 = 0;
							$tot_budget_ytd1 = 0;
							$tot_thn_lalu1=0;
							
							$tot_parent2 = 0;
							$tot_bln_lalu2=0;
							$tot_budget2 = 0;
							$tot_parent_ytd2 = 0;
							$tot_budget_ytd2 = 0;
							$tot_thn_lalu2=0;
						}
					?>
						<tr>
							<td colspan="4"><?=$namarekening1;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="3"><?=$namarekening2;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					<?php	
					}else{ // $parent1 != $prev_parent1
						if($parent2 != $prev_parent2){
							$isheader = true;
							if($prev_parent2 != ''){
								$persenbudget2 = $tot_budget2 != 0 ? $tot_parent2 / $tot_budget2 * 100 : 0;
								$persensales2 = $netsales['NetSales']!=0 ? $tot_parent2/$netsales['NetSales']*100 : 0 ;
								$persenbudgetytd2 = $tot_budget_ytd2!=0 ? $tot_parent_ytd2 / $tot_budget_ytd2 * 100 : 0;
								$persensalesytd2 = $netsales['NetSalesYTD']!=0 ? $tot_parent_ytd2/$netsales['NetSalesYTD']*100 : 0 ;
								$persen_bln_lalu2 = $tot_bln_lalu2 != 0 ? $tot_parent2 / $tot_bln_lalu2 * 100 : 0;
								$persen_thn_lalu2 = $tot_thn_lalu2 != 0 ? $tot_parent_ytd2 / $tot_thn_lalu2 * 100 : 0;
							?>
								<tr>
									<td>&nbsp;</td>
									<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_parent2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persen_bln_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_budget2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persenbudget2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persensales2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_parent_ytd2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persen_thn_lalu2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($tot_budget_ytd2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persenbudgetytd2);?></b></td>
									<td align="right"><b><?=$mylib->ubah_format($persensalesytd2);?></b></td>
								</tr>
							<?php
								$tot_parent2 = 0;
								$tot_budget2 = 0;
								$tot_parent_ytd2 = 0;
								$tot_budget_ytd2 = 0;
								$tot_bln_lalu2=0;
								$tot_thn_lalu2=0;
							}
							?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><?=$namarekening2;?></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<?php	
						} // End of $parent2 != $prev_parent2
					}
					//if ($isheader==false){ // End of $parent1 != $prev_parent1
					?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?=$kdrekening;?></td>
						<td><?=$namarekening;?></td>
						<td onclick="get_choose('<?=$bulanaktif;?>','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($nilai);?></td>
						<td onclick="get_choose('<?=$bulanaktif;?>','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($nilaibulanlalu);?></td>
						<td onclick="get_choose('<?=$bulanaktif;?>','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persenbulanlalu);?></td>
						<td onclick="get_choose('<?=$bulanaktif;?>','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($budget);?></td>
						<td onclick="get_choose('<?=$bulanaktif;?>','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persenbudget);?></td>
						<td onclick="get_choose('<?=$bulanaktif;?>','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persensales);?></td>
						<td onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($awal+$nilai);?></td>
						<td onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($nilaitahunlalu);?></td>
						<td onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persentahunlalu);?></td>
						<td onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($budgettahunini);?></td>
						<td onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persenbudgetytd);?></td>
						<td onclick="get_choose('01','<?=$bulanaktif;?>','<?=$tahunaktif;?>','<?=$divisi;?>','<?=$kdrekening; ?>','<?=$kdrekening; ?>','<?=$type1;?>')" align="right"><?=$mylib->ubah_format($persensalesytd);?></td>
					</tr>
				<?php
					//}
					$prev_parent1 = $parent1;
					$prev_parent2 = $parent2;
					$prev_namarekening1 = $namarekening1;
					$prev_namarekening2 = $namarekening2;
					if($parent1=='40' || $parent1=='43'){
						$tot_labarugi += $nilai;
						$tot_labarugi_bln_lalu += $nilaibulanlalu;
						$tot_labarugi_thn_lalu += $nilaitahunlalu;
						$tot_labarugi_ytd += $nilai+$awal;
						$tot_bud_labarugi += $budget;
						$tot_bud_labarugi_ytd += $budgettahunini;
					}else{
						$tot_labarugi -= $nilai;
						$tot_labarugi_bln_lalu -= $nilaibulanlalu;
						$tot_labarugi_thn_lalu -= $nilaitahunlalu;
						$tot_labarugi_ytd -= $nilai+$awal;
						$tot_bud_labarugi -= $budget;
						$tot_bud_labarugi_ytd -= $budgettahunini;	
					}
					$tot_parent1 += $nilai;
					$tot_parent2 += $nilai;
					$tot_bln_lalu1 += $nilaibulanlalu;
					$tot_thn_lalu1 += $nilaitahunlalu;
					$tot_bln_lalu2 += $nilaibulanlalu;
					$tot_thn_lalu2 += $nilaitahunlalu;
					//echo 'tot_thn_lalu2 : '.$tot_thn_lalu2;
					$tot_budget1 += $budget;
					$tot_budget2 += $budget;
					$tot_parent_ytd1 += $nilai+$awal;
					$tot_parent_ytd2 += $nilai+$awal;
					$tot_budget_ytd1 += $budgettahunini;
					$tot_budget_ytd2 += $budgettahunini;
					
				} // End Of For
				$persenbudget1 = $tot_budget1!=0 ? $tot_parent1 / $tot_budget1 * 100 : 0;
				$persenbulanlalu1 = $tot_bln_lalu1!=0 ? $tot_parent1 / $tot_bln_lalu1 * 100 : 0;
				$persensales1 = $netsales['NetSales']!=0 ? $tot_parent1/$netsales['NetSales']*100 : 0 ;
				$persenbudgetytd1 = $tot_budget_ytd1!=0 ? $tot_parent_ytd1 / $tot_budget_ytd1 * 100 : 0;
				$persentahunlalu1 = $tot_thn_lalu1!=0 ? $tot_parent_ytd1 / $tot_thn_lalu1 * 100 : 0;
				$persensalesytd1 = $netsales['NetSalesYTD']!=0 ? $tot_parent_ytd1/$netsales['NetSalesYTD']*100 : 0 ;
				
				$persenbudget2 = $tot_budget2!=0 ? $tot_parent2 / $tot_budget2 * 100 : 0;
				$persenbulanlalu2 = $tot_bln_lalu2!=0 ? $tot_parent2 / $tot_bln_lalu2 * 100 : 0;
				$persensales2 = $netsales['NetSales']!=0 ? $tot_parent2/$netsales['NetSales']*100 : 0 ;
				$persenbudgetytd2 = $tot_budget_ytd2!=0 ? $tot_parent_ytd2 / $tot_budget_ytd2 * 100 : 0;
				$persentahunlalu2 = $tot_thn_lalu2!=0 ? $tot_parent_ytd2 / $tot_thn_lalu2 * 100 : 0;
				$persensalesytd2 = $netsales['NetSalesYTD']!=0 ? $tot_parent_ytd2/$netsales['NetSalesYTD']*100 : 0 ;
				?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3"><b><?='TOTAL '. $prev_namarekening2;?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_parent2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenbulanlalu2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_budget2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenbudget2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persensales2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_parent_ytd2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persentahunlalu2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_budget_ytd2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenbudgetytd2);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persensalesytd2);?></b></td>
				</tr>
				<tr>
					<td colspan="4"><b><?='TOTAL  '. $prev_namarekening1;?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_parent1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_bln_lalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenbulanlalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_budget1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenbudget1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persensales1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_parent_ytd1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_thn_lalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persentahunlalu1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($tot_budget_ytd1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persenbudgetytd1);?></b></td>
					<td align="right"><b><?=$mylib->ubah_format($persensalesytd1);?></b></td>
				</tr>
				<?php
				if($parent1=='44' || $parent1=='45'){ // laba rugi kotor
					switch ($parent1) {
						case '44':
				        	$labarugi = 'LABA RUGI SEBELUM PAJAK';
				        	break;
				    	case '45':
					        $labarugi = 'LABA RUGI SETELAH PAJAK';
				        	break;
					}
					
				?>
					<tr>
						<td class="text text-center" colspan="4"><strong><?=$labarugi;?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi_bln_lalu);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_bln_lalu);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_bud_labarugi);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_bud);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi_ytd);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi_thn_lalu);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_thn_lalu);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_bud_labarugi_ytd);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_budytd);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_salesytd);?></strong></td>
					</tr>
				<?php
				}
										
					$persenbudget1 = $tot_penyusutan_budget_bulanini!=0 ? $tot_penyusutan_bulanini / $tot_penyusutan_budget_bulanini * 100 : 0;
					$persenbulanlalu1 = $tot_penyusutan_bulan_lalu!=0 ? $tot_penyusutan_bulanini / $tot_penyusutan_bulan_lalu * 100 : 0;
					$persensales1 = $netsales['NetSales']!=0 ? $tot_penyusutan_bulanini/$netsales['NetSales']*100 : 0 ;
					$persenbudgetytd1 = $tot_penyusutan_budget_tahunini!=0 ? $tot_penyusutan_tahun_ini / $tot_penyusutan_budget_tahunini * 100 : 0;
					$persentahunlalu1 = $tot_penyusutan_tahun_lalu!=0 ? $tot_penyusutan_tahun_ini / $tot_penyusutan_tahun_lalu * 100 : 0;
					$persensalesytd1 = $netsales['NetSalesYTD']!=0 ? $tot_penyusutan_tahun_ini/$netsales['NetSalesYTD']*100 : 0 ;
					
					$persenbudget_ebitda = ($lru_bud_ini+$tot_penyusutan_budget_bulanini) !=0 ? ($lru_bulan_ini+$tot_penyusutan_bulanini) / ($lru_bud_ini+$tot_penyusutan_budget_bulanini) * 100 : 0;
					$persenbulanlalu_ebitda = ($lru_bln_lalu+$tot_penyusutan_bulan_lalu) !=0 ? ($lru_bulan_ini+$tot_penyusutan_bulanini) / ($lru_bln_lalu+$tot_penyusutan_bulan_lalu) * 100 : 0;
					$persensales_ebitda = $netsales['NetSales']!=0 ? ($lru_bulan_ini+$tot_penyusutan_bulanini)/$netsales['NetSales']*100 : 0 ;
					$persenbudgetytd_ebitda = ($lru_bud_tahun+$tot_penyusutan_budget_tahunini) !=0 ? ($lru_tahun_ini+$tot_penyusutan_tahun_ini) / ($lru_bud_tahun+$tot_penyusutan_budget_tahunini) * 100 : 0;
					$persentahunlalu_ebitda = $tot_penyusutan_tahun_lalu!=0 ? ($lru_tahun_ini+$tot_penyusutan_tahun_ini) / ($lru_tahun_lalu+$tot_penyusutan_tahun_lalu) * 100 : 0;
					$persensalesytd_ebitda = $netsales['NetSalesYTD']!=0 ? ($lru_tahun_ini+$tot_penyusutan_tahun_ini)/$netsales['NetSalesYTD']*100 : 0 ;
				?>
				<tr></tr>
				<tr>
					<td class="text text-center" colspan="4"><strong>Total Beban Penyusutan dan Amortisasi</strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan_bulanini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan_bulan_lalu);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persenbulanlalu1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan_budget_bulanini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persenbudget1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensales1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan_tahun_ini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan_tahun_lalu);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persentahunlalu1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan_budget_tahunini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persenbudgetytd1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensalesytd1);?></strong></td>
				</tr>
				<tr>
					<td class="text text-center" colspan="4"><strong>EBITDA</strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_bulan_ini+$tot_penyusutan_bulanini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_bln_lalu+$tot_penyusutan_bulan_lalu);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persenbulanlalu_ebitda);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_bud_ini+$tot_penyusutan_budget_bulanini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persenbudget_ebitda);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensales_ebitda);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_tahun_ini+$tot_penyusutan_tahun_ini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_tahun_lalu+$tot_penyusutan_tahun_lalu);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persentahunlalu_ebitda);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_bud_tahun+$tot_penyusutan_budget_tahunini);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persenbudgetytd_ebitda);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensalesytd_ebitda);?></strong></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
</div>
</div>