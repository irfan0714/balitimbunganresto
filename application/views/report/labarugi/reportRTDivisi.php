<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/labarugi/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="labarugidivisi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>
   
   <ol class="breadcrumb">
    	<?php
		for($i=0;$i<count($judul);$i++){
			?>
			<li><strong><?php echo $judul[$i]; ?></strong></li>
		<?php
		}
		?>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered table-bordered responsive" border="1">
	    	<thead>
				<tr>
					<th width="40%" colspan="4" rowspan="2" ><center>Rekening</center></th>
					<th width="30%" colspan="2"><center>OH</center></th>
					<th width="30%" colspan="2"><center>Resto</center></th>
					<th width="30%" colspan="2"><center>BE</center></th>
					<th width="30%" colspan="2"><center>Head Office</center></th>
					<th width="30%" colspan="2"><center>Total</center></th>
				</tr>
				<tr>
					<th ><center>Aktual</center></th>
					<th><center>%Sales</center></th>
					<th><center>Aktual</center></th>
					<th><center>%Sales</center></th>
					<th><center>Aktual</center></th>
					<th><center>%Sales</center></th>
					<th><center>Aktual</center></th>
					<th><center>%Sales</center></th>
					<th><center>Aktual</center></th>
					<th><center>%Sales</center></th>
				</tr>
			</thead>
		
			<tbody>
				<?php
				if(count($data)==0)
				{
					echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}
				$prev_parent1 = '';
				$prev_parent2 = '';
				$tot_parent11 = 0;
				$tot_parent12 = 0;
				$tot_parent13 = 0;
				$tot_parent14 = 0;
				$tot_parent15 = 0;
				$tot_parent21 = 0;
				$tot_parent22 = 0;
				$tot_parent23 = 0;
				$tot_parent24 = 0;
				$tot_parent25 = 0;
				
				$tot_labarugi1 = 0;
				$tot_labarugi2 = 0;
				$tot_labarugi3 = 0;
				$tot_labarugi4 = 0;
				$tot_labarugi5 = 0;
				
				$tot_penyusutan1 = 0;				
				$tot_penyusutan2 = 0;
				$tot_penyusutan3 = 0;
				$tot_penyusutan4 = 0;
				$tot_penyusutan5 = 0;
				
				$lru_actual1 = 0;
				$lru_actual2 = 0;
				$lru_actual3 = 0;
				$lru_actual4 = 0;
				$lru_actual5 = 0;
				
				$norek_penyusutan =array('42050115','42050120','42050125','42050130','42050135','42050140','42050145');
				
				for($i=0;$i<count($data);$i++){
					$parent1 = $data[$i]['Parent1'];
					$namarekening1 = $data[$i]['NamaRekening1'];
					$parent2 = $data[$i]['Parent2'];
					$namarekening2 = $data[$i]['NamaRekening2'];
					$kdrekening = $data[$i]['KdRekening'];
					$namarekening = $data[$i]['NamaRekening'];
					if($parent1=='40' || $parent1=='43'){
						$nilai1  = $data[$i]['NilaiDiv1']*-1;
						$nilai2  = $data[$i]['NilaiDiv2']*-1;
						$nilai3  = $data[$i]['NilaiDiv3']*-1;
						$nilai4  = $data[$i]['NilaiDiv5']*-1;
					}else{
						$nilai1  = $data[$i]['NilaiDiv1'];
						$nilai2  = $data[$i]['NilaiDiv2'];
						$nilai3  = $data[$i]['NilaiDiv3'];
						$nilai4  = $data[$i]['NilaiDiv5'];
					}
					
					$nilai5  = $nilai1+$nilai2+$nilai3+$nilai4;
					
					if(in_array($kdrekening,$norek_penyusutan)){
						$tot_penyusutan1 += $nilai1;				
						$tot_penyusutan2 += $nilai2;
						$tot_penyusutan3 += $nilai3;
						$tot_penyusutan4 += $nilai4;
						$tot_penyusutan5 += $nilai5;
					}
					
					$netsales5['NetSales'] = $netsales1['NetSales']+$netsales2['NetSales']+$netsales3['NetSales']+$netsales4['NetSales'];
					
					$persensales1 = $netsales1['NetSales'] !=0 ? $nilai1/$netsales1['NetSales']*100 : 0 ;
					$persensales2 = $netsales2['NetSales'] !=0 ? $nilai2/$netsales2['NetSales']*100 : 0 ;
					$persensales3 = $netsales3['NetSales'] !=0 ? $nilai3/$netsales3['NetSales']*100 : 0 ;
					$persensales4 = $netsales4['NetSales'] !=0 ? $nilai4/$netsales4['NetSales']*100 : 0 ;
					$persensales5 = $netsales5['NetSales'] !=0 ? $nilai5/$netsales5['NetSales']*100 : 0 ;
					
					$persen_labarugi_sales1 = $netsales1['NetSales'] !=0 ? $tot_labarugi1/$netsales1['NetSales']*100 : 0 ;
					$persen_labarugi_sales2 = $netsales2['NetSales'] !=0 ? $tot_labarugi2/$netsales2['NetSales']*100 : 0 ;
					$persen_labarugi_sales3 = $netsales3['NetSales'] !=0 ? $tot_labarugi3/$netsales3['NetSales']*100 : 0 ;
					$persen_labarugi_sales4 = $netsales4['NetSales'] !=0 ? $tot_labarugi4/$netsales4['NetSales']*100 : 0 ;
					$persen_labarugi_sales5 = $netsales5['NetSales'] !=0 ? $tot_labarugi5/$netsales5['NetSales']*100 : 0 ;
					
					$isheader = false;
					if($parent1 != $prev_parent1){
						$isheader=true;
						if($prev_parent1 != ''){
							$persensales11 = $netsales1['NetSales'] !=0 ? $tot_parent11/$netsales1['NetSales']*100 : 0 ;
							$persensales12 = $netsales2['NetSales'] !=0 ? $tot_parent12/$netsales2['NetSales']*100 : 0 ;
							$persensales13 = $netsales3['NetSales'] !=0 ? $tot_parent13/$netsales3['NetSales']*100 : 0 ;
							$persensales14 = $netsales4['NetSales'] !=0 ? $tot_parent14/$netsales4['NetSales']*100 : 0 ;
							$persensales15 = $netsales5['NetSales'] !=0 ? $tot_parent15/$netsales5['NetSales']*100 : 0 ;
							
							$persensales21 = $netsales1['NetSales'] !=0 ? $tot_parent21/$netsales1['NetSales']*100 : 0 ;
							$persensales22 = $netsales2['NetSales'] !=0 ? $tot_parent22/$netsales2['NetSales']*100 : 0 ;
							$persensales23 = $netsales3['NetSales'] !=0 ? $tot_parent23/$netsales3['NetSales']*100 : 0 ;
							$persensales24 = $netsales4['NetSales'] !=0 ? $tot_parent24/$netsales4['NetSales']*100 : 0 ;
							$persensales25 = $netsales5['NetSales'] !=0 ? $tot_parent25/$netsales5['NetSales']*100 : 0 ;
						?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><?='TOTAL '. $prev_namarekening2;?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent21);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales21);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent22);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales22);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent23);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales23);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent24);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales24);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent25);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales25);?></td>
							</tr>
							<tr>
								<td colspan="4"><?='TOTAL '. $prev_namarekening1;?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent11);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales11);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent12);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales12);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent13);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales13);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent14);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales14);?></td>
								<td align="right"><?=$mylib->ubah_format($tot_parent15);?></td>
								<td align="right"><?=$mylib->ubah_format($persensales15);?></td>
							</tr>
						<?php
							if($parent1=='42' || $parent1=='43' || $parent1=='45'){ // laba rugi kotor
								switch ($parent1) {
    								case '42':
							        	$labarugi = 'LABA RUGI KOTOR';
							        	break;
							    	case '43':
								        $labarugi = 'LABA RUGI USAHA';
								        $lru_actual1 = $tot_labarugi1;
								        $lru_actual2 = $tot_labarugi2;
								        $lru_actual3 = $tot_labarugi3;
								        $lru_actual4 = $tot_labarugi4;
								        $lru_actual5 = $tot_labarugi5;
							        	break;
							        case '45':
								        $labarugi = 'LABA RUGI SEBELUM PAJAK';
							        	break;
								}
								
							?>
								<tr>
									<td class="text text-center" colspan="4"><strong><?=$labarugi;?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi1);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales1);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi2);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales2);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi3);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales3);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi4);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales4);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi5);?></strong></td>
									<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales5);?></strong></td>
								</tr>
							<?php	
							}
							$tot_parent11 = 0;
							$tot_parent12 = 0;
							$tot_parent13 = 0;
							$tot_parent14 = 0;
							$tot_parent15 = 0;
							
							$tot_parent21 = 0;
							$tot_parent22 = 0;
							$tot_parent23 = 0;
							$tot_parent24 = 0;
							$tot_parent25 = 0;
						}
					?>
						<tr>
							<td colspan="4"><?=$namarekening1;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="3"><?=$namarekening2;?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					<?php	
					}else{ // $parent1 != $prev_parent1
						if($parent2 != $prev_parent2){
							$isheader = true;
							if($prev_parent2 != ''){
								$persensales21 = $netsales1['NetSales'] !=0 ? $tot_parent21 /$netsales1['NetSales']*100 : 0 ;
								$persensales22 = $netsales2['NetSales'] !=0 ? $tot_parent22 /$netsales2['NetSales']*100 : 0 ;
								$persensales23 = $netsales3['NetSales'] !=0 ? $tot_parent23 /$netsales3['NetSales']*100 : 0 ;
								$persensales24 = $netsales4['NetSales'] !=0 ? $tot_parent24 /$netsales4['NetSales']*100 : 0 ;
								$persensales25 = $netsales5['NetSales'] !=0 ? $tot_parent25 /$netsales5['NetSales']*100 : 0 ;
							?>
								<tr>
									<td>&nbsp;</td>
									<td colspan="3"><?='TOTAL '. $prev_namarekening2;?></td>
									<td align="right"><?=$mylib->ubah_format($tot_parent21);?></td>
									<td align="right"><?=$mylib->ubah_format($persensales21);?></td>
									<td align="right"><?=$mylib->ubah_format($tot_parent22);?></td>
									<td align="right"><?=$mylib->ubah_format($persensales22);?></td>
									<td align="right"><?=$mylib->ubah_format($tot_parent23);?></td>
									<td align="right"><?=$mylib->ubah_format($persensales23);?></td>
									<td align="right"><?=$mylib->ubah_format($tot_parent24);?></td>
									<td align="right"><?=$mylib->ubah_format($persensales24);?></td>
									<td align="right"><?=$mylib->ubah_format($tot_parent25);?></td>
									<td align="right"><?=$mylib->ubah_format($persensales25);?></td>
								</tr>
							<?php
								$tot_parent21 = 0;
								$tot_parent22 = 0;
								$tot_parent23 = 0;
								$tot_parent24 = 0;
								$tot_parent25 = 0;
							}
							?>
							<tr>
								<td>&nbsp;</td>
								<td colspan="3"><?=$namarekening2;?></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<?php	
						} // End of $parent2 != $prev_parent2
					}
					//if ($isheader==false){ // End of $parent1 != $prev_parent1
					?>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?=$kdrekening;?></td>
						<td><?=$namarekening;?></td>
						<td align="right"><?=$mylib->ubah_format($nilai1);?></td>
						<td align="right"><?=$mylib->ubah_format($persensales1);?></td>
						<td align="right"><?=$mylib->ubah_format($nilai2);?></td>
						<td align="right"><?=$mylib->ubah_format($persensales2);?></td>
						<td align="right"><?=$mylib->ubah_format($nilai3);?></td>
						<td align="right"><?=$mylib->ubah_format($persensales3);?></td>
						<td align="right"><?=$mylib->ubah_format($nilai4);?></td>
						<td align="right"><?=$mylib->ubah_format($persensales4);?></td>
						<td align="right"><?=$mylib->ubah_format($nilai5);?></td>
						<td align="right"><?=$mylib->ubah_format($persensales5);?></td>
					</tr>
				<?php
					//}
					$prev_parent1 = $parent1;
					$prev_parent2 = $parent2;
					$prev_namarekening1 = $namarekening1;
					$prev_namarekening2 = $namarekening2;
					if($parent1=='40' || $parent1=='43'){
						$tot_labarugi1 += $nilai1;
						$tot_labarugi2 += $nilai2;
						$tot_labarugi3 += $nilai3;
						$tot_labarugi4 += $nilai4;
						$tot_labarugi5 += $nilai5;
					}else{
						$tot_labarugi1 -= $nilai1;
						$tot_labarugi2 -= $nilai2;
						$tot_labarugi3 -= $nilai3;
						$tot_labarugi4 -= $nilai4;
						$tot_labarugi5 -= $nilai5;
					}
					$tot_parent11 += $nilai1;
					$tot_parent12 += $nilai2;
					$tot_parent13 += $nilai3;
					$tot_parent14 += $nilai4;
					$tot_parent15 += $nilai5;
					
					$tot_parent21 += $nilai1;
					$tot_parent22 += $nilai2;
					$tot_parent23 += $nilai3;
					$tot_parent24 += $nilai4;
					$tot_parent25 += $nilai5;
					
					
				} // End Of For
				
				$persensales11 = $netsales1['NetSales'] !=0 ? $tot_parent11/$netsales1['NetSales']*100 : 0 ;
				$persensales12 = $netsales2['NetSales'] !=0 ? $tot_parent12/$netsales2['NetSales']*100 : 0 ;
				$persensales13 = $netsales3['NetSales'] !=0 ? $tot_parent13/$netsales3['NetSales']*100 : 0 ;
				$persensales14 = $netsales4['NetSales'] !=0 ? $tot_parent14/$netsales4['NetSales']*100 : 0 ;
				$persensales15 = $netsales5['NetSales'] !=0 ? $tot_parent15/$netsales5['NetSales']*100 : 0 ;
				
				$persensales21 = $netsales1['NetSales'] !=0 ? $tot_parent21/$netsales1['NetSales']*100 : 0 ;
				$persensales22 = $netsales2['NetSales'] !=0 ? $tot_parent22/$netsales2['NetSales']*100 : 0 ;
				$persensales23 = $netsales3['NetSales'] !=0 ? $tot_parent23/$netsales3['NetSales']*100 : 0 ;
				$persensales24 = $netsales4['NetSales'] !=0 ? $tot_parent24/$netsales4['NetSales']*100 : 0 ;
				$persensales25 = $netsales5['NetSales'] !=0 ? $tot_parent25/$netsales5['NetSales']*100 : 0 ;
				
				?>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3"><?='TOTAL '. $prev_namarekening2;?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent21);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales21);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent22);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales22);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent23);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales23);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent24);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales24);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent25);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales25);?></td>
				</tr>
				<tr>
					<td colspan="4"><?='TOTAL '. $prev_namarekening1;?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent11);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales11);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent12);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales12);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent13);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales13);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent14);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales14);?></td>
					<td align="right"><?=$mylib->ubah_format($tot_parent15);?></td>
					<td align="right"><?=$mylib->ubah_format($persensales15);?></td>
				</tr>
				<?php
				if($parent1=='44' || $parent1=='45'){ // laba rugi kotor
					switch ($parent1) {
						case '44':
				        	$labarugi = 'LABA RUGI SEBELUM PAJAK';
				        	break;
				    	case '45':
					        $labarugi = 'LABA RUGI SETELAH PAJAK';
				        	break;
					}
					
				?>
					<tr>
						<td class="text text-center" colspan="4"><strong><?=$labarugi;?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi1);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales1);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi2);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales2);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi3);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales3);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi4);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales4);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($tot_labarugi5);?></strong></td>
						<td align="right"><strong><?=$mylib->ubah_format($persen_labarugi_sales5);?></strong></td>
					</tr>
				<?php
				}
				$persensusut1 = $netsales1['NetSales'] !=0 ? $tot_penyusutan1/$netsales1['NetSales']*100 : 0 ;
				$persensusut2 = $netsales2['NetSales'] !=0 ? $tot_penyusutan2/$netsales2['NetSales']*100 : 0 ;
				$persensusut3 = $netsales3['NetSales'] !=0 ? $tot_penyusutan3/$netsales3['NetSales']*100 : 0 ;
				$persensusut4 = $netsales4['NetSales'] !=0 ? $tot_penyusutan4/$netsales4['NetSales']*100 : 0 ;
				$persensusut5 = $netsales5['NetSales'] !=0 ? $tot_penyusutan5/$netsales5['NetSales']*100 : 0 ;
				
				$persen_ebitda1 = $netsales1['NetSales'] !=0 ? ($lru_actual1+$tot_penyusutan1)/$netsales1['NetSales']*100 : 0 ;
				$persen_ebitda2 = $netsales2['NetSales'] !=0 ? ($lru_actual2+$tot_penyusutan2)/$netsales2['NetSales']*100 : 0 ;
				$persen_ebitda3 = $netsales3['NetSales'] !=0 ? ($lru_actual3+$tot_penyusutan3)/$netsales3['NetSales']*100 : 0 ;
				$persen_ebitda4 = $netsales4['NetSales'] !=0 ? ($lru_actual4+$tot_penyusutan4)/$netsales4['NetSales']*100 : 0 ;
				$persen_ebitda5 = $netsales5['NetSales'] !=0 ? ($lru_actual5+$tot_penyusutan5)/$netsales5['NetSales']*100 : 0 ;
				
				?>
				<tr></tr>
				<tr>
					<td class="text text-center" colspan="4"><strong>Total Penyusutan dan Amortisasi</strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensusut1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan2);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensusut2);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan3);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensusut3);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan4);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensusut4);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($tot_penyusutan5);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persensusut5);?></strong></td>
				</tr>
				<tr>
					<td class="text text-center" colspan="4"><strong>EBITDA</strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_actual1+$tot_penyusutan1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persen_ebitda1);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_actual2+$tot_penyusutan2);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persen_ebitda2);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_actual3+$tot_penyusutan3);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persen_ebitda3);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_actual4+$tot_penyusutan4);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persen_ebitda4);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($lru_actual5+$tot_penyusutan5);?></strong></td>
					<td align="right"><strong><?=$mylib->ubah_format($persen_ebitda5);?></strong></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>
</div>
</div>