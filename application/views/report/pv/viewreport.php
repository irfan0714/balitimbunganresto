<?php
$gantikursor = "onkeydown=\"changeCursor(event,'order',this)\"";
$reportlib = new report_lib();
if ($excel == "") {
    $this->load->view('header');
    $tglx = date('m-Y');
    $tglawal = "01-" . $tglx;
    $tglskrng = date('d-m-Y');
    ?>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
    <script language="javascript">
        function loading() {
            base_url = $("#baseurl").val();
            $('#tgl_1').Zebra_DatePicker({format: 'd-m-Y'});
            $('#tgl_2').Zebra_DatePicker({format: 'd-m-Y'});
        }
    //echo $excel;
    </script>


    <style>
        .tablebawah{
            border-collapse: collapse;
            border: 1px solid;
        }

        .tablebawah th {
            text-align: left;
            font-weight: bold;
            color: #303641;
            font-size: 13px;
            text-align: center;
        }
        .tablebawah td,th {
            padding: 5px;
            border: 1px solid;
        }
        .tablebawah td {
            background-color: #DDDDDD;
        }
        .tablebawah tr:nth-child(odd) td{
            background-color: #7EFC7E;
        }
        .tableatas{
            border-collapse: collapse;
            border: 1px solid;
        }
        .tableatas td{
            padding: 2px;
            border: 1px solid;
        }
    </style>


    <body>
        <h2><?= $judul; ?></h2>
        <fieldset><legend>Pencarian Data</legend>
            <form method="post" id="form1" name="form1" action="" onSubmit="cek()">
                <table width="70%" class="tableatas">
                    <tr>
                        <td width="25%"><b>Cetak Per</b></td>
                        <td width="5%" align="center"><b>:</b></td>
                        <td colspan="3">
                            <input type="radio" id="pilihan" name="pilihan" value="detail" <?= $aa ?>>Detail&nbsp;
                            <input type="radio" id="pilihan" name="pilihan" value="detailRekening" <?= $bb ?>>Detail Per Rekening&nbsp;
                            <input type="radio" id="pilihan" name="pilihan" value="RekapTRX" <?= $cc ?>>Rekap Per Transaksi&nbsp;
                            <input type="radio" id="pilihan" name="pilihan" value="RekapRekening" <?= $dd ?>>Rekap Per Rekening&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="25%"><b>No Transaksi</b></td>
                        <td width="5%" align="center"><b>:</b></td>
                        <td width="30%"><input type="text" name="no1" id="no1" size="10"></td>
                        <td width="10%"><b>s/d</b></td>
                        <td width="30%"><input type="text" name="no2" id="no2" size="10"></td>
                    </tr>
                    <?php
                    echo $reportlib->write_textbox_combo("Tanggal", "tgl_1", "tgl_2", $tgl_1, $tgl_2, "15", "15", "readonly='readonly'");
                    ?>
                  <!--  <tr>-->
                  <!--    <td><b>TglDokumen</b></td>-->
                  <!--    <td align="center"><b>:</b></td>-->
                  <!--    <td><input type="text" name="tgl_1" id="tgl_1" size="15" value="--><?//=(!empty($pilihan))?$tgl_1:$tglawal;?><!--">-->
                  <!--<!--		<input id="popUp" title="Click to show calendar" style="BORDER-RIGHT: #333333 1px solid; BORDER-TOP: #333333 1px solid; FONT-SIZE: 11px; BORDER-LEFT: #333333 1px solid; BORDER-BOTTOM: #333333 1px solid; HEIGHT: 17px" accesskey="popUpCalc" onClick="popUpCalendar(this, form1.tgl_1, 'dd-mm-yyyy');" type="button" value="V" name="popUpCalc"></td>-->
                  <!--	<td><b>s/d</b></td>-->
                  <!--    <td><input type="text" name="tgl_2" id="tgl_2" size="15" value="--><?//=(!empty($pilihan))?$tgl_2:$tglskrng;?><!--">-->
                  <!--<!--		<input id="popUp" title="Click to show calendar" style="BORDER-RIGHT: #333333 1px solid; BORDER-TOP: #333333 1px solid; FONT-SIZE: 11px; BORDER-LEFT: #333333 1px solid; BORDER-BOTTOM: #333333 1px solid; HEIGHT: 17px" accesskey="popUpCalc" onClick="popUpCalendar(this, form1.tgl_2, 'dd-mm-yyyy');" type="button" value="V" name="popUpCalc"></td>-->
                    <!--  </tr>-->
                    <tr>
                    <input type="hidden" name="par" id="par">
                    <td><input type="button" value="Search" onClick="cek()"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    </tr>
                </table>
            </form>
        </fieldset>
        <?php
    }
    ?>
    <br>

    <?php
    if (!empty($excel)) {
        header('Content-Type: application/vnd.ms-excel;');
        header('Content-Disposition: attachment; filename="report POS.xls"');
    }
    ?>
    <?php if ($pilihan == "detail") { ?>
        <div id="tabel1" style="<?= $display1 ?>">
            <?php
            if ($excel != "ok") {
                ?>

                <div style="text-align:right;width:900px;">
                    <span>
                        <img src="<?= base_url(); ?>public/images/ic_excel.gif" width="16" height="16" title="search" border="0" onclick="ExportToExcel()">&nbsp;Export To Excel
                    </span>

                </div>
            <?php } ?>
            <fieldset><legend>Report Transaksi</legend>
                <table class="tablebawah">
                    <tr> 
                        <th nowrap>NoDokumen</th>
                        <th nowrap>TglDokumen</th>
                        <th nowrap>Nama Bank</th>
                        <th nowrap>No Bukti</th>
                        <th nowrap>Kd Rekening</th>
                        <th nowrap>Nama Rekening</th>
                        <th nowrap>Keterangan Detail</th>
                        <th nowrap>Jumlah</th>
                    </tr>
                    <?php
//    print_r($detail);
                    for ($a = 0; $a < count($detail); $a++) {

                        if ($temp_so_last == $detail[$a]['NoDokumen']) {
                            
                        } else {
                            if ($temp_so_last !== $detail[$a]['NoDokumen'] && !empty($temp_so_last)) {
                                ?>
<!--                                <tr> 
                                    <td colspan="8" align="right">&nbsp;</td>
                                </tr>-->
                                <?php
                            }$temp_so_last = $detail[$a]['NoDokumen'];
                        }
                        if (!empty($detail[$a]['NoDokumen'])) {
                            ?>
                            <tr> 
                                <td align="center" nowrap> 
                            <?php
                            if ($temp_so1 == $detail[$a]['NoDokumen']) {
                                $temp_so_last = $detail[$a]['NoDokumen'];
                                echo "&nbsp;";
                            } else {
                                echo $detail[$a]['NoDokumen'];
                                $temp_so1 = $detail[$a]['NoDokumen'];
                            }
                            ?>
                                </td>
                                <td align="center" nowrap> 
                                    <?php
                                    if ($temp_so2 == $detail[$a]['NoDokumen']) {
                                        
                                    } else {
                                        $tgl = explode("-", $detail[$a]['TglDokumen']);
                                        echo $tgl[2] . "-" . $tgl[1] . "-" . $tgl[0];
                                        $temp_so2 = $detail[$a]['NoDokumen'];
                                    }
                                    ?>
                                </td>
                                <td align="center" nowrap>
                                    <?php
                                    if ($temp_so3 == $detail[$a]['NoDokumen']) {
                                        
                                    } else {
                                        echo $detail[$a]['NamaKasBank'];
                                        $temp_so3 = $detail[$a]['NoDokumen'];
                                    }
                                    ?>
                                </td>
                                <td align="center" nowrap>
                                    <?php
                                    if ($temp_so4 == $detail[$a]['NoDokumen']) {
                                        
                                    } else {
                                        echo $detail[$a]['NoBukti'];
                                        $temp_so4 = $detail[$a]['NoDokumen'];
                                    }
                                    ?>
                                </td>
                                <td align="center" nowrap> <?= $detail[$a]['KdRekening']; ?>      </td>
                                <td align="left" nowrap> <?= $detail[$a]['NamaRekening']; ?>    </td>
                                <td align="left" nowrap><?= $detail[$a]['Keterangan']; ?></td>
                                <td align="right" nowrap><?= number_format($detail[$a]['Jumlah'], 2, ',', '.'); ?></td>
                            </tr>
            <?php
            $total_netto = $detail[$a]['Jumlah'] + $total_netto;
        }
    }
    ?>
                    <tr> 
                        <td colspan="8" align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td colspan="5" align="center"><b>GRAND TOTAL</b></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="right" nowrap><b>
    <?= number_format($total_netto, 2, ',', '.'); ?>
                            </b></td>
                    </tr>
                </table>
            </fieldset>
        </div>
<?php } elseif ($pilihan == "RekapTRX") { ?>
        <div id="tabel2" style="<?= $display2 ?>">
    <?php
    if ($excel != "ok") {
        ?>

                <div style="text-align:right;width:900px;">
                    <span>
                        <img src="<?= base_url(); ?>public/images/ic_excel.gif" width="16" height="16" title="search" border="0" onclick="ExportToExcel()">&nbsp;Export To Excel
                    </span>
                </div>
    <?php } ?>


            <fieldset><legend>Report Transaksi</legend>
                <table width="80%" class="tablebawah">
                    <tr> 
                        <th nowrap>NoDokumen</th>
                        <th nowrap>TglDokumen</th>
                        <th nowrap>Nama Bank</th>
                        <th nowrap>No Bukti</th>
                        <th nowrap>Keterangan</th>
                        <th nowrap>Jumlah</th>
                    </tr>
    <?php
    for ($a = 0; $a < count($transaksi); $a++) {
        $tglx = explode("-", $transaksi[$a]['TglDokumen']);
        ?>
                        <tr>
                            <td align="center" nowrap><?= $transaksi[$a]['NoDokumen']; ?></td>
                            <td align="center" nowrap><?echo $tglx[2]."-".$tglx[1]."-".$tglx[0];?></td>
                            <td align="center" nowrap><?= $transaksi[$a]['NamaKasBank']; ?></td>
                            <td align="center" nowrap><?= $transaksi[$a]['NoBukti']; ?></td>
                            <td align="left" nowrap><?= $transaksi[$a]['keterangan']; ?></td>
                            <td align="right" nowrap><?= number_format($transaksi[$a]['JumlahPayment'], 2, ',', '.'); ?></td>
                        </tr>
        <?php
        $total_trans = $transaksi[$a]['JumlahPayment'] + $total_trans;
    }
    ?>
                    <tr> 
                        <td colspan="6" align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td colspan="4" align="center"><b>GRAND TOTAL</b></td>
                        <td>&nbsp;</td>
                        <td align="right"><b><?= number_format($total_trans, 2, ',', '.'); ?></b></td>
                    </tr>
                </table>
            </fieldset>
        </div>
<?php } else if ($pilihan == "detailRekening") { ?>
        <div id="tabel3" style="<?= $display3 ?>">
    <?php
    if ($excel != "ok") {
        ?>

                <div style="text-align:right;width:900px;">
                    <span>
                        <img src="<?= base_url(); ?>public/images/ic_excel.gif" width="16" height="16" title="search" border="0" onclick="ExportToExcel()">&nbsp;Export To Excel
                    </span>
                </div>
    <?php } ?>
            <fieldset><legend>Report Rekap Per Rekening</legend>
                <table width="60%" class="tablebawah">
                    <tr> 
                        <th nowrap>Kode Rekening</th>
                        <th nowrap>Nama Rekening</th>
                        <th nowrap>Tanggal</th>
                        <th nowrap>NoDokumen</th>
                        <th nowrap>No Bukti</th>
                        <th nowrap>Keterangan Detail</th>
                        <th nowrap>Jumlah</th>
                    </tr>
    <?php
    $total_barang = 0;
    for ($a = 0; $a < count($detailRk); $a++) {
        $tglx = explode("-", $detailRk[$a]['TglDokumen']);
        ?>
                        <tr>
                            <td align="center" nowrap> 
                        <?php
                        if ($temp_so1 == $detailRk[$a]['KdRekening']) {
                            
                        } else {
                            echo $detailRk[$a]['KdRekening'];
                            $temp_so1 = $detailRk[$a]['KdRekening'];
                        }
                        ?>
                            </td>
                            <td align="left" nowrap> 
                                <?php
                                if ($temp_so2 == $detailRk[$a]['KdRekening']) {
                                    
                                } else {
                                    echo $detailRk[$a]['NamaRekening'];
                                    $temp_so2 = $detailRk[$a]['KdRekening'];
                                }
                                ?>
                            </td>
                            <td align="center" nowrap><?echo $tglx[2]."-".$tglx[1]."-".$tglx[0];?></td>
                            <td align="center" nowrap><?= $detailRk[$a]['NoDokumen']; ?></td>
                            <td align="center" nowrap><?= $detailRk[$a]['NoBukti']; ?></td>
                            <td align="left" nowrap><?= $detailRk[$a]['Keterangan']; ?></td>
                            <td align="right" nowrap><?= number_format($detailRk[$a]['Jumlah'], 2, ',', '.'); ?></td>
                        </tr>
        <?php
        $total_barang = $detailRk[$a]['Jumlah'] + $total_barang;
    }
    ?>
                    <tr> 
                        <td colspan="8" align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td colspan="5" align="center"><b>GRAND TOTAL</b></td>
                        <td>&nbsp;</td>
                        <td align="right" nowrap><b><?= number_format($total_barang, 2, ',', '.'); ?></b></td>
                    </tr>
                </table>
            </fieldset>
        </div>
<?php } else { ?>
        <div id="tabel4" style="<?= $display4 ?>">
    <?php
    if ($excel != "ok") {
        ?>

                <div style="text-align:right;width:900px;">
                    <span>
                        <img src="<?= base_url(); ?>public/images/ic_excel.gif" width="16" height="16" title="search" border="0" onclick="ExportToExcel()">&nbsp;Export To Excel
                    </span>
                </div>
    <?php } ?>
            <fieldset><legend>Report Transaksi</legend>
                <table border="1" class="tablebawah">

                    <tr> 
                        <th nowrap>Kode Rekening</th>
                        <th nowrap>Nama Rekening</th>
                        <th nowrap>Jumlah</th>
                    </tr>
    <?php
    $total_nilai = 0;
    for ($a = 0; $a < count($rekening); $a++) {
        ?>
                        <tr> 
                            <td align="center" nowrap><?= $rekening[$a]['KdRekening']; ?></td>
                            <td align="center" nowrap><?= $rekening[$a]['NamaRekening']; ?></td>
                            <td align="right" nowrap><?= number_format($rekening[$a]['Jumlah'], 2, ',', '.'); ?></td>
                        </tr>
                        <?php
                        $total_nilai = $rekening[$a]['Jumlah'] + $total_nilai;
                    }
                    ?>
                    <tr> 
                        <td colspan="3" align="center">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td colspan="2" align="center"><b>GRAND TOTAL</b></td>
                        <td align="right" nowrap><b><?= number_format($total_nilai, 2, ',', '.'); ?></b></td>
                    </tr>
                </table>
            </fieldset>
        </div>
<?php } ?>
</body>
<script>
    function cek()
    {
        if (document.getElementById('pilihan').value !== "")
        {
            if (document.getElementById('no1').value !== "")
            {
                if (document.getElementById('no2').value !== "")
                {
                    no1 = document.getElementById('no1').value;
                    no2 = document.getElementById('no2').value;
                    if (no1 > no2)
                    {
                        alert("No Struk 1 harus lebih kecil dari No Struk 2");
                    }
                    else {
                        document.getElementById('par').value = "";
                        document.form1.submit();
                    }
                }
                else {
                    alert("No Struk 2 tidak boleh kosong");
                }
            }
            else {
                if (document.getElementById('no2').value == "")
                {
                    document.getElementById('par').value = "";
                    document.form1.submit();
                }
                else {
                    alert("No Struk 1 tidak boleh kosong");
                }
            }
        }
        else
        {
            alert("pilih dulu");
        }
    }

    function ExportToExcel() {
        document.getElementById('par').value = "ok";
        document.form1.submit();
    }

</script>
<p>&nbsp</p>
<?php
$this->load->view('footer');
?>