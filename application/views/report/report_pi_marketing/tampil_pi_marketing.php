<br>
<table class="table table-bordered" border="1">
	<thead class="title_table">
		<tr>
			<td align="center">No Dokumen</td>
			<td align="center">No RGM</td>
			<td align="center">No POM</td>
			<td align="center">No PRM</td>
			<td align="center">No Proposal</td>
			<td align="center">Tanggal</td>
			<td align="center">Supplier</td>
			<td align="center">Keterangan</td>
			<td align="center">Rekening</td>
			<td align="center">Nama Rekening</td>
			<td align="center">Sub Divisi</td>
			<td align="center">Rincian</td>
			<td align="center">Nilai</td>
		</tr>		
	</thead>
	<tbody>
	<?php foreach($viewdata AS $val){?>
			<tr>
			<td align="center"><?php echo $val['NoDokumen'];?></td>
			<td align="center"><?php echo $val['RGNo'];?></td>
			<td align="center"><?php echo $val['PONo'];?></td>
			<td align="center"><?php echo $val['NoPr'];?></td>
			<td align="center"><?php echo $val['NoProposal'];?></td>
			<td align="center"><?php echo $val['TglDokumen'];?></td>
			<td align="left"><?php echo $val['Nama'];?></td>
			<td align="left"><?php echo $val['Keterangan'];?></td>
			<td align="left"><?php echo $val['KdRekening'];?></td>
			<td align="left"><?php echo $val['NamaRekening'];?></td>
			<td align="left"><?php echo $val['NamaSubDivisi'];?></td>
			<td align="left"><?php echo $val['Deskripsi'];?></td>
			<td align="right"><?php echo ubah_format($val['Jumlah']);?></td>
			</tr>
	<?php } ?>
	</tbody> 
</table>
<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
