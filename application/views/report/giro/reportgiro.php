<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/giro/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportgiro.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');$('#search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Giro</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
		<th align="center" bgcolor="#f3f7bb">No Giro</th>
		<th align="center" bgcolor="#f3f7bb">No Transaksi</th>
		<th align="center" bgcolor="#f3f7bb">Tanggal</th>
		<th align="center" bgcolor="#f3f7bb">Jenis Giro</th>
		<th align="center" bgcolor="#f3f7bb">Bank Cair</th>
		<th align="center" bgcolor="#f3f7bb">Keterangan</th>
		<th align="center" bgcolor="#f3f7bb">Tgl Cair</th>
		<th align="center" bgcolor="#f3f7bb">Tgl JTO</th>
		<th align="center" bgcolor="#f3f7bb">Status</th>
		<th align="center" bgcolor="#f3f7bb">Jumlah</th>
	</tr>
<?php
	if(count($hasil)>0){
		$total1 = 0;
		for($s=0;$s<count($hasil);$s++)
		{
		    $total1 += (float)$hasil[$s]['NilaiGiro'];
	?>
			<tr>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NoGiro']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NoTransaksi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_tanggal($hasil[$s]['TglTerima'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Jenis']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['BankCair']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Keterangan']?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_tanggal($hasil[$s]['TglCair'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_tanggal($hasil[$s]['TglJTo'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Status']?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['NilaiGiro'])?></td>
			</tr>
	<?php
		}
	?>
		<tr>
			<td nowrap align='center' bgcolor='#f7d7bb' colspan="9"><b>Total</b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total1)?></b></td>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="10"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>