<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
function loading(){
	base_url = $("#baseurl").val();
	$('#tgl1a').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
	$('#tgl2a').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
	$('#tgl1b').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
	$('#tgl2b').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
	$('#tgl1c').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
	$('#tgl2c').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
}
function submitThis()
{
	$("#excel").val("");
	$("#print").val("");
	$("#search").submit();
}

</script>
<body onload="loading()">
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/giro/cari/" onsubmit="return false">
<table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<tr>
				<td nowrap>Pilihan</td>
				<td nowrap>:</td>
				<td nowrap>
				<input type="radio" checked="checked" name="opt" value="BC" <?php echo $cekbc; ?> > <b>Belum Cair</b>
				<input type="radio" name="opt" value="SC" <?php echo $ceksc; ?> > <b>Sudah Cair</b>
				<input type="radio" name="opt" value="JT" <?php echo $cekjt; ?> > <b>Sudah Jatuh Tempo</b>
				<input type="radio" name="opt" value="SG" <?php echo $ceksg; ?> > <b>Semua Giro</b>
				</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<?php
				echo $reportlib->write_textbox_combo("Tanggal Buka","tgl1a","tgl2a",$tgl1a,$tgl2a,"10","10","readonly='readonly'");	
				echo $reportlib->write_textbox_combo("Tanggal JTO","tgl1b","tgl2b",$tgl1b,$tgl2b,"10","10","readonly='readonly'");	
				echo $reportlib->write_textbox_combo("Tanggal Cair","tgl1c","tgl2c",$tgl1c,$tgl2c,"10","10","readonly='readonly'");	
                echo $reportlib->write_plain_combo("Jenis","jenis",$listjenis,$jenis,"Jenis","NamaJenis",3);				
				echo $reportlib->write_plain_combo("Bank Cair","kdkasbank",$listkasbank,$kdkasbank,"KdKasBank","NamaKasBank",3);
				?>
			</table>
		</td>
	</tr>
	<tr bordercolor="#FFFFFF">
		<td colspan="2" align="center"><input type="button" value="Search (*)" onclick="submitThis()">
		<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
		<input type='hidden' value='<?=$excel?>' id="excel" name="excel">
		<input type='hidden' value='<?=$print?>' id="print" name="print">
		</td>
	</tr>
</table>
</form>
</body>
<?php
if($tampilkan)
{
	$this->load->view("report/giro/reportgiro");
}
$this->load->view('footer'); ?>