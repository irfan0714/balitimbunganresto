<?php
error_reporting(0);
?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportdetailticketwilayah.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
	    	<thead>
				<tr>
					<th width="30"><center>NO</center></th>
					<th ><center>NEGARA</center></th>
					<th width="50"><center>TOTAL</center></th>
				</tr>
								
			</thead>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			
			?>
			
			    <tr bgcolor="#f4f4f4">
					<td colspan="100%"><?php echo '<b>With Normal Ticket</b>'; ?></td>
			     </tr>
			
			<?php
			$no=1;
			$tot_jml = 0;
			foreach($hasil AS $val)
			{	
					
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					
					<td align="right"><?php echo $no; ?></td>
					<td align="left"><?php echo $val['country_name']; ?></td>
					<td align="right"><?php echo $val['JML']; ?></td>
				</tr>
				<?php
				
				$no++;
				$tot_jml+=$val['JML'];
			}
			?>
			     <tr bgcolor="#f4f4f4">
					<td colspan="2"><?php echo '<b>SUB TOTAL</b>'; ?></td>
					<td align="right"><?php echo $tot_jml; ?></td>
			     </tr>
			     <tr bgcolor="#f4f4f4">
					<td colspan="100%"><?php echo '<b>With Contract Rate</b>'; ?></td>
			     </tr>
			     <tr>
			        <td align="right"><?php echo $no; ?></td>
					<td align="left"><?php echo 'Country Contract Rate Without Ticket'; ?></td>
			     	<td align="right"><?php echo $total_ticket->total_ticket-$tot_jml; ?></td>
			     </tr>
			     <tr bgcolor="#f4f4f4">
					<td colspan="2"><?php echo '<b>SUB TOTAL</b>'; ?></td>
					<td align="right"><?php echo $total_ticket->total_ticket-$tot_jml; ?></td>
			     </tr>
			     
			     <tr bgcolor="#f4f4f4">
					<td colspan="2"><?php echo '<b>TOTAL</b>'; ?></td>
					<td align="right"><?php echo $total_ticket->total_ticket; ?></td>
			     </tr>
			</tbody>
		</table>
		
		<br><br>
		<table class="table table-bordered responsive">
	    	<thead>
				<tr>
					<th width="30"><center>NO</center></th>
					<th ><center>DETAIL WILAYAH INDONESIA DENGAN NORMAL TIKET</center></th>
					<th width="50"><center>TOTAL</center></th>
				</tr>
								
			</thead>
			<tbody>
			
			<?php
			if(count($hasil_kota)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$nos=1;
			$tot_kota = 0;
			foreach($hasil_kota AS $vals)
			{	
	            
					
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					
					<td align="right"><?php echo $nos; ?></td>
					<td align="left"><?php echo $vals['nama_kota']; ?></td>
					<td align="right"><?php echo $vals['JLMS']; ?></td>
				</tr>
				<?php
				
				$nos++;
				$tot_kota+=$vals['JLMS'];
			}
			?>
			
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>