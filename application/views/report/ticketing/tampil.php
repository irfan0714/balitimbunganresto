<?php
error_reporting(0);

unset($arr_data);

foreach($hasil as $val)
{
	$arr_data["list_tgl"][$val["tanggal"]]=$val["tanggal"];
	$arr_data["jenis"][$val["tanggal"]][$val["jenis"]]=$val["jenis"];
	$arr_data["nama_jenis"][$val["tanggal"]][$val["jenis"]]=$val["NamaJenis"];
	$arr_data["total_qty"][$val["tanggal"]][$val["jenis"]]=$val["total_qty"];
	$arr_data["total_harga"][$val["tanggal"]][$val["jenis"]]=$val["total_harga"];
	
	$arr_data["jumlah_qty"][$val["tanggal"]] +=$val["total_qty"];
	$arr_data["jumlah_ttl"][$val["tanggal"]] +=$val["total_harga"];
	
	$arr_data["total_perjenis"][$val["jenis"]] +=$val["total_qty"];
	$arr_data["total_harga_perjenis"][$val["jenis"]] +=$val["total_harga"];
	
	$arr_data["total_all_jenis"] +=$val["total_qty"];
	$arr_data["total_all_harga"] +=$val["total_harga"];
	
}

$arr_jb = array('Cash','Debit','Kredit','Voucher');

foreach($jenis_bayar as $val)
{
	$arr_data["nilai_tunai"][$val["tglTiket"]][$arr_jb[0]] = $val["n_tunai"];
	$arr_data["nilai_debit"][$val["tglTiket"]][$arr_jb[1]] = $val["n_debit"];
	$arr_data["nilai_kredit"][$val["tglTiket"]][$arr_jb[2]] = $val["n_kredit"];
	$arr_data["nilai_voucher"][$val["tglTiket"]][$arr_jb[3]] = $val["n_voucher"];
}

/*echo "<pre>";
print_r($arr_data["nilai_tunai"]);
echo "</pre>";*/

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportdetailticket.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
	    	<thead>
				<tr>
					<th width="30" rowspan="2"><center>NO</center></th>
					<th width="200" rowspan="2"><center>TANGGAL</center></th>
					<th colspan="4"><center>JENIS</center></th>
					<th rowspan="2"><center>TOTAL JENIS</center></th>
					<th colspan="4"><center>JUMLAH (RP)</center></th>
					<th colspan="4" style="background: #ff7575;"><center>JENIS PEMBAYARAN</center></th>
					<th rowspan="2"><center>TOTAL</center></th>
				</tr>
				<tr>
					<?php
					foreach($listjenis as $jns)
					{
						?>
						<th><center><?php echo $jns["NamaJenis"]; ?></center></th>
						<?php
					}
					
					foreach($listjenis as $jns)
					{
						?>
						<th><center><?php echo $jns["NamaJenis"]; ?></center></th>
						<?php
					}
					?>
					
					<?php
					foreach($arr_jb as $key)
					{
						?><td><center><?php echo $key; ?></center></td><?php	
					}
					?>
					
				</tr>
				
			</thead>
			<tbody>
			
			<?php
			if(count($arr_data["list_tgl"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$no=1;
			foreach($arr_data["list_tgl"] as $tanggal => $val)
			{		
				$jumlah_qty = $arr_data["jumlah_qty"][$tanggal];
				$jumlah_ttl = $arr_data["jumlah_ttl"][$tanggal];
				
				$nilai_tunai = $arr_data["nilai_tunai"][$tanggal][$arr_jb[0]];
				$nilai_debit = $arr_data["nilai_debit"][$tanggal][$arr_jb[1]];
				$nilai_kredit = $arr_data["nilai_kredit"][$tanggal][$arr_jb[2]];
				$nilai_voucher = $arr_data["nilai_voucher"][$tanggal][$arr_jb[3]];
				
	            $bgcolor = "";
	            if($no%2==0)
	            {
	                $bgcolor = "background:#f7f7f7;";
	            }
				
				if($jumlah_ttl <> ($nilai_tunai+$nilai_debit+$nilai_kredit+$nilai_voucher)){
					$bgcolor = "background:#ffff00;";
				}
					
				?>
				<tr title="<?php echo $tanggal; ?>" style="<?php echo $bgcolor; ?>" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td align="center"><?php echo $no; ?></td>
					<td align="center"><?php echo $mylib->ubah_tanggal($tanggal) ?></td>
					
					<?php
					foreach($listjenis as $jns)
					{
						$total_qty = $arr_data["total_qty"][$tanggal][$jns["idjenis"]];
						
						?><td align="right"><?php echo $total_qty; ?></td><?php
					}
					
					?><td align="right"><?php echo $jumlah_qty; ?></td><?php
					
					foreach($listjenis as $jns)
					{
						$total_harga = $arr_data["total_harga"][$tanggal][$jns["idjenis"]];
						
						?><td align="right"><?php echo $mylib->ubah_format($total_harga); ?></td><?php
	
					}
					?>
					
					<td align="right"><?php echo $mylib->ubah_format($nilai_tunai); ?></td>
					<td align="right"><?php echo $mylib->ubah_format($nilai_debit); ?></td>
					<td align="right"><?php echo $mylib->ubah_format($nilai_kredit); ?></td>
					<td align="right"><?php echo $mylib->ubah_format($nilai_voucher); ?></td>
					
					<td align="right"><?php echo $mylib->ubah_format($jumlah_ttl); ?></td>
				</tr>
				<?php
				
				$no++;
				
				
				$gt_tunai += $nilai_tunai;
				$gt_debit += $nilai_debit;
				$gt_kredit += $nilai_kredit;
				$gt_voucher += $nilai_voucher;
			}
			?>
				
			<tr>
				<td colspan="2">&nbsp;</td>
				<?php
				foreach($listjenis as $jns)
				{
					$total_perjenis = $arr_data["total_perjenis"][$jns["idjenis"]];
					
					?><td align="right"><b><?php echo $total_perjenis; ?></b></td><?php
				}
				?>
				<td align="right"><b><?php echo $arr_data["total_all_jenis"]; ?></b></td>
				
				<?php
				foreach($listjenis as $jns)
				{
					$total_harga_perjenis = $arr_data["total_harga_perjenis"][$jns["idjenis"]];
					
					?><td align="right"><b><?php echo $mylib->ubah_format($total_harga_perjenis); ?></b></td><?php
				}
				?>
				
				<td align="right"><b><?php echo $mylib->ubah_format($gt_tunai); ?></b></td>
				<td align="right"><b><?php echo $mylib->ubah_format($gt_debit); ?></b></td>
				<td align="right"><b><?php echo $mylib->ubah_format($gt_kredit); ?></b></td>
				<td align="right"><b><?php echo $mylib->ubah_format($gt_voucher); ?></b></td>
				
				<td align="right"><b><?php echo $mylib->ubah_format($arr_data["total_all_harga"]); ?></b></td>
			</tr>
			
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>