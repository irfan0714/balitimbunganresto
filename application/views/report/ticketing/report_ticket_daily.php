<?php 
$this->load->view('header');
    
    
?>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
    
    
    <div class="main-content">
    
        <ol class="breadcrumb bc-3">
            <li>
                <a href="index.php">
                    <i class="entypo-home"></i>Home
                </a>
            </li>
            <li>NPM</li>
            <li class="active"><strong>Report Ticketing</strong></li>
        </ol>
        
        <hr/>
        <br/>
        
        <form method="POST" name="theform" id="theform">
        
        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            
            <table class="table table-bordered responsive">
                <thead>
                    <tr>
                        <th width="100">Tanggal</th>
                        <th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
                    </tr>
                    
                    
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
                </thead>
                
            </table> 
            <br><br>
            
                             <table class="table table-bordered responsive" style="color: black;" border="<?php echo $table_border; ?>">
                        <thead>
                            <tr>
                                <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
                                <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
                                <th rowspan="2" style="vertical-align: middle;">Hari</th>
                                <th style="vertical-align: middle; text-align: center;" colspan="4">Tiket</th>
                            </tr>
                            
                            <tr>
								<th style="text-align: right;">Asing</th>
                                <th style="text-align: right;">Lokal</th>
                                <th style="text-align: right;">Jumlah Ticket</th>
                                <th style="text-align: right;">Total (Rp)</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <?php
                            $no = 1; 
                            
                                
                                if($btn_excel)
                                {
                                ?>
                                <tr style="<?php echo $bg_color; ?> color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo date("d/m/Y", $date); ?></td>
                                    <td style="<?php echo $bg_hari; ?>"><?php echo date("l", $date); ?></td>
                                    <td align="right"><?php echo format_number($lokal, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($asing, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($all_ticket, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo ""; ?></td>
                                    <td align="right"><?php echo format_number($total_OH, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($total_LR, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($total_BE, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($TotalNilai, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($komisi, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($compliment, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($omzet, 0, "", "", "ind"); ?></td>
                                </tr> 
                                <?php    
                                }
                                else
                                {
                        ?>
                            <tr style="<?php echo $bg_color; ?> color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                <td><?php echo $no; ?></td>
                                <td><?php echo date("d/m/Y", $date); ?></td>
                                <td style="<?php echo $bg_hari; ?>"><?php echo date("l", $date); ?></td>
                                <td align="right"><?php echo format_number($lokal); ?></td>
                                <td align="right"><?php echo format_number($asing); ?></td>
                                <td align="right"><?php echo format_number($all_ticket); ?></td>
                                <td align="right"><?php echo ""; ?></td>
                                <td align="right"><?php echo format_number($total_OH); ?></td>
                                <td align="right"><?php echo format_number($total_LR); ?></td>
                                <td align="right"><?php echo format_number($total_BE); ?></td>
                                <td align="right"><?php echo format_number($TotalNilai); ?></td>
                                <td align="right"><?php echo format_number($komisi); ?></td>
                                <td align="right"><?php echo format_number($compliment); ?></td>
                                <td align="right"><?php echo format_number($omzet); ?></td>
                            </tr>
                        
                        </tbody>
                        
                        
                        <?php
                            if($btn_excel)
                            {
                                ?>
                                <tfoot>
                                    <tr style="text-align: right; font-weight: bold; color: black;">
                                        <td colspan="3">Grand Total</td>
                                        <td align="right"><?php echo format_number($arr_total["lokal"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["asing"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["all_ticket"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number(0); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["OH"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["LR"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["BE"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Nilai"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Komisi"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Compliment"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Omzet"], 0, "", "", "ind"); ?></td>
                                    </tr>
                                </tfoot> 
                                <?php     
                            }
                            else                                                 
                            {
                                ?>
                                 <tfoot>
                                    <tr style="text-align: right; font-weight: bold; color: black;">
                                        <td colspan="3">Grand Total</td>
                                        <td align="right"><?php echo format_number($arr_total["lokal"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["asing"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["all_ticket"]); ?></td>
                                        <td align="right"><?php echo format_number(0); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["OH"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["LR"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["BE"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Nilai"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Komisi"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Compliment"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Omzet"]); ?></td>
                                    </tr>
                                </tfoot> 
                                <?php
                            }
                        ?>
                        
                    </table>
                    <?php
                }
                
                if(!$btn_excel)
                {
            ?>
            
        
        </div>
        
        </form>
        <?php 
                
        ?>
        
<?php 
            include("footer.php"); 

            }
?>