<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Ticketing Pertanggal";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Reprot <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_ticketing/search_report/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">
	    
	       <tr>
	            <td class="title_table" width="150">Period</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Type Report</td>
	            <td> 
	            	<select size="1" height="1" name ="tipe_report" id="tipe_report" class="form-control-new">
					<option  <?php if($tipe_report=='1'){ echo "selected='selected'"; } ?> value='1'> Per Tanggal </option>
					<option  <?php if($tipe_report=='2'){ echo "selected='selected'"; } ?> value='2'> Penjualan Ticket </option>
					<option  <?php if($tipe_report=='3'){ echo "selected='selected'"; } ?> value='3'> Sebaran Wilayah </option>
					<option  <?php if($tipe_report=='4'){ echo "selected='selected'"; } ?> value='4'> Loyalitas Customer </option>
					</select>
	            </td>
	        </tr> 
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php

if ($tampilkanDT) 
{
    if($tipe_report=='1'){
				$this->load->view("report/ticketing/tampil");
			}else if($tipe_report=='2'){
				//penjualan ticket
				$this->load->view("report/ticketing/tampil_ticket");
			}else if($tipe_report=='3'){
				//sebaran wilayah
				$this->load->view("report/ticketing/tampil_wilayah");
			}else if($tipe_report=='4'){
				//loyalitas customer
				$this->load->view("report/ticketing/tampil_customer");
			}
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>