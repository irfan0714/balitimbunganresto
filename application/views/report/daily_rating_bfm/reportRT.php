<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
	function pop_up_pemakaian_voucher(v_date)
	{
		url = '<?= base_url()?>/index.php/report/daily_sales/voucher/';
	    try{
	        windowOpener(600, 800, 'Pemakaian Voucher', url+v_date, 'Pemakaian Voucher') ;   
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="dailysales.xls"');
    }
   
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    $arr_data  = array();
	$arr_total = array();
	
    for($i=$mylib->parsedate($v_date_from);$i<=$mylib->parsedate($v_date_to);$i=$i+86400)
    {
        $arr_data["list_date"][$i] = $i;
    }
    
    
    
	
    
    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold; border-collapse: collapse;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT RATING DAILY '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
		        <th rowspan="2" style="vertical-align: middle;">Jenis</th>
		        <th rowspan="2" style="vertical-align: middle;">PCode</th>
		        <th rowspan="2" style="vertical-align: middle;">Nama</th>
		        <th rowspan="2" style="vertical-align: middle;">Qty</th>
		        <th rowspan="2" style="vertical-align: middle;">Netto</th>
		        <th rowspan="2" style="vertical-align: middle; display:none;">Berat</th>
		        <th rowspan="2" style="vertical-align: middle; display:none;">Netto Berat</th>
		    </tr>
		</thead>

		<tbody>
		<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td align="left" colspan="100%"><b>MAKANAN</b></td>
	    </tr>
		';

    $no = 1; 
    $bg_color='';
    foreach($makanan as $val)
    {
       
    	$echo .= '
    	<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td align="center">'.$val['NamaGroupBarang'].'</td>
	        <td align="center">'.$val['PCode'].'</td>
	        <td align="left">'.$val['NamaLengkap'].'</td>
	        <td align="center">'.$val['Qty'].'</td>
	        <td align="right">'.$val['Netto'].'</td>
	        <td align="center" style="vertical-align: middle; display:none;">'.$val['Berat'].'</td>
	        <td align="right" style="vertical-align: middle; display:none;">'.$val['NettoBerat'].'</td>
	    </tr>
    	';
        

        $no++;
    }
    
	$echo .= '
		</tbody>';

	$echo .= '
		</table>';
		
		
		
	$echo .= '<br><br>
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
		        <th rowspan="2" style="vertical-align: middle;">Jenis</th>
		        <th rowspan="2" style="vertical-align: middle;">PCode</th>
		        <th rowspan="2" style="vertical-align: middle;">Nama</th>
		        <th rowspan="2" style="vertical-align: middle;">Qty</th>
		        <th rowspan="2" style="vertical-align: middle;">Netto</th>
		    </tr>
		</thead>

		<tbody>
		<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td align="left" colspan="100%"><b>MINUMAN</b></td>
	    </tr>
		';

    $no = 1; 
    $bg_color='';
    foreach($minuman as $val)
    {
       
    	$echo .= '
    	<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td align="center">'.$val['NamaGroupBarang'].'</td>
	        <td align="center">'.$val['PCode'].'</td>
	        <td align="left">'.$val['NamaLengkap'].'</td>
	        <td align="center">'.$val['Qty'].'</td>
	        <td align="right">'.$val['Netto'].'</td>
	    </tr>
    	';
        

        $no++;
    }
    
	$echo .= '
		</tbody>';

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Report Rating Daily ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>