<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
	function pop_up_pemakaian_voucher(v_date)
	{
	    try{
	        windowOpener(600, 800, 'Pemakaian Voucher', 'report_daily_pop_up_pemakaian_voucher.php?v_date='+v_date, 'Pemakaian Voucher')    
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="report_sales_persediaan.xls"');
	}
   
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    //saldo awal
    for($i=0;$i<count($stock);$i++){
        $pcode = $stock[$i]['PCode'];    
        $namabarang = $stock[$i]['NamaLengkap'];    
        $saldoawal = $stock[$i]['saldoawal'];
        
        $arr_data["stok"][$pcode] = $saldoawal;
        $arr_data["namabarang"][$pcode] = $namabarang;
    }
    
    //mutasi
    for($i=0;$i<count($mutasi);$i++){
        $pcode = $mutasi[$i]['PCode'];    
        $in = $mutasi[$i]['jml_in'];
        $out = $mutasi[$i]['jml_out'];
        
        $arr_data["stok"][$pcode] += $in - $out;
    }
    
    for($i=0;$i<count($hasil);$i++){
        $pcode = $hasil[$i]['PCode'];    
        $qty = $hasil[$i]['Qty'];
        $qtytoday = $hasil[$i]['QtyToday'];
        
        $arr_data["Qty_sales"][$pcode] = $qty;
        $arr_data["Qty_today"][$pcode] = $qtytoday;
    }
    
	$table_border = 0;
	$style ="";
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
        $style = "style='border-collapse: collapse;'";
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT SALES & PERSEDIAAN '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'" '.$style.'>
		<thead>
	    	<tr>
		        <th width="30">No</th>
		        <th style="text-align: center;">PCode</th>
		        <th style="text-align: center;">Nama Barang</th>
		        <th style="text-align: center;">Sales Hari ini</th>
		        <th style="text-align: center;">Sales Bulan Ini</th>
		        <th style="text-align: center;">Persediaan</th>
		    </tr>
		</thead>

		<tbody>
		';

    $no = 1;
    $saldo_awal = 0;
    $jml_sales = 0;
    $jml_sedia = 0; 
    $jml_sales_today = 0;
    foreach($stock as $val)
    {
        $qty = $arr_data["Qty_sales"][$val['PCode']]+0;
        $qtytoday = $arr_data["Qty_today"][$val['PCode']]+0;
        $persediaan = $arr_data["stok"][$val['PCode']]+0;
        $namabarang = $arr_data["namabarang"][$val['PCode']];
        
    	$echo .= '
    	<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td align="center">'.$val['PCode'].'</td>
	        <td align="left">'.$namabarang.'</td>
	        <td align="right">'.number_format($qtytoday).'</td>
	        <td align="right">'.number_format($qty).'</td>
	        <td align="right">'.$persediaan.'</td>
	       </tr>
    	';

        $no++;
        $jml_sales +=$qty;
        $jml_sedia +=$persediaan;
        $jml_sales_today += $qtytoday;
    }
    
	$echo .= '
		</tbody>';
	    
	$echo .= '
         <tfoot>
            <tr style="text-align: right; font-weight: bold; color: black;">
                <td colspan="3">Grand Total</td>
                <td align="right">'.$mylib->format_number($jml_sales_today, 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($jml_sales, 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($jml_sedia, 0, "", "", "ind").'</td>
                 </tr>
        </tfoot> 
	    ';

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Report Sales dan Persediaan ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>