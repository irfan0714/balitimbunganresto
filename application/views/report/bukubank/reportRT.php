<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_buku_bank/search_report/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="bukubank.xls"');
	}
    
    if($excel == "excel")
    {
        $table_border = 1;
        $style = "style='border-collapse: collapse;'";
    }else{
		$table_border = 0;
        $style = "style='border-collapse: collapse;'";
	}
	
	if ($excel != "excel") {
	    ?>
	    <span style="margin-bottom:10px; display: inline-table;">
	        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
	                    document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
	    </span>
        <?php
    }
    
	if($excel == "excel"){
	?>
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">REPORT BUKU BANK</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
	<?php
	}
	?>
	
	<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'" '.$style.'>
		<thead>
	    	<tr>
		        <th style="vertical-align: middle; text-align: center;">Tanggal</th>
		        <th style="vertical-align: middle; text-align: center;">No Dokumen</th>
		        <th style="vertical-align: middle; text-align: center;">Keterangan</th>
		        <th style="vertical-align: middle; text-align: center;">Masuk</th>
		        <th style="vertical-align: middle; text-align: center;">Keluar</th>
		        <th style="vertical-align: middle; text-align: center;">Saldo</th>
		    </tr>
		</thead>
		<tbody>
		<tr>
			<td>&nbsp;</td>	
			<td>&nbsp;</td>	
			<td>Saldo Awal</td>	
			<td>&nbsp;</td>	
			<td>&nbsp;</td>	
			<td align="right"><?= number_format($saldoawal[0]['akhir'],0,',','.');?></td>	
		</tr>
		<?php
		$totdebet=0;
		$totkredit=0;
    	foreach($data as $val)
    	{
    		$tgldokumen = $val['TglDokumen'];
    		$nodokumen = $val['NoDokumen'];
    		$keterangan = $val['Keterangan'];
    		$debet = $val['Debet'];
    		$kredit = $val['Kredit'];
    		$totdebet += $debet;
			$totkredit += $kredit;
    	?>    
    		<tr>
		        <td align="center"><?=$tgldokumen;?></td>
		        <td align="left"><?=$nodokumen;?></td>
		        <td align="left"><?=$keterangan;?></td>
		        <td align="right"><?=number_format($debet,0,',','.');?></td>
		        <td align="right"><?=number_format($kredit,0,',','.');?></td>
	       </tr>
		<?php
    	}
    	$saldoakhir = $saldoawal[0]['akhir']+$totdebet-$totkredit;
    	?>
    	<tr>
			<td>&nbsp;</td>	
			<td>&nbsp;</td>	
			<td>Saldo Akhir</td>	
			<td align="right"><?= number_format($totdebet,0,',','.');?></td>	
			<td align="right"><?= number_format($totkredit,0,',','.');?></td>	
			<td align="right"><?= number_format($saldoakhir,0,',','.');?></td>	
		</tr>
		</tbody>
	</table>
</form>
	</div>
</div>