<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Buku Bank";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_buku_bank/search_report/">       
        	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            	<table class="table table-bordered responsive">
                	<thead>
	                	<tr>
		                    <td width="150"><b>Tahun</b></td>
		                    <td width="3">:</td>
	                         <td> 
	                            <select class="form-control-new" name="v_tahun" id="v_tahun">
	                                    <option <?= $v_tahun=='2016' ? 'selected' : '' ; ?> value="2016">2016</option>
	                                    <option <?= $v_tahun=='2017' ? 'selected' : '' ;?> value="2017">2017</option>
	                                    <option <?= $v_tahun=='2018' ? 'selected' : '' ;?> value="2018">2018</option>
	                                    <option <?= $v_tahun=='2019' ? 'selected' : '' ;?> value="2019">2019</option>
	                                    <option <?= $v_tahun=='2020' ? 'selected' : '' ;?> value="2020">2020</option>
	                                    <option <?= $v_tahun=='2021' ? 'selected' : '' ;?> value="2021">2021</option>
	                                    <option <?= $v_tahun=='2022' ? 'selected' : '' ;?> value="2022">2022</option>
	                            </select>
	                         </td>
		                </tr> 
                		<tr>
		                    <td width="150"><b>Bulan</b></td>
		                    <td>:</td>
		                    <td> 
	                            <select class="form-control-new" name="v_bulan" id="v_bulan">
                                    <option  <?= $v_bulan=='1' ? 'selected' : '' ; ?> value="1">Januari</option>
                                    <option  <?= $v_bulan=='2' ? 'selected' : '' ; ?> value="2">Februari</option>
                                    <option  <?= $v_bulan=='3' ? 'selected' : '' ; ?> value="3">Maret</option>
                                    <option  <?= $v_bulan=='4' ? 'selected' : '' ; ?> value="4">April</option>
                                    <option  <?= $v_bulan=='5' ? 'selected' : '' ; ?> value="5">Mei</option>
                                    <option  <?= $v_bulan=='6' ? 'selected' : '' ; ?> value="6">Juni</option>
                                    <option  <?= $v_bulan=='7' ? 'selected' : '' ; ?> value="7">Juli</option>
                                    <option  <?= $v_bulan=='8' ? 'selected' : '' ; ?> value="8">Agustus</option>
                                    <option  <?= $v_bulan=='9' ? 'selected' : '' ; ?> value="9">Sepetember</option>
                                    <option  <?= $v_bulan=='10' ? 'selected' : '' ; ?> value="10">Oktober</option>
                                    <option  <?= $v_bulan=='11' ? 'selected' : '' ; ?> value="11">November</option>
                                    <option  <?= $v_bulan=='12' ? 'selected' : '' ; ?> value="12">Desember</option>
	                            </select>
		                    </td>
		                </tr>
		                <tr>
		                    <td width="150"><b>Kas/Bank</b></td>
		                    <td width="3">:</td>
	                         <td> 
	                            <select class="form-control-new" name="v_bank" id="v_bank">
	                                <?php
	                                	foreach($listbank as $bank){
	                                ?>
											<option  <?= $v_bank==$bank['KdKasBank'] ? 'selected' : '' ; ?> value="<?=$bank['KdKasBank'];?>"><?=$bank['NamaKasBank'];?></option>		
									<?php
										}
	                                ?>
	                            </select>
	                         </td>
		                </tr> 
                	</thead>
                	<tr>
	                    <td>&nbsp;</td>
	                    <td>&nbsp;</td>
	                    <td>
	                        <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
	                        <input type='hidden' value='<?= $print ?>' id="print" name="print">
	                        <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                        <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
	                    </td>
	                </tr>
            	</table> 
            </div>
	    </form> 
	</div>
</div>

<?php
if ($tampilkan) 
{
   	$this->load->view("report/bukubank/reportRT");
}

$this->load->view('footer');
?>