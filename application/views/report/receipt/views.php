<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
function loading(){
	base_url = $("#baseurl").val();
    $('#tgl1').Zebra_DatePicker({format: 'd-m-Y'});
    $('#tgl2').Zebra_DatePicker({format: 'd-m-Y'});
}
function submitThis()
{
	$("#excel").val("");
	$("#print").val("");
//	$("#search").submit();
    document.getElementById("search").submit();
}

</script>
<body onload="loading()">
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/receipt/cari/" onsubmit="return false">
<table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<tr>
				<td nowrap>Pilihan</td>
				<td nowrap>:</td>
				<td nowrap>
				<input type="radio" name="opt" value="RH" <?php echo $cekrh; ?> > <b>Rekap Harian</b>
				<input type="radio" name="opt" value="RT" <?php echo $cekrt; ?> > <b>Rekap per Transaksi</b>
				<input type="radio" name="opt" value="DT" <?php echo $cekdt; ?> > <b>Detail per Transaksi</b>
				</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<?php
				echo $reportlib->write_textbox_combo("Tanggal","tgl1","tgl2",$tgl1,$tgl2,"15","15","readonly='readonly'");
 				echo $reportlib->write_plain_combo("Kas Bank","kdkasbank",$listkasbank,$kdkasbank,"KdKasBank","NamaKasBank",3);
					echo $reportlib->write_plain_combo("Rekening","kdrekening",$listrekening,$kdrekening,"KdRekening","NamaRekening",3);
				?>
				<tr>
					<td nowrap>Urut per Rekening</td>
					<td nowrap>:</td>
					<td nowrap>
					<input type="radio" name="opt1" value="YY" <?php echo $cekyy; ?> > <b>Ya</b>
					<input type="radio" name="opt1" value="TT" <?php echo $cektt; ?> > <b>Tidak</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bordercolor="#FFFFFF">
		<td colspan="2" align="center"><input type="button" value="Search (*)" onclick="submitThis()">
		<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
		<input type='hidden' value='<?=$excel?>' id="excel" name="excel">
		<input type='hidden' value='<?=$print?>' id="print" name="print">
		</td>
	</tr>
</table>
</form>
</body>
<?php
if($tampilkanRH)
{
	$this->load->view("report/receipt/reportRH");
}
if($tampilkanRT)
{
	$this->load->view("report/receipt/reportRT");
}
if($tampilkanDT)
{
	$this->load->view("report/receipt/reportDT");
}
$this->load->view('footer'); ?>