<?php

$this->load->helper('print_helper');
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

//$ftext = printer_open("\\\\".$_SERVER['REMOTE_ADDR']."\\HPLaserJ");//
//$ftext = printer_open("\\\\192.168.0.30\\EPSON");
//$ftext = printer_open("\\\\".$ip."\\".$nm_printer);
//$ftext = printer_open($_SERVER['REMOTE_ADDR']);
$ftext = '';
//$ftext = printer_open("EPSON");
//printer_set_option($ftext, PRINTER_MODE, "raw");
//printer_set_option($ftext, PRINTER_COPIES, "1");

foreach ($header as $key => $val) {
	
	$alamatPT = $store[0]['Alamat1PT'];
	$tgl = $val['Tanggal'];
	$tgl_1 = explode("-", $tgl);
	$tgl_tampil = $tgl_1[2] . "/" . $tgl_1[1] . "/" . $tgl_1[0];

	$q = "SELECT KdDivisi, Keterangan, Minimum FROM	memo_struk 
			WHERE 1 
			AND KdDivisi IN(SELECT KdDivisi FROM kassa WHERE id_kassa = '".$val['NoKassa']."') 
			AND status = 'Y'"; 
	$qry = mysql_query($q);
	$num123 = mysql_num_rows($qry);
	if($num123>0){
		$hsl = mysql_fetch_object($qry);
		$exp = explode("||",$hsl->Keterangan);
		$ket1 = $exp[0];
		$ket2 = $exp[1];
		$ket3 = $exp[2];
		$minimum = $hsl->Minimum;
	}

	
	mysql_free_result($qry);

	$q = "SELECT NoStruk, NomorVoucher, NilaiVoucher FROM transaksi_detail_voucher WHERE NoStruk='".$val['NoStruk']."'";


	$qry = mysql_query($q);
	$num = mysql_num_rows($qry);
	$num2=0;
	//Cek Apakah Pembayaran menggunakan Voucher
	if($num>0){
		$listvoucher = "";
		while($hsl = mysql_fetch_object($qry)){
			$nomorvoucher = $hsl->NomorVoucher;
			$nilaivoucher = $hsl->NilaiVoucher;
			if($listvoucher==""){
				$listvoucher = "'".$hsl->NomorVoucher;
			}
			else{
					$listvoucher = $listvoucher."','".$hsl->NomorVoucher;
			}
		}
		$listvoucher = $listvoucher."'";
		//Cek Sisa Voucher (tabel voucher vs tabel transaksi_detail_voucher)
		//tampilkan yang sisa saja
		$qirf = "SELECT * FROM voucher WHERE novoucher in($listvoucher)";
		$qryirf = mysql_query($qirf);
		$numirf = mysql_num_rows($qryirf);
		$jenis="";
		if($numirf>0){
			$hslirf = mysql_fetch_object($qryirf);
			$jenis     = $hslirf->jenis;
		}
		
		if($jenis==5){
			$num2 = 0;
		}else{
			$q2 = "SELECT novoucher, selisih FROM (
				SELECT v.addDate,v.novoucher,v.nominal,IFNULL(totalpakai,0),(v.nominal-IFNULL(totalpakai,0)) AS selisih
				FROM voucher v
				LEFT JOIN (SELECT NomorVoucher, SUM(NilaiVoucher) AS totalpakai 
					   FROM transaksi_detail_voucher
					   WHERE NomorVoucher IN($listvoucher)
					   GROUP BY NomorVoucher) AS d
					ON v.novoucher=d.`NomorVoucher`
				WHERE 1
				AND v.novoucher IN($listvoucher)
				ORDER BY v.novoucher ASC
			)AS a 
			WHERE selisih!=0";
			$qry2 = mysql_query($q2);
			$num2 = mysql_num_rows($qry2);	
			if($num2>0){
				$hsl2 = mysql_fetch_object($qry2);
				$novoucher = $hsl2->novoucher;
				$sisa      = $hsl2->selisih;	
			}
			mysql_free_result($qry2);
		}
	}
	
	mysql_free_result($qry);


	//printer_draw_bmp($ftext, "public/images/Logosg.png", 60, 5);  // Logo Dir, lenght H , With V
	// for($i=0;$i<3;$i++){
		//printer_dos(55);
		printer_dos($ftext, $reset . $elite);
		printer_dos($ftext, $dwidth . str_pad($store[0]['NamaPT'], 39, " ", STR_PAD_BOTH) . $ndwidth . "\r\n");
		printer_dos($ftext, str_pad("NPWP : " . $store[0]['NPWP'], 39, " ", STR_PAD_BOTH) . "\r\n");
		// printer_dos($ftext, str_pad($store[0]['NamaAlias'], 39, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, str_pad($store[0]['Alamat1PT'], 39, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, str_pad($store[0]['Alamat2PT'], 39, " ", STR_PAD_BOTH) . "\r\n");

		printer_dos($ftext, "\r\n");
		printer_dos($ftext, str_pad($tgl_tampil . " - " . $val['Waktu'], 39, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, "Struk : " . $val['NoKassa'] . "-" . $val['NoStruk'] . " / " . $val['Kasir'] . " - " . $val['KdAgent'] . "\r\n");
		if($val['userdisc']!='' && $val['KdMember']==''){
			printer_dos($ftext, "Otorisasi : " . $val['userdisc'] ."\r\n");	
		}
		if($val['KdMember']!=''){
			printer_dos($ftext, "Member : " . $val['NamaMember'] ."\r\n");	
		}
		if($val['NamaGroupDisc']!=''){
			printer_dos($ftext, "Disc Khusus : " . $val['NamaGroupDisc'].'-'.$val['NoCard'] ."\r\n");	
		}

		printer_dos($ftext, "========================================\r\n");
		$bt = 0;
		$gt = 0;

		$sql_detail = "SELECT a.NoStruk,a.PCode,b.NamaStruk,a.Qty,a.Harga,a.Netto,Disc1,Ketentuan1 
			FROM transaksi_detail a, masterbarang b WHERE 1 AND a.NoStruk = '".$val['NoStruk']."' AND a.Qty<>0 
			AND a.PCode=b.Pcode ORDER BY Waktu ASC"; 
		$excuteDetail = mysql_query($sql_detail);

		while($row_detail = mysql_fetch_array($excuteDetail)){	
			if($row_detail['Qty']*1>0)
			{
				$gross = $row_detail['Qty'] * round($row_detail['Harga']);
				printer_dos($ftext, str_pad(substr($row_detail['NamaStruk'], 0, 20), 20) .
					str_pad($row_detail['Qty'], 5, " ", STR_PAD_LEFT) .
					str_pad(round($row_detail['Harga']), 7, " ", STR_PAD_LEFT) .
					str_pad(round($gross), 8, " ", STR_PAD_LEFT) . "\r\n");
					//str_pad(round($row_detail[$a]['Netto']), 8, " ", STR_PAD_LEFT) . "\r\n");
				if ($row_detail['Disc1']>0) {
					printer_dos($ftext, str_pad(substr("Discount", 0, 20), 10) .
						str_pad('', 17, " ", STR_PAD_LEFT) .
						str_pad("", 5, " ", STR_PAD_LEFT) .
						str_pad("(" . $row_detail['Disc1'], 8, " ", STR_PAD_LEFT) . ") \r\n");
				}
				if ($row_detail['Disc1']<0) {
					printer_dos($ftext, str_pad(substr("Tambahan", 0, 20), 10) .
						str_pad('', 17, " ", STR_PAD_LEFT) .
						str_pad("", 5, " ", STR_PAD_LEFT) .
						str_pad($row_detail['Disc1']*-1, 8, " ", STR_PAD_LEFT) . " \r\n");
				}
				$bt += $row_detail['Netto'];
				$gt += $gross;
			}
		}
		printer_dos($ftext, "----------------------------------------\r\n");
		printer_dos($ftext, str_pad("Sub Total " . $val['TotalItem'] . " item", 24) . ":" . str_pad(number_format($gt, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		/*
		if ($val['Ttl_Charge'] <> 0){
		    printer_dos($ftext, str_pad("Service charge ", 24) . ":" . str_pad(number_format($val['Ttl_Charge'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
			printer_dos($ftext, str_pad("Total Sales ", 24) . ":" . str_pad(number_format($val['TotalNilai'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		}
	    */
		if ($val['Discount'] > 0){
			printer_dos($ftext, str_pad("Discount ", 24) . ":" . str_pad(number_format($val['Discount'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
			printer_dos($ftext, str_pad("Total ", 24) . ":" . str_pad(number_format($bt, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		}
		if ($val['Discount'] < 0){
			printer_dos($ftext, str_pad("Tambahan ", 24) . ":" . str_pad(number_format($val['Discount']*-1, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
			printer_dos($ftext, str_pad("Total ", 24) . ":" . str_pad(number_format($bt, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		}
		//printer_dos($ftext, str_pad(" ", 25). str_pad("===============", 15, " ", STR_PAD_LEFT) . "\r\n");
		printer_dos($ftext, "----------------------------------------\r\n");
		if ($val['Valas'] <> 0)
		    printer_dos($ftext, str_pad("Valas ".$val['Valuta'].' '.$val['Valas'], 24) . ":" . str_pad(number_format($val['Valas']*$val['Kurs'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		if ($val['Point'] <> 0)
		    printer_dos($ftext, str_pad("Point ", 24) . ":" . str_pad(number_format($val['Point'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		if ($val['Tunai'] <> 0)
		    printer_dos($ftext, str_pad("Tunai ", 24) . ":" . str_pad(number_format($val['Tunai'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		if ($val['KKredit'] <> 0)
		    printer_dos($ftext, str_pad("Kartu Kredit ", 24) . ":" . str_pad(number_format($val['KKredit'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		if ($val['KDebit'] <> 0)
		    printer_dos($ftext, str_pad("Kartu Debit ", 24) . ":" . str_pad(number_format($val['KDebit'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		if ($val['Voucher']+$val['VoucherTravel'] <> 0)
		    printer_dos($ftext, str_pad("Voucher ", 24) . ":" . str_pad(number_format($val['Voucher']+$val['VoucherTravel'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		printer_dos($ftext, "----------------------------------------\r\n");
		printer_dos($ftext, str_pad("Total Bayar ", 24) . ":" . str_pad(number_format($val['TotalBayar'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		printer_dos($ftext, str_pad("Kembali ", 24) . ":" . str_pad(number_format($val['Kembali'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . $ndwidth . "\r\n");
		printer_dos($ftext, "========================================\r\n");
		printer_dos($ftext, "             ===Terima kasih===          \r\n");
		printer_dos($ftext, "\r\n");
		printer_dos($ftext, str_pad("Barang yang sudah dibeli tidak", 35, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, str_pad("dapat dikembalikan.", 35, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, str_pad("Untuk barang kena pajak harga sudah ", 35, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, str_pad("termasuk pajak.", 35, " ", STR_PAD_BOTH) . "\r\n\r\n");
		
		printer_dos($ftext, str_pad("DPP ", 24) . ":" . str_pad(number_format($val['DPP'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		printer_dos($ftext, str_pad("Tax ", 24) . ":" . str_pad(number_format($val['TAX'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		printer_dos($ftext, "----------------------------------------\r\n");
		if ($val['KdMember'] <> ''){
			printer_dos($ftext, str_pad("Point ", 24) . ":" . str_pad(floor($bt/$val['NilaiPoint']), 15, " ", STR_PAD_LEFT) . "\r\n");
			printer_dos($ftext, str_pad("Akumulasi Point ", 24) . ":" . str_pad(number_format($val['JumlahPoint'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
		}
		printer_dos($ftext, "\r\n");
		printer_dos($ftext, str_pad($store[0]['NamaAlias'], 39, " ", STR_PAD_BOTH) . "\r\n");
		printer_dos($ftext, str_pad("www.secretgarden.co.id", 39, " ", STR_PAD_BOTH) . "\r\n");
		// printer_dos($ftext, $dwidth . str_pad($store[0]['NamaPT'], 39, " ", STR_PAD_BOTH) . $ndwidth . "\r\n");
		// printer_dos($ftext, str_pad("NPWP : " . $store[0]['NPWP'], 39, " ", STR_PAD_BOTH) . "\r\n");

		if($num2>0){
			$sisavoucher = "Sisa Voucher ".$novoucher." = ".number_format($sisa, 0, ',', '.');
			printer_dos($ftext, "----------------------------------------\r\n");
			printer_dos($ftext, str_pad($sisavoucher, 35, " ", STR_PAD_BOTH) . "\r\n");
		}
		if($num123>0 AND $bt>=$minimum){
			printer_dos($ftext, "----------------------------------------\r\n");
			printer_dos($ftext, str_pad($ket1, 35, " ", STR_PAD_BOTH) . "\r\n");
			printer_dos($ftext, str_pad($ket2, 35, " ", STR_PAD_BOTH) . "\r\n");
			printer_dos($ftext, str_pad($ket3, 35, " ", STR_PAD_BOTH) . "\r\n");
		//	printer_dos($ftext, str_pad("Free Black Eye Coffee Lampung 100gr", 35, " ", STR_PAD_BOTH) . "\r\n");
		//	printer_dos($ftext, str_pad("*Minimum Purchase IDR 500.000", 35, " ", STR_PAD_BOTH) . "\r\n");
		}
		printer_dos($ftext, "\r\n");
		printer_dos($ftext, "\r\n");
		
	// }

		printer_dos($ftext, $fcut);
}

		//printer_dos($ftext, str_pad("Total All", 39, " ", STR_PAD_BOTH) . "\r\n");
printer_dos($ftext, $op_cash);
// printer_close($ftext);
?>