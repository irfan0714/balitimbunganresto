<?php
$this->load->view('header');
$reportlib = new report_lib();

?>

<script>

    function submitThis()
    {
		nostiker   = $("#v_nostiker").val();

		if(nostiker != ""){
			$.ajax({
				url : "<?php echo base_url(); ?>index.php/report/cetakstruk_all/cek_struk",
				type : "POST",
				data : "nostiker="+nostiker,
				beforeSend : function(){
					$("#btn_search").html("<i class='entypo-search'></i> Loading ...");
				},
				success : function(res){

					if(res == "invalid"){
						$("#error_sticker").show();
					}else{
						$('#search').submit();
					}

					$("#btn_search").html("<i class='entypo-search'></i> Cetak");
				},
				error : function(){
					alert("error request");
				}
			});

		}else{

			$('#search').submit();
		}
			
    }

</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Cetak Ulang Struk All </strong></li>
		</ol>
		
		<form action="<?php echo base_url(); ?>index.php/report/cetakstruk_all/cetak/" method="POST" name="search" id="search">
		
	    <table class="table table-bordered responsive"> 
			<tr>
					<td class="title_table" width="150">Tanggal</td>
					<td>
						<input type="text" class="form-control-new datepicker" value="<?php echo date("d-m-Y") ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10"> S/D
						<input type="text" class="form-control-new datepicker" value="<?php echo date("d-m-Y") ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
					</td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">No Sticker</td>
	        	<td>
	        		<input type="text" class="form-control-new" value="" name="v_nostiker" id="v_nostiker" size="10">	
	        	</td>
	        </tr> 
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Cetak<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	    <p style="color:red;">*update system 27/12/2021, cetak all struk dengan range tanggal dan sticker</p>
	</div>
</div>

<?php
$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>