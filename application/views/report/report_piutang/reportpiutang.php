<?php
$mylib = new globallib();

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_piutang/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportpiutang.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">Pelanggan</td>
                <td style="vertical-align: middle; text-align: center;">No Faktur</td>
                <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;">Jt Tempo</td>
                <td style="vertical-align: middle; text-align: center;">Type</td>
                <td style="vertical-align: middle; text-align: center;">Nilai Faktur</td>
                <td style="vertical-align: middle; text-align: center;">Sisa</td></td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$prev_matauang = '';			
			$prev_customer = '';
			$subtotalcustomer = 0;
			$subtotalmatauang = 0;
			$grandtotal = 0;
			for($i=0;$i<count($hasil);$i++)
			{		
				$customer = $hasil[$i]['KdCustomer'];
				$nama = $hasil[$i]['Nama'];
				$nodokumen = $hasil[$i]['NoDokumen'];
				$tanggal = $hasil[$i]['Tanggal'];
				$jatuhtempo = $hasil[$i]['JatuhTempo'];
				$nilaitransaksi = $hasil[$i]['NilaiTransaksi'];
				$tipetransaksi = $hasil[$i]['tipetransaksi'];
				$sisa = $hasil[$i]['SisaHutang'];
				if($tipetransaksi == 'C' || $tipetransaksi == 'CM' || $tipetransaksi == 'CS'){
					$sisa = $sisa * -1;
					$nilaitransaksi = $nilaitransaksi * -1;
				}
				
				$subtotalcustomer += $sisa;
				$subtotalmatauang += $sisa;
				$grandtotal += $sisa;
				if($prev_customer == $customer){
					$nama_echo = '';
				}else{
					$nama_echo = $nama;
				}
				?>
				<tr>
					<td nowrap><?= $nama_echo; ?></td>
					<td nowrap><?= $nodokumen; ?></td>
					<td nowrap><?= $tanggal; ?></td>
					<td nowrap><?= $jatuhtempo; ?></td>
					<td nowrap><?= $tipetransaksi; ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($nilaitransaksi); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($sisa); ?></td>
				</tr>
			<?php
				if($i<count($hasil)-1){
					if($customer != $hasil[$i+1]['KdCustomer']){
					?>
					<tr>
						<td colspan="6" style="text-align: center;font-weight: bold;">Sub Total <?=$nama;?></td>
						<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalcustomer);?></td>
					</tr>
					<?php	
						$subtotalcustomer = 0;
					}
				}
				$prev_customer = $customer;
			}
			?>
			</tbody>
			<tr>
				<td colspan="6" style="text-align: center;font-weight: bold;">Sub Total <?=$nama;?></td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalcustomer);?></td>
			</tr>
			<tr>
				<td colspan="6" style="text-align: center;font-weight: bold;">Total</td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($grandtotal);?></td>
			</tr>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
