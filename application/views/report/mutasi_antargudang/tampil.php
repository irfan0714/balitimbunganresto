<?php
$mylib = new globallib();

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_mutasi_antargudang/search_report/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="mutasiantargudang.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">No Dokumen</td>
                <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;">Tujuan</td>
                <td style="vertical-align: middle; text-align: center;">Gudang Asal</td>
                <td style="vertical-align: middle; text-align: center;">Gudang Tujuan</td>
                <td style="vertical-align: middle; text-align: center;">Keterangan</td>
                <td style="vertical-align: middle; text-align: center;">Konfirmasi</td>
                <td style="vertical-align: middle; text-align: center;">PCode</td>
                <td style="vertical-align: middle; text-align: center;">Nama Barang</td>
                <td style="vertical-align: middle; text-align: center;">Qty</td>
                <td style="vertical-align: middle; text-align: center;">Satuan</td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$prevdokumen = '';
			for($i=0;$i<count($hasil);$i++)
			{		
				$nodokumen = $hasil[$i]['NoDokumen'];
				$tgldokumen = $hasil[$i]['TglDokumen'];
				$purpose = $hasil[$i]['purpose'];
				$gudangasal = $hasil[$i]['GudangAsal'];
				$gudangtujuan = $hasil[$i]['GudangTujuan'];
				$keterangan = $hasil[$i]['Keterangan'];
				$konfirmasi = $hasil[$i]['MovingConfirmation'];
				$pcode = $hasil[$i]['PCode'];
				$namalengkap = $hasil[$i]['NamaLengkap'];
				$qty = $hasil[$i]['Qty'];
				$satuan = $hasil[$i]['Satuan'];
				$konfirmasi = $konfirmasi==1 ? 'Ya' : 'Tidak';
                
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
				<?php
					if($prevdokumen==$nodokumen){
						$nodokumen_echo = '';
						$tgldokumen_echo = '';
						$purpose_echo = '';
						$gudangasal_echo = '';
						$gudangtujuan_echo = '';
						$keterangan_echo = '';
						$konfirmasi_echo = '';
					}else{
						$nodokumen_echo = $nodokumen;
						$tgldokumen_echo = $mylib->ubah_tanggal($tgldokumen);
						$purpose_echo = $purpose;
						$gudangasal_echo = $gudangasal;
						$gudangtujuan_echo = $gudangtujuan;
						$keterangan_echo = $keterangan;
						$konfirmasi_echo = $konfirmasi;
					}
				?>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $nodokumen_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $tgldokumen_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $purpose_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $gudangasal_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $gudangtujuan_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $keterangan_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $konfirmasi_echo; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $pcode; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $namalengkap; ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>" style="text-align: right;"><?php echo ubah_format($qty); ?></td>
					<td nowrap title="<?php echo $nodokumen." :: ".$tgldokumen; ?>"><?php echo $satuan; ?></td>
				</tr>
			<?php
				$prevdokumen = $nodokumen;
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
