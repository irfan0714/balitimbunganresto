<?php 

if(!$btn_excel)
{
	$this->load->view('header'); 

	$modul = "Fluktuatif Harga";
	
}

if($btn_excel)
{
	$file_name = "Report_pr_non_po.xls";
	        
	header("Content-Disposition".": "."attachment;filename=$file_name");
	header("Content-type: application/vnd.ms-excel");
}

if(!$btn_excel)
{
	

?>

<script>
function cekTheform()
{
    document.getElementById("theform").submit();	
	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action="<?php echo base_url();?>index.php/report/report_fluktuatif_harga/search_report">
		
	    <table class="table table-bordered responsive">     
	        <tr>
	            <td class="title_table" width="150">Date</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	s/d
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            	
	            </td>
	        </tr>                 
	        <tr>
	            <td class="title_table">Supplier</td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="all">All</option>
	            		<?php
	            		foreach($msupplier as $val)
	            		{
		            		$selected="";
		            		if($v_supplier)
		            		{
								if($v_supplier==$val["KdSupplier"])
								{
									$selected = 'selected="selected"';
								}
							}
							
							?><option value="<?php echo $val["KdSupplier"]; ?>" <?php echo $selected; ?> ><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>    
	        <tr>
	        	<td class="title_table">Nama Barang</td>
	        	<td>
	        		<select name="v_pcode" class="form-control-new" style="width: 200px;" id="barang_supplier">
	        			<?php 
	        				if($analisa == TRUE){
	        					foreach ($barang_supplier as $key => $value) {
	        						$select = ($v_pcode == $value['KdBarang'])?"selected":"";
	        						echo "<option value='".$value['KdBarang']."' ".$select.">".$value['KdBarang']." ~ ".$value['NamaLengkap']."</option>";
	        					}
	        				} 
	        			?>
	        		</select>
	        	</td>
	        </tr>      	
	        <tr>
	        	 <td class="title_table">&nbsp;</td>
	            <td colspan="100%">
					<input type='hidden' name="flag" id="flag" value="analisa">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_analisa" id="btn_analisa"  value="Analisa">Analisa<i class="entypo-check"></i></button>
		        </td>
	        </tr>  
	    </table>
        
    	<?php
    	
}
    	if($flag || $btn_excel)
    	{
    		$mylib = new globallib();
    	
    	?>
    	
    	
    	<?php
    	if(!$btn_excel)
    	{
		?>
		<button type="submit" class="btn btn-green btn-icon btn-sm icon-left" name="btn_excel" id="btn_excel" value="Excel">Export To Excel<i class="entypo-download"></i></button>
    	<br><br>
		<?php	
		}
    	?>
    	          <?php if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="13">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="13">REPORT FLUKTUATIF HARGA</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="13">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    } ?>
    	
    	
    	<?php
    	if($btn_excel)
    	{
			$table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
		}
		else
		{
			$table = '<table class="table table-bordered responsive">';	
		}
		
			echo $table;
			
    	?>
	    		<thead class="title_table">
	    			<tr>
	    				<th><center>No.</center></th>
	    				<th><center>Tanggal</center></th>
	    				<!-- <th><center>No. PO</center></th> -->
	    				<th><center>Supplier</center></th>
	    				<th><center>PCode</center></th>
	    				<th><center>Nama Barang</center></th>
	    				<th><center>Qty</center></th>
	    				<th><center>Satuan</center></th>
	    				<th><center>Harga</center></th>
	    				<th><center>Fluktuatif</center></th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    			<?php
	    			if(count($header)==0)
	    			{
						echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
					}
					$NoDokumen_prev = "";
					$no = 1;
					$a = "";

					foreach($header as  $val)
					{	
				
						$selisih = 0;
						if($val['PCode'] == $a){
							$selisih = $nil_prev - $val['Harga'];

							if($selisih > 0){
								$persen = ($selisih / $nil_prev) * 100 ;
								$fluktuaitf = "<i class='entypo-down'></i>";
								$bg = "style='background-color:lightgreen'";
							}else if ($selisih == 0){
								$persen_ = ($selisih / $nil_prev) * 100 ;
								$persen = abs($persen_);
								$fluktuaitf = "<i class='entypo-right'></i>";
								$bg = "style='background-color:yellow'";
							}else{
								$persen_ = ($selisih / $val['Harga']) * 100 ;
								$persen = abs($persen_);
								$fluktuaitf = "<i class='entypo-up'></i>";
								$bg = "style='background-color:red;color:white;'";
							}
						}
					?>
						<tr>
							<td align="center"><?php echo $no; ?></td>
							<td align="center"><?php echo $val['Tanggal']; ?></td>
							<!-- <td align="center"><?php echo $val['NoPO']; ?></td> -->
							<td align="center"><?php echo $val['Nama']; ?></td>
							<td align="center"><?php echo $val['PCode']; ?></td>
							<td align="center"><?php echo $val['NamaLengkap']; ?></td>
							<td align="center"><?php echo $val['Qty']; ?></td>
							<td align="center"><?php echo $val['Satuan']; ?></td>
							<td align="right"><?php echo number_format($val['Harga'],0,"","."); ?></td>
							<?php if($no ==1){ ?>
							<td align="center"></td>
							<?php } ?>
							<?php if($no !=1){ ?>
							<td align="center">
								<table  width="100%" <?php echo $bg; ?>>
									<tr>
										<td><?php echo $fluktuaitf; ?></td>
										<td align="right"><?php echo round($persen,2)."%"; ?></td>
									</tr>
								</table>
							</td>
							<?php } ?>
						</tr>
					<?php
						$nil_prev = $val['Harga'];
						$a = $val['PCode'];
						$no++;
					 } ?>
	    		</tbody>
    			</table>
    	<?php	
			
						
		}
		
		
		if(!$btn_excel)
		{
    	?>
    	
	    </form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

$(document).ready(function(){

		$("#v_supplier").change(function(){
			base_url = $("#base_url").val();
			$.ajax({
				url : base_url+"index.php/report/report_fluktuatif_harga/getBarangSupplier",
				type : "POST",
				data : "KdSupplier="+ $(this).val(),
				success : function(res){
					var data = JSON.parse(res);

					var option = "<option value=''>Pilih Nama Barang</option>";
					$.each(data,function(key,val){
						console.log(val);
						option += "<option value='"+key+"'>"+val+"</option>";
					});

					$('#barang_supplier').html(option);
					
				},
				error : function(e){
					alert(e);
				}

			})
		});
});
</script>

<?php
}
?>