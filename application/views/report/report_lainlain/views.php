<?php 

if(!$btn_excel)
{
	$this->load->view('header'); 

	$modul = "Report Lain-Lain";
}

if($btn_excel)
{
	$file_name = "Report_lain_lain.xls";
	        
	header("Content-Disposition".": "."attachment;filename=$file_name");
	header("Content-type: application/vnd.ms-excel");
}

if(!$btn_excel)
{
	

?>

<script>
function cekTheform()
{
	if(document.getElementById("v_start_date").value=="")
    {
        alert("Tanggal Awal harus diisi");
        document.getElementById("v_start_date").focus();
        return false;
    }
	else if(document.getElementById("v_end_date").value=="")
    {
        alert("Tanggal Akhir harus diisi");
        document.getElementById("v_end_date").focus();
        return false;
    }
	if(document.getElementById("v_type").value=="")
    {
        alert("Type harus dipilih");
        return false;
    }
    
    document.getElementById("theform").submit();	
	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/report/report_lain_lain/search_report'>
		
	    <table class="table table-bordered responsive">   
	        
	        <tr>
	            <td class="title_table" width="150">Periode <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	s/d
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            	
	            </td>
	        </tr>                     
	        
	        <tr>
	            <td class="title_table">Type <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;">
	            		<option value="">Pilih Type</option>
	            		<?php
	            		foreach($mtype as $val)
	            		{
	            			$selected = "";
	            			if($flag)
	            			{
	            				if($v_type==$val)
	            				{
									$selected = 'selected="selected"';	
								}
							}
							
							?><option value="<?php echo $val; ?>" <?php echo $selected; ?>><?php echo $val; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="gudang" id="gudang" style="width: 200px;">
	            		<option value="0">Semua Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
	            			$selected = "";
	            			if($flag)
	            			{
	            				if($v_gudang==$val['KdGudang'])
	            				{
									$selected = 'selected="selected"';	
								}
							}
							
							?><option value="<?php echo $val['KdGudang']; ?>" <?php echo $selected; ?>><?php echo $val['Keterangan']; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="status" id="status" style="width: 200px;">
	            		<option value=""  <?= $status=='' ? 'selected="selected"':  '' ;?>>Semua</option>
	            		<option value="0" <?= $status=='0' ? 'selected="selected"': '' ;?>>Pending</option>
	            		<option value="1" <?= $status=='1' ? 'selected="selected"': '' ;?>>Open</option>
	            		<option value="2" <?= $status=='2' ? 'selected="selected"': '' ;?>>Void</option>
	            	</select>  
	            </td>
	        </tr>
	        
	        <tr>
	        	 <td class="title_table">&nbsp;</td>
	            <td colspan="100%">
					<input type='hidden' name="flag" id="flag" value="analisa">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_analisa" id="btn_analisa"  value="Analisa">Analisa<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        
	    </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font><br/><br/><br/>
        
    	<?php
    	
}
    	if($analisa || $btn_excel)
    	{
    		foreach($header as $val)
			{
				$arr_data["list_nodok"][$val["NoDokumen"]] = $val["NoDokumen"];
				$arr_data["tgldokumen"][$val["NoDokumen"]] = $val["TglDokumen"];
				$arr_data["kdgudang"][$val["NoDokumen"]] = $val["KdGudang"];
				$arr_data["nama_gudang"][$val["NoDokumen"]] = $val["nama_gudang"];
				$arr_data["PurposeId"][$val["NoDokumen"]] = $val["PurposeId"];
				$arr_data["purpose"][$val["NoDokumen"]] = $val["purpose"];
				$arr_data["keterangan"][$val["NoDokumen"]] = $val["Keterangan"];
				$arr_data["status"][$val["NoDokumen"]] = $val["Status"];
			}
			
    		foreach($detail as $val)
			{
				$arr_data["list_detail"][$val["NoDokumen"]][$val["Sid"]] = $val["Sid"];
				$arr_data["pcode"][$val["NoDokumen"]][$val["Sid"]] = $val["PCode"];
				$arr_data["namabarang"][$val["NoDokumen"]][$val["Sid"]] = $val["NamaBarang"];
				$arr_data["qty"][$val["NoDokumen"]][$val["Sid"]] = $val["Qty"];
				$arr_data["satuan"][$val["NoDokumen"]][$val["Sid"]] = $val["Satuan"];
				$arr_data["keterangans"][$val["NoDokumen"]][$val["Sid"]] = $val["Keterangan"];
			}
    	?>
    	
    	
    	<?php
    	if(!$btn_excel)
    	{
		?>
		<button type="submit" class="btn btn-green btn-icon btn-sm icon-left" name="btn_excel" id="btn_excel" value="Excel">Export To Excel<i class="entypo-download"></i></button>
    	
    	<div>&nbsp;&nbsp;<br/></div>
		<?php	
		}
    	?>
    	
    	
    	<?php
    	if($btn_excel)
    	{
			$table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
		}
		else
		{
			$table = '<table class="table table-bordered responsive">';	
		}
		
			echo $table;
    	?>
    	
    		<thead class="title_table">
    			<tr>
    				<th rowspan="2">Tanggal</th>
    				<th rowspan="2">No Dokumen</th>
    				<th rowspan="2">Status</th>
    				<th rowspan="2">Type</th>
    				<th rowspan="2">Gudang</th>
    				<th colspan="5" align="center">Detail</th>
    			</tr>
    			<tr>
    				<th>PCode</th>
    				<th>Nama Barang</th>
    				<th>Qty</th>
    				<th>Satuan</th>
    				<th>Keterangan</th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php
    			if(count($arr_data["list_nodok"])==0)
    			{
					echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}
				
				$nodok_prev = "";
				foreach($arr_data["list_nodok"] as $nodok => $val)
				{
					 $tgldokumen = $arr_data["tgldokumen"][$nodok];
					 $purpose = $arr_data["purpose"][$nodok];
					 $nama_gudang = $arr_data["nama_gudang"][$nodok];
					 $status = $arr_data["status"][$nodok];
					 
					 if($status==0)
					    $status = 'pending';
					elseif($status==1)
					   $status = 'Open';
					else 
					    $status = 'void';
					 
					 foreach($arr_data["list_detail"][$nodok] as $sid => $val2)
					 {
						$pcode = $arr_data["pcode"][$nodok][$sid];
						$namabarang = $arr_data["namabarang"][$nodok][$sid];
						$qty = $arr_data["qty"][$nodok][$sid];
						$satuan = $arr_data["satuan"][$nodok][$sid];
						$keterangan = $arr_data["keterangans"][$nodok][$sid];
						
						
						$echo_nodok = "&nbsp;";
						$echo_tgldokumen = "&nbsp;";
						$echo_purpose = "&nbsp;";
						$echo_nama_gudang = "&nbsp;";
						$echo_status = "&nbsp;";
						
						if($nodok_prev!=$nodok)
						{
							$echo_nodok	= $nodok;
							$echo_tgldokumen = $tgldokumen;
							$echo_purpose = $purpose;
							$echo_nama_gudang = $nama_gudang;
							$echo_status = $status;
						}
						
				?>
					<tr>
						<td align="center"><?php echo $echo_tgldokumen; ?></td>
						<td align="center"><?php echo $echo_nodok; ?></td>
						<td align="center"><?php echo $echo_status; ?></td>
						<td><?php echo $echo_purpose; ?></td>
						<td><?php echo $echo_nama_gudang; ?></td>
						
						<td align="center"><?php echo $pcode; ?></td>
						<td><?php echo $namabarang; ?></td>
						<td align="right"><?php echo number_format($qty,0,',','.'); ?></td>
						<td><?php echo $satuan; ?></td>
						<td><?php echo $keterangan; ?></td>
					</tr>
				<?php
						$nodok_prev = $nodok;
					 }
					
					?>
					<tr><td colspan="100%">&nbsp;</td></tr>
					<?php
				}
    			?>
    		</tbody>
    	</table>
    	<?php				
		}
		
		
		if(!$btn_excel)
		{
    	?>
    	
	    </form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<?php
}
?>