<p>
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportkomisi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <div>
            <input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()">

        </div>
        <?php
    }
    ?>
    <br>

    <br>
    <table align="left" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td nowrap colspan="8"><strong><font face="Arial" size="2">Laporan Pembayaran Komisi Per Detail</font></strong></td>
        </tr>
        <?php
        for ($a = 0; $a < count($judul); $a++) {
            ?>
            <tr>
                <td nowrap colspan="8"><strong><font face="Arial" size="2"><?= $judul[$a] ?></font></strong></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br>
    <br><br>
    <table border="1" cellpadding="1" cellspacing="0" class="table table-bordered table-responsive table-hover">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>No Transaksi</th>
                <th>PCode</th>
                <th>Nama Barang</th>
                <th>Qty</th>
                <th>Harga</th>
                <th>Komisi (%)</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <?php
        if (count($hasil) > 0) {
            $total1 = 0;
            $temp = "";
            for ($s = 0; $s < count($hasil); $s++) {
                $total1 += (float) $hasil[$s]['Nilai'];
                $tgl = $mylib->ubah_tanggal($hasil[$s]['TglTransaksi']);
                $NoTrans = $hasil[$s]['NoTransaksi'];

                if ($temp == $tgl) {
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <?php
                        if ($temp_no == $NoTrans) {
                            ?>
                            <td nowrap >&nbsp;</td>
                            <?php
                        } else {
                            ?>
                            <td nowrap><?= $NoTrans ?></td>
                            <?php
                            $temp_no = $NoTrans;
                        }
                        ?>
                        <td><?= $hasil[$s]['PCode'] ?></td>
                        <td><?= $hasil[$s]['NamaLengkap'] ?></td>
                        <td style="text-align: right;"><?= $hasil[$s]['Qty'] ?></td>
                        <td style="text-align: right;"><?= $mylib->ubah_format($hasil[$s]['Harga']) ?></td>
                        <td style="text-align: right;"><?= $hasil[$s]['Komisi'] ?></td>
                        <td style="text-align: right;"><?= $mylib->ubah_format($hasil[$s]['Nilai']) ?></td>

                    </tr>
                    <?php
                } else {
                    ?>
                    <tr>
                        <td><?= $tgl ?>&nbsp;</td>
                        <td><?= $NoTrans ?>&nbsp;</td>
                        <td><?= $hasil[$s]['PCode'] ?></td>
                        <td><?= $hasil[$s]['NamaLengkap'] ?></td>
                        <td style="text-align: right;"><?= $hasil[$s]['Qty'] ?></td>
                        <td style="text-align: right;"><?= $mylib->ubah_format($hasil[$s]['Harga']) ?></td>
                        <td style="text-align: right;"><?= $hasil[$s]['Komisi'] ?></td>
                        <td style="text-align: right;"><?= $mylib->ubah_format($hasil[$s]['Nilai']) ?></td>

                    </tr>
                    <?php
                    $temp = $tgl;
                    $temp_no = $NoTrans;
                }
            }
            ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="7"><b>Total</b></td>
                <td nowrap align='right' bgcolor='#f7d7bb'><b><?= $mylib->ubah_format($total1) ?></b></td>
            </tr>
            <?php
        } else {
            ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="8"><b>Tidak ada data</b></td>
            </tr>
            <?php
        }
        ?>
    </table>
</form>