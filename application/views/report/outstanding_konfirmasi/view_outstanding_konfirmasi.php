<?php $this->load->view('header_part1')?>

	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	<script>
	$(document).ready(function(){
		$('#tgldari').Zebra_DatePicker({format: 'Y-m-d'});
        $('#tglsampai').Zebra_DatePicker({format: 'Y-m-d'});
	});
	</script>
	<script> 
    function submitThis()
    { 
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>
</head>
<?php $this->load->view('header_part3')?>

<form method="POST"  name="search" action='<?=base_url(); ?>index.php/report/report_outstanding_konfirmasi/tampil'>

<table>
	<tr>
		<td>
			Range Tanggal&nbsp;&nbsp;Dari
		</td>
		<td>
			&nbsp;&nbsp;<input type='text' name='tgldari'  value="<?php echo $tgldari?>" size='12' id="tgldari"  class="form-control-new" readonly />
			&nbsp;Sampai&nbsp;
			&nbsp;<input type='text' name='tglsampai' value="<?php echo $tglsampai?>" size='12' id="tglsampai" class="form-control-new" readonly />
			
		&nbsp;<input type="submit" name='submit' value="TAMPIL" class='btn btn-default'>
		&nbsp;<input type="submit" name='submit' value="XLS"  id="excel" class='btn btn-default'>
		</td>
	</tr>
</table>
</form> 

<?php

if ($tampilkanDT) 
{
    $this->load->view("report/outstanding_konfirmasi/tampil_outstanding_konfirmasi");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>