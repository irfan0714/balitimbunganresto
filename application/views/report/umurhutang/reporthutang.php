<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/umurhutang/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportumurhutang.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');$('#search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Umur Hutang</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
		<th align="center" bgcolor="#f3f7bb">No Transaksi</th>
		<th align="center" bgcolor="#f3f7bb">Tanggal</th>
		<th align="center" bgcolor="#f3f7bb">Jenis</th>
		<th align="center" bgcolor="#f3f7bb">Supplier</th>
		<th align="center" bgcolor="#f3f7bb">Nama</th>
		<th align="center" bgcolor="#f3f7bb">Tgl JTO</th>
		<th align="center" bgcolor="#f3f7bb">Hutang</th>
		<th align="center" bgcolor="#f3f7bb">Bayar</th>
		<th align="center" bgcolor="#f3f7bb">Sisa</th>
		<th align="center" bgcolor="#f3f7bb">Saat Ini</th>
		<th align="center" bgcolor="#f3f7bb">01-30</th>
		<th align="center" bgcolor="#f3f7bb">31-60</th>
		<th align="center" bgcolor="#f3f7bb">61-90</th>
		<th align="center" bgcolor="#f3f7bb">>=91</th>
		<th align="center" bgcolor="#f3f7bb">Umur</th>
	</tr>
<?php
	if(count($hasil)>0){
		$total1 = 0;
		$total2 = 0;
		$total3 = 0;
		$total4 = 0;
		$total5 = 0;
		$total6 = 0;
		$total7 = 0;
		$total8 = 0;
		for($s=0;$s<count($hasil);$s++)
		{
		    $umur1 = 0;
			$umur2 = 0;
			$umur3 = 0;
			$umur4 = 0;
			$umur5 = 0;
		    $total1 += (float)$hasil[$s]['TotalHutang'];
			$total2 += (float)$hasil[$s]['TotalBayar'];
			$total3 += (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			if($hasil[$s]['umur']<=0)
			{
			   $total4 += (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			   $umur1 = (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			}
			if($hasil[$s]['umur']>=1&&$hasil[$s]['umur']<=30)
            {			
			   $total5 += (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			   $umur2 = (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			}
			if($hasil[$s]['umur']>=31&&$hasil[$s]['umur']<=60)
	        {		
    			$total6 += (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
				$umur3 = (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			}
			if($hasil[$s]['umur']>=61&&$hasil[$s]['umur']<=90)
			{
    			$total7 += (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
				$umur4 = (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			}
			if($hasil[$s]['umur']>=91)
			{
			   $total8 += (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			   $umur5 = (float)$hasil[$s]['TotalHutang']-(float)$hasil[$s]['TotalBayar'];
			}
	?>
			<tr>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NoTransaksi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_tanggal($hasil[$s]['TglTransaksi'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Jenis']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KdSupplier']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Nama']?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_tanggal($hasil[$s]['TglJto'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['TotalHutang'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['TotalBayar'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['TotalHutang']-$hasil[$s]['TotalBayar'])?></td>
				
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($umur1)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($umur2)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($umur3)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($umur4)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($umur5)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$hasil[$s]['umur']?></td>
			</tr>
	<?php
		}
	?>
		<tr>
			<td nowrap align='center' bgcolor='#f7d7bb' colspan="6"><b>Total</b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total1)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total2)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total3)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total4)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total5)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total6)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total7)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total8)?></b></td>
			<td nowrap align='center' bgcolor='#f7d7bb'><b></b></td>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="15"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>