<?php
	$this->load->view('header');
	$reportlib	= new report_lib();
	$mylib		= new globallib();
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
    
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
//	$("#search").submit();
        document.getElementById("search").submit();
    }

</script>
<body>
	<div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
					<?=$judul_r;?>
                </div>
            </div>			
		</div>
		<div class="row">
	<div class="col-md-12">
    <p>
    <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/<?=$ul1?>/<?=$ul2?>/search_report/" onsubmit="return false">
        <div class="col-lg-5">
            <?php
//						print_r($divisi);
                        $mylib->option_boostrap ("Divisi","div",$divisi,"","KdDivisi","NamaDivisi","","");
			?>
             <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
						<input type="hidden" id="baseurl" name="baseurl" value="<?=base_url();?>" >
						
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/<?=$ul1?>/<?=$ul2?>/"><i class="entypo-back"></i>Reset</a>&nbsp;
                            <button class="btn btn-primary" type="submit" onclick="return submitThis();"><i class="entypo-drive"></i>Search(*)</button>
							
                        <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                        <input type='hidden' value='<?= $print ?>' id="print" name="print">

                        </div>
                    </div>        
                   
        </div>
    </form>
    <p />&nbsp;
	    <p />&nbsp;
		    <p />&nbsp;
			    <p />&nbsp;
    <br>
    <br>
    <br>
    <br>
    <br>
</body>
<?php
if ($tampilkanDT) {
    $this->load->view("report/l_barang/tampil");
}

$this->load->view('footer');
?>