<p>
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_komisi/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportkomisi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <div>
            <input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()">

        </div>
        <?php
    }
    ?>
    <br>

    <br>
    <table align="left" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td nowrap colspan="8"><strong><font face="Arial" size="2">Laporan Pembayaran Komisi Per Tour Leader Detail</font></strong></td>
        </tr>
        <?php
        for ($a = 0; $a < count($judul); $a++) {
            ?>
            <tr>
                <td nowrap colspan="8"><strong><font face="Arial" size="2"><?= $judul[$a] ?></font></strong></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br>
    <br><br>
    <table border="1" cellpadding="1" cellspacing="0" class="table table-bordered table-responsive table-hover">
        <thead>
            <tr>
                <th>Kode Tour</th>
                <th>Nama</th>
                <th>Telp</th>
                <th>Total Sales</th>
                <th>Total Komisi</th>
                <th>No Stiker</th>
                <th>No Register</th>
            </tr>
        </thead>
        <?php
        if (count($hasil) > 0) {
            $total1 = 0;
            $temp = "";
            for ($s = 0; $s < count($hasil); $s++) {
                $total1 += (float) $hasil[$s]['TotalKomisi'];
//                $tgl = $mylib->ubah_tanggal($hasil[$s]['TglTransaksi']);
//                $NoTrans = $hasil[$s]['NoTransaksi'];

               ?>
                    <tr>
                        <td><?= $hasil[$s]['KdAgent'] ?>&nbsp;</td>
                        <td><?= $hasil[$s]['Nama'] ?>&nbsp;</td>
                        <td><?= $hasil[$s]['Phone'] ?>&nbsp;</td>
                        <td style="text-align: right;"><?= $mylib->ubah_format($hasil[$s]['TotalSales']) ?></td>
                        <td style="text-align: right;"><?= $mylib->ubah_format($hasil[$s]['TotalKomisi']) ?></td>
                        <td><?= $hasil[$s]['NoStiker'] ?>&nbsp;</td>
                        <td><?= $hasil[$s]['KdRegister'] ?>&nbsp;</td>
                    </tr>
                    <?php
              
            }
            ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="3"><b>Total</b></td>
                <td nowrap align='right' bgcolor='#f7d7bb'><b><?= $mylib->ubah_format($total1) ?></b></td>
            </tr>
            <?php
        } else {
            ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="7"><b>Tidak ada data</b></td>
            </tr>
            <?php
        }
        ?>
    </table>
</form>