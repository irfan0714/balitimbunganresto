<p>
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportkomisi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <div>
            <input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()">

        </div>
        <?php
    }
    ?>
    <br>

    <br>
    <table align="left" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td nowrap colspan="8"><strong><font face="Arial" size="2">Laporan Pembayaran Komisi Per Detail</font></strong></td>
        </tr>
        <?php
        for ($a = 0; $a < count($judul); $a++) {
            ?>
            <tr>
                <td nowrap colspan="8"><strong><font face="Arial" size="2"><?= $judul[$a] ?></font></strong></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <br>
    <br><br>
    <table border="1" cellpadding="1" cellspacing="0" class="table table-bordered table-responsive table-hover">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Nama Barang</th>
                <th>HPD Jual</th>
                <th>Service Charge</th>
                <th>PPN</th>
                <th>Disc Karyawan</th>
				<th>Disc Lokal</th>
                <th>Komisi (%)</th>
                <th>Divisi</th>
            </tr>
        </thead>
					
        <?php
        if (count($hasil) > 0) {
            $total1 = 0;
            $temp = "";
            for ($s = 0; $s < count($hasil); $s++) {
				?>
					<tr>
						<td><?= $hasil[$s]['PCode'] ?></td>
                        <td><?= $hasil[$s]['NamaLengkap'] ?></td>
						<td><?= $hasil[$s]['hJ'] ?></td>
                        <td><?= $hasil[$s]['Service_charge'] ?></td>
						<td><?= $hasil[$s]['PPN'] ?></td>
                        <td><?= $hasil[$s]['DiscInternal'] ?></td>
						<td><?= $hasil[$s]['DiscLokal'] ?></td>
                        <td><?= $hasil[$s]['KomisiLokal'] ?></td>
                        <td><?= $hasil[$s]['NamaDivisi'] ?></td>


                    </tr>
                    <?php
                
            }
           
        } else {
            ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="8"><b>Tidak ada data</b></td>
            </tr>
            <?php
        }
        ?>
    </table>
</form>