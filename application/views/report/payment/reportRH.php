<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/payment/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportrekapharianpayment.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel'
                   onclick="$('#excel').val('excel');
                   document.getElementById('search').submit()">

</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Rekap Harian PV</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
		<th align="center" bgcolor="#f3f7bb">Kas / Bank</th>
		<th align="center" bgcolor="#f3f7bb">Nama Kas / Bank</th>
		<th align="center" bgcolor="#f3f7bb">Rekening</th>
		<th align="center" bgcolor="#f3f7bb">Nama Rekening</th>
		<th align="center" bgcolor="#f3f7bb">Jumlah</th>
	</tr>
<?php
	if(count($hasil)>0){
		$total0 = 0;
		for($s=0;$s<count($hasil);$s++)
		{
		    $total0 += (float)$hasil[$s]['Jumlah'];
	?>
			<tr>
				
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KdKasBank']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaKasBank']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KdRekening']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaRekening']?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['Jumlah'])?></td>
			</tr>
	<?php
		}
	?>
		<tr>
		    
			<td nowrap align='center' bgcolor='#f7d7bb' colspan="4"><b>Total</b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total0)?></b></td>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="5"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>