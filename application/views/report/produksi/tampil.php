<?php
$mylib = new globallib();

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_produksi/search_report/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportproduksi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td width="30" style="vertical-align: middle; text-align: center;" rowspan="2">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;" rowspan="2">Formula</td>
                <td style="vertical-align: middle; text-align: center;" rowspan="2">No Batch</td>
                <td style="vertical-align: middle; text-align: center;" rowspan="2">Rosting Qty</td>
                <td style="vertical-align: middle; text-align: center;" colspan="7">Hasil</td>
            </tr>
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">PCode</td>
                <td style="vertical-align: middle; text-align: center;">Nama Barang</td>
                <td style="vertical-align: middle; text-align: center;">Qty</td>
                <td style="vertical-align: middle; text-align: center;">QC</td>
                <td style="vertical-align: middle; text-align: center;">Reject</td>
                <td style="vertical-align: middle; text-align: center;">Berat</td>
                <td style="vertical-align: middle; text-align: center;">%Hasil</td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			for($i=0;$i<count($hasil);$i++)
			{		
				$productiondate = $hasil[$i]['productiondate'];
				$formulaname = $hasil[$i]['formulaname'];
				$batchnumber = $hasil[$i]['batchnumber'];
				$mixquantity = $hasil[$i]['mixquantity'];
				$inventorycode = $hasil[$i]['inventorycode'];
				$namalengkap = $hasil[$i]['namalengkap'];
				$quantity = $hasil[$i]['quantity'];
				$quantityqc = $hasil[$i]['quantityqc'];
				$quantity_qc_reject = $hasil[$i]['quantity_qc_reject'];
				$fgwgt = $hasil[$i]['fgwgt'];
				$Persentase = $hasil[$i]['Persentase'];
                
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>"><?php echo $mylib->ubah_tanggal($productiondate); ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>"><?php echo $formulaname; ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>"><?php echo $batchnumber; ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" style="text-align: right;"><?php echo ubah_format($mixquantity); ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" ><?php echo $inventorycode; ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>"><?php echo $namalengkap; ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" style="text-align: right;"><?php echo ubah_format($quantity); ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" style="text-align: right;"><?php echo ubah_format($quantityqc); ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" style="text-align: right;"><?php echo ubah_format($quantity_qc_reject); ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" style="text-align: right;"><?php echo ubah_format($fgwgt); ?></td>
					<td nowrap title="<?php echo $productiondate." :: ".$formulaname." :: ".$batchnumber; ?>" style="text-align: right;"><?php echo ubah_format($Persentase); ?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
