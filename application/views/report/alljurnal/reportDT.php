<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/alljurnal/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportdetailalljurnal.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');document.getElementById('search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Detail Transaksi PV</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table class="tablebawah" width="95%">
	<tr>
		<th align="center" bgcolor="#f3f7bb">No Referensi</th>
		<th align="center" bgcolor="#f3f7bb">Tanggal</th>
<!--		<th align="center" bgcolor="#f3f7bb">Jenis</th>-->
		<th align="center" bgcolor="#f3f7bb">Kode</th>
		<th align="center" bgcolor="#f3f7bb">Nomor Transaksi</th>
<!--		<th align="center" bgcolor="#f3f7bb">Keterangan</th>-->
		<th align="center" bgcolor="#f3f7bb">Rekening</th>
		<th align="center" bgcolor="#f3f7bb">Nama Rekening</th>
		<th align="center" bgcolor="#f3f7bb">Sub Divisi</th>
		<th align="center" bgcolor="#f3f7bb">Keterangan Detail</th>
		<th align="center" bgcolor="#f3f7bb">Kepada/Dari</th>
		<th align="center" bgcolor="#f3f7bb">Debit</th>
		<th align="center" bgcolor="#f3f7bb">Kredit</th>
		<th align="center" bgcolor="#f3f7bb">User Add</th>
		<th align="center" bgcolor="#f3f7bb">Nama Karyawan</th>

	</tr>
<?php
	if(count($hasil)>0){
		$total1 = 0;
		$total2 = 0;
		for($s=0;$s<count($hasil);$s++)
		{
		    $total1 += (float)$hasil[$s]['Debit'];
			$total2 += (float)$hasil[$s]['Kredit'];
	?>
			<tr>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NoReferensi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_tanggal($hasil[$s]['Tanggal'])?></td>
<!--				<td nowrap bgcolor="#ccddff">--><?//=$hasil[$s]['JenisJurnal']?><!--</td>-->
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KodeJurnal']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NoTransaksi']?></td>
<!--				<td nowrap bgcolor="#ccddff">--><?//=$hasil[$s]['KetHeader']?><!--</td>-->
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KdRekening']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaRekening']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaSubDivisi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Keterangan']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KepadaDari']?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['Debit'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['Kredit'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['UserTrans']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['employee_name']?></td>
			</tr>
	<?php
		}
	?>
		<tr>
			<td nowrap align='center' bgcolor='#f7d7bb' colspan="9"><b>Total</b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total1)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total2)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'></td>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="12"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>