<script>
    
	function mouseclick1(resepid)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    base_url = "<?php echo base_url();?>";
	    
	    try{
	        windowOpener(600, 900, 'Recipe',base_url+'index.php/report/report_food_cost/popup/'+resepid , 'Recipe')    
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}  
	
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_food_cost/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="report_food_cost.xls"');
	}
    
    if($excel == "Excel")
    {
        $table_border = 1;
        $style = "style='border-collapse: collapse;'";
    }else{
		$table_border = 0;
        $style = "style='border-collapse: collapse;'";
	}
    
    
    $echo = '';
	if($excel == "Excel"){
		$echo = '
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">REPORT FOOD COST</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'" '.$style.'>
		<thead>
	    	<tr>
		        <th width="50">ID</th>
		        <th style="text-align: center;">PCode</th>
		        <th style="text-align: center;">Nama Barang</th>
		        <th style="text-align: center;">HPP</th>
		        <th style="text-align: center;">Harga Jual</th>
		        <th style="text-align: center;">Food Cost</th>
		        <th style="text-align: center;">Tgl Hitung</th>
		    </tr>
		</thead>
		<tbody>
		';

    echo $echo;
    
    foreach($hasil as $val)
    {
    ?>    
    	<tr onclick="mouseclick1('<?php echo $val['resep_id'];?>')">
	        <td><?=$val['resep_id'];?></td>
	        <td align="left"><?= $val['PCode'];?></td>
	        <td align="left"><?=$val['NamaLengkap'];?></td>
	        <td align="right"><?=number_format($val['HPP'],0,',','.');?></td>
	        <td align="right"><?=number_format($val['HargaJual'],0,',','.');?></td>
	        <td align="right"><?=number_format($val['FoodCost'],2,',','.');?></td>
	        <td align="center"><?=$val['LastCalculateDate'];?></td>
	       </tr>
	<?php
    }
    ?>
	</tbody>
	</table>
</form>
	</div>
</div>