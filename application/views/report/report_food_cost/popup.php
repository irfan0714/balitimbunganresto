<?php 
$modul = "Recipe";
?>
<head>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/Logosg.png" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
    
</head>
<div class="row" >
    <div class="col-md-12" align="left">
    	
		<form method='post' name="theform" id="theform">
		
	    <table class="table table-bordered responsive"> 
	        <tr>
	            <td class="title_table" width="150">Recipe ID</td>
	            <td>
	            	<input type="readonly" class="form-control-new datepicker" value="<?php echo $header[0]['resep_id']; ?>" name="resep_id" id="resep_od" size="50">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Nama Recipe</td>
	            <td> 
		            <input type="hidden" class="form-control-new datepicker" value="<?php echo $header[0]['PCode']; ?>" name="PCode" id="PCode">
	            	<input type="readonly" class="form-control-new datepicker" value="<?php echo $header[0]['NamaLengkap']; ?>" name="namalengkap" id="namalengkap" size="100">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jenis <font color="red"><b>(*)</b></font></td>
	            <td colspan="3">
	            	<select class="form-control-new" name="v_jenis" id="v_jenis" style="width: 200px;">
	            		<option <?php if($header[0]['Jenis']=='C'){ echo "selected='selected'"; } ?> value="C">Compliment</option>
	            		<option <?php if($header[0]['Jenis']=='R'){ echo "selected='selected'"; } ?> value="R">Publish</option>
	            	</select>
	            </td>
	        </tr>  
	        
	        <tr>
	            <td class="title_table">Qty</td>
	            <td> 
	            	<input type="text" class="form-control-new" value="<?php echo $header[0]['Qty']; ?>" name="Qty" id="Qty">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Satuan</td>
	            <td> 
	            	<input type="readonly" class="form-control-new" value="<?php echo $header[0]['Satuan']; ?>" name="Satuan" id="Satuan">
	            </td>
	        </tr>
	        
	        <tr>
        		<td colspan="100%" >
					<table class="table table-bordered responsive">
						<thead class="title_table">
							<tr>
							    <th width="30"><center>No</center></th>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>                  
							    <th width="100"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
								<th width="150"><center>Harga</center></th>	
								<th width="150"><center>Total</center></th>
							</tr>
						</thead>
						<tbody>
						
						<?php
						$no=1;
						$gtotal = 0;
						foreach($detail as $val)
						{
							$total = $val["Harga"]* $val['Qty'];
							$gtotal += $total;
							?>
							<tr>
								<td align="center">
									<center><?php echo $no; ?></center>
								</td>
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaLengkap"]; ?></td>
								<td align="right"><?php echo number_format($val["Qty"],1,',','.'); ?></td>
								<td align="center"><?php echo $val["Satuan"]; ?></td>
								<td align="right"><?php echo number_format($val["Harga"],2,',','.'); ?></td>
								<td align="right"><?php echo number_format($total,2,',','.'); ?></td>
						    </tr>
							<?php
							$no++;
						}
						?>
						<tr>
							<td colspan="5">&nbsp;</td>
							<td align="left"><b>HPP</b></td>
							<td align="right"><b><?php echo number_format($gtotal,2,',','.'); ?></b></td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
							<td align="left"><b>Harga Jual</b></td>
							<td align="right"><b><?php echo number_format($header[0]['Harga1c'],2,',','.'); ?></b></td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
							<td align="left"><b>Food Cost (%)</b></td>
							<td align="right"><b><?php echo number_format($gtotal/$header[0]['Harga1c']*100,2,',','.'); ?></b></td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
							<td align="left"><b>Profit Margin (%)</b></td>
							<td align="right"><b><?php echo number_format(($header[0]['Harga1c']-$gtotal)/$header[0]['Harga1c']*100,2,',','.'); ?></b></td>
						</tr>
						
							
						</tbody>
					</table>
        		</td>
	        </tr>
	    </table>
	    
	    </form> 
	</div>
</div>