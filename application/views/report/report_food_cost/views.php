<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Food Cost";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_food_cost/cari/">       
        	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            	<table class="table table-bordered responsive">
                	<thead>
                    	<tr>
                        	<th width="100">Jenis Recipe</th>
                        	<th>: 
				            	<select class="form-control-new" name="Jenis" id="Jenis" style="width: 200px;">
				            		<option <?php if($Jenis=="C"){ echo "selected='selected'"; } ?> value="C">Compartion</option>
				            		<option <?php if($Jenis=="R"){ echo "selected='selected'"; } ?> value="R">Publish</option>
				            	</select>
                        	</th>
                    	</tr>
                    	<tr>
                        	<td>&nbsp;</td>
                        	<td>
                            	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        	</td>
                    	</tr>
                	</thead>
                
            	</table> 
            </div>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT) 
{
   	$this->load->view("report/report_food_cost/reportRT");
}

$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>