<br>
<table class="table table-bordered" border="1">
	<thead class="title_table">
		<tr>
			<td rowspan="2" align="center" style="background-color: #ffffff">Kode</td>
			<td rowspan="2" align="center" style="background-color: #ffffff">Nama</td>
			<td colspan="3" align="center" style="background-color: #ffffff">Penjualan</td>
			<td colspan="3" align="center" style="background-color: #ffffff">Kunjungan</td>
			<td rowspan="2" align="center" style="background-color: #ffffff">Total Komisi</td>
		</tr>
		<tr>
			<td align="center" style="background-color: #ffffff">Pencapaian</td>
			<td align="center" style="background-color: #ffffff">%</td>
			<td align="center" style="background-color: #ffffff">Komisi</td>
			<td align="center" style="background-color: #ffffff">Pencapaian</td>
			<td align="center" style="background-color: #ffffff">%</td>
			<td align="center" style="background-color: #ffffff">Komisi</td>
		</tr>
	</thead>
	<tbody>
	    <tr>
			<td align="left" colspan="9" style="background-color: #f0f0f0"><b>Insentif Salesman</b></td>
		</tr>
	<?php
	    $total_pencapaian=0;
		for($i=0; $i<count($listemp);$i++){

		?>
		<tr>
			<td width="100"><b><?=$listemp[$i]['KdSalesman'];?></b></td>
			<td width="200"><b><?=$listemp[$i]['NamaSalesman'];?></b></td>
			<?php
				$total=0;
			    $kdsalesman= $listemp[$i]['KdSalesman'];
				foreach($viewdata as $val){
				if($val['KdSalesman']==$kdsalesman){
				$total+=$val['pencapaian'];
				}				
				}
			?>		
			<td width="100" align="right"><b><?php echo ubah_format($total);?></b></td>
			<?php 
			if(round($total)>=60000000 AND round($total)<=80000000){
			$disk_ = "0.50";	
			$disk = 0.50;
			}else if(round($total)>=81000000 AND round($total)<=100000000){
			$disk_ = "0.75";
			$disk = 0.75;
			}else if(round($total)>=101000000 AND round($total)<=150000000){
			$disk_ = "1.00";
			$disk = 1.00;
			}else if(round($total)>=151000000 AND round($total)<=200000000){
			$disk_ = "1.50";
			$disk = 1.50;
			}else{
			$disk_ = "0.00";
			$disk = 0.00;
			}
			?>
			<td width="100" align="right"><b><?php echo $disk_;?></b></td>
			<td width="100" align="right"><b><?php echo ubah_format(($total*($disk/100)));?></b></td>
			<?php
			    $kdsalesman= $listemp[$i]['KdSalesman'];
				foreach($viewdatakunj as $val){
				if($val['KdSalesman']==$kdsalesman AND $val['KdSalesman']!='YA0001'){
				$totkunj=$val['tot_kunj'];
				}				
				}
			?>
			<td width="100" align="right"><b><?php echo $totkunj;?></b></td>
			<td width="100" align="center"><b><?php echo round(($totkunj/6)*100);?></b></td>
			<?php 
			if (($totkunj/6)*100>=90.01 AND ($totkunj/6)*100<=100 ){
			$kom = 150000;
			}else if (($totkunj/6)*100>=100.01 AND ($totkunj/6)*100<=110){
			$kom = 300000;
			}else{
			$kom = 0;
			}
			?>
			<td width="100" align="right"><b><?php echo ubah_format($kom);?></b></td>
			<td width="100" align="right"><b><?php echo ubah_format($kom+($total*($disk/100)));?></b></td>
		</tr>
		<?php } ?>
		
		
		
		<!-- insentif supervisor -->
		<tr>
			<td align="left" colspan="9" style="background-color: #f0f0f0"><b>Insentif Supervisor</b></td>
		</tr>
		<?php
		for($i=0; $i<count($listempspv);$i++){

		?>
		<tr>
			<td width="100"><b><?=$listempspv[$i]['KdSupervisor'];?></b></td>
			<td width="200"><b><?=$listempspv[$i]['NamaSupervisor'];?></b></td>
			<?php
				$totalspv=0;
			    $kdsupervisor= $listempspv[$i]['KdSupervisor'];
				foreach($viewdata as $val){
				if($val['KdSupervisor']==$kdsupervisor){
				$totalspv+=$val['pencapaian'];
				}				
				}
			?>		
			<td width="100" align="right"><b><?php echo ubah_format($totalspv);?></b></td>
			<?php 
			if(round($totalspv)>=240000000 AND round($totalspv)<=300000000){
			$disk2_ = "0.50";
			$disk2 = 0.50;
			}else if(round($totalspv)>=301000000 AND round($totalspv)<=350000000){
			$disk2_ = "0.75";
			$disk2 = 0.75;
			}else if(round($totalspv)>=351000000 AND round($totalspv)<=400000000){
			$disk2_ = "1.00";
			$disk2 = 1.00;
			}else{
			$disk2_ = "0.00";
			$disk2 = 0.00;
			}
			?>
			<td width="100" align="right"><b><?php echo $disk2_;?></b></td>
			<td width="100" align="center"><b><?php echo ubah_format(($totalspv*($disk2/100)));?></b></td>
			<?php
			    $kdsupervisor= $listempspv[$i]['KdSupervisor'];
				foreach($viewdatakunj as $val){
				if($val['KdSupervisor']==$kdsupervisor){
				$totkunjspv=$val['tot_kunj'];
				}				
				}
			?>
			<td width="100" align="right"><b><?php echo $totkunjspv;?></b></td>
			<td width="100" align="center"><b><?php echo round(($totkunjspv/4)*100);?></b></td>
			<?php 
			if (($totkunj/4)*100>=90.01 AND ($totkunj/4)*100<=100 ){
			$kom = 150000;
			}else if (($totkunj/4)*100>=100.01 AND ($totkunj/4)*100<=110){
			$kom = 300000;
			}else if (($totkunj/4)*100> 111){
			$kom = 300000;
			}else{
			$kom = 0;
			}
			?>
			<td width="100" align="right"><b><?php echo ubah_format($kom);?></b></td>
			<td width="100" align="right"><b><?php echo ubah_format($kom+($total*($disk/100)));?></b></td>
			
		</tr>
		<?php } ?>
		
		<!-- insentif manager -->
		<tr>
			<td align="left" colspan="9" style="background-color: #f0f0f0"><b>Insentif Manager</b></td>
		</tr>
		<td colspan="2" width="100"><b>MANAGER</b></td>
		<?php
				$totalmgr=0;
				foreach($viewdata as $val){
				if(!empty($val['KdSupervisor']) AND !empty($val['KdSalesman'])) {
				$totalmgr+=$val['pencapaian'];
				}				
				}
			?>		
			<td width="100" align="right"><b><?php echo ubah_format($totalmgr);?></b></td>
			<?php 
			if(round($totalmgr)>=400000000 AND round($totalmgr)<=500000000){
			$disk3_ = "0.50";
			$disk3 = 0.50;
			}else if(round($totalmgr)>=501000000 AND round($totalmgr)<=60000000){
			$disk3_ = "0.75";
			$disk3 = 0.75;
			}else if(round($totalmgr)>=601000000 AND round($totalmgr)<=700000000){
			$disk3_ = "1.00";
			$disk3 = 1.00;
			}else{
			$disk3_ = "0.00";
			$disk3 = 0.00;
			}
			?>
			<td width="100" align="right"><b><?php echo $disk3_;?></b></td>
			<td width="100" align="center"><b><?php echo ubah_format(($totalmgr*($disk3/100)));?></b></td>
			<td colspan="4">&nbsp;</td>
	</tbody> 
</table>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>