<?php $this->load->view('header_part1')?>

	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	<script>
	$(document).ready(function(){
		$('#tgldari').Zebra_DatePicker({format: 'Y-m-d'});
        $('#tglsampai').Zebra_DatePicker({format: 'Y-m-d'});
	});
	</script>
	<script> 
    function submitThis()
    { 
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>
</head>
<?php $this->load->view('header_part3')?>

<form method="POST"  name="search" action='<?=base_url(); ?>index.php/report/insentif_sls_spv_mgr/tampil'>

<table>
	<tr>
		<td>
			Tahun&nbsp;&nbsp;
		</td>
		<td>
							<select class="form-control-new" name="tahun" id="tahun">
                                    <option  <?php if($tahun==""){ echo "selected='selected'"; } ?> value=""> -- Pilih -- </option>
                                    <option  <?php if($tahun=="2017"){ echo "selected='selected'"; } ?> value="2017">2017</option>
                                    <option  <?php if($tahun=="2018"){ echo "selected='selected'"; } ?> value="2018">2018</option>
                                    <option  <?php if($tahun=="2019"){ echo "selected='selected'"; } ?> value="2019">2019</option>
                                    <option  <?php if($tahun=="2020"){ echo "selected='selected'"; } ?> value="2020">2020</option>
                            </select>
                            &nbsp;&nbsp;Bulan&nbsp;&nbsp;
                            <select class="form-control-new" name="bulan" id="bulan">
                                    <option   <?php if($bulan==""){ echo "selected='selected'"; } ?>value=""> -- Pilih -- </option>
                                    <option   <?php if($bulan=="01"){ echo "selected='selected'"; } ?> value="01">Januari</option>
                                    <option   <?php if($bulan=="02"){ echo "selected='selected'"; } ?> value="02">Februari</option>
                                    <option   <?php if($bulan=="03"){ echo "selected='selected'"; } ?> value="03">Maret</option>
                                    <option   <?php if($bulan=="04"){ echo "selected='selected'"; } ?> value="04">April</option>
                                    <option   <?php if($bulan=="05"){ echo "selected='selected'"; } ?> value="05">Mei</option>
                                    <option   <?php if($bulan=="06"){ echo "selected='selected'"; } ?> value="06">Juni</option>
                                    <option   <?php if($bulan=="07"){ echo "selected='selected'"; } ?> value="07">Juli</option>
                                    <option   <?php if($bulan=="08"){ echo "selected='selected'"; } ?> value="08">Agustus</option>
                                    <option   <?php if($bulan=="09"){ echo "selected='selected'"; } ?> value="09">Sepetember</option>
                                    <option   <?php if($bulan=="10"){ echo "selected='selected'"; } ?> value="10">Oktober</option>
                                    <option   <?php if($bulan=="11"){ echo "selected='selected'"; } ?> value="11">November</option>
                                    <option   <?php if($bulan=="12"){ echo "selected='selected'"; } ?> value="12">Desember</option>
                            </select>
                            
		&nbsp;<input type="submit" name='submit' value="TAMPIL" class='btn btn-default'>
		&nbsp;<input type="submit" name='submit' value="XLS"  id="excel" class='btn btn-default'>
		</td>
	</tr>
</table>
</form> 

<?php

if ($tampilkanDT) 
{
    $this->load->view("report/report_insentif_sls_spv_mgr/tampil_insentif_sls_spv_mgr");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>