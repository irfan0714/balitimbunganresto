<table class="table table-bordered responsive" border="1">
	<thead class="title_table">
		<tr>
			<td align="center">No</td>
			<td align="center">MATA UANG</td>
			<td align="center">SUPPLIER</td>
			<td align="center">TANGGAL</td>
			<td align="center">NO PO</td>
			<td align="center">NO PENERIMA</td>
			<td align="center">NO INVOICE</td>
			<td align="center">DPP</td>
			<td align="center">PPN</td>
			<td align="center">TOTAL</td>
		</tr>
	</thead>
	 <tbody>
		<tr >
			<?php
			//a24
			if(count($viewdata)==0) 
			{
				echo "<tr><td colspan='10' align='center'>Tidak Ada Data</td></tr>";
			}
				$no=1;

				$subtotal_supplier = $total_per_mata_uang = 0;
			
				for($i=0 ; $i<count($viewdata) ; $i++) 
				{ 
			
				if($viewdata[$i]['MataUang'] == 'IDR')
					{
						$subtotal_supplier += $viewdata[$i]['Total'];
						$total_per_mata_uang += $viewdata[$i]['Total'];

				?>	

							<td><?=$no;?></td>
					<?php
							if($viewdata[$i]['MataUang']==@$viewdata[$i-1]['MataUang'])
							{
						
									?>
								<td align="left"></td>	
								<?php
							}
							else
							{
								?>
								<td align="left" ><b><?=$viewdata[$i]['MataUang']?></b></td>	
								<?php
							}
							?>
					<?php
							//nama supplier
							if($viewdata[$i]['Nama']==@$viewdata[$i-1]['Nama'])
							{

									?>
								<td align="left"></td>	
								<?php
							}
							else
							{
								?>
								<td align="left"><?=$viewdata[$i]['Nama']?></td>	
								<?php
							}
									?>
							<td align="left" ><?=$viewdata[$i]['Tanggal']?></td>
							<td align="left" ><?=$viewdata[$i]['NoPO']?></td>
							<td align="left" ><?=$viewdata[$i]['NoPenerimaan']?></td>
							<td align="left" ><?=$viewdata[$i]['NoFaktur']?></td>
							<td align="right"><?=number_format($viewdata[$i]['DPP'], 0, ',', '.')?></td>	
							<td align="right"><?=number_format($viewdata[$i]['PPN'], 0, ',', '.')?> %</td>	
							<td align="right"><?=number_format($viewdata[$i]['Total'], 0, ',', '.')?></td>
							<?php
				
						if($viewdata[$i]['KdSupplier']!=@$viewdata[$i+1]['KdSupplier'])
						{
							?>
							<tr>
								<td colspan="7"></td>
								<td align="right" colspan="2" style="background:#ffcc00"><b>Sub Total <?=$viewdata[$i]['Nama']?></b></td>
								<td align="right" style="background:#ffcc00"><?=number_format($subtotal_supplier, 0, ',', '.')?></td>
							</tr>
								<?php 
								$subtotal_supplier = 0;
						} 

				
						if($viewdata[$i]['MataUang']!=@$viewdata[$i+1]['MataUang'])
						{
							?>
							
							<tr>

								<td colspan="7"></td>
								<td align="right" style="background:#33ccff" colspan="2"><b>Total Per <?=$viewdata[$i]['MataUang']?></b></td>
								<td align="right" style="background:#33ccff"><?=number_format($total_per_mata_uang, 0, ',', '.')?></td>
							</tr>
							
								<?php 
								$total_per_mata_uang = 0;
								$subtotal_supplier = 0;
						} 
					}
						?>	
				
				</tr>

				<?php
					if($viewdata[$i]['MataUang'] == 'USD')
					{
						$subtotal_supplier += $viewdata[$i]['Total'];
						$total_per_mata_uang += $viewdata[$i]['Total'];

				?>	

							<td><?=$no;?></td>
					<?php
							if($viewdata[$i]['MataUang']==@$viewdata[$i-1]['MataUang'])
							{
						
									?>
								<td align="left"></td>	
								<?php
							}
							else
							{
								?>
								<td align="left" ><b><?=$viewdata[$i]['MataUang']?></b></td>	
								<?php
							}
							?>
					<?php
							//nama supplier
							if($viewdata[$i]['Nama']==@$viewdata[$i-1]['Nama'])
							{

									?>
								<td align="left"></td>	
								<?php
							}
							else
							{
								?>
								<td align="left"><?=$viewdata[$i]['Nama']?></td>	
								<?php
							}
							?>
					<td align="left" ><?=$viewdata[$i]['Tanggal']?></td>
					<td align="left" ><?=$viewdata[$i]['NoPO']?></td>
					<td align="left" ><?=$viewdata[$i]['NoPenerimaan']?></td>
					<td align="left" ><?=$viewdata[$i]['NoFaktur']?></td>
					<td align="right"><?=number_format($viewdata[$i]['DPP'], 0, ',', '.')?></td>	
					<td align="right"><?=number_format($viewdata[$i]['PPN'], 0, ',', '.')?> %</td>	
					<td align="right"><?=number_format($viewdata[$i]['Total'], 0, ',', '.')?></td>
					<?php
				
						if($viewdata[$i]['KdSupplier']!=@$viewdata[$i+1]['KdSupplier'])
						{
							?>
							<tr>

								<td colspan="7"></td>
								<td align="right" style="background:#ffcc00" colspan="2"><b>Sub Total <?=$viewdata[$i]['Nama']?></b></td>
								<td align="right" style="background:#ffcc00"><?=number_format($subtotal_supplier, 0, ',', '.')?></td>
							</tr>
								<?php 
								$subtotal_supplier = 0;
						} 
				
						if($viewdata[$i]['MataUang']!=@$viewdata[$i+1]['MataUang'])
						{
							?>
							
							<tr>

								<td colspan="7"></td>
								<td align="right" style="background:#33ccff" colspan="2"><b>Total Per <?=$viewdata[$i]['MataUang']?></b></td>
								<td align="right" style="background:#33ccff"><?=number_format($total_per_mata_uang, 0, ',', '.')?></td>
							</tr>
							
								<?php 
								$total_per_mata_uang = 0;
								$subtotal_supplier = 0;
						} 
					}
				?>


			<?php
				
				$no++;
					
				}
				?>
		</tr>
	</tbody>
</table>
</body>
</html>