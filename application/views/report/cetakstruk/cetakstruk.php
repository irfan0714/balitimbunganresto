<?php
$this->load->view('header');
$reportlib = new report_lib();

?>

<script>
    function submitThis()
    {
    	nostruk = $("#nostruk").val();
    	$.ajax({
    		url :"<?php echo base_url(); ?>index.php/report/cetakstruk/cari_kassa/"+nostruk,
    		success: function(res){
    			nokassa = res;
    			// nokassa = nostruk.substring(6,8);
		    	url_link = "<?php echo base_url(); ?>index.php/report/cetakstruk/cari";
		    	url_pos = "<?php echo base_url(); ?>index.php/transaksi/pos/cetak/"+nostruk+"/"+nokassa;
		    	url_touch = "<?php echo base_url(); ?>index.php/transaksi/pos_touch/cetakstruk/"+nostruk+"/"+nokassa;
		    	url_ticket_swing = "<?php echo base_url(); ?>index.php/transaksi/ticketing/create_ticket_wahana_payment_cetak_ulang/"+nostruk;

		        $.post(url_link, {nostruk: nostruk}, function(result){
		        	console.log(result);
		        	if(result==3){ // pos
						 $('#search').attr('action', url_pos).submit();
					}

					// else if(result==999){ // ticket swing
					// 	 $('#search').attr('action', url_ticket_swing).submit();
					// }
					else{ // pos touch
						$('#search').attr('action', url_touch).submit();
					}
		    	});
    		}
    	});



    }
</script>

<div class="row">
    <div class="col-md-12" align="left">

    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Cetak Ulang Struk</strong></li>
		</ol>

		<form method="POST" name="search" id="search">

	    <table class="table table-bordered responsive">
			<tr>
					<td class="title_table" width="150">Type</td>
					<td>
						<select class="form-control-new" name="v_tipe" id="v_tipe" style="width: 200px;">
		            		<option value="1">Sales</option>
		            		<!-- <option value="2">Ticket Swing</option> -->
		            	</select>
					</td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">No Struk</td>
	            <td>
	            	<input type="text" name="nostruk" id="nostruk" />
	            </td>
	        </tr>
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Cetak<i class="entypo-search"></i></button>
		       	</td>
	        </tr>

	    </table>
	    </form>
	</div>
</div>

<?php
$this->load->view('footer');
?>
