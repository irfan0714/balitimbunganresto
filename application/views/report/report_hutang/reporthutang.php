<?php
$mylib = new globallib();

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_hutang/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reporthutang.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">Rekening</td>
                <td style="vertical-align: middle; text-align: center;">Supplier</td>
                <td style="vertical-align: middle; text-align: center;">No Faktur</td>
                <td style="vertical-align: middle; text-align: center;">No PO</td>
                <td style="vertical-align: middle; text-align: center;">No RG</td>
                <td style="vertical-align: middle; text-align: center;">No Supplier</td>
                <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;">Jt Tempo</td>
                <td style="vertical-align: middle; text-align: center;">Type</td>
                <td style="vertical-align: middle; text-align: center;">Nilai Faktur</td>
                <td style="vertical-align: middle; text-align: center;">Sisa</td></td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$prev_rekening = '';			
			$prev_supplier = '';
			$subtotalsupplier = 0;
			$subtotalrekening = 0;
			$grandtotal = 0;
			for($i=0;$i<count($hasil);$i++)
			{	
				$kdrekening = $hasil[$i]['KdRekening'];	
				$matauang = $hasil[$i]['MataUang'];
				$supplier = $hasil[$i]['KdSupplier'];
				$nama = $hasil[$i]['Nama'];
				$nodokumen = $hasil[$i]['NoDokumen'];
				$nopo = $hasil[$i]['NoPO'];
				$nopenerimaan = $hasil[$i]['NoPenerimaan'];
				$nofaktursupplier = $hasil[$i]['NoFakturSupplier'];
				$tanggal = $hasil[$i]['Tanggal'];
				$jatuhtempo = $hasil[$i]['JatuhTempo'];
				$nilaitransaksi = $hasil[$i]['NilaiTransaksi'];
				$tipetransaksi = $hasil[$i]['tipetransaksi'];
				$sisa = $hasil[$i]['SisaHutang'];
				if($tipetransaksi == 'D' || $tipetransaksi == 'DM'){
					$sisa = $sisa * -1;
					$nilaitransaksi = $nilaitransaksi * -1;
				}
				
				$subtotalsupplier += $sisa;
				$subtotalrekening += $sisa;
				$grandtotal += $sisa;
				
				?>
				<tr>
				<?php
					if($prev_rekening == $kdrekening){
						$kdrekening_echo = '';
					}else{
						$kdrekening_echo = $kdrekening;
					}
					
					if($prev_supplier == $supplier){
						$nama_echo = '';
					}else{
						$nama_echo = $nama;
					}
				?>
					<td nowrap><?= $kdrekening_echo; ?></td>
					<td nowrap><?= $nama_echo; ?></td>
					<td nowrap><?= $nodokumen; ?></td>
					<td nowrap><?= $nopo; ?></td>
					<td nowrap><?= $nopenerimaan; ?></td>
					<td nowrap><?= $nofaktursupplier; ?></td>
					<td nowrap><?= $tanggal; ?></td>
					<td nowrap><?= $jatuhtempo; ?></td>
					<td nowrap><?= $tipetransaksi; ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($nilaitransaksi); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($sisa); ?></td>
				</tr>
			<?php
				if($i<count($hasil)-1){
					if($supplier != $hasil[$i+1]['KdSupplier']){
					?>
					<tr>
						<td colspan="10" style="text-align: center;font-weight: bold;">Sub Total <?=$nama;?></td>
						<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalsupplier);?></td>
					</tr>
					<?php	
						$subtotalsupplier = 0;
					}
					
					if($kdrekening != $hasil[$i+1]['KdRekening']){
					?>
					<tr>
						<td colspan="10" style="text-align: center;font-weight: bold;">Sub Total <?=$kdrekening;?></td>
						<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalrekening);?></td>
					</tr>
					<?php	
						$subtotalrekening = 0;
					}
				}
				
				$prev_rekening = $kdrekening;			
				$prev_supplier = $supplier;
			}
			?>
			</tbody>
			<tr>
				<td colspan="10" style="text-align: center;font-weight: bold;">Sub Total <?=$nama;?></td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalsupplier);?></td>
			</tr>
			<tr>
				<td colspan="10" style="text-align: center;font-weight: bold;">Total <?=$kdrekening;?></td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalrekening);?></td>
			</tr>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>
