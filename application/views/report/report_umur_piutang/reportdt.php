<?php
$mylib = new globallib();
?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_hutang/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportumurpiutang.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">Pelanggan</td>
                <td style="vertical-align: middle; text-align: center;">No Faktur</td>
                <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;">Jt Tempo</td>
                <td style="vertical-align: middle; text-align: center;">Nilai Faktur</td>
                <td style="vertical-align: middle; text-align: center;">Sisa</td>
                <td style="vertical-align: middle; text-align: center;">Belum JT</td>
                <td style="vertical-align: middle; text-align: center;">01-30</td>
                <td style="vertical-align: middle; text-align: center;">31-60</td>
                <td style="vertical-align: middle; text-align: center;">61-90</td>
                <td style="vertical-align: middle; text-align: center;">>=91</td>
                <td style="vertical-align: middle; text-align: center;">Umur</td>
                
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$prev_customer = '';
			$subtotalcustomer = 0;
			$grandtotal = 0;
			
			$subtot_customer_aging1 = 0;
			$subtot_customer_aging31 = 0;
			$subtot_customer_aging61 = 0;
			$subtot_customer_aging91 = 0;
			$subtot_customer_belumjt = 0;
			
			$grandtotal_aging1 = 0;
			$grandtotal_aging31 = 0;
			$grandtotal_aging61 = 0;
			$grandtotal_aging91 = 0;
			$grandtotal_belumjt = 0;
			
			for($i=0;$i<count($hasil);$i++)
			{	
				$customer = $hasil[$i]['KdCustomer'];
				$nama = $hasil[$i]['Nama'];
				$nodokumen = $hasil[$i]['NoDokumen'];
				$tanggal = $hasil[$i]['Tanggal'];
				$jatuhtempo = $hasil[$i]['JatuhTempo'];
				$nilaitransaksi = $hasil[$i]['NilaiTransaksi'];
				$sisa = $hasil[$i]['SisaPiutang'];
				$belumjt = $hasil[$i]['BelumJT'];
				$aging1 = $hasil[$i]['Aging1'];
				$aging31 = $hasil[$i]['Aging31'];
				$aging61 = $hasil[$i]['Aging61'];
				$aging91 = $hasil[$i]['Aging91'];
				$umur = $hasil[$i]['Umur'];
				
				$subtotalcustomer += $sisa;
				$grandtotal += $sisa;
				
				$subtot_customer_aging1 += $aging1;
				$subtot_customer_aging31 += $aging31;
				$subtot_customer_aging61 += $aging61;
				$subtot_customer_aging91 += $aging91;
				$subtot_customer_belumjt += $belumjt;
				
				$grandtotal_aging1 += $aging1;
				$grandtotal_aging31 += $aging31;
				$grandtotal_aging61 += $aging61;
				$grandtotal_aging91 += $aging91;
				$grandtotal_belumjt += $belumjt;
				
				?>
				<tr>
				<?php
					if($prev_customer == $customer){
						$nama_echo = '';
					}else{
						$nama_echo = $nama;
					}
				?>
					<td nowrap><?= $nama_echo; ?></td>
					<td nowrap><?= $nodokumen; ?></td>
					<td nowrap><?= $tanggal; ?></td>
					<td nowrap><?= $jatuhtempo; ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($nilaitransaksi); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($sisa); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($belumjt); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging1); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging31); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging61); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging91); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($umur); ?></td>
				</tr>
			<?php
				if($i<count($hasil)-1){
					if($customer != $hasil[$i+1]['KdCustomer']){
					?>
					<tr>
						<td colspan="4" style="text-align: center;font-weight: bold;">Sub Total <?=$nama;?></td>
						<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalcustomer);?></td>
						<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_belumjt); ?></td>
						<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging1); ?></td>
						<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging31); ?></td>
						<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging61); ?></td>
						<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging91); ?></td>
					</tr>
					<?php	
						$subtotalcustomer = 0;
						$subtot_customer_belumjt = 0;
						$subtot_customer_aging1 = 0;
						$subtot_customer_aging31 = 0;
						$subtot_customer_aging61 = 0;
						$subtot_customer_aging91 = 0;
						
					}
				}
				
				$prev_customer = $customer;
			}
			?>
			</tbody>
			<tr>
				<td colspan="4" style="text-align: center;font-weight: bold;">Sub Total <?=$nama;?></td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotalcustomer);?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_belumjt); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging1); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging31); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging61); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($subtot_customer_aging91); ?></td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;font-weight: bold;">Grand Total</td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($grandtotal);?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_belumjt); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging1); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging31); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging61); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging91); ?></td>
			</tr>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
