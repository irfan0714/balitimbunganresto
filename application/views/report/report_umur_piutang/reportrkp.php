<?php
$mylib = new globallib();
?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_piutang/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="reportumurpiutang.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">Pelanggan</td>
                <td style="vertical-align: middle; text-align: center;">Piutang</td>
                <td style="vertical-align: middle; text-align: center;">Belum JT</td>
                <td style="vertical-align: middle; text-align: center;">01-30</td>
                <td style="vertical-align: middle; text-align: center;">31-60</td>
                <td style="vertical-align: middle; text-align: center;">61-90</td>
                <td style="vertical-align: middle; text-align: center;">>=91</td>
                
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$prev_customer = '';
			$subtotalcustomer = 0;
			$subtotalmatauang = 0;
			$grandtotal = 0;
			
			$subtot_customer_aging1 = 0;
			$subtot_customer_aging31 = 0;
			$subtot_customer_aging61 = 0;
			$subtot_customer_aging91 = 0;
			$subtot_customer_belumjt = 0;
			
			$grandtotal_aging1 = 0;
			$grandtotal_aging31 = 0;
			$grandtotal_aging61 = 0;
			$grandtotal_aging91 = 0;
			$grandtotal_belumjt = 0;
			
			for($i=0;$i<count($hasil);$i++)
			{		
				$customer = $hasil[$i]['KdCustomer'];
				$nama = $hasil[$i]['Nama'];
				$nilaitransaksi = $hasil[$i]['NilaiTransaksi'];
				$sisa = $hasil[$i]['SisaPiutang'];
				$belumjt = $hasil[$i]['BelumJT'];
				$aging1 = $hasil[$i]['Aging1'];
				$aging31 = $hasil[$i]['Aging31'];
				$aging61 = $hasil[$i]['Aging61'];
				$aging91 = $hasil[$i]['Aging91'];
				
				$grandtotal += $sisa;
				
				$grandtotal_aging1 += $aging1;
				$grandtotal_aging31 += $aging31;
				$grandtotal_aging61 += $aging61;
				$grandtotal_aging91 += $aging91;
				$grandtotal_belumjt += $belumjt;
				
				?>
				<tr>
					<td nowrap><?= $nama; ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($sisa); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($belumjt); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging1); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging31); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging61); ?></td>
					<td nowrap style="text-align: right;"><?= ubah_format($aging91); ?></td>
				</tr>
			<?php
				
				$prev_customer = $customer;
			}
			?>
			</tbody>
			<tr>
				<td style="text-align: center;font-weight: bold;">Total</td>
				<td style="text-align: right;font-weight: bold;"><?=ubah_format($grandtotal);?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_belumjt); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging1); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging31); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging61); ?></td>
				<td nowrap style="text-align: right;font-weight: bold;"><?= ubah_format($grandtotal_aging91); ?></td>
			</tr>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
