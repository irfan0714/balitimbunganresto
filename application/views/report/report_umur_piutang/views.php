<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Umur Piutang";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_umur_piutang/cari/">      
        	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            	<table class="table table-bordered responsive">
                	<thead>
                    	<tr>
                        	<th width="100">Per Tanggal</th>
                        	<th>: 
                            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date; ?>" name="v_date" id="v_date" size="10" maxlength="10">
                        	</th>
                    	</tr>
                    	<tr>
                        	<th width="100">Type</th>
                        	<th>: 
                            	<select id="type" name="type">
                    				<option value='D' <?php if($type=='D') echo "selected";?>>Detail</option>
                    				<option value='R' <?php if($type=='R') echo "selected";?>>Rekap</option>
                    			</select>
                        	</th>
                    	</tr>
                    	<tr>
                        	<td>&nbsp;</td>
                        	<td>
                        		<input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                            	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                        	</td>
                    	</tr>
                	</thead>
                
            	</table> 
            </div>
	    </form> 
	</div>
</div>

<?php
if ($tampilkan) 
{
	if($type=='D')
   		$this->load->view("report/report_umur_piutang/reportdt");
   	else
   		$this->load->view("report/report_umur_piutang/reportrkp");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>