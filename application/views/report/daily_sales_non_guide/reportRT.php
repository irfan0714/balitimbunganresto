<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales_non_guide/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="dailysales.xls"');
    }
   
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    $arr_data  = array();
	$arr_total = array();
	
    for($i=$mylib->parsedate($v_date_from);$i<=$mylib->parsedate($v_date_to);$i=$i+86400)
    {
        $arr_data["list_date"][$i] = $i;
    }
    
	for($i=0;$i<count($tamu);$i++){
        $tanggal = $tamu[$i]['Tanggal'];
        $jmltamu = $tamu[$i]['Tamu'];
        $arr_data['tamu'][$tanggal] = $jmltamu ;                   
    }
    
    for($i=0;$i<count($salesrec);$i++){
        $Tanggal = $salesrec[$i]['Tanggal'];
        $jenis = $salesrec[$i]['Jenis'];
        $NamaSubKategori = $salesrec[$i]['NamaSubKategori'];
        $Sales = $salesrec[$i]['Sales'];
        
        $arr_data["total_TotalNilai"][$Tanggal] += $Sales;
        
        switch($jenis){
			case 1:
			 	$arr_data["total_OH"][$Tanggal] += $Sales;
				break;
			case 2:
				$arr_data["total_SG"][$Tanggal] += $Sales; 
				break;
			case 3:
				$arr_data["total_BE"][$Tanggal] += $Sales; 
				break;
			case 4:
				$arr_data["total_aksesoris"][$Tanggal] += $Sales; 
				break;
		}
    }
    
    $akhir =0;
    $tg=0;
    $d=0;
    $tl=0;
    for($i=0;$i<count($komisi);$i++){
        $NoTransaksi = $komisi[$i]['NoTransaksi'];    
        $TglTransaksi = $komisi[$i]['TglTransaksi'];
        $Total = $komisi[$i]['Total'];
        
        $arr_data["komisi"][$TglTransaksi]+=$Total;
    }

    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT DAILY '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th width="30">No</th>
		        <th style="text-align: center;">Tanggal</th>
		        <th style="text-align: center;">Hari</th>
		        <th style="text-align: center;">Herborist</th>
		        <th style="text-align: center;">SG</th>
		        <th style="text-align: center;">BE</th>
		        <th style="text-align: center;">Aksesoris</th>
		        <th style="text-align: center;">Sales</th>
		    </tr>
		</thead>

		<tbody>
		';

    $no = 1; 
    foreach($arr_data["list_date"] as $date=>$val)
    {
        $TotalNilai = $arr_data["total_TotalNilai"][date("Y-m-d", $date)];
        
        $total_OH = $arr_data["total_OH"][date("Y-m-d", $date)];
        $total_SG = $arr_data["total_SG"][date("Y-m-d", $date)];
        $total_BE = $arr_data["total_BE"][date("Y-m-d", $date)];
        $total_aksesoris = $arr_data["total_aksesoris"][date("Y-m-d", $date)];
		        
        $bg_hari ="";
        if(date("l", $date)=="Saturday" || date("l", $date)=="Sunday")
        {
            $bg_hari = "background: #ff99cc;";
        }

    	$echo .= '
    	<tr style="'.$bg_color.' color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td>'.date("d/m/Y", $date).'</td>
	        <td style="'.$bg_hari.'">'.date("l", $date).'</td>
	        <td align="right">'.$mylib->format_number($total_OH, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_SG, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_BE, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($total_aksesoris, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($TotalNilai, 0, "", "", "ind").'</td>
	    </tr>
    	';
        
        $arr_total["OH"] += $total_OH;
        $arr_total["SG"] += $total_SG;
        $arr_total["BE"] += $total_BE;
        $arr_total["aksesoris"] += $total_aksesoris;
        $arr_total["Nilai"] += $TotalNilai;

        $no++;
    }
    
	$echo .= '
		</tbody>';
	    
	$echo .= '
         <tfoot>
            <tr style="text-align: right; font-weight: bold; color: black;">
                <td colspan="3">Grand Total</td>
                <td align="right">'.$mylib->format_number($arr_total["OH"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["SG"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["BE"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["aksesoris"], 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($arr_total["Nilai"], 0, "", "", "ind").'</td>
            </tr>
        </tfoot> 
	    ';

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = $store[0]['NamaPT']." - Report Daily ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>