<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_bpermintaan/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportdetailpermintaanbarang.xls"');
}
if($excel!="excel")
{ ?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel' onclick="$('#excel').val('excel');$('#search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Detail Permintaan Barang - <?=$jenis?></font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
		<th align="center" bgcolor="#f3f7bb">No Dokumen</th>
		<th align="center" bgcolor="#f3f7bb">Tanggal</th>
		<th align="center" bgcolor="#f3f7bb">KdDivisi</th>
		<th align="center" bgcolor="#f3f7bb">Nama Divisi</th>
		<th align="center" bgcolor="#f3f7bb">KdGudang</th>
		<th align="center" bgcolor="#f3f7bb">Nama Gudang</th>
		<th align="center" bgcolor="#f3f7bb">Keterangan</th>
		<th align="center" bgcolor="#f3f7bb">Kode Barang</th>
		<th align="center" bgcolor="#f3f7bb">Nama Barang</th>
		<th align="center" bgcolor="#f3f7bb">Qty</th>
		<th align="center" bgcolor="#f3f7bb">Satuan</th>
		<th align="center" bgcolor="#f3f7bb">Status</th>
	</tr>
<?php
	if(count($hasil)>0){
		for($s=0;$s<count($hasil);$s++)
		{
	?>
			<tr>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NoDokumen']?></td>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_format_tanggal($hasil[$s]['TglDokumen'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KdDivisi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaDivisi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['KdGudang']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaGudang']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Keterangan']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['PCode']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['NamaBarang']?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil[$s]['Qty'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['Satuan']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil[$s]['StatusT']?></td>
			</tr>
	<?php
		}
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="13"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>