<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
function rawform(no)
{
	base_url = $("#baseurl").val();
	url = base_url+"index.php/pop/barangreport/index/"+no+"/";
	window.open(url,'popuppage','scrollbars=yes,width=750,height=500,top=150,left=150');
}
function loading(){
	base_url = $("#baseurl").val();
	$('#tgl1').Zebra_DatePicker({format: 'd-m-Y'});
	$('#tgl2').Zebra_DatePicker({format: 'd-m-Y'});
	//$('#tgl1').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
	//$('#tgl2').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
}
function submitThis()
{
	$("#excel").val("");
	$("#print").val("");
	userlevel= $("#userlevel").val();
	tgl1 = $("#tgl1").val();
	tgl2 = $("#tgl2").val();
	yymmdd1 = tgl1.split('-');
	yymmdd2 = tgl2.split('-');
	
	t1=new Date(yymmdd1[2],yymmdd1[1],yymmdd1[0]);
	t2=new Date(yymmdd2[2],yymmdd2[1],yymmdd2[0]);
	
	selisih = parseInt((t2-t1)/(24*3600*1000));

	$("#search").submit();
}

</script>
<body onload="loading()">
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_bpermintaan/cari/">
<table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<tr>
				<td nowrap>Pilihan</td>
				<td nowrap>:</td>
				<td nowrap>
				<input type="radio" checked="checked" name="opt" value="RT" <?php echo $cekrt; ?> > <b>Rekap per Transaksi</b>
				<input type="radio" name="opt" value="DT" <?php echo $cekdt; ?> > <b>Detail per Transaksi</b>
				</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
				<?php
				echo $reportlib->write_textbox_combo("Tanggal","tgl1","tgl2",$tgl1,$tgl2,"15","15","readonly='readonly'");			
				echo $reportlib->write_plain_combo("Gudang","gudang",$listgudang,$gudang,"KdGudang","Keterangan",3);
				echo $reportlib->write_plain_combo("Divisi","divisi",$listdivisi,$divisi,"KdDivisi","NamaDivisi",3);
				echo $reportlib->write_plain_combo("Barang","barang",$listbarang,$barang,"PCode","NamaLengkap",3);
				?>
				<td>Jenis Report</td>
				<td>:</td>
				<td>
				<input type="radio" checked="checked" name="opt1" value="F1" <?php echo $cekf1; ?> > <b>Open</b>
				<input type="radio" name="opt1" value="F0" <?php echo $cekf0; ?> > <b>Pending</b>
				<input type="radio" name="opt1" value="F2" <?php echo $cekf2; ?> > <b>Semua</b>
				</td>
			</table>
		</td>
	</tr>
	<tr bordercolor="#FFFFFF">
		<td colspan="2" align="center"><input type="button" value="Search (*)" onclick="submitThis()">
		<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
		<input type='hidden' value='<?=$userlevel?>' id="userlevel" name="userlevel">
		<input type='hidden' value='<?=$excel?>' id="excel" name="excel">
		<input type='hidden' value='<?=$print?>' id="print" name="print">
		</td>
	</tr>
</table>
</form>
</body>
<?php
if($tampilkanRT)
{
	$this->load->view("report/report_bpermintaan/reportRT");
}
if($tampilkanDT)
{
	$this->load->view("report/report_bpermintaan/reportDT");
}
$this->load->view('footer'); ?>