<?php
$this->load->view('header');
$reportlib = new report_lib();

?>

<script>

    function submitThis()
    {
		nostruk   = $("#v_struk_terakhir").val();

		if(nostruk != ""){
			$.ajax({
				url : "<?php echo base_url(); ?>index.php/report/cetakstruk_store/cek_struk",
				type : "POST",
				data : "nostruk="+nostruk,
				beforeSend : function(){
					$("#btn_search").html("<i class='entypo-search'></i> Loading ...");
				},
				success : function(res){

					if(res == "invalid"){
						$("#error_struk").show();
					}else{
						$('#search').submit();
					}

					$("#btn_search").html("<i class='entypo-search'></i> Cetak");
				},
				error : function(){
					alert("error request");
				}
			});

		}else{

			$('#search').submit();
		}
			
    }

</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Cetak Ulang Struk Store </strong></li>
		</ol>
		
		<form action="<?php echo base_url(); ?>index.php/report/cetakstruk_store/cetak/" method="POST" name="search" id="search">
		
	    <table class="table table-bordered responsive"> 
			<tr>
					<td class="title_table" width="150">Store</td>
					<td>
						<select class="form-control-new" name="v_store" id="v_store" style="width: 200px;">
							<?php 
								foreach ($store as $key => $val) {
									echo "<option value='".$val['KodeStore']."'>".$val['NamaStore']."</option>";
								}
								
							?>
		            		
		            	</select>
					</td>
	        </tr>  
	        <tr>
					<td class="title_table" width="150">Tanggal</td>
					<td>
						<input type="text" class="form-control-new datepicker" value="<?php echo date("d-m-Y") ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10"> S/D
						<input type="text" class="form-control-new datepicker" value="<?php echo date("d-m-Y") ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
					</td>
	        </tr>  
	        <tr>
					<td class="title_table" width="150">Struk Terakhir ( *opsi )</td>
					<td>
						<input type="text" class="form-control-new" value="" name="struk_terakhir" id="v_struk_terakhir" size="20" maxlength="20"> 
						<p style="color:red;display: none;" id="error_struk">nomor struk tidak valid</p>
					</td>
	        </tr> 
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Cetak<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	    <p style="color:red;">*update system 07/08/2019, cetak struk dengan range tanggal</p>
	</div>
</div>

<?php
$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>