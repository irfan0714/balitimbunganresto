<?php 

if(!$btn_excel)
{
	$this->load->view('header'); 

	$modul = "PR Non PO";
	
}

if($btn_excel)
{
	$file_name = "Report_pr_non_po.xls";
	        
	header("Content-Disposition".": "."attachment;filename=$file_name");
	header("Content-type: application/vnd.ms-excel");
}

if(!$btn_excel)
{
	

?>

<script>
function cekTheform()
{
	/*if(document.getElementById("v_start_date").value=="")
    {
        alert("Tanggal Awal harus diisi");
        document.getElementById("v_start_date").focus();
        return false;
    }*/
    
    document.getElementById("theform").submit();	
	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action="<?php echo base_url();?>index.php/report/report_pr_non_po/search_report">
		
	    <table class="table table-bordered responsive">     
	        
	        <tr>
	            <td class="title_table" width="150">Date</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	s/d
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            	
	            </td>
	        </tr>                 
	        
	        <!--<tr>
	            <td class="title_table">Supplier</td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="all">All</option>
	            		<?php
	            		foreach($msupplier as $val)
	            		{
		            		$selected="";
		            		if($v_supplier)
		            		{
								if($v_supplier==$val["KdSupplier"])
								{
									$selected = 'selected="selected"';
								}
							}
							
							?><option value="<?php echo $val["KdSupplier"]; ?>" <?php echo $selected; ?> ><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>-->          
	        
	        
	        <tr>
	        	 <td class="title_table">&nbsp;</td>
	            <td colspan="100%">
					<input type='hidden' name="flag" id="flag" value="analisa">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_analisa" id="btn_analisa"  value="Analisa">Analisa<i class="entypo-check"></i></button>
		        </td>
	        </tr>  
	    </table>
        
    	<?php
    	
}
    	if($flag || $btn_excel)
    	{
    		$mylib = new globallib();
    	
    	?>
    	
    	
    	<?php
    	if(!$btn_excel)
    	{
		?>
		<button type="submit" class="btn btn-green btn-icon btn-sm icon-left" name="btn_excel" id="btn_excel" value="Excel">Export To Excel<i class="entypo-download"></i></button>
    	<br><br>
		<?php	
		}
    	?>
    	
    	          <?php if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="13">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="13">REPORT PR Non PO</td>   
                            </tr>
                            
                            <tr>
                                <td colspan="13">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    } ?>
    	
    	
    	<?php
    	if($btn_excel)
    	{
			$table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
		}
		else
		{
			$table = '<table class="table table-bordered responsive">';	
		}
		
			echo $table;
			
    	?>
	    		<thead class="title_table">
	    			<tr>
	    				<th><center>No. PR</center></th>
	    				<th><center>No. PB</center></th>
	    				<th><center>Gudang</center></th>
	    				<th><center>Tgl. Dokumen</center></th>
	    				<th><center>Tgl. Kebutuhan</center></th>
	    				<th><center>Keterangan</center></th>
	    				<th><center>PCode</center></th>
	    				<th><center>Nama Barang</center></th>
	    				<th><center>Qty PB</center></th>
	    				<th><center>Qty PR</center></th>
	    				<th><center>Satuan</center></th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    			<?php
	    			if(count($header)==0)
	    			{
						echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
					}
					$NoDokumen_prev = "";
					$no = 0;
                    $no_echo = 0;
					foreach($header as  $val)
					{
					                    $no_echo = "";
                                        $NoDokumen_echo = "";
                                        $NoDokumenPB_echo = "";
                                        $TglDokumen_echo = "";
                                        $TglTerima_echo = "";
										$Ket_echo = "";
										$NamaGudang ="";
                                        
					               if($NoDokumen_prev!=$val['NoDokumen'])
                                    {
                                        $no++;
                                        $no_echo = $no;
                                        $NoDokumen_echo = $val['NoDokumen'];
                                        $NoDokumenPB_echo = $val['NoPermintaan'];
                                        $TglDokumen_echo = $val['TglDokumen_'];
                                        $TglTerima_echo = $val['TglTerima_'];
										$Ket_echo = $val['Keterangan'];
										$NamaGudang = $val['NamaGudang'];
                                    }		
					?>
						<tr>
							<td align="center"><?php echo $NoDokumen_echo ; ?></td>
							<td align="center"><?php echo $NoDokumenPB_echo ; ?></td>
							<td align="center"><?php echo $NamaGudang ; ?></td>
							<td align="center"><?php echo $TglDokumen_echo; ?></td>
							<td align="center"><?php echo $TglTerima_echo; ?></td>
							<td><?php echo $Ket_echo; ?></td>
							<td align="center"><?php echo $val['PCode']; ?></td>
							<td align="left"><?php echo $val['NamaLengkap']; ?></td>
							<td align="right"><?php echo $val['QtyPB']; ?></td>
							<td align="right"><?php echo $val['QtyPR']; ?></td>
							<td align="center"><?php echo $val['Satuan']; ?></td>
						</tr>
					<?php
					
						$NoDokumen_prev = $val['NoDokumen'];					
					}
	    			?>
	    		</tbody>
    			</table>
    	<?php	
			
						
		}
		
		
		if(!$btn_excel)
		{
    	?>
    	
	    </form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
function get_choose(nodokumen,type)
{
		base_url = $("#base_url").val();
		if(type=="Non PB"){
		url = base_url+"index.php/transaksi/purchase_request/pop_edit_form/"+nodokumen+"/0";
		}else{
		url = base_url+"index.php/transaksi/purchase_request/pop_edit_form/"+nodokumen+"/1";
		}
		windowOpener(1000, 1200, 'Detail Purchase Request', url, 'Detail Purchase Request')
	
}
</script>

<?php
}
?>