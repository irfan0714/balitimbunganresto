<?php
$this->load->view('header');
//$reportlib = new report_lib();

$modul = "Tour leader Pertanggal";
?>

<script>
    $(function () {
        $('#dtpicker').datepicker({
            format: 'MM yy',
            viewMode: "months",
            minViewMode: "months",
            toValue: "#alt_date",
            altFormat: "dd-MM-yy",
            autoClose: true
        });
        $('#dtpicker').datepicker().on('changeDate', function (e) {
            $('#alt_date').val(e.format('mm-yyyy'));
             $(this).datepicker('hide');
        });
    });

    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }

</script>
<div class="row">
    <div class="col-md-12" align="left">

        <ol class="breadcrumb">
            <li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
        </ol>

        <form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_tourleader/search_report/" onsubmit="return false">

            <table class="table table-bordered responsive">                        

                <tr>
                    <td width="150">Bulan</td>
                    <td> 
                        <input type="text" name="dtpicker" id="dtpicker" value="<?php echo $dtpicker;?>" class="form-control-new datepicker"/>
                        <input type="hidden" id="alt_date" name="alt_date" value="<?php echo $alt_date;?>"/>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                        <input type='hidden' value='<?= $print ?>' id="print" name="print">
                        <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                        <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
                    </td>
                </tr>

            </table>
        </form> 
    </div>
</div>

<?php
if ($tampilkanDT) {
    $this->load->view("report/analisa/tampil_omzet_tourleader");
}

$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>


