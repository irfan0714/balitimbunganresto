<?php 

if(!$btn_excel)
{
	$this->load->view('header'); 

	$modul = "BANQUET EVENT ORDER (BEO)";
	
}

if($btn_excel)
{
	$file_name = "Report_beo.xls";
	        
	header("Content-Disposition".": "."attachment;filename=$file_name");
	header("Content-type: application/vnd.ms-excel");
}

if(!$btn_excel)
{
	

?>

<script>
function cekTheform()
{
	   
    document.getElementById("theform").submit();	
	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action="<?php echo base_url();?>index.php/report/report_beo/search_report">
		
	    <table class="table table-bordered responsive">     
	        
	        <tr>
	            <td class="title_table" width="150">Bulan</td>
	            <td> 
	            	
	            	        <select class="form-control-new" name="bulan" id="bulan" style="width: 100px">
                                    <option   <?php if($bulan==""){ echo "selected='selected'"; } ?>value=""> -- Pilih -- </option>
                                    <option   <?php if($bulan=="01"){ echo "selected='selected'"; } ?> value="01">Januari</option>
                                    <option   <?php if($bulan=="02"){ echo "selected='selected'"; } ?> value="02">Februari</option>
                                    <option   <?php if($bulan=="03"){ echo "selected='selected'"; } ?> value="03">Maret</option>
                                    <option   <?php if($bulan=="04"){ echo "selected='selected'"; } ?> value="04">April</option>
                                    <option   <?php if($bulan=="05"){ echo "selected='selected'"; } ?> value="05">Mei</option>
                                    <option   <?php if($bulan=="06"){ echo "selected='selected'"; } ?> value="06">Juni</option>
                                    <option   <?php if($bulan=="07"){ echo "selected='selected'"; } ?> value="07">Juli</option>
                                    <option   <?php if($bulan=="08"){ echo "selected='selected'"; } ?> value="08">Agustus</option>
                                    <option   <?php if($bulan=="09"){ echo "selected='selected'"; } ?> value="09">Sepetember</option>
                                    <option   <?php if($bulan=="10"){ echo "selected='selected'"; } ?> value="10">Oktober</option>
                                    <option   <?php if($bulan=="11"){ echo "selected='selected'"; } ?> value="11">November</option>
                                    <option   <?php if($bulan=="12"){ echo "selected='selected'"; } ?> value="12">Desember</option>
                            </select>
	            	
	            </td>
	        </tr>                 
	        
	        <tr>
	            <td class="title_table" width="150">Tahun</td>
	            <td> 
	            	       <select class="form-control-new" name="tahun" id="tahun" style="width: 100px">
                                    <option  <?php if($tahun==""){ echo "selected='selected'"; } ?> value=""> -- Pilih -- </option>
                                    <option  <?php if($tahun=="2017"){ echo "selected='selected'"; } ?> value="2017">2017</option>
                                    <option  <?php if($tahun=="2018"){ echo "selected='selected'"; } ?> value="2018">2018</option>
                                    <option  <?php if($tahun=="2019"){ echo "selected='selected'"; } ?> value="2019">2019</option>
                                    <option  <?php if($tahun=="2020"){ echo "selected='selected'"; } ?> value="2020">2020</option>
                            </select>	            	
	            </td>
	        </tr> 
	        
	        <tr>
	            <td class="title_table">Travel </td>
	            <td> 
	            	<select class="form-control-new" name="v_travel" id="v_travel" style="width: 200px;">
	            		<option value=""> All </option>
	            		<?php
	            		foreach($v_tourtravel as $val)
	            		{
	            			$selected="";
							if($travel==$val["KdTravel"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        
	         <tr>
	            <td class="title_table" width="150">Status</td>
	            <td> 
	            	
	            	    <select class="form-control-new" name="v_status" id="v_status" style="width: 100px;">
		            		<option <?php if($status_konfirmasi==0){ echo "selected='selected'"; } ?> value="0">All</option>
		            		<option <?php if($status_konfirmasi==1){ echo "selected='selected'"; } ?> value="1">Confim / Fix BEO</option>
		            		<option <?php if($status_konfirmasi==2){ echo "selected='selected'"; } ?> value="2">No Confirm / Booking</option>
		            	</select>
	            	
	            </td>
	        </tr> 
	        
	        
	        <tr>
	            <td class="title_table" width="150">Sort By</td>
	            <td> 
	            	
	            	    <select class="form-control-new" name="v_sort" id="v_sort" style="width: 100px;">
		            		<option <?php if($status_sort==1){ echo "selected='selected'"; } ?> value="1">No. BEO</option>
		            		<option <?php if($status_sort==2){ echo "selected='selected'"; } ?> value="2">Tanggal</option>
		            	</select>
	            	
	            </td>
	        </tr>        
	        
	        
	        <tr>
	        	 <td class="title_table">&nbsp;</td>
	            <td colspan="100%">
					<input type='hidden' name="flag" id="flag" value="analisa">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_analisa" id="btn_analisa"  value="Analisa">Analisa<i class="entypo-check"></i></button>
		        </td>
	        </tr>  
	    </table>
        
    	<?php
    	
}
    	if($flag || $btn_excel)
    	{
    		$mylib = new globallib();
    	
    	?>
    	
    	
    	<?php
    	if(!$btn_excel)
    	{
		?>
		<button type="submit" class="btn btn-green btn-icon btn-sm icon-left" name="btn_excel" id="btn_excel" value="Excel">Export To Excel<i class="entypo-download"></i></button>
    	<br><br>
		<?php	
		}
    	?>
    	
    	          <?php if($btn_excel)
                    {
                        ?>
                        <table style="font-weight: bold;">
                            <tr>
                                <td colspan="13">PT. NATURA PESONA MANDIRI</td>   
                            </tr>
                            <tr>
                                <td colspan="13">REPORT BANQUET EVENT ORDER (BEO) bulan <?php echo $bulan; ?> tahun <?php echo $tahun; ?></td>   
                            </tr>
                            
                            <tr>
                                <td colspan="13">&nbsp;</td>   
                            </tr>
                        </table>
                        <?php
                    } ?>
    	
    	
    	<?php
    	if($btn_excel)
    	{
			$table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
		}
		else
		{
			$table = '<table class="table table-bordered responsive">';	
		}
		
			echo $table;
			
    	?>
	    		<thead class="title_table">
	    			<tr>
	    				<th><center>No. BEO</center></th>
	    				<th><center>Tgl. Kedatangan</center></th>
	    				<th><center>Jam Kedatangan</center></th>
	    				<th><center>Tour And Travel</center></th>
	    				<th><center>Group Code</center></th>
	    				<th><center>Kota</center></th>
	    				<th><center>Tour</center></th>
	    				<th><center>Lounch</center></th>
	    				<th><center>Dinner</center></th>
	    				<th><center>PIC Rombongan</center></th>
	    				<th><center>PIC SGV</center></th>
	    				<th><center>Keterangan</center></th>
	    				<th><center>Status</center></th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    			<?php
	    			if(count($header)==0)
	    			{
						echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
					}
					foreach($header as  $val)
					{
						/*if($val['status_konfirmasi']=="0"){
							$sts = "No Confirm";
						}else{
							$sts = "Confirm";
						}*/
						
						if(substr($val['NoDokumen'], 0, 3)!="SGV"){
							$sts = "No Confirm";
						}else{
							$sts = "Confirm";
						}
						
						if($val['NoDokumen']==""){
							$dok = "Booking";
						}else{
							$dok = $val['NoDokumen'];
						}
						
						//tour only
						if($val['Event1']=="10" OR $val['Event2']=="10" OR $val['Event3']=="10" OR $val['Event4']=="10" OR $val['Event5']=="10"){
						$tour = $val['Participants'];
						$lounch = "";
						$dinner = "";
						//lounch only
						}else if($val['Event1']=="4" OR $val['Event2']=="4" OR $val['Event3']=="4" OR $val['Event4']=="4" OR $val['Event5']=="4"){
						$tour = "";
						$lounch = $val['Participants'];
						$dinner = "";
						//dinner only
						}else if($val['Event1']=="5" OR $val['Event2']=="5" OR $val['Event3']=="5" OR $val['Event4']=="5" OR $val['Event5']=="5"){
						$tour = "";
						$lounch = "";
						$dinner = $val['Participants'];
						//tour and lounch
						}else if($val['Event1']=="11" OR $val['Event2']=="11" OR $val['Event3']=="11" OR $val['Event4']=="11" OR $val['Event5']=="11"){
						$tour = $val['Participants'];
						$lounch = $val['Participants'];
						$dinner = "";
						//tour and dinner
						}else if($val['Event1']=="12" OR $val['Event2']=="12" OR $val['Event3']=="12" OR $val['Event4']=="12" OR $val['Event5']=="12"){
						$tour = $val['Participants'];
						$lounch = "";
						$dinner = $val['Participants'];
						}else{
						$tour = "";
						$lounch = "";
						$dinner = "";
						}
					?>
						<tr>
							<td align="center"><?php echo $dok ; ?></td>
							<td align="center"><?php echo $val['Tgl_Kedatangan'] ; ?></td>
							<td align="center"><?php echo $val['Jam_Kedatangan']; ?></td>
							<td align="center"><?php echo $val['Nama']; ?></td>
							<td><?php echo $val['GroupCode']; ?></td>
							<td align="center"><?php echo $val['Nationality']; ?></td>
							<td align="center"><?php echo $tour; ?></td><!-- tour -->
							<td align="center"><?php echo $lounch; ?></td><!-- lounch -->
							<td align="center"><?php echo $dinner; ?></td><!-- dinner -->
							<td align="center"><?php echo $val['Contact']; ?></td>
							<td align="center"><?php echo $val['NamaSalesman']; ?></td>
							<td align="justify"><?php echo $val['Ket1']." ".$val['Ket2']." ".$val['Ket3']." ".$val['Ket4']; ?></td>
							<td align="center"><?php echo $sts; ?></td>
						</tr>
					<?php
										
					}
	    			?>
	    		</tbody>
    			</table>
    	<?php	
			
						
		}
		
		
		if(!$btn_excel)
		{
    	?>
    	
	    </form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>


<?php
}
?>