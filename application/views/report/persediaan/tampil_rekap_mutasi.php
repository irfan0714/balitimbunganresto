<?php
$mylib = new globallib();


//echo "<pre>";
//print_r($hasil);
//die();
//echo "</pre>";
?>

<style>
    .link_data{
        text-decoration: underline;
        font-size: normal;
    }

    .link_data:hover{
        text-decoration: none;
    }
</style>

<script>

//     document.onreadystatechange = function () {
//        if (document.readyState === 'complete') {
//             alert("tes");
//        }
//    }
    $(window).bind("load", function () {
        $('#theModal').modal('hide');
    });

    function loadModal() {
        $('#pleaseWaitDialog').modal();
    }

</script>
<div class="row">
    <div class="col-md-12" align="left">
<!--        <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_tourleader/search_report/" onsubmit="return false">-->
        <?php
        if ($excel == "excel") {    	
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Rekap Mutasi Barang.xls"');
        }
        if ($excel != "excel") {
            ?>
            <span style="margin-bottom:10px; display: inline-table;">
                <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                            document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
            </span>
            <?php
        }
        ?>

        <br/>

        <ol class="breadcrumb">
            <li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
        </ol>

        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <table class="table table-bordered responsive table-hover">
                <thead>
                    <tr>
                        <th rowspan="2" width="30" style="vertical-align: middle; text-align: center;">Divisi</th>
                        <th rowspan="2" width="30" style="vertical-align: middle; text-align: center;">Sub Divisi</th>
                        <th rowspan="2" width="30" style="vertical-align: middle; text-align: center;">Rekening</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Saldo Awal</th>
                        <th colspan="5" style="vertical-align: middle; text-align: center;">Masuk</th>
                        <th colspan="4" style="vertical-align: middle; text-align: center;">Keluar</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Stock Opname</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Saldo Akhir</th>
                    </tr>
                    <tr>
                    	<th style="vertical-align: middle; text-align: center;">Pembelian</th>
                        <th style="vertical-align: middle; text-align: center;">Produksi</th>
                        <th style="vertical-align: middle; text-align: center;">Sales Return</th>
                        <th style="vertical-align: middle; text-align: center;">Penerimaan</th>
                        <th style="vertical-align: middle; text-align: center;">Paket</th>
                        
                        <th style="vertical-align: middle; text-align: center;">Pengeluaran</th>
                        <th style="vertical-align: middle; text-align: center;">Paket</th>
                        <th style="vertical-align: middle; text-align: center;">Produksi</th>
                        <th style="vertical-align: middle; text-align: center;">Sales</th>
                    </tr>
                </thead>
                <tbody>
                <?php
            		$ttl_R = 0;
					$ttl_RG = 0;
					$ttl_MM_PL = 0;
					$ttl_MC_DL = 0;
					$ttl_SO = 0;
					$ttl_MX = 0;
					$ttl_MP_IN = 0;
					$ttl_MP_OUT = 0;
					$ttl_FG = 0;
					$ttl_RB_SR = 0;
					$ttl_awal = 0;
					$ttl_akhir = 0;
					
					$stl_R = 0;
					$stl_RG = 0;
					$stl_MM_PL = 0;
					$stl_MC_DL = 0;
					$stl_SO = 0;
					$stl_MX = 0;
					$stl_MP_IN = 0;
					$stl_MP_OUT = 0;
					$stl_FG = 0;
					$stl_RB_SR = 0;
					$stl_awal = 0;
					$stl_akhir = 0;
                    
                    $prev_namadivisi = '';  
                   	foreach($detail as $rec){
                        $namadivisi = $rec['NamaDivisi'];
                        $namasubdivisi = $rec['NamaSubDivisi'];
                        $kdrekening = $rec['KdRekeningPersediaan'];
                        $namarekening = $rec['NamaRekening'];
                        $kdsubdivisi = $rec['KdSubDivisi'];
                        
                        $jml_R = $rec['jml_R'];
						$jml_RG = $rec['jml_RG'];
						$jml_MM_PL = $rec['jml_MM_PL'];
						$jml_MC_DL = $rec['jml_MC_DL'];
						$jml_SO = $rec['jml_SO'];
						$jml_MX = $rec['jml_MX'];
						$jml_MP_IN = $rec['jml_MP_IN'];
						$jml_MP_OUT = $rec['jml_MP_OUT'];
						$jml_FG = $rec['jml_FG'];
						$jml_RB_SR = $rec['jml_RB_SR'];
						$saldo_awal = $sawal[$kdsubdivisi][$kdrekening];
                        
                        	
						$saldoakhir = $saldo_awal+$jml_RG+$jml_FG+$jml_RB_SR+$jml_MC_DL+$jml_MP_IN
									  -$jml_MM_PL-$jml_MX-$jml_R-$jml_MP_OUT+$jml_SO;
									  
						if($prev_namadivisi!=$namadivisi and $prev_namadivisi!=''){
				?>
							<tr>
			                	<td nowrap align="center" colspan="3"><?= 'Sub Total ' .$prev_namadivisi ;?></td>
			                    <td nowrap align="right"><?php echo number_format($stl_awal,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_RG,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_FG,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_RB_SR,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_MC_DL,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_MP_IN,0,',','.'); ?></td>
							
								<td nowrap align="right"><?php echo number_format($stl_MM_PL,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_MP_OUT,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_MX,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_R,0,',','.'); ?></td>
			                    
								<td nowrap align="right"><?php echo number_format($stl_SO,0,',','.'); ?></td>
								<td nowrap align="right"><?php echo number_format($stl_akhir,0,',','.'); ?></td>
			                </tr>		
		        <?php
				        	$stl_R = 0;
							$stl_RG = 0;
							$stl_MM_PL = 0;
							$stl_MC_DL = 0;
							$stl_SO = 0;
							$stl_MX = 0;
							$stl_MP_IN = 0;
							$stl_MP_OUT = 0;
							$stl_FG = 0;
							$stl_RB_SR = 0;
							$stl_awal = 0;
							$stl_akhir = 0;
	                    
						}
				?>
						<tr>
	                        <td nowrap><?= $namadivisi ;?></td>
	                        <td nowrap><?= $namasubdivisi ;?></td>
	                        <td nowrap><?= $kdrekening.'-'.$namarekening ;?></td>
	                        <td nowrap align="right"><?php echo number_format($saldo_awal,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_RG,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_FG,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_RB_SR,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MC_DL,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MP_IN,0,',','.'); ?></td>
						
							<td nowrap align="right"><?php echo number_format($jml_MM_PL,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MP_OUT,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MX,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_R,0,',','.'); ?></td>
                            
							<td nowrap align="right"><?php echo number_format($jml_SO,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($saldoakhir,0,',','.'); ?></td>
                    	</tr>
                <?php  
                	$stl_R += $jml_R;
					$stl_RG += $jml_RG;
					$stl_MM_PL += $jml_MM_PL;
					$stl_MC_DL += $jml_MC_DL;
					$stl_SO += $jml_SO;
					$stl_MX += $jml_MX;
					$stl_MP_IN += $jml_MP_IN;
					$stl_MP_OUT += $jml_MP_OUT;
					$stl_FG += $jml_FG;
					$stl_RB_SR += $jml_RB_SR;
					$stl_awal += $saldo_awal;
                    $stl_akhir += $saldoakhir;
                    
                	$ttl_R += $jml_R;
					$ttl_RG += $jml_RG;
					$ttl_MM_PL += $jml_MM_PL;
					$ttl_MC_DL += $jml_MC_DL;
					$ttl_SO += $jml_SO;
					$ttl_MX += $jml_MX;
					$ttl_MP_IN += $jml_MP_IN;
					$ttl_MP_OUT += $jml_MP_OUT;
					$ttl_FG += $jml_FG;
					$ttl_RB_SR += $jml_RB_SR;
					$ttl_awal += $saldo_awal;
                    $ttl_akhir += $saldoakhir;
                    $prev_namadivisi = $namadivisi;
                }
                ?>
    			<tr>
                	<td nowrap align="center" colspan="3"><?= 'Sub Total ' .$prev_namadivisi ;?></td>
                    <td nowrap align="right"><?php echo number_format($stl_awal,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_RG,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_FG,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_RB_SR,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_MC_DL,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_MP_IN,0,',','.'); ?></td>
				
					<td nowrap align="right"><?php echo number_format($stl_MM_PL,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_MP_OUT,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_MX,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_R,0,',','.'); ?></td>
                    
					<td nowrap align="right"><?php echo number_format($stl_SO,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($stl_akhir,0,',','.'); ?></td>
                </tr>		
                <tr>
                	<td nowrap align="center" colspan="3">Total</td>
                    <td nowrap align="right"><?php echo number_format($ttl_awal,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_RG,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_FG,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_RB_SR,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_MC_DL,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_MP_IN,0,',','.'); ?></td>
				
					<td nowrap align="right"><?php echo number_format($ttl_MM_PL,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_MP_OUT,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_MX,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_R,0,',','.'); ?></td>
                    
					<td nowrap align="right"><?php echo number_format($ttl_SO,0,',','.'); ?></td>
					<td nowrap align="right"><?php echo number_format($ttl_akhir,0,',','.'); ?></td>
                </tr>
                <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php

function ubah_format($harga) {
    $s = number_format($harga, 2, ',', '.');
    return $s;
}
?>
