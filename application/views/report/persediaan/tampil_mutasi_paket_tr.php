<?php
$mylib = new globallib();


//echo "<pre>";
//print_r($hasil);
//die();
//echo "</pre>";
?>

<style>
    .link_data{
        text-decoration: underline;
        font-size: normal;
    }

    .link_data:hover{
        text-decoration: none;
    }
</style>

<script>

//     document.onreadystatechange = function () {
//        if (document.readyState === 'complete') {
//             alert("tes");
//        }
//    }
    $(window).bind("load", function () {
        $('#theModal').modal('hide');
    });

    function loadModal() {
        $('#pleaseWaitDialog').modal();
    }

    function PopUpVoucher(id)
    {
        base_url = "<?php echo base_url(); ?>";
        url = base_url + "index.php/report/report_kasir/pop_up_detail_voucher/index/" + id + "/";
        windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>
<div class="row">
    <div class="col-md-12" align="left">
<!--        <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_tourleader/search_report/" onsubmit="return false">-->
        <?php
        if ($excel == "excel") {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Report_Mutasi_Paket.xls"');
        }
        if ($excel != "excel") {
            ?>
            <span style="margin-bottom:10px; display: inline-table;">
                <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                            document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
            </span>
            <?php
        }
        ?>

        <br/>

        <ol class="breadcrumb">
            <li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
        </ol>

        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <table class="table table-bordered responsive table-hover">
                <thead>
                    <tr>
                        <th width="30" style="vertical-align: middle; text-align: center;">No</th>
                        <th width="30" style="vertical-align: middle; text-align: center;">ID Mutasi</th>
                        <th style="vertical-align: middle; text-align: center;">Tanggal</th>
                        <th style="vertical-align: middle; text-align: center;">Kode Paket</th>
                        <th style="vertical-align: middle; text-align: center;">Nama Paket</th>
                        <th style="vertical-align: middle; text-align: center;">Sub Divisi</th>
                        <th style="vertical-align: middle; text-align: center;">Jenis</th>
                        <th style="vertical-align: middle; text-align: center;">Qty</th>
                        <th style="vertical-align: middle; text-align: center;">Gudang</th>
                        <th style="vertical-align: middle; text-align: center;">Keterangan</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (count($hasil) == 0) {
                        echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
                    }

                    $no = 1;
                    foreach ($hasil as $key => $value) {
                        ?>
                        <tr>
                            <td nowrap align="center"> <?php echo $no; ?></td>
                            <td nowrap> <?php echo $value['id_mutasi']; ?></td>
                            <td nowrap> &nbsp;<?php echo $value['Tanggal']; ?></td>
                            <td nowrap> <?php echo $value['MPCode']; ?></td>
                            <td nowrap> <?php echo $value['NamaPaket']; ?></td>
                            <td nowrap> <?php echo $value['NamaDivisi']; ?></td>
                            <td nowrap> <?php echo $value['Jenis_mutasi']=='B' ? 'Buah Paket' : 'Pecah Paket'; ?></td>
                            <td nowrap><?php echo $value['Qty']; ?></td>
                            <td nowrap><?php echo $value['namagudang']; ?></td>
                            <td nowrap><?php echo $value['Keterangan']; ?></td>
                            <?php
                            $no++;
                        }
                        ?>

                    </tr>
                </tbody>
            </table>
        </div>

        <!--        </form>-->
    </div>
</div>
<?php

function ubah_format($harga) {
    $s = number_format($harga, 2, ',', '.');
    return $s;
}
?>