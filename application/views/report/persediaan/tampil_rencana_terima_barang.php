<?php
$mylib = new globallib();


/*echo "<pre>";
print_r($hasil_cari);
die();
echo "</pre>";*/
?>

<style>
    .link_data{
        text-decoration: underline;
        font-size: normal;
    }

    .link_data:hover{
        text-decoration: none;
    }
</style>

<script>

//     document.onreadystatechange = function () {
//        if (document.readyState === 'complete') {
//             alert("tes");
//        }
//    }
    $(window).bind("load", function () {
        $('#theModal').modal('hide');
    });

    function loadModal() {
        $('#pleaseWaitDialog').modal();
    }

    function PopUpVoucher(id)
    {
        base_url = "<?php echo base_url(); ?>";
        url = base_url + "index.php/report/report_kasir/pop_up_detail_voucher/index/" + id + "/";
        windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>
<div class="row">
    <div class="col-md-12" align="left">
        <?php
        if ($excel == "excel") {    	
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Report_rencana_terima_barang.xls"');
        }
        if ($excel != "excel") {
            ?>
            <span style="margin-bottom:10px; display: inline-table;">
                <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                            document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
            </span>
            <?php
        }
        ?>

        <br/>

        <ol class="breadcrumb">
            <li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
        </ol>

        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <table border="1" class="table table-bordered responsive table-hover">
                <tr>
	    				<th rowspan="2" align="center">Supplier</th>
	    				<th rowspan="2" align="center">No PO</th>
	    				<th rowspan="2" align="center">Tanggal PO</th>
	    				<th rowspan="2" align="center">PCode</th>
	    				<th rowspan="2" align="center">Nama Barang</th>
	    				<th colspan="2" align="center">PO</th>
	    				<th colspan="2" align="center" >Gudang</th>
	    				<th rowspan="2" align="center">Qty Terima</th>
	    		</tr>
	    		<tr>
	    			<th align="center">Qty</th>
	    			<th align="center">Satuan</th>
	    			<th align="center">Qty</th>
	    			<th align="center">Satuan</th>
	    		</tr>
	    		</thead>
                <tbody>
                <?php
					foreach ($hasil_cari as $key => $val) { 
				?>
                        <tr>
                        		<td><?php echo $val['KdSupplier']." :: ".$val['Nama'];?></td>
								<td><?php echo $val['NoDokumen'];?></td>
			    				<td><?php echo $val['TglDokumen'];?></td>
			    				<td><?php echo $val['PCode'];?></td>
			    				<td><?php echo $val['NamaLengkap'];?></td>
			    				<td align="right"><?php echo $val['Qty'];?></td>
			    				<td><?php echo $val['Satuan'];?></td>
			    				<td align="right"><?php echo $val['QtyPcs'];?></td>
			    				<td><?php echo $val['SatuanSt'];?></td>
			    				<td>&nbsp;</td>						
							</tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

        <!--        </form>-->
    </div>
</div>
<?php

?>