<?php
$mylib = new globallib();


//echo "<pre>";
//print_r($hasil);
//die();
//echo "</pre>";
?>

<style>
    .link_data{
        text-decoration: underline;
        font-size: normal;
    }

    .link_data:hover{
        text-decoration: none;
    }
</style>

<script>

//     document.onreadystatechange = function () {
//        if (document.readyState === 'complete') {
//             alert("tes");
//        }
//    }
    $(window).bind("load", function () {
        $('#theModal').modal('hide');
    });

    function loadModal() {
        $('#pleaseWaitDialog').modal();
    }

    function PopUpVoucher(id)
    {
        base_url = "<?php echo base_url(); ?>";
        url = base_url + "index.php/report/report_kasir/pop_up_detail_voucher/index/" + id + "/";
        windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>
<div class="row">
    <div class="col-md-12" align="left">
<!--        <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_tourleader/search_report/" onsubmit="return false">-->
        <?php
        if ($excel == "excel") {    	
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Report_Mutasi_Barang.xls"');
        }
        if ($excel != "excel") {
            ?>
            <span style="margin-bottom:10px; display: inline-table;">
                <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                            document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
            </span>
            <?php
        }
        ?>

        <br/>

        <ol class="breadcrumb">
            <li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
        </ol>

        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <table class="table table-bordered responsive table-hover">
                <thead>
                    <tr>
                        <th rowspan="3" width="30" style="vertical-align: middle; text-align: center;">Kode</th>
                        <th rowspan="3" width="30" style="vertical-align: middle; text-align: center;">Nama Barang</th>
                        <th rowspan="3" style="vertical-align: middle; text-align: center;">Satuan</th>
                        
                    	<th colspan="14" style="vertical-align: middle; text-align: center;">Quantity</th>
                    	<th colspan="14" style="vertical-align: middle; text-align: center;">Value</th>
                    </tr>
                    <tr>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Saldo Awal</th>
                        <th colspan="5" style="vertical-align: middle; text-align: center;">Masuk</th>
                        <th colspan="5" style="vertical-align: middle; text-align: center;">Keluar</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Stock Opname</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Adjustment</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Saldo Akhir</th>
                        
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Saldo Awal</th>
                        <th colspan="5" style="vertical-align: middle; text-align: center;">Masuk</th>
                        <th colspan="5" style="vertical-align: middle; text-align: center;">Keluar</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Stock Opname</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Adjustment</th>
                        <th rowspan="2" style="vertical-align: middle; text-align: center;">Saldo Akhir</th>
                    </tr>
                    
                    <tr>
                    
                    	<th style="vertical-align: middle; text-align: center;">Pembelian</th>
                        <th style="vertical-align: middle; text-align: center;">Produksi</th>
                        <th style="vertical-align: middle; text-align: center;">Sales Return</th>
                        <th style="vertical-align: middle; text-align: center;">Penerimaan</th>
                        <th style="vertical-align: middle; text-align: center;">Paket</th>
                        
                        <th style="vertical-align: middle; text-align: center;">Pengeluaran</th>
                        <th style="vertical-align: middle; text-align: center;">Paket</th>
                        <th style="vertical-align: middle; text-align: center;">Retur Beli</th>
                        <th style="vertical-align: middle; text-align: center;">Produksi</th>
                        <th style="vertical-align: middle; text-align: center;">Sales</th>
                        
                        <th style="vertical-align: middle; text-align: center;">Pembelian</th>
                        <th style="vertical-align: middle; text-align: center;">Produksi</th>
                        <th style="vertical-align: middle; text-align: center;">Sales Return</th>
                        <th style="vertical-align: middle; text-align: center;">Penerimaan</th>
                        <th style="vertical-align: middle; text-align: center;">Paket</th>
                        
                        <th style="vertical-align: middle; text-align: center;">Pengeluaran</th>
                        <th style="vertical-align: middle; text-align: center;">Paket</th>
                        <th style="vertical-align: middle; text-align: center;">Retur Beli</th>
                        <th style="vertical-align: middle; text-align: center;">Produksi</th>
                        <th style="vertical-align: middle; text-align: center;">Sales</th>
                        
                        
                        
                    </tr>
                    
                </thead>
                <tbody>
                <?php
                   foreach($list_KdKategori as $KdKategori=>$val)
                            {
                            	
                            	$NamaKategori = $NamaKategoris[$KdKategori];
                                ?>
                                    <tr>
                                        <td colspan="100%" style="font-weight: bold;"><?php echo $NamaKategori; ?></td>
                                    </tr>
                            	<?php
                
                
                
					foreach($Kategori_PCode[$KdKategori] as $PCode=> $val)
                        {
                        	
                        $NamaLengkap = $NamaLengkaps[$PCode];
                        $satuan = $satuans[$PCode];
                        $saldo_awal = $list_saldo_awal[$PCode];	
                        $jml_R = $list_jml_R[$PCode];
						$jml_RG = $list_jml_RG[$PCode];
						$jml_MM_PL = $list_jml_MM_PL[$PCode];
						$jml_MC_DL = $list_jml_MC_DL[$PCode];
						$jml_SO = $list_jml_SO[$PCode];
						$jml_MX = $list_jml_MX[$PCode];
						$jml_MP_IN = $list_jml_MP_IN[$PCode];
						$jml_MP_OUT = $list_jml_MP_OUT[$PCode];
						$jml_FG = $list_jml_FG[$PCode];
						$jml_RB = $list_jml_RB[$PCode];
                        $jml_SR = $list_jml_SR[$PCode];
						$jml_ADJ = $list_jml_ADJ[$PCode];
						
						$saldo_awal_val = $list_saldo_awal_val[$PCode];	
                        $jml_R_val = $list_jml_R_val[$PCode];
						$jml_RG_val = $list_jml_RG_val[$PCode];
						$jml_MM_PL_val = $list_jml_MM_PL_val[$PCode];
						$jml_MC_DL_val = $list_jml_MC_DL_val[$PCode];
						$jml_SO_val = $list_jml_SO_val[$PCode];
						$jml_MX_val = $list_jml_MX_val[$PCode];
						$jml_MP_IN_val = $list_jml_MP_IN_val[$PCode];
						$jml_MP_OUT_val = $list_jml_MP_OUT_val[$PCode];
						$jml_FG_val = $list_jml_FG_val[$PCode];
						$jml_RB_val = $list_jml_RB_val[$PCode];
                        $jml_SR_val = $list_jml_SR_val[$PCode];
						$jml_ADJ_val = $list_jml_ADJ_val[$PCode];
                        
                        	
						$saldoakhir = $saldo_awal+$jml_RG+$jml_FG+(-$jml_RB)+$jml_MC_DL+$jml_MP_IN
									  -$jml_MM_PL-$jml_MX-$jml_R-$jml_MP_OUT+$jml_SR+$jml_SO+$jml_ADJ;
						$saldoakhir_val = $saldo_awal_val+$jml_RG_val+$jml_FG_val+(-$jml_RB_val)+$jml_MC_DL_val+$jml_MP_IN_val
									  -$jml_MM_PL_val-$jml_MX_val-$jml_R_val-$jml_MP_OUT_val+$jml_SR_val+$jml_SO_val+$jml_ADJ_val;
									  
                        $cektransaksi = abs($saldo_awal)+abs($jml_RG+$jml_FG)+abs($jml_RB)+abs($jml_MC_DL)+abs($jml_MP_IN)
                                      +abs($jml_MM_PL)+abs($jml_MX+$jml_R)+abs($jml_MP_OUT)+abs($jml_SR)+abs($jml_SO)+abs($jml_ADJ)+abs($saldoakhir);
				        if($cektransaksi>0){ 
				       	
				        ?>
						<?php  if($search_by=='gudang'){?>
                        <tr data-toggle="tooltip" data-placement="top" data-original-title="Klik Untuk Detail" onclick="get_choose('<?php echo $PCode; ?>','<?php echo $bulan; ?>','<?php echo $tahun;?>','gdg','<?php echo $gudang; ?>')" >
                        <?php }else{?>
						<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik Untuk Detail" onclick="get_choose('<?php echo $PCode; ?>','<?php echo $bulan; ?>','<?php echo $tahun;?>','dvs','<?php echo $divisi; ?>')" >	
						<?php } ?>   
                            <td nowrap align="center"><?php echo $PCode; ?></td>
                            <td nowrap><?php echo $NamaLengkap; ?></td>
                            <td nowrap align="right"><?php echo $satuan; ?></td>
                            <td nowrap align="right"><?php echo number_format($saldo_awal,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_RG,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_FG,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_SR,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MC_DL,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MP_IN,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MM_PL,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MP_OUT,0,',','.'); ?></td>
                            <td nowrap align="right"><?php echo number_format($jml_RB,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MX,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_R,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_SO,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_ADJ,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($saldoakhir,0,',','.'); ?></td>
							
							<td nowrap align="right"><?php echo number_format($saldo_awal_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_RG_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_FG_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_SR_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MC_DL_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MP_IN_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MM_PL_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MP_OUT_val,0,',','.'); ?></td>
                            <td nowrap align="right"><?php echo number_format($jml_RB_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_MX_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_R_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_SO_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($jml_ADJ_val,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($saldoakhir_val,0,',','.'); ?></td>
                    </tr>
                <?php } 
                }
                ?>
                <?php }?>
                <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php

function ubah_format($harga) {
    $s = number_format($harga, 2, ',', '.');
    return $s;
}
?>

<script>
function get_choose(pcode,bln,thn,tipe,filed)
{
		base_url = $("#base_url").val();
		url = base_url+"index.php/pop/pop_up_mutasi_barang/index/"+pcode+"/"+bln+"/"+thn+"/"+filed+"/"+tipe+"/";
		windowOpener(600, 600, 'Detail Mutasi Barang', url, 'Detail Mutasi Barang')
	
}
</script>