<?php
$this->load->view('header');
//$reportlib = new report_lib();

$modul = "Mutasi Paket";
?>

<script>


    $(function () {
        $('#dtpicker1').datepicker({
            format: 'dd-mm-yyyy',
            autoClose: true
        });
        $('#dtpicker1').datepicker().on('changeDate', function (e) {
            $(this).datepicker('hide');
        });

        $('#dtpicker2').datepicker({
            format: 'dd-mm-yyyy',
            autoClose: true
        });
        $('#dtpicker2').datepicker().on('changeDate', function (e) {
            $(this).datepicker('hide');
        });

    });

    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");

        document.getElementById("search").submit();
        var interval = setInterval(function () {
            $('#pleaseWaitDialog').modal();
        }, 100);


//        jQuery(window).load(function () {
//            clearInterval(interval);
//            close();
//        });

    }

</script>
<div class="row">
    <div class="col-md-12" align="left">
        <ol class="breadcrumb">
            <li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
        </ol>

        <form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_mutasipaket/search_report/" onsubmit="return false" data-target="#pleaseWaitDialog1">

            <table class="table table-bordered responsive">                        
                <tr>
                    <td width="150">Pilihan</td>
                    <td> 
                        <input type="radio" name="opt" value="TR" <?php echo $cektr; ?> > <b>Per Transaksi</b>
                        <input type="radio" name="opt" value="DT" <?php echo $cekdt; ?> > <b>Per Detail</b>
                    </td>
                </tr>

                <tr>
                    <td width="150">Periode</td>
                    <td> 
                        <input type="text" name="dtpicker1" id="dtpicker1" value="<?php echo $dtpicker1; ?>" class="form-control-new datepicker"/>
                        <input type="text" name="dtpicker2" id="dtpicker2" value="<?php echo $dtpicker2; ?>" class="form-control-new datepicker"/>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                        <input type='hidden' value='<?= $print ?>' id="print" name="print">
                        <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                        <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
                    </td>
                </tr>

            </table>


            <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1>Processing...</h1>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form> 
        <?php
        if ($tampilkanTR) {
            $this->load->view("report/persediaan/tampil_mutasi_paket_tr");
        }
        if ($tampilkanDT) {
            $this->load->view("report/persediaan/tampil_mutasi_paket_dt");
        }
        ?>
    </div>
</div>




<?php
$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>


