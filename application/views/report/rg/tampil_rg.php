<?php
$mylib = new globallib();


//echo "<pre>";
//print_r($hasil);
//die();
//echo "</pre>";
?>

<style>
    .link_data{
        text-decoration: underline;
        font-size: normal;
    }

    .link_data:hover{
        text-decoration: none;
    }
</style>

<script>

//     document.onreadystatechange = function () {
//        if (document.readyState === 'complete') {
//             alert("tes");
//        }
//    }
    $(window).bind("load", function () {
        $('#theModal').modal('hide');
    });

    function loadModal() {
        $('#pleaseWaitDialog').modal();
    }

    function PopUpVoucher(id)
    {
        base_url = "<?php echo base_url(); ?>";
        url = base_url + "index.php/report/report_kasir/pop_up_detail_voucher/index/" + id + "/";
        windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>
<div class="row">
    <div class="col-md-12" align="left">
<!--        <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_tourleader/search_report/" onsubmit="return false">-->
        <?php
        if ($excel == "excel") {    	
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Report_Mutasi_Barang.xls"');
        }
        if ($excel != "excel") {
            ?>
            <span style="margin-bottom:10px; display: inline-table;">
                <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                            document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
            </span>
            <?php
        }
        ?>

        <br/>

        <ol class="breadcrumb">
            <li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
        </ol>

        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <table class="table table-bordered responsive table-hover">
                <thead>
                    <tr>
						 <th width="20" style="vertical-align: middle; text-align: center;">Supplier</th>
                        <th width="30" style="vertical-align: middle; text-align: center;">PC Code</th>
                        <th width="30" style="vertical-align: middle; text-align: center;">Nama Barang</th>
                        <th style="vertical-align: middle; text-align: center;">Total Qty</th>
                        <th style="vertical-align: middle; text-align: center;">Satuan</th>
						<th style="vertical-align: middle; text-align: center;">Harga</th>
						<th style="vertical-align: middle; text-align: center;">Jumlah</th>
                   </tr>
                    
                </thead>
                <tbody>
                <?php
					if($ada_data=="NO"){?>
						           <tr>
                                        <td colspan="100%" style="font-weight: bold;" align="center"><?php echo "Tidak Ada Data."; ?></td>
                                    </tr>
					<?php die;}
					$jml_qty_seluruh = 0;
					$jml_total_seluruh = 0;
                   foreach($list_KdSubKategori as $KdSubKategori=>$val)
                            {
                            	
                            	$NamaSubKategori = $NamaSubKategoris[$KdSubKategori];
                                ?>
                                    <tr>
                                        <td colspan="100%" style="font-weight: bold;"><?php echo $NamaSubKategori; ?></td>
                                    </tr>
                            	<?php
                
                
					$jml_qty = 0;
					$jml_total = 0;
					foreach($SubKategori_PCode[$KdSubKategori] as $PCode=> $val)
                        {
						$Qty = $quantity[$PCode];
						$harga = $list_hargas[$PCode];						
                        $NamaSupplier = $NamaSuppliers[$PCode];	
                        $NamaLengkap = $NamaLengkaps[$PCode];
                        $satuan = $satuans[$PCode];
                        
				        ?>
                        <tr>
							<td nowrap align="left"><?php echo $NamaSupplier; ?></td>
                            <td nowrap align="center"><?php echo $PCode; ?></td>
                            <td nowrap><?php echo $NamaLengkap; ?></td>
                            <td nowrap align="right"><?php echo $Qty; ?></td>
                            <td nowrap align="right"><?php echo $satuan; ?></td>
							<td nowrap align="right"><?php echo number_format($harga,0,',','.'); ?></td>
							<td nowrap align="right"><?php echo number_format($harga*$Qty,0,',','.'); ?></td>
							
                    </tr>
					
                <?php 
					$jml_qty += $Qty;
					$jml_total += $harga*$Qty;
                }
                ?>
                                <tr style="text-align: right; font-weight: bold;">
                                    <td colspan="6">Total <?php echo $NamaSubKategori; ?></td>
                                    <td><?php echo number_format($jml_total,0,',','.'); ?></td>
                                </tr>
				<?php
                    $jml_qty_seluruh += $jml_qty;
					$jml_total_seluruh += $jml_total;
					?>
                <?php }?>
				
				<tr style="text-align: right; font-weight: bold;">
                                    <td colspan="6">Total Keseluruhan</td>
                                    <td><?php echo number_format($jml_total_seluruh,0,',','.'); ?></td>
                                </tr>
								
                <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                </tbody>
            </table>
        </div>

        <!--        </form>-->
    </div>
</div>
<?php

function ubah_format($harga) {
    $s = number_format($harga, 2, ',', '.');
    return $s;
}
?>

<script>
function get_choose(pcode,bln,thn,tipe,filed)
{
		base_url = $("#base_url").val();
		url = base_url+"index.php/pop/pop_up_mutasi_barang/index/"+pcode+"/"+bln+"/"+thn+"/"+filed+"/"+tipe+"/";
		windowOpener(600, 600, 'Detail Mutasi Barang', url, 'Detail Mutasi Barang')
	
}
</script>