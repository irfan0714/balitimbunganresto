<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Grouping Terima Barang";
?>

<script>

	$( document ).ready(function() {
	var searchby = document.getElementById('search_by').value;
	if(searchby=="gudang"){
    document.getElementById('gdg').hidden=false;
    document.getElementById('dvs').hidden=true;
    }else{
	document.getElementById('gdg').hidden=true;
    document.getElementById('dvs').hidden=false;
	}
	});

    $(function () {
        $('#dtpicker1').datepicker({
            format: 'dd-mm-yyyy',
            autoClose: true
        });
        $('#dtpicker1').datepicker().on('changeDate', function (e) {
            $(this).datepicker('hide');
        });

        $('#dtpicker2').datepicker({
            format: 'dd-mm-yyyy',
            autoClose: true
        });
        $('#dtpicker2').datepicker().on('changeDate', function (e) {
            $(this).datepicker('hide');
        });

    });
    
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");

        document.getElementById("search").submit();
        var interval = setInterval(function () {
            $('#pleaseWaitDialog').modal();
        }, 100);


//        jQuery(window).load(function () {
//            clearInterval(interval);
//            close();
//        });

    }

</script>
<div class="row">
    <div class="col-md-12" align="left">
        <ol class="breadcrumb">
            <li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
        </ol>

        <form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/rg_grouping/search_report/" onsubmit="return false" data-target="#pleaseWaitDialog1">

            <table class="table table-bordered responsive">  
            
                <tr>
                     <td width="150"><b>Tahun</b></td>
                      <td width="3">:</td>
                         <td> 
                            <select class="form-control-new" name="tahun" id="tahun">
                                    <option <?= $tahun=='2016' ? 'selected' : '' ; ?> value="2016">2016</option>
                                    <option  <?= $tahun=='2017' ? 'selected' : '' ;?> value="2017">2017</option>
                                    <option  <?= $tahun=='2018' ? 'selected' : '' ;?> value="2018">2018</option>
                            </select>
                         </td>
                </tr> 
                
                <tr>
                     <td width="150"><b>Bulan</b></td>
                      <td>:</td>
                         <td> 
                            <select class="form-control-new" name="bulan" id="bulan">
                                    <option  <?= $bulan=='01' ? 'selected' : '' ; ?> value="01">Januari</option>
                                    <option  <?= $bulan=='02' ? 'selected' : '' ; ?> value="02">Februari</option>
                                    <option  <?= $bulan=='03' ? 'selected' : '' ; ?> value="03">Maret</option>
                                    <option  <?= $bulan=='04' ? 'selected' : '' ; ?> value="04">April</option>
                                    <option  <?= $bulan=='05' ? 'selected' : '' ; ?> value="05">Mei</option>
                                    <option  <?= $bulan=='06' ? 'selected' : '' ; ?> value="06">Juni</option>
                                    <option  <?= $bulan=='07' ? 'selected' : '' ; ?> value="07">Juli</option>
                                    <option  <?= $bulan=='08' ? 'selected' : '' ; ?> value="08">Agustus</option>
                                    <option  <?= $bulan=='09' ? 'selected' : '' ; ?> value="09">Sepetember</option>
                                    <option  <?= $bulan=='10' ? 'selected' : '' ; ?> value="10">Oktober</option>
                                    <option  <?= $bulan=='01' ? 'selected' : '' ; ?> value="11">November</option>
                                    <option  <?= $bulan=='12' ? 'selected' : '' ; ?> value="12">Desember</option>
                            </select>
                         </td>
                </tr>
                
                <tr>
                     <td width="150"><b>Search By</b></td>
                      <td width="3">:</td>
                         <td> 
                            <select class="form-control-new" name="search_by" id="search_by" onchange="srcby()">
                                    <option <?= $search_by=='gudang' ? 'selected' : '' ; ?> value="gudang">Gudang</option>
                                    <option  <?= $search_by=='divisi' ? 'selected' : '' ;?> value="divisi">Divisi</option>
                            </select>
                         </td>
                </tr>
                              
                <?php
				//echo $reportlib->write_plain_combo("Divisi","divisi",$listdivisi,$divisi,"KdDivisi","NamaDivisi",3);
                echo $reportlib->write_plain_combo_report_mutasi("Gudang","gudang","gdg",$listgudang,$gudang,"KdGudang","Keterangan",3);
				?>
				
				<?php
				//echo $reportlib->write_plain_combo("Divisi","divisi",$listdivisi,$divisi,"KdDivisi","NamaDivisi",3);
                echo $reportlib->write_plain_combo_report_mutasi("Divisi","divisi","dvs",$listdivisi,$divisi,"KdDivisi","NamaDivisi",3);
				?>
				
                <tr>
                    <td>&nbsp;</td>
                     <td>&nbsp;</td>
                    <td>
                        <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                        <input type='hidden' value='<?= $print ?>' id="print" name="print">
                        <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                        <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
                    </td>
                </tr>

            </table>


            <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1>Processing...</h1>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form> 
        <?php
        if ($tampilkandata) {
            $this->load->view("report/rg/tampil_rg");
        }
                   
        ?>
    </div>
</div>




<?php
$this->load->view('footer');
?>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>

<script>
	function srcby(){
		var searchby = document.getElementById('search_by').value;
		if(searchby=="gudang"){
			document.getElementById('gdg').hidden=false;
			document.getElementById('dvs').hidden=true;
		}else{
			document.getElementById('gdg').hidden=true;
			document.getElementById('dvs').hidden=false;
		}
	
	}
</script>


