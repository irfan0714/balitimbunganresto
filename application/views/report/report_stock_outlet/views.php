<?php
$this->load->view('header');
$reportlib = new report_lib();

if($jenis=='A')
	$modul = "Stock Outlet";
else
	$modul = "Stock BTS";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_stock_outlet/cari/">       
        	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            	<table class="table table-bordered responsive">
                	<tr style="display:none;">
                    	<td width="100">Tanggal</td>
                    	<td> 
                        	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="tanggal" id="tanggal" size="10" maxlength="10">
                        </td>
                	</tr>
                	
                	<tr>
                    	<td style="display:none;">&nbsp;</td>
                    	<td>
                        	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="View">
                        	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                    	</td>
                	</tr>
            	</table> 
            </div>
	    </form> 
	</div>
</div>

<?php
if ($tampilkanDT) 
{
   	$this->load->view("report/report_stock_outlet/reportRT");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>