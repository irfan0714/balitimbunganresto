<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="dailysales.xls"');
    }
   
    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    $safety_stock = $cek_safety_stock->safetystock;
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold; ">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT STOCK BTS '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style=" color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th style="vertical-align: middle;">PCode</th>
		        <th style="vertical-align: middle;" colspan="2">Nama Barang</th>
		        <th style="vertical-align: middle;">Terjual</th>
		        <th style="vertical-align: middle;">Bebek</th>
		        <th style="vertical-align: middle;">BEO</th>
		        <th style="vertical-align: middle;">Bebek</th>
		    </tr>
		</thead>

		<tbody>
		<tr>
	        <td colspan="6"><b>STOCK BEBEK DI GUDANG BTS (ekor)</b></td>
	        <td align="right">'.$stock_bebek_di_gudang_bts.'</td>
	    </tr>
	    <tr>
	        <td colspan="6"><b>STOCK BUMBU DI GUDANG BTS (gr)</b></td>
	        <td align="right">'.$stock_bumbu_di_gudang_bts.'</td>
	    </tr>
		<tr>
	        <td colspan="6"><b>SAFETY STOCK BEBEK</b></td>
	        <td align="right">'.$safety_stock.'</td>
	    </tr>
	    <tr>
	        <td colspan="100%"><b>PENJUALAN KEMARIN</b></td>
	    </tr>
		';

    $no = 1;
    $hit_jual=0;
    $hit_jual_bebek=0;
    $hit_beo=0; 
    $hit_beo_bebek=0; 
    $hit_bumbu=0;
    foreach($datanya AS $val){
		$pcode 		= $val['PCode'];
		$barang 	= $val['NamaLengkap'];
		$penjualan 	= $val['Qty'];
		$beo 		= $cek_beo[$pcode];
		$bebek 		= $cek_bebek[$pcode];
		$bumbu 		= $cek_bumbu[$pcode];
		
		if($beo==""){
			$beo=0;
		}
		
		
		
		        
    	$echo .= '
    	
    	<tr>
	        <td>'.$pcode.'</td>
	        <td colspan="2">'.$barang.'</td>
	        <td bgcolor="yellow" align="right">'.$penjualan.'</td>
	        <td bgcolor="yellow" align="right">'.$penjualan*$bebek.'</td>
	        <td bgcolor="#04eee2" align="right">'.$beo.'</td>
	        <td bgcolor="#04eee2" align="right">'.$beo*$bebek.'</td>
	    </tr>';
	    
        $hit_jual_bebek	+=$penjualan*$bebek;
        $hit_jual		+=$penjualan;
        $hit_beo		+=$beo; 
        $hit_beo_bebek	+=$beo*$bebek; 
        $hit_bumbu		+=(($penjualan+$beo)*$bumbu);
        $no++;
    }
    $total_bumbu = $hit_bumbu;
    $sup_bebek = $hit_jual_bebek+$hit_beo_bebek;
    
    if($stock_bebek_di_gudang_bts==0){
		$sup_bebek = $safety_stock+$sup_bebek;
	}else{
		$sup_bebek = $safety_stock-($stock_bebek_di_gudang_bts-$hit_jual_bebek);
	}
	
	$echo .= '
	     <tr>
	        <td colspan="2"><b>TOTAL</b></td>
	        <td></td>
	        <td bgcolor="#f9c595" align="right">'.$hit_jual.'</td>
	        <td bgcolor="#f49b4a" align="right">'.$hit_jual_bebek.'</td>
	        <td bgcolor="#f9c595" align="right">'.$hit_beo.'</td>
	        <td bgcolor="#f49b4a" align="right">'.$hit_beo_bebek.'</td>
	      
	    </tr>
	    <tr>
	        <td colspan="6"><b>TOTAL REKOMENDASI PENGERIMAN BEBEK (Ekor)</b></td>
	        <td align="right">'.$sup_bebek.'</td>
	      
	    </tr>
	    <tr>
	        <td colspan="6"><b>TOTAL REKOMENDASI PENGERIMAN BUMBU (Gr)</b></td>
	        <td align="right">'.($sup_bebek*50.0).'</td>
	      
	    </tr>
		</tbody>';	    

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Report Stock Outlet ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>