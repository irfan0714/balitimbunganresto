<?php 

if(!$btn_excel)
{
	$this->load->view('header'); 

	$modul = "Report Purchase Order";
	
}

if($btn_excel)
{
	$file_name = "Report_purchase_order.xls";
	        
	header("Content-Disposition".": "."attachment;filename=$file_name");
	header("Content-type: application/vnd.ms-excel");
}

if(!$btn_excel)
{
	

?>

<script>
function cekTheform()
{
	/*if(document.getElementById("v_start_date").value=="")
    {
        alert("Tanggal Awal harus diisi");
        document.getElementById("v_start_date").focus();
        return false;
    }*/
    
    document.getElementById("theform").submit();	
	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action="<?php echo base_url();?>index.php/report/report_purchase_order/search_report">
		
	    <table class="table table-bordered responsive">
	    	 <tr>
	            <td class="title_table" width="150">Report Type</td>
	            <td>
	            	<label><input type="radio" name="v_type" id="v_type" value="summary" <?php if($v_type){if($v_type=="summary"){ echo 'checked="checked"'; }} ?> />&nbsp;Summary&nbsp;&nbsp;</label>
	            	<label><input type="radio" name="v_type" id="v_type" value="detail" <?php if($v_type){if($v_type=="detail"){ echo 'checked="checked"'; }} ?> />&nbsp;Detail </label>
	            </td>
	        </tr>     
	        
	        <tr>
	            <td class="title_table" width="150">Date</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	s/d
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            	
	            </td>
	        </tr>    
	        
	        <tr>
	            <td class="title_table" width="150">Warehouse</td>
	            <td> 
	            <select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            	<option value="all">All</option>
	            	<?php
	            	foreach($mgudang as $val)
	            	{
	            		$selected="";
	            		if($v_gudang)
	            		{
							if($v_gudang==$val["KdGudang"])
							{
								$selected = 'selected="selected"';
							}
						}
						
						?><option value="<?php echo $val["KdGudang"]; ?>" <?php echo $selected; ?> ><?php echo $val["Keterangan"]; ?></option><?php	
					}
	            	?>
	            </select>
	            </td>
	        </tr>                  
	        
	        <tr>
	            <td class="title_table">Supplier</td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="all">All</option>
	            		<?php
	            		foreach($msupplier as $val)
	            		{
		            		$selected="";
		            		if($v_supplier)
		            		{
								if($v_supplier==$val["KdSupplier"])
								{
									$selected = 'selected="selected"';
								}
							}
							
							?><option value="<?php echo $val["KdSupplier"]; ?>" <?php echo $selected; ?> ><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>           
	        
	        <tr>
	            <td class="title_table">Currency</td>
	            <td> 
	            	<select class="form-control-new" name="v_currency" id="v_currency" style="width: 200px;">
	            		<option value="all">All</option>
	            		<?php
	            		foreach($mcurrency as $val)
	            		{
		            		$selected="";
		            		if($v_currency)
		            		{
								if($v_currency==$val["Kd_Uang"])
								{
									$selected = 'selected="selected"';
								}
							}
							
							?><option value="<?php echo $val["Kd_Uang"]; ?>" <?php echo $selected; ?> ><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>   
	        
	        <tr>
	            <td class="title_table" width="150">PO No</td>
	            <td>
	            	<input type="text" class="form-control-new" name="v_pono" id="v_pono" value="<?php echo $v_pono; ?>" size="35"/>
	            </td>
	        </tr> 
	        
	        <tr>
	            <td class="title_table" width="150">PR No</td>
	            <td>
	            	<input type="text" class="form-control-new" name="v_prno" id="v_prno" value="<?php echo $v_prno; ?>" size="35"/>
	            </td>
	        </tr>         
	        
	        <tr>
	            <td class="title_table">Status</td>
	            <td> 
	            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
	            		<option value="all" <?php if($v_status=="all"){echo 'selected="selected"';} ?> >All</option>
	            		<option value="0" <?php if($v_status=="0"){echo 'selected="selected"';} ?> >Pending</option>
	            		<option value="1" <?php if($v_status=="1"){echo 'selected="selected"';} ?> >Open</option>
	            		<option value="2" <?php if($v_status=="2"){echo 'selected="selected"';} ?> >Void</option>
	            		<option value="3" <?php if($v_status=="3"){echo 'selected="selected"';} ?> >Close</option>
	            	</select>  
	            </td>
	        </tr>     
	        
	        <tr>
	            <td class="title_table">Order By</td>
	            <td> 
	            	<select class="form-control-new" name="v_orderby" id="v_orderby" style="width: 200px;">
	            		<option value="podate" <?php if($v_orderby){if($v_orderby=="podate"){echo 'selected="selected"';}} ?> >PO Date</option>
	            		<option value="supplier" <?php if($v_orderby){if($v_orderby=="supplier"){echo 'selected="selected"';}} ?>>Supplier</option>
	            		<option value="currency" <?php if($v_orderby){if($v_orderby=="currency"){echo 'selected="selected"';}} ?>>Currency</option>
	            	</select>  
	            </td>
	        </tr>
	        
	        <tr>
	        	 <td class="title_table">&nbsp;</td>
	            <td colspan="100%">
					<input type='hidden' name="flag" id="flag" value="analisa">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_analisa" id="btn_analisa"  value="Analisa">Analisa<i class="entypo-check"></i></button>
		        </td>
	        </tr>  
	    </table>
        
    	<?php
    	
}
    	if($flag || $btn_excel)
    	{
    		$mylib = new globallib();
    	
			foreach($header as $val)
			{
				if($v_orderby=="podate")
				{
					$arr_data["list_pono"][$val["NoDokumen"]]=$val["NoDokumen"];
					$arr_data["nopr"][$val["NoDokumen"]]=$val["NoPr"];
					$arr_data["tgldokumen"][$val["NoDokumen"]]=$mylib->ubah_tanggal($val["TglDokumen"]);
					$arr_data["tglterima"][$val["NoDokumen"]]=$mylib->ubah_tanggal($val["TglTerima"]);
					$arr_data["kdsupplier"][$val["NoDokumen"]]=$val["KdSupplier"];
					$arr_data["keterangan"][$val["NoDokumen"]]=$val["Keterangan"];
					$arr_data["kdgudang"][$val["NoDokumen"]]=$val["KdGudang"];
					$arr_data["top"][$val["NoDokumen"]]=$val["TOP"];
					$arr_data["jumlah"][$val["NoDokumen"]]=$val["Jumlah"];
					$arr_data["discharga"][$val["NoDokumen"]]=$val["DiscHarga"];
					$arr_data["ppn"][$val["NoDokumen"]]=$val["PPn"];
					$arr_data["nilaippn"][$val["NoDokumen"]]=$val["NilaiPPn"];
					$arr_data["total"][$val["NoDokumen"]]=$val["Total"];
					$arr_data["status"][$val["NoDokumen"]]=$val["Status"];
					$arr_data["adduser"][$val["NoDokumen"]]=$val["AddUser"];
					$arr_data["approval_by"][$val["NoDokumen"]]=$val["Approval_By"];
					$arr_data["approval_status"][$val["NoDokumen"]]=$val["Approval_Status"];
					$arr_data["nama_supplier"][$val["NoDokumen"]]=$val["nama_supplier"];
					$arr_data["contact"][$val["NoDokumen"]]=$val["Contact"];
					$arr_data["currencycode"][$val["NoDokumen"]]=$val["currencycode"];
					$arr_data["nama_gudang"][$val["NoDokumen"]]=$val["nama_gudang"];
					$arr_data["nama_currency"][$val["NoDokumen"]]=$val["nama_currency"];
				}
				
				if($v_orderby=="supplier")
				{
					$arr_data["list_supplier"][$val["KdSupplier"]]=$val["KdSupplier"];
					$arr_data["nama_supplier"][$val["KdSupplier"]]=$val["nama_supplier"];
					
					$arr_data["pono"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["NoDokumen"];
					$arr_data["nopr"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["NoPr"];
					$arr_data["tgldokumen"][$val["KdSupplier"]][$val["NoDokumen"]]=$mylib->ubah_tanggal($val["TglDokumen"]);
					$arr_data["keterangan"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Keterangan"];
					$arr_data["kdgudang"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["KdGudang"];
					$arr_data["top"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["TOP"];
					$arr_data["jumlah"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Jumlah"];
					$arr_data["discharga"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["DiscHarga"];
					$arr_data["ppn"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["PPn"];
					$arr_data["nilaippn"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["NilaiPPn"];
					$arr_data["total"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Total"];
					$arr_data["status"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Status"];
					$arr_data["approval_by"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Approval_By"];
					$arr_data["approval_status"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Approval_Status"];
					$arr_data["contact"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["Contact"];
					$arr_data["currencycode"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["currencycode"];
					$arr_data["nama_gudang"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["nama_gudang"];
					$arr_data["nama_currency"][$val["KdSupplier"]][$val["NoDokumen"]]=$val["nama_currency"];
					
				}
				
				if($v_orderby=="currency")
				{
					$arr_data["list_currency"][$val["currencycode"]]=$val["currencycode"];
					
					$arr_data["pono"][$val["currencycode"]][$val["NoDokumen"]]=$val["NoDokumen"];
					$arr_data["nopr"][$val["currencycode"]][$val["NoDokumen"]]=$val["NoPr"];
					$arr_data["tgldokumen"][$val["currencycode"]][$val["NoDokumen"]]=$mylib->ubah_tanggal($val["TglDokumen"]);
					$arr_data["keterangan"][$val["currencycode"]][$val["NoDokumen"]]=$val["Keterangan"];
					$arr_data["kdgudang"][$val["currencycode"]][$val["NoDokumen"]]=$val["KdGudang"];
					$arr_data["kdsupplier"][$val["currencycode"]][$val["NoDokumen"]]=$val["KdSupplier"];
					$arr_data["nama_supplier"][$val["currencycode"]][$val["NoDokumen"]]=$val["nama_supplier"];
					$arr_data["top"][$val["currencycode"]][$val["NoDokumen"]]=$val["TOP"];
					$arr_data["jumlah"][$val["currencycode"]][$val["NoDokumen"]]=$val["Jumlah"];
					$arr_data["discharga"][$val["currencycode"]][$val["NoDokumen"]]=$val["DiscHarga"];
					$arr_data["ppn"][$val["currencycode"]][$val["NoDokumen"]]=$val["PPn"];
					$arr_data["nilaippn"][$val["currencycode"]][$val["NoDokumen"]]=$val["NilaiPPn"];
					$arr_data["total"][$val["currencycode"]][$val["NoDokumen"]]=$val["Total"];
					$arr_data["status"][$val["currencycode"]][$val["NoDokumen"]]=$val["Status"];
					$arr_data["approval_by"][$val["currencycode"]][$val["NoDokumen"]]=$val["Approval_By"];
					$arr_data["approval_status"][$val["currencycode"]][$val["NoDokumen"]]=$val["Approval_Status"];
					$arr_data["contact"][$val["currencycode"]][$val["NoDokumen"]]=$val["Contact"];
					$arr_data["currencycode"][$val["currencycode"]][$val["NoDokumen"]]=$val["currencycode"];
					$arr_data["nama_gudang"][$val["currencycode"]][$val["NoDokumen"]]=$val["nama_gudang"];
					$arr_data["nama_currency"][$val["currencycode"]][$val["NoDokumen"]]=$val["nama_currency"];
					
				}
				
				
			}
			
			if($v_type=="detail")
			{
				foreach($detail as $val)
				{
					$arr_data["list_detail"][$val["NoDokumen"]][$val["Sid"]]=$val["Sid"];
					$arr_data["pcode"][$val["NoDokumen"]][$val["Sid"]]=$val["PCode"];
					$arr_data["qty"][$val["NoDokumen"]][$val["Sid"]]=$val["Qty"];
					$arr_data["qtypcs"][$val["NoDokumen"]][$val["Sid"]]=$val["QtyPcs"];
					$arr_data["satuan"][$val["NoDokumen"]][$val["Sid"]]=$val["Satuan"];
					$arr_data["harga"][$val["NoDokumen"]][$val["Sid"]]=$val["Harga"];
					$arr_data["disc1"][$val["NoDokumen"]][$val["Sid"]]=$val["Disc1"];
					$arr_data["disc2"][$val["NoDokumen"]][$val["Sid"]]=$val["Disc2"];
					$arr_data["potongan"][$val["NoDokumen"]][$val["Sid"]]=$val["Potongan"];
					$arr_data["jumlah"][$val["NoDokumen"]][$val["Sid"]]=$val["Jumlah"];
					$arr_data["ppn"][$val["NoDokumen"]][$val["Sid"]]=$val["PPn"];
					$arr_data["total"][$val["NoDokumen"]][$val["Sid"]]=$val["Total"];
					$arr_data["nama_barang"][$val["NoDokumen"]][$val["Sid"]]=$val["nama_barang"];
				}
			}
			
    	?>
    	
    	
    	<?php
    	if(!$btn_excel)
    	{
		?>
		<button type="submit" class="btn btn-green btn-icon btn-sm icon-left" name="btn_excel" id="btn_excel" value="Excel">Export To Excel<i class="entypo-download"></i></button>
    	
    	<div>&nbsp;&nbsp;<br/></div>
		<?php	
		}
    	?>
    	
    	
    	<?php
    	if($btn_excel)
    	{
			$table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
		}
		else
		{
			$table = '<table class="table table-bordered responsive">';	
		}
		
			echo $table;
				
			if($v_type=="summary")
			{
				if($v_orderby=="podate")
				{
    	?>
	    		<thead class="title_table">
	    			<tr>
	    				<th>PO Date</th>
	    				<th>PO No</th>
	    				<th>PR No</th>
	    				<th>Warehouse</th>
	    				<th>Kode</th>
	    				<th>Supplier</th>
	    				<th>Contact Person</th>
	    				<th>Term Of Payment</th>
	    				<th><nobr>Approved By</nobr></th>
	    				<th>Status</th>
	    				<th>Note</th>
	    				<th>Currency</th>
	    				<th>Sub Total</th>
	    				<th>Diskon</th>
	    				<th>PPN</th>
	    				<th>Total</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    			<?php
	    			if(count($arr_data["list_pono"])==0)
	    			{
						echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
					}
					
					$tgldokumen_prev = "";
					foreach($arr_data["list_pono"] as $pono => $val)
					{
						$tgldokumen = $arr_data["tgldokumen"][$pono];
						$nopr = $arr_data["nopr"][$pono];
						$nama_gudang = $arr_data["nama_gudang"][$pono];
						$kdsupplier = $arr_data["kdsupplier"][$pono];
						$nama_supplier = $arr_data["nama_supplier"][$pono];
						$contact = $arr_data["contact"][$pono];
						$top = $arr_data["top"][$pono];
						$approval_by = $arr_data["approval_by"][$pono];
						$status = $arr_data["status"][$pono];
						$keterangan = $arr_data["keterangan"][$pono];
						$currencycode = $arr_data["currencycode"][$pono];
						$jumlah = $arr_data["jumlah"][$pono];
						$discharga = $arr_data["discharga"][$pono];
						$ppn = $arr_data["ppn"][$pono];
						$nilaippn = $arr_data["nilaippn"][$pono];
						$total = $arr_data["total"][$pono];
						
						$echo_status = "";
						if($status=="0")
						{
							$echo_status = "<div class='label label-warning'>Pending</div>";
						}
						else if($status=="1")
						{
							$echo_status = "<div class='label label-info'>Open</div>";
						}
						else if($status=="2")
						{
							$echo_status = "<div class='label label-danger'>Void</div>";
						}
						else if($status=="3")
						{
							$echo_status = "<div class='label label-success'>Close</div>";
						}
						 
						$echo_tgldokumen = "&nbsp;";
						if($tgldokumen_prev!=$tgldokumen)
						{
							$echo_tgldokumen	= $tgldokumen;
						}
						
						$echo_disc = ($jumlah*5)/100;
						$echo_ppn  = (($jumlah-$echo_disc)*$ppn)/100;
						$echo_total = ($jumlah-$echo_disc)+$echo_ppn;
							
					?>
						<tr>
							<td align="center"><?php echo $echo_tgldokumen; ?></td>
							<td align="center"><?php echo $pono; ?></td>
							<td><?php echo $nopr; ?></td>
							<td><nobr><?php echo $nama_gudang; ?></nobr></td>
							<td><?php echo $kdsupplier; ?></td>
							<td><nobr><?php echo $nama_supplier; ?></nobr></td>
							<td><nobr><?php echo $contact; ?></nobr></td>
							<td align="right"><?php echo $top; ?></td>
							<td><?php echo $approval_by; ?></td>
							<td><?php echo $echo_status; ?></td>
							<td><nobr><?php echo $keterangan; ?></nobr></td>
							<td><?php echo $currencycode; ?></td>
							<td align="right"><?php echo number_format($jumlah,2); ?></td>
							<td align="right"><?php echo number_format($echo_disc,2); ?></td>
							<td align="right"><?php echo number_format($echo_ppn,2); ?></td>
							<td align="right"><?php echo number_format($echo_total,2); ?></td>
						</tr>
					<?php
						$tgldokumen_prev = $tgldokumen;
											
					}
	    			?>
	    		</tbody>
    			</table>
    	<?php	
				}	
			
				if($v_orderby=="supplier")
				{					
			?>
			
				<thead class="title_table">
	    			<tr>
	    				<th>Kode</th>
	    				<th>Supplier</th>
	    				<th>PO Date</th>
	    				<th>PO No</th>
	    				<th>PR No</th>
	    				<th>Warehouse</th>
	    				<th>Contact Person</th>
	    				<th>Term Of Payment</th>
	    				<th><nobr>Approved By</nobr></th>
	    				<th>Status</th>
	    				<th>Note</th>
	    				<th>Currency</th>
	    				<th>Sub Total</th>
	    				<th>Diskon</th>
	    				<th>PPN</th>
	    				<th>Total</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    		<?php
	    		$kdsupplier_prev ="";
	    		foreach($arr_data["list_supplier"] as $kdsupplier => $val)
	    		{
					$nama_supplier = $arr_data["nama_supplier"][$kdsupplier];
					
					foreach($arr_data["pono"][$kdsupplier] as $pono => $val2)
					{
						$nopr = $arr_data["nopr"][$kdsupplier][$pono];
						$tgldokumen = $arr_data["tgldokumen"][$kdsupplier][$pono];
						$keterangan = $arr_data["keterangan"][$kdsupplier][$pono];
						$kdgudang = $arr_data["kdgudang"][$kdsupplier][$pono];
						$top = $arr_data["top"][$kdsupplier][$pono];
						$jumlah = $arr_data["jumlah"][$kdsupplier][$pono];
						$discharga = $arr_data["discharga"][$kdsupplier][$pono];
						$ppn = $arr_data["ppn"][$kdsupplier][$pono];
						$nilaippn = $arr_data["nilaippn"][$kdsupplier][$pono];
						$total = $arr_data["total"][$kdsupplier][$pono];
						$status = $arr_data["status"][$kdsupplier][$pono];
						$approval_by = $arr_data["approval_by"][$kdsupplier][$pono];
						$approval_status = $arr_data["approval_status"][$kdsupplier][$pono];
						$contact = $arr_data["contact"][$kdsupplier][$pono];
						$currencycode = $arr_data["currencycode"][$kdsupplier][$pono];
						$nama_gudang = $arr_data["nama_gudang"][$kdsupplier][$pono];
						$nama_currency = $arr_data["nama_currency"][$kdsupplier][$pono];
						
						$echo_status = "";
						if($status=="0")
						{
							$echo_status = "<div class='label label-warning'>Pending</div>";
						}
						else if($status=="1")
						{
							$echo_status = "<div class='label label-info'>Open</div>";
						}
						else if($status=="2")
						{
							$echo_status = "<div class='label label-danger'>Void</div>";
						}
						else if($status=="3")
						{
							$echo_status = "<div class='label label-success'>Close</div>";
						}
						
						$echo_kdsupplier = "";
						$echo_nama_supplier = "";
						if($kdsupplier_prev!=$kdsupplier)
						{
							$echo_kdsupplier = $kdsupplier;
							$echo_nama_supplier = $nama_supplier;
						}
						
						$echo_disc = ($jumlah*5)/100;
						$echo_ppn  = (($jumlah-$echo_disc)*$ppn)/100;
						$echo_total = ($jumlah-$echo_disc)+$echo_ppn;
						
						?>
						<tr>
							<td><?php echo $echo_kdsupplier; ?></td>
							<td><nobr><?php echo $echo_nama_supplier; ?></nobr></td>
							<td align="center"><?php echo $tgldokumen; ?></td>
							<td><?php echo $pono; ?></td>
							<td><?php echo $nopr; ?></td>
							<td><nobr><?php echo $nama_gudang; ?></nobr></td>
							<td><nobr><?php echo $contact; ?></nobr></td>
							<td align="right"><?php echo $top; ?></td>
							<td><?php echo $approval_by; ?></td>
							<td><?php echo $echo_status; ?></td>
							<td><nobr><?php echo $keterangan; ?></nobr></td>
							<td><?php echo $currencycode; ?></td>
							<td align="right"><?php echo number_format($jumlah,2); ?></td>
							<td align="right"><?php echo number_format($echo_disc,2); ?></td>
							<td align="right"><?php echo number_format($echo_ppn,2); ?></td>
							<td align="right"><?php echo number_format($echo_total,2); ?></td>
						</tr>
					<?php
						
						$kdsupplier_prev = $kdsupplier;
						
					}
				}
	    		
	    		?>	    		
	    		</tbody>
				</table>
			<?php
					
				}
			
				if($v_orderby=="currency")
				{
									
			?>
			
				<thead class="title_table">
	    			<tr>
	    				<th>Currency</th>
	    				<th>PO Date</th>
	    				<th>PO No</th>
	    				<th>PR No</th>
	    				<th>Warehouse</th>
	    				<th>Kode</th>
	    				<th>Supplier</th>
	    				<th>Contact Person</th>
	    				<th>Term Of Payment</th>
	    				<th><nobr>Approved By</nobr></th>
	    				<th>Status</th>
	    				<th>Note</th>
	    				<th>Sub Total</th>
	    				<th>Diskon</th>
	    				<th>PPN</th>
	    				<th>Total</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    		<?php
	    		$currency_prev ="";
	    		foreach($arr_data["list_currency"] as $currency => $val)
	    		{
					foreach($arr_data["pono"][$currency] as $pono => $val2)
					{
						$nopr = $arr_data["nopr"][$currency][$pono];
						$tgldokumen = $arr_data["tgldokumen"][$currency][$pono];
						$keterangan = $arr_data["keterangan"][$currency][$pono];
						$kdgudang = $arr_data["kdgudang"][$currency][$pono];
						$kdsupplier = $arr_data["kdsupplier"][$currency][$pono];
						$nama_supplier = $arr_data["nama_supplier"][$currency][$pono];
						$top = $arr_data["top"][$currency][$pono];
						$jumlah = $arr_data["jumlah"][$currency][$pono];
						$discharga = $arr_data["discharga"][$currency][$pono];
						$ppn = $arr_data["ppn"][$currency][$pono];
						$nilaippn = $arr_data["nilaippn"][$currency][$pono];
						$total = $arr_data["total"][$currency][$pono];
						$status = $arr_data["status"][$currency][$pono];
						$approval_by = $arr_data["approval_by"][$currency][$pono];
						$approval_status = $arr_data["approval_status"][$currency][$pono];
						$contact = $arr_data["contact"][$currency][$pono];
						$nama_gudang = $arr_data["nama_gudang"][$currency][$pono];
						$nama_currency = $arr_data["nama_currency"][$currency][$pono];
						
						$echo_status = "";
						if($status=="0")
						{
							$echo_status = "<div class='label label-warning'>Pending</div>";
						}
						else if($status=="1")
						{
							$echo_status = "<div class='label label-info'>Open</div>";
						}
						else if($status=="2")
						{
							$echo_status = "<div class='label label-danger'>Void</div>";
						}
						else if($status=="3")
						{
							$echo_status = "<div class='label label-success'>Close</div>";
						}
						
						$echo_currency = "";
						if($currency_prev!=$currency)
						{
							$echo_currency = $currency;
						}
						
						$echo_disc = ($jumlah*5)/100;
						$echo_ppn  = (($jumlah-$echo_disc)*$ppn)/100;
						$echo_total = ($jumlah-$echo_disc)+$echo_ppn;
						
						?>
						<tr>
							<td><?php echo $echo_currency; ?></td>
							<td align="center"><?php echo $tgldokumen; ?></td>
							<td><?php echo $pono; ?></td>
							<td><?php echo $nopr; ?></td>
							<td><nobr><?php echo $nama_gudang; ?></nobr></td>
							<td><?php echo $kdsupplier; ?></td>
							<td><nobr><?php echo $nama_supplier; ?></nobr></td>
							<td><nobr><?php echo $contact; ?></nobr></td>
							<td align="right"><?php echo $top; ?></td>
							<td><?php echo $approval_by; ?></td>
							<td><?php echo $echo_status; ?></td>
							<td><nobr><?php echo $keterangan; ?></nobr></td>
							<td align="right"><?php echo number_format($jumlah,2); ?></td>
							<td align="right"><?php echo number_format($echo_disc,2); ?></td>
							<td align="right"><?php echo number_format($echo_ppn,2); ?></td>
							<td align="right"><?php echo number_format($echo_total,2); ?></td>
						</tr>
					<?php
						
						$currency_prev = $currency;
						
					}
				}
	    		
	    		?>	    		
	    		</tbody>
				</table>
			<?php
					
				}
			}
			
			if($v_type=="detail")
			{
				if($v_orderby=="podate")
				{
			?>
					<thead class="title_table">
		    			<tr>
		    				<th>PO Date</th>
		    				<th>PO No</th>
		    				<th>PR No</th>
		    				<th>Warehouse</th>
		    				<th>Kode</th>
		    				<th>Supplier</th>
		    				<th><nobr>Contact Person</nobr></th>
		    				<th><nobr>Term Of Payment</nobr></th>
		    				<th><nobr>Approved By</nobr></th>
		    				<th>Status</th>
		    				<th>Note</th>
		    				<th>Currency</th>
		    				<th>Item</th>
		    				<th>Quantity</th>
		    				<th>Unit</th>
		    				<th>Price</th>
		    				<th>Disc</th>
		    				<th>Sub Total</th>
		    			</tr>
		    		</thead>
		    		<tbody>
    		<?php
    				if(count($arr_data["list_pono"])==0)
	    			{
						echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
					}
				
					$pono_prev = "";
					foreach($arr_data["list_pono"] as $pono => $val)
					{
						$tgldokumen = $arr_data["tgldokumen"][$pono];
						$nopr = $arr_data["nopr"][$pono];
						$nama_gudang = $arr_data["nama_gudang"][$pono];
						$kdsupplier = $arr_data["kdsupplier"][$pono];
						$nama_supplier = $arr_data["nama_supplier"][$pono];
						$contact = $arr_data["contact"][$pono];
						$top = $arr_data["top"][$pono];
						$approval_by = $arr_data["approval_by"][$pono];
						$status = $arr_data["status"][$pono];
						$keterangan = $arr_data["keterangan"][$pono];
						$currencycode = $arr_data["currencycode"][$pono];
						$ppn = $arr_data["ppn"][$pono];
						$discharga = $arr_data["discharga"][$pono];
						
						foreach($arr_data["list_detail"][$pono] as $sid => $val2)
						{
							$pcode = $arr_data["pcode"][$pono][$sid];
							$qty = $arr_data["qty"][$pono][$sid];
							$qtypcs = $arr_data["qtypcs"][$pono][$sid];
							$satuan = $arr_data["satuan"][$pono][$sid];
							$harga = $arr_data["harga"][$pono][$sid];
							$disc1 = $arr_data["disc1"][$pono][$sid];
							$disc2 = $arr_data["disc2"][$pono][$sid];
							$potongan = $arr_data["potongan"][$pono][$sid];
							$jumlah = $arr_data["jumlah"][$pono][$sid];
							$total = $arr_data["total"][$pono][$sid];
							$nama_barang = $arr_data["nama_barang"][$pono][$sid];
						
							$echo_status = "";
							if($status=="0")
							{
								$echo_status = "Pending";
							}
							else if($status=="1")
							{
								$echo_status = "Open";
							}
							else if($status=="2")
							{
								$echo_status = "Void";
							}
							else if($status=="3")
							{
								$echo_status = "Close";
							}
							 
							$echo_tgldokumen = "&nbsp;";
							$echo_pono = "&nbsp;";
							$echo_nopr = "&nbsp;";
							$echo_nama_gudang = "&nbsp;";
							$echo_kdsupplier = "&nbsp;";
							$echo_nama_supplier = "&nbsp;";
							$echo_contact = "&nbsp;";
							$echo_top = "&nbsp;";
							$echo_approval_by = "&nbsp;";
							$echo_echo_status = "&nbsp;";
							$echo_keterangan = "&nbsp;";
							$echo_currencycode = "&nbsp;";
							if($pono_prev!=$pono)
							{
								$echo_tgldokumen = $tgldokumen;
								$echo_pono	= $pono;
								$echo_nopr = $nopr;
								$echo_nama_gudang = $nama_gudang;
								$echo_kdsupplier = $kdsupplier;
								$echo_nama_supplier = $nama_supplier;
								$echo_contact = $contact;
								$echo_top = $top;
								$echo_approval_by = $approval_by;
								$echo_echo_status = $echo_status;
								$echo_keterangan = $keterangan;
								$echo_currencycode = $currencycode;
							}
							
							$disc = ($disc1*1)+($disc2*1);
							$echo_disc = (($qty*$harga)*$disc)/100;
							$echo_subtotal = ($qty*$harga)-$echo_disc;
							
				?>
							<tr>
								<td align="center"><?php echo $echo_tgldokumen; ?></td>
								<td align="center"><?php echo $echo_pono; ?></td>
								<td><?php echo $echo_nopr; ?></td>
								<td><nobr><?php echo $echo_nama_gudang; ?></nobr></td>
								<td><?php echo $echo_kdsupplier; ?></td>
								<td><nobr><?php echo $echo_nama_supplier; ?></nobr></td>
								<td><nobr><?php echo $echo_contact; ?></nobr></td>
								<td align="right"><?php echo $echo_top; ?></td>
								<td><nobr><?php echo $echo_approval_by; ?></nobr></td>
								<td><?php echo $echo_echo_status; ?></td>
								<td><nobr><?php echo $echo_keterangan; ?></nobr></td>
								<td><?php echo $echo_currencycode; ?></td>
								
								<td><nobr><?php echo $pcode." - ".$nama_barang; ?></nobr></td>
								<td align="right"><?php echo number_format($qty,2); ?></td>
								<td><?php echo $satuan; ?></td>
								<td align="right"><?php echo number_format($harga,2); ?></td>
								<td align="right"><?php echo number_format($echo_disc,2); ?></td>
								<td align="right"><?php echo number_format($echo_subtotal,2); ?></td>								
							</tr>
				<?php
							$pono_prev = $pono;		
							
							
							$arr["sub_total"][$pono] +=$echo_subtotal;	
						}
						
							$echo_disc_header = ($arr["sub_total"][$pono]*$discharga)/100;
							$echo_ppn_header = (($arr["sub_total"][$pono]-$echo_disc_header)*$ppn)/100;
							$echo_total_header = ($arr["sub_total"][$pono]-$echo_disc_header)+$echo_ppn_header;
						
						?>
						<tr>
							<td colspan="17" align="right"><b>Total</b></td>
							<td align="right"><?php echo number_format($arr["sub_total"][$pono],2); ?></td>
						</tr>
						<tr>
							<td colspan="17" align="right"><b>DISC <?php echo number_format($discharga,2); ?> %</b></td>
							<td align="right"><?php echo number_format($echo_disc_header,2); ?></td>
						</tr>
						<tr>
							<td colspan="17" align="right"><b>PPN <?php echo number_format($ppn,2); ?> %</b></td>
							<td align="right"><?php echo number_format($echo_ppn_header,2); ?></td>
						</tr>
						<tr>
							<td colspan="17" align="right"><b>GRAND Total</b></td>
							<td align="right"><?php echo number_format($echo_total_header,2); ?></td>
						</tr>
						<tr>
							<td colspan="18" style="background:#e3e1db">&nbsp;</td>
						</tr>
						
						<?php	
					}
    		?>
    				</tbody>
	    			</table>
			<?php	
				}				
			
				if($v_orderby=="supplier")
				{
										
			?>
				<thead class="title_table">
	    			<tr>
	    				<th>Kode</th>
	    				<th>Supplier</th>
	    				<th>PO Date</th>
	    				<th>PO No</th>
	    				<th>PR No</th>
	    				<th>Warehouse</th>
	    				<th>Contact Person</th>
	    				<th>Term Of Payment</th>
	    				<th><nobr>Approved By</nobr></th>
	    				<th>Status</th>
	    				<th>Note</th>
	    				<th>Currency</th>
	    				
	    				<th>Item</th>
	    				<th>Quantity</th>
	    				<th>Unit</th>
	    				<th>Price</th>
	    				<th>Disc</th>
	    				<th>Sub Total</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    		<?php
	    		$kdsupplier_prev ="";
	    		foreach($arr_data["list_supplier"] as $kdsupplier => $val)
	    		{
					$nama_supplier = $arr_data["nama_supplier"][$kdsupplier];
					
					foreach($arr_data["pono"][$kdsupplier] as $pono => $val2)
					{
						$nopr = $arr_data["nopr"][$kdsupplier][$pono];
						$tgldokumen = $arr_data["tgldokumen"][$kdsupplier][$pono];
						$keterangan = $arr_data["keterangan"][$kdsupplier][$pono];
						$kdgudang = $arr_data["kdgudang"][$kdsupplier][$pono];
						$top = $arr_data["top"][$kdsupplier][$pono];
						$jumlah = $arr_data["jumlah"][$kdsupplier][$pono];
						$discharga = $arr_data["discharga"][$kdsupplier][$pono];
						$ppn = $arr_data["ppn"][$kdsupplier][$pono];
						$nilaippn = $arr_data["nilaippn"][$kdsupplier][$pono];
						$total = $arr_data["total"][$kdsupplier][$pono];
						$status = $arr_data["status"][$kdsupplier][$pono];
						$approval_by = $arr_data["approval_by"][$kdsupplier][$pono];
						$approval_status = $arr_data["approval_status"][$kdsupplier][$pono];
						$contact = $arr_data["contact"][$kdsupplier][$pono];
						$currencycode = $arr_data["currencycode"][$kdsupplier][$pono];
						$nama_gudang = $arr_data["nama_gudang"][$kdsupplier][$pono];
						$nama_currency = $arr_data["nama_currency"][$kdsupplier][$pono];
						
						foreach($arr_data["list_detail"][$pono] as $sid => $val3)
						{
							$pcode = $arr_data["pcode"][$pono][$sid];
							$qty = $arr_data["qty"][$pono][$sid];
							$qtypcs = $arr_data["qtypcs"][$pono][$sid];
							$satuan = $arr_data["satuan"][$pono][$sid];
							$harga = $arr_data["harga"][$pono][$sid];
							$disc1 = $arr_data["disc1"][$pono][$sid];
							$disc2 = $arr_data["disc2"][$pono][$sid];
							$potongan = $arr_data["potongan"][$pono][$sid];
							$jumlah = $arr_data["jumlah"][$pono][$sid];
							$total = $arr_data["total"][$pono][$sid];
							$nama_barang = $arr_data["nama_barang"][$pono][$sid];
						
							$echo_status = "";
							if($status=="0")
							{
								$echo_status = "Pending";
							}
							else if($status=="1")
							{
								$echo_status = "Open";
							}
							else if($status=="2")
							{
								$echo_status = "Void";
							}
							else if($status=="3")
							{
								$echo_status = "Close";
							}
							 
							$echo_kdsupplier = "&nbsp;";
							$echo_nama_supplier = "&nbsp;";
							if($kdsupplier_prev!=$kdsupplier)
							{
								$echo_kdsupplier = $kdsupplier;	
								$echo_nama_supplier = $nama_supplier;
							}
							
							$disc = ($disc1*1)+($disc2*1);
							$echo_disc = (($qty*$harga)*$disc)/100;
							$echo_subtotal = ($qty*$harga)-$echo_disc;
							
				?>
							<tr>
								<td><?php echo $echo_kdsupplier; ?></td>
								<td><nobr><?php echo $echo_nama_supplier; ?></nobr></td>
								<td align="center"><?php echo $tgldokumen; ?></td>
								<td><?php echo $pono; ?></td>
								<td><?php echo $nopr; ?></td>
								<td><nobr><?php echo $nama_gudang; ?></nobr></td>
								<td><nobr><?php echo $contact; ?></nobr></td>
								<td align="right"><?php echo $top; ?></td>
								<td><nobr><?php echo $approval_by; ?></nobr></td>
								<td><?php echo $echo_status; ?></td>
								<td><nobr><?php echo $keterangan; ?></nobr></td>
								<td><?php echo $currencycode; ?></td>
								
								<td><nobr><?php echo $pcode." - ".$nama_barang; ?></nobr></td>
								<td align="right"><?php echo number_format($qty,2); ?></td>
								<td><?php echo $satuan; ?></td>
								<td align="right"><?php echo number_format($harga,2); ?></td>
								<td align="right"><?php echo number_format($echo_disc,2); ?></td>
								<td align="right"><?php echo number_format($echo_subtotal,2); ?></td>								
							</tr>
				<?php
							$kdsupplier_prev = $kdsupplier;		
							
						}
					}
				}
	    		
	    		?>	    		
	    		</tbody>
				</table>
			<?php
					
				}
			
				if($v_orderby=="currency")
				{
										
			?>
				<thead class="title_table">
	    			<tr>
	    				<th>Currency</th>
	    				<th>PO Date</th>
	    				<th>PO No</th>
	    				<th>PR No</th>
	    				<th>Warehouse</th>
	    				<th>Kode</th>
	    				<th>Supplier</th>
	    				<th>Contact Person</th>
	    				<th>Term Of Payment</th>
	    				<th><nobr>Approved By</nobr></th>
	    				<th>Status</th>
	    				<th>Note</th>
	    				
	    				<th>Item</th>
	    				<th>Quantity</th>
	    				<th>Unit</th>
	    				<th>Price</th>
	    				<th>Disc</th>
	    				<th>Sub Total</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    		<?php
	    		$kdsupplier_prev ="";
	    		foreach($arr_data["list_currency"] as $currency => $val)
	    		{
					foreach($arr_data["pono"][$currency] as $pono => $val2)
					{
						$nopr = $arr_data["nopr"][$currency][$pono];
						$tgldokumen = $arr_data["tgldokumen"][$currency][$pono];
						$keterangan = $arr_data["keterangan"][$currency][$pono];
						$kdgudang = $arr_data["kdgudang"][$currency][$pono];
						$kdsupplier = $arr_data["kdsupplier"][$currency][$pono];
						$nama_supplier = $arr_data["nama_supplier"][$currency][$pono];
						$top = $arr_data["top"][$currency][$pono];
						$jumlah = $arr_data["jumlah"][$currency][$pono];
						$discharga = $arr_data["discharga"][$currency][$pono];
						$ppn = $arr_data["ppn"][$currency][$pono];
						$nilaippn = $arr_data["nilaippn"][$currency][$pono];
						$total = $arr_data["total"][$currency][$pono];
						$status = $arr_data["status"][$currency][$pono];
						$approval_by = $arr_data["approval_by"][$currency][$pono];
						$approval_status = $arr_data["approval_status"][$currency][$pono];
						$contact = $arr_data["contact"][$currency][$pono];
						$nama_gudang = $arr_data["nama_gudang"][$currency][$pono];
						$nama_currency = $arr_data["nama_currency"][$currency][$pono];
						
						foreach($arr_data["list_detail"][$pono] as $sid => $val3)
						{
							$pcode = $arr_data["pcode"][$pono][$sid];
							$qty = $arr_data["qty"][$pono][$sid];
							$qtypcs = $arr_data["qtypcs"][$pono][$sid];
							$satuan = $arr_data["satuan"][$pono][$sid];
							$harga = $arr_data["harga"][$pono][$sid];
							$disc1 = $arr_data["disc1"][$pono][$sid];
							$disc2 = $arr_data["disc2"][$pono][$sid];
							$potongan = $arr_data["potongan"][$pono][$sid];
							$jumlah = $arr_data["jumlah"][$pono][$sid];
							$total = $arr_data["total"][$pono][$sid];
							$nama_barang = $arr_data["nama_barang"][$pono][$sid];
						
							$echo_status = "";
							if($status=="0")
							{
								$echo_status = "Pending";
							}
							else if($status=="1")
							{
								$echo_status = "Open";
							}
							else if($status=="2")
							{
								$echo_status = "Void";
							}
							else if($status=="3")
							{
								$echo_status = "Close";
							}
							 
							$echo_currency = "";
							if($currency_prev!=$currency)
							{
								$echo_currency = $currency;
							}
							
							$disc = ($disc1*1)+($disc2*1);
							$echo_disc = (($qty*$harga)*$disc)/100;
							$echo_subtotal = ($qty*$harga)-$echo_disc;
							
				?>
							<tr>
								<td><?php echo $echo_currency; ?></td>
								<td align="center"><?php echo $tgldokumen; ?></td>
								<td><?php echo $pono; ?></td>
								<td><?php echo $nopr; ?></td>
								<td><nobr><?php echo $nama_gudang; ?></nobr></td>
								<td><?php echo $kdsupplier; ?></td>
								<td><nobr><?php echo $nama_supplier; ?></nobr></td>
								<td><nobr><?php echo $contact; ?></nobr></td>
								<td align="right"><?php echo $top; ?></td>
								<td><nobr><?php echo $approval_by; ?></nobr></td>
								<td><?php echo $echo_status; ?></td>
								<td><nobr><?php echo $keterangan; ?></nobr></td>
								
								<td><nobr><?php echo $pcode." - ".$nama_barang; ?></nobr></td>
								<td align="right"><?php echo number_format($qty,2); ?></td>
								<td><?php echo $satuan; ?></td>
								<td align="right"><?php echo number_format($harga,2); ?></td>
								<td align="right"><?php echo number_format($echo_disc,2); ?></td>
								<td align="right"><?php echo number_format($echo_subtotal,2); ?></td>								
							</tr>
				<?php
							$kdsupplier_prev = $kdsupplier;		
							
						}
					}
				}
	    		
	    		?>	    		
	    		</tbody>
				</table>
			<?php
					
				}
			}
						
		}
		
		
		if(!$btn_excel)
		{
    	?>
    	
	    </form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<?php
}
?>