<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
function submitThis()
{
    if($("#barang").val()=="" || $("#barang").val()=="")
	{
	   alert("Barang atau Gudang harus di pilih.");
	}
	else
	{
	$("#excel").val("");
	$("#print").val("");
//	$("#search").submit();
        document.getElementById("search").submit();
	}
}

</script>
<body>
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/kartu_stock/cari/" onsubmit="return false">
<table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
			    <tr>
					<td nowrap>Tahun</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="tahun" name="tahun">
					<option value="">--Select--</option>
					<?php
					for($a = 0;$a<count($tahun);$a++){
						$select = "";
						if($tahunaktif==$tahun[$a]['Tahun']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$tahun[$a]['Tahun']?>"><?=$tahun[$a]['Tahun']?></option>
					<?php
					}
					?>
					</select>
				</tr>

				<tr>
					<td nowrap>Bulan</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="bulan" name="bulan">
					<option value="">--Select--</option>
					<?php
					for($a = 0;$a<count($bulan);$a++){
						$select = "";
						if($bulanaktif==$bulan[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$bulan[$a]?>"><?=$bulan[$a]?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				<?php			
				echo $reportlib->write_plain_combo("Gudang","gudang",$listgudang,$gudang,"KdGudang","Keterangan",3);
				echo $reportlib->write_plain_combo("Barang","barang",$listbarang,$barang,"PCode","NamaLengkap",3);
				?>
			</table>
		</td>
	</tr>
	<tr bordercolor="#FFFFFF">
		<td colspan="2" align="center"><input type="button" value="Search (*)" onclick="submitThis()">
		<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
		<input type='hidden' value='<?=$userlevel?>' id="userlevel" name="userlevel">
		<input type='hidden' value='<?=$excel?>' id="excel" name="excel">
		<input type='hidden' value='<?=$print?>' id="print" name="print">
		</td>
	</tr>
</table>
</form>
</body>
<?php
if($tampilkanRT)
{
	$this->load->view("report/kartu_stock/reportRT");
}
$this->load->view('footer'); ?>