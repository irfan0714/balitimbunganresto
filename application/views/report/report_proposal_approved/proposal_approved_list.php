<?php
$this->load->view('header'); 

$arrbulan = array('0','Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'); 
?>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/report/report_proposal_approved/'>
<table>
	<tr>
		<td>
		<label>No. Dokumen </label>
		<input type="text" size="20" maxlength="30" name="no_dokumen" id="no_dokumen" class="form-control-new" value="<?php echo $no_dokumen; ?>"/>
		<label>Divisi</label>
		<select style="" class="form-control-new"  size="1" height="1"  name ="divisi" >
			<option value="">all</option>
			<?php 
				foreach ($divisi as $val) {
					$select = ($val['KdDivisi'] == $v_divisi)?"selected":"";;
					echo "<option value='".$val['KdDivisi']."' ".$select.">".$val['NamaDivisi']."</option>";
				}
				?>
		</select>
        <label>Approved By</label>
		<select style="" class="form-control-new"  size="1" height="1"  name ="approved_by" id ="approved_by">
			<option value="">all</option>
				<?php 
				foreach ($approval_user as $val) {
					$select = ($val['approve_by'] == $approved_by)?"selected":"";;
					echo "<option value='".$val['approve_by']."' ".$select.">".$val['approve_by']."</option>";
				}
				?>
		</select>
		<label>Bulan</label>
		<select style="" class="form-control-new"  size="1" height="1"  name ="bulan" >
			<?php 
				for($i=1;$i<=date('m') ;$i++){
					$select = ( $bulan == $i)?"selected":"";
					echo "<option value='".$i."' ".$select.">".$arrbulan[$i]."</option>";
				}
			?>
		</select>
		<label>Tahun</label>
		<select style="" class="form-control-new"  size="1" width="10" height="1"  name ="tahun" >
			<?php 
				for($i=2017;$i<=date('Y') ;$i++){
					$select = ( $tahun == $i)?"selected":"";
					echo "<option value='".$i."' ".$select.">".$i."</option>";
				}
			?>
		</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search"></td>
		<button class="btn btn-default pull-right">Export To Excel</button>

	</tr>
</table>
</form>
<hr>
<table class="table table-bordered ">
	 <thead class="title_table">
        <tr>
            <th rowspan="2">No</th>
			<th rowspan="2">No Proposal</th>
			<th rowspan="2">Nama Proposal</th>
	        <th rowspan="2">Tanggal</th>
	        <th rowspan="2">Divisi</th>
            <th rowspan="2">Aktivitas</th>
            <th rowspan="2">Periode</th>
            <th rowspan="2">Latar Belakang</th>
            <th rowspan="2">tujuan</th>
            <th rowspan="2">Mekasnisme</th>
			<th rowspan="2">Added By</th>
            <th rowspan="2">Status</th>
            <th colspan="4" align="center">Proposal Detail</th>
            <th colspan="2" align="center">Proposal Target</th>
    	</tr>
    	<tr>
    		<th>Keterangan Detail</th>
    		<th>Harga</th>
    		<th>Qty</th>
    		<th>Total</th>
    		<th>Keterangan Target</th>
    		<th>Nilai</th>
    	</tr>
    </thead>
    <tbody>
    	<?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Data Is Not Found</td></tr>";
            }
        	$no = 1;
        	$nom = 1;
        	$total = 0;
        	$NoProposal_Temp = "";
            foreach ($data as $val) {
                if($val['Status']==0 and $val['Status_Reject']==0){
				$Status="<font color='black'><b>Pending</b></font>";
				}else if($val['Status']==1){
					if($val['Status_approve1']==0 and $val['Status_approve2']==0 and $val['Status_approve3']==0 and $val['Status_Reject']==0){
						$Status="<font color='blue'><b>Waiting Approve 1 (".$val['Approval1Name'].")</b></font>";
					}else if($val['Status_approve1']==1 and $val['Status_approve2']==0 and $val['Status_approve3']==0 and $val['Status_Reject']==0){
						$Status="<font color='orange'><b>Waiting Approve 2 (".$val['Approval2Name'].")</b></font>";
					}else if($val['Status_approve1']==1 and $val['Status_approve2']==1 and $val['Status_approve3']==0 and $val['Status_Reject']==0){
						$Status="<font color='#cc04f4'><b>Waiting Approve 3 (".$val['Approval3Name'].")</b></font>";
					}else if($val['Status_approve1']==1 and $val['Status_approve2']==1 and $val['Status_approve3']==1 and $val['Status_Reject']==0){
						$Status="<font color='green'><b>OK</b></font>";
					}else{
						$Status="<font color='red'>Reject by ".$val['whoisreject']." pada ".$val['RejectDate']." karena ".$val['Ket_Reject']."</font>";
					}
				}else if($val['Status']==2){
						$Status="<font color='red'><b>Void</b></font>";
				}else{
					$Status="<font color='red'>Reject by ".$val['whoisreject']." pada ".$val['RejectDate']." karena ".$val['Ket_Reject']."</font>";
			}

			$NoProposal = "";
			$NamaProposal = "";
			$Tanggal_ = "";
			$NamaDivisi = "";
			$NamaAktivitas = "";
			$Periode = "";
			$LatarBelakang = "";
			$LatarBelakang_title = "";
			$Tujuan = "";
			$Mekanisme = "";
			$add_user = "";
			$approve_user = "";
			$sts = "";
			$Tanggal = "";
			if($NoProposal_Temp != $val['NoProposal']){
               
				$NoProposal = $val["NoProposal"];
				$NamaProposal = $val["NamaProposal"];
				$Tanggal = $val["Tanggal"];
				$NamaDivisi = $val["NamaDivisi"];
				$NamaAktivitas = $val["NamaAktivitas"];
				$Periode = $val["PeriodeAwal"]." - ".$val["PeriodeAkhir"];
				$LatarBelakang = substr($val["LatarBelakang"],0,20)."...";
				$LatarBelakang_title =$val["LatarBelakang"];
				$Tujuan = substr($val["Tujuan"],0,20)."...";
				$Mekanisme = substr($val["Mekanisme"],0,20)."...";
				$add_user = $val["employee_name"];
				$sts = $Status;
				if($val["Approval1Name"] == $val['whoisreject']){$apv1Icon= "<i class='entypo-cancel'></i>";}else{$apv1Icon= "<i class='entypo-check'></i>";} ;
				if($val["Approval2Name"] == $val['whoisreject']){$apv2Icon= "<i class='entypo-cancel'></i>";}else{$apv2Icon= "<i class='entypo-check'></i>";} ;
				if($val["Approval3Name"] == $val['whoisreject']){$apv3Icon= "<i class='entypo-cancel'></i>";}else{$apv3Icon= "<i class='entypo-check'></i>";} ;
				$approve_user = " <table>
						        	<tr><td>".$val["Approval1Name"]."</td><td>".$apv1Icon."</td></tr>
						        	<tr><td>".$val["Approval2Name"]."</td><td>".$apv2Icon."</td></tr>
						        	<tr><td>".$val["Approval3Name"]."</td><td>".$apv3Icon."</td></tr>
						          </table> ";
				$no=0;
			}
            ?>
    	<tr style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $nom; ?>')" onmouseout="change_onMouseOut('<?php echo $nom; ?>')" id="<?php echo $nom; ?>">
    		<td><?php echo $no; ?></td>
    		<td><?php echo $NoProposal; ?></td>
    		<td><?php echo $NamaProposal; ?></td>
    		<td><?php echo $Tanggal; ?></td>
    		<td><?php echo $NamaDivisi; ?></td>
    		<td><?php echo $NamaAktivitas; ?></td>
    		<td><?php echo $Periode; ?></td>
    		<td title="<?php echo $LatarBelakang_title; ?>"><?php echo $LatarBelakang; ?></td>
    		<td><?php echo $Tujuan; ?></td>
    		<td><?php echo $Mekanisme; ?></td>
    		<td><?php echo $add_user; ?></td>
    		<td><?php echo $sts."<br>".$approve_user; ?></td>
    		<td><?php echo $val["Keterangan"]; ?></td>
    		<td align="right"><?php echo $val["HargaSatuan"]; ?></td>
    		<td><?php echo $val["Qty"]; ?></td>
    		<td align="right"><?php echo $val["Qty"]*$val["HargaSatuan"]; ?></td>
    		<td>-</td>
    		<td>-</td>
    	</tr>
    	<?php
		    	$no++;
		    	$nom++;
                $NoProposal_Temp = $val['NoProposal'];
                $total += $val["Qty"]*$val["HargaSatuan"];
            }
            ?>
    </tbody>
</table>
<?php
$this->load->view('footer'); ?>

<!-- Default modal -->
    <div id="default_modal" class="modal fade" tabindex="-1" role="dialog">
	<form action="#" id="form" class="form-horizontal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- New invoice template -->
          <div class="panel">
            <div class="panel-body">
                <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
				<input type="hidden" name="v_username" id="v_username" value="<?php echo $username; ?>">
                <div class="row invoice-header">
	                <div class="col-lg-12">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                  <h4><b>Buat Proposal</b></h4>
	                </div>
                </div>
            </div>
			<div class="table-responsive">
              <table class="table table-bordered responsive">
                <tbody>
                  <tr>
                    <td class="title_table" width="150">No Proposal</td>
                    <td>
					<input readonly id="noproposal" name="noproposal" placeholder="Auto Generate" class="form-control-new" type="text" style="width:200px;">
					</td>
                  </tr>

                  <tr>
                    <td class="title_table" width="150">Nama Proposal</td>
                    <td>
					<input id="namaproposal" name="namaproposal" placeholder="Nama Proposal" class="form-control-new" type="text" style="width:530px;">
					</td>
                  </tr>

                  <tr>
                    <td class="title_table" width="150">Tanggal Dokumen</td>
                    <td>
					<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	                </td>
                  </tr>

                  <tr>
                    <td class="title_table" width="150">Divisi</td>
                    <td>
					<select name="divisi" id="divisi" class="form-control-new" style="width:150px;">

                           <option value=""> -- Pilih -- </option>
		            		<?php
		            		foreach($divisi as $val)
		            		{
		            			$selected="";
								if($div_==$val["KdDivisi"])
								{
									$selected='selected="selected"';
								}
								?><option <?php echo $selected; ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
							}
		            		?>
	            		</select>
					</td>
                  </tr>

				  <tr>
                    <td class="title_table" width="150">Aktivitas</td>
                    <td>
					<input type="text" class="form-control-new" size="20" maxlength="10" name="v_keyword_aktivitas" id="v_keyword_aktivitas" placeholder="Keyword" onblur="cari_aktivitas('<?php echo base_url(); ?>')">
                        <span id="select_rekening">
                        <select name="kdaktivitas" id="kdaktivitas" class="form-control-new" style="width:400px;">

                           <option value=""> -- Pilih -- </option>
		            		<?php
		            		foreach($aktivitas as $val)
		            		{
								?><option value="<?php echo $val["KdAktivitas"]; ?>"><?php echo $val["NamaAktivitas"]; ?></option><?php
							}
		            		?>
	            		</select>
                        </span>
					</td>
                  </tr>
				  <tr>
                    <td class="title_table" width="150">Periode Awal</td>
                    <td>
					<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_periode_awal" id="v_tgl_periode_awal" size="10" maxlength="10">
	                </td>
                  </tr>
				  <tr>
                    <td class="title_table" width="150">Periode Akhir</td>
                    <td>
					<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_periode_akhir" id="v_tgl_periode_akhir" size="10" maxlength="10">
	                </td>
                  </tr>
				  <tr>
                    <td class="title_table" width="150">Latar Belakang</td>
                    <td>
					<textarea id="latar_blk" name="latar_blk" rows="5" cols="40"  style="width: 530px;height: 70px;" class="form-control-new"></textarea>
					</td>
                  </tr>
                  <tr>
                    <td class="title_table" width="150">Tujuan</td>
                    <td>
					<textarea id="tujuan" name="tujuan" rows="5" cols="40"  style="width: 530px;height: 70px;" class="form-control-new"></textarea>
					</td>
                  </tr>
                  <tr>
                    <td class="title_table" width="150">Mekanisme</td>
                    <td>
					<textarea id="mekanisme" name="mekanisme" rows="5" cols="40"  style="width: 530px;height: 70px;" class="form-control-new"></textarea>
					</td>
                  </tr>
                  <tr>
                    <td class="title_table" width="150">PIC</td>
                    <td>
					<input type="text" class="form-control-new" size="20" maxlength="10" name="v_keyword_employee_pic" id="v_keyword_employee_pic" placeholder="Keyword" onblur="cari_employee_pic('<?php echo base_url(); ?>')">
                        <span id="select_employee_pic">
                        <select name="employee_pic" id="employee_pic" class="form-control-new" style="width:200px;">

                           <option value=""> -- Pilih -- </option>
		            		<?php
		            		foreach($employee_pic as $val)
		            		{
								?><option  value="<?php echo $val["username"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
							}
		            		?>
	            		</select>
                        </span>
					</td>
                  </tr>

                  <tr>
                    <td class="title_table" width="150">Approval 1</td>
                    <td>
					<input type="text" class="form-control-new" size="20" maxlength="10" name="v_keyword_employee_approval1" id="v_keyword_employee_approval1" placeholder="Keyword" onblur="cari_employee_approval1('<?php echo base_url(); ?>')">
                        <span id="select_employee_pic">
                        <select name="employee_approval1" id="employee_approval1" class="form-control-new" style="width:200px;">

                           <option value=""> -- Pilih -- </option>
		            		<?php
		            		foreach($employee_approval1 as $val)
		            		{
								?><option value="<?php echo $val["username"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
							}
		            		?>
	            		</select>
                        </span>
                        <span id="teks_email" style="display: none">
	                    Cari Alamat Email ...
	                    </span>
                        <span id="view_email" style="display: none">
	                     	<input data-toggle="tooltip" data-placement="top" data-original-title="isi alamat email." type='text' class='form-control-new' size='50' maxlength='50' name='v_email_approve1' id='v_email_approve1' value='' placeholder="Alamat Email.">
	                    </span>

	                    <span id="email_ok" style="display: none">
	                    &nbsp;<img src="../../../public/images/accept.png"/>
	                    </span>

	                    <span id="email_no" style="display: none">
	                    &nbsp;<img src="../../../public/images/cancel.png"/>
	                    </span>

					</td>
                  </tr>

                  <tr>
                    <td class="title_table" width="150">Approval 2</td>
                    <td>
					<input type="hidden" id="employee_approval2" name="employee_approval2" value="dicky0707">
					<b>Dicky Prasetyo</b>
					</td>
                  </tr>

                  <tr>
                    <td class="title_table" width="150">Approval 3</td>
                    <td>
					<input type="hidden" id="employee_approval3" name="employee_approval3" value="trisno1402">
					<b>Bambang Sutrisno</b>
					</td>
                  </tr>

				  <tr style="display: none">
				  <td class="title_table">Type</td>
				  <td>
				  <input type="text" value="0" id="v_status" name="v_status" >
				  <input type="text" value="0" id="v_approve1" name="v_approve1" >
				  <input type="text" value="0" id="v_approve2" name="v_approve2" >
				  <input type="text" value="0" id="v_approve3" name="v_approve3" >
		          </td>
		          </tr>


              </table>
            </div>
			<br><br>

			<div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="entypo-list"></i> Proposal Detail</a></li>
                <li><a href="#tab2" data-toggle="tab"><i class="entypo-pencil"></i> Proposal Target</a></li>
              </ul>
              <div class="tab-content">
			    <div class="tab-pane fade in active" id="tab1">
		            <div class="table-responsive">


						<!-- edit only  -->
			        <table id="edit_only_detail" name="edit_only" class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="334"><center>Keterangan Detail</center></th>
		                        <th width="101"><center>Harga</center></th>
		                        <th width="100"><center>Qty</center></th>
														 <th width="100"><center>Total</center></th>
		                        <th width="58"><center>Action</center>
								</th>

							</tr>
						</thead>
						<tbody id="edit_only_detail2" border="1">
						</tbody>
					</table>

			<!-- edit only ------------------------------ -->


		              <table class="table table-striped table-bordered" id="TabelDetail1">
		                <thead class="title_table">
		                  <tr>
		                    <th><center>Keterangan Detail</center></th>
		                    <th width="150"><center>Harga</center></th>
		                    <th width="50"><center>Qty</center></th>
		                    <th width="100"><center>
									    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew_detail()">
												<i class="entypo-plus"></i>
											</button></center>
							</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  <?php $no=1; ?>

						  <tr id="baris_detail<?php echo $no; ?>">
		                    <td>
							<input type="text" class="form-control-new" size="70" name="v_ket_detail[]" id="v_ket_detail<?php echo $no;?>" placeholder="Keterangan">
							</td>
							<td>
							<input style="text-align: right;" type="text" class="form-control-new" size="20" name="v_harga_detail[]" id="v_harga_detail<?php echo $no;?>" placeholder="Harga">
							</td>
		                    <td>
							<input style="text-align: right;" id="v_qty_detail<?php echo $no;?>" name="v_qty_detail[]" class="form-control-new" type="text" placeholder="Qty">
		                    </td>
							                <td align="center" id="btn_del_detail">
						                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_detail_<?php echo $no;?>]" id="btn_del_detail_detail_<?php echo $no;?>" value="Save" onclick='deleteRow_detail(this)'>
												<i class="entypo-trash"></i>
											</button>
						    </td>
		                  </tr>
		              </table>
		            </div>
            	 </div>


            	 <div class="tab-pane body fade" id="tab2">
		             <div class="table-responsive">


					 <!-- edit only ------------------------------ -->
			        <table id="edit_only_target" name="edit_only" class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="334"><center>Keterangan Target</center></th>
		                        <th width="101"><center>Nilai</center></th>
		                        <!--<th width="100"><center>Qty</center></th>-->
		                        <th width="58"><center>Action</center>
								</th>

							</tr>
						</thead>
						<tbody id="edit_only_target2" border="1">
						</tbody>
					</table>

			<!-- edit only ------------------------------ -->


		              <table class="table table-striped table-bordered" id="TabelDetail2">
		                <thead class="title_table">
		                  <tr>
		                    <th><center>Keterangan Target</center></th>
		                    <th width="120"><center>Nilai</center></th>
		                    <!--<th width="30"><center>Qty</center></th>-->
		                    <th width="100"><center>
									    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew_target()">
												<i class="entypo-plus"></i>
											</button></center>
							</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  <?php $no_=1; ?>

						  <tr id="baris_target<?php echo $no_; ?>">
		                    <td>
							<input type="text" class="form-control-new" size="70" name="v_ket_target[]" id="v_ket_target<?php echo $no_;?>" placeholder="Keterangan">
							</td>
							<td>
							<input style="text-align: right;" type="text" class="form-control-new" size="20" name="v_harga_target[]" id="v_harga_target<?php echo $no_;?>" placeholder="Nilai">
							</td>
		                    <!--<td>
							<input id="v_qty_target<?php echo $no_;?>" name="v_qty_target[]" class="form-control-new" type="text" placeholder="Qty">
		                    </td>-->
							<td align="center" id="btn_del_detail2">
						                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_target_<?php echo $no_;?>]" id="btn_del_detail_target_<?php echo $no_;?>" value="Save" onclick='deleteRow_target(this)'>
												<i class="entypo-trash"></i>
											</button>
						    </td>
		                  </tr>
		              </table>
		            </div>
            	 </div>


            </div>
            </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
			<button id="btnVoid" type="button" onclick="batal_void('<?php echo base_url(); ?>')" class="btn btn-danger">Proposal Batal</button>
            <button id="btnSave" type="button" onclick="save('<?php echo base_url(); ?>')" class="btn btn-info">Simpan.</button>
			<button id="btnSaveSend" type="button" onclick="simpan_kirim('<?php echo base_url(); ?>')" class="btn btn-success">Simpan &amp; Kirim</button>

			<span id="vAprv1" style="display:none">
			<button id="btnApprove" type="button" onclick="approval1('<?php echo base_url(); ?>')" class="btn btn-success">Approve</button>
			</span>

			<span id="vAprv2" style="display:none">
			<button id="btnApprove" type="button" onclick="approval2('<?php echo base_url(); ?>')" class="btn btn-success">Approve</button>
			</span>

			<span id="vAprv3" style="display:none">
			<button id="btnApprove" type="button" onclick="approval3('<?php echo base_url(); ?>')" class="btn btn-success">Approve</button>
			</span>

			<span id="vRejc1Tamp" style="display:none">
			<button id="btnReject_tampil" type="button" onclick="reject_tampil1('<?php echo base_url(); ?>')" class="btn btn-orange">Reject</button>
			</span>

			<span id="vRejc2Tamp" style="display:none">
			<button id="btnReject_tampil" type="button" onclick="reject_tampil2('<?php echo base_url(); ?>')" class="btn btn-orange">Reject</button>
			</span>

			<span id="vRejc3Tamp" style="display:none">
			<button id="btnReject_tampil" type="button" onclick="reject_tampil3('<?php echo base_url(); ?>')" class="btn btn-orange">Reject</button>
			</span>

			<span id="vRejc1" style="display:none">
			<button id="btnReject" type="button" onclick="reject1('<?php echo base_url(); ?>')" class="btn btn-danger">Reject</button>
			</span>

			<span id="vRejc2" style="display:none">
			<button id="btnReject" type="button" onclick="reject2('<?php echo base_url(); ?>')" class="btn btn-danger">Reject</button>
			</span>

			<span id="vRejc3" style="display:none">
			<button id="btnReject" type="button" onclick="reject3('<?php echo base_url(); ?>')" class="btn btn-danger">Reject</button>
			</span>

			<input type="text" class="form-control-new" size="50" name="alasan_reject" id="alasan_reject" placeholder="Isi Alasan Reject">

		</div>

		  <div class="panel-body">
              <h6>&nbsp;</h6>
			  &nbsp;
          </div>
		  </form>
        </div>
      </div>
    </div>
    </div>
    <!-- /default modal -->

<script type="text/javascript">
		var save_method;

        function form_tambah_proposal(){
		    save_method = 'add';
			id_='0';
			$('#form')[0].reset(); // reset form on modals
			$('.form-group').removeClass('has-error'); // clear error class
			$('.help-block').empty(); // clear error string
			$('#edit_only_detail').attr('hidden',true);
			$('#edit_only_target').attr('hidden',true);
			$('#TabelDetail1').attr('hidden',false);
			$('#TabelDetail2').attr('hidden',false);

			$("#btnVoid").css("display","none");
			$("#btnApprove").css("display","none");
			$("#btnReject_tampil").css("display","none");
			$("#btnReject").css("display","none");
			$("#alasan_reject").css("display","none");

			$("#view_email").css("display","none");
			$("#teks_email").css("display","none");
			$("#email_ok").css("display","none");
			$("#email_no").css("display","none");

			$("#btnSave").css("display","");
			$("#btnSaveSend").css("display","none");

			$('#default_modal').modal('show'); // show modal
		}

		function save(url)
		{
			/*email = $('#v_email_approve1').val();
			if(email==''){
				alert('Input Email Level Approval 1.');
				$('#v_email_approve1').focus();
				return false;
			}*/

			pic = $('#employee_pic').val();
			kdatv = $('#kdaktivitas').val();

			employee_approve1 = $('#employee_approval1').val();

			if(pic==""){
				alert("PIC Harus Di Pilih.");
				return false;
			}

			if(kdatv==""){
				alert("AKTIVITAS Harus Di Pilih.");
				return false;
			}

			if(employee_approve1==""){
				alert("Employee Approval 1 Harus Di Pilih.");
				return false;
			}

			$('#v_status').val('0');
			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function simpan_kirim(url)
		{
			email = $('#v_email_approve1').val();
			if(email==''){
				alert('Input Email Level Approval 1.');

				$("#view_email").css("display","");
				$('#v_email_approve1').val("");
				$('#v_email_approve1').focus();
				$("#email_no").css("display","");

				$('#v_email_approve1').focus();
				return false;
			}

			$('#v_status').val('1');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function approval1(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('0');
			$('#v_approve3').val('0');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function approval2(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('1');
			$('#v_approve3').val('0');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function approval3(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('1');
			$('#v_approve3').val('1');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function reject1(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('2');
			$('#v_approve2').val('0');
			$('#v_approve3').val('0');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function reject2(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('2');
			$('#v_approve3').val('0');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function reject3(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('1');
			$('#v_approve3').val('2');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function reject2(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('2');
			$('#v_approve3').val('0');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function reject3(url)
		{
			$('#v_status').val('1');
			$('#v_approve1').val('1');
			$('#v_approve2').val('1');
			$('#v_approve3').val('2');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function batal_void(url)
		{
			$('#v_status').val('2');

			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/proposal/save_proposal/";
			} else {
				url_ = url+"index.php/transaksi/proposal/edit_proposal/";
			}

		    $('#btnSave').text('confirm...'); //change button text
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";

			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Proposal Berhasil Disimpan.");
							location.reload();

					},
					error: function(e)
					{
						alert(e);
					}
				});

		}

		function cari_aktivitas(url)
		{
			var act=$("#v_keyword_aktivitas").val();
			$.ajax({
					url: url+"index.php/transaksi/proposal/ajax_aktivitas/",
					data: {id:act},
					type: "POST",
					dataType: 'html',
					success: function(res)
					{

						$('#kdaktivitas').html(res);
					},
					error: function(e)
					{
						alert(e);
					}
				});


   		}

   		function cari_employee_pic(url)
		{
			var act=$("#v_keyword_employee_pic").val();
			$.ajax({
					url: url+"index.php/transaksi/proposal/ajax_employee_pic/",
					data: {id:act},
					type: "POST",
					dataType: 'html',
					success: function(res)
					{

						$('#employee_pic').html(res);
					},
					error: function(e)
					{
						alert(e);
					}
				});


   		}

   		function cari_employee_approval1(url)
		{
			var act=$("#v_keyword_employee_approval1").val();
			$.ajax({
					url: url+"index.php/transaksi/proposal/ajax_employee_approval1/",
					data: {id:act},
					type: "POST",
					dataType: 'html',
					success: function(res)
					{

						$('#employee_approval1').html(res);
					},
					error: function(e)
					{
						alert(e);
					}
				});


   		}



   		function edit_proposal(nopro,url){

			save_method = 'edit';
			$('.form-group').removeClass('has-error');
			$('.error_message').css('display','none');

			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/proposal/edit/"+nopro+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {


						    $.ajax({
								url : url+"index.php/transaksi/proposal/edit_proposal_detail/"+nopro+"/",
								type: "GET",
								dataType: "html",
								success: function(res)
								{
								  $('#edit_only_detail2').html(res);

								},
								error: function(e)
								{
									alert(e);
								}
							});


							$.ajax({
								url : url+"index.php/transaksi/proposal/edit_proposal_target/"+nopro+"/",
								type: "GET",
								dataType: "html",
								success: function(res)
								{
								  $('#edit_only_target2').html(res);

								},
								error: function(e)
								{
									alert(e);
								}
							});


			            $('[name="noproposal"]').val(data.NoProposal);
			            $('[name="namaproposal"]').val(data.NamaProposal);
						$('[name="v_tgl_dokumen"]').val(data.Tanggal);
						$('[name="divisi"]').val(data.KdDivisi);
						$('[name="kdaktivitas"]').val(data.KdAktivitas);
						$('[name="v_tgl_periode_awal"]').val(data.PeriodeAwal);
						$('[name="v_tgl_periode_akhir"]').val(data.PeriodeAkhir);
						$('[name="latar_blk"]').val(data.LatarBelakang);
						$('[name="tujuan"]').val(data.Tujuan);
						$('[name="mekanisme"]').val(data.Mekanisme);
						$('[name="employee_pic"]').val(data.PIC);
						$('[name="v_status"]').val(data.Status);
						$('[name="employee_approval1"]').val(data.Approval1Name);
						$('[name="v_approve1"]').val(data.Status_approve1);
						$('[name="employee_approval2"]').val(data.Approval2Name);
						$('[name="v_approve2"]').val(data.Status_approve2);
						$('[name="employee_approval3"]').val(data.Approval3Name);
						$('[name="v_approve3"]').val(data.Status_approve3);
						$('[name="v_email_approve1"]').val(data.v_email_approve1);

						usr = $('#v_username').val();
						apv1 = data.Approval1Name;
						apv2 = data.Approval2Name;
						apv3 = data.Approval3Name;

						//alert(usr+"#"+apv3);

						sts_apv1 = data.Status_approve1;
						sts_apv2 = data.Status_approve2;
						sts_apv3 = data.Status_approve3;

						//alert(usr+"#"+apv3+"#"+sts_apv1+"#"+sts_apv2+"#"+sts_apv3);

						sts = $('#v_status').val();
						if(sts=='0'){
							$("#btnVoid").css("display","");
							$("#btnApprove").css("display","none");
							$("#btnReject_tampil").css("display","none");
							$("#btnReject").css("display","none");
							$("#alasan_reject").css("display","none");
							$("#btnSave").css("display","");
							$("#btnSaveSend").css("display","");
						}else if(sts=='1'){
							if(usr==apv1 && sts_apv1=='0'){
								$("#btnVoid").css("display","none");
								$("#btnApprove").css("display","");
								$("#btnReject").css("display","none");
								$("#btnReject_tampil").css("display","");
								$("#alasan_reject").css("display","none");
								$("#btnSave").css("display","none");
								$("#btnSaveSend").css("display","none");
								$("#vAprv1").css("display","");
								$("#vRejc1Tamp").css("display","");
							}else if(usr==apv2 && sts_apv1=='1' && sts_apv2=='0'){
								$("#btnVoid").css("display","none");
								$("#btnApprove").css("display","");
								$("#btnReject").css("display","");
								$("#btnReject_tampil").css("display","none");
								$("#alasan_reject").css("display","none");
								$("#btnSave").css("display","none");
								$("#btnSaveSend").css("display","none");
								$("#vAprv2").css("display","");
								$("#vRejc2Tamp").css("display","");
							}else if(usr==apv3 && sts_apv1=='1' && sts_apv2=='1' && sts_apv3=='0'){
								$("#btnVoid").css("display","none");
								$("#btnApprove").css("display","");
								$("#btnReject").css("display","");
								$("#btnReject_tampil").css("display","none");
								$("#alasan_reject").css("display","none");
								$("#btnSave").css("display","none");
								$("#btnSaveSend").css("display","none");
								$("#vAprv3").css("display","");
								$("#vRejc3Tamp").css("display","");
							}
						}else{
							$("#btnVoid").css("display","none");
							$("#btnApprove").css("display","none");
							$("#btnReject_tampil").css("display","none");
							$("#btnReject").css("display","none");
							$("#alasan_reject").css("display","none");
							$("#btnSave").css("display","none");
							$("#btnSaveSend").css("display","none");
						}

						$('#v_keyword_aktivitas').val('');
						$('#v_keyword_employee_pic').val('');
						$('#v_keyword_employee_approval1').val('');

					$('#edit_only_detail').attr('hidden',false);
			        $('#edit_only_target').attr('hidden',false);

			        $('#btnSave').attr('disabled',false);
			        $('#unhide_alasan_reject').attr('hidden',true);
			        $('#default_modal').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}


		function view_proposal(nopro,url){

			save_method = 'edit';
			$('.form-group').removeClass('has-error');
			$('.error_message').css('display','none');

			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/proposal/edit/"+nopro+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {


						    $.ajax({
								url : url+"index.php/transaksi/proposal/edit_proposal_detail/"+nopro+"/",
								type: "GET",
								dataType: "html",
								success: function(res)
								{
								  $('#edit_only_detail2').html(res);

								},
								error: function(e)
								{
									alert(e);
								}
							});


							$.ajax({
								url : url+"index.php/transaksi/proposal/edit_proposal_target/"+nopro+"/",
								type: "GET",
								dataType: "html",
								success: function(res)
								{
								  $('#edit_only_target2').html(res);

								},
								error: function(e)
								{
									alert(e);
								}
							});


			            $('[name="noproposal"]').val(data.NoProposal);
			            $('[name="namaproposal"]').val(data.NamaProposal);
						$('[name="kdaktivitas"]').val(data.Tanggal);
						$('[name="divisi"]').val(data.KdDivisi);
						$('[name="kdaktivitas"]').val(data.KdAktivitas);
						$('[name="v_tgl_periode_awal"]').val(data.PeriodeAwal);
						$('[name="v_tgl_periode_akhir"]').val(data.PeriodeAkhir);
						$('[name="latar_blk"]').val(data.LatarBelakang);
						$('[name="tujuan"]').val(data.Tujuan);
						$('[name="mekanisme"]').val(data.Mekanisme);
						$('[name="employee_pic"]').val(data.PIC);
						$('[name="employee_approval1"]').val(data.Approval1Name);
						$('[name="employee_approval2"]').val(data.Approval2Name);
						$('[name="employee_approval3"]').val(data.Approval3Name);

						$('#v_keyword_aktivitas').val('');
						$('#v_keyword_employee_pic').val('');
						$('#v_keyword_employee_approval1').val('');

					$('#TabelDetail1').attr('hidden',true);
					$('#TabelDetail2').attr('hidden',true);

					$("#btnVoid").css("display","none");
					$("#btnSave").css("display","none");
					$("#btnSaveSend").css("display","none");
					$("#btnApprove").css("display","none");
					$("#btnReject_tampil").css("display","none");
					$("#btnReject").css("display","none");
					$("#alasan_reject").css("display","none");


					$("#view_email").css("display","none");
					$("#teks_email").css("display","none");
					$("#email_ok").css("display","none");
					$("#email_no").css("display","none");


					$('#unhide_alasan_reject').attr('hidden',true);
			        $('#default_modal').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}



		function delete_proposal(id,url)
		{
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url+"index.php/transaksi/proposal/delete_proposal/",
					data: {id:id},
					type: "POST",
					dataType: "html",
					success: function(data)
					{
						    $('#default_modal').modal('hide');
							alert("Proposal Berhasil Dihapus.");
							location.reload();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});

			}
		}

		function detailNew_detail()
		{
			var clonedRow = $("#TabelDetail1 tr:last").clone(true);
			var intCurrentRowId = parseFloat($('#TabelDetail1 tr').length )-2;
			nama = document.getElementsByName("v_ket_detail[]");
			temp = nama[intCurrentRowId].id;
			intCurrentRowId = temp.substr(12,temp.length-12);
			var intNewRowId = parseFloat(intCurrentRowId) + 1;
			$("#v_ket_detail" + intCurrentRowId , clonedRow ).attr( { "id" : "v_ket_detail" + intNewRowId,"value" : ""} );
			$("#v_harga_detail" + intCurrentRowId , clonedRow ).attr( { "id" : "v_harga_detail" + intNewRowId,"value" : ""} );
			$("#v_qty_detail" + intCurrentRowId , clonedRow ).attr( { "id" : "v_qty_detail" + intNewRowId,"value" : 0} );
			$("#btn_del_detail_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_detail_" + intNewRowId} );
			$("#TabelDetail1").append(clonedRow);
			$("#TabelDetail1 tr:last" ).attr( "id", "baris_detail" +intNewRowId ); // change id of last row
			$("#v_ket_detail" + intNewRowId).focus();
			ClearBaris_detail(intNewRowId);
		}

		function ClearBaris_detail(id)
		{
			$("#v_ket_detail"+id).val("");
			$("#v_harga_detail"+id).val("");
			$("#v_qty_detail"+id).val("");
		}

		function detailNew_target()
		{
			var clonedRow = $("#TabelDetail2 tr:last").clone(true);
			var intCurrentRowId = parseFloat($('#TabelDetail2 tr').length )-2;
			nama = document.getElementsByName("v_ket_target[]");
			temp = nama[intCurrentRowId].id;
			intCurrentRowId = temp.substr(12,temp.length-12);
			var intNewRowId = parseFloat(intCurrentRowId) + 1;
			$("#v_ket_target" + intCurrentRowId , clonedRow ).attr( { "id" : "v_ket_target" + intNewRowId,"value" : ""} );
			$("#v_harga_target" + intCurrentRowId , clonedRow ).attr( { "id" : "v_harga_target" + intNewRowId,"value" : ""} );
			$("#v_qty_target" + intCurrentRowId , clonedRow ).attr( { "id" : "v_qty_target" + intNewRowId,"value" : 0} );
			$("#btn_del_detail_target_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_target_" + intNewRowId} );
			$("#TabelDetail2").append(clonedRow);
			$("#TabelDetail2 tr:last" ).attr( "id", "baris_target" +intNewRowId ); // change id of last row
			$("#v_ket_target" + intNewRowId).focus();
			ClearBaris_target(intNewRowId);
		}

		function ClearBaris_target(id)
		{
			$("#v_ket_target"+id).val("");
			$("#v_harga_target"+id).val("");
			$("#v_qty_target"+id).val("");
		}

		function deleteRow_detail(obj)
		{
			objek = obj.id;
			id = objek.substr(22,objek.length-3);

			var lastRow = document.getElementsByName("v_ket_detail[]").length;

			if( lastRow > 1)
			{
				$('#baris_detail'+id).remove();
			}else{
					alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
			}
		}

		function deleteRow_target(obj)
		{
			objek = obj.id;
			id = objek.substr(22,objek.length-3);

			var lastRow = document.getElementsByName("v_ket_target[]").length;

			if( lastRow > 1)
			{
				$('#baris_target'+id).remove();
			}else{
					alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
			}
		}

		function delete_detail(id,brg)
		{

			var url_=$('#base_url').val();
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url_+"index.php/transaksi/proposal/delete_detail/",
					data: {nopro:id,barang:brg},
					type: "POST",
					dataType: "JSON",
					success: function(data)
					{
						//if success reload ajax table
						$('#default_modal').modal('hide');
					    location.reload();
						//reload_table();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});
			}
		}

		function delete_target(id,brg)
		{
			//alert(id+" - "+brg);
			var url_=$('#base_url').val();
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url_+"index.php/transaksi/proposal/delete_target/",
					data: {nopro:id,barang:brg},
					type: "POST",
					dataType: "JSON",
					success: function(data)
					{
						//if success reload ajax table
						$('#default_modal').modal('hide');
					    location.reload();
						//reload_table();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});
			}
		}

		function PopUpPrint(kode, baseurl)
	    {
	        url = "index.php/transaksi/proposal/create_pdf/" + escape(kode);
	        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=1280,height=800,top=50,left=50');
	    }

	    function unhide1(){
			pil = $('#v_approve1').val();
			if(pil=="2"){
				$('#unhide_alasan_reject').attr('hidden',false);
				$('#unhide_alasan_reject').focus();
			}else{
				$('#unhide_alasan_reject').attr('hidden',true);
			}
		}

		function unhide2(){
			pil = $('#v_approve2').val();
			if(pil=="2"){
				$('#unhide_alasan_reject').attr('hidden',false);
				$('#unhide_alasan_reject').focus();
			}else{
				$('#unhide_alasan_reject').attr('hidden',true);
			}
		}

		function unhide3(){
			pil = $('#v_approve3').val();
			if(pil=="2"){
				$('#unhide_alasan_reject').attr('hidden',false);
				$('#unhide_alasan_reject').focus();
			}else{
				$('#unhide_alasan_reject').attr('hidden',true);
			}
		}

		function reject_tampil1(){
			$("#btnVoid").css("display","none");
			$("#btnApprove").css("display","none");
			$("#btnReject").css("display","");
			$("#btnReject_tampil").css("display","none");
			$("#alasan_reject").css("display","");
			$("#btnSave").css("display","none");
			$("#btnSaveSend").css("display","none");
			$("#alasan_reject").val();
			$("#vRejc1").css("display","");
			$("#vAprv1").css("display","none");
			$("#vRejc1Tamp").css("display","none");
		}

		function reject_tampil2(){
			$("#btnVoid").css("display","none");
			$("#btnApprove").css("display","none");
			$("#btnReject").css("display","");
			$("#btnReject_tampil").css("display","none");
			$("#alasan_reject").css("display","");
			$("#btnSave").css("display","none");
			$("#btnSaveSend").css("display","none");
			$("#alasan_reject").val();
			$("#vRejc2").css("display","");
			$("#vAprv2").css("display","none");
			$("#vRejc2Tamp").css("display","none");
		}

		function reject_tampil3(){
			$("#btnVoid").css("display","none");
			$("#btnApprove").css("display","none");
			$("#btnReject").css("display","");
			$("#btnReject_tampil").css("display","none");
			$("#alasan_reject").css("display","");
			$("#btnSave").css("display","none");
			$("#btnSaveSend").css("display","none");
			$("#alasan_reject").val();
			$("#vRejc3").css("display","");
			$("#vAprv3").css("display","none");
			$("#vRejc3Tamp").css("display","none");
		}

		function cek_email_old(){

			var username = $('#employee_approval1').val();
			var url_=$('#base_url').val();
			$("#view_email").css("display","none");
			$("#email_no").css("display","none");
			$("#email_ok").css("display","none");
			$("#teks_email").css("display","");
			// ajax delete data to database
				$.ajax({
					url : url_+"index.php/transaksi/proposal/cari_email/",
					data: {user : username},
					type: "POST",
					dataType: "JSON",
					success: function(data)
					{
					    $("#teks_email").css("display","none");
						$("#view_email").css("display","");
						if(data.email==""){
							$('#v_email_approve1').val("");
							$('#v_email_approve1').focus();
							$("#email_no").css("display","");
						}else{
							$('#v_email_approve1').val(data.email);
							$("#email_ok").css("display","");
						}


					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});

		}

</script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
