<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Mutasi Hutang";
?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_mutasi_hutang/cari/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Periode</td>
	            <td> 
	            	<select id="bulan" name="bulan">
					<?php
					for($a = 0;$a<count($bulan);$a++){
						$select = "";
						if($bulanaktif==$bulan[$a]){
							$select = "selected";
						}
					?>
						<option <?=$select;?> value= "<?=$bulan[$a]?>"><?=$bulan[$a]?></option>
					<?php
					}
					?>
					</select>
					<select id="tahun" name="tahun">
					<?php
					for($a = 0;$a<count($tahun);$a++){
						$select = "";
						if($tahunaktif==$tahun[$a]){
							$select = "selected";
						}
					?>
						<option <?=$select;?> value= "<?=$tahun[$a]?>"><?=$tahun[$a]?></option>
					<?php
					}
					?>
					</select>
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="100">Rekening</td>
	        	<td>
	        		<select  id="kdrekening" name="kdrekening">
					<?php
					for($a = 0;$a<count($kdrekening);$a++){
						$select = "";
						if($kdrekeningaktif == $kdrekening[$a]['KdRekening']){
							$select = "selected";
						}
					?>
						<option <?=$select;?> value= "<?=$kdrekening[$a]['KdRekening'];?>"><?=$kdrekening[$a]['NamaRekening'];?></option>
					<?php
					}
					?>
					</select>
	        	</td>
	        </tr>
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Submit<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php
if($tampilkanRT)
{
	$this->load->view("report/report_mutasi_hutang/reportRT");
}
$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>