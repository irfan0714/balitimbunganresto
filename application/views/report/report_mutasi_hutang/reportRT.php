<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_mutasi_hutang/cari/" onsubmit="return false">
<?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="reportmutasihutang.xls"');
    }
    if ($excel != "excel") {
?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
<?php
    }
?>
    <ol class="breadcrumb">
    	<?php
		for($i=0;$i<count($judul);$i++){
			?>
			<li><strong><?php echo $judul[$i]; ?></strong></li>
		<?php
		}
		?>
	</ol>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
	    <th style="text-align:center" bgcolor="#f3f7bb">Supplier</th>
		<th style="text-align:center" bgcolor="#f3f7bb">Awal</th>
		<th style="text-align:center" bgcolor="#f3f7bb">Beli</th>
		<th style="text-align:center" bgcolor="#f3f7bb">CN</th>
		<th style="text-align:center" bgcolor="#f3f7bb">Bayar</th>
		<th style="text-align:center" bgcolor="#f3f7bb">DN</th>
		<th style="text-align:center" bgcolor="#f3f7bb">Akhir</th>
	</tr>
<?php
	$tawal = 0;
	$tbeli = 0;
	$tcn=0;
	$tbayar=0;
	$tdn=0;
	$ttotal=0;
	for($i=0;$i<count($hasil0);$i++){
		$supplier = $hasil0[$i]['Nama'];
		$awal = $hasil0[$i]['Awal'];
		$beli = $hasil0[$i]['Beli'];
		$cn = $hasil0[$i]['CN'];
		$bayar = $hasil0[$i]['Bayar'];
		$dn = $hasil0[$i]['DN'];
		$total = $awal+$beli+$cn-$bayar-$dn;
		
		$tawal += $awal;
		$tbeli += $beli;
		$tcn += $cn;
		$tbayar += $bayar;
		$tdn += $dn;
		$ttotal += $total;
?>
		<tr>
			<td nowrap align='left' bgcolor="#ccddff"><?=$supplier;?></td>
			<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($awal);?></td>
			<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($beli);?></td>
			<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($cn);?></td>
			<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($bayar);?></td>
			<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($dn);?></td>
			<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($total);?></td>
		</tr>
<?php
	}
?>
	<tr>
		<td nowrap align='center' bgcolor="#f7d7bb">Total</td>
		<td nowrap align='right' bgcolor="#f7d7bb"><?=$mylib->ubah_format($tawal);?></td>
		<td nowrap align='right' bgcolor="#f7d7bb"><?=$mylib->ubah_format($tbeli);?></td>
		<td nowrap align='right' bgcolor="#f7d7bb"><?=$mylib->ubah_format($tcn);?></td>
		<td nowrap align='right' bgcolor="#f7d7bb"><?=$mylib->ubah_format($tbayar);?></td>
		<td nowrap align='right' bgcolor="#f7d7bb"><?=$mylib->ubah_format($tdn);?></td>
		<td nowrap align='right' bgcolor="#f7d7bb"><?=$mylib->ubah_format($ttotal);?></td>
	</tr>
</table>
</form>