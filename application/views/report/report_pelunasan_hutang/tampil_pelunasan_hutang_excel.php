<br>
<table class="table table-bordered" border="1">
	<thead class="title_table">
		<tr>
			<td align="center">No Transaksi</td>
			<td align="center">No. PV</td>
			<td align="center">Tanggal</td>
			<td align="center">Supplier</td>
			<td align="center">Ket</td>
			<td align="center">No. PI</td>
			<td align="center">No. RG</td>
			<td align="center">No. PO</td>
			<td align="center">Nilai</td>
			<td align="center">Bayar</td>
		</tr>		
	</thead>
	<tbody>
	<?php foreach($viewdata AS $val){?>
			<tr>
			<td align="center"><?php echo $val['NoTransaksi'];?></td>
			<td align="center"><?php echo $val['NoPaymentVoucher'];?></td>
			<td align="center"><?php echo $val['Tanggal'];?></td>
			<td align="left"><?php echo $val['Nama'];?></td>
			<td align="left"><?php echo $val['Keterangan'];?></td>
			<td align="center"><?php echo $val['NoFaktur'];?></td>
			<td align="center"><?php echo $val['NoRG'];?></td>
			<td align="center"><?php echo $val['NoPO'];?></td>
			<td align="right"><?php echo ubah_format($val['NilaiFaktur']);?></td>
			<td align="right"><?php echo ubah_format($val['NilaiBayar']);?></td>
			</tr>
	<?php } ?>
			<tr>
				<td align="right" colspan="8"><b>Total</b></td>
				<td align="right"><?php echo ubah_format($total[0]['total_faktur']);?></td>
			   <td align="right"><?php echo ubah_format($total[0]['total_bayar']);?></td>
			</tr>
	</tbody> 
</table>
<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>