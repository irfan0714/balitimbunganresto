<?php
$this->load->view('header');
$reportlib = new report_lib();
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
    function loading() {
        base_url = $("#baseurl").val();
        $('#tgl1').Zebra_DatePicker({format: 'd-m-Y'});
        $('#tgl2').Zebra_DatePicker({format: 'd-m-Y'});
    }
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
//	$("#search").submit();
        document.getElementById("search").submit();
    }

</script>
<body onload="loading()">
    <p>
    <form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_komisikhusus/cari/" onsubmit="return false">
        <div class="col-lg-5">
            <table border="0" class="table table-responsive" >
                <tr>
                    <td nowrap>Pilihan</td>
                    <td nowrap>:</td>
                    <td nowrap>
                        <input type="radio" name="opt" value="DT" <?php echo $cekdt; ?> > <b>Detail</b>
                        <input type="radio" name="opt" value="TR" <?php echo $cektr; ?> > <b>Transaksi</b>
                        <input type="radio" name="opt" value="TL" <?php echo $cektl; ?> > <b>Tour Leader</b>
                        <input type="radio" name="opt" value="TD" <?php echo $cektd; ?> > <b>Tour Leader Detail</b>
                    </td>
                    <?php
                    echo $reportlib->write_textbox_combo("Tanggal", "tgl1", "tgl2", $tgl1, $tgl2, "15", "15", "readonly='readonly'");
                    ?>
                    <td>No Transaksi</td>
                    <td>:</td>
                    <td>
                        <input type="text" name="notrans" id="notrans" value="<?php echo $notrans; ?>" />
                    </td>

                </tr>
                <tr>
                    <td><input type="button" value="Search (*)" onclick="submitThis()">
                        <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                        <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                        <input type='hidden' value='<?= $print ?>' id="print" name="print">
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</body>
<?php
if ($tampilkanDT) {
    $this->load->view("report/komisikhusus/tampil");
}
if ($tampilkanTR) {
    $this->load->view("report/komisikhusus/tampil_TR");
}
if ($tampilkanTL) {
    $this->load->view("report/komisikhusus/tampil_TL");
}
if ($tampilkanTD) {
    $this->load->view("report/komisikhusus/tampil_TD");
}
$this->load->view('footer');
?>