<?php
if ($excel != "excel") {
	$this->load->view('header');
}
$reportlib = new report_lib();
?>
<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/outstanding_um_supplier/" >
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="outstanding_um_supplier.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i>Daftar Uang Muka Supplier Belum Lunas</strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">Nama
                	<input type="hidden" name='excel' id="excel" value=""/>
                </td>
                <td style="vertical-align: middle; text-align: center;">Tanggal</td>
                <td style="vertical-align: middle; text-align: center;">No. Dokumen</td>
                <td style="vertical-align: middle; text-align: center;">No. PV</td>
                <td style="vertical-align: middle; text-align: center;">Keterangan</td>
                <td style="vertical-align: middle; text-align: center;">Sisa</td>
                
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			$total = 0;
			foreach($hasil as $rec){
				$nama = $rec['Nama'];	
				$nodokumen = $rec['NoDokumen'];
				$nopv = $rec['NoPV'];
				$tgldokumen = $rec['TglDokumen'];
				$keterangan = $rec['Keterangan'];
				$sisa = $rec['Sisa'];
				$total += $sisa;
			?>
				<tr>
					<td nowrap style="text-align: left;"><?=$nama;?></td>
					<td nowrap style="text-align: left;"><?=$tgldokumen;?></td>
					<td nowrap style="text-align: left;"><?=$nodokumen;?></td>
					<td nowrap style="text-align: left;"><?=$nopv;?></td>
					<td nowrap style="text-align: left;"><?=$keterangan;?></td>
					<td nowrap style="text-align: right;"><?=ubah_format($sisa);?></td>
				</tr>
			<?php
			}	
			?>
			<tr>
				<td colspan="5" style="text-align: center;">TOTAL</td></td>
				<td nowrap style="text-align: right;"><?=ubah_format($total);?></td>
			</tr>
			</tbody>
		</table>
	</div>
</form>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
