<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_pendapatan_kasir/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="pendaptan_kasir.xls"');
	}
    
    if($excel == "Excel")
    {
        $table_border = 1;
        $style = "style='border-collapse: collapse;'";
    }else{
		$table_border = 0;
        $style = "style='border-collapse: collapse;'";
	}
    
    
    
	if($excel == "Excel"){
	?>
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">REPORT PENDAPATAN KASIR</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
	<?php
	}
	?>
	
	<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'" '.$style.'>
		<thead>
	    	<tr>
		        <th rowspan="2" style="vertical-align: middle; text-align: center;">Tanggal</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: center;">Tunai</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: center;">Kredit</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: center;">Debit</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: center;">Voucher</th>
		        <th colspan="3" style="vertical-align: middle; text-align: center;">USD</th>
		        <th colspan="3" style="vertical-align: middle; text-align: center;">CNY</th>
		        <th rowspan="2" style="vertical-align: middle; text-align: center;">Total</th>
		    </tr>
		    <tr>
		        <th style="vertical-align: middle; text-align: center;">USD</th>
		        <th style="vertical-align: middle; text-align: center;">Kurs</th>
		        <th style="vertical-align: middle; text-align: center;">Rp</th>
		        <th style="vertical-align: middle; text-align: center;">CNY</th>
		        <th style="vertical-align: middle; text-align: center;">Kurs</th>
		        <th style="vertical-align: middle; text-align: center;">Rp</th>
		    </tr>
		</thead>
		<tbody>
		<?php
		$tot_tunai = 0;
		$tot_kkredit = 0;
		$tot_kdebit= 0;
		$tot_voucher = 0;
		$tot_usd = 0;
		$tot_usdrp = 0;
		$tot_cny = 0;
		$tot_cnyrp=0;
		$tot_totalnilai=0;
    	foreach($data as $val)
    	{
    		$kursusd = $val['USD'] > 0 ? $val['USDRp']/$val['USD'] : 0;
    		$kurscny = $val['CNY'] > 0 ? $val['CNYRp']/$val['CNY'] : 0;
    ?>    
    		<tr>
		        <td align="center"><?=$val['Tanggal'];?></td>
		        <td align="right"><?=number_format($val['Tunai'],0,',','.');?></td>
		        <td align="right"><?=number_format($val['KKredit'],0,',','.');?></td>
		        <td align="right"><?=number_format($val['KDebit'],0,',','.');?></td>
		        <td align="right"><?=number_format($val['Voucher'],0,',','.');?></td>
		        <td align="right"><?=number_format($val['USD'],2,',','.');?></td>
		        <td align="right"><?=number_format($kursusd,0,',','.');?></td>
		        <td align="right"><?=number_format($val['USDRp'],0,',','.');?></td>
		        <td align="right"><?=number_format($val['CNY'],2,',','.');?></td>
		        <td align="right"><?=number_format($kurscny,0,',','.');?></td>
		        <td align="right"><?=number_format($val['CNYRp'],0,',','.');?></td>
		        <td align="right"><?=number_format($val['TotalNilai'],0,',','.');?></td>
	       </tr>
	<?php
			$tot_tunai += $val['Tunai'];
			$tot_kkredit += $val['KKredit'];
			$tot_kdebit += $val['KDebit'];
			$tot_voucher += $val['Voucher'];
			$tot_usd += $val['USD'];
			$tot_usdrp += $val['USDRp'];
			$tot_cny += $val['CNY'];
			$tot_cnyrp += $val['CNYRp'];
			$tot_totalnilai += $val['TotalNilai'];
    	}
    ?>
    	<tr>
    		<td align="center">Total</td>
	        <td align="right"><?=number_format($tot_tunai,0,',','.');?></td>
	        <td align="right"><?=number_format($tot_kkredit,0,',','.');?></td>
	        <td align="right"><?=number_format($tot_kdebit,0,',','.');?></td>
	        <td align="right"><?=number_format($tot_voucher,0,',','.');?></td>
	        <td align="right"><?=number_format($tot_usd,2,',','.');?></td>
	        <td align="right">&nbsp;</td>
	        <td align="right"><?=number_format($tot_usdrp,0,',','.');?></td>
	        <td align="right"><?=number_format($tot_cny,2,',','.');?></td>
	        <td align="right">&nbsp;</td>
	        <td align="right"><?=number_format($tot_cnyrp,0,',','.');?></td>
	        <td align="right"><?=number_format($tot_totalnilai,0,',','.');?></td>
    	</tr>
		</tbody>
	</table>
</form>
	</div>
</div>