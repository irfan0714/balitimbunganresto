<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Rencana Bayar";

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_rencana_bayar/cari/">      
        	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            	<table class="table table-bordered responsive">
                	<thead>
                    	<tr>
                        	<th width="100">Tanggal</th>
                        	<th>: 
                            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date; ?>" name="v_date" id="v_date" size="10" maxlength="10">
                            	&nbsp;s/d&nbsp;
                            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_date2; ?>" name="v_date2" id="v_date2" size="10" maxlength="10">
                        	</th>
                    	</tr>
                    	<tr>
                        	<td>&nbsp;</td>
                        	<td>
                        		<input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                            	<input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                        	</td>
                    	</tr>
                	</thead>
                
            	</table> 
            </div>
	    </form> 
	</div>
</div>

<?php
if ($tampilkan) 
{
   	$this->load->view("report/report_rencana_bayar/reportdtl");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>