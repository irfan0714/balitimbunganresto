<?php
$mylib = new globallib();

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_rencana_bayar/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="rencanabayar.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive" border="1">
            <tr class="title_table">
                <td style="vertical-align: middle; text-align: center;">Kas/Bank</td>
                <td style="vertical-align: middle; text-align: center;">Tgl</td>
                <td style="vertical-align: middle; text-align: center;">No Rencana</td>
                <td style="vertical-align: middle; text-align: center;">Keterangan</td>
                <td style="vertical-align: middle; text-align: center;">Nilai Faktur</td>
                <td style="vertical-align: middle; text-align: center;">PPH</td>
                <td style="vertical-align: middle; text-align: center;">Admin</td>
                <td style="vertical-align: middle; text-align: center;">Pembulatan</td>
                <td style="vertical-align: middle; text-align: center;">Rencana Bayar</td>
                <td style="vertical-align: middle; text-align: center;">Jt Tempo</td>
                <td style="vertical-align: middle; text-align: center;">Nama</td>
                <td style="vertical-align: middle; text-align: center;">No Rekening</td>
                <td style="vertical-align: middle; text-align: center;">No. PV</td>
                <td style="vertical-align: middle; text-align: center;">Jumlah Dibayar</td>
            </tr>
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}else{
				$prev_bank = '';			
				$subtotNilaiFaktur = 0;
				$subtotPPH= 0;
				$subtotAdmin=0;
				$subtotPembulatan=0;
				$subbayar = 0;
				$subjumlahpayment = 0;
				$totNilaiFaktur = 0;
				$totPPH= 0;
				$totAdmin=0;
				$totPembulatan=0;
				$totbayar = 0;
				$totjumlahpayment=0;
				for($i=0;$i<count($hasil);$i++)
				{		
					$namakasbank = $hasil[$i]['NamaKasBank'];
					$tgldokumen = $hasil[$i]['TglDokumen'];
					$norencanabayar = $hasil[$i]['NoRencanaBayar'];
					$keterangan = $hasil[$i]['Keterangan'];
					$nilaifaktur = $hasil[$i]['NilaiFaktur'];
					$nilaipph = $hasil[$i]['NilaiPPH'];
					$biayaadmin = $hasil[$i]['BiayaAdmin'];
					$pembulatan = $hasil[$i]['Pembulatan'];
					$jatuhtempo = $hasil[$i]['JatuhTempo'];
					$nama = $hasil[$i]['Nama'];
					$norekening = $hasil[$i]['NoRekening'];
					$namabank = $hasil[$i]['NamaBank'];
					$namapemilik = $hasil[$i]['NamaPemilik'];
					$nopv = $hasil[$i]['NoPaymentVoucher'];
					$jumlahpayment = $hasil[$i]['JumlahPayment'];
					
					$bayar = $nilaifaktur - $nilaipph + $biayaadmin + $pembulatan;
					$subtotNilaiFaktur += $nilaifaktur;
					$subtotPPH += $nilaipph;
					$subtotAdmin += $biayaadmin;
					$subtotPembulatan += $pembulatan;
					$subbayar += $bayar;
					$subjumlahpayment += $jumlahpayment;
					$totNilaiFaktur += $nilaifaktur;
					$totPPH += $nilaipph;
					$totAdmin += $biayaadmin;
					$totPembulatan += $pembulatan;
					$totbayar += $bayar;
					$totjumlahpayment += $jumlahpayment;
					
					?>
					<tr>
					<?php
						if($prev_bank == $namakasbank){
							$namakasbank_echo = '';
						}else{
							$namakasbank_echo = $namakasbank;
						}
						
					?>
						<td nowrap><?= $namakasbank_echo; ?></td>
						<td nowrap><?= $tgldokumen; ?></td>
						<td nowrap><?= $norencanabayar; ?></td>
						<td nowrap><?= $keterangan; ?></td>
						<td nowrap style="text-align: right;"><?= ubah_format($nilaifaktur); ?></td>
						<td nowrap style="text-align: right;"><?= ubah_format($nilaipph); ?></td>
						<td nowrap style="text-align: right;"><?= ubah_format($biayaadmin); ?></td>
						<td nowrap style="text-align: right;"><?= ubah_format($pembulatan); ?></td>
						<td nowrap style="text-align: right;"><?= ubah_format($bayar); ?></td>
						<td nowrap><?= $jatuhtempo; ?></td>
						<td nowrap><?= $nama; ?></td>
						<td nowrap><?= $norekening; ?></td>
						<td nowrap><?= $nopv; ?></td>
						<td nowrap style="text-align: right;"><?= ubah_format($jumlahpayment); ?></td>
					</tr>
				<?php
					if($i<count($hasil)-1){
						if($namakasbank != $hasil[$i+1]['NamaKasBank']){
						?>
						<tr>
							<td colspan="4" style="text-align: center;font-weight: bold;">Sub Total <?=$namakasbank;?></td>
							<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotNilaiFaktur);?></td>
							<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotPPH);?></td>
							<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotAdmin);?></td>
							<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotPembulatan);?></td>
							<td style="text-align: right;font-weight: bold;"><?=ubah_format($subbayar);?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td style="text-align: right;font-weight: bold;"><?=ubah_format($subjumlahpayment);?></td>
						</tr>
						<?php	
							$subtotNilaiFaktur = 0;
							$subtotPPH= 0;
							$subtotAdmin=0;
							$subtotPembulatan=0;
							$subbayar = 0;
							$subjumlahpayment = 0;
						}
					}
					
					$prev_bank = $namakasbank;
				}
				?>
				</tbody>
				<tr>
					<td colspan="4" style="text-align: center;font-weight: bold;">Sub Total <?=$namakasbank;?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotNilaiFaktur);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotPPH);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotAdmin);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($subtotPembulatan);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($subbayar);?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($subjumlahpayment);?></td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: center;font-weight: bold;">Grand Total</td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($totNilaiFaktur);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($totPPH);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($totAdmin);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($totPembulatan);?></td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($totbayar);?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="text-align: right;font-weight: bold;"><?=ubah_format($totjumlahpayment);?></td>
				</tr>
			<?php
			}
			?>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>
