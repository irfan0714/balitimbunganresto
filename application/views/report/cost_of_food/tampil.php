<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_cost_of_good/search_report/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($excel == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="report_cost_of_food.xls"');
}
if($excel!="excel")
{ 
?>
<div style="margin-left:10px">
<input name='submit' type='submit' value='export to excel'
                   onclick="$('#excel').val('excel');
                   document.getElementById('search').submit()">
</div>
<?php
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul?></font></strong></td>
	</tr>
</table>
<p>

<?php
if($excel=="excel")
{
	$table = '<table border="1" cellpadding="0" cellspacing="0" width="100%">';
}
else
{
	$table = '<table class="table table-bordered responsive">';	
}
	
echo $table;
?>    	
<thead class="title_table">
	<tr>
		<th>Sub Kategori</th>
		<th>Type</th>
		<th>PCode</th>
		<th>Nama Barang</th>
		<th>Satuan</th>
		<th>Qty</th>
		<th>Nilai</th>
	</tr>
</thead>
<tbody>
	<?php
	if(count($data)==0)
	{
		echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
	}
		
	$prev_purpose = "";
	$prev_subkategori='';
	$subtotalpurpose = 0;
	$subtotalsubkategori = 0;
	
	for($i=0;$i<count($data);$i++){
		
		$subkategori = $data[$i]['NamaSubKategori'];	
		$purpose = $data[$i]['purpose'];
		$pcode = $data[$i]['PCode'];
		$namabarang = $data[$i]['NamaLengkap'];
		$qty = $data[$i]['Qty'];
		$nilai = $data[$i]['Nilai'];
		$satuan = $data[$i]['Satuan'];
				
		if($purpose != $prev_purpose or $subkategori != $prev_subkategori){
			if($prev_purpose != ''){
			?>
				<tr>
					<td colspan="6" align="center">SUB TOTAL <?=$prev_purpose;?> </td>
					<td align="right"><?= number_format($subtotalpurpose); ?></td>
				</tr>
			<?php
				$subtotalpurpose = 0;
			}
		}
		
		if($subkategori != $prev_subkategori){
			if($prev_subkategori != ''){
				?>
				<tr>
					<td colspan="6" align="center">TOTAL <?=$prev_subkategori;?></td>
					<td align="right"><?= number_format($subtotalsubkategori); ?></td>
				</tr>
			<?php
				$subtotalsubkategori = 0;	
			}
		}
		?>
		<tr>
		<?php
			if($subkategori == $prev_subkategori){
				$p_subkategori = "<td>&nbsp;</td>";
			}else{
				$p_subkategori = "<td>$subkategori</td>";
			}
			echo $p_subkategori;
			
			if($purpose != $prev_purpose || $subkategori != $prev_subkategori ){
				$p_purpose = "<td>$purpose</td>";
			}else{
				$p_purpose = "<td>&nbsp;</td>";
			}
			echo $p_purpose;
		?>
			<td><?= $pcode; ?></td>
			<td><?= $namabarang; ?></td>
			<td><?= $satuan; ?></td>
			<td align="right"><?= number_format($qty); ?></td>
			<td align="right"><?= number_format($nilai); ?></td>
		</tr>
		<?php
			$subtotalpurpose += $nilai;
			$subtotalsubkategori += $nilai;
			$prev_purpose = $purpose;
			$prev_subkategori = $subkategori;
	}
	?>
	<tr>
		<td colspan="6" align="center">SUB TOTAL <?=$purpose;?></td>
		<td align="right"><?= number_format($subtotalpurpose); ?></td>
	</tr>
	<tr>
		<td colspan="6" align="center">TOTAL <?=$subkategori;?></td>
		<td align="right"><?= number_format($subtotalsubkategori); ?></td>
	</tr>

	<tr><td colspan="100%">&nbsp;</td></tr>
</tbody>
</table>
</form> 
    	