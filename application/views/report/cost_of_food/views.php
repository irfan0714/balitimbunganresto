<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script language="javascript">
function loading(){
	base_url = $("#baseurl").val();
    $('#tgl1a').Zebra_DatePicker({format: 'd-m-Y'});
    $('#tgl1b').Zebra_DatePicker({format: 'd-m-Y'});
    $('#tgl2a').Zebra_DatePicker({format: 'd-m-Y'});
    $('#tgl2b').Zebra_DatePicker({format: 'd-m-Y'});
}
function submitThis()
{
	$("#excel").val("");
	$("#print").val("");
    document.getElementById("search").submit();
//	$("#search").submit();
}

</script>
<body onload="loading()">
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_cost_of_food/search_report/" onsubmit="return false">
	<table class="table table-bordered responsive">   
    	<tr>
        	<td class="title_table" width="150">Periode <font color="red"><b>(*)</b></font></td>
        	<td> 
        		<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
        		s/d
        		<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
        	
            </td>
        </tr>                     
	        
    	<tr>
    	    <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
        	<td> 
        		<select class="form-control-new" name="gudang" id="gudang" style="width: 200px;">
            		<option value="">Pilih Gudang</option>
            		<?php
            		foreach($gudang as $val)
            		{
            			$selected = "";
            			if($flag)
            			{
            				if($v_gudang==$val['KdGudang'])
            				{
								$selected = 'selected="selected"';	
							}
						}
						
						?><option value="<?php echo $val['KdGudang']; ?>" <?php echo $selected; ?>><?php echo $val['Keterangan']; ?></option><?php
					}
            		?>
            	</select>  
            </td>
    	</tr>
	        
    	<tr>
            <td class="title_table">Sub Kategori <font color="red"><b>(*)</b></font></td>
            <td> 
            	<select class="form-control-new" name="v_subkategori" id="v_subkategori" style="width: 200px;">
            		<option value="">-Semua Sub Kategori-</option>
            		<?php
            		foreach($subkategori as $val)
            		{
            			$selected = "";
            			if($flag)
            			{
            				if($v_subkategori==$val['KdSubKategori'])
            				{
								$selected = 'selected="selected"';	
							}
						}
						
						?>
						<option value="<?php echo $val['KdSubKategori']; ?>" <?php echo $selected; ?>><?php echo $val['NamaSubKategori']; ?></option>
					<?php
					}
            		?>
            	</select>  
            </td>
        </tr>
	        
    	<tr>
    		<td class="title_table">&nbsp;</td>
        	<td colspan="2" align="left"><input type="button" value="Search (*)" onclick="submitThis()">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='hidden' value='<?=$excel?>' id="excel" name="excel">
				<input type='hidden' value='<?=$print?>' id="print" name="print">
        	</td>
    	</tr>
    
	</table>
        
    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font><br/><br/><br/>
</form>
</body>
<?php
if($tampilkan)
{
	$this->load->view("report/cost_of_food/tampil");
}
$this->load->view('footer'); ?>
