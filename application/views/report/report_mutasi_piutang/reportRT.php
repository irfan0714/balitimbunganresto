<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_mutasi_piutang/cari/" onsubmit="return false">
<?php
$mylib = new globallib();
if ($print == "excel"){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="reportmutasipiutang.xls"');
}
?>
<table align="left" border="0" cellpadding="3" cellspacing="3" style="border-collapse: collapse;margin-left:10px">
	<tr>
		<td nowrap colspan="<?count($judul)?>"><strong><font face="Arial" size="2">Laporan Mutasi Piutang</font></strong></td>
	</tr>
<?php
	for($a=0;$a<count($judul);$a++)
	{
	?>
	<tr>
		<td nowrap><strong><font face="Arial" size="2"><?=$judul[$a]?></font></strong></td>
	</tr>
	<?php
	}
?>
</table>
<p>
<table border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse;margin-left:10px;margin-top:10px;margin-bottom:10px" bordercolor="#111111" width="100%">
	<tr>
	    <th align="center" bgcolor="#f3f7bb">Tanggal</th>
		<th align="center" bgcolor="#f3f7bb">Jenis</th>
		<th align="center" bgcolor="#f3f7bb">Nomor Bukti</th>
		<th align="center" bgcolor="#f3f7bb">Nilai Masuk</th>
		<th align="center" bgcolor="#f3f7bb">Nilai Keluar</th>
		<th align="center" bgcolor="#f3f7bb">Nilai Akhir</th>
<?php
	if((count($hasil0)+count($hasil1))>0){
		$total6 = 0;
		if(count($hasil0)>0)
		{
	    ?>
	    <tr>
				<td nowrap align='center' bgcolor='#f7d7bb' colspan="7"><b>Saldo Awal</b></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil0->NilaiAwal)?></td>
		</tr>
		<?php
		$total6 = $hasil0->NilaiAwal;
		}
		$total2 = 0;
		$total4 = 0;
		
		for($s=0;$s<count($hasil1);$s++)
		{
		    if($hasil1[$s]['TipeTransaksi']=="I")
			{
			$total2 += (float)$hasil1[$s]['Jumlah'];
			$total6 += (float)$hasil1[$s]['Jumlah'];
			}
			else
			{
			$total4 += (float)$hasil1[$s]['Jumlah'];
			$total6 -= (float)$hasil1[$s]['Jumlah'];
			}	
	?>
			<tr>
				<td nowrap bgcolor="#ccddff"><?=$mylib->ubah_format_tanggal($hasil1[$s]['Tanggal'])?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil1[$s]['TipeTransaksi']?></td>
				<td nowrap bgcolor="#ccddff"><?=$hasil1[$s]['NoDokumen']?></td>
				<?php
				if($hasil1[$s]['TipeTransaksi']=="I")
			    {   
				?>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil1[$s]['Jumlah'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format(0)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($total6)?></td>
				<?php
				}else
				{
				?>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format(0)?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($hasil1[$s]['Jumlah'])?></td>
				<td nowrap align='right' bgcolor="#ccddff"><?=$mylib->ubah_format($total6)?></td>
				<?php
				}
				?>
			</tr>
	<?php
	    }
		?>
		<tr>
			<td nowrap align='center' bgcolor='#f7d7bb' colspan="3"><b>Total</b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total2)?></b></td>
			<td nowrap align='right' bgcolor='#f7d7bb'><b><?=$mylib->ubah_format($total4)?></b></td>
			<td nowrap align='center' bgcolor='#f7d7bb'>&nbsp;</td>
		</tr>
	<?php
	}
	else
	{ ?>
	<tr>
		<td nowrap align='center' bgcolor='#f7d7bb' colspan="6"><b>Tidak ada data</b></td>
	</tr>
<?php
	}
	?>
</table>
</form>