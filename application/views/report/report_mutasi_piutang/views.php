<?php
$this->load->view('header'); 
$reportlib = new report_lib();
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
function submitThis()
{
    if($("#customer").val()=="")
	{
	   alert("Customer harus di pilih.");
	}
	else
	{
	$("#print").val("screen");
	$("#search").submit();
	}
}

function submit2This()
{
	$("#print").val("excel");
	$("#search").submit();
}

</script>
<body>
<p>
<form method="POST" name="search" id="search" action="<?=base_url()?>/index.php/report/report_mutasi_piutang/cari/" onsubmit="return false">
<table border="2" style="margin-left:10px" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="3">
			    <tr>
					<td nowrap>Tahun</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="tahun" name="tahun">
					<option value="">--Select--</option>
					<?php
					for($a = 0;$a<count($tahun);$a++){
						$select = "";
						if($tahunaktif==$tahun[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$tahun[$a]?>"><?=$tahun[$a]?></option>
					<?php
					}
					?>
					</select>
				</tr>

				<tr>
					<td nowrap>Bulan</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="bulan" name="bulan">
					<option value="">--Select--</option>
					<?php
					for($a = 0;$a<count($bulan);$a++){
						$select = "";
						if($bulanaktif==$bulan[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$bulan[$a]?>"><?=$bulan[$a]?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr bordercolor="#FFFFFF">
		<td colspan="2" align="center"><input type="button" value="Screen" onclick="submitThis()">
		<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
		<input type='hidden' value='<?=$userlevel?>' id="userlevel" name="userlevel">
		<input type='button' value='Excel' id="excel" name="excel" onclick="submit2This()">
		<input type='hidden' value='<?=$print?>' id="print" name="print">
		</td>
	</tr>
</table>
</form>
</body>
<?php
if($tampilkanRT)
{
	$this->load->view("report/report_mutasi_piutang/reportRT");
}
$this->load->view('footer'); ?>