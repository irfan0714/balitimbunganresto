<br>
<table class="table table-bordered" border="1">
	<thead class="title_table">
		<tr>
			<td align="center">Tanggal</td>
			<td align="center">No. RG</td>
			<td align="center">No. PO</td>
			<td align="center">No. Faktur</td>
			<td align="center">No Faktur Supplier</td>
			<td align="center">No. Faktur Pajak</td>
			<td align="center">Supplier</td>
			<td align="center">DPP</td>
			<td align="center">PPN</td>
			<td align="center">Total</td>
		</tr>		
	</thead>
	<tbody>
	<?php 
		$totdpp = 0;
		$tottax =0;
		$tottotal = 0;
		foreach($viewdata AS $val){
	?>
			<tr>
			<td align="center"><?php echo $val['Tanggal'];?></td>
			<td align="center"><?php echo $val['NoPenerimaan'];?></td>
			<td align="center"><?php echo $val['NoPO'];?></td>
			<td align="left"><?php echo $val['NoFaktur'];?></td>
			<td align="left"><?php echo $val['NoFakturSupplier'];?></td>
			<td align="left"><?php echo $val['NoFakturPajak'];?></td>
			<td align="left"><?php echo $val['Nama'];?></td>
			<td align="right"><?php echo ubah_format($val['DPP']);?></td>
			<td align="right"><?php echo ubah_format($val['Tax']);?></td>
			<td align="right"><?php echo ubah_format($val['Total']);?></td>
			</tr>
	<?php
			$totdpp += $val['DPP'];
			$tottax += $val['Tax'];
			$tottotal += $val['Total'];
		} 
	?>
			<tr>
				<td align="right" colspan="7"><b>Total</b></td>
				<td align="right"><?php echo ubah_format($totdpp);?></td>
				<td align="right"><?php echo ubah_format($tottax);?></td>
				<td align="right"><?php echo ubah_format($tottotal);?></td>
			</tr>
	</tbody> 
</table>

<?
function ubah_format($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>