<?php

unset($arr_data);

foreach($hasil as $val)
{
	$arr_data["list_transaksi"][$val["KdDivisi"]]=$val["KdDivisi"];
	//$arr_data["kddivisi"][$val["NoStruk"]]=$val["KdDivisi"];
	$arr_data["namadivisi"][$val["KdDivisi"]]=$val["NamaDivisi"];
	$arr_data["list_transaksi_detail"][$val["KdDivisi"]][$val["PCode"]]=$val["PCode"];
	
	//$arr_data["pcode"][$val["NoStruk"]]=$val["PCode"];
	$arr_data["namalengkap"][$val["KdDivisi"]][$val["PCode"]]=$val["NamaLengkap"];
	$arr_data["qty"][$val["KdDivisi"]][$val["PCode"]]=$val["Qty"];
	$arr_data["harga"][$val["KdDivisi"]][$val["PCode"]]=$val["Harga"];
	$arr_data["bruto"][$val["KdDivisi"]][$val["PCode"]]=$val["Bruto"];
	$arr_data["discountdetail"][$val["KdDivisi"]][$val["PCode"]]=$val["DiscDetail"];
	$arr_data["netto"][$val["KdDivisi"]][$val["PCode"]]=$val["Netto"];
}

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
	    	<thead>
                <tr>
                    <th width="30" style="vertical-align: middle; text-align: center;">No</th>
                    <th style="vertical-align: middle; text-align: center;">Divisi</th>                    
                    <th style="text-align: center; background: #73e8e4;">PCode</th>
                    <th style="text-align: center; background: #73e8e4;">Nama</th>
					<th style="text-align: center; background: #73e8e4;">Qty</th>
                    <th style="text-align: center; background: #73e8e4;">Harga</th>
                    <th style="text-align: center; background: #73e8e4;">Bruto</th>
					<th style="text-align: center; background: #73e8e4;">Disc</th>
					<th style="text-align: center; background: #73e8e4;">Netto</th>
                </tr>
                
			</thead>
			<tbody>
			
			<?php
			if(count($arr_data["list_transaksi"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$no=1;
			$tempdivisi = "";
			foreach($arr_data["list_transaksi"] as $kddivisi => $val)
			{		
				
				//$kddivisi = $arr_data["kddivisi"][$nostruk];
				$namadivisi = $arr_data["namadivisi"][$kddivisi];
				
				foreach($arr_data["list_transaksi_detail"][$kddivisi] as $pcode=>$val)
				{
				
				$namalengkap = $arr_data["namalengkap"][$kddivisi][$pcode];
				$qty = $arr_data["qty"][$kddivisi][$pcode];
				$harga = $arr_data["harga"][$kddivisi][$pcode];
				$bruto = $arr_data["bruto"][$kddivisi][$pcode];
				$discountdetail = $arr_data["discountdetail"][$kddivisi][$pcode];
				$netto = $arr_data["netto"][$kddivisi][$pcode];
				$disc_all = $bruto - $netto;				
				
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $namadivisi." :: ".$namalengkap; ?>"><?php echo $no; ?></td>
					<td nowrap title="<?php echo $namadivisi." :: ".$namalengkap; ?>"><?php echo $namadivisi;/*if($tempdivisi!=$kddivisi){echo $namadivisi; }else{ echo "&nbsp;";}*/ ?></td>
					
					<td nowrap title="PCode = <?php echo $pcode; ?>" align="left"><?php echo $pcode; ?></td>
					<td nowrap title="Nama = <?php echo $namalengkap; ?>" align="left"><?php echo $namalengkap; ?></td>
					<td nowrap title="Qty = <?php echo $mylib->format_number($qty); ?>" align="right"><?php echo $mylib->format_number($qty); ?></td>
					<td nowrap title="Harga = <?php echo $mylib->format_number($harga); ?>" align="right"><?php echo $mylib->format_number($harga); ?></td>
					<td nowrap title="Bruto = <?php echo $mylib->format_number($bruto); ?>" align="right"><?php echo $mylib->format_number($bruto); ?></td>
					<td nowrap title="Disc = <?php echo $mylib->format_number($disc_all); ?>" align="right"><?php echo $mylib->format_number($disc_all); ?></td>					
					<td nowrap style="border-right-color: #73e8e4 ;"  title="Netto = <?php echo $mylib->format_number($netto); ?>" align="right"><?php echo $mylib->format_number($netto); ?></td>				
										
				</tr>
				<?php
	
				$arr_data["gt_qty"] += $qty;
				//$arr_data["gt_harga"] += $harga;
				$arr_data["gt_bruto"] += $bruto;
				$arr_data["gt_disc"] += $disc_all;
				$arr_data["gt_netto"] += $netto;
				
				$no++;
				$tempdivisi = $kddivisi;
				}
			}

			if(count($arr_data["list_transaksi"])>0)
			{
			?>
			<thead>
				<tr>
					<td colspan="4" align="right"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo $mylib->format_number($arr_data["gt_qty"]); ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo $mylib->format_number($arr_data["gt_bruto"]); ?></b></td>					
					<td align="right"><b><?php echo $mylib->format_number($arr_data["gt_disc"]); ?></b></td>
					<td align="right" style="border-right-color: #73e8e4;"><b><?php echo $mylib->format_number($arr_data["gt_netto"]); ?></b></td>
					
				</tr>
			</thead>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>