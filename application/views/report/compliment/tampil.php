<?php
$mylib = new globallib();

unset($arr_data);

$arr_data = array();
$arr_data["list_compliment"] = array();

foreach($hasil as $val)
{
	$arr_data["list_compliment"][$val["NoStruk"]]=$val["NoStruk"];
	
	$arr_data["kddivisi"][$val["NoStruk"]]=$val["KdDivisi"];
	$arr_data["namadivisi"][$val["NoStruk"]]=$val["NamaDivisi"];
	$arr_data["nokassa"][$val["NoStruk"]]=$val["NoKassa"];
	$arr_data["tanggal"][$val["NoStruk"]]=$val["Tanggal"];
	$arr_data["kasir"][$val["NoStruk"]]=$val["Kasir"];
	$arr_data["discount"][$val["NoStruk"]]=$val["Discount"]; 
}

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<script>
	function PopUpVoucher(id)
    {
		base_url = "<?php echo base_url(); ?>";
		url = base_url+"index.php/report/report_kasir/pop_up_detail_voucher/index/"+id+"/";
		windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_ticketing/cari/" onsubmit="return false">
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportCompliment.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
	    	<thead>
                <tr>
                    <th width="30" style="vertical-align: middle; text-align: center;">No</th>
                    <th style="vertical-align: middle; text-align: center;">No Struk</th>
                    <th style="vertical-align: middle; text-align: center;">Tanggal</th>
                    <th style="vertical-align: middle; text-align: center;">Kasir</th>
                    <th style="vertical-align: middle; text-align: center;">Compliment</th>
                </tr>
			</thead>
			<tbody>
			
			<?php
			if(count($arr_data["list_compliment"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$arr_data["gt_discount"] ="";
			$no=1;
			foreach($arr_data["list_compliment"] as $nostruk => $val)
			{		
				$kddivisi = $arr_data["kddivisi"][$nostruk];
				$namadivisi = $arr_data["namadivisi"][$nostruk];
				$nokassa = $arr_data["nokassa"][$nostruk];
				$tanggal = $arr_data["tanggal"][$nostruk];
				$kasir = $arr_data["kasir"][$nostruk];
				$discount = $arr_data["discount"][$nostruk];
				
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="">
					<td nowrap align="center" ><?php echo $no; ?></td>
					<td nowrap><?php echo $nostruk; ?></td>
					<td nowrap align="center"><?php echo $mylib->ubah_tanggal($tanggal); ?></td>
					<td nowrap><?php echo $kasir; ?></td>
					<td nowrap align="right"><?php echo $mylib->format_number($discount); ?></td>
				</tr>
				<?php
	
				$arr_data["gt_discount"] += $discount;
				
				$no++;
			}
			?>
			
			<?php
			if(count($arr_data["list_compliment"])>0)
			{
			?>
			<thead>
				<tr>
					<td colspan="4" align="right"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo $mylib->format_number($arr_data["gt_discount"]); ?></b></td>
				</tr>
			</thead>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>