<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Compliment";

if($v_pilihan=="transaksi"){
	$check1 = "checked";
	$check2 = "";
	$check3 = "";
}
elseif($v_pilihan=="detail"){
	$check1 = "";
	$check2 = "checked";
	$check3 = "";
}
elseif($v_pilihan=="barang"){
	$check1 = "";
	$check2 = "";
	$check3 = "checked";
}
else{
	$check1 = "checked";
	$check2 = "";
	$check3 = "";
}

foreach($mkasir as $val)
{
	$arr_data["kasir"][$val["UserName"]] = $val["UserName"];
    $arr_data["employee_name"][$val["UserName"]] = $val["employee_name"];
}

foreach($mkasirfromheader as $val)
{
	$arr_data["kasir"][$val["Kasir"]] = $val["Kasir"];
    $arr_data["employee_name"][$val["Kasir"]] = $val["employee_name"];
}                                                                                   

foreach($arr_data["kasir"] as $kasir => $val)
{     
	if($userlevel==-1)
	{
		$arr_data["all_kasir"][$kasir]=$kasir;
	}
	else
	{
		$arr_data["all_kasir"][$username] = $kasir;
	}
}

?>

<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_compliment/search_report/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        
			<tr>
	        	<td class="title_table" width="150">Rekap Per-</td>
	        	<td>     
                    <div class="radio" style="float:left; width:100px; margin-top:0px;" >
                        <label><input type="radio" name="v_pilihan" id="v_pilihan" value='transaksi' <? echo $check1;?> /> Transaksi</label>
                    </div>     
                    <div class="radio" style="float:left;  width:100px; margin-top:0px;display: none;"> 
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='detail' <? echo $check2;?> /> Detail</label>       
					 </div>   
                     <div class="radio" style="float:left;  width:100px; margin-top:0px;display: none;"> 
					    <label><input type="radio" name="v_pilihan" id="v_pilihan" value='barang' <? echo $check3;?> /> Barang</label>       
	        	     </div>
                </td>
	        </tr>
			
	        <tr>
	            <td class="title_table" width="150">Period</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Divisi</td>
	            <td> 
	            	<select class="form-control-new" name="v_divisi" id="v_divisi" style="width: 200px;">
	            		
	            		<?php
	            		if($userlevel==-1 OR $userlevel==16)
						{
	            		?>
	            		<option value="">All</option>
	            		<?php
	            		}
	            		
	            		foreach($mtype as $val)
	            		{
	            			$selected="";
	            			if($v_divisi==$val["KdDivisi"])
	            			{
								$selected = "selected='selected'";
							}
	            			
							?><option <?php echo $selected ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Kasir</td>
	            <td> 
	            	<select class="form-control-new" name="v_kasir" id="v_kasir" style="width: 200px;">
	            		<option value="">All</option>
	            		<?php
	            		foreach($arr_data["all_kasir"] as $kasir => $val)
	            		{
                            $employee_name = $arr_data["employee_name"][$kasir];
                             
	            			$selected="";
	            			if($v_kasir==$kasir)
	            			{
								$selected = "selected='selected'";
							}
							
							?><option <?php echo $selected ?> value="<?php echo $kasir; ?>"><?php echo $kasir." :: ".$employee_name; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
                    <input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                    <input type='hidden' value='<?= $print ?>' id="print" name="print">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php

if ($tampilkanDT) 
{

	if($v_pilihan=="transaksi")
	{
        $this->load->view("report/compliment/tampil");
	}
	elseif($v_pilihan=="detail"){
		 $this->load->view("report/compliment/tampil_detail");
	}
	elseif($v_pilihan=="barang"){
		 $this->load->view("report/compliment/tampil_barang");
	}

   
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>