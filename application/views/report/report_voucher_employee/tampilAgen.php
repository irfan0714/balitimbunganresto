<?php

$mylib = new globallib();

unset($arr_data);

$arr_data = array();
$arr_data["list_transaksi"] = array();

foreach($hasil as $val)
{
	$arr_data["list_transaksi"][$val["NoStruk"]]=$val["NoStruk"];
	$arr_data["nokassa"][$val["NoStruk"]]=$val["NoKassa"];
	$arr_data["kasir"][$val["NoStruk"]]=$val["Kasir"];
	$arr_data["kddivisi"][$val["NoStruk"]]=$val["KdDivisi"];
	$arr_data["namadivisi"][$val["NoStruk"]]=$val["NamaDivisi"];
	$arr_data["subdivisi"][$val["NoStruk"]]=$val["SubDivisi"];
	$arr_data["tanggal"][$val["NoStruk"]]=$val["Tanggal"];
	$arr_data["waktu"][$val["NoStruk"]]=$val["Waktu"];
	$arr_data["KdAgent"][$val["NoStruk"]]=$val["KdAgent"];
	$arr_data["tunai"][$val["NoStruk"]]=$val["Tunai"];
	$arr_data["kdebit"][$val["NoStruk"]]=$val["KDebit"];
	$arr_data["kkredit"][$val["NoStruk"]]=$val["KKredit"];
	$arr_data["voucher"][$val["NoStruk"]]=$val["Voucher"];
	$arr_data["discount"][$val["NoStruk"]]=$val["Discount"];
	$arr_data["totalnilai"][$val["NoStruk"]]=$val["Sales"];
	$arr_data["totalbayar"][$val["NoStruk"]]=$val["TotalBayar"];
	$arr_data["kembali"][$val["NoStruk"]]=$val["Kembali"];
	$arr_data["tax"][$val["NoStruk"]]=$val["Tax"];
	$arr_data["charge"][$val["NoStruk"]]=$val["Total_Charge"];
	
	$arr_data["totaldetail"][$val["NoStruk"]]=$val["Gross"];
	$arr_data["discountdetail"][$val["NoStruk"]]=$val["Discount"];
	$arr_data["netto"][$val["NoStruk"]]=$val["Nett"];    
}

$arr_data["list_voucher"]= array();
foreach($hasil_voucher as $val)
{
	$arr_data["list_voucher"][$val["Tanggal"]][$val["NoStruk"]] += $val["NilaiVoucher"];
}

?>

<style>
.link_data{
	text-decoration: underline;
	font-size: normal;
}

.link_data:hover{
	text-decoration: none;
}
</style>

<script>
	function PopUpVoucher(id)
    {
		base_url = "<?php echo base_url(); ?>";
		url = base_url+"index.php/report/report_kasir/pop_up_detail_voucher/index/"+id+"/";
		windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_kasir/search_report/" onsubmit="return false"/>
    <?php
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_transaksi.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" rowspan="2" style="vertical-align: middle; text-align: center;">No</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Divisi</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Kasir</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Tanggal</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Waktu</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">No Struk</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">No Stiker</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Total</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Disc</td>
					<td rowspan="2" style="vertical-align: middle; text-align: center;">Disc/Compliement</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Netto</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Charge</td>
					<td rowspan="2" style="vertical-align: middle; text-align: center;">Tax</td>
					<td rowspan="2" style="vertical-align: middle; text-align: center;">GrandTotal</td>
					
                    <td colspan="5" style="text-align: center;">Pembayaran</td>
                    
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Total Bayar</td>
                    <td rowspan="2" style="vertical-align: middle; text-align: center;">Kembali</td>

                </tr>
                
                <tr class="title_table">
                    <td style="text-align: center;">NetTunai</td>
					<td style="text-align: center;">Tunai</td>
                    <td style="text-align: center;">Kredit</td>
                    <td style="text-align: center;">Debit</td>
                    <td style="text-align: center;">Voucher</td>
                </tr>
			<tbody>
			
			<?php
			if(count($arr_data["list_transaksi"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$arr_data["gt_bruto"] ="";
			$arr_data["gt_discountdetail"]  ="";
			$arr_data["gt_discount"]  ="";
			$arr_data["gt_netto"]  ="";
			$arr_data["gt_charge"] ="";
			$arr_data["gt_tax"] ="";
			$arr_data["gt_grandtotal"] ="";
			
			$arr_data["gt_nettunai"] ="";
			$arr_data["gt_tunai"] ="";
			$arr_data["gt_kredit"] ="";
			$arr_data["gt_debit"]  ="";
			$arr_data["gt_voucher"]="";
			
			$arr_data["gt_totalbayar"] ="";
			$arr_data["gt_kembali"] ="";
			$no=1;
			
			$nostikers = "";
			
			foreach($arr_data["list_transaksi"] as $nostruk => $val)
			{		
				$nokassa = $arr_data["nokassa"][$nostruk];
				$kasir = $arr_data["kasir"][$nostruk];
				$kddivisi = $arr_data["kddivisi"][$nostruk];
				$namadivisi = $arr_data["namadivisi"][$nostruk];
				$subdivisi = $arr_data["subdivisi"][$nostruk];
				$tanggal = $arr_data["tanggal"][$nostruk];
				$waktu = $arr_data["waktu"][$nostruk];
				$tunai = $arr_data["tunai"][$nostruk];
				$kdebit = $arr_data["kdebit"][$nostruk];
				$kkredit = $arr_data["kkredit"][$nostruk];
				$voucher = $arr_data["voucher"][$nostruk];
				$discount = $arr_data["discount"][$nostruk];
				$totalnilai = $arr_data["totalnilai"][$nostruk];
				$totalbayar = $arr_data["totalbayar"][$nostruk];
				$kembali = $arr_data["kembali"][$nostruk];
				$KdAgent = $arr_data["KdAgent"][$nostruk];
				$tax = $arr_data["tax"][$nostruk];
				$totalcharge = $arr_data["charge"][$nostruk];			
				$nettunai = $tunai - $kembali;
				
				$bruto = $arr_data["totaldetail"][$nostruk];
				$discountdetail = $arr_data["discountdetail"][$nostruk];
				$netto = $arr_data["netto"][$nostruk];		
				
				$nilai_voucher = $arr_data["list_voucher"][$mylib->ubah_tanggal($tanggal)][$nostruk];	
				
				$arr_date = explode("-",$tanggal);
				
				if($arr_date[1]*1<=5)
				{
					$echo_voucher = $voucher;	
				}
				else
				{
					$echo_voucher = $nilai_voucher;		
				}
						
				//netto = Bruto - DiscHeader - DiscDetail;
				$pembayaran = $tunai + $kdebit + $kkredit + $echo_voucher;
				if($kddivisi!=3){
					$grandtotal = $netto + $totalcharge + $tax;	
				}
				else{
						if($totalcharge>0){
							$grandtotal = $netto + $totalcharge + $tax;
						}
						else{
								$grandtotal = $netto;
						}
				}
				
				if($totalcharge==0){
					$totalcharge = 0;
					//$tax = 0;
				}
                
                $grandtotal = $totalnilai;
				
				$jm_pembayaran = (($tunai*1)+($kkredit*1)+($kdebit*1)+($echo_voucher*1))-($kembali*1);
				
                /*
				$bg_salah="";
				if(($jm_pembayaran*1)!=($grandtotal*1))
				{
					$bg_salah = "background-color: #eef15f;";
					
				}
                */
				
				$echo_grandtotal = $totalnilai;
                
                $bg_salah="";
                if(($jm_pembayaran*1)!=($echo_grandtotal*1))
                {
                    $bg_salah = "background-color: #FFFFFF;";
                    
                }
				
								
				if($nostikers!="" AND $nostikers!=$KdAgent){
				?>
					<tr bgcolor='#e6e6e6'>
						<td colspan="8" align="center"><b>Total</b>&nbsp;</td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_charge"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tax"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_total"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_nettunai"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tunai"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_kredit"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_debit"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_voucher"]); ?></b></td>
						
						<td align="right"><b><?php echo ubah_format($arr_data["t_bayar"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_kembali"]); ?></b></td>
						
						
						
					</tr>		
				<?
					$arr_data["t_qty"] = 0;
					//$arr_data["t_harga"] = 0;
					$arr_data["t_bruto"] = 0;
					$arr_data["t_bayar"] = 0;
					$arr_data["t_kembali"] = 0;
					$arr_data["t_charge"] = 0;
					$arr_data["t_tax"] = 0;
					$arr_data["t_total"] = 0;
					$arr_data["t_nettunai"] = 0;
					$arr_data["t_tunai"] = 0;
					$arr_data["t_kredit"] = 0;
					$arr_data["t_debit"] = 0;
					$arr_data["t_voucher"] = 0;
				}
					
				
				?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)" style="<?php echo $bg_salah; ?>">
					<td nowrap align="center" title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $no; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $namadivisi; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>"><?php echo $kasir; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $mylib->ubah_tanggal($tanggal); ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $waktu; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $nostruk; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$kasir." :: ".$nostruk; ?>" align="center"><?php echo $KdAgent; ?></td>
					<td nowrap title="Total = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Disc = <?php echo ubah_format($discountdetail); ?>" align="right"><?php echo ubah_format($discountdetail); ?></td>
					<td nowrap title="Disc/Compliement = <?php echo ubah_format($discount); ?>" align="right"><?php echo ubah_format($discount); ?></td>
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>
					<td nowrap title="Charge = <?php echo ubah_format($totalcharge); ?>" align="right"><?php echo ubah_format($totalcharge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($tax); ?>" align="right"><?php echo ubah_format($tax); ?></td>					
					<td nowrap style="border-right-color: #009999 ;"  title="GrandTotal = <?php echo ubah_format($echo_grandtotal); ?>" align="right"><?php echo ubah_format($echo_grandtotal); ?></td>
					
					<td nowrap title="NetTunai = <?php echo ubah_format($nettunai); ?>" align="right"><?php echo ubah_format($nettunai); ?></td>
					<td nowrap title="Tunai = <?php echo ubah_format($tunai); ?>" align="right" ><?php echo ubah_format($tunai); ?></td>
					<td nowrap title="Kredit = <?php echo ubah_format($kkredit); ?>" align="right"><?php echo ubah_format($kkredit); ?></td>
					<td nowrap title="Debit = <?php echo ubah_format($kdebit); ?>" align="right"><?php echo ubah_format($kdebit); ?></td>
					
					<?php
					if($arr_date[1]*1<=5)
					{
						?>
						<td nowrap title="Voucher = <?php echo ubah_format($voucher); ?>" align="right" style="border-right-color: #009999;"><?php echo ubah_format($voucher); ?></td>
						<?php
					}
					else
					{
						?>
						<td nowrap title="Voucher = <?php echo ubah_format($nilai_voucher); ?>" align="right" style="border-right-color: #009999;"><?if ($excel != "excel"){?><a href="javascript:void(0);" onclick="PopUpVoucher('<?php echo $nostruk; ?>')" class="link_data" title="Detail No Voucher" ><?php echo ubah_format($nilai_voucher); ?></a><?}else{ echo ubah_format($nilai_voucher);}?></td>
						<?php
					}
					?>
					
					
					
					<td nowrap style="border-right-color: #009999;" title="Total Bayar = <?php echo ubah_format($jm_pembayaran); ?>" align="right"><?php echo ubah_format($jm_pembayaran); ?></td>
					<td nowrap title="Kembali = <?php echo ubah_format($kembali); ?>" align="right"><?php echo ubah_format($kembali); ?></td>
					
					
				</tr>
				<?php
	
				    $arr_data["t_qty"] += 0;
					//$arr_data["t_harga"] = 0;
					$arr_data["t_bruto"] += $bruto;
					$arr_data["t_disc"] += 0;
					$arr_data["t_netto"] += 0;
					$arr_data["t_charge"] +=  0;
					$arr_data["t_tax"] += $tax;
					$arr_data["t_total"] += $echo_grandtotal;
					$arr_data["t_nettunai"] += $nettunai;
					$arr_data["t_tunai"] += $tunai;
					$arr_data["t_kredit"] += $kkredit;
					$arr_data["t_debit"] += $kdebit;
					$arr_data["t_voucher"] += $nilai_voucher;
					$arr_data["t_bayar"] += $jm_pembayaran;
					$arr_data["t_kembali"] += $kembali;
					
					
					
				$arr_data["gt_bruto"] += $bruto;
				$arr_data["gt_discountdetail"] += $discountdetail;
				$arr_data["gt_discount"] += $discount;
				$arr_data["gt_netto"] += $netto;
				$arr_data["gt_charge"] += $totalcharge;
				$arr_data["gt_tax"] += $tax;
				$arr_data["gt_grandtotal"] += $echo_grandtotal;
				
				$arr_data["gt_nettunai"] += $nettunai;
				$arr_data["gt_tunai"] += $tunai;
				$arr_data["gt_kredit"] += $kkredit;
				$arr_data["gt_debit"] += $kdebit;
				$arr_data["gt_voucher"] += $echo_voucher;
				
				$arr_data["gt_totalbayar"] += $jm_pembayaran;
				$arr_data["gt_kembali"] += $kembali;
				
				$no++;
				$nostikers=$KdAgent;
			}
			?>
			<tr bgcolor='#e6e6e6'>
						<td colspan="8" align="center"><b>Total</b>&nbsp;</td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_charge"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tax"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_total"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_nettunai"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tunai"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_kredit"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_debit"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_voucher"]); ?></b></td>
						
						<td align="right"><b><?php echo ubah_format($arr_data["t_bayar"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_kembali"]); ?></b></td>
						
						
						
					</tr>
			<?php
			if(count($arr_data["list_transaksi"])>0)
			{
			?>
				<tr class="title_table">
					<td colspan="7" align="right"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_bruto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_discountdetail"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_discount"]); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_netto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_charge"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_tax"]); ?></b></td>
					<td align="right" style="border-right-color: #73e8e4;"><b><?php echo ubah_format($arr_data["gt_grandtotal"]); ?></b></td>
					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_nettunai"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_tunai"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_kredit"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_debit"]); ?></b></td>
					<td style="border-right-color: #ff7575;" align="right"><b><?php echo ubah_format($arr_data["gt_voucher"]); ?></b></td>
					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_totalbayar"]); ?></b></td>
					<td align="right" style="border-right-color: #c1f966;" ><b><?php echo ubah_format($arr_data["gt_kembali"]); ?></b></td>				
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}
?>