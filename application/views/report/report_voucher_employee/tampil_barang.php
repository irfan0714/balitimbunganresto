<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_kasir/search_report/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    $x = ($excel == "excel");
    
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_barang.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                        document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" style="text-align: center;">No</td>
                    <td style="text-align: center;">Sub Divisi</td>                    
                    <td style="text-align: center;">PCode</td>
                    <td style="text-align: center;">Nama</td>
					<td style="text-align: center;">Qty</td>
					<td style="text-align: center;">Qty Compliment</td>
                    <td style="text-align: center;">Harga</td>
                    <td style="text-align: center;">Bruto</td>
					<td style="text-align: center;">Disc</td>
					<td style="text-align: center;">Netto</td>
                </tr>
                
			<tbody>
			
			<?php
			if(count($hasil)==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			$no=1;
			$tempdivisi = "";
			$tempsubdivisi = "";
			$sbruto = 0;
			$sdisc = 0;
			$snetto= 0;
			
			$dbruto = 0;
			$ddisc = 0;
			$dnetto= 0;
			
			$gbruto = 0;
			$gdisc = 0;
			$gnetto = 0;
			for($i=0;$i<count($hasil);$i++)
			{		
				$namadivisi = $hasil[$i]['NamaDivisi'];
				$subdivisi = $hasil[$i]['SubDivisi'];
				$pcode = $hasil[$i]['PCode'];
				$namalengkap = $hasil[$i]['NamaLengkap'];
				$qty = 	$hasil[$i]['Qty'];
				$qtycomp = 	$hasil[$i]['QtyCompliment'];
				$harga = $hasil[$i]['Harga'];
				$bruto = $hasil[$i]['Gross'];
				$disc = $hasil[$i]['Discount'];
				$netto = $hasil[$i]['Nett'];
				if($tempsubdivisi != '' and $tempsubdivisi != $subdivisi){
				?>
					<tr class="title_table">
						<td nowrap colspan="4" align="center"><?php echo 'Sub Total ' . $tempsubdivisi; ?></td>
						<td nowrap align="right"><?php echo "&nbsp"; ?></td>
						<td nowrap align="right"><?php echo "&nbsp"; ?></td>
						<td nowrap align="right"><?php echo "&nbsp"; ?></td>
						<td nowrap title="Bruto = <?php echo ubah_format($sbruto); ?>" align="right"><?php echo ubah_format($sbruto); ?></td>
						<td nowrap title="Disc = <?php echo ubah_format($sdisc); ?>" align="right"><?php echo ubah_format($sdisc); ?></td>					
						<td nowrap title="Netto = <?php echo ubah_format($snetto); ?>" align="right"><?php echo ubah_format($snetto); ?></td>				
					</tr>
				<?
					$sbruto = 0;
					$sdisc = 0;
					$snetto = 0;
				}
				if($tempdivisi != '' and $tempdivisi != $namadivisi){
				?>
					<tr class="title_table">
						<td nowrap colspan="4" align="center"><?php echo 'Total ' . $tempdivisi; ?></td>
						<td nowrap align="right"><?php echo "&nbsp"; ?></td>
						<td nowrap align="right"><?php echo "&nbsp"; ?></td>
						<td nowrap align="right"><?php echo "&nbsp"; ?></td>
						<td nowrap title="Bruto = <?php echo ubah_format($dbruto); ?>" align="right"><?php echo ubah_format($dbruto); ?></td>
						<td nowrap title="Disc = <?php echo ubah_format($ddisc); ?>" align="right"><?php echo ubah_format($ddisc); ?></td>					
						<td nowrap title="Netto = <?php echo ubah_format($dnetto); ?>" align="right"><?php echo ubah_format($dnetto); ?></td>				
					</tr>
				<?
					$dbruto = 0;
					$ddisc = 0;
					$dnetto = 0;
				}
				?>
				
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $subdivisi." :: ".$namalengkap; ?>"><?php echo $i+1; ?></td>
					<td nowrap title="Divisi = <?php echo $subdivisi; ?>" align="left"><?php echo $subdivisi; ?></td>					
					<td nowrap title="PCode = <?php echo $pcode; ?>" align="left" style="mso-number-format: '\@'"><?php echo $pcode; ?></td>
					<td nowrap title="Nama = <?php echo $namalengkap; ?>" align="left"><?php echo $namalengkap; ?></td>
					<td nowrap title="Qty = <?php echo ubah_format2($qty); ?>" align="right"><?php echo ubah_format2($qty); ?></td>
					<td nowrap title="Qty Comliment = <?php echo ubah_format2($qtycomp); ?>" align="right"><?php echo ubah_format2($qtycomp); ?></td>
					<td nowrap title="Harga = <?php echo ubah_format($harga); ?>" align="right"><?php echo ubah_format($harga); ?></td>
					<td nowrap title="Bruto = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Disc = <?php echo ubah_format($disc); ?>" align="right"><?php echo ubah_format($disc); ?></td>					
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>				
										
				</tr>
				<?php
	
				$sbruto += $bruto;
				$sdisc += $disc;
				$snetto += $netto;
				
				$dbruto += $bruto;
				$ddisc += $disc;
				$dnetto += $netto;
				
				$gbruto += $bruto;
				$gdisc += $disc;
				$gnetto += $netto;
				
				$no++;
				$tempsubdivisi = $subdivisi;
				$tempdivisi = $namadivisi;
			}

			if(count($hasil)>0)
			{
			?>
				<tr class="title_table">
					<td nowrap colspan="4" title="Divisi = <?php echo $tempdivisi; ?>" align="center"><?php echo 'Sub Total ' . $tempsubdivisi; ?></td>
					<td nowrap align="right"><?php echo "&nbsp"; ?></td>
					<td nowrap align="right"><?php echo "&nbsp"; ?></td>
					<td nowrap align="right"><?php echo "&nbsp"; ?></td>
					<td nowrap align="right"><?php echo ubah_format($sbruto); ?></td>
					<td nowrap align="right"><?php echo ubah_format($sdisc); ?></td>					
					<td nowrap align="right"><?php echo ubah_format($snetto); ?></td>				
				</tr>
				<tr class="title_table">
					<td nowrap colspan="4" title="Divisi = <?php echo $tempdivisi; ?>" align="center"><?php echo 'Total ' . $tempdivisi; ?></td>
					<td nowrap align="right"><?php echo "&nbsp"; ?></td>
					<td nowrap align="right"><?php echo "&nbsp"; ?></td>
					<td nowrap align="right"><?php echo "&nbsp"; ?></td>
					<td nowrap align="right"><?php echo ubah_format($dbruto); ?></td>
					<td nowrap align="right"><?php echo ubah_format($ddisc); ?></td>					
					<td nowrap align="right"><?php echo ubah_format($dnetto); ?></td>				
				</tr>
				<tr></tr>
				<tr class="title_table">
					<td colspan="4" align="center"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo ubah_format($gbruto); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($gdisc); ?></b></td>
					<td align="right"><b><?php echo ubah_format($gnetto); ?></b></td>
					
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}

function ubah_format2($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>