<?php
$this->load->view('header');
$reportlib = new report_lib();

$modul = "Voucher Karyawan";

?>


<div class="row">
    <div class="col-md-12" align="left">

    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i>Report <?php echo $modul; ?></strong></li>
		</ol>

		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/report/report_voucher_employee/search_report/">

	    <table class="table table-bordered responsive">

	        <tr>
	            <td class="title_table" width="150">Periode</td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
	            </td>
	        </tr>

	        <tr>
	        	<td class="title_table" width="150">No Struk</td>
	        	<td>
	        		<input type="text" class="form-control-new" value="<?php echo $v_nostruk; ?>" name="v_nostruk" id="v_nostruk" size="34">
	        	</td>
	        </tr>
	        <tr>
	            <td class="title_table">Divisi</td>
	            <td>
	            	<select class="form-control-new" name="v_divisi" id="v_divisi" style="width: 200px;">

	            		<?php
	            		if($userlevel==-1 || $userlevel==13 || $userlevel==16)
						{
	            		?>
	            		<option value="">All</option>
	            		<?php
	            		}

	            		foreach($ArrDivisi as $val)
	            		{
	            			$selected="";
	            			if($v_divisi==$val["KdDivisi"])
	            			{
								$selected = "selected='selected'";
							}

							?><option <?php echo $selected ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">Karyawan</td>
	            <td>
	            	<select class="form-control-new" name="v_nik" id="v_nik" style="width: 300px;">
	            		<option value="">All</option>
	            		<?php
	            		foreach($ArrEmployee as $key => $val){ ?>
	            			<option value="<?php echo $val['nik']; ?>"><?php echo $val['nik']." :: ".$val['keterangan']; ?></option>
	            		<?php } ?>
	            	</select>
	            </td>
	        </tr>

	        <tr>
	        	<td>&nbsp;</td>
	            <td>
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="submit" class="btn btn-info btn-icon btn-sm icon-left"  name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left"  name="btn_excel" id="btn_excel"  value="Search">Export Excel<i class="entypo-search"></i></button>
		       	</td>
	        </tr>

	    </table>
	    </form>
	</div>
</div>
<div id="load-data"></div>

<?php

$this->load->view('footer');
?>

<script type="text/javascript">
	
	$("#search").submit(function(e){
		link = $(this).attr('action');
		e.preventDefault();
		
		$.ajax({
			url : link,
			type : "POST",
			data : new FormData(this),
			processData : false, 
            contentType : false, 
            beforeSend: function(){
                $('#load-data').html('loading...');
            },
          	success: function(response){
          		$('#load-data').html(response);	
          	},
          	error : function(e){
          		alert('error '+e);
          	}
		});
	});

    $("#btn_excel").click(function(){

        var v_start_date = $("#v_start_date").val();
        var v_end_date = $("#v_end_date").val();
        var v_nostruk = $("#v_nostruk").val();
        var v_divisi = $("#v_divisi").val();
        var v_nik = $("#v_nik").val();

        var param ='#'+v_start_date;
            param +='#'+v_end_date;
            param +='#'+v_nostruk;
            param +='#'+v_divisi;
            param +='#'+v_nik;

            var b64 = btoa(param);
            console.log(b64);
        window.open('<?php echo base_url(); ?>index.php/report/report_voucher_employee/export_excel/'+b64, '_blank');
    });
</script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
