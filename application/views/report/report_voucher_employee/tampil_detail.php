<?php

unset($arr_data);
$arr_data = array();
$arr_data["list_transaksi"] = array();

foreach($hasil as $val)
{
	$arr_data["list_transaksi"][$val["NoStruk"]]=$val["NoStruk"];
	$arr_data["nokassa"][$val["NoStruk"]]=$val["NoKassa"];
	$arr_data["kasir"][$val["NoStruk"]]=$val["Kasir"];
	$arr_data["kddivisi"][$val["NoStruk"]]=$val["KdDivisi"];
	$arr_data["namadivisi"][$val["NoStruk"]]=$val["NamaDivisi"];
	$arr_data["tanggal"][$val["NoStruk"]]=$val["Tanggal"];
	$arr_data["waktu"][$val["NoStruk"]]=$val["Waktu"];
	
	$arr_data["totalnilai"][$val["NoStruk"]]=$val["TotalNilai"];
	$arr_data["totalbayar"][$val["NoStruk"]]=$val["TotalBayar"];
	$arr_data["voucher"][$val["NoStruk"]]=$val["Voucher"];
	$arr_data["nik"][$val["NoStruk"]]=$val["KdCustomer"];
	$arr_data["disc"][$val["NoStruk"]]=$val["Discount"];
	$arr_data["item"][$val["NoStruk"]]=$val["TotalItem"];
	
	$arr_data["list_transaksi_detail"][$val["NoStruk"]][$val["PCode"]]=$val["PCode"];
	
	//$arr_data["pcode"][$val["NoStruk"]]=$val["PCode"];
	$arr_data["namalengkap"][$val["NoStruk"]][$val["PCode"]]=$val["NamaLengkap"];
	$arr_data["qty"][$val["NoStruk"]][$val["PCode"]]=$val["Qty"];
	$arr_data["berat"][$val["NoStruk"]][$val["PCode"]]=$val["Berat"];
	$arr_data["harga"][$val["NoStruk"]][$val["PCode"]]=$val["Harga"];
	$arr_data["bruto"][$val["NoStruk"]][$val["PCode"]]=$val["Bruto"];
	$arr_data["discountdetail"][$val["NoStruk"]][$val["PCode"]]=$val["DiscDetail"];
	$arr_data["netto"][$val["NoStruk"]][$val["PCode"]]=$val["Netto"];
	$arr_data["service_charge"][$val["NoStruk"]][$val["PCode"]]=$val["Service_charge"];
	$arr_data["ppn"][$val["NoStruk"]][$val["PCode"]]=$val["PPN"];
	$arr_data["Tunai"][$val["NoStruk"]]=$val["Tunai"];
	$arr_data["Point"][$val["NoStruk"]]=$val["Point"];
	$arr_data["KKredit"][$val["NoStruk"]]=$val["KKredit"];
	$arr_data["KDebit"][$val["NoStruk"]]=$val["KDebit"];
	$arr_data["GoPay"][$val["NoStruk"]]=$val["GoPay"];
	$arr_data["Voucher"][$val["NoStruk"]]=$val["Voucher"];
}
$arr_employee = array();
foreach ($ArrEmployee as $val) {
	$arr_employee[$val['nik']] =  $val['keterangan'];
}

$arr_voucher_employee = array();
foreach ($ArrVoucherEmployee as $val) {
	$arr_voucher_employee[$val['NoStruk']] =  $val['NilaiVoucher'];
}

// echo "<pre>";print_r($arr_vocuher_employee);

?>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/report_kasir/search_report/" onsubmit="return false"/>
    <?php
    $mylib = new globallib();
    if ($excel == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ReportKasir_detail.xls"');
    }
    if ($excel != "excel") {
        ?>
        <span style="margin-bottom:10px; display: inline-table;">
        <!-- <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button> -->
        </span>
        <?php
    }
    ?>

    <br/>
    
    <ol class="breadcrumb">
		<li><strong><i class="entypo-doc-text"></i><?php echo $judul; ?></strong></li>
	</ol>
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
                <tr class="title_table">
                    <td width="30" style="text-align: center;">No</td>
                    <td style="text-align: center;">NIK</td>
                    <td style="text-align: center;">Nama Karyawan</td>
                    <td style="text-align: center;">Divisi</td>
                    <td style="text-align: center;">Kasir</td>
                    <td style="text-align: center;">Tanggal</td>
                    <td style="text-align: center;">Waktu</td>
                    <td style="text-align: center;">No Struk</td>
                    
                    <td style="text-align: center;">PCode</td>
                    <td style="text-align: center;">Nama</td>
					<td style="text-align: center;">Qty</td>
					<td style="text-align: center;">Berat (gr)</td>
                    <td style="text-align: center;">Harga</td>
                    <td style="text-align: center;">Bruto</td>
					<td style="text-align: center;">Disc</td>
					<td style="text-align: center;">Netto</td>
					<td style="text-align: center;">Charge</td>
					<td style="text-align: center;">Tax</td>
					<td style="text-align: center;">Total</td>
					<td style="text-align: center;">Tunai</td>
					<td style="text-align: center;">Point</td>
					<td style="text-align: center;">KKredit</td>
					<td style="text-align: center;">KKdebit</td>
					<td style="text-align: center;">Gopay</td>
					<td style="text-align: center;">Voucher</td>
					<td style="text-align: center;">Voucher Karyawan</td>
                </tr>            
			<tbody>
			
			<?php
			if(count($arr_data["list_transaksi"])==0)
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			
			
			$tempnostruk = "";
			$tempnik = "";
			$Tunai_temp = "";
			$Point_temp = "";
			$KKredit_temp = "";
			$KDebit_temp = "";
			$GoPay_temp = "";
			$Voucher_temp = "";
			$VoucherEmployee_temp = "";
			foreach($arr_data["list_transaksi"] as $nostruk => $val)
			{		
				
				$nokassa = $arr_data["nokassa"][$nostruk];
				$kasir = $arr_data["kasir"][$nostruk];
				$kddivisi = $arr_data["kddivisi"][$nostruk];
				$nik = $arr_data["nik"][$nostruk];
				$namadivisi = $arr_data["namadivisi"][$nostruk];
				$tanggal = $arr_data["tanggal"][$nostruk];
				$waktu = $arr_data["waktu"][$nostruk];
				
				$totalnilai = $arr_data["totalnilai"][$nostruk];
				$totalbayar = $arr_data["totalbayar"][$nostruk];
				$voucher = $arr_data["voucher"][$nostruk];
				$disc = $arr_data["disc"][$nostruk];
				$totalitem = $arr_data["item"][$nostruk];
				$Tunai = $arr_data["Tunai"][$nostruk];
				$Point = $arr_data["Point"][$nostruk];
				$KKredit = $arr_data["KKredit"][$nostruk];
				$KDebit = $arr_data["KDebit"][$nostruk];
				$GoPay = $arr_data["GoPay"][$nostruk];
				$Voucher = $arr_data["Voucher"][$nostruk];
				
				$no=1;
				$nomor=1;
				foreach($arr_data["list_transaksi_detail"][$nostruk] as $pcode=>$val)
				{
				

				$namalengkap = $arr_data["namalengkap"][$nostruk][$pcode];
				$qty = $arr_data["qty"][$nostruk][$pcode];
				$berat= $arr_data["berat"][$nostruk][$pcode];
				$harga = $arr_data["harga"][$nostruk][$pcode];
				$bruto = $arr_data["bruto"][$nostruk][$pcode];
				$discountdetail = $arr_data["discountdetail"][$nostruk][$pcode];
				$netto = $arr_data["netto"][$nostruk][$pcode];
				
				$berat_view = ($berat != 0)? $berat:"";
				if($berat>0){
					$qty = ($berat/100)*$qty;
				}else{
					$qty = $qty ;
				}

				if($berat>0){
					$bruto = $bruto*($berat/100);
				}else{
					$bruto = $bruto*1 ;
				}

				$service_charge = $arr_data["service_charge"][$nostruk][$pcode];
				$ppn = $arr_data["ppn"][$nostruk][$pcode];
				
				$charge = $netto * ($service_charge/100);
				$tax = round(($netto * ($ppn/100)) + ($charge * ($ppn/100)));
				
				if($service_charge>0){
					$total = $netto + $charge + $tax;
				}
				else{
						$total = $netto  + $tax;
				}
				$disc_all = $bruto - $netto;
				
				if($tempnostruk!=$nostruk){
					$voucher_karyawan = $arr_voucher_employee[$nostruk];
				}else{
					$voucher_karyawan = 0;
				}
				if($tempnostruk!="" and $tempnostruk!=$nostruk){
				?>
					<tr bgcolor='#e6e6e6'>
						<td colspan="11" align="center"><b>Total</b>&nbsp;</td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo "&nbsp"; ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
						<td align="right"><b><?php echo ubah_format($arr_data["t_disc"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_netto"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_charge"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tax"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_total"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($Tunai_temp); ?></b></td>
						<td align="right"><b><?php echo ubah_format($Point_temp); ?></b></td>
						<td align="right"><b><?php echo ubah_format($KKredit_temp); ?></b></td>
						<td align="right"><b><?php echo ubah_format($KDebit_temp); ?></b></td>
						<td align="right"><b><?php echo ubah_format($GoPay_temp); ?></b></td>
						<td align="right"><b><?php echo ubah_format($Voucher_temp); ?></b></td>
						<td align="right"><b><?php echo ubah_format($VoucherEmployee_temp); ?></b></td>
					</tr>		
				<?
					$arr_data["t_qty"] = 0;
					//$arr_data["t_harga"] = 0;
					$arr_data["t_bruto"] = 0;
					$arr_data["t_disc"] = 0;
					$arr_data["t_netto"] = 0;
					$arr_data["t_charge"] = 0;
					$arr_data["t_tax"] = 0;
					$arr_data["t_total"] = 0;
				}				
				if($tempnik!="" and $tempnik!=$nik){ ?>
					<tr bgcolor='#b2a2aa' style="color: white;">
						<td colspan="17" align="right"><b>Total </b>&nbsp;</td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_tax_per_nik"]); ?></b></td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_total_per_nik"]); ?></b></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td style="font-size: 8px !important;">Voucher Terpakai</td>
						<td align="right"><b><?php echo ubah_format($arr_data["t_total_voucher_karyawan"]); ?></b></td>
					</tr>
				<? 
					$arr_data["t_tax_per_nik"] = 0;
					$arr_data["t_total_per_nik"] = 0;
					$arr_data["t_total_voucher_karyawan"] =0;
				} ?>

				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td nowrap align="center" title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>"><?php echo $no; ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>"><?php if($tempnostruk!=$nostruk){echo $nik; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>"><?php if($tempnostruk!=$nostruk){echo $arr_employee[$nik]; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>"><?php if($tempnostruk!=$nostruk){echo $namadivisi; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>"><?php if($tempnostruk!=$nostruk){echo $kasir; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"><?php if($tempnostruk!=$nostruk){echo $mylib->ubah_tanggal($tanggal); }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"><?php if($tempnostruk!=$nostruk){echo $waktu; }else{ echo "&nbsp;";} ?></td>
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"><?php if($tempnostruk!=$nostruk){echo $nostruk; }else{ echo "&nbsp;";} ?></td>
					
					<td nowrap title="PCode = <?php echo $pcode; ?>" align="left" style="mso-number-format: '\@'"><?php echo $pcode; ?></td>
					<td nowrap title="Nama = <?php echo $namalengkap; ?>" align="left"><?php echo $namalengkap; ?></td>
					<td nowrap title="Qty = <?php echo $qty; ?>" align="right"><?php echo ubah_format($qty); ?></td>
					<td nowrap title="Qty = <?php echo $berat_view; ?>" align="right"><?php echo $berat_view; ?></td>
					<td nowrap title="Harga = <?php echo ubah_format($harga); ?>" align="right"><?php echo ubah_format($harga); ?></td>
					<td nowrap title="Bruto = <?php echo ubah_format($bruto); ?>" align="right"><?php echo ubah_format($bruto); ?></td>
					<td nowrap title="Disc = <?php echo ubah_format($disc_all); ?>" align="right"><?php echo ubah_format($disc_all); ?></td>					
					<td nowrap title="Netto = <?php echo ubah_format($netto); ?>" align="right"><?php echo ubah_format($netto); ?></td>				
					<td nowrap title="Charge = <?php echo ubah_format($charge); ?>" align="right"><?php echo ubah_format($charge); ?></td>
					<td nowrap title="Tax = <?php echo ubah_format($tax); ?>" align="right"><?php echo ubah_format($tax); ?></td>
					<td nowrap title="Total = <?php echo ubah_format($total); ?>" align="right"><?php echo ubah_format($total); ?></td>	
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
					<td nowrap title="<?php echo $tanggal." :: ".$namadivisi." :: ".$nik." :: ".$nostruk; ?>" align="center"></td>				
				</tr>
				<?php
				$Tunai_temp = $arr_data["Tunai"][$nostruk];
				$Point_temp = $arr_data["Point"][$nostruk];
				$KKredit_temp = $arr_data["KKredit"][$nostruk];
				$KDebit_temp = $arr_data["KDebit"][$nostruk];
				$GoPay_temp = $arr_data["GoPay"][$nostruk];
				$Voucher_temp = $arr_data["Voucher"][$nostruk];
				$VoucherEmployee_temp = $arr_voucher_employee[$nostruk];
				$arr_data["t_qty"] += $qty;
				//$arr_data["t_harga"] += $harga;
				$arr_data["t_bruto"] += $bruto;
				$arr_data["t_disc"] += $disc_all;
				$arr_data["t_netto"] += $netto;
				$arr_data["t_charge"] += $charge;
				$arr_data["t_tax"] += $tax;
				$arr_data["t_total"] += $total;
				$arr_data["t_tax_per_nik"] += $tax;
				$arr_data["t_total_per_nik"] += $total;
				$arr_data["t_total_voucher_karyawan"] += $voucher_karyawan;
	
				$arr_data["gt_qty"] += $qty;
				//$arr_data["gt_harga"] += $harga;
				$arr_data["gt_bruto"] += $bruto;
				$arr_data["gt_disc"] += $disc_all;
				$arr_data["gt_netto"] += $netto;
				$arr_data["gt_charge"] += $charge;
				$arr_data["gt_tax"] += $tax;
				$arr_data["gt_total"] += $total;

				
				$no++;
				$tempnostruk = $nostruk;				
				$tempnik = $nik;				
				}
			}
			?>
				<tr bgcolor='#e6e6e6'>
					<td colspan="11" align="center"><b>Total</b>&nbsp;</td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_bruto"]); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($arr_data["t_disc"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_netto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_charge"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_tax"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_total"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($Tunai_temp); ?></b></td>
					<td align="right"><b><?php echo ubah_format($Point_temp); ?></b></td>
					<td align="right"><b><?php echo ubah_format($KKredit_temp); ?></b></td>
					<td align="right"><b><?php echo ubah_format($KDebit_temp); ?></b></td>
					<td align="right"><b><?php echo ubah_format($GoPay_temp); ?></b></td>
					<td align="right"><b><?php echo ubah_format($Voucher_temp); ?></b></td>
					<td align="right"><b><?php echo ubah_format($VoucherEmployee_temp); ?></b></td>
				</tr>	
				<tr bgcolor='#b2a2aa' style="color: white;">
					<td colspan="18" align="right"><b>Total </b>&nbsp;</td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_total_per_nik"]); ?></b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="font-size: 8px !important;">Voucher Terpakai</td>
					<td align="right"><b><?php echo ubah_format($arr_data["t_total_voucher_karyawan"]); ?></b></td>
				</tr>
			<?php
			if(count($arr_data["list_transaksi"])>0)
			{
			?>
				<tr class="title_table">
					<td colspan="11" align="center"><b>Grand Total</b>&nbsp;</td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo "&nbsp"; ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_bruto"]); ?></b></td>					
					<td align="right"><b><?php echo ubah_format($arr_data["gt_disc"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_netto"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_charge"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_tax"]); ?></b></td>
					<td align="right"><b><?php echo ubah_format($arr_data["gt_total"]); ?></b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	

	
</form>
	</div>
</div>

<?
function ubah_format($harga){
	$s = number_format($harga, 2, ',', '.');
	return $s;
}

function ubah_format2($harga){
	$s = number_format($harga, 0, ',', '.');
	return $s;
}
?>