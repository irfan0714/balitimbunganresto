<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/daily_sales_per_store/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="dailysales.xls"');
    }
   
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    $arr_data  = array();
	$arr_total = array();
	
    for($i=$mylib->parsedate($v_date_from);$i<=$mylib->parsedate($v_date_to);$i=$i+86400)
    {
        $arr_data["list_date"][$i] = $i;
    }
    
    
    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">REPORT DAILY Per Store '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
		        <th rowspan="2" style="vertical-align: middle;">Hari</th>';
		        
	foreach($store as $outlet){
		$echo .= '<th style="vertical-align: middle; text-align: center;" colspan="5">'. $outlet['NamaStore'].'</th>';
	}
	$echo .= '
			</tr>
		</thead>
		<tbody>';

    $no = 1; 
    $tot_sales = array();
    $tot_guest = array();
    $tot_struk = array();
    foreach($arr_data["list_date"] as $date=>$val)
    {
        $bg_hari ="";
        if(date("l", $date)=="Saturday" || date("l", $date)=="Sunday")
        {
            $bg_hari = "background: #ff99cc;";
        }

    	$echo .= '
    	<tr style="color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.date("d/m/Y", $date).'</td>
	        <td style="'.$bg_hari.'">'.date("l", $date).'</td>';
	    $tothari = 0;    
	    $tgl = date('Y-m-d');
	    foreach($store as $outlet){
	    	$kdstore = $outlet['KodeStore'];
	    	$sales = isset($salesrec[$tgl][$kdstore]) ? $salesrec[$tgl][$kdstore] : 0;
	    	$struk = isset($strukrec[$tgl][$kdstore]) ? $strukrec[$tgl][$kdstore] : 0;
	    	$guest = isset($guestrec[$tgl][$kdstore]) ? $guestrec[$tgl][$kdstore] : 0;
	    	$salesperstruk = $struk>0 ? $sales/$struk : 0;
	    	$salesperguest = $guest>0 ? $sales/$guest : 0;
	    	$tothari += $sales;
	    	
	    	if (isset($tot_sales[$kdstore])){
				$tot_sales[$kdstore] += $sales;
			} else{
				$tot_sales[$kdstore] = $sales;
			}
			
			if (isset($tot_guest[$kdstore])){
				$tot_guest[$kdstore] += $sales;
			} else{
				$tot_guest[$kdstore] = $sales;
			}
			
			if (isset($tot_struk[$kdstore])){
				$tot_struk[$kdstore] += $sales;
			} else{
				$tot_struk[$kdstore] = $sales;
			}
	    	
			if($outlet['IsRestoCafe']==0){
				$echo .= '
					<th style="text-align: right;">'.$mylib->format_number($struk, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($sales, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($salesperstruk, 0, "", "", "ind").'</th>';
			}else{
				$echo .= '
					<th style="text-align: right;">'.$mylib->format_number($guest, 0, "", "", "ind").'</th>
					<th style="text-align: right;">'.$mylib->format_number($struk, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($sales, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($salesperstruk, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($salesperguest, 0, "", "", "ind").'</th>';
			}
			
		}
		$echo .= '<th style="text-align: right;">'.$mylib->format_number($tothari, 0, "", "", "ind").'</th>
	    </tr>
    	';
        $no++;
    }
    
	$echo .= '
		</tbody>';
	    
	$echo .= '
         <tfoot>
            <tr style="text-align: right; font-weight: bold; color: black;">
                <td colspan="3">Grand Total</td>';
    $tothari = 0;    
    foreach($store as $outlet){
	    $kdstore = $outlet['KodeStore'];
	    $sales = isset($tot_sales[$kdstore]) ? $tot_sales[$kdstore] : 0;
    	$struk = isset($tot_struk[$kdstore]) ? $tot_struk[$kdstore] : 0;
    	$guest = isset($tot_guest[$kdstore]) ? $tot_struk[$kdstore] : 0;
    	$salesperstruk = $struk>0 ? $sales/$struk : 0;
    	$salesperguest = $guest>0 ? $sales/$guest : 0;
    	$tothari += $sales;
		if($outlet['IsRestoCafe']==0){
				$echo .= '
					<th style="text-align: right;">'.$mylib->format_number($struk, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($sales, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($salesperstruk, 0, "", "", "ind").'</th>';
			}else{
				$echo .= '
					<th style="text-align: right;">'.$mylib->format_number($guest, 0, "", "", "ind").'</th>
					<th style="text-align: right;">'.$mylib->format_number($struk, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($sales, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($salesperstruk, 0, "", "", "ind").'</th>
			        <th style="text-align: right;">'.$mylib->format_number($salesperguest, 0, "", "", "ind").'</th>';
			}
			
	}
	
    $echo .= '<th style="text-align: right;">'.$mylib->format_number($tothari, 0, "", "", "ind").'</th>
            </tr>
        </tfoot> 
	    ';

	$echo .= '
		</table>';

	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Report Daily ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail);$i++){
			$email_address = $listemail[$i]['email_address'];
			$email_name = $listemail[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>