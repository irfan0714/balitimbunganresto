<?php $this->load->view('header_part1')?>
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	
</head>
<?php $this->load->view('header_part3')?>
 
<body>
 
<hr/>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">

<form method="post" action="<?=base_url(); ?>/index.php/report/report_hutang_supplier/view">
	<table>
		<tr>
			<td>&nbsp;<input type="submit" name='submit' value="TAMPIL"  id="excel" class='btn btn-default'></td>
		</tr>
</table>
</form>
<br>
<?php 
if ($tampilkanDT) 
{
    $this->load->view("report/report_hutang_supplier/tampil");
}

?>

</div>
	<?php 
	$this->load->view('footer'); 
//	$this->load->view('footer_part1');
	?>
<!--dua div dibawah, untuk nutup div yg ada di header-->
	</div>	
</div>
<!--eo dua div diatas-->
</body>
</html>