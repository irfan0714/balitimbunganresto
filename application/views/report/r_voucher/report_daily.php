<?php 
    include("header.php");
    
    if(!isset($_POST["v_date_from"])){ $v_date_from = isset($_POST["v_date_from"]); } else { $v_date_from = $_POST["v_date_from"]; }
    if(!isset($_POST["v_date_to"])){ $v_date_to = isset($_POST["v_date_to"]); } else { $v_date_to = $_POST["v_date_to"]; }
    
    if(!isset($_POST["btn_submit"])){ $btn_submit = isset($_POST["btn_submit"]); } else { $btn_submit = $_POST["btn_submit"]; }
    if(!isset($_POST["btn_excel"])){ $btn_excel = isset($_POST["btn_excel"]); } else { $btn_excel = $_POST["btn_excel"]; }
    
    $icon_type_change = "entypo-up-dir";
    
    $modul = "Report Daily";
    
    
    if($v_date_from=="")
    {
        $v_date_from = date("d/m/Y");
    }
    
    if($v_date_to=="")
    {
        $v_date_to = date("d/m/Y");
    }
    
    $q   = "SELECT Kasir FROM `transaksi_header` WHERE 1 GROUP BY Kasir ORDER BY Kasir ASC";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($Kasir) = $row;
        
        $Kasir = strtolower($Kasir);
        
        $arr_data["list_Kasir"][$Kasir] = $Kasir;
        
        if($Kasir=="ariesta" || $Kasir=="dedek" || $Kasir=="degler" || $Kasir=="desakmanik" || $Kasir=="feby" || $Kasir=="puri" || $Kasir=="tutik" || $Kasir=="omingsgv")
        {
            $arr_data["type_Kasir"][$Kasir] = "Oemah Herborist";    
        }
        else if($Kasir=="viorinresto" || $Kasir=="dewiresto" || $Kasir=="ferryresto" || $Kasir=="widiaresto")
        {
            $arr_data["type_Kasir"][$Kasir] = "Luwus Rice";    
        }
        else
        {
            $arr_data["type_Kasir"][$Kasir] = "Black Eye";    
        }
    }
    
    if($btn_excel)
    {
        header("Content-Disposition".": "."attachment;filename=repor-daily.xls");
        header("Content-type: application/vnd.ms-excel");
    }
    else
    {
     
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />
                                                
    <title><?php echo $modul; ?> - NPM</title>
    <link rel="shortcut icon" href="public/images/Logosg.png" >
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/NotoSans.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/skins/black.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="assets/css/my.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/js.js"></script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
    function start_page()
    {
        document.getElementById("v_keyword").focus();    
    }
    
    
function mouseover(target)
{  
    if(target.bgColor!="#cafdb5"){        
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#c1cdd8';
    }
}
    
function mouseout(target)
{
    if(target.bgColor!="#cafdb5"){ 
        if (target.bgColor=='#ccccff')
            target.bgColor='#ccccff';
        else
            target.bgColor='#FFFFFF';
            
    }    
}

function mouseclick(target, idobject, num)
{
                   
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
    for(i=0;i<num;i++){
        if (document.getElementById(idobject+'_'+i) != undefined){
            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
            if (target.id == idobject+'_'+i)
                target.bgColor='#ccccff';
        }
    }
}

function mouseclick1(target)
{
    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
    if(target.bgColor!="#cafdb5")
    {
        target.bgColor="#cafdb5";
    }
    else
    {
        target.bgColor="#FFFFFF";
    }
}  
    </script>

</head>

<body class="page-body skin-black">

<div class="page-container sidebar-collapsed">
    
    <?php include("menu_kiri.php"); ?>
    
    <div class="main-content">
    
        <ol class="breadcrumb bc-3">
            <li>
                <a href="index.php">
                    <i class="entypo-home"></i>Home
                </a>
            </li>
            <li>NPM</li>
            <li class="active"><strong><?php echo $modul; ?></strong></li>
        </ol>
        
        <hr/>
        <br/>
        
        <form method="POST" name="theform" id="theform">
        
        <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
            
            <table class="table table-bordered responsive">
                <thead>
                    <tr>
                        <th width="100">Tanggal</th>
                        <th>: 
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_from; ?>" name="v_date_from" id="v_date_from" size="10" maxlength="10">
                            s/d
                            <input type="text" class="form-control-new datepicker" value="<?php echo $v_date_to; ?>" name="v_date_to" id="v_date_to" size="10" maxlength="10">
                        </th>
                    </tr>
                    
                    
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_submit" id="btn_submit" value="Submit">
                            <input type="submit" class="btn btn-info btn-icon btn-sm icon-center" name="btn_excel" id="btn_excel" value="Excel">
                        </td>
                    </tr>
                </thead>
                
            </table> 
            <br><br>
            
            <?php 
    }
            
                if($btn_submit || $btn_excel)
                {
                   
                    $where_date = "";
                    if($v_date_from=="" && $v_date_to=="")
                    {
                        die("Tanggal Harus diisi");
                    }
                    
                    
                    for($i=parsedate($v_date_from);$i<=parsedate($v_date_to);$i=$i+86400)
                    {
                        $arr_data["list_date"][$i] = $i;
                    }
                    
                    //echo "<pre>";
                    //print_r($arr_data["list_date"]);
                    //echo "</pre>";
                    
                    // data ticket
                    
                    
                    $q = "
                            SELECT
                                `ticket`.noticket,
                                `ticket`.add_date,
                                `ticket`.jenis,
                                `ticket`.harga
                            FROM
                                `ticket`
                            WHERE
                                1
                                AND `ticket`.add_date BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                                AND ticket.`noidentitas` != '1234'
                            ORDER BY
                                `ticket`.noticket ASC
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($noticket, $add_date, $jenis, $harga) = $row;    
                        
                        if($jenis==1)
                        {
                            $arr_data["lokal"][$add_date]++;    
                        }
                        else
                        {
                            $arr_data["asing"][$add_date]++;
                        }
                        
                        $arr_data["all_ticket"][$add_date]++;
                        
                    }
                    
                    //echo "<pre>";
                    //print_r($arr_data["lokal"]);
                    //echo "</pre>";
                    
                    // data sales
                    $counter = 1;
                    $q = "
                            SELECT
                                `transaksi_header`.Kasir,
                                `transaksi_header`.Tanggal,
                                `transaksi_header`.Waktu,
                                `transaksi_header`.NoStruk,
                                (COALESCE(`transaksi_header`.Tunai, NULL, '0') - COALESCE(`transaksi_header`.Kembali, NULL, '0')) AS Tunai,
                                COALESCE(`transaksi_header`.KDebit, NULL, '0') AS KDebit,
                                COALESCE(`transaksi_header`.KKredit, NULL, '0') AS KKredit,
                                COALESCE(`transaksi_header`.Voucher, NULL, '0') AS Voucher,
                                COALESCE(`transaksi_header`.TotalNilai, NULL, '0') AS TotalNilai,
                                COALESCE(`transaksi_header`.TotalBayar, NULL, '0') AS TotalBayar
                            FROM
                                `transaksi_header`
                            WHERE
                                1
                                AND `transaksi_header`.Tanggal BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                                AND `transaksi_header`.status = '1'
                            ORDER BY
                                `transaksi_header`.Kasir ASC,
                                `transaksi_header`.Tanggal ASC,
                                `transaksi_header`.Waktu ASC
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($Kasir, $Tanggal, $Waktu, $NoStruk, $Tunai, $KDebit, $KKredit, $Voucher, $TotalNilai, $TotalBayar) = $row;
                        
                        $Kasir = strtolower($Kasir);
                        
                        $arr_data["list_data"][$counter] = $counter;
                        $arr_data["data_Kasir"][$counter] = $Kasir;
                        $arr_data["data_Tanggal"][$counter] = $Tanggal;
                        $arr_data["data_Waktu"][$counter] = $Waktu;
                        $arr_data["data_NoStruk"][$counter] = $NoStruk;
                        $arr_data["data_Tunai"][$counter] = $Tunai;
                        $arr_data["data_KDebit"][$counter] = $KDebit;
                        $arr_data["data_KKredit"][$counter] = $KKredit;
                        $arr_data["data_Voucher"][$counter] = $Voucher;
                        $arr_data["data_TotalNilai"][$counter] = $TotalNilai;
                        $arr_data["data_TotalBayar"][$counter] = $TotalBayar;
                        
                        if($Voucher*1!=0){ $arr_data["data_Tunai"][$counter] = 0; }
                        
                        $arr_data["total_TotalNilai"][$Tanggal] += $TotalNilai;
                        
                        if($arr_data["type_Kasir"][$Kasir]=="Oemah Herborist")
                        {
                            $arr_data["total_OH"][$Tanggal] += $TotalNilai;    
                        }
                        else if($arr_data["type_Kasir"][$Kasir]=="Luwus Rice")
                        {
                            $arr_data["total_LR"][$Tanggal] += $TotalNilai;    
                        }
                        else if($arr_data["type_Kasir"][$Kasir]=="Black Eye")
                        {
                            $arr_data["total_BE"][$Tanggal] += $TotalNilai;    
                        }
                        
                        $counter++;
                    }
                    
                    foreach($arr_data["list_data"] as $counter=>$val)
                    {
                        $Kasir = $arr_data["data_Kasir"][$counter];
                        $Tanggal = $arr_data["data_Tanggal"][$counter];
                        $Waktu = $arr_data["data_Waktu"][$counter];
                        $NoStruk = $arr_data["data_NoStruk"][$counter];
                        $Tunai = $arr_data["data_Tunai"][$counter];
                        $KDebit = $arr_data["data_KDebit"][$counter];
                        $KKredit = $arr_data["data_KKredit"][$counter];
                        $Voucher = $arr_data["data_Voucher"][$counter];
                        $TotalNilai = $arr_data["data_TotalNilai"][$counter];
                        $TotalBayar = $arr_data["data_TotalBayar"][$counter];
                        
                        $arr_data["list_data_oke"][$counter] = $counter;
                        
                        $bg_color = "";
                        //if($Voucher*1==0)
                        //{
                            if($TotalNilai!= ($Tunai+$KDebit+$KKredit+$Voucher) )
                            {
                                $bg_color = "background-color: yellow;";
                                
                                if($Voucher*1!=0)
                                {
                                    $q = "UPDATE transaksi_header SET Voucher = TotalNilai WHERE NoStruk = '".$NoStruk."'";
                                    if(!mysql_query($q)){ echo "Gagal Update transaksi_header Voucher dengan NoStruk ".$NoStruk."<br>"; }
                                    $q = "UPDATE transaksi_detail_bayar SET NilaiVoucher = '".$TotalNilai."' WHERE NoStruk = '".$NoStruk."'";
                                    if(!mysql_query($q)){ echo "Gagal Update transaksi_detail_bayar Voucher dengan NoStruk ".$NoStruk."<br>"; }
                                }
                            }
                        //}   
                        
                        // cek transaksi detail nya
                        $q = "
                                SELECT
                                    transaksi_detail_bayar.NoStruk
                                FROM
                                    transaksi_detail_bayar
                                WHERE
                                    1
                                    AND transaksi_detail_bayar.NoStruk = '".$NoStruk."'
                                ORDER BY
                                    transaksi_detail_bayar.NoStruk
                                LIMIT
                                    0,1
                        ";  
                        $qry = mysql_query($q);
                        $row = mysql_fetch_array($qry);
                        list($NoStruk_oke) = $row;
                        
                        if($NoStruk_oke=="")
                        {
                            // kalo gak ada, insert ke detail bayar
                            
                            if($Tunai*1!=0)
                            {
                                $jenis = "T";
                            }
                            else if($KDebit*1!=0)
                            {
                                $jenis = "D";
                            }
                            else if($KKredit*1!=0)
                            {
                                $jenis = "K";
                            }
                            else if($Voucher*1!=0)
                            {
                                $jenis = "V";
                            }
                            
                            $q = "
                                    INSERT INTO `transaksi_detail_bayar`
                                    SET `Gudang` = '00',
                                      `NoKassa` = '1',
                                      `NoStruk` = '".$NoStruk."',
                                      `Jenis` = '".$jenis."',
                                      `Kode` = 'UPD',
                                      `NomorKKredit` = '',
                                      `NomorKDebet` = '',
                                      `NomorVoucher` = '',
                                      `Status` = '',
                                      `Keterangan` = '',
                                      `ExpDate` = '0000-00-00',
                                      `NilaiTunai` = '".$Tunai."',
                                      `NilaiKredit` = '".$KKredit."',
                                      `NilaiDebet` = '".$KDebit."',
                                      `NilaiVoucher` = '".$Voucher."',
                                      `Currency` = 'IDR-1'
                            ";
                            if(!mysql_query($q))
                            {
                                die("Gagal transaksi_detail_bayar");
                            }
                        }
                    }  
                    
                     // data komisi
                      $q = "
                            SELECT
                                `finance_komisi_header`.NoTransaksi,
                                `finance_komisi_header`.TglTransaksi,
                                `finance_komisi_header`.Total
                            FROM
                                `finance_komisi_header`
                            WHERE
                                1
                                AND `finance_komisi_header`.TglTransaksi BETWEEN '".format_save_date($v_date_from)."' AND '".format_save_date($v_date_to)."'
                            ORDER BY
                                `finance_komisi_header`.TglTransaksi ASC
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($NoTransaksi, $TglTransaksi, $Total) = $row;    
                        
                        $arr_data["total_komisi"][$TglTransaksi] += $Total;
                        
                    }
                    
                    
                    // data compliment
                    $q = "
                            SELECT 
                              `transaksi_detail`.NoStruk,
                              `transaksi_detail`.Tanggal,
                              SUM(`transaksi_detail`.Qty * `transaksi_detail`.Harga) AS total 
                            FROM
                              `transaksi_detail` 
                              INNER JOIN `transaksi_header` ON
                                `transaksi_detail`.NoStruk = `transaksi_header`.NoStruk
                                AND `transaksi_header`.Status = '1'  
                                AND (`transaksi_header`.TotalNilai*1) = '0'  
                            WHERE 1 
                              AND `transaksi_detail`.Tanggal BETWEEN '".format_save_date($v_date_from)."' 
                              AND '".format_save_date($v_date_to)."'  
                            GROUP BY
                              `transaksi_detail`.NoStruk,
                              `transaksi_detail`.Tanggal
                    ";
                    $qry = mysql_query($q);
                    while($row = mysql_fetch_array($qry))
                    {
                        list($NoStruk, $Tanggal, $Total) = $row;    
                        
                        $arr_data["total_compliment"][$Tanggal] += $Total;  
                    }
                    
                    $table_border = 0;
                    if($btn_excel)
                    {
                        $table_border = 1;
                    }
                     
                    
                    ?>
                    <table class="table table-bordered responsive" style="color: black;" border="<?php echo $table_border; ?>">
                        <thead>
                            <tr>
                                <th width="30" rowspan="2" style="vertical-align: middle;">No</th>
                                <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
                                <th rowspan="2" style="vertical-align: middle;">Hari</th>
                                <th style="vertical-align: middle; text-align: center;" colspan="4">Tiket</th>
                                <th style="vertical-align: middle; text-align: center;" colspan="4">Sales</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Komisi</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Compliment</th>
                                <th rowspan="2" style="vertical-align: middle; text-align: right;">Omzet</th>
                            </tr>
                            
                            <tr>
                                <th style="text-align: right;">Lokal</th>
                                <th style="text-align: right;">Asing</th>
                                <th style="text-align: right;">Jumlah Ticket</th>
                                <th style="text-align: right;">Total (Rp)</th>
                            
                            
                                <th style="text-align: right;">Oemah Herborist</th>
                                <th style="text-align: right;">Luwus Rice</th>
                                <th style="text-align: right;">Black Eye</th>
                                <th style="text-align: right;">Total (Rp)</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <?php
                            $no = 1; 
                            foreach($arr_data["list_date"] as $date=>$val)
                            {
                                $TotalNilai = $arr_data["total_TotalNilai"][date("Y-m-d", $date)];
                                $total_OH = $arr_data["total_OH"][date("Y-m-d", $date)];
                                $total_LR = $arr_data["total_LR"][date("Y-m-d", $date)];
                                $total_BE = $arr_data["total_BE"][date("Y-m-d", $date)];
                                
                                $lokal = $arr_data["lokal"][date("Y-m-d", $date)];
                                $asing = $arr_data["asing"][date("Y-m-d", $date)];
                                $all_ticket = $arr_data["all_ticket"][date("Y-m-d", $date)];
                                
                                
                                $komisi = $arr_data["total_komisi"][date("Y-m-d", $date)];
                                $compliment = $arr_data["total_compliment"][date("Y-m-d", $date)];
                                
                                $omzet = $TotalNilai - $komisi;
                                
                                $bg_hari ="";
                                if(date("l", $date)=="Saturday" || date("l", $date)=="Sunday")
                                {
                                    $bg_hari = "background: #ff99cc;";
                                }
                                
                                if($btn_excel)
                                {
                                ?>
                                <tr style="<?php echo $bg_color; ?> color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo date("d/m/Y", $date); ?></td>
                                    <td style="<?php echo $bg_hari; ?>"><?php echo date("l", $date); ?></td>
                                    <td align="right"><?php echo format_number($lokal, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($asing, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($all_ticket, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo ""; ?></td>
                                    <td align="right"><?php echo format_number($total_OH, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($total_LR, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($total_BE, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($TotalNilai, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($komisi, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($compliment, 0, "", "", "ind"); ?></td>
                                    <td align="right"><?php echo format_number($omzet, 0, "", "", "ind"); ?></td>
                                </tr> 
                                <?php    
                                }
                                else
                                {
                        ?>
                            <tr style="<?php echo $bg_color; ?> color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
                                <td><?php echo $no; ?></td>
                                <td><?php echo date("d/m/Y", $date); ?></td>
                                <td style="<?php echo $bg_hari; ?>"><?php echo date("l", $date); ?></td>
                                <td align="right"><?php echo format_number($lokal); ?></td>
                                <td align="right"><?php echo format_number($asing); ?></td>
                                <td align="right"><?php echo format_number($all_ticket); ?></td>
                                <td align="right"><?php echo ""; ?></td>
                                <td align="right"><?php echo format_number($total_OH); ?></td>
                                <td align="right"><?php echo format_number($total_LR); ?></td>
                                <td align="right"><?php echo format_number($total_BE); ?></td>
                                <td align="right"><?php echo format_number($TotalNilai); ?></td>
                                <td align="right"><?php echo format_number($komisi); ?></td>
                                <td align="right"><?php echo format_number($compliment); ?></td>
                                <td align="right"><?php echo format_number($omzet); ?></td>
                            </tr>
                        <?php 
                                }
                                
                                $arr_total["lokal"] += $lokal;
                                $arr_total["asing"] += $asing;
                                $arr_total["all_ticket"] += $all_ticket;
                                $arr_total["OH"] += $total_OH;
                                $arr_total["LR"] += $total_LR;
                                $arr_total["BE"] += $total_BE;
                                $arr_total["Nilai"] += $TotalNilai;
                                $arr_total["Komisi"] += $komisi;
                                $arr_total["Compliment"] += $compliment;
                                $arr_total["Omzet"] += $omzet;
                        
                                $no++;
                            }
                        ?>
                        </tbody>
                        
                        
                        <?php
                            if($btn_excel)
                            {
                                ?>
                                <tfoot>
                                    <tr style="text-align: right; font-weight: bold; color: black;">
                                        <td colspan="3">Grand Total</td>
                                        <td align="right"><?php echo format_number($arr_total["lokal"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["asing"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["all_ticket"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number(0); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["OH"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["LR"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["BE"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Nilai"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Komisi"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Compliment"], 0, "", "", "ind"); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Omzet"], 0, "", "", "ind"); ?></td>
                                    </tr>
                                </tfoot> 
                                <?php     
                            }
                            else                                                 
                            {
                                ?>
                                 <tfoot>
                                    <tr style="text-align: right; font-weight: bold; color: black;">
                                        <td colspan="3">Grand Total</td>
                                        <td align="right"><?php echo format_number($arr_total["lokal"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["asing"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["all_ticket"]); ?></td>
                                        <td align="right"><?php echo format_number(0); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["OH"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["LR"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["BE"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Nilai"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Komisi"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Compliment"]); ?></td>
                                        <td align="right"><?php echo format_number($arr_total["Omzet"]); ?></td>
                                    </tr>
                                </tfoot> 
                                <?php
                            }
                        ?>
                        
                    </table>
                    <?php
                }
                
                if(!$btn_excel)
                {
            ?>
            
        
        </div>
        
        </form>
        <?php 
                
        ?>
        
<?php 
            include("footer.php"); 

            }
?>