<?php
$this->load->view('header');
?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<link rel="stylesheet" href="<?=base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">

<link rel="stylesheet" href="<?=base_url();?>public/css_calendar/eventCalendar.css">
<link rel="stylesheet" href="<?=base_url();?>public/css_calendar/eventCalendar_theme_responsive.css">
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>-->

<script type="text/javascript">
$(function() {
	$( "#SubDivisi" ).change(function() {
		var SubDivisi = $("#SubDivisi").val();
		//alert(SubDivisi);
		if (SubDivisi == 0){
		}else{
			var kdSubDivisi = {kdSubDivisi:$("#SubDivisi").val()};
			$.ajax({
					type: "POST",
					url : "<?php echo site_url('barcode/get_product')?>"+"/"+SubDivisi,
					data: kdSubDivisi,
					success: function(msg){
						$('#lblproduct').css('display','block');				
						$('#dv_product').html(msg);						
					}
			});
		}
	});
});
</script>
<body>

	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-gradient" data-collapsed="0">
			
				<div class="panel-heading">
					<div class="panel-title">
						Cetak Barcode
					</div>
					
					<div class="panel-options">
						<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
						<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
					</div>
				</div>
				
				<div class="panel-body">
				
					<form method='post' action='<?= base_url(); ?>index.php/barcode/<?=$action?>' role="form" class="form-horizontal form-groups-bordered">
						
						<div class="form-group" style="display:none">
							<label class="col-sm-2 control-label">Sub Divisi</label>
							
							<div class="col-sm-5">
								
								<select name="SubDivisi" id="SubDivisi" class="select2" data-allow-clear="true" data-placeholder="Select sub divisi...">
									<option></option>
									<optgroup label="">
										<?php foreach($SubDivisi as $rowkat){ ?>
										<option value="<?=$rowkat['KdSubDivisi'];?>"><?=$rowkat['NamaSubDivisi'];?></option>
										<? } ?>
									</optgroup>
								</select>
								
							</div>
						</div>
						
						<div class="form-group" style="display:block" id="lblproduct">
							<label class="col-sm-2 control-label">Nama Produk</label>
							
							<div class="col-sm-5">
								
								<!--<div id="dv_product">
								</div>-->
								<select name="KdProduk" id="KdProduk" class="select2" data-allow-clear="true" data-placeholder="Select sub divisi...">
									<option></option>
									<optgroup label="">
										<?php foreach($Product as $rowpro){ ?>
										<option value="<?=$rowpro['PCode'];?>"><?php echo $rowpro['PCode']." ~ ".$rowpro['NamaLengkap'];?></option>
										<? } ?>
									</optgroup>
								</select>
								
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Jumlah</label>
							
							<div class="col-sm-5">
								
								<input type="text" name="jml" class="form-control" id="field-1" style="width:100px">
								
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label"> </label>
							<div class="col-sm-6">
								<button class="btn btn-green" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Cetak</button>
							</div>
						</div>
						
					</form>
				
				</div>
				
			</div>
		
		</div>
	</div>

</body>

<link rel="stylesheet" href="<?=base_url();?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?=base_url();?>assets/js/select2/select2.css">
<script src="<?=base_url();?>assets/js/select2/select2.min.js"></script>
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?=base_url();?>public/js_calendar/moment.js" type="text/javascript"></script>
<script src="<?=base_url();?>public/js_calendar/jquery.eventCalendar.js" type="text/javascript"></script>
<!--<script src="<?=base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>-->
<?php $this->load->view('footer'); ?>