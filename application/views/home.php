<!-- panicbutton -->
<link rel="stylesheet" href="<?= base_url();?>assets/css/select2.css">
<link rel="stylesheet" href="<?= base_url();?>assets/fontawesome/css/font-awesome.min.css">
<!-- <script src="<?php echo base_url(); ?>assets/js/sweetalert2.all.js"></script> -->
<script src="<?= base_url();?>assets/js/newdatatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/js/newdatatables/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="<?= base_url();?>assets/fancybox/jquery.fancybox.min.css" />
<script src="<?= base_url();?>assets/fancybox/jquery.fancybox.min.js"></script>


<script>
function GetRevisi(){





  var jenisnya = $("#jrevisi").val();

  if(jenisnya == "OR"){
    $("#revgambar").removeClass('hide');
    $("#divnodok").removeClass('hide');
    $("#divdokumen").addClass('hide');

  }

  // AJAX
  $.ajax({
  type: 'ajax',
  method: 'post',
  url: '<?php echo base_url() ?>index.php/panicbutton/GetRevisi',
  data: {jenis: jenisnya},
  async: false,
  dataType: 'json',
  success: function(data1){
    $("#dokumen").removeClass('form-control');

    var html2 = '';
    var i;
    //$(".modal-body #bookId").val( data1.names );

    for(i=0; i<data1.length; i++){

        html2 +='<option value="'+data1[i].NoDokumen+'">'+data1[i].NoDokumen+'</option>';

    }
    $('#dokumen').html(html2);

  },
  error: function(){
    // alert('Could not Show Dokumen');
    swal("Anda Memilih Lainnya", "Silakan Jelaskan Permasalahan Anda Secara Detail!");
  }
});

// AJAX

// GET Approval
$.ajax({
type: 'ajax',
method: 'post',
url: '<?php echo base_url() ?>index.php/panicbutton/GetApproval',
data: {jenis: jenisnya},
async: false,
dataType: 'json',
success: function(data1){
  // $("#dokumen").removeClass('form-control');

  var html2 = '';
  var i;
  //$(".modal-body #bookId").val( data1.names );

  for(i=0; i<data1.length; i++){

      html2 +='<option value="'+data1[i].TelegramID+'">'+data1[i].employee_name+'</option>';

  }
  $('#approval').html(html2);

},
error: function(){
  alert('Could not Show Approval');
}
});
// GET Approval



    $.fn.select2.defaults = $.extend($.fn.select2.defaults, {
        allowClear: true, // Adds X image to clear select
        closeOnSelect: true, // Only applies to multiple selects. Closes the select upon selection.
        placeholder: 'Select...',
        minimumResultsForSearch: 5 // Removes search when there are 15 or fewer options
    });


        // Single select example if using params obj or configuration seen above
        var configParamsObj = {
            placeholder: 'Select an option...', // Place holder text to place in the select
            minimumResultsForSearch: 3 // Overrides default of 15 set above
        };
        $("#dokumen").select2(configParamsObj);



}
</script>

<style>

/* Panic Button Style */
.label-container{
position:fixed;
bottom:48px;
right:105px;
display:table;
visibility: hidden;
}

.label-text{
color:#FFF;
background:rgba(51,51,51,0.5);
display:table-cell;
vertical-align:middle;
padding:10px;
border-radius:3px;
}

.label-arrow{
display:table-cell;
vertical-align:middle;
color:#333;
opacity:0.5;
}

.float{
position:fixed;
width:60px;
height:60px;
bottom:40px;
right:40px;
background-color:#F33;
color:#FFF;
border-radius:50px;
text-align:center;
box-shadow: 2px 2px 3px #999;
z-index:1000;
animation: bot-to-top 2s ease-out;
}

.uli{
position:fixed;
right:40px;
padding-bottom:20px;
bottom:80px;
z-index:100;
}

.lii{
list-style:none;
margin-bottom:10px;
}

.hide{
display: none;
}

.aa{
background-color:#F33;
color:#FFF;
border-radius:50px;
text-align:center;
box-shadow: 2px 2px 3px #999;
width:60px;
height:60px;
display:block;
}

ul:hover{
visibility:visible!important;
opacity:1!important;
}


.my-float{
font-size:24px;
margin-top:18px;
}
/*
.sfloat{
font-size: 20px;
} */

a#menu-share + ul{
visibility: hidden;
}

a#menu-share:hover + ul{
visibility: visible;
animation: scale-in 0.5s;
}

a#menu-share i{
animation: rotate-in 0.5s;
}

a#menu-share:hover > i{
animation: rotate-out 0.5s;
}

@keyframes bot-to-top {
0%   {bottom:-40px}
50%  {bottom:40px}
}

@keyframes scale-in {
from {transform: scale(0);opacity: 0;}
to {transform: scale(1);opacity: 1;}
}

@keyframes rotate-in {
from {transform: rotate(0deg);}
to {transform: rotate(360deg);}
}

@keyframes rotate-out {
from {transform: rotate(360deg);}
to {transform: rotate(0deg);}
}

</style>




<!-- panicbutton -->



<script>
    function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
    {
        var centerWidth = (window.screen.width - windowWidth) / 2;
        var centerHeight = (window.screen.height - windowHeight) / 2;
        //alert('aaaa');

        newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth +
            ',height=' + windowHeight +
            ',left=' + centerWidth +
            ',top=' + centerHeight
        );

        newWindow.focus();
        return newWindow.name;
    }

    function pop_up_memo()
    {
        windowOpener('600', '800', 'Memo', '<?php echo base_url(); ?>inventory/npm_memo.php', 'Memo')
    }

    function pop_up_memo_edit(memo_id)
    {
        windowOpener('600', '800', 'Memo', '<?php echo base_url(); ?>inventory/npm_memo.php?action=edit&id='+memo_id, 'Memo')
    }

    function pop_up_memo_delete(memo_id)
    {
        windowOpener('600', '800', 'Memo', '<?php echo base_url(); ?>inventory/npm_memo.php?action=delete&id='+memo_id, 'Memo')
    }
</script>

<?php
    function format_show_datetime($date)
    {
        $arr_format = explode(" ", $date);
        $arr_date   = explode("-", $arr_format[0]);
        $arr_hour   = explode(":", $arr_format[1]);

        $return     = $arr_date[2]."/".$arr_date[1]."/".$arr_date[0]." ".$arr_hour[0].":".$arr_hour[1].":".$arr_hour[2];

        return $return;
    }
?>

<div class="profile-env">
	<header class="row">
		<div class="col-sm-2">
			<a href="#" class="profile-picture">
				<img src="<?php echo base_url(); ?>public/images/<?php echo $this->session->userdata('Logo');?>" style="width: 115px;" class="img-responsive "> </a>
		</div>
		<div class="col-sm-7">
			<ul class="profile-info-sections">
				<li>
					<div class="profile-name">
						<?php
							if($employee_id)
							{
								echo "<strong>Hai,</strong>";
								echo "<span><a>".$myprofile->employee_name."</a></span>";
                // print_r($panicbutton);
							}
							else
							{
								echo "<strong>Hai,</strong>";
								echo "<span><a>".$username."</a></span>";

							}
						?>


					</div>
				</li>
			</ul>
		</div>

	</header>

	<section class="profile-info-tabs">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-10">
				<p>Selamat Datang di Aplikasi <strong><?php echo $this->session->userdata('PT');?></strong>, Silakan pilih menu disamping untuk mengelola aplikasi.</p>
			</div>
		</div>
	</section>

    <?php
        if($username=="ambar0410" || $username=="dicky0707" || $username=="hendri1003" || $username=="frangky2311" || $username=="krisna337")
        {
    ?>
    <div title="TAMBAH MEMO" style="cursor: pointer;" onclick="pop_up_memo()"><img src="<?php echo base_url(); ?>inventory/images/add.gif" alt=""> TAMBAH MEMO</div>
    <?php
        }

            unset($arr_data);
            if(!isset($arr_data)){ $arr_data = isset($arr_data); } else { $arr_data = $arr_data; }

            $q = "
                    SELECT
                        memo.memo_id,
                        memo.memo_date,
                        memo.subject,
                        memo.content,
                        memo.edited_date,
                        memo.edited_user
                    FROM
                        memo
                    WHERE
                        1
                    ORDER BY
                        memo.edited_date DESC
                    LIMIT
                        0,1
            ";
            $qry = mysql_query($q);
            while($row = mysql_fetch_array($qry))
            {
                list(
                    $memo_id,
                    $memo_date,
                    $subject,
                    $content,
                    $edited_date,
                    $edited_user
                ) = $row;

                $arr_data["list_memo"][$memo_id] = $memo_id;
                $arr_data["memo_date"][$memo_id] = $memo_date;
                $arr_data["subject"][$memo_id] = $subject;
                $arr_data["content"][$memo_id] = $content;
                $arr_data["edited_date"][$memo_id] = $edited_date;
                $arr_data["edited_user"][$memo_id] = $edited_user;
            }
    ?>
    <br>
    <?php
        if(count($arr_data["list_memo"])*1>0)
        {
            foreach($arr_data["list_memo"] as $memo_id=>$val)
            {
                $memo_date = $arr_data["memo_date"][$memo_id];
                $subject = $arr_data["subject"][$memo_id];
                $content = $arr_data["content"][$memo_id];
                $edited_date = $arr_data["edited_date"][$memo_id];
                $edited_user = $arr_data["edited_user"][$memo_id];

                $content_echo = str_replace("\n", "<br>", $content);
        ?>
        <div title="<?php echo $subject; ?>" style="margin-bottom: 5px;">
            <div style="font-size: 14px; font-weight: bold;">

                <?php
                    if($username=="ambar0410" || $username=="dicky0707" || $username=="hendri1003")
                    {

                ?>
                <img style="cursor: pointer;" onclick="pop_up_memo_edit('<?php echo $memo_id; ?>')" src="<?php echo base_url(); ?>inventory/images/edit.png" alt="Edit <?php echo $subject; ?>" title="Edit <?php echo $subject; ?>">
                <img style="cursor: pointer;" onclick="pop_up_memo_delete('<?php echo $memo_id; ?>')" src="<?php echo base_url(); ?>inventory/images/delete.gif" alt="Hapus <?php echo $subject; ?>" title="Hapus <?php echo $subject; ?>">
                <?php
                    }
                ?>
                <?php echo $subject; ?>
            </div>
            <div style="font-size: 11px; font-weight: bold;"><?php echo $edited_user; ?> Last Edited : <?php echo format_show_datetime($edited_date); ?></div>

            <div style="font-size: 12px; border-bottom: 2px solid gray; margin-bottom: 5px; margin-top: 5px;"><?php echo $content_echo; ?></div>
        </div>
        <?php
            }
        }
    ?>





</div>
<div id="panic">
  <?php if($this->session->userdata('panicbutton')== "Y"){

    ?>

  <a href="#" class="float tooltip-primary" id="menu-share" data-toggle="tooltip" data-placement="top" data-original-title="Panic Button">
  <i class="fa fa-database my-float"></i>
  </a>
  <ul class="uli">
  <li class="lii"><a href="#" id="menu-facebook" class="aa tooltip-primary id-status" data-toggle="tooltip" data-placement="top" data-original-title="Buat Request">
  <i class="fa fa-pencil my-float"></i>
  </a></li>
  <li class="lii"><a href="#" id="menu-twitter" class="aa tooltip-primary id-data" data-toggle="tooltip" data-placement="top" data-original-title="Data Request">
  <i class="fa fa-bullhorn my-float"></i>
  </a></li>
  <li class="lii"><a href="#" id="menu-twitter" class="aa tooltip-primary item-history id-history" data-toggle="tooltip" data-placement="top" data-original-title="Status Request">
  <i class="fa fa-bell-o my-float"></i>
  </a></li>
  <?php
    if($username=="krisna337" || $username=="tonny1205" || $username=="mechael0101"){


   ?>
  <li class="lii"><a href="#" id="menu-twitter" class="aa tooltip-primary item-all id-all" data-toggle="tooltip" data-placement="top" data-original-title="All Request">
  <i class="fa fa-table my-float"></i>
  </a></li>
  </ul>

  <?php
  }
  }
  ?>


  <script>

  function ShowDetail(tes){
  	var go = tes;
  	var id = $('#id'+go).val();
  	//ajaxget data1
  	$.ajax({
  	type: 'ajax',
  	method: 'post',
  	url: '<?php echo base_url() ?>index.php/panicbutton/getDataRevisi',
  	data: {norevisi: id},
  	async: false,
  	dataType: 'json',
  	success: function(data1){

  		var status;
  		if(data1.Status == '0'){
  			status = "Pending";
  		}else if (data1.Status == '1') {
  			status = "Approve";
  		}else {
  			status = "Reject";
  		}
  		swal({
    title: 'Detail Request <u>'+id+'<u> ',
    html: '<table class="table table-bordered table-hover" align="right"><tr><td>No Revisi</td><td>'+data1.NoRevisi+'</td></tr>'
  	+'<tr><td>Jenis</td><td>'+data1.JenisRevisi+'</td></tr>'
  	+'<tr><td>No Dokumen</td><td>'+data1.NoDokumen+'</td></tr>'
  	+'<tr><td>Approval</td><td>'+data1.Approval+'</td></tr>'
  	+'<tr><td>Waktu Request</td><td>'+data1.TglRequest+'</td></tr>'
  	+'<tr><td>Pemohon</td><td>'+data1.NamaLengkap+'</td></tr>'
  	+'<tr><td>Status</td><td>'+status+'</td></tr>'
  	+'<tr><td>Alasan</td><td>'+data1.Alasan+'</td></tr>'
  	+'<tr><td>Waktu Respon</td><td>'+data1.TglApprove+'</td></tr>'
  	+'<tr><td colspan="2">'
  	+'<a data-fancybox="gallery" href="<?= base_url()?>'+data1.image+'"><img src="<?= base_url()?>'+data1.image+'" style="width:100%;" alt="NO IMAGE"></a></td></tr></table>'

  })


  	},
  	error: function(){
  		alert('Could not Show Fitur');
  	}
  	});
  	//ajax


  }

  $('#panic').on('click', '.id-status', function(){
  		$('#modal-default').modal('show');
  });

  $('#panic').on('click', '.id-data', function(){
  		$('#modal-status').modal('show');

  });

  $('#panic').on('click', '.id-history', function(){
  		$('#modal-history').modal('show');
  });

  $('#panic').on('click', '.id-all', function(){
  		$('#modal-list').modal('show');
  });

  function getDataAllReq(){
  	var user = "<?=$this->session->userdata('username')?>";
  	// GET Approval
  	$.ajax({
  	type: 'ajax',
  	method: 'post',
  	url: '<?php echo base_url() ?>index.php/panicbutton/GetAll',
  	data: {user: user},
  	async: false,
  	dataType: 'json',
  	success: function(data1){
  		// $("#dokumen").removeClass('form-control');

  		var html2 = '';
  		var html3 = '';
  		var i;
  		//$(".modal-body #bookId").val( data1.names );
  		var no =1;
  		for(i=0; i<data1.length; i++){
  				var status;

  				var approveby;

  				if(data1[i].Respon_by=='1'){
  					approveby = '<span class="badge" style="background-color:#0088cc !important;color:white"><i class="fa fa-telegram"></i> Mobile</span>';
  				}else if(data1[i].Respon_by=='2'){
  					approveby = '<span class="badge" style="background-color:#00a65a !important;color:white"><i class="fa fa-laptop"></i> Desktop</span>';
  				}else {
  					approveby = '<span class="badge" style="background-color:#f39c12 !important;color:white">norespon</i></span> ';
  				}


  				var id =data1[i].NoRevisi;
  				if(data1[i].Status == '0'){
  					status = '<span class="badge" style="background-color:#f39c12 !important;color:white">Pending</span>';
  				}else if (data1[i].Status == '1') {
  					status = '<span class="badge" style="background-color:#00a65a !important;color:white">Approve</span>';
  				}else if (data1[i].Status == '3') {
  					status = '<span class="badge" style="background-color:#0088cc !important;color:white">On Prosess</span>';
  				}else if (data1[i].Status == '4') {
  					status = '<span class="badge" style="background-color:#00e676 !important;color:white">Done</span>';
  				}else {
  					status = '<span class="badge" style="background-color:#dd4b39 !important;color:white">Reject</span>';
  				}
  				if(data1[i].Type == '1'){
  					html2 +='<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk Detail" style="cursor:pointer; ">'+
  									'<td onclick="ShowDetail('+no+')">'+
  									'<input type="hidden" id="id'+no+'" value="'+id+'">'+
  									data1[i].NoRevisi+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].NoDokumen+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].TglRequest+
  									'</td>'+
  									'<td align="center" onclick="ShowDetail('+no+')">'+
  									status+
  									'</td>'+
  									'<td>'+
  									approveby+
  									'</td>'+
  									'</tr>';

  									no++;
  				}else {
  					html3 +='<tr>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									'<input type="hidden" id="id'+no+'" value="'+id+'">'+
  									data1[i].NoRevisi+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].NoDokumen+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].TglRequest+
  									'</td>'+
  									'<td align="center">'+
  									status+
  									'</td>'+
  									'<td>'+
  									'<select data="'+data1[i].NoRevisi+'" class="form-control item-respon">'+
  									'<option value="">Status</option>'+
  									'<option value="0">Pending</option>'+
  									'<option value="3">On Proses</option>'+
  									'<option value="4">Done</option>'+
  									'<option value="5">Reject</option>'+
  									'</select>'+
  									'</td>'+
  									'</tr>';

  									no++;
  				}

  		}
  		$('#isiall').html(html2);
  		$('#isiall2').html(html3);
  		$('[data-toggle="tooltip"]').tooltip();
  	},
  	error: function(){
  		alert('Could not Show Approval');
  	}
  	});
  }

  $('#panic').on('click', '.item-all', function(){

  getDataAllReq();
  	// GET Approval

  $('#tall').DataTable();
  $('#tall2').DataTable();

  });

  $('#isiall2').on('change', '.item-respon', function(){
  	var id = $(this).attr('data');
  	var value = $(this).val();
  	// alert(id+value);

  	// break;

  	//ajax approve
  			$.ajax({
  			type: 'ajax',
  			method: 'post',
  			url: '<?php echo base_url() ?>index.php/panicbutton/responreq',
  			data: {id:id,status:value},
  			async: false,
  			dataType: 'json',
  			success: function(data){
  				swal("Respon Request Berhasil");
  				$('#isiall2').html("<tr><td colspan='5'>Loading..</td></tr>");
  				// getDataRequest();
  				getDataAllReq();

  				// $('#isistatusnya').html("<tr><td colspan='5'>Loading..</td></tr>");
  			},
  			error: function(){
  				alert('Could not get Data from Database');
  			}
  		});
  	//ajax approve

  });

  $('#panic').on('click', '.item-history', function(){

  	var user = "<?=$this->session->userdata('username')?>";
  	// GET Approval
  	$.ajax({
  	type: 'ajax',
  	method: 'post',
  	url: '<?php echo base_url() ?>index.php/panicbutton/GetHistory',
  	data: {user: user},
  	async: false,
  	dataType: 'json',
  	success: function(data1){
  		// $("#dokumen").removeClass('form-control');

  		var html2 = '';
  		var html3 = '';
  		var i;

  		//$(".modal-body #bookId").val( data1.names );
  		var no =1;
  		for(i=0; i<data1.length; i++){
  				var status;
  				var type;
  			  var approveby;

  				if(data1[i].Respon_by=='1'){
  					approveby = '<span class="badge" style="background-color:#0088cc !important;color:white"><i class="fa fa-telegram"></i> Mobile</span>';
  				}else if(data1[i].Respon_by=='2'){
  					approveby = '<span class="badge" style="background-color:#00a65a !important;color:white"><i class="fa fa-laptop"></i> Desktop</span>';
  				}else {
  					approveby = '<span class="badge" style="background-color:#f39c12 !important;color:white">norespon</i></span> ';
  				}

  				if(data1[i].JenisRevisi == 'DL'){
  					type = 'Pending DL';
  				}else if (data1[i].JenisRevisi == 'PL') {
  					type = 'Pending PL';
  				}else if (data1[i].JenisRevisi == 'MS') {
  					type = 'Pending MS';
  				}else if (data1[i].JenisRevisi == 'SO') {
  					type = 'Pending SO';
  				}
  				var id =data1[i].NoRevisi;
  				if(data1[i].Status == '0'){
  					status = '<span class="badge" style="background-color:#f39c12 !important;color:white">Pending</span>';
  				}else if (data1[i].Status == '1') {
  					status = '<span class="badge" style="background-color:#00a65a !important;color:white">Approve</span>';
  				}else {
  					status = '<span class="badge" style="background-color:#dd4b39 !important;color:white">Reject</span>';
  				}
  				if(data1[i].Type == '1'){
  					html2 +='<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk Detail" style="cursor:pointer; ">'+
  									'<td onclick="ShowDetail('+no+')">'+
  									'<input type="hidden" id="id'+no+'" value="'+id+'">'+
  									data1[i].NoRevisi+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									type+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].NoDokumen+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].TglRequest+
  									'</td>'+
  									'<td align="center" onclick="ShowDetail('+no+')">'+
  									status+
  									'</td>'+
  									'<td align="center">'+
  									approveby+
  									'</td>'+
  									'</tr>';

  									no++;
  				}else {
  					html3 +='<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk Detail" style="cursor:pointer; ">'+
  									'<td onclick="ShowDetail('+no+')">'+
  									'<input type="hidden" id="id'+no+'" value="'+id+'">'+
  									data1[i].NoRevisi+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									type+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].NoDokumen+
  									'</td>'+
  									'<td onclick="ShowDetail('+no+')">'+
  									data1[i].TglRequest+
  									'</td>'+
  									'<td align="center" onclick="ShowDetail('+no+')">'+
  									status+
  									'</td>'+
  									'</tr>';

  									no++;
  				}

  		}
  		$('#isihistory').html(html2);
  		$('#isihistory2').html(html3);
  		$('[data-toggle="tooltip"]').tooltip();

  	},
  	error: function(){
  		alert('Could not Show Approval');
  	}
  	});
  	// GET Approval

  $('#thistory').DataTable();
  $('#thistory2').DataTable();

  });

  function getDataRequest(){

  	var user = "<?=$this->session->userdata('username')?>";
  	// GET Status
  	$.ajax({
  	type: 'ajax',
  	method: 'post',
  	url: '<?php echo base_url() ?>index.php/panicbutton/GetStatus',
  	data: {user: user},
  	async: false,
  	dataType: 'json',
  	success: function(data1){
  		// $("#dokumen").removeClass('form-control');

  		var html2 = '';
  		// var html3 = '';
  		var i;
  		var no=1;
  		//$(".modal-body #bookId").val( data1.names );

  		for(i=0; i<data1.length; i++){
  				var status;
  				var id =data1[i].NoRevisi;
  				var action;
  				if(data1[i].Status == '0'){
  					action = '<button class="btn btn-success buttonapprove" data-toggle="tooltip" data="'+data1[i].NoRevisi+'" data-placement="top" data-original-title="Approve"><i class="fa fa-check"></i></button>'+'&nbsp'+
  					'<button class="btn btn-danger buttonreject" data-toggle="tooltip"  data="'+data1[i].NoRevisi+'" data-placement="top" data-original-title="Reject""><i class="fa fa-times"></i></button>';
  				}else{
  					action = 'No Action';
  				}
  				if(data1[i].Status == '0'){
  					status = '<span class="badge" style="background-color:#f39c12 !important;color:white">Pending</span>';
  				}else if (data1[i].Status == '1') {
  					status = '<span class="badge" style="background-color:#00a65a !important;color:white">Approve</span>';
  				}else {
  					status = '<span class="badge" style="background-color:#dd4b39 !important;color:white">Reject</span>';
  				}
  				html2 +='<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk Detail" style="cursor:pointer; ">'+
  								'<td onclick="ShowDetail('+no+')">'+
  								'<input type="hidden" id="id'+no+'" value="'+id+'">'+
  								data1[i].NoRevisi+
  								'</td>'+
  								'<td onclick="ShowDetail('+no+')">'+
  								data1[i].NoDokumen+
  								'</td>'+
  								'<td onclick="ShowDetail('+no+')">'+
  								data1[i].TglRequest+
  								'</td>'+
  								'<td align="center" onclick="ShowDetail('+no+')">'+
  								status+
  								'</td>'+
  								'<td>'+
  								action
  								'</td>'+
  								'</tr>';

  								no++;


  		}
  		$('#isistatusnya').html(html2);
  		$('[data-toggle="tooltip"]').tooltip();

  		// $('#isistatus2').html(html3);

  	},
  	error: function(){
  		alert('Could not Show Approval');
  	}
  	});
  	// GET Approval

  }

  $('#panic').on('click', '.id-data', function(){


  getDataRequest();


  $('#example').DataTable();
  // $('#example2').DataTable();

  });


  $('#isistatusnya').on('click', '.buttonapprove', async function(){
  	var id = $(this).attr('data');

  	// alert(id);
  	//ajax approve
  			$.ajax({
  			type: 'ajax',
  			method: 'post',
  			url: '<?php echo base_url() ?>index.php/panicbutton/approvereq',
  			data: {id:id},
  			async: false,
  			dataType: 'json',
  			success: function(data){
  				swal("Approve Request Berhasil");
  				$('#isistatusnya').html("<tr><td colspan='5'>Loading..</td></tr>");
  				getDataRequest();

  				// $('#isistatusnya').html("<tr><td colspan='5'>Loading..</td></tr>");
  			},
  			error: function(){
  				alert('Could not get Data from Database');
  			}
  		});
  	//ajax approve

  });


  // function Approve(id){
  // 	alert(id);
  // }


  function fixBootstrapModal() {
    $('#modal-status').modal('hide');
  }

  // call this before hiding SweetAlert (inside done callback):
  function restoreBootstrapModal() {
    $('#modal-status').modal('show');
  }

  $('#isistatusnya').on('click', '.buttonreject', async function(){
  	var id = $(this).attr('data');

  	// alert(id);
  	fixBootstrapModal();

  	const {value: text} = await swal({
    input: 'textarea',
    inputPlaceholder: 'Masukan Alasan Reject!',
    showCancelButton: true
  	})

  	if (text) {
  		//ajax reject
  				$.ajax({
  				type: 'ajax',
  				method: 'post',
  				url: '<?php echo base_url() ?>index.php/panicbutton/rejectreq',
  				data: {id:id,alasan:text},
  				async: false,
  				dataType: 'json',
  				success: function(data){
  					swal("Reject Request Berhasil");
  					$('#isistatusnya').html("<tr><td colspan='5'>Loading..</td></tr>");
  				},
  				error: function(){
  					alert('Could not get Data from Database');
  				}
  			});
  		//ajax reject
  		// $('#modal-status').removeData('modal');
  		getDataRequest();
  	  // swal(text)
  		restoreBootstrapModal();
  	}

  });

  </script>

  <script>
  //ajax
$("#requestform").unbind('submit').bind('submit', function() {

  // alert('TES AJA');
  if($('#cekbox').not(':checked').length){
    swal("Gagal!", "Pastikan Kalimat Pernyataan di Centang!", "error");
    return false;
  }else{

    $('#btnSave').val('Sending...');
    $('#btnSave').attr('disabled', true);

    var form = $(this);


    $.ajax({
      url: form.attr('action'),
      type: form.attr('method'),
      data:new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      dataType: 'json',
      success:function(response) {
        if(response.success == true) {
          $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
            response.messages+
          '</div>').fadeIn().delay(4000).fadeOut('slow');

          $('#btnSave').val('Request');
          $('#btnSave').attr('disabled', false);

          $("#requestform")[0].reset();
          $(".text-danger").remove();
          $(".form-group").removeClass('has-error').removeClass('has-success');

        }
        else {
          $("#messages").html('<div class="alert alert-danger alert-dismissible" role="alert">'+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
            response.messages+
          '</div>').fadeIn().delay(4000).fadeOut('slow');
          $.each(response.messages, function(index, value) {
            var element = $("#"+index);

            $(element)
            .closest('.form-group')
            .removeClass('has-error')
            .removeClass('has-success')
            .addClass(value.length > 0 ? 'has-error' : 'has-success')
            .find('.text-danger').remove();

            $(element).after(value);

          });
        }

      } // /success

    });	 // /ajax

    return false;
  }






});
// endajax
  </script>
</div>
