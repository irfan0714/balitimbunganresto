<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Neon Admin Panel"/>
    <meta name="author" content=""/>

    <title>Inventory & Retail System</title>
    <link rel="shortcut icon" href="<?= base_url();?>public/images/logos.png" >

    <!--<link rel="stylesheet" href="<?= base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">
    <script src="<?= base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>-->

		<link rel="stylesheet" href="<?= base_url();?>public/css/css/menu.css"/>
		<link rel="stylesheet" href="<?= base_url();?>public/css/css/main.css"/>
		<link rel="stylesheet" href="<?= base_url();?>public/css/css/bgimg.css"/>
		<link rel="stylesheet" href="<?= base_url();?>public/css/css/font.css"/>
		<link rel="stylesheet" href="<?= base_url();?>public/css/css/font-awesome.min.css"/>
</head>
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
    var baseurl = '';
</script>

<div class="login-container">
    
    <div class="login-form">

    <div class="login-content">
    		
	<div class="background"></div>
	<div class="backdrop"></div>
	<div class="login-form-container" id="login-form">
		<div class="login-form-content">
			<div class="login-form-header">
				<div class="logo">
					<img src="<?= base_url(); ?>public/images/LogoBaliTimbungan.png" width="120" alt=""/>
				</div>
				<h3><b>LOGIN USER</b></h3>
			</div>
						
			<form action="<?= base_url(); ?>index.php/welcome/setNaturaSessionDB" method="post" class="login-form">
				<div class="input-container">
					<img src="<?= base_url(); ?>public/images/users.png" width="20" alt=""/>
					<input type="text" name="kode" placeholder="Username" style="width: 200px;border: 0;"/>
				</div>
				<div class="input-container">
					<img src="<?= base_url(); ?>public/images/keys.png" width="20" alt=""/>
					<input type="password"  id="login-password" class="input" name="nama" placeholder="Password"/>
				</div>
				<div class="input-container">
					<img src="<?= base_url(); ?>public/images/homes.png" width="20" alt=""/>
					<!--<select name='CompanyID' class="input" id="CompanyID" style="width: 300px;">
                            <option value="">Choose Corporate</option>
                        	<option value="0">PT. NATURA PESONA MANDIRI</option>
                        	<option value="1">PT. BALI SUMBER SAMUDERA</option>
                    </select>-->
                    
                    <select class="input" name="CompanyID" id="CompanyID" style="width: 300px;">
	            		
	            		<?php
	            		foreach($corporate as $val)
	            		{
							?><option value="<?php echo $val["id"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>
				</div>
				<div class="rememberme-container" style="display: none;">
					<label for="rememberme" class="rememberme"><span>Biarkan tetap masuk</span></label>
				</div>
				
				<input type="submit" name="login" value="Login" class="button"/>
			
			</form>
		</div>
	    </div>
			
        </div>

    </div>

</div>


<!-- Bottom Scripts -->
<script src="<?= base_url(); ?>assets/js/gsap/main-gsap.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.js"></script>
<script src="<?= base_url(); ?>assets/js/joinable.js"></script>
<script src="<?= base_url(); ?>assets/js/resizeable.js"></script>
<script src="<?= base_url(); ?>assets/js/neon-api.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/neon-login.js"></script>
<script src="<?= base_url(); ?>assets/js/neon-custom.js"></script>
<script src="<?= base_url(); ?>assets/js/neon-demo.js"></script>

</body>
</html>