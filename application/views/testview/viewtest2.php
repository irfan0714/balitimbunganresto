<?php $this->load->view('header_part1')?>
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
</head>
<?php $this->load->view('header_part3')?>

<form method="POST"  name="search" action='<?=base_url(); ?>index.php/transaksi/permintaan_barang/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
			<input type='text' name='optionValue' size='20' maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" />
            &nbsp;<b>Gudang</b>&nbsp;
			<select name="option" class="form-control-new"  id="option">
				<option value='PCode'>PCODE</option>
				<option value='NamaLengkap'>NAMA BARANG</option>
			</select>
            &nbsp;
        </div>
        <div class="col-md-4" align="right">
			<i class="entypo-search"></i><input type='submit' name='submit' class="btn btn-info btn-icon btn-sm icon-left" value='Cari' />
            <a href="<?=site_url()?>testfolder/testclass/" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="50"><center>PCODE</center></th>
				<th><center>NAMA BARANG</center></th>
				<th width="10"><center>EDIT</center></th>
			</tr>
        </thead>
        <tbody>
			<?php 
			if(count($viewdata)==0) {
				echo "<tr><td colspan='3' align='center'>Tidak Ada Data</td></tr>";
			}
			for($i=0 ; $i<count($viewdata) ; $i++) { 
				$bgcolor	= ($i % 2 == 0)?"#f7f7f7":"white";
			?>
			<tr style="background:<?=$bgcolor?>">
				<td><?=$viewdata[$i]['PCode'];?></td>
				<td><?=$viewdata[$i]['NamaLengkap'];?></td>
				<td align='center'>
					<a href="<?=site_url()?>testfolder/testclass/<?=$viewdata[$i]['PCode']?>">
						<i class="entypo-pencil"></i>
					</a>
				</td>
			</tr>
			<?php } ?>         
        </tbody>
    </table>
	
    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?=$this->pagination->create_links()?>
            </div>
        </div>
    </div>
</div>
	<?php 
	$this->load->view('footer'); 
//	$this->load->view('footer_part1');
	?>
<!--dua div dibawah, untuk nutup div yg ada di header-->
	</div>	
</div>
<!--eo dua div diatas-->
</body>
</html>