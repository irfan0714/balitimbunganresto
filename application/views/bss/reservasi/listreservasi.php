<?php 
$this->load->view('header'); 
$UserLevel = $this->session->userdata('userlevel');
$ses_login = $this->session->userdata('username');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
    function toRp(a, b, c, d, e) {
        e = function (f) {
            return f.split('').reverse().join('')
        };
        b = e(parseInt(a, 10).toString());
        for (c = 0, d = ''; c < b.length; c++) {
            d += b[c];
            if ((c + 1) % 3 === 0 && c !== (b.length - 1)) {
                d += '.';
            }
        }
        return'Rp.\t' + e(d) + ',00'
    }

    $("input").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("radio").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    function ajax_edit(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('bss/reservasi/ajax_edit_bayar') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       //alert(data);
            {
                var sisa = 0;
                $('[name="NoDokumen"]').val(data.NoDokumen);
                $('[name="DisplayNoDokumen"]').text(data.NoDokumen);
                
                $('[name="Nama"]').val(data.Nama);
                $('[name="DisplayNama"]').text(data.Nama);
                
                $('[name="Total"]').val(data.Total);
                $('[name="DisplayTotal"]').text(toRp(data.Total));
                
                $('[name="Dp"]').val(data.dp);
                $('[name="DisplayDp"]').text(toRp(data.dp));
                
                sisa = $('[name="Total"]').val() - $('[name="Dp"]').val();
                $('[name="Sisa"]').val(sisa);
                $('[name="DisplaySisa"]').text(toRp(sisa));
                $('#modal_payment_method').modal('show');
                $('.modal-title').text('Metode Pembayaran');
                document.getElementById('v_no_bukti').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
    }

    function save()
    {
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled', true);
        var url;

        if (save_method == 'add') {
            url = "<?php echo site_url('bss/reservasi/ajax_add') ?>";
        } else {
            url = "<?php echo site_url('bss/reservasi/ajax_update_bayar') ?>";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function (data)
            {
                if (data.status)
                {
                    id= data.NoDokumen;
                    $('#modal_payment_method').modal('hide');
                    print_langsung(id);
                    window.location.reload(true);
                    //alert("OK");
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('save');
               // $('#btnSave').attr('disabled', false);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //alert('Error adding / update data');
                $('#modal_payment_method').modal('hide');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled', false);

            }
        });
    }

    function cek_bayar() {
        $(function () {
            sisa = $("#Sisa").val();
            if ($('input[name=rbtPembayaran]:checked').val() == "T") {
//                $("#PaymentT").attr("style", "display:''");
//                $("#PaymentD").attr("style", "display:none");
//                $("#PaymentK").attr("style", "display:none");
                $("#PaymentT").val(sisa);
                $("#PaymentD").val(0);
                $("#PaymentK").val(0);
            } else if ($('input[name=rbtPembayaran]:checked').val() == "K") {
//                $("#PaymentT").attr("style", "display:none");
//                $("#PaymentD").attr("style", "display:none");
//                $("#PaymentK").attr("style", "display:''");
                $("#PaymentK").val(sisa);
                $("#PaymentT").val(0);
                $("#PaymentD").val(0);
            } else {
//                $("#PaymentT").attr("style", "display:none");
//                $("#PaymentD").attr("style", "display:''");
//                $("#PaymentK").attr("style", "display:none");
                $("#PaymentD").val(sisa);
                $("#PaymentT").val(0);
                $("#PaymentK").val(0);
            }
        });
    }

    function print_langsung(id)
    {
        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('bss/reservasi/cetakStruk') ?>/" + nodok,
            type: "POST",
            success: function (data)
            {
                $('#modal_payment_method').modal('hide');
            }
//            ,
//            error: function (textStatus, errorThrown)
//            {
//                alert('Error Print');
//            }
        }
        );
    }

    function PopUpPrint(kode, baseurl)
    {
        url = "index.php/bss/reservasi/create_pdf/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=1280,height=800,top=50,left=50');
    }

    function PopUpPrint2(kode, baseurl)
    {
        url = "index.php/bss/reservasi/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=1280,height=800,top=50,left=50');
    }
    
    function windowOpener(windowHeight, windowWidth, windowName, windowUri, name)
    {
        var centerWidth = (window.screen.width - windowWidth) / 2;
        var centerHeight = (window.screen.height - windowHeight) / 2;
        //alert('aaaa');

        newWindow = window.open(windowUri, windowName, 'resizable=yes,scrollbars=yes,width=' + windowWidth +
            ',height=' + windowHeight +
            ',left=' + centerWidth +
            ',top=' + centerHeight
        );

        newWindow.focus();
        return newWindow.name;
    } 
    
    function pop_proforma(NoDokumen)
    {
        windowUri = "<?php echo base_url(); ?>inventory/reservasi_pop_proforma.php?id="+NoDokumen;
        windowOpener('600', '800', 'Print Proforma', windowUri, 'Print Proforma');
    }
    
    function cancel_reservasi(id)
    {
     
     	save_method = 'edit';
     	
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('bss/reservasi/ajax_cancel_reservasi') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       
            {
                $('[name="id"]').val(data.id);
                $('[name="v_no_beo"]').val(data.NoDokumen);
                $('[name="v_nama_travel"]').val(data.Nama);
                $('[name="v_date_reservasi"]').val(data.TglDokumenx);
                
                $('#modal_cancel_reservasi').modal('show');
                $('#btnSave').attr('disabled',false);
                $('.modal-title').text('Cancel Reservasi');
                document.getElementById('v_alasan').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
              
    }
    
    function save_cancel_reservasi()
    {
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled', true);
        var url;

        if (save_method == 'add') {
            url = "<?php echo site_url('bss/reservasi/cancel_reservasi') ?>";
        } else {
            url = "<?php echo site_url('bss/reservasi/cancel_reservasi') ?>";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function (data)
            {
                if (data.status)
                {
                    $('#modal_cancel_reservasi').modal('hide');
                    window.location.reload(true);
                   
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('save');
               // $('#btnSave').attr('disabled', false);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //alert('Error adding / update data');
                $('#modal_payment_method').modal('hide');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled', false);

            }
        });
    }
</script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/bss/reservasi/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="10" maxlength="10" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>"/>
            &nbsp;
            <b>Head Channel</b>&nbsp;
            <select class="form-control-new" name="search_head_channel" id="search_head_channel" style="width: 80px;">
                <option value="">All</option>
                <?php
                foreach ($supervisor as $val) {
                    $selected = "";
                    if ($search_head_channel) {
                        if ($val["KdSupervisor"] == $search_head_channel) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdSupervisor"]; ?>"><?php echo $val["NamaSupervisor"]; ?></option><?php
                }
                ?>
            </select> 
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status" style="width: 70px;">
                <option value="">All</option>
                <?php
                foreach ($status as $val) {
                    $selected = "";
                    if ($search_status) {
                        if ($val["KdStatus"] == $search_status) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdStatus"]; ?>"><?php echo $val["NamaStatus"]; ?></option><?php
                }
                ?>
            </select> 
            				<!--&nbsp;
            				<b>Bulan</b>&nbsp;
                            <select class="form-control-new" name="search_bulan" id="search_bulan">
                                    <option  <?= $search_bulan=='01' ? 'selected' : '' ; ?> value="01">Januari</option>
                                    <option  <?= $search_bulan=='02' ? 'selected' : '' ; ?> value="02">Februari</option>
                                    <option  <?= $search_bulan=='03' ? 'selected' : '' ; ?> value="03">Maret</option>
                                    <option  <?= $search_bulan=='04' ? 'selected' : '' ; ?> value="04">April</option>
                                    <option  <?= $search_bulan=='05' ? 'selected' : '' ; ?> value="05">Mei</option>
                                    <option  <?= $search_bulan=='06' ? 'selected' : '' ; ?> value="06">Juni</option>
                                    <option  <?= $search_bulan=='07' ? 'selected' : '' ; ?> value="07">Juli</option>
                                    <option  <?= $search_bulan=='08' ? 'selected' : '' ; ?> value="08">Agustus</option>
                                    <option  <?= $search_bulan=='09' ? 'selected' : '' ; ?> value="09">Sepetember</option>
                                    <option  <?= $search_bulan=='10' ? 'selected' : '' ; ?> value="10">Oktober</option>
                                    <option  <?= $search_bulan=='01' ? 'selected' : '' ; ?> value="11">November</option>
                                    <option  <?= $search_bulan=='12' ? 'selected' : '' ; ?> value="12">Desember</option>
                            </select>
                            
                            &nbsp;
            				<b>Tahun</b>&nbsp;
                           <select class="form-control-new" name="search_tahun" id="search_tahun">
                                    <option  <?= $search_tahun=='2017' ? 'selected' : '' ;?> value="2017">2017</option>
                                    <option  <?= $search_tahun=='2018' ? 'selected' : '' ;?> value="2018">2018</option>
                            </select>-->
                            
                    &nbsp;
		            <b>Tanggal</b>&nbsp;
		            <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_awal!="" && $search_tgl_awal!="00-00-0000") { echo $search_tgl_awal; }else{echo date('d-m-Y');}  ?>" name="v_tgl_mulai" id="v_tgl_mulai" size="10" maxlength="10">
			        &nbsp;s/d&nbsp;
			        <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_akhir!="" && $search_tgl_akhir!="00-00-0000") { echo $search_tgl_akhir; }else{echo date('d-m-Y');}  ?>" name="v_tgl_akhir" id="v_tgl_akhir" size="10" maxlength="10">
	        
	        		&nbsp;
            		<b>Sort By</b>&nbsp;
                    <select class="form-control-new" name="search_sort" id="search_sort" style="width: 70px;">
                          <option  <?= $search_sort=='1' ? 'selected' : '' ;?> value="1">No. BEO DESC</option>
                          <option  <?= $search_sort=='2' ? 'selected' : '' ;?> value="2">No. BEO ASC</option>
                          <option  <?= $search_sort=='3' ? 'selected' : '' ;?> value="3">Reservasi Date ASC</option>
                          <option  <?= $search_sort=='4' ? 'selected' : '' ;?> value="4">Reservasi Date DESC</option>
                   </select>
            
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-success btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Find<i class="entypo-search"></i></button>
			<?php if($UserLevel=="-1" || $UserLevel=="29"){ ?>
            <a href="<?php echo base_url() . "index.php/bss/reservasi/add_reservasi/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Add<i class="entypo-plus"></i></a>
            <a href="<?php echo base_url() . "index.php/bss/reservasi/booking/"; ?>" class="btn btn-primary btn-icon btn-sm icon-left" title="" >Booking<i class="entypo-plus"></i></a>
			<?php } ?>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

    <div class="row" style="display: none;">
        <div class="col-md-12">
        	<b><center><?php echo "Bulan ".date('m')." Tahun ".date('Y') ;?></center></b><br>
        	
        	<div class="col-md-12" align="center">
            <button type="button" class="btn btn-primary btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;<b>Total <?=$booking->booking;?> Booking</b>
           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-info btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;<b>Total <?=$beo->beo;?> BEO</b>
          
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          
            <button type="button" class="btn btn-default btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;<b>Total <?=$pending->pending;?> Pending</b>
         
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          
            <button type="button" class="btn btn-warning btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;<b>Total <?=$waiting->waiting;?> Waiting Approve</b>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-success btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;<b>Total <?=$approved->approved;?> Approved</b>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;<b>Total <?=$rejected->rejected;?> Rejected</b>
           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-inner"><i class="entypo-trash"></i></button>&nbsp;<b>Total <?=$canceled->canceled;?> Canceled</b>
           
            </div>
         </div>
    </div>
    
    <hr/>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th><center>No</center></th>
        <th ><center>No. Document</center></th>
        <th><center>Reservasi Date</center></th>
        <th ><center>Company</center></th>
        <th ><center>Group Code</center></th>
        <th><center>Participants</center></th>
        <!--<th><center>Activities</center></th>-->
        <th><center>Head Channel</center></th>
        <th width="250"><center>Status</center></th>
        <th ><center>Navigasi</center></th>
        <?php if($ses_login == "ni0504" OR $ses_login == "mechael0101" OR $ses_login == "tonny1205" ){ ?>
        <th ><center>&nbsp;</center></th>
        <?php } ?>
        </tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $kdtravel = $val["KdTravel"];
                $travel = $this->reservasi_model->getTravel($kdtravel);
                $nama = $travel->Nama;

                $bgcolor = "";
				
				if($ses_login == $val["HdChannel"]){
					if($val["status"]=="0" AND $val["status_approve"]=="0"){
						$bgcolor = "background:orange";
					}else if($val["status"]=="1" AND $val["status_approve"]=="0"){
					    $bgcolor = "background:yellow";
					}else{
						$bgcolor = "background:#00FFFF";
					}
				}else{
					if($UserLevel == '23' OR $UserLevel == '29' OR $UserLevel == '-1'){
						if(substr($val["NoDokumen"], 0, 3)=="SGV"){
							if($val["status"]=="0" AND $val["status_approve"]=="0" AND $val["status_reject"]=="0"){
							$bgcolor = "background:#6df9e0";
							}else if($val["status"]=="1" AND $val["status_approve"]=="0" AND $val["status_reject"]=="0"){
							    $bgcolor = "background:yellow";
							}else if($val["status"]=="0" AND $val["status_approve"]=="0" AND $val["status_reject"]=="1"){
							    $bgcolor = "background:#fcb79f";
							}else if($val["status"]=="4"){
							    $bgcolor = "background:#fc020f";
							}else{
								$bgcolor = "background:#ffffff";
							}
						}else{
							if($val["status"]=="4"){
								$bgcolor = "background:#fc020f";
							}else{
								$bgcolor = "background:#e3e8e3";
							}
						}
					}
					else if ($no % 2 == 0) {
						$bgcolor = "background:#f7f7f7;";
					}
				}
				
				if($val["status"]=="4"){
					$sts = "<font color='black'>Di Cancel pada ".$val["tgl_reject"].", karena ".$val["ket_reject"]."</font>";
                }else if($val["edit_request"]=="1"){
					$sts = "Waiting Approve Edit Request";
                }else if($val["status"]=="0" AND $val["status_approve"]=="0" AND $val["status_reject"]=="0"){
					$sts = "Pending";
				}else if($val["status"]=="1" AND $val["status_approve"]=="0" AND $val["status_reject"]=="0"){
					$sts = "Waiting Approve...";
				}else if($val["status"]=="0" AND $val["status_approve"]=="0" AND $val["status_reject"]=="1"){
					$sts = "<font color='red'>Reject Karena ".$val["ket_reject"]." pada ".$val["tgl_reject"]." oleh ".$val["NamaSupervisor"]."</font>";
				}else if($val["status"]=="1" AND $val["status_approve"]=="1" AND $val["status_reject"]=="0"){
					$sts = "<font color='blue'>Approve Pada ".$val["tgl_approve"]." oleh ".$val["NamaSupervisor"]."</font>";
				}else if($val["status"]=="1" AND $val["status_approve"]=="0" AND $val["status_reject"]=="1"){
					$sts = "<font color='green'>Waiting Approve After Reject, Karena ".$val["ket_reject"]."</font>";
				}else if($val["status"]=="1" AND $val["status_approve"]=="1" AND $val["status_reject"]=="1"){
					$sts = "<font color='blue'>Approve Kedua Setelah Reject Pada ".$val["tgl_approve"]." oleh ".$val["NamaSupervisor"]."</font>";
				}
				
				if($val["NoDokumen"]==""){
					$nodoks = "Booking";
				}else{
					$nodoks = $val["NoDokumen"];
				}
				
				if($val["status"]=="4"){
					$wrn = "black";
				}else{
					$wrn = "black";
				}
				
                ?>
                <tr title="<?php echo $nodoks; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" id="<?php echo $no; ?>">
                    <td><?php echo "<font color='".$wrn."'>".$no."</font>"; ?></td>
                    <td align="center"><?php echo "<font color='".$wrn."'>".$nodoks."</font>"; ?></td>
                    <td align="center"><?php echo "<font color='".$wrn."'>".$val["TglDokumen"]."</font>"; ?></td>
                    <td align="left"><?php echo "<font color='".$wrn."'>".$nama."</font>"; ?></td>
                    <td align="left"><?php echo "<font color='".$wrn."'>".$val["GroupCode"]."</font>"; ?></td>
                    <td align="center"><?php echo "<font color='".$wrn."'>".$val["adultParticipants"]." adult, ".$val["childParticipants"]." child, ".$val["kidsParticipants"]." infants</font>"; ?></td>
                    <!--<td align="left"><?php echo $val["nama_aktivitas_beo"]; ?></td>-->
					<td align=""><?="<font color='".$wrn."'>".$val["HdChannel"]."</font>";?></td>
                    <!--<td><?php echo $val["AddUser"]; ?></td>-->
                    <td align="center"><label><?php echo $sts; ?></label></td>
                    <td align="center">
						
						<!-- USER LEVEL SALES -->
						<?php if($UserLevel=="22" || $UserLevel=="25" || $UserLevel=="9" || $UserLevel=="13" ||$UserLevel=="10" || $UserLevel=="5" || $UserLevel=="32" ||$UserLevel=="6" || $UserLevel=="7" || $UserLevel=="15"){ ?>
								<!-- model lama 30052017 
								<a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->
								<?php if($val["status_approve"]=="1"){?>
								<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrint('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
									<i class="entypo-export"></i>
								</button>
								
								<a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
								<?php } ?>
                        <?php }else if($UserLevel=="-1" or $UserLevel=="23" or $UserLevel=="29"){ ?>
                                <?php if($val["status"]=="3"){ ?>
									<!--<button type="button" class="btn btn-red btn-sm sm-new tooltip-inner" disabled="" data-toggle="tooltip" data-placement="top" data-original-title="bayar" title="" onclick="ajax_edit('<?= $val['NoDokumen']; ?>')">
										<i class="entypo-print"></i>
									</button>-->
                                <?php }else{?>
                                    <?php if($ses_login=="mechael0101" || $ses_login=="tonny1205" || $ses_login=="ni0504"){?>
									<!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-inner"  data-toggle="tooltip" data-placement="top" data-original-title="bayar" title="" onclick="ajax_edit('<?= $val['NoDokumen']; ?>')">
										<i class="entypo-print"></i>
									</button>-->
								    <?php } ?>
                                <?php } ?>
								
								<?php if($ses_login=="mechael0101" || $ses_login=="tonny1205" || $ses_login=="ni0504"){?>
								<!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrint('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
                                    <i class="entypo-export"></i>
                                </button>-->
                                
                                <?php } ?>
								
                                <?php if( ($UserLevel=="-1" or $UserLevel=="23" or $UserLevel=="29") and ($val["status"]=="0" OR $val["status_reject"]=="1") AND ($ses_login == "ni0504" OR $ses_login == "tonny1205" OR $ses_login == "mechael0101")){?>                                
									<a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
								<?php } ?>
								
                                
								
						<?php }/*else{ ?>
								<?php if($UserLevel=="7"){?>
									<a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
								<?php }else{ ?>
									<a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
								<?php } ?>
								
						<?php } */?>
						
						<!-- if atau kondisi lain tertentu 1-->
                            
                        <?php if($ses_login=="hendri1003" || $ses_login=="sari1608"){?>
                            <a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <a href="javascript:void(0)" onclick="pop_proforma('<?php echo $val["id"]; ?>')" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Print Proforma" title=""><i class="entypo-print"></i></a>
                        <?php 
                            }
                        ?>
						
						<!-- if atau kondisi lain tertentu 2-->
							
						<?php if($UserLevel=="-1" or $UserLevel=="23" or $UserLevel=="29"){
								 
									if($val["status"]=="1" AND 
									   $val["status_approve"]=="0" AND 
									   $val["status_reject"]=="1"
									   AND $ses_login == $val["HdChannel"]){?>
									   
											<a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
									<?php 
									}else if($val["status"]=="1" AND 
									         $val["status_approve"]=="0" AND 
											 $val["status_reject"]=="0" AND 
											 $ses_login == $val["HdChannel"]){ ?>
											 
											 <a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            
									<?php 
									}else if($val["status"]=="1" AND $val["status_approve"]=="1"){?>
									
											 <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrint('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
			                                    <i class="entypo-export"></i>
			                                 </button>
			                                
											 <a href="<?php echo base_url(); ?>index.php/bss/reservasi/edit_reservasi/<?php echo $val["id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
									<?php }
						}
                        ?>
                    </td>
                    
                    <?php if($ses_login=="mechael0101" || $ses_login=="tonny1205" || $ses_login=="ni0504"){?>  
                	<td>
                	<?php if($val["status"]!="4"){?>
                	<!--<a href="<?php echo base_url(); ?>index.php/bss/reservasi/cancel_reservasi/<?php echo $val["id"]; ?>" class="btn btn-danger btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="top" title="Cancel"><i class="entypo-cross">&nbsp;<b>X</b>&nbsp;</i></a>-->
                	<button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit Bayar" title="" onclick="cancel_reservasi('<?php echo $val["id"]; ?>')" >
                                <i class="entypo-cross">&nbsp;<b>X</b>&nbsp;</i>
                    </button>	
                	</td>
                	<?php } ?>
                	<?php } ?> 
                
                </tr>
                             
                <?php
                $no++;
            }
            
            
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>
<!--
<div id="modal_payment_method" class="modal fade" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Metode Pembayaran</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NoDokumen</label>
                            <div class="col-md-6">
                                <input name="NoDokumen" class="form-control" type="hidden" readonly="readonly">
                                <span name="DisplayNoDokumen" class="form-control"></span> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">No Bukti</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" name="v_no_bukti" id="v_no_bukti" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker" value="" name="v_date_reservasi" id="v_date_reservasi" >
                            </div>
                        </div>
                                                
                        <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Company</label>
                            <div class="col-md-6">
                                <input name="Nama" class="form-control" type="hidden">
                                <span name="DisplayNama" class="form-control"></span>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="control-label col-md-3">Kasir</label>
                            <div class="col-md-6">
                                <input name="Kasir" class="form-control" type="hidden" readonly="readonly" value="<?php echo $this->session->userdata('username'); ?>">
                                <span name="DisplayKasir" class="form-control"><?php echo $this->session->userdata('username'); ?></span>
                            </div>-->
                            
                        
                            
                        <!--<div class="form-group">
                            <label class="control-label col-md-3">Kas / Bank</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_KdKasBank" id="v_KdKasBank" style="width: 100%;">
	            		<option value="">- Pilih Kas Bank -</option>
	            		<?php
	            		foreach($kasbank as $val)
	            		{
							?><option value="<?php echo $val["KdKasBank"]; ?>"><?php echo $val["NamaKasBank"]; ?></option><?php
						}
	            		?>
	            	</select>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Total</label>
                            <div class="col-md-6">
                                <input name="Total" class="form-control" type="hidden" readonly="readonly">
                                <span name="DisplayTotal" class="form-control"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">DP</label>
                            <div class="col-md-6">
                                <input name="Dp" class="form-control" type="hidden" readonly="readonly">
								<span name="DisplayDp" class="form-control"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Sisa Bayar</label>
                            <div class="col-md-6">
                                <input name="Sisa" id="Sisa" class="form-control" type="hidden" readonly="readonly">
                                <span name="DisplaySisa" class="form-control"></span>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="control-label col-md-3">Pembayaran</label>
                            <div class="col-md-6">
                                <label class="radio-inline">
                                    <input name="rbtPembayaran" type="radio" value="T" onclick="cek_bayar();" />Tunai
                                </label>
                                <label class="radio-inline">
                                    <input name="rbtPembayaran" type="radio" value="D" onclick="cek_bayar();"/>Debit
                                </label>
                                <label class="radio-inline">
                                    <input name="rbtPembayaran" type="radio" value="K" onclick="cek_bayar();"/>Kredit
                                </label>
                                <span class="help-block"></span>
                                <input name="PaymentT" id="PaymentT" class="form-control" type="text" style="display: none">
                                <input name="PaymentK" id="PaymentK" class="form-control" type="text" style="display: none">
                                <input name="PaymentD" id="PaymentD" class="form-control" type="text" style="display: none">
                            </div>
                        </div>-->
                    <!--</div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-info btn-icon btn-sm icon-left">Save & Print<i class="entypo-check"></i></button>
                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" data-dismiss="modal">Cancel<i class="entypo-cancel"></i></button>
            </div>
        </div><!-- /.modal-content -->
    <!--</div><!-- /.modal-dialog -->
<!--</div>-->


<div id="modal_cancel_reservasi" class="modal fade" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Cancel Reservasi</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">No. BEO</label>
                            <div class="col-md-6">
                                <input name="v_no_beo" id="v_no_beo" class="form-control" value="" type="text" disabled> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker" value="" disabled name="v_date_reservasi" id="v_date_reservasi" >
                            </div>
                        </div>
                           
                        <div class="form-group">
                            <label class="control-label col-md-3">Company</label>
                            <div class="col-md-6">
                                <input type="text" name="v_nama_travel" value="" class="form-control" disabled id="v_nama_travel">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Alasan</label>
                            <div class="col-md-6">
                                <input id="v_alasan" name="v_alasan" class="form-control" type="text"  value="">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_cancel_reservasi()" class="btn btn-info btn-icon btn-sm icon-left">Save<i class="entypo-check"></i></button>
                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" data-dismiss="modal">Cancel<i class="entypo-cancel"></i></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer'); ?>
