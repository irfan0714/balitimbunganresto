<!DOCTYPE html>
<html>
    <head>
        <title>PDF File</title>
    </head>
    <body>
        <div align="center">
        </div>
        <table width="85%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
            
            <tr>
                <td align="center"><b>BANQUET EVENT ORDER</b></td>
            </tr>
            <tr>
                <td >
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                        <tr>
                            <td>GROUP NAME<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="nama_travel" name="nama_travel" class="form-control" value=""/><td>
                            <td><td>
                        </tr>
                        <tr>
                            <td>COMPANY<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="company" name="company" class="form-control" value=""/><td>
                            <td><td>
                        </tr>
                        <tr>
                            <td>TELEPHONE<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="Telepon" name="Telepon" class="form-control" value=""/><td>
                            <td><td>
                        </tr>
                        <tr>
                            <td>ADDRESS<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="alamat" name="alamat" class="form-control" value=""/><td>
                            <td><td>
                        </tr>
                        <tr>
                            <td>NATIONALITY<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="nationality" name="nationality" class="form-control" value=""/><td>
                            <td><td>
                        </tr>
                        <tr>
                            <td>PARTICIPANTS<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="Participants" name="Participants" class="form-control" value=""/><td>
                            <td><td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td>
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                        <tr>
                            <td>EVENT<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="Event" name="Event" class="form-control" value=""/><td>
                        </tr>
                        <tr>
                            <td>CONTACT<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="Contact" name="Contact" class="form-control" value=""/><td>
                        </tr>
                        <tr>
                            <td>PHONE<td>
                            <td>:<td>
                            <td width="200"><input type="text" id="Phone" name="Phone" class="form-control" value=""/><td>
                        </tr>
                        <tr>
                            <td>SALES IN CHARGE <td>
                            <td>:<td>
                            <td width="200"><input type="text" id="Sales_In_Charge" name="Sales_In_Charge" class="form-control" value=""/><td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="place_herborist" name="place_herborist" type="checkbox" value="1">HERBORIST
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="place_the_luwus" name="place_the_luwus" type="checkbox" value="1">THE LUWUS
                        </label>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="place_black_eye_coffee" name="place_black_eye_coffee" type="checkbox" value="1">BLACK EYE COFFEE
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="place_chappel" name="place_chappel" type="checkbox" value="1">CHAPPEL
                        </label>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="place_the_rice_view" name="place_the_rice_view" type="checkbox" value="1">THE RICE VIEW
                        </label>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <table border="1" align="center" cellpadding="1" cellspacing="0">
            <thead>
                <tr>
                    <td><b>S.No.</b></td>
                    <td><b>First Name </b></td>

                </tr>
            </thead>
            <tbody> 
                <?php
                $count = 1;
                foreach ($results as $s) {
                    ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $s['NoDokumen']; ?></td>

                    </tr>
                    <?php
                    $count++;
                }
                ?>
            </tbody>
        </table>

        <table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="table table-bordered responsive">
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_wedding_party" name="func_type_wedding_party" type="checkbox" value="1">WEDDING PARTY
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_coffee_tea_break" name="func_type_coffee_tea_break" type="checkbox" value="1">COFFEE & TEA BREAK
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_cocktail_party" name="func_type_cocktail_party" type="checkbox" value="1">COCKTAIL PARTY
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_breakfast" name="func_type_breakfast" type="checkbox" value="1">BREAKFAST
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_birthday_party" name="func_type_birthday_party" type="checkbox" value="1">BIRTHDAY PARTY
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_training" name="func_type_training" type="checkbox" value="1">TRAINING
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_lunch" name="func_type_lunch" type="checkbox" value="1">LUNCH
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_meeting" name="func_type_meeting" type="checkbox" value="1">MEETING
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_dinner" name="func_type_dinner" type="checkbox" value="1">DINNER
                        </label>
                    </div>
                </td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input id="func_type_other" name="func_type_other" type="checkbox" value="1" >OTHER
                        </label>
                    </div>

                </td>
            </tr>
        </table>

    </body>
</html>