<?php
$this->load->view('header'); ?>
<meta http-equiv="refresh" content="300" />
<script language="javascript" src="<?= base_url(); ?>public/js/global.js">
</script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js">
</script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
  function cek(baseurl,trans){
	
		url = "index.php/pop/pop_new_temporary_item/index/"+trans;
    window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
	}
	function gantiSearch()
	{
		if($("#searchby").val()=="NoDokumen"||$("#searchby").val()=="NoOrder")
		{
			$("#normaltext").css("display","");
			$("#datetext").css("display","none");
			$("#date1").datepicker("destroy");
			$("#date1").val("");
		}
		else
		{
			$("#datetext").css("display","");
			$("#normaltext").css("display","none");
			$("#stSearchingKey").val("");
			$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url();?>/public/images/calendar.png' });
		}
	}
	function deleteTrans(nodok,url)
	{
		var r=confirm("Apakah Anda Ingin Menghapus Transaksi "+nodok+" ?");
		if(r==true)
		{
			$.post(url+"index.php/bss/pos_touch/delete_pos_touch",
				{
					kode:nodok
				},
				function(data)
				{
					window.location = url+"index.php/bss/pos_touch";
				});
		}
	}
	function PopUpPrint(kassa,notrans,baseurl)
	{
		url="index.php/bss/pos_touch/cetak/"+escape(kassa)+"/"+escape(notrans);
		window.open(baseurl+url,'popuppage','scrollbars=yes, width=600,height=500,top=50,left=50');
	}

	function Proses(kdmeja, order, selesai){
		if(order != selesai){
			alert('Ada Pesanan yang belum keluar. \n Periksa kembali!');
			return
		}else{
			window.open('<?=base_url();?>index.php/bss/pos_touch/add_new/'+kdmeja);
		}
	}

	function refresh_halaman(){
	location.reload();
}

</script>

<button class="btn btn-green btn-icon btn-sm icon-left" onClick="refresh_halaman()";>Refresh<i class="entypo-search"></i></button>

<form method="POST"  name="search" action="">
	<table align='center'>
		<tr>
			<td id="normaltext" style="">
				<input type='text' size='20' name='stSearchingKey' id='stSearchingKey'>
			</td>
			<td id="datetext" style="display:none">
				<input type='text' size='10' readonly='readonly' name='date1' id='date1'>
			</td>
			<td>
				<select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()">
					<option value="NoDokumen">
						No Transaksi
					</option>
					<option value="TglDokumen">
						Tgl Transaksi
					</option>
				</select>
			</td>
			<td>
				<input type="submit" value="Search (*)">
			</td>
		</tr>
	</table>
</form>
<br>
<table align = 'center' border='1' class='table_class_list'>
	<tr>
		<?php
		if($link->view == "Y"||$link->edit == "Y"||$link->delete == "Y"){
			?>
			<th>
			</th>
			<?php
		}
		$mylib = new globallib();
		echo $mylib->write_header($header);
		?>
	</tr>
	<?php
	if(count($data) == 0){
		?>
		<td nowrap colspan="<?php echo count($header) + 1;?>" align="center">
			Tidak Ada Data
		</td>
		<?php
	}
	for($a = 0;$a < count($data);$a++){
		?>
		<tr>
		<?php
		if($link->edit == "Y"||$link->delete == "Y"){
			?>
			<td nowrap>
				<?php
				if($link->view == "Y"){
					?>
					<img src='<?=base_url();?>public/images/printer.png' border = '0' title = 'Print' onclick="Proses('<?=$data[$a]['KdMeja'];?>','<?=$data[$a]['Order'];?>','<?=$data[$a]['Selesai'];?>');"/></a>
					<?php
				}
				if($link->edit == "Y"){
					?>
					<!--<a 	href="<?=base_url();?>index.php/bss/pos_touch/edit/<?=$data[$a]['NoTrans'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Konfirm'/></a>
					-->
					<?php
				}
				if($link->delete == "Y"){
					?>
					<!--<img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?=$data[$a]['NoTrans'];?>','<?=base_url();?>');"/>
					-->
					<?php
				}

				?>
			</td>
			<?php
		} ?>
		<td nowrap>
			<?=$data[$a]['Tanggal'];?>
		</td>
		<td nowrap>
			<?=$data[$a]['KdMeja'];?>
		</td>
		<td nowrap>
			<?=$data[$a]['Order'];?>
		</td>
		<td nowrap>
			<?=$data[$a]['Selesai'];?>
		</td>
		<td nowrap onclick="cek('<?=base_url();?>','<?=$data[$a]['NoTrans'];?>')">
			<?=$data[$a]['MenuBaru'];?>
		</td>
		<tr>
		<?php
	}
	?>
</table>
<table align = 'center'  >
	<tr>
		<td>
			<?php echo $this->pagination->create_links(); ?>
		</td>
	</tr>
	<?php
	if($link->add == "Y"){
		?>
		<tr>
		<td nowrap colspan="3">
			<a 	href="<?=base_url();?>index.php/bss/pos_touch/add_new/">
				<img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/>
			</a>
		</td>
		<?php
	} ?>
</table>
<?php
$this->load->view('footer'); ?>
