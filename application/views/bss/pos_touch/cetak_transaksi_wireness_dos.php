<?php
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);
$alamatPT = $store[0]['Alamat1PT'];
$tgl = $header[0]['Tanggal'];
$tgl_1 = explode("-", $tgl);
$tgl_tampil = $tgl_1[2] . "/" . $tgl_1[1] . "/" . $tgl_1[0];
$ftext = '';
//printer_draw_bmp($ftext, "public/images/Logosg.png", 60, 5);  // Logo Dir, lenght H , With V

for($i=1;$i<=3;$i++){
printer_dos($ftext, $reset . $elite);
printer_dos($ftext, $dwidth . str_pad($store[0]['NamaPT'], 39, " ", STR_PAD_BOTH) . $ndwidth . "\r\n");
// printer_dos($ftext, str_pad($store[0]['NamaAlias'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_dos($ftext, str_pad($store[0]['Alamat1PT'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_dos($ftext, str_pad($store[0]['Alamat2PT'], 39, " ", STR_PAD_BOTH) . "\r\n");

printer_dos($ftext, "\r\n");
printer_dos($ftext, str_pad($header[0]['KdMeja']. ' - '. $tgl_tampil . " - " . $header[0]['Waktu'], 39, " ", STR_PAD_BOTH) . "\r\n");
//printer_dos($ftext, "Struk : " . $header[0]['NoKassa'] . "-" . $header[0]['NoStruk'] . " / " . $header[0]['Kasir'] ."-". $header[0]['KdAgent']."\r\n");
printer_dos($ftext, "Struk : " . $header[0]['NoKassa'] . "-" . $header[0]['NoStruk'] . " / " .$header[0]['Kasir']."-". $header[0]['KdAgent']."\r\n");
if($header[0]['userdisc']!='' && $header[0]['KdMember']==''){
printer_dos($ftext, "Otorisasi : " . $header[0]['userdisc'] ."\r\n");	
}
if($header[0]['KdMember']!=''){
printer_dos($ftext, "Member : " . $header[0]['NamaMember'] ."\r\n");	
}
if($header[0]['NamaGroupDisc']!=''){
printer_dos($ftext, "Disc Khusus : " . $header[0]['NamaGroupDisc'].'-'.$header[0]['NoCard'] ."\r\n");	
}
printer_dos($ftext, "========================================\r\n");
$bt = 0;
$totdisc = 0;
for ($a = 0; $a < count($detail); $a++) {
	
	        $q = "
	                SELECT
	                    *
	                FROM
	                masterbarang_touch a
	                WHERE 
	                a.KdSubKategori='0122'
	                AND
	                a.PCode='".$detail[$a]['PCode']."'
	        	";
	        $qry = mysql_query($q);
	        $row = mysql_num_rows($qry);
	        if($row>0){
				$tanda="*";	
			}else{
				$tanda="";
			}

	$totdisc += round($detail[$a]['Disc1']);
    //cek apakah ada berat
    if($detail[$a]['Berat']>0){
        $ada_berat =' @'.$detail[$a]['Berat'];
        $subtotal = $detail[$a]['Qty']*(round($detail[$a]['Harga'])*($detail[$a]['Berat']*1/100));
        $qtynya = round(($detail[$a]['Berat']*1)/100,2);
    }else{
        $ada_berat ='';
        $subtotal = $detail[$a]['Qty']*round($detail[$a]['Harga']);
        $qtynya = $detail[$a]['Qty']; 
    }
    printer_dos($ftext, str_pad(substr($tanda.$detail[$a]['NamaStruk'], 0, 20), 20) .
        str_pad($qtynya, 5, " ", STR_PAD_LEFT) .
        str_pad(round($detail[$a]['Harga']), 7, " ", STR_PAD_LEFT) .
        str_pad(round($subtotal), 8, " ", STR_PAD_LEFT) . "\r\n");
        
    if ($detail[$a]['Disc1']>0) {
    	if($header[0]['nilaidisc']>0){
			printer_dos($ftext, str_pad(substr("Discount", 0, 20), 32) .
            	str_pad("(" . $detail[$a]['Disc1'], 8, " ", STR_PAD_LEFT) . ") \r\n");	
		}else{
			printer_dos($ftext, str_pad(substr("Disc Khusus", 0, 20), 32) .
            	str_pad("(" . $detail[$a]['Disc1'], 8, " ", STR_PAD_LEFT) . ") \r\n");	
		}
        
    }
    if ($detail[$a]['Disc1']<0) {
        printer_dos($ftext, str_pad(substr("Tambahan", 0, 20), 32) .
            str_pad($detail[$a]['Disc1']*-1, 8, " ", STR_PAD_LEFT) . " \r\n");
    }

    $bt += $subtotal;
}
printer_dos($ftext, "----------------------------------------\r\n");
printer_dos($ftext, str_pad("Total " . $header[0]['TotalItem'] . " item", 24) . ":" . str_pad(number_format($bt, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if($totdisc>0){
	if($header[0]['nilaidisc']>0){
		printer_dos($ftext, str_pad("Discount ", 24) . ":" . str_pad(number_format($totdisc, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");	
	}else{
		printer_dos($ftext, str_pad("Disc Khusus ", 24) . ":" . str_pad(number_format($totdisc, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");	
	}
}
if($totdisc<0){
	printer_dos($ftext, str_pad("Tambahan ", 24) . ":" . str_pad(number_format($totdisc*-1, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");	
}

if ($header[0]['Ttl_Charge'] <> 0){
    printer_dos($ftext, str_pad("Service charge ", 24) . ":" . str_pad(number_format($header[0]['Ttl_Charge'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
	
if ($header[0]['TAX'] <> 0){
    printer_dos($ftext, str_pad("Tax ", 24) . ":" . str_pad(number_format($header[0]['TAX'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
printer_dos($ftext, str_pad("Total Sales ", 24) . ":" . str_pad(number_format($header[0]['TotalNilai'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");

/*
if ($header[0]['Discount'] <> 0)
    printer_dos($ftext, str_pad("Discount " . $header[0]['InitDisc'], 24) . ":" . str_pad(number_format($header[0]['DiscRupiah'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
*/

printer_dos($ftext, "----------------------------------------\r\n");
if ($header[0]['Tunai'] <> 0)
    printer_dos($ftext, str_pad("Tunai ", 24) . ":" . str_pad(number_format($header[0]['Tunai'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if ($header[0]['KKredit'] <> 0)
    printer_dos($ftext, str_pad("Kartu Kredit ", 24) . ":" . str_pad(number_format($header[0]['KKredit'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if ($header[0]['KDebit'] <> 0)
    printer_dos($ftext, str_pad("Kartu Debit ", 24) . ":" . str_pad(number_format($header[0]['KDebit'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if ($header[0]['Voucher']+$header[0]['VoucherTravel'] <> 0)
    printer_dos($ftext, str_pad("Voucher ", 24) . ":" . str_pad(number_format($header[0]['Voucher']+$header[0]['VoucherTravel'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if ($header[0]['Point'] <> 0)
    printer_dos($ftext, str_pad("Potong Point ", 24) . ":" . str_pad(number_format($header[0]['Point'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if ($header[0]['GoPay'] <> 0)
    printer_dos($ftext, str_pad("Go Pay ", 24) . ":" . str_pad(number_format($header[0]['GoPay'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
printer_dos($ftext, "----------------------------------------\r\n");
printer_dos($ftext, str_pad("Total Bayar ", 24) . ":" . str_pad(number_format($header[0]['TotalBayar'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
printer_dos($ftext, str_pad("Kembali ", 24) . ":" . str_pad(number_format($header[0]['Kembali'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . $ndwidth . "\r\n");
printer_dos($ftext, "========================================\r\n");
printer_dos($ftext, "             ===Terima kasih===         \r\n");
printer_dos($ftext, "----------------------------------------\r\n");
printer_dos($ftext, str_pad($store[0]['NamaAlias'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_dos($ftext, str_pad("NPWP : " . $store[0]['NPWP'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_dos($ftext, str_pad("www.bebektimbungan.co.id", 39, " ", STR_PAD_BOTH) . "\r\n");
if($header[0]['KdMember']<>''){
	printer_dos($ftext, "----------------------------------------\r\n");
	printer_dos($ftext, str_pad("Point ", 24) . ":" . str_pad(number_format(floor($header[0]['TotalNilai']/$header[0]['NilaiPoint']), 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
	printer_dos($ftext, str_pad("Total Point ", 24) . ":" . str_pad(number_format($header[0]['JumlahPoint'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
// printer_dos($ftext, $dwidth . str_pad($store[0]['NamaPT'], 39, " ", STR_PAD_BOTH) . $ndwidth . "\r\n");
// printer_dos($ftext, str_pad("NPWP : " . $store[0]['NPWP'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, $fcut);
}
printer_dos($ftext, $op_cash);
?>