<?php
$this->load->view('header'); ?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js">
</script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js">
</script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
	function gantiSearch()
	{
		if($("#searchby").val()=="NoDokumen"||$("#searchby").val()=="NoOrder")
		{
			$("#normaltext").css("display","");
			$("#datetext").css("display","none");
			$("#date1").datepicker("destroy");
			$("#date1").val("");
		}
		else
		{
			$("#datetext").css("display","");
			$("#normaltext").css("display","none");
			$("#stSearchingKey").val("");
			$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url();?>/public/images/calendar.png' });
		}
	}
	function deleteTrans(nodok,url)
	{
		var r=confirm("Apakah Anda Ingin Menghapus Transaksi "+nodok+" ?");
		if(r==true)
		{
			$.post(url+"index.php/bss/order_touch/delete_pos_touch",
				{
					kode:nodok
				},
				function(data)
				{
					window.location = url+"index.php/bss/order_touch";
				});
		}
	}
	function PopUpPrint(jenis, kassa,notrans,baseurl)
	{
		if(jenis==1){
			url="index.php/bss/order_touch/cetakmeja/"+escape(notrans);	
		}elseif(jenis==2){
			url="index.php/bss/order_touch/cetakminum/"+escape(notrans);	
		}else{
			url="index.php/bss/order_touch/cetakmakanan/"+escape(notrans);	
		}
		
		
		window.open(baseurl+url,'popuppage','scrollbars=yes, width=600,height=500,top=50,left=50');
	}
</script>
<a href="<?=base_url();?>index.php/transaksi/pending_order/" target="_blank"><button>Pending Order</button></a>
<form method="POST"  name="search" action="">
	<table align='center'>
		<tr>
			<td id="normaltext" style="">
				<input type='text' size='20' name='stSearchingKey' id='stSearchingKey'>
			</td>
			<td id="datetext" style="display:none">
				<input type='text' size='10' readonly='readonly' name='date1' id='date1'>
			</td>
			<td>
				<select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()">
					<option value="NoDokumen">
						No Transaksi
					</option>
					<option value="TglDokumen">
						Tgl Transaksi
					</option>
				</select>
			</td>
			<td>
				<input type="submit" value="Search (*)">
			</td>
		</tr>
	</table>
</form>
<br>
<table align = 'center' border='1' class='table_class_list'>
	<tr>
		<?php
		$mylib = new globallib();
		echo $mylib->write_header($header);
		?>
	</tr>
	<?php
	if(count($data) == 0){
		?>
		<td nowrap colspan="<?php echo count($header) + 1;?>" align="center">
			Tidak Ada Data
		</td>
		<?php
	}
	for($a = 0;$a < count($data);$a++){
		?>
		<tr>
			<td nowrap>
				<?=$data[$a]['NoKassa'];?>
			</td>
			<td nowrap>
				<?=$data[$a]['NoTrans'];?>
			</td>
			<td nowrap>
				<?=$data[$a]['Tanggal'];?>
			</td>
			<td nowrap>
				<?=$data[$a]['Waktu'];?>
			</td>
			<td nowrap>
				<?=$data[$a]['TotalItem'];?>
			</td>
			<td nowrap>
				<?=$data[$a]['KdMeja'];?>
			</td>
			<td nowrap>
				<?=$data[$a]['KdPersonal'];?>
			</td>
			<td nowrap>
				<a href="<?php echo base_url();?>index.php/bss/order_touch/cetakmeja/<?=$data[$a]['NoTrans'];?>">
					<img src='<?=base_url();?>public/images/meja.jpg' height='20' width='20' border = '0' title = 'Meja' />
		  		</a>
		  		<a href="<?php echo base_url();?>index.php/bss/order_touch/cetakminum/<?=$data[$a]['NoTrans'];?>">
					<img src='<?=base_url();?>public/images/drink.jpg' height='20' width='20' border = '0' title = 'Minuman' />
		  		</a>
		  		<a href="<?php echo base_url();?>index.php/bss/order_touch/cetakmakan/<?=$data[$a]['NoTrans'];?>">
					<img src='<?=base_url();?>public/images/food.jpg' height='20' width='20' border = '0' title = 'Makanan' />
		  		</a>
		  		<a href="<?php echo base_url();?>index.php/bss/order_touch/cetakgrill/<?=$data[$a]['NoTrans'];?>">
					<img src='<?=base_url();?>public/images/grill.jpg' height='20' width='20' border = '0' title = 'Grill' />
		  		</a>
		  		<a href="<?php echo base_url();?>index.php/bss/order_touch/cetakall/<?=$data[$a]['NoTrans'];?>">
					<img src='<?=base_url();?>public/images/printer.png' height='20' width='20' border = '0' title = 'Semua' />
		  		</a>
		  		<a href="<?php echo base_url();?>index.php/bss/order_touch/cancelall/<?=$data[$a]['NoTrans'];?>">
					<img src='<?=base_url();?>public/images/cancel.png' height='20' width='20' border = '0' title = 'Cancel All' />
		  		</a>
			</td>
		<tr>
		<?php
	}
	?>
</table>
<table align = 'center'  >
	<tr>
		<td>
			<?php echo $this->pagination->create_links(); ?>
		</td>
	</tr>
	<?php
	//if($link->add == "Y"){
		?>
		<tr>
		<td nowrap colspan="3">
			<a 	href="<?=base_url();?>index.php/bss/order_touch/add_new/">
				<img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/>
			</a>
		</td>
		</tr>
		<?php
	//} ?>
</table>
<?php
$this->load->view('footer'); ?>