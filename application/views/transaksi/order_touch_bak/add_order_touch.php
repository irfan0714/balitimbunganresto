<!DOCTYPE html>
<?php
$session_name=$this->session->userdata('username');
$gantikursor = "onkeydown=\"changeCursor(event,'order_touch',this)\"";?>
<script src="<?php echo base_url();?>public/js/shortcuts_v1.js" type="text/javascript"></script>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<style type='text/css'>
body{
	cursor: pointer;
}
table tr td:active{
	background-color:#369;
	color:#FFF;
}
table tr td{
	border:1px solid #FFF;
}
.boxdisplay{
	color:black;
	font-size:15px;
	text-align:left;
	width:21%;
	height:60%;
	}
.sptombol{
	color:white;
	background-color:#666;
	font-size:30px;
	text-align:center;
	width:7%;
	height:12%;
	}
.angka{
	color:white;
	background-color:#666;
	font-size:30px;
	text-align:center;
	width:7%;
	height:12%;
	}
.angka2{
	color:red;
	background-color:white;
	font-size:30px;
	text-align:center;
	width:7%;
	height:12%;
	}
.subkategori{
	color:white;
	background-color:#333;
	font-size:15px;
	padding:5px;
	text-align:center;
	font-weight:bold;
	width:7%;
	height:12%;
	}
.quitbtn{
	color:white;
	background-color:#f00000;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.cancelbtn{
	color:white;
	background-color:#FF1000;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.jam{
	color:white;
	background-color:#999;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.menutengah{
	color:#369;
	background-color:#EEE;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.menutengahkosong{
	color:#FF0000;
	background-color:#EEE;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.tabledisplay .menukanan{
	color:white;
	background-color:#999;
	font-size:17px;
	text-align:center;
	width:5%;
	height:12%;
	padding-left:1px;
	padding-right:1px;
	padding-top:2px;
	padding-bottom:2px;
	}
.tabledisplay .menukanan1{
	color:White;
	background-color:#ffa500;
	font-size:17px;
	text-align:center;
	width:5%;
	height:12%;
	padding-left:1px;
	padding-right:1px;
	padding-top:2px;
	padding-bottom:2px;
	}
.tabledisplay .menudone{
	color:White;
	background-color:#00b0c5;
	font-size:17px;
	text-align:center;
	width:5%;
	height:12%;
	padding-left:1px;
	padding-right:1px;
	padding-top:2px;
	padding-bottom:2px;
	}
.kategori{
	font-weight:bold;
	color:White;
	background-color:#666;
	font-size:15px;
	text-align:center;
	width:5%;
	height:12%;
	}
.kategori1{
	font-weight:bold;
	color:white;
	background-color:#ffa000;
	font-size:15px;
	text-align:center;
	width:5%;
	height:12%;
	}
.updown{
	padding-top:7px;
	padding-bottom:7px;
	color:white;
	background-color:#666666;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.updown1{
	color:white;
	background-color:#ffa500;
	font-size:15px;
	text-align:center;
	width:7%;
	height:12%;
	}
.column2{
	color:black;
	background-color:#CCC;
	font-weight:bold;
	font-size:30px;
	text-align:center;
	width:14%;
	height:12%;
	}
.column3{
	color:black;
	background-color:#CCC;
	font-size:20px;
	text-align:center;
	width:5%;
	height:12%;
	}
.menu1{
	color:white;
	background-color:#ff0000;
	font-size:20px;
	text-align:center;
	width:12%;
	height:21%;
	}
.tabledisplay{
	height:100%;
	width:100%;
	margin-bottom:0px;
	margin-top:0px;
	background-color:#EEE;
	border:5px solid #00b0c5;
}
.tabledisplay tr td:active{
	background-color:#EEE;
	color:black;
}
.tabledisplay tr td{
	margin:auto;
	padding:1px;
}
.displaytext{
	background-color:#FFF;
	cursor:pointer;
	color:black;
	font-size:15px;
	text-align:left;
	border:1px solid #00b0c5;
	height:22px;
	width:300px;
	text-align:left;
	}
.displaytext:active{
	background:#333;
	color:white;
	}
.displaytextatas{
	color:black;
	font-size:20px;
	width:380px;
	text-align:center;
	border:none;
	margin-left:auto;
	margin-right:auto;
	}
.boxdisplay2{
width:100;height:100;
}
</style>
<script language="javascript">
$(document).keypress(function(e) {
	if(window.event) // IE
	{
		var code = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		var code = e.which;
	}
	//alert(code);
	if (code == 13) 
	{
		$("#enter").click();
	}
	if ((code == 8) && ($("#keterangan").val()=="")&&($("#flagsearch").val()==0)) 
	{
		if($("#statusisi").val()=="menu" && ($("#keterangan").val()=="")&&($("#flagsearch").val()==0))
		{
			return false;
		}
		else
		{
			$("#del").click();
			return false;
		}
	}
	if (code == 45) 
	{
		$("#minus").click();
	}
	if (code == 43) 
	{
		$("#plus").click();
	}
	if (code == 48) 
	{
		$("#nol").click();
	}
	if (code == 49) 
	{
		$("#satu").click();
	}
	if (code == 50) 
	{
		$("#dua").click();
	}
	if (code == 51) 
	{
		$("#tiga").click();
	}
	if (code == 52) 
	{
		$("#empat").click();
	}
	if (code == 53) 
	{
		$("#lima").click();
	}
	if (code == 54) 
	{
		$("#enam").click();
	}
	if (code == 55) 
	{
		$("#tujuh").click();
	}
	if (code == 56) 
	{
		$("#delapan").click();
	}
	if (code == 57) 
	{
		$("#sembilan").click();
	}
	
});

function clearangka()
{
   $("#nol").attr({"class":"angka"})
   $("#satu").attr({"class":"angka"})
   $("#dua").attr({"class":"angka"})
   $("#tiga").attr({"class":"angka"})
   $("#empat").attr({"class":"angka"})
   $("#lima").attr({"class":"angka"})
   $("#enam").attr({"class":"angka"})
   $("#tujuh").attr({"class":"angka"})
   $("#delapan").attr({"class":"angka"})
   $("#sembilan").attr({"class":"angka"})
}

function clearmenukanan()
{
   $("#waitress").attr({"class":"menukanan"})
   $("#table").attr({"class":"menukanan"})
   $("#guest").attr({"class":"menukanan"})
   $("#agent").attr({"class":"agent"})
}

function cekPassword(id)
{
	var lastRow = document.getElementsByName("id[]").length;
	id0 = id.substr(0,3)+md5(id.substr(3,id.length-3));
	for(index=0;index<lastRow;index++){
		nama = document.getElementsByName("id[]");
		temp = nama[index].id;
		indexs = temp.substr(2,temp.length-2);
		//alert($("#id"+indexs).val()+$("#password"+indexs).val())
		if(id0 == $("#id"+indexs).val()+$("#password"+indexs).val())
		{
		    $("#kdpersonal").val($("#id"+indexs).val())
			$("#namapersonal").val($("#username"+indexs).val())
			return true;
		}
	}
	alert("Password salah");
	clearIsian();
	clearMenuTengah();
    return false;
}

function setLokasi()
{
    start = parseFloat($("#menutengah").val());
	lastRow = parseFloat($("#lenlokasi").val());
	if(lastRow-start>14)
	   lastRow1=14+start;
	else
	   lastRow1=lastRow;
	//alert(start);
	//alert(lastRow);
	//alert(lastRow1);
	for(index=1;index<=15;index++){
	    if(start+index-2<lastRow)
		{
		   nama = document.getElementsByName("namalokasi[]");
		   temp = nama[start+index-2].id;
		   indexs = temp.substr(10,temp.length-10);
		//alert($("#id"+indexs).val()+$("#password"+indexs).val())
		   namalokasi = $("#namalokasi"+indexs).val();
		   kdlokasi = $("#kdlokasi"+indexs).val();
		}else
		{
		   namalokasi = "";
		   kdlokasi = "";
		}
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = namalokasi;
		$("#menutengah"+urutan).attr({"value":kdlokasi});
	}
	/*
	for(index=start;index<=lastRow1;index++){
		nama = document.getElementsByName("namalokasi[]");
		temp = nama[index-1].id;
		indexs = temp.substr(10,temp.length-10);
		//alert($("#id"+indexs).val()+$("#password"+indexs).val())
		namalokasi = $("#namalokasi"+indexs).val();
		kdlokasi = $("#kdlokasi"+indexs).val();
		urutan0 = "0"+parseFloat(index-start+1);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = namalokasi;
		$("#menutengah"+urutan).attr({"value":kdlokasi});
	}
	*/
}

function setKeterangan()
{
    start = parseFloat($("#menutengah").val());
	lastRow = parseFloat($("#lenketr").val());
	if(lastRow-start>14)
	   lastRow1=14+start;
	else
	   lastRow1=lastRow;
	//alert(start);
	//alert(lastRow);
	//alert(lastRow1);
	for(index=1;index<=15;index++){
	    if(start+index-2<lastRow)
		{
		   nama = document.getElementsByName("ketrdtl[]");
		   temp = nama[start+index-2].id;
		   indexs = temp.substr(7,temp.length-7);
		//alert($("#id"+indexs).val()+$("#password"+indexs).val())
		   ketrdtl = $("#ketrdtl"+indexs).val();
		}else
		{
		   ketrdtl = "";
		}
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = ketrdtl;
		$("#menutengah"+urutan).attr({"value":ketrdtl});
	}
}

function setupmenu(jenis,kode)
{   
   if(jenis==1)
   {
        index01 = "";
	    index02 = "";
	    index03 = "";
		subindex0101 = "";
	    subindex0102 = "";
	    subindex0103 = "";
		subindex0104 = "";
	    subindex0105 = "";
	    subindex0106 = "";
		subindex0107 = "";
	    subindex0108 = "";
	    subindex0109 = "";
		subindex0110 = "";
	    subindex0111 = "";
	    subindex0112 = "";
		subindex0113 = "";
	    subindex0114 = "";
		subindex0201 = "";
	    subindex0202 = "";
	    subindex0203 = "";
		subindex0204 = "";
	    subindex0205 = "";
	    subindex0206 = "";
		subindex0207 = "";
	    subindex0208 = "";
	    subindex0209 = "";
		subindex0210 = "";
	    subindex0211 = "";
	    subindex0212 = "";
		subindex0213 = "";
	    subindex0214 = "";
		subindex0301 = "";
	    subindex0302 = "";
	    subindex0303 = "";
		subindex0304 = "";
	    subindex0305 = "";
	    subindex0306 = "";
		subindex0307 = "";
	    subindex0308 = "";
	    subindex0309 = "";
		subindex0310 = "";
	    subindex0311 = "";
	    subindex0312 = "";
		subindex0313 = "";
	    subindex0314 = "";
        if(kode=="00")
		{
			indexid = 0;
			lastRow = document.getElementsByName("namalengkap[]").length;
	
			for(index=0;index<lastRow;index++){
			   nama = document.getElementsByName("kdkategori[]");
			   temp = nama[index].id;
			   indexs = temp.substr(10,temp.length-10);
			   kategori = $("#kdkategori"+indexs).val();
			   subkategori = $("#kdsubkategori"+indexs).val();
			   //if(index<=5)
			   //{
			   //alert(kode);alert(kategori);
			   //}
			   if(kategori=='01')
			   {
				  index01 = index01 + indexs + "*";
			   }
			   if(kategori=='02')
			   {
				  index02 = index02 + indexs + "*";
			   }
			   if(kategori=='03')
			   {
				  index03 = index03 + indexs + "*";
			   }
			   if(subkategori=='0101')
			   {
				  subindex0101 = subindex0101 + indexs + "*";
			   }
			   if(subkategori=='0102')
			   {
				  subindex0102 = subindex0102 + indexs + "*";
			   }
			   if(subkategori=='0103')
			   {
				  subindex0103 = subindex0103 + indexs + "*";
			   }
			   if(subkategori=='0104')
			   {
				  subindex0104 = subindex0104 + indexs + "*";
			   }
			   if(subkategori=='0105')
			   {
				  subindex0105 = subindex0105 + indexs + "*";
			   }
			   if(subkategori=='0106')
			   {
				  subindex0106 = subindex0106 + indexs + "*";
			   }
			   if(subkategori=='0107')
			   {
				  subindex0107 = subindex0107 + indexs + "*";
			   }
			   if(subkategori=='0108')
			   {
				  subindex0108 = subindex0108 + indexs + "*";
			   }
			   if(subkategori=='0109')
			   {
				  subindex0109 = subindex0109 + indexs + "*";
			   }
			   if(subkategori=='0110')
			   {
				  subindex0110 = subindex0110 + indexs + "*";
			   }
			   if(subkategori=='0111')
			   {
				  subindex0111 = subindex0111 + indexs + "*";
			   }
			   if(subkategori=='0112')
			   {
				  subindex0112 = subindex0112 + indexs + "*";
			   }
			   if(subkategori=='0113')
			   {
				  subindex0113 = subindex0113 + indexs + "*";
			   }
			   if(subkategori=='0114')
			   {
				  subindex0114 = subindex0114 + indexs + "*";
			   }
			   if(subkategori=='0201')
			   {
				  subindex0201 = subindex0201 + indexs + "*";
			   }
			   if(subkategori=='0202')
			   {
				  subindex0202 = subindex0202 + indexs + "*";
			   }
			   if(subkategori=='0203')
			   {
				  subindex0203 = subindex0203 + indexs + "*";
			   }
			   if(subkategori=='0204')
			   {
				  subindex0204 = subindex0204 + indexs + "*";
			   }
			   if(subkategori=='0205')
			   {
				  subindex0205 = subindex0205 + indexs + "*";
			   }
			   if(subkategori=='0206')
			   {
				  subindex0206 = subindex0206 + indexs + "*";
			   }
			   if(subkategori=='0207')
			   {
				  subindex0207 = subindex0207 + indexs + "*";
			   }
			   if(subkategori=='0208')
			   {
				  subindex0208 = subindex0208 + indexs + "*";
			   }
			   if(subkategori=='0209')
			   {
				  subindex0209 = subindex0209 + indexs + "*";
			   }
			   if(subkategori=='0210')
			   {
				  subindex0210 = subindex0210 + indexs + "*";
			   }
			   if(subkategori=='0211')
			   {
				  subindex0211 = subindex0211 + indexs + "*";
			   }
			   if(subkategori=='0212')
			   {
				  subindex0212 = subindex0212 + indexs + "*";
			   }
			   if(subkategori=='0213')
			   {
				  subindex0213 = subindex0213 + indexs + "*";
			   }
			   if(subkategori=='0214')
			   {
				  subindex0214 = subindex0214 + indexs + "*";
			   }
			   if(subkategori=='0301')
			   {
				  subindex0301 = subindex0301 + indexs + "*";
			   }
			   if(subkategori=='0302')
			   {
				  subindex0302 = subindex0302 + indexs + "*";
			   }
			   if(subkategori=='0303')
			   {
				  subindex0303 = subindex0303 + indexs + "*";
			   }
			   if(subkategori=='0304')
			   {
				  subindex0304 = subindex0304 + indexs + "*";
			   }
			   if(subkategori=='0305')
			   {
				  subindex0305 = subindex0305 + indexs + "*";
			   }
			   if(subkategori=='0306')
			   {
				  subindex0306 = subindex0306 + indexs + "*";
			   }
			   if(subkategori=='0307')
			   {
				  subindex0307 = subindex0307 + indexs + "*";
			   }
			   if(subkategori=='0308')
			   {
				  subindex0308 = subindex0308 + indexs + "*";
			   }
			   if(subkategori=='0309')
			   {
				  subindex0309 = subindex0309 + indexs + "*";
			   }
			   if(subkategori=='0310')
			   {
				  subindex0310 = subindex0310 + indexs + "*";
			   }
			   if(subkategori=='0311')
			   {
				  subindex0311 = subindex0311 + indexs + "*";
			   }
			   if(subkategori=='0312')
			   {
				  subindex0312 = subindex0312 + indexs + "*";
			   }
			   if(subkategori=='0313')
			   {
				  subindex0313 = subindex0313 + indexs + "*";
			   }
			   if(subkategori=='0314')
			   {
				  subindex0314 = subindex0314 + indexs + "*";
			   }
			}
			$("#index01").val(index01);
			$("#index02").val(index02);
			$("#index03").val(index03);
			$("#indexa").val(index01);
			$("#subindex0101").val(subindex0101);
			$("#subindex0102").val(subindex0102);
			$("#subindex0103").val(subindex0103);
			$("#subindex0104").val(subindex0104);
			$("#subindex0105").val(subindex0105);
			$("#subindex0106").val(subindex0106);
			$("#subindex0107").val(subindex0107);
			$("#subindex0108").val(subindex0108);
			$("#subindex0109").val(subindex0109);
			$("#subindex0110").val(subindex0110);
			$("#subindex0111").val(subindex0111);
			$("#subindex0112").val(subindex0112);
			$("#subindex0113").val(subindex0113);
			$("#subindex0114").val(subindex0114);
			$("#subindex0201").val(subindex0201);
			$("#subindex0202").val(subindex0202);
			$("#subindex0203").val(subindex0203);
			$("#subindex0204").val(subindex0204);
			$("#subindex0205").val(subindex0205);
			$("#subindex0206").val(subindex0206);
			$("#subindex0207").val(subindex0207);
			$("#subindex0208").val(subindex0208);
			$("#subindex0209").val(subindex0209);
			$("#subindex0210").val(subindex0210);
			$("#subindex0211").val(subindex0211);
			$("#subindex0212").val(subindex0212);
			$("#subindex0213").val(subindex0213);
			$("#subindex0214").val(subindex0214);
			$("#subindex0301").val(subindex0301);
			$("#subindex0303").val(subindex0303);
			$("#subindex0303").val(subindex0303);
			$("#subindex0304").val(subindex0304);
			$("#subindex0305").val(subindex0305);
			$("#subindex0306").val(subindex0306);
			$("#subindex0307").val(subindex0307);
			$("#subindex0308").val(subindex0308);
			$("#subindex0309").val(subindex0309);
			$("#subindex0310").val(subindex0310);
			$("#subindex0311").val(subindex0311);
			$("#subindex0312").val(subindex0312);
			$("#subindex0313").val(subindex0313);
			$("#subindex0314").val(subindex0314);
			//alert(index01);
			//clearSubKategori();
		    //setSubKategori('01');
		}
		else
		{
		    clearSubKategori();
		    setSubKategori(kode);
		    $("#indexa").val($("#index"+kode).val());
		}
   }
   if(jenis==2)
   {
       $("#indexa").val($("#subindex"+kode).val());
   }
}

function setupfilter(kode)
{   
	indexid = 0;
	lastRow = document.getElementsByName("namalengkap[]").length;
    index01 = "";
	for(index=0;index<lastRow;index++){
	   nama = document.getElementsByName("kdkategori[]");
	   temp = nama[index].id;
	   indexs = temp.substr(10,temp.length-10);
	   namabarang = $("#namalengkap"+indexs).val();
	   posisi = namabarang.search(kode);
	   if(posisi>=0)
	   {
		  index01 = index01 + indexs + "*";
	   }
	}
	$("#indexa").val(index01);
}

function setMenu()
{
    start = parseFloat($("#menutengah").val());
	//lastRow = parseFloat($("#lenbarang").val());
	lastRow = document.getElementsByName("namalengkap[]").length;
	if(lastRow-start>14)
	{
	   lastRow1=14+start;
	}
	else
	{
		lastRow1=lastRow;
	}
	//alert(start);
	//alert(lastRow);
	//alert(lastRow1);
	indexa = $("#indexa").val();
	//alert(indexa);
    indexa = indexa.split('*');
	lastRow = indexa.length-1;
	for(index=1;index<=15;index++){
		//alert(start+index-2);
		//alert(lastRow);
		
	    if(start+index-2<lastRow)
		{
		   nama = document.getElementsByName("namalengkap[]");
		   temp = nama[start+index-2].id;
		   indexs = temp.substr(8,temp.length-8);
		   indexs = indexa[start+index-2];
		   namalengkap = $("#namalengkap"+indexs).val();
		   pcode = $("#pcodebrg"+indexs).val();
		   flagready = $("#flagready"+indexs).val();
		   satuan = $("#satuanbrg"+indexs).val();
		   harga = $("#hargabrg"+indexs).val();
		   komisi = $("#komisibrg"+indexs).val();
		   printer = $("#printerbrg"+indexs).val();
		   //alert(harga);
		   //alert(komisi);
		}else
		{
		   namalengkap = "";
		   pcode = "";
		   flagready= "T";
		}
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    if(flagready=="T")
		{
			$("#menutengah"+urutan).attr({"class":"menutengahkosong"});
			text.innerHTML = namalengkap;
		}
		if(flagready=="Y")
		{
			$("#menutengah"+urutan).attr({"class":"menutengah"});
			text.innerHTML = namalengkap;
		}
		$("#menutengah"+urutan).attr({"value":pcode});
		$("#satuan"+urutan).attr({"value":satuan});
		$("#harga"+urutan).attr({"value":harga});
		$("#printer"+urutan).attr({"value":printer});
		$("#komisi"+urutan).attr({"value":komisi});
	}
}

function setSubKategori(kode)
{
    //start = parseFloat($("#menutengah").val());
	//lastRow = parseFloat($("#lenbarang").val());
	lastRow = document.getElementsByName("namasubkategori[]").length;
	kolom = 1;
	for(index=0;index<lastRow;index++){
    	   nama = document.getElementsByName("namasubkategori[]");
		   temp = nama[index].id;
		   indexs = temp.substr(15,temp.length-15);
		   kdsubkategori = $("#_kdsubkategori"+indexs).val();
		   kdkategori = $("#_kdkategori"+indexs).val();
		   namasubkategori = $("#namasubkategori"+indexs).val();
		   //alert(indexs);
		   //alert(namalengkap);
           
		   //alert(kode+"-"+kdkategori+"-"+kdsubkategori+namasubkategori);
		   if(kdkategori==kode)
		   {
			   urutan0 = "0"+parseFloat(kolom);
			   urutan=urutan0.substr(-2);
				//alert(urutan);
			   var text = document.getElementById("subkategori"+urutan);
			   text.innerHTML = namasubkategori;
			   $("#subkategori"+urutan).attr({"value":kdsubkategori});
			   kolom = kolom + 1;
		   }
	}
}

function clearMenuTengah()
{
	for(index=1;index<=15;index++){
	    namamenu = "";
	    kodemenu = "";
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = namamenu;
		$("#menutengah"+urutan).attr({"value":kodemenu});
	}
	$("#menutengah").val(1);
}

function clearSubKategori()
{
	for(index=1;index<=14;index++){
	    namamenu = "";
	    kodemenu = "";
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("subkategori"+urutan);
	    text.innerHTML = namamenu;
		$("#subkategori"+urutan).attr({"value":kodemenu});
	}
}

function clearIsian()
{
    $("#hdisplay0").val("");
	$("#hdisplay1").val("");
	$("#dtext01").val("");
	$("#dtext02").val("");
	$("#dtext02").attr('disabled',true);
	$("#dtext02").attr({"placeholder":""});
}

function kosongarray()
{
	var lastRow = document.getElementsByName("pcode[]").length;
	//alert(lastRow);
	for(index=1;index<lastRow;index++){
	    //alert(index);
		//displaytext();
		nama = document.getElementsByName("pcode[]");
		temp = nama[index].id;
		//alert(index+";"+temp);
		indexs = temp.substr(5,temp.length-5);
		$('#baris'+indexs).remove();
	}
	$("#nourut0").val(1);
	$("#pcode0").val("");
	$("#tmppcode0").val("");
	$("#nama0").val("");
	$("#tmpqty0").val(0);
	$("#qty0").val(0);
	$("#harga0").val(0);
	$("#printer0").val("");
	$("#komisi0").val("");
	$("#satuan0").val("");
	$("#keterangan0").val("");
	$(".displaytext").val("");
}

function addIt2(cKey)
{
    id = cKey.getAttribute("id");
    totalitem = document.getElementsByName("pcode[]").length;
	transaksi = $('#transaksi').val();
	if(transaksi=="yes")
	{
	    if(id=="commit")
		{
			if(confirm("Proses order ini?"))
			{
			   $("#order_touch").submit();
			}
		}
		if(id=="quit")
		{
			if(confirm("Keluar dari transaksi order?"))
			{
			   kosongarray();
			   displaytext();
			   $('#transaksi').val("no");
			   $("#order_touch").submit();
			}
		}
		if(id=="cancel")
		{
			if(confirm("Hapus transaksi order ini?"))
			{
				$('#transaksi').val("no");
				kosongarray();
				clearIsian();
				clearMenuTengah();
				kosongarray();
				$("#statusisi").val("waitress");
				var wtr = document.getElementById('waitress');
				var tbl = document.getElementById('table');
				var gst = document.getElementById('guest');
				var kms = document.getElementById('agent');
	 			wtr.innerHTML = "";
	 			tbl.innerHTML = "Table"+" </br> -";
	 			gst.innerHTML = "Guest"+" </br> -";
				gst.innerHTML = "";
				$("#waitress").attr({"class":"menukanan1"});
				$("#table").attr({"class":"menukanan"});
				$("#guest").attr({"class":"menukanan"});
				$("#agent").attr({"class":"menukanan"});
			}
		}
    }
	else
	{
		if(id=="quit")
		{
			if(confirm("Keluar dari transaksi order?"))
			{
			   $("#order_touch").submit();
			}
		}
	}
}

function addIt(cKey)
{
    //alert(cKey.getAttribute("id"));
	id = cKey.getAttribute("id");
	val = cKey.getAttribute("value");
	nclass = cKey.getAttribute("class");
	
	prevtext = $("#prevtext").val();
	prevclass = $("#prevclass").val();
	prevmenu = $("#prevmenu").val();
	statusketr = $("#statusketr").val();
	//if((id!="plus")&&(id!="minus")&&(id!="enter")&&(id!="kategori01")&&(id!="kategori02")&&(id!="kategori03"))
	if(nclass=="menutengah")
	{
	   text = document.getElementById(id).innerHTML;
	   if(text!="")
	   {
	   if(statusketr==1)
	   {
			$("#dtext02").val(text);
			$("#dtext02").focus();
			$("#keterangan").val($("#dtext02").val());
	   }
	   else
	   {
			if($("#pcode").val() == val)
			{
				$("#qty").val((parseFloat($("#qty").val()))+1);
			}else
			{
				$("#qty").val(1);
			}
		   $("#pcode").val(val);
		   $("#nmbrg").val(text);
		   satuanid = id.substr(-2);
		   satuan = $("#satuan"+satuanid).val();
		   $("#satuan").val(satuan);
		   harga = $("#harga"+satuanid).val();
		   $("#harga").val(harga);
		   komisi = $("#komisi"+satuanid).val();
		   $("#komisi").val(komisi);
		   printer = $("#printer"+satuanid).val();
		   $("#printer").val(printer);
	   }
	   }else{
			$("#qty").val("");
	   }
	}
	else
	   text = prevtext;
	//alert('**'+text+'**');
				  
	//alert(nclass);
	waitress = $("#waitress").val();
	statusisi = $("#statusisi").val();
	flagangka = $("#flagangka").val();
	hdisplay0 = $("#hdisplay0").val();
	hdisplay1 = $("#hdisplay1").val();
	hdisplay0len = hdisplay0.length;
	$("#ceknilai").val(prevclass+"*"+text+"*"+id+"*"+nclass);
	
	if(flagangka==1)
	{
	   clearangka();
	   $("#flagangka").val(0);
	}
	if(id=="clear")
	{
	   clearIsian();
	   return
	}	
	flagsearch = $("#flagsearch").val();
	if(((id=="search")&&(statusisi=="menu"))||((id=="enter")&&(flagsearch==1)))
	{
	   if(flagsearch==0)
	   {
		   $("#menutengah").val(1);	
		   $("#statusketr").val(0);
		   $("#dtext02").attr('disabled',false);
		   $("#dtext02").attr({"placeholder":"Search Item"});
		   $("#dtext02").focus();
		   $("#flagsearch").val(1);
	   }else if(flagsearch==1)
	   {
	      $("#flagsearch").val(0);
		  searchtext = $("#dtext02").val().toUpperCase();
		  $("#dtext02").attr('disabled',true);
		  $("#dtext02").attr({"placeholder":""});
		  $("#statusketr").val(0);
		  $("#keterangan").val($("#dtext02").val());
		  setupfilter(searchtext);
	      setMenu();
	   }
	}
	if(id=="del")
	{
		$("#hdisplay0").val(hdisplay0.substr(0,hdisplay0len-1));
		$("#hdisplay1").val(hdisplay1.substr(0,hdisplay0len-1));
		$("#dtext01").val(hdisplay1.substr(0,hdisplay0len-1));
		return false;
	}
	if((id=="waitress")&&((val=="")||(val==0)))
	{
	   $("#statusisi").val("waitress");
	   statusisi = "waitress";
	}
	if(nclass=='subkategori')
	{
		$("#menutengah").val(1);
		$("#posisisubkategori").val(val);
		setupmenu(2,val);
	    setMenu();
	}
	if((id!=prevmenu)&&(id=="kategori01"||id=="kategori02"||id=="kategori03"))//&&(prevmenu=="kategori01"||prevmenu=="kategori02"||prevmenu=="kategori03"))
	{
	    $("#prevtext").val("");
	    $("#menutengah").val(1);
	    $("#qty").val(1);
		$("#harga").val(0);
		$("#komisi").val("");
		$("#printer").val("");
	    $("#dtext00").val("");
		$("#dtext01").val("");
		$("#hdisplay0").val("");
		$("#hdisplay1").val("");
		$("#statusketr").val(0);
		$("#posisisubkategori").val("");
	}
	if(id=="kategori01")
	{
	   if(statusisi=="menu")
	   {
	      $("#kategori01").attr({"class":"updown1"});
		  $("#kategori02").attr({"class":"kategori"});
		  $("#kategori03").attr({"class":"kategori"});
	      setupmenu(1,'01');
		  setMenu();
		  $("#posisi").val("01");
	   }
	}
	if(id=="kategori02")
	{
	   if(statusisi=="menu")
	   {
	      $("#kategori01").attr({"class":"kategori"});
		  $("#kategori02").attr({"class":"updown1"});
		  $("#kategori03").attr({"class":"kategori"});
	      setupmenu(1,'02');
	      setMenu(); 
		  $("#posisi").val("02");
	   }
	}
	if(id=="kategori03")
	{
	   if(statusisi=="menu")
	   {
	      $("#kategori01").attr({"class":"kategori"});
		  $("#kategori02").attr({"class":"kategori"});
		  $("#kategori03").attr({"class":"updown1"});
	      setupmenu(1,'03');
	      setMenu();
		  $("#posisi").val("03");
	   }
	}
	if(statusketr==1)
	{
		start = parseFloat($("#menutengah").val());
		lastRow = parseFloat($("#lenlokasi").val());
		if(id=="down2")
		{
		   if(start+14<lastRow)
			 $("#menutengah").val(start+3);
		}
		if(id=="up2")
		{
		   if(start-3>0)
			 $("#menutengah").val(start-3);
		}
		if(id=="pagedown2")
		{
		   if(start+29<lastRow)
			 $("#menutengah").val(start+15);
		   else
			 $("#menutengah").val(Math.floor(lastRow/15)*15+1);
		}
		if(id=="pageup2")
		{
		   if(start-15>0)
			 $("#menutengah").val(start-15);
		   else
			 $("#menutengah").val(1);
		}
	}
	switch(statusisi)
	{
	    case "waitress":
		    //alert("masuk");
				if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
				{
					if((hdisplay0len>0)&&((id=="table")||(id=="enter")))
					{  
						if(cekPassword(hdisplay0))
						{
							namapersonal = $("#namapersonal").val();
							kdpersonal = $("#kdpersonal").val();
							var text = document.getElementById('waitress');
					  
							text.innerHTML = namapersonal;
							$("#waitress").attr({"class":"menudone"});
							$("#table").attr({"class":"menukanan1"});
							$("#idpersonal").val(kdpersonal);
							$("#statusisi").val("table");
							setupmenu(1,"00");
							clearIsian();
							clearMenuTengah();
							setLokasi();
						}
					}
					else
					{
						alert("Masukkan kode anda dahulu");
						clearIsian();
						clearMenuTengah();
						kosongarray();
						$("#waitress").attr({"class":"menukanan1"});
						$("#table").attr({"class":"menukanan"});
						$("#guest").attr({"class":"menukanan"});
						$("#agent").attr({"class":"menukanan"});
					}
				}
				else
				{
					dtext00 = hdisplay0+val;
					dtext01 = hdisplay1+"*";
					$("#hdisplay0").val(dtext00);
					$("#hdisplay1").val(dtext01);
					$("#dtext01").val(dtext01);
				}
			break;
	    case "table":
		
			if((nclass!="menutengah"&&nclass!="updown")||id=="enter")
			{
				if((hdisplay0len>0)&&((id=="agent")||(id=="enter")))
			    {  
				  kodetable = $("#dtext01").val();
				  //alert(kodetable);
				  var text = document.getElementById('table');
				  text.innerHTML = "Table "+"</br>"+kodetable;
				  $("#table").attr({"class":"menudone"});
				  $("#agent").attr({"class":"menukanan1"});
				  $("#idtable").val(kodetable);
				  $("#statusisi").val("agent");
				  clearIsian();
				  clearMenuTengah();
			    }
				else
				 {
					alert("Masukkan kode table dahulu");
				 }
			}else
			{
			    $("#dtext01").val(val);
				$("#hdisplay0").val(val);
			    $("#hdisplay1").val(val);
				start = parseFloat($("#menutengah").val());
				lastRow = parseFloat($("#lenlokasi").val());
				if(id=="down2")
				{
				   if(start+14<lastRow)
					 $("#menutengah").val(start+3);
				}
				if(id=="up2")
				{
				   if(start-3>0)
					 $("#menutengah").val(start-3);
				}
				if(id=="pagedown2")
				{
				   if(start+29<lastRow)
					 $("#menutengah").val(start+15);
				   else
				     $("#menutengah").val(Math.floor(lastRow/15)*15+1);
				}
				if(id=="pageup2")
				{
				   if(start-15>0)
					 $("#menutengah").val(start-15);
				   else
				     $("#menutengah").val(1);
				}
				setLokasi();
			}
			break;
		case "agent":
		    //alert("masuk");
	        if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
			{
			   if((hdisplay0len>0)&&((id=="guest")||(id=="enter")))
			   {  
			   
					kodeagent = $("#dtext01").val();
					if((kodeagent > 9999)||(kodeagent < 1000))
					{
						alert("Kode Agen Salah!");
						clearIsian();
					}else{
						var text = document.getElementById('agent');
						text.innerHTML = kodeagent;
						$("#agent").attr({"class":"menudone"});
						$("#idagent").val(kodeagent);
						$("#statusisi").val("guest");
						clearIsian();
						clearMenuTengah();
					}
			   }
			   else
				   alert("Masukkan kode agen")
			}else
			{
			   dtext00 = hdisplay0+val;
			   dtext01 = hdisplay1+val;
			   $("#hdisplay0").val(dtext00);
			   $("#hdisplay1").val(dtext01);
			   $("#dtext01").val(dtext01);
			}
			break;
		case "guest":
		    //alert("masuk");
	        if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
			{
			   if((hdisplay0len>0)&&((id=="table")||(id=="enter")))
			   {  
			   
					kodeguest = $("#dtext01").val();
					if(kodeguest > 999)
					{
						alert("Maksimal Guest 999 !");
						clearIsian();
					}else{
						var text = document.getElementById('guest');
						text.innerHTML = "Guest "+"</br>"+kodeguest;
						$("#guest").attr({"class":"menudone"});
						posisi = $("#posisi").val();
						if(posisi=='01')
						$("#kategori01").attr({"class":"updown1"});
						if(posisi=='02')
						$("#kategori02").attr({"class":"updown1"});
						if(posisi=='03')
						$("#kategori03").attr({"class":"updown1"});
						$("#idguest").val(kodeguest);
						$("#statusisi").val("menu");
						clearIsian();
						clearMenuTengah();
						setupmenu(1,posisi);
						setMenu();
					}
			   }
			   else
				   alert("Masukkan jumlah tamu")
			}else
			{
			   dtext00 = hdisplay0+val;
			   dtext01 = hdisplay1+val;
			   $("#hdisplay0").val(dtext00);
			   $("#hdisplay1").val(dtext01);
			   $("#dtext01").val(dtext01);
			}
			break;
		case "menu":
			if((nclass!="menutengah"&&nclass!="updown"&&nclass!="kategori"&&id!="plus"&&id!="minus")||id=="void"||id=="enter")
			{
				if((hdisplay0len>0)&&(id=="enter"||id=="void"))
			    {  
				  //totalitem = parseFloat($("#totalitem").val())+1;
				  //$("#totalitem").val(totalitem);
				  saveData(id);
				  
				  totalitem = document.getElementsByName("pcode[]").length;
				  if(totalitem>15)
				     $("#posisitampil").val(totalitem-14);
				  else
				     $("#posisitampil").val(1);
				  displaytext();
				  clearIsian();
				  /*
				  posisi = $("#posisi").val();
				  if(posisi=="01")
				  {
				  $("#kategori01").attr({"class":"updown1"});
				  $("#kategori02").attr({"class":"kategori"});
				  $("#kategori03").attr({"class":"kategori"});
				  //setupmenu(1,'01');
				  //setMenu();
				  }
				  if(posisi=="02")
				  {
				  $("#kategori01").attr({"class":"kategori"});
				  $("#kategori02").attr({"class":"updown1"});
				  $("#kategori03").attr({"class":"kategori"});
				  //setupmenu(1,'02');
				  //setMenu();
				  }
				  if(posisi=="03")
				  {
				  $("#kategori01").attr({"class":"kategori"});
				  $("#kategori02").attr({"class":"kategori"});
				  $("#kategori03").attr({"class":"updown1"});
				  //setupmenu(1,'03');
				  //setMenu();
				  }
				  
					kode = $("#posisisubkategori").val();
					setupmenu(2,kode);
					setMenu();
					//clearSubKategori();
					//setSubKategori(kode);
					$("#indexa").val($("#index"+kode).val());
				  */
				  $("#qty").val(1);
				  $("#prevtext").val("");
				  $("#statusketr").val(0);
				  statusketr=0;
			    }
				if((hdisplay0len>0)&&id=="note")
				{
					$("#flagsearch").val(0);
					$("#dtext02").val("");
					$("#menutengah").val(1);
				    setKeterangan();
					
					$("#statusketr").val(1);
					$("#dtext02").attr('disabled',false);
					$("#dtext02").attr({"placeholder":"Input Keterangan Menu"});
					$("#dtext02").focus();
				}
				
				if(statusketr==1)
				    setKeterangan();
			    //else
				//  alert("Silahkan pilih menu")
			}else
			{
			    //alert(id);
				//alert(text);
				if(statusketr==0)
				{
					start = parseFloat($("#menutengah").val());
					lastRow = parseFloat($("#lenbarang").val());
					if(id=="down2")
					{
					   if(start+14<lastRow)
						 $("#menutengah").val(start+3);
					}
					if(id=="up2")
					{
					   if(start-3>0)
						 $("#menutengah").val(start-3);
					}
					if(id=="pagedown2")
					{
					   if(start+29<lastRow)
						 $("#menutengah").val(start+15);
					   else
						 $("#menutengah").val(Math.floor(lastRow/15)*15+1);
					}
					if(id=="pageup2")
					{
					   if(start-15>0)
						 $("#menutengah").val(start-15);
					   else
						 $("#menutengah").val(1);
					}
					
					if(id=="down1"||id=="up1"||id=="pagedown1"||id=="pageup1")
					{
						start = parseFloat($("#posisitampil").val());
						totalitem = document.getElementsByName("pcode[]").length;
						if(id=="down1")
						{
						   if(start+14<totalitem)
							 $("#posisitampil").val(start+1);
						}
						if(id=="up1")
						{
						   if(start-1>0)
							 $("#posisitampil").val(start-1);
						}
						if(id=="pagedown1")
						{
						   if(start+15<totalitem)
							 $("#posisitampil").val(start+5);
						   else
							 $("#posisitampil").val(Math.floor(totalitem/15)*15+1);
						}
						if(id=="pageup1")
						{
						   if(start-5>0)
							 $("#posisitampil").val(start-5);
						   else
							 $("#posisitampil").val(1);
						}
						displaytext();
					}
					
					if((id=="plus")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!=''))))
					{
					   qty = parseFloat($("#qty").val())+1;
					   $("#qty").val(qty);
					}
					if((id=="minus")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!=''))))
					{
					   qty = parseFloat($("#qty").val())-1;
					   if(qty>0)
						  $("#qty").val(qty);
					}
					setMenu();
					if((nclass=="menutengah"||id=="plus"||id=="minus")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!=''))))
					{
						//alert('**'+text+'**');
						qty = $("#qty").val();
						
						text1 = qty+" "+text;
						
						$("#dtext00").val(val);
						$("#dtext01").val(text1);
						$("#hdisplay0").val(val);
						$("#hdisplay1").val(text1);
					}
				}
				
			}
			break;
	}
	
	$("#prevclass").val(nclass);
	$("#prevmenu").val(id);
	if((id!="plus")&&(id!="minus")&&(id!="enter")&&(id!="kategori01")&&(id!="kategori02")&&(id!="kategori03"))
	   $("#prevtext").val(text);
	/*
	if(id=="boxpicture")
	{
		$("#boxtext").css("display","");
		$("#boxpicture").css("display","none")
	}else if(id=="boxtext")
	{
		$("#boxtext").css("display","none")
		$("#boxpicture").css("display","")
	}*/
	if(cKey.getAttribute("class")=="angka")
	{
		$("#flagangka").val(1)
		cKey.setAttribute("class", "angka2")
	}
/*
	if(nclass=="menukanan"||nclass=="menukanan1")
		clearmenukanan()
	if(cKey.getAttribute("class")=="menukanan")
	cKey.setAttribute("class", "menukanan1")
*/
//var text = document.getElementById('dtext01');
	//text.innerHTML = dtext01;

//cKey.innerHTML= "class";
}

function displaytext()
{
    totalitem = document.getElementsByName("pcode[]").length;
	posisitampil = parseFloat($("#posisitampil").val());
	for(index=0;index<15;index++){
	    mulai = index+posisitampil-1;
		index0 = "0"+parseFloat(index+1);
		index1 = index0.substr(-2);
	    if(mulai<totalitem)
		{
		   //alert(mulai);
		   nourut = $("#nourut" + mulai).val();
		   qty = $("#qty" + mulai).val();
		   harga = $("#harga" + mulai).val();
		   nama = $("#nama" + mulai).val();
		   ketr = $("#keterangan" + mulai).val();
		   if(nourut<10)
		   {
			nourut = "0"+nourut;
		   }
		   if(ketr!="")
		   {
		   hdisplay = nourut+"| "+qty+" |"+nama+"|"+ketr+"| "+harga;
	       }
		   else
		   {
			hdisplay = nourut+"| "+qty+" |"+nama+"| "+harga;
		   }
		   $("#displaytext"+index1).val(hdisplay);
		}
		else
		   $("#displaytext"+index1).val("");
	}
}

function saveData(id)
{
		//saveThis();
		
		pcode = $("#pcode").val();
		qty = parseFloat($("#qty").val());
		if(id=="void")qty = qty * -1;
		satuan = $("#satuan").val();
		harga = $("#harga").val();
		komisi = $("#komisi").val();
		printer = $("#printer").val();
		keterangan = $("#keterangan").val();
		editflag =$("#EditFlg").val();
		//alert(editflag);
		//editflag = 0;
		if(editflag==0){
			
			lastRow = document.getElementsByName("pcode[]").length;
			//alert(lastRow);
			datadouble = false;
			for(index=0;index<lastRow;index++)
			{
				indexs = index; 
				if($("#pcode"+indexs).val()==pcode&&$("#satuan"+indexs).val()==satuan)
				{
				   if(qty-Math.round(qty)==0&&$("#pcode"+indexs).val()-Math.round($("#pcode"+indexs).val())==0)
				   {
    				   datadouble = true;
	    			   break;
				   }
				}
				
			}
			
			//alert(indexs);
			if(datadouble)
			{
				qtyasli = parseFloat($("#tmpqty" + indexs).val());
				if($("#statusubah").val()==1 && id!="void")
				{
					qtybaru = qty;
				}
				else
				{
					qtybaru = qtyasli+qty;
				}
				//if(qtybaru<0)qtybaru=0;
				if(qtybaru<1)
				{
					if(indexs==0 && lastRow==1)
					{
						$("#nourut"+indexs).val("");
						$("#pcode"+indexs).val("");
						$("#nama"+indexs).val("");
						$("#qty" + indexs).val("");
						$("#harga" + indexs).val("");
						$("#satuan"+indexs).val("");
						$("#keterangan"+indexs).val("");
						$("#counter"+indexs).val("");
						$("#counterambil"+indexs).val("");
						$("#tmppcode"+indexs).val("");
						$("#tmpqty" + indexs).val("");
						$("#komisi" + indexs).val("");
						$("#printer" + indexs).val("");
					}
					if(indexs > 0 && indexs==(lastRow-1))
					{
						$("#baris"+indexs).remove();
					}
					if(((indexs > 0)||(indexs==0)) && (indexs < (lastRow-1)))
					{
//						alert(indexs);
						for(indexa=(indexs+1);indexa<(lastRow);indexa++)
						{
							indexb = indexa;
							//alert(indexb);
							$("#nourut"+indexb).val(($("#nourut"+indexb).val())-1);
							$("#counter"+indexb).val(($("#counter"+indexb).val())-1);
							$("#counterambil"+indexb).val(($("#counterambil"+indexb).val())-1);
						}
						
						$("#nourut"+indexs).val("");
						$("#pcode"+indexs).val("");
						$("#nama"+indexs).val("");
						$("#qty" + indexs).val("");
						$("#harga" + indexs).val("");
						$("#satuan"+indexs).val("");
						$("#keterangan"+indexs).val("");
						$("#counter"+indexs).val("");
						$("#counterambil"+indexs).val("");
						$("#tmppcode"+indexs).val("");
						$("#tmpqty" + indexs).val("");
						$("#komisi" + indexs).val("");
						$("#printer" + indexs).val("");
						
						for(indexc=indexs;indexc<lastRow;indexc++)
						{
							indexd = indexc;
							//alert(indexd);
							$("#nourut"+indexd).val($("#nourut"+(indexd+1)).val());
							$("#pcode"+indexd).val($("#pcode"+(indexd+1)).val());
							$("#nama"+indexd).val($("#nama"+(indexd+1)).val());
							$("#qty"+indexd).val($("#qty"+(indexd+1)).val());
							$("#harga"+indexd).val($("#harga"+(indexd+1)).val());
							$("#satuan"+indexd).val($("#satuan"+(indexd+1)).val());
							$("#keterangan"+indexd).val($("#keterangan"+(indexd+1)).val());
							$("#counter"+indexd).val($("#counter"+(indexd+1)).val());
							$("#counterambil"+indexd).val($("#counterambil"+(indexd+1)).val());
							$("#tmppcode"+indexd).val($("#tmppcode"+(indexd+1)).val());
							$("#tmpqty"+indexd).val($("#tmpqty"+(indexd+1)).val());
							$("#komisi"+indexd).val($("#komisi"+(indexd+1)).val());
							$("#printer"+indexd).val($("#printer"+(indexd+1)).val());
						}
						
						$("#baris"+(lastRow-1)).remove();
					}
				}
				else{
					$("#tmpqty" + indexs).val(qtybaru);
					$("#qty" + indexs).val(qtybaru);
					$("#keterangan" + indexs).val(keterangan);
				}
				
			}else
			{
			    if(id=="enter")
				{
					if(lastRow==1&&$("#pcode0").val()=="")
					{
						newid = 0;
					}else
					{
						newid = detailNew();
					}
					nourut = parseFloat($("#nourut" + newid).val());
					$("#nourut" + newid).val(newid+1);
					$("#pcode" + newid).val(pcode);
					$("#tmppcode" + newid).val(pcode);
					$("#nama" + newid).val($("#nmbrg").val());
					$("#tmpqty" + newid).val(qty);
					$("#qty" + newid).val(qty);
					$("#harga" + newid).val(harga);
					$("#satuan" + newid).val(satuan);
					$("#keterangan" + newid).val(keterangan);
					$("#komisi" + newid).val(komisi);
					$("#printer" + newid).val(printer);
					$("#transaksi").val("yes");
					pcode = $("#pcode" + newid).val();
				}
			}
			$("#statusubah").val(0);
		}
		else
		{
			indexs = parseFloat($("#NoUrut").val())-1;
			//alert(indexs);
	        $("#keterangan" + indexs).val(keterangan);
			$("#tmpqty" + indexs).val(qty);
			$("#qty" + indexs).val(qty);
		}
		//alert(pcode);
		hitungnetto();
		var totalsales = $("#totalsales").val();
		var taxsales = $("#taxsales").val();
		var scsales = $("#scsales").val();
		var nettosales = $("#nettosales").val();
		var displaytotal = document.getElementById("displaytotal");
		/*displaytotal.innerHTML = "Total " + totalsales + "<br>"+
								 "SC " + scsales + "<br>"
		                         "Tax " + taxsales + "<br>"+
								 "Netto " + nettosales;*/
		displaytotal.innerHTML =  number_format(nettosales, 0, ',', '.');
		
		$("#pcode").val("");
		$("#nmbrg").val("");
		$("#qty").val("");
		$("#harga").val("");
		$("#satuan").val("");
		$("#keterangan").val("");
		$("#komisi").val("");
		$("#printer").val("");
		$("#NoUrut").val("");
		$("#EditFlg").val(0);
		//$("#pcode").focus();
}

function number_format(a, b, c, d)
{
	a = Math.round(a * Math.pow(10, b)) / Math.pow(10, b);
	e = a + '';
	f = e.split('.');
	if (!f[0]) {
	f[0] = '0';
	}
	if (!f[1]) {
	f[1] = '';
	}
	if (f[1].length < b) {
	g = f[1];
	for (i=f[1].length + 1; i <= b; i++) {
	g += '0';
	}
	f[1] = g;
	}
	if(d != '' && f[0].length > 3) {
	h = f[0];
	f[0] = '';
	for(j = 3; j < h.length; j+=3) {
	i = h.slice(h.length - j, h.length - j + 3);
	f[0] = d + i +  f[0] + '';
	}
	j = h.substr(0, (h.length % 3 == 0) ? 3 : (h.length % 3));
	f[0] = j + f[0];
	}
	c = (b <= 0) ? '' : c;
	
	return f[0] + c + f[1];
}

function hitungnetto()
{
	
	var lastRow = document.getElementsByName("pcode[]").length;
	var total = 0;
	for(index=0;index<lastRow;index++)
	{
		indexs = index; 
		totalbaris = parseFloat($("#qty"+indexs).val())*parseFloat($("#harga"+indexs).val());
		total += totalbaris;
	}
	totalsales = Math.round(total);
	scsales = Math.round(total*0.05);
	taxsales = Math.round((totalsales+scsales)*0.1);
	nettosales = totalsales+scsales+taxsales;
	
	$("#totalsales").val(totalsales);
	$("#scsales").val(scsales);
	$("#taxsales").val(taxsales);
	$("#nettosales").val(nettosales);
	/*
	$("#total_rupiah").val(number_format(total, 0, ',', '.'));
	$("#nilai_sc_rupiah").val(number_format(nilsc, 0, ',', '.'));
	$("#nilai_pb1_rupiah").val(number_format(nilaipb1, 0, ',', '.'));
	$("#netto_rupiah").val(number_format(nettobulat, 0, ',', '.'));
	total = number_format(total, 0, ',', '.');
	*/
}

function detailNew()
{
	var clonedRow = $("#detail1 tr:last").clone(true);
	var intCurrentRowId = parseFloat($('#detail1 tr').length)-1;
	lastRow = document.getElementsByName("pcode[]").length;
	//alert(lastRow);
	//alert(intCurrentRowId);
	//alert(intCurrentRowId);
	var intNewRowId = parseFloat(intCurrentRowId)+1;
	$("#nourut" + intCurrentRowId , clonedRow ).attr( { "id" : "nourut" + intNewRowId, "value" : ""} );
	$("#pcode" + intCurrentRowId , clonedRow ).attr( { "id" : "pcode" + intNewRowId, "value" : ""} );
	$("#nama" + intCurrentRowId , clonedRow ).attr( { "id" : "nama" + intNewRowId, "value" : ""} );
	$("#qty" + intCurrentRowId , clonedRow ).attr( { "id" : "qty" + intNewRowId, "value" : ""} );
	$("#harga" + intCurrentRowId , clonedRow ).attr( { "id" : "harga" + intNewRowId, "value" : ""} );
	$("#printer" + intCurrentRowId , clonedRow ).attr( { "id" : "printer" + intNewRowId, "value" : ""} );
	$("#komisi" + intCurrentRowId , clonedRow ).attr( { "id" : "komisi" + intNewRowId, "value" : ""} );
	$("#satuan" + intCurrentRowId , clonedRow ).attr( { "id" : "satuan" + intNewRowId, "value" : ""} );
	$("#keterangan" + intCurrentRowId , clonedRow ).attr( { "id" : "keterangan" + intNewRowId, "value" : ""} );
	$("#tmppcode" + intCurrentRowId , clonedRow ).attr( { "id" : "tmppcode" + intNewRowId, "value" : ""} );
	$("#tmpqty" + intCurrentRowId , clonedRow ).attr( { "id" : "tmpqty" + intNewRowId, "value" : ""} );
	$("#detail1").append(clonedRow);
	$("#detail1 tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
	return intNewRowId;
}

//Toha - 051214
function popup_barang_soldout()
{
	baseurl = $("#baseurl").val();
	window.open(baseurl+"index.php/transaksi/barang_soldout/justlist", "_blank", "toolbar=no, scrollbars=yes, resizable=no, top=100, left=300, width=700, height=500");
}

function klikdisptext(cKey)
{
	id = cKey.getAttribute("id");
	val = cKey.getAttribute("value");
	nclass = cKey.getAttribute("class");
	
	posisitampil = parseFloat($("#posisitampil").val());
	
	id2 = id.split("displaytext");
	if(posisitampil==1)
	{
		id3 = (parseFloat(id2[1])-1);
	}
	else
	{
		id3 = (parseFloat(id2[1])+posisitampil-2);
	}
	
	nourut = $("#nourut"+id3).val();
	pcode = $("#pcode"+id3).val();
	nama = $("#nama"+id3).val();
	qty = $("#qty"+id3).val();
	satuan = $("#satuan"+id3).val();
	harga = $("#harga"+id3).val();
	komisi = $("#komisi"+id3).val();
	printer = $("#printer"+id3).val();
	tmppcode = $("#tmppcode"+id3).val();
	tmpqty = $("#tmpqty"+id3).val();
	counter = $("#counter"+id3).val();
	counterambil = $("#counterambil"+id3).val();
	keterangan = $("#keterangan"+id3).val();
	if(nama)
	{
	$("#NoUrut").val(nourut);	
	$("#pcode").val(pcode);	
	$("#tmppcode").val(tmppcode);	
	$("#nmbrg").val(nama);	
	$("#qty").val(qty);
	$("#harga").val(harga);	
	$("#komisi").val(komisi);	
	$("#printer").val(printer);	
	$("#satuan").val(satuan);	
	$("#keterangan").val(keterangan);	
	
	text = (qty+" "+nama);
	$("#dtext01").val(text);
	$("#dtext01").focus();
	
	$("#hdisplay0").val(pcode);
	$("#hdisplay1").val(text);
	
	$("#dtext02").val(keterangan);
	
	$("#statusubah").val(1);
	}
}
function text02()
{
	teks = $('#dtext02').val();
	searchtext = $("#dtext02").val().toUpperCase();
	flagsearch = $('#flagsearch').val().toUpperCase();
	if(flagsearch==1)
	{
		setupfilter(searchtext);
	    setMenu();
	}
	else
	{
		$('#keterangan').val(teks);
	}
}
</script>
<html style="height: 100%;">
<head>
<title>Secret Garden Village - Order Touch</title>
</head>
<!--onload="popup_barang_soldout();$('#dtext02').attr('disabled',true);"-->
<body style="height: 100%;margin:0px 0px 0px 0px;" >
<form method='post' name="order_touch" id="order_touch" action='<?=base_url();?>index.php/transaksi/order_touch/save_trans' onsubmit="return false" style="height: 100%;">
<input type="hidden" name="baseurl" id="baseurl" value="<?=base_url();?>">
<input type="hidden" name="kassa" id="kassa" value="<?=$NoKassa?>">
<input type="hidden" name="kasir" id="kasir" value="<?=$session_name?>">
<input type="hidden" name="store" id="store" value="<?=$store[0]['KdCabang']?>">
<input type="hidden" name="jenisprint" id="jenisprint" value="<?=$datakassa[0]['JenisPrint']?>">
<input type="hidden" name="kategorikassa" id="kategorikassa" value="<?=$datakassa[0]['KdKategori']?>">
<input type="hidden" id="ceknilai" name="ceknilai" value="">
<input type="hidden" id="transaksi" name="transaksi" value="no">
<input type="hidden" id="flagangka" name="flagangka" value=0>
<input type="hidden" id="hdisplay0" name="hdisplay0" value="">
<input type="hidden" id="hdisplay1" name="hdisplay1" value="">
<input type="hidden" id="statusubah" name="statusubah" value=0>
<input type="hidden" id="flagsearch" name="flagsearch" value=0>

<input type="hidden" id="kdpersonal" name="kdpersonal" value="">
<input type="hidden" id="namapersonal" name="namapersonal" value="">

<input type="hidden" id="statusisi" name="statusisi" value="waitress">
<input type="hidden" id="menutengah" name="menutengah" value=1>

<input type="hidden" id="totalitem" name="totalitem" value=0>
<input type="hidden" id="posisitampil" name="posisitampil" value=0>
<input type="hidden" id="posisi" name="posisi" value="<?=$datakassa[0]['KdKategori']?>">
<input type="hidden" id="posisisubkategori" name="posisisubkategori">


<input type="hidden" id="prevclass" name="prevclass" value="">
<input type="hidden" id="prevtext" name="prevtext" value="">
<input type="hidden" id="prevmenu" name="prevmenu" value="">
<input type="hidden" id="qty" name="qty" value=1>

<input type="hidden" id="totalsales" name="totalsales" value=0>
<input type="hidden" id="scsales" name="scsales" value=0>
<input type="hidden" id="taxsales" name="taxsales" value=0>
<input type="hidden" id="nettosales" name="nettosales" value=0>

<input type="hidden" id="indexa" name="indexa" value="">
<input type="hidden" id="index01" name="index01" value="">
<input type="hidden" id="index02" name="index02" value="">
<input type="hidden" id="index03" name="index03" value="">

<input type="hidden" id="subindex0101" name="subindex0101" value="">
<input type="hidden" id="subindex0102" name="subindex0102" value="">
<input type="hidden" id="subindex0103" name="subindex0103" value="">
<input type="hidden" id="subindex0104" name="subindex0104" value="">
<input type="hidden" id="subindex0105" name="subindex0105" value="">
<input type="hidden" id="subindex0106" name="subindex0106" value="">
<input type="hidden" id="subindex0107" name="subindex0107" value="">
<input type="hidden" id="subindex0108" name="subindex0108" value="">
<input type="hidden" id="subindex0109" name="subindex0109" value="">
<input type="hidden" id="subindex0110" name="subindex0110" value="">
<input type="hidden" id="subindex0111" name="subindex0111" value="">
<input type="hidden" id="subindex0112" name="subindex0112" value="">
<input type="hidden" id="subindex0113" name="subindex0113" value="">
<input type="hidden" id="subindex0114" name="subindex0114" value="">

<input type="hidden" id="subindex0201" name="subindex0201" value="">
<input type="hidden" id="subindex0202" name="subindex0202" value="">
<input type="hidden" id="subindex0203" name="subindex0203" value="">
<input type="hidden" id="subindex0204" name="subindex0204" value="">
<input type="hidden" id="subindex0205" name="subindex0205" value="">
<input type="hidden" id="subindex0206" name="subindex0206" value="">
<input type="hidden" id="subindex0207" name="subindex0207" value="">
<input type="hidden" id="subindex0208" name="subindex0208" value="">
<input type="hidden" id="subindex0209" name="subindex0209" value="">
<input type="hidden" id="subindex0210" name="subindex0210" value="">
<input type="hidden" id="subindex0211" name="subindex0211" value="">
<input type="hidden" id="subindex0212" name="subindex0212" value="">
<input type="hidden" id="subindex0213" name="subindex0213" value="">
<input type="hidden" id="subindex0214" name="subindex0214" value="">

<input type="hidden" id="subindex0301" name="subindex0301" value="">
<input type="hidden" id="subindex0302" name="subindex0302" value="">
<input type="hidden" id="subindex0303" name="subindex0303" value="">
<input type="hidden" id="subindex0304" name="subindex0304" value="">
<input type="hidden" id="subindex0305" name="subindex0305" value="">
<input type="hidden" id="subindex0306" name="subindex0306" value="">
<input type="hidden" id="subindex0307" name="subindex0307" value="">
<input type="hidden" id="subindex0308" name="subindex0308" value="">
<input type="hidden" id="subindex0309" name="subindex0309" value="">
<input type="hidden" id="subindex0310" name="subindex0310" value="">
<input type="hidden" id="subindex0311" name="subindex0311" value="">
<input type="hidden" id="subindex0312" name="subindex0312" value="">
<input type="hidden" id="subindex0313" name="subindex0313" value="">
<input type="hidden" id="subindex0314" name="subindex0314" value="">

<input type="hidden" id="idpersonal" name="idpersonal" value="">
<input type="hidden" id="idtable" name="idtable" value="">
<input type="hidden" id="idguest" name="idguest" value="">
<input type="hidden" id="idagent" name="idagent" value="">

<input type="hidden" id="satuan01" name="satuan01" value="">
<input type="hidden" id="satuan02" name="satuan02" value="">
<input type="hidden" id="satuan03" name="satuan03" value="">
<input type="hidden" id="satuan04" name="satuan04" value="">
<input type="hidden" id="satuan05" name="satuan05" value="">
<input type="hidden" id="satuan06" name="satuan06" value="">
<input type="hidden" id="satuan07" name="satuan07" value="">
<input type="hidden" id="satuan08" name="satuan08" value="">
<input type="hidden" id="satuan09" name="satuan09" value="">
<input type="hidden" id="satuan10" name="satuan10" value="">
<input type="hidden" id="satuan11" name="satuan11" value="">
<input type="hidden" id="satuan12" name="satuan12" value="">
<input type="hidden" id="satuan13" name="satuan13" value="">
<input type="hidden" id="satuan14" name="satuan14" value="">
<input type="hidden" id="satuan15" name="satuan15" value="">

<input type="hidden" id="harga01" name="harga01" value="">
<input type="hidden" id="harga02" name="harga02" value="">
<input type="hidden" id="harga03" name="harga03" value="">
<input type="hidden" id="harga04" name="harga04" value="">
<input type="hidden" id="harga05" name="harga05" value="">
<input type="hidden" id="harga06" name="harga06" value="">
<input type="hidden" id="harga07" name="harga07" value="">
<input type="hidden" id="harga08" name="harga08" value="">
<input type="hidden" id="harga09" name="harga09" value="">
<input type="hidden" id="harga10" name="harga10" value="">
<input type="hidden" id="harga11" name="harga11" value="">
<input type="hidden" id="harga12" name="harga12" value="">
<input type="hidden" id="harga13" name="harga13" value="">
<input type="hidden" id="harga14" name="harga14" value="">
<input type="hidden" id="harga15" name="harga15" value="">

<input type="hidden" id="komisi01" name="komisi01" value="">
<input type="hidden" id="komisi02" name="komisi02" value="">
<input type="hidden" id="komisi03" name="komisi03" value="">
<input type="hidden" id="komisi04" name="komisi04" value="">
<input type="hidden" id="komisi05" name="komisi05" value="">
<input type="hidden" id="komisi06" name="komisi06" value="">
<input type="hidden" id="komisi07" name="komisi07" value="">
<input type="hidden" id="komisi08" name="komisi08" value="">
<input type="hidden" id="komisi09" name="komisi09" value="">
<input type="hidden" id="komisi10" name="komisi10" value="">
<input type="hidden" id="komisi11" name="komisi11" value="">
<input type="hidden" id="komisi12" name="komisi12" value="">
<input type="hidden" id="komisi13" name="komisi13" value="">
<input type="hidden" id="komisi14" name="komisi14" value="">
<input type="hidden" id="komisi15" name="komisi15" value="">

<input type="hidden" id="printer01" name="printer01" value="">
<input type="hidden" id="printer02" name="printer02" value="">
<input type="hidden" id="printer03" name="printer03" value="">
<input type="hidden" id="printer04" name="printer04" value="">
<input type="hidden" id="printer05" name="printer05" value="">
<input type="hidden" id="printer06" name="printer06" value="">
<input type="hidden" id="printer07" name="printer07" value="">
<input type="hidden" id="printer08" name="printer08" value="">
<input type="hidden" id="printer09" name="printer09" value="">
<input type="hidden" id="printer10" name="printer10" value="">
<input type="hidden" id="printer11" name="printer11" value="">
<input type="hidden" id="printer12" name="printer12" value="">
<input type="hidden" id="printer13" name="printer13" value="">
<input type="hidden" id="printer14" name="printer14" value="">
<input type="hidden" id="printer15" name="printer15" value="">

<input type="hidden" name="EditFlg" id="EditFlg" value=0>
<input type="hidden" id="NoUrut" name="NoUrut" size="2" value="" onkeydown="keyShortcut(event,'editdata',this)">	
<input type="hidden" id="pcode" name="pcode" size="15" maxlength="15" onkeydown="keyShortcut(event,'pcode',this)">
<input type="hidden" id="tmppcode" name="tmppcode" value="">
<input type="hidden" readonly id="nmbrg" name="nmbrg" size="60" value="">
<input type="hidden" id="qty" name="qty" size="9" onKeyUp="SetQty()" onkeydown="keyShortcut(event,'qty',this)" class="InputAlignRight">
<input type="hidden" id="satuan" name="satuan" value="" class="InputAlignRight">
<input type="hidden" id="harga" name="harga" value="" class="InputAlignRight">
<input type="hidden" id="komisi" name="komisi" value="" class="InputAlignRight">
<input type="hidden" id="printer" name="printer" value="" class="InputAlignRight">
<input type="hidden" id="keterangan" name="keterangan" size="60" onkeydown="keyShortcut(event,'keterangan',this)" value="">

<div style="display:none">
<table name="detail1" id="detail1"> 
<tr id="baris0">
    <td>
		<input type="hidden" size="2" id="nourut0" name="nourut[]"readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="15" id="pcode0" name="pcode[]" readonly="readonly" value="">
		<input type="hidden" size="60" id="nama0" name="nama[]" readonly="readonly" value="">		
		<input type="hidden" size="9" id="qty0" name="qty[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="harga0" name="harga[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="satuan0" name="satuan[]" readonly="readonly" value="">
		<input type="hidden" size="2" id="printer0" name="printer[]" readonly="readonly" value="">
		<input type="hidden" size="2" id="komisi0" name="komisi[]" readonly="readonly" value="">
		<input type="hidden" size="60" id="keterangan0" name="keterangan[]"readonly="readonly" value="">
		<input type="hidden" id="counter0" name="counter[]">
		<input type="hidden" id="counterambil0" name="counterambil[]">
		<input type="hidden" id="tmppcode0" name="tmppcode[]" value="">
		<input type="hidden" id="tmpqty0" name="tmpqty[]" value="">
	</td>
</tr>
</table>
</div>

<?
for($z=0;$z<count($barang);$z++)
{
?>
		<input type="hidden" id="namalengkap<?=$z?>" name="namalengkap[]" value="<?=$barang[$z]['namalengkap']?>">
		<input type="hidden" id="pcodebrg<?=$z?>" name="pcodebrg[]" value="<?=$barang[$z]['pcode']?>">
		<input type="hidden" id="kdkategori<?=$z?>" name="kdkategori[]" value="<?=$barang[$z]['kdkategori']?>">
		<input type="hidden" id="kdsubkategori<?=$z?>" name="kdsubkategori[]" value="<?=$barang[$z]['kdsubkategori']?>">
		<input type="hidden" id="hargabrg<?=$z?>" name="hargabrg[]" value="<?=$barang[$z]['harga']?>">
		<input type="hidden" id="satuanbrg<?=$z?>" name="satuanbrg[]" value="<?=$barang[$z]['satuan']?>">
		<input type="hidden" id="flagready<?=$z?>" name="flagready[]" value="<?=$barang[$z]['flagready']?>">
		<input type="hidden" id="komisibrg<?=$z?>" name="komisibrg[]" value="<?=$barang[$z]['komisi']?>">
		<input type="hidden" id="printerbrg<?=$z?>" name="printerbrg[]" value="<?=$barang[$z]['printer']?>">
<?
}
?>
<input type="hidden" id="lenbarang" name="lenbarang" value=<?=$z?>>
<?
for($z=0;$z<count($kategori);$z++)
{
?>
		<input type="hidden" id="kdkategori0<?=$z?>" name="kdkategori0[]" value="<?=$kategori[$z]['kdkategori']?>">
		<input type="hidden" id="namakategori0<?=$z?>" name="namakategori[]" value="<?=$kategori[$z]['namakategori']?>">
<?
}
?>
<input type="hidden" id="lenkategori" name="lenkategori" value=<?=$z?>>
<?
for($z=0;$z<count($subkategori);$z++)
{
?>
		<input type="hidden" id="_kdkategori<?=$z?>" name="_kdkategori[]" value="<?=$subkategori[$z]['kdkategori']?>">
		<input type="hidden" id="_kdsubkategori<?=$z?>" name="_kdsubkategori[]" value="<?=$subkategori[$z]['kdsubkategori']?>">
		<input type="hidden" id="namasubkategori<?=$z?>" name="namasubkategori[]" value="<?=$subkategori[$z]['namasubkategori']?>">
<?
}
?>
<input type="hidden" id="lensubkategori" name="lensubkategori" value=<?=$z?>>
<?
for($z=0;$z<count($userid);$z++)
{
?>
		<input type="hidden" id="id<?=$z?>" name="id[]" value="<?=$userid[$z]['id']?>">
		<input type="hidden" id="username<?=$z?>" name="username[]" value="<?=$userid[$z]['username']?>">
		<input type="hidden" id="password<?=$z?>" name="password[]" value="<?=$userid[$z]['password']?>">
<?
}
?>
<input type="hidden" id="lenuserid" name="lenuserid" value=<?=$z?>>
<?
for($z=0;$z<count($lokasi);$z++)
{
?>
		<input type="hidden" id="kdlokasi<?=$z?>" name="kdlokasi[]" value="<?=$lokasi[$z]['kdlokasi']?>">
		<input type="hidden" id="namalokasi<?=$z?>" name="namalokasi[]" value="<?=$lokasi[$z]['keterangan']?>">
<?
}
?>
<input type="hidden" id="lenlokasi" name="lenlokasi" value=<?=$z?>>
<?
for($z=0;$z<count($keterangan);$z++)
{
?>
		<input type="hidden" id="ketrdtl<?=$z?>" name="ketrdtl[]" value="<?=$keterangan[$z]['keterangan']?>">
<?
}
?>
<input type="hidden" id="lenketr" name="lenketr" value=<?=$z?>>
<input type="hidden" id="statusketr" name="statusketr" value=0>
<table border="1" style="width:100%;height:100%;">
  <tr>
    <td onClick="addIt(this)" class="kategori" id='kategori01' value='1'><?=$kategori[0]['namakategori'];?></td>
    <td onClick="addIt(this)" class="kategori" id='kategori02' value='0'><?=$kategori[1]['namakategori'];?></td>
	<td onClick="addIt(this)" class="kategori" id='kategori03' value='0'><?=$kategori[2]['namakategori'];?></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori01' value='katergori01'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori02' value='katergori02'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori03' value='katergori03'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori04' value='katergori04'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori05' value='katergori05'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori06' value='katergori06'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori07' value='katergori07'></td>
	<td onClick="addIt2(this)" class="cancelbtn" id='cancel' value='1' colspan='1'>Cancel Trans</td>
	<td onClick="addIt2(this)" class="quitbtn" id='quit' value='1' colspan='1'>Quit</td>
  </tr>
  <tr>
    <td colspan="3" id="displaytext" class="subkategori">
	   <table>
	   <tr><td><input type="text" class="displaytextatas" size=30 id='dtext01' value='' readonly='readonly'></td></tr>
	   <tr><td><input type="text" class="displaytextatas" size=30 id='dtext02' value='' onkeyup="text02();" disabled='disabled'></td></tr>
	   </table>
	</td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori08' value='katergori08'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori09' value='katergori09'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori10' value='katergori10'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori11' value='katergori11'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori12' value='katergori12'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori13' value='katergori13'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori14' value='katergori14'></td>
    <td height="30" align="center" class="displaytotal" id="displaytotal" colspan='3'> 
	<!--<div id="jam2">
	<script language="javascript">
	jam2();
	</script>
	</div>-->	
	</td>
  </tr>
  <tr>
	<td rowspan="6" colspan="3" class="boxdisplay" id="boxtext">
		<table class='tabledisplay'>
		<tr>
		<td>
		<table>
		<tr>
		<td onClick="addIt(this)" colspan="1" class="menukanan1" id="waitress" value="0">User</br>-</td>
		<td onClick="addIt(this)" colspan="1" class="menukanan" id="table" value="0">Table</br>-</td>
		<td onClick="addIt(this)" colspan="1" class="menukanan" id="agent" value="0">Agen</br>-</td>
		<td onClick="addIt(this)" colspan="1" class="menukanan" id="guest" value="0">Guest</br>-</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext01' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id='up1' value='up1'>Up</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext02' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext03' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext04' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id="down1" value="0">Down</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext05' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext06' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext07' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id='search' value='0'>Search</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext08' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext09' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext10' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id='void' value='0'>Void</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext11' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext12' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext13' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt2(this)" class="column3" id='commit' value='0'>Commit</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext14' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext15' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
	   </table>
	   </td>
	   </tr>
	   </table>
	</td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah01' value='menutengah01'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah02' value='menutengah02'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah03' value='menutengah03'></td>
	<td onClick="addIt(this)" class="updown" id='up2' value='0'>Up</td>
	<td onClick="addIt(this)" class="updown" id='pageup2' value='0' colspan="2">Page Up</td>
	
	</tr>
  
  <tr>
	<td onClick="addIt(this)" rowspan="5" colspan="3" class="boxdisplay1" id="boxpicture" style="display:none">
		 <img width=100% height=100% src="<?=base_url();?>public/images/barang/1101.jpg"/>
	</td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah04' value='menutengah04'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah05' value='menutengah05'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah06' value='menutengah06'></td>
	<td onClick="addIt(this)" class="updown" id='down2' value='0'>Down</td>
	<td onClick="addIt(this)" class="updown" id="pagedown2" value="0" colspan="2">Page Down</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah07' value='menutengah04'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah08' value='menutengah05'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah09' value='menutengah06'></td>
    <td onClick="addIt(this)" class="angka" id='tujuh' value='7'>7</td>
	<td onClick="addIt(this)" class="angka" id='delapan' value='8'>8</td>
    <td onClick="addIt(this)" class="angka" id='sembilan' value='9'>9</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah10' value='menutengah10'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah11' value='menutengah11'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah12' value='menutengah12'></td>
    <td onClick="addIt(this)" class="angka" id='empat' value='4'>4</td>
	<td onClick="addIt(this)" class="angka" id='lima' value='5'>5</td>
    <td onClick="addIt(this)" class="angka" id='enam' value='6'>6</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah13' value='menutengah13'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah14' value='menutengah14'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah15' value='menutengah15'></td>
    <td onClick="addIt(this)" class="angka" id='satu' value='1'>1</td>
	<td onClick="addIt(this)" class="angka" id='dua' value='2'>2</td>
    <td onClick="addIt(this)" class="angka" id='tiga' value='3'>3</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" class="column2" colspan="2" id='plus' value='0'>+</td>
	<td onClick="addIt(this)" class="column2" colspan="2" id='minus' value='0'>-</td>
    <td onClick="addIt(this)" class="sptombol" id='note' value=''>Note</td>
	<td onClick="addIt(this)" class="sptombol" id='clear' value='0'>Clear</td>
    <td onClick="addIt(this)" class="sptombol" id='del' value='0'>Del</td>
	<td onClick="addIt(this)" class="angka" id='nol' value='0'>0</td>
    <td onClick="addIt(this)" class="sptombol" id='enter' value='0'>Ent</td>
  </tr>
</table>
</form>
</body>
</html>