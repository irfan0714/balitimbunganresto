
<!-- Modal History -->
<div class="modal fade" id="modal-recipe" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Pilih Recipe</h4>
        </div>
        <div class="modal-body">


					<table id="thisrecipe" class="table table-striped table-bordered" style="width:100%">
					<thead>
							<tr>
									<th>RECIPE ID</th>
									<th>NAMA</th>
									<th>ACTION</th>


							</tr>
					</thead>
					<tbody id="isirecipe">

					</tbody>

					</table>



          <div id="messages"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- Modal History -->
<?php

$this->load->view('header');

$modul = "Permintaan Khusus";

//$arr_data(unset);

$counter=1;
foreach($market_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["market_list_name"][$counter]=$val["market_list_name"];
	$arr_data["pcode"][$counter]=$val["pcode"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	$arr_data["satuanst"][$counter]=$val["SatuanSt"];

	$counter++;
}
?>

<script src="<?= base_url();?>assets/js/newdatatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/js/newdatatables/dataTables.bootstrap.min.js"></script>

<script language="javascript" src="<?=base_url();?>public/js/permintaan_barang.js"></script>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }

    function add_PCode()
    {
        targetBlank("<?=base_url()."inventory/npm_master_barang.php";?>");
    }

		function PilihRecipe(kode){
			var go = kode;
	  	var id = $('#id'+go).val();

			// alert(id);

			//ajaxget data1
	  	$.ajax({
	  	type: 'ajax',
	  	method: 'post',
	  	url: '<?php echo base_url() ?>index.php/transaksi/permintaan_khusus/pilihrecipe',
	  	data: {norecipe: id},
	  	async: false,
	  	dataType: 'json',
	  	success: function(data){

				// $("#dokumen").removeClass('form-control');

	  		var html3 = '';

	  		var i;


	  		//$(".modal-body #bookId").val( data1.names );
	  		var no =1;
	  		for(i=0; i<data.length; i++){



							var id=data[i].PCode;
	  					html3 +='<tr id="baris'+no+'">'+
	  									'<td>'+
											'<nobr>'+
											'<input type="text" class="form-control-new" name="pcode[]" id="pcode'+no+'" value="'+data[i].PCode+'" style="width: 100px;" onblur="UpdateItemID(this);"/>'+
 										 	'<a href="javascript:void(0)" id="get_pcode'+no+'" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>'+
											'</nobr>'+
											'</td>'+
											'<td>'+
	  									'<input readonly type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang'+no+'" value="'+data[i].NamaLengkap+'" style="width: 100%;"/>'+
	  									'</td>'+
	  									'<td>'+
	  									'<input type="text" class="form-control-new" name="v_qty[]" id="v_qty'+no+'" value="'+data[i].Qty+'" style="text-align: right;"/>'+
	  									'</td>'+
											'<td>'+
											'<select class="form-control-new" name="v_satuan[]" id="v_satuan'+no+'" >'+
					            '<option value="'+data[i].Satuan+'">'+data[i].Satuan+'</option>'+
											'</select>'+
											'</td>'+
											'<td>'+
											'<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_'+no+']" id="btn_del_detail_'+no+'" value="Save" onclick="deleteRow(this)">'+
											'<i class="entypo-trash"></i></button>'+
											'</td>'+
	  									'</tr>';

	  									no++;


	  		}
	  		$('#isipk').html(html3);


	  		// $('#isihistory2').html(html3);
	  		// $('[data-toggle="tooltip"]').tooltip();
				$('#modal-recipe').modal('hide');



	  	},
	  	error: function(){
	  		alert('Could not Show Fitur');
	  	}
	  	});
	  	//ajax
		}

		function showModal(){
			$('#modal-recipe').modal('show');

			// GET Approval
			var user='tes';
	  	$.ajax({
	  	type: 'ajax',
	  	method: 'post',
	  	url: '<?php echo base_url() ?>index.php/transaksi/permintaan_khusus/getrecipe',
	  	data: {user: user},
	  	async: false,
	  	dataType: 'json',
	  	success: function(data1){
	  		// $("#dokumen").removeClass('form-control');

	  		var html2 = '';

	  		var i;


	  		//$(".modal-body #bookId").val( data1.names );
	  		var no =1;
	  		for(i=0; i<data1.length; i++){

					var jenis;
					if(data1[i].Jenis=='C'){
						jenis = 'Compliment';
					}else {
						jenis = 'Publish';

					}

							var id=data1[i].resep_id;
	  					html2 +='<tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk Pilih" style="cursor:pointer; ">'+
	  									'<td onclick="PilihRecipe('+no+')">'+
	  									'<input type="hidden" id="id'+no+'" value="'+id+'">'+
	  									data1[i].resep_id+
	  									'</td>'+
	  									'<td onclick="PilihRecipe('+no+')">'+
	  									data1[i].NamaLengkap+
	  									'</td>'+
	  									'<td align="center" onclick="PilihRecipe('+no+')">'+
	  									jenis+
	  									'</td>'+
	  									'</tr>';

	  									no++;


	  		}
	  		$('#isirecipe').html(html2);
	  		// $('#isihistory2').html(html3);
	  		$('[data-toggle="tooltip"]').tooltip();

	  	},
	  	error: function(){
	  		alert('Could not Show Approval');
	  	}
	  	});
	  	// GET Approval

			$('#thisrecipe').DataTable();


		}



		// $('#panic').on('click', '.id-status', function(){
	  // 		$('#modal-default').modal('show');
	  // });
</script>

<div class="row">
    <div class="col-md-12" align="left">

    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>

		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/permintaan_khusus/save_data'>

	    <table class="table table-bordered responsive">

	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
					<tr>
	            <td class="title_table" width="150">No PB</td>
	            <td><b>Generate Saat Status OPEN</b></td>
	        </tr>

	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table" width="150">Tanggal Kebutuhan<font color="red"><b>(*)</b></font></td><!-- dulunya estimasi terima -->
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_est_terima" id="v_est_terima" size="10" maxlength="10">
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">Divisi <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<select class="form-control-new" name="v_divisi" id="v_divisi" style="width: 200px;">
	            		<option value="">Pilih Divisi</option>
	            		<?php
	            		foreach($mdivisi as $val)
	            		{

							?><option value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
	        </tr>

            <tr>
                <td colspan="100%">
                    <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Add PCode" title="" name="btn_add_pcode" id="btn_add_pcode" value="Add PCode" onClick="add_PCode()">
                        <i class="entypo-plus"></i> Add PCode
                    </button>
										<button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Add Recipe" title="" name="btn_add_pcode" id="btn_add_recipe" value="Add Recipe" onClick="showModal()">
												<i class="entypo-plus"></i> Add Recipe
										</button>
                </td>
            </tr>

	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody id="isipk">

							<?php $no=1; ?>

							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input readonly type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="toFormat2('v_qty<?php echo $no;?>')" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>" >
					                <option value=""> -- Pilih -- </option>

				                	</select>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>

						</tbody>
					</table>
	        	</td>
	        </tr>



	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/permintaan_khusus/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>

	    </table>

	    </form>

        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>

	</div>
</div>

<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
