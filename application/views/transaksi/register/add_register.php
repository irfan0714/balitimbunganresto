<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<!--<script language="javascript" src="--><?//= base_url(); ?><!--public/js/komisi.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
    
    function submit_this() {
//        if(!$('#kdagent').val()){
//            alert('Masukan Kd Group');
//            $('#kdagent').focus();
//            return false;
//        }

		if(document.getElementById('kode').value=="")
		{
			alert("Kode Member harus di isi");
			return false
		}
         else if(document.getElementById('tourtravel').value=="")
        {
            alert("Tour Travel harus di isi");
            return false
        }
		else if(document.getElementById('nostiker').value=="")
		{
			alert("Nomor stiker harus di isi");
			return false
		}
       
	}

    function generateList(){
        base_url = $("#baseurl").val();
        url = base_url+"index.php/pop/tour_leader/index/";
        window.open(url,'popuppage','width=950,height=550,top=100,left=100');
    }
    
    function targetBlank (url) {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
    
    function addMember()
    {
        url = "<?= base_url(); ?>index.php/master/tour_leader/";
        targetBlank (url);
    }

</script>

<body onload="firstLoad('komisi'); //loading()">
    <form method='post' name="komisi" id="komisi" action='<?= base_url(); ?>index.php/transaksi/register/save_new_register' onsubmit="return submit_this();" class="form-horizontal">
        <div class="col-md-12">
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        <?=$judul;?>
                    </div>
                </div>
                <div class="panel-body">
                
                	<div class="form-group">
                        <label class="col-sm-2"> Kode Member </label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="form-control-new datepicker" value="<?php echo $aplikasi->TglTrans; ?>" name="tgl" id="tgl" size="10" maxlength="10">
                            </div>
                        </div>
                    </div>

                    <?php
                        $mylib = new globallib();
                        //echo $mylib->neonwrite_textbox("Tanggal","tgl",$aplikasi->TglTrans,"15","10","","text",$gantikursor,"1");
                        echo $mylib->neonwrite_textbox("Jam","jam",date("H:i:s"),"15","10","readonly='readonly'","text",$gantikursor,"1");
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2"> Kode Member </label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input name="kode" type="text" readonly="readonly" class="form-control" id="kode" />
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();" title="Search Member">Go</button>
                                    &nbsp;&nbsp;&nbsp;
                                    <button class="btn btn-primary" type="button" id="btnaddmember" onclick="addMember();" title="Add Member"> + </button>
                                </span> 
                            </div>
                        </div>
                    </div>


                    <?php
                        echo $mylib->neonwrite_textbox("Nama","nama","","15","10","readonly='readonly'","text",$gantikursor,"1");
                       // echo $mylib->neonwrite_textbox("Nama tour & travel","nmtour","","15","10","readonly='readonly'","text",$gantikursor,"1");

                    ?>
                     <div class="form-group">
                        <label class="col-sm-2 control"> Tour Travel</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="tourtravel" name="tourtravel">
                                    <option value="">--Please Select--</option>
                                        <?php foreach ($tourtravel->result() as $as): ?> 
                                            <option value="<?=$as->KdTravel?>"><?=$as->Nama?></option>
                                        <?php endforeach;?>
                                    </select>

                            </div>
                    </div>
            <br>
            
                  <div class="form-group">
                        <label class="col-sm-2"> No Stiker </label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input name="nostiker" type="text" class="form-control" id="nostiker" onblur="jml_digit()"/>
                                <span id="span_loading" style="float: right; display: none;"><img src="../../../../public/images/ajax-image.gif"/>&nbsp;&nbsp;<b> Cek No. Stiker...</b></span>
                            </div>
                        </div>
                    </div>
            
            <?php
                        //echo $mylib->neonwrite_textbox("No Stiker","nostiker","","15","10","","text",$gantikursor,"1");
                        echo $mylib->neonwrite_textbox("Jumlah Rombongan","jumlah","","15","10","","text",$gantikursor,"1");
                        echo $mylib->neonwrite_textbox("No Mobil","ket","","35","35","","text",$gantikursor,"1");
						echo $mylib->neonwrite_textbox("Bank","bank","","35","35","","text",$gantikursor,"1");
						echo $mylib->neonwrite_textbox("No Rek Bank","norekbank","","35","35","","text",$gantikursor,"1");
                    ?>


                    <div id="Layer1" style="display:none">
                        <p align="center">
                            <img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
                        </p>
                    </div> 


                    <input type='hidden' id="nodok" name="nodok" value="">
                    <input type='hidden' id="flag" name="flag" value="add">
                    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/transaksi/register/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit" id="btn_save" onclick="return submit_this;"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                    
                </div>



            </div>
        </div>
    </div>
</form>

<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>


<script>

function jml_digit()
{
	stiker = $('#nostiker').val();
	panjang = stiker.length;
    
		if(panjang>4){
		alert('No Stiker Yang Di Izinkan Hanya 4 Karakter.');	
		$('#nostiker').val("");
		return false;
		}
	
	
	//cek apakah no. stiker tersebut sudah digunkan ditanggal yang sama.

	tgl = $('#tgl').val();
	if(stiker!=""){
		
		$("#span_loading").css("display","");
		$('#btn_save').attr('disabled',true);
		
		$('#jumlah').attr('disabled',true);
		$('#ket').attr('disabled',true);
		$('#bank').attr('disabled',true);
		$('#norekbank').attr('disabled',true);
		
		$.ajax({
            url: "<?php echo site_url('transaksi/register/cek_stiker') ?>/"+stiker+"/"+tgl,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data.status==true){
                  	$("#span_loading").css("display","none");
				  	alert('No Stiker '+stiker+' Duplicate di Tanggal '+tgl+' ini. Ganti dengan No. Stiker Lain.');	
					$('#nostiker').val("");
					$('#btn_save').attr('disabled',false);
					$('#jumlah').attr('disabled',false);
					$('#ket').attr('disabled',false);
					$('#bank').attr('disabled',false);
					$('#norekbank').attr('disabled',false);
					$('#nostiker').focus();
					return false;
				  }else{
				  	$("#span_loading").css("display","none");
				  	$('#btn_save').attr('disabled',false);
				  	$('#jumlah').attr('disabled',false);
					$('#ket').attr('disabled',false);
					$('#bank').attr('disabled',false);
					$('#norekbank').attr('disabled',false);
				  }     		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
        
	}
	

}
</script>

