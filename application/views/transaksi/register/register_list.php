<?php
$this->load->view('header');
$searchby = $this->input->post('searchby');
$date1 = $this->input->post('date1');
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<!--<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>-->
<script language="javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script language="javascript">
    $(function () {
                $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy'
                });
            });
            
    
    function deleteTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menghapus Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/keuangan/paymentv/delete_paymentv/", {
                kode: nodok},
            function (data) {
                //                            alert("data");
                window.location = url + "index.php/keuangan/paymentv/";
            });
        }
    }
    function PopUpPrint(kode, baseurl)
    {
        url = "index.php/transaksi/register/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }
	
	function cetakprint(kode, baseurl)
    {
        url = "index.php/transaksi/register/cetakprinter/" + escape(kode);
        window.location=baseurl + url;
    }
	
    function gantiSearch()
    {
        if ($("#searchby").val() == "NoTransaksi" || $("#searchby").val() == "Nama")
        {
            $("#normaltext").css("display", "");
            $("#datetext").css("display", "none");
            //$("#date1").datepicker("destroy");
            $("#date1").val("");
        }
        else
        {
            $("#datetext").css("display", "");
            $("#normaltext").css("display", "none");
            $("#stSearchingKey").val("");
            //$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>/public/images/calendar.png' });
            //$('#date1').Zebra_DatePicker({format: 'd-m-Y'});
        }
    } 
    
    function start_page()
    {
        document.getElementById("stSearchingKey").focus();
    }
</script>
<body onload="/* option(); */ start_page();">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$judul?>
                </div>
            </div>
            <div class="panel-body">
			<?php if ($date1 != ""){
					$hide = "style='display:'";
					$hide2 = "style='display:none'";
					}else{
						$hide = "style='display:none'";					
						$hide2 = "style='display:'";
					}
				?>
              <form method="POST"  name="search" action="">
                    <div class="control-group">
                        <div class="controls" align="right">
                            <table border="0">
                                <tr>
                                    <td id="normaltext" <?=$hide2;?>><input type='text' size='20' name='stSearchingKey' id='stSearchingKey' class="form-control" value="<?php echo @$_POST['stSearchingKey']; ?>"></td>
                                    <td id="datetext" <?=$hide;?>><input type='text' size='12' name='date1' id='date1' class="form-control datepicker" data-date-format="dd-mm-yyyy" value="<?php echo @$_POST['date1']; ?>"></td>
                                    <td>
                                        <select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()" class="form-control">
                                            <option <?php if ($searchby == "NoTransaksi") echo "selected='selected'"; ?> value="NoTransaksi">No Transaksi</option>
                                            <option <?php if ($searchby == "Tanggal") echo "selected='selected'"; ?> value="Tanggal">Tanggal</option>
                                            <option <?php if ($searchby == "Nama") echo "selected='selected'"; ?> value="Nama">Member</option>
                                        </select>
                                    </td>
                                    <td><button type="submit" class="btn-primary">GO</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>

                <br>

                <table align = 'center' class='table table-bordered table-hover responsive'>
                    <thead>
                        <tr>
                            <th nowrap>NoTransaksi</th>    
                            <th nowrap>Tanggal</th>
                            <th nowrap>Jam</th>
                            <th nowrap>Member</th>
							<th nowrap>Bank - No Rekening</th>
                            <th nowrap>Tour Travel</th>
                            <th nowrap>No Stiker</th>
                            <th nowrap>Estimasi Rombongan</th>
                            <th nowrap>No Mobil</th>
                            <?php
                            if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <th nowrap>Action</th>
                            <?php } $mylib = new globallib(); ?>
                        </tr>
                    </thead>
                    <?php
                    if (count($data) == 0) {
                        ?>
                        <td nowrap colspan="<?php echo count($header) + 1; ?>" align="center">Tidak Ada Data</td>
                        <?php
                    }
                    for ($a = 0; $a < count($data); $a++) {
                        ?>
                        <tr>
                            <td nowrap><?= $data[$a]['KdRegister']; ?></td>
                            <td nowrap><?= $mylib->ubah_format_tanggal($data[$a]['Tanggal']); ?></td>
                            <td nowrap><?= $data[$a]['Jam']; ?></td>
                            <td nowrap><?= stripslashes($data[$a]['KdTourLeader']).' - '.stripslashes($data[$a]['nmAgen']); ?></td>
							<td nowrap><?= $data[$a]['Bank'].' - '.$data[$a]['NoRekBank']?></td>
                            <td nowrap><?= stripslashes($data[$a]['NamaTravel']); ?></td>
                            <td nowrap><?= stripslashes($data[$a]['NoStiker']); ?></td>
                            <td nowrap><?= stripslashes($data[$a]['Jumlah']); ?></td>
                            <td nowrap><?= stripslashes($data[$a]['Keterangan']); ?></td>
                            <?php
                            if ($link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <td nowrap>
                                    <?php
                                    if ($link->view == "Y") {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/printer.png' border = '0' title = 'Print' onclick="cetakprint('<?= $data[$a]['KdRegister']; ?>', '<?= base_url(); ?>');"/></a>
                                        <?php
                                    }
                                    if ($link->edit == "Y" && (substr($data[$a]['Tanggal'], 4, 2) == date('m'))) {
                                        ?>
                                        <a href="<?= base_url(); ?>index.php/keuangan/komisi/edit_komisi/<?= $data[$a]['KdRegister']; ?>"><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                                        <?php
                                    }
                                    if ($link->delete == "Y" && (substr($data[$a]['Tanggal'], 4, 2) == date('m'))) {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?= $data[$a]['KdRegister']; ?>', '<?= base_url(); ?>');"/>
                                        <?php
                                    }
                                    ?>
                                </td>
                            <?php } ?>

                            <?php
                        }
                        ?>
                </table>
                <table align = 'center'  >
                    <tr>
                        <td>
                            <?php echo $this->pagination->create_links(); ?>
                        </td>
                    </tr>
                    <?php
                    if ($link->add == "Y") {
                        ?>
                        <tr>
                            <td nowrap colspan="3">
                                <a href="<?= base_url(); ?>index.php/transaksi/register/add_new/"><img src='<?= base_url(); ?>public/images/add.png' border = '0' title = 'Add'/></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php $this->load->view('footer'); ?>
