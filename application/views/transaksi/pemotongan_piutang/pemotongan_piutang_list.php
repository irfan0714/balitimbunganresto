<?php $this->load->view('header'); ?>
<head>
</head>
<script language="javascript" src="<?= base_url(); ?>public/js/pemotongan_piutang.js"></script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/pemotongan_piutang/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />  
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
                <option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Close</option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2">Void</option>
            </select> 
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
           
            <a href="<?php echo base_url() . "index.php/transaksi/pemotongan_piutang/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>

        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>CN Allocation No</center></th>
		        <th width="100"><center>Credit Note No</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th width="150"><center>Customer</center></th>
		        <th width="50"><center>Currency</center></th>
		        <th width="100"><center>Total Amount</center></th>
		        <!--<th width="100"><center>Total Amount IDR</center></th>-->
		        <th width="50"><center>Status</center></th>
                <th width="70"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Pending</b></font>";
                } else if ($val["status"] == 1) {
                    $echo_status = "<font style='color:#6bcf29'><b>Close</b></font>";
                } else if ($val["status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>View</b></font>";
                }
                ?>
                <tr title="<?php echo $val["cnallocationno"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["cnallocationno"]; ?></td>
                    <td align="center"><?php echo $val["cnno"]; ?></td>
                    <td align="center"><?php echo $val["cnadate"]; ?></td>
                    <td align="left"><?php echo $val["customerid"]." :: ".$val["Nama"]; ?></td>
                    <td align="center"><?php echo $val["currencycode"]; ?></td>
                    <td align="right"><?php echo number_format($val["allocationtotal"],0); ?></td>
                    <!--<td align="right"><?php echo number_format($val["allocationidrtotal"],0); ?></td>-->
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                        if ($val["status"] == 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/pemotongan_piutang/edit_pemotongan_piutang/<?php echo $val["cnallocationno"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>

                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["cnallocationno"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }

                        if ($val["status"] == 1) {
                            ?>
                            
                            <a href="<?php echo base_url(); ?>index.php/transaksi/pemotongan_piutang/view_pemotongan_piutang/<?php echo $val["cnallocationno"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>

                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/pemotongan_piutang/edit_pemotongan_piutang/<?php echo $val["cnno"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["cnallocationno"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }

                        if ($val["status"] == 2) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/pemotongan_piutang/view_pemotongan_piutang/<?php echo $val["cnallocationno"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['cnno'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>-->
                            
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["cnallocationno"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }
                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
