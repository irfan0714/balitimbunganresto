<?php 

$this->load->view('header'); 

$modul = "Pemotongan Piutang";

?>

<script language="javascript" src="<?=base_url();?>public/js/pemotongan_piutang.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<script>
	function type_onchange(){
		type = $("#v_type").val();
		
		$.ajax({
	         url: "<?=base_url();?>index.php/transaksi/pemotongan_piutang/ajax_customer/",
	         type: "POST",
	         async: true,
	         data: {Type: type },
	         success: function(res)
				{
					$('#KdCustomer').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
      	});
	}
	
	function ambilpiutang(){
		
		 var url = $("#base_url").val();
	     var customer = $('#KdCustomer').val();
	     var tgl = $('#v_tanggal_dokumen').val();
	     
				$.post(url+"index.php/transaksi/pemotongan_piutang/getDebitCreditNo", { KdCustomer: $("#KdCustomer").val()},
				function(data){
				   $("#creditno").empty();
				   $("#creditno").append(data);
				   $("#creditno2").empty();
				   $("#creditno2").append(data);
				});
	     
			  $.ajax({
					url: url+"index.php/transaksi/pemotongan_piutang/ambilpiutang/",
					data: {customer:customer,tanggal:tgl},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#TabelDetail').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
					   });    	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/pemotongan_piutang/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal Debit</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tanggal_dokumen" id="v_tanggal_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	         <tr>
	            <td class="title_table">Tipe</td>
	            <td>
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 25%;" onchange="type_onchange()">
	            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
	            		<option value="">- Pilih Tipe -</option>
	            		<option value="CS">CN Distribusi</option>
	            		<option value="CR">CN Reservasi</option>
	            	</select>
	            </td>
	        </tr>
	        
	        <tr>
				<td class="title_table">Customer </td>
				<td nowrap>
				<select class="form-control-new" name="KdCustomer" id="KdCustomer" size="1" onchange="ambilcreditno('<?=base_url();?>')" style="width: 25%;">
				<option  value=""> Pilih Customer </option>
					<?php
						for($s=0;$s<count($mcustomer);$s++)
						{
							?>
							<option <?php if($mcustomer[$s]['Nama']==$spp) echo "selected"; ?> value="<?=$mcustomer[$s]['KdCustomer'];?>"><?=$mcustomer[$s]['Nama'];?></option>
						<?php
						}
					?>
				</select></td>
			</tr>
			
			<tr>
				<td class="title_table">Credit Note No </td>
				<td nowrap>
				<select class="form-control-new" name="creditno" id="creditno" size="1" style="width: 25%;">
				<option <?php if($creditno=="") echo "selected"; ?> value=""> Pilih Credit Note No </option>
					<?php
						for($s=0;$s<count($anak);$s++)
						{
							?>
							<option <?php if($anak[$s]['nama']==$creditno) echo "selected"; ?> value="<?=$anak[$s]['nama'];?>"><?=$anak[$s]['nama'];?></option>
						<?php
						}
					?>
				</select></td>
			</tr>
	        
	        <tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Mata Uang", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	       
	        <!--<tr>
	            <td>&nbsp;</td>
	            <td> Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="" name="v_amount" id="v_amount" maxlength="255" size="25%" style="text-align: right;">
	            	 &nbsp;&nbsp;&nbsp;&nbsp;
	            	 Allocation Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="" name="v_amount_allocation" id="v_amount_allocation" maxlength="255" size="25%" style="text-align: right;">
	            </td>
	        </tr>
	        
	        <tr>
	            <td>&nbsp;</td>
	            <td> Amount IDR &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="" name="v_amount_idr" id="v_amount_idr" maxlength="255" size="25%" style="text-align: right;">
	            	 &nbsp;&nbsp;&nbsp;&nbsp;
	            	 Allocation Amount IDR &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="" name="v_amount_allocation_idr" id="v_amount_allocation_idr" maxlength="255" size="25%" style="text-align: right;">
	            </td>
	        </tr>-->
	        
	        <!--<tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;">
		            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
		            		<!--<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Pending</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Close</option>
		            	</select>
		            </td>
		        </tr>
            
            <tr>
                <td colspan="100%"  align="right">
                    <?php 
                    if($this->uri->segment(4)=='1'){?>
						<a href="<?php echo base_url() . "index.php/transaksi/sales_invoice/add_new/"; ?>" class="btn btn-orange btn-icon btn-sm icon-left" title="" >Unlock Add Item<i class="entypo-key"></i></a>
					<?php }else{?>
						<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-md md-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari Account Pick List" title="">Add Item<i class="entypo-plus"></i></a>
					<?php }
                     ?>
                    </td>
            </tr>-->
	        
	        <!--<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor Rekening</center></th>
								<th><center>Nama Rekening</center></th> 
							    <th width="450"><center>Deskripsi</center></th>
							    <th width="200"><center>Amount</center></th>
							    <th width="50"><center>
							    	Action</center>
							    </th>
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($datado)<=0){
						 	echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
						 }else{
						 	$no=0;
						 	foreach($datado as $val){
						 		$no		= $no + 1;
								?>
								<tr>
									<td>
									<?php echo $val['coano']; ?>
									<input type="hidden" class="form-control-new" name="v_coano[]" id="v_coano<?php echo $no;?>" value="<?php echo $val['coano']; ?>" style="width: 100%;"/>										
									</td>
									<td>
									<?php echo $val['NamaRekening']; ?>
										
									</td>
									<td>
										<input type="text" class="form-control-new" name="v_dekripsi[]" id="v_deskripsi<?php echo $no;?>" value=""  style="width: 100%;"/>
									</td>
									<td>
										<input type="text" class="form-control-new" name="v_amount[]" id="v_amount<?php echo $no;?>" value="" onkeypress="hitung(event,'<?=$no;?>');" style="text-align: right; width: 100%;" />
									</td>
									<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail('<?php echo $val["dndid"]; ?>','<?php echo $val["coano"]; ?>','<?php echo $val["adduser"]; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
								</tr>
								<?php
								
							}
						 }
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>-->
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="batal('<?php echo base_url(); ?>');">Batal<i class="entypo-check"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                </td>
	        </tr>
	        
	    </table>
	    <!--
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data Summary</li>
			</ol>
			
	         <table class="table table-bordered responsive">
			 
	            <tr>
	            	<td align="right" width="87%">Total</td>
		            <td nowrap align="right">
		            	<input type="text" name="total" id="total" class="form-control-new" value=""  maxlength="255" size="50%" style="text-align: right;">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">Discount</td>
		            <td align="right">
		            	<input type="text" name="diskon" id="diskon" class="form-control-new" value=""  maxlength="255" size="5%" style="text-align: right;">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_diskon" id="potongan_diskon" class="form-control-new" value=""  maxlength="255" size="30%" style="text-align: right;">		            		            	
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">PPN</td>
		            <td nowrap align="right">
		            	<input type="text" name="ppn" id="ppn" class="form-control-new" value="10"  maxlength="255" size="5%" style="text-align: right;">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_ppn" id="potongan_ppn" class="form-control-new" value=""  maxlength="255" size="30%" style="text-align: right;">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150"><b>Grand Total</b></td>
		            <td align="right">
		            	<input type="text" name="grandtotal" id="grandtotal" class="form-control-new" value=""  maxlength="255" size="50%" style="text-align: right;">
		            	<input type="hidden" name="grandtotalhidden" id="grandtotalhidden" class="form-control-new" value=""  maxlength="255" size="50%" style="text-align: right;">
		            </td>
	            </tr>
	         </table>-->
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>