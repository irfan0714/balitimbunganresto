<?php
$this->load->library('globallib');
$mylib = new globallib();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Proposal</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 10pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 9pt;
			vertical-align: top;
		}
		
	</style>
    <body>
    
   <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
            	<td colspan="5" align="left">
            		PT. Natura Pesona Mandiri<br>
					Jl. Raya Denpasar Bedugul KM.36 Tabanan<br>
					Bali 82191 - Indonesia<br>
					Phone : +62 361 4715379<br>
            	</td>
            </tr>
            
            <tr>
                <td colspan="5" align="center"><b>PURCHASE REQUEST NON STOCK</b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><span><?= "No : ".$nomor; ?></span></td>
            </tr>
            
    </table> 
    <br>
    <table width="30%"  border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        <tr>
                            <td>Tanggal PR</td>
                            <td>:</td>
                            <td width="80" align="right"><span><?= $mylib->ubah_tanggal($header[0]['TglDokumen']); ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>Divisi</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header[0]['NamaDivisi']; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td>Tanggal Terima</td>
                            <td>:</td>
                            <td width="80" align="right"><span><?= $mylib->ubah_tanggal($header[0]['TglTerima']); ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="200" align="right"><span>&nbsp;</span></td>
                        </tr>
		</table>
		<br>
		
		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
			<tr>
				<td>No</td>
				<td>Nama Barang</td>
				<td>Qty</td>
			</tr>
			<?php 
			$no = '1';
			foreach($detail AS $val){?>
			<tr>
				<td><?= $no;?></td>
				<td><?= $val['NamaBarang'];?></td>
				<td><?= $val['Qty'];?></td>
			</tr>
			<?php 
			$no++;
			} ?>
		</table> 
		
		<br>
		
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
			<tr>
				<td align="center">Dibuat Oleh,</td>
				<td align="center">DiKetahui Oleh,</td>
				<td align="center">DiTerima Oleh,</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">..................</td>
				<td align="center">..................</td>
				<td align="center">..................</td>
			</tr>
		</table>  

</body>
</html>