<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table>
	<tr>
		<td><input class="form-control-new" type='text' size'20' name='stSearchingKey' id='stSearchingKey'>
		<select class="form-control-new"  size="1" height="1" name ="searchby" id ="searchby">
				<option value="NoDokumen">No Purchase </option>
				<option value="NoProposal">No Proposal </option>
				<option value="Keterangan">Keterangan </option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search"></td>
		
	</tr>
</table>
</form>
<button class="btn btn-info pull-right" style="margin-right:10px;" onclick="form_tambah_pr_marketing()"><i class="entypo-plus"></i> &nbsp Buat Purchase Request</button>
<br><br><br>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
				<th width="100"><center>No Purchase</center></th>
		        <th width="100"><center>No Proposal</center></th>
				<th width="100"><center>Tgl Dokumen</center></th>
				<th width="100"><center>Tgl Terima</center></th>
				<th width="200"><center>Divisi</center></th>
				<th><center>Keterangan</center></th>
		        <th width="100"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }
			
			
			
			
            $no = 1;
            foreach ($data as $val) {
			
			if($val["Status"]==0){
			$statuz="Pending";
			
			}else if($val["Status"]==1){
			
			$statuz="Open";
			}else{
			$statuz="Void";
			}
                
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $val["NoProposal"]; ?></td>
					<td align="center"><?php echo $val["TglDokumen_"]; ?></td>
					<td align="center"><?php echo $val["TglTerima_"]; ?></td>
					<td align="left"><?php echo $val["NamaDivisi"]; ?></td>
					<td align="left"><?php echo $val["Keterangan"]; ?></td>
					<td align="center"><?php echo $statuz; ?></td>
                    <td align="center">
					
							<?php if($val["Status"]==0){?>
							<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="edit_pr('<?php echo $val["NoDokumen"];?>','<?php echo base_url(); ?>')"><i class="entypo-pencil"></i></button>
							<?php }else if($val["Status"]==1){?>
							<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="view_pr('<?php echo $val["NoDokumen"];?>','<?php echo base_url(); ?>')"><i class="entypo-eye"></i></button>
                    		
                    		<!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="delete_pr_marketing('<?php echo $val["NoDokumen"]; ?>','<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>-->
                            
                            <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print Dot.Matrix" title="" onclick="PopUpPrint('<?= $val['NoDokumen'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>
                            
                            <button type="button" class="btn btn-orange btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrintPDF('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
								<i class="entypo-export"></i>
							</button>
                            
                            
							<?php } ?>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<?php
$this->load->view('footer'); ?>

	<!-- /default modal -->
	<div class="modal fade in custom-width" id="default_modal">
	    <div class="modal-dialog " style="width:90%">
	        <div class="modal-content">
	            <div class="modal-header"> 
	            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4><b>Buat Purchase Request Marketing</b></h4>
	            </div>
	            <div class="modal-body" style="height:350px;overflow-y: scroll;">
	                <form action="<?=base_url();?>index.php/transaksi/pr_marketing/save_pr" id="form" name="form" method="POST" class="form-horizontal">
						<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
						<div class="table-responsive">
          					<table class="table table-bordered responsive">
            					<tbody>
              						<tr>
                						<td class="title_table" width="150">No Dokumen</td>
                						<td>
										<input readonly id="nopr" name="nopr" placeholder="Auto Generate" class="form-control-new" type="text" style="width:200px;">
										</td>
              						</tr>	
			  						<tr>
                						<td class="title_table" width="150">No Proposal</td>
                						<td> 
                							<input type="text" class="form-control-new" size="20" maxlength="10" name="v_keyword_proposal" id="v_keyword_proposal" placeholder="Keyword" onblur="cari_proposal('<?php echo base_url(); ?>')">
                    						<span id="select_rekening">
                        						<select name="noproposal" id="noproposal" class="form-control-new" style="width:400px;">
                           							<option value=""> -- Pilih -- </option>
		            								<?php
		            								foreach($proposal as $val)
		            								{
								            			$selected="";
														if($act_==$val["NoProposal"])
														{
															$selected='selected="selected"';
														}
													?>
													<option <?php echo $selected; ?> value="<?php echo $val["NoProposal"]; ?>"><?php echo $val["NamaProposal"]; ?></option><?php
													}
		            								?>
            									</select>    
                							</span>
										</td>
          							</tr>
			  						<tr>
                						<td class="title_table" width="150">Tanggal Dokumen</td>
                						<td>
										<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
                						</td>
              						</tr>
		  							<tr>
            							<td class="title_table" width="150">Tanggal Terima</td>
            							<td>
										<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen_terima" id="v_tgl_dokumen_terima" size="10" maxlength="10">
            							</td>
          							</tr>
			  						<tr>
                						<td class="title_table" width="150">Divisi</td>
                						<td>
										<select name="divisi" id="divisi" class="form-control-new" style="width:150px;">
                       						<option value=""> -- Pilih -- </option>
	            							<?php
	            							foreach($divisi as $val)
            								{
	            								$selected="";
												if($div_==$val["KdDivisi"])
												{
													$selected='selected="selected"';
												}
												?><option <?php echo $selected; ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
											}
	            							?>
            							</select>
										</td>
              						</tr>
			  						<tr>
                						<td class="title_table" width="150">Note</td>
                						<td>
										<input id="keterangan" name="keterangan" class="form-control-new" type="text" style="width: 530px;">
										</td>
              						</tr>
			  
			  						<tr>
                						<td class="title_table" width="150">Status</td>
                						<td>
											<select class="form-control-new" name="v_status" id="v_status" style="width: 150px;">
	            								<option value="0">Pending</option>
	            								<option value="1">Open</option>
	            								<option value="2">Void</option>
	            							</select>
										</td>
              						</tr>
              					</tbody>
      						</table>
    					</div>
  						<br>
        				<br>
						<!-- edit only ------------------------------ -->
						<div class="table-responsive">
	        				<table id="edit_only" name="edit_only" class="table table-bordered responsive">
		 							<thead class="title_table">
									<tr>
					    				<th width="611"><center>Nama Barang</center></th>
										<th><center>Qty</center></th>
										<th width="100"><center>Action</center>
										</th>
					    
									</tr>
								</thead>
								<tbody id="edit_only2" border="1">
								</tbody>
							</table>
						</div>
						<!-- edit only ------------------------------ -->
        				<div class="table-responsive">
              				<table class="table table-striped table-bordered" id="TabelDetail">
                				<thead class="title_table">
                  				<tr>
	                    				<th><center>Nama Barang</center></th>
	                    				<th><center>Qty</center></th>
	                    				<th width="100">
	                    					<center>
							    				<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
													<i class="entypo-plus"></i>
												</button>
											</center>
										</th>
	              					</tr>
            					</thead>
            					<tbody>
				                  	<?php $no=1; ?>
								  	<tr id="baris<?php echo $no; ?>">
					                    <td>
										<input type="text" class="form-control-new" size="100"  name="nmbrg[]" id="nmbrg<?php echo $no;?>" placeholder="Nama Barang">       
										</td>
					                    <td>
										<input style="text-align: right;" id="qty<?php echo $no;?>" name="qty[]" class="form-control-new" type="text">
					                    </td>
										<td align="center">
						                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
												<i class="entypo-trash"></i>
											</button>
									    </td>
				                  	</tr>
			                  	</tbody>
			              	</table>
        				</div>
    					<h6>Notes &amp; Information:</h6>
			  			Budget Marketing Secret Garden Village
					</form>
	            </div>
	            <div class="modal-footer"> 
	            	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        			<button id="btnSave" type="button" onclick="save('<?php echo base_url(); ?>')" class="btn btn-success">Save</button>
	            </div>
	        </div>
	    </div>
	</div>

<script type="text/javascript">
		var save_method;
		
        function form_tambah_pr_marketing(){
		    save_method = 'add';
			id_='0';
			$('#form')[0].reset(); // reset form on modals
			$('.form-group').removeClass('has-error'); // clear error class
			$('.help-block').empty(); // clear error string
			$('#TabelDetail').attr('hidden',false);
			$('#edit_only').attr('hidden',true);
			$('#btnSave').attr('disabled',false);
			$('#default_modal').modal('show'); // show modal	
		}
		
		
		function save(url)
		{
			
			// if($('#noproposal').val() == ''){

			// 	alert('Wajib Isi Proposal!!!');

			// 	return false;
			// }
			
			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/pr_marketing/save_pr/";
			} else {
				url_ = url+"index.php/transaksi/pr_marketing/edit_pr_/";
			}
			
		    $('#btnSave').text('confirm...'); //change button text 
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";
			
			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Purchase Request Berhasil Disimpan.");
							location.reload();
						
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
				
		}
		
		
		function cari_proposal(url)
		{
			var pro=$("#v_keyword_proposal").val();
			$.ajax({
					url: url+"index.php/transaksi/pr_marketing/ajax_proposal/",
					data: {id:pro},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#noproposal').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
				}); 
			
			   	
   		}
   		
   		function edit_pr(nodok,url){
   			console.log('test', nodok);
			save_method = 'edit';
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/pr_marketing/edit_pr/"+nodok+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
					
							$.ajax({
								url : url+"index.php/transaksi/pr_marketing/edit_pr_detail/"+nodok+"/",
								type: "GET",
								dataType: "html",
								success: function(res)
								{
								  $('#edit_only2').html(res);
									
								},
								error: function(e) 
								{
									alert(e);
								}
							}); 
							
			            $('#nopr').val(data.NoDokumen);
						$('#noproposal').val(data.NoProposal);
						$('#v_tgl_dokumen').val(data.TglDokumen);
						$('#v_tgl_dokumen_terima').val(data.TglTerima);
						$('#divisi').val(data.KdDivisi);
						$('#keterangan').val(data.Keterangan);
						$('#v_status').val(data.Status);
						$('#TabelDetail').attr('hidden',false);
						$('#nmbrg1').val("");
						$('#qty1').val("");
						$('#edit_only').attr('hidden',false);
						$('#btnSave').attr('disabled',false);
			            $('#default_modal').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}
		
		function view_pr(nodok,url){
   			
			save_method = 'edit';
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/pr_marketing/edit_pr/"+nodok+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
					
						$.ajax({
								url : url+"index.php/transaksi/pr_marketing/edit_pr_detail/"+nodok+"/",
								type: "GET",
								dataType: "html",
								success: function(res)
								{
								  $('#edit_only2').html(res);
									
								},
								error: function(e) 
								{
									alert(e);
								}
							}); 
							
			            $('#nopr').val(data.NoDokumen);
						$('#noproposal').val(data.NoProposal);
						$('#v_tgl_dokumen').val(data.TglDokumen);
						$('#v_tgl_dokumen_terima').val(data.TglTerima);
						$('#divisi').val(data.KdDivisi);
						$('#keterangan').val(data.Keterangan);
						$('#v_status').val(data.Status);
						$('#TabelDetail').attr('hidden',true);
						$('#btnSave').attr('disabled',true);
			            $('#default_modal').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}
							
		function delete_pr_marketing(id,url)
		{
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url+"index.php/transaksi/pr_marketing/delete_pr_marketing/",
					data: {id:id},
					type: "POST",
					dataType: "html",
					success: function(data)
					{
						    $('#default_modal').modal('hide');
							alert("Purchase Return Berhasil Dihapus.");
							location.reload();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});

			}
		}
		
		function detailNew()
		{
			var clonedRow = $("#TabelDetail tr:last").clone(true);
			var intCurrentRowId = parseFloat($('#TabelDetail tr').length )-2;
			nama = document.getElementsByName("nmbrg[]");
			temp = nama[intCurrentRowId].id;
			intCurrentRowId = temp.substr(5,temp.length-5);
			var intNewRowId = parseFloat(intCurrentRowId) + 1;
			$("#nmbrg" + intCurrentRowId , clonedRow ).attr( { "id" : "nmbrg" + intNewRowId,"value" : ""} );
			$("#qty" + intCurrentRowId , clonedRow ).attr( { "id" : "qty" + intNewRowId,"value" : 0} );
			$("#btn_del_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_" + intNewRowId} );
			$("#TabelDetail").append(clonedRow);
			$("#TabelDetail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
			$("#nmbrg" + intNewRowId).focus();
			ClearBaris(intNewRowId);
		}
		
		function ClearBaris(id)
		{
			$("#nmbrg"+id).val("");
			$("#qty"+id).val("");
		}
		
		function deleteRow(obj)
		{
			objek = obj.id;
			id = objek.substr(15,objek.length-3);
			
			var lastRow = document.getElementsByName("nmbrg[]").length;
			
			if( lastRow > 1)
			{
				$('#baris'+id).remove();
			}else{
					alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
			}
		}
		
		function delete_detail(id,brg)
		{
			//alert(id+" - "+brg);
			var url_=$('#base_url').val();
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url_+"index.php/transaksi/pr_marketing/delete_detail/",
					data: {nodok:id,barang:brg},
					type: "POST",
					dataType: "JSON",
					success: function(data)
					{
						//if success reload ajax table
						$('#default_modal').modal('hide');
					    location.reload();
						//reload_table();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});
			}
		}
		
		
		function PopUpPrint(nodok, baseurl)
		{
		    url = "index.php/transaksi/pr_marketing/vewPrint/" + escape(nodok);
		    window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
		}
		
		function PopUpPrintPDF(nodok, baseurl)
		{
		    url = "index.php/transaksi/pr_marketing/create_pdf/" + escape(nodok);
		    window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
		}
		

</script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>