<?php
    $this->load->view('header');
    $session_name = $this->session->userdata('username');
?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/font-awesome/css/font-awesome.min.css">
<script src="<?php echo base_url(); ?>public/js/shortcuts_v1.js" type="text/javascript"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/grup_harga.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/transaksi_sales.js"></script>
<!--<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>-->
<script src="<?= base_url(); ?>public/js/incrementing.js"></script>
<!--<script language="javascript" src="<?= base_url(); ?>public/js/jquery.js"></script>-->

<style type="text/css">
    .huruf {
        font-family: Arial; /* font name */
        font-size: 12px; /* font size */
    }

    .cetak {
        font-family: Courier New; /* font name */
        font-size: 10px; /* font size */
    }

    .tgl {
        font-family: Verdana; /* font name */
        font-size: 13px; /* font size */
        font-weight: bold;
        color: #ffffff;
    }

    .clock {
        font-family: Verdana; /* font name */
        font-size: 20px; /* font size */
        font-weight: bold;
        color: #000;
        border-color: #ffffff;
		background-color: #f5f5f6;
    }
</style>
<script>
$( document ).ready(function() {
    document.getElementById('id_tunai').disabled=true;
    document.getElementById('id_kredit').disabled=true;
    document.getElementById('id_debet').disabled=true;
    document.getElementById('id_voucher').disabled=true;
    document.getElementById('cash_bayar').disabled=true;
    document.getElementById('kredit_bayar').disabled=true;
    document.getElementById('debet_bayar').disabled=true;
    document.getElementById('voucher_bayar').disabled=true;
});
</script>
<body onLoad="start();">
<?php
// $tgl = date('d-m-Y');
// $jam = date('H:i:s');
// if (empty($struk)) {
    // $no = 1;
// } else {
    // $no = $struk;
// }
?>
<div class="table-responsive">
<table width="85%" align="center" border="0" cellpadding="0" cellspacing="0" class="table table-bordered responsive">
<form name="form1" id="form1" method="post" action="<?= base_url(); ?>index.php/transaksi/ticketing/insert_trans" onsubmit='cek_form();'>
<tr>
<td width="70%" valign="top" height="20%">
    
        <table width='100%' align="center" class='table table-bordered responsive' name="detail">
            <thead>
            <tr>
                <th width="30%">Category</th>
                <th width="20%">Qty</th>
                <th width="15%">Harga</th>
                <th width="15%">Netto</th>
            </tr>
            </thead>
			<?php
				for($i=0;$i<count($list);$i++){
			?>
            <tr>
                <td>
					<div style="font-size:20px;"><?=$list[$i]['NamaLengkap'];?></div>
					<input type="hidden" name="NamaLengkap<?=$i;?>" id="NamaLengkap<?=$i;?>" value="<?=$list[$i]['NamaLengkap'];?>">
					<input type="hidden" name="PCode<?=$i;?>" id="PCode<?=$i;?>" value="<?=$list[$i]['PCode'];?>">
				</td>
                <td>
					<div class="input-spinner" style="float: left;">
						<button type="button" class="btn btn-danger" style="float: left;">-</button>
							<input type="text" class="form-control size-1" style="width:55px; float: left; font-size: 15px;" name="qty1<?=$i;?>" id="qty1<?=$i;?>" value="0" onKeyUp="getTotal('<?=$i;?>');" onKeyDown="MoveNext(event, '<?=$i;?>', '<?= base_url(); ?>')"/>
						<button type="button" class="btn btn-green" style="float: left;">+</button>
					</div>&nbsp;
					<button type="button" class="btn btn-gold" onClick="resetQty('<?=$i;?>');"><i class="fa fa-refresh"></i></button>
                </td>
                <td>
                    <input type="text" class="form-control" style="font-size: 16px;text-align: right;" readonly id="jualm1<?=$i;?>" name="jualm1<?=$i;?>" size="12" value="<?=$list[$i]['Harga1c'];?>" class="InputAlignRight">
                </td>
                <td>
                    <input type="text" class="form-control" style="font-size: 16px;text-align: right;" readonly id="netto1<?=$i;?>" name="netto1<?=$i;?>" size="12" value="0" class="InputAlignRight">
                </td>
            </tr>
			<input type="hidden" name="nmticket<?=$i;?>" id="nmticket<?=$i;?>" value="<?=$list[$i]['PCode'];?>">
			<input type="hidden" name="jenis<?=$i;?>" id="jenis<?=$i;?>" value="<?=$list[$i]['KdSubDivisi'];?>">
			<input type="hidden" name="tipe<?=$i;?>" id="tipe<?=$i;?>" value="<?=$list[$i]['Tipe'];?>">
			<input type="hidden" name="charge<?=$i;?>" id="charge<?=$i;?>" value="<?=$list[$i]['Service_charge'];?>">
			<input type="hidden" name="nominalVouch<?=$i;?>" id="nominalVouch<?=$i;?>" value="<?=$list[$i]['Harga1c'];?>">
				<?php } ?>
			<input type="hidden" name="jml" id="jml" value="<?=count($list);?>">
            <input type="hidden" name="kassa" id="kassa" value="<?= $NoKassa ?>">
            <input type="hidden" name="kasir" id="kasir" value="<?= $session_name ?>">
            <!--<input type="hidden" name="no" id="no" value="<?= $no ?>">-->
            <input type="hidden" name="store" id="store" value="<?= $store[0]['KdCabang'] ?>">
            <input type="hidden" name="brgDetail" id="brgDetail" value="">
        </table>
</td>
<td rowspan="2" valign="top" width="30%">
        <fieldset>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="clock">
                        <div id="jam" align="center">
                            <script language="javascript">
                                jam();
                            </script>
                        </div>
                    </div>
                </div>
				<div class="panel-heading">
                    <div class="panel-body">
						<div style="text-align:center;font-size:20px;">Total Ticket Hari Ini :</div>
						<div style="text-align:center;font-size:20px;">
						<?php if(strlen($ttlticket)=="1"){
							echo "0".$ttlticket;
						}else{
							echo $ttlticket;
						}
						?>
						</div>
					</div>
                </div>
                <div class="panel-body">
                    <br>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">No Transaksi</label>
                        <div class="col-sm-8">
                            <input type='text' id='NoTrans' name='NoTrans' style="font-size: 16px;text-align: center;" class="form-control input-sm" value="<?= $NoTrans ?>">
                        </div>
                    </div>
					<br>
					<div class="form-group">
                        <label class="col-sm-4 control-label">No Identitas</label>
                        <div class="col-sm-8">
                            <input type='text' id='NoIdentitas' name='NoIdentitas' style="font-size: 50px;text-align: center;height:100px;" class="form-control input-sm" onKeyDown="MoveNext(event, 'identitas', '<?= base_url(); ?>')" maxlength="4" value="">
                        </div>
                    </div>
                </div>
            </div>
            <table class="table responsive">
                <tbody>
                <tr class="warning">
                    <td width="5%">&nbsp;</td>
                    <td class="left">Item in cart :</td>
                    <td class="left">
                        <div id="TotalItem"></div>
                        <input type='hidden' id='TotalItem2' name='TotalItem2'  value="">
                        <input type='hidden' id='dpp' name='dpp'  value="">
                        <input type='hidden' id='tax' name='tax'  value="">
                    </td>
                </tr>
                <tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h5>Netto : </h5></td>
                    <td class="left"><h5><div id="TotalNetto"></div> </h5></td>
					<input type='hidden' id='TotalNettoHidde' name='TotalNettoHidde'  value="0">
                </tr>

                <tr class="success">
                        <td width="5%">&nbsp;</td>
                        <td class="left"><h5>Service Charge : </h5></td>
                        <td class="left"><h5><div id="SerCharge"></div> </h5></td>
						<input type='hidden' id='SerCharge2' name='SerCharge2'  value="0">
                </tr>

                <tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h4>Total : </h4></td>
                    <td class="left"><h4><div id="ttlall"></div></h4></td>
					<input type='hidden' id='ttlall2' name='ttlall2'  value="0">
                </tr>

                </tbody>
            </table>

            <div class="panel panel-primary">
                <div class="panel-heading" align="center">
                    Jenis Bayar
                </div>
                <div class="panel-body">
                    <table align="center" class="table-responsive" border="0" width="95%">
                        <tr>
                            <td>
                                <input type='hidden' id='confirm_struk' name='confirm_struk' size='10' value="">
                                <input type='hidden' id='confirm_kassa' name='confirm_kassa' size='10' value="<?= $NoKassa ?>">
                                <input type='hidden' id='service_charge' name='service_charge' size='10'
                                       value="<?=$Charge ?>">
                                <input type='hidden' id='total_biaya' name='total_biaya' size='10'
                                       class="InputAlignRight" value="<?=$TotalNetto ?>">
                            </td>
                        </tr>
                        <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo3("Currency", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
                        <tr>
                            <td nowrap>
								<input type="radio" id="pilihan" name="pilihan" value="cash" onClick="oncheked()" >
								Tunai
                            </td>
                            <td nowrap align="right"><input type='text' id='id_tunai' name='id_tunai' size='10'
                                       onKeyDown="TunaiCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"></td>
                            <td nowrap><input type='text' id='cash_bayar' name='cash_bayar' size='10'
                                              onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td>
								<input type="radio" id="pilihan" name="pilihan" value="kredit" onClick="oncheked()">
								Kredit
                            </td>
                            <td><input type='text' id='id_kredit' name='id_kredit' size='10'
                                       onKeyDown="KreditCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"></td>
                            <td><input type='text' id='kredit_bayar' name='kredit_bayar' size='10'
                                       onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input type="radio" id="pilihan" name="pilihan" value="debet" onClick="oncheked()">
                                Debit
                            </td>
                            <td><input type='text' id='id_debet' name='id_debet' size='10'
                                       onKeyDown="DebetCustomer(event, '1', '<?= base_url(); ?>')" class="form-control">
                            </td>
                            <td><input type='text' id='debet_bayar' name='debet_bayar' size='10' onKeyUp="SetKembali()"
                                       class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input type="radio" id="pilihan" name="pilihan" value="voucher" onClick="oncheked()">
                                Voucher
                            </td>
                            <td><input type='text' id='id_voucher' name='id_voucher' size='10'
                                       onKeyDown="VoucherCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"></td>
                            <td><input type='text' id='voucher_bayar' name='voucher_bayar' size='10'
                                       onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                    </table>

                </div>
            </div>
			
            <table class="table responsive">
                <tbody>
                <tr class="info">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h3>Total Bayar </h3></td>
                    <td class="left">
                        <h3><span id='total_bayar' name='total_bayar' onKeyUp="SetKembali()"></span></h3>
                        <input type='hidden' id='total_bayar_hide' name='total_bayar_hide' size="10">
                    </td>
                </tr>
                <tr class="info">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h3>Kembali </h3></td>
                    <td class="left"><h3><span id='cash_kembali' name='cash_kembali'> </span></h3>
					<input type='hidden' id='cash_kembali_hide' name='cash_kembali_hide' size="10">
					</td>
                </tr>
                </tbody>
            </table>

            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="table responsive">
                <tr>
                    <td align="center" height="40"><input type='button' class="btn btn-success" value='Complete'
                                                          onClick="cek_form()">
                        <input type='reset' value='Clear' class="btn btn-danger" onClick="refresh();">
                    </td>
                </tr>
            </table>
        </fieldset>
	</div>
</td>
</tr>
</form>
<tr>
    <td valign="top">
        
    </td>
</tr>
<tr>
    <td colspan="2">
    <fieldset>
        <table width="100%" border="0" align="center" class="huruf">
            <tr>
                <td width="5%">&nbsp;</td>
                <td width="10%"><b>Kassa : </b><?= $NoKassa ?></td>
                <td width="5%">&nbsp;</td>
                <td width="15%"><b>Kasir : </b><?= $session_name ?></td>
                <td width="5%">&nbsp;</td>
                <!--<td width="10%"><b>No Struk : </b><?= $no ?></td> -->
                <td width="50%">&nbsp;</td>
            </tr>
        </table>
    </fieldset>
    </td>
</tr>
</table>
</div>

</body>

<?php
$this->load->view('footer');
?>

<link rel="stylesheet" href="<?= base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/select2/select2.css">

<script src="<?= base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?= base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2/select2.min.js"></script>
<script>
	function MoveNext(e, row, url){
		if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {
			var identitas = document.getElementById('NoIdentitas').value;
			var check = document.getElementById('pilihan').value;
			if(row=="identitas"){
				if(identitas!=""){
					if(check=="cash"){
						document.getElementById("pilihan").checked = true;
						document.getElementById('cash_bayar').disabled=false;
						document.getElementById('cash_bayar').focus();
					}
				}else{
					alert("Nomor identitas masih kosong");
				}
			}else{
				document.getElementById('NoIdentitas').focus();
			}
		}
	}
    function EditRecord(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {

            NoUrut = document.getElementById('NoUrut').value * 1;
            noLast = document.getElementById('noLast').value;
            var nil;
            nil = NoUrut * 1;
            if (isNaN(nil)) {
                alert("Format No Urut salah, Harap masukkan angka");
            }
            else if (nil >= noLast) {
                alert("Data tidak ditemukan, data terakhir adalah " + (noLast - 1));
            }
            else {

                kode = document.getElementById('pc' + NoUrut).value;


                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/penjualan/EditRecord/' + kode + '/',
                    success: function (msg) {

                        var jsdata = msg;
                        eval(jsdata);

                        document.getElementById('kdbrg1').value = datajson[0].KodeBarang;
                        document.getElementById('nmbrg1').value = datajson[0].NamaStruk;
                        document.getElementById('qty1').value = datajson[0].Qty;
                        document.getElementById('jualm1').value = number_format(datajson[0].Harga, 0, ',', '.');
                        document.getElementById('netto1').value = number_format(datajson[0].Netto, 0, ',', '.');
                        document.getElementById('jualmtanpaformat1').value = datajson[0].Harga;
                        document.getElementById('nettotanpaformat1').value = datajson[0].Netto;

                        document.getElementById('NoUrut').readOnly = true;
                        document.getElementById('kdbrg1').readOnly = true;
                        document.getElementById('qty1').readOnly = false;
                        document.getElementById('qty1').focus();
                        document.getElementById('EditFlg').value = 1;
                    }
                });
            }
        }
    }

    function CustomerView(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {
            pelanggan = document.getElementById('pelanggan').value;
            if (pelanggan !== "") {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/penjualan/CustomerView/' + pelanggan + '/',
                    success: function (msg) {
                        var jsdata = msg;
                        eval(jsdata);
                        document.getElementById('id_customer').value = datajson[0].KdCustomer;
                        document.getElementById('nama_customer').value = datajson[0].Nama;
                        document.getElementById('gender_customer').value = datajson[0].Gender;
                        document.getElementById('tgl_customer').value = datajson[0].TglLahir;
                        document.getElementById('cash_bayar').focus();
                    }
                });
            }
            else {
                alert("masukkan nomor Id Customer");
            }
        }
    }

    function VoucherCustomer(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }

        if (code == 13) {
            id_voucher = document.getElementById('id_voucher').value;
            if (id_voucher !== "") {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/penjualan/VoucherCustomer/' + id_voucher + '/',
                    success: function (msg) {
                        var jsdata = msg;
                        eval(jsdata);
                        document.getElementById('id_voucher').value = datajson[0].KdVoucher;
                        document.getElementById('voucher_bayar').value = datajson[0].Nominal;
                        document.getElementById('voucher_bayar').disabled = false;

                        SetKembali()
                    }
                });
            }
            else {
                alert("masukkan nomor voucher");
            }
        }

    }

    function cek_form() {
        total_bayar = document.getElementById('total_bayar_hide').value;
        identitas = document.getElementById('NoIdentitas').value;
		if(identitas!=""){
			if (total_bayar == "") {
				alert("Transaksi belum dibayar");
				document.getElementById('cash_bayar').focus();
			}
			else {
				if (total_biaya > total_bayar) {
					alert("Total bayar masih kurang");
					document.getElementById('cash_bayar').focus();
				}
				else {
					//no = document.getElementById('no').value;
					document.form1.submit();
				}
			}
		}else{
			alert("Nomor identitas masih kosong");
			document.getElementById('NoIdentitas').focus();
		}
    }

</script>
