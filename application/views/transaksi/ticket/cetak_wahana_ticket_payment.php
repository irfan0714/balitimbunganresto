<?php
$this->load->helper('print_helper');
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

$ftext = '';
$alamatPT = $store[0]['Alamat1PT'];
$tgl = $expDate;
$tgl_1 = explode("-", $tgl);
$tgl_tampil = $tgl_1[2] . "/" . $tgl_1[1] . "/" . $tgl_1[0];

        $q2 = "SELECT * FROM ticket_head a WHERE a.`notrans`='$notrans';";
		$qry2 = mysql_query($q2);
		$hsl2 = mysql_fetch_object($qry2);
		
for($i=0;$i<2;$i++){
	printer_dos($ftext, $reset . $elite);
	printer_dos($ftext, $dwidth . str_pad($store[0]['NamaPT'], 39, " ", STR_PAD_BOTH) . $ndwidth . "\r\n");
	printer_dos($ftext, str_pad("NPWP : " . $store[0]['NPWP'], 39, " ", STR_PAD_BOTH) . "\r\n");
	printer_dos($ftext, str_pad($store[0]['Alamat1PT'], 39, " ", STR_PAD_BOTH) . "\r\n");
	printer_dos($ftext, str_pad($store[0]['Alamat2PT'], 39, " ", STR_PAD_BOTH) . "\r\n");

	printer_dos($ftext, "\r\n");
	printer_dos($ftext, str_pad("Tanggal" . " : " . $tgl_tampil, 39, " ", STR_PAD_BOTH) . "\r\n");
	printer_dos($ftext, "Struk : 92-" . $hsl2->notrans . " / " . $hsl2->user . " - " . $hsl2->noidentitas . "\r\n");

	printer_dos($ftext, "========================================\r\n");
	
	printer_dos($ftext, str_pad(substr("TIKET SWING SGV", 0, 20), 20) .
				str_pad($hsl2->qty, 5, " ", STR_PAD_LEFT) .
				str_pad(round($hsl2->ttl_amount/$hsl2->qty), 7, " ", STR_PAD_LEFT) .
				str_pad(round($hsl2->ttl_amount), 8, " ", STR_PAD_LEFT) . "\r\n");
	printer_dos($ftext, "----------------------------------------\r\n");
	printer_dos($ftext, str_pad("Sub Total " . $hsl2->qty . " item", 24) . ":" . str_pad(number_format($hsl2->ttl_amount, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
	
	if ($hsl2->nilai_tunai <> 0)
	    printer_dos($ftext, str_pad("Tunai ", 24) . ":" . str_pad(number_format($hsl2->nilai_tunai, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
	if ($hsl2->nilai_kredit <> 0)
	    printer_dos($ftext, str_pad("Kartu Kredit ", 24) . ":" . str_pad(number_format($hsl2->nilai_kredit, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
	if ($hsl2->nilai_debit <> 0)
	    printer_dos($ftext, str_pad("Kartu Debit ", 24) . ":" . str_pad(number_format($hsl2->nilai_debit, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
	
	printer_dos($ftext, "----------------------------------------\r\n");
	printer_dos($ftext, str_pad("Total Bayar ", 24) . ":" . str_pad(number_format($hsl2->ttl_bayar, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
	printer_dos($ftext, str_pad("Kembali ", 24) . ":" . str_pad(number_format($hsl2->ttl_kembali, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . $ndwidth . "\r\n");
	
	printer_dos($ftext, "========================================\r\n");
	printer_dos($ftext, "             ===Terima kasih===          \r\n");
	printer_dos($ftext, "\r\n");
	printer_dos($ftext, str_pad("Barang yang sudah dibeli tidak", 35, " ", STR_PAD_BOTH) . "\r\n");
	printer_dos($ftext, str_pad("dapat dikembalikan.", 35, " ", STR_PAD_BOTH) . "\r\n");
	printer_dos($ftext, str_pad("Untuk barang kena pajak harga sudah ", 35, " ", STR_PAD_BOTH) . "\r\n");
	
	printer_dos($ftext, "\r\n");
	printer_dos($ftext, "\r\n");
	printer_dos($ftext, $fcut);
}
printer_dos($ftext, $op_cash);
?>