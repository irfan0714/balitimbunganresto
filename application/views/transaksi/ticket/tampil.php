<?php
    $this->load->view('header');
    $session_name = $this->session->userdata('username');
?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/font-awesome/css/font-awesome.min.css">
<script src="<?php echo base_url(); ?>public/js/shortcuts_v1.js" type="text/javascript"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/grup_harga.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/transaksi_sales.js"></script>
<!--<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>-->
<script src="<?= base_url(); ?>public/js/incrementing.js"></script>
<!--<script language="javascript" src="<?= base_url(); ?>public/js/jquery.js"></script>-->

<style type="text/css">
    .huruf {
        font-family: Arial; /* font name */
        font-size: 12px; /* font size */
    }

    .cetak {
        font-family: Courier New; /* font name */
        font-size: 10px; /* font size */
    }

    .tgl {
        font-family: Verdana; /* font name */
        font-size: 13px; /* font size */
        font-weight: bold;
        color: #ffffff;
    }

    .clock {
        font-family: Verdana; /* font name */
        font-size: 20px; /* font size */
        font-weight: bold;
        color: #000;
        border-color: #ffffff;
		background-color: #f5f5f6;
    }
</style>
<script>
$( document ).ready(function() {
    document.getElementById('id_tunai').disabled=true;
    document.getElementById('id_kredit').disabled=true;
    document.getElementById('id_kredit_edc').disabled=true;
    document.getElementById('id_debet').disabled=true;
    document.getElementById('id_debet_edc').disabled=true;
    document.getElementById('id_voucher').disabled=true;
    //document.getElementById('id_member').disabled=true;
    document.getElementById('cash_bayar').disabled=true;
    document.getElementById('kredit_bayar').disabled=true;
    document.getElementById('debet_bayar').disabled=true;
    document.getElementById('voucher_bayar').disabled=true;
    document.getElementById('City').disabled=true;
    
    //document.getElementById("tb_jenis_ticket").style.display = "none";
    ip = document.getElementById('ip').value;
    //if(ip=='192.168.10.61x'){
    if(ip=='192.168.10.227'){
		document.getElementById("div_travel").style.display = "none";
    	document.getElementById("div_tour_guide").style.display = "none";
    	document.getElementById("div_petugas_stiker").style.display = "none";
    	document.getElementById("div_nama_customer").style.display = "none";
    	document.getElementById("div_nama_kota").style.display = "none";
	}

    jennis = document.getElementById('tampils_jenis').value;
    if(jennis=='7'){
        document.getElementById("tampils").style.display = "none";
    }else{
        document.getElementById("tampils").style.display = "A";
    }
    
});

function on_cheked()
{
	if(document.form1.pilihan[0].checked == true){
		
		document.getElementById('id_member').value = "";
		
		document.getElementById('cash_bayar').disabled=false;
		document.getElementById('cash_bayar').focus();
		
		if(document.getElementById('id_kredit').value == "")
		{ document.getElementById('id_kredit').disabled=true;}
		else { document.getElementById('id_kredit').disabled=false;}
		
		if(document.getElementById('id_debet').value == "")
		{ document.getElementById('id_debet').disabled=true;}
		else{document.getElementById('id_debet').disabled=false;}
		
		if(document.getElementById('id_voucher').value == "")
		{ document.getElementById('id_voucher').disabled=true;}
		else{document.getElementById('id_voucher').disabled=false;}
		
		if(document.getElementById('kredit_bayar').value == "")
		{document.getElementById('kredit_bayar').disabled = true;}
		else{document.getElementById('kredit_bayar').disabled = false;}
		
		if(document.getElementById('debet_bayar').value == "")
		{document.getElementById('debet_bayar').disabled = true;}
		else{document.getElementById('debet_bayar').disabled = false;}
		
		if(document.getElementById('voucher_bayar').value == "")
		{document.getElementById('voucher_bayar').disabled = true;}
		else{document.getElementById('voucher_bayar').disabled = false;}
		
		if(document.getElementById('id_tunai').value == "")
		{document.getElementById('id_tunai').disabled = true;}
		else{document.getElementById('id_tunai').disabled = false;}
		
		if(document.getElementById('id_member').value == "")
		{document.getElementById('id_member').disabled = true;}
		else{document.getElementById('id_member').disabled = false;}
		
		$("#ket_free_ticket").css("display","none");
		$("#checklist").css("display","none");
}
else if(document.form1.pilihan[1].checked == true){
	
	    document.getElementById('id_member').value = "";
	
		if(document.getElementById('cash_bayar').value == "")
		{ document.getElementById('cash_bayar').disabled=true;}
		else { document.getElementById('cash_bayar').disabled=false;}
		
		document.getElementById('kredit_bayar').disabled=false;
		document.getElementById('kredit_bayar').focus();
		
		if(document.getElementById('id_tunai').value == "")
		{ document.getElementById('id_tunai').disabled=true;}
		else{document.getElementById('id_tunai').disabled=false;}
		
		if(document.getElementById('id_kredit').value == "")
        { 
            document.getElementById('id_kredit').disabled=false;
            document.getElementById('id_kredit_edc').disabled=false;
        }
        else{
             document.getElementById('id_kredit').disabled=false;
             document.getElementById('id_kredit_edc').disabled=false;
        }
	
		if(document.getElementById('id_debet').value == "")
        { 
            document.getElementById('id_debet').disabled=true;
            document.getElementById('id_debet_edc').disabled=true;
        }
        else{
            document.getElementById('id_debet').disabled=false;
            document.getElementById('id_debet_edc').disabled=false;
        }
		
		if(document.getElementById('id_voucher').value == "")
		{ document.getElementById('id_voucher').disabled=true;}
		else{document.getElementById('id_voucher').disabled=false;}
		
		if(document.getElementById('debet_bayar').value == "")
		{document.getElementById('debet_bayar').disabled = true;}
		else{document.getElementById('debet_bayar').disabled = false;}
		
		if(document.getElementById('voucher_bayar').value == "")
		{document.getElementById('voucher_bayar').disabled = true;}
		else{document.getElementById('voucher_bayar').disabled = false;}
		
		if(document.getElementById('id_member').value == "")
		{document.getElementById('id_member').disabled = true;}
		else{document.getElementById('id_member').disabled = false;}
		
		$("#ket_free_ticket").css("display","none");
		$("#checklist").css("display","none");
}
else if(document.form1.pilihan[2].checked == true){
	    
	    document.getElementById('id_member').value = "";
	
		if(document.getElementById('cash_bayar').value == "")
		{ document.getElementById('cash_bayar').disabled=true;}
		else { document.getElementById('cash_bayar').disabled=false;}
		
		document.getElementById('debet_bayar').disabled=false;
		document.getElementById('debet_bayar').focus();
		
		if(document.getElementById('id_tunai').value == "")
		{ document.getElementById('id_tunai').disabled=true;}
		else{document.getElementById('id_tunai').disabled=false;}
		
		if(document.getElementById('id_kredit').value == "")
        { 
            document.getElementById('id_kredit').disabled=true;
            document.getElementById('id_kredit_edc').disabled=true;
        }
        else{
            document.getElementById('id_kredit').disabled=false;
            document.getElementById('id_kredit_edc').disabled=false;
        }
	
		if(document.getElementById('id_debet').value == "")
		{ 
            document.getElementById('id_debet').disabled=false;
            document.getElementById('id_debet_edc').disabled=false;
        }
		else{
            document.getElementById('id_debet').disabled=false;
            document.getElementById('id_debet_edc').disabled=false;
        }
		
		if(document.getElementById('id_voucher').value == "")
		{ document.getElementById('id_voucher').disabled=true;}
		else{document.getElementById('id_voucher').disabled=false;}
		
		if(document.getElementById('kredit_bayar').value == "")
		{document.getElementById('kredit_bayar').disabled = true;}
		else{document.getElementById('kredit_bayar').disabled = false;}
		
		if(document.getElementById('voucher_bayar').value == "")
		{document.getElementById('voucher_bayar').disabled = true;}
		else{document.getElementById('voucher_bayar').disabled = false;}
		
		if(document.getElementById('id_member').value == "")
		{document.getElementById('id_member').disabled = true;}
		else{document.getElementById('id_member').disabled = false;}
		
		$("#ket_free_ticket").css("display","none");
		$("#checklist").css("display","none");	
	}
else if(document.form1.pilihan[3].checked == true){
		
		document.getElementById('id_member').value = "";
		
		if(document.getElementById('cash_bayar').value == "")
		{ document.getElementById('cash_bayar').disabled=true;}
		else { document.getElementById('cash_bayar').disabled=false;}
		
		if(document.getElementById('id_tunai').value == "")
		{ document.getElementById('id_tunai').disabled=true;}
		else{document.getElementById('id_tunai').disabled=false;}
		
		if(document.getElementById('id_kredit').value == "")
		{ document.getElementById('id_kredit').disabled=true;}
		else { document.getElementById('id_kredit').disabled=false;}
		
		if(document.getElementById('id_debet').value == "")
		{ document.getElementById('id_debet').disabled=true;}
		else{document.getElementById('id_debet').disabled=false;}
		
		document.getElementById('voucher_bayar').disabled=false;
		document.getElementById('voucher_bayar').focus();
		
		if(document.getElementById('kredit_bayar').value == "")
		{document.getElementById('kredit_bayar').disabled = true;}
		else{document.getElementById('kredit_bayar').disabled = false;}
		
		if(document.getElementById('debet_bayar').value == "")
		{document.getElementById('debet_bayar').disabled = true;}
		else{document.getElementById('debet_bayar').disabled = false;}

		if(document.getElementById('id_voucher').value == "")
		{document.getElementById('id_voucher').disabled = true;}
		else{document.getElementById('id_voucher').disabled = false;}
		
		if(document.getElementById('id_member').value == "")
		{document.getElementById('id_member').disabled = true;}
		else{document.getElementById('id_member').disabled = false;}
		
		$("#ket_free_ticket").css("display","none");
		$("#checklist").css("display","none");			
	}
else if(document.form1.pilihan[4].checked == true){
		document.getElementById('id_member').value = "";
		
		if(document.getElementById('cash_bayar').value == "")
		{ document.getElementById('cash_bayar').disabled=true;}
		else { document.getElementById('cash_bayar').disabled=false;}
		
		if(document.getElementById('id_tunai').value == "")
		{ document.getElementById('id_tunai').disabled=true;}
		else{document.getElementById('id_tunai').disabled=false;}
		
		if(document.getElementById('id_kredit').value == "")
		{ document.getElementById('id_kredit').disabled=true;}
		else { document.getElementById('id_kredit').disabled=false;}
		
		if(document.getElementById('id_debet').value == "")
		{ document.getElementById('id_debet').disabled=true;}
		else{document.getElementById('id_debet').disabled=false;}
		
		if(document.getElementById('kredit_bayar').value == "")
		{document.getElementById('kredit_bayar').disabled = true;}
		else{document.getElementById('kredit_bayar').disabled = false;}
		
		if(document.getElementById('debet_bayar').value == "")
		{document.getElementById('debet_bayar').disabled = true;}
		else{document.getElementById('debet_bayar').disabled = false;}

		if(document.getElementById('id_voucher').value == "")
		{document.getElementById('id_voucher').disabled = true;}
		else{document.getElementById('id_voucher').disabled = false;}
		
		if(document.getElementById('id_member').value == "")
		{document.getElementById('id_member').disabled = true;}
		else{document.getElementById('id_member').disabled = false;}
		
		document.getElementById('id_member').disabled=false;
		document.getElementById('id_member').focus();	
		
		$("#ket_free_ticket").css("display","none");
		$("#checklist").css("display","none");	
	}	
}
</script>
<!--<body onLoad="start();">-->
<body>
<!--<div class="table-responsive">-->
<table width="85%" align="center" border="0" cellpadding="0" cellspacing="0" class="table table-bordered responsive">
    <?php if($search_jenis=='7'){?>
      <form name="form1" id="form1" method="post" action="<?= base_url(); ?>index.php/transaksi/pos/save_trans_feeding_fish" onsubmit="cek_form();">
    <?php }else{ ?>
      <form name="form1" id="form1" method="post" action="<?= base_url(); ?>index.php/transaksi/ticketing/insert_trans" onsubmit="cek_form();">
    <?php }?>

<input type="hidden" id="ip" value="<?=$ip;?>"/>
<input type="hidden" name="tiket_enty" value="<?=$tiket_entry;?>"/>
<tr>
<td width="63%" valign="top" height="20%">
				<!--<div class="col-sm-12">
                
				<div class="panel-heading">
				    
                    <div class="clock">
                        <div id="jam" align="center">
                        
                            <script language="javascript">
                                jam();
                            </script>
                        </div>
                    </div>
                </div>
				<div class="panel-heading">
                    <div class="panel-body">
						<div style="text-align:center;font-size:18px;">Total Ticket <?php echo $wahana;?> Hari Ini :</div>
						<div style="text-align:center;font-size:18px;">
						<?php if(strlen($ttlticket)=="1"){
							echo "0".$ttlticket;
						}else{
							echo $ttlticket;
						}
						?>
						</div>
					</div>
                </div>-->
                </div>
		          
                        <div class="col-sm-12">
							<select id='SubKatTicket' name='SubKatTicket' class="select2" data-allow-clear="true" data-placeholder="Pilih Jenis Ticket" style="margin-bottom:5px;" onChange="get_jenis_ticket('<?=$NoKassa;?>','<?=$session_name;?>','<?=$store[0]['KdCabang'];?>');">
								<option value=""></option>
								<optgroup label="">
								<?php foreach ($subkatticket AS $rowTicket){
									
									$selected = "";
				                    if ($search_jenis) {
				                        if ($rowTicket['idsubkategori'] == $search_jenis) {
				                            $selected = 'selected="selected"';
				                        }
				                    }
									
									?>								
									<option <?php echo $selected ?> value="<?=$rowTicket['idsubkategori'];?>"><?=$rowTicket['NamaSubkategori'];?></option>
								<?php } ?>
								</optgroup>
							</select>
                        </div>
                    
        <div id="tb_jenis_ticket">
        <table  width='100%' align="center" class='table table-bordered responsive' name="detail">
        <tr bgcolor="#41c0f8">
        			<td colspan="2"><h4><font color="white"><b>
                    Total Ticket <?php echo $wahana;?> Hari Ini : 
                    <?php if(strlen($ttlticket)=="1"){
							echo "0".$ttlticket;
						}else{
							echo $ttlticket;
						}
						?>
                    </b></font></h4></td>
                    <td colspan="2" align="right"><b>
                    	<input type='hidden' id='NoTrans' name='NoTrans' style="border:0;background:#41c0f8;font-size: 12px;text-align: right; " class="form-control input-sm" value="<?= $NoTrans ?>" readonly>
                    	<h4><font color="white"><b><?= $NoTrans ?></b></font></h4>
                    </b></td>
                </tr>
            <tr bgcolor="#ade4fc">
                    <td align="left" colspan="2"><h4><font color="#6f6c6f"><b>Total Ticket :&nbsp;<span id="TotalItem">0</span></b></font></h4></td>
                    <td align="right" colspan="2"><h4><font color="#6f6c6f"><b>Total Rp. :&nbsp;<span id="ttlall">0</span></b></font></h4></td>
					    <input type='hidden' id='ttlall2' name='ttlall2'  value="0">
                        <input type='hidden' id='TotalItem2' name='TotalItem2'  value="">
                        <input type='hidden' id='dpp' name='dpp'  value="">
                        <input type='hidden' id='tax' name='tax'  value="">
                </tr>
            <tr bgcolor="#d0d3d5" align="center">
                <td width="30%"><h4><font color="#6f6c6f"><b>Categories</b></font></h4></td>
                <td width="20%"><h4><font color="#6f6c6f"><b>Qty</b></font></h4></td>
                <td width="15%"><h4><font color="#6f6c6f"><b>Harga</b></font></h4></td>
                <td width="15%"><h4><font color="#6f6c6f"><b>Netto</b></font></h4></td>
            </tr>
			<?php
				for($i=0;$i<count($list);$i++){
			?>
            <tr>
                <td>
					<div style="font-size:18px;"><?=$list[$i]['NamaLengkap'];?></div>
					<input type="hidden" name="NamaLengkap<?=$i;?>" id="NamaLengkap<?=$i;?>" value="<?=$list[$i]['NamaLengkap'];?>">
				</td>
                <td>
					<div class="input-spinner" style="float: left;">
						<button type="button" class="btn btn-danger" style="float: left;">-</button>
							<input type="text" class="form-control size-1" style="width:55px; float: left; font-size: 15px;" name="qty1<?=$i;?>" id="qty1<?=$i;?>" value="0" onKeyUp="getTotal('<?=$i;?>');" onKeyDown="MoveNext(event, '<?=$i;?>', '<?= base_url(); ?>')"/>
						<button type="button" class="btn btn-green" style="float: left;">+</button>
					</div>&nbsp;
					<button type="button" class="btn btn-gold" onClick="resetQty('<?=$i;?>');"><i class="fa fa-refresh"></i></button>
                </td>
                <td>
                    <input readonly type="text" class="form-control" style="font-size: 16px;text-align: right;" id="jualm1<?=$i;?>" name="jualm1<?=$i;?>" size="12" value="<?=$list[$i]['Harga1c'];?>" class="InputAlignRight">
                </td>
                <td>
                    <input type="text" class="form-control" style="font-size: 16px;text-align: right;" readonly id="netto1<?=$i;?>" name="netto1<?=$i;?>" size="12" value="0" class="InputAlignRight">
                </td>
            </tr>
			<input type="hidden" name="nmticket<?=$i;?>" id="nmticket<?=$i;?>" value="<?=$list[$i]['PCode'];?>">
			
			<input type="hidden" name="jenis<?=$i;?>" id="jenis<?=$i;?>" value="1">
			<input type="hidden" name="tipe<?=$i;?>" id="tipe<?=$i;?>" value="<?=$list[$i]['Tipe'];?>">
			<input type="hidden" name="charge<?=$i;?>" id="charge<?=$i;?>" value="<?=$list[$i]['Service_charge'];?>">
			<input type="hidden" name="nominalVouch<?=$i;?>" id="nominalVouch<?=$i;?>" value="<?=$list[$i]['cashback'];?>">
				<?php } ?>
			<input type="hidden" name="jml" id="jml" value="<?=count($list);?>">
            <input type="hidden" name="kassa" id="kassa" value="<?= $NoKassa ?>">
            <input type="hidden" name="kasir" id="kasir" value="<?= $session_name ?>">
            <!--<input type="hidden" name="no" id="no" value="<?= $no ?>">-->
            <input type="hidden" name="store" id="store" value="<?= $store[0]['KdCabang'] ?>">
            <input type="hidden" name="brgDetail" id="brgDetail" value="">
        </table>
        
        </div>
</td>
<td rowspan="2" valign="top" width="37%" bgcolor="#41c0f8">
        <!--<fieldset>-->
            <!--<div class="panel panel-primary">-->
                <!--<div class="panel-heading">
                    <div class="clock">
                        <div id="jam" align="center">
                            <script language="javascript">
                                jam();
                            </script>
                        </div>
                    </div>
                </div>
				<div class="panel-heading">
                    <div class="panel-body">
						<div style="text-align:center;font-size:20px;">Total Ticket <?php echo $wahana;?> Hari Ini :</div>
						<div style="text-align:center;font-size:20px;">
						<?php if(strlen($ttlticket)=="1"){
							echo "0".$ttlticket;
						}else{
							echo $ttlticket;
						}
						?>
						</div>
					</div>
                </div>-->
                
                   
                    <!--<div class="form-group">
                        <label class="col-sm-4 control-label">No Transaksi</label>
                        <div class="col-sm-8">
                            <input type='text' id='NoTrans' name='NoTrans' style="font-size: 16px;text-align: center;margin-bottom:5px;" class="form-control input-sm" value="<?= $NoTrans ?>" readonly>
                        </div>
                    </div>-->
                    <input type="hidden" name="tampils_jenis" id="tampils_jenis" value="<?php echo $search_jenis ;?>" />
                    <div id="tampils">
					<div class="form-group" id="div_travel">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>Tour Travel</b></font></h4></label>
                        <div class="col-sm-8">
							<select id='TourTravel' name='TourTravel' class="select2" data-allow-clear="true" data-placeholder="Pilih Tour Travel" style="margin-bottom:5px;" onChange="get_tour_guide();">
								<option value=""></option>
								<optgroup label="">
								<?php foreach ($TourTravel AS $rowTravel){?>
									<option value="<?=$rowTravel['KdTravel'];?>"><?=$rowTravel['Nama'];?></option>
								<?php } ?>
								</optgroup>
							</select>
                        </div>
                    </div>
					<div class="form-group" id="div_tour_guide">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>Tour Guide</b></font></h4></label>
                        <div class="col-sm-8">
							<select id='TourGuide' name='TourGuide' class="select2" data-allow-clear="true" data-placeholder="Pilih Tour Guide" style="margin-bottom:5px;" onChange="">
								<option value=""></option>
								<optgroup label="" id="OptTourGuide">
								</optgroup>
							</select>
                        </div>
                    </div>
					<div class="form-group" id="div_petugas_stiker">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>Petugas Stiker</b></font></h4></label>
                        <div class="col-sm-8">
							<select id='PetugasId' name='PetugasId' class="select2" data-allow-clear="true" data-placeholder="Pilih Petugas Sticker" style="margin-bottom:5px;" onChange="SelectMoveNext()">
								<option value=""></option>
								<optgroup label="">
									<?php foreach ($PetugasSticker AS $rowStck){?>
										<option value="<?=$rowStck['username'];?>"><?=$rowStck['employee_name'];?></option>
									<?php } ?>
								</optgroup>
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>No Stiker</b></font></h4></label>
                        <div class="col-sm-8">
                            <input type='text' id='NoIdentitas' name='NoIdentitas' style="font-size: 30px;text-align: center;height:35px;margin-bottom:5px;" class="form-control input-sm" onKeyDown="MoveNext(event, 'identitas', '<?= base_url(); ?>')" maxlength="4" value="">
                        </div>
                    </div>
					
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>Negara</b></font></h4></label>
                        <div class="col-sm-8">
							<select id='Country' name='Country' class="select2" data-allow-clear="true" data-placeholder="Pilih Negara" style="margin-bottom:5px;" onChange="get_kota()">
								<option value=""></option>
								<optgroup label="">
								<?php foreach ($Country AS $rowCountry){?>
									<option value="<?=$rowCountry['country_code'];?>"><?=$rowCountry['country_name'];?></option>
								<?php } ?>
								</optgroup>
							</select>
                        </div>
                    </div>
                
                <div class="form-group" id="div_nama_kota">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>Kota Indoneisa</b></font></h4></label>
                        <div class="col-sm-8">
							<select id='City' name='City' class="select2" data-allow-clear="true" data-placeholder="Pilih Kota Di Indonesia" style="margin-bottom:5px;" onChange="">
								<option value=""></option>
								<optgroup label="">
								<?php foreach ($City AS $rowCity){?>
									<option value="<?=$rowCity['id_kota'];?>"><?=$rowCity['nama_kota'];?></option>
								<?php } ?>
								</optgroup>
							</select>
                        </div>
                    </div>
                    
                    <div class="form-group" id="div_nama_customer">
                        <label class="col-sm-4 control-label"><h4><font color="white"><b>Nama Customer</b></font></h4></label>
                        <div class="col-sm-8">
                            <input type='text' id='NamaRombongan' name='NamaRombongan' style="font-size: 16px;text-align: center;margin-bottom:5px;" class="form-control input-sm" onKeyDown="MoveNext(event, 'rombongan', '<?= base_url(); ?>')" value="">
                        </div>
                    </div>


                </div>
                
                
            <!--</div>-->
            <table class="table responsive">
                <tbody>
                <!--<tr class="warning">
                    <td width="5%">&nbsp;</td>
                    <td class="left">Total Ticket :</td>
                    <td class="left">
                        <div id="TotalItem"></div>
                        <input type='hidden' id='TotalItem2' name='TotalItem2'  value="">
                        <input type='hidden' id='dpp' name='dpp'  value="">
                        <input type='hidden' id='tax' name='tax'  value="">
                    </td>
                </tr>-->
                <tr class="success" style="display: none">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h5>Netto : </h5></td>
                    <td class="left"><h5><div id="TotalNetto"></div> </h5></td>
					<input type='hidden' id='TotalNettoHidde' name='TotalNettoHidde'  value="0">
                </tr>

                <tr class="success" style="display: none">
                        <td width="5%">&nbsp;</td>
                        <td class="left"><h5>Service Charge : </h5></td>
                        <td class="left"><h5><div id="SerCharge"></div> </h5></td>
						<input type='hidden' id='SerCharge2' name='SerCharge2'  value="0">
                </tr>

				<tr bgcolor="#ade4fc">
                        <td width="1%"></td>
                        <td class="left">                      
                        <h4><font color="#6f6c6f"><b>Discount</b></font></h4><?
						//Tambahan Frangky 15/12/2016 
						
						$q = "SELECT Minimum, Nilai
								FROM discountheader h
								INNER JOIN(SELECT NoTrans, Nilai FROM discountdetail WHERE Jenis='X' GROUP BY NoTrans, Nilai) AS d
								ON h.NoTrans=d.NoTrans
								WHERE 1
								AND h.Status='1'
								AND h.TglAwal<='".$store[0]['TglTrans']."' AND h.TglAkhir>='".$store[0]['TglTrans']."'";
						$qry = mysql_query($q);
						$hsl=mysql_fetch_array($qry);
						$minimum = $hsl['Minimum'];
						$nil_disc = $hsl['Nilai'];
						mysql_free_result($qry);
						
						?>
						
						<input type='hidden' id='minimum' name='minimum' value='<? echo $minimum;?>'>
						<input type='hidden' id='nil_disc' name='nil_disc' value='<? echo $nil_disc;?>'>						
						
						</td>
						<td class="left"><b><input style="text-align: right; border:0 " type='text' id='id_discount' name='id_discount' size='10' value=0
								   onKeyDown="InputDiscount(event, '1', '<?= base_url(); ?>')" class="form-control">
						</b></td>
                </tr>
                
                <!--<tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h4>Total : </h4></td>
                    <td class="left"><h4><div id="ttlall"></div></h4></td>
					<input type='hidden' id='ttlall2' name='ttlall2'  value="0">
                </tr>-->

                </tbody>
            </table>

            <!--<div class="panel panel-primary">
                <div class="panel-heading" align="center">
                    Jenis Bayar
                </div>
                <div class="panel-body">-->
                    <table align="center" class="table-responsive" border="0" width="95%">
                        <tr>
                            <td>
                                <input type='hidden' id='confirm_struk' name='confirm_struk' size='10' value="">
                                <input type='hidden' id='confirm_kassa' name='confirm_kassa' size='10' value="<?= $NoKassa ?>">
                                <input type='hidden' id='service_charge' name='service_charge' size='10'
                                       value="<?=$Charge ?>">
                                <input type='hidden' id='total_biaya' name='total_biaya' size='10'
                                       class="InputAlignRight" value="<?=$TotalNetto ?>">
                                       
                                <input type="hidden" id="listvoucher" name="listvoucher" value="">
								<input type="hidden" id="listvouchket" name="listvouchket" value="">
								<input type="hidden" id="listvouchsaldo" name="listvouchsaldo" value="">
								<input type="hidden" id="listvouchpakai" name="listvouchpakai" value="">
								<input type="hidden" id="listjenis" name="listjenis" value="">
								
								
                            </td>
                        </tr>
                        <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo_pos("Currency", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
                        <tr>
                            <td nowrap>
								<input type="radio"  id="pilihan" name="pilihan" value="cash" onClick="on_cheked()" >
								<font color="white"><b>Tunai</b></font>
                            </td>
                            <td colspan="2" nowrap align="right"><input type='text' id='id_tunai' name='id_tunai' size='10'
                                       onKeyDown="TunaiCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"></td>
                            <td nowrap><input type='text' id='cash_bayar' name='cash_bayar' size='10'
                                              onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td>
								<input type="radio" id="pilihan" name="pilihan" value="kredit" onClick="on_cheked()">
								<font color="white"><b>Kredit</b></font>
                            </td>
                            <!-- <td><input type='text' id='id_kredit' name='id_kredit' size='10'
                                       onKeyDown="KreditCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"></td> -->
                            <td>
                            <select  class="form-control" id='id_kredit' name='id_kredit'>
							<option value="">-Kartu-</option>
							<?php
							foreach($kartu as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
							</td>

                            <td>
                            <select  class="form-control" id='id_kredit_edc' name='id_kredit_edc'>
							<option value="">-EDC-</option>
							<?php
							foreach($edc as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
							</td>
                           
                            <td><input type='text' id='kredit_bayar' name='kredit_bayar' size='10'
                                       onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input type="radio" id="pilihan" name="pilihan" value="debet" onClick="on_cheked()">
                               <font color="white"><b>Debit</b></font>
                            </td>
                            <!-- <td><input type='text' id='id_debet' name='id_debet' size='10'
                                       onKeyDown="DebetCustomer(event, '1', '<?= base_url(); ?>')" class="form-control">
                            </td> -->
                            <td>
                            <select  class="form-control" id='id_debet' name='id_debet'>
							<option value="">-Kartu-</option>
							<?php
							foreach($kartu as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
                            </td>
                            <td>
                            <select  class="form-control" id='id_debet_edc' name='id_debet_edc'>
							<option value="">-EDC-</option>
							<?php
							foreach($edc as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
                            </td>
                            <td><input type='text' id='debet_bayar' name='debet_bayar' size='10' onKeyUp="SetKembali()"
                                       class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input type="radio" id="pilihan" name="pilihan" value="voucher" onClick="on_cheked(),VoucherCustomerTicket(event, '1', '<?= base_url(); ?>')">
                                <font color="white"><b>Voucher</b></font>
                            </td>
                            <td colspan="2"><input type='text' id='id_voucher' name='id_voucher' size='10'
                                       onKeyDown="VoucherCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"></td>
                            <td><input type='text' id='voucher_bayar' name='voucher_bayar' size='10'
                                       onKeyUp="SetKembali()" class="form-control" dir="rtl" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td>
                                <font color="white"><b>ID Member</b></font>
                            </td>
                            <td colspan="2"> 
                            <input  type='text' id='member' name='member' size='10' class="form-control" readonly="readonly">
                            <input  type='hidden' id='id_member' name='id_member' size='10'
                                       onblur="cekFreeTicket('<?= base_url(); ?>')" onKeyDown="cekMember(event, '1', '<?= base_url(); ?>')" class="form-control">
                            </td>
                            <td>&nbsp;
                            <span id="span_loading" style=" display: none;"><img src="<?= base_url();?>public/images/ajax-image.gif"/></span>
                            <span id="checklist" style="display: none;"><img src="<?= base_url();?>public/images/accept.png"/></span>&nbsp;<span id="ket_free_ticket" style=" display: none;"></span>
                            <input type='hidden' id='jml_free_ticket' name='jml_free_ticket' value="">
                            </td>
                        </tr>
                    </table>

               <!-- </div>
            </div>-->
			<br>
            <table class="table responsive">
                <tbody>
                <tr class="info">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><font size="5" color="#6f6c6f"> Bayar </font></td>
                    <td class="left">
                        <font size="5" color="#6f6c6f"> <span id='total_bayar' name='total_bayar' onKeyUp="SetKembali()"></span></font>
                        <input type='hidden' id='total_bayar_hide' name='total_bayar_hide' size="10">
                    </td>
                </tr>
                <tr class="info">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><font size="5" color="#6f6c6f"> Kembali </font></td>
                    <td class="left"><font size="5" color="#6f6c6f"> <span id='cash_kembali' name='cash_kembali'> </span></font>
					<input type='hidden' id='cash_kembali_hide' name='cash_kembali_hide' size="10">
					</td>
                </tr>
                </tbody>
            </table>

            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="table responsive">
                <tr bgcolor="#41c0f8">
                    <td align="center" height="40"><input type='button' class="btn btn-success" value='Complete'
                                                          onClick="cek_form()">
                        <input type='reset' value='Clear' class="btn btn-danger" onClick="refresh();">
                    </td>
                </tr>
            </table>
        <!--</fieldset>
	</div>-->
</td>
</tr>
</form>
<!--<tr>
    <td colspan="2">
    <fieldset>
        <table width="100%" border="0" align="center" class="huruf">
            <tr>
                <td width="5%">&nbsp;</td>
                <td width="10%"><b>Kassa : </b><?= $NoKassa ?></td>
                <td width="5%">&nbsp;</td>
                <td width="15%"><b>Kasir : </b><?= $session_name ?></td>
                <td width="5%">&nbsp;</td>
                <!--<td width="10%"><b>No Struk : </b><?= $no ?></td> -->
                <!--<td width="50%">&nbsp;</td>
            </tr>
        </table>
    </fieldset>
    </td>
</tr>-->
</table>
<!--</div>-->

</body>
<div style="display: none">
<?php
$this->load->view('footer');
?>
</div>
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/select2/select2.css">

<script src="<?= base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?= base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2/select2.min.js"></script>
<script>
	function SelectMoveNext(){
		document.getElementById('NoIdentitas').focus();
	}
	
	function MoveNext(e, row, url){
		if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {
			var identitas = document.getElementById('NoIdentitas').value;
			var check = document.getElementById('pilihan').value;
			if(row=="identitas"){
				document.getElementById('NamaRombongan').focus();
			}else if(row=="rombongan"){
				if(identitas!=""){
					if(check=="cash"){
						document.getElementById("pilihan").checked = true;
						document.getElementById('cash_bayar').disabled=false;
						document.getElementById('cash_bayar').focus();
					}
				}else{
					("Nomor identitas masih kosong");
					document.getElementById('NoIdentitas').focus();
				}
			}else{
				document.getElementById('NoIdentitas').focus();
			}
		}
	}
    function EditRecord(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {

            NoUrut = document.getElementById('NoUrut').value * 1;
            noLast = document.getElementById('noLast').value;
            var nil;
            nil = NoUrut * 1;
            if (isNaN(nil)) {
                alert("Format No Urut salah, Harap masukkan angka");
            }
            else if (nil >= noLast) {
                alert("Data tidak ditemukan, data terakhir adalah " + (noLast - 1));
            }
            else {

                kode = document.getElementById('pc' + NoUrut).value;


                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/penjualan/EditRecord/' + kode + '/',
                    success: function (msg) {

                        var jsdata = msg;
                        eval(jsdata);

                        document.getElementById('kdbrg1').value = datajson[0].KodeBarang;
                        document.getElementById('nmbrg1').value = datajson[0].NamaStruk;
                        document.getElementById('qty1').value = datajson[0].Qty;
                        document.getElementById('jualm1').value = number_format(datajson[0].Harga, 0, ',', '.');
                        document.getElementById('netto1').value = number_format(datajson[0].Netto, 0, ',', '.');
                        document.getElementById('jualmtanpaformat1').value = datajson[0].Harga;
                        document.getElementById('nettotanpaformat1').value = datajson[0].Netto;

                        document.getElementById('NoUrut').readOnly = true;
                        document.getElementById('kdbrg1').readOnly = true;
                        document.getElementById('qty1').readOnly = false;
                        document.getElementById('qty1').focus();
                        document.getElementById('EditFlg').value = 1;
                    }
                });
            }
        }
    }

    function CustomerView(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {
            pelanggan = document.getElementById('pelanggan').value;
            if (pelanggan !== "") {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/penjualan/CustomerView/' + pelanggan + '/',
                    success: function (msg) {
                        var jsdata = msg;
                        eval(jsdata);
                        document.getElementById('id_customer').value = datajson[0].KdCustomer;
                        document.getElementById('nama_customer').value = datajson[0].Nama;
                        document.getElementById('gender_customer').value = datajson[0].Gender;
                        document.getElementById('tgl_customer').value = datajson[0].TglLahir;
                        document.getElementById('cash_bayar').focus();
                    }
                });
            }
            else {
                alert("masukkan nomor Id Customer");
            }
        }
    }

    function VoucherCustomer(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }

        if (code == 13) {
            id_voucher = document.getElementById('id_voucher').value;
            if (id_voucher !== "") {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/penjualan/VoucherCustomer/' + id_voucher + '/',
                    success: function (msg) {
                        var jsdata = msg;
                        eval(jsdata);
                        document.getElementById('id_voucher').value = datajson[0].KdVoucher;
                        document.getElementById('voucher_bayar').value = datajson[0].Nominal;
                        document.getElementById('voucher_bayar').disabled = false;

                        SetKembali()
                    }
                });
            }
            else {
                alert("masukkan nomor voucher");
            }
        }

    }

    function cek_form() { //titik
    	ip = document.getElementById('ip').value;
        total_bayar = document.getElementById('total_bayar_hide').value;
        identitas = document.getElementById('NoIdentitas').value;
        rombongan = document.getElementById('NamaRombongan').value;
        petugas = document.getElementById('PetugasId').value;
        country = document.getElementById('Country').value;
        city = document.getElementById('City').value;
        subkategori = document.getElementById('SubKatTicket').value;
        jml_ticket = document.getElementById('TotalItem2').value;
        jml_free_ticket = document.getElementById('jml_free_ticket').value;
        id_member = document.getElementById('id_member').value;
        tampils = document.getElementById('tampils_jenis').value;
        nil_kredit = document.getElementById('kredit_bayar').value;
        id_kredit = document.getElementById('id_kredit').value;
        id_kredit_edc = document.getElementById('id_kredit_edc').value;
        nil_debet = document.getElementById('debet_bayar').value;
        id_debet = document.getElementById('id_debet').value;
        id_debet_edc = document.getElementById('id_debet_edc').value;
        // console.log(petugas+" test");
        
		if(tampils!='7'){
		// if(ip!='192.168.10.61x'){
           if(ip!='192.168.10.227'){
			if(identitas!="" && petugas!=""){
                if (total_bayar == "") {
					alert("Transaksi belum dibayar");
					document.getElementById('cash_bayar').focus();
				}
				else {
					if (total_biaya > total_bayar) {
						alert("Total bayar masih kurang");
						document.getElementById('cash_bayar').focus();
					}else if(parseFloat(nil_kredit)>0 && (id_kredit=="" || id_kredit_edc=="")){
                        alert("Silahkan Pilih Bank Kartu atau EDC Bank Pada Tipe Payment Kredit");
                    }else if(parseFloat(nil_debet)>0 && (id_debet=="" || id_debet_edc=="")){
                            alert("Silahkan Pilih Bank Kartu atau EDC Bank Pada Tipe Payment Debet");
                    }else if(country==""){
						alert("Negara Belum Dipilih");
						document.getElementById('Country').focus();
					}else if(country=="ID" && city==""){
						alert("Kota Di Indonesia Belum Dipilih");
						document.getElementById('City').focus();
					}else if(subkategori=="9" && id_member!=""){
						if(jml_ticket>jml_free_ticket){
							alert("Melebihi Kuota Free Ticket.");
						}else{
							document.form1.submit();
						}
					}else {
						//no = document.getElementById('no').value;
						document.form1.submit();
					}
                }
            }else{
				if(identitas=="" && petugas!=""){
					alert("Nomor Sticker masih kosong");
					document.getElementById('NoIdentitas').focus();
				}else if(identitas!="" && petugas==""){
					alert("Nama Petugas masih kosong");
					document.getElementById('PetugasId').focus();
				}else{
					alert("Silahkan isi data yang masih kosong");
					document.getElementById('NoIdentitas').focus();
				}
			}
		
	    }else{
			if(identitas==""){
				alert("Nomor Sticker masih kosong");
				document.getElementById('NoIdentitas').focus();
		    }else if(country==""){
				alert("Negara Belum Dipilih");
				document.getElementById('Country').focus();
			}else if (total_biaya > total_bayar) {
				alert("Total bayar masih kurang");
				document.getElementById('cash_bayar').focus();
			}else if(parseFloat(nil_kredit)>0 && (id_kredit=="" || id_kredit_edc=="")){
                        alert("Silahkan Pilih Bank Kartu atau EDC Bank Pada Tipe Payment Kredit");
            }else if(parseFloat(nil_debet)>0 && (id_debet=="" || id_debet_edc=="")){
                            alert("Silahkan Pilih Bank Kartu atau EDC Bank Pada Tipe Payment Debet");
            }else{
				document.form1.submit();
			}
		}
        }else{
            if (total_bayar == "") {
                                alert("Transaksi belum dibayar");
                                document.getElementById('cash_bayar').focus();
                            }
                            else {
                                if (total_biaya > total_bayar) {
                                    alert("Total bayar masih kurang");
                                    document.getElementById('cash_bayar').focus();
                                }else if(subkategori=="9" && id_member!=""){
                                    if(jml_ticket>jml_free_ticket){
                                        alert("Melebihi Kuota Free Ticket.");
                                    }else{
                                        document.form1.submit();
                                    }
                                }
                                else {
                                    //no = document.getElementById('no').value;
                                    document.form1.submit();
                                }
                            }
                }
    }

	function get_tour_guide(){
		var KdTravel = document.getElementById('TourTravel').value;
		// alert(KdTravel);
		$.ajax({
		  url: "<?php echo site_url('transaksi/ticketing/get_tour_guide'); ?>/" + KdTravel,
		  type: "GET",
		  dataType: "JSON",
		  success: function(data)
		  
		  {
			$("#OptTourGuide").html(data);
			for (var i = 0; i < data.length; i++) {
				$('#OptTourGuide').append('<option value="' + data[i].KdTourLeader + '">' + data[i].Nama+ '</option>');
			}
		  }
		  ,
		  error: function (textStatus, errorThrown)
		  {
			alert('Error get data');
		  }
		});
	}
	
	function get_jenis_ticket(kassa,session_name,store){
		
		var jenis_ticket = document.getElementById('SubKatTicket').value;
		
		window.location = "<?php echo site_url('transaksi/ticketing/index'); ?>/" + jenis_ticket;	

		//ini adalah dulu pakai ajax tapi entah kenapa hasil ajax muncul ketika ngepost save ga ke ditek
		//sehingga cara lain seperti metode search 26-07-2017
		
		
		/*document.getElementById("tb_jenis_ticket").innerHTML = "Loading...";
		
		$.ajax({
		  url: "<?php echo site_url('transaksi/ticketing/get_jenis_ticket'); ?>/" + jenis_ticket,
		  data: {tck:jenis_ticket,kssa:kassa,sess:session_name,str:store},
		  type: "POST",
		  dataType: 'html',					
				 success: function(res)
				 {
					$('#tb_jenis_ticket').html(res);
				 },
				 error: function(e) 
				 {
					alert(e);
				 } 
		});*/
	}
	
	function get_kota(){
		   var country = document.getElementById('Country').value;
		   if(country=="ID"){
		   	document.getElementById('City').disabled=false;
		   }else{
		   		document.getElementById('City').disabled=true;
		   }	   
		}
		
	function kurang_pesanan(obj){
								objek = obj.id;
	                            no = objek.substr(6,objek.length-6); 
                                var jml=parseInt($('#qty1'+no).val());
                                if(jml>0){
                                    jml=jml-1;
                                }                              
                                $('#qty1'+no).val(jml);
                                getTotal(no);
                }

    function tambah_pesanan(obj){
    	                         
    	                        objek = obj.id;
	                            no = objek.substr(6,objek.length-6); 
                                var jml=parseInt($('#qty1'+no).val());
                                maks_beli=0;
                                if(maks_beli>jml || maks_beli==0){
                                    jml=jml+1;
                                }else{
                                    alert('maksimum pembelian '+maks_beli);
                                }                              
                                $('#qty1'+no).val(jml);
                                getTotal(no);
                }
                
    function VoucherCustomerTicket(e, row, url) {
		totalbiaya = parseFloat($("#total_biaya").val());
		if(totalbiaya==0)
		{
		   alert("Total Ticket masih NOL");	
		}else
		{
		url = url+"index.php/pop/voucher3/index/";
		window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
	}
	
	
	function InputDiscount(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			
				url = url+"index.php/pop/discount_ticket/index/";
				window.open(url,'popuppage','width=750,height=500,top=100,left=150');
			
		}
	}
	
	
	function hitung_ticket(){
		//alert(document.getElementById('TotalNettoHidde').value);
		//alert(document.getElementById('id_discount').value);
		
		total_netto = parseInt(document.getElementById('TotalNettoHidde').value);
		discount = parseInt(document.getElementById('id_discount').value);
		
		hasil = total_netto - (total_netto*discount/100);
		document.getElementById('ttlall').innerHTML = number_format(hasil,0,',','.');
		document.getElementById('total_biaya').value = hasil;
		document.getElementById('ttlall2').value = hasil;
		
	}

</script>
