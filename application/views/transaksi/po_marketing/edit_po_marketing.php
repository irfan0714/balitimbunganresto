<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Purchase Order";

?>
<script language="javascript" src="<?=base_url();?>public/js/po_marketing.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/po_marketing/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen_; ?>">
		<input type="hidden" name="v_jml_proposal" id="v_jml_proposal" value="<?php echo $nil_proposal->total_proposal; ?>">
		<input type="hidden" name="v_cek_penggunaan_proposal" id="v_cek_penggunaan_proposal" value="<?php echo $cek_penggunaan_proposal->total; ?>">
	    <input type="hidden" name="v_gudang" id="v_gudang" value="<?php echo $header->KdGudang; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td colspan="3"><b><?php echo $header->NoDokumen_; ?></b></td>
	            
	            	            
	            
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->TglDokumen_!="" && $header->TglDokumen_!="00-00-0000") { echo $header->TglDokumen_; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	            
	        
	        	<td class="title_table" width="200">Supplier</td>
                <td>
                    <select name="v_supplier" id="v_supplier" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_supplier', this.value)">  
                       <option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
	            			$selected="";
							if($header->KdSupplier==$val["KdSupplier"])
							{
								$selected='selected="selected"';
							}
							
							?><option  <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?> 
                    </select>
                </td>
	        
	        </tr>
	        
	        <tr>
	            <!--<td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
	            			$selected="";
							if($header->KdGudang==$val["KdGudang"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>-->
				
				<td class="title_table">&nbsp;</td>
			<td>&nbsp;</td>
	            <!--<td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>-->
	            
	            
	            
	            <td class="title_table">TOP (Hari)</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->top_; ?>" name="v_top" id="v_top" maxlength="255" size="50%"></td>
	        
	        
	        </tr>
	        
	        <tr>
	            <td class="title_table">No PR</td>
                <td>
                    <input type="text" class="form-control-new" readonly="readonly" size="20" name="v_NoPr" id="v_NoPr" value="<?php echo $header->NoPr;?>">
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_search_pr()">&nbsp;&nbsp;<i class="entypo-search"></i>Cari</button>
                </td>
                
                <td class="title_table">Currency</td>
                <td>
                	<select name="v_currencycode" id="v_currencycode" class="form-control-new" style="width: 200px;">  
                        	<option value="">Pilih Mata Uang</option>
		            		<?php
		            		foreach($currency as $val)
		            		{
		            			$selected="";
		            			if($header->currencycode_== $val['Kd_Uang'])
								{
								$selected='selected="selected"';
								}
								?>
							<option <?php echo $selected; ?> value="<?php echo $val['Kd_Uang']; ?>"><?php echo $val['Kd_Uang']." - ".$val['Keterangan']; ?></option>	
							<?php
							}
		            		?>
	            	</select>
                </td>
                
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->Keterangan_; ?>" name="v_note" id="v_note" maxlength="255" size="50%"></td>
	        
	        	<td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->TglTerima_!="" && $header->TglTerima_!="00-00-0000") { echo $header->TglTerima_; }else{echo date('d-m-Y');}  ?>" name="v_tgl_terima" id="v_tgl_terima" size="10" maxlength="10">
				
	            </td>
	        
	        </tr>
	        
	        <?php
			if($header->Status_==0 or $header->Status_==2)
			{
			?>
		        <tr>
		            <td class="title_table">Status</td>
		            <td colspan="3">
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status_==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->Status_==1){ echo "selected='selected'"; } ?> value="1">Kirim PO</option>
		            		<option <?php if($header->Status_==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
			<?php
			}
			else
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            <?php
		            if($header->Status_==1)
		            {
						echo "<b>Kirim PO</b>";
					}
					else if($header->Status_==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr> 
			<?php
			}
			?>
            
			
			<?php
			if($header->status==0 or $header->status==2)
			{
			?>
			
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="300"><center>Nama Barang</center></th>               
							    <th width="50"><center>Sisa Qty Request</center></th>
							    <th width="30"><center>Qty</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>Disc (%)</center></th>
							    <th width="100"><center>Potongan (IDR)</center></th>
							    <th width="100"><center>Sub Total</center></th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden" name="grdTotal" id="grdTotal" value=""/>
						  <?php 
						  $Sid=1;
						  foreach($detail_list as $val)
						  {?>
							
							  <tr>
								<td align="left"><?php echo $val["NamaBarang"]; ?><input type="hidden" name="v_nmbarang[]" value="<?php echo $val["NamaBarang"]; ?>"></td>
								<td align="center"><?php echo $val["Qty"]; ?><input type="hidden" name="v_qty_request[]" value="<?php echo $val["Qty"]; ?>"></td>
								<td align="right"><input style="text-align: right; width: 60px;" type="text" class="form-control-new" name="v_Qty[]" id="v_Qty_<?php echo $Sid; ?>" value="<?php echo $val["QtyTerima"]; ?>" ></td>
                                <td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal, total dan grand total." name="v_Harga[]" id="v_Harga_<?php echo $Sid; ?>" value="<?php echo $val["HargaSatuan"]; ?>" onkeydown="HitungHarga(event, 'harga', this);" dir="rtl" ></td>
							  	<td align="right"><input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc[]" id="v_Disc_<?php echo $Sid; ?>" onkeydown="HitungHarga2(event, 'harga', this);"  value="<?php echo $val["Disc"]; ?>"></td>
							  	<td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_Potongan[]" id="v_Potongan_<?php echo $Sid; ?>" onkeydown="HitungHarga3(event, 'harga', this);" value="<?php echo $val["Potongan"]; ?>" ></td>
							  	<td align="right"><input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_subtotal[]" id="v_subtotal_<?php echo $Sid; ?>" dir="rtl" value="<?php echo $val["Jumlah"]; ?>" ></td>
								<td style="display: none"><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $Sid; ?>" value="<?php echo $val['Total'] ?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
							  </tr>							
							<?php  $Sid++; } ?>
						</tbody>	
						<!-- style="display: none" -->
						   <tr style="color: black; font-weight: bold;">
                                <td colspan="5" rowspan="4">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL
                                
                                </td>
                                <td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_Jumlah" id="v_Jumlah" value="<?php echo number_format($header->Jumlah,0);?>"></td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total." style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" onkeydown="HitungHarga(event, 'diskon', this);" value="<?php echo number_format($header->DiscHarga,0);?>" > (%)</td>
                                <td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_pot_disc" id="v_pot_disc" value="<?php echo number_format($header->Diskon,0);?>"></td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total."style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn" id="v_PPn" value="<?php echo number_format($header->PPn_,0);?>" onkeydown="HitungHarga(event, 'ppn', this);"> (%)</td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo number_format($header->NilaiPPn,0);?>">
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                </td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" name="v_Total" id="v_Total" value="<?php echo number_format($header->Total,0);?>">
                                </td>
                            </tr>
											
					</table>
	        	</td>
	        </tr>
	        
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/po_marketing/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/po_marketing/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->dono)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>