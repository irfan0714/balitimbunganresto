♠<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Proposal</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 10pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 9pt;
			vertical-align: top;
		}
		
	</style>
    <body>
    
   <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
            	<td colspan="5" align="left">
            		<?php echo $pt->Nama;?><br>
					<?php echo $pt->Alamat1;?><br>
					<?php echo $pt->Alamat2;?><br>
					<?php echo $pt->TelpPT;?><br>
            	</td>
            </tr>
            
            <tr>
                <td colspan="5" align="center"><b>PURCHASE ORDER NON STOCK</b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><span><?= "No : ".$nomor; ?></span></td>
            </tr>
            
    </table> 
    
    <br>
    <table width="30%"  border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        <tr>
                            <td>Tanggal POM</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->TglDokumen_; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>Supplier</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->Nama; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td>Estimasi</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->TglTerima_; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>TOP</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->top_; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td>Mata Uang</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->currencycode_; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>Contact</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->Telepon; ?></span></td>
                        </tr>
                         <tr>
                            <td>No. PRM</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->NoPr; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <!-- <td>Contact</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->Telepon; ?></span></td> -->
                        </tr>
		</table>
		<br>
		
		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
			<tr>
				<td align="center" width="50">No</td>
				<td align="center" width="350">Nama Barang</td>
				<td align="center">Qty</td>
				<td align="center">Harga Satuan</td>
				<td align="center">SubTotal</td>
			</tr>
			<?php 
			$no = '1';
			foreach($detail AS $val){?>
			<tr>
				<td align="center"><?= $no;?></td>
				<td><?= $val['NamaBarang'];?></td>
				<td align="center"><?= $val['QtyTerima'];?></td>
				<td align="right"><?= number_format($val['HargaSatuan']);?></td>
				<td align="right"><?= number_format($val['Total']);?></td>
			</tr>
			<?php 
			$no++;
			} ?>
			
			<tr>
				<td>Note</td>
				<td colspan="2"><?= $header->Keterangan; ?></td>
				<td>Total</td>
				<td align="right"><?= number_format($header->Jumlah); ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>Disc</td>
				<td align="right"><?= number_format($header->DiscHarga); ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>PPN&nbsp;&nbsp;<?= number_format($header->PPn_)."%"; ?></td>
				<td align="right"><?= number_format($header->NilaiPPn); ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>Grand Total</td>
				<td align="right"><?= number_format($header->Total); ?></td>
			</tr>
		</table> 
		
		<br>
		
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
			<tr>
				<td align="center">Dibuat Oleh,</td>
				<td align="center">DiKetahui Oleh,</td>
				<td align="center">DiTerima Oleh,</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">..................</td>
				<td align="center">..................</td>
				<td align="center">..................</td>
			</tr>
		</table> 
    

</body>
</html>