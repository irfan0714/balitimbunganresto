<?php 

$this->load->view('header'); 

$modul = "Purchase Order";

?>

<script language="javascript" src="<?=base_url();?>public/js/po_marketing.js"></script>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/po_marketing/save_data'>
		<input type="hidden" name="v_jml_proposal" id="v_jml_proposal" value="<?php echo $nil_proposal->total_proposal; ?>">
		<input type="hidden" name="v_cek_penggunaan_proposal" id="v_cek_penggunaan_proposal" value="<?php echo $cek_penggunaan_proposal->total; ?>">
	    <table class="table table-bordered responsive">                        
	
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	            
	            <td class="title_table" width="200">Supplier</td>
                <td>
                    <select name="v_supplier" id="v_supplier" class="form-control-new" style="width: 200px;" onchange="cari_top()">  
                       <option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
							?><option value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?> 
                    </select>
                </td>
	            
	            
	            
	        </tr>
			
			<tr>
			 
	           <td class="title_table">&nbsp;</td>
			<td>&nbsp;</td>
	            <!--<td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>-->
	            
	            <td class="title_table">TOP (Hari)</td>
                <td>
                    <input type="text" class="form-control-new" style="width: 200px;" name="v_top" id="v_top">
                </td>
	            
	        </tr>
	        
	        <tr>
	            <td class="title_table">No PR</td>
                <td>
                    <input readonly type="text" class="form-control-new" size="20" name="v_NoPr" id="v_NoPr" onblur="detail()">
                    <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_search_pr()">&nbsp;&nbsp;<i class="entypo-search"></i>Cari</button>
                </td>
                
                <td class="title_table">Currency</td>
                <td>
                	<select name="v_currencycode" id="v_currencycode" class="form-control-new" style="width: 200px;">  
                        	<option value="">Pilih Mata Uang</option>
		            		<?php
		            		foreach($currency as $val)
		            		{
								?><option value="<?php echo $val["Kd_Uang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
							}
		            		?>
	            	</select>
                </td>
                
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="50%"></td>
	        
	        	<td class="title_table" width="150">Estimasi Terima</td>
                <td>
                	<input type="text" class="form-control-new datepicker" name="v_TglTerima" id="v_TglTerima" size="10" maxlength="10" value="">
      			</td> 
	        
	        </tr>
			
			<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
					
					</table>
				</td>
			</tr>
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/po_marketing/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
	
</div>
    	
<?php $this->load->view('footer'); ?>
<script>


function CopyQtyPR(id){
        //objek = obj.id;
		//id = objek.substr(9,objek.length-9);
        QtyPR=  $("#v_qty_request" + id).val();
		//console.log('QtyPR ',QtyPR,'id ',id);
		$("#v_Qty_"+id).val(QtyPR);
	}
	
function detail(){
	var nopr=$('#v_NoPr').val();
	var url=$('#base_url').val();

	$.ajax({
		url : url+"index.php/transaksi/po_marketing/save_detail_temp/"+nopr+"/",
		type: "GET",
		dataType: "html",
		success: function(res)
		{
			$('#TabelDetail').html(res);
			
		},
		error: function(e) 
		{
			alert(e);
		}
	});


}
	
</script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	
	function cari_top(){
		var supplier = $('#v_supplier').val();
		var url =$('#base_url').val();
		$.ajax({
					url: url+"index.php/transaksi/po_marketing/top/",
					data: {id:supplier},
					type: "POST",
					dataType: "json",
			        success: function(data)
					{
						$('#v_top').val(data.top);
						
					} 
				});
				
	}
</script>

	

