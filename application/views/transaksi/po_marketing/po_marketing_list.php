<?php $this->load->view('header'); ?>
<head>
</head>
<script language="javascript" src="<?= base_url(); ?>public/js/po_marketing.js"></script>

<script type="text/javascript">
    
    function toExcel(){
        // alert("TES");
        $('#tgl_awal1').val($('#tgl_awal').val());
        $('#tgl_akhir1').val($('#tgl_akhir').val());
        $('#supplier1').val($('#search_supplier').val());
        
        //search_supplier
        $('#exportform').submit();

    }
</script>

<form method="POST"  name="export" id="exportform" action='<?php echo base_url(); ?>index.php/transaksi/po_marketing/ExportPOM'>
    <input type="hidden" name="tgl_awal1" id="tgl_awal1" value="">
    <input type="hidden" name="tgl_akhir1" id="tgl_akhir1" value="">
    <input type="hidden" name="supplier1" id="supplier1" value="">

</form>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/po_marketing/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            <!--&nbsp;<b>Gudang</b>&nbsp;
            <select class="form-control-new" name="search_gudang" id="search_gudang">
                <option value="">All</option>
                <?php
                foreach ($mgudang as $val) {
                    $selected = "";
                    if ($search_gudang) {
                        if ($val["KdGudang"] == $search_gudang) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
                }
                ?>
            </select>-->
           

            &nbsp;<b>Supplier</b>&nbsp;
            <select class="form-control-new" name="search_supplier" id="search_supplier" style="width: 100px;">
                <option value="">All</option>
                <?php
				
                foreach ($supplier as $val) {
                    $selected = "";
                    if ($search_supplier) {
                        if ($val["KdSupplier"] == $search_supplier) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
                }
                ?>
            </select>  
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Close</option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2">Void</option>
            </select>  
            &nbsp;
            <b>Tanggal</b>&nbsp;
            <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_awal!="" && $search_tgl_awal!="00-00-0000") { echo $search_tgl_awal; }else{echo date('d-m-Y');}  ?>" name="tgl_awal" id="tgl_awal" size="10" maxlength="10">
            &nbsp;s/d&nbsp;
            <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_akhir!="" && $search_tgl_akhir!="00-00-0000") { echo $search_tgl_akhir; }else{echo date('d-m-Y');}  ?>" name="tgl_akhir" id="tgl_akhir" size="10" maxlength="10">
             
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" onClick="toExcel();">Excel<i class="entypo-export"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/po_marketing/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>No Dokumen</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th width="100"><center>No Purchase Request</center></th>
		        <th><center>Supplier</center></th>
		        <th><center>Keterangan</center></th>
		        <th width="100"><center>Total</center></th>
		        <th width="100"><center>Approval</center></th>
		        <th width="100"><center>Status</center></th>
                <th width="120"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Pending</b></font>";
                } else if ($val["status"] == 1) {
                    $echo_status = "<font style='color:#6bcf29'><b>Open</b></font>";
                } else if ($val["status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>Void</b></font>";
                }
                
                if ($val["Approval_Status_"]==0){
					$app="waiting";
				}else if ($val["Approval_Status_"]==1){
					$app= "<font color='blue'>".$val["Approval_By_"]." / ".$val["Approval_Date_"]."</font>";
				}else if ($val["Approval_Status_"]==2){
					$app= "<font color='red'>".$val["Approval_By_"]." / ".$val["Approval_Date_"]." / ".$val["Approval_Remarks_"]."</font>";
				}
				
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $val["TglDokumen"]; ?></td>
                    <td align="left"><?php echo $val["NoPr"]; ?></td>
                    <td align="left"><?php echo $val["KdSupplier"]." :: ".$val["Nama"]; ?></td>
                    <td align="left"><?php echo $val["Keterangan"]; ?></td>
                    <td align="right"><?php echo number_format($val["Total"],0); ?></td>
                    <td align="left"><?php echo $app; ?></td>
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                        if ($val["status"] == 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/po_marketing/edit_po_marketing/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>

                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }

                        if ($val["status"] == 1) {
                            ?>
                            
                            <a href="<?php echo base_url(); ?>index.php/transaksi/po_marketing/view_po_marketing/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>

                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/po_marketing/edit_po_marketing/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->
							<button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print Dot Matrix" title="" onclick="PopUpPrint('<?= $val['NoDokumen'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>
                            
                            <button type="button" class="btn btn-orange btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print To PDF" title="" onclick="PopUpPrintPDF('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
								<i class="entypo-export"></i>
							</button>                            
                            
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
							
                            <?php
                        }

                        if ($val["status"] == 2) {
                            ?>
                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/po_marketing/edit_po_marketing/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>-->
							<a href="<?php echo base_url(); ?>index.php/transaksi/po_marketing/view_po_marketing/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/po_marketing/edit_po_marketing/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['NoDokumen'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>-->
                            
                            
                           <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }
                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>


<?php $this->load->view('footer'); ?>

<script>
	function PopUpPrintPDF(nodok, baseurl)
		{
		    url = "index.php/transaksi/po_marketing/create_pdf/" + escape(nodok);
		    window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
		}
</script>
