<?php 
$this->load->view('header');
?>

<script>
	function getDetail(){
    	// var tahun = $('#v_tahun').val();
    	// var bulan = $('#v_bulan').val();

    	var v_start_date = $('#v_start_date').val();
    	var v_end_date = $('#v_end_date').val();
		var btn = $('#btn_proses').val();
    	
    	var vdata = new Array(2);
    	vdata[0] = v_start_date;
    	vdata[1] = v_end_date;
		vdata[2] = btn;
    	var jsonString = JSON.stringify(vdata);
    	
    	var pleaseWait = $('#pleaseWaitDialog');

    	/*
    	$('#pleaseWaitDialog').modal('show');
		var interval = setInterval(function () {
            $('#pleaseWaitDialog').modal();
        }, 100);
        */
        pleaseWait.modal('show');
    	$.ajax({
	        type: "POST",
	        url: "<?=base_url();?>index.php/transaksi/generate_invoice_vci/getDetail/",
	        data: {data: vdata},
	        success: function(data) {
	        	/*
	        	var interval = setInterval(function () {
					$('#pleaseWaitDialog').modal('hide');
				}, 100);
				*/
				$('#UpdateDetail').html(data);
				pleaseWait.modal('hide');
			}
	    });
	}

    function getDetailTombolAjaib(){
        // var tahun = $('#v_tahun').val();
        // var bulan = $('#v_bulan').val();

        $('#excel').val('');
        var v_start_date = $('#v_start_date').val();
        var v_end_date = $('#v_end_date').val();
        var btn = $('#btn_proses').val();
        var vdata = new Array(2);
        vdata[0] = v_start_date;
        vdata[1] = v_end_date;
        vdata[2] = btn;
        var jsonString = JSON.stringify(vdata);
        
        var pleaseWait = $('#pleaseWaitDialog');

        /*
        $('#pleaseWaitDialog').modal('show');
        var interval = setInterval(function () {
            $('#pleaseWaitDialog').modal();
        }, 100);
        */
        pleaseWait.modal('show');
        $.ajax({
            type: "POST",
            url: "<?=base_url();?>index.php/transaksi/generate_invoice_vci/getDetailTombolAjaib/",
            data: {data: vdata},
            success: function(data) {
                /*
                var interval = setInterval(function () {
                    $('#pleaseWaitDialog').modal('hide');
                }, 100);
                */
                $('#UpdateDetail').html(data);
                pleaseWait.modal('hide');
            }
        });
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Generate Invoice VCI</strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action="<?= base_url(); ?>index.php/transaksi/generate_invoice_vci/simpan/">
		
	    <table class="table table-bordered responsive">   
	        <!-- <tr>
	            <td class="title_table" width="150">Tahun</td>
	            <td> 
	            	<select class="form-control-new" name="v_tahun" id="v_tahun" style="width: 100px;">
	            	<?php
	            		for($i=0;$i<count($tahun);$i++){
						?>
							<option value=<?=$tahun[$i];?>><?=$tahun[$i];?></option>
						<?php
						}
	            	?>
	            </td>
	        </tr> -->
	        <!-- <tr>
	            <td class="title_table" width="150">Bulan</td>
	            <td> 
	            	<select class="form-control-new" name="v_bulan" id="v_bulan" style="width: 50px;">
	            	<?php
	            		for($i=0;$i<count($bulan);$i++){
						?>
							<option value=<?=$bulan[$i];?>><?=$bulan[$i];?></option>
						<?php
						}
	            	?>
	            </td>
	        </tr> -->
	        <tr>
	            <td class="title_table" width="150">Tanggal</td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo ""; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10" autocomplete="off">
	            	&nbsp;
	            	s/d
	            	&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo ""; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10" autocomplete="off">
	            </td>
	        </tr>
	        <tr>
            	<td>&nbsp;</td>
            	<td>
					<input type='hidden' value='<?= $excel ?>' id="excel" name="excel">
                	<input type="button" class="btn btn-info btn-icon btn-sm icon-center" name="btn_proses" id="btn_proses" value="Proses" onclick="getDetail()">
                    <input type="button" class="btn btn-info btn-icon btn-sm icon-center" name="btn_proses" id="btn_proses" value="Excel" onclick="getDetail()">
					<input type="button" class="btn btn-gold btn-icon btn-sm icon-center" name="btn_ajaib" id="btn_proses" value="View Data" onclick="getDetailTombolAjaib()">
            	</td>
        	</tr>
	    </table>
		<div id="list">
			<tr>
				<td colspan="100%">
					<span id='UpdateDetail'></span>
				</td>
			</tr>
		</div>
	</form>	     
	</div>
</div>
<!-- Modal -->
<div class="modal" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h1>Processing...</h1>
      </div>
      <div class="modal-body">
        <div class="progress">
          <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
            <span class="sr-only">40% Complete (success)</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
