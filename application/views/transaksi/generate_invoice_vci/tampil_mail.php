<?php
//print_r($data);
	$mylib = new globallib();
    $table_border = 1;
    
    $echo = '';
	$echo = '

		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">Laporan Penjualan NPM ke VCI '.$tgl_awal.' s/d '.$tgl_akhir.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th rowspan="2" style="vertical-align: middle;">Kode</th>
		        <th rowspan="2" style="vertical-align: middle;">Nama Barang</th>
				<th style="vertical-align: middle; text-align: center;" colspan="3">Hari Ini</th>
				<th style="vertical-align: middle; text-align: center;" colspan="3">Bulan Ini</th>
		    </tr>
		    <tr>
		        <th style="text-align: right;">Qty</th>
				<th style="text-align: right;">Rp Beli</th>
		        <th style="text-align: right;">Rp Jual</th>
		        <th style="text-align: right;">Qty</th>
				<th style="text-align: right;">Rp Beli</th>
		        <th style="text-align: right;">Rp Jual</th>
		    </tr>
		</thead>

		<tbody>
		';

    $no = 1; 
	$tottd = 0;
	$tot = 0;
	$totqtytd = 0;
	$totqty = 0;
	$totbeli = 0;
	$totbelitd = 0;
    for($i=0;$i<count($data);$i++)
    {
        $pcode = $data[$i]['PCode'];
		$nama  = $data[$i]['NamaLengkap'];
		$harga  = $data[$i]['Harga'];
		$hargabeli  = $data[$i]['HargaBeli'];
		$qtytd  = $data[$i]['QtyTd']+0;
		$qty  = $data[$i]['Qty']+0;
		
		// print_r($qtytd);
		
		$tottd += $qtytd * $harga;
		$tot   += $qty * $harga;
		$totbeli += $qty * $hargabeli;
		$totbelitb += $qtytd * $hargabeli;
		
		$belitd = $qtytd*$hargabeli;
		$beli = $qty*$hargabeli;
		$jualtd = $qtytd*$harga;
		$jual =  $qty*$harga;
		
		$totqty += $qty;
		$totqtytd += $qtytd;
		
		$echo .= '
    	<tr>
	        <td align="left">'.$pcode.'</td>
	        <td align="left">'.$nama.'</td>
	        <td align="right">'.$mylib->format_number($qtytd, 0, "", "", "ind").'</td>
			<td align="right">'.$mylib->format_number($belitd, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($jualtd, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($qty, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($beli, 0, "", "", "ind").'</td>
	        <td align="right">'.$mylib->format_number($jual, 0, "", "", "ind").'</td>
	    </tr>
    	';
        
    }
    
	$echo .= '
		</tbody>';
	    
	$echo .= '
         <tfoot>
            <tr style="text-align: right; font-weight: bold; color: black;">
                <td colspan="2">Total</td>
                <td align="right">'.$mylib->format_number($totqtytd, 0, "", "", "ind").'</td>
				<td align="right">'.$mylib->format_number($totbelitd, 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($tottd, 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($totqty, 0, "", "", "ind").'</td>
				<td align="right">'.$mylib->format_number($totbeli, 0, "", "", "ind").'</td>
                <td align="right">'.$mylib->format_number($tot, 0, "", "", "ind").'</td>
            </tr>
        </tfoot> 
	    ';

	$echo .= '
		</table>';

	echo $echo;
	
	$subject = "Report Penjulan NPM Ke VCI ".$v_date_from." s/d ".$v_date_to;
        
	$body = $echo;
	
	$to = "";
	$to_name = "";
	for($i=0;$i<count($listemail);$i++){
		$email_address = $listemail[$i]['email_address'];
		$email_name = $listemail[$i]['email_name'];
		
		$to .= $email_address.";";
		$to_name .= $email_name.";";
	}
	
	$author = "Cron Job";
	$mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    
?>