<?php
if($sudahada){
?>
	<table class="table table-bordered responsive" id="TabelDetail">
		<tr>
			<td><font color="red">Gagal!! Data Sudah Pernah dibuat</font></td>
		</tr>
	</table>
<?php
}else{

	if ($excel == "excel") {    	
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Generate_invoice_vci.xls"');
        }
        if ($excel != "excel") {
            ?>
            <span style="margin-bottom:10px; display: inline-table;">
                <button type="submit" name='submit' class="btn btn-info btn-icon btn-sm icon-left" onclick="$('#excel').val('excel');
                            document.getElementById('search').submit()" value="export to excel">export to excel<i class="entypo-download" ></i></button>
            </span>
            <?php
        }
		
?>
<table class="table table-bordered responsive" id="TabelDetail">
	<thead class="title_table">
		<tr>
			<th width="50"><center>PCode</center></th>
			<th><center>Nama Barang</center></th> 
			<th width="100"><center>Qty</center></th>              
			<th width="100"><center>Harga Contract</center></th>
			<th width="100"><center>Total</center></th>
			<th width="100"><center>Harga Jual </center></th>
			<th width="100"><center>Total</center></th>

	    </tr>
	</thead>
	<tbody>
		
		<?php  
		$subtotal = 0;
		$ppn = 0;
		$subtotalcontract=0;
		for($a=0 ; $a<count($data) ; $a++) {
			$total = $data[$a]['Qty'] * $data[$a]['Harga'];
			$totalcontract = $data[$a]['Qty'] * $data[$a]['HargaContract'];
			$subtotal += $total;
			$subtotalcontract += $totalcontract;
			IF($data[$a]['Harga']==0 || $data[$a]['HargaContract']==0){
				$warna = "background-color:red";
			}else{
				$warna = "background-color:white";
			}
		?>
		<tr style="<?=$warna;?>">
	        <td>
            	<?=$data[$a]['PCode'];?>
            </td>
            <td>
            	<?=$data[$a]['NamaLengkap'];?> 
            </td>
            <td>
            	<?=$data[$a]['Qty'];?>
            </td>
            <td>
            	<?=$data[$a]['HargaContract'];?> 
            </td>
            <td>
            	<?=$totalcontract;?> 
            </td>
            <td>
            	<?=$data[$a]['Harga'];?> 
            </td>
            <td>
            	<?=$total;?> 
            </td>
        </tr>
	    <?php
	    }
	    ?>
	    <tr>
	    	<td colspan="3">
	    		<?php
	    		if($Lengkap){
					echo '&nbsp;';
				}else{
					echo '<font color="red">Ada barang yang belum ada harga!!</font>';
				}
	    		?>
				</td>
	    	<td class="title_table" align="right">
	    		DPP
	    	</td>
	    	<td>
	    		<?=round($subtotalcontract);?>  
	    	</td>
	    	<td>
	    		&nbsp;
	    	</td>
	    	<td>
	    		<?=round($subtotal);?> 
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="3">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		PPN
	    	</td>
	    	<td>
	    		<?=round($subtotalcontract*0.1);?>
	    	</td>
	    	<td>
	    		&nbsp;
	    	</td>
	    	<td>
	    		<?=round($subtotal*0.1);?> 
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="3">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total
	    	</td>
	    	<td>
	    		<?=round($subtotalcontract*1.1);?>
	    	</td>
	    	<td>
	    		&nbsp;
	    	</td>
	    	<td>
	    		<?=round($subtotal*1.1);?>
	    	</td>
	    </tr>
	    <?php
	    if($Lengkap){
	    ?>
	    	<tr>
        	<td colspan="5" align="center">
            	
        	</td>
    	</tr>
		<?php	
		}
		?>
	    
	</tbody>
</table>
<?php
}
?>