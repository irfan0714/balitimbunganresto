 <?php 
	
	 //$mylib = new globallib();
	/*if($submit=="TAMPIL"){
			$this->load->view('transaksi/report_shift/reportshiftview', $data);
		}
		else if($submit=="XLS")
		{
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="reportshift.xls"');
		}      
?>*/?>
<table class="table table-bordered responsive" border="1">
        <thead class="title_table">
            <tr>

				<th width="5" align="center"><center>No</center></th>
				<th width="200"><center>Nama Karyawan</center></th>
				<th width="40"><center>Tanggal</center></th>
				<th width="50"><center>Hari</center></th>
				<th width="50"><center>Jam Masuk</center></th>
				<th width="50"><center>Jam Keluar</center></th>
				<th width="50"><center>Shift</center></th>
			</tr>
        </thead>
        <tbody>
		<?php 
			if(count($viewdata)==0) 
			{
				echo "<tr><td colspan='5' align='center'>Tidak Ada Data</td></tr>";
			}
			$no=1;
			for($i=0 ; $i<count($viewdata) ; $i++) 
			{ 
				$bgcolor	= ($i % 2 == 0)?"#C8DFE6":"WHITE";
				$tanggal	= $viewdata[$i]['Tanggal'];
				$isnewdate	= false;
				
				$shiftnya	= $viewdata[$i]['Shift'];
				
				$jammasuk	= $viewdata[$i]['TimeIn'];
				$jampulang	= $viewdata[$i]['TimeOut'];
		?>
		 
				<tr style="background:<?=$bgcolor?>">
				
				<td align="center"><?=$no;?></td>
				<td align="left"><?=$viewdata[$i]['NamaKaryawan']?></td>	
				<td align="center"><?=$viewdata[$i]['Tanggal']?></td>	
				<td align="center"><?=$viewdata[$i]['Hari']?></td>
			<?php
			//time in
				if($shiftnya == 1)
					{	?>
				<?php
						$shift1masuk 	= '08:00:00';
						
						if($jammasuk > $shift1masuk)
							{
							?>		
								<td align="center" style="background:#FFFF00"><?=$viewdata[$i]['TimeIn']?></td>
							<?php  
							}
						
						
						else
							{
							?>			
								<td align="center"  style="background:<?=$bgcolor?>"><?=$viewdata[$i]['TimeIn']?></td>
							<?php  
							}
					}
				else if($shiftnya == 2)
					{
						$shift2			= '10:00:00';
						if($jammasuk > $shift2)
							{
							?>
								<td align="center" style="background:#FFFF00"><?=$viewdata[$i]['TimeIn']?></td>
								
							<?php
							}
						
						else
							{
							?>
								<td align="center"  style="background:<?=$bgcolor?>"><?=$viewdata[$i]['TimeIn']?></td>
							<?php
							}
							
						
					}
				else if($shiftnya == 3)
					{
						$shift3			= '12:00:00';
						
						if($jammasuk > $shift3)
							{
							?>
								<td align="center" style="background:#FFFF00"><?=$viewdata[$i]['TimeIn']?></td>
						<?php
							}
						
						else
							{
						?>
								<td align="center"  style="background:<?=$bgcolor?>"><?=$viewdata[$i]['TimeIn']?></td>
						<?php
							}
						}
				else if($shiftnya == 0)
				{
					?>
					<td align="center"><?=$viewdata[$i]['TimeIn']?></td>
					<?php
				}
				?>						
						<?php
						
						//timeout
						
				if($shiftnya == 1)
					{	?>
				<?php
						$shift1pulang 	= '16:00:00';
						
						if($jampulang < $shift1pulang)
							{
							?>		
								
								<td align="center" style="background:#FFFF00"><?=$viewdata[$i]['TimeOut']?></td>
							<?php  
							}
						
						
						else
							{
							?>			
								<td align="center"  style="background:<?=$bgcolor?>"><?=$viewdata[$i]['TimeOut']?></td>
							<?php  
							}
					}
				else if($shiftnya == 2)
					{
						$shift2pulang		= '18:00:00';
						
						if($jampulang < $shift2pulang)
							{
							?>
								
								<td align="center" style="background:#FFFF00"><?=$viewdata[$i]['TimeOut']?></td>
							<?php
							}
						
						else
							{
							?>
								<td align="center"  style="background:<?=$bgcolor?>"><?=$viewdata[$i]['TimeOut']?></td>
							<?php
							}
					}
					
				else if($shiftnya == 3)
					{
						$shift3pulang		= '20:00:00';
						
						if($jampulang > $shift3pulang)
							{
							?>
								<td align="center" style="background:#FFFF00"><?=$viewdata[$i]['TimeOut']?></td>
						<?php
							}
						
						else
							{
						?>
								<td align="center"  style="background:<?=$bgcolor?>"><?=$viewdata[$i]['TimeOut']?></td>
						<?php
							}
						}
				else if($shiftnya == 0)
				{
					?>
					<td align="center"><?=$viewdata[$i]['TimeOut']?></td>
					<?php
				}
				?>
				<td align="center"><?=$viewdata[$i]['Shift']?></td>	
			</tr>
			<?php  
			$no++;
			} 
			?>         
        </tbody>
    </table>