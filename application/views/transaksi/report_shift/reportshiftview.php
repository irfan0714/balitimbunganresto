<?php $this->load->view('header_part1');
$reportlib = new report_lib();
?>

	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	<script>
	$(document).ready(function(){
		$('#tgldari').Zebra_DatePicker({format: 'Y-m-d'});
        $('#tglsampai').Zebra_DatePicker({format: 'Y-m-d'});
	});
	</script>
	<script>
    function submitThis()
    {
        $("#excel").val("");
        $("#print").val("");
        document.getElementById("search").submit();
    }
</script>
</head>
<?php $this->load->view('header_part3')?>

<form method="POST"  name="search" action='<?=base_url(); ?>/index.php/hr/reportshift/view'>

    <div class="row">
        <div class="col-md-8">
			<table>
				<tr>
					<td>
						Range Tanggal&nbsp;&nbsp;Dari
					</td>
					<td>
							&nbsp;&nbsp;<input type='text' name='tgldari'  value="<?=$tgldari?>" size='12' id="tgldari"  class="form-control-new" readonly />
						&nbsp;Sampai&nbsp;
						&nbsp;<input type='text' name='tglsampai' value="<?=$tglsampai?>" size='12' id="tglsampai" class="form-control-new" readonly />
						
					&nbsp;<input type="submit" name='submit' value="TAMPIL" class='btn btn-default'>
					&nbsp;<input type="submit" name='submit' value="XLS"  id="excel" class='btn btn-default' >
					</td>&nbsp;
				</tr>
				<tr>
					<td>
						Nama Karyawan&nbsp;
					</td>
					<td>
						&nbsp;&nbsp;<input type="text" name='namakaryawan' value="<?=$nama?>"  id="namakaryawan" class="form-control-new" />
					</td>
				</tr>
			</table>
        </div>
        <div class="col-md-4" align="right">
        </div>
    </div>
</form>

<?php

if ($tampilkanDT) 
{
    $this->load->view("transaksi/report_shift/tampil");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>