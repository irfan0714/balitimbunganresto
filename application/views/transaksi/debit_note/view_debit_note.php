<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Debit Note";

$counter=1;
foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script language="javascript" src="<?=base_url();?>public/js/debit_note.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/debit_note/save_data'>
	    <input type="hidden" name="v_dnno" id="v_dnno" value="<?php echo $header->dnno; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->dnno; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal Credit Note </td>
	            <td> 
					
					<input type="text" class="form-control-new datepicker" value="<?php if($header->dndate_indo!="" && $header->dndate_indo!="0000-00-00") { echo $header->dndate_indo; }else{echo date('d-m-Y');}  ?>" name="v_tgl_debit_note" id="v_tgl_debit_note" size="10" maxlength="10" disabled="true">	
	            </td>
	        </tr>
	        
	           <tr>
		            <td class="title_table">Tipe</td>
		            <td>
		            	<select class="form-control-new" name="v_type" id="v_type" style="width: 25%;" disabled="true">
		            		
		            		<option value="">- Pilih Tipe -</option>
		            		<option <?php if($header->dntype=="C"){ echo "selected='selected'"; } ?> value="C">Credit Note</option>
		            		<option <?php if($header->dntype=="D"){ echo "selected='selected'"; } ?> value="D">Debit Note</option>
		            	</select>
		            </td>
		        </tr>
		        
		        <tr>
		            <td class="title_table">Nama Rekening</td>
		            <td>
		            	<input type="text" class="form-control-new" value="<?=$header->KdRekening." - ".$header->NamaRekening;?>" name="v_rekening" id="v_rekening" maxlength="255" size="48%" onkeyup="cariRek()">
		            </td>
		        </tr>
	        
	        <!--<tr>
	            <td class="title_table">Penjual </td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 25%;">
	            		<option value="">- Pilih Penjual -</option>
	            		<?php
	            		foreach($msupplier as $val)
	            		{
	            			$selected="";
							if($header->supplierid==$val["KdSupplier"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>-->
			
			<tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Mata Uang", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="48" disabled="true"></td>
	        </tr>
	        
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;" disabled="true">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Close</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
				
				<?php if($header->purchasereturnno!=""){?>
				<tr >
		            <td bgcolor="red"><font color="white"><b>Purchase Number</b></font></td>
		            <td><b><?php echo $header->purchasereturnno; ?></b></td>
		        </tr>
				<?php } ?>
				
				
			<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor Rekening</center></th>
								<th><center>Nama Rekening</center></th> 
								<th width="200"><center>SubDivisi</center></th>
							    <th width="200"><center>Deskripsi</center></th>
							    <th width="200"><center>Amount</center></th>
							    
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($datado)<=0){
						 	echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
						 }else{
						 	$no=0;
						 	foreach($datado as $val){
						 		$no		= $no + 1;
								?>
								<tr>
									<td><?php echo $val['coano']; ?>
									
										<input type="hidden" class="form-control-new" name="v_dndid[]" id="v_dndid<?php echo $no;?>" value="<?php echo $val['dndid']; ?>" style="width: 100%;"/>
										<input type="hidden" class="form-control-new" name="v_coano[]" id="v_coano<?php echo $no;?>" value="<?php echo $val['coano']; ?>" style="width: 100%;"/>
									</td>
									<td><?php echo $val['NamaRekening']." - ".$val["KdSubDivisi"]; ?></td>
									
									<td>
										<select class="form-control-new" name="v_subdivisi[]" id="v_subdivisi<?php echo $no;?>" style="width: 200px;" disabled="true">
						            		<option value="">Pilih Subdivisi</option>
						            		<?php
						            		foreach($subdivisi as $val2)
						            		{
						            			$selected="";
												if($val["KdSubDivisi"]==$val2["KdSubdivisi"])
												{
													$selected='selected="selected"';
												}
												?><option <?php echo $selected; ?> value="<?php echo $val2["KdSubdivisi"]; ?>"><?php echo $val2["NamaSubDivisi"]; ?></option><?php
											}
						            		?>
						            	</select> 
									</td>
									
									<td>
										<input type="text" class="form-control-new" name="v_dekripsi[]" id="v_deskripsi<?php echo $no;?>" value="<?php echo $val['description']; ?>"  style="width: 100%;" disabled="true"/>
									</td>
									<td align='right'>
										<input type="text" class="form-control-new" name="v_amount[]" id="v_amount<?php echo $no;?>" value="<?php echo $val['value']; ?>" onkeypress="hitung(event,'<?=$no;?>');" style="text-align: right; width: 100%;" disabled="true"/>
									</td>
									
								</tr>
								<?php
							}
						 }
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
		   
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/debit_note/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<!--<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>-->
						<font style="color: red; font-style: italic; font-weight: bold;">View Only.</font>
					</td>
		        </tr>
				
		
	        
	    </table>
		
			
		<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data Credit Note</li>
			</ol>
			
	         <table class="table table-bordered responsive">
			 <?php 
			 if($hitungdo->discpercent==0){
			 	$potongan_diskon_=0;
			 }else{
			 	$potongan_diskon_=($hitungdo->discpercent/100)*$hitungdodetail->amountdetail;
			 }
			 
			 $potongan_ppn_=($hitungdo->vatpercent/100)*$hitungdodetail->amountdetail;
			 $gradtotal_=($hitungdodetail->amountdetail-$potongan_diskon_)+$potongan_ppn_;
			 
			 ?>
	            <tr>
	            	<td align="right" width="87%">Total</td>
		            <td nowrap align="right">
		            	<input type="text" name="total" id="total" class="form-control-new" value="<?php echo number_format($hitungdodetail->amountdetail,0);?>"  maxlength="255" size="50%" style="text-align: right;" disabled="true">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">Discount</td>
		            <td align="right">
		            	<input type="text" name="diskon" id="diskon" class="form-control-new" value="<?php echo number_format($hitungdo->discpercent,0);?>"  maxlength="255" size="5%" style="text-align: right;" disabled="true">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_diskon" id="potongan_diskon" class="form-control-new" value="<?php echo number_format($potongan_diskon_,0);?>"  maxlength="255" size="30%" style="text-align: right;" disabled="true">		            		            	
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">PPN</td>
		            <td nowrap align="right">
		            	<input type="text" name="ppn" id="ppn" class="form-control-new" value="<?php echo number_format($hitungdo->vatpercent,0);?>"  maxlength="255" size="5%" style="text-align: right;" disabled="true">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_ppn" id="potongan_ppn" class="form-control-new" value="<?php echo number_format($potongan_ppn_,0);?>"  maxlength="255" size="30%" style="text-align: right;" disabled="true">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150"><b>Grand Total</b></td>
		            <td align="right">
		            	<input type="text" name="grandtotal" id="grandtotal" class="form-control-new" value="<?php echo number_format($gradtotal_,0);?>"  maxlength="255" size="50%" style="text-align: right;" disabled="true">
		            	<input type="hidden" name="grandtotalhidden" id="grandtotalhidden" class="form-control-new" value="<?php echo $gradtotal_;?>"  maxlength="255" size="50%" style="text-align: right;" disabled="true">
		            </td>
	            </tr>
	         </table>
		
	    </form> 
	    
	    <?php
        if($header->dnno)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>