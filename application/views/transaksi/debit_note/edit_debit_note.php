<?php 
$this->load->view('header'); 
$this->load->library('globallib');
$user = $this->session->userdata('username');
$mylib = NEW Globallib;

$modul = "Debit Note";

?>
<script language="javascript" src="<?=base_url();?>public/js/debit_note.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		
		if(!empty($header->purchasereturnno)){
			    ?><div class="alert alert-<?php echo 'danger';?>"><?php echo "<b>PENTING :</b> Debit Note Ini Sudah Mempunyai No Purchase Return, Jika ingin diubah maka ubah dengan Amount yang sama."; ?></div><?php
              }
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/debit_note/save_data'>
	    <input type="hidden" name="v_dnno" id="v_dnno" value="<?php echo $header->dnno; ?>">
	    <input type="hidden" id="cekvalidasi" name="cekvalidasi" value="0">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->dnno; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal Credit Note </td>
	            <td> 
		            <input type="text" class="form-control-new datepicker" value="<?php if($header->dndate_indo!="" && $header->dndate_indo!="00-00-0000") { echo $header->dndate_indo; }else{echo date('d-m-Y');}  ?>" name="v_tanggal_dokumen" id="v_tanggal_dokumen" size="10" maxlength="10">	
	            </td>
	        </tr>
	        
	        <tr>
		            <td class="title_table">Tipe</td>
		            <td>
		            	<select class="form-control-new" name="v_type" id="v_type" style="width: 25%;">
		            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
		            		<option <?php if($header->dntype=="C"){ echo "selected='selected'"; } ?> value="C">CN Hutang Dagang</option>
		            		<option <?php if($header->dntype=="D"){ echo "selected='selected'"; } ?> value="D">DN Hutang Dagang</option>
		            		<!--<option <?php if($header->dntype=="CM"){ echo "selected='selected'"; } ?> value="CM">CN Hutang Biaya</option>
		            		<option <?php if($header->dntype=="DM"){ echo "selected='selected'"; } ?> value="DM">DN Hutang Biaya</option>-->
		            	</select>
		            </td>
		        </tr>
		        
		        <tr>
		            <td class="title_table">Nama Rekening</td>
		            <td>
		            	<input type="text" class="form-control-new" value="<?=$header->KdRekening." - ".$header->NamaRekening;?>" name="v_rekening" id="v_rekening" maxlength="255" size="48%" onblur="validasi()" onkeyup="cariRek()">
		            	<span id="benar" style="display: none;">
		            	<img src="../../../../public/images/accept.png"/>
		            	</span>
		            	<span id="salah" style="display: none;">
		            	<img src="../../../../public/images/cancel.png"/>
		            	</span>
		            </td>
		        </tr>
		        
		        <!--<tr>
	            <td class="title_table">Subdivisi</td>
	            <td> 
	            	<select class="form-control-new" name="v_subdivisi" id="v_subdivisi" style="width: 25%;">
	            		<option value="">Pilih Subdivisi</option>
	            		<?php
	            		foreach($subdivisi as $val)
	            		{
	            			$selected="";
							if($header->KdSubdivisi==$val["KdSubdivisi"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSubdivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">Penjual </td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 25%;">
	            		<option value="">- Pilih Penjual -</option>
	            		<?php
	            		foreach($msupplier as $val)
	            		{
	            			$selected="";
							if($header->supplierid==$val["KdSupplier"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
			
			<tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Mata Uang", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="48"></td>
	        </tr>
	        
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Close</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
				
				<?php if($header->purchasereturnno!=""){?>
				<tr >
		            <td bgcolor="red"><font color="white"><b>Purchase Number</b></font></td>
		            <td><b><?php echo $header->purchasereturnno; ?></b></td>
		        </tr>
				<?php } ?>
				
				<?php
			if(count($datado)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor Rekening</center></th>
								<th><center>Nama Rekening</center></th> 
								<th width="200"><center>SubDivisi</center></th>
							    <th width="200"><center>Deskripsi</center></th>
							    <th width="200"><center>Amount</center></th>
							    <?php if($header->status==0 OR $user=="i2911" OR $user=="mechael0101" OR $user=="yunita1006"){ ?>
							    <th width="50"><center>
							    	Action</center>
							    </th>
							    <?php } ?>
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($datado)<=0){
						 	echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
						 }else{
						 	$no=0;
						 	foreach($datado as $val){
								?>
								<tr>
									<td><?php echo $val['coano']; ?></td>
									<td><?php echo $val['NamaRekening']; ?></td>
									
									<td>
										<select class="form-control-new" style="width: 200px;">
						            		<option value="">Pilih Subdivisi</option>
						            		<?php
						            		foreach($subdivisi as $val2)
						            		{
						            			$selected="";
												if($val["KdSubDivisi"]==$val2["KdSubdivisi"])
												{
													$selected='selected="selected"';
												}
												?><option <?php echo $selected; ?> value="<?php echo $val2["KdSubdivisi"]; ?>"><?php echo $val2["NamaSubDivisi"]; ?></option><?php
											}
						            		?>
						            	</select> 
									</td>
									
									<td><?php echo $val['description']; ?></td>
									</td>
									<td align='right'><?php echo number_format($val['value'],0); ?></td>
									</td>
									<?php if($header->status==0 OR $user=="i2911" OR $user=="mechael0101" OR $user=="yunita1006"){ ?>
									<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail2('<?php echo $val["dndid"]; ?>','<?php echo $val["coano"]; ?>','<?php echo $val["dnno"]; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
					                <?php } ?>
								</tr>
								<?php
							}
						 }
						?>	
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        
	        <?php
	        }
	        ?>
	        
	        <tr>
	        	<td colspan="100%"><b>Tambah Baru</b></td>
	        </tr>
	        
	        
			
			<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor Rekening</center></th>
								<th><center>Nama Rekening</center></th>
								<th width="200"><center>SubDivisi</center></th> 
							    <th width="200"><center>Deskripsi</center></th>
							    <th width="200"><center>Amount</center></th>
							    <th width="50"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button>
							    	</center>
							    </th>
							</tr>
						</thead>
						<tbody>
						<?php
						 	$no=1;
								?>
								<tr id="baris<?php echo $no; ?>">
									<td>
									<nobr>
				                	<input type="text" class="form-control-new" name="v_coano[]" id="v_coano<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_coano<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>										
									</td>
									<td>
										<input type="text" class="form-control-new" name="v_namarekening[]" id="v_namarekening<?php echo $no;?>" value="" style="width: 400px;"/>
									</td>
									<td>
										<select class="form-control-new" name="v_subdivisi[]" id="v_subdivisi<?php echo $no;?>" style="width: 200px;">
						            		<option value="">Pilih Subdivisi</option>
						            		<?php
						            		foreach($subdivisi as $val)
						            		{
												?><option value="<?php echo $val["KdSubdivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
											}
						            		?>
						            	</select> 
									</td>
									
									<td>
										<input type="text" class="form-control-new" name="v_dekripsi[]" id="v_deskripsi<?php echo $no;?>" value=""  style="width: 100%;"/>
									</td>
									<td>
										<input type="text" class="form-control-new" name="v_amount[]" id="v_amount<?php echo $no;?>" value="" onkeypress="hitung(event,'<?=$no;?>');" style="text-align: right; width: 100%;" />
									</td>
									<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
					                </td>
								</tr>
								<?php
						 
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
		   
			<?php
			if($header->status==0)
			{
			?>
				        	        
	         <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/debit_note/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform2();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        
		        
		        <?php if($user=="i2911" OR $user=="mechael0101" OR $user=="gracia0308"){?>
		        
		        <tr>
		            <td colspan="100%" align="center">
						<input type='hidden' name="flag" id="flag" value="edit">
						<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
		                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/debit_note/"; ?>">Keluar<i class="entypo-cancel"></i></button>
	                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform2();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
					</td>
		        </tr>
		        
		        <?php }else{?>
		        
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/debit_note/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>
					</td>
		        </tr>
		        
		        <?php } ?>
		        
				<?php
			}
	        ?>
		
	        
	    </table>
		
			
		<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data Credit Note</li>
			</ol>
			
	         <table class="table table-bordered responsive">
			 <?php 
			 if($hitungdo->discpercent==0){
			 	$potongan_diskon_=0;
			 }else{
			 	$potongan_diskon_=($hitungdo->discpercent/100)*$hitungdodetail->amountdetail;
			 }
			 
			 $potongan_ppn_=($hitungdo->vatpercent/100)*$hitungdodetail->amountdetail;
			 $gradtotal_=($hitungdodetail->amountdetail-$potongan_diskon_)+$potongan_ppn_;
			 
			 ?>
	            <tr>
	            	<td align="right" width="87%">Total</td>
		            <td nowrap align="right">
		            	<input type="text" name="total" id="total" class="form-control-new" value="<?php echo number_format($hitungdodetail->amountdetail,0);?>"  maxlength="255" size="50%" style="text-align: right;">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">Discount</td>
		            <td align="right">
		            	<input type="text" name="diskon" id="diskon" class="form-control-new" value="<?php echo number_format($hitungdo->discpercent,0);?>"  maxlength="255" size="5%" style="text-align: right;">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_diskon" id="potongan_diskon" class="form-control-new" value="<?php echo number_format($potongan_diskon_,0);?>"  maxlength="255" size="30%" style="text-align: right;">		            		            	
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">PPN</td>
		            <td nowrap align="right">
		            	<input type="text" name="ppn" id="ppn" class="form-control-new" value="<?php echo number_format($hitungdo->vatpercent,0);?>"  maxlength="255" size="5%" style="text-align: right;">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_ppn" id="potongan_ppn" class="form-control-new" value="<?php echo number_format($potongan_ppn_,0);?>"  maxlength="255" size="30%" style="text-align: right;">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150"><b>Grand Total</b></td>
		            <td align="right">
		            	<input type="text" name="grandtotal" id="grandtotal" class="form-control-new" value="<?php echo number_format($gradtotal_,0);?>"  maxlength="255" size="50%" style="text-align: right;">
		            	<input type="hidden" name="grandtotalhidden" id="grandtotalhidden" class="form-control-new" value="<?php echo $gradtotal_;?>"  maxlength="255" size="50%" style="text-align: right;">
		            </td>
	            </tr>
	         </table>
		
	    </form> 
	    
	    <?php
        if($header->dnno)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>
<script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.css"/>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	
	var listrek = '';
	
	$(function() {
  		var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: ''}
	     }).done(function(reks){
	     	 listrek = reks.split('#');
	     });;
	});
	
	function cariRek(){
		rek = $("#v_rekening").val();
		$("#benar").css("display","none");
		$("#salah").css("display","none");
		$("#cekvalidasi").val('0');
		
		$("#v_rekening").autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
   				 $("#v_rekening").val(value.substr(0,8));  
   				 //$("#v_namarekening").autocomplete("destroy");
			}
    	});		
		
	};
	
	function validasi(){
		rek = $("#v_rekening").val(); 
		pisah = rek.split('-');
		kdrekening = pisah[0];	
		var baseurl = "<?= base_url();?>index.php/transaksi/debit_note/cekrekening" ;
		
		$.ajax({
					url: baseurl,
					data: {kdrekening:kdrekening},
					type: "POST",
					dataType: 'JSON',					
					success: function(res)
					{
						//alert(res.success);
						if(res.success==true){
							$("#benar").css("display","");
							$("#cekvalidasi").val('0');
						}else{
							$("#salah").css("display","");
							$("#cekvalidasi").val('1');
						}
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
	};
	
	/*
	function cekTheform2()
	{
		var yesSubmit = true;
    	url = $("#base_url").val();
		tanggal = $("#v_tanggal_dokumen").val();
		$.ajax({
			url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
			data: {tgl:tanggal,jenis:'Kas'},
			type: "POST",
			dataType: 'json',					
			success: function(data)
			{
				if(data=='0'){
					alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
					document.getElementById('v_tanggal_dokumen').focus();
					yesSubmit = false;
				}else{
					yesSubmit = true;
				}	
			},
			error: function(e) 
			{
				//aler
			},
			async:false 
	 	});        
        if(yesSubmit)
        {
			//alert("Hello");
			document.getElementById("theform").submit();	
		}
	}
	*/
	
	function cekTheform2()
{
	var pcode1 = document.getElementsByName("pcode[]").length;
	
	if(document.getElementById("v_type").value=="")
    {
        alert("Tipe harus dipilih");
        document.getElementById("v_type").focus();
        return false;
    }
    else if(document.getElementById("v_rekening").value=="")
    {
        alert("Account Rekening harus dipilih");
        document.getElementById("v_rekening").focus();
        return false;
    }
    else if(document.getElementById("cekvalidasi").value=="1")
    {
        alert("Account Rekening harus dipilih");
        document.getElementById("v_rekening").focus();
        return false;
    }
	else if(document.getElementById("v_supplier").value=="")
    {
        alert("Penjual harus dipilih");
        document.getElementById("v_supplier").focus();
        return false;
    }
    else
    {
    	var yesSubmit = true;
    	url = $("#base_url").val();
		tanggal = $("#v_tanggal_dokumen").val();
		$.ajax({
			url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
			data: {tgl:tanggal,jenis:'Kas'},
			type: "POST",
			dataType: 'json',					
			success: function(data)
			{
				if(data=='0'){
					alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
					document.getElementById('v_tanggal_dokumen').focus();
					yesSubmit = false;
				}else{
					yesSubmit = true;
				}	
			},
			error: function(e) 
			{
				//aler
			},
			async:false 
	 	});        
        if(yesSubmit)
        {
			//alert("Hello");
			document.getElementById("theform").submit();	
		}
	}
}
	
</script>