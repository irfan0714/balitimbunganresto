<?php
$mylib = new globallib();
$this->load->helper('terbilang');

$total_tambahan=0;
foreach($getTambahan AS $valx){
$total_tambahan+=$valx['Nilai'];
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Reservasi</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 8pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 8pt;
		}
		
	</style>
    <body>
        <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
                <td colspan="5" align="center"><img src="<?= base_url(); ?>public/images/Logosgs.png" width="400" alt=""/></td>
            </tr>
            <tr>
                <td colspan="5" align="center"><b></b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><h3>INVOICE</h3></td>
            </tr>
            <br><br>
            <tr>
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td width="150">Bills To&nbsp;&nbsp;:</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150"><b><?=$getHeader->Customer;?></b></td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                                                
                        <tr>
                            <td width="150"><?=$getHeader->Alamat;?></td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150"><?=$getHeader->Tlp;?></td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                    </table>
                </td>
                <td></td>
                
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<!-- <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr> -->
                        
                        <tr>
                            <td width="100">Invoice Date</td>
                            <td width="1">:</td>
                            <td width="150"><?=$mylib->ubah_tanggal($getHeader->TglTrans);?></td>

                        </tr>
                                                
                        <tr>
                            <td width="100">Invoice Number</td>
                            <td width="1">:</td>
                            <td width="150"><?=$getHeader->NoDokumen;?></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Due Date</td>
                            <td width="1">:</td>
                            <td width="150"><?=$mylib->ubah_tanggal($getHeader->Jatuh_Tempo);?></td>

                        </tr>
                    </table>
                </td>
                
            </tr>
            
        </table>
        <br>
        
        
        <table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="non-border-table" >
                        <tr>
                            <th width="200">Arrival Date</th>
                            <th>Descriptions</th>
							<th width="200">Total Value (Rp)</th>
                        </tr>
                        
                        <?php $tot=0;$tgl="";foreach($getDetail AS $val){
                        	if($tgl==""){
							   $tanggals = $mylib->ubah_tanggal($val['Tanggal']);
							}else if($tgl==$mylib->ubah_tanggal($val['Tanggal'])){
							   $tanggals = "";
							}else if($tgl!=$mylib->ubah_tanggal($val['Tanggal'])){
							   $tanggals = $mylib->ubah_tanggal($val['Tanggal']);
							}
                        	?>
                        <tr>
                            <td width="200" align="center"><?php echo $tanggals;?></td>
                            <td align="left"><?php echo $val['NamaLengkap']." ".$val['Keterangan'];?></td>
							<td width="200" align="right"><?php echo number_format($val['Nettoz']);?></td>
                        </tr>
                        <?php $tot+=$val['Netto'];$tgl=$mylib->ubah_tanggal($val['Tanggal']);} ?>
                        <tr>
                            <td align="right" colspan="2"><b>Total</b></td>
                            <!-- <td width="200" align="right"><b><?php echo number_format($tot);?></b></td> -->
                            <td width="200" align="right"><b><?= number_format($getHeader->Total_Nilais);?></b></td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2"><b>Down Payment</b></td>
                            <!-- <td width="200" align="right"><b><?php echo number_format($tot);?></b></td> -->
                            <td width="200" align="right"><b><?= number_format($getHeader->Total_Dp);?></b></td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2"><b>Grand Total</b></td>
							<!-- <td width="200" align="right"><b><?php echo number_format($tot);?></b></td> -->
                            <td width="200" align="right"><b><?= number_format(floatval($getHeader->Total_Nilais)-floatval($getHeader->Total_Dp) );?></b></td>
                        </tr>
                        <tr>
                        	<td  colspan="3" align="left"><b>Be Regarded &nbsp;&nbsp;:&nbsp;&nbsp; <?php echo terbilang((floatval($getHeader->Total_Nilais)-floatval($getHeader->Total_Dp) ))." Rupiah"; ?></b></td>
                        </tr>
        </table>
                    
  <br>                 
<table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            
            <tr>
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td colspan="100%"><b>TERM OF PAYMENT &nbsp;&nbsp;:</b></td>
                            <td width="1"></td>
                            <td colspan="100%"></td>

                        </tr>
                        
                        <tr>
                            <td colspan="100%">Payment Cash At Secret Garden Village</td>
                            <td width="1"></td>
                            <td colspan="100%"></td>

                        </tr>
                                                
                        <tr>
                            <td width="150">Or</td>
                            <td width="1"></td>
                            <td colspan="100%"></td>

                        </tr>
                        
                        <tr>
                            <td colspan="100%">Please Kindly Wire The Payment To Our Bank Account Below</td>
                            <td width="1">:</td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">BANK NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">BANK MANDIRI ( Cabang Tabanan )</td>

                        </tr>
                        
                        <tr>
                            <td width="150">ACCOUNT NUMBER</td>
                            <td width="1">:</td>
                            <td width="150">175.0000.260793</td>

                        </tr>
                        
                         <tr>
                            <td width="150">ACCOUNT NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">PT. NATURA PESONA MANDIRI</td>

                        </tr>
                        
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">BANK NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">BANK BCA ( Cabang Tabanan )</td>

                        </tr>
                        
                        <tr>
                            <td width="150">ACCOUNT NUMBER</td>
                            <td width="1">:</td>
                            <td width="150">142.042.4244 ( IDR )</td>

                        </tr>
                        
                         <tr>
                            <td width="150">ACCOUNT NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">PT. NATURA PESONA MANDIRI</td>

                        </tr>
                        
                    </table>
                </td>
                <td></td>
                
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                                                
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                    </table>
                </td>
                
            </tr>
            
        </table>
        
        <br><br>
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table" >
        	<tr>
        		<td align="center">Prepared By,</td>
        		<td align="center" style="display:none;"></td>
        		<td align="center">Approved By,</td>
        	</tr>
        	
        	<tr>
        		<td align="center" height="50">&nbsp;</td>
        		<td align="center" height="50" style="display:none;">&nbsp;</td>
        		<td align="center" height="50">&nbsp;</td>
        	</tr>
        	
        	<tr>
        		<td align="center">Mela Gena Yani</td>
        		<td align="center" style="display:none;"></td>
        		<td align="center">Bambang Sutrisno</td>
        	</tr>
        	<tr>
        		<td align="center">Staff Finance</td>
        		<td align="center" style="display:none;"></td>
        		<td align="center">Sr. FA & Adm Manager</td>
        	</tr>
        </table>

</body>
</html>