<?php 
$this->load->view('header');
$mylib = new globallib();
 ?>
<head>
</head>
<!--<script language="javascript" src="<?= base_url(); ?>public/js/uang_muka_beo33.js"></script>-->


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/invoice_non_reservasi/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" /> 
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Close</option>
            </select>  
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="button" class="btn btn-success btn-sm icon-left" onClick="refresh()">Refresh</button>
            <button type="button" class="btn btn-warning btn-icon btn-sm icon-left" onClick="cari_data()">Search<i class="entypo-search"></i></button>
            <!--<a href="<?php echo base_url() . "index.php/transaksi/uang_muka_beo/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>-->
            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="create()">Create Invoice<i class="entypo-plus"></i></button>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
   <div id="getList"></div>
</div>


			<div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <center><!--<h1><b>Page Loading...</b></h1><br>-->
                            <img src="<?= base_url() ?>public/images/dot_loader.gif" />
                        </center>
                        </div>
                    </div>
                </div>
            </div>


<div id="modal_invoice_method" class="modal fade" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Invoice</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="form-body">
                        
                        <div style="display: none;" class="form-group">
                            <label class="control-label col-md-3">No. Invoice</label>
                            <div class="col-lg-12">
                            <input type="text" class="form-control-new" style="width: 50%;" maxlength="30" name="v_no_invoice" id="v_no_invoice" >
                        </div>
                        </div>
                        
                        
                        
                        <div class="table-responsive">
              <table class="table table-bordered responsive">
                <tbody>
                  <?php $tgl1=date('Y-m-d');$tgl2 = date('d-m-Y', strtotime('+14 days', strtotime($tgl1))); ?>
                  <tr>
                    <td class="title_table" width="150">Tgl Jatuh Tempo</td>
                    <td>
					<input type="text" class="form-control-new datepicker" size="10" value="<?php echo $tgl2; ?>" name="tgl_jatuh_tempo" id="tgl_jatuh_tempo">
          <input type="text" class="form-control-new datepicker" size="10" value="" name="tgltrans" id="tgltrans">

					</td>
                  </tr>
                
                  <tr>
                    <td class="title_table" width="150">Customer</td>
                    <td>
					<input type="text" class="form-control-new" size="50" name="customer" id="customer" placeholder="Michael Jackson">
					</td>
                  </tr>
                  
                  <tr>
                    <td class="title_table" width="150">Address</td>
                    <td>
					<input type="text" class="form-control-new" size="100" name="alamat" id="alamat" placeholder="Tabanan, Bali Indonesia">
					</td>
                  </tr>
                  
                  <tr>
                    <td class="title_table" width="150">No. Tlp</td>
                    <td>
					<input type="text" class="form-control-new" size="20" name="tlp" id="tlp" placeholder="0123456789">
					</td>
                  </tr>
                  
                  <tr>
                    <td class="title_table" width="150">Detail Struk</td>
                    <td>
					<input type="text" class="form-control-new" size="100" name="detail_struk" id="detail_struk" placeholder="18011299001#18011299002#18011299001#">
					        <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Cek" title="" onclick="cek('<?= base_url(); ?>');">
								<i class="entypo-search"></i>Cek&nbsp;&nbsp;
							</button><br><font size='2' color="red"><b>Formatnya nostruk1# atau nostruk1#nostruk2#nostruk3#</b></font>
					</td>
                  </tr>
                  
                   <tr id="loading" style="display: none;">
                    <td colspan="100%" align="center"><h1><b>Pencarian Data...</b></h1><br><img src="<?= base_url() ?>public/images/dot_loader.gif" /></td>
                   </tr>
                  
                  </tbody>
                  </table>
                  </div>
                  
                  
                  
              <div id="list" style="display: none" class="table-responsive">
                  					
              <table class="table table-bordered responsive" >
       		 			<thead class="title_table">
							<tr>
							    <th width="10"><center>No</center></th>
							    <th width="50"><center>No.Struk</center></th>
							    <th width="50"><center>PCode</center></th>
		                        <th><center>Nama Barang</center></th>
		                        <th width="20"><center>Qty</center></th>		                        
		                        <th width="50"><center>Harga</center></th>
							    <th width="50"><center>Diskon</center></th>
							    <th width="50"><center>Netto</center></th>
							</tr>
						</thead>
						<tbody id="detail_invoice" border="1">
						</tbody>
					</table>
					
					<hr>
					<table class="table table-striped table-bordered" id="TabelDetail1">
		                <thead class="title_table" style="max-width: 100%;">
		                  <tr>
		                    <th><center>Biaya Tambahan</center></th>
		                    <th width="120"><center>Harga</center></th>
		                    <th width="50"><center>
									    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew_detail()">
												<i class="entypo-plus"></i>
											</button></center>
							</th>
		                  </tr>
		                </thead>
		                
		                <tbody>
		                  <?php $no=1; ?>
									
						  <tr id="baris_detail<?php echo $no; ?>">
		                    <td>
							<input type="text" class="form-control-new" size="100" name="v_ket_tambahan[]" id="v_ket_tambahan<?php echo $no;?>" placeholder="Keterangan">
							</td>
							<td>
							<input style="text-align: right;" type="text" class="form-control-new" size="20" name="v_harga_tambahan[]" id="v_harga_tambahan<?php echo $no;?>" placeholder="Harga" onkeyup="Hitung(this)">
							</td>
							<td align="center">
						                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_detail_<?php echo $no;?>]" id="btn_del_detail_detail_<?php echo $no;?>" value="Save" onclick='deleteRow_detail(this)'>
												<i class="entypo-trash"></i>
											</button>
						    </td>
		                  </tr>		                  
		                </tbody>  
		                
					  </table>
					  				  
					  <table class="table table-bordered responsive">
					       <tr bgcolor="#f2df96">
							    <td width="86%" align="right">Total Menu</td>
								<td align='right'><input type="text" class="form-control-new" readonly style="text-align: right; background:#f2df96; " id="totalan_menu" name="totalan_menu" value="0"> <input type="hidden" class="form-control-new" readonly style="text-align: right; background:#f2df96; " id="totalan_menu2" name="totalan_menu2" value="0"></td>	  	
							</tr>
							 <tr bgcolor="#f9d43e">
                  <td width="86%" align="right">Down Payment</td>
                <td align='right'><input type="text" class="form-control-new" readonly style="text-align: right; background:#f9d43e; " id="totalan_tambahan" name="totalan_tambahan" value="0"></td>      
              </tr>
							<tr bgcolor="#f9d43e">
							    <td width="86%" align="right">Total Biaya Tambahan</td>
								<td align='right'><input type="text" class="form-control-new" readonly style="text-align: right; background:#f9d43e; " id="totalan_tambahan" name="totalan_tambahan" value="0"></td>	  	
							</tr>
							
							<tr bgcolor="#f2c204">
							    <td width="86%" align="right">Grand Total</td>
								<td align='right'><input type="text" class="form-control-new" readonly style="text-align: right; background:#f9d43e; " id="grand_totals" name="grand_totals" value=""></td>	  	
							</tr>
					  </table>
					
                  </div>
                  
                  
                         
                    </div>
                </form>
                
                <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-info btn-icon btn-sm icon-left">Create<i class="entypo-check"></i></button>
                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" data-dismiss="modal">Cancel<i class="entypo-cancel"></i></button>
                </div>
                <br><br><br>
            
            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

	$(document).ready(function()
		{
			getData();				
		});
		
	
	function getData(){
		base_url = $("#baseurl").val();
			
			$('#pleaseWaitDialog').modal('show');
			
	    	$.ajax({
				type: "POST",
				url: base_url + "index.php/transaksi/invoice_non_reservasi/getList/",
				success: function(data) {
					$('#getList').html(data);
					
						$('#pleaseWaitDialog').modal('hide');
					
			
				}
			});
	}

	function create()
    {
     save_method = 'add';
     
     document.getElementById("list").style.display = "none";
     $('#v_keyword_travel').val("");
     $('#v_travel').val("");
     $('#v_beo').val("");
     
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        		
        		$('[name="v_no_invoice"]').val('');
                $('#modal_invoice_method').modal('show');
                $('#btnSave').attr('disabled',false);
                $('.modal-title').text('Create Invoice Non Reservasi');
                
                
    }
    
    function cek()
    {
       if($('#customer').val()==""){
	   		alert("Nama Customer Belum Diisi.");
	   		$('#customer').focus();
	   		return false;
	   }else if($('#alamat').val()==""){
	   		alert("Alamat Belum Diisi.");
	   		$('#alamat').focus();
	   		return false;
	   }else if($('#tlp').val()==""){
	   		alert("Telpon/HP Belum Diisi.");
	   		$('#tlp').focus();
	   		return false;
	   }else if($('#detail_struk').val()==""){
	   		alert("Nomor Struk Belum Diisi.");
	   		$('#detail_struk').focus();
	   		return false;
	   }
       
    	
       $('#btnSave').attr('disabled', true);
       document.getElementById("loading").style.display = "";
       var url = "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_cek') ?>";
	   beo = $('#v_beo').val();
		 $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "html",
            async: false,
            success: function (res)
            {
    
                $('#detail_invoice').html(res);
                //$('#totalan_menu').val($('#totalan').val());
                
                if($('#totalannet').val()==0 || $('#totalannet').val()=="" || $('#totalannet').val()=='0'){
					$('#totalannet').val(0);
				}
				
                $('#totalan_menu').val($('#totalannet2').val());
                $('#totalan_menu2').val($('#totalannet2').val());
                $('#grand_totals').val($('#totalannet2').val());
                                
                document.getElementById("list").style.display = "";
                $('#btnSave').attr('disabled', false);
				document.getElementById("loading").style.display = "none";
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#modal_invoice_method').modal('hide');
                $('#btnSave').attr('disabled', false);

            }
        });
        
    }
    
    function edit_invoice(id)
    {
     
     	save_method = 'edit';
     	document.getElementById("list").style.display = "";
     	
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        //nodok = id.replace(/\//g, "-");
        nodok = id;
        $.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_edit_invoice') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       
            {
            	$('[name="v_no_invoice"]').val(data.NoDokumen);
                $('[name="v_beo"]').val(data.NoReservasi);
                $('[name="v_travel"]').val(data.KdTravel);
                
                
                
                
                           //detail
                           $.ajax({
								url: "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_cek') ?>",
					            type: "POST",
					            data: $('#form').serialize(),
					            dataType: "html",
					            success: function (res)
					            {
					    
					                $('#detail_invoice').html(res);
					                //$('#totalan_menu').val($('#totalan').val());
					                $('#totalan_menu').val($('#totalan2').val());
					                $('#totalan_menu2').val($('#totalan2').val());
					                $('#grand_totals').val($('#totalan2').val());
					                
					                $('#btnSave').attr('disabled', false);
					                

					            },
								error: function(e) 
								{
									alert(e);
								}
							});
                
                
                			//tambahan
                            $.ajax({
								url: "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_view_tambahan') ?>",
					            type: "POST",
					            data: $('#form').serialize(),
					            dataType: "html",
					            success: function (res)
					            {
					                
					                $('#detail_invoice_tambahan_edit').html(res);
					                $('#totalan_tambahan').val($('#totalan_tambahan_detail').val());
					                $('#btnSave').attr('disabled', false);

					            },
								error: function(e) 
								{
									alert(e);
								}
							});
                
               
               
                
                $('#modal_invoice_method').modal('show');
                $('#btnSave').attr('disabled',false);
                $('.modal-title').text('Edit Invoice '+nodok);
                hitung_ulang();
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
              
    }
       
        
    function save()
    {
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled', true);
        var url;

        if (save_method == 'add') {
            url = "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_add') ?>";
        } else {
            url = "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_update_invoice') ?>";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function (data)
            {
                if (data.status)
                {
                    $('#modal_invoice_method').modal('hide');
                    getData();
                    //window.location.reload(true);
                   
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('save');
               // $('#btnSave').attr('disabled', false);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //alert('Error adding / update data');
                $('#modal_invoice_method').modal('hide');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled', false);

            }
        });
    }
    	
function bayar(nodok,url)
{
	var r=confirm("Apakah Invoice "+nodok+" Ini Akan Di Bayar?")
	if (r==true)
	{
		
		$.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/bayar') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data){
				  	getData();
				  }      		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
	}
	else
	{
  		return false;
	}
}


function VoidTrans(nodok,url)
{
	var r=confirm("Apakah Invoice "+nodok+" Ini Akan Di Void?")
	if (r==true)
	{
		
		$.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/void_invoice') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data){
				  	getData();
				  }      		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
	}
	else
	{
  		return false;
	}
}


function hapus_invoice(nodok)
{
	var r=confirm("Apakah Invoice "+nodok+" Ini Akan Di Hapus?")
	if (r==true)
	{
		
		$.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/hapus_invoice') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data){
				  	getData();
				  }      		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
	}
	else
	{
  		return false;
	}
}



function delete_detail_tambahan(id, url)
{
	var r=confirm("Apakah Detail Ini Akan Di Hapus?")
	if (r==true)
	{
		$('#pleaseWaitDialog').modal('show');
		
		$.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/delete_detail_tambahan') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data){
				  	       $('#pleaseWaitDialog').modal('hide');
				  	       $('#pleaseWaitDialog').modal('show');
				  	       //REFRESH tambahan
                            $.ajax({
								url: "<?php echo site_url('transaksi/invoice_non_reservasi/ajax_view_tambahan') ?>",
					            type: "POST",
					            data: $('#form').serialize(),
					            dataType: "html",
					            success: function (res)
					            {
					                
					                $('#detail_invoice_tambahan_edit').html(res);
					                
					                $('#btnSave').attr('disabled', false);
					                $('#pleaseWaitDialog').modal('hide');

					            },
								error: function(e) 
								{
									alert(e);
								}
							});
				  	
				  }      		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
	}
	else
	{
  		return false;
	}
}

function cari_travel(url)
		{
			var act=$("#v_keyword_travel").val();
			$.ajax({
					url: url+"index.php/transaksi/invoice_non_reservasi/ajax_travel/",
					data: {id:act},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#v_travel').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
				}); 
			
			   	
   		}

function cari_data()
{
	    keyword = $('#search_keyword').val();
	    status = $('#search_status').val();
	    if(keyword==""){
			key=0;
		}else{
			key = keyword;
		}
		
		$('#pleaseWaitDialog').modal('show');
		
		$.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/getList') ?>/" + key+"/"+status,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#pleaseWaitDialog').modal('hide');
				       		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	
}

function refresh()
{
	    
		
		$('#pleaseWaitDialog').modal('show');
		
		$.ajax({
            url: "<?php echo site_url('transaksi/invoice_non_reservasi/getList') ?>/0/",
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#pleaseWaitDialog').modal('hide');
				       		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	
}


            function get_beo(url)
				{
					v_travel = document.getElementById("v_travel").value;
					//alert(v_travel);
					document.getElementById("td_beo").innerHTML = "Loading...";
				     
						  $.ajax({
								url: url+"index.php/transaksi/invoice_non_reservasi/getDataReservasi/",
								data: {trv:v_travel},
								type: "POST",
								dataType: 'html',					
								success: function(res,data)
								{
									$('#v_beo').html(res);
									document.getElementById("td_beo").innerHTML = "";
								},
								error: function(e) 
								{
									alert(e);
								} 
					   });
				}
				
	function PopUpPrint(kode, baseurl)
    {
        url = "index.php/transaksi/invoice_non_reservasi/create_pdf/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=1280,height=800,top=50,left=50');
    }
    
    function detailNew_detail()
		{
			var clonedRow = $("#TabelDetail1 tr:last").clone(true);
			var intCurrentRowId = parseFloat($('#TabelDetail1 tr').length )-2;
			nama = document.getElementsByName("v_ket_tambahan[]");
			temp = nama[intCurrentRowId].id;
			intCurrentRowId = temp.substr(14,temp.length-14);
			var intNewRowId = parseFloat(intCurrentRowId) + 1;
			$("#v_ket_tambahan" + intCurrentRowId , clonedRow ).attr( { "id" : "v_ket_tambahan" + intNewRowId,"value" : ""} );
			$("#v_harga_tambahan" + intCurrentRowId , clonedRow ).attr( { "id" : "v_harga_tambahan" + intNewRowId,"value" : ""} );
			$("#btn_del_detail_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_detail_" + intNewRowId} );
			$("#TabelDetail1").append(clonedRow);
			$("#TabelDetail1 tr:last" ).attr( "id", "baris_detail" +intNewRowId ); // change id of last row
			$("#v_ket_tambahan" + intNewRowId).focus();
			ClearBaris_detail(intNewRowId);
		}
		
	function ClearBaris_detail(id)
		{
			$("#v_ket_tambahan"+id).val("");
			$("#v_harga_tambahan"+id).val("");
		}
	
	function Hitung(obj) {
			
            objek = obj.id;
            
                grdTotal = 0;
                id = parseFloat(objek.substr(16, objek.length - 16));
                hrgx = $("#v_harga_tambahan" + id).val(); 
                totalNetto();            
        }
        
   function hitung_ulang(){
   	menu = parseFloat($('#totalan_menu2').val());
   	tambahan = parseFloat($('#totalan_tambahan').val());
   	$('#grand_total').val(menu+tambahan);
   }
        
    function totalNetto()
	{
	    var lastRow = document.getElementsByName("v_harga_tambahan[]").length;
	    var stotal = 0;
	    
	    for (index = 0; index < lastRow; index++)
	    {
	        indexs = index - 1;
	        nama = document.getElementsByName("v_harga_tambahan[]");
	        temp = nama[index].id;
	        temp1 = nama[index].value;
			temp1=parseFloat(nama[index].value);
	        stotal += temp1;
	    }
	    $("#totalan_tambahan").val(Math.round(stotal));
		tot_menu = parseFloat($("#totalan_menu2").val());
		$("#grand_totals").val(tot_menu+stotal)
	}
	
	function deleteRow_detail(obj)
		{
			objek = obj.id;
			id = objek.substr(22,objek.length-3);
			var lastRow = document.getElementsByName("v_ket_tambahan[]").length;
			
			if( lastRow > 1)
			{
				$('#baris_detail'+id).remove();
			}else{
					alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
			}
			
			//harga di refresh
            totalNetto();
			
		}
	
</script>
