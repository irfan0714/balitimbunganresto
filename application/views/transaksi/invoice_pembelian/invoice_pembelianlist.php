<?php $this->load->view('header'); ?>


<script language="javascript" src="<?= base_url(); ?>public/js/permintaan_barang.js"></script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/purchase_invoice/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="50" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/purchase_invoice/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>No Faktur</center></th>
		        <th width="100"><center>No Faktur Supplier</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th width="100"><center>No PO</center></th>
		        <th width="300"><center>Supplier</center></th>
		        <th width="100"><center>Navigasi</center></th>
        	</tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                ?>
                <tr title="<?php echo $val["NoFaktur"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="left"><?php echo $val["NoFaktur"]; ?></td>
                    <td align="left"><?php echo $val["NoFakturSupplier"]; ?></td>
                    <td align="center"><?php echo $val["Tanggal"]; ?></td>
                    <td align="left"><?php echo $val["NoPO"]; ?></td>
                    <td align="left"><?php echo $val["Nama"]; ?></td>
                    <td align="center">

                        <a href="<?php echo base_url(); ?>index.php/transaksi/purchase_invoice/view/<?php echo $val["NoFaktur"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                        
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
