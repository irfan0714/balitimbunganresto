<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Purchase Invoice";

//$arr_data(unset);

?>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
    
    function submit(){
		$("#theform").submit();
	}
    
    function SupplierChange(){
    	$.ajax({
	        type: "POST",
	        url: "<?=base_url();?>index.php/transaksi/purchase_invoice/getPO/" + $("#KdSupplier").val(),
	        success: function(json){
	        	json = eval(json);
	        	$('#NoPO').empty();
	        	$('#NoPenerimaan').empty();
	        	$('#NoPO').append("<option value=''>"+"-----Pilih-----"+"</option>");
	        	for(i=0;i<json.length;i++){
		        	obj = json[i];
		        	NoPO = obj.NoDokumen;
					$('#NoPO').append("<option value='"+NoPO+"'>"+NoPO+"</option>");
				};
			}
	    });
	};
	
	function POChange(){
    	$.ajax({
	        type: "POST",
	        url: "<?=base_url();?>index.php/transaksi/purchase_invoice/getRG/" + $("#NoPO").val()+"/"+$("#tanggal").val(),
	        success: function(json){
	        	json = eval(json);
	        	if(json.length>0){
		        	obj = json[0];
		        	$('#ppn').val(obj.PPn) ;
		        	$('#mata_uang').val(obj.currencycode) ;
		        	$('#NoPenerimaan').empty();
		        	for(i=0;i<json.length;i++){
			        	obj = json[i];
			        	NoPenerimaan = obj.NoDokumen;
						$('#NoPenerimaan').append("<option value='"+NoPenerimaan+"'>"+NoPenerimaan+"</option>");
					};
				}else{
					$('#ppn').val(0) ;
		        	$('#mata_uang').val(0) ;
		        	$('#NoPenerimaan').empty();
				}
			}
	    });
	};
	
	function getDetail() {
		var NoPenerimaan = $('#NoPenerimaan').val();
			
		$.ajax({
			type: "POST",
			url: "<?=base_url();?>index.php/transaksi/purchase_invoice/getDetail/",
			data : { NoPenerimaan:NoPenerimaan },
			success: function(data) {
				$('#UpdateDetail').html(data);
			}
		});
	};
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_invoice/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	            <td class="title_table" width="150">Tanggal JT <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="jatuhtempo" id="jatuhtempo" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="tanggal" id="tanggal" size="10" maxlength="10">
	            </td>
	            <td class="title_table" width="150">Mata Uang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" name="mata_uang" id="mata_uang" size="15" maxlength="15" readonly>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Supplier <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?=form_dropdownDB_initJS('KdSupplier', $supplier, 'KdSupplier', 'Nama', '', '', '--------- Pilih ---------', 'id="KdSupplier" onchange="SupplierChange()" class="widthonehundred");"');?>
	            </td>
	            <td class="title_table" width="150">Kurs <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="1" class="form-control-new" name="kurs" id="kurs" size="15" maxlength="15"  style="text-align: right;">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Faktur Supplier <font color="red"><b>(*)</b></font></td>
	           	<td> 
	            	<input type="text" class="form-control-new" name="nofaktursupplier" id="nofaktursupplier" size="50" maxlength="50">
	            </td>
	            <td class="title_table" width="150">PPN (%) <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" name="ppn" id="ppn" size="15" maxlength="15" readonly style="text-align: right;">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. PO <font color="red"><b>(*)</b></font></td>
	           	<td> 
	            	<select name = 'NoPO' id = 'NoPO' onchange='POChange()'>
        				<option value="">-- Select Type --</option>
    				</select
	            </td>
	            <td class="title_table">No. Faktur Pajak <font color="red"><b>(*)</b></font></td>
	           	<td> 
	            	<input type="text" class="form-control-new" name="nofakturpajak" id="nofakturpajak" size="50" maxlength="50">
	            </td>

	        </tr>
	        <tr>
	        	<td class="title_table">No. Penerimaan <font color="red"><b>(*)</b></font></td>
	        	<td>
		        	<select name="NoPenerimaan" id="NoPenerimaan" multiple>
		        	</select>
		        </td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="getDetail();" name="btn_proses" id="btn_proses"  value="Proses">Proses<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<span id='UpdateDetail'></span>
	        	</td>
	        </tr>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_invoice/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onclick="submit();" value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
	new Spry.Widget.ValidationTextField("nofaktursupplier", "none");
	new Spry.Widget.ValidationSelect("KdSupplier");
	new Spry.Widget.ValidationSelect("NoPO");
	new Spry.Widget.ValidationTextField("tanggal", "date", {format:"dd-mm-yyyy", useCharacterMasking:true});
	new Spry.Widget.ValidationTextField("kurs", "real", {useCharacterMasking:true});
</script>
