<table class="table table-bordered responsive" id="TabelDetail">
	<thead class="title_table">
		<tr>
			<th width="5"><center>No</center></th>
			<th width="50"><center>Kode</center></th>
			<th><center>Nama Barang</center></th>               
			<th width="100"><center>No Penerimaan</center></th>
			<th width="100"><center>Qty</center></th>
			<th width="100"><center>Satuan</center></th>
			<th width="50"><center>Harga</center></th>
			<th width="100"><center>Total</center></th>
	    </tr>
	</thead>
	<tbody>
		
		<?php 
		$subtotal = 0;
		$ppn = 0;
		for($a=0 ; $a<$num ; $a++) {
			$subtotal += $row[$a]['Harga']*$row[$a]['Qty'];
			$ppn += ($row[$a]['Harga']*$row[$a]['Qty'])*$row[$a]['PPN']/100;
		?>
		<tr id="baris<?php echo $a; ?>">
	        <td>
	            <input type="text" class="form-control-new" name="nourut[]" id="nourut<?php echo $a;?>" value='<?=$a+1;?>' readonly style="text-align: right;">
	        </td>
	        <td>
            	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $a;?>" value='<?=$row[$a]['PCode'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="namabarang[]" id="namabarang<?php echo $a;?>" value='<?=$row[$a]['NamaBarang'];?>' readonly style="width: 100%;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="nopenerimaandtl[]" id="nopenerimaandtl<?php echo $a;?>" value='<?=$row[$a]['NoPenerimaan'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="qty[]" id="qty<?php echo $a;?>" value='<?=number_format($row[$a]['Qty'], 4, ',', '.');?>' readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="satuan[]" id="satuan<?php echo $a;?>" value='<?=$row[$a]['Satuan'];?>' readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="harga[]" id="harga<?php echo $a;?>" value='<?=number_format($row[$a]['Harga'], 4, ',', '.');?>' readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="total[]" id="total<?php echo $a;?>" value='<?=number_format($row[$a]['Harga']*$row[$a]['Qty'], 4, ',', '.');?>' readonly style="text-align: right;">
            </td>
	    </tr>
	    <?php
	    }
	    ?>
	    <tr>
	    	<td colspan="5">
	    		
	    	</td>
	    	<td>
	    		<td class="title_table">Sub Total </td>
	    	</td>
	     	<td>
            	<input type="text" class="form-control-new" name="subtotal" id="subtotal" value='<?=number_format($subtotal, 4, ',', '.');?>' readonly style="text-align: right;">
            </td>
	    </tr>
	    <tr>
	    	<td colspan="5">
	    		
	    	</td>
	    	<td>
	    		<td class="title_table">PPN </td>
	    	</td>
	     	<td>
            	<input type="text" class="form-control-new" name="valppn" id="valppn" value='<?=number_format($ppn, 4, ',', '.');?>' readonly style="text-align: right;">
            </td>
	    </tr>
	    <tr>
	    	<td colspan="5">
	    		
	    	</td>
	    	<td>
	    		<td class="title_table">Total </td>
	    	</td>
	     	<td>
            	<input type="text" class="form-control-new" name="gtotal" id="gtotal" value='<?=number_format($subtotal+$ppn, 4, ',', '.');?>' readonly style="text-align: right;">
            </td>
	    </tr>
	</tbody>
</table>