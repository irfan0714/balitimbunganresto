<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Purchase Invoice";

//$arr_data(unset);

?>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>View <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" >
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td>
	            	<input type="text" value='<?=$data['NoFaktur'];?>' class="form-control-new" name="nofaktur" id="nofaktur" size="15" maxlength="15"  style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Tanggal JT <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value='<?=$data['JatuhTempo'];?>' class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="jatuhtempo" id="jatuhtempo" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value='<?=$data['Tanggal'];?>' class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="tanggal" id="tanggal" size="10" maxlength="10">
	            </td>
	            <td class="title_table" width="150">Mata Uang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value='<?=$data['MataUang'];?>' class="form-control-new" name="mata_uang" id="mata_uang" size="15" maxlength="15" readonly>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Supplier <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value='<?=$data['Nama'];?>' class="form-control-new" name="supplier" id="Nama" size="15" maxlength="50">
	            </td>
	            <td class="title_table" width="150">Kurs <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value='<?=$data['Kurs'];?>' class="form-control-new" name="kurs" id="kurs" size="15" maxlength="15"  style="text-align: right;">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Faktur Supplier <font color="red"><b>(*)</b></font></td>
	           	<td> 
	            	<input type="text" value='<?=$data['NoFakturSupplier'];?>'  class="form-control-new" name="nofaktursupplier" id="nofaktursupplier" size="50" maxlength="50">
	            </td>
	            <td class="title_table" width="150">PPN (%) <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value='<?=$data['PPN'];?>' class="form-control-new" name="ppn" id="ppn" size="15" maxlength="15" readonly style="text-align: right;">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. PO <font color="red"><b>(*)</b></font></td>
	           	<td> 
	            	<input type="text" value='<?=$data['NoPO'];?>' class="form-control-new" name="nofakturpajak" id="nofakturpajak" size="15">
	            </td>
	            <td class="title_table">No. Faktur Pajak <font color="red"><b>(*)</b></font></td>
	           	<td> 
	            	<input type="text" value='<?=$data['NoFakturPajak'];?>' class="form-control-new" name="nofakturpajak" id="nofakturpajak" size="50" maxlength="50">
	            </td>

	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
	        		<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
								<th width="5"><center>No</center></th>
								<th width="20"><center>Kode</center></th>
								<th width="200"><center>Nama Barang</center></th>               
								<th width="100"><center>No Penerimaan</center></th>
								<th width="50"><center>Qty</center></th>
								<th width="100"><center>Satuan</center></th>
								<th width="50"><center>Harga</center></th>
								<th width="100"><center>Total</center></th>
						    </tr>
						</thead>
						<tbody>
							
							<?php 
							$subtotal = 0;
							$ppn = 0;
							for($a=0 ; $a<count($row) ; $a++) {
								$subtotal += $row[$a]['Harga']*$row[$a]['Qty'];
								$ppn += ($row[$a]['Harga']*$row[$a]['Qty'])*$data['PPN']/100;
							?>
							<tr id="baris<?php echo $a; ?>">
						        <td>
						            <input type="text" class="form-control-new" name="nourut[]" id="nourut<?php echo $a;?>" value='<?=$a+1;?>' readonly style="text-align: right;">
						        </td>
						        <td>
					            	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $a;?>" value='<?=$row[$a]['KdBarang'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="namabarang[]" id="namabarang<?php echo $a;?>" value='<?=$row[$a]['NamaLengkap'];?>' readonly style="width: 100%;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="nopenerimaandtl[]" id="nopenerimaandtl<?php echo $a;?>" value='<?=$row[$a]['NoPenerimaan'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="qty[]" id="qty<?php echo $a;?>" value='<?=number_format($row[$a]['Qty'], 0, ',', '.');?>' readonly style="text-align: right;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="satuan[]" id="satuan<?php echo $a;?>" value='<?=$row[$a]['Satuan'];?>' readonly style="text-align: left;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="harga[]" id="harga<?php echo $a;?>" value='<?=number_format($row[$a]['Harga'], 0, ',', '.');?>' readonly style="text-align: right;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="total[]" id="total<?php echo $a;?>" value='<?=number_format($row[$a]['Harga']*$row[$a]['Qty'], 0, ',', '.');?>' readonly style="text-align: right;">
					            </td>
						    </tr>
						    <?php
						    }
						    ?>
						    <tr>
						    	<td colspan="5">
						    		
						    	</td>
						    	<td>
						    		<td class="title_table">Sub Total </td>
						    	</td>
						     	<td>
					            	<input type="text" class="form-control-new" name="subtotal" id="subtotal" value='<?=number_format($subtotal, 0, ',', '.');?>' readonly style="text-align: right;">
					            </td>
						    </tr>
						    <tr>
						    	<td colspan="5">
						    		
						    	</td>
						    	<td>
						    		<td class="title_table">PPN </td>
						    	</td>
						     	<td>
					            	<input type="text" class="form-control-new" name="valppn" id="valppn" value='<?=number_format($ppn, 0, ',', '.');?>' readonly style="text-align: right;">
					            </td>
						    </tr>
						    <tr>
						    	<td colspan="5">
						    		
						    	</td>
						    	<td>
						    		<td class="title_table">Total </td>
						    	</td>
						     	<td>
					            	<input type="text" class="form-control-new" name="gtotal" id="gtotal" value='<?=number_format($subtotal+$ppn, 0, ',', '.');?>' readonly style="text-align: right;">
					            </td>
						    </tr>
						</tbody>
					</table>
	        	</td>
	        </tr>
	    </table>
	    
	    </form> 
	    
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
		</ol>
			
        <table class="table table-bordered responsive">
            <tr>
            	<td class="title_table" width="150">Author</td>
	            <td><?php echo $data['AddUser']." :: ".$data['AddDate']; ?></td>
            </tr>
            <tr>
            	<td class="title_table" width="150">Edited</td>
	            <td><?php echo $data['EditUser']." :: ".$data['EditDate']; ?></td>
            </tr>
         </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

