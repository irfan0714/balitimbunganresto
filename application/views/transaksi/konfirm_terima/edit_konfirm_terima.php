<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'konfirmterima',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/konfirm_terima.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="loading()">
<form method='post' name="konfirmterima" id="konfirmterima" action='<?=base_url();?>index.php/transaksi/konfirm_terima/save_new_terima' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Edit Konfirmasi Hutang</legend>
			<table class="table_class_list">
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("No","nodok",$header->NoDokumen,"11","11","readonly='readonly'","text","","1");
			echo $mylib->write_textbox("Tanggal","tgl",$header->Tgl,"10","10","readonly='readonly'","text",$gantikursor,"1");
		    ?>
			<tr>
				<td nowrap>Asal Data</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="M" <?php echo $sumberM; ?> name="sumberMT" id="sumberMT" onclick="ubahsumberMT();"/>Manual
					<input type="radio" value="T" <?php echo $sumberT; ?> name="sumberMT" id="sumberMT" onclick="ubahsumberMT();"/>Terima Barang
				</td>
			</tr>
			<tr>
				<td nowrap>No Terima</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="11" size="11" value="<?=$header->NoOrder?>" name="noorder" id="noorder" disabled onkeydown="keyShortcut(event,'order',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickOrder();" id="btnorder" disabled>
				<input type="hidden" name="hiddennoorder" id="hiddennoorder" value="<?=$header->NoOrder?>">
				</td>
			</tr>
			<?php
			echo $mylib->write_combo("Gudang","gudang",$mgudang,$gudang,"KdGudang","NamaGudang",$gantikursor,"onchange='CatatGudang()'","ya");
			?>
			<tr>
				<td nowrap>Supplier</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" disabled value=<?=$header->KdSupplier?> name="kdsupplier" id="kdsupplier" onkeydown="keyShortcut(event,'supplier',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickSupplier();" id="btnsupplier" disabled>
				<input type="hidden" name="hiddensupplier" id="hiddensupplier" value=<?=$header->KdSupplier?>>
				<input type="text" name="suppliername" disabled id="suppliername" size="30" value="<?=$header->Nama?>" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>Payment</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="C" <?php echo $sumberC; ?> name="sumber" id="sumberC" onclick="ubahpayment();"/>Cash
					<input type="radio" value="K" <?php echo $sumberK; ?> name="sumber" id="sumberK" onclick="ubahpayment();"/>Credit
				    <input type="text" name="top" id="top" size="3"  value="<?=$header->TOP?>" <?php echo $topdisabled; ?> onkeydown="keyShortcut(event,'top',this);">Hari
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Tgl Jatuh Tempo","tgljto",$header->TgJto,"10","10","readonly='readonly'","text","","1");
			echo $mylib->write_textbox("Keterangan","ket",$header->Keterangan,"35","30","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Invoice","jumlah",$header->Jumlah,"25","20","readonly='readonly'","text","","1","");
			echo $mylib->write_number("PPN Beli","ppn",$header->PPnSupp,"25","20","readonly='readonly'","hidden",$gantikursor,"1","onKeyUp=\"hitungPPN()\"");
			echo $mylib->write_number("PPN Beli","nilaippn",$header->NilaiPPn,"25","20","readonly='readonly'","text",$gantikursor,"1","onKeyUp=\"hitungPPN()\"");
			echo $mylib->write_number("Pembulatan","pembulatan",$header->Pembulatan,"25","20","","text",$gantikursor,"1","onKeyUp=\"totalNetto()\"");
			echo $mylib->write_number("Total Invoice","total",$header->Total,"25","20","readonly='readonly'","text","","1","");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
			  <p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
		</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()" id="newrow"></td>
					<td>KdBarang</td>
					<td>NamaBarang</td>
					<td>Ext</td>
					<td>Satuan</td>
					<td>Qty</td>
					<td>Harga</td>
					<td>Disc1</td>
					<td>Disc2</td>
					<td>Jumlah</td>
					<td>PPn</td>
					<td>Total</td>
				</tr>
			<?php
			for($z=0;$z<count($detail);$z++){
			  
			    $select0 = "";
			    $selectB = "";
				$selectT = "";
				$selectK = "";
			    if($detail[$z]['NamaSatuan']==$detail[$z]['Nama0'])
				{ $select0 = "selected='selected'"; $nilsatuan = "1|".$detail[$z]['Satuan0'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama1'])
				{ $selectB = "selected='selected'"; $nilsatuan = "1|".$detail[$z]['Satuan1'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama2'])
				{ $selectT = "selected='selected'"; $nilsatuan = "2|".$detail[$z]['Satuan2'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama3'])
				{ $selectK = "selected='selected'"; $nilsatuan = "3|".$detail[$z]['Satuan3'];}
				$msatuan = "<option ".$select0."value='0|".$detail[$z]['Satuan0']."'>".$detail[$z]['Nama0']."</option>";
				$msatuan .= "<option ".$selectB."value='1|".$detail[$z]['Satuan1']."'>".$detail[$z]['Nama1']."</option>";
				$msatuan .= "<option ".$selectT."value='2|".$detail[$z]['Satuan2']."'>".$detail[$z]['Nama2']."</option>";
				$msatuan .= "<option ".$selectK."value='3|".$detail[$z]['Satuan3']."'>".$detail[$z]['Nama3']."</option>";
				
		    	$mylib->write_detailFB1($z+1,$detail[$z]['PCode'],$detail[$z]['NamaLengkap'],$detail[$z]['PCodeExt'],$detail[$z]['QtyInput'],
				round($detail[$z]['Jumlah']),round($detail[$z]['PPn']),round($detail[$z]['Total']),$detail[$z]['Harga'],$detail[$z]['Disc1'],$detail[$z]['Disc2'],
				$detail[$z]['Harga'],$detail[$z]['PCode'],$detail[$z]['NamaSatuan'],$detail[$z]['harga0b'],"",$msatuan,
				$detail[$z]['harga0b'],$detail[$z]['harga1b'],$detail[$z]['harga2b'],$detail[$z]['harga3b'],
				$detail[$z]['konv0st'],$detail[$z]['konv1st'],$detail[$z]['konv2st'],$detail[$z]['konv3st'],
				$detail[$z]['KonversiSt'],$detail[$z]['SatuanSt'],$detail[$z]['NamaSatuanSt'],
				$detail[$z]['KdKategori'],$detail[$z]['KdBrand'],$fromPO,"tersimpan");
			}
			?>
			<script language="javascript">
				detailNew();
				jQuery("input[name='sumberMT']").each(function(i) {
					jQuery(this).attr('disabled', 'disabled');
				});
				$("#gudang").attr("disabled","disabled");
				$("#qty1").focus();
			</script>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="limitkredit" name="limitkredit">
				<input type='hidden' id="limitfaktur" name="limitfaktur">
				<input type='hidden' id="hiddengudang" name="hiddengudang" value="<?=$gudang?>">
				<input type='hidden' id="hiddensumberMT" name="hiddensumberMT" value="<?=$header->SumberKonfirm?>">
				<input type='hidden' id="hiddensumber" name="hiddensumber" value="<?=$header->Payment?>">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="kdgroupext" name="kdgroupext" value="<?=$header->KdGroupext?>">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/konfirm_terima/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer');
?>