<?php
$this->load->view('header');
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Terima Barang";
?>

<script>
    function cekTheform(obj)
    {
    	var grdTotal = document.getElementById('grdTotal');
    	if(grdTotal.value==0){
			alert("Grand Total = 0 , Silahkan enter disalah satu kolom Harga untuk mendapatkan Grand Total.");
			document.getElementById("harga1").focus();
		}else{
	        var nodoksupplaier = document.getElementById('nodoksupplier');
	        var nofatpajak = document.getElementById('nofpajak');
	        
        	if(nodoksupplaier.value==""){
				alert("No.Dokumen Supplier Tidak Boleh Kosong... ");
				document.getElementById("nodoksupplier").focus();	
			}else if(nofatpajak.value==""){
				alert("No.Faktur Pajak  Tidak Boleh Kosong... ");
				document.getElementById("nofpajak").focus();	
			}else if (document.getElementById("harga1").value == ""){
            	alert("Harga harus diisi..");
            	document.getElementById("harga1").focus();
            	return false;
        	}else {
        		url = $("#base_url").val();
				tanggal = $("#v_tgl_dokumen").val();        		
        		$.ajax({
					url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
					data: {tgl:tanggal,jenis:'Stock'},
					type: "POST",
					dataType: 'json',					
					success: function(data)
					{
						if(data=='0'){
							alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
							document.getElementById('v_tgl_dokumen').focus();
						}else{
							document.getElementById("theform").submit();
						}	
					},
					error: function(e) 
					{
						//alert(e);
					} 
			 	});
        	}
        }
    }

    function HitungHarga(e, flag, obj) {
    	
    	//var e = window.event;
        if (window.event) // IE
        {
            var code = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            var code = e.which;
        }
        if (code == 13) {
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(5, objek.length - 5));

                hrg = parseFloat($("#harga" + id).val());
                qty = parseFloat($("#qty" + id).val());
                ppn = parseFloat($("#ppn" + id).val());
                $("#jumlah" + id).val( (hrg * qty).toFixed(4));
                subtot = $("#jumlah" + id).val();
                ppnhitung = parseFloat(subtot * ppn/100);
                //alert(ppnhitung);
                hslppn = parseFloat(subtot) + parseFloat(ppnhitung);
                $("#ppn_" + id).val(ppnhitung);
                $("#total" + id).val((hslppn).toFixed(4));
                totalNetto();
            }

        }
    }
    
    function HitungHarga2(e, flag, obj) {
    	
    	//var e = window.event;
        if (window.event) // IE
        {
            var code = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            var code = e.which;
        }
        if (code == 13) {
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(3, objek.length - 3));

                hrg = parseFloat($("#harga" + id).val());
                qty = parseFloat($("#qty" + id).val());
                ppn = parseFloat($("#ppn" + id).val());
                $("#jumlah" + id).val( (hrg * qty).toFixed(4));
                subtot = $("#jumlah" + id).val();
                ppnhitung = parseFloat(subtot * ppn/100);
                //alert(ppnhitung);
                hslppn = parseFloat(subtot) + parseFloat(ppnhitung);
                $("#ppn_" + id).val(ppnhitung);
                $("#total" + id).val((hslppn).toFixed(4));
                totalNetto();
            }

        }
    }
    
    function HitungHarga3( flag, obj) {
    	
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(3, objek.length - 3));

                hrg = parseFloat($("#harga" + id).val());
                qty = parseFloat($("#qty" + id).val());
                ppn = parseFloat($("#ppn" + id).val());
                //$("#jumlah" + id).val(hrg * qty);
                //subtot = $("#jumlah" + id).val();
                ppnhitung = parseFloat((hrg * qty) * ppn/100);
                //alert(ppnhitung);
                //hslppn = parseFloat(subtot) + parseFloat(ppnhitung);
                $("#ppn_" + id).val(ppnhitung);
                //$("#total" + id).val(hslppn);
                //totalNetto();
            }

        
    }
    
    function totalNetto()
{
    var lastRow = document.getElementsByName("jumlah[]").length;
    var total = 0;
    var sppn = 0;
    var stotal = 0;
    for (index = 0; index < lastRow; index++)
    {
        indexs = index - 1;
        nama = document.getElementsByName("jumlah[]");
        temp = nama[index].id;
        temp1 = parseFloat(nama[index].value);
        stotal += temp1;
        
        /*nama = document.getElementsByName("ppn[]");
        temp = nama[index].id;
        temp1 = parseFloat(nama[index].value);
        sppn += temp1;*/
        
        nama = document.getElementsByName("ppn_[]");
        temp = nama[index].id;
        temp1 = parseFloat(nama[index].value);
        sppn += temp1;
        
         nama = document.getElementsByName("total[]");
        temp = nama[index].id;
        temp1 = parseFloat(nama[index].value);
        total += temp1;
    }
    /*ongkir = $("#v_ongkir").val();
    grandtotal = parseFloat(ongkir)+total;
    $("#stotal").val(Math.round(stotal));
    //$("#sppn").val(Math.round(total-stotal));
    $("#sppn").val(Math.round(sppn));
    $("#grdTotal").val(Math.round(grandtotal));
    $("#grandtotal").val(Math.round(grandtotal));*/
    
    ongkir = $("#v_ongkir").val();
    grandtotal = parseFloat(ongkir)+total;
    $("#stotal").val((stotal).toFixed(4));
    $("#sppn").val((sppn).toFixed(4));
    $("#grdTotal").val((grandtotal).toFixed(4));
    $("#grandtotal").val((grandtotal).toFixed(4));

}

// Ajax post
$(document).ready(function() {
    $("#btn_update").click(function(event) {
        event.preventDefault();
        var v_no_dokumenf       = $('#v_no_dokumen').val();
        var v_no_pof            = $('#v_no_po').val();
        var v_tgl_dokumenf      = $('#v_tgl_dokumen').val();
        var v_FlagKonfirmasif   = $('#v_FlagKonfirmasi').val();
        var v_EditUserf         = $('#v_EditUser').val();
        var nofpajakf           = $('#nofpajak').val();
        $.ajax({
            type: "POST",
            url: "<?= base_url(); ?>" + "index.php/transaksi/konfirm_terima/update_data_faktur",
            dataType : "JSON",
            data: { v_no_dokumenf: v_no_dokumenf, v_no_pof: v_no_pof, v_tgl_dokumenf: v_tgl_dokumenf, v_FlagKonfirmasif: v_FlagKonfirmasif, v_EditUserf: v_EditUserf, nofpajakf: nofpajakf },
            success: function(data){
                alert("Update Faktur Pajak Berhasil");
                //console.log(data);
                window.location = "<?= base_url(); ?>" + "index.php/transaksi/konfirm_terima";
            },
            error: function() 
            {
                alert("Update Faktur Pajak Berhasil");
                window.location = "<?= base_url(); ?>" + "index.php/transaksi/konfirm_terima";
                //alert(e);
            } 
        });
    });
});

</script>

<div class="row" >
    <div class="col-md-12" align="left">


        <ol class="breadcrumb title_table">
            <li><strong><i class="entypo-pencil"></i>Konfirm <?php echo $modul; ?></strong></li>
        </ol>

        <?php
        if ($this->session->flashdata('msg')) {
            $msg = $this->session->flashdata('msg');
            ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
        }
        ?>

        <?php
        if ($ceknodok->NoDokumen != "") {
            ?>
            <div class="alert alert-info">
                <strong>PR</strong> dengan nomor <strong><?php echo $header->NoDokumen; ?></strong>  
                telah berhasil dibuat <strong>PO</strong> dengan no <strong><?php echo $ceknodok->NoDokumen; ?></strong> 
                oleh <strong><?php echo $ceknodok->AddUser; ?></strong> pada tanggal <strong><?php echo $ceknodok->AddDate; ?></strong>
            </div>
            <?php
        }
        ?>

        <form method='post' name="theform" id="theform" action='<?= base_url(); ?>index.php/transaksi/konfirm_terima/save_data'>

            <table class="table table-bordered responsive"> 
                <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen; ?>" /> 
                <input type="hidden" name="v_no_po" id="v_no_po" value="<?php echo $header->PoNo; ?>" />  
                <input type="hidden" name="v_tgl_dokumen" id="v_tgl_dokumen" value="<?php echo $header->Tanggal; ?>" />  
                
                <input type="hidden" name="v_FlagKonfirmasi" id="v_FlagKonfirmasi" value="<?php echo $header->FlagKonfirmasi; ?>" /> 
                
                <input type="hidden" name="v_EditUser" id="v_EditUser" value="<?php echo $header->EditUser; ?>" />  

                <tr>
                    <td class="title_table" width="150">No Dokumen</td>
                    <td width="250"><b><?php echo $header->NoDokumen; ?></b></td>
                    <td class="title_table" width="150">Gudang </td>
                    <td> 
                        <?php echo $header->NamaGudang; ?>
                    </td>
                </tr>

                <tr>
                    <td class="title_table" width="150">No PO </td>
                    <td><b><?php echo $header->PoNo; ?></b></td>
                    <td class="title_table" width="150">Keterangan</td>
                    <td><?php echo $header->Keterangan; ?></td>
                </tr>

                <tr>
                    <td class="title_table" width="150">Tanggal </td>
                    <td> 
                        <?php echo $header->Tanggal; ?>
                    </td>

                    <?php
                    if ($header->Status == 0) {
                        ?>
                        <td class="title_table">Status </td>
                        <td colspan="3">
                            <select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
                                <option <?php
                                if ($header->Status == 0) {
                                    echo "selected='selected'";
                                }
                                ?> value="0">Pending</option>
                                <option <?php
                                if ($header->Status == 1) {
                                    echo "selected='selected'";
                                }
                                ?> value="1">Open</option>
                                <option <?php
                                if ($header->Status == 2) {
                                    echo "selected='selected'";
                                }
                                ?> value="2">Void</option>
                            </select>
                        </td>
                        <?php
                    } else {
                        ?>
                        <td class="title_table">Status </td>
                        <td colspan="3">
                            <?php
                            if ($header->Status == 1) {
                                echo "<b>Open</b>";
                            } else if ($header->Status == 2) {
                                echo "<b>Void</b>";
                            }
                            ?>
                        </td>
                        <?php
                    }
                    ?>
                </tr>
                <tr>
                    <td class="title_table">Supplier </td>
                    <td> 
                        <?php echo $header->Nama; ?>
                    </td>
					<td class="title_table">No.Dokumen Supplier </td>
					<td>
						<input type="text" name="nodoksupplier" id="nodoksupplier" class="form-control-new" />
					</td>
                </tr>
				<tr>
                    <td class="title_table">No.Faktur Pajak </td>
                    <td> 
                        <input type="text" name="nofpajak" id="nofpajak" style="text-align:left" class="form-control-new" />
                        <?php
                            if ($header->FlagKonfirmasi == 'Y') {
                        ?>
                        <button type="button" class="btn btn-green" name="btn_update" onclick="cekfakturform();" id="btn_update" value="Update">Update</button>
                        <?php
                            }
                        ?>
                    </td>
                </tr>

                <tr id="tr_detail_pcode_pb">
                    <td colspan="100%" >
                        <table class="table table-bordered responsive">
                            <thead class="title_table">
                                <tr>
                                    <th width="30"><center>No</center></th>
                            <th width="100"><center>PCode</center></th>
                            <th><center>Nama Barang</center></th>               
                            <th width="100"><center>Qty PO</center></th>          
                            <th width="50"><center>Qty</center></th>
                            <th width="100"><center>Satuan</center></th>
                            <th width="100"><center>Harga PO</center></th>
                            <th width="100"><center>Harga</center></th>
                            <th width="100"><center>PPn (%)</center></th>
                            <th width="100"><center>Subtotal</center></th>
                </tr>
                </thead>
                <tbody>
                    <input type="hidden" name="grdTotal" id="grdTotal" value=""/>
                    <?php
                    $no = 1;
                    
                    foreach ($detail as $val) {
                        ?>
                        <tr id="baris<?php echo $no ?>">

                            <td align="center">
                    <center>
                        <?php echo $no; ?>
                    </center>
                    </td>
                    <td align="center"><input type="hidden" name="pcode[]" id="pcode<?php echo $no; ?>" value="<?php echo $val['PCode']; ?>" /><?php echo $val['PCode']; ?></td>
                    <td><?php echo $val['NamaLengkap'] ?></td>
                    <td align="right"><?php echo $val['QtyPcs']; ?></td>
                    <td align="right"><input type="hidden" name="qty[]" id="qty<?php echo $no; ?>" value="<?php echo $val['Qty']; ?>"/><?php echo $val['Qty']; ?></td>
                    <td align="center"><?php echo $val['Satuan']; ?></td>
                    <td align="center"><input type="text" readonly data-toggle="tooltip" data-placement="top" value="<?php echo $val['HargaPO']; ?>" dir="rtl" class="form-control-new" /></td>
                    <td align="center"><input type="text" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal" name="harga[]" id="harga<?php echo $no; ?>" value="<?php echo $val['Harga']; ?>" onkeydown="HitungHarga(event, 'harga', this);" dir="rtl" class="form-control-new" /></td>
                    <td align="right"><input type="text" name="ppn[]" id="ppn<?php echo $no; ?>" onkeydown="HitungHarga2(event, 'harga', this);" onblur="HitungHarga3('harga', this);" value="<?php echo $val['PPn'] ?>" dir="rtl" class="form-control-new"/></td>
                    <td align="right"><input type="text" name="jumlah[]" id="jumlah<?php echo $no; ?>" value="<?php echo $val['Jumlah'] ?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
                    <!--<td style="display: none"><input type="text" name="ppn[]" id="ppn<?php echo $no; ?>" value="<?php echo $val['PPn'] ?>" dir="rtl" class="form-control-new"/></td>-->
                    <td style="display: none"><input type="text" name="total[]" id="total<?php echo $no; ?>" value="<?php echo $val['Total'] ?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
                    <td style="display: none"><input type="text" name="ppn_[]" id="ppn_<?php echo $no; ?>" value="0" dir="rtl" class="form-control-new" readonly="readonly"/></td>
                    </tr>
                    <?php
                    $no++;
                }
                ?>
                </tbody>
                
            </table>
            
            <table border="0" class="table table-bordered responsive">
            <tr>
            	<td width="90%"><b><p align="right">Total</p></b>
            </td>
            <td>
            	<input type="text" id="stotal" value="0" dir="rtl" class="form-control-new" readonly="readonly"/></td>
            </tr> 
            <tr>
            	<td>
            		<b><p align="right">PPN</b></p>
            	</td>
    	        <td>
	           		<input type="text" id="sppn" value="0" dir="rtl" class="form-control-new" readonly="readonly"/>
            	</td>
            </tr>
            <tr>
            	<td>
            		<b><p align="right">Ongkos Kirim</b></p>
            	</td>
            	<td>
            		<input type="text" name='v_ongkir' id="v_ongkir" value=<?=$header->BiayaTransport;?> dir="rtl" class="form-control-new" onblur="totalNetto();">
            	</td>
            </tr> 
            <tr>
            	<td width="90%">
            		<b><p align="right">Grand Total</p></b>
            	</td>
            <td>
            	<input type="text" id="grandtotal" value="0" dir="rtl" class="form-control-new" readonly="readonly"/></td>
            </tr> 
            </table>
            
            </td>
            </tr>


            <?php
            if ($header->FlagKonfirmasi == 'T') {
                ?>
                <tr>
                    <td colspan="100%" align="center">
                        <input type='hidden' name="flag" id="flag" value="edit">
                        <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                        <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close" value="Batal" onclick=parent.location = "<?php echo base_url() . "index.php/transaksi/konfirm_terima/"; ?>">Batal<i class="entypo-cancel"></i></button>
                        <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                    </td>
                </tr>
                <?php
            }
            ?>
            </table>

        </form> 

        <?php
        if ($header->NoDokumen) {
            ?>
            <ol class="breadcrumb title_table">
                <li><i class="entypo-vcard"></i>Information data</li>
            </ol>

            <table class="table table-bordered responsive">
                <tr>
                    <td class="title_table" width="150">Author</td>
                    <td><b><?php echo $header->AddUser . " :: " . $header->AddDate; ?></b></td>
                </tr>
                <tr>
                    <td class="title_table" width="150">Edited</td>
                    <td><b><?php echo $header->EditUser . " :: " . $header->EditDate; ?></b></td>
                </tr>
            </table>	
            <?php
        }
        ?>


    </div>
</div>

<?php $this->load->view('footer'); ?>

