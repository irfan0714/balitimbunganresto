<?php $this->load->view('header'); ?>

<script>
    function ajax_edit(id)
    {
    	//menggunakan method update
        save_method = 'update';
        
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        //nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('transaksi/konfirm_terima/ajax_edit_konfirm') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                    // alert(data);
                    {
                        var sisa = 0;
                        $('[name="NoDokumen"]').val(data.NoDokumen);
                        $('[name="DisplayNoDokumen"]').text(data.NoDokumen);
                        $('[name="TglDokumen"]').val(data.TglDokumen);
                        $('[name="DisplayTglDokumen"]').text(data.TglDokumen);
                        $('[name="PoNo"]').val(data.PoNo);
                        $('[name="DisplayPoNo"]').text(data.PoNo);
//                sisa = $('[name="Total"]').val() - $('[name="Dp"]').val();
//                $('[name="Sisa"]').val(sisa);
//                $('[name="DisplaySisa"]').text(toRp(sisa));
                        $('#modal_konfirm_terima').modal('show');
                        $('.modal-title').text('Konfirmasi Terima Barang');
                    }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
    }
</script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/konfirm_terima/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;<b>Gudang</b>&nbsp;
            <select class="form-control-new" name="search_gudang" id="search_gudang">
                <option value="">All</option>
                <?php
                foreach ($mgudang as $val) {
                    $selected = "";
                    if ($search_gudang) {
                        if ($val["KdGudang"] == $search_gudang) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
                }
                ?>
            </select>
            &nbsp;<b>Divisi</b>&nbsp;
            <select class="form-control-new" name="search_divisi" id="search_divisi">
                <option value="">All</option>
                <?php
                foreach ($mdivisi as $val) {
                    $selected = "";
                    if ($search_divisi) {
                        if ($val["KdDivisi"] == $search_divisi) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
                }
                ?>
            </select>  
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
                <option <?php
                if ($search_status == "0") {
                    echo 'selected="selected"';
                }
                ?> value="0">Pending</option>
                <option <?php
                if ($search_status == "1") {
                    echo 'selected="selected"';
                }
                ?> value="1">Open</option>
                <option <?php
                if ($search_status == "2") {
                    echo 'selected="selected"';
                }
                ?> value="2">Void</option>
            </select>  
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
        <th width="150"><center>No Dokumen</center></th>
        <th width="100"><center>Tanggal</center></th>
        <th width="100"><center>Po No</center></th>
        <th><center>Supplier</center></th>
        <th><center>Gudang</center></th>
        <th width="100"><center>Status</center></th>
        <th width="100"><center>Navigasi</center></th>
        </tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == 0) {
                    $echo_status = "<font style='color:#ff1c1c'><b>Pending</b></font>";
                } else if ($val["Status"] == 1) {
                    $echo_status = "<font style='color:#6bcf29'><b>Open</b></font>";
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:#3399ff;'><b>Void</b></font>";
                } 
                if ($val["FlagKonfirmasi"] == 'Y') {
                    $echo_status = "<font style='color:#3399ff;'><b>Konfirm</b></font>";
                }
                
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $val["Tanggal"]; ?></td>
                    <td align="center"><?php echo $val["PoNo"]; ?></td>
                    <td><?php echo $val["Nama"]; ?></td>
                    <td><?php echo $val["NamaGudang"]; ?></td>
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                      //  if ($val["Status"] == 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/konfirm_terima/edit_form/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Konfirm" title=""><i class="entypo-pencil"></i></a>
                            
                            <?php
                      //  }

                       
                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div id="modal_konfirm_terima" class="modal fade" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Konfirmasi Terima Barang</h3>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                        <input type="hidden" value="" name="id"/>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">NoDokumen</label>
                                <div class="col-md-8">
                                    <input name="NoDokumen" class="form-control" type="hidden" readonly="readonly">
                                    <span name="DisplayNoDokumen" class="form-control"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">TglDokumen</label>
                                <div class="col-md-6">
                                    <input name="TglDokumen" class="form-control" type="hidden" readonly="readonly">
                                    <span name="DisplayTglDokumen" class="form-control"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">PO Number</label>
                                <div class="col-md-6">
                                    <input name="PoNo" class="form-control" type="hidden" readonly="readonly">
                                    <span name="DisplayPoNo" class="form-control"></span>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-info btn-icon btn-sm icon-left">Save & Print<i class="entypo-check"></i></button>
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" data-dismiss="modal">Cancel<i class="entypo-cancel"></i></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
