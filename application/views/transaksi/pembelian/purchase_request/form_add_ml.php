<?php

$this->load->view('header');

$modul = "Purchase Request";

?>

<script>
	function CallAjax(tipenya,param1,param2,param3,param4,param5)
	{

	    try{
	        if (!tipenya) return false;

	        if (param1 == undefined) param1 = '';
	        if (param2 == undefined) param2 = '';
	        if (param3 == undefined) param3 = '';
	        if (param4 == undefined) param4 = '';
	        if (param5 == undefined) param5 = '';

	        var variabel;
	        variabel = "";

	        var base_url = "<?php echo base_url(); ?>";

	        if(tipenya=='search_keyword_pb')
	        {
						// alert(tipenya);

        		document.getElementById("span_no_pb").innerHTML = 'Loading...';

	            $.post(base_url + "index.php/transaksi/purchase_request/getAjax/", {ajax:tipenya, v_keyword:param1},
        		function (data) {
        			document.getElementById("span_no_pb").innerHTML = data;
        		});
	        }
	        else if(tipenya=='ajax_nopb')
	        {
	            $.post(base_url + "index.php/transaksi/purchase_request/getAjax/", {ajax:tipenya, v_nopb:param1},
        		function (data) {

        			if(data)
        			{
        				arr_data = data.split('||');

	        			document.getElementById("pb_buat_oleh").innerHTML = arr_data[0];
	        			document.getElementById("pb_est_terima").innerHTML = arr_data[1];
	        			document.getElementById("v_est_terima").value = arr_data[1];
	        			document.getElementById("pb_divisi").innerHTML = arr_data[2];
	        			document.getElementById("pb_gudang").innerHTML = arr_data[3];
	        			document.getElementById("pb_keterangan").innerHTML = arr_data[4];
	        			document.getElementById("tr_detail_pcode_pb").innerHTML = arr_data[5];
							}

							else
							{
	        			document.getElementById("pb_buat_oleh").innerHTML = "";
	        			document.getElementById("pb_est_terima").innerHTML = "";
	        			document.getElementById("pb_divisi").innerHTML ="";
	        			document.getElementById("pb_gudang").innerHTML = "";
	        			document.getElementById("pb_keterangan").innerHTML = "";
	        			document.getElementById("tr_detail_pcode_pb").innerHTML = "";
							}

        			});
	        	}
						else if(tipenya=='ajax_noml')
		        {
		            $.post(base_url + "index.php/transaksi/purchase_request/getAjax/", {ajax:tipenya, v_nopb:param1},
	        		function (data) {

	        			if(data)
	        			{
	        				arr_data = data.split('||');

		        			document.getElementById("pb_buat_oleh").innerHTML = arr_data[0];
		        			document.getElementById("pb_est_terima").innerHTML = arr_data[1];
		        			document.getElementById("v_est_terima").value = arr_data[1];
		        			document.getElementById("pb_divisi").innerHTML = arr_data[2];
		        			document.getElementById("pb_gudang").innerHTML = arr_data[3];
		        			document.getElementById("pb_keterangan").innerHTML = arr_data[4];
		        			document.getElementById("tr_detail_pcode_pb").innerHTML = arr_data[5];
								}

								else
								{
		        			document.getElementById("pb_buat_oleh").innerHTML = "";
		        			document.getElementById("pb_est_terima").innerHTML = "";
		        			document.getElementById("pb_divisi").innerHTML ="";
		        			document.getElementById("pb_gudang").innerHTML = "";
		        			document.getElementById("pb_keterangan").innerHTML = "";
		        			document.getElementById("tr_detail_pcode_pb").innerHTML = "";
								}

	        			});
		        	}
						else if(tipenya=='ajax_ml')
		        {
							// alert("TES");
							document.getElementById("span_no_pb").innerHTML = 'Loading...';

							 $.post(base_url + "index.php/transaksi/purchase_request/getAjax/", {ajax:tipenya, v_keyword:param1},
						 	function (data) {
								// alert(data);
							 document.getElementById("span_no_pb").innerHTML = data;
						 	});
						}

	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description AJAX : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}
</script>

<script language="javascript" src="<?=base_url();?>public/js/purchase_request_v2.js"></script>

<div class="row">
    <div class="col-md-12" align="left">

    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>

		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_request/save_data_ml'>

	    <table class="table table-bordered responsive">

	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td width="200"><b>AutoGenerate</b></td>

	            <td class="title_table" colspan="2">INFORMASI MARKET LIST</td>
	        </tr>

	        <tr>
	            <td class="title_table" width="150">No ML <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input type="text" name="v_keyword_pb" id="v_keyword_pb" value="" placeholder="Filter No ML" class="form-control-new" onkeyup="CallAjax('ajax_ml', this.value)" />&nbsp;

	            	<span id="span_no_pb">
		            	<select name="v_no_pb" id="v_no_pb" class="form-control-new" style="width: 200px;" onchange="CallAjax('ajax_noml', this.value)">
		            		<option value="">Pilih No ML</option>
		            		<?php
		            		foreach($mpb as $val)
		            		{
								?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
							}
		            		?>
		            	</select>
	            	</span>
	            </td>

	            <td class="title_table" width="150">Dibuat Oleh</td>
	            <td id="pb_buat_oleh"></td>
	        </tr>

	        <tr>
	            <td class="title_table" width="150">Tanggal Dokumen<font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>

	            <td class="title_table">Tanggal Kebutuhan</td><!-- Estimasi Terima -->
	            <td id="pb_est_terima">
	            </td>

	        </tr>

	        <tr>
	            <td class="title_table">Estimasi Terima <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_est_terima" id="v_est_terima" size="10" maxlength="10">
	            </td>

	            <td class="title_table">Divisi</td>
	            <td id="pb_divisi"></td>
	        </tr>

	        <tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>
	            </td>

	            <td class="title_table">Gudang</td>
	            <td id="pb_gudang"></td>
	        </tr>

	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>

	            <td class="title_table">Keterangan</td>
	            <td id="pb_keterangan"></td>
	        </tr>

	        <tr id="tr_detail_pcode_pb"></tr>

	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_request/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>

	    </table>

	    </form>

        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>

	</div>
</div>

<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
