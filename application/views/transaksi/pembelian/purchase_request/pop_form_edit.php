<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/my.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/js.js"></script>
<?php 
 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Purchase Request";

$counter=1;
foreach($detailunion as $val)
{
	$arr_data["list_detail"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["PCode"];
	$arr_data["namabarang"][$counter]=$val["NamaBarang"];
	$arr_data["qtypermintaan"][$counter]=$val["QtyPermintaan"];
	$arr_data["qty"][$counter]=$val["Qty"];
	$arr_data["qtypcs"][$counter]=$val["QtyPcs"];
	$arr_data["satuan"][$counter]=$val["Satuan"];
	$arr_data["namasatuan"][$counter]=$val["NamaSatuan"];
	$arr_data["qtypcsterima"][$counter]=$val["QtyPcsTerima"];
	
	$counter++;
}

?>

<script language="javascript" src="<?=base_url();?>public/js/purchase_request.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
    	
		
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<?php
		if($ceknodok->NoDokumen!="")
		{
			?>
			<div class="alert alert-info">
				<strong>PR</strong> dengan nomor <strong><?php echo $header->NoDokumen; ?></strong>  
				telah berhasil dibuat <strong>PO</strong> dengan no <strong><?php echo $ceknodok->NoDokumen; ?></strong> 
				oleh <strong><?php echo $ceknodok->AddUser; ?></strong> pada tanggal <strong><?php echo $ceknodok->AddDate; ?></strong>
			</div>
			<?php
		}
		?>
		<br><br>
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_request/save_data'>
		
	    <table class="table table-bordered responsive"> 
	    	<input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen; ?>" />   
	    	<input type="hidden" name="v_no_pb" id="v_no_pb" value="<?php echo $header->NoPermintaan; ?>" />                     
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td width="200"><b><?php echo $header->NoDokumen; ?></b></td>
	            <?php if($type=="1"){?>
	            <td class="title_table" colspan="2">INFORMASI PERMINTAAN BARANG</td>
				<?php } ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">No PB <font color="red"><b>(*)</b></font></td>
	            <td><b><?php echo $header->NoPermintaan; ?></b></td>
	            <?php if($type=="1"){?>
	            <td class="title_table" width="150">Dibuat Oleh</td>
	            <td id="pb_buat_oleh"><b><?php echo $detailpb->AddUser." :: ".$detailpb->Tanggal; ?></b></td>
				<?php }?>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $header->Tanggal; ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	            <?php if($type=="1"){?>
	            <!--<td class="title_table">Estimasi Terima</td>-->
	            <td class="title_table">Tanggal Butuh</td>
	            <td id="pb_est_terima"><b><?php echo $detailpb->TglTerima; ?></b></td>
				<?php } ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Estimasi Terima <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $header->TglTerima; ?>" name="v_est_terima" id="v_est_terima" size="10" maxlength="10">
	            </td>
	            <?php if($type=="1"){?>
	            <td class="title_table">Divisi</td>
	            <td id="pb_divisi"><b><?php echo $detailpb->NamaDivisi; ?></b></td>
				<?php } ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang as $val)
	            		{
	            			$selected="";
	            			if($header->KdGudang==$val["KdGudang"])
	            			{
								$selected="selected='selected'";	
							}
							
							?><option <?php echo $selected ?> value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	            <?php if($type=="1"){?>
	            <td class="title_table">Gudang</td>
	            <td id="pb_gudang"><b><?php echo $detailpb->nama_gudang; ?></b></td>
				<?php } ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->Keterangan; ?>" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
	        <?php if($type=="1"){?>
	            <td class="title_table">Keterangan</td>
	            <td id="pb_keterangan"><b><?php echo $detailpb->Keterangan; ?></b></td>
			<?php } ?>
	        </tr>
	        
	        <?php
			if($header->Status==0)
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td colspan="3">
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->Status==1){ echo "selected='selected'"; } ?> value="1">Open</option>
		            		<option <?php if($header->Status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>  
			<?php
			}
			else
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td colspan="3">
		            <?php
		            if($header->Status==1)
		            {
						echo "<b>Open</b>";
					}
					else if($header->Status==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr>  
			<?php
			}
			?>
	        
	        <tr id="tr_detail_pcode_pb">
        		<td colspan="100%" >
					<table class="table table-bordered responsive">
						<thead class="title_table">
							<tr>
							    <th width="30"><center>No</center></th>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="100"><center>Qty PB</center></th>          
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    
							    <?php
								if($header->Status==0)
								{
								?>
							    	<th width="100"><center>Delete</center></th>
							    <?php
							    }
							    ?>
							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$no=1;
						foreach($arr_data["list_detail"] as $counter => $val)
						{
							$pcode = $arr_data["pcode"][$counter];
							$namabarang = $arr_data["namabarang"][$counter];
							$qtypermintaan = $arr_data["qtypermintaan"][$counter];
							$qty = $arr_data["qty"][$counter];
							$qtypcs= $arr_data["qtypcs"][$counter];
							$satuan = $arr_data["satuan"][$counter];
							$namasatuan = $arr_data["namasatuan"][$counter];
							$qtypcsterima = $arr_data["qtypcsterima"][$counter];
							
							?>
							<tr id="baris<?php echo $no ?>">
								<td align="center">
									<center>
									<?php echo $no; ?>
									<input type="hidden" name="pb_pcode[]" id="pb_pcode<?php echo $no; ?>" value="<?php echo $pcode; ?>" />
									<input type="hidden" name="pb_namabarang[]" id="pb_namabarang<?php echo $no; ?>" value="<?php echo $namabarang; ?>"/>	
									<input type="hidden" name="pb_qty[]" id="pb_qty<?php echo $no; ?>" value="<?php echo $qty; ?>"/>	
									<input type="hidden" name="pb_satuan[]" id="pb_satuan<?php echo $no; ?>" value="<?php echo $satuan; ?>"/>		
									</center>
								</td>
								<td align="center"><?php echo $pcode; ?></td>
								<td><?php echo $namabarang; ?></td>
								<td align="right"><?php echo $mylib->format_number($qty,2); ?></td>
								<td align="right"><input type="text" name="v_qty[]" id="v_qty<?php echo $no; ?>" onblur="toFormat2('v_qty<?php echo $no;?>')"  value="<?php echo $mylib->format_number($qtypermintaan,2); ?>" class="form-control-new" size="10" style="text-align: right;" /></td>
								<td align="center"><?php echo $namasatuan; ?></td>
								<?php
								if($header->Status==0)
								{
								?>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				                <?php
							    }
							    ?>
						    </tr>
							<?php
							$no++;
						}
						
						?>
							
						</tbody>
					</table>
        		</td>
	        </tr>
	        
	        <?php
			if($header->Status==0)
			{
			?>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_request/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        <?php
		    }
		    else
		    {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
		            	<input type='hidden' name="flag" id="flag" value="edit">
						<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="CloseWindow()">Tutup<i class="entypo-cancel"></i></button>
					</td>
		        </tr>
				<?php
				
			}
		    ?>
	        
	    </table>
	    
	    </form> 
	    
         
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
function CloseWindow()
		{
			window.close();
		}
	</script>