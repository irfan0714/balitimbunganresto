<?php 
$mylib = new globallib();
 ?>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">

<table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>Exp Date</center></th>
		        <th width="300"><center>Tour Travel</center></th>
		        <th width="100"><center>No. Voucher</center></th>
		        <th width="100"><center>Vouc. Travel</center></th>
		        <th width="100"><center>Nilai</center></th>
		        <th width="200"><center>BEO</center></th>
		        <th width="200"><center>Keterangan</center></th>
                <th width="100"><center>Place</center></th>
		        <th width="100"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = $startnum;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                $bts="";

                if($val["bts"] == 1){
                    $bts = "<button class='btn btn-flat btn-orange btn-block'>BTS</button>";
                }else{
                    $bts = "<button class='btn btn-flat btn-success btn-block'>SGV</button>";

                }

                if ($val["status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Pending</b></font>";
                } else if ($val["status"] == 1) {
                    $echo_status = "<font style='color:#ff1c1c'><b>Close</b></font>";
                } else if ($val["status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>Void</b></font>";
                }
                
                if($val["uang_muka_beo"] == "Y"){
					$color = "yellow";
					$text = "From Uang Muka BEO";
				}else{
					$color = "";
					$text = "From Voucher BEO";
				}
				
				if($val["Voc_Glondongan"]==""){
					$pocher = $val["no_voucher"];
					$pocher_travel =$val["no_voucher_travel"];
				}else{
					$pocher = $val["Voc_Glondongan"];
					$pisah = explode('-',$val["Voc_Glondongan"]);
					$pocher_travel = "V".$pisah[0]."-V".$pisah[1];
				}
                ?>
                <tr title="<?php echo $text." - ".$val["no_voucher"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td bgcolor="<?=$color;?>"><?php echo $no; ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["expDate"]); ?></td>
                    <td align="left"><?php echo $val["Nama"]; ?></td>
                    <td align="left"><?php echo $pocher;?></td>
                    <td align="left"><?php echo $pocher_travel;?></td>
                    <td align="right"><?php echo number_format($val["nilai"]); ?></td>
                    <td align="left"><?php echo $val["BEO"]; ?></td>
                    <td align="left"><?php echo $val["Keterangan"]; ?></td> 
                    <td align="left"><?php echo $bts; ?></td> 
                    <td align="center"><?php echo $echo_status; ?></td>                                       
                    <td align="center">

                        <?php
                        if ($val["status"] == 0) {
                            ?>
                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/uang_muka_beo/edit_uang_muka_beo/<?php echo $val["id"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->

                            <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit Bayar" title="" onclick="edit_bayar('<?php echo $val["id"]; ?>')" >
                                <i class="entypo-pencil"></i>
                            </button>
                            
                            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Close" title="" onclick="lock('<?php echo $val["id"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-key"></i>
                            </button>
                            <?php
                        }

                        if ($val["status"] == 1) {
                            ?>
                            
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit Bayar" title="" onclick="view_bayar('<?php echo $val["id"]; ?>')" >
                                <i class="entypo-eye"></i>
                            </button>
                            
                            <a href="<?php echo base_url(); ?>index.php/transaksi/voucher_beo/doPrint/<?php echo $val["id"]; ?>"  class="btn btn-orange btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-print"></i></a>

                            <?php if($val["push"] == 0) {?>
                            <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Upload BTS" title="" onclick="upload_bts('<?php echo $val["id"]; ?>')" >
                                <i class="entypo-upload"></i>
                            </button>
                            <?php }?>
                           

                            
                            <?php
                        }

                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
    
</div>	
