<?php 
$this->load->view('header');
$mylib = new globallib();
 ?>
<head>
</head>
<!--<script language="javascript" src="<?= base_url(); ?>public/js/uang_muka_beo33.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>



<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/voucher_beo/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
    <input type='hidden' value='<?= $offset ?>' id="offset" name="offset">
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" /> 
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Close</option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2">Void</option>
            </select>  
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="button" class="btn btn-success btn-sm icon-left" onClick="refresh()">Refresh</button>
            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="cari_data()">Search<i class="entypo-search"></i></button>
            <!--<a href="<?php echo base_url() . "index.php/transaksi/uang_muka_beo/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>-->
            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="bayar()">Add<i class="entypo-plus"></i></button>
        </div>
    </div>
</form>
<br>
<div style="color: red;"><b>Note : Tekan Tombol <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary" ><i class="entypo-upload"></i></button> Untuk Upload Voucher ke Bebek Timbungan Sunset</b></div>
<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
   <div id="getList"></div>
</div>


			<div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);margin-top: 25%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>Loading...</h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php $this->load->view('footer'); ?>
<div id="modal_payment_method" class="modal fade" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Voucher</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <input type="hidden" value="" name="no_receipt"/>
                    <div class="form-body">

                         <div class="form-group">
                            <label class="control-label col-md-3">Voucher BTS</label>
                            <div class="col-md-1">
                                <input type="checkbox" class="form-control" value="1" name="v_bts" id="v_bts" >
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Exp Date</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker" value="" name="v_date_payment" id="v_date_payment" >
                            </div>
                        </div>
                                                
                        <div class="form-body">
                        
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-md-3">No. Voucher</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" name="v_no_voucher" id="v_no_voucher" readonly>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="control-label col-md-3">Tour Travel</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_tourtravel" id="v_tourtravel" style="width: 100%;">
			            		<option value="">- Pilih Tour Travel -</option>
			            		<?php
			            		foreach($kdtravel as $val)
			            		{
									?><option value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
								}
			            		?>
			            	</select>
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Type Option</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_type_beo" id="v_type_beo" style="width: 100%;" onchange="cek_type()">
			            		<option value="1">BEO</option>
			            		<option value="2">Non BEO</option>
			            	</select>
                            </div>
                            
                        </div>
                        
                        
                        <div id="tampil_beo" class="form-group">
                            <label class="control-label col-md-3">BEO</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_beo" id="v_beo" style="width: 100%;">
			            		<option value="">- Pilih BEO -</option>
			            		<?php
			            		foreach($beo as $val)
			            		{
									?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
								}
			            		?>
			            	</select>
                        </div>
                            
                        </div>
                        
                                                
                        <div class="form-group">
                            <label class="control-label col-md-3">No. Voucher Travel</label>
                            <div class="col-md-6">
                                <input placeholder="XYZ01234 or ABCDEF0001-ABCDEF0099" type="text" class="form-control" value="" name="v_no_voucher_travel" id="v_no_voucher_travel" >
                            	<font size='1'><b>*) Jika Voucher Lebih Dari 1 Dan Berurut Maka Formatnya VoucherAwal-VoucherAkhir (format terdiri dari 6 Huruf Awal 4 Angka Akhir Cont. ABCEDF0001-ABCEDF0999)</b></font>
                            </div>
                        </div>                            
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Nilai</label>
                            <div class="col-md-6">
                                <input style="text-align: right;" name="Nilai" id="Nilai" class="form-control" type="text" value="0" >
                            </div>
                        </div>
                        
                        <!--<div class="form-group">
                            <label class="control-label col-md-3">No Bukti</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" name="v_no_bukti" id="v_no_bukti" >
                            </div>
                        </div>-->
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" name="v_ket" id="v_ket" >
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-info btn-icon btn-sm icon-left">Save<i class="entypo-check"></i></button>
                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" data-dismiss="modal">Cancel<i class="entypo-cancel"></i></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>



<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

	$(document).ready(function()
		{
			getData();				
		});
		
	
	function getData(){
		base_url = $("#baseurl").val();
		offset = $("#offset").val();
			
			$('#pleaseWaitDialog').modal('show');
			
	    	$.ajax({
				type: "POST",
				url: base_url + "index.php/transaksi/voucher_beo/getList/"+offset,
				success: function(data) {
					$('#getList').html(data);
					
						$('#pleaseWaitDialog').modal('hide');
					
			
				}
			});
	}

	function bayar()
    {
     save_method = 'add';
     
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        
         $.ajax({
            url: "<?php echo site_url('transaksi/voucher_beo/generate_number_voucher') ?>/",
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       //alert(data);
            {
                $('[name="v_no_voucher"]').val(data.v_no_voucher);
                
                $('#modal_payment_method').modal('show');
                $('#btnSave').attr('disabled',false);
                $('.modal-title').text('Voucher');
                //document.getElementById('v_no_bukti').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
                
    }
    
    function edit_bayar(id)
    {
     
     	save_method = 'edit';
     	
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('transaksi/voucher_beo/ajax_edit_bayar') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       //alert(data);
            {
                
                $('[name="id"]').val(data.id);
                $('[name="v_date_payment"]').val(data.expDate);
                $('[name="v_no_voucher"]').val(data.no_voucher);
                $('[name="v_tourtravel"]').val(data.tourtravel);
                $('[name="v_no_voucher_travel"]').val(data.no_voucher_travel);
                $('[name="Nilai"]').val(data.nilai);
                $('[name="v_no_bukti"]').val(data.NoBukti);
                $('[name="no_receipt"]').val(data.no_receipt);
                $('[name="v_ket"]').val(data.Keterangan);
                $('[name="v_beo"]').val(data.BEO);
                
                if($('#v_beo').val()!=""){
				$('[name="v_type_beo"]').val("1");	
				document.getElementById("tampil_beo").style.display = "";
				}else{				
				$('[name="v_type_beo"]').val("2");	
				document.getElementById("tampil_beo").style.display = "none";				
				}
                
                $('#modal_payment_method').modal('show');
                $('#btnSave').attr('disabled',false);
                $('.modal-title').text('Voucher');
                //document.getElementById('v_no_bukti').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
              
    }
    
    function view_bayar(id)
    {
     
     	save_method = 'view';
     	
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('transaksi/voucher_beo/ajax_edit_bayar') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                $('[name="id"]').val(data.id);
                $('[name="v_date_payment"]').val(data.expDate);
                $('[name="v_no_voucher"]').val(data.no_voucher);
                $('[name="v_tourtravel"]').val(data.tourtravel);
                $('[name="v_no_voucher_travel"]').val(data.no_voucher_travel);
                $('[name="Nilai"]').val(data.nilai);
                $('[name="v_no_bukti"]').val(data.NoBukti);
                $('[name="no_receipt"]').val(data.no_receipt);
                $('[name="v_ket"]').val(data.Keterangan);
                $('[name="v_beo"]').val(data.BEO);
                
                if($('#v_beo').val()!=""){
				$('[name="v_type_beo"]').val("1");	
				document.getElementById("tampil_beo").style.display = "";
				}else{				
				$('[name="v_type_beo"]').val("2");	
				document.getElementById("tampil_beo").style.display = "none";				
				}
                
                
                $('#modal_payment_method').modal('show');
                $('#btnSave').attr('disabled',true);
                $('.modal-title').text('Voucher');
                //document.getElementById('v_no_bukti').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
              
    }

    function upload_bts(id)
    {
     
        
        swal({
              title: "Anda Yakin Upload Voucher ini ke BTS?",
              text: "Cek Lagi sebelum Upload ke BTS",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {

                $('#pleaseWaitDialog').modal('show');

                nodok = id.replace(/\//g, "-");
                $.ajax({
                    url: "<?php echo site_url('transaksi/voucher_beo/doUpload') ?>/" + nodok,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        
                         $('#pleaseWaitDialog').modal('hide');

                         if(data=="sukses"){
                            swal("Success!", "Upload Voucher ke BTS Sukses!", "success");
                            getData(); 
                         }else{
                            swal("Failed!", "Upload Voucher ke BTS Gagal!", "error");

                         }
                            getData(); 

                        
                        
                    }
                    ,
                    error: function (textStatus, errorThrown)
                    {
                        alert('Error Upload to BTS');
                    }
                }
                );

              } else {
                swal("Oke! Upload Cancel");
              }
            });

        
              
    }
    
    function save()
    {
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled', true);
        var url;

        if (save_method == 'add') {
            url = "<?php echo site_url('transaksi/voucher_beo/ajax_add') ?>";
        } else {
            url = "<?php echo site_url('transaksi/voucher_beo/ajax_update_bayar') ?>";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function (data)
            {
                if (data.status)
                {
                    id= data.NoDokumen;
                    $('#modal_payment_method').modal('hide');
                    getData();
                    //window.location.reload(true);
                   
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('save');
               // $('#btnSave').attr('disabled', false);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //alert('Error adding / update data');
                $('#modal_payment_method').modal('hide');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled', false);

            }
        });
    }
    	
function lock(nodok,url)
{
	var r=confirm("Record Pembayaran ini akan di Lock?")
	if (r==true)
	{
		//window.location = url+"index.php/transaksi/voucher_beo/lock/"+nodok;
		
		$.ajax({
            url: "<?php echo site_url('transaksi/voucher_beo/lock') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data){
				  	getData();
				  }      		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
	}
	else
	{
  		return false;
	}
}

function cari_data()
{
	    keyword = $('#search_keyword').val();
	    status = $('#search_status').val();
	    offset = "0";
	    if(keyword==""){
			key=0;
		}else{
			key = keyword;
		}
		
		$('#pleaseWaitDialog').modal('show');
		
		$.ajax({
            url: "<?php echo site_url('transaksi/voucher_beo/getList') ?>/" +offset+"/"+ key+"/"+status,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#pleaseWaitDialog').modal('hide');
				       		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	
}

function refresh()
{
	    
		
		$('#pleaseWaitDialog').modal('show');
		
		$.ajax({
            url: "<?php echo site_url('transaksi/voucher_beo/getList') ?>/0/",
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#pleaseWaitDialog').modal('hide');
				       		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	
}

function cek_type(){
	 type = $('#v_type_beo').val();
	 if(type=="1"){
	 	document.getElementById("tampil_beo").style.display = "";
	 }else{
	 	document.getElementById("tampil_beo").style.display = "none";
	 }
	
}

	
</script>
