<?php
    $this->load->view('header');	
	$this->load->view('js/TextValidation');
	$this->load->view('js/SelectValidation');
?>
<style>
.widthonehundred{
	width:150px;
}
.widthtwohundred{
	width:220px;
}
.RTL {
	text-align:right;
}

</style>
<script>
	$(document).ready(function(){
		
	});
		
	Array.prototype.indexOf = function( v, b, s ) {
		for( var i = +b || 0, l = this.length; i < l; i++ ) {
			if( this[i]===v || s && this[i]==v ) { return i; }
		}
		return -1;
	};

	Array.prototype.unique = function(b) {
		var a = [], i, len = this.length;
		for(i=0; i<len; i++ ) {
			if( a.indexOf( this[i], 0, b ) < 0 ) {
				a.push( this[i] );
			}
		}
		return a;
	};
	
</script>
<body>
	<form action='<?=base_url()?>index.php/pembayaran_supplier/tambahproses/' method='post' id='form1' name='form1'>
	<div class="row">
		<div class="col-md-1">
			Travel
		</div>
		<div class="col-md-2">
			<?=form_dropdownDB_init('KdTravel', $mastertravel, 'KdTravel', 'Nama', '', '', '------------ Pilih --------', 'id="KdTravel" class="widthonehundred" class="form-control-new"');?>
		</div>
		<div class="col-md-1">
			Keterangan
		</div>
		<div class="col-md-2">
			<input type="text" name="Keterangan" id="Keterangan" value="" class="form-control-new" />
		</div>
		<div class="col-md-6">
			&nbsp;
		</div>
	</div>
		<br/>
	<div class="row">
		<div class="col-md-1">
			Komisi I
		</div>
		<div class="col-md-2">
			<select name="Komisi1H" id="Komisi1H" />
				<option value="">-- Pilih Jenis Komisi --</option>
				<option value="OFFICE">OFFICE</option>
				<option value="GUIDE">GUIDE</option>
				<option value="TOUR LEADER">TOUR LEADER</option>
				<option value="DRIVER">DRIVER</option>
			</select>
		</div>
		<div class="col-md-1">
			Komisi II
		</div>
		<div class="col-md-2">
			<select name="Komisi2H" id="Komisi2H" />
				<option value="">-- Pilih Jenis Komisi --</option>					
				<option value="GUIDE">GUIDE</option>
				<option value="TOUR LEADER">TOUR LEADER</option>
				<option value="DRIVER">DRIVER</option>
				<option value="OFFICE">OFFICE</option>
			</select>
		</div>
		<div class="col-md-6">
			&nbsp;
		</div>
	</div>
		<br/>
	<div class="row">	
		<div class="col-md-1">
			Komisi III
		</div>
		<div class="col-md-2">
			<select name="Komisi3H" id="Komisi3H" />
				<option value="">-- Pilih Jenis Komisi --</option>					
				<option value="TOUR LEADER">TOUR LEADER</option>
				<option value="DRIVER">DRIVER</option>
				<option value="OFFICE">OFFICE</option>
				<option value="GUIDE">GUIDE</option>
			</select>
		</div>
		<div class="col-md-1">
			Komisi IV
		</div>
		<div class="col-md-2">
			<select name="Komisi4H" id="Komisi4H" />
				<option value="">-- Pilih Jenis Komisi --</option>					
				<option value="DRIVER">DRIVER</option>
				<option value="OFFICE">OFFICE</option>
				<option value="GUIDE">GUIDE</option>
				<option value="TOUR LEADER">TOUR LEADER</option>
			</select>
		</div>
		<div class="col-md-6">
			&nbsp;
		</div>
	</div>
		<br/>
	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
	<table class="table table-bordered responsive">
		<thead class="title_table">
			<tr id="tr0">
				<th>Barang</th>
				<th>Komisi I</th>
				<th>Komisi II</th>
				<th>Komisi III</th>
				<th>Komisi IV</th>
				<th>Komisi IVbag</th>
				<th>Total</th>
				<th colspan="2">&nbsp;</th>
			</tr>	
		</thead>
		<tbody></tbody>
			<tr>
				<td align='center' colspan='9'>
					<input type="submit" name='submit' value="Simpan" class='button'>
				</td>
			</tr>
	</table>
	</div>
	</form>	
</div>
	<script>
		new Spry.Widget.ValidationTextField("KdTravel", "none");
		new Spry.Widget.ValidationTextField("Keterangan", "none");
		new Spry.Widget.ValidationSelect("Komisi1H");
		new Spry.Widget.ValidationTextField("dataunique");
	</script>
</body>

<?php $this->load->view('footer')?>