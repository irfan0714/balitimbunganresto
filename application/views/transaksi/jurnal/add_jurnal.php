<?php
$this->load->view('header'); 
$url = base_url();
$gantikursor = "onkeydown=\"changeCursor(event,'jurnal',this)\"";
$cek = "onblur=\"cektanggal('$url')\"";?>
    <script language="javascript" src="<?=base_url();?>public/js/jurnal.js"></script>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
    
    <script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.css"/>
<style type="text/css">
<!--
/*#Layer1 {*/
	/*position:absolute;*/
	/*left:45%;*/
	/*top:40%;*/
	/*width:0px;*/
	/*height:0px;*/
	/*z-index:1;*/
	/*background-color:#FFFFFF;*/
/*}*/
-->
</style>
<body onload="firstLoad('jurnal');loading()">
<?php
if($pesan<>"new")
{
    ?>
	<script language="javascript">
	alert('Nomor giro sudah ada : '+'<?=$pesan?>');
    </script>
	<?
}
?>
<form method='post' name="jurnal" id="jurnal" action='<?=base_url();?>index.php/transaksi/jurnal/save_new_jurnal' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Jurnal</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="11" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"20","20","readonly='readonly'","text",$cek,"1");
			
			echo '<span id="span_loading" style=" display: none;"><img src="../../../../../public/images/ajax-image.gif"/> &nbsp;&nbsp;CEK TANGGAL DALAM PERIODE GL <br><br></span>'
			
			?>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select class="form-control-new" size="1" id="jenistr" name="jenistr" <?=$gantikursor;?>>
					<option value="">--Please Select--</option>
					<?php
					$jenis = "1";
					$nilaijenis = array_keys($mjenis);
					for($a = 0;$a<count($mjenis);$a++){
					 	$select = "";
					 	if($jenis==$nilaijenis[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenis[$nilaijenis[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
						
			
			<?
			//echo $mylib->write_combo("Dept","dept",$mdept,"","KdDepartemen","NamaDepartemen",$gantikursor,"onchange=\"simpanDept();\"","ya");
			//echo $mylib->write_combo("Project","project",$mproject,"","KdProject","NamaProject",$gantikursor,"onchange=\"simpanProject();\"","ya");
			//echo $mylib->write_combo("Cost Center","costcenter",$mcostcenter,"","KdCostCenter","NamaCostCenter",$gantikursor,"onchange=\"simpanCostCenter();\"","ya");
			
			echo $mylib->write_textbox("Nomor Transaksi","notrans","","25","25","","text",$gantikursor,"1");
			
		    //echo $mylib->write_textbox("Keterangan","ket","","75","70","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Debit","jumlahdebit","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
		    echo $mylib->write_number("Jumlah Kredit","jumlahkredit","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="detailNew()"></td>
					<td>Nama Rekening</td>
					<td style="display:none;">No Rekening</td>
					<td>SubDivisi</td>
					<td>Debit</td>
					<td>Kredit</td>
					<td>Keterangan</td>
				</tr>
				<?=$mylib->write_detail_jurnal(1,"","","",0,0,"",$sbdivisi)?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
			    <input type='hidden' id="dept" name="dept" value="<?=$aplikasi->DefaultKdDepartemen?>">
				<input type='hidden' id="project" name="project" value="<?=$aplikasi->DefaultKdProject?>">
				<input type='hidden' id="costcenter" name="costcenter" value="<?=$aplikasi->DefaultKdCostCenter?>">
			    <input type='hidden' id="hidedept" name="hidedept" value="<?=$aplikasi->DefaultKdDepartemen?>">
				<input type='hidden' id="hideproject" name="hideproject" value="<?=$aplikasi->DefaultKdProject?>">
				<input type='hidden' id="hidecostcenter" name="hidecostcenter" value="<?=$aplikasi->DefaultKdCostCenter?>">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/jurnal/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>
<script>

	var listrek = '';
	
	$(function() {
  		var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: ''}
	     }).done(function(reks){
	     	 listrek = reks.split('#');
	     });;
	});
	
	function pindahin(e,obj){
		
		id = obj.id;
		counter = id.substr(9,id.length-1)
	
		
		if(window.event) // IE
			{
				var code = e.keyCode;
			}
			else if(e.which) // Netscape/Firefox/Opera
			{
				var code = e.which;
			}
			if (code == 13) {
				$('#debit'+counter).val('');
		        $('#debit'+counter).focus();
		    }
		
	}
	
	function pindahlagi(e,obj){
		
		id = obj.id;
		counter = id.substr(10,id.length-1)
		
		
		if(window.event) // IE
			{
				var code = e.keyCode;
			}
			else if(e.which) // Netscape/Firefox/Opera
			{
				var code = e.which;
			}
			if (code == 13) {
		        $('#subdivisi'+counter).focus();
		    }
		
	}
	
	function tambahbaris(e,obj){
		
		id = obj.id;
		counter = id.substr(10,id.length-1)
		
		
		if(window.event) // IE
			{
				var code = e.keyCode;
			}
			else if(e.which) // Netscape/Firefox/Opera
			{
				var code = e.which;
			}
			if (code == 13) {
		        detailNew();
		    }
		
	}
	
	function cariRek(obj){
		id = obj.id;
		rek = $("#"+id).val();
		id_length = id.length;
		counter = id.substr(12,id_length-1)
				
		//var reklist = loadRekening(id);
		
		$("#"+id).autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
    			//alert(value.substr(0,7));
   				 //$("#"+id).val(value.substr(0,7)); 
   				 $("#kdrekening"+counter).val(value.substr(0,8));  
   				 $("#namarekening"+counter).autocomplete("destroy");
   				 $("#subdivisi"+counter).focus();
			}
    	});		
		
	};
	
	function cektanggal(url){
		tgl =  $('#tgl').val();
		if(tgl!=''){
			$("#span_loading").css("display","");
			$.ajax({
						url: url+"index.php/transaksi/jurnal/ajax_cek_tanggal/",
						data: {tgl:tgl},
						type: "POST",
						dataType: 'JSON',					
						success: function(res)
						{
							
							$("#span_loading").css("display","none");
							if(res.hasil=='0'){
								$('#tgl').focus();
								$('#tgl').val('');
								alert('Tanggal Harus Dalam Kurun waktu bulan GL atau Melebihi Tanggal '+res.tanggal);	
							}
						},
						error: function(e) 
						{
							alert(e);
						} 
					});
		}
	}
	
	function deleteRowJurnal(obj)
{
	objek = obj.id;
	id = objek.substr(3,objek.length-3);
	kdrekening = $("#kdrekening"+id).val();
	var banyakBaris = 1;
	var lastRow = document.getElementsByName("kdrekening[]").length;
	var index = 0;
	var indexs = 0;
	for(index=0;index<lastRow;index++){
		nama = document.getElementsByName("kdrekening[]");
		temp = nama[index].id;
		indexs = temp.substr(10,temp.length-10);
		if($("#savekdrekening"+indexs).val()!=""){
			banyakBaris++;
		}
	}
	//if($("#savekdrekening"+id).val()==""&&banyakBaris>1){
		$('#baris'+id).remove();
		ids = id-1;
		InputDebit(ids);
		InputKredit(ids);
		
	//}
	/*else if($("#savekdrekening"+id).val()==""&&banyakBaris==1){
		alert("Baris ini tidak dapat dihapus\nMinimal harus ada 1 baris");
	}
	else{
		if(banyakBaris==2)
		{
			alert("Baris ini tidak dapat dihapus\nMinimal harus ada 1 baris tersimpan");
		}
		else
		{
			no = $("#nodok").val();
			if(kdrekening!=""){
				var r=confirm("Apakah Anda Ingin Menghapus Kode Rekening "+kdrekening+" ?");
				if(r==true){
					$('#baris'+id).remove();
					if(no!=""){
						deleteItem(kdrekening,id);
					}
				}
			}
		}
	}*/
}
	
</script>