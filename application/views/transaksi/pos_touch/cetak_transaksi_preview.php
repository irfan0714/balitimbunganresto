<?php
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

//$ftext = printer_open("\\\\".$_SERVER['REMOTE_ADDR']."\\HPLaserJ");//
//$ftext = printer_open("\\\\192.168.0.30\\EPSON");
//$ftext = printer_open("\\\\".$ip."\\".$nm_printer);
//$ftext = printer_open($_SERVER['REMOTE_ADDR']);
$ftext = printer_open($nm_printer);
printer_set_option($ftext, PRINTER_MODE, "raw");
printer_set_option($ftext, PRINTER_COPIES, "1");
$alamatPT = $store[0]['Alamat1PT'];
$tgl = $header[0]['Tanggal'];
$tgl_1 = explode("-", $tgl);
$tgl_tampil = $tgl_1[2] . "/" . $tgl_1[1] . "/" . $tgl_1[0];

//printer_draw_bmp($ftext, "public/images/Logosg.png", 60, 5);  // Logo Dir, lenght H , With V

//printer_write(55);
printer_write($ftext, $reset . $elite);

printer_write($ftext, $dwidth.str_pad("PREVIEW BILL",39," ",STR_PAD_BOTH).$ndwidth."\r\n");

printer_write($ftext, "\r\n");
printer_write($ftext, str_pad($tgl_tampil . " - " . $header[0]['Waktu'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_write($ftext, "Struk : " . $header[0]['NoKassa'] . "-" . $header[0]['NoTrans'] . " / " . $header[0]['Kasir']." / ".$header[0]['IDAgent'] . "\r\n");
printer_write($ftext, "========================================\r\n");
$bt = 0;
$totdisc = 0;
for ($a = 0; $a < count($detail); $a++) {
	$subtotal = $detail[$a]['Qty']*round($detail[$a]['Harga']);
	$totdisc += round($detail[$a]['Disc']);
    printer_write($ftext, str_pad(substr($detail[$a]['NamaStruk'], 0, 20), 20) .
        str_pad($detail[$a]['Qty'], 5, " ", STR_PAD_LEFT) .
        str_pad(round($detail[$a]['Harga']), 7, " ", STR_PAD_LEFT) .
        str_pad(round($subtotal), 8, " ", STR_PAD_LEFT) . "\r\n");
    $bt += $subtotal;
}
printer_write($ftext, "----------------------------------------\r\n");
printer_write($ftext, str_pad("Total " . $header[0]['TotalItem'] . " item", 24) . ":" . str_pad(number_format($bt, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if($totdisc>0){
	printer_write($ftext, str_pad("Discount ", 24) . ":" . str_pad(number_format($totdisc, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");	
}
if ($header[0]['Ttl_Charge'] <> 0){
    printer_write($ftext, str_pad("Service charge ", 24) . ":" . str_pad(number_format($header[0]['Ttl_Charge'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
if ($header[0]['TAX'] <> 0){
    printer_write($ftext, str_pad("TAX ", 24) . ":" . str_pad(number_format($header[0]['TAX'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
//if ($header[0]['TAX'] <> 0){
//    printer_write($ftext, str_pad("Tax ", 24) . ":" . str_pad(number_format($header[0]['TAX'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
//}
if ($header[0]['Voucher'] <> 0){
    printer_write($ftext, str_pad("Voucher ", 24) . ":" . str_pad(number_format($header[0]['Voucher'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}

$gtotal = $header[0]['TotalNilai'] - $header[0]['Voucher'];
printer_write($ftext, str_pad("Total Sales ", 24) . ":" . str_pad(number_format($gtotal, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");

printer_write($ftext, "========================================\r\n");
printer_write($ftext, "             ===Terima kasih===          \r\n");

/*
if($kdkategori=="03")
{
printer_write($ftext, $dwidth.str_pad("JOB ORDER",39," ",STR_PAD_BOTH).$ndwidth."\r\n");
printer_write($ftext, str_pad($tgl_tampil . " - " . $header[0]['Waktu'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_write($ftext, "Struk : " . $header[0]['NoKassa'] . "-" . $header[0]['NoStruk'] . " / " . $header[0]['Kasir'] . "\r\n");
printer_write($ftext, "----------------------------------------\r\n");
for ($a = 0; $a < count($detail); $a++) {
    printer_write($ftext, $dwidth.str_pad(substr($detail[$a]['NamaStruk'], 0, 20), 20) .
        str_pad($detail[$a]['Qty'], 5, " ", STR_PAD_LEFT).$ndwidth."\r\n");
}
printer_write($ftext, "----------------------------------------\r\n");
}
*/
printer_write($ftext, "\r\n");
printer_write($ftext, $fcut);
printer_close($ftext);
?>