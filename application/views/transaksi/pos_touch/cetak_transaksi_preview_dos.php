<?php 
$alamatPT = $store[0]['Alamat1PT'];
$tgl = $header[0]['Tanggal'];
$tgl_1 = explode("-", $tgl);
$tgl_tampil = $tgl_1[2] . "/" . $tgl_1[1] . "/" . $tgl_1[0];
?>
<?=$reset.$elite?>
<?=aligncenter($store[0]['NamaPT'],39).$ndwidth?><?=$nl?>
<?=aligncenter($store[0]['NPWP'],39).$ndwidth?><?=$nl?>
<?=aligncenter($store[0]['Alamat1PT'],39).$ndwidth?><?=$nl?>
<?=aligncenter($store[0]['Alamat2PT'],39).$ndwidth?>
<?=$nl?>
<?=$nl?>
<?=aligncenter($header[0]['KdMeja'] ." - " . $tgl_tampil . " - " . $header[0]['Waktu'],39);?><?=$nl?>
<?=aligncenter("Struk : " . $header[0]['NoKassa'] . "-" . $header[0]['NoTrans'] . " / " . $header[0]['Kasir']."-".$header[0]['IDAgent']);?><?=$nl?>
========================================<?=$nl?>
<?php
$bt = 0;
$totdisc = 0;
for ($a = 0; $a < count($detail); $a++) {
	$subtotal = $detail[$a]['Qty']*round($detail[$a]['Harga']);
	$totdisc += round($detail[$a]['Disc']);
    echo alignleft(substr($detail[$a]['NamaStruk'],0,18), 19) .
        alignright($detail[$a]['Qty'], 4) .
        alignright(round($detail[$a]['Harga']), 6) .
        alignright(round($subtotal), 7) . $nl;
    if($detail[$a]['Disc']>0){
		echo alignleft('Discount',31) .
        alignright(round($detail[$a]['Disc']*-1), 7) . $nl;
	}
	if($detail[$a]['Disc']<0){
		echo alignleft('Tambahan',31) .
        alignright(round($detail[$a]['Disc']*-1), 7) . $nl;
	}
    $bt += $subtotal;
}
?>
----------------------------------------<?=$nl?>
<?php
echo alignleft("Total " . $header[0]['TotalItem'] . " item", 22) . ":" . alignright(number_format($bt, 0, ',', '.'), 15) . $nl;
if($totdisc>0){
	echo alignleft("Discount ", 22) . ":" . alignright(number_format($totdisc, 0, ',', '.'), 15) . $nl;	
}
if($totdisc<0){
	echo alignleft("Tambahan ", 22) . ":" . alignright(number_format($totdisc*-1, 0, ',', '.'), 15) . $nl;	
}
if ($header[0]['Ttl_Charge'] <> 0){
    echo alignleft("Service charge ", 22) . ":" . alignright(number_format($header[0]['Ttl_Charge'], 0, ',', '.'), 15) . $nl;
}
if ($header[0]['TAX'] <> 0){
    echo alignleft("TAX ", 22) . ":" . alignright(number_format($header[0]['TAX'], 0, ',', '.'), 15) . $nl;
}
if ($header[0]['Voucher'] <> 0){
    echo alignleft("Voucher ", 22) . ":" . alignright(number_format($header[0]['Voucher'], 0, ',', '.'), 15) . $nl;
}

$gtotal = $header[0]['TotalNilai'] - $header[0]['Voucher'];
echo alignleft("Total Sales ", 22) . ":" . alignright(number_format($gtotal, 0, ',', '.'), 15) . $nl;
?>
========================================<?=$nl?>
             ===Terima kasih===         <?=$nl?>
<?=$nl.$fcut?>