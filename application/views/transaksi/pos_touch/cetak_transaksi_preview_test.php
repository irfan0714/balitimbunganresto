<?php
function printer_test($a) {
	echo $a;
}
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

$alamatPT = $store[0]['Alamat1PT'];
$tgl = $header[0]['Tanggal'];
$tgl_1 = explode("-", $tgl);
$tgl_tampil = $tgl_1[2] . "/" . $tgl_1[1] . "/" . $tgl_1[0];

printer_test( $reset . $elite);

printer_test( $dwidth.str_pad("PREVIEW BILL",39," ",STR_PAD_BOTH).$ndwidth."\r\n");

printer_test( "\r\n");
printer_test( str_pad($tgl_tampil . " - " . $header[0]['Waktu'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_test( "Struk : " . $header[0]['NoKassa'] . "-" . $header[0]['NoTrans'] . " / " . $header[0]['Kasir']." / ".$header[0]['IDAgent'] . "\r\n");
printer_test( "========================================\r\n");
$bt = 0;
$totdisc = 0;
for ($a = 0; $a < count($detail); $a++) {
	$subtotal = $detail[$a]['Qty']*round($detail[$a]['Harga']);
	$totdisc += round($detail[$a]['Disc']);
    printer_test( str_pad(substr($detail[$a]['NamaStruk'], 0, 20), 20) .
        str_pad($detail[$a]['Qty'], 5, " ", STR_PAD_LEFT) .
        str_pad(round($detail[$a]['Harga']), 7, " ", STR_PAD_LEFT) .
        str_pad(round($subtotal), 8, " ", STR_PAD_LEFT) . "\r\n");
    $bt += $subtotal;
}
printer_test( "----------------------------------------\r\n");
printer_test( str_pad("Total " . $header[0]['TotalItem'] . " item", 24) . ":" . str_pad(number_format($bt, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
if($totdisc>0){
	printer_test( str_pad("Discount ", 24) . ":" . str_pad(number_format($totdisc, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");	
}
if ($header[0]['Ttl_Charge'] <> 0){
    printer_test( str_pad("Service charge ", 24) . ":" . str_pad(number_format($header[0]['Ttl_Charge'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
if ($header[0]['TAX'] <> 0){
    printer_test( str_pad("TAX ", 24) . ":" . str_pad(number_format($header[0]['TAX'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}
//if ($header[0]['TAX'] <> 0){
//    printer_test( str_pad("Tax ", 24) . ":" . str_pad(number_format($header[0]['TAX'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
//}
if ($header[0]['Voucher'] <> 0){
    printer_test( str_pad("Voucher ", 24) . ":" . str_pad(number_format($header[0]['Voucher'], 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");
}

$gtotal = $header[0]['TotalNilai'] - $header[0]['Voucher'];
printer_test( str_pad("Total Sales ", 24) . ":" . str_pad(number_format($gtotal, 0, ',', '.'), 15, " ", STR_PAD_LEFT) . "\r\n");

printer_test( "========================================\r\n");
printer_test( "             ===Terima kasih===          \r\n");

?>