<?php $this->load->view('header_part1')?>
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	<script>
	$(document).ready(function(){
		$('#tgldari').Zebra_DatePicker({format: 'Y-m-d'});
        $('#tglsampai').Zebra_DatePicker({format: 'Y-m-d'});
	});
	</script>
</head>
<?php $this->load->view('header_part3')?>

<form method="POST"  name="search" action='<?=base_url(); ?>index.php/transaksi/pembayaran_komisi_group/submit'>
   <table cellspacing="7">
   		<tr>
   			<td >Tanggal</td>
            <td>&nbsp;&nbsp;<input type='text' name='tgldari' value="<?= $tgl?>" size='12' id="tgldari"  class="form-control-new" readonly /></td>
   		</tr>
        <tr>
            <td >Group</td>
            <td style="display">
                        &nbsp;&nbsp;<select name="group" id="groupp" class="form-control-new">
                            <option value='' width="120">-Pilih-</option>
                            
                                <?php foreach ($combo->result() as $as): ?>  
                                
                           <option value="<?=$as->KdTravel;?>"><?=$as->Keterangan?></option>
                                <?php endforeach;?>
                        </select>
                    </td>
        </tr>
        <tr>
            <td >Kas / Bank</td>
            <td style="display">
                        &nbsp;&nbsp;<select name="kas" id="kas" class="form-control-new">
                            <option value='' width="120">-Pilih-</option>
                            
                                <?php foreach ($combokas->result() as $kas): ?>
                                
                            <option value="<?=$kas->KdKasBank;?>"><?=$kas->NamaKasBank?></option>
                                <?php endforeach;?>
                        </select>
                    </td>
        </tr>
        <tr>
            <td >Dibayar Kepada</td>
            <td>&nbsp;&nbsp;<input type="text" name='topaid' value="<?=$paidto;?>" class="form-control-new" /></td>
        </tr>
        <tr>
            <td >Nomor Bukti</td>
            <td>&nbsp;&nbsp;<input type="text" name='nobukti' value="<?=$nobukti?>" class="form-control-new" /></td>
        </tr>
        <tr>
            <td >Keterangan</td>
            <td>&nbsp;&nbsp;<input type="text" name='keterangan' value="<?=$keterangan;?>" class="form-control-new" /></td>
        </tr>
        <tr>
            <td style="display" >
                        &nbsp;<input type="submit" name="submit" id="submit" value="submit" class='btn btn-default'><br>
            </td>
        </tr>
   </table>
</form>

<hr/>

    <?php
    if($tampilkanDT)
    {
        $this->load->view('transaksi/pembayaran_komisi_group/tampil');
    }
    ?>
	<?php 
	$this->load->view('footer'); 
//	$this->load->view('footer_part1');
	?>
<!--dua div dibawah, untuk nutup div yg ada di header-->
	</div>	
</div>
<!--eo dua div diatas-->
</body>
</html>