<?php
$this->load->view('header');

$searchby = $this->input->post('searchby');
$date1 = $this->input->post('date1');
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>

<script language="javascript">
    $(function () {
                $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy'
                });
            });

    function bayartrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Membayar Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/transaksi/pembayaran_komisi_group_front/save_new_komisi", {
                kode: nodok},
            function (data) {
                //                            alert("data");
                window.location = url + "index.php/transaksi/pembayaran_komisi_group_front/save_new_komisi";


            });
        }
    }
            
    
    function deleteTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menghapus Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/keuangan/payment/delete_payment/", {
                kode: nodok},
            function (data) {
                //                            alert("data");
                window.location = url + "index.php/keuangan/payment/";
            });
        }
    }
    function PopUpPrint(kode, baseurl)
    {
        url = "index.php/transaksi/pembayaran_komisi_group_front/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }

    function gantiSearch()
    {
        if ($("#searchby").val() == "NoTransaksi")
        {
            $("#normaltext").css("display", "");
            $("#datetext").css("display", "none");
            //$("#date1").datepicker("destroy");
            $("#date1").val("");
        }
        else
        {
            $("#datetext").css("display", "");
            $("#normaltext").css("display", "none");
            $("#stSearchingKey").val("");
            //$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>/public/images/calendar.png' });
            //$('#date1').Zebra_DatePicker({format: 'd-m-Y'});
        }
    }
</script>
<body onload="option();">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    List Pembayaran Komisi Group 
                </div>
            </div>
            <div class="panel-body">
				<?php if ($date1 != ""){
					$hide = "style='display:'";
					$hide2 = "style='display:none'";
					}else{
						$hide = "style='display:none'";					
						$hide2 = "style='display:'";
					}
				?>
               <form method="POST"  name="search" action="">
                    <div class="control-group">
                        <div class="controls" align="right">
                            <table border="0">
                                <tr>
                                    <td id="normaltext" <?=$hide2;?>><input type='text' size='20' name='stSearchingKey' id='stSearchingKey' class="form-control" value="<?php echo @$_POST['stSearchingKey']; ?>"></td>

                                    <td id="datetext" <?=$hide;?>><input type='text' size='12' name='date1' id='date1' class="form-control datepicker" data-date-format="dd-mm-yyyy" value="<?php echo @$_POST['date1']; ?>"></td>

                                    <td>
                                        <select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()" class="form-control">
                                            <option <?php if ($searchby == "NoTransaksi") echo "selected='selected'"; ?> value="NoTransaksi">No Transaksi</option>

                                            <option <?php if ($searchby == "TanggalTransaksi") echo "selected='selected'"; ?> value="TglTransaksi">Tanggal</option>
                                        </select>
                                    </td>
                                    <td><button type="submit" class="btn-primary">GO</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>

                <br>
                 <table align = 'center' class='table table-bordered table-hover responsive'>
                            <thead>
                                <tr>
                                    <th nowrap><center>NoTransaksi</center></th>    
                                    <th nowrap><center>Tanggal</center></th>
                                    <th nowrap><center>Kd Travel</center></th>
                                    <th nowrap><center>Keterangan</center></th>
                                    <th nowrap><center>Kode Kas Bank</center></th>
                                    <th nowrap><center>Penerima</center></th>
                                    <th nowrap><center>No Bukti</center></th>
                                    <th nowrap><center>No Dokumen</center></th>
                                    <th nowrap><center>Komisi 1</center></th>
                                    <th nowrap><center>Komisi 2</center></th>
                                    <th nowrap><center>Komisi 3</center></th>
                                    <th nowrap><center>Komisi 4</center></th>
                                    <th nowrap><center>Total</center></th>

                                    <?php
                                    //if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                                        ?>
                                        <th colspan="2" nowrap ><center>Action</center></th>
                                    <?php //} 
                                    $mylib = new globallib(); ?>
                                </tr>
                            </thead>
                <?php

                    
                    $total =0;
                    $totalnya = 0;
                    $totalkomisi = 0;
                   // $totalkomisi1=0;
                    $totalkomisijadi=0;
                for ($a = 0; $a < count($data); $a++) 
                    {
                        ?>
                <form action=" <?=base_url(); ?>index.php/transaksi/pembayaran_komisi_group_front/save_new_komisi/" method="POST">

                    
                    <?php
                    if (count($data) == 0) {
                        ?>
                        <tr>
                            <td nowrap colspan="13  " align="center">Tidak Ada Data</td>
                        </tr>
                        <?php
                    }

                        
                        $komisi1 = $data[$a]['Komisi1'];
                            $repkomisi1=str_replace(",","", $komisi1);
                            $hslkomisi1 = str_replace(".", "", $repkomisi1);


                        $komisi2 = $data[$a]['Komisi2'];
                            $repkomisi2=str_replace(",","", $komisi2);
                            $hslkomisi2 = str_replace(".", "", $repkomisi2);


                        $komisi3 = $data[$a]['Komisi3'];
                            $repkomisi3=str_replace(",","", $komisi3);
                            $hslkomisi3 = str_replace(".", "", $repkomisi3);

                         $komisi4 = $data[$a]['Komisi4'];
                            $repkomisi4=str_replace(",","", $komisi4);
                            $hslkomisi4 = str_replace(".", "", $repkomisi4);

                            $total = $hslkomisi1+$hslkomisi2+$hslkomisi3+$hslkomisi4;


                     // $total =  @number_format($tes,2,".",",");
                      $totalnya = number_format($total, 0, ',', '.');
                    	//$grdtotal += $data[$a]['Total'];
                                             ?>

                        <input type="hidden" value="<?=$data[$a]['Penerima'];?>" name="penerima">
                        <input type="hidden" value="<?=$data[$a]['NoBukti'];?>" name="NoBukti">
                        <input type="hidden" value="<?=$data[$a]['NoDokumen'];?>" name="NoDokumen">
                        <input type="hidden" value="<?=$hslkomisi1;?>" name="Komisi1">
                        <input type="hidden" value="<?=$hslkomisi2;?>" name="Komisi2">
                        <input type="hidden" value="<?=$hslkomisi3;?>" name="komisi3">
                        <input type="hidden" value="<?=$hslkomisi4;?>" name="komisi4">
                        <input type="hidden" value="<?=$total;?>" name="total">
                        <input type="hidden" value="<?=$data[$a]['KdKasBank'];?>" name="KdKasBank">
                        <input type="hidden" value="<?=$data[$a]['Keterangan'];?>" name="Keterangan">
                        <input type="hidden" value="<?=$data[$a]['KdTravel'];?>" name="KdTravel">
                        <input type="hidden" value="<?=$data[$a]['TglTransaksi'];?>" name="TglTransaksi">
                        <input type="hidden" value="<?=$data[$a]['NoTransaksi'];?>" name="NoTransaksi">


                        <tr>
                            <td nowrap><?= $data[$a]['NoTransaksi']; ?></td>
                            <td nowrap><?= $data[$a]['TglTransaksi']; ?></td>
                            <td nowrap><?= $data[$a]['KdTravel']; ?></td>
                            <td nowrap><?= stripslashes($data[$a]['Keterangan']); ?></td>
                            <td nowrap><?= $data[$a]['KdKasBank']; ?></td>
                            <td nowrap><?= $data[$a]['Penerima']; ?></td>
                            <td nowrap><?= $data[$a]['NoBukti']; ?></td>
                            <td nowrap><?= $data[$a]['NoDokumen']; ?></td>
                            <td nowrap align='right'><?=$data[$a]['Komisi1']?> </td>
                            <td nowrap align='right'><?=$data[$a]['Komisi2']?></td>
                            <td nowrap align='right'><?=$data[$a]['Komisi3']?></td>
                            <td nowrap align='right'><?=$data[$a]['Komisi4']?></td>
                            <td nowrap align='right'><?=$totalnya?></td>
                            

                            <?php

                            //if ($link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <td nowrap>
                                    <?php
                                   // if ($link->view == "Y") {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/></a>
                                        <?php
                                   // }
                                    //if ($link->edit == "Y" && (substr($data[$a]['TglTransaksi'], 4, 2) == date('m'))) {
                                        ?>
                                        <!--<a href="<?= base_url(); ?>index.php/transaksi/pembayaran_komisi_group_front/edit_komisi/<?= $data[$a]['NoTransaksi']; ?>">--><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                                        <?php
                                   // }
                                   // if ($link->delete == "Y" && (substr($data[$a]['TglTransaksi'], 4, 2) == date('m'))) {
                                        ?>
                                        <!--<img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/>-->
                                        <img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete' onclick=""/>


                                        <!--<img src='<?= base_url(); ?>public/images/checkout.png' border = '0' width="20" title = 'Bayar' onclick="bayartrans('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/>
                                         -->

                                        <?php
                                   // }
                                    
                                    ?>
                                </td>
                                <td style="display: ">
                                    <input type="submit" name="bayar" value="Pay" class="btn btn-default">
                                </td>
                                
                            <?php //} ?>
							</tr>
                             </form>
                            <?php

                                $totalkomisi=$totalkomisi + $total;

                                 $totalkomisijadi = number_format($totalkomisi, 0, ',', '.');
                         }
                           /* $tot = 0;
                            $tot += $grdtotal;*/
                            
                            ?>
                            <tfoot>
                            	<tr>
                            		<td colspan="12" width="77%" style="text-align: right;">
                            			<b>Total Komisi</b>
                            		</td>
                                    <td style="text-align: right;">
                                        <b><?php echo $totalkomisijadi;?></b>
                                        <b><?php ?></b>
                                    </td>
                            		</td>
                            	</tr>
                            </tfoot>
                  
                </table>
                <table align = 'center'  >
                    <tr>
                        <td>
                            <?php echo $this->pagination->create_links(); ?>
                        </td>
                    </tr>
                    <?php
                  //  if ($link->add == "Y") {
                        ?>
                        <tr>
                            <td nowrap colspan="3">
                                <a href="<?= base_url(); ?>index.php/transaksi/pembayaran_komisi_group"><img src='<?= base_url(); ?>public/images/add.png' border = '0' title = 'Add'/></a>
                            </td>
                        </tr>
                    <?php // } ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php $this->load->view('footer'); ?>
