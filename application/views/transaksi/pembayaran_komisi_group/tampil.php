<?php
$mylib = new globallib();
?>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<form action="<?php echo base_url();?>index.php/transaksi/pembayaran_komisi_group/save" method="post">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="40"><center>No</center></th>
                <th><center>PCODE</center></th>
                <th><center>NAMA BARANG</center></th>
                <th width="120"><center>QTY</center></th>
                <th width="120"><center>NETTO</center></th>
                <th width="120"><center>OFFICE</center></th>
                <th width="120"><center>GUIDE</center></th>
                <th width="120"><center>TL</center></th>
                <th width="120"><center>DRIVER</center></th>
                <th width="120"><center>TOTAL</center></th>
            </tr>
        </thead>
        <tbody> 
         <?php
                  /* for($i=0; $i<count($data);$i++)
                    {*/
                        ?>
                  
                   <?php
                   //}
                   ?>
                
                 <?php
                 if(count($data)>0)
                 {
                        $total1 = 0;
                        $total2 = 0;
                        $total3 = 0;
                        $total4 = 0;
                        $total5 = 0;
                        $total6 = 0;
                    $no=1;

                    for($i=0; $i<count($data);$i++)
                    {

                        $bgcolor    = ($i % 2 == 0)?"#C8DFE6":"WHITE";

                        $total1 += (float)$data[$i]['Netto'];
                        $total2 += (float)$data[$i]['komisi1']/100*$data[$i]['Netto'];
                        $total3 += (float)$data[$i]['komisi2']/100*$data[$i]['Netto'];
                        $total4 += (float)$data[$i]['komisi3']/100*$data[$i]['Netto'];
                        $total5 += (float)$data[$i]['komisi4']/100*$data[$i]['Netto'];
                        $total6 += (float)$data[$i]['total']/100*$data[$i]['Netto'];

                    ?>
                    <tr style="background:<?=$bgcolor?>">

                     <input type="hidden" value="<?=$tgl;?>" name="tgl">
                     <input type="hidden" value="<?=$kas;?>" name="kas">
                     <input type="hidden" value="<?=$group;?>" name="group">
                     <input type="hidden" value="<?=$paidto;?>" name="paidto">
                     <input type="hidden" value="<?=$nobukti;?>" name="nobukti">
                     <input type="hidden" value="<?=$keterangan;?>" name="keterangan">
                     <input type="hidden" value="<?=$data[$i]['NoStruk'];?>" name="nostruk[]">
                     <input type="hidden" value="<?=$data[$i]['PCode'];?>" name="pcode[]">
                     <input type="hidden" value="<?=$data[$i]['NamaLengkap'];?>" name="namalengkap[]">
                     <input type="hidden" value="<?=$mylib->ubah_format($data[$i]['Qty'])?>" name="qty[]">
                     <input type="hidden" value="<?=$mylib->ubah_format($data[$i]['Netto'])?>" name="Netto[]">
                     <input type="hidden" value="<?=$mylib->ubah_format($data[$i]['komisi1']/100*$data[$i]['Netto'])?>" name="office[]">
                    <input type="hidden" value="<?=$mylib->ubah_format($data[$i]['komisi2']/100*$data[$i]['Netto'])?>" name="guide[]">
                    <input type="hidden" value="<?=$mylib->ubah_format($data[$i]['komisi3']/100*$data[$i]['Netto'])?>" name="tl[]">
                     <input type="hidden" value="<?=$mylib->ubah_format($data[$i]['komisi4']/100*$data[$i]['Netto'])?>" name="driver[]">


                        <td><?=$no;?></td>
                        <td><?=$data[$i]['PCode'];?></td>
                        <td><?=$data[$i]['NamaLengkap'];?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['Qty'])?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['Netto'])?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['komisi1']/100*$data[$i]['Netto'])?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['komisi2']/100*$data[$i]['Netto'])?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['komisi3']/100*$data[$i]['Netto'])?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['komisi4']/100*$data[$i]['Netto'])?></td>
                        <td nowrap align='right'><?=$mylib->ubah_format($data[$i]['total']/100*$data[$i]['Netto'])?></td>
                    </tr>
                    <?php

                    $no++;
                     }
                ?>
                    <tr>
                        <td nowrap align='center' bgcolor='#F0E68C' colspan="4"><b>Total</b></td>
                        <td nowrap align='right' bgcolor='#F0E68C '><b><?=$mylib->ubah_format($total1)?></b></td>
                        <td nowrap align='right' bgcolor='#F0E68C '><b><?=$mylib->ubah_format($total2)?></b></td>
                        <td nowrap align='right' bgcolor='#F0E68C '><b><?=$mylib->ubah_format($total3)?></b></td>
                        <td nowrap align='right' bgcolor='#F0E68C '><b><?=$mylib->ubah_format($total4)?></b></td>
                        <td nowrap align='right' bgcolor='#F0E68C '><b><?=$mylib->ubah_format($total5)?></b></td>
                        <td nowrap align='right' bgcolor='#F0E68C '><b><?=$mylib->ubah_format($total6)?></b></td>
                    </tr>
                    <tr>
                        <td nowrap align='center' colspan="10" style="display"><input type="submit" style="background-color: #87CEEB"  name="save" value="Save"  class="btn  btn-icon "></td>
                    </tr>
                <?php
                }
                else
                { ?>
                <tr>
                    <td nowrap align='center' bgcolor='#FFA500' colspan="10"><b>Tidak ada data</b></td>
                </tr>
                    <?php

                    }
                 ?>
                 </tr>
        </tbody>
    </table>
    </form>
</div>