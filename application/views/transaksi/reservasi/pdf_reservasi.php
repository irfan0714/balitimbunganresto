<?php
$mylib = new globallib();
$kdtravel = $results[0]['KdTravel'];
$travel = $this->reservasi_model->getTravel($kdtravel);
$nama = $travel->Nama;
$contact = $travel->Contact;
$phone = $travel->Phone;
$fax = $travel->Fax;
$alamat = $travel->Alamat;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Reservasi</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 7.5pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 7.5pt;
		}
		
	</style>
    <body>
        <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
                <td colspan="5" align="center"><img src="<?= base_url(); ?>public/images/Logosg.png" width="120" alt="Secret Garden Village"/></td>
            </tr>
            <tr>
                <td colspan="5" align="center"><b>BANQUET EVENT ORDER</b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><span id="NoDokumen"> <?= $results[0]['NoDokumen']; ?></span></td>
            </tr>
            <br><br>
            <tr>
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                            <td width="100">RESERVASI DATE</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $reservasidate; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">GROUP NAME</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $nama; ?></span></td>

                        </tr>
                        <tr>
                            <td>COMPANY</td>
                            <td>:</td>
                            <td><span id="company" name="company" ><?= $results[0]['KdTravel']; ?></span></td>

                        </tr>
                        <tr>
                            <td>TELEPHONE</td>
                            <td>:</td>
                            <td><span id="Telepon" name="Telepon"><?= $phone; ?></span></td>

                        </tr>
                        <tr>
                            <td valign="top">ADDRESS</td>
                            <td valign="top">:</td>
                            <td valign="top"><span id="alamat" name="alamat"><?= $alamat; ?></span></td>

                        </tr>
                        <tr>
                            <td>NATIONALITY</td>
                            <td>:</td>
                            <td><span id="nationality" name="nationality" >INDONESIA</span></td>

                        </tr>
                        <tr>
                            <td>PARTICIPANTS</td>
                            <td>:</td>
                            <td><span id="Participants" name="Participants"><?= $results[0]['adultParticipants']." Adult, ".$results[0]['childParticipants']." Child, ".$results[0]['kidsParticipants']." Infants"; ?></span></td>

                        </tr>
                    </table>
                </td>
                <td></td>
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        <tr>
                            <td width="100">ACTIVITIES 1</td>
                            <td width="1">:</td>
                            <td width="200"><span id="Event1" name="Event1">
                            <?php	            		
			            		foreach($aktivitas as $val)
			            		{
			            			$selected="";
									if($results[0]['Event1']==$val["id_aktivitas_beo"])
									{
										$aktivitas1=$val['nama_aktivitas_beo'];
									}
									?><?php
								}
	            		    ?>
                            
                           <span><?= $aktivitas1; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td width="100">ACTIVITIES 2</td>
                            <td width="1">:</td>
                            <td width="200"><span id="Event2" name="Event2">
                            <?php	            		
			            		foreach($aktivitas as $val)
			            		{
			            			$selected="";
									if($results[0]['Event2']==$val["id_aktivitas_beo"])
									{
										$aktivitas2=$val['nama_aktivitas_beo'];
									}
									?><?php
								}
	            		     ?>
                            
                           <span><?= $aktivitas2; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td width="100">ACTIVITIES 3</td>
                            <td width="1">:</td>
                            <td width="200"><span id="Event3" name="Event3">
                            <?php	            		
			            		foreach($aktivitas as $val)
			            		{
			            			$selected="";
									if($results[0]['Event3']==$val["id_aktivitas_beo"])
									{
										$aktivitas3=$val['nama_aktivitas_beo'];
									}
									?><?php
								}
	            		     ?>
                            
                           <span><?= $aktivitas3; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td width="100">ACTIVITIES 4</td>
                            <td width="1">:</td>
                            <td width="200"><span id="Event4" name="Event4">
                            <?php	            		
			            		foreach($aktivitas as $val)
			            		{
			            			$selected="";
									if($results[0]['Event4']==$val["id_aktivitas_beo"])
									{
										$aktivitas4=$val['nama_aktivitas_beo'];
									}
									?><?php
								}
	            		     ?>
                            
                           <span><?= $aktivitas4; ?></span></td>
                        </tr>
                        
                        
                        <tr>
                            <td width="100">ACTIVITIES 5</td>
                            <td width="1">:</td>
                            <td width="200"><span id="Event5" name="Event5">
                            <?php	            		
			            		foreach($aktivitas as $val)
			            		{
			            			$selected="";
									if($results[0]['Event5']==$val["id_aktivitas_beo"])
									{
										$aktivitas5=$val['nama_aktivitas_beo'];
									}
									?><?php
								}
	            		     ?>
                            
                           <span><?= $aktivitas5; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td>CONTACT</td>
                            <td>:</td>
                            <td width="50"><span id="Contact" name="Contact" ><?= substr($results[0]['Contact'], 0, 10); ?></span></td>
                        </tr>
                        <tr>
                            <td valign="top">PHONE</td>
                            <td valign="top">:</td>
                            <td width="50"><span id="Phone" name="Phone" ><?= $results[0]['Phone']; ?></span></td>
                        </tr>
                        <tr>
                            <td>SALES IN CHARGE</td>
                            <td>:</td>
                            <td width="50">
                            
                            <?php foreach($sales as $val)
	            		    {
	            		    $NamaSalesman="";
							if($results[0]['Sales_In_Charge']==$val["KdSalesman"])
							{
								$NamaSalesman=$val["NamaSalesman"];
							} ?>
							 <?= $NamaSalesman; ?>
							<?php }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td>JAM KEDATANGAN</td>
                            <td> : </td>
                            <td width="50"><?= $results[0]['Jam_Kedatangan']; ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td width="50">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
        <br>
        <tabel width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
        	<tr>
        		<td>
                   
                        <label>
                            <?php $checked = $results[0]['place_herborist'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_herborist" name="place_herborist" type="checkbox" <?php echo $checked; ?>>HERBORIST
                        </label>
                    
                	&nbsp;&nbsp;&nbsp;
            
                        <label>
                            <?php $checked = $results[0]['place_the_luwus'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_the_luwus" name="place_the_luwus" type="checkbox" <?php echo $checked; ?>>THE LUWUS ATAS
                        </label>
                        
                    &nbsp;&nbsp;&nbsp;
            
                        <label>
                            <?php $checked = $results[0]['place_the_luwus_bawah'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_the_luwus_bawah" name="place_the_luwus_bawah" type="checkbox" <?php echo $checked; ?>>THE LUWUS BAWAH
                        </label>
                    
                        
          			&nbsp;&nbsp;&nbsp;
          			
          				<label>
                            <?php $checked = $results[0]['place_the_rice_view'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_the_rice_view" name="place_the_rice_view" type="checkbox" <?php echo $checked; ?>>THE RICE VIEW
                        </label>
                        
                    &nbsp;&nbsp;&nbsp;
                        
                        <label>
                            <?php $checked = $results[0]['place_black_eye_coffee'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_black_eye_coffee" name="place_black_eye_coffee" type="checkbox" <?php echo $checked; ?>>BLACK EYE COFFEE
                        </label>
                        
                    <br><br> 
                                     
                        <label>
                            <?php $checked = $results[0]['place_chappel'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_chappel" name="place_chappel" type="checkbox" <?php echo $checked; ?>>CHAMBER
                        </label>
                        
                        &nbsp;&nbsp;&nbsp;
              
                        <label>
                            <?php $checked = $results[0]['place_garden_amphi_theater'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_garden_amphi_theater" name="place_garden_amphi_theater" type="checkbox" <?php echo $checked; ?>>GARDEN AMPHI THEATER
                        </label>
                        
                        &nbsp;&nbsp;&nbsp;                        
                        
                        <label>
                            <?php $checked = $results[0]['place_bale_agung'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_bale_agung" name="place_bale_agung" type="checkbox" <?php echo $checked; ?>>BALE AGUNG
                        </label>
                        
                        &nbsp;&nbsp;&nbsp;
              
                        <label>
                            <?php $checked = $results[0]['place_deck_view'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_deck_view" name="place_deck_view" type="checkbox" <?php echo $checked; ?>>DECK VIEW
                        </label>
                        
                        &nbsp;&nbsp;&nbsp;
                        
                        <label>
                            <?php $checked = $results[0]['place_rice_field_deck'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_rice_field_deck" name="place_rice_field_deck" type="checkbox" <?php echo $checked; ?>>RICE FIELD DECK
                        </label>
                        
                        &nbsp;&nbsp;&nbsp;
                        
                        <label>
                            <?php $checked = $results[0]['place_sanitasi'] == '1' ? "checked='checked'" : '' ?>
                            <input id="place_sanitasi" name="place_sanitasi" type="checkbox" <?php echo $checked; ?>>JASA SANITASI
                        </label>
                        
                    <br><br> 
                                     
                        
          
                </td>
        	</tr>
        </tabel>
        <br><br>

        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="border-table">
            <!--<tr bgcolor="#EEEEEE">
                <td colspan="2" align="center" width="50%" class="border-table"><strong>SET UP INSTRUCTION</strong></td>
                <td colspan="2" align="center" width="50%" class="border-table"><strong>SET UP INSTRUCTION</strong></td>
            </tr>
            <tr bgcolor="#EEEEEE">
                <td colspan="2" align="center" width="50%" class="border-table"><strong>FUNCTION TYPE</strong></td>
                <td colspan="2" align="center" width="50%" class="border-table"><strong>BANQUET</strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center" width="50%" valign="top" class="border-table">
                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_wedding_party'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_wedding_party" name="func_type_wedding_party" type="checkbox" <?php echo $checked; ?>>WEDDING PARTY
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_coffee_tea_break'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_coffee_tea_break" name="func_type_coffee_tea_break" type="checkbox" <?php echo $checked; ?>>COFFEE & TEA BREAK
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_cocktail_party'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_cocktail_party" name="func_type_cocktail_party" type="checkbox" <?php echo $checked; ?>>COCKTAIL PARTY
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_breakfast'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_breakfast" name="func_type_breakfast" type="checkbox" <?php echo $checked; ?>>BREAKFAST
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_birthday_party'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_birthday_party" name="func_type_birthday_party" type="checkbox" <?php echo $checked; ?>>BIRTHDAY PARTY
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_training'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_training" name="func_type_training" type="checkbox" <?php echo $checked; ?>>TRAINING
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_lunch'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_lunch" name="func_type_lunch" type="checkbox" <?php echo $checked; ?>>LUNCH
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_meeting'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_meeting" name="func_type_meeting" type="checkbox" <?php echo $checked; ?>>MEETING
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_dinner'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_dinner" name="func_type_dinner" type="checkbox" <?php echo $checked; ?>>DINNER
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['func_type_other'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="func_type_other" name="func_type_other" type="checkbox" <?php echo $checked; ?> >OTHER
                                    </label>
                                </div>
                                <span id="func_type_other_content" name="func_type_other_content" style="display:none;" >
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="2" align="center" width="50%" class="border-table">
                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="2">Please prepare meeting room set up as below details :</td>
                        </tr>
                        <tr bgcolor="#EEEEEE">
                            <td width="200" align="center">Description</td>
                            <td align="center"><span class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_est_terima" id="v_est_terima" maxlength="10"><?php echo date('d-m-Y'); ?></td>
                        </tr>
                        <tr>
                            <td>Venue</td>
                            <td><span id="banquet_venue" name="banquet_venue" ><?php echo $results[0]['banquet_venue'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Event</td>
                            <td><span id="banquet_event" name="banquet_event" ><?php echo $results[0]['banquet_event'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Table Set Up</td>
                            <td><span id="banquet_table_set_up" name="banquet_table_set_up" ><?php echo $results[0]['banquet_table_set_up'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Persons</td>
                            <td><span id="banquet_persons" name="banquet_persons" ><?php echo $results[0]['banquet_persons'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Registration</td>
                            <td><span id="banquet_registration" name="banquet_registration" ><?php echo $results[0]['banquet_registration'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Coffee Break I</td>
                            <td><span id="banquet_coffee_break_1" name="banquet_coffee_break_1" ><?php echo $results[0]['banquet_coffee_break_1'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Lunch</td>
                            <td><span id="banquet_lunch" name="banquet_lunch" ><?php echo $results[0]['banquet_lunch'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Coffee Break II</td>
                            <td><span id="banquet_coffee_break_2" name="banquet_coffee_break_2" ><?php echo $results[0]['banquet_coffee_break_2'] ?></span></td>
                        </tr>
                        <tr>
                            <td>Dinner</td>
                            <td><span id="banquet_dinner" name="banquet_dinner" ><?php echo $results[0]['banquet_dinner'] ?></span></td>
                        </tr>
                    </table>
                </td>
            </tr>-->
            <tr bgcolor="#EEEEEE">
                <td colspan="2" align="center" width="50%" class="border-table"><strong>FRONT OFFICE</strong></td>
                <td colspan="2" align="center" width="50%" class="border-table"><strong>F & B KITCHEN</strong></td>
            </tr>
            <tr>
                <td colspan="2" width="100%" height="70" style="vertical-align: top;" class="border-table">
                    <div class="well">
                        <?php echo $results[0]['desc_front_office'] ?>
                    </div>
                </td>
                <td colspan="2" width="100%" height="70" style="vertical-align: top;" class="border-table">
                    <div class="form-group">
                        <?php echo $results[0]['desc_f_b_kitchen'] ?>
                    </div>
                </td>
            </tr>

            <!--<tr bgcolor="#EEEEEE">
                <td colspan="2" align="center" width="50%" class="border-table"><strong>EQUIPMENT</strong></td>
                <td colspan="2" align="center" width="50%" class="border-table"><strong>ENGINEERING & EDP</strong></td>
            </tr>

            <tr>
                <td rowspan="3" colspan="2" align="center" width="50%" valign="top" class="border-table">
                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="table table-bordered responsive">
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <input id="equipment_flipchart" name="equipment_flipchart" type="checkbox" <?php echo $checked; ?>>FLIPCHART
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <input id="equipment_mineral_water" name="equipment_mineral_water" type="checkbox" <?php echo $checked; ?>>MINERAL WATER
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['equipment_lcd_projector'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="equipment_lcd_projector" name="equipment_lcd_projector" type="checkbox" <?php echo $checked; ?>>LCD PROJECTOR
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['equipment_notepad_pencil'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="equipment_notepad_pencil" name="equipment_notepad_pencil" type="checkbox" <?php echo $checked; ?>>NOTE PAD + PENCIL
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['equipment_mint'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="equipment_mint" name="equipment_mint" type="checkbox" <?php echo $checked; ?>>MINT
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['equipment_standard_set_up'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="equipment_standard_set_up" name="equipment_standard_set_up" type="checkbox" <?php echo $checked; ?>>STANDARD SET UP
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['equipment_screen'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="equipment_screen" name="equipment_screen" type="checkbox" <?php echo $checked; ?>>SCREEN
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <?php $checked = $results[0]['equipment_sound_system'] == '1' ? "checked='checked'" : '' ?>
                                        <input id="equipment_sound_system" name="equipment_sound_system" type="checkbox" <?php echo $checked; ?>>SOUND SYSTEM
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="2" align="center" width="50%" class="border-table">
                    <div class="form-group">
                        <?php echo $results[0]['desc_engineering_edp'] ?>
                    </div>
                </td>
            </tr>
            <tr bgcolor="#EEEEEE">
                <td colspan="2" align="center" width="50%" class="border-table"><strong>SPORT & RECREATION</strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center" width="50%" class="border-table">
                    <div class="form-group">
                        <?php echo $results[0]['desc_sport_recreation'] ?>
                    </div>
                </td>
            </tr>-->
            <tr bgcolor="#EEEEEE">
                <td colspan="2" align="center" width="50%" class="border-table"><strong>BLACK EYE COFFEE</strong></td>
                <td colspan="2" align="center" width="50%" class="border-table"><strong>OEMAH HERBORIST</strong></td>
            </tr>
            <tr>
                <td colspan="2" width="50%" height="70" style="vertical-align: top;" class="border-table">
                    <div class="form-group">
                        <?php echo $results[0]['desc_black_aye'] ?>
                    </div>
                </td>
                <td colspan="2" width="50%" height="70" style="vertical-align: top;"class="border-table">
                    <div class="form-group">
                        <?php echo $results[0]['desc_oemah_herborist'] ?>
                    </div>
                </td>
            </tr>
            <tr bgcolor="#EEEEEE">
		<td colspan="2" align="center" width="50%" class="border-table"><strong>ACCOUNTING</strong></td>
		<td colspan="2" align="center" width="50%" class="border-table"><strong>SECURITY / GA / CLEANING SERVICE</strong></td>
	</tr>
	<tr>
		<td colspan="2" width="50%" height="70" style="vertical-align: top;" class="border-table">
			<div class="form-group">
			<?php echo $results[0]['desc_accounting'] ?>
			</div>
		</td>
		<td colspan="2" width="50%" height="70" style="vertical-align: top;" class="border-table">
			<div class="form-group">
				<?php echo $results[0]['desc_security'] ?>
			</div>
		</td>
	</tr>
	<tr bgcolor="#EEEEEE">
		<td colspan="2" align="center" width="50%" class="border-table"><strong>ENGINERING</strong></td>
		<td colspan="2" align="center" width="50%" class="border-table"><strong>DESIGN GRAPHIS</strong></td>
	</tr>
	<tr>
		<td colspan="2" width="50%" height="70" style="vertical-align: top;" class="border-table">
			<div class="form-group">
				<?php echo $results[0]['desc_engineering_edp'] ?>
			</div>
		</td>
		<td colspan="2" width="50%" height="70" style="vertical-align: top;" class="border-table">
			<div class="form-group">
				<?php echo $results[0]['desc_graphis'] ?>
			</div>
		</td>
	</tr>
    <tr bgcolor="#EEEEEE">
        <td colspan="2" align="center" width="100%" class="border-table"><strong>JASA SANITASI</strong></td>
    </tr>
    <tr>
        <td colspan="2" width="100%" height="70" style="vertical-align: top;" class="border-table">
            <div class="form-group">
                <?php echo $results[0]['desc_jasa_sanitasi'] ?>
            </div>
        </td>
    </tr>
            <tr>
			<?php 
				if($v_level_user=="7"){ 
					$level	= "Sales Supervisor";
				}elseif($v_level_user=="6"){
					$level = "Marketing";
				}else{
					$level = "Administrator";
				}
			?>
                <td colspan="4" align="left" width="100%">
                    <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                        <tr>
                            <td colspan="4">Prepared by,</td>
                        </tr>
                        <tr>
                            <!--<td colspan="4"><img src="<?=base_url();?>public/images/ttd_<?=$v_add_userid;?>.png" width="100" alt="" /></td>-->
                            <td colspan="4"><img src="<?=base_url();?>public/images/ttd2.png" width="85" alt="" /></td>
                        </tr>
                        <tr>
                            <td colspan="4"><u><?php echo $adm_sales_nama;?></u></td>
            </tr>
            <tr>
                <td colspan="4"><?php echo $adm_sales_jabatan;?></td>
            </tr>
            <!--<tr>
                <td>CC : </td>
                <td colspan="3">
					<table width="100%" cellpadding="5" border="0">
						<?php 
						error_reporting(0);
						if(!empty($NoDok)){
							foreach($group_cc_value as $key){
								$data['list_gc'][$NoDok][$key['group_cc_id']]=$key['group_cc_id'];
							}
						}
						$no = 1;
						for($a=0;$a<count($group_cc);$a++){
						
						$gc_value = $data['list_gc'][$NoDok][$group_cc[$a]['group_cc_id']];
						
						$gc_checked = "";
						if($gc_value==$group_cc[$a]['group_cc_id']){
							$gc_checked = "checked";
						}
						if(($no % 5) == 0 ) echo "<tr>";
						?>
							<td>
								<div class="checkbox">
									<label>
										<input id="cc_food_beverage" name="group_cc[]" type="checkbox" value="<?=$group_cc[$a]['group_cc_id'];?>" <?php echo $gc_checked;?> ><?=$group_cc[$a]['group_cc_name'];?>
									</label>
								</div>
							</td>
						<?php
						if(($no % 5) == 4 ) echo "</tr>";   
						$no++;
						}
						?>
					</table>
				</td>
            </tr>-->
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" align="center" width="50%">
    </td>
    <td colspan="2" align="center" width="50%">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
            <?php
            $total = $results[0]['Total'];
            $dp = $results[0]['dp'];
            $sisa = $total - $dp;
            ?>
            <tr>
                <td>Total</td>
                <td>: Rp. </td>
                <td align="right">
                    <span id="Total" name="Total" value="" style="text-align: right;" ><?= $mylib->ubah_format($results[0]['Total']); ?></span>
                </td>
            </tr>
            <tr>
                <td>DP</td>
                <td>: Rp. </td>
                <td align="right">
                    <span id="dp" name="dp" value="" style="text-align: right;" ><?= $mylib->ubah_format($results[0]['dp']); ?></span>
                </td>
            </tr>
            <tr>
                <td>Sisa Pembayaran</td>
                <td>: Rp. </td>
                <td align="right">
                    <span id="sisa_pembayaran" name="sisa_pembayaran" value="" style="text-align: right;"><?= $mylib->ubah_format($sisa); ?> </span>
                </td>
            </tr>
            <tr>
                <td valign="top">Remarks Payment</td>
                <td valign="top">:</td>
                <td>
                    <div class="form-group">
                        <?php echo $results[0]['Remarks_Payment'] ?>
                    </div>
                </td>
            </tr>

        </table>
    </td>
</tr>
</table>

</body>
</html>