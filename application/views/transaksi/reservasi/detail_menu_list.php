<?php
$session_name = $this->session->userdata('username');
$UserLevel = $this->session->userdata('userlevel');
?>

<table class="table table-bordered responsive">
  <thead>
    <tr>
      <th width="100"><center>PCode</center></th>
      <th width="200"><center>Nama Menu</center></th>
      <th width="100"><center>Qty</center></th>
      <th width="100"><center>Diskon (%)</center></th>
      <th width="100"><center>Harga</center></th>
      <th width="100"><center>Total</center></th>
      <?php if($session_name=="ni0504" OR $session_name=="tonny1205" OR $session_name=="mechael0101" OR $session_name=="julianti"){?>
        <th width="100"><center>Action</center></th>
      <?php } ?>
    </tr>
  </thead>
  <?php
  $gtotal = 0;
  foreach($detailMenu as $rows){
    //$total = $rows['Qty']*$rows['Harga'];
    //$gtotal = $gtotal + $total;
    /**
    * perhitungan ini sudah di masterbarang nya
    $harga_sudah_pajak_5 = $rows['Harga'] + ($rows['Harga']*(5/100));
    $harga_sudah_pajak_10 = $harga_sudah_pajak_5 + ($harga_sudah_pajak_5*(10/100));
    $harga_fix = $harga_sudah_pajak_10;
    */
    $harga_fix = $rows['Harga'];

    if($rows['Diskon']==0){
      $total = $rows['Qty']*$harga_fix;
    }else{
      $total = ($rows['Qty']*$harga_fix) - (($rows['Qty']*$harga_fix)*$rows['Diskon']/100);
    }

    $gtotal = $gtotal + $total;

    ?>
    <tr>
      <td><?=$rows['PCode'];?></td>
      <td align="center"><?=$rows['NamaBarang'];?></td>
      <td align="center"><?=number_format($rows['Qty']);?></td>
      <td align="center"><?=number_format($rows['Diskon']);?></td>
      <td align="right"><?=number_format($harga_fix);?></td>
      <td align="right"><?=number_format($total);?></td>
      <?php if($session_name=="ni0504" OR $session_name=="tonny1205" OR $session_name=="mechael0101" OR $session_name=="julianti"){?>
        <td align="center">
          <!--<a href="<?php echo base_url(); ?>index.php/pop/pop_up_detail_menu/hapus_trans_detail/<?php echo $rows["sid"]; ?>/<?php echo $nodokumen;?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title=""><i class="entypo-trash"></i></a>-->
          <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" onclick="hapus_trans_detail('<?php echo $rows['NoDokumen']; ?>','<?=$rows['PCode'];?>','<?=$total;?>', '<?php echo base_url(); ?>');" >
            <i class="entypo-trash"></i>
          </button>
        </td>
      <?php } ?>
    </tr>
  <?php } ?>
  <tr>
    <td colspan="4">Grand Total</td>
    <td align="right"><?=number_format($gtotal);?></td>
    <td></td>
  </tr>
  <input type='hidden' name="gtotal" id="gtotal" value="<?=$gtotal;?>">
</table>
