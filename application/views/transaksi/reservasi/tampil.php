<?php
$this->load->view('header');
$session_name = $this->session->userdata('username');
$UserLevel = $this->session->userdata('userlevel');
$mylib = new globallib();
?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/font-awesome/css/font-awesome.min.css">
<script src="<?php echo base_url(); ?>public/js/shortcuts_v1.js" type="text/javascript"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/grup_harga.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/transaksi_sales.js"></script>
<script src="<?= base_url(); ?>public/js/incrementing.js"></script>
<script src="<?= base_url(); ?>public/js/reservasi.js"></script>

<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
<script src="<?= base_url();?>public/js/js.js"></script>

<style type='text/css'>
.devy {
  margin: 0 auto;
  width:10px;
  border:solid 2px red;
  border-radius: 5px;
  color: blue;
  font: bold 15px/40px Verdana,serif;
  text-shadow: 1px 2px 1px red;
  text-align:center;
  transition: all 0.2s;
  -moz-transition: all 0.2s;
  -webkit-transition: all 0.2s;
  -ms-transition: all 0.2s;
  -o-transition: all 0.2s}

  .devy:hover {
    /*nilai perubahan*/
    background: red;
    color: #fff;
    text-shadow: 1px 2px 2px #111;
    transform:rotate(20deg);
    -webkit-transform:rotate(20deg);
    -moz-transform:rotate(20deg);
    box-shadow: 2px 4px 3px 2px #222}
  </style>

  <script>
  function noseri(){

    var tanggal = $('#v_date_reservasi').val();
    var url =$('#base_url').val();
    $.ajax({
      url: url+"index.php/transaksi/reservasi/noseri/",
      data: {tgl:tanggal},
      type: "POST",
      dataType: "json",
      success: function(data)
      {
        var nomor = $('#NoDokumen').val(data.NoDokumen);

      }
    });

  }

  function noseri_booking(){
    var tanggal = $('#v_date_reservasi').val();
    var url =$('#base_url').val();
    $.ajax({
      url: url+"index.php/transaksi/reservasi/noseri_booking/",
      data: {tgl:tanggal},
      type: "POST",
      dataType: "json",
      success: function(data)
      {
        var nomor = $('#NoDokumen').val(data.NoDokumen);

      }
    });

  }

  function cekTheformReserv()
  {
	  var datenya = $('#v_date_reservasi').val();
    var newdate = datenya.split("-").reverse().join("-");
    var mydate = new Date(newdate);
    var getmon = mydate.getMonth();
    var getyer = mydate.getFullYear();
    var gettanggal =  mydate.getDate();

    // var tanggalblok = [13,14,16,17,19];
    //var tanggalblok = [14,16,17];
    var tanggalblok = [32];

    if(getmon == 3 && getyer == 2019){

       for (var i = 0; i < tanggalblok.length; i++) {
         if(gettanggal == tanggalblok[i]){
          alert("BOOKINGAN PENUH");
          return false;

          }
       }     
     }

     // cek venue
     var place=0;

    if($("#place_herborist").is(':checked')){
        place++;
    }else if($("#place_the_luwus").is(':checked')){
        place++;
    }else if($("#place_the_luwus_bawah").is(':checked')){
        place++;
    }else if($("#place_the_rice_view").is(':checked')){
        place++;
    }else if($("#place_black_eye_coffee").is(':checked')){
        place++;
    }else if($("#place_chappel").is(':checked')){
        place++;
    }else if($("#place_garden_amphi_theater").is(':checked')){
        place++;
    }else if($("#place_black_eye_sunset_road").is(':checked')){
        place++;
    }else if($("#place_bale_agung").is(':checked')){
        place++;
    }else if($("#place_deck_view").is(':checked')){
        place++;
    }else if($("#place_rice_field_deck").is(':checked')){
        place++;
    }else if($("#place_bts").is(':checked')){
        place++;
    }else if($("#place_sanitasi").is(':checked')){
        place++;
    }

    if(place==0){
        alert("Venue Harus Diisi!")

        return false;
    }
     // cek venue
	
    if(document.getElementById("company").value=="")
    {
      alert("Company masih kosong");
      return false;
    }
    else if(document.getElementById("Total").value=="")
    {
      alert("Total transaksi masih kosong");
      return false;
    }else{
      document.getElementById("theform").submit();
      return true;
    }
  }

</script>

<style>
.border-form-bold{
  border: 1px solid #9B9E9A;
}
</style>
<body class="page-body">
  <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
  <div class="table-responsive">
    <?php
    if($type=="add"){
      $action="save_data";
      $NoDok	= $NoDokumen;
      $KdTravel	= "";
      $nama_travel	= "";
      $group_code	="";
      $company	= "";
      $Telepon	= "";
      $alamat	= "";
      $nationality	= "";
      $adultParticipants	= "";
      $childParticipants	= "";
      $kidsParticipants	= "";
      $Jam_Kedatangan	= date("h:i:s a");
      $Event1	= "";
      $Event2	= "";
      $Event3	= "";
      $Event4	= "";
      $Event5	= "";
      $Contact	= "";
      $Phone	= "";
      $Sales_In_Charge	= "";
      $place_herborist	= "";
      $place_the_luwus	= "";
      $place_the_luwus_bawah	= "";
      $place_black_eye_coffee	= "";
      $place_the_rice_view	= "";
      $place_chappel	= "";
	
      $place_bts	= "";
      $place_sanitasi	= "";
      $place_rice_field_deck	= "";
      $place_garden_amphi_theater	= "";
      $place_black_eye_sunset_road="";
      $place_bale_agung	= "";
      $place_deck_view	= "";

      $func_type_wedding_party	= "";
      $func_type_coffee_tea_break	= "";
      $func_type_cocktail_party	= "";
      $func_type_breakfast	= "";
      $func_type_birthday_party	= "";
      $func_type_training	= "";
      $func_type_lunch	= "";
      $func_type_meeting	= "";
      $func_type_dinner	= "";
      $func_type_other	= "";
      $func_type_other_content	= "";
      $func_css_other_content	= "style='display:none'";
      $banquet_venue	= "";
      $banquet_event	= "";
      $banquet_table_set_up	= "";
      $banquet_persons	= "";
      $banquet_registration	= "";
      $banquet_coffee_break_1	= "";
      $banquet_lunch	= "";
      $banquet_coffee_break_2	= "";
      $banquet_dinner	= "";
      $equipment_flipchart	= "";
      $equipment_mineral_water	= "";
      $equipment_lcd_projector	= "";
      $equipment_notepad_pencil	= "";
      $equipment_mint	= "";
      $equipment_standard_set_up	= "";
      $equipment_screen	= "";
      $equipment_sound_system	= "";
      $billing_instruction	= "";
      $desc_front_office	= "";
      $desc_f_b_kitchen	= "";
      $desc_black_aye	= "";
      $desc_oemah_herborist	= "";
      $desc_engineering_edp	= "";
      $desc_sport_recreation	= "";
      $desc_rates	= "";
      $desc_housekeeping	= "";
      $desc_accounting	= "";
      $desc_security	= "";
      $desc_graphis	= "";
      $desc_jasa_sanitasi	= "";
      $desc_biaya_tambahan = "";
      $reservasi_date = date('d-m-Y');
      $dp_date = "0000-00-00";
      $biaya_tambahan = "";
      $voucher = "";
      $dp	= 0;
      $Total	= 0;
      $Remarks_Payment	= "";
      $cc_cro = "";
      $cc_black_eye = "";
      $cc_senior_om = "";
      $cc_s_m = "";
      $cc_herborist = "";
      $cc_operasional_manager = "";
      $cc_finance = "";
      $cc_food_beverage = "";

      $sisa_pembayaran = 0;
      $setTotal = 0;
      //$display = "readonly";
      $display = "";
      $status ="0";
      $edit_request="0";
    }
    else{
      $action=="save_data_edit";
      $id = $id;
      $NoDok = $NoDokumen;
      $KdTravel = $v_KdTravel;
      $nama_travel	= $v_nama_travel;
      $group_code	= $v_group_code;
      $company	= $v_Company;
      $Telepon	= $v_Telepon;
      $alamat	= $v_Alamat;
      $nationality	= $v_Nationality;
      $adultParticipants = $v_adultParticipants;
      $childParticipants = $v_childParticipants;
      $kidsParticipants = $v_kidsParticipants;
      $Jam_Kedatangan = $v_Jam_Kedatangan;
      $Event1 = $v_Event1;
      $Event2 = $v_Event2;
      $Event3 = $v_Event3;
      $Event4 = $v_Event4;
      $Event5 = $v_Event5;
      $Contact = $v_Contact;
      $Phone = $v_Phone;
      $status =$v_status;
      $Sales_In_Charge = $v_Sales_In_Charge;

      if($v_place_herborist=="1"){$place_herborist =  "checked";}else{$place_herborist =  "";}
      if($v_place_the_luwus=="1"){$place_the_luwus =  "checked";}else{$place_the_luwus =  "";}
      if($v_place_the_luwus_bawah=="1"){$place_the_luwus_bawah =  "checked";}else{$place_the_luwus_bawah =  "";}
      if($v_place_black_eye_coffee=="1"){$place_black_eye_coffee =  "checked";}else{$place_black_eye_coffee =  "";}
      if($v_place_the_rice_view=="1"){$place_the_rice_view =  "checked";}else{$place_the_rice_view =  "";}
      if($v_place_chappel=="1"){$place_chappel =  "checked";}else{$place_chappel =  "";}

      if($v_place_bts=="1"){$place_bts =  "checked";}else{$place_bts =  "";}
      if($v_place_sanitasi=="1"){$place_sanitasi =  "checked";}else{$place_sanitasi =  "";}

      if($v_place_rice_field_deck=="1"){$place_rice_field_deck =  "checked";}else{$place_rice_field_deck =  "";}
      if($v_place_garden_amphi_theater=="1"){$place_garden_amphi_theater =  "checked";}else{$place_garden_amphi_theater =  "";}
      if($v_place_black_eye_sunset_road=="1"){$place_black_eye_sunset_road =  "checked";}else{$place_black_eye_sunset_road =  "";}
      if($v_place_bale_agung=="1"){$place_bale_agung =  "checked";}else{$place_bale_agung =  "";}
      if($v_place_deck_view=="1"){$place_deck_view =  "checked";}else{$place_deck_view =  "";}

      if($v_func_type_wedding_party=="1"){$func_type_wedding_party =  "checked";}else{$func_type_wedding_party =  "";}
      if($v_func_type_coffee_tea_break=="1"){$func_type_coffee_tea_break =  "checked";}else{$func_type_coffee_tea_break =  "";}
      if($v_func_type_cocktail_party=="1"){$func_type_cocktail_party =  "checked";}else{$func_type_cocktail_party =  "";}
      if($v_func_type_breakfast=="1"){$func_type_breakfast =  "checked";}else{$func_type_breakfast =  "";}
      if($v_func_type_birthday_party=="1"){$func_type_birthday_party =  "checked";}else{$func_type_birthday_party =  "";}
      if($v_func_type_training=="1"){$func_type_training =  "checked";}else{$func_type_training =  "";}
      if($v_func_type_lunch=="1"){$func_type_lunch =  "checked";}else{$func_type_lunch =  "";};
      if($v_func_type_meeting=="1"){$func_type_meeting =  "checked";}else{$func_type_meeting =  "";}
      if($v_func_type_dinner=="1"){$func_type_dinner =  "checked";}else{$func_type_dinner =  "";}
      if($v_func_type_other=="1"){
        $func_type_other =  "checked";
        $func_css_other_content	= "style='display:block'";
      }else{
        $func_type_other =  "";
        $func_css_other_content	= "style='display:none'";
      }

      $func_type_other_content = $v_func_type_other_content;
      $banquet_venue = $v_banquet_venue;
      $banquet_event = $v_banquet_event;
      $banquet_table_set_up = $v_banquet_table_set_up;
      $banquet_persons = $v_banquet_persons;
      $banquet_registration = $v_banquet_registration;
      $banquet_coffee_break_1 = $v_banquet_coffee_break_1;
      $banquet_lunch = $v_banquet_lunch;
      $banquet_coffee_break_2 = $v_banquet_coffee_break_2;
      $banquet_dinner = $v_banquet_dinner;

      if($v_equipment_flipchart=="1"){$equipment_flipchart =  "checked";}else{$equipment_flipchart =  "";}
      if($v_equipment_mineral_water=="1"){$equipment_mineral_water =  "checked";}else{$equipment_mineral_water =  "";}
      if($v_equipment_lcd_projector=="1"){$equipment_lcd_projector =  "checked";}else{$equipment_lcd_projector =  "";}
      if($v_equipment_notepad_pencil=="1"){$equipment_notepad_pencil =  "checked";}else{$equipment_notepad_pencil =  "";}
      if($v_equipment_mint=="1"){$equipment_mint =  "checked";}else{$equipment_mint =  "";}
      if($v_equipment_standard_set_up=="1"){$equipment_standard_set_up =  "checked";}else{$equipment_standard_set_up =  "";}
      if($v_equipment_screen=="1"){$equipment_screen =  "checked";}else{$equipment_screen =  "";}
      if($v_equipment_sound_system=="1"){$equipment_sound_system =  "checked";}else{$equipment_sound_system =  "";}

      $billing_instruction = $v_billing_instruction;
      $desc_front_office = $v_desc_front_office;
      $desc_f_b_kitchen = $v_desc_f_b_kitchen;
      $desc_black_aye = $v_desc_black_aye;
      $desc_oemah_herborist = $v_desc_oemah_herborist;
      $desc_engineering_edp = $v_desc_engineering_edp;
      $desc_sport_recreation = $v_desc_sport_recreation;
      $desc_rates = $v_desc_rates;
      $desc_housekeeping = $v_desc_housekeeping;
      $desc_security = $v_desc_security;
      $desc_accounting = $v_desc_accounting;
      $desc_graphis = $v_desc_graphis;
      $desc_jasa_sanitasi = $v_desc_jasa_sanitasi;
      $desc_biaya_tambahan = $v_desc_biaya_tambahan;
      $reservasi_date = date('d-m-Y',strtotime($v_reservasi_date));
      if($v_dp_date==""){
        $v_dp_date = date('d-m-Y');
      }else{
        $v_dp_date = $v_dp_date;
      }
      $dp_date = date('d-m-Y',strtotime($v_dp_date));
      $biaya_tambahan = $v_biaya_tambahan;
      $voucher = $v_VoucherCode;
      $dp = $v_dp;
      $Total = $v_Total;
      $Remarks_Payment = $v_Remarks_Payment;

      $cc_cro = "";
      $cc_black_eye = "";
      $cc_senior_om = "";
      $cc_s_m = "";
      $cc_herborist = "";
      $cc_operasional_manager = "";
      $cc_finance = "";
      $cc_food_beverage = "";

      $sisa_pembayaran = ($Total) -$dp;
      $setTotal = ($Total);
      $display = "";
      $edit_request= $v_edit_request;
    }
    //echo $edit_request;
    if($booking=="Y"){
      $judul = "BOOKING BANQUET EVENT ORDER";
    }else{
      $judul = "BANQUET EVENT ORDER";
    }
    ?>
    <form method='post' name="theform" id="theform" enctype="multipart/form-data"  action='<?=base_url();?>index.php/transaksi/reservasi/<?=$action;?>'>
      <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
      <table width="85%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
        <tr>
          <td colspan="5" align="center"><img src="<?=base_url();?>public/images/LogoBaliTimbungan.png" width="100" alt="Secret Garden Village"/></td>
        </tr>
        <tr>
          <td colspan="5" align="center"><h4><b><?=$judul;?></b></h4>
          </tr>
          <tr>
            <td width="300" align="center" colspan="5">
              <?php if($booking=="Y"){
                $class_btns = "font-size:18px; text-align:center;height:30px;width:290px;background:black;color:white";
              }else{
                $class_btns = "font-size:18px; text-align:center;height:30px;width:290px;background:#5d98f7;color:white";
              }

              ?>
              <?php if($type=="add"){ ?>
                <input type="text" id="NoDokumen" name="NoDokumen" class="form-control" value="<?=$NoDok;?>" style="<?=$class_btns;?>" readonly/>
              <?php }else{ ?>
                <?php if($tampil_no_beo=="Y"){?>
                  <input type="text" id="NoDokumen" name="NoDokumen" class="form-control" value="<?=$NoDok;?>" style="font-size:18px; text-align:center;height:30px;width:290px;background:black;color:white" readonly/>
                  <center><font color="red" size="1.9"><b>Booking Changed to this BEO number if click Save & send</b></font></center>
                  <input type="text" id="NoDokumen_beo" name="NoDokumen_beo" class="form-control" value="<?=$NoDokumen_beo;?>" style="font-size:18px; text-align:center;height:30px;width:290px;background:green;color:white" readonly/>
                  <?}else{?>
                    <input type="text" id="NoDokumen" name="NoDokumen" class="form-control" value="<?=$NoDok;?>" style="<?=$class_btns;?>" readonly/>
                    <?}?>
                  <?php }?>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <input type="hidden" id="KdTravel" name="KdTravel" class="form-control" value="<?=$KdTravel;?>"/>
                  <input type="hidden" id="id" name="id" class="form-control" value="<?=$id;?>"/>
                  <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                    <tr>
                      <td>RESERVASI DATE<td>
                        <td>:<td>

                          <?php if($booking=="Y"){?>
                            <td width="200"><input onchange=noseri_booking(); type="text" class="form-control datepicker" value="<?php echo $reservasi_date; ?>" name="v_date_reservasi" id="v_date_reservasi"  maxlength="10" ><?php //echo date('d-m-Y'); ?></td>
                          <?php }else{ ?>
                            <td width="200"><input onchange=noseri(); type="text" class="form-control datepicker" value="<?php echo $reservasi_date; ?>" name="v_date_reservasi" id="v_date_reservasi"  maxlength="10"><?php //echo date('d-m-Y'); ?></td>
                          <?php } ?>

                        </tr>
                        <tr>
                          <td>COMPANY<td>
                            <td>:<td>
                              <td width="200"><input type="text" id="company" name="company" class="form-control border-form-bold" value="<?=$company;?>" readonly /><td>
                                <?php if($booking=="Y"){
                                  $class_btn = "btn btn-primary btn-sm sm-new tooltip-primary";
                                }else{
                                  $class_btn = "btn btn-info btn-sm sm-new tooltip-primary";
                                } ?>
                                <td><a href="javascript:void(0)" id="get_group_name1" onclick="getGroupTravel(this)" class="<?=$class_btn;?>" data-toggle="tooltip" data-placement="top" data-original-title="Search" title=""><i class="entypo-search"></i></a><td>
                                </tr>
                                <tr>
                                  <td>GROUP CODE<td>
                                    <td>:<td>
                                      <td width="200"><input type="text" id="group_code" name="group_code" class="form-control border-form-bold" value="<?=$group_code;?>"/><td>
                                        <td><td>
                                        </tr>
                                        <tr>
                                          <td>GROUP NAME<td>
                                            <td>:<td>
                                              <td width="200"><input type="text" id="nama_travel" name="nama_travel" class="form-control border-form-bold" value="<?=$nama_travel;?>"/><td>
                                                <td><td>
                                                </tr>
                                                <tr>
                                                  <td>TELEPHONE<td>
                                                    <td>:<td>
                                                      <td width="200"><input type="text" id="Telepon" name="Telepon" class="form-control border-form-bold" value="<?=$Telepon;?>"/><td>
                                                        <td><td>
                                                        </tr>
                                                        <tr>
                                                          <td>ADDRESS<td>
                                                            <td>:<td>
                                                              <td width="200"><input type="text" id="alamat" name="alamat" class="form-control border-form-bold" value="<?=$alamat;?>"/><td>
                                                                <td><td>
                                                                </tr>
                                                                <tr>
                                                                  <td>NATIONALITY<td>
                                                                    <td>:<td>
                                                                      <td width="200"><input type="text" id="nationality" name="nationality" class="form-control border-form-bold" value="<?=$nationality;?>"/><td>
                                                                        <td><td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td>ADULT PARTICIPANTS<td>
                                                                            <td>:<td>
                                                                              <td width="200"><input type="text" id="adultParticipants" name="adultParticipants" class="form-control border-form-bold" value="<?=$adultParticipants;?>" style="width: 100px;float:left;"/><div style="float:left;margin-left:10px;">PERSON</div><td>
                                                                                <td><td>
                                                                                </tr>
                                                                                <tr>
                                                                                  <td>CHILD PARTICIPANTS<td>
                                                                                    <td>:<td>
                                                                                      <td width="200"><input type="text" id="adultParticipants" name="childParticipants" class="form-control border-form-bold" value="<?=$childParticipants;?>" style="width: 100px;float:left;"/><div style="float:left;margin-left:10px;">PERSON</div><td>
                                                                                        <td><td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <td>INFANT PARTICIPANTS<td>
                                                                                            <td>:<td>
                                                                                              <td width="200"><input type="text" id="kidsParticipants" name="kidsParticipants" class="form-control border-form-bold" value="<?=$kidsParticipants;?>" style="width: 100px;float:left;"/><div style="float:left;margin-left:10px;">PERSON</div><td>
                                                                                                <td><td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                  <td>Jam Kedatangan<td>
                                                                                                    <td>:<td>
                                                                                                      <td width="200"><input type="time" id="jam_kedatangan" name="jam_kedatangan" class="form-control border-form-bold" value="<?=$Jam_Kedatangan;?>" style="width: 100px;float:left;"/><div style="float:left;margin-left:10px;"></div><td>
                                                                                                        <td><td>
                                                                                                        </tr>
                                                                                                      </table>
                                                                                                    </td>
                                                                                                    <td></td>
                                                                                                    <td colspan="2">
                                                                                                      <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                                                                                                        <tr>
                                                                                                          <td>ACTIVITIES 1<td>
                                                                                                            <td>:<td>
                                                                                                              <td width="200"><!--<input type="text" id="Event" name="Event" class="form-control border-form-bold" value="<?=$Event;?>"/>-->
                                                                                                                <select class="form-control-new" id="Event1" name="Event1" style="width: 200px;">
                                                                                                                  <option value="">-- Choose Activities --</option>
                                                                                                                  <?php

                                                                                                                  foreach($aktivitas as $val)
                                                                                                                  {
                                                                                                                    $selected="";
                                                                                                                    if($Event1==$val["id_aktivitas_beo"])
                                                                                                                    {
                                                                                                                      $selected='selected="selected"';
                                                                                                                    }
                                                                                                                    ?><option <?php echo $selected; ?> value="<?php echo $val["id_aktivitas_beo"]; ?>"><?php echo $val["nama_aktivitas_beo"]; ?></option><?php
                                                                                                                  }
                                                                                                                  ?>
                                                                                                                </select>
                                                                                                                <td>
                                                                                                                </tr>

                                                                                                                <tr>
                                                                                                                  <td>ACTIVITIES 2<td>
                                                                                                                    <td>:<td>
                                                                                                                      <td width="200"><!--<input type="text" id="Event" name="Event" class="form-control border-form-bold" value="<?=$Event;?>"/>-->
                                                                                                                        <select class="form-control-new" id="Event2" name="Event2" style="width: 200px;">
                                                                                                                          <option value="">-- Choose Activities --</option>
                                                                                                                          <?php

                                                                                                                          foreach($aktivitas as $val)
                                                                                                                          {
                                                                                                                            $selected="";
                                                                                                                            if($Event2==$val["id_aktivitas_beo"])
                                                                                                                            {
                                                                                                                              $selected='selected="selected"';
                                                                                                                            }
                                                                                                                            ?><option <?php echo $selected; ?> value="<?php echo $val["id_aktivitas_beo"]; ?>"><?php echo $val["nama_aktivitas_beo"]; ?></option><?php
                                                                                                                          }
                                                                                                                          ?>
                                                                                                                        </select>
                                                                                                                        <td>
                                                                                                                        </tr>

                                                                                                                        <tr>
                                                                                                                          <td>ACTIVITIES 3<td>
                                                                                                                            <td>:<td>
                                                                                                                              <td width="200"><!--<input type="text" id="Event" name="Event" class="form-control border-form-bold" value="<?=$Event;?>"/>-->
                                                                                                                                <select class="form-control-new" id="Event3" name="Event3" style="width: 200px;">
                                                                                                                                  <option value="">-- Choose Activities --</option>
                                                                                                                                  <?php

                                                                                                                                  foreach($aktivitas as $val)
                                                                                                                                  {
                                                                                                                                    $selected="";
                                                                                                                                    if($Event3==$val["id_aktivitas_beo"])
                                                                                                                                    {
                                                                                                                                      $selected='selected="selected"';
                                                                                                                                    }
                                                                                                                                    ?><option <?php echo $selected; ?> value="<?php echo $val["id_aktivitas_beo"]; ?>"><?php echo $val["nama_aktivitas_beo"]; ?></option><?php
                                                                                                                                  }
                                                                                                                                  ?>
                                                                                                                                </select>
                                                                                                                                <td>
                                                                                                                                </tr>

                                                                                                                                <tr>
                                                                                                                                  <td>ACTIVITIES 4<td>
                                                                                                                                    <td>:<td>
                                                                                                                                      <td width="200"><!--<input type="text" id="Event" name="Event" class="form-control border-form-bold" value="<?=$Event;?>"/>-->
                                                                                                                                        <select class="form-control-new" id="Event4" name="Event4" style="width: 200px;">
                                                                                                                                          <option value="">-- Choose Activities --</option>
                                                                                                                                          <?php

                                                                                                                                          foreach($aktivitas as $val)
                                                                                                                                          {
                                                                                                                                            $selected="";
                                                                                                                                            if($Event4==$val["id_aktivitas_beo"])
                                                                                                                                            {
                                                                                                                                              $selected='selected="selected"';
                                                                                                                                            }
                                                                                                                                            ?><option <?php echo $selected; ?> value="<?php echo $val["id_aktivitas_beo"]; ?>"><?php echo $val["nama_aktivitas_beo"]; ?></option><?php
                                                                                                                                          }
                                                                                                                                          ?>
                                                                                                                                        </select>
                                                                                                                                        <td>
                                                                                                                                        </tr>

                                                                                                                                        <tr>
                                                                                                                                          <td>ACTIVITIES 5<td>
                                                                                                                                            <td>:<td>
                                                                                                                                              <td width="200"><!--<input type="text" id="Event" name="Event" class="form-control border-form-bold" value="<?=$Event;?>"/>-->
                                                                                                                                                <select class="form-control-new" id="Event5" name="Event5" style="width: 200px;">
                                                                                                                                                  <option value="">-- Choose Activities --</option>
                                                                                                                                                  <?php

                                                                                                                                                  foreach($aktivitas as $val)
                                                                                                                                                  {
                                                                                                                                                    $selected="";
                                                                                                                                                    if($Event5==$val["id_aktivitas_beo"])
                                                                                                                                                    {
                                                                                                                                                      $selected='selected="selected"';
                                                                                                                                                    }
                                                                                                                                                    ?><option <?php echo $selected; ?> value="<?php echo $val["id_aktivitas_beo"]; ?>"><?php echo $val["nama_aktivitas_beo"]; ?></option><?php
                                                                                                                                                  }
                                                                                                                                                  ?>
                                                                                                                                                </select>
                                                                                                                                                <td>
                                                                                                                                                </tr>

                                                                                                                                                <tr>
                                                                                                                                                  <td>CONTACT<td>
                                                                                                                                                    <td>:<td>
                                                                                                                                                      <td width="200"><input type="text" id="Contact" name="Contact" class="form-control border-form-bold" value="<?=$Contact;?>"/><td>
                                                                                                                                                      </tr>
                                                                                                                                                      <tr>
                                                                                                                                                        <td>PHONE<td>
                                                                                                                                                          <td>:<td>
                                                                                                                                                            <td width="200"><input type="text" id="Phone" name="Phone" class="form-control border-form-bold" value="<?=$Phone;?>"/><td>
                                                                                                                                                            </tr>
                                                                                                                                                            <tr>
                                                                                                                                                              <td>SALES IN CHARGE <td>
                                                                                                                                                                <td>:<td>
                                                                                                                                                                  <!--<td width="200"><input type="text" id="Sales_In_Charge" name="Sales_In_Charge" class="form-control border-form-bold" value="<?=$Sales_In_Charge;?>"/><td>-->
                                                                                                                                                                  <td>
                                                                                                                                                                    <select class="form-control-new" name="Sales_In_Charge" id="Sales_In_Charge" style="width: 200px;">
                                                                                                                                                                      <option value="">-- Choose Sales --</option>
                                                                                                                                                                      <?php

                                                                                                                                                                      foreach($sales as $val)
                                                                                                                                                                      {
                                                                                                                                                                        $selected="";
                                                                                                                                                                        if($Sales_In_Charge==$val["KdSalesman"])
                                                                                                                                                                        {
                                                                                                                                                                          $selected='selected="selected"';
                                                                                                                                                                        }
                                                                                                                                                                        ?><option <?php echo $selected; ?> value="<?php echo $val["KdSalesman"]; ?>"><?php echo $val["KdSalesman"]." - ".$val["NamaSalesman"]; ?></option><?php
                                                                                                                                                                      }
                                                                                                                                                                      ?>
                                                                                                                                                                    </select>
                                                                                                                                                                  </td>
                                                                                                                                                                </tr>
                                                                                                                                                              </table>
                                                                                                                                                            </td>
                                                                                                                                                          </tr>
                                                                                                                                                          <tr bgcolor="#EEEEEE">
                                                                                                                                                            <td colspan="100%"><b>VENUE</b></td>
                                                                                                                                                          </tr>
                                                                                                                                                          <tr>
                                                                                                                                                            <td>
                                                                                                                                                              <div class="checkbox">
                                                                                                                                                                <label>
                                                                                                                                                                  <input class="devy" id="place_herborist" name="place_herborist" type="checkbox" value="1" <?=$place_herborist;?>>Resto Sarinah
                                                                                                                                                                </label>
                                                                                                                                                              </div>
                                                                                                                                                            </td>
                                                                                                                                                          </tr>>
                                                                                                                                                          </tr>

                                                                                                                                                          <tr bgcolor="#EEEEEE">
                                                                                                                                                            <td colspan="100%" align="left"><b>Menu Detail</b>
                                                                                                                                                              <?php if($UserLevel=="6" || $UserLevel=="7" || $UserLevel=="-1" || $UserLevel=="23" || $UserLevel=="29" || $UserLevel=="30"){?>
                                                                                                                                                                <?php if(($type=="add" OR $type=="edit") AND ($session_name=="mechael0101" OR $session_name=="tonny1205" OR $session_name=="julianti" OR $session_name=="yuri0717" OR $session_name=="vicko0604")){?>
                                                                                                                                                                  <a href="javascript:void(0)" id="add_detail_pcode" onclick="buka_detail_beo()" class="<?=$class_btn;?>" data-toggle="tooltip" data-placement="top" data-original-title="Add Menu" title="" style="float:right;"><i class="entypo-plus"></i></a>
                                                                                                                                                                <?php }else{ ?>
                                                                                                                                                                  <a href="javascript:void(0)" id="add_detail_pcode" onclick="buka_detail_beo()" class="<?=$class_btn;?>" data-toggle="tooltip" data-placement="top" data-original-title="View Menu" title="" style="float:right;"><i class="entypo-eye"></i></a>
                                                                                                                                                                <?php }
                                                                                                                                                              }else{ ?>
                                                                                                                                                                <?php if($type=="edit"){?>
                                                                                                                                                                  <a href="javascript:void(0)" id="add_detail_pcode" onclick="buka_detail_beo()" class="<?=$class_btn;?>" data-toggle="tooltip" data-placement="top" data-original-title="View Menu" title="" style="float:right;"><i class="entypo-eye"></i></a>
                                                                                                                                                                <?php } ?>
                                                                                                                                                              <?php } ?>
                                                                                                                                                            </td>
                                                                                                                                                          </tr>
                                                                                                                                                        </table>











                                                                                                                                                        <div id="getDetailMenu" style="display:none;">
                                                                                                                                                          <div class="row">
                                                                                                                                                            <?if($booking=="Y"){
                                                                                                                                                              $warna="primary";
                                                                                                                                                              $bacaan = "Simpan Menu Booking";
                                                                                                                                                            }else{
                                                                                                                                                              $warna="info";
                                                                                                                                                              $bacaan = "Simpan Menu BEO";
                                                                                                                                                            }?>
                                                                                                                                                            <?php if( $session_name=="julianti" || $session_name=="mechael0101" || $session_name=="tonny1205" || $session_name=="dicky0707" || $session_name=="sri2018" || $session_name=="vicko0604" || $session_name=="mechael0101"){?>
                                                                                                                                                              <div class="col-md-12" align="left">

                                                                                                                                                                <!--<ol class="breadcrumb">
                                                                                                                                                                <li><strong><h3>
                                                                                                                                                                Detail <?php echo " Menu ".$NoDok; ?>
                                                                                                                                                              </strong></li>
                                                                                                                                                            </ol>-->

                                                                                                                                                            <table class="table table-bordered responsive">
                                                                                                                                                              <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                                                                                                                                              <input type='hidden' name="jml" id="jml" value="1">
                                                                                                                                                              <input type='hidden' name="nodokumen" id="nodokumen" value="<?=$NoDok;?>">
                                                                                                                                                              <tr>
                                                                                                                                                                <td colspan="100%">
                                                                                                                                                                  <table class="table table-bordered responsive" id="TabelDetail">
                                                                                                                                                                    <thead>
                                                                                                                                                                      <tr>
                                                                                                                                                                        <th width="100"><center>PCode</center></th>
                                                                                                                                                                        <th><center>Nama Menu</center></th>
                                                                                                                                                                        <th width="50"><center>Qty</center></th>
                                                                                                                                                                        <th width="50"><center>Diskon (%)</center></th>
                                                                                                                                                                        <th width="50"><center>Harga</center></th>
                                                                                                                                                                        <th width="50"><center>Total</center></th>
                                                                                                                                                                        <th width="100"><center>
                                                                                                                                                                          <button type="button" class="btn btn-<?=$warna;?> btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="addRow()">
                                                                                                                                                                            <i class="entypo-plus"></i>
                                                                                                                                                                          </button></center>
                                                                                                                                                                        </th>
                                                                                                                                                                      </tr>
                                                                                                                                                                    </thead>
                                                                                                                                                                    <tbody>

                                                                                                                                                                      <?php $no=1; ?>

                                                                                                                                                                      <tr id="baris<?php echo $no; ?>">
                                                                                                                                                                        <td>
                                                                                                                                                                          <nobr>
                                                                                                                                                                            <input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;"/>
                                                                                                                                                                            <a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-<?=$warna;?> btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari Menu" title=""><i class="entypo-search"></i></a>
                                                                                                                                                                          </nobr>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td>
                                                                                                                                                                          <input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td align="right">
                                                                                                                                                                          <input type="text" class="form-control-new" name="v_qty[]" id="v_qty<?php echo $no;?>" value="0" onBlur="toFormat('v_qty<?php echo $no;?>');" onKeyUp="getTotal(v_qty<?php echo $no;?>),toFormat('v_qty<?php echo $no;?>')" style="text-align: right;"/>
                                                                                                                                                                          <input type="hidden" class="form-control-new" name="v_hidden_qty[]" id="v_hidden_qty<?php echo $no;?>" value="0" style="text-align: right;"/>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td align="right">
                                                                                                                                                                          <input type="text" class="form-control-new" name="v_diskon[]" id="v_diskon<?php echo $no;?>" value="0" onBlur="toFormat('v_diskon<?php echo $no;?>');" onKeyUp="getDiskon(v_diskon<?php echo $no;?>),toFormat('v_diskon<?php echo $no;?>')" style="text-align: right;"/>
                                                                                                                                                                          <input type="hidden" class="form-control-new" name="v_hidden_diskon[]" id="v_hidden_diskon<?php echo $no;?>" value="0" style="text-align: right;"/>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td align="right">
                                                                                                                                                                          <input type="text" class="form-control-new" name="v_harga[]" id="v_harga<?php echo $no;?>" value="0" onBlur="toFormat('v_harga<?php echo $no;?>');" onKeyUp="getTotal(v_harga<?php echo $no;?>),toFormat('v_harga<?php echo $no;?>')" style="text-align: right;"/>
                                                                                                                                                                          <input type="hidden" class="form-control-new" name="v_hidden_harga[]" id="v_hidden_harga<?php echo $no;?>" value="0" style="text-align: right;"/>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td align="right">
                                                                                                                                                                          <input type="text" class="form-control-new" name="v_total[]" id="v_total<?php echo $no;?>" value="0" onBlur="toFormat('v_total<?php echo $no;?>');"style="text-align: right;" readonly />
                                                                                                                                                                        </td>
                                                                                                                                                                        <td align="center">
                                                                                                                                                                          <button type="button" class="btn btn-<?=$warna;?> btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
                                                                                                                                                                            <i class="entypo-trash"></i>
                                                                                                                                                                          </button>
                                                                                                                                                                        </td>
                                                                                                                                                                      </tr>
                                                                                                                                                                    </tbody>
                                                                                                                                                                  </table>
                                                                                                                                                                  <table class="table table-bordered responsive">
                                                                                                                                                                    <tr>
                                                                                                                                                                      <td align="right" colspan="4"><b>Grand Total</b></td>
                                                                                                                                                                      <td align="right" width="100">
                                                                                                                                                                        <input type="text" class="form-control-new" name="v_grand_total" onblur="toFormat('v_grand_total')" id="v_grand_total" value="0" style="text-align: right;" readonly />
                                                                                                                                                                        <input type="hidden" class="form-control-new" name="v_hidden_grand_total" id="v_hidden_grand_total" value="0" style="text-align: right;"/>
                                                                                                                                                                      </td>
                                                                                                                                                                      <td align="right" width="100"></td>
                                                                                                                                                                    </tr>
                                                                                                                                                                  </table>
                                                                                                                                                                </td>
                                                                                                                                                              </tr>

                                                                                                                                                              <tr>
                                                                                                                                                                <td colspan="100%" align="center">
                                                                                                                                                                  <input type='hidden' name="flag" id="flag" value="add">
                                                                                                                                                                  <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                                                                                                                                                  <!--<button type="button" class="btn btn-<?=$warna;?> btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan"><?=$bacaan;?><i class="entypo-check"></i></button>
                                                                                                                                                                  <button type="button" class="btn btn-info btn-icon btn-sm icon-left" name="btn_cancel" id="btn_cancel"  value="Batal" onclick="formReset(this);">Batal<i class="entypo-cancel"></i></button>-->
                                                                                                                                                                  <?php if($booking=="T"){?>
                                                                                                                                                                    <button type="button" class="btn btn-red btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="tutup_detail_beo()">Tutup<i class="entypo-cancel-circled"></i></button>
                                                                                                                                                                    <?}else{?>
                                                                                                                                                                      <button type="button" class="btn btn-red btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="tutup_detail_beo()">Tutup.<i class="entypo-cancel-circled"></i></button>
                                                                                                                                                                    <?php }?>
                                                                                                                                                                  </td>
                                                                                                                                                                </tr>

                                                                                                                                                                <tr>
                                                                                                                                                                  <td colspan="100%" align="center" id="teks">

                                                                                                                                                                  </td>
                                                                                                                                                                </tr>

                                                                                                                                                                <tr>
                                                                                                                                                                  <td colspan="100%" align="center">
                                                                                                                                                                    <div id="getDetailListMenu"></div>
                                                                                                                                                                  </td>
                                                                                                                                                                </tr>

                                                                                                                                                              </table>

                                                                                                                                                            </div>
                                                                                                                                                          <?php }else{ ?>
                                                                                                                                                            <div class="col-md-12" align="left">

                                                                                                                                                              <!--<ol class="breadcrumb">
                                                                                                                                                              <li><strong><i class="entypo-eye"></i>View <?php echo $modul; ?></strong></li>
                                                                                                                                                            </ol>-->
                                                                                                                                                            <div id="getDetailListMenu"></div>
                                                                                                                                                            <center><button type="button" class="btn btn-red btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick="tutup_detail_beo2()">Tutup<i class="entypo-cancel-circled"></i></button></center>
                                                                                                                                                            <br><br>
                                                                                                                                                          </div>
                                                                                                                                                        <?php } ?>
                                                                                                                                                      </div>
                                                                                                                                                    </div>










                                                                                                                                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="table table-bordered responsive">
                                                                                                                                                      <tr bgcolor="#EEEEEE">
                                                                                                                                                      </tr>
                                                                                                                                                      <tr bgcolor="#EEEEEE">
                                                                                                                                                        <td colspan="4" align="center" width="100%"><strong>RESTO SARINAH</strong></td>
                                                                                                                                                      </tr>
                                                                                                                                                      <tr>
                                                                                                                                                        <td colspan="4" align="center" width="50%">
                                                                                                                                                          <div class="form-group">
                                                                                                                                                            <textarea id="desc_front_office" name="desc_front_office" class="form-control border-form-bold" id="field-ta" placeholder="contoh :1.aaaaa, 2.bbbbb, 3.ccccc ,dst." style="height:165px;"><?=$desc_front_office;?></textarea>
                                                                                                                                                          </div>

                                                                                                                                                        </td>
                                                                                                                                                      </tr>

                                                                                                                                                      

                                                                                                                                                      <tr>
                                                                                                                                                        <td colspan="4" align="center" width="100%" align="center">
                                                                                                                                                          <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                                                                                                                                                            <tr>
                                                                                                                                                              <td colspan="4">Prepared by,</td>
                                                                                                                                                            </tr>
                                                                                                                                                            <tr>
                                                                                                                                                              <!--<td colspan="4"><img src="<?=base_url();?>public/images/ttd_<?=$v_add_userid;?>.png" width="100" alt="" /></td>-->

                                                                                                                                                              <td colspan="4"><br><br><br>Nindya</td>

                                                                                                                                                            </tr>
                                                                                                                                                            <tr>
                                                                                                                                                              <td colspan="4"><u><?php echo $adm_sales_nama;?></u></td>
                                                                                                                                                            </tr>
                                                                                                                                                            <tr>
                                                                                                                                                              <td colspan="4"><?php echo $adm_sales_jabatan;?></td>
                                                                                                                                                            </tr>
                                                                                                                                                            <tr>
                                                                                                                                                              <td>CC : </td>
                                                                                                                                                              <td colspan="3">
                                                                                                                                                                <table width="100%" cellpadding="5" border="0">
                                                                                                                                                                  <?php
                                                                                                                                                                  error_reporting(0);
                                                                                                                                                                  if(!empty($NoDok)){
                                                                                                                                                                    foreach($group_cc_value as $key){
                                                                                                                                                                      $data['list_gc'][$NoDok][$key['group_cc_id']]=$key['group_cc_id'];
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                  $no = 1;
                                                                                                                                                                  for($a=0;$a<count($group_cc);$a++){

                                                                                                                                                                    $gc_value = $data['list_gc'][$NoDok][$group_cc[$a]['group_cc_id']];
                                                                                                                                                                    $gc_checked = "";
                                                                                                                                                                    if($gc_value==$group_cc[$a]['group_cc_id']){
                                                                                                                                                                      $gc_checked = "checked";
                                                                                                                                                                    }
                                                                                                                                                                    if(($no % 5) == 0 ) echo "<tr>";
                                                                                                                                                                    ?>
                                                                                                                                                                    <td>
                                                                                                                                                                      <div class="checkbox">
                                                                                                                                                                        <label>
                                                                                                                                                                          <input class="devy" id="cc_food_beverage" name="group_cc[]" type="checkbox" value="<?=$group_cc[$a]['group_cc_id'];?>" <?php echo $gc_checked;?> ><?=$group_cc[$a]['group_cc_name'];?>
                                                                                                                                                                        </label>
                                                                                                                                                                      </div>
                                                                                                                                                                    </td>
                                                                                                                                                                    <?php
                                                                                                                                                                    if(($no % 5) == 4 ) echo "</tr>";
                                                                                                                                                                    $no++;
                                                                                                                                                                  }
                                                                                                                                                                  ?>
                                                                                                                                                                </table>
                                                                                                                                                              </td>
                                                                                                                                                            </tr>
                                                                                                                                                          </table>
                                                                                                                                                        </td>
                                                                                                                                                      </tr>

                                                                                                                                                      <tr bgcolor="#EEEEEE">
                                                                                                                                                        <td colspan="100%" align="center" width="50%"><strong>BIAYA TAMBAHAN</strong></td>
                                                                                                                                                      </tr>

                                                                                                                                                      <tr>

                                                                                                                                                        <td colspan="100%" align="center" width="50%">
                                                                                                                                                          <div class="form-group">
                                                                                                                                                            <textarea id="desc_biaya_tambahan" name="desc_biaya_tambahan" class="form-control border-form-bold" id="field-ta" placeholder="" style="height:120px;"><?=$desc_biaya_tambahan;?></textarea>
                                                                                                                                                          </div>
                                                                                                                                                        </td>


                                                                                                                                                      </tr>

                                                                                                                                                      <tr>
                                                                                                                                                        <td colspan="2" align="center" width="50%">
                                                                                                                                                        </td>
                                                                                                                                                        <td colspan="2" align="center" width="50%">
                                                                                                                                                          <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
                                                                                                                                                            <tr>
                                                                                                                                                              <td width="300">Biaya Tambahan</td>
                                                                                                                                                              <td>:</td>
                                                                                                                                                              <td>
                                                                                                                                                                <!--<input type="text" id="Biaya_tambahan" name="Biaya_tambahan" class="form-control border-form-bold" onBlur="toFormat('Biaya_tambahan');" value="<?=number_format($biaya_tambahan,0);?>" style="text-align: right;" onKeyUp="getTambahanBiaya(),toFormat('Biaya_tambahan')" />-->
                                                                                                                                                                <input type="text" id="Biaya_tambahan" name="Biaya_tambahan" class="form-control border-form-bold"  value="<?=number_format($biaya_tambahan,0);?>" style="text-align: right;" onKeyUp="getTambahanBiaya(),toFormat('Biaya_tambahan')" />
                                                                                                                                                                <input type='hidden' name="hidden_biaya_tambahan" id="hidden_biaya_tambahan" value="<?=$biaya_tambahan;?>">
                                                                                                                                                              </td>
                                                                                                                                                            </tr>

                                                                                                                                                            <tr>
                                                                                                                                                              <td width="300">Biaya Set Reservasi</td>
                                                                                                                                                              <td>:</td>
                                                                                                                                                              <td>
                                                                                                                                                                <input type="text" id="Total" name="Total" class="form-control border-form-bold" onBlur="toFormat('Total');" value="<?=number_format($Total-$biaya_tambahan,0);?>" style="text-align: right;" onKeyUp="getTotalBayar();" readonly />
                                                                                                                                                                <input type='hidden' name="hidden_total" id="hidden_total" value="<?=$Total-$biaya_tambahan;?>">
                                                                                                                                                              </td>
                                                                                                                                                            </tr>

                                                                                                                                                            <tr>
                                                                                                                                                              <td width="300">Total</td>
                                                                                                                                                              <td>:</td>
                                                                                                                                                              <td>
                                                                                                                                                                <input type="text" id="setTotal" name="setTotal" class="form-control border-form-bold" onBlur="toFormat('setTotal');" value="<?=number_format($setTotal,0);?>" style="text-align: right;" onKeyUp="getTotalBayar();" readonly />
                                                                                                                                                                <input type='hidden' name="hidden_settotal" id="hidden_settotal" value="<?=$setTotal;?>">
                                                                                                                                                              </td>
                                                                                                                                                            </tr>

                                                                                                                                                            <?php if($type=="edit"){?>
                                                                                                                                                              <tr>
                                                                                                                                                                <td>Deposit Date</td>
                                                                                                                                                                <td>:</td>
                                                                                                                                                                <td>
                                                                                                                                                                  <input type="text" class="form-control border-form-bold datepicker" style="text-align:center;" value="<?php echo $dp_date; ?>" name="v_date_dp" id="v_date_dp" maxlength="10">
                                                                                                                                                                </td>
                                                                                                                                                              </tr>
                                                                                                                                                            <?php } ?>


                                                                                                                                                            <?php if($booking=="Y"){
                                                                                                                                                              $nampak = "style='display: none'";
                                                                                                                                                            }else{
                                                                                                                                                              $nampak = "style='display:'";
                                                                                                                                                            } ?>
                                                                                                                                                            <tr <?=$nampak;?>>
                                                                                                                                                              <td>Jenis Pembayaran</td>
                                                                                                                                                              <td>:</td>
                                                                                                                                                              <td align="left">
                                                                                                                                                                <select class="form-control border-form-bold" name="jenis" id="jenis" style="width: 300px;" onchange="muncul()">
                                                                                                                                                                  <option  <?php if($voucher==""){ echo "selected='selected'"; } ?> value="1">Uang</option>
                                                                                                                                                                  <option  <?php if($voucher!=""){ echo "selected='selected'"; } ?> value="2">Voucher</option>
                                                                                                                                                                </select>
                                                                                                                                                              </td>
                                                                                                                                                            </tr>
                                                                                                                                                            <tr id="iddp" <?=$nampak;?>>
                                                                                                                                                              <td>Deposit</td>
                                                                                                                                                              <td>:</td>
                                                                                                                                                              <?php
                                                                                                                                                              if($session_name=="mechael0101" OR $session_name=="tonny1205" OR $session_name=="julianti" OR $session_name=="yuri0717" OR $session_name=="riska3004"){
                                                                                                                                                                $read="";
                                                                                                                                                              }else{
                                                                                                                                                                $read="disabled";
                                                                                                                                                              }?>
                                                                                                                                                              <td align="right">
                                                                                                                                                                <!--<select class="form-control-new" name="pilihdp" id="pilihdp" style="width: 100px;" onchange="getDPBayar()" <?=$read;?>>
                                                                                                                                                                <option value="">-- Choose Deposit --</option>
                                                                                                                                                                <?php

                                                                                                                                                                foreach($deposit as $val)
                                                                                                                                                                {
                                                                                                                                                                ?><option value="<?php echo $val["id"]."#".$val["Nilai"]; ?>"><?php echo $val["no_receipt"]." - ".$mylib->ubah_tanggal($val["Tanggal"])." - ".number_format($val["Nilai"]); ?></option><?php
                                                                                                                                                              }
                                                                                                                                                              ?>
                                                                                                                                                            </select>-->
                                                                                                                                                            <input type="text" id="dp" name="dp" class="form-control-new" onBlur="toFormat('dp');" value="<?=number_format($dp);?>" style="width: 300px;text-align: right;" onKeyUp="getDPBayar(),toFormat('dp')"/><!-- 180PX -->
                                                                                                                                                            <input type='hidden' name="hidden_dp" id="hidden_dp" value="<?=$dp;?>">
                                                                                                                                                          </td>
                                                                                                                                                        </tr>
                                                                                                                                                        <tr id="idvoucher" <?=$nampak;?>>
                                                                                                                                                          <td>ID Voucher</td>
                                                                                                                                                          <td>:</td>
                                                                                                                                                          <td align="right">
                                                                                                                                                            <!--<select class="form-control-new" name="pilihvoucher" id="pilihvoucher" style="width: 100px;" onchange="getVoucher()">
                                                                                                                                                            <option value="">-- Choose Voucher --</option>
                                                                                                                                                            <?php

                                                                                                                                                            foreach($voucher_beo as $val)
                                                                                                                                                            {
                                                                                                                                                            ?><option value="<?php echo $val["id"]."#".$val["no_voucher"]; ?>"><?php echo $val["no_voucher_travel"]." - ".$mylib->ubah_tanggal($val["expDate"])." - ".number_format($val["nilai"]); ?></option><?php
                                                                                                                                                          }
                                                                                                                                                          ?>
                                                                                                                                                        </select>-->
                                                                                                                                                        <input type="text" id="voucher" name="voucher" class="form-control-new"  value="<?=$voucher;?>" style="width: 300px;text-align: right;" /><!-- 180PX -->
                                                                                                                                                      </td>
                                                                                                                                                    </tr>


                                                                                                                                                    <tr id="idsisa">
                                                                                                                                                      <td>Sisa Pembayaran</td>
                                                                                                                                                      <td>:</td>
                                                                                                                                                      <td>
                                                                                                                                                        <?php
                                                                                                                                                        if($voucher==""){
                                                                                                                                                          $sis_bayar = $sisa_pembayaran;
                                                                                                                                                        }else{
                                                                                                                                                          $sis_bayar = $biaya_tambahan;
                                                                                                                                                        }
                                                                                                                                                        ?>
                                                                                                                                                        <input type="text" id="sisa_pembayaran" name="sisa_pembayaran" class="form-control border-form-bold" value="<?=number_format($sis_bayar);?>" style="text-align: right;" readonly />
                                                                                                                                                        <input type='hidden' name="hidden_sisa_pembayaran" id="hidden_sisa_pembayaran" value="<?=$sis_bayar;?>">
                                                                                                                                                      </td>
                                                                                                                                                    </tr>
                                                                                                                                                    <tr>
                                                                                                                                                      <td>Remarks Payment</td>
                                                                                                                                                      <td>:</td>
                                                                                                                                                      <td>
                                                                                                                                                        <div class="form-group">
                                                                                                                                                          <textarea id="Remarks_Payment" name="Remarks_Payment" class="form-control border-form-bold" id="field-ta" placeholder="" style="height:165px;" <?=$display;?> ><?=$Remarks_Payment;?></textarea>
                                                                                                                                                        </div>
                                                                                                                                                      </td>
                                                                                                                                                    </tr>

                                                                                                                                                    <?php if($booking=="Y"){
                                                                                                                                                      $class_btnx = "btn btn-primary btn-icon btn-sm icon-left";
                                                                                                                                                      $value_button="Booking";
                                                                                                                                                    }else{
                                                                                                                                                      $class_btnx = "btn btn-info btn-icon btn-sm icon-left";
                                                                                                                                                      $value_button="Simpan";
                                                                                                                                                    } ?>
                                                                                                                                                    <tr>
                                                                                                                                                      <td colspan="3">
                                                                                                                                                        <?php
                                                                                                                                                        if($status=="0"){
                                                                                                                                                          if($UserLevel=="6" || $UserLevel=="7" || $UserLevel=="-1" ||  $UserLevel=="16" || $UserLevel=="23" || $UserLevel=="29" || $UserLevel=="30" || $UserLevel=="32"){ ?>
                                                                                                                                                            <button type="button" class="<?=$class_btnx;?>" onclick="cekTheformReserv();" name="btn_save" id="btn_save"  value="Simpan"><?=$value_button;?><i class="entypo-check"></i></button>
                                                                                                                                                            <?php if($type=="edit"){?>
                                                                                                                                                              <input type="hidden"  name="v_approve" id="v_approve" value="0">
                                                                                                                                                              <input type="hidden"  name="v_approve_head" id="v_approve_head" value="0">
                                                                                                                                                              <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheformReserv2();" name="btn_save_kirim" id="btn_save_kirim"  value="Simpan & Kirim">Simpan & Kirim<i class="entypo-check"></i></button>
                                                                                                                                                              <button type="button" class="btn btn-red btn-icon btn-sm icon-left" name="btn_cancel" id="btn_cancel"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi/batal_reservasi/".$NoDok.""; ?>">Batalkan<i class="entypo-cancel"></i></button>
                                                                                                                                                            <?php } ?>
                                                                                                                                                            <button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi/"; ?>">Keluar<i class="entypo-cancel-circled"></i></button>
                                                                                                                                                          <?php }else{ ?>
                                                                                                                                                            <button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi/"; ?>">Keluar<i class="entypo-cancel-circled"></i></button>
                                                                                                                                                          <?php }

                                                                                                                                                        }else if($status=="1" AND $UserLevel=="23" || $session_name=="dicky0707"){?>
                                                                                                                                                          <input type="hidden"  name="v_approve" id="v_approve" value="0">
                                                                                                                                                          <!--<input type="hidden"  name="v_approve_head" id="v_approve_head" value="0">
                                                                                                                                                          <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="approve();" name="btn_approve" id="btn_approve"  value="Approve">Approve<i class="entypo-check"></i></button>
                                                                                                                                                          <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheformReserv2();" name="btn_save_kirim" id="btn_save_kirim"  value="Simpan & Kirim">Simpan & Kirim<i class="entypo-check"></i></button>-->
                                                                                                                                                          <button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi/"; ?>">Keluar<i class="entypo-cancel-circled"></i></button>

                                                                                                                                                          <input type="hidden"  name="v_approve_head" id="v_approve_head" value="0">
                                                                                                                                                          <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="approve();" name="btn_approve" id="btn_approve"  value="Approve">Approve<i class="entypo-check"></i></button>
                                                                                                                                                          <input type="hidden"  name="v_reject_head" id="v_reject_head" value="0">
                                                                                                                                                          <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" onclick="reject();" name="btn_reject" id="btn_reject"  value="Reject">Reject<i class="entypo-check"></i></button>
                                                                                                                                                          <input type="text" name="ket_reject" id="ket_reject" class="form-control-new" style="width:300px;" placeholder="Input Alasan Jika Reject"/>


                                                                                                                                                        <? }else{?>
                                                                                                                                                          <button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi/"; ?>">Keluar<i class="entypo-cancel-circled"></i></button>
                                                                                                                                                          <!--<input type="hidden"  name="v_approve_head" id="v_approve_head" value="0">
                                                                                                                                                          <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="approve();" name="btn_approve" id="btn_approve"  value="Approve">Approve<i class="entypo-check"></i></button> -->
                                                                                                                                                          <?php if($edit_request=="0" AND ($session_name=="julianti" OR $session_name=="riska3004" OR $session_name=="yuri0717" OR  $session_name=="mechael0101" OR $session_name=="tonny1205")){?>
                                                                                                                                                            <input type="hidden"  name="v_edit_request" id="v_edit_request" value="0">
                                                                                                                                                            <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" onclick="edit_request();" name="btn_edit_request" id="btn_edit_request"  value="Reject">Request Edit<i class="entypo-check"></i></button>
                                                                                                                                                            <input type="text" name="ket_edit_request" id="ket_edit_request" class="form-control-new" style="width:300px;" placeholder="Input Jika Edit Request"/>


                                                                                                                                                          <?php }} ?>
                                                                                                                                                        </td>
                                                                                                                                                      </tr>
                                                                                                                                                    </table>
                                                                                                                                                  </td>
                                                                                                                                                </tr>
                                                                                                                                              </table>
                                                                                                                                            </form>


                                                                                                                                          </body>



                                                                                                                                          <?php
                                                                                                                                          $this->load->view('footer');
                                                                                                                                          ?>

                                                                                                                                          <link rel="stylesheet" href="<?= base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.css">
                                                                                                                                          <link rel="stylesheet" href="<?= base_url(); ?>assets/js/select2/select2-bootstrap.css">
                                                                                                                                          <link rel="stylesheet" href="<?= base_url(); ?>assets/js/select2/select2.css">

                                                                                                                                          <script src="<?= base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
                                                                                                                                          <script src="<?= base_url(); ?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
                                                                                                                                          <script src="<?= base_url(); ?>assets/js/select2/select2.min.js"></script>

                                                                                                                                          <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
                                                                                                                                          <script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
                                                                                                                                          <script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

                                                                                                                                          <script>
                                                                                                                                          $(document).ready(function() {

                                                                                                                                            voucher = $('#voucher').val();
                                                                                                                                            if(voucher==""){
                                                                                                                                              document.getElementById("idvoucher").style.display = "none";
                                                                                                                                            }else{
                                                                                                                                              document.getElementById("idvoucher").style.display = "";
                                                                                                                                              document.getElementById("iddp").style.display = "none";
                                                                                                                                            }

                                                                                                                                            $('#jam_kedatangan').timepicker({
                                                                                                                                              //showPeriodLabels: false
                                                                                                                                              showMeridian : false
                                                                                                                                            });


                                                                                                                                            //load view detail list menu
                                                                                                                                            getData();


                                                                                                                                          });

                                                                                                                                          function getData(){

                                                                                                                                            base_url = $("#baseurl").val();
                                                                                                                                            nodok= $("#NoDokumen").val();

                                                                                                                                            $.ajax({
                                                                                                                                              type: "GET",
                                                                                                                                              url: base_url + "index.php/transaksi/reservasi/getDetailListMenu/"+nodok,
                                                                                                                                              success: function(data) {
                                                                                                                                                $('#getDetailListMenu').html(data);


                                                                                                                                              }
                                                                                                                                            });
                                                                                                                                          }

                                                                                                                                          function muncul(){
                                                                                                                                            jenis = $('#jenis').val();
                                                                                                                                            Biaya_tambahan = $('#hidden_biaya_tambahan').val();
                                                                                                                                            hidden_total = $('#hidden_total').val();
                                                                                                                                            if(jenis=="1"){
                                                                                                                                              document.getElementById("iddp").style.display = "";
                                                                                                                                              //document.getElementById("idsisa").style.display = "";
                                                                                                                                              document.getElementById("idvoucher").style.display = "none";
                                                                                                                                              $('#voucher').val("");
                                                                                                                                              hitung = parseFloat(hidden_total)+parseFloat(Biaya_tambahan);
                                                                                                                                              $('#sisa_pembayaran').val(hitung);
                                                                                                                                              document.getElementById("dp").focus();
                                                                                                                                            }else{
                                                                                                                                              document.getElementById("iddp").style.display = "none";
                                                                                                                                              //document.getElementById("idsisa").style.display = "none";
                                                                                                                                              document.getElementById("idvoucher").style.display = "";
                                                                                                                                              $('#dp').val("0");
                                                                                                                                              $('#sisa_pembayaran').val(Biaya_tambahan);
                                                                                                                                              document.getElementById("voucher").focus();
                                                                                                                                            }



                                                                                                                                          }

                                                                                                                                          function hapus_trans_detail(nodok,pcode,total,url){
                                                                                                                                            var r=confirm("Apakah anda akan menghapus Menu tersebut?")
                                                                                                                                            if (r==true)
                                                                                                                                            {
                                                                                                                                              document.getElementById("teks").innerHTML = "Proses Hapus Detail Menu...";

                                                                                                                                              var total_old = reform(document.getElementById("Total").value)*1;
                                                                                                                                              total_baru = total_old  - total;

                                                                                                                                              document.getElementById("Total").value = format(total_baru);
                                                                                                                                              document.getElementById("hidden_total").value = total_baru;

                                                                                                                                              getTotalBayar();
                                                                                                                                              getTambahanBiaya();

                                                                                                                                              $.ajax({
                                                                                                                                                url: "<?php echo site_url('transaksi/reservasi/hapus_trans_detail') ?>/" + nodok+"/"+pcode,
                                                                                                                                                type: "GET",
                                                                                                                                                dataType: "JSON",
                                                                                                                                                success: function (data)
                                                                                                                                                {
                                                                                                                                                  if(data){
                                                                                                                                                    getData();
                                                                                                                                                    document.getElementById("teks").innerHTML = "";
                                                                                                                                                  }
                                                                                                                                                }
                                                                                                                                                ,
                                                                                                                                                error: function (textStatus, errorThrown)
                                                                                                                                                {
                                                                                                                                                  alert('Error get data');
                                                                                                                                                }
                                                                                                                                              }
                                                                                                                                            );
                                                                                                                                          }
                                                                                                                                          else
                                                                                                                                          {
                                                                                                                                            return false;
                                                                                                                                          }
                                                                                                                                        }
                                                                                                                                      </script>
