<?php
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E';
$nbold = chr(27) . 'F';
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

//$ftext = printer_open("\\\\".$_SERVER['REMOTE_ADDR']."\\HPLaserJ");//
//$ftext = printer_open("\\\\192.168.0.30\\EPSON");
//$ftext = printer_open("\\\\".$ip."\\".$nm_printer);
//$ftext = printer_open($_SERVER['REMOTE_ADDR']);
//$ftext = printer_open("epson");
            
$ftext = printer_open($nm_printer); 
printer_set_option($ftext, PRINTER_MODE, "raw");
printer_set_option($ftext, PRINTER_COPIES, "1"); 
$alamatPT=$store[0]['Alamat1PT'];
$tgl=$header[0]['AddDate'];
$tgl_1=explode("-",$tgl);
$tgl_tampil=$tgl_1[2]."/".$tgl_1[1]."/".$tgl_1[0];
printer_write($ftext, $reset.$elite);
printer_write($ftext, str_pad($store[0]['NamaAlias'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_write($ftext, str_pad($store[0]['Alamat1PT'], 39, " ", STR_PAD_BOTH) . "\r\n");
printer_write($ftext, str_pad($store[0]['Alamat2PT'], 39, " ", STR_PAD_BOTH) . "\r\n");

//printer_write($ftext, "\r\n");
printer_write($ftext, "========================================\r\n");
printer_write($ftext, str_pad("Struk",11).": " .$header[0]['NoDokumen']."\r\n");
printer_write($ftext, str_pad("Tanggal",11).": ".date('d-m-Y', strtotime($header[0]['AddDate']))."\r\n") ;
printer_write($ftext, str_pad("Kasir",11).": " .$this->session->userdata('username')."\r\n");
printer_write($ftext, "----------------------------------------\r\n");
for($a=0;$a<count($detail);$a++){
	printer_write($ftext, str_pad($detail[$a]['PCode'],8)." ".str_pad(substr($detail[$a]['NamaBarang'],0,20),20)."\r\n".
	       str_pad(round($detail[$a]['Qty']),10," ",STR_PAD_LEFT)." X ".
		   str_pad(number_format($detail[$a]['Harga'],0,',','.'),10," ",STR_PAD_LEFT).
                        str_pad(number_format($detail[$a]['Qty'] * $detail[$a]['Harga'],0,',','.'),17," ",STR_PAD_LEFT)."\r\n");   
	
}
printer_write($ftext, "----------------------------------------\r\n");
printer_write($ftext, str_pad("Total ".count($detail)." item",24).":".str_pad(number_format($header[0]['Total'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
//if($header[0]['Discount']<>0)
//printer_write($ftext, str_pad("Discount ".$header[0]['InitDisc'],24).":".str_pad(number_format($header[0]['DiscRupiah'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
//printer_write($ftext, str_pad("Total Rupiah ",24).":".str_pad(number_format($header[0]['Total'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
printer_write($ftext, str_pad("DP ",24).":".str_pad(number_format($header[0]['dp'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
printer_write($ftext, "----------------------------------------\r\n");
if($header[0]['Sisa_Tunai']<>0)
printer_write($ftext, str_pad("Tunai ",24).":".str_pad(number_format($header[0]['Sisa_Tunai'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
if($header[0]['Sisa_Kredit']<>0)
printer_write($ftext, str_pad("Kartu Kredit ",24).":".str_pad(number_format($header[0]['Sisa_Kredit'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");
if($header[0]['Sisa_Debit']<>0)
printer_write($ftext, str_pad("Kartu Debit ",24).":".str_pad(number_format($header[0]['Sisa_Debit'], 0, ',', '.'),15," ",STR_PAD_LEFT)."\r\n");

printer_write($ftext, "========================================\r\n");
printer_write($ftext, "             ===Terima kasih===          \r\n");
printer_write($ftext, "\r\n");
printer_write($ftext, "\r\n");
printer_write($ftext, "\r\n");
printer_write($ftext, $fcut);
printer_write($ftext, $op_cash);
printer_close($ftext);
echo "<script type='text/javascript'>$('#modal_payment_method').modal('hide');</script>";
?>