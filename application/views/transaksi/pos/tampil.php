<?php
$this->load->view('header');
$session_name = $this->session->userdata('username');
?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">
<script src="<?php echo base_url(); ?>public/js/shortcuts_v1.js" type="text/javascript"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/grup_harga.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/transaksi_sales.js"></script>
<script>
	$(function() {
    	toLCD2("Selamat Datang","Di Secret Garden");
	});

    function key() {
        document.getElementById('kdbrg1').focus();
		shortcut("F2",function(){
			document.getElementById('cash_bayar').focus();
		});
		shortcut("F3",function(){
			document.getElementById('kredit_bayar').focus();
		});
		shortcut("F4",function(){
			document.getElementById('debet_bayar').focus();
		});
		shortcut("F5",function(){
			document.getElementById('id_discount').focus();
		});
/*
        shortcut("F8", function () {
			
			var indexs = document.getElementsByName("pcode00[]").length;
			
			pcodeasli = $("#lkodebarang"+indexs).val();
			namaasli = $("#lkodebarang"+indexs).val();
			qtyasli = parseFloat($("#ljumlahqty" + indexs).val());
			hargaasli  = parseFloat($("#ljumlahharga" + indexs).val());
			discasli = parseFloat($("#ldisc" + indexs).val());
			nettoasli = parseFloat($("#ljumlahnetto" + indexs).val());
			nourut = $("#lfield_sales_temp" + indexs).val();

			document.getElementById('NoUrut').value = nourut;
			document.getElementById('kdbrg1').value = pcodeasli;
			document.getElementById('nmbrg1').value = namaasli;
			document.getElementById('qty1').value = qtyasli;
			document.getElementById('jualm1').value = hargaasli;
			document.getElementById('netto1').value = nettoasli;
			document.getElementById('jualmtanpaformat1').value = hargaasli;
			document.getElementById('nettotanpaformat1').value = nettoasli;

			document.getElementById('NoUrut').readOnly = true;
			document.getElementById('kdbrg1').readOnly = true;
			document.getElementById('qty1').readOnly = false;
			document.getElementById('qty1').focus();

			document.getElementById('EditFlg').value = 1;
        });
		*/
    }
    function start() {
        // cek cookies penjualanPOS
		/*
        dt = (getCookie("penjualanPOS"));
//        alert(dt.length);
//        alert(eval(dt).length);
        if(dt.length > 0){
            barisPOS();
        }else {
            document.getElementById('temp_pos1').style.display = "block";
//            document.getElementById('idbayarkosong').style.display = "block";
//            document.getElementById('idbayar').style.display = "block";
            document.getElementById('temp_pos2').style.display = "none";
        }
		*/
        document.getElementById('kdbrg1').focus();
    }
    
    function cetakstrukterakhir(){
 		window.location='<?php echo base_url();?>index.php/transaksi/pos/cetakstrukakhir';
	}


</script>
<style type="text/css">
    .huruf {
        font-family: Arial; /* font name */
        font-size: 12px; /* font size */
    }

    .cetak {
        font-family: Courier New; /* font name */
        font-size: 10px; /* font size */
    }

    .tgl {
        font-family: Verdana; /* font name */
        font-size: 13px; /* font size */
        font-weight: bold;
        color: #ffffff;
    }

    .clock {
        font-family: Verdana; /* font name */
        font-size: 20px; /* font size */
        font-weight: bold;
        color: #FFCC00;
        border-color: #ffffff;
    }

		 select.Menu{
		font-family: Verdana,sans serif;
		font-size: 14pt;
		font-weight: bold;
		font-style: normal;
		}

		option.Menu1{
		color:#FFFFFF;
		background-color:#C0C0C0;
		}
		option.Menu2{
		color:#FFFFFF;
		background-color:#0000FF;
		}
		option.Menu3{
		color:#FFFFFF;
		background-color:#964B00;
		}
		
		option.Menu4{
		color:#FFFFFF;
		background-color:#00FF00;
		}
		option.Menu5{
		color:#FFFFFF;
		background-color:#000000;
		}
		option.Menu6{
		color:#FFFFFF;
		background-color:#FFFF00;
		}
		
		option.Menu7{
		color:#FFFFFF;
		background-color:#FF00FF;
		}
		option.Menu8{
		color:#FFFFFF;
		background-color:#FF0000;
		}
		option.Menu9{
		color:#FFFFFF;
		background-color:#FF7F00;
		}
		
		option.Menu10{
		color:#FFFFFF;
		background-color:#FFC0CB;
		}
		option.Menu11{
		color:#000000;
		background-color:#FFFFFF;
		}
		option.Menu12{
		color:#FFFFFF;
		background-color:#BF00FF;
		}
		
		option.Menu13{
		color:#FFFFFF;
		background-color:#00FFFF;
		}
		option.Menu14{
		color:#000000;
		background-color:#FFD700;
		}
		option.Menu15{
		color:#FFFFFF;
		background-color:#000080;
		}
		
</style>

<body onLoad="start();key();">
<?php
$tgl = date('d-m-Y');
$jam = date('H:i:s');
if (empty($struk)) {
    $no = 1;
} else {
    $no = $struk;
}
?>
<div class="table-responsive">
<table width="85%" align="center" border="0" cellpadding="0" cellspacing="0" class="table table-bordered responsive">
<form name="form1" id="form1" method="post" action="<?= base_url(); ?>index.php/transaksi/pos/save_trans"
          onsubmit='cek_form();'>
<td width="70%" valign="top" height="20%">
     <input type="hidden" name="EditFlg" id="EditFlg" value="0">
     <input type="hidden" name="tot_diskon" id="tot_diskon" value="0">
     <input type="hidden" name="v_kdstore" id="v_kdstore" value="<?= $NoKassa[0]['KdStore'];?>">
        <!--                        <div><button class="btn btn-blue btn-icon" type="button" onClick="popbrgFromSales('1', '-->
        <? ////= base_url(); ?><!--//');" >ITEM LIST <i class="entypo-basket"></i></button></div>-->
        <!--                        </br>-->
        <table width='100%' align="center" class='table table-bordered responsive' >
            <tr>
                <td><b>PERNJUALAN MARKETPLACE</b></td>
                <td>
                    <select class="form-control" id="KdMarketPlace">
                        <option value="">Pilih</option>
                        <?php foreach ($GroupHarga as $key => $value) {echo "<option value='" . $value['GroupHargaID'] . "'>" . $value['GroupHargaName'] . "</option>";}?>
                    </select>
                </td>
            </tr>
        </table>
        <table width='100%' align="center" class='table table-bordered responsive' name="detail">
            <thead>
            <tr>
                <th width="5%">Edit</th>
                <th width="15%">Item</th>
                <th width="30%">Nama</th>
                <th width="10%">Qty</th>
                <th width="14%">Harga</th>
				<th width="12%">Disc</th>
                <th width="14%">Netto</th>
            </tr>
            </thead>
            <tr>
                <td><input type="text" id="NoUrut" class="form-control" name="NoUrut" size="1" maxlength="1"
                           onKeyDown="EditRecord(event, 'NoUrut', '<?= base_url(); ?>')"></td>
                <!-- Ubah maxleng sesuai dengan panjang barcode jika ingin menggukan barcode by mart -->
                <td><input type="text" id="kdbrg1" class="form-control" name="kdbrg1" size="18" maxlength="18"
                           onKeyDown="getCodeForSales(event, '1', '<?= base_url(); ?>', 'insert')"></td>
                <td><input type="text" class="form-control" readonly id="nmbrg1" name="nmbrg1" size="35" value=""></td>
                <td><input type="text" class="form-control" id="qtym1" name="qtym1" size="8" onKeyUp="SetNetto()"
                           onKeyDown="getCodeForSales(event, '1', '<?= base_url(); ?>', 'edit')" readonly>
                    <input type="hidden" readonly id="disk1" name="disk1" size="12" value="">
                </td>
                <td>
                    <input type="text" class="form-control" readonly id="jualm1" name="jualm1" size="12" value=""
                           class="InputAlignRight">
                    <input type="hidden" readonly id="jualmtanpaformat1" name="jualmtanpaformat1" size="12" value=""
                           class="InputAlignRight">
                </td>
				<td>
                    <input type="text" class="form-control" readonly id="discm1" name="discm1" size="9" value=""
                           class="InputAlignRight">
                    <input type="hidden" readonly id="discmtanpaformat1" name="discmtanpaformat1" size="9" value=""
                           class="InputAlignRight">
                </td>
                <td>
                    <input type="text" class="form-control" readonly id="nettom1" name="nettom1" size="12" value=""
                           class="InputAlignRight">
                    <input type="hidden" readonly id="nettotanpaformat1" name="nettotanpaformat1" size="12" value=""
                           class="InputAlignRight">
                </td>
            </tr>
            <input type="hidden" name="kassa" id="kassa" value="<?= $NoKassa[0]['id_kassa'] ?>">
            <input type="hidden" name="kasir" id="kasir" value="<?= $session_name ?>">
            <input type="hidden" name="no" id="no" value="<?= $no ?>">
            <input type="hidden" name="store" id="store" value="<?= $NoKassa[0]['KdStore'] ?>">
			<input type="hidden" name="tgltrans" id="tgltrans" value="<?= $store[0]['TglTrans'] ?>">
            <input type="hidden" name="brgDetail" id="brgDetail" value="">

        </table>
</td>
<td rowspan="2" valign="top" width="30%">
    
        
            <!--<div class="panel panel-primary">-->
                <div class="panel-heading" style="display: none;">
                    <div class="clock">
                        <div id="jam" align="center">
                        
                            <script language="javascript">
                                jam();
                            </script>
                        </div>
                    </div>
                </div>
                
                <input type='hidden' id='pelanggan' name='pelanggan' size='10'
                           onKeyDown="CustomerView(event, '1', '<?= base_url(); ?>')" class="form-control">
                           
                <input type='hidden' id='nama_customer' name='nama_customer' size='20' readonly
                           class="form-control">
                                      
                <!--<div class="panel-body">
                    <br>-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label class="col-sm-4 control-label">Id Customer</label>-->
                    <!--                                        <div class="col-sm-8">-->
                    
                    <!--                                        </div>-->
                    <!--                                    </div>-->
                    <!--                                    <br>-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label class="col-sm-4 control-label"></label>-->
                    <!--                                        <div class="col-sm-8">-->
                    <!--                                            <input type='hidden' id='id_customer' name='id_customer' size='10' readonly>-->
                    <!--                                            <input type='hidden' id='gender_customer' name='gender_customer' size='10' readonly>-->
                    <!--                                            <input type='hidden' id='tgl_customer' name='tgl_customer' size='10' readonly>-->
                    
                    <!--                                        </div>-->
                    <!--                                    </div>-->
                    <!--                                    <br>-->
                    <!--<div class="form-group">
                        <label class="col-sm-4 control-label">No Stiker</label>

                        <div class="col-sm-8">
                            <input type='text' id='kdagent' name='kdagent' size='4' maxlength="4"
                                   class="form-control input-sm">
                        </div>
                    </div>
                    &nbsp;
                    <input type='hidden' id='gsa' name='gsa' value='0'>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">GSA Color</label>

                        <div class="col-sm-8">
                            <select id='gsa' name='gsa' class="form-control" data-allow-clear="true" data-placeholder="Pilih Tour Travel" style="margin-bottom:5px;">
								<option value=""> -- Pilih Warna -- </option>
								
								
								<?php 
								$urut=1;
								foreach($gsa_list AS $ar){?>
									<?
										if($ar['KdGsa']==0){
											$teks="Non GSA";
										}else{
											$teks=$ar['warna'];
										}
									?>
									<option  class="Menu<?=$ar['id_warna'];?>" value="<?=$ar['KdGsa'];?>"><?=$teks;?></option>
								<?php 
								$urut++;} ?>
								
							</select>
                        </div>
                    </div>-->
                <!--</div>-->
            <!--</div>-->
            <table class="table responsive">
                <tbody>
                <tr>
                    <td width="5%">&nbsp;</td>
                    <td class="left"><b>No. Sticker </b></td>
                    <td class="left"><b>
                        <input type='text' id='kdagent' name='kdagent' size='4' maxlength="4"
                                   class="form-control input-sm"></b>
                    </td>
                </tr>
                <tr class="warning">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><b>Item in cart </b></td>
                    <td class="left"><b>
                        <div id="TotalItem"></div>
                        <input type='hidden' id='TotalItem2' name='TotalItem2'  value=0>
                        <input type='hidden' id='dpp' name='dpp'  value=0>
                        <input type='hidden' id='tax' name='tax'  value=0>
						<input type='hidden' id='iduserdiscount' name='iduserdiscount'  value=''>
						<input type='hidden' id='iddiscountasli' name='iddiscountasli'  value=0>
						</b>
                    </td>
                </tr>
                <tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><b>Total Barang Promo  </b></td>
                    <td class="left"><b><div id="ttlpromo"></div></b></td>
                </tr>
                
                <tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><b>Netto </b> </td>
                    <td class="left"><b><div id="TotalNetto"></div></b></td>
                </tr>
				<tr class="success">
                        <td width="5%">&nbsp;</td>
                        <td class="left">                      
                        <b>Discount</b><?
						//Tambahan Frangky 15/12/2016 
						
						$q = "SELECT Minimum, Nilai
								FROM discountheader h
								INNER JOIN(SELECT NoTrans, Nilai FROM discountdetail WHERE Jenis='X' GROUP BY NoTrans, Nilai) AS d
								ON h.NoTrans=d.NoTrans
								WHERE 1
								AND h.Status='1'
								AND h.TglAwal<='".$store[0]['TglTrans']."' AND h.TglAkhir>='".$store[0]['TglTrans']."'";
						$qry = mysql_query($q);
						$hsl=mysql_fetch_array($qry);
						$minimum = $hsl['Minimum'];
						$nil_disc = $hsl['Nilai'];
						mysql_free_result($qry);
						
						?>
						
						<input type='hidden' id='minimum' name='minimum' value='<? echo $minimum;?>'>
						<input type='hidden' id='nil_disc' name='nil_disc' value='<? echo $nil_disc;?>'>						
						
						</td>
						<td class="left"><b><input style="text-align: right;" type='text' id='id_discount' name='id_discount' size='5' value=0
								   onKeyDown="InputDiscount(event, '1', '<?= base_url(); ?>')" class="form-control">
								   
								   <input type='hidden' id='jenis_discount' name='jenis_discount' value='' placeholder="jenis_disc">
								   <input type='hidden' id='id_members' name='id_members' value='' placeholder="members">
								   <input type='hidden' id='id_card' name='id_card' value='' placeholder="card">
								   <input type='hidden' id='nm_card' name='nm_card' value='' placeholder="nama card">
								   <input type='hidden' id='persentase_card' name='persentase_card' value='' placeholder="persentase card">
						</b></td>
                </tr>
				<!--
                <tr class="success">
                        <td width="5%">&nbsp;</td>
                        <td class="left"><h5>Service Charge : </h5></td>
                        <td class="left"><h5><div id="Charge"></div> </h5></td>
                </tr>
				-->
                <tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><b>Rounding  </b></td>
                    <td class="left"><b><div id="rounding"></div></b></td>
                    <input type='hidden' id='rounding_hide' name='rounding_hide' size="10" value=0>
                </tr>
                
				
                <tr class="success">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><b>Total  </b></td>
                    <td class="left"><b><div id="ttlall"></div></b></td>
                </tr>


                </tbody>
            </table>

            <!--<div class="panel panel-primary">
                <div class="panel-heading" align="center" style="display: none;">
                    Jenis Bayar
                </div>-->
                <div class="panel-body">
                    <table align="center" class="table-responsive" border="0" width="95%">
                        <tr>
                            <td>
                                <input type='hidden' id='confirm_struk' name='confirm_struk' size='10' value="">
                                <input type='hidden' id='confirm_kassa' name='confirm_kassa' size='10' value="<?= $NoKassa[0]['id_kassa'] ?>">
                                <input type='hidden' id='service_charge' name='service_charge' size='10'
                                       value="<?= $Charge ?>">
                                <input type='hidden' id='total_biaya' name='total_biaya' size='10'
                                       class="InputAlignRight" value=0>
								<input type="hidden" id="listpcode" name="listpcode" value="">
								<input type="hidden" id="listqty" name="listqty" value="">
								<input type="hidden" id="listharga" name="listharga" value="">
								<input type="hidden" id="listdisc" name="listdisc" value="">
								<input type="hidden" id="listnetto" name="listnetto" value="">
								<input type="hidden" id="listkomisi" name="listkomisi" value="">
								<input type="hidden" id="listpdisc" name="listpdisc" value="">
								<input type="hidden" id="listkomisilokal" name="listkomisilokal" value="">
								<input type="hidden" id="listpdisclokal" name="listpdisclokal" value="">
								<input type="hidden" id="listjenisdisc" name="listjenisdisc" value="">
								<input type="hidden" id="listnilaidisc" name="listnilaidisc" value="">
								<input type="hidden" id="listkhusus" name="listkhusus" value="">
								
								<input type="hidden" id="listvoucher" name="listvoucher" value="">
								<input type="hidden" id="listvouchket" name="listvouchket" value="">
								<input type="hidden" id="listvouchsaldo" name="listvouchsaldo" value="">
								<input type="hidden" id="listvouchpakai" name="listvouchpakai" value="">
								<input type="hidden" id="listjenis" name="listjenis" value="">
								
								<input type="hidden" id="listcompliment" name="listcompliment" value="">
								<input type="hidden" id="listcomplket" name="listcomplket" value="">
								<input type="hidden" id="listcomplsaldo" name="listcomplsaldo" value="">
								<input type="hidden" id="listcomplpakai" name="listcomplpakai" value="">
								<input type="hidden" id="listcompljenis" name="listcompljenis" value="">
                            </td>
                        </tr>
                        
                        <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo_pos("Currency", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
						<tr>
                            <td nowrap>
                                <input style="display: none;" type="radio" id="pilihan" name="pilihan" value="cash"
                                       onClick="oncek()" "checked">
                                <b>Point</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                           
                            <td nowrap colspan="3"><input type='text' id='bayar_point' name='bayar_point' size='10'
                                              onKeyUp="SetKembali()" class="form-control" dir="rtl" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <input style="display: none;" type="radio" id="pilihan" name="pilihan" value="cash"
                                       onClick="oncek()" "checked">
                                <b>Tunai</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                           
                            <td nowrap colspan="3"><input type='text' id='cash_bayar' name='cash_bayar' size='10'
                                              onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input style="display: none;" type="radio" id="pilihan" name="pilihan" value="kredit" onClick="oncek()">
                                <b>Kredit</b>
                            </td>
                            <td>
							<!-- <input type='text' id='id_kredit' name='id_kredit' size='10' placeholder='test 1'
                                       onKeyDown="KreditCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"> -->
							<select  class="form-control" id='id_kredit' name='id_kredit'>
							<option value="">-Kartu-</option>
							<?php
							foreach($kartu as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
							</td>
                            <td>
							<!-- <input type='text' id='id_kredit' name='id_kredit' size='10' placeholder='test 2'
                                       onKeyDown="KreditCustomer(event, '1', '<?= base_url(); ?>')"
                                       class="form-control"> -->
							<select  class="form-control" id='id_kredit_edc' name='id_kredit_edc'>
							<option value="">-EDC-</option>
							<?php
							foreach($edc as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
							</td>
                            <td><input type='text' id='kredit_bayar' name='kredit_bayar' size='10' placeholder=''
                                       onKeyUp="SetKembali()" class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input style="display: none;" type="radio" id="pilihan" name="pilihan" value="debet" onClick="oncek()">
                                <b>Debit</b>
                            </td>
                            <td>
							<!-- <input type='text' id='id_debet' name='id_debet' size='10' placeholder='test 4'
                                       onKeyDown="DebetCustomer(event, '1', '<?= base_url(); ?>')" class="form-control"> -->
							<select  class="form-control" id='id_debet' name='id_debet'>
							<option value="">-Kartu-</option>
							<?php
							foreach($kartu as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
                            </td>
                            <td>
							<!-- <input type='text' id='id_debet' name='id_debet' size='10' placeholder='test 5'
                                       onKeyDown="DebetCustomer(event, '1', '<?= base_url(); ?>')" class="form-control"> -->
							<select  class="form-control" id='id_debet_edc' name='id_debet_edc'>
							<option value="">-EDC-</option>
							<?php
							foreach($edc as $val)
							{
								?><option value="<?php echo $val["id"]; ?>"><?php echo $val["nama"]; ?></option><?php
							}
							?>
							</select>
                            </td>
                            <td><input type='text' id='debet_bayar' name='debet_bayar' size='10' placeholder='' onKeyUp="SetKembali()"
                                       class="form-control" dir="rtl"></td>
                        </tr>
                        <tr>
                            <td><input style="display: none;" type="radio" id="pilihan" name="pilihan" value="voucher" onClick="VoucherCustomer(event, '1', '<?= base_url(); ?>')">
                                <button type="button" class="btn btn-orange btn-sm " onclick="VoucherCustomer(event, '1', '<?= base_url(); ?>')" name="btn_voucher" id="btn_voucher"  value="Simpan"><b>Voucher</b>&nbsp;&nbsp;&nbsp;&nbsp;</button>
                            </td>
                            <td><input type='text' id='id_voucher' name='id_voucher' size='10' value=''
                                       onKeyDown="VoucherCustomer(event, '1', '<?= base_url(); ?>')" class="form-control" readonly>
							</td>
                            <td colspan='2'><input  type='text' id='voucher_bayar' name='voucher_bayar' size='10'
                                       onKeyUp="SetKembali()" class="form-control" readonly></td>
                                       
                             <td><input type='hidden' id='jenis_voucher' name='jenis_voucher' size='10'
                                        class="form-control" readonly></td>
                                       							
                        </tr>
						<tr>
                            <td><input style="display: none;" type="radio" id="pilihan" name="pilihan" value="compliment" onClick="ComplimentCustomer(event, '1', '<?= base_url(); ?>')">
                                <button type="button" class="btn btn-primary btn-sm " onclick="ComplimentCustomer(event, '1', '<?= base_url(); ?>')" name="btn_compliment" id="btn_compliment"  value="Simpan"><b>Compliment</b></button>
                            </td>
                            <td><input type='text' id='id_compliment' name='id_compliment' size='10' value=''
                                       onKeyDown="ComplimentCustomer(event, '1', '<?= base_url(); ?>')" class="form-control" readonly>
							</td>
                            <td colspan='2'><input type='text' id='compliment_bayar' name='compliment_bayar' size='10'
                                       onKeyUp="SetKembali()" class="form-control" readonly></td>							
                        </tr>
						<tr>
                            <td><input style="display: none;" type="radio" id="pilihan" name="pilihan" value="debet" onClick="oncek()">
                                <b>Valas</b>
                            </td>
                            <td><input type='text' id='id_valas' name='id_valas' size='10' value=''
                                       onKeyDown="ValasCustomer(event, '1', '<?= base_url(); ?>')" class="form-control">
                            </td>
                            <td colspan='2'><input type='text' id='valas_bayar' name='valas_bayar' size='10' onKeyUp="SetKembali()"
                                       class="form-control" dir="rtl"></td>
                        </tr>
						<tr>
							<td></td>
							<td></td>
							<td colspan='2'>
							<input type="text" id="keterangan" name="keterangan" value="" class="form-control" readonly>
							<input type="hidden" id="jenisvoucher" name="jenisvoucher" value="">
							<input type="hidden" id="jenistiket" name="jenistiket" value="0">
							<input type="hidden" id="jeniscompliment" name="jeniscompliment" value="">
							<input type="hidden" id="rupdisc" name="rupdisc" value="">
							<input type="hidden" id="discount_bayar" name="discount_bayar" value=0>
							<input type="hidden" id="id_kurs" name="id_kurs" value=0>
							<input type="hidden" id="id_valasasli" name="id_valasasli" value="">
							</td>
						</tr>
                    </table>

                </div>
            <!--</div>-->

            <table class="table responsive">
                <tbody>
                <tr class="info">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h3>Total Bayar </h3></td>
                    <td class="left">
                        <h3><span id='total_bayar' name='total_bayar' onKeyUp="SetKembali()"></span></h3>
                        <input type='hidden' id='total_bayar_hide' name='total_bayar_hide' size="10" value=0>
                    </td>
                </tr>
                <tr class="info">
                    <td width="5%">&nbsp;</td>
                    <td class="left"><h3>Kembali </h3></td>
                    <td class="left"><h3><span id='cash_kembali' name='cash_kembali'> </span></h3></td>
                </tr>
                </tbody>
            </table>

            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="table responsive">
                <tr>
                    <td align="center" height="40"><input type='button' class="btn btn-success" value='Complete'
                                                          onClick="cek_form()">
                        <input type='button' value='Delete' class="btn btn-danger" onClick="clear_trans()">
                        <input type='button' value='Cetak Struk Terakhir' class="btn btn-info" onClick="cetakstrukterakhir()">
						<input type='button' value='Show Total' class="btn btn-warning" onClick="showtotal()">
					</td>
                </tr>
            </table>
      	<tr>
    <td valign="top">
                <table width='100%' align='center' class='table table-bordered table-responsive' border="0">
                    <thead>
                    <tr>
  
                            <th width="5%">No</th>
                            <th width="10%">Item</th>
                            <th width="35%">Nama</th>
                            <th width="10%">Qty</th>
                            <th width="15%">Harga</th>
                            <th width="15%">Disc</th>
                            <th width="15%">Netto</th>
                     </tr>
                    </thead>
				</table>
                <table width='100%' align='center' class='table_class_listpos' border="0" name="detail1" id="detail1"> 
                    <tr id="baris1">
                        <td height="23" width="5%">
                            <div id="lfield_sales_temp1"></div>
                        </td>
                        <td width="10%">
                            <div id="lkodebarang1"></div>
                        </td>
                        <td width="35%">
                            <div id="lnamabarang1"></div>
                        </td>
                        <td align="right" width="10%">
                            <div id="ljumlahqty1"></div>
                        </td>
                        <td align="right" width="15%">
                            <div id="ljumlahharga1"></div>
                        </td>
                        <td align="right" width="15%">
                            <div id="ldisc1"></div>
                        </td>
                        <td align="right" width="15%">
                            <div id="ljumlahnetto1"></div>
                        <input type="hidden" size="15" id="pcode1" name="pcode00[]" maxlength="20" value="">
						<input type="hidden" size="8" id="qty1" name="qty00[]" readonly="readonly" value=0>
						<input type="hidden" size="8" id="kdgroupbarang1" name="kdgroupbarang00[]" readonly="readonly" value=0>
			     		<input type="hidden" size="14" id="harga1" name="harga00[]"readonly="readonly" value=0>
						<input type="hidden" size="8" id="disc1" name="disc00[]" readonly="readonly" value=0>
						<input type="hidden" size="14" id="netto1" name="netto00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="pdisc1" name="pdisc00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="komisi1" name="komisi00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="pdisclokal1" name="pdisclokal00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="komisilokal1" name="komisilokal00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="jenisdisc1" name="jenisdisc00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="nilaidisc1" name="nilaidisc00[]" readonly="readonly" value=0>
						<input type="hidden" size="2" id="khusus1" name="khusus00[]" readonly="readonly" value=0>
						</td>
					</tr>
                </table>
    </td>
	</tr>
	 
	 </td>
	</form>
<tr>
    <td colspan="2">
    
        <table width="100%" border="0" align="center" class="huruf">
            <tr>
                <td width="5%">&nbsp;</td>
                <td width="10%"><b>Kassa : </b><?= $NoKassa[0]['id_kassa']."/".$NoKassa[0]['KdStore']; ?></td>
                <td width="5%">&nbsp;</td>
                <td width="15%"><b>Kasir : </b><?= $session_name ?></td>
                <td width="5%">&nbsp;</td>
                <td width="50%"><b>Id Struk : </b><?= $no ?></td>  <!--//wscrev-->
                <td width="5%">&nbsp;</td>
            </tr>
        </table>
   
    </td>
</tr>
</table>
</div>

</body>
<div style="display: none;">
<?php
$this->load->view('footer');
?>
</div>
<script>
    function EditRecord(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {
//alert("oke");
            //alert(document.getElementById('NoUrut').value *1);
           // alert(document.getElementById('noLast').value *1);
            NoUrut = document.getElementById('NoUrut').value * 1;
//            noLast = document.getElementById('noLast').value;
            var nil;
            nil = NoUrut * 1;
            if (isNaN(nil)) {
                alert("Format No Urut salah, Harap masukkan angka");
            }
//            else if (nil >= noLast) {
//                alert("Data tidak ditemukan, data terakhir adalah " + (noLast - 1));
//            }
            else {

//                kode = document.getElementById('pc' + NoUrut).value;
//alert(kode);

                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/pos/EditRecord/' + NoUrut + '/',
                    success: function (msg) {
                        dt = msg.split("||");
                        //alert(dt[0]);
                        document.getElementById('NoUrut').value = NoUrut;
                        document.getElementById('kdbrg1').value = dt[0];
                        document.getElementById('nmbrg1').value = dt[1];
                        document.getElementById('qty1').value = dt[3];
                        document.getElementById('jualm1').value = number_format(dt[3], 0, ',', '.');
                        document.getElementById('netto1').value = number_format(dt[4], 0, ',', '.');
                        document.getElementById('jualmtanpaformat1').value = dt[2];
                        document.getElementById('nettotanpaformat1').value = dt[4];

//                        var jsdata = msg;
//                        eval(jsdata);
//                        alert(datajson[0].pcode)
//                        document.getElementById('kdbrg1').value = datajson[0].KodeBarang;
//                        document.getElementById('nmbrg1').value = datajson[0].NamaStruk;
//                        document.getElementById('qty1').value = datajson[0].Qty;
//                        document.getElementById('jualm1').value = number_format(datajson[0].Harga, 0, ',', '.');
//                        document.getElementById('netto1').value = number_format(datajson[0].Netto, 0, ',', '.');
//                        document.getElementById('jualmtanpaformat1').value = datajson[0].Harga;
//                        document.getElementById('nettotanpaformat1').value = datajson[0].Netto;

                        document.getElementById('NoUrut').readOnly = true;
                        document.getElementById('kdbrg1').readOnly = true;
                        document.getElementById('qty1').readOnly = false;
                        document.getElementById('qty1').focus();
                        document.getElementById('EditFlg').value = 1;
                    }
                });
            }
        }
    }

    function CustomerView(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }
        if (code == 13) {
            pelanggan = document.getElementById('pelanggan').value;
            if (pelanggan !== "") {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/pos/CustomerView/' + pelanggan + '/',
                    success: function (msg) {
                        var jsdata = msg;
                        eval(jsdata);
                        document.getElementById('id_customer').value = datajson[0].KdCustomer;
                        document.getElementById('nama_customer').value = datajson[0].Nama;
                        document.getElementById('gender_customer').value = datajson[0].Gender;
                        document.getElementById('tgl_customer').value = datajson[0].TglLahir;
                        document.getElementById('cash_bayar').focus();
                    }
                });
            }
            else {
                alert("masukkan nomor Id Customer");
            }
        }
    }

	function VoucherCustomer(e, row, url) {
		totalbiaya = parseFloat($("#total_biaya").val());
		if(totalbiaya==0)
		{
		   alert("Total Belanja masih NOL");	
		}else
		{
		url = url+"index.php/pop/voucher/index/";
		window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
	}
	
	function ComplimentCustomer(e, row, url) {
		totalitem = parseFloat($("#TotalItem2").val());
		//alert(totalitem);
		if(totalitem==0)
		{
		   alert("Total Belanja masih NOL");	
		}else
		{
		url = url+"index.php/pop/compliment/index/";
		window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
	}
	
	
	
	function DiscountGroup(e, row, url) {
		totalitem = parseFloat($("#TotalItem2").val());
		//alert(totalitem);
		if(totalitem==0)
		{
		   alert("Total Belanja masih NOL");	
		}else
		{
		url = url+"index.php/pop/discount_group_khusus/index/";
		window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
	}
	
	function InputDiscount(e, row, url) {
		if(window.event)
		{
			var code = e.keyCode;
		}
		else if(e.which)
		{
			var code = e.which;
		}
		if (code == 13)
		{
			id_discount = parseFloat(document.getElementById('id_discount').value);
			if(id_discount!=0)
			{
				totalitem = parseFloat($("#TotalItem2").val());
				//alert(totalitem);
				if(totalitem==0)
				{
				   alert("Total Belanja masih NOL");	
				}else
				{
				url = url+"index.php/pop/discount/index/";
				window.open(url,'popuppage','width=750,height=500,top=100,left=150');
				}
			}else
			{
				document.getElementById('iddiscountasli').value = id_discount;
				HitungNetto();
			}
		}
	}
	
    function Voucher1Customer(e, row, url) {
        if (window.event) {
            var code = e.keyCode;
        }
        else if (e.which) {
            var code = e.which;
        }

        if (code == 13) {
            id_voucher = document.getElementById('id_voucher').value;
            if (id_voucher !== "") {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url(); ?>index.php/transaksi/pos/VoucherCustomer/' + id_voucher + '/',
                    success: function (msg) {
						if(msg=="salah")
						{
							document.getElementById('id_voucher').value = "";
							document.getElementById('keterangan').value = "";
							document.getElementById('jenisvoucher').value = "";
							alert("Kode voucher salah!");
						}
							else
						{
                        var jsdata = msg;
                        eval(jsdata);
                        document.getElementById('id_voucher').value = datajson[0].novoucher;
                        document.getElementById('voucher_bayar').value = datajson[0].nominal;
						document.getElementById('keterangan').value = datajson[0].keterangan;
						document.getElementById('jenisvoucher').value = datajson[0].jenis;
						document.getElementById('rupdisc').value = datajson[0].rupdisc;
                        document.getElementById('voucher_bayar').disabled = false;
						
						if(datajson[0].jenis=='3')
						{
						   	var lastRow = document.getElementsByName("pcode00[]").length;
							totalnetto = 0;
							totaldisc = 0;
							var listpcode="";
							var listqty="";
							var listharga="";
							var listdisc="";
							var listnetto="";
							var listpdisc="";
							var listkomisi="";
							var listpdisclokal="";
							var listkomisilokal="";
							for(index=1;index<=lastRow;index++)
							{
								indexs = index; 
								qty = parseFloat($("#qty" + indexs).val());
								harga = parseFloat($("#harga" + indexs).val());
								pdisc = parseFloat($("#pdisc" + indexs).val());
								disc = pdisc/100*(qty*harga);
								netto = (qty*harga)-disc;
								//alert(qty);
								//alert(harga);
								//alert(disc);
								//alert(netto);
								$("#netto" + indexs).val(netto);
								$("#disc" + indexs).val(disc);
								$("#ldisc" + indexs).val(disc);
								$("#ljumlahnetto" + indexs).val(netto);	
								document.getElementById('ldisc'+indexs).innerHTML = disc;
								document.getElementById('ljumlahnetto'+indexs).innerHTML = netto;
								totalnetto = totalnetto + netto;
								totaldisc = totaldisc + disc;
								
								listpcode = listpcode + $("#pcode" + index).val() + "##";
								listqty = listqty + $("#qty" + index).val() + "##";
								listharga = listharga + $("#harga" + index).val() + "##";
								listdisc = listdisc + $("#disc" + index).val() + "##";
								listnetto = listnetto + $("#netto" + index).val() + "##";
								listpdisc = listpdisc + $("#pdisc" + index).val() + "##";
								listkomisi = listkomisi + $("#komisi" + index).val() + "##";
								listpdisclokal = listpdisclokal + $("#pdisclokal" + index).val() + "##";
								listkomisilokal = listkomisilokal + $("#komisilokal" + index).val() + "##";
							}
							$("#listpcode").val(listpcode);
							$("#listqty").val(listqty);
							$("#listharga").val(listharga);
							$("#listdisc").val(listdisc);
							$("#listnetto").val(listnetto);
							$("#listpdisc").val(listpdisc);
							$("#listkomisi").val(listkomisi);
							$("#listpdisclokal").val(listpdisclokal);
							$("#listkomisilokal").val(listkomisilokal);
							$("#discount_bayar").val(totaldisc);
							tax = Math.floor(totalnetto / 11);
							dpp = totalnetto - tax;
							//ppn = Math.floor(netto*0.1);
							//total = Math.floor(totalnetto);
							total = Math.round(totalnetto);
                            total_ = Math.round(totalnetto / 100) * 100;
							//total_ = Math.round(netto / 100) * 100;
                            if (total_ >= total){
                                pembulatan = total_ - total;
                            }else if(total_ <= total){
                                pembulatan = total - total_;
                            }
							document.getElementById('TotalItem').innerHTML = number_format(lastRow,0,',','.');// TotalItem in cart
							document.getElementById('TotalItem2').value = (lastRow) ;// TotalItem in cart
							document.getElementById('dpp').value = Math.floor(dpp)  ;// dpp
							document.getElementById('tax').value =  Math.floor(tax) ;// tax
							document.getElementById('rounding').innerHTML = number_format(pembulatan,0,',','.'); //  total rounding
                            document.getElementById('ttlall').innerHTML = number_format(total,0,',','.'); //  total setelah service charge
							document.getElementById('TotalNetto').innerHTML = number_format(total,0,',','.');
							document.getElementById('service_charge').value = 0;// TotalItem in cart
							//document.getElementById('Charge').innerHTML = number_format(0 ,0,',','.');;// TotalItem service charge
							document.getElementById('total_biaya').value = total_  ;
                            document.getElementById('rounding_hide').value = pembulatan  ;
						}
                        SetKembali();
						}
                    }
                });
            }
            else {
				document.getElementById('idvoucher').value = "";
				document.getElementById('keterangan').value = "";
				document.getElementById('jenisvoucher').value = "";
                alert("masukkan nomor voucher");
            }
        }

    }
	
		function showtotal(){
			document.getElementById('ttlall').value;
			var satu = "Total :";
			var dua = $('#ttlall').html();

			// alert(dua);

			toLCD2(satu,dua);
		}
    
    function getDiscCard() {
    	            NamaCard  = $("#nm_card").val();
    	            KdStore  = $("#v_kdstore").val();
					//ambil nilai diskon kartu sesuai dengan jenis KdGroupBarang
					if(NamaCard!=""){
						$.ajax({
				            url: "<?php echo site_url('transaksi/pos/carddisc') ?>/" + NamaCard+"/"+KdStore,
				            type: "GET",
				            dataType: "JSON",
				            success: function(res)
				            {
				            	$("#persentase_card").val(res.persen);
							}
		       			 });	 		       			 	 
		       		}
    }
	
	function HitungNetto() {
		
			    jenisvoucher  = $("#jenisvoucher").val();
				jenistiket  = $("#jenistiket").val();
				pilihan  = $("#pilihan").val();
				id_compliment  = $("#id_compliment").val();
				NamaCard  = $("#nm_card").val();
				id_members  = $("#id_members").val();
				KdStore  = $("#v_kdstore").val();
				
				iduserdiscount = parseFloat($("#iduserdiscount").val());
				//alert(pilihan);
				var lastRow = document.getElementsByName("pcode00[]").length;
				totalnetto = 0;
				totaldisc = 0;
				discx=0;
				var listpcode="";
				var listqty="";
				var listharga="";
				var listdisc="";
				var listnetto="";
				var listpdisc="";
				var listkomisi="";
				var listpdisclokal="";
				var listkomisilokal="";
				var listjenisdisc="";
				var listnilaidisc="";
				var listkhusus="";
				for(index=1;index<=lastRow;index++)
				{
					indexs = index; 
					pcode = $("#pcode" + indexs).val();
					persen_ketentuan_diskon = parseInt($("#nilaidisc" + indexs).val());
					
					if(NamaCard==""){
					id_discount = parseFloat($("#id_discount").val());	
					}else if(id_members!=""){
						id_discount = parseFloat($("#id_discount").val());
					}else{
						id_discount = 0;
					}
				
					//----------
					if(NamaCard!=""){	
					    kdgroupbarang = $("#kdgroupbarang" + indexs).val();						
				        $.ajax({
				            url: "<?php echo site_url('transaksi/pos/carddisc') ?>/" + NamaCard +"/"+ KdStore +"/"+ kdgroupbarang,
				            type: "GET",
				            dataType: "JSON",
				            success: function(res)
				            {
				            	if(persen_ketentuan_diskon>parseInt(res.PersenDisc)){
				            		id_discount = persen_ketentuan_diskon;
				            		max_discount = 0;	
				            	}else{
				            		id_discount = res.PersenDisc;
				            		max_discount = res.MaxDiscount;	
				            	}
				            }, 
							async: false
				        });
					}	
					//========
					
					
					if(id_members!=""){
						if(persen_ketentuan_diskon>id_discount){
				            		id_discount = persen_ketentuan_diskon;
				        }else{
				            		id_discount = id_discount;	
				        }
					
				    }
					//alert(id_discount);
					
		       		qty = parseFloat($("#qty" + indexs).val());
					harga = parseFloat($("#harga" + indexs).val());
					pdisc = parseFloat($("#pdisc" + indexs).val());
					pdisclokal = parseFloat($("#pdisclokal" + indexs).val());
					komisilokal = parseFloat($("#komisilokal" + indexs).val());
					
					jenisdisc = $("#jenisdisc" + indexs).val();
					nilaidisc = parseFloat($("#nilaidisc" + indexs).val());
					khusus = $("#khusus" + indexs).val();
					disc = 0;
					//alert(jenisdisc);
					if((pilihan=='compliment')&&(id_compliment!='')){
						disc = qty*harga;
					}else{
						if(jenisvoucher=='3')
						{
						   disc = pdisc/100*(qty*harga);
						}
						else if(jenistiket=='1')
						{
							disc = pdisclokal/100*(qty*harga);
							$("#komisi" + index).val(komisilokal);
						}
						if(jenisdisc=="P") //discount promo
						{
							disc = nilaidisc/100*(qty*harga);
							if((jenistiket=="1")&&(pdisclokal>nilaidisc))
							{
							   disc = pdisclokal/100*(qty*harga);
							}
							if((jenisvoucher=='3')&&(pdisc>nilaidisc))
							{
							   disc = pdisc/100*(qty*harga);
							}
						}
						
						//tonny
						if(jenisdisc=="X" && $("#iduserdiscount").val()=="DISCKHUSUS") //discount khusus
						{
							disc = nilaidisc/100*(qty*harga);
							if((jenistiket=="1")&&(pdisclokal>nilaidisc))
							{
							   disc = pdisclokal/100*(qty*harga);
							}
							if((jenisvoucher=='3')&&(pdisc>nilaidisc))
							{
							   disc = pdisc/100*(qty*harga);
							}
						}
						
					}
					if((id_discount>0)&&(iduserdiscount!=''))
					{
						 discid = id_discount/100*(qty*harga);
								
								if(discid>disc)
								{
									disc = discid;
								}
						
					if(NamaCard=="3"){
						if((disc+totaldisc)>parseInt(max_discount)){
							disc=parseInt(max_discount)-totaldisc;
						}else{
							disc=disc;
						}
					}
					
					}
					
					//TAMBAHAN FKY
					//if(($("#iduserdiscount").val()=="DISCKHUSUS") && (khusus=="no")){
					//	disc = 0;
					//}
					
					disc = Math.round(disc);
					netto = (qty*harga)-disc;
					//alert(qty);
					//alert(harga);
					//alert(disc);
					//alert(netto);
					$("#netto" + indexs).val(netto);
					$("#disc" + indexs).val(disc);
					$("#ldisc" + indexs).val(disc);
					$("#ljumlahnetto" + indexs).val(netto);	
					document.getElementById('ldisc'+indexs).innerHTML = disc;
					document.getElementById('ljumlahnetto'+indexs).innerHTML = netto;
					totalnetto = totalnetto + netto;
					totaldisc = totaldisc + disc;
					
					listpcode = listpcode + $("#pcode" + index).val() + "##";
					listqty = listqty + $("#qty" + index).val() + "##";
					listharga = listharga + $("#harga" + index).val() + "##";
					listdisc = listdisc + $("#disc" + index).val() + "##";
					listnetto = listnetto + $("#netto" + index).val() + "##";
					listpdisc = listpdisc + $("#pdisc" + index).val() + "##";
					listkomisi = listkomisi + $("#komisi" + index).val() + "##";
					listpdisclokal = listpdisclokal + $("#pdisclokal" + index).val() + "##";
					listkomisilokal = listkomisilokal + $("#komisilokal" + index).val() + "##";
					
					
				}
				
				$("#tot_diskon").val(totaldisc);
				
				$("#listpcode").val(listpcode);
				$("#listqty").val(listqty);
				$("#listharga").val(listharga);
				$("#listdisc").val(listdisc);
				$("#listnetto").val(listnetto);
				$("#listpdisc").val(listpdisc);
				$("#listkomisi").val(listkomisi);
				$("#listpdisclokal").val(listpdisclokal);
				$("#listkomisilokal").val(listkomisilokal);
				$("#discount_bayar").val(totaldisc);
				tax = Math.floor(totalnetto / 11);
				dpp = totalnetto - tax;
				//ppn = Math.floor(netto*0.1);
				//total = Math.floor(totalnetto);
                //total_ = Math.round(netto / 100) * 100;
				
				total = Math.round(totalnetto);
                total_ = Math.round(totalnetto / 100) * 100;
                if (total_ >= total){
                    pembulatan = total_ - total;
                }else if(total_ <= total){
                    pembulatan = total - total_;
                }
				document.getElementById('TotalItem').innerHTML = number_format(lastRow,0,',','.');// TotalItem in cart
				document.getElementById('TotalItem2').value = (lastRow) ;// TotalItem in cart
				document.getElementById('dpp').value = Math.floor(dpp)  ;// dpp
				document.getElementById('tax').value =  Math.floor(tax) ;// tax
                document.getElementById('rounding').innerHTML = number_format(pembulatan,0,',','.'); //  total rounding
				document.getElementById('ttlall').innerHTML = number_format(total_,0,',','.'); //  total setelah service charge
				document.getElementById('TotalNetto').innerHTML = number_format(total,0,',','.');
				document.getElementById('service_charge').value = 0;// TotalItem in cart
				//document.getElementById('Charge').innerHTML = number_format(0 ,0,',','.');;// TotalItem service charge
				document.getElementById('total_biaya').value = total_  ;
                document.getElementById('rounding_hide').value = pembulatan  ;
				
			SetKembali();
			//toLCD2('TOTAL',  'Rp ' . total);
			/*
			kirim = "Total Sales~-~"+total;
			$.ajax({
				type: "POST",
				url: "<?= base_url(); ?>index.php/transaksi/pos/displaycust/"+kirim,
				success: function(msg){
				}
			});
			*/
    }
	

    function cek_form() {
    	
    			nostiker = $("#kdagent").val();
    			gsa = $("#gsa").val();
    	        if (nostiker=="")
				{
					alert("No Stiker Belum Diisi. Minimal - ( strip satu kali)");
					document.getElementById('kdagent').focus();
				    return false;
				}
				else if (gsa=="")
				{
					alert("Warna GSA Belum Dipilih.");
					document.getElementById('gsa').focus();
					return false;
				}

				//cek kartu dan EDC nya
				bankkredit = $("#id_kredit").val();
				bankkreditedc = $("#id_kredit_edc").val();
				bankdebet = $("#id_debet").val();
				bankdebetedc = $("#id_debet_edc").val();
				kredit_bayar = $("#kredit_bayar").val();
				debet_bayar = $("#debet_bayar").val();
				
				if(parseFloat(kredit_bayar)>0){
					if(bankkredit=="" || bankkreditedc==""){
						alert("mohon pilih jenis kartu bank dan Mesin EDC nya pada tipe Payment Kredit");
						return false;
					}
				}

				if(parseFloat(debet_bayar)>0){
					if(bankdebet=="" || bankdebetedc==""){
					alert("mohon pilih jenis kartu bank dan Mesin EDC nya pada tipe Payment Debit");
							return false;
					}
				}

    	
        total_bayar = parseFloat(document.getElementById('total_bayar_hide').value);
		total_biaya = parseFloat(document.getElementById('total_biaya').value);
		pilihan  = $("#pilihan").val();
		id_compliment  = $("#id_compliment").val();
		iddiscountasli = parseFloat(document.getElementById('iddiscountasli').value);
		id_discount = parseFloat(document.getElementById('id_discount').value);
		id_valas = document.getElementById('id_valas').value;
		id_valasasli = document.getElementById('id_valasasli').value;
		id_kurs = parseFloat(document.getElementById('id_kurs').value);
		valas_bayar = parseFloat(document.getElementById('valas_bayar').value);
		if(isNaN(valas_bayar))
		  valas_bayar=0;
		if(id_discount!=iddiscountasli)
		{
			alert("Discount perlu di validasi ulang ");
		}else
		{
			if((valas_bayar!=0)&&((id_valas!=id_valasasli)||(id_kurs<=1)))
			{
				alert("Kode Valas perlu di validasi ulang")
			}
			else
			{
				if (((total_bayar == "")||(total_bayar == 0))&&((pilihan!='compliment')||(id_compliment=='')))
				{
					alert("Transaksi belum dibayar");
					document.getElementById('cash_bayar').focus();
				}
				else {
					if (total_biaya > total_bayar) {
						alert("Total bayar masih kurang");
						document.getElementById('cash_bayar').focus();
					}
					else {
						no = document.getElementById('no').value;
						document.form1.submit();
					}
				}
			}
		}
    }
    
    function toLCD(varsatu, vardua, vartiga) {
		var url	= 'http://<?=$iplokal?>/displaylcd/getlcd.php?varsatu='+varsatu+'&vardua='+vardua+'&vartiga='+vartiga+'&callback=?';	
		$.ajax({
			type: 'GET',
			url: url,
			async: false,
			jsonpCallback: 'jsonCallback',
			contentType: "application/json",
			dataType: 'jsonp'
		});
	}
	
	function toLCD2(varsatu, vardua) {
		
		var url	= 'http://<?=$iplokal?>/displaylcd/getlcd2.php?varsatu='+varsatu+'&vardua='+vardua+'&callback=?';	
		$.ajax({
			type: 'GET',
			url: url,
			async: false,
			jsonpCallback: 'jsonCallback',
			contentType: "application/json",
			dataType: 'jsonp'
		});
	}	

	function jsonCallback(dataarray) {
		//return messege nya dikirim ke console firebug 
		console.log(dataarray.pesan[0].msg);
	}
	
    function disabledMarketPlace(){
        $("#KdMarketPlace").attr('disabled','disabled');
    }
	
</script>







