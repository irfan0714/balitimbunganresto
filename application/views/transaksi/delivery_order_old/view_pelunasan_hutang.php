<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Pelunasan Hutang";

//$arr_data(unset);

?>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>View <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_invoice/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td>
	            	<input type="text" value="<?=$data['NoTransaksi']; ?>" class="form-control-new" name="notransaksi" id="notransaksi" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?=$data['TglDokumen']; ?>" name="tgldokumen" id="tgldokumen" readonly size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['NamaKasBank']; ?>" class="form-control-new" name="namabank" id="namabank" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Mata Uang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['MataUang']; ?>" class="form-control-new" name="matauang" id="matauang" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Kurs <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['Kurs']; ?>" class="form-control-new" name="kurs" id="kurs" size="15" maxlength="15"  style="text-align: right;">
	            </td>
	            <td class="title_table" width="150">No Bukti <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NoBukti']; ?>" class="form-control-new" name="NoBukti" id="NoBukti" size="30" maxlength="25">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Supplier <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NamaSupplier']; ?>" class="form-control-new" name="namasupplier" id="namasupplier" size="30" maxlength="25">
	            </td>
	            <td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['Keterangan']; ?>" class="form-control-new" name="keterangan" id="keterangan" size="75" maxlength="100"  style="text-align: left;">
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
								<th width="50"><center>No Faktur</center></th>
								<th><center>No Faktur Supplier</center></th>               
								<th width="100"><center>No PO</center></th>
								<th width="100"><center>Tanggal</center></th>
								<th width="100"><center>Nilai</center></th>
								<th width="100"><center>Bayar</center></th>
						    </tr>
						</thead>
						<tbody>
							
							<?php 
							$subtotal = 0;
							$ppn = 0;
							for($a=0 ; $a<count($row) ; $a++) {
							?>
							<tr id="baris<?php echo $a; ?>">
						        <td>
					            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="NoFakturSupplier[]" id="NoFakturSupplier<?php echo $a;?>" value='<?=$row[$a]['NoFakturSupplier'];?>' readonly style="width: 100%;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="NoPO[]" id="NoPO<?php echo $a;?>" value='<?=$row[$a]['NoPO'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" VALUE="<?=number_format($row[$a]['NilaiFaktur'], 0, ',', '.');?>" readonly style="text-align: right;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>" VALUE="<?=number_format($row[$a]['NilaiBayar'], 0, ',', '.');?>" style="text-align: right;">
					            </td>
						    </tr>
						    <?php
						    }
						    ?>
						</tbody>
					</table>
	        	</td>
	        </tr>
	    </table>
	    
	    </form> 
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
		</ol>
			
        <table class="table table-bordered responsive">
            <tr>
            	<td class="title_table" width="150">Author</td>
	            <td><?php echo $data['AddUser']." :: ".$data['AddDate']; ?></td>
            </tr>
            <tr>
            	<td class="title_table" width="150">Edited</td>
	            <td><?php echo $data['EditUser']." :: ".$data['EditDate']; ?></td>
            </tr>
         </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
