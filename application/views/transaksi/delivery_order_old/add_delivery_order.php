<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Delivery Order";

//$arr_data(unset);

?>

<script>
	function pickThis()
	{
	
		url = "<?=base_url()?>"+"index.php/transaksi/delivery_order/popupbarang";
		windowOpener(500, 700, 'Data Produksi', url, 'Data Produksi');
	}	

    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
    
    function submit(){
		$("#theform").submit();
	}
    
    function getDetail(){
    	var KdSupplier = $('#KdSupplier').val();
    	var MataUang = $('#kd_uang').val();
    	var vdata = new Array(2);
    	vdata[0] = KdSupplier;
    	vdata[1] = MataUang;
    	var jsonString = JSON.stringify(vdata);
    	$.ajax({
	        type: "POST",
	        url: "<?=base_url();?>index.php/transaksi/pelunasan_hutang/getDetail/",
	        data: {data: vdata},
	        success: function(data) {
				$('#UpdateDetail').html(data);
			}
	    });
	}
	
	function formatdec(nilai){
		var MataUang = $('#kd_uang').val();
		if(MataUang=='IDR'){
			result = toFormat(nilai);
		}else{
			result = toFormat4(nilai);	
		}
		return result;
	}

</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/delivery_order/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No DO</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="tgldokumen" id="tgldokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?=form_dropdownDB_initJS('KdGudang', $warehouse, 'KdGudang', 'Keterangan', '', '', '--------- Pilih ---------', 'id="KdGudang" class="widthonehundred");"');?>
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Pelanggan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?=form_dropdownDB_initJS('KdCustomer', $customer, 'KdCustomer', 'Nama', '', '', '--------- Pilih ---------', 'id="KdCustomer" class="widthonehundred");"');?>
	            </td>
	            
	        </tr>       
	        <tr>
	            <td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" name="keterangan" id="keterangan" size="75" maxlength="100"  style="text-align: left;">
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
								<th width="50"><center>PCode</center></th>
								<th><center>Nama Barang</center></th>               
								<th width="100"><center>Referensi</center></th>
								<th width="100"><center>Stok</center></th>
								<th width="100"><center>Qty</center></th>
								<th width="100"><center>Satuan</center></th>
								<th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
						</thead>
						<tbody>
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="left">
				                	<input type="text" class="form-control-new" name="v_noref[]" id="v_noref<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_stock[]" onblur="toFormat2('v_stock<?php echo $no;?>')" id="v_stock<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="toFormat2('v_qty<?php echo $no;?>')" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/pelunasan_hutang/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onclick="submit();" value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
	new Spry.Widget.ValidationSelect("KdSupplier");
	new Spry.Widget.ValidationTextField("tgldokumen", "date", {format:"dd-mm-yyyy", useCharacterMasking:true});
	new Spry.Widget.ValidationTextField("kurs", "real", {useCharacterMasking:true});
</script>
