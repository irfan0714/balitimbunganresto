<link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
<link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
<link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/css/paging5.css"/>

<script language="javascript" src="<?= base_url(); ?>/public/js/jquery.js"></script>
<br>
<form method="POST"  name="search" action="">
    <table align='center'>
        <tr>
            <td><input type='text' size='50' name='stSearchingKey' id='stSearchingKey'></td>
            <td>
                <select size="1" height="1" name ="searchby" id ="searchby">
                    <option value="NamaLengkap">Nama Barang</option>
                    <option value="PCode">Kode Barang</option>
                </select>
            </td>
            <td>&nbsp; <input type="submit" value="Search (*)"></td>
        </tr>
    </table>
</form>

<br>

<table align='center' border='1' class='table_class_list' width="700">
	<table class="table table-bordered responsive">
    	<thead class="title_table">
        	<tr>
	        	<th width="10"><center> </center></th>
	            <th width="10%"><center>PCode</center></th>
		        <th width="30%"><center>Nama Barang</center></th>
		        <th	width="20%"><center>No Batch</center></th>
		        <th width="20%"><center>Tgl Produksi</center></th>
		        <th width="15%"><center>Qty</center></th>
	    	</tr>
    	</thead>
    	<tbody>
	    	<tr>
	    		<?php
	        	for($i=0;$i<count($detail);$i++){
	        	?>
					<td nowrap>
						<a href="" onclick="getCode('<?=$i;?>')">
							<img src="<?=base_url();?>/public/images/pick.png" border="0" alt="Select" Title="Pilih">
						</a>
					</td>
					<td nowrap><?=$detail[$i]['PCode'];?></td>
					<td nowrap><?=$detail[$i]['NamaLengkap'];?></td>
					<td nowrap><?=$detail[$i]['BatchNumber'];?></td>
					<td nowrap><?=$detail[$i]['ProductionDate'];?></td>
					<td align="right" nowrap><?=$detail[$i]['RemainQuantity'];?></td>
				<?php
				}
	        	?>
	    	</tr>
	    </tbody>
    </table>
</table>
<table align = 'center'>
     <tr><td><input type="button" value="Close" onclick = "closing()"></td></tr>
</table>

<script language="javascript">
    function closing()
    {
        window.close();
    }
</script>