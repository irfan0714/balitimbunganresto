<table class="table table-bordered responsive" id="TabelDetail">
	<thead class="title_table">
		<tr>
			<th width="50"><center>No Faktur</center></th>
			<th><center>No Faktur Supplier</center></th>               
			<th width="100"><center>No PO</center></th>
			<th width="100"><center>Tanggal</center></th>
			<th width="100"><center>Nilai</center></th>
			<th width="100"><center>Bayar</center></th>
	    </tr>
	</thead>
	<tbody>
		
		<?php 
		$subtotal = 0;
		$ppn = 0;
		for($a=0 ; $a<$num ; $a++) {
		?>
		<tr id="baris<?php echo $a; ?>">
	        <td>
            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoFakturSupplier[]" id="NoFakturSupplier<?php echo $a;?>" value='<?=$row[$a]['NoFakturSupplier'];?>' readonly style="width: 100%;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoPO[]" id="NoPO<?php echo $a;?>" value='<?=$row[$a]['NoPO'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" VALUE="<?=number_format($row[$a]['Sisa'], 0, ',', '.');?>" readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>" onblur="formatdec('Bayar<?php echo $a;?>')" style="text-align: right;">
            </td>
	    </tr>
	    <?php
	    }
	    ?>
	</tbody>
</table>