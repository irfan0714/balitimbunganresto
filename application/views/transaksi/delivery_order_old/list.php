<?php $this->load->view('header'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>

<script>
$(document).ready(function() {
	$('#tglmulai').datepicker();
	$('#tglakhir').datepicker();
});
	
</script>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/delivery_order/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="50" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;
            <b>Tanggal</b>&nbsp;
            <input type="text" size="10" maxlength="10" name="tglmulai" id="tglmulai" class="form-control-new datepicker""/>&nbsp;
            s/d&nbsp;
            <input type="text" size="10" maxlength="10" name="tglakhir" id="tglakhir" class="form-control-new datepicker""/>&nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/delivery_order/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="75"><center>No DO</center></th>
		        <th width="75"><center>Tanggal</center></th>
		        <th width="75"><center>Gudang</center></th>
		        <th width="150"><center>Pelanggan</center></th>
		        <th width="150"><center>Note</center></th>
		        <th width="50"><center>Navigasi</center></th>
        	</tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                ?>
                <tr title="<?php echo $val["DONo"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td align="left"><?php echo $val["DONo"]; ?></td>
                    <td align="left"><?php echo $val["DoDate"]; ?></td>
                    <td align="center"><?php echo $val["WarehouseCode"]; ?></td>
                    <td align="left"><?php echo $val["Nama"]; ?></td>
                    <td align="left"><?php echo $val["Note"]; ?></td>
                    <td align="center">

                        <a href="<?php echo base_url(); ?>index.php/transaksi/delivery_order/view/<?php echo $val["DoNo"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                        
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
