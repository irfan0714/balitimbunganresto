<?php 

$this->load->view('header'); 

$modul = "Purchase Order";

?>

<script language="javascript" src="<?=base_url();?>public/js/purchase_order_v4.js"></script>
<script src="<?= base_url();?>public/js/js.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_order/save_data'>
		
	    <table class="table table-bordered responsive">                        

	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Supplier <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;" onchange="cari_top()">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
							?><option value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">TOP ( Hari ) <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_top" id="v_top" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. PR <font color="red"><b>(*)</b></font></td>
	            <td>
	            <input readonly type="text" class="form-control-new" value="" name="v_nopr" id="v_nopr" maxlength="255" style="width: 180px;" onblur="detail()">
	            <a href="javascript:void(0)" id="get_pr" onclick="pickThisNew(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PR" title=""><i class="entypo-search"></i></a>		
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Untuk Divisi <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_namadivisi" id="v_namadivisi" maxlength="255" style="width: 200px;" readonly="">
	            	<input type="hidden" class="form-control-new" value="" name="v_kddivisi" id="v_kddivisi" maxlength="255" style="width: 200px;" readonly="">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Currency.</td>
                <td>
                	<select name="v_currencycode" id="v_currencycode" class="form-control-new" style="width: 200px;" onchange="tampil_kurs()">  
                        	<option value="">Pilih Mata Uang</option>
		            		<?php
		            		foreach($currency as $val)
		            		{
								?><option value="<?php echo $val["Kd_Uang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
							}
		            		?>
	            	</select>
	            	<span id="td_kurs" style="display: none;">
	            		<input style="text-align: right;" type="text" class="form-control-new" placeholder="Input Kurs" value="" name="kurs" id="kurs"/>
	            	</span>
	            	
                </td>  
	        </tr>
	        
	        <!--<tr>
	            <td class="title_table">Contact Person <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_contactperson" id="v_contactperson" maxlength="255" size="100"></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Estimasi Terima <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_terima" id="v_tgl_terima" size="10" maxlength="10">
	            </td>
	        </tr>
           	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
					
					</table>
				</td>
			</tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

	function CopyQtyPR(obj){
        objek = obj.id;
		id = objek.substr(9,objek.length-9);
        QtyPR=  $("#v_qty_request_hidden_" + id).val();
        
		$("#v_Qty_dec"+id).val(QtyPR);
		console.log(QtyPR);
	}

	function cek(id,harga){

		var harga_dec = formatRupiahAngka(harga);
		$("#v_Harga_dec"+id).val(harga_dec);
		$('#modal-history1').modal('hide');
		hitungSubtotal(id);
	}

	function ShowHistory(pcode, id){
		console.log('pcode ',pcode,'id ',id);

		$.ajax({
			type: 'ajax',
			method: 'post',
			url: '<?php echo base_url() ?>index.php/transaksi/purchase_order/GetHistoryBeli',
			data: {pcode: pcode},
			async: false,
			dataType: 'json',
			success: function(data1){

				var html2 = '';
			
				var i;
				var no =1;
				for(i=0; i<data1.length; i++){
					
					var harga = parseInt(data1[i].Harga).toFixed(2);
					html2 +='<tr>'+
							'<td>'+no+'</td>'+
							'<td>'+data1[i].Tanggal+'</td>'+
							'<td>'+data1[i].Nama+'</td>'+
							'<td>'+data1[i].PCode+'</td>'+
							'<td>'+data1[i].NamaLengkap+'</td>'+
							'<td>'+data1[i].Satuan+'</td>'+
							'<td>'+formatRupiahAngka(harga)+'</td>'+
							"<td>"+"<button onclick='cek(\""+id+"\",\""+harga+"\")' class='btn btn-primary btn-xs'>Select</button></td>"+
							'</tr>';
					no++;

				}
				$('#isipembelian').html(html2);
			
			},
			error: function(){
				alert('Could not Show History');
			}
		});
  

		$('#modal-history1').modal('show');
		// alert(pcode);
		$('#riwayatbeli').DataTable();
	}
	
	function tampil_kurs(){
		var v_currencycode = $('#v_currencycode').val();
		if(v_currencycode=="USD"){
			$('#td_kurs').css("display","");
			$('#kurs').focus();
		}else{
			$('#td_kurs').css("display","none");
		}
	}
	
	function cari_top(){
		var supplier = $('#v_supplier').val();
		var url =$('#base_url').val();
		$.ajax({
					url: url+"index.php/transaksi/po_marketing/top/",
					data: {id:supplier},
					type: "POST",
					dataType: "json",
			        success: function(data)
					{
						$('#v_top').val(data.top);
						
					} 
				});
				
	}
	
	function detail(){
	var nopr=$('#v_nopr').val();
	var supplier=$('#v_supplier').val();
	var url=$('#base_url').val();
	
		$.ajax({
			url : url+"index.php/transaksi/purchase_order/save_detail_temp/"+nopr+"/"+supplier,
			type: "GET",
			dataType: "html",
			success: function(res)
			{
				$('#TabelDetail').html(res);
				
			},
			error: function(e) 
			{
				alert(e);
			}
		});


	}


	/*
	function HitungOnBlurQty(flag,obj) {
		
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(9, objek.length - 9));
				qty = parseFloat(reform($("#v_Qty_dec" + id).val()));
                hrgx = $("#v_Harga_" + id).val();                
                discx = $("#v_Disc_" + id).val();
				potx = $("#v_Potongan_" + id).val();
				
				
				if(hrgx==""){
					hrg=1;
				}else{
					hrg = parseFloat($("#v_Harga_" + id).val());  
				}
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat($("#v_Potongan_" + id).val());
				}
				
				
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                
                $("#v_sJumlah_" + id).val(subtot);
                $("#v_Qty_" + id).val(qty);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }

    
	
	function HitungOnBlur(flag,obj) {
	
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(11, objek.length - 11));
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg = parseFloat(reform($("#v_Harga_dec" + id).val()));                
                discx = $("#v_Disc_" + id).val();
				potx = $("#v_Potongan_" + id).val();
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat($("#v_Potongan_" + id).val());
				}
				
				
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                $("#v_sJumlah_" + id).val(subtot);
                $("#v_Harga_" + id).val(hrg);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
    
    function HitungOnBlur2( flag, obj) {
	
            objek = obj.id;
			
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(7, objek.length - 7));
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg = parseFloat($("#v_Harga_" + id).val());                
                
                discx = $("#v_Disc_" + id).val();
				potx = $("#v_Potongan_" + id).val();
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat($("#v_Potongan_" + id).val());
				}
				
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
    
    
    function HitungOnBlur3( flag, obj) {
	
    	
            objek = obj.id;
			
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(14, objek.length - 14));
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg = parseFloat($("#v_Harga_" + id).val());                
                discx = $("#v_Disc_" + id).val();
				potx = reform($("#v_Potongan_dec" + id).val());
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat(reform($("#v_Potongan_dec" + id).val()));
				}
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                $("#v_sJumlah_" + id).val(subtot);
                $("#v_Potongan_" + id).val(potx);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
	
	*/
	
	/* Fungsi */
	function formatRupiah(obj)
	{ 
		angka = $('#'+obj).val();
	  
		var number_string = angka.replace(/[^.\d]/g, '').toString(),
			split	= number_string.split('.'),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
		if (ribuan) {
			separator = sisa ? ',' : '';
			rupiah += separator + ribuan.join(',');
		}
		
		rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
		$('#'+obj).val(rupiah);
	}
	
	function formatRupiahAngka(angka)
	{ 	
		var number_string = angka.replace(/[^.\d]/g, '').toString(),
			split	= number_string.split('.'),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
		if (ribuan) {
			separator = sisa ? ',' : '';
			rupiah += separator + ribuan.join(',');
		}
		
		rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
		return rupiah;
	}
	
	function pickThisNew(obj)
{
	    base_url = $("#base_url").val();
	    supplier = $("#v_supplier").val();
	    
	    if(supplier==""){
	    	alert("Supplier Belum Di Pilih...");
			return false;
		}	    

	    objek = obj.id;
		id = parseFloat(objek.substr(9,objek.length-9));
		url = base_url+"index.php/pop/pop_up_cari_purchase_request/index/0/1/";
		windowOpener(525, 1000, 'Cari Purchase Request No', url, 'Cari Purchase Request No')
}

    function konversiSatuan(pcode,id,qty){
        var QtyPR=  $("#v_qty_request_hidden_" + id).val() * 1;
        var Satuan_From=  $("#v_satuan_from" + id).val();
        var Satuan_To=  $("#v_satuan_to" + id).val();
        var url=$('#base_url').val();
        $.ajax({
			url : url+"index.php/transaksi/purchase_order/get_konversi_new",
			type: "POST",
			data: "pcode="+pcode+"&satuan_from="+Satuan_From+"&satuan_to="+Satuan_To+"&qty="+QtyPR,
			success: function(res)
			{
				var nilai = parseFloat(res).toFixed(2);
				var nil = formatRupiahAngka(nilai);
				$("#v_Qty_dec"+id).val(nil);
				$("#v_Qty_"+id).val(res);	

				$("v_satuan_konvert"+id).val(Satuan_To);

				hitungSubtotal(id,res);

			},
			error: function(e) 
			{
				alert(e);
			}
		});

    }

    function hitungSubtotal(id,qty_konvert='')
    {
    	var qty=0;
		var qty_      = $("#v_Qty_dec"+id).val() == "" ? 0 : $("#v_Qty_dec"+id).val(); 
		var harga_   = $("#v_Harga_dec"+id).val() == "" ? 0 : $("#v_Harga_dec"+id).val();
		var disc     = $("#v_Disc_dec"+id).val() == "" ? 0 : $("#v_Disc_dec"+id).val();
		var potongan_ = $("#v_Potongan_dec"+id).val() == ""? 0 : $("#v_Potongan_dec"+id).val();

		if(qty_konvert != ''){
			qty = qty_konvert;
		}else{
			qty = qty_ != 0 ? qty_.replace(/,/g, ''):qty_; 
		}
		
		var harga = harga_ != 0 ? harga_.replace(/,/g, ''):harga_;
		var potongan = potongan_ != 0 ? potongan_.replace(/,/g, ''):potongan_;
		var tot_harga = parseFloat(harga)*parseFloat(qty);

		var subtotal = (tot_harga - (parseFloat(disc)/100 * tot_harga)) - parseFloat(potongan).toFixed(2);
		var subtotal_dec = formatRupiahAngka(subtotal.toString());

		$("#v_subtotal_dec"+id).val(subtotal_dec);
		$("#v_subtotal_"+id).val(subtotal);
		$("#v_Harga_"+id).val(harga);
		$("#v_Qty_"+id).val(qty);
		$("#v_Disc_"+id).val(disc);
		$("#v_Potongan_"+id).val(potongan);
		// $("#view_disc"+id).html(parseInt(disc)/100 * tot_harga);
		sumAll('v_subtotal','v_Jumlah_dec','v_Jumlah');
    }

    function sumAll(nameInput,id_view,id){
		var values = $("input[name='"+nameInput+"[]']").map(function(){return $(this).val();}).get();

        var count = values.length;
		var total =0;
		var jumlah=0;
		var tot =0;
	  	for(var i = 0;i<count;i++){
	  		if(values[i] == ''){
	  			jumlah     = 0;
	  		}else{
	  			jumlah     = values[i].replace(/\,/g, '');
	  		}
	  		
			tot = parseFloat(jumlah);

			total += tot;
	  		
	  	}

		var total_round = Math.round(total).toFixed(2);
		var total_view = formatRupiahAngka(total_round.toString());
	  	$("#"+id_view).val(total_view);
	  	$("#"+id).val(total_round);
		  
		hitungGrandTotal();
	}

	function hitungGrandTotal(){
		var total_ = $("#v_Jumlah_dec").val() == "" ? 0 : $("#v_Jumlah_dec").val();
		var disc_  = $("#v_pot_disc_dec").val() == "" ? 0 : $("#v_pot_disc_dec").val();
		var ppn_   = $("#v_NilaiPPn_dec").val() == "" ? 0 : $("#v_NilaiPPn_dec").val();
		
		var total  = total_ != 0 ? total_.replace(/,/g, ''):total_;
		var disc   = disc_ != 0 ? disc_.replace(/,/g, ''):disc_;
		var ppn    = ppn_ != 0 ? ppn_.replace(/,/g, ''):ppn_;

		var grandTotal_notRound = (parseFloat(total) - parseFloat(disc)) + parseFloat(ppn);
		var grandTotal = Math.round(grandTotal_notRound).toFixed(2);
		var grandTotal_dec = formatRupiahAngka(grandTotal.toString());
		$("#v_Total").val(grandTotal);
		$("#v_Total_dec").val(grandTotal_dec);
	}

	function potongDiscTotal(){ 
		var total = $("#v_Jumlah").val();
		var diskonHarga = $("#v_DiscHarga").val() == "" ? 0 : $("#v_DiscHarga").val();
		var potongan = (parseInt(diskonHarga)/100) * parseInt(total); 

		var potongan_dec = formatRupiahAngka(potongan.toString());

		$("#v_pot_disc_dec").val(potongan_dec);
		$("#v_pot_disc").val(potongan);

		hitungGrandTotal();
	}

	function tambahPpn(){
		var total = $("#v_Jumlah").val();
		var ppn_persen = $("#v_PPn").val() == "" ? 0 : $("#v_PPn").val();
		var ppn = (parseInt(ppn_persen)/100) * parseInt(total); 

		var ppn_dec = formatRupiahAngka(ppn.toString());

		$("#v_NilaiPPn_dec").val(ppn_dec);
		$("#v_NilaiPPn").val(ppn);

		hitungGrandTotal();
	}
	
</script>