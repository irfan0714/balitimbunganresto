<?php 
$this->load->view('header'); 
$this->load->library('globallib');
$this->load->helper('terbilang');

$mylib = NEW Globallib;

$modul = "Purchase Order";
$counter=1;

foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script src="<?= base_url();?>assets/js/newdatatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/js/newdatatables/dataTables.bootstrap.min.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/purchase_order_v4.js"></script>
<script src="<?= base_url();?>public/js/js.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_order/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen; ?>">
	    <input type="hidden" name="v_gudang" id="v_gudang" value="<?php echo $header->KdGudang; ?>">
	    <input type="hidden" name="v_approve" id="v_approve" value="">
	    <input type="hidden" name="v_approve_edit" id="v_approve_edit" value=""> 
	    <input type="hidden" name="v_edit_after_reject" id="v_edit_after_reject" value="">
		<input type="hidden" name="v_reject" id="v_reject" value="">
		<input type="hidden" name="v_request_edit" id="v_request_edit" value="">  
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->NoDokumen; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->TglDokumens!="" && $header->TglDokumens!="00-00-0000") { echo $header->TglDokumens; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        
	        <!--<tr>
	            <td class="title_table" width="150">Warehouse</td>
	            <td><b><?php echo $header->warehousecode." :: ".$header->Keterangan; ?></b></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">Gudang</td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
	            			$selected="";
							if($header->KdGudang==$val["KdGudang"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Supplier </td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
	            			$selected="";
							if($header->KdSupplier==$val["KdSupplier"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">TOP ( Hari )</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->TOP; ?>" name="v_top" id="v_top" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. PR</td>
	            <td>
	            <input readonly type="text" class="form-control-new" value="<?php echo $header->NoPr; ?>" name="v_nopr" id="v_nopr" maxlength="255" style="width: 200px;" onblur="detail()">
	            <!--<a disabled href="javascript:void(0)" id="get_pr" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PR" title=""><i class="entypo-search"></i></a>		
	            --></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Untuk Divisi</td>
	            <td>
	                <input type="text" class="form-control-new" value="<?php echo $header->notes; ?>" name="v_namadivisi" id="v_namadivisi" maxlength="255" style="width: 200px;" readonly="">
	            	<input type="hidden" class="form-control-new" value="<?php echo $header->notes; ?>" name="v_kddivisi" id="v_kddivisi" maxlength="255" style="width: 200px;" readonly="">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Currency</td>
                <td>
                	<select name="v_currencycode" id="v_currencycode" class="form-control-new" style="width: 200px;">  
                        	<option value="">Pilih Mata Uang</option>
		            		<?php
		            		foreach($currency as $val)
		            		{
		            			$selected="";
								if($header->currencycode==$val["Kd_Uang"])
								{
									$selected='selected="selected"';
								}
							
								?><option <?php echo $selected;?> value="<?php echo $val["Kd_Uang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
							}
		            		?>
	            	</select>
	            	<span id="td_kurs" style="display: ;">
	            		<input type="text" style="text-align: right;" class="form-control-new" placeholder="Input Kurs" value="<?php echo $header->kurs; ?>" name="kurs" id="kurs"/>
	            	</span>
                </td>   
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
			
			<tr>
	            <td class="title_table" width="150">Estimasi Terima</td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->TglTerimas!="" && $header->TglTerimas!="00-00-0000") { echo $header->TglTerimas; }else{echo date('d-m-Y');}  ?>" name="v_tgl_terima" id="v_tgl_terima" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        <?php if($header->Status==2){?>
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            		<option <?php if($header->Status==3){ echo "selected='selected'"; } ?> value="3">UnVoid</option>
		            	</select>
		            </td>
		        </tr>
			<?php }else{ ?>
				<tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->Status==1){ echo "selected='selected'"; } ?> value="1">Kirim PO</option>
		            		<option <?php if($header->Status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
			<?php } ?>
			<?php
			
			$qs="
					SELECT 
					  trans_terima_header.NoDokumen,
	                  trans_terima_header.PoNo
					FROM
					  trans_terima_header 
					WHERE 1
					AND trans_terima_header.PoNo='".$header->NoDokumen."' 
					AND trans_terima_header.Status<>'2';
				";
				$sqls=mysql_query($qs);
				$rows=mysql_fetch_array($sqls);
				if(!empty($rows)){
					$rg_status = "<font color='green'><b>OK</b></font>";
				}else{
					$rg_status = "<font color='red'><b>NO</b></font>";
				}
				
			if($cekrg=="0"){	
			if($header->Status==1 AND $header->approvstat==0)
			{
			?>
			    <tr>
		        	
		            <td colspan="100%">
						<font style="color: orange; font-style: italic; font-weight: bold;">INFO : PO Sedang Proses Menunggu Approval </font> <font color="red"><b><i><?=$keterangan;?></i></b></font>
					</td>
		        </tr>	        
	        <?php
	        }else if($header->Status==1 AND $header->approvstat==1){
				if($rg_status=="OK"){?>
					<tr>
		        	
		            <td colspan="100%">
						<font style="color: green; font-style: italic; font-weight: bold;">INFO : PO Tidak dapat diedit, Karena Sudah Ada Penerimaan RG.</font>
					</td>
		        </tr>
					<?}else{?>
			    <tr>
		        	
		            <td colspan="100%">
						<font style="color: green; font-style: italic; font-weight: bold;">INFO : PO Tidak dapat diedit, Karena PO Sudah Approve.</font>
					</td>
		        </tr>	        
	        <?php }
			}else if($header->Status==1 AND $header->approvstat==2){
				?>
			    <tr>
		        	
		            <td colspan="100%">
						<font style="color: red; font-style: italic; font-weight: bold;">INFO : PO Tidak dapat diedit, Karena PO Sudah Direject.</font>
					</td>
		        </tr>	        
	        <?php
			}
			else if($header->Status==1 AND $header->approvstat==9){
				?>
			    <tr>
		        	
		            <td colspan="100%">
						<font style="color: red; font-style: italic; font-weight: bold;">INFO : PO Sedang Proses Menunggu Approval Level 2 </font> <font color="red"><b><i><?=$keterangan;?></i></b></font>
					</td>
		        </tr>	        
	        <?php
			}
			else if($header->Status==2){
				?>
			    <tr>
		        	
		            <td colspan="100%">
						<font style="color: red; font-style: italic; font-weight: bold;">INFO : PO Tidak dapat diedit, Karena PO Sudah Di Void.</font>
					</td>
		        </tr>	        
	        <?php
			}else if($header->Status==4){
				?>
			    <tr>
		        	
		            <td colspan="100%">
						<font style="color: blue; font-style: italic; font-weight: bold;">INFO : PO Tidak dapat diedit, Karena Proses Approval Edit PO.</font>
					</td>
		        </tr>	        
	        <?php
			}
			}else{?>
				<tr>
		        	
		            <td colspan="100%">
						<font style="color: blue; font-style: italic; font-weight: bold;">INFO : PO Tidak dapat diedit, Karena PO sudah ada RG <?php echo $norg;?>.</font>
					</td>
		        </tr>
			<?php }
	        ?>
			
			<?php
			if(count($detail_list)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
								<th width="30"><center>No</center></th>
								<th width="50"><center>PCode</center></th>
							    <th width="300"><center>Nama Barang</center></th>               
							    <th width="100"><center>Qty Request</center></th>
							    <th width="50"><center>Satuan</center></th>
							    <th width="30"><center>Qty</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>History</center></th>
							    <th width="100"><center>Disc (%)</center></th>
							    <th width="100"><center>Potongan (IDR)</center></th>
							    <th width="100"><center>Sub Total</center></th>							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$Sid=1;
						$subtotals = 0;
						foreach($detail_list as $val)
						{
							?>
							<tr>
								<td align="center"><?php echo $Sid; ?></td>
								<td align="center"><?php echo $val["PCode"]; ?><input type="hidden" name="v_pcode[]" id="v_pcode<?php echo $Sid;?>" value="<?php echo $val["PCode"]; ?>"></td>
								<td align="left"><?php echo $val["NamaLengkap"]; ?><input type="hidden" name="v_nmbarang[]" value="<?php echo $val["NamaLengkap"]; ?>"></td>
								<td align="center"><?php echo number_format($val["QtyPR"])." ".$val["SatuanPR"]; ?><input type="hidden" name="v_qty_request[]" value="<?php echo $val["QtyPR"]; ?>"></td> 
								<td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $Sid;?>">
				                	<option value=""> -- Pilih -- </option>
				                	<?php
				                	$this->load->model('transaksi/purchase_order_model');
				                	$satuan = $this->purchase_order_model->getSatuanPcode($val["PCode"]);
				            		foreach($satuan as $vals)
				            		{
				            			$selected="";
										if($vals["Satuan"]==$val["Satuan"])
										{
											$selected='selected="selected"';
										}
										
										?><option <?php echo $selected;?> value="<?php echo $vals["Satuan"]; ?>"><?php echo $vals["NamaSatuan"]; ?></option><?php
									}
				            		?>
				                	</select>
				                </td>
								<td align="right">
								<input style="text-align: right; width: 60px;" type="text" class="form-control-new" id="v_Qty_dec<?php echo $Sid; ?>" value="<?php echo number_format($val["Qty"],2); ?>" onkeyup="HitungOnBlurQty('harga',this),formatRupiah('v_Qty_dec<?php echo $Sid; ?>')">
								<input style="text-align: right; width: 60px;" type="hidden" class="form-control-new" name="v_Qty[]" id="v_Qty_<?php echo $Sid; ?>" value="<?php echo $val["Qty"]; ?>">	
								</td>
								
                                <td align="right">
                                <input style="text-align: right; width: 100px;" type="text" class="form-control-new" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal, total dan grand total."  id="v_Harga_dec<?php echo $Sid; ?>" value="<?php echo number_format($val["Harga"],4); ?>"  onkeyup="HitungOnBlur('harga',this),formatRupiah('v_Harga_dec<?php echo $Sid; ?>')" >
                                <input style="text-align: right; width: 100px;" type="hidden" class="form-control-new" name="v_Harga[]" id="v_Harga_<?php echo $Sid; ?>" value="<?php echo $val["Harga"]; ?>"  dir="rtl" >	
                                </td>
                                <td align="center">
                                	<span id="history" onclick="ShowHistory('<?php echo $val["PCode"]; ?>')" class="btn btn-orange">Show</span>
                                </td>
                                
							  	<td align="right">
							  	<input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc[]" id="v_Disc_<?php echo $Sid; ?>" onkeydown="HitungHarga2(event, 'harga', this);"  onkeyup="HitungOnBlur2('harga',this)" value="<?php echo $val["Disc1"]; ?>">
							  	</td>
							  	
							  	<td align="right">
							  	<input style="text-align: right; width: 100px;" type="text" class="form-control-new" id="v_Potongan_dec<?php echo $Sid; ?>" onkeyup="HitungOnBlur3('harga',this),formatRupiah('v_Potongan_dec<?php echo $Sid; ?>')" value="<?php echo number_format($val["Potongan"],2); ?>" >
							  	<input style="text-align: right; width: 100px;" type="hidden" class="form-control-new" name="v_Potongan[]" id="v_Potongan_<?php echo $Sid; ?>" value="<?php echo $val["Potongan"]; ?>" >	
							  	</td>
							  	<td align="right">
							  	<input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" id="v_subtotal_dec<?php echo $Sid; ?>" dir="rtl" value="<?php echo number_format($val["Jumlah"]); ?>" >
							  	<input readonly style="text-align: right; width: 100px;" type="hidden" class="form-control-new" name="v_subtotal[]" id="v_subtotal_<?php echo $Sid; ?>" dir="rtl" value="<?php echo $val["Jumlah"]; ?>" >	
							  	</td>
								<td style="display: none"><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $Sid; ?>" value="<?php echo $val['Total'] ?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
							  </tr>
							<?php
							$Sid++;
							$subtotals += $val["Qty"] * $val["Harga"];
						}
						
						?>
							
						</tbody>
						
						
						     <tr style="color: black; font-weight: bold;">
                                <td colspan="9" rowspan="4">
                                    <!--Terbilang :--> &nbsp;
                                </td>
                                <td style="text-align: right;">
                                TOTAL
                                
                                </td>
                                <td style="text-align: right;">
                                <input readonly style="text-align: right;" class="form-control-new" type="text" id="v_Jumlah_dec" value="<?php echo number_format($header->Jumlah);?>">
                                <input readonly style="text-align: right;" class="form-control-new" type="hidden" name="v_Jumlah" id="v_Jumlah" value="<?php echo $header->Jumlah;?>">	
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total." style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" onkeyup="HitungHarga(event, 'diskon', this);" value="<?php echo $header->DiscHarga;?>" > (%)</td>
                                <?php $diskons = $header->Jumlah*$header->DiscHarga/100;?>
                                <td style="text-align: right;">
                                <input readonly style="text-align: right;" class="form-control-new" type="text" id="v_pot_disc_dec" value="<?php echo number_format($diskons);?>">
                                <input readonly style="text-align: right;" class="form-control-new" type="hidden" name="v_pot_disc" id="v_pot_disc" value="<?php echo $diskons;?>">	
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total."style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn" id="v_PPn" value="<?php echo $header->PPn_;?>" onkeyup="HitungHarga(event, 'ppn', this);"> (%)</td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" id="v_NilaiPPn_dec" value="<?php echo number_format($header->Jumlah*$header->PPn_/100);?>">
                                    <input readonly style="text-align: right;" class="form-control-new" type="hidden" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo $header->Jumlah*$header->PPn_/100;?>">
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                </td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" id="v_Total_dec" value="<?php echo number_format($header->Jumlah+($header->Jumlah*$header->PPn_/100)-$diskons);?>">
                                    <input readonly style="text-align: right;" class="form-control-new" type="hidden" name="v_Total" id="v_Total" value="<?php echo $header->Jumlah+($header->Jumlah*$header->PPn_/100)-$diskons;?>">
                                </td>
                            </tr>
						
						
					</table>
	        	
	        	</td>
	        </tr>
	        <tr style="color: black; font-weight: bold;">
	        	<td colspan="100%"> Terbilang : <span id="terbilangin"><?php echo terbilang(ROUND($header->Jumlah+($header->Jumlah*$header->PPn_/100)-$diskons))." Rupiah";?></span></td>
	        </tr>
	        
	        <?php
	        }
	        ?>
			
			<?php
			if($header->Status==0)
			{//hello
				if($hak_otorisasi=="Y"){
			?>
			<tr>
	            <td colspan="100%" align="center">
					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
				</td>
	        </tr>
	        <?php
	        	}else{?>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>	
				<?php }
	        }
	        else
	        {
	        	?>
		        <tr>
		        	
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						
						<?php  if ($hak_otorisasi=="Y"){
							         if($header->approvstat==0 OR $header->approvstat==9){  
							         	if($level_otorisasi=="po_level_1" AND $header->Total<=10000000){
													?>
													
												        <span style="float: right;">
												            
						                                    <button type="button" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $header->NoDokumen; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
						                                    
						                                    <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
						                                    
						                                    
						                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
						                                    <button style="display: none;" type="button" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $header->NoDokumen; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
						                               </span>
								        <?php }elseif($level_otorisasi=="po_level_2" AND $header->Total>10000000){
													?>
												        <span style="float: right;">
												            
						                                    <button type="button" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $header->NoDokumen; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
						                                    
						                                    <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-info btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
						                                    
						                                    
						                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Alasan Reject">
						                                    <button style="display: none;" type="button" name="btn_reject" id="btn_reject" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $header->NoDokumen; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
						                               </span>
								        <?php }else{
									//ketika mau batalin agar bisa diedit?>
					   					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
					   					<?php if($cekrg=="0"){ ?>
												<?php }if($header->Request_Status=="1" AND $header->Total<=10000000 AND $level_otorisasi=="po_level_1"){
												?>
														<span style="float: right;">
														<button type="button" name="btn_approve_edit" id="btn_approve_edit" class="btn btn-success btn-icon btn-sm icon-left" onclick="confirm_approve_edit('<?php echo $header->NoDokumen; ?>');" value="Approve_edit">Setujui Edit PO<i class="entypo-check"></i></button>
														</span>
												<?php
												}else if($header->Request_Status=="1" AND $header->Total>=10000000 AND $level_otorisasi=="po_level_2"){
												?>
														<span style="float: right;">
														<button type="button" name="btn_approve_edit" id="btn_approve_edit" class="btn btn-success btn-icon btn-sm icon-left" onclick="confirm_approve_edit('<?php echo $header->NoDokumen; ?>');" value="Approve_edit">Setujui Edit PO<i class="entypo-check"></i></button>
														</span>
												<?php
												}
										} ?>
									<?php
								}
					   		}else{
					   			if($header->approvstat==0){
					   				// walaupun sudah dikirim, masih bisa edit selagi belum di approve?>
					   			<tr>
						            <td colspan="100%" align="center">
										<input type='hidden' name="flag" id="flag" value="edit">
										<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
						                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
					                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
									</td>
						        </tr>
					   			<?php }else{
					   						if($header->Status==4){?>
					   							
					   					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
					                    		
					   				<?php }else{												
												
					   				?>
					   			        <!--<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>-->
					                    <?php if($cekrg=="0" AND $header->approvstat!=2){?>
					                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
										<span style="float: right;">
					   					
	                                    <button type="button" name="btn_confirm_request" id="btn_confirm_request" class="btn btn-success btn-icon btn-sm icon-left" value="Request2" onclick="muncul_request()">Request Edit PO<i class="entypo-check"></i></button>    
	                                    
	                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_request_Remarks" id="v_request_Remarks" value="" placeholder="Alasan Request Edit PO">
	                                    <button style="display: none;" type="button" name="btn_request" id="btn_request" class="btn btn-orange btn-icon btn-sm icon-left" onclick="confirm_request('<?php echo $header->NoDokumen; ?>');" value="Request2">Kirim<i class="entypo-check"></i></button>    
	                                    </span>
										<?php }else{?>
											
											
											<tr>
									            <td colspan="100%" align="center">
													<input type='hidden' name="flag" id="flag" value="edit">
													<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
									                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
								                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="confirm_edit_after_reject('<?php echo $header->NoDokumen; ?>');" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
												</td>
									        </tr>
											
										<?php } ?>
					   	<?php }
					   	   } 
					   	} ?>
					</td>
		        </tr>
				<?php

			}
	        ?>
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->NoDokumen)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->AddUser." :: ".$header->AddDate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->EditUser." :: ".$header->EditDate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	function ShowHistory(pcode){
		
		$.ajax({
	  	type: 'ajax',
	  	method: 'post',
	  	url: '<?php echo base_url() ?>index.php/transaksi/purchase_order/GetHistoryBeli',
	  	data: {pcode: pcode},
	  	async: false,
	  	dataType: 'json',
	  	success: function(data1){

  		var html2 = '';
  	
  		var i;
  		var no =1;
  		for(i=0; i<data1.length; i++){
  					
  								html2 +='<tr>'+
  									'<td>'+
  									no+
  									'</td>'+
  									'<td>'+
  									data1[i].Tanggal+
  									'</td>'+
  									'<td>'+
  									data1[i].Nama+
  									'</td>'+
  									'<td>'+
  									data1[i].PCode+
  									'</td>'+
  									'<td>'+
  									data1[i].NamaLengkap+
  									'</td>'+
  									'<td>'+
  									data1[i].Satuan+
  									'</td>'+
  									'<td>'+
  									data1[i].Harga+
  									'</td>'+
  									'</tr>';
  									no++;

  		}
  		$('#isipembelian').html(html2);
  		
	  	},
	  	error: function(){
	  		alert('Could not Show History');
	  	}
	  	});
  

		$('#modal-history1').modal('show');
		// alert(pcode);
		$('#riwayatbeli').DataTable();
	}

	function muncul_request()
        {   
            document.getElementById("btn_confirm_request").style.display = "none";    
            
            document.getElementById("v_request_Remarks").style.display = "";    
            document.getElementById("btn_request").style.display = "";    
            
            document.getElementById("v_request_Remarks").focus();
        }
        
	function muncul_reject()
        {
            document.getElementById("btn_approve").style.display = "none";    
            document.getElementById("btn_confirm_reject").style.display = "none";    
            
            document.getElementById("v_Approval_Remarks").style.display = "";    
            document.getElementById("btn_reject").style.display = "";    
            
            document.getElementById("v_Approval_Remarks").focus();
        }
        
	function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve "+name+" ? ");
            
            if(r==true)
            {
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
    function confirm_reject(name)
        {
            var r = confirm("Anda yakin ingin Reject "+name+" ? ");
            if(r==true)
            {
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
        }
    function confirm_request(name)
        {
            var r = confirm("Anda yakin ingin Request Edit PO "+name+" ? ");
            if(r==true)
            {
                document.getElementById("v_request_edit").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
         function confirm_edit_after_reject(name)
        {
            var r = confirm("Anda yakin ingin Kirim Kembali PO "+name+" ini Setalah diReject? dan apakah anda sudah memastikan dengan benar?");
            if(r==true)
            {
                document.getElementById("v_edit_after_reject").value = '1';
                document.getElementById("theform").submit();
            }
        }
    function confirm_approve_edit(name)
        {
            var r = confirm("Anda yakin ingin Setujui Edit PO "+name+" ? ");
            if(r==true)
            {
                document.getElementById("v_approve_edit").value = '1';
                document.getElementById("theform").submit();
            }
        }
		
			function HitungOnBlurQty(flag,obj) {
			
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(9, objek.length - 9));
				qty = parseFloat(reform($("#v_Qty_dec" + id).val()));
                hrgx = $("#v_Harga_" + id).val();                
                discx = $("#v_Disc_" + id).val();
				potx = $("#v_Potongan_" + id).val();
				
				
				if(hrgx==""){
					hrg=1;
				}else{
					hrg = parseFloat($("#v_Harga_" + id).val());  
				}
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat($("#v_Potongan_" + id).val());
				}
				
				
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                
                $("#v_sJumlah_" + id).val(subtot);
                $("#v_Qty_" + id).val(qty);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
	
	function HitungOnBlur(flag,obj) {
		
            objek = obj.id;
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(11, objek.length - 11));
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg = parseFloat(reform($("#v_Harga_dec" + id).val()));                
                discx = $("#v_Disc_" + id).val();
				potx = $("#v_Potongan_" + id).val();
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat($("#v_Potongan_" + id).val());
				}
				
				
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                $("#v_sJumlah_" + id).val(subtot);
                $("#v_Harga_" + id).val(hrg);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
    
    function HitungOnBlur2( flag, obj) {
	
            objek = obj.id;
			
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(7, objek.length - 7));
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg = parseFloat($("#v_Harga_" + id).val());                
                
                discx = $("#v_Disc_" + id).val();
				potx = $("#v_Potongan_" + id).val();
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat($("#v_Potongan_" + id).val());
				}
				
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4) );
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                $("#v_sJumlah_" + id).val(subtot);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
    
    
    function HitungOnBlur3( flag, obj) {
	
    	
            objek = obj.id;
			
            if (flag == 'harga') {
                grdTotal = 0;
                id = parseFloat(objek.substr(14, objek.length - 14));
				qty = parseFloat($("#v_Qty_" + id).val());
                hrg = parseFloat($("#v_Harga_" + id).val());                
                discx = $("#v_Disc_" + id).val();
				potx = reform($("#v_Potongan_dec" + id).val());
				
				if(discx==""){
					disc=0;
				}else{
					disc = parseFloat($("#v_Disc_" + id).val());
				}
				
				if(potx==""){
					pot=0;
				}else{
					pot = parseFloat(reform($("#v_Potongan_dec" + id).val()));
				}
				//hasil diskon
				nil_dis = (disc/100)*(hrg * qty);
				//hitung
                $("#v_subtotal_" + id).val( (((hrg * qty)-nil_dis)-pot).toFixed(4));
                subtot = $("#v_subtotal_" + id).val();
                document.getElementById("v_subtotal_dec" + id).value = formatRupiahAngka(subtot);
                $("#v_sJumlah_" + id).val(subtot);
                $("#v_Potongan_" + id).val(potx);
                totalNetto();
            }else if (flag == 'diskon') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }else if (flag == 'ppn') {
				jml = parseFloat($("#v_Jumlah").val());
				discharga = parseFloat($("#v_DiscHarga").val());                
                ppn = parseFloat($("#v_PPn").val());
				
				//hasil diskon
				nil_dis = (discharga/100)*(jml);
				$("#v_pot_disc").val(nil_dis);
				//hitung ppn
				nil_ppn = (ppn/100)*(jml);
				$("#v_NilaiPPn").val(nil_ppn);
				
				
                $("#v_Total").val((jml-nil_dis)+nil_ppn);
            }

        
    }
	
	
	
	/* Fungsi */
	function formatRupiah(obj)
	{ 
		angka = $('#'+obj).val();
	  
		var number_string = angka.replace(/[^.\d]/g, '').toString(),
			split	= number_string.split('.'),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
		if (ribuan) {
			separator = sisa ? ',' : '';
			rupiah += separator + ribuan.join(',');
		}
		
		rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
		$('#'+obj).val(rupiah);
	}
	
	function formatRupiahAngka(angka)
	{ 	
		var number_string = angka.replace(/[^.\d]/g, '').toString(),
			split	= number_string.split('.'),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);
		
		if (ribuan) {
			separator = sisa ? ',' : '';
			rupiah += separator + ribuan.join(',');
		}
		
		rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
		return rupiah;
	}
</script>