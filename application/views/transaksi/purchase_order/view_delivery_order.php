<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Delivery Order";

$counter=1;
foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namasatuan"][$counter]=$val["NamaSatuan"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script language="javascript" src="<?=base_url();?>public/js/delivery_order_v4.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/delivery_order/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->dono; ?>">
	    <input type="hidden" name="v_gudang" id="v_gudang" value="<?php echo $header->warehousecode; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->dono; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            <b><?php echo $header->dodate; ?></b>
	            
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Gudang</td>
	            <td><b><?php echo $header->warehousecode." :: ".$header->Keterangan; ?></b></td>
	        </tr>
	        
	        
	        <tr>
	            <td class="title_table" width="150">Customer</td>
	            <td><b><?php echo $header->customerid." :: ".$header->Nama; ?></b></td>
	        </tr>	        
	        
	        
	        <tr>
	            <td class="title_table">Contact Person</td>
	            <td><?php echo $header->contactperson; ?></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Keterangan</td>
	            <td><b><?php echo $header->note; ?></b></td>
	        </tr>
	        
             
			<?php
			if($header->status==0 or $header->status==1 or $header->status==2)
			{?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            <?php
					if($header->status==0)
		            {
						echo "<b>Pending</b>";
					}
					else if($header->status==1)
		            {
						echo "<b>Close</b>";
					}
					else if($header->status==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr> 
			<?php
			}
			?>
			
			<?php
			if(count($detail)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							   							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$i=1;
						foreach($detail as $val)
						{
							?>
							<tr >
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaBarang"]; ?></td>
								<td align="right" ><?php echo $mylib->format_number($val["Qty"],2); ?></td>
								<td align="center"><?php echo $val["Satuan"]; ?></td>
								
				                
							</tr>
							<?php
							$i++;
						}
						
						?>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        
	        <?php
	        }
	        ?>
			
			<?php
			if($header->status==1 or $header->status==2)
			{
			?>
			
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							</tr>
						</thead>
						<tbody>
							
								<?php
																
								foreach($arr_data["list_market"] as $counter => $val)
								{
									
									$pcode= $arr_data["pcode"][$counter];
									$quantity=$arr_data["quantity"][$counter];
									$satuan= $arr_data["satuan"][$counter];
									$NamaSatuan = $arr_data["namasatuan"][$counter];
									$namalengkap= $arr_data["namalengkap"][$counter];
								?>
								<tr>
					                <td>
						                <input type="hidden" name="market_pcode[]" id="market_pcode<?php echo $counter;?>" value="<?php echo $pcode; ?>"/>
						                <input type="hidden" name="market_namabarang[]" id="market_namabarang<?php echo $counter;?>" value="<?php echo $namalengkap; ?>"/>
						                <input type="hidden" name="market_satuan[]" id="market_satuan<?php echo $counter;?>" value="<?php echo $satuan; ?>"/>
						                <?php echo $pcode; ?></td>
					                <td><?php echo $namalengkap; ?></td>
					                <td align="right">
					                	<input type="text" class="form-control-new" name="market_qty[]" onblur="toFormat2('market_qty<?php echo $counter;?>')" id="market_qty<?php echo $counter;?>" value="<?php echo $quantity; ?>" style="text-align: right;" disabled="true"/>
					                </td>
					                <td><?php echo $NamaSatuan; ?></td>
					            </tr>
								<?php	
														
								}
								?>
								
							</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <t
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/delivery_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
              </td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/delivery_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->dono)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>