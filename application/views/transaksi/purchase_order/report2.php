<script>
    
	function mouseover(target)
	{  
	    if(target.bgColor!="#cafdb5"){        
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#c1cdd8';
	    }
	}
    
	function mouseout(target)
	{
	    if(target.bgColor!="#cafdb5"){ 
	        if (target.bgColor=='#ccccff')
	            target.bgColor='#ccccff';
	        else
	            target.bgColor='#FFFFFF';
	            
	    }    
	}

	function mouseclick(target, idobject, num)
	{
	                   
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;            
	    for(i=0;i<num;i++){
	        if (document.getElementById(idobject+'_'+i) != undefined){
	            document.getElementById(idobject+'_'+i).bgColor='#f5faff';
	            if (target.id == idobject+'_'+i)
	                target.bgColor='#ccccff';
	        }
	    }
	}

	function mouseclick1(target)
	{
	    //var pjg = document.getElementById(idobject + '_sum').innerHTML;  
	    if(target.bgColor!="#cafdb5")
	    {
	        target.bgColor="#cafdb5";
	    }
	    else
	    {
	        target.bgColor="#FFFFFF";
	    }
	}  
	
	function pop_up_pemakaian_voucher(v_date)
	{
	    try{
	        windowOpener(600, 800, 'Pemakaian Voucher', 'report_daily_pop_up_pemakaian_voucher.php?v_date='+v_date, 'Pemakaian Voucher')    
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }
	}
</script>
<div class="row">
	<div class="col-md-12" align="left">

<form method="POST" name="search" id="search" action="<?= base_url() ?>index.php/report/rangking_guide/cari/" onsubmit="return false">
    <?php
    $mylib = new globallib();
    if ($excel == "Excel") {
        header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="reminderpo.xls"');
    }
    
    $where_date = "";
    if($v_date_from=="" && $v_date_to=="")
    {
        die("Tanggal Harus diisi");
    }
    
    $table_border = 0;
    if($excel == "Excel" || $kirim_email=='Y')
    {
        $table_border = 1;
    }
    
    $echo = '';
	if($excel == "Excel" || $kirim_email=='Y'){
		$echo = '
		<table style="font-weight: bold;">
		    <tr>
		        <td colspan="17">'.$store[0]['NamaPT'].'</td>   
		    </tr>
		    <tr>
		        <td colspan="17">
		        Dear Bambang Sutrisno,
		        <br><br>
		        For Your Information, 
		        <br>
		        REPORT PURCHASE ORDER BELUM DI APPROVE '.$v_date_from.' s/d '.$v_date_to.'</td>   
		    </tr>                            
		    <tr>
		        <td colspan="17">&nbsp;</td>   
		    </tr>
		</table>
		';
	}
	$echo .= '
		<table class="table table-bordered responsive" style="color: black;" border="'.$table_border.'">
		<thead>
	    	<tr>
		        <th width="30">No</th>
		        <th style="text-align: center;">Tanggal</th>
		        <th style="text-align: center;">No PO</th>
		        <th style="text-align: center;">No PR</th>		        
		        <th style="text-align: center;">Total</th>
		    </tr>
		</thead>

		<tbody>
	';
	
    $no = 1;
    foreach($datapo2 as $val)
    {
        $tanggal = $val['TglDokumen'];
        $no_po = $val['NoDokumen'];
        $no_pr = $val['NoPr'];
        $tots = $val['Total'];
    	$echo .= '
    	<tr style="color: black;" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
	        <td>'.$no.'</td>
	        <td>'.$mylib->ubah_tanggal($tanggal).'</td>
	        <td align="center">'.$no_po.'</td>
	        <td align="center">'.$no_pr.'</td>
	        <td align="right">'.$mylib->format_number($tots, 0, "", "", "ind").'</td>
	    </tr>
    	';
        $no++;
    }
    $echo .= '
		</tbody>';
		
	    
		$echo .= '
		</table>';
		
		$echo .= '
		<br><br>
		<b>Mohon Segera Di Follow Up.</b><br>
		<b>Terima Kasih</b>';
		
	echo $echo;
	
	if($kirim_email=="Y")
    {
        $subject = "Reminder PO Belum Di Approve ".$v_date_from." s/d ".$v_date_to;
        
        $body = $echo;
        
        $to = "";
        $to_name = "";
        for($i=0;$i<count($listemail2);$i++){
			$email_address = $listemail2[$i]['email_address'];
			$email_name = $listemail2[$i]['email_name'];
			
			$to .= $email_address.";";
            $to_name .= $email_name.";";
		}
		
        $author = "Cron Job";
        $mylib->send_email_multiple($subject, $body, $to, $to_name, $author); 
    }

    ?>

</form>
	</div>
</div>