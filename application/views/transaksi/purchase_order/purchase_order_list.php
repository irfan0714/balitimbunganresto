<?php $this->load->view('header'); 

?>
<head>
</head>
<script language="javascript" src="<?= base_url(); ?>public/js/purchase_order_v4.js"></script>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/purchase_order/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
	<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    <div class="row">
        <div class="col-md-10">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;<b>Gudang</b>&nbsp;
            <select class="form-control-new" name="search_gudang" id="search_gudang" style="width: 50px;">
                <option value="">All</option>
                <?php
                foreach ($mgudang as $val) {
                    $selected = "";
                    if ($search_gudang) {
                        if ($val["KdGudang"] == $search_gudang) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
                }
                ?>
            </select>
            &nbsp;<b>Supplier</b>&nbsp;
            <select class="form-control-new" name="search_supplier" id="search_supplier" style="width: 50px;">
                <option value="">All</option>
                <?php
				
                foreach ($supplier as $val) {
                    $selected = "";
                    if ($search_supplier) {
                        if ($val["KdSupplier"] == $search_supplier) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
                }
                ?>
            </select>  
            &nbsp;
            <b>Tanggal</b>&nbsp;
            <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_awal!="" && $search_tgl_awal!="00-00-0000") { echo $search_tgl_awal; }else{echo date('d-m-Y');}  ?>" name="v_tgl_mulai" id="v_tgl_mulai" size="10" maxlength="10">
	        &nbsp;s/d&nbsp;
	        <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_akhir!="" && $search_tgl_akhir!="00-00-0000") { echo $search_tgl_akhir; }else{echo date('d-m-Y');}  ?>" name="v_tgl_akhir" id="v_tgl_akhir" size="10" maxlength="10">
	         
            &nbsp;
            
            &nbsp;<b>Total</b>&nbsp;
            <select class="form-control-new" name="search_total" id="search_total" style="width: 50px;">
                <option value="">All</option>
				<option <?php if($search_total=="0"){ echo 'selected="selected"'; } ?> value="0">Total <= 10.000.000</option>
                <option <?php if($search_total=="1"){ echo 'selected="selected"'; } ?> value="1">Total > 10.000.000</option>
            </select>
            
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status" style="width: 100px;">
                <option value="">All</option>
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0"><b>Pending</b></option>
				<option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1"><b>Open</b></option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2"><b>Void</b></option>
                <option <?php if($search_status=="3"){ echo 'selected="selected"'; } ?> value="3"><b>-- All Reject --</b></option>
                <option <?php if($search_status=="4"){ echo 'selected="selected"'; } ?> value="4">Reject By vicko0604</option>
                <option <?php if($search_status=="5"){ echo 'selected="selected"'; } ?> value="5">Reject By wieok3110</option>
                <option <?php if($search_status=="6"){ echo 'selected="selected"'; } ?> value="6"><b>-- All Waiting Approve --</b> </option>
                <option <?php if($search_status=="7"){ echo 'selected="selected"'; } ?> value="7">Waiting Approve By vicko0604</option>
                <option <?php if($search_status=="8"){ echo 'selected="selected"'; } ?> value="8">Waiting Approve By wieok3110</option>
                <option <?php if($search_status=="9"){ echo 'selected="selected"'; } ?> value="9"><b>-- All Approve --</b></option>
                <option <?php if($search_status=="10"){ echo 'selected="selected"'; } ?> value="10">Approve By vicko0604</option>
                <option <?php if($search_status=="11"){ echo 'selected="selected"'; } ?> value="11">Approve By wieok3110</option>
                <option <?php if($search_status=="12"){ echo 'selected="selected"'; } ?> value="12"><b>-- All Request Edit --</b></option>
                <option <?php if($search_status=="13"){ echo 'selected="selected"'; } ?> value="13">Request Edit By vicko0604</option>
                <option <?php if($search_status=="14"){ echo 'selected="selected"'; } ?> value="14">Request Edit By wieok3110</option>
            </select>
        </div>

        <div class="col-md-2" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/purchase_order/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>


<div class="row">
        <div class="col-md-12">
        	<b><center><?php echo "Bulan ".date('m')." Tahun ".date('Y') ;?></center></b><br>
        	
        	<div class="col-md-12" align="center">
            <button type="button" class="btn btn-primary btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$pending->pending;?> PO Pending</b>
           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-info btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$approved->approved;?> PO Approved</b>
          
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          
            <button type="button" class="btn btn-default btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$void->void;?> PO Void</b>
         
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          
            <button type="button" class="btn btn-warning btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$waiting->waiting;?> All PO Waiting Approve</b>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-inner"><i class="entypo-trash"></i></button>&nbsp;&nbsp;<b><?=$rejected->rejected;?> All PO Rejected</b>
           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-success btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$request->request;?> All Request Edit PO</b>
           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-orange btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$po5->po5;?> PO < 5jt</b>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-purple btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$po510->po510;?> PO > 5jt & PO < 10jt</b>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            <button type="button" class="btn btn-info btn-sm sm-new tooltip-inner"><i class="entypo-pencil"></i></button>&nbsp;&nbsp;<b><?=$po10->po10;?> PO > 10jt</b>
            </div>
         </div>
    </div>
    
    <hr/>
    

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="10"><center>No</center></th>
		        <th width="30"><center>No Dokumen</center></th>
		        <th width="30"><center>Tanggal</center></th>
		        <th width="30"><center>No PR</center></th>
		        <th width="150"><center>Supplier</center></th>
		        <th width="150"><center>Gudang</center></th>
		        <!--<th width="30"><center>&nbsp;</center></th>-->
		        <th width="50"><center>Grand Total</center></th>
		        <th width="100"><center>Approval</center></th>
                <th width="50"><center>PO Status</center></th>
                <th width="50"><center>RG Status</center></th>
                <th width="50"><center>Konfr. Terima</center></th>
                <th width="10"><center>&nbsp;</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
			
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == 0) {
					if($val["Approval_by"]==""){
                    $echo_status = "<font style='color:#000000'><b>Pending</b></font>";
					}else{
					$echo_status = "<font style='color:blue'><b>Editing PO</b></font>";
					}
                } else if ($val["Status"] == 1) {
				    if ($val["Approval_Status"] == 0 OR $val["Approval_Status"] == 1 OR $val["Approval_Status"] == 9) {
                    $echo_status = "<font style='color:green'><b>Open</b></font>";
					$color = "blue";
					}else{
					$echo_status = "<font style='color:orange'><b>Reject</b></font>";
					$color = "orange";
					}
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:red;'><b>Void</b></font>";
					$color = "red";
                }else if ($val["Status"] == 4) {
                    $echo_status = "<font style='color:blue;'><b>Waiting Approve Editing PO, Because </font><font color='red'>".$val["Request_Remarks"]."</b></font>";
					$color = "red";
                }
                
                //cek apakah PO sudah ada di RG
	            $qs="
					SELECT 
					  trans_terima_header.NoDokumen,
	                  trans_terima_header.PoNo
					FROM
					  trans_terima_header 
					WHERE 1
					AND trans_terima_header.PoNo='".$val["NoDokumen"]."' 
					AND trans_terima_header.Status<>'2';
				";
				$sqls=mysql_query($qs);
				$rows=mysql_fetch_array($sqls);
				if(!empty($rows)){
					    //$rg_status= "<font color='green' size='3'><b>O</b></font>";
					    if($this->uri->segment(3)=="index"){
						$rg_status = '<img src="../../../../public/images/accept.png"/>';
						}else{
						$rg_status = '<img src="../../../public/images/accept.png"/>';
						}
					}else{
						if($this->uri->segment(3)=="index"){
						$rg_status = '<img src="../../../../public/images/cancel.png"/>';
						}else{
						$rg_status = '<img src="../../../public/images/cancel.png"/>';
						}
					}
				
				//konfirm terima
				$qsa="
				SELECT 
				  trans_terima_header.FlagKonfirmasi
				FROM
				  trans_terima_header 
				WHERE 1
				AND trans_terima_header.PoNo='".$val["NoDokumen"]."'
			";
			$sqlsa=mysql_query($qsa);
			$rowsa=mysql_fetch_array($sqlsa);
			if($rowsa['FlagKonfirmasi']=="Y"){
				if($this->uri->segment(3)=="index"){
				$rg_status_konfirm = '<img src="../../../../public/images/accept.png"/>';
				}else{
				$rg_status_konfirm = '<img src="../../../public/images/accept.png"/>';
				}
			}else{
				if($this->uri->segment(3)=="index"){
				$rg_status_konfirm = '<img src="../../../../public/images/cancel.png"/>';
				}else{
				$rg_status_konfirm = '<img src="../../../public/images/cancel.png"/>';
				}
			}
			
			if($val["Approval_by"]=="" AND $val["Status"]=="1" AND $val["Total"]<=10000000){
				$apprv = "<b><center><font style='color:#79e8e4'>Waiting Approve </font> <font color='red'></font> <b>vicko0604</b></center></b></center></b>";
			
			}else if($val["Approval_by"]=="" AND $val["Status"]=="1" AND $val["Total"]>10000000 AND $val["Approval_Status"]==0){
				$apprv = "<b><center><font style='color:#79e8e4'>Waiting Approve </font> <font color='red'></font> <b>wieok3110</b></center></b>";
			
			}else if($val["Approval_by"]=="" AND $val["Approval_Date"]=="" AND $val["Status"]=="0"){
				$apprv = "<b><center><font style='color:#000000'>New PO</font></center></b>";
			
			}else if($val["Approval_by"]==""  AND $val["Approval_Date"]!="00-00-0000 12:00:00" AND $val["Status"]=="0"){
				$apprv = "<b><center><font style='color:blue'>Edit PO After Approve</font></center></b>";
			
			}else if($val["Approval_by"]==""  AND $val["Status"]=="0"){
				$apprv = "<b><center><font style='color:green'>UnVoid PO</font></center></b>";
			
			}else if($val["Approval_by"]=="" AND $val["Status"]==""){
				$apprv = "<b><center><font style='color:#000000'>New PO</font></center></b>";
			
			}else if($val["Approval_by"]!="" AND $val["Status"]=="2"){
				$apprv = "<b><center><font style='color:red'>None</font></center></b>";
			
			}else if($val["Approval_by"]=="" AND $val["Status"]=="2"){
				$apprv = "<b><center><font style='color:red'>None</font></center></b>";
			
			}else{
					if($val["Approval_by"]!="" AND $val["Approval_Status"]!="2" AND ( $val["Approval_Remarks"]=="Done" OR $val["Approval_Remarks"]=="")){
					$apprv = "<font color='green' size='2'>Approved By </font>".$val["Approval_by"].", ".$val["Approval_Date"];
					}else if($val["Approval_by"]!="" AND $val["Approval_Status"]=="9"){
					$apprv = "<font color='green' size='2'> Approved By </font>".$val["Approval_by"].", ".$val["Approval_Date"]."</font>, <b>Waiting Approvel Level 2 trisno1402.</b>";
					}else if($val["Approval_by"]!="" AND $val["Approval_Status"]=="1" AND $val["Approval_Remarks"]=="Approval Level 2" ){
					$apprv = "<font color='green' size='2'>Approved By </font>".$val["Approval_by"].", ".$val["Approval_Date"];
					}else {
					$apprv = "<font color='red' size='2'>Rejected By </font> ".$val["Approval_by"].", ".$val["Approval_Date"].", <font color='red' size='2'>".$val["Approval_Remarks"]."</font>";
					}				
			}
			
                ?>
                <tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk detail <?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $no; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $val["TglDokumen"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $val["NoPr"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="left"><?php echo $val["NamaSupplier"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="left"><?php echo $val["NamaGudang"]; ?></td>
                    <!--<td align="left"><?php echo $val["currencycode"]; ?></td>-->
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="right"><?php echo number_format($val["Total"],2); ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="left"><?php echo "<font style='color:".$color."'>".$apprv."</font>"; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $echo_status; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $rg_status; ?></td>
                    <td onclick="editing('<?php echo $val["NoDokumen"]; ?>')" align="center"><?php echo $rg_status_konfirm; ?></td>
                    <td align="center">

                        <?php 
                        $user = $this->session->userdata('username'); 

                        if($val["Status"]=="1" AND $val["Approval_by"]!=""){
                            if($val["AddUser"] == $user OR $user == "hilda1012" OR $user == "yuri0717" OR $user == "ni0707" OR $user == "krisna337" OR $user == "yudhi26071" OR $user == "boi1603" OR $user == "riska0701" OR $user == "andrew0808"  OR $user == "gusti0802" OR $user == "linda3009" OR $user == 'irfan1407' OR $user == 'mechael0101' OR $user == 'wendi0108' OR $user == 'vicko0604'){ ?>
                            
                            <?php  if($val["Approval_Status"]!=='9'){ ?>
							<a href="<?php echo base_url(); ?>index.php/transaksi/purchase_order/doPrint/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-print"></i></a>
                            <?php 
                            }
                            ?>
							
                        <?php }
                            } ?>    

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<?php $this->load->view('footer'); ?>
<script>
function editing(nodok){
var url=$('#base_url').val();
window.location = url+"index.php/transaksi/purchase_order/edit_purchase_order/"+nodok;	
}
</script>
