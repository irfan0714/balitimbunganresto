<?php 
$this->load->view('header'); 

$modul = "Reservasi Konfirmasi";

?>
<script language="javascript" src="<?=base_url();?>public/js/reservasi_konfirmasi.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span id="span_loading" style="float: right; display: none;"><img src="../../../../public/images/ajax-image.gif"/></span>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/reservasi_konfirmasi/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $data->NoDokumen; ?>" />
	        <input type="hidden" name="v_tourtravel" id="v_tourtravel" value="<?php echo $data->KdTravel; ?>" />
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $data->NoDokumen; ?></b></td>	        
	            
	            <td class="title_table" colspan="2">Informasi Tour</td>
	                
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td width="500"> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $data->TglKonfirmasi; ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="12" maxlength="10">
	            </td>
	            
	            <td class="title_table" width="150">Tour Group</td>
	            <td id="td_tourgroup"><?php echo $datatravel->Nama; ?></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Jam <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input class="form-control-new timepicker" data-template="dropdown" data-show-seconds="true" value="<?php echo $data->JamKonfirmasi; ?>" name="v_jam" id="v_jam" data-show-meridian="true" data-minute-step="5" data-second-step="5" type="text" size="12" maxlength="12">
	            </td>
	            
	            <td class="title_table" width="150">Tour PIC</td>
	            <td id="td_tourpic"><?php echo $datatravel->Contact; ?></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No Reservasi <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?php
					if($data->status==1)
					{
						echo "<b>".$data->NoReservasi."</b>";
					}
					else
					{
					?>
	            	<input type="text" class="form-control-new" value="" name="v_keyword_noreservasi" id="v_keyword_noreservasi" size="25" onkeyup="CallAjax('search_keyword_noreservasi', this.value)">&nbsp;
	            	<span id="span_noreservasi">
	            	<select class="form-control-new" name="v_noreservasi" id="v_noreservasi" style="width: 270px;" onchange="CallAjax('ajax_noreservasi', this.value)">
	            		<option value="">Pilih</option>
	            		<?php
	            		foreach($mreservasi as $val)
	            		{
	            			$selected = "";
	            			if($val["NoDokumen"]==$data->NoReservasi)
	            			{
								$selected = 'selected="selected"';
							}
						?>
							<option <?php echo $selected; ?> value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option>
						
						<?php
						}
	            		?>
	            	</select>  
	            	</span>
	            	
	            	<?php
	            	}
	            	?>
	            </td>
	            
	            <td class="title_table" width="150">Tour Telp.</td>
	            <td id="td_tourtelp"><?php echo $datatravel->Phone; ?></td>
	            
	        </tr>
	        
	        <tr>
	            <td class="title_table">Tour Leader <font color="red"><b>(*)</b></font></td>
	            <td>
	            	
				    Tour Leader <input type="text" class="form-control-new" value="<?php echo $data->JmlTourLeader; ?>" name="v_tl" onblur="toFormat('v_tl')" id="v_tl" maxlength="5" size="5" style="text-align: right;">
	            	&nbsp; Tour Guide <input type="text" class="form-control-new" value="<?php echo $data->JmlTourGuide; ?>" name="v_tg" onblur="toFormat('v_tg')" id="v_tg" maxlength="5" size="5" style="text-align: right;">
	            	
	            </td>
	            
	            <td class="title_table" width="150">Email</td>
	            <td id="td_touremail"><?php echo $datatravel->Email; ?></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jumlah Rombongan <font color="red"><b>(*)</b></font></td>
	            <td>
		            Dewasa<input type="text" class="form-control-new" value="<?php echo $data->Dewasa; ?>" name="v_dewasa" onblur="toFormat('v_dewasa')" id="v_dewasa" maxlength="5" size="5" style="text-align: right;">
		            &nbsp;Anak<input type="text" class="form-control-new" value="<?php echo $data->Anak; ?>" name="v_anak" onblur="toFormat('v_anak')" id="v_anak" maxlength="5" size="5" style="text-align: right;">
		            &nbsp;Batita<input type="text" class="form-control-new" value="<?php echo $data->Batita; ?>" name="v_batita" onblur="toFormat('v_batita')" id="v_batita" maxlength="5" size="5" style="text-align: right;">
	        	</td>
	            <td class="title_table" width="150">Alamat</td>
	            <td id="td_touralamat"><?php echo $datatravel->Alamat; ?></td>
	        </tr>
	        
	        <!--<tr>
	            <td class="title_table">No Stiker <font color="red"><b>(*)</b></font></td>
	            <td colspan="3"><input type="text" class="form-control-new" value="<?php echo $data->NoStiker; ?>" name="v_nostiker" id="v_nostiker" maxlength="30" size="80"></td>
	        </tr>-->
	        
	        <!--<tr>
	            <td class="title_table">GSA</td>
	            <td> 
	            	<select class="form-control-new" name="v_gsa" id="v_gsa" style="width: 200px;">
	            		<option value=""> -- Pilih -- </option>
	            		<?php
	            		foreach($gsa as $val)
	            		{
	            			$selected="";
							if($data->gsa==$val["employee_id"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["employee_id"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">No Voucher</td>
	            <td colspan="3"><input type="text" class="form-control-new" value="<?php echo $data->NoVoucher; ?>" name="v_novoucher" id="v_novoucher" maxlength="30" size="80"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td colspan="3"><input type="text" class="form-control-new" value="<?php echo $data->keterangan; ?>" name="v_keterangan" id="v_keterangan" maxlength="255" size="80"></td>
	        </tr>
			
			<tr>
	            <td class="title_table">Status</td>
	            <td colspan="3">
	            	<?php
					if($data->status==0)
					{
					?>
					<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
	            		<option value="0">Open</option>
	            		<option value="1">Close</option>
	            	</select>
					<?php	
					}
					else
					{
						echo "<b>Close</b>";	
					}
					?>
	            </td>
	        </tr>
			
			<tr>
			<td colspan="100%">
			<?php if($edit){ ?>
				<table class="table table-bordered" id="TabelDetail">
        				<thead>
							<tr>
							    <th width="100"><center>No Stiker</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="nostiker[]" id="nostiker<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	</nobr>
				                </td>
				              <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
					<?php }?>
					Detail No. Stiker					
					<table class="table table-bordered">
        				<thead>
							<tr>
							    <th width="100"><center>No Stiker</center></th>
							    <th width="100"><center>Delete</center>
							    </th>
							</tr>
						</thead>
						<tbody>
						<?php 
						foreach($detail_sticker as $key){?>
						<tr>
						<td><?php echo $key['NoSticker'];?></td>
						<td align="center">
						<?php if($edit){ ?>
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail('<?php echo $key['id']; ?>','<?php echo $key['NoDokumen']; ?>','<?php echo  $key['NoSticker']; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
						<?php }else{
							echo "X";
							} ?>				
					                </td>
						</tr>
						<?php } ?>
						</tbody>
						</table>
			</td>
			</tr>
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td colspan="3">
	            <?php
	            if($data->status==1)
	            {
				?>
					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi_konfirmasi/"; ?>">Keluar<i class="entypo-cancel"></i></button>
					<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada, Karena Status sudah Close.</font>
				<?php
				}
				else
				{
				?>
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi_konfirmasi/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        <?php	
				}
	            ?>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
	    
	    <ol class="breadcrumb title_table">
			<li><i class="entypo-vcard"></i>Information data</li>
		</ol>
		
        <table class="table table-bordered responsive">
            <tr>
            	<td class="title_table" width="150">Author</td>
	            <td><b><?php echo $data->AddUser." :: ".$data->AddDate; ?></b></td>
            </tr>
            <tr>
            	<td class="title_table" width="150">Edited</td>
	            <td><b><?php echo $data->EditUser." :: ".$data->EditDate; ?></b></td>
            </tr>
        </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
	
</div>		
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
function detailNew()
{
	var clonedRow = $("#TabelDetail tr:last").clone(true);
	var intCurrentRowId = parseFloat($('#TabelDetail tr').length )-2;
	nama = document.getElementsByName("nostiker[]");
	temp = nama[intCurrentRowId].id;
	intCurrentRowId = temp.substr(8,temp.length-8);
	var intNewRowId = parseFloat(intCurrentRowId) + 1;
	$("#nostiker" + intCurrentRowId , clonedRow ).attr( { "id" : "nostiker" + intNewRowId,"value" : ""} );
	$("#cek_trans" + intCurrentRowId , clonedRow ).attr( { "id" : "cek_trans" + intNewRowId,"value" : ""} );
	$("#bt" + intCurrentRowId , clonedRow ).attr( { "id" : "bt" + intNewRowId,"value" : ""} );
	$("#ct" + intCurrentRowId , clonedRow ).attr( { "id" : "ct" + intNewRowId,"value" : ""} );
	$("#cmb" + intCurrentRowId , clonedRow ).attr( { "id" : "cmb" + intNewRowId,"value" : ""} );
	$("#std_oh" + intCurrentRowId , clonedRow ).attr( { "id" : "std_oh" + intNewRowId,"value" : ""} );
	$("#std_res" + intCurrentRowId , clonedRow ).attr( { "id" : "std_res" + intNewRowId,"value" : ""} );
	$("#std_be" + intCurrentRowId , clonedRow ).attr( { "id" : "std_be" + intNewRowId,"value" : ""} );
	$("#btn_del_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_" + intNewRowId} );
	$("#TabelDetail").append(clonedRow);
	$("#TabelDetail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
	$("#nostiker" + intNewRowId).focus();
	ClearBaris(intNewRowId);
}

function ClearBaris(id)
{
	$("#nostiker"+id).val("");
	$("#cek_trans"+id).val("0");
	$("#bt"+id).val("0");
	$("#ct"+id).val("0");
	$("#cmb"+id).val("0");
	$("#std_oh"+id).val("0");
	$("#std_res"+id).val("0");
	$("#std_be"+id).val("0");
}

function deleteRow(obj)
{
	objek = obj.id;
	id = objek.substr(15,objek.length-3);
	
	var lastRow = document.getElementsByName("nostiker[]").length;
	
	if( lastRow > 1)
	{
		$('#baris'+id).remove();
	}else{
			alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
	}
}

function deleteDetail(id,nodok,nostiker,url)
	{
		var r=confirm("Apakah Anda Ingin Menghapus No.Stiker "+nostiker+" ?")
		if (r==true)
		{
			window.location = url+"index.php/transaksi/reservasi_konfirmasi/delete_detail_sticker/"+id+"/"+nodok+"/"+nostiker+"";	
		}
		else
		{
	  		return false;
		}
	}
</script>