<?php 

$this->load->view('header'); 

$modul = "Reservasi Konfirmasi";

?>
<script language="javascript" src="<?=base_url();?>public/js/reservasi_konfirmasi.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
			<span id="span_loading" style="float: right; display: none;"><img src="../../../../public/images/ajax-image.gif"/></span>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/reservasi_konfirmasi/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        <input type="hidden" name="v_tourtravel" id="v_tourtravel" value="" />
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>	        
	            
	            <td class="title_table" colspan="2">Informasi Tour</td>
	                
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td width="500"> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="12" maxlength="10">
	            </td>
	            
	            <td class="title_table" width="150">Tour Group</td>
	            <td id="td_tourgroup"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Jam <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input class="form-control-new timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="11:25 AM" name="v_jam" id="v_jam" data-show-meridian="true" data-minute-step="5" data-second-step="5" type="text" size="12" maxlength="12">
	            </td>
	            
	            <td class="title_table" width="150">Tour PIC</td>
	            <td id="td_tourpic"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No Reservasi <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" value="" name="v_keyword_noreservasi" id="v_keyword_noreservasi" size="25" onkeyup="CallAjax('search_keyword_noreservasi', this.value)">&nbsp;
	            	<span id="span_noreservasi">
	            	<select class="form-control-new" name="v_noreservasi" id="v_noreservasi" style="width: 270px;" onchange="CallAjax('ajax_noreservasi', this.value)">
	            		<option value="">Pilih</option>
	            		<?php
	            		foreach($mreservasi as $val)
	            		{
						?>
							<option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option>
						
						<?php
						}
	            		?>
	            	</select>  
	            	</span>
	            </td>
	            
	            <td class="title_table" width="150">Tour Telp.</td>
	            <td id="td_tourtelp"></td>
	            
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jumlah TL / TG<font color="red"><b>(*)</b></font></td>
	            <td>
	            	<!--<span id="span_keyword_tourleader"><input type="text" class="form-control-new" value="" name="v_keyword_tourleader" id="v_keyword_tourleader" size="15" onkeyup="CallAjax('search_keyword_tourleader', this.value)">&nbsp;</span>
	            	<span id="span_tourleader">
	            	<select class="form-control-new" name="v_tourleader" id="v_tourleader" style="width: 255px;">
	            		<option value="">Pilih</option>
	            		<?php
	            		foreach($mtourleader as $val)
	            		{
						?>
							<option value="<?php echo $val["KdTourLeader"]; ?>"><?php echo $val["Nama"]; ?></option>
						
						<?php
						}
	            		?>
	            	</select>  
	            	</span>
	            	<span id="span_tourleader_text" style="display: none;"><input type="text" name="v_tourleader_new" id="v_tourleader_new" value="" class="form-control-new" size="67"/></span>
	            	<label><input type="checkbox" name="chk_tourleader" id="chk_tourleader" class="text" onclick="change_tourleader()" />&nbsp;Lainnya</label>-->
					Tour Leader <input type="text" class="form-control-new" value="" name="v_tl" onblur="toFormat('v_tl')" id="v_tl" maxlength="5" size="5" style="text-align: right;">
	            	&nbsp; Tour Guide <input type="text" class="form-control-new" value="" name="v_tg" onblur="toFormat('v_tg')" id="v_tg" maxlength="5" size="5" style="text-align: right;">
	            	
	            </td>
	        
	            <td class="title_table" width="150">Email</td>
	            <td id="td_touremail"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jumlah Rombongan <font color="red"><b>(*)</b></font></td>
	            <td>
	            	Dewasa <input type="text" class="form-control-new" value="" name="v_dewasa" onblur="toFormat('v_dewasa')" id="v_dewasa" maxlength="5" size="5" style="text-align: right;">
	            	&nbsp; Anak <input type="text" class="form-control-new" value="" name="v_anak" onblur="toFormat('v_anak')" id="v_anak" maxlength="5" size="5" style="text-align: right;">
	            	&nbsp; Batita <input type="text" class="form-control-new" value="" name="v_batita" onblur="toFormat('v_batita')" id="v_batita" maxlength="5" size="5" style="text-align: right;">
	            </td>
	        
	            <td class="title_table" width="150">Alamat</td>
	            <td id="td_touralamat"></td>
	        </tr>
	        
	        <!--<tr>
	            <td class="title_table">No Stiker <font color="red"><b>(*)</b></font></td>
	            <td colspan="3"><input type="text" class="form-control-new" value="" name="v_nostiker" id="v_nostiker" maxlength="4" size="80"></td>
	        </tr>-->
			
	        <!--<tr>
	            <td class="title_table">GSA<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gsa" id="v_gsa" style="width: 200px;">
	            		<option value=""> -- Pilih -- </option>
	            		<?php
	            		foreach($gsa as $val)
	            		{
							?><option value="<?php echo $val["employee_id"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">No Voucher</td>
	            <td colspan="3"><input type="text" class="form-control-new" value="" name="v_novoucher" id="v_novoucher" maxlength="30" size="80"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td colspan="3"><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="80"></td>
	        </tr>
			
			<tr>
				<td colspan="100%">
				<span id='cek_stiker'></span>
				<table class="table table-bordered responsive" id="TabelDetail">
        				<thead>
							<tr>
							    <td width="100" class="title_table"><center>No Stiker</center></td>
							    <td width="100" class="title_table"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </td>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="nostiker[]" id="nostiker<?php echo $no;?>" value="" style="width: 100px;" onblur="jml_digit(this)"/>
				                	</nobr>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
				</td>
			</tr>
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td colspan="3">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/reservasi_konfirmasi/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
function detailNew()
{
	var clonedRow = $("#TabelDetail tr:last").clone(true);
	var intCurrentRowId = parseFloat($('#TabelDetail tr').length )-2;
	nama = document.getElementsByName("nostiker[]");
	temp = nama[intCurrentRowId].id;
	intCurrentRowId = temp.substr(8,temp.length-8);
	var intNewRowId = parseFloat(intCurrentRowId) + 1;
	$("#nostiker" + intCurrentRowId , clonedRow ).attr( { "id" : "nostiker" + intNewRowId,"value" : ""} );
	$("#cek_trans" + intCurrentRowId , clonedRow ).attr( { "id" : "cek_trans" + intNewRowId,"value" : ""} );
	$("#bt" + intCurrentRowId , clonedRow ).attr( { "id" : "bt" + intNewRowId,"value" : ""} );
	$("#ct" + intCurrentRowId , clonedRow ).attr( { "id" : "ct" + intNewRowId,"value" : ""} );
	$("#cmb" + intCurrentRowId , clonedRow ).attr( { "id" : "cmb" + intNewRowId,"value" : ""} );
	$("#std_oh" + intCurrentRowId , clonedRow ).attr( { "id" : "std_oh" + intNewRowId,"value" : ""} );
	$("#std_res" + intCurrentRowId , clonedRow ).attr( { "id" : "std_res" + intNewRowId,"value" : ""} );
	$("#std_be" + intCurrentRowId , clonedRow ).attr( { "id" : "std_be" + intNewRowId,"value" : ""} );
	$("#btn_del_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_" + intNewRowId} );
	$("#TabelDetail").append(clonedRow);
	$("#TabelDetail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
	$("#nostiker" + intNewRowId).focus();
	ClearBaris(intNewRowId);
}

function ClearBaris(id)
{
	$("#nostiker"+id).val("");
	$("#cek_trans"+id).val("0");
	$("#bt"+id).val("0");
	$("#ct"+id).val("0");
	$("#cmb"+id).val("0");
	$("#std_oh"+id).val("0");
	$("#std_res"+id).val("0");
	$("#std_be"+id).val("0");
}

function deleteRow(obj)
{
	objek = obj.id;
	id = objek.substr(15,objek.length-3);
	
	var lastRow = document.getElementsByName("nostiker[]").length;
	
	if( lastRow > 1)
	{
		$('#baris'+id).remove();
	}else{
			alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
	}
}

function jml_digit(obj)
{
	objek = obj.id;
	id = objek.substr(8,objek.length-8);
	
	stiker = $('#nostiker'+id).val();
	
	panjang = stiker.length;
    
    if(panjang>4){
	alert('No Stiker Yang Di Izinkan Hanya 4 Karakter.');	
	$('#nostiker'+id).val("");
	return false;
	}
	
	//cek apakah no. stiker tersebut sudah digunkan ditanggal yang sama namun beo yang berbeda.
	beo = $('#v_noreservasi').val();
	tgl = $('#v_tgl_dokumen').val();
	if(beo==""){
		alert('No. Reservasi Harus Di Pilih...');
		$('#nostiker'+id).val("");	
	    return false;
	}else if(stiker!=""){
		
		document.getElementById("cek_stiker").innerHTML = "Cek No.Stiker...";
		$('#btn_save').attr('disabled',true);
		$('#btn_tambah_baris').attr('disabled',true);
		$('#btn_del_detail_'+id).attr('disabled',true);
		
		
		$.ajax({
            url: "<?php echo site_url('transaksi/reservasi_konfirmasi/cek_stiker') ?>/"+stiker+"/"+tgl+"/"+beo,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data.status==true){
                  	document.getElementById("cek_stiker").innerHTML = "Duplicate";
				  	alert('No Stiker '+stiker+' Duplicate dengan No. Reservasi lain di Tanggal '+tgl+' ini.');	
					$('#nostiker'+id).val("");
					document.getElementById("cek_stiker").innerHTML = "";
					$('#btn_save').attr('disabled',false);
					$('#btn_tambah_baris').attr('disabled',false);
					$('#btn_del_detail_'+id).attr('disabled',false);
					return false;
				  }else{
				  	//document.getElementById("cek_stiker").innerHTML = "Ok";
				  	document.getElementById("cek_stiker").innerHTML = "";
				  	$('#btn_save').attr('disabled',false);
					$('#btn_tambah_baris').attr('disabled',false);
					$('#btn_del_detail_'+id).attr('disabled',false);
				  }     		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );	
        
	}
	

}
</script>