<?php $this->load->view('header'); ?>

<script language="javascript" src="<?=base_url();?>public/js/reservasi_konfirmasi.js"></script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/reservasi_konfirmasi/search'>
	<input type="hidden" name="btn_search" id="btn_search" value="y"/>
	<div class="row">
		<div class="col-md-10">
			<b>Search</b>&nbsp;
			<input type="text" size="30" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" />
		</div>
		
		<div class="col-md-2" align="right">
			<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
			<a href="<?php echo base_url()."index.php/transaksi/reservasi_konfirmasi/add_new/"; ?>" onClick="show_loading_bar(100)" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
		</div>
	</div>
</form>

<hr/>

<?php
if($this->session->flashdata('msg'))
{
  $msg = $this->session->flashdata('msg');
  
  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
}
?>
	
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
	<table class="table table-bordered responsive">
        <thead class="title_table">
			<tr>
				<th width="30"><center>No</center></th>
				<th width="100"><center>Tanggal</center></th>
				<th width="100"><center>Jam</center></th>
				<th width="100"><center>No Dokumen</center></th>
				<th width="200"><center>No Reservasi</center></th>
				<!--=<th width="250"><center>Tour Leader</center></th>-->
				<!--<th width="100"><center>No Stiker</center></th>-->
				<th width="250"><center>No Voucher</center></th>
				<th width="100"><center>Jumlah</center></th>
				<th width="100"><center>Status</center></th>
				<th width="100"><center>Navigasi</center></th>
			</tr>
		</thead>
		<tbody>
		
		<?php
		if(count($data)==0)
		{
			echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
		}
		
		$no=1;
		foreach($data as $val)
		{
            $bgcolor = "";
            if($no%2==0)
            {
                $bgcolor = "background:#f7f7f7;";
            }
            
            if($val["status"]==0)
            {
				$status = "Open";
			}
			else
			{
				$status = "Close";
			}
            
			?>
			<tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
				<td><?php echo $no; ?></td>
                <td align="center"><?php echo $val["TglKonfirmasi"]; ?></td>
                <td align="center"><?php echo $val["JamKonfirmasi"]; ?></td>
                <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                <td align="center"><?php echo $val["NoReservasi"]; ?></td>
                <!--<td><?php echo $val["TourLeaderName"]; ?></td>-->
                <!--<td><?php echo $val["NoStiker"]; ?></td>-->
                <td><?php echo $val["NoVoucher"]; ?></td>
                <td align="right"><?php echo $val["JumlahRombongan"]; ?></td>
                <td align="center"><?php echo $status; ?></td>
                <td align="center">
	               
					<a href="<?php echo base_url();?>index.php/transaksi/reservasi_konfirmasi/edit_form/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
        			
        			<?php
        			if($val["status"]==1)
        			{
        			?>
	        			<!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?php echo $val["NoDokumen"]; ?>','<?php echo base_url(); ?>');" >
							<i class="entypo-print"></i>
						</button>-->
					<?php
					}
        			?>
        			
					
					
                </td>
            </tr>
			<?php	
			
			$no++;			
		}
		?>
		
		</tbody>
	</table>

	<div class="row">
		<div class="col-xs-6 col-left">
			<div id="table-2_info" class="dataTables_info">&nbsp;</div>
		</div>
		<div class="col-xs-6 col-right">
			<div class="dataTables_paginate paging_bootstrap">
				<?php echo $pagination; ?>
			</div>
		</div>
	</div>	
	
</div>
	

<?php $this->load->view('footer'); ?>
