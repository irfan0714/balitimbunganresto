<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Sales Invoice";

$counter=1;
foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script language="javascript" src="<?=base_url();?>public/js/sales_invoice.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/sales_invoice/save_data'>
	    <input type="hidden" name="v_invoiceno" id="v_invoiceno" value="<?php echo $header->invoiceno; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->invoiceno; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal Invoice </td>
	            <td> 
					
					<input type="text" class="form-control-new datepicker" value="<?php if($header->sidate_indo!="" && $header->sidate_indo!="00-00-0000") { echo $header->sidate_indo; }else{echo date('d-m-Y');}  ?>" name="v_invoice_date" id="v_invoice_date" size="10" maxlength="10">	
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Due Date </td>
	            <td> 
					
					<input type="text" class="form-control-new datepicker" value="<?php if($header->duedate_indo!="" && $header->duedate_indo!="00-00-0000") { echo $header->duedate_indo; }else{echo date('d-m-Y');}  ?>" name="v_invoice_due_date" id="v_invoice_due_date" size="10" maxlength="10">	
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Customer </td>
	            <td> 
	            	<select class="form-control-new" name="v_customer" id="v_customer" style="width: 25%;">
	            		<option value="">Pilih Customer</option>
	            		<?php
	            		foreach($customer as $val)
	            		{
	            			$selected="";
							if($header->customerid==$val["KdCustomer"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdCustomer"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
			
			<tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Currency", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="48"></td>
	        </tr>
	        
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Close</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
			<?php
											if($header->status==0 or $header->status==2)
											{
											?>
            <tr>
                <td colspan="100%"  align="right">
                    <a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-md md-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title="">Add Item<i class="entypo-search"></i></a>
                     </td>
            </tr>
           <?php } ?>
			<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor DO</center></th>
								<th width="80"><center>PCode</center></th>
							    <th><center>Item</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>Total</center></th>
								<?php
											if($header->status==0)
											{
											?>
							    <!--<th width="50"><center>
							    	Action</center>-->
							    </th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($datado)<=0){
						 	echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
						 }else{
                            $sum_total =0;
                            $sum_disc =0;
						 	foreach($datado as $val){
						 		$total=$val['gross'] * $val['quantity'];
								?>
								<tr>
									<td><?php echo $val['dono']; ?></td>
									<td><?php echo $val['inventorycode']; ?></td>
									<td><?php echo $val['NamaLengkap']; ?></td>
									<td align='center'><?php echo $val['quantity']; ?></td>
									<td align='center'><?php echo $val['SatuanSt']; ?></td>
									<td align='right'><?php echo number_format($val['gross'],0); ?></td>
									<td align='right'><?php echo number_format($total,0); ?></td>

											<?php
											if($header->status==1)
											{
											?>
									<!--<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail2('<?php echo $val["salesinvoicedetailid"]; ?>','<?php echo $val["inventorycode"]; ?>','<?php echo $val["dono"]; ?>','<?php echo base_url(); ?>','<?php echo $header->invoiceno;; ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>-->
									<?php }?>
								</tr>
								<?php
                                $sum_total += $total;
                                $sum_disc += $val['disc'] * $val['quantity'];
							}
						 }
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
		   
			<?php
			if($header->status==0 or $header->status==2)
			{
			?>
				        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/sales_invoice/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/sales_invoice/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
		
	        
	    </table>
		
			
		<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data Sales Invoice</li>
			</ol>
			
	         <table class="table table-bordered responsive">
			 <?php 
			    //diskon
				// if($val['diskon']==0){
    // 				$potongan_diskon = 0;
				// }else{
    // 				$potongan_diskon = ($val['diskon']/100)*$val['total'];
				// }
				
				//ppn
                $potongan_ppn=(10/100)* ($sum_total-$sum_disc);
                
                //gradtotal
                $grandtotal=($sum_total-$sum_disc)+$potongan_ppn;
                
				
			 ?>
	            <tr>
	            	<td align="right" width="87%">Total</td>
		            <td align="right">
					    <?php echo number_format($sum_total,0); ?>
		            	<input type="hidden" class="form-control-new" value="<?php echo number_format($sum_total,0); ?>"  maxlength="255" size="100%">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">Discount <?php echo  abs($header->discpercentage); ?>%</td>
		            <td align="right">
					    <?php echo number_format($sum_disc,0); ?>
		            	<input type="hidden" class="form-control-new" value="<?php echo number_format($sum_disc,0); ?>"  maxlength="255">
		            	<input type="hidden" class="form-control-new" value="<?php echo number_format($potongan_diskon,0); ?>"  maxlength="255">		            		            	
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">PPN 10%</td>
		            <td align="right">
					    <?php echo number_format($potongan_ppn,0); ?>
		            	<input type="hidden" class="form-control-new" value="<?php echo "10"; ?>" maxlength="255" size="25%">
		            	<input type="hidden" class="form-control-new" value="<?php echo number_format($potongan_ppn,0); ?>"  maxlength="255" >
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150"><b>Grand Total</b></td>
		            <td align="right">
					    <b><?php echo number_format($grandtotal,0); ?></b>
		            	<input type="hidden" class="form-control-new" value="<?php echo $grandtotal; ?>" name="v_grandtotal" id="v_grandtotal" maxlength="255" >
		            </td>
	            </tr>
	         </table>
		
	    </form> 
	    
	    <?php
        if($header->invoiceno)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>