<?php 

$this->load->view('header'); 

$modul = "Sales Invoice";

?>

<script language="javascript" src="<?=base_url();?>public/js/sales_invoice.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/sales_invoice/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Invoice Date <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_invoice_date" id="v_invoice_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Due Date <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_invoice_due_date" id="v_invoice_due_date" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Customer <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_customer" id="v_customer" style="width: 25%;">
	            		<option value="">Pilih Customer</option>
	            		<?php
	            		foreach($customer as $val)
	            		{
							?><option value="<?php echo $val["KdCustomer"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Currency", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	       
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="48%"></td>
	        </tr>
	        
	        <!--<tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;">
		            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
		            		<!--<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Pending</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Close</option>
		            	</select>
		            </td>
		        </tr>-->
            
            <tr>
                <td colspan="100%"  align="right">
                    
						<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-md md-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title="">Add Item<i class="entypo-search"></i></a>
					
                    </td>
            </tr>
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor DO</center></th>
								<th width="80"><center>PCode</center></th>
							    <th><center>Item</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>Total</center></th>
							    <!--<th width="50"><center>
							    	Action</center>
							    </th>-->
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($datado)<=0){
						 	echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
						 }else{
						 	foreach($datado as $val){
						 		$total=$val['nettprice']*$val['quantity'];
								?>
								<tr>
									<td><?php echo $val['dono']; ?></td>
									<td><?php echo $val['inventorycode']; ?></td>
									<td><?php echo $val['NamaLengkap']; ?></td>
									<td align='center'><?php echo $val['quantity']; ?></td>
									<td align='center'><?php echo $val['SatuanSt']; ?></td>
									<td align='right'><?php echo number_format($val['nettprice'],0); ?></td>
									<td align='right'><?php echo number_format($total,0); ?></td>
									<!--<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail('<?php echo $val["salesinvoicedetailid"]; ?>','<?php echo $val["inventorycode"]; ?>','<?php echo $val["dono"]; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>-->
								</tr>
								<?php
							}
						 }
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="batal('<?php echo base_url(); ?>');">Batal<i class="entypo-check"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                </td>
	        </tr>
	        
	    </table>
	    
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
			 <?php foreach($hitungdo as $val){
			    //diskon
				if($val['diskon']==0){
				$potongan_diskon = 0;
				}else{
				$potongan_diskon = ($val['diskon']/100)*$val['total'];
				}
				
				//ppn
				$potongan_ppn=(10/100)*$val['total'];
				
				//gradtotal
				$grandtotal=($val['total']-$potongan_diskon)+$potongan_ppn;
				
			 ?>
	            <tr>
	            	<td align="right" width="87%">Total</td>
		            <td align="right">
					    <?php echo number_format($val['total'],0); ?>
		            	<input type="hidden" class="form-control-new" id="nTotal" value="<?php echo $val['total']; ?>"  maxlength="255" size="100%">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">
                        Discount <input type="test" style="text-align: right;width: 50px;" name="pDiscount" maxlength="2" id="pDiscount" value="<?php echo $val['diskon']*1; ?>"> %
                    </td>
		            <td align="right">
					    <p id="vDiscount"><?php echo number_format($potongan_diskon,0); ?></p>
		            	<input type="hidden" id="nDiscount" class="form-control-new" value="<?php echo $val['diskon']; ?>"  maxlength="255">
		            	<input type="hidden" class="form-control-new" value="<?php echo number_format($potongan_diskon,0); ?>"  maxlength="255">		            		            	
		            </td>
	            </tr>  
	            <tr>
	            	<td align="right" width="150">PPN 10%</td>
		            <td align="right">
					    <p id="vPPN"><?php echo number_format($potongan_ppn,0); ?></p>
		            	<input type="hidden" class="form-control-new" value="<?php echo "10"; ?>" maxlength="255" size="25%">
		            	<input type="hidden" name="nPPN" id="nPPN" class="form-control-new" value="<?php echo $potongan_ppn; ?>"  maxlength="255" >
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150"><b>Grand Total</b></td>
		            <td align="right">
					    <b id="gt"><?php echo number_format($grandtotal,0); ?></b>
		            	<input type="hidden" class="form-control-new" value="<?php echo $grandtotal; ?>" name="v_grandtotal" id="v_grandtotal" maxlength="255" >
		            </td>
	            </tr>
				<?php } ?>
	         </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
    $("#pDiscount").keyup(function(){
        var persen = $(this).val();
        var total = $("#nTotal").val();

        nilDiscount = Math.ceil(parseInt(total) * (persen/100));
        tot = total - nilDiscount;
        nilPPN = Math.ceil(parseInt(tot) * (10/100));

        grandTotal = tot +  Math.ceil(nilPPN);
        vnilDiscount = formatRupiah(nilDiscount.toString());
        $("#vDiscount").html(vnilDiscount);
        $("#nDiscount").val(nilDiscount);
        vnilPPN = formatRupiah(nilPPN.toString());
        $("#vPPN").html(vnilPPN);
        $("#nPPN").val(nilPPN);
        vgt = formatRupiah(grandTotal.toString());
        $("#gt").html(vgt);
        $("#v_grandtotal").val(grandTotal);

    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
     
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }
     
        rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ?   rupiah : '');
    }
</script>