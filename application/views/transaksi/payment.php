<?php
class payment extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/keuangan/paymentmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '15';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/keuangan/payment/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/keuangan/payment/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/keuangan/payment/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$thnbln = $data['tahun'].$data['bulan'];
			$data['header']		 		= array("No Dokumen","Tanggal","Kas Bank","Jumlah","Nomor Bukti","Keterangan","Username");
	        $config['total_rows']		= $this->paymentmodel->num_payment_row($id,$with,$thnbln);		
	        $data['data']	= $this->paymentmodel->getpaymentList($config['per_page'],$page,$id,$with,$thnbln);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/keuangan/payment/paymentlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new($pesan){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->paymentmodel->getDate();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$tgl = '01'.'-'.$data['bulan'].'-'.$data['tahun'];
			$data['tanggal'] = $aplikasi->TglTrans;
			$data['aplikasi'] = $aplikasi;
			if(($data['tahun']==substr($data['tanggal'],-4))&&($data['bulan']==substr($data['tanggal'],3,2)))
			     $data['tanggal'] = $aplikasi->TglTrans;
		    else
			     $data['tanggal'] = $tgl;
			$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			$data['mkasbank'] = $this->paymentmodel->getKasBank();
			$data['mpersonal'] = $this->paymentmodel->getPersonal();
			$data['mcostcenter'] = $this->paymentmodel->getCostCenter();
			$data['pesan'] = $pesan;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/keuangan/payment/add_payment',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "payment.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
//		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
//		$data['string2'] = "(                     )         (                      )";
        $data['string1'] = "     Dibuat Oleh                Diperiksa Oleh                   Menyetujui";
        $data['string2'] = "(                     )    (                      )       (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_keuangan', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$id = $this->uri->segment(4);
		$header	 = $this->paymentmodel->getHeader($id);
		$data['header']	 = $header;
		$detail	 = $this->paymentmodel->getDetailForPrint($id);
		$data['judul1'] = array("No Payment","Kas Bank","Keterangan");
		$data['niljudul1'] = array($header->NoDokumen,$header->KdKasBank." - ".stripslashes($header->NamaKasBank),stripslashes($header->Keterangan));
		$data['judul2'] = array("Tanggal","Nomor Bukti");
		$data['niljudul2'] = array($header->Tanggal,$header->NoBukti);
		$data['judullap'] = "Payment Voucher";
		$data['url'] = "payment/printThis/".$id;
		$data['colspan_line'] = 4;
        $data['lebar_detail'] = array(15,30,30,15);// total 95  Rekening 	Nama 	Keterangan 	Jumlah
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("Rekening","Nama","Keterangan","Jumlah");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0,0);
		for($m=0;$m<count($detail);$m++)
		{
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['KdRekening']);
			$list_detail[] = stripslashes($detail[$m]['NamaRekening']);
			$list_detail[] = stripslashes($detail[$m]['Keterangan']);
			$list_detail[] = number_format($detail[$m]['Jumlah'],2,",",".");
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array("Jumlah Payment");
		$data['footer2']  = array(number_format($header->JumlahPayment,2,",","."));
        $data['brs_footer']= array(20,3,15);
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		$data['string1'] = "Dibuat Oleh";
		$data['string2'] = "Menyetujui";
		$data['string3'] = "(____________________)";
		$data['string4'] = "(____________________)";
		return $data;
	}

    function delete_payment(){
     	$id = $this->input->post('kode');
		//$datagiro	= $this->paymentmodel->getGiro($id);
		$lanjut = 1;
		$ret = "";
//		if(count($datagiro)>0)
//		{
//		   if($datagiro[0]['Status']=='C')
//		   {
//		      $lanjut = 0;
//			  $ret    = "girocair";
//		   }
//        }
		
		if($lanjut==1)
		{
			$this->paymentmodel->locktables('trans_payment_header,trans_payment_detail');
			$this->db->update('trans_payment_header', array('Status'=>'B'), array('NoDokumen' => $id));
			$this->db->update('trans_payment_detail', array('Status'=>'B'), array('NoDokumen' => $id));
//			$this->db->delete('bukugiro', array('NoTransaksi' => $id, 'Jenis' => 'P'));
			$this->paymentmodel->unlocktables();
		}
		echo $ret;   
	}

    function edit_payment($id){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$data['header']	= $this->paymentmodel->getHeader($id);
	    	$data['detail']	= $this->paymentmodel->getDetail($id);
			$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			$data['mkasbank'] = $this->paymentmodel->getKasBank();
			$data['mpersonal'] = $this->paymentmodel->getPersonal();
			$data['mcostcenter'] = $this->paymentmodel->getCostCenter();
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/keuangan/payment/edit_payment', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	
	function getkdrekening()
	{
		$kode = $this->input->post('kdrekening');
		$valreken = $this->paymentmodel->findrekening($kode);
		if(count($valreken)!=0)
		{
			$kode = $valreken->kdrekening."*-*".$valreken->namarekening;
		}
		else
		{
			$kode = "";
		}
		echo $kode;
	}

	function save_new_payment(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$kasbank = $this->input->post('kasbank');
		$personal = $this->input->post('personal');
		$costcenter = $this->input->post('costcenter');
		/*
		$nogiro = strtoupper(addslashes($this->input->post('nogiro')));
		$tglcair = $this->input->post('tglcair');
		$bankcair = $this->input->post('bankcair');
		*/
		$nogiro = "";
		$tglcair = "0000-00-00";
		$bankcair = "";
		$nobukti = strtoupper(addslashes($this->input->post('nobukti')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = $this->input->post('kdrekening');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$counter = $this->input->post('urutan');
		if(($jenis=="2")&&($no==""))
		{
	       $cekgiro = $this->paymentmodel->cekgiro($nogiro);
		   $adagiro = count($cekgiro);
		}
		else
		   $adagiro = "";
		if($adagiro=="")
		{
			if($no=="")
			{
				$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair);
			}
			else
			{
				$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair);
			}
			for($x=0;$x<count($kdrekening);$x++)
			{
				$kdrekening1 = strtoupper(addslashes(trim($kdrekening[$x])));
				$jumlah1 = $jumlah[$x];
				$savekdrekening1 = $savekdrekening[$x];
				$keterangan1 = $keterangan[$x];
				$counter1 = $counter[$x];
				if($kdrekening1!=""){
					$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter1,$kdrekening1,$jumlah1,$keterangan1,$user,$savekdrekening1);
				}
			}
			redirect('/keuangan/payment/');
		}else
		{
		  $this->add_new($nogiro);
		}
	}
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$kasbank = $this->input->post('kasbank');
		$personal = $this->input->post('personal');
		$costcenter = $this->input->post('costcenter');
		/*
		$nogiro = strtoupper(addslashes($this->input->post('nogiro')));
		$tglcair = $this->input->post('tglcair');
		$bankcair = $this->input->post('bankcair');
		*/
		$nogiro = "";
		$tglcair = "0000-00-00";
		$bankcair = "";
		$nobukti = strtoupper(addslashes($this->input->post('nobukti')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = strtoupper(addslashes($this->input->post('kdrekening')));
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$counter = $this->input->post('urutan');
		$jumlahpayment = $this->input->post('jumlahpayment');
		if(($jenis=="2")&&($no==""))
		{
	       $cekgiro = $this->paymentmodel->cekgiro($nogiro);
		   $adagiro = count($cekgiro);
		}
		else
		   $adagiro = "";
		if($adagiro=="")
		{
			if($no=="")
			{
				$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair);
			}
			else
			{
				$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair);
			}
			$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening);
			echo $no;
		}
		else
		{
		    echo "adagiro";
		}
	}

	function insertNewHeader($flag,$tgl,$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$tglcair,$bankcair)
	{
		$this->paymentmodel->locktables('counter,trans_payment_header');
		$new_no = $this->paymentmodel->getNewNo($tgl);
		$no = $new_no->NoPayment;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'NoDokumen'	=> $no,
			'TglDokumen' => $tgl,
			'Jenis' => $jenis,
			'KdKasBank' => $kasbank,
			'KdCostCenter' => $costcenter,
			'KdPersonal' => $personal,
			'NoGiro' => $nogiro,
			'KdBankCair' => $bankcair,
			'TglCair' => $tglcair,
			'NoBukti' => $nobukti,
			'Keterangan' => $ket,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'JumlahPayment' => $jumlahpayment,
			'Status' => ' '
		);
		$this->db->insert('trans_payment_header', $data);
//		if($jenis=='2')
//		{
//			$data = array(
//				'NoGiro' => $nogiro,
//				'NoTransaksi' => $no,
//				'TglTerima' => $tgl,
//				'TglJTo' => $tglcair,
//				'NilaiGiro' => $jumlahpayment,
//				'Jenis' => 'P',
//				'Status' => 'B',
//				'BankCair' => $bankcair,
//				'Keterangan' => $ket,
//				'UserTrans' => $user
//			);
//			$this->db->insert('bukugiro', $data);
//		}
		$this->paymentmodel->unlocktables();
		return $no;
	}
	function updateHeader($flag,$no,$tgl,$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$tglcair,$bankcair)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_header');
		if($jenis=='1')
		{
		   $nogiro = "";
		   $bankcair = "";
		   $tglcair = "0000-00-00";
		}
		$data = array(
		    'TglDokumen' => $tgl,
			'Jenis' => $jenis,
		    'KdKasBank' => $kasbank,
			'KdCostCenter' => $costcenter,
			'KdPersonal' => $personal,
			'NoGiro' => $nogiro,
			'KdBankCair' => $bankcair,
			'TglCair' => $tglcair,
			'NoBukti' => $nobukti,
			'Keterangan' => $ket,
			'JumlahPayment' => $jumlahpayment
		);
		if($flag=="edit")
		{
			$data['EditDate'] = $tgl1;
			$data['EditUser'] = $user;
			$this->db->update('trans_payment_detail', array('EditDate'=> $tgl1,'EditUser'=>$user), array('NoDokumen' => $no));
		}
		$this->db->update('trans_payment_header', $data, array('NoDokumen' => $no));
//		$this->db->delete('bukugiro', array('NoTransaksi' => $no, 'Jenis' => 'P'));
//		if($jenis=='2')
//		{
//			$data = array(
//				'NoGiro' => $nogiro,
//				'NoTransaksi' => $no,
//				'TglTerima' => $tgl,
//				'TglJTo' => $tglcair,
//				'NilaiGiro' => $jumlahpayment,
//				'Jenis' => 'P',
//				'Status' => 'B',
//				'BankCair' => $bankcair,
//				'Keterangan' => $ket,
//				'UserTrans' => $user
//			);
//			$this->db->insert('bukugiro', $data);
//		}
		$this->paymentmodel->unlocktables();
	}

	function insertDetail($flag,$no,$tgl,$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_detail');
		if($savekdrekening==""){
			$data = array(
				'NoDokumen'	=> $no,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
				'Keterangan' => $keterangan,
				'Urutan'	=> $counter,
				'Status' => ' '
			);
			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->insert('trans_payment_detail', $data);
		}
		else 
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
				'Keterangan' => $keterangan
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_payment_detail', $data, array('NoDokumen' => $no,'Urutan'=>$counter));
		}
		$this->paymentmodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$counter = $this->input->post('urutan');
		$kdrekening = $this->input->post('kdrekening');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$this->paymentmodel->locktables('trans_payment_detail');
		$this->db->update('trans_payment_detail', array('Status' => 'B') ,array('NoDokumen' => $id,'Urutan'=>$counter));
		$this->db->update('trans_payment_header', array('JumlahPayment' => $jumlahpayment) ,array('NoDokumen' => $id));
		$this->paymentmodel->unlocktables();
	}

}
?>