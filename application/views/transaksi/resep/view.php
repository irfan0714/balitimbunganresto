<?php
$this->load->view('header');
$searchby = $this->input->post('searchby');
$date1 = $this->input->post('date1');
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<!--<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>-->
<script language="javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script language="javascript">
    $(function () {
                $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy'
                });
            });
    
    function PopUpPrint(kode, baseurl)
    {
        url = "index.php/transaksi/resep/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }

    function PopUpExcel(kode, baseurl)
    {
        url = "index.php/transaksi/resep/cetakexcel/" + escape(kode);
        window.location = baseurl+ url;
    }
	
	function cetakprint(kode, baseurl)
    {
        url = "index.php/transaksi/resep/cetakprinter/" + escape(kode);
        window.location=baseurl + url;
    }
	
   
    
    function start_page()
    {
        document.getElementById("stSearchingKey").focus();
    }
</script>
<body onload="/* option(); */ start_page();">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$judul?>
                </div>
            </div>
            <div class="panel-body">
              <form method="POST"  name="search" action="<?=base_url();?>index.php/transaksi/resep/">
                    <div class="control-group">
                        <div class="controls">
                            <table border="0" align="right">
                                <tr>
                                    <td>
                                        <div class="panel-title">
                                           Standard Menu
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="menu" class="form-control-new">
                                    &nbsp;
                                    &nbsp;
                                    </td>
                                    <td style="display">
                                        <input type="submit" name="submit" value="Submit" class='btn btn-default'>
                                    </td>
                                </tr>
                            </table>

                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </form>

                <br>
                <table align = 'center' class='table table-bordered table-hover responsive'>
                    <thead>
                        <tr>
                            <th nowrap><center>Nama Menu</center></th>  
                            <?php
                            if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <th nowrap colspan="2"><center>Action</center></th>
                            <?php } 
                            $mylib = new globallib(); ?>
                        </tr>
                    </thead>
                    <br>
                    <?php
                    if (count($data) == 0) {
                        ?>
                        <td nowrap colspan="2" align="center">Tidak Ada Data</td>
                        <?php
                    }
                    for ($a = 0; $a < count($data); $a++) {
                        ?>
                    <form action="<?= base_url(); ?>index.php/transaksi/resep/edit/<?= $data[$a]['resep_id']; ?>" method="post">
                    
                        <tr>
                            <td nowrap><?= $data[$a]['NamaLengkap']; ?></td>

                            <input type="hidden" name="namamenu" value="<?= $data[$a]['NamaLengkap']; ?>">
                            <input type="hidden" name="pcodenamamenu" value="<?= $data[$a]['Pcode']; ?>">

                            <?php
                            for($i=0; $i<count($samedit); $i++)
                            {
                            ?>
                            <input type="hidden" name="id_resep" value="<?= $samedit[$i]['resep_id']; ?>">
                            <input type="hidden" name="id" value="<?= $samedit[$i]['Pcode']; ?>">
                            <input type="hidden" name="namalengkap" value="<?= $samedit[$i]['NamaLengkap']; ?>">
                            <input type="hidden" name="isi" value="<?= $samedit[$i]['IsiPerPack']; ?>">
                            <input type="hidden" name="satuan" value="<?= $samedit[$i]['Satuan']; ?>">
                            <input type="hidden" name="qty" value="<?= $samedit[$i]['Qty']; ?>">
                            <?php
                            }
                            ?>
                            <?php
                            if ($link->edit == "Y") {
                                ?>
                                <td width="100" align="center">
                                    <?php
                                    if ($link->view == "Y") {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?= $data[$a]['resep_id']; ?>', '<?= base_url(); ?>');"/></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <?php
                                    }
                                     if ($link->view == "Y") {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/ic_excel.gif' border = '0' title = 'Print' onclick="PopUpExcel('<?= $data[$a]['resep_id']; ?>', '<?= base_url(); ?>');"/></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <?php
                                    }
                                    if ($link->edit == "Y") {
                                        ?>
                                        <td width="100" align="center"><input type="submit" name="edit" value="Edit" class='btn btn-default'></td>
                                        <?php
                                    }
                                   
                                    ?>
                                </td>
                            <?php } ?>
                    </tr>

                </form>
                            <?php
                        }
                        ?>
                </table>
                <table align = 'center'  >
                   
                    <?php
                   // if ($link->add == "Y") {
                        ?>
                        <tr>
                            <td nowrap colspan="3">
                                <a href="<?= base_url(); ?>index.php/transaksi/resep/add_new"><img src='<?= base_url(); ?>public/images/add.png' border = '0' title = 'Add'/></a>
                            </td>
                        </tr>
                    <?php //} ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php $this->load->view('footer'); ?>
