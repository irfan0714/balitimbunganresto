<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<!--<script language="javascript" src="--><?//= base_url(); ?><!--public/js/komisi.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
    
    /*function submit_this() {
//        if(!$('#kdagent').val()){
//            alert('Masukan Kd Group');
//            $('#kdagent').focus();
//            return false;
//        }
        if(document.getElementById('qty').value=="")
        {
            alert("Kuantiti harus di isi");
            return false
        }
        
	}*/
     function search() {
//        if(!$('#kdagent').val()){
//            alert('Masukan Kd Group');
//            $('#kdagent').focus();
//            return false;
//        }
        
        
        if(document.getElementById('namalengkap').value=="")
        {
            alert("Nama harus di isi");
            return false
        }
        
    }
    
    function generateList(){
        base_url = $("#baseurl").val();
        url = "<?=base_url();?>"+"index.php/pop/resep/index/";
        window.open(url,'popuppage','width=950,height=550,top=100,left=100');
    }

    function generateKode(){
        base_url = $("#baseurl").val();
        url = "<?=base_url();?>"+"index.php/pop/resep/index/";
        window.open(url,'popuppage','width=950,height=550,top=100,left=100');
    }
    
    function targetBlank (url) {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
    
    function addMember()
    {
        url = "<?= base_url(); ?>index.php/master/tour_leader/";
        targetBlank (url);
    }

</script>

<body>
    
    <div class="panel panel-gradient">
        <div class="panel-heading">
            <div class="panel-title">
                <?=$judul;?>
            </div>
        </div>

            <div class="panel-body">

                <form action="<?= base_url();?>index.php/transaksi/resep/show/" method="post" onsubmit="return search();">
                    <table align="center">
                        <tr>
                            <td align=" style="display"><b>Nama Menu  &nbsp;&nbsp;&nbsp;</b></td>

                            <td align="center" style="display">
                                <div class="input-group">
                                    <input name="nama" type="text" readonly="readonly" value="<?=$namabarang?>" class="form-control-new" id="namalengkap" readonly="readonly"/>
                                    <input name="pcodetouch" type="hidden" readonly="readonly" value="<?=$PCode?>" class="form-control-new" id="kode" readonly="readonly"/>
                                        
                                    <button class="btn btn-info btn-icon btn-sm icon-left" type="button" onclick="generateList();" title="Search Member">
                                    Cari Nama
                                    <i class="entypo-search"></i></button>

                                        &nbsp;&nbsp;&nbsp;
                                </div>
                            </td>
                            <td align="center" style="display">
                                <div class="input-group">
                                    <input name="kode" type="hidden" size="20"  readonly="readonly" class="form-control-new" id="kode" />

                                    &nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" align="center" style="display"><br><font color="red"><b><?= $error;?>&nbsp;&nbsp;&nbsp;</b></font></td>
                        </tr>

                        <tr>
                            <td colspan="3" align="center">
                                <br><input type="submit" name="submit" value="Tampil" class="btn btn-default">
                            </td>

                            <br>

                        </tr>

                    </table>
                </form>
            </div>
        </div>
                
        <?php
            if($tampil)
            {

                $this->load->view('transaksi/resep/viewlist',$data);
            }
        ?>

<?php $this->load->view('footer'); ?>
