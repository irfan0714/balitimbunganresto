<?php 

$this->load->view('header'); 

$modul = "Credit Note";

?>

<script language="javascript" src="<?=base_url();?>public/js/credit_note.js"></script>
<script>
	function type_onchange(){
		type = $("#v_type").val();
		
		$.ajax({
	         url: "<?=base_url();?>index.php/transaksi/credit_note/ajax_customer/",
	         type: "POST",
	         async: true,
	         data: {Type: type },
	         success: function(res)
				{
					$('#v_customer').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
      	});
	}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name='theform' id='theform'' action='<?=base_url();?>index.php/transaksi/credit_note/save_data'>
		<input type="hidden" id="cekvalidasi" name="cekvalidasi" value="0">
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal credit</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tanggal_dokumen" id="v_tanggal_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        	<!--<tr>
		            <td class="title_table">Tipe</td>
		            <td>
		            	<select class="form-control-new" name="v_type" id="v_type" style="width: 25%;">
		            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
		            		<!--<option value="">- Pilih Tipe -</option>
		            		<option <?php if($header->dntype=="C"){ echo "selected='selected'"; } ?> value="C">CN Hutang Dagang</option>
		            		<option <?php if($header->dntype=="D"){ echo "selected='selected'"; } ?> value="D">DN Hutang Dagang</option>
		            		<option <?php if($header->dntype=="CM"){ echo "selected='selected'"; } ?> value="CM">CN Hutang Biaya</option>
		            		<option <?php if($header->dntype=="DM"){ echo "selected='selected'"; } ?> value="DM">DN Hutang Biaya</option>
		            	</select>
		            </td>
		        </tr>-->
	        
	             <tr>
		            <td class="title_table">Tipe</td>
		            <td>
		            	<select class="form-control-new" name="v_type" id="v_type" style="width: 25%;" onchange="type_onchange()">
		            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
		            		<option value="">- Pilih Tipe -</option>
		            		<option value="CS">CN Distribusi</option>
		            		<option value="CR">CN Reservasi</option>
		            		<option value="DS">DN Distribusi</option>
		            		<option value="DR">DN Reservasi</option>
		            	</select>
		            </td>
		        </tr>
		        
		        <tr>
		            <td class="title_table">Nama Rekening</td>
		            <td>
		            	<input type="text" class="form-control-new" value="" name="v_rekening" id="v_rekening" maxlength="255" size="48%" onblur="validasi()" onkeyup="cariRek()">
		            	<span id="benar" style="display: none;">
		            	<img src="<?=base_url();?>public/images/accept.png"/>
		            	</span>
		            	<span id="salah" style="display: none;">
		            	<img src="<?=base_url();?>public/images/cancel.png"/>
		            	</span>
		            	
		            </td>
		        </tr>
		        
		      <!-- <tr>
	            <td class="title_table">Subdivisi <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_subdivisi" id="v_subdivisi" style="width: 25%;">
	            		<option value="">Pilih Subdivisi</option>
	            		<?php
	            		foreach($subdivisi as $val)
	            		{
							?><option value="<?php echo $val["KdSubdivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr> -->
	        	        
	        <tr>
	            <td class="title_table">Pelanggan </td>
	            <td> 
	            	<select class="form-control-new" name="v_customer" id="v_customer" style="width: 25%;">
	            		<option value="">- Pilih Pelanggan -</option>
	            		<?php
	            		foreach($mcustomer as $val)
	            		{
							?><option value="<?php echo $val["KdCustomer"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Mata Uang", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	       
	        <tr>
	            <td class="title_table">Keterangan </td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="48%"></td>
	        </tr>
	        
	        <!--<tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;">
		            		<!--<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="1">Pending</option>-->
		            		<!--<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Pending</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Close</option>
		            	</select>
		            </td>
		        </tr>-->
            
            <!--<tr>
                <td colspan="100%"  align="right">
                    <?php 
                    if($this->uri->segment(4)=='1'){?>
						<a href="<?php echo base_url() . "index.php/transaksi/sales_invoice/add_new/"; ?>" class="btn btn-orange btn-icon btn-sm icon-left" title="" >Unlock Add Item<i class="entypo-key"></i></a>
					<?php }else{?>
						<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-md md-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari Account Pick List" title="">Add Item<i class="entypo-plus"></i></a>
					<?php }
                     ?>
                    </td>
            </tr>-->
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>Nomor Rekening</center></th>
								<th><center>Nama Rekening</center></th>
								<th width="200"><center>SubDivisi</center></th> 
							    <th width="200"><center>Deskripsi</center></th>
							    <th width="200"><center>Amount</center></th>
							    <th width="50"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button>
							    	</center>
							    </th>
							</tr>
						</thead>
						<tbody>
						<?php
						 	$no=1;
								?>
								<tr id="baris<?php echo $no; ?>">
									<td>
									<nobr>
				                	<input type="text" class="form-control-new" name="v_coano[]" id="v_coano<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_coano<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>										
									</td>
									<td>
										<input type="text" class="form-control-new" name="v_namarekening[]" id="v_namarekening<?php echo $no;?>" value="" style="width: 400px;"/>
									</td>
									<td>
										<select class="form-control-new" name="v_subdivisi[]" id="v_subdivisi<?php echo $no;?>" style="width: 200px;">
						            		<option value="">Pilih Subdivisi</option>
						            		<?php
						            		foreach($subdivisi as $val)
						            		{
												?><option value="<?php echo $val["KdSubdivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
											}
						            		?>
						            	</select> 
									</td>
									
									<td>
										<input type="text" class="form-control-new" name="v_dekripsi[]" id="v_deskripsi<?php echo $no;?>" value=""  style="width: 100%;"/>
									</td>
									<td>
										<input type="text" class="form-control-new" name="v_amount[]" id="v_amount<?php echo $no;?>" value="" onblur="hitung(event,'<?=$no;?>');" style="text-align: right; width: 100%;" />
									</td>
									<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
					                </td>
								</tr>
								<?php
						 
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="batal('<?php echo base_url(); ?>');">Batal<i class="entypo-check"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform2();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                </td>
	        </tr>
	        
	    </table>
	    
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data Summary</li>
			</ol>
			
	         <table class="table table-bordered responsive">
			 
	            <tr>
	            	<td align="right" width="87%">Total</td>
		            <td nowrap align="right">
		            	<input type="text" name="total" id="total" class="form-control-new" value=""  maxlength="255" size="50%" style="text-align: right;">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">Discount</td>
		            <td align="right">
		            	<input type="text" name="diskon" id="diskon" class="form-control-new" value="0"  maxlength="255" size="5%" style="text-align: right;">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_diskon" id="potongan_diskon" class="form-control-new" value="0"  maxlength="255" size="30%" style="text-align: right;">		            		            	
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150">PPN</td>
		            <td nowrap align="right">
		            	<input type="text" name="ppn" id="ppn" class="form-control-new" value="0"  maxlength="255" size="5%" style="text-align: right;">
		            	%&nbsp;&nbsp;&nbsp;&nbsp;
		            	<input type="text" name="potongan_ppn" id="potongan_ppn" class="form-control-new" value="0"  maxlength="255" size="30%" style="text-align: right;">
		            </td>
	            </tr>
	            <tr>
	            	<td align="right" width="150"><b>Grand Total</b></td>
		            <td align="right">
		            	<input type="text" name="grandtotal" id="grandtotal" class="form-control-new" value=""  maxlength="255" size="50%" style="text-align: right;">
		            	<input type="hidden" name="grandtotalhidden" id="grandtotalhidden" class="form-control-new" value=""  maxlength="255" size="50%" style="text-align: right;">
		            </td>
	            </tr>
	         </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.css"/>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	
	var listrek = '';
	
	$(function() {
  		var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: ''}
	     }).done(function(reks){
	     	 listrek = reks.split('#');
	     });;
	});
	
	function cariRek(){
		rek = $("#v_rekening").val(); 
		$("#benar").css("display","none");
		$("#salah").css("display","none");
		$("#cekvalidasi").val('0');
		
		$("#v_rekening").autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
   				 $("#v_rekening").val(value.substr(0,8));  
   				 //$("#v_namarekening").autocomplete("destroy");
			}
    	});		
		
	};
	
	function validasi(){
		rek = $("#v_rekening").val(); 
		pisah = rek.split('-');
		kdrekening = pisah[0];	
		var baseurl = "<?= base_url();?>index.php/transaksi/credit_note/cekrekening" ;
		
		$.ajax({
					url: baseurl,
					data: {kdrekening:kdrekening},
					type: "POST",
					dataType: 'JSON',					
					success: function(res)
					{
						//alert(res.success);
						if(res.success==true){
							$("#benar").css("display","");
							$("#cekvalidasi").val('0');
						}else{
							$("#salah").css("display","");
							$("#cekvalidasi").val('1');
						}
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
	};
	
	function cekTheform2()
{
	var pcode1 = document.getElementsByName("pcode[]").length;
	
	if(document.getElementById("v_type").value=="")
    {
        alert("Tipe harus dipilih");
        document.getElementById("v_type").focus();
        return false;
    }
    else if(document.getElementById("v_rekening").value=="")
    {
        alert("Account Rekening harus dipilih");
        document.getElementById("v_rekening").focus();
        return false;
    }
    else if(document.getElementById("cekvalidasi").value=="1")
    {
        alert("Account Rekening harus dipilih");
        document.getElementById("v_rekening").focus();
        return false;
    }
	else if(document.getElementById("v_customer").value=="")
    {
        alert("Penjual harus dipilih");
        document.getElementById("v_customer").focus();
        return false;
    }
    else
    {
	
    	var yesSubmit = true;
    	        
        if(yesSubmit)
        {
			//alert("Hello");
			document.getElementById("theform").submit();	
		}
			
	}
}
	
</script>