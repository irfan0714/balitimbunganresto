<?php
$mylib = new globallib();
?>
<table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th width="300"><center>Tour Travel</center></th>
		        <th width="200"><center>Bank</center></th>
		        <th width="100"><center>Nilai</center></th>
		        <th width="100"><center>No. RV</center></th>		        
		        <th width="100"><center>No. Voucher</center></th>
		        <th width="200"><center>Keterangan</center></th>
		        <th width="200"><center>BEO</center></th>
		        <th width="50"><center>Status</center></th>
                <th width="70"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = $startnum;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Pending</b></font>";
                } else if ($val["Status"] == 1) {
                    $echo_status = "<font style='color:#ff1c1c'><b>Close</b></font>";
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>Void</b></font>";
                }
                
                if ($val["BEO"] == '0') {
                    $echo_beo = "";
                } else {
                    $echo_beo = $val["BEO"];
                }
                ?>
                <tr title="<?php echo $val["id"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["Tanggal"]); ?></td>
                    <td align="left"><?php echo $val["Nama"]; ?></td>
                    <td align="left"><?php echo $val["NamaKasBank"];?></td>
                    <td align="right"><?php echo number_format($val["Nilai"]); ?></td>
                    <td align="left"><?php echo $val["no_receipt"]; ?></td>
                    <td align="left"><?php echo $val["NoVoucher"]; ?></td>
                    <td align="left"><?php echo $val["Keterangan"]; ?></td> 
                    <td align="left"><?php echo $echo_beo; ?></td> 
                    <td align="center"><?php echo $echo_status; ?></td>                                       
                    <td align="center">

                        <?php
                        if ($val["Status"] == 0) {
                            ?>
                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/uang_muka_beo/edit_uang_muka_beo/<?php echo $val["id"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->

                            <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit Bayar" title="" onclick="edit_bayar('<?php echo $val["id"]; ?>')" >
                                <i class="entypo-pencil"></i>
                            </button>
                            
                            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Close" title="" onclick="lock('<?php echo $val["id"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-key"></i>
                            </button>
                            <?php
                        }

                        if ($val["Status"] == 1) {
                            ?>
                            
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit Bayar" title="" onclick="view_bayar('<?php echo $val["id"]; ?>')" >
                                <i class="entypo-eye"></i>
                            </button>
                            
                            <?php
                        }

                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

    </div>