<?php 
$this->load->view('header');
$mylib = new globallib();
 ?>
<head>
</head>
<!--<script language="javascript" src="<?= base_url(); ?>public/js/uang_muka_beo33.js"></script>-->


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/uang_muka_beo/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
    <input type='hidden' value='<?= $offset ?>' id="offset" name="offset">
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" /> 
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Close</option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2">Void</option>
            </select>  
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="button" class="btn btn-success btn-sm icon-left" onClick="refresh()">Refresh</button>
            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="cari_data()">Search<i class="entypo-search"></i></button>
            <!--<a href="<?php echo base_url() . "index.php/transaksi/uang_muka_beo/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>-->
            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="bayar()">Add<i class="entypo-plus"></i></button>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<span id='getList'></span>
    	

</div>


         <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>Loading...</h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

<div id="modal_payment_method" class="modal fade" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Uang Muka</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <input type="hidden" value="" name="no_receipt"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control datepicker" value="" name="v_date_payment" id="v_date_payment" >
                            </div>
                        </div>
                                                
                        <div class="form-body">
                            
                        <div class="form-group">
                            <label class="control-label col-md-3">Tour Travel</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_tourtravel" id="v_tourtravel" style="width: 100%;" onchange="cari_beo('<?= base_url() ?>')">
	            		<option value="">- Pilih Tour Travel -</option>
	            		<?php
	            		foreach($kdtravel as $val)
	            		{
							?><option value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>
                            </div>
                            
                        </div>
                            
                        <div class="form-group">
                            <label class="control-label col-md-3">Kas / Bank</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_KdKasBank" id="v_KdKasBank" style="width: 100%;">
			            		<option value="">- Pilih Kas Bank -</option>
			            		<?php
			            		foreach($kasbank as $val)
			            		{
									?><option value="<?php echo $val["KdKasBank"]; ?>"><?php echo $val["NamaKasBank"]; ?></option><?php
								}
			            		?>
			            	</select>
                        </div>
                            
                        </div>
                        
                        
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">BEO</label>
                            <div class="col-md-6">
                  			<select class="form-control-new" name="v_beo" id="v_beo" style="width: 100%;">
			            		<option value="">- Pilih BEO -</option>
			            		<?php
			            		foreach($beo as $val)
			            		{
									?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
								}
			            		?>
			            	</select>
                        </div>
                            
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Nilai</label>
                            <div class="col-md-6">
                                <input style="text-align: right;" name="Nilai" id="Nilai" class="form-control" type="text" value="0" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Biaya Admin</label>
                            <div class="col-md-6">
                                <input style="text-align: right;" name="BiayaAdmin" id="BiayaAdmin" class="form-control" type="text" value="0" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">No Bukti</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" name="v_no_bukti" id="v_no_bukti" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" name="v_ket" id="v_ket" >
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-info btn-icon btn-sm icon-left">Save<i class="entypo-check"></i></button>
                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" data-dismiss="modal">Cancel<i class="entypo-cancel"></i></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>   

<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>

<script>

	$(document).ready(function()
		{
			getData();				
		});
		
	
	function getData(){
		base_url = $("#baseurl").val();
		offset = $("#offset").val();
			
			$('#pleaseWaitDialog').modal('show');
			
	    	$.ajax({
				type: "POST",
				url: base_url + "index.php/transaksi/uang_muka_beo/getList/"+offset,
				success: function(data) {
					$('#getList').html(data);
					
						$('#pleaseWaitDialog').modal('hide');
					
			
				}
			});
	}

	function bayar()
    {
     save_method = 'add';
     
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        
     $('#modal_payment_method').modal('show'); 
     $('#btnSave').attr('disabled',false);         
    }
    
    function edit_bayar(id)
    {
     
     	save_method = 'edit';
     	
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('transaksi/uang_muka_beo/ajax_edit_bayar') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       //alert(data);
            {
                var sisa = 0;
                $('[name="id"]').val(data.id);
                $('[name="v_date_payment"]').val(data.Tanggal);
                $('[name="v_tourtravel"]').val(data.KdTravel);
                $('[name="v_KdKasBank"]').val(data.KdBank);
                $('[name="Nilai"]').val(data.Nilai);
                $('[name="BiayaAdmin"]').val(data.BiayaAdmin);
                $('[name="v_no_bukti"]').val(data.NoBukti);
                $('[name="v_ket"]').val(data.Keterangan);
                $('[name="no_receipt"]').val(data.no_receipt);
                $('[name="v_beo"]').val(data.BEO);
                
                $('#modal_payment_method').modal('show');
                $('#btnSave').attr('disabled',false);
                $('.modal-title').text('Uang Muka');
                //document.getElementById('v_no_bukti').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
              
    }
    
    function view_bayar(id)
    {
     
     	save_method = 'view';
     	
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        nodok = id.replace(/\//g, "-");
        $.ajax({
            url: "<?php echo site_url('transaksi/uang_muka_beo/ajax_edit_bayar') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
                       //alert(data);
            {
                var sisa = 0;
                $('[name="id"]').val(data.id);
                $('[name="v_date_payment"]').val(data.Tanggal);
                $('[name="v_tourtravel"]').val(data.KdTravel);
                $('[name="v_KdKasBank"]').val(data.KdBank);
                $('[name="Nilai"]').val(data.Nilai);
                $('[name="BiayaAdmin"]').val(data.BiayaAdmin);
                $('[name="v_no_bukti"]').val(data.NoBukti);
                $('[name="v_ket"]').val(data.Keterangan);
                $('[name="no_receipt"]').val(data.no_receipt);
                $('[name="v_beo"]').val(data.BEO);
                
                $('#modal_payment_method').modal('show');
                $('#btnSave').attr('disabled',true);
                $('.modal-title').text('Uang Muka');
                //document.getElementById('v_no_bukti').focus();
        		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
              
    }
    
    function save()
    {
		    // ajax
        var jumhari = '';
        var jenisnya = 'tahun';
        var tgltrans = '';
		var tglpayment= $('#v_date_payment').val();
        $.ajax({
        type: 'ajax',
        method: 'post',
        url: '<?php echo base_url() ?>index.php/transaksi/uang_muka_beo/getPeriodeTutupBulan',
        data: {jenis: jenisnya},
        async: false,
        dataType: 'json',
        success: function(data1){
          tgltrans = data1.TglTrans;
        },
        error: function(){
          alert('Could not Show Fitur');
        }
		});

        //convert to timestamp
        var dates1 = tgltrans.split("-");
        var newDate1 = dates1[1]+"/"+dates1[0]+"/"+dates1[2];
        var PeriodeKasBank = new Date(newDate1).getTime();

        var dates2 = tglpayment.split("-");
        var newDate2 = dates2[1]+"/"+dates2[0]+"/"+dates2[2];
        var PeriodeBEO = new Date(newDate2).getTime();

        if(PeriodeBEO < PeriodeKasBank){
          swal("Sudah Tutup Bulan", "Periode "+tglpayment+" KasBank Sudah Ditutup", "warning");
          return false;
        }
		
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled', true);
        var url;

        if (save_method == 'add') {
            url = "<?php echo site_url('transaksi/uang_muka_beo/ajax_add') ?>";
        } else {
            url = "<?php echo site_url('transaksi/uang_muka_beo/ajax_update_bayar') ?>";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function (data)
            {
                if (data.status)
                {
                    $('#modal_payment_method').modal('hide');
                    //window.location.reload(true);
                    getData();
                   
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('Save');
               // $('#btnSave').attr('disabled', false);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                //alert('Error adding / update data');
                $('#modal_payment_method').modal('hide');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled', false);

            }
        });
    }
    	
function lock(nodok,url)
{
	var r=confirm("Record Pembayaran ini akan di Lock?")
	if (r==true)
	{
		//window.location = url+"index.php/transaksi/uang_muka_beo/lock/"+nodok;	
		
		$.ajax({
            url: "<?php echo site_url('transaksi/uang_muka_beo/lock') ?>/" + nodok,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data){
				  	getData();
				  }      		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	}
	else
	{
  		return false;
	}
}


function cari_data()
{
	    keyword = $('#search_keyword').val();
	    status = $('#search_status').val();
	    if(keyword==""){
			key=0;
		}else{
			key = keyword;
		}
		
		$('#pleaseWaitDialog').modal('show');
		
		
		$.ajax({
            url: "<?php echo site_url('transaksi/uang_muka_beo/getList') ?>/0/" + key+"/"+status,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#pleaseWaitDialog').modal('hide');
				       		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	
}

function refresh()
{		
		$('#pleaseWaitDialog').modal('show');
		
		$.ajax({
            url: "<?php echo site_url('transaksi/uang_muka_beo/getList') ?>/0/",
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#pleaseWaitDialog').modal('hide');
				       		
            }
            ,
            error: function (textStatus, errorThrown)
            {
                alert('Error get data');
            }
        }
        );
	
}

	function cari_beo(url)
	{
		
		var act=$("#v_tourtravel").val();
		$.ajax({
				url: url+"index.php/transaksi/uang_muka_beo/ajax_list_beo/",
				data: {KdTravel:act},
				type: "POST",
				dataType: 'html',					
				success: function(res)
				{
					
					$('#v_beo').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
			}); 
		
		   	
	}
	
	
</script>
