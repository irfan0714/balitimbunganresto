<table class="table table-bordered responsive" id="TabelUM">
	<thead class="title_table">
		<tr>
			<th width="100"><center>No. Dokumen</center></th>
			<th width="100"><center>Tanggal</center></th> 
			<th><center>Keterangan</center></th>              
			<th width="100"><center>Sisa</center></th>
	    </tr>
	</thead>
	<tbody>
	<?php
	foreach($dataums as $dataum ){
	?>
		<tr>
        	<td>
        		<input type="text" name="NoUM[]" class="form-control-new"  value='<?=$dataum['NoUM'];?>' readonly>
        	</td>
        	<td>
        		<input type="text" name="TglUM[]" class="form-control-new"  value='<?=$dataum['TglUM'];?>' readonly>
        	</td>
        	<td>
        		<input type="text" name="KeteranganUM[]" style="width: 100%;" class="form-control-new"  value='<?=$dataum['KeteranganUM'];?>' readonly>
        	</td>
        	<td>
        		<input type="text" name="NilaiUM[]" style="text-align: right;" class="form-control-new"  value='<?=number_format($dataum['NilaiUM'], 0, '.', ',');?>' readonly>
        	</td>
        </tr>
	<?php	
	}
	?>
	</tbody>
</table>


<table class="table table-bordered responsive" id="TabelDetail">
	<thead class="title_table">
		<tr>
			<th width="50"><center>No Faktur</center></th>
			<th><center>No Faktur Supplier</center></th> 
			<th width="100"><center>No RG</center></th>              
			<th width="100"><center>No PO</center></th>
			<th width="100"><center>Jatuh Tempo</center></th>
			<th width="100"><center>Nilai</center></th>
			<th width="100"><center>Bayar</center></th>
	    </tr>
	</thead>
	<tbody>
		
		<?php 
		$subtotal = 0;
		$ppn = 0;
		$num=count($row);
		$totfaktur=0;
		for($a=0 ; $a<$num ; $a++) {
		?>
		<tr id="baris<?php echo $a; ?>">
	        <td>
            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoFakturSupplier[]" id="NoFakturSupplier<?php echo $a;?>" value='<?=$row[$a]['NoFakturSupplier'];?>' readonly style="width: 100%;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoRG[]" id="NoRG<?php echo $a;?>" value='<?=$row[$a]['NoRG'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoPO[]" id="NoPO<?php echo $a;?>" value='<?=$row[$a]['NoPO'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
            </td>
            <td>
            	<input type="hidden" class="form-control-new" name="vSisa[]" id="vSisa<?php echo $a;?>" VALUE="<?=$row[$a]['Sisa'];?>" readonly style="text-align: right;">
            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" ondblclick="CopySisa(<?php echo $a;?>)" VALUE="<?=number_format($row[$a]['Sisa'], 4, '.', ',');?>" readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>"  VALUE="<?=number_format($row[$a]['NilaiBayar'], 4, '.', ',');?>" onchange="CekBayar(this);HitungBayar(<?php echo $a;?>);" style="text-align: right;" readonly>
            	<input type="hidden" class="form-control-new" name="vBayar[]" id="vBayar<?php echo $a;?>" VALUE="<?=$row[$a]['NilaiBayar'];?>" style="text-align: right;">
            </td>
	    </tr>
	    <?php
	    	$totfaktur += $row[$a]['NilaiBayar']; 
	    }
	    $totbayar = $totfaktur+$data['BiayaAdmin']-$data['NilaiPPH']+$data['Pembulatan'];
	    ?>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Faktur
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vtotalfaktur" id="vtotalfaktur" VALUE="<?=$totfaktur;?>" readonly style="text-align: right;">
	    		<input type="text" class="form-control-new" name="totalfaktur" id="totalfaktur" VALUE="<?=number_format($totfaktur, 4, '.', ',');?>" readonly style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Biaya Admin(+)
	    	</td>
	    	<td>
		    	<input type="hidden" class="form-control-new" name="vbiayaadmin" id="vbiayaadmin" VALUE="<?=$data['BiayaAdmin'];?>" style="text-align: right;">
	    		<input type="text" class="form-control-new" name="biayaadmin" id="biayaadmin" VALUE="<?=number_format($data['BiayaAdmin'], 4, '.', ',');?>"  onchange="BiayaAdminBlur()" style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		<select class="form-control-new" name="vkdrekeningpph" id="vkdrekeningpph">
            		<option value="">- Rek PPH -</option>
            		<?php
            		foreach($rekeningpph as $val)
            		{
            		?>	
            			<option <?=$val['KdRekening']==$data['KdRekeningPPH']?"selected" : "" ;?> value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option>
            		<?php
					}
            		?>
	            </select>
	    	</td>
	    	<td>
		    	<input type="hidden" class="form-control-new" name="vpph" id="vpph" VALUE="<?=$data['NilaiPPH'];?>" style="text-align: right;">
	    		<input type="text" class="form-control-new" name="pph" id="pph" VALUE="<?=number_format($data['NilaiPPH'], 4, '.', ',');?>"  onchange="PPHBlur()" style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Bayar
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vtotalbayar" id="vtotalbayar" VALUE="<?=$totbayar;?>"  style="text-align: right;">
	    		<input type="text" class="form-control-new" name="totalbayar" id="totalbayar" VALUE="<?=number_format($totbayar, 4, '.', ',');?>"  onchange="TotalBayarBlur()" style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Selisih Pembulatan
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vpembulatan" id="vpembulatan" VALUE="<?=$data['Pembulatan'];?>" readonly style="text-align: right;">
	    		<input type="text" class="form-control-new" name="pembulatan" id="pembulatan" VALUE="<?=number_format($data['Pembulatan'], 4, '.', ',');?>" readonly style="text-align: right;">
	    	</td>
	    </tr>
	</tbody>
</table>
<font style="color: red; font-style: italic; font-weight: bold;">Note: Untuk pembayaran Full, double click di nilai faktur ybs.</font>
