<?php $this->load->view('header'); ?>
<script>
	function PopUpPrint(kode,baseurl)
	{
		url="index.php/keuangan/payment/cetak/"+escape(kode);
		window.open(baseurl+url,'popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
	}
</script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/pelunasan_hutang/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-5">
            <b>Search</b>&nbsp;
            <input type="text" size="50" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;
        </div>
        <div class="col-md-3">
        	<b>Tanggal</b>&nbsp;
        	<input type="text" class="form-control-new datepicker" name="tgldari" id="tgldari" size="10" maxlength="10">
        	&nbsp;<b>-</b>&nbsp;
        	<input type="text" class="form-control-new datepicker" name="tglsampai" id="tglsampai" size="10" maxlength="10">
        </div>
        <div class="col-md-2" align="right">
        	<b>Jenis</b>&nbsp;
        	<select class="form-control-new" name="Jenis" id="Jenis"">
	        	<option value="">All</option>
	        	<option value="0">Unpaid</option>
	        	<option value="1">Paid</option>
	        	<option value="2">Void</option>
	        </select>
        </div>
        <div class="col-md-2" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <!-- <a href="<?php echo base_url() . "index.php/transaksi/pelunasan_hutang/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a> -->
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="75"><center>No Transaksi</center></th>
                <th width="75"><center>No Rencana Bayar</center></th>
		        <th width="75"><center>No. PV</center></th>
		        <th width="75"><center>Tanggal</center></th>
		        <th width="150"><center>Supplier</center></th>
		        <th width="150"><center>Keterangan</center></th>
		        <th width="25"><center>Status</center></th>
		        <th width="50"><center>Navigasi</center></th>
        	</tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }
                if($val['status']==0){
					$status = 'Unpaid';
				}elseif($val['status']==1){
					$status = 'Paid';
				}else{
					$status = 'Void';
				}

                ?>
                <tr title="<?php echo $val["NoTransaksi"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td align="left"><?php echo $val["NoTransaksi"]; ?></td>
                    <td align="left"><?php echo $val["NoRencanaBayar"]; ?></td>
                    <td align="left"><?php echo $val["NoPaymentVoucher"]; ?></td>
                    <td align="center"><?php echo $val["TglDokumen"]; ?></td>
                    <td align="left"><?php echo $val["Nama"]; ?></td>
                    <td align="left"><?php echo $val["Keterangan"]; ?></td>
                    <td align="left"><?= $status ; ?></td>
                    <td align="center">
                    <?php
						if($status=='Unpaid'){
					?>
							 <a href="<?php echo base_url(); ?>index.php/transaksi/pelunasan_hutang/edit/<?php echo $val["NoTransaksi"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Bayar" title=""><i class="entypo-pencil"></i></a>							
					<?php
						}
					?>
                        <a href="<?php echo base_url(); ?>index.php/transaksi/pelunasan_hutang/view/<?php echo $val["NoTransaksi"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
  						<?php
  						if($status=='Paid'){
						?>	
							<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['NoPaymentVoucher'];?>', '<?= base_url(); ?>');" ><i class="entypo-print"></i></button>					
						<?php
						} 
						?>  
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>