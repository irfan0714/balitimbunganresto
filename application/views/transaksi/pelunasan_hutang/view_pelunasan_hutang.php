<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Pelunasan Hutang";
//$arr_data(unset);

?>

<script>
	function simpan(){
    	if(parseFloat($('#vtotalbayar').val()) > 0){
			$("#theform").submit();	
		}else{
			alert("Total Bayar masih Nol.");
		}
	}
	
	function formatdec(nilai){
		var MataUang = $('#kd_uang').val();
		if(MataUang=='IDR'){
			result = toFormat(nilai);
		}else{
			result = toFormat4(nilai);	
		}
		
		return result;
	}
	
	function HitungBayar(id){
		bayar = $("#Bayar"+id).val();
		var xbayar = formatdec('Bayar'+id);
		$("#vBayar"+id).val(bayar);
		
		var lastRow = document.getElementsByName("NoFaktur[]").length;
    	var total = 0;
    	for (index = 0; index < lastRow; index++)
    	{
			nama = document.getElementsByName("vBayar[]");
        	temp = nama[index].id;
        	temp1 = parseFloat(nama[index].value);
        	
        	if(temp1!='' && !isNaN(temp1)){
				total += temp1;	
			}
		}
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (total + biayaadmin - pph);
		
		$("#vtotalfaktur").val(total);
		$("#totalfaktur").val(total);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('totalfaktur');
		formatdec('pembulatan');
	}
	
	function BiayaAdminBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#biayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vbiayaadmin").val(biayaadmin);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('biayaadmin');
	}
	
	function PPHBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#pph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('pph');
	}
	
	function TotalBayarBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#totalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vtotalbayar").val(totalbayar);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('totalbayar');
	}
	
	function CopySisa(id){
		sisa =  parseFloat($("#vSisa" + id).val());
		$("#vBayar"+id).val(sisa);
		$("#Bayar"+id).val(sisa);
		HitungBayar(id);
	}
	
	function CekBayar(obj) {
		objek = obj.id;
        id = parseFloat(objek.substr(5, objek.length - 5));

        bayar = parseFloat($("#Bayar" + id).val());
        sisa = parseFloat($("#vSisa" + id).val());
        
        if(bayar>sisa){
			alert("Pembayaran lebih besar dari nilai faktur.");
			$("#Bayar" + id).val(0);
		}
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/pelunasan_hutang/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td>
	            	<input type="text" value="<?=$data['NoTransaksi']; ?>" class="form-control-new" name="notransaksi" id="notransaksi" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?=$data['TglDokumen']; ?>" name="tgldokumen" id="tgldokumen" readonly size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<select class="form-control-new" name="KdKasBank" id="KdKasBank">
	            	<?php
	            	foreach($kasbank as $bank){
	            	?>
						<option <?php if($bank['KdKasBank']==$data['KdKasBank']){ echo "selected='selected'"; } ?> value="<?=$bank['KdKasBank'];?>"><?=$bank['NamaKasBank'];?></option>
					<?php
					}
					?>
	            </td>
	            <td class="title_table" width="150">Mata Uang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['MataUang']; ?>" class="form-control-new" name="kd_uang" id="kd_uang" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Kurs <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['Kurs']; ?>" class="form-control-new" name="kurs" id="kurs" size="15" maxlength="15" style="text-align: right;">
	            </td>
	            <td class="title_table" width="150">No Bukti <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NoBukti']; ?>" class="form-control-new" name="NoBukti" id="NoBukti" size="30" maxlength="25">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Supplier <font color="red"><b>(*)</b></font></td>
	            <td> 
		            <input type="hidden"  value="<?=$data['KdSupplier']; ?>" class="form-control-new" name="KdSupplier" id="KdSupplier" readonly size="30" maxlength="25">
	            	<input type="text"  value="<?=$data['NamaSupplier']; ?>" class="form-control-new" name="namasupplier" id="namasupplier" readonly size="30" maxlength="25">
	            </td>
	            <td class="title_table" width="150">No. Rek Supplier <font color="red"><b>(*)</b></font></td>
				<td>
          			<select class="form-control-new" name="NoRekSupplier" id="NoRekSupplier">
	            		<?php
	            		foreach($RekSupplier as $rek){
	            			if($data['NoRekeningSupplier']==$rek['NoRekSupplier']){
								$selected = "selected=true";
							}else{
								$selected = "";
							}
						?>
							<option <?=$selected;?> value="<?=$rek['NoRekSupplier'];?>"><?=$rek['NoRekSupplier'];?></option>
						<?php	
						}
	            		?>
	            	</select>
				</td>
	            
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td colspan="3"> 
	            	<input type="text" value="<?=stripslashes($data['Keterangan']); ?>" class="form-control-new" name="keterangan" id="keterangan" size="75" maxlength="100" style="text-align: left;">
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<?php $this->load->view('/transaksi/pelunasan_hutang/pelunasan_hutang_detail');?>
	        	</td>
	        </tr>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/rencana_bayar/"; ?>">Batal<i class="entypo-cancel"></i></button>
   					<?php
   					if($flag=='edit'){
					?>
						<button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onclick="simpan();" value="Simpan">Simpan<i class="entypo-check"></i></button>
					<?php	
					}    
					?>             
		         </td>
	        </tr>
	    </table>
	    
	    </form> 
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
		</ol>
			
        <table class="table table-bordered responsive">
            <tr>
            	<td class="title_table" width="150">Author</td>
	            <td><?php echo $data['AddUser']." :: ".$data['AddDate']; ?></td>
            </tr>
            <tr>
            	<td class="title_table" width="150">Edited</td>
	            <td><?php echo $data['EditUser']." :: ".$data['EditDate']; ?></td>
            </tr>
         </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
