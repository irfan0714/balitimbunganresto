<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Kunjungan";

?>
<script language="javascript" src="<?=base_url();?>public/js/kunjungan.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/kunjungan/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->noku; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->noku; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	          <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->dodate!="" && $header->dodate!="00-00-0000") { echo $header->dodate; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Salesman </td>
	            <td> 
	            	<select class="form-control-new" name="v_salesman" id="v_salesman" style="width: 200px;">
	            		<option value=""> -- Pilih -- </option>
	            		<?php
	            		foreach($salesman as $val)
	            		{
	            			$selected="";
							if($header->KdSalesman==$val["KdSalesman"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSalesman"]; ?>"><?php echo $val["NamaSalesman"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
			<tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <?php
			if($header->status==0 or $header->status==2)
			{
			?>
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Aktif</option>
		            		<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Non Aktif</option>
		            	</select>
		            </td>
		        </tr>
			<?php
			}
			else
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            <?php
		            if($header->status==1)
		            {
						echo "<b>Non Aktif</b>";
					}
					else if($header->status==2)
		            {
						//echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr> 
			<?php
			}
			?>
            
            
			<?php
			if(count($detail_list)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="100"><center>Kode Travel</center></th>
							    <th><center>Nama Tour Travel</center></th> 
							    <th width="300"><center>Contact</center></th> 
								<th width="250"><center>Phone</center></th>
							    
							    <?php
							    if($header->status==0)
							    {
									echo '<th width="100"><center>Delete</center></th>';	
								}
							    ?>
							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$i=1;
						foreach($detail_list as $val)
						{
							?>
							<tr>
								<td align="center"><?php echo $val["KdTour"]; ?></td>
								<td><?php echo $val["Nama"]; ?></td>
								<td align="left"><?php echo $val["Contact"]; ?></td>
								<td align="left"><?php echo $val["Phone"]; ?></td>
								
								<?php
							    if($header->status==0)
							    {
							    	?>
							    	<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail('<?php echo $val["id_kunj_detail"]; ?>','<?php echo $val["KdTour"]; ?>','<?php echo $header->noku; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
							    	<?php									
								}
							    ?>
				                
							</tr>
							<?php
							$i++;
						}
						
						?>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        
	        <?php
	        }
	        ?>
			
			<?php
			if($header->status==0 or $header->status==2)
			{
			?>
			
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>Kode Travel</center></th>
							    <th><center>Nama Tour Travel</center></th> 
							    <th width="300"><center>Contact</center></th> 
								<th width="250"><center>Phone</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="kdtour[]" id="kdtour<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_kdtour<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari Kode Tour" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namatour[]" id="v_namatour<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_contact[]" id="v_contact<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
								<td>
				                	<input type="text" class="form-control-new" name="v_phone[]" id="v_phone<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/kunjungan/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/kunjungan/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">View Only.</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->noku)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->AddUser." :: ".$header->AddDate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->EditUser." :: ".$header->EditDate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>