<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'lainlain',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/lainlain.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<style type="text/css">
    .InputAlignRight
    {
        text-align: right;
    }
    <!--
    #Layer1 {
        position:absolute;
        left:45%;
        top:40%;
        width:0px;
        height:0px;
        z-index:1;
        background-color:#FFFFFF;
    }
    -->
</style>
<SCRIPT language="javascript">
    function goBack()
    {
        window.history.back()
    }

    function deleteRow(obj)
    {
        var objek = obj.id;
        var id = objek.substr(3, objek.length - 3);
        tbl.deleteRow(id);
    }
    function hapus(tableID) {
        try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;

            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if (null != chkbox && true == chkbox.checked) {
                    if (rowCount <= 1) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }

            }
        } catch (e) {
            alert(e);
        }
    }
//        =================================================
    function panggil(i) { // panggil kode barang
        var objek = i.id;
        var id = objek.substr(4, objek.length - 4);
        if (cekheader())
        {
//            var trx = $("#hidetipe").val();
            var url = "<?php echo base_url(); ?>index.php/pop/baranglainlain/index/" + id + "/";
            window.open(url, 'popuppage', 'scrollbars=yes,width=750,height=400,top=200,left=150');
        }
    }

    function caritgl() {
        base_url = $("#baseurl").val();
        $('#tgl').datepicker({dateFormat: 'dd-mm-yy', mandatory: true, showOn: "both", buttonImage: base_url + "public/images/calendar.png", buttonImageOnly: true});
    }

</SCRIPT>  
<body onload="firstLoad('lainlain');
      ">
    <form method='post' name="lainlain" id="lainlain" action='<?= base_url(); ?>index.php/transaksi/lainlain/save_new_lainlain' onsubmit="return false" class="form-horizontal form-groups-bordered">
        <table align = 'center' >
            <!--   =============== form header ==========================================================   -->
            <tr>
                <td>
                    <fieldset class="disableMe">
                        <legend class="legendStyle">Add Penerimaan / Pengeluaran Lain</legend>
                        <div class="form-group">
                            <table class="table_class_list">
                                <tr id="nodokumen" style="display:none">
                                    <td nowrap>No</td>
                                    <td nowrap>:</td>
                                    <td nowrap colspan="1" ><input type="text" size="10" readonly="readonly" id="nodok" name="nodok" /></td>
                                </tr>
                                <?php
                                $mylib = new globallib();
                                echo $mylib->write_textbox("Tgl Dokumen", "tgl", $tanggal->TglTrans, "10", "10", "readonly='readonly'", "text", $gantikursor, "1");
                                ?>   
                                <tr>
                                    <td nowrap>Type</td>
                                    <td nowrap>:</td>
                                    <td nowrap><select name="tipe" id="tipe" onChange="gantiTipe(document.Calc.tipe.selectedIndex)">
                                            <option value="">--Please Select--</option>
                                            <option value="masuk" >Penerimaan</option>
                                            <option value="keluar" >Pengeluaran</option>
                                        </select>
                                    </td>
                                </tr>
                                <?php
                                //echo $mylib->write_combo("CNDN","cndn",$mcndn,"","KdCNDN","Nama",$gantikursor,"onchange=\"simpanKontak();\"","ya");
                                echo $mylib->write_combo("Gudang", "gudang", $mgudang, "", "KdGudang", "Nama", $gantikursor, "onchange=\"simpanKontak();\"", "ya");
                                echo $mylib->write_textbox("Keterangan *", "ket", "", "45", "30", "", "text", $gantikursor, "1");
                                ?>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <!--                ======================== End header ======================-->

            <!--                +++++++++++++++++++++++= Form Detail =++++++++++++++++++++-->
            <tr>
                <td>
                    <fieldset class = "disableMe">
                        <legend class="legendStyle">Detail</legend>
                        <div class="form-group">
                            <div id="Layer1" style="display:none">
                                <p align="center">
                                    <img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
                                </p>
                            </div>


                            <table class="table_class_list" id="detail">
                                <tr id="baris0">                    
                                    <td><img src="<?= base_url(); ?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()" title="tambah baris"></td>
                                    <td>KdBarang</td>
                                    <td>NamaBarang</td>
                                    <td>Qty</td>
                                    <td>Nilai</td>
                                    <td>Total Nilai</td>
                                </tr>
                                <?= $mylib->write_detail_lainlain(1, "", "", 0, "", "") ?>

                            </table>   
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td nowrap>
                    <input type='hidden' id="totalbaris" name="totalbaris" value="1">
                    <input type='hidden' id="transaksi" name="transaksi" value="no">
                    <input type='hidden' id="hidetipe" name="hidetipe" value="masuk">
                    <input type='hidden' id="flag" name="flag" value="add">
                    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                    <input type='button' value='Save' onclick="saveAll()" class="btn btn-default"/>&nbsp;
                    <input type="button" value="Back" onclick="goBack()" class="btn btn-default"/>
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">

        var selectmenu = document.getElementById("tipe")
        selectmenu.onchange = function () { //run some code when "onchange" event fires
            var pilih = $("#tipe").val();
            $("#hidetipe").val(pilih);
            if (pilih == "keluar")
            {
                $("#btnatr1").val("Lihat");
            }
            else if (pilih == "masuk")
            {
                $("#btnatr1").val("Isi");
                if ($("#mandatattr1").val() == "yes") {
                }
            }
        }

    </script>

    <?php $this->load->view('footer'); ?>