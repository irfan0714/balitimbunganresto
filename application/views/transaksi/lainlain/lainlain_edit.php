<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'lainlain',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/lainlain.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<style type="text/css">
    .InputAlignRight
    {
        text-align: right;
    }
    <!--
    #Layer1 {
        position:absolute;
        left:45%;
        top:40%;
        width:0px;
        height:0px;
        z-index:1;
        background-color:#FFFFFF;
    }
    -->
</style>
<SCRIPT language="javascript">
    function goBack()
    {
        window.history.back()
    }

    function deleteRow(obj)
    {
        var objek = obj.id;
        var id = objek.substr(3, objek.length - 3);
        tbl.deleteRow(id);
    }
    function hapus(tableID) {
        try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;

            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if (null != chkbox && true == chkbox.checked) {
                    if (rowCount <= 1) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }

            }
        } catch (e) {
            alert(e);
        }
    }
//        =================================================
    function panggil(i) { // panggil kode barang
        if (cekheader())
        {
            var trx = $("#hidetipe").val();
            var url = "<?php echo base_url(); ?>index.php/pop/barangownerlainlain/pilih/" + i + "/" + trx + "/";
            window.open(url, 'popuppage', 'scrollbars=yes,width=750,height=400,top=200,left=150');
        }
    }

    function clokasi(i) { // panggil kode lokasi
        var objek = i.id;
        var id = objek.substr(6, objek.length - 6);
        if (cekheader())
        {
            var trx = $("#hidetipe").val();
            var bln = $("#tgl").val();
            if (cekbrg(id)) {
                var kd = $("#pcode" + id).val();
                var url = "<?php echo base_url(); ?>index.php/pop/carilokasi/cari/" + trx + "/" + kd + "/" + id + "/" + bln + "/";
                window.open(url, 'popuppage', 'scrollbars=yes,width=750,height=400,top=200,left=150');
            }
        }
    }

    function caritgl() {
        base_url = $("#baseurl").val();
        $('#tgl').datepicker({dateFormat: 'dd-mm-yy', mandatory: true, showOn: "both", buttonImage: base_url + "public/images/calendar.png", buttonImageOnly: true});
    }

</SCRIPT>
<body onload="firstLoad('lainlain');
        caritgl()">
    <form method='post' name="lainlain" id="lainlain" action='<?= base_url(); ?>index.php/transaksi/lainlain/save_new_lainlain' onsubmit="return false" class="form-horizontal form-groups-bordered">
        <table align = 'center' >
            <!--   =============== form header ==========================================================   -->
            <tr>
                <td>
                    <fieldset class="disableMe">
                        <legend class="legendStyle">Add Penerimaan / Pengeluaran Lain</legend>
                        <div class="form-group">
                            <table class="table_class_list">
                                <tr id="nodokumen" style="display:none">
                                    <td nowrap>No</td>
                                    <td nowrap>:</td>
                                    <td nowrap colspan="1" ><input type="text" size="10" readonly="readonly" id="nodok" name="nodok" /></td>
                                </tr>
                                <?php
                                $mylib = new globallib();
                                echo $mylib->write_textbox("No", "nodok", $header->NoDokumen, "10", "10", "readonly='readonly'", "text", $gantikursor, "1");
                                echo $mylib->write_textbox("Tgl Dokumen", "tgl", $header->TglDokumen, "10", "10", "readonly='readonly'", "text", $gantikursor, "1");
                                echo $mylib->write_textbox("Tipe", "tipe", $header->NamaKeterangan, "15", "12", "readonly='readonly'", "text", $gantikursor, "1");
                                echo $mylib->write_textbox("Gudang", "gudang", $header->KdGudang, "15", "12", "readonly='readonly'", "text", $gantikursor, "1");
                                echo $mylib->write_textbox("Keterangan *", "ket", $header->Keterangan, "35", "30", "", "text", $gantikursor, "1");
                                ?>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <!--                ======================== End header ======================-->

            <!--                +++++++++++++++++++++++= Form Detail =++++++++++++++++++++-->
            <tr>
                <td>
                    <fieldset class = "disableMe">
                        <legend class="legendStyle">Detail</legend>
                        <div class="form-group">
                            <div id="Layer1" style="display:none">
                                <p align="center">
                                    <img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
                                </p>
                            </div>


                            <table class="table_class_list" id="detail">
                                <thead>
                                    <tr id="baris0">
                                        <td><img src="<?= base_url(); ?>/public/images/table_add.png" width="16" height="16" border="0" onClick="tambahbaris('dataTable')" title="tambah baris"></td>
                                        <td>KdBarang</td>
                                        <td>NamaBarang</td>
                                        <td>Qty</td>
                                        <td>Nilai</td>
                                        <td>Total Nilai</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $modeller = new lainlain_model();
                                    $brs = 1;
                                    for ($a = 0; $a < count($detail); $a++) {
                                        $pcode = $detail[$a]['PCode'];
                                        $nama = $detail[$a]['NamaInitial'];
                                        $qty = $detail[$a]['QtyPcs'];
                                        $qtypcs = $detail[$a]['QtyPcs'];
                                        ?>
                                        <tr id="tr<?= $brs ?>" >
                                            <td><img src="<?= base_url(); ?>/public/images/del.png" width="16" height="16" border="0" title="hapus baris" onclick="DeleteRow('tr<?= $brs ?>', <?= $brs ?>);
                                                    GetTotal(<?= $brs ?>);"></td>
                                            <td nowrap>
                                                <input type="text" id="pcode<?= $brs ?>" name="pcode[]" size="5" maxlength="6" onkeydown="keyShortcut(event, 'pcode',<?= $brs ?>)" value="<?= $detail[$a]['PCode'] ?>">
<!--                                                <img src="<?= base_url(); ?>/public/images/pick.png" width="16" height="16" border="0" onClick="panggil(<?= $brs ?>);">-->
                                            </td>
                                            <td nowrap><input type="text" id="nama<?= $brs ?>" name="nama[]" size="25" readonly="readonly" value="<?= $detail[$a]['NamaInitial'] ?>"></td>
                                            <td nowrap><input type="text" id="qty<?= $brs ?>" name="qty[]" size="5" maxlength="11" onkeyup="InputQty(this)" value="<?= $detail[$a]['QtyPcs'] ?>"></td>
                                            <td nowrap><input type="text" id="hrg<?= $brs ?>" name="hrg[]" size="8" onkeyup="Inputhrg(this)" value="<?= $detail[$a]['Harga'] ?>" ></td>
                                            <td nowrap><input type="text" id="netto<?= $brs ?>" name="netto[]" size="8" readonly="readonly" value="<?php echo ($detail[$a]['Harga']) * ($detail[$a]['QtyPcs']) ?>" ></td>


                                            <!--                                        ===================== file hidden ===============================-->

                                    <input type="hidden" id="temppcode<?= $brs ?>" name="temppcode[]" value="<?= $pcode ?>">
                                    <input type="hidden" id="tempqty<?= $brs ?>" name="tempqty[]" value="<?= $qtypcs ?>">
                                    <input type="hidden" id="qtypcs<?= $brs ?>" name="qtypcs[]" value="<?= $qtypcs ?>">

                                    </td>
                                    </tr>
                                    <?php
                                    $brs++;
                                }
                                ?>
                                </tbody>   
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td nowrap>
                    <input type='hidden' id="totalbaris" name="totalbaris" value="1">
                    <input type='hidden' id="transaksi" name="transaksi" value="no">
                    <input type='hidden' id="hidetipe" name="hidetipe" value="masuk">
                    <input type='hidden' id="flag" name="flag" value="edit">
                    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                    <input type='button' value='Save' onclick="saveAll()" class="btn btn-default"/>
<!--                        <input type="button" value="Back" ONCLICK =parent.location="<?= base_url(); ?>index.php/transaksi/lainlain/" />-->
                    <input type="button" value="Back" onclick="goBack()" class="btn btn-default"/>
                </td>
            </tr>
        </table>
    </form>
    <?php $this->load->view('footer'); ?>