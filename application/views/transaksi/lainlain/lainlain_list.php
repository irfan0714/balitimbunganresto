<?php
$this->load->view('header');
?>

<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
    function gantiSearch()
    {
        if ($("#searchby").val() == "NoDokumen")
        {
            $("#normaltext").css("display", "");
            $("#datetext").css("display", "none");
            //$("#date1").datepicker("destroy");
            $("#date1").val("");
        }
        else
        {
            $("#datetext").css("display", "");
            $("#normaltext").css("display", "none");
            $("#stSearchingKey").val("");
            //$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>/public/images/calendar.png' });
            $('#date1').Zebra_DatePicker({format: 'd-m-Y'});
        }
    }
    function kirimdata(noorder, base_url)
    {
        x = confirm("Kirim Extrack ke counter ?");
        if (x) {
            $("#Layer1").attr("style", "display:");
            $('fieldset.disableMe :input').attr('disabled', true);
            $("#formext").submit();
        }
    }

    function PopUpPrint(kode, baseurl)
    {
        url = "index.php/transaksi/lainlain/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }
    function PopUpPrintBarcode(kode, baseurl)
    {
        url = "index.php/transaksi/lainlain/print_barcode/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=500,height=500,top=50,left=180');
    }
    function saveThis(kode)
    {
        //child.close();
        setTimeout("saveAttr('" + kode + "');", 0.5);
    }
    function saveAttr(kode)
    {
        saveItem(kode);
    }

    function saveItem(kode)
    {
        qtycetak = $("#qtyctk").val();
        //alert(qtycetak);
        base_url = $("#baseurl").val();
        alert(base_url);
        flag = $("#flag").val();
        $.post(base_url + "index.php/transaksi/lainlain/save_cetak_attr", {
            noterima: kode, qtycetak: qtycetak},
        function (data) {
            //alert(data);
        });
    }
    function deleteTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menghapus Transaksi " + nodok + " ?");
        if (r == true) {
//                        $.post(url+"index.php/transaksi/lainlain/delete_item",{ 
//				kode:nodok},
//			function(data){
//				window.location = url+"index.php/transaksi/lainlain";
//			});
        }
    }
</script>
<form method="POST"  name="search" action="">
    <table align='center'>
        <tr>
            <td id="normaltext" style=""><input type='text' size='20' name='stSearchingKey' id='stSearchingKey'></td>
            <td id="datetext" style="display:none"><input type='text' size='10' readonly='readonly' name='date1' id='date1'></td>
            <td>
                <select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()">
                    <option value="NoDokumen">No Dokumen</option>
                    <option value="TglDokumen">Tgl Dokumen</option>
                </select>
            </td>
            <td><input type="submit" value="Search (*)"></td>
        </tr>
    </table>
</form>

<br>
    <table align = 'center' border='1' class='table_class_list'>
        <tr>
            <?php
            if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                echo "<th></th>";
                $mylib = new globallib();
                echo $mylib->write_header($header);
            }
            ?>
        </tr>
        <?php
        if (count($data) == 0) {
            ?>
            <td nowrap colspan="<?php echo count($header) + 1; ?>" align="center">Tidak Ada Data</td>
            <?php
        }
        for ($a = 0; $a < count($data); $a++) {
            ?>
            <tr>

                <?php
                if ($link->edit == "Y" || $link->delete == "Y") {
                    ?>
                    <td nowrap>

                        <?php
                        if ($link->view == "Y") {
                            ?>
                            <img src='<?= base_url(); ?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?= $data[$a]['NoDokumen']; ?>', '<?= base_url(); ?>');"/></a>
                            <?php
                        }
                        if ($link->edit == "Y" && ($data[$a]['Tipe'] == "Pengeluaran" || $data[$a]['Tipe'] == "Penerimaan")) {
                            ?>
                            <a href="<?= base_url(); ?>index.php/transaksi/lainlain/edit_lainlain/<?= $data[$a]['NoDokumen']; ?>"><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                            <?php
                        }
                        if ($link->delete == "Y" && strtotime($tanggal) <= strtotime($data[$a]['TglDokumen']) && ($data[$a]['Tipe'] == "Pengeluaran" || $data[$a]['Tipe'] == "Penerimaan")) {
                            ?>
                            <img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete'onclick="deleteTrans('<?= $data[$a]['NoDokumen']; ?>', '<?= base_url(); ?>');"/>		
                            <?php
                        }
                        ?><form method="post" id="formext" name="formext" action="<?= base_url() ?>index.php/transaksi/lainlain/doThis/">



                            <input type="hidden" id="hdok" name="hdok" value="<?= $data[$a]['NoDokumen']; ?>" />
                        </form>
                    </td>
                    <?php
                } else {
                    echo "<td></td>";
                }
                ?>
                <td nowrap><?= $data[$a]['NoDokumen']; ?></td>
                <td nowrap><?= $data[$a]['Tanggal']; ?></td>
                <td nowrap><?= $data[$a]['Tipe']; ?></td>
                <td nowrap><?= stripslashes($data[$a]['Keterangan']); ?></td>
            <input type='hidden' id="qtyctk" name="qtyctk" value="<?= $qtyctk ?>">
            <input type='hidden' id="baseurl" name="baseurl" value="<?= base_url(); ?>">
            <tr>
                <?php
            }
            ?>
    </table>
<table align = 'center'  >
    <tr>
        <td>
            <?php echo $this->pagination->create_links(); ?>
        </td>
    </tr>
    <?php
    if ($link->add == "Y") {
        ?>
        <tr>
            <td nowrap colspan="3">
                <a 	href="<?= base_url(); ?>index.php/transaksi/lainlain/add_new/"><img src='<?= base_url(); ?>public/images/add.png' border = '0' title = 'Add'/></a>
            </td>
        </tr>
    <?php } ?>
</table>
<?php $this->load->view('footer'); ?>

