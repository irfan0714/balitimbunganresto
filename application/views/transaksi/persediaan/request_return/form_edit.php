<?php 
$this->load->view('header'); 

$mylib = NEW Globallib;

$modul = "Request Return";

?>

<script language="javascript" src="<?=base_url();?>public/js/request_return.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
    		
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/request_return/save_data'>
			
		    <table class="table table-bordered responsive">                        
		        <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen; ?>"/>
		        <input type="hidden" name="v_status_header" id="v_status_header" value="<?php echo $header->Status; ?>"/>
		    	 <tr>
		            <td class="title_table" width="150">No Dokumen</td>
		            <td  width="200"><b><?php echo $header->NoDokumen; ?></b></td>
		            
		            <td class="title_table" colspan="2">Informasi Receipt Good (RG)</td>
		            
		        </tr>
		        
		        <tr>
		            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
		            <td> 
		            	<input type="text" class="form-control-new datepicker" value="<?php echo $header->TglDokumen; ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
		            </td>
		            
		            <td class="title_table" width="150">Gudang <font color="red"><b>(*)</b></font></td>
		            <td id="show_rg_gudang" ><?php echo $header->gudang_nama; ?></td>
		        </tr>
		        
		        <tr>
		            <td class="title_table" width="150">RG No <font color="red"><b>(*)</b></font></td>
		            <td> 
		            	<input type="hidden" name="v_rg_no" id="v_rg_no" value="<?php echo $header->RgNo; ?>"/>
		            	<input type="hidden" name="v_rg_gudang" id="v_rg_gudang" value="<?php echo $header->KdGudang; ?>"/>
		            	<input type="hidden" name="v_rg_pono" id="v_rg_pono" value="<?php echo $header->PoNo; ?>"/>
		            	<input type="hidden" name="v_rg_supplier" id="v_rg_supplier" value="<?php echo $header->KdSupplier; ?>"/>
		            	<?php echo $header->RgNo; ?>
		            </td>
		            
		            <td class="title_table">Po No <font color="red"><b>(*)</b></font></td>
		            <td id="show_rg_pono" ><?php echo $header->PoNo; ?></td>
		            
		        </tr>
		        
		        <tr>
		            <td class="title_table">Keterangan <font color="red"><b>(*)</b></font></td>
		            <td><input type="text" class="form-control-new" value="<?php echo $header->Keterangan; ?>" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
		            
		            
		            <td class="title_table">Supplier <font color="red"><b>(*)</b></font></td>
		            <td id="show_rg_supplier" ><?php echo $header->suppliername; ?></td>
		        </tr>
	        
		        <?php
				if($header->Status==0)
				{
				?>
			        <tr>
			            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
			            <td colspan="3">
			            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
			            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
			            		<option <?php if($header->Status==1){ echo "selected='selected'"; } ?> value="1">Open</option>
			            		<option <?php if($header->Status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
			            	</select>
			            </td>
			        </tr>  
				<?php
				}
				else
				{
				?>
			        <tr>
			            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
			            <td colspan="3">
			            <?php
			            if($header->Status==1)
			            {
							echo "<b>Open</b>";
						}
						else if($header->Status==2)
			            {
							echo "<b>Void</b>";
						}
			            ?>
			            	
			            </td>
			        </tr>  
				<?php
				}
				?>
		        
		        <tr>
		        	<td colspan="100%">
						
						<table class="table table-bordered responsive">
	       		 			<thead class="title_table">
								<tr>
								    <th width="30"><center>No</center></th>
								    <th width="100"><center>PCode</center></th>
								    <th><center>Nama Barang</center></th>               
								    <th width="100"><center>Qty RG</center></th>    
								    <th width="100"><center>Qty Return</center></th>
								    <th width="100"><center>Satuan</center></th>		
								    <th width="100"><center>Keterangan</center></th>								    
								</tr>
							</thead>
							<tbody>
							
							<?php
							$i=1;
							foreach($detailunion as $val)
							{
								?>
								<tr>
									<td align="center">
										<input type="hidden" name="pcode[]" id="pcode<?php echo $i; ?>" value="<?php echo $val["PCode"]; ?>"/>
										<input type="hidden" name="v_qty[]" id="v_qty<?php echo $i; ?>" value="<?php echo $val["Qty"]; ?>"/>
										<input type="hidden" name="v_qtypcs[]" id="v_qtypcs<?php echo $i; ?>" value="<?php echo $val["QtyPcs"]; ?>"/>
										<input type="hidden" name="v_satuan[]" id="v_satuan<?php echo $i; ?>" value="<?php echo $val["Satuan"]; ?>"/>
										<?php echo $i; ?>
											
									</td>
									<td align="center"><?php echo $val["PCode"]; ?></td>
									<td><?php echo $val["NamaLengkap"]; ?></td>
									<td align="right"><?php echo $mylib->format_number($val["Qty"],2); ?></td>
									<td align="center"><input type="text" name="v_qty_return[]" class="form-control-new" id="v_qty_return<?php echo $i; ?>" onblur="toFormat2('v_qty_return<?php echo $i; ?>')" value="<?php echo $mylib->format_number($val["QtyReturn"],2); ?>" size="10" style="text-align: right;"/></td>
									<td align="center"><?php echo $val["Satuan"]; ?></td>
									<td><input type="text" id="v_keterangan_pcode<?php echo $i; ?>" name="v_keterangan_pcode[]" class="form-control-new" value="<?php echo $val["Keterangan"]; ?>" size="50"/></td>
								</tr>
								<?php
								$i++;
							}
							
							?>
								
							</tbody>
						</table>
		        	
		        	</td>
		        </tr>
		        
		        <?php
		        if($ses_login=="sari1608" || $ses_login=="hendri1003")
                {
                	$echo_approve = "Appove";
                	$echo_ket_approve = "";
                	$echo_ket_display = "style='display:none;'";
                	if($header->Approval_Status==2)
                	{
                		$echo_approve = "Reject";
						$echo_ket_approve = $header->Approval_Remarks; 	
                		$echo_ket_display = "";
					}
					
                    if($header->Status==1 && $header->Approval_Status==0)
                    {
                        ?>
                         <tr>
				        	<td class="title_table">Approval <font color="red"><b>(*)</b></font></td>
				        	<td colspan="3">
				        		<select class="form-control-new" name="v_approval" id="v_approval" style="width: 250px;" onchange="muncul_reject(this.value)">
				        			<option value="">Pilih</option>
				        			<option value="1">Approve</option>
				        			<option value="2">Reject</option>
				        		</select>
				        	</td>
				        </tr>
				        
				        <tr id="tr_remark_reject" style="display: none;">
				        	<td class="title_table">Keterangan Reject <font color="red"><b>(*)</b></font></td>
				        	<td colspan="3"><input type="text" class="form-control-new" value="" name="v_keterangan_reject" id="v_keterangan_reject" maxlength="255" size="100"></td>
				        </tr>
                        <?php
                    }
                    else if($header->Status==1 && $header->Approval_Status!=0)
                    {
                        ?>
                         <tr>
				        	<td class="title_table">Approval <font color="red"><b>(*)</b></font></td>
				        	<td colspan="3"><b><?php echo $echo_approve; ?></b></td>
				        </tr>
				        
				        <tr <?php echo $echo_ket_display; ?>>
				        	<td class="title_table">Keterangan Reject <font color="red"><b>(*)</b></font></td>
				        	<td colspan="3"><?php echo $echo_ket_approve; ?></td>
				        </tr>
                        <?php
					}    
                	?>
                	
                	<tr>
			        	<td>&nbsp;</td>
			            <td colspan="100%">
							<input type='hidden' name="flag" id="flag" value="approval">
							<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
							<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close" value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/request_return/"; ?>">Keluar<i class="entypo-cancel"></i></button>    
	                            
							<?php 
		                    if($header->Status==1 && $header->Approval_Status==0)
		                    {
	                            ?>
	                            <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onclick="cekTheform();" value="Approve">Simpan<i class="entypo-check"></i></button>    
	                            <?php
		                    }
		                    else if($header->Status==0 && $header->Approval_Status==0)
		                    {
								echo '<font style="color: red; font-weight: bold;">Button simpan Approval tidak ada, Karena Status masih pending.</font>';	
							}
		                    else if($header->Status==2 && $header->Approval_Status==0)
		                    {
								echo '<font style="color: red; font-weight: bold;">Button Simpan tidak ada, Karena Status sudah void.</font>';
							}
		                    ?>
					
						</td>
				    </tr>
                	<?php                
                }
                else
                {
                	?>
                	<tr>
			        	<td>&nbsp;</td>
			            <td colspan="100%">
							<input type='hidden' name="flag" id="flag" value="edit">
							<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
			            	<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/request_return/"; ?>">Keluar<i class="entypo-cancel"></i></button>
					            	
					        <?php
							if($header->Status==0)
							{
								?>
					            	<button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
						        <?php
							}
							else if($header->Status==2)
							{
								echo '<font style="color: red; font-weight: bold;">Button Simpan tidak ada, Karena Status sudah void.</font>';
							}
							else
							{
								?>
						           	<font style="color: red; font-weight: bold;">Button Simpan tidak ada, Karena Status sudah terkirim.</font>
						     		<span style='float: right; color: blue;'>Approval Waiting Kadek Sariasih</span>
						     	<?php
							}
							?>    
					
						</td>
				    </tr>
                	<?php					
				}
		        ?>
		        
		        
		    </table>
	    
	    </form> 
	    
	    <?php
        if($header->NoDokumen)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->AddUser." :: ".$header->AddDate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->EditUser." :: ".$header->EditDate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font><br/>
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Jika Pcode tidak ada yang di return, Qty Return kosongkan atau diisi dengan 0.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>