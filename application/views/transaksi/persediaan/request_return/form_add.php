<?php 

$this->load->view('header'); 

$modul = "Request Return";

?>
<script language="javascript" src="<?=base_url();?>public/js/request_return.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/request_return/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td  width="200"><b>AutoGenerate</b></td>
	            
	            <td class="title_table" colspan="2">Informasi Receipt Good (RG)</td>
	            
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	            
	            <td class="title_table" width="150">Gudang <font color="red"><b>(*)</b></font></td>
	            <td id="show_rg_gudang" >&nbsp;</td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">RG No <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="hidden" name="v_rg_no" id="v_rg_no" value=""/>
	            	<input type="hidden" name="v_rg_gudang" id="v_rg_gudang" value=""/>
	            	<input type="hidden" name="v_rg_pono" id="v_rg_pono" value=""/>
	            	<input type="hidden" name="v_rg_supplier" id="v_rg_supplier" value=""/>
	            	
	            	<input type="text" name="show_rg_no" id="show_rg_no" value="" placeholder="RG No" class="form-control-new" disabled=""/>&nbsp;
	            	<a href="javascript:void(0)" onclick="getRg()" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari RG" title=""><i class="entypo-search"></i></a>
	            </td>
	            
	            <td class="title_table">Po No <font color="red"><b>(*)</b></font></td>
	            <td id="show_rg_pono" >&nbsp;</td>
	            
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
	            
	            
	            <td class="title_table">Supplier <font color="red"><b>(*)</b></font></td>
	            <td id="show_rg_supplier" >&nbsp;</td>
	        </tr>
	        
	        <tr id="tr_detail_pcode_rg"></tr>
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td colspan="100%">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">      
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/request_return/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>