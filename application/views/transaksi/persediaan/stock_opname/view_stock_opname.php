<?php 
$this->load->view('header'); 
//echo "<pre>";print_r($list_KdSubKategori);echo "</pre>";die();

$modul = "Stock Opname";
?>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>View <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/stock_opname/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen; ?>">
	    <input type="hidden" name="v_gudang" id="v_gudang" value="<?php echo $header->KdGudang; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->NoDokumen; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->TglDokumen!="" && $header->TglDokumen!="00-00-0000") { echo $header->TglDokumen; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        
	        <!--<tr>
	            <td class="title_table" width="150">Warehouse</td>
	            <td><b><?php echo $header->KdGudang." :: ".$header->Keterangan; ?></b></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
	            			$selected="";
							if($header->KdGudang==$val["KdGudang"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        
	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->Keterangan; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <?php
			if($header->status==0 or $header->status==2)
			{
			?>
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->Status==1){ echo "selected='selected'"; } ?> value="1">Open</option>
		            	</select>
		            </td>
		        </tr>
			<?php
			}
			else
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            <?php
		            if($header->Status==1)
		            {
						echo "<b>Open</b>";
					}
					else if($header->status==2)
		            {
						//echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr> 
			<?php
			}
			?>
            
            <tr <?php echo $view_action_button;?>>
	            <td></td>
	            <td>            
	            				<?php if($header->Status==0 OR $header->Approval_Status==0 OR $header->Approval_Status==1 OR $header->Approval_Status==2)
		            				{?>	
		            			<input type="hidden" value="1" name="v_export_excel" id="v_export_excel">
	            				<button type="button" name="btn_excel" id="btn_excel" class="btn btn-info btn-icon btn-sm icon-left" onclick="export_excel('<?php echo $header->NoDokumen;?>');" value="Excel">Export Excel<i class="entypo-check"></i></button>
	            			    <?php }
	            			    
	            			       if($header->Status==1 AND $header->Approval_Status==0)
		            				{?>					
		            				<span style="float: right;">		            				
		            				<?php if($ses_login=='tonny1205' or $ses_login=='mechael0101' or $ses_login=='trisno1402' or $ses_login=='william1001'){?>
		                                
		                                <input type="hidden" value="0" name="v_approve" id="v_approve">
		                                <button type="submit" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $header->NoDokumen;?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
		                                <!--<button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-danger btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>--> 
		                                
		                               <input type="hidden" value="0" name="v_reject" id="v_reject">
		                               <button  type="submit" name="btn_reject" id="btn_reject" class="btn btn-danger btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $header->NoDokumen;?>');" value="Reject">Reject<i class="entypo-check"></i></button>  
		                               <input style=" width: 150px;" type="text" class="form-control-new" name="v_Approval_Remarks" id="v_Approval_Remarks" value="" placeholder="Di isi Jika Reject SO">
	                                <?}else{?>
										<font color="blue">Waiting Approval Bpk. Bambang Sutrisno Atau Bpk. William Budiarsa.</font>
									<?php }?>                      
                                	</span>
                                <?php }else if($header->Status==1 AND $header->Approval_Status==1){ ?>
                                	<font color="blue"><p align="right">SO <?php echo $header->NoDokumen;?> telah di Approve.</p></font>
                                <?php }else if($header->Status==1 AND $header->Approval_Status==2){ ?>
                                	<font color="blue"><p align="right">SO <?php echo $header->NoDokumen;?> telah di Reject.</p></font>
                                <?php } ?>
	            </td>
	        </tr>
	        
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
                                <td width="30">No</td>    
                                <td>PCode</td>    
                                <td>Nama Barang</td>    
                                <td align="right">Qty Fisik</td>    
                                <td align="left">Satuan</td>    
                                <td align="right">Qty Program</td>    
                                <td align="right">Selisih</td>    
                            </tr>
						</thead>
						<tbody>
						
						<?php
						foreach($list_KdSubKategori as $KdSubKategori=>$val)
                            {
                                $NamaSubKategori = $NamaSubKategoris[$KdSubKategori];
                                
                                    ?>
                                    <tr>
                                        <td colspan="100%" style="font-weight: bold;"><?php echo $NamaSubKategori; ?></td>
                                    </tr>
						<?php
						$i=1;
						$tot_fisik =0; 
                        $tot_program =0;
                        $tot_selisih =0;
						
						$tot_all_fisik = 0; 
                        $tot_all_program = 0;
                        $tot_all_selisih = 0;
						foreach($SubKategori_PCode[$KdSubKategori] as $PCode=>$val)
                        {
                            $NamaLengkap = $NamaLengkaps[$PCode];
                            $satuan = $satuans[$PCode];
							$QtyFisik= $datafisik["data_QtyFisik"][$PCode];
							$stock = $get_stock["akhir"][$PCode];
							
							$selisih = $QtyFisik - $stock;
							?>
							<tr>
							    <td align="center"><?php echo $i; ?></td>
								<td align="center"><?php echo $PCode; ?></td>
								<td><?php echo $NamaLengkap; ?></td>
								<td align="right"><input value="<?php echo $QtyFisik; ?>" style="width: 150px; text-align: right;" class="form-control-new" type="text" name="v_QtyFisik_<?php echo $PCode; ?>" id="v_QtyFisik_<?php echo $PCode; ?>" onkeyup="calculate('<?php echo $PCode; ?>')"></td>
								<td><?php echo $satuan; ?></td>
								<td style="text-align: right;"><?php echo $stock; ?>
									<input type="hidden" name="v_QtyProgram_<?php echo $PCode; ?>" id="v_QtyProgram_<?php echo $PCode; ?>" value="<?php echo $stock; ?>">
								</td>
								<td style="text-align: right;" id="td_selisih_<?php echo $PCode; ?>">
								<input type="text" style="width: 150px; text-align: right;" class="form-control-new" id="v_selisih_<?php echo $PCode; ?>" name="v_selisih_<?php echo $PCode; ?>" value="<?php echo $selisih; ?>"/>
								
								
				                
							</tr>
							<?php
							$i++;
							$tot_fisik += $QtyFisik; 
                            $tot_program += $stock;
                            $tot_selisih += $selisih;
							
							$tot_all_fisik += $tot_fisik; 
                            $tot_all_program += $tot_program;
                            $tot_all_selisih += $tot_selisih;
							
						}
						?>
                                <tr style="text-align: right; font-weight: bold;">
                                    <td colspan="3">Total <?php echo $NamaSubKategori; ?></td>
                                    <td><?php echo $tot_fisik; ?></td>
                                    <td>&nbsp;</td>
                                    <td><?php echo $tot_program; ?></td>
                                    <td><?php echo $tot_selisih; ?></td>
                                </tr>
                                <?php
							
							
						}
						?>
							
						</tbody>
						
						<!--<tfoot>
                            <tr style="text-align: right; font-weight: bold;">
                                <td colspan="3">GRAND TOTAL</td>
                                <td><?php echo $tot_all_fisik; ?></td>
                                <td>&nbsp;</td>
                                <td><?php echo $tot_all_program; ?></td>
                                <td><?php echo $tot_all_selisih; ?></td>
                            </tr> 
                        </tfoot>-->
						
					</table>
	        	
	        	</td>
	        </tr>
	        
			
			<?php
			if($header->Status==0 or $header->Status==2 )
			{
			?>
	        
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/stock_opname/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save"  onclick="cekTheform();" value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<input type='hidden' name="flag" id="flag" value="edit">
					    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/stock_opname/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->NoDokumen)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->AddUser." :: ".$header->AddDate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->EditUser." :: ".$header->EditDate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
	function calculate(PCode)
        {
            QtyProgram = $("#v_QtyProgram_"+PCode).val();
            QtyFisik = $("#v_QtyFisik_"+PCode).val();
            selisih = QtyFisik - QtyProgram ;
            $("#v_selisih_"+PCode).val(selisih);
            
        }
        
        function cekTheform()
		{
		    	var yesSubmit = true;
		    	
		        if(yesSubmit)
		        {
					document.getElementById("theform").submit();	
				}  		
		}
		
		function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve "+name+" ? ");
            if(r)
            {
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
        function confirm_reject(name)
        {
        	var yesSubmit = true;
        	
        	/*if(document.getElementById("v_Approval_Remarks").value=="")
		    {
		        alert("Reject Remarks Harus Diisi...");
		        document.getElementById("v_Approval_Remarks").focus();
		        yesSubmit = false;
		        return false;
		    }*/
		    
				if(yesSubmit)
                {
					var r = confirm("Anda yakin ingin Reject "+name+" ? ");
		            if(r)
		            {
		                document.getElementById("v_reject").value = '1';
		                document.getElementById("theform").submit();
		            }
		        }
            
            
        }
        
        function export_excel(name)
        {
        	
        	
        	
            var r = confirm("Anda yakin ingin Export Excel "+name+" ? ");
            if(r)
            {
                document.getElementById("v_export_excel").value = '2';
                document.getElementById("theform").submit();
            }
        }
</script>
