<?php 

$this->load->view('header'); 

$modul = "Penerimaan Lain";

?>

<script language="javascript" src="<?=base_url();?>public/js/penerimaan_lain_v2.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/penerimaan_lain/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Type <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;" onchange="proposal()">
	            		<option value="">Pilih Type</option>
	            		<?php
	            		foreach($minmut as $val)
	            		{
							?><option value="<?php echo $val["purposeid"]; ?>"><?php echo $val["purpose"]; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>

	        <tr id="view_proposal" <?php echo $view_proposal;?> >
	            <td class="title_table">Proposal</td>
	            <td>
	            	<select class="form-control-new" name="v_proposal" id="v_proposal" style="width: 200px;">
	            		<option value="">Pilih Proposal</option>
	            		<?php
	            		foreach($noproposal as $val)
	            		{

							?><option  value="<?php echo $val["NoProposal"]; ?>"><?php echo $val["NoProposal"]; ?></option><?php
						}
	            		?>
	            	</select> 
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Gudang<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;" onchange="cek_tgl_approve_so('<?php echo base_url(); ?>')">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Keterangan</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="toFormat('v_qty<?php echo $no;?>')" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>">
				                	<option value=""> -- Pilih -- </option>
					                <?php        	                					                
					                /*foreach($msatuan as $value)
					                {
					                	?><option value="<?php echo $value["KdSatuan"] ; ?>"><?php echo $value["NamaSatuan"] ; ?></option><?php
									}*/									
					                ?>
				                	</select>
																			
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_keterangan_pcode[]" id="v_keterangan_pcode<?php echo $no;?>" value="" style="width: 300px;" />
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/penerimaan_lain/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
function proposal(){
	  var tipe= $('#v_type').val();
	  var url= $('#base_url').val();

	  		$.ajax({
					url: url+"index.php/transaksi/penerimaan_lain/intmutpurpose/",
					data: {tipes:tipe},
					type: "POST",
					dataType: 'json',					
					success: function(data)
					{
						
						if(data=='1'){
							document.getElementById('view_proposal').style.display = "";
						}else{
							document.getElementById('view_proposal').style.display = "none";
							$('#v_proposal').val("");
						}	
					    						
					},
					error: function(e) 
					{
						//alert(e);
					} 
			});
}

function popupproposal(){
	var	base_url = $("#base_url").val();
		url = base_url+"index.php/pop/pop_up_cari_proposal/";
		
		windowOpener(600, 800, 'Cari Proposal', url, 'Cari Proposal');
}

}
</script>