<?php 

$this->load->view('header'); 

$modul = "Mutasi Gudang";

?>

<script>
	function CallAjax(tipenya,param1,param2,param3,param4,param5)
	{
	    try{    
	        if (!tipenya) return false;
	        
	        if (param1 == undefined) param1 = '';
	        if (param2 == undefined) param2 = '';
	        if (param3 == undefined) param3 = '';
	        if (param4 == undefined) param4 = '';
	        if (param5 == undefined) param5 = '';
	        
	        var variabel;
	        variabel = "";
	        
	        var base_url = "<?php echo base_url(); ?>";
	              
	        if(tipenya=='search_keyword_pb')
	        {  
        		document.getElementById("span_no_pb").innerHTML = 'Loading...';
	            
	            $.post(base_url + "index.php/transaksi/mutasi_gudang/getAjax/", {ajax:tipenya, v_keyword:param1},
        		function (data) {
        			document.getElementById("span_no_pb").innerHTML = data;	
        		});
	        }
	        else if(tipenya=='ajax_nopb')
	        {  
	            $.post(base_url + "index.php/transaksi/mutasi_gudang/getAjax/", {ajax:tipenya, v_nopb:param1},
        		function (data) {
        			
        			if(data)
        			{
        				document.getElementById("tr_detail_pcode_pb").innerHTML = data;	
        				$("#tr_detail_pcode_new").css("display","none");
					}
					else
					{
        				document.getElementById("tr_detail_pcode_pb").innerHTML = "";	
        				$("#tr_detail_pcode_new").css("display","");
					}
        				
        		});
	        }
	    }
	    catch(err)
	    {
	        txt  = "There was an error on this page.\n\n";
	        txt += "Error description AJAX : "+ err.message +"\n\n";
	        txt += "Click OK to continue\n\n";
	        alert(txt);
	    }  
	}
	
</script>

<script language="javascript" src="<?=base_url();?>public/js/mutasi_gudang.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/mutasi_gudang/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Type <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;">
	            		<option value="">Pilih Type</option>
	            		<?php
	            		foreach($minmut as $val)
	            		{
							?><option value="<?php echo $val["purposeid"]; ?>"><?php echo $val["purpose"]; ?></option><?php
						}
	            		?>
	            	</select>  
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Gudang Asal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang_from" id="v_gudang_from" style="width: 200px;" onchange="cek_tgl_approve_so_type2('<?php echo base_url(); ?>')">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang_asal as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Gudang Tujuan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang_to" id="v_gudang_to" style="width: 200px;" onchange="cek_tgl_approve_so_type2('<?php echo base_url(); ?>')">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang_tujuan as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">No PB</td>
	            <td> 
	            	<input type="text" name="v_keyword_pb" id="v_keyword_pb" value="" placeholder="Filter No PB" class="form-control-new" onkeyup="CallAjax('search_keyword_pb', this.value)" />&nbsp;
	            	
	            	<span id="span_no_pb">
		            	<select name="v_no_pb" id="v_no_pb" class="form-control-new" style="width: 200px;" onchange="CallAjax('ajax_nopb', this.value)">
		            		<option value="">Pilih No PB</option>
		            		<?php
		            		foreach($mpb as $val)
		            		{
								?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
							}
		            		?>
		            	</select>
	            	</span>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan</td>
	            <td><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
	        </tr>
	        
	        <tr id="tr_detail_pcode_pb"></tr>
	        
	        <tr id="tr_detail_pcode_new">
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Keterangan</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="cek(this);" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>" >
					                <option value=""> -- Pilih -- </option>
									<?php
					                /*foreach($msatuan as $value)
					                {
					                	?><option value="<?php echo $value["KdSatuan"] ; ?>"><?php echo $value["NamaSatuan"] ; ?></option><?php
									}*/
					                ?>
				                	</select>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_keterangan_pcode[]" id="v_keterangan_pcode<?php echo $no;?>" value="" style="width: 300px;" />
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/mutasi_gudang/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>