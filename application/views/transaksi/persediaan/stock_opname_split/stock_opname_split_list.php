<?php $this->load->view('header'); ?>
<head>
</head>
<script language="javascript" src="<?= base_url(); ?>public/js/stock_opname33.js"></script>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/stock_opname_split/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;<b>Gudang</b>&nbsp;
            <select class="form-control-new" name="search_gudang" id="search_gudang">
                <option value="">All</option>
                <?php
                foreach ($mgudang as $val) {
                    $selected = "";
                    if ($search_gudang) {
                        if ($val["KdGudang"] == $search_gudang) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
                }
                ?>
            </select>
             &nbsp;&nbsp;<b>Tanggal</b>&nbsp;
            <input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen1" id="v_tgl_dokumen1" size="10" maxlength="10">
            
            &nbsp;<b>s.d</b>&nbsp;
            <input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen2" id="v_tgl_dokumen2" size="10" maxlength="10">
            
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/stock_opname_split/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><strong><center>No</center></strong></th>
	            <th><strong><center>No Dokumen</center></strong></th>
	            <th><strong><center>Tanggal</center></strong></th>
	            <th><strong><center>Gudang</center></strong></th>
	            <th><strong><center>Author</center></strong></th>
	            <th><strong><center>Keterangan</center></strong></th>
                <!--<th><strong><center>Approval</center></strong></th>-->    
	            <th width="80"><strong><center>Status</center></strong></th> 
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }
                
                if($val['Approval_By']=="")
	            {
	                $approval = "waiting";
	            } 
	            else
	            {
	                if($val['Approval_Status']==1)
	                {
	                    $approval = "<font color='blue'>Approved By ".$val['Approval_By']." - ".$val['Approval_Date']."</font>";
	                }
	                else if($val['Approval_Status']==2)
	                {
	                    $approval = "<font color='red'>Reject By ".$val['Approval_By']." - ".$val['Approval_Date']."<br>".$val['Approval_Remarks']."</font>";
	                }
	            }

                if ($val["Status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Splited</b></font>";
                } else if ($val["Status"] == 1) {
                    $echo_status = "<font style='color:#6bcf29'><b>Joined</b></font>";
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>Void</b></font>";
                }
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $val["TglDokumen"]; ?></td>
                    <td align="left"><?php echo $val["KdGudang"]." :: ".$val["NamaGudang"]; ?></td>
                    <td align="center"><?php echo $val["AddUser"]; ?></td>
                    <td align="left"><?php echo $val["Keterangan"]; ?></td>
                    <!--<td align="left"><?php echo $approval; ?></td>-->
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                        if ($val["Status"] == 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/stock_opname_split/edit_stock_opname_split/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
							
							<?php if ($val['Approval_Status']!=1 ){ ?>
                            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
						<?php } ?>
                            <?php
                        }

                        if ($val["Status"] == 1) {
                            ?>
                            
                            <a href="<?php echo base_url(); ?>index.php/transaksi/stock_opname_split/view_stock_opname_split/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
							<?php if ($val["Approval_Status"] == 0 OR $val["Approval_Status"] == 2) {?>
                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/stock_opname_split/edit_stock_opname_split/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->
							<?php } ?>
							<!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['NoDokumen'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>
                            
                            
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>-->
							
                            <?php
                        }

                        if ($val["Status"] == 2) {
                            ?>
                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/stock_opname/edit_stock_opname/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>-->
							<a href="<?php echo base_url(); ?>index.php/transaksi/stock_opname/view_stock_opname/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/stock_opname/edit_stock_opname/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['NoDokumen'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>-->
                            
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>-->
                            <?php
                        }
                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
function deleteTrans(nodok,url)
{
	var r=confirm("Apakah Anda Ingin Menghapus No Dokumen "+nodok+" ?")
	if (r==true)
	{
		window.location = url+"index.php/transaksi/stock_opname/delete_trans/"+nodok;	
	}
	else
	{
  		return false;
	}
}
</script>
