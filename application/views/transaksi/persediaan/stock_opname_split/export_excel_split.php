<table class="table table-bordered responsive" border="1">
       		 			<thead class="title_table">
							<tr>
                                <td width="30">No</td>
                                <td>PCode</td>    
                                <td>Nama Barang</td>    
                                <td align="right">Qty Fisik</td>    
                                <td align="left">Satuan</td>    
                                <td align="right">Qty Program</td>    
                                <td align="right">Selisih</td>    
                            </tr>
						</thead>
						<tbody>
						
						<?php
						foreach($list_KdSubKategori as $KdSubKategori=>$val)
                            {
                                $NamaSubKategori = $NamaSubKategoris[$KdSubKategori];
                                
                                    ?>
                                    <tr>
                                        <td colspan="7" style="font-weight: bold;"><?php echo $NamaSubKategori; ?></td>
                                    </tr>
						<?php
						$i=1;
						$tot_fisik =0; 
                        $tot_program =0;
                        $tot_selisih =0;
						
						$tot_all_fisik = 0; 
                        $tot_all_program = 0;
                        $tot_all_selisih = 0;
						foreach($SubKategori_PCode[$KdSubKategori] as $PCode=>$val)
                        {
                            $NamaLengkap = $NamaLengkaps[$PCode];
                            $satuan = $satuans[$PCode];
							$Sid = $sid["data_Sid"][$PCode];
							$QtyFisik= $datafisik["data_QtyFisik"][$PCode];
							$stock = $get_stock["akhir"][$PCode];
							
							$selisih = $QtyFisik - $stock;
							?>
							<tr>
							    <td align="center"><?php echo $i; ?></td>
								<td align="center"><?php echo "#".$PCode; ?></td>
								<td><?php echo $NamaLengkap; ?></td>
								<td align="right"><?php echo $QtyFisik; ?></td>
								<td><?php echo $satuan; ?></td>
								<td style="text-align: right;"><?php echo $stock; ?>
								</td>
								<td style="text-align: right;" id="td_selisih_<?php echo $PCode; ?>">
								<?php echo $selisih; ?></td>
								
								
				                
							</tr>
							<?php
							$i++;
							$tot_fisik += $QtyFisik; 
                            $tot_program += $stock;
                            $tot_selisih += $selisih;
							
							$tot_all_fisik += $tot_fisik; 
                            $tot_all_program += $tot_program;
                            $tot_all_selisih += $tot_selisih;
							
						}
						?>
                                <tr style="text-align: right; font-weight: bold;">
                                    <td colspan="3">Total <?php echo $NamaSubKategori; ?></td>
                                    <td><?php echo $tot_fisik; ?></td>
                                    <td>&nbsp;</td>
                                    <td><?php echo $tot_program; ?></td>
                                    <td><?php echo $tot_selisih; ?></td>
                                </tr>
                                <?php
							
							
						}
						?>
							
						</tbody>
						
						<!--<tfoot>
                            <tr style="text-align: right; font-weight: bold;">
                                <td colspan="3">GRAND TOTAL</td>
                                <td><?php echo $tot_all_fisik; ?></td>
                                <td>&nbsp;</td>
                                <td><?php echo $tot_all_program; ?></td>
                                <td><?php echo $tot_all_selisih; ?></td>
                            </tr> 
                        </tfoot>-->
						
					</table>