<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Split Order";

?>
<script language="javascript" src="<?=base_url();?>public/js/split_order.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action="<?=base_url();?>index.php/transaksi/split_order/save_data">
	    <input type="hidden" name="v_trans" id="v_trans" value="<?php echo $header->NoTrans; ?>">
	    <input type="hidden" name="v_meja" id="v_meja" value="<?php echo $header->KdMeja; ?>">
	    <input type="hidden" name="v_totalitem" id="v_totalitem" value="<?php echo $header->TotalItem; ?>">
	    <input type="hidden" name="v_nokassa" id="v_nokassa" value="<?php echo $header->NoKassa; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <!--<tr>
	            <td class="title_table" width="150">No Transaksi</td>
	            <td><b><?php echo $header->NoTrans; ?></b></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table" width="150">No Meja</td>
	            <td><b><?php echo $header->KdMeja; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->dodate!="" && $header->dodate!="00-00-0000") { echo $header->dodate; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table">Jumlah Split</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $jmlsplit; ?>" name="v_jml_split" id="v_jml_split" maxlength="255" size="10">
	            </td>
	        </tr>
            
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
                <th width="30"><center>No Trans</center></th>
		        <!--<th width="30"><center>Meja</center></th>
		        <th width="30"><center>PCode</center></th>-->
		        <th width="100"><center>Item</center></th>
		        <th width="30"><center>Qty</center></th>
		        <?php for($i=0;$i<$jmlsplit;$i++){
		        	if($i+1==1){
						$hrf="A";
					}else 
					if($i+1==2){
						$hrf="B";
					}else
					if($i+1==3){
						$hrf="C";
					}else
					if($i+1==4){
						$hrf="D";
					}else
					if($i+1==5){
						$hrf="E";
					}else
					if($i+1==6){
						$hrf="F";
					}else
					if($i+1==7){
						$hrf="G";
					}else
					if($i+1==8){
						$hrf="H";
					}else
					if($i+1==9){
						$hrf="I";
					}else
					if($i+1==10){
						$hrf="J";
					}
		        	?>
		        <th width="30"><center>Split Order <?php echo $hrf; ?></center></th>
		        <?php } ?>
		        <th width="30"><center>Item dibayar</center></th>
                </tr>
            
        </thead>
        <tbody>

            <?php
            if (count($detail) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($detail as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f0f0f0;";
                }
				
				        /*if ($val["status_detail"] == 0) {
                        $echo_status = "<font style='color:#068725'><b>Procces</b></font>";
						} else if ($val["status_detail"] == 1) {
							$echo_status = "<font style='color:#3933ff;'><b>Complete</b></font>";
						} else if ($val["status_detail"] == 2) {
							$echo_status = "<font style='color:##FF0000;'><b>Cancel</b></font>";
						}*/
				
                ?>
                <tr title="<?php echo $val["NamaLengkap"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><h4><b><?php echo $no; ?></b></h4></td>
                    <td align="center"><h4><b><input readonly class="form-control-new" type="text" name="notrans[]" id="notrans<?php echo $no;?>" value="<?php echo $val["NoTrans"]; ?>" /></b></h4></td>
                    <td style="display: none" align="center"><h4><b><input readonly class="form-control-new" size="20" type="text" name="kdmeja[]" id="kdmeja<?php echo $no;?>" value="<?php echo $val["KdMeja"]; ?>" /></b></h4></td>
                    <td style="display: none" align="center"><h4><b><input readonly class="form-control-new" size="20" type="text" name="pcode[]" id="pcode<?php echo $no;?>" value="<?php echo $val["PCode"]; ?>" /></b></h4></td>
                    <td align="left"><h4><b><input readonly class="form-control-new" type="text" value="<?php echo $val["NamaLengkap"]; ?>" /></b></h4></td>
                    <td align="center"><h4><b><input style="text-align: right;" readonly class="form-control-new" size="10" type="text" name="qty[]" id="qty<?php echo $no;?>" value="<?php echo $val["Qty"]; ?>" /></b></h4></td>
                    <?php for($i=0;$i<$jmlsplit;$i++){
                    	?>
			        <td align="center"><h4><b><input style="text-align: right;" class="form-control-new" size="5" onblur="HitungJml(this)" type="text" name="split<?php echo $no.$i+1; ?>" id="split<?php echo $no.$i+1;?>" /></b></h4></td>
			        <?php } ?>
			        <!--<td align="center"><h4><b><input class="form-control-new" size="10" type="button" name="save[]" id="save<?php echo $no;?>" onclick="save_detail('<?=base_url();?>',this)" value="Save" /></b></h4></td>-->
                    <td><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $no; ?>" value="0" dir="rtl" class="form-control-new" readonly="readonly"/></td>
                </tr>
                <?php
                $no++;
            }
            ?>
            <!-- style="display: none" -->
			<tr>
			<td colspan='4' align='center'>
			
			<!-- Qty -->
			<input type="hidden" name="v_sJumlah_qty" id="v_sJumlah_qty" value="<?php echo $sumdetail->sum_qty;?>" dir="rtl" class="form-control-new" readonly="readonly"/>
			
			<!-- Qty Sum Split-->
			<input type="hidden" name="v_tot_sJumlah" id="v_tot_sJumlah" value="0" dir="rtl" class="form-control-new" readonly="readonly"/>
			
			<!-- jml split yang tersave -->
			<input type="hidden" name="v_sum_save" id="v_sum_save" value="1" dir="rtl" class="form-control-new" readonly="readonly"/></td>
			
			<?php for($i=0;$i<$jmlsplit;$i++){?>
<td align="center"><h4><b><input class="form-control-new" size="15" type="button" name="save[]" id="save<?php echo $i+1;?>" onclick="save_detail('<?=base_url();?>',this)" value="Save" /></b></h4></td>
			<?php } ?>
			</tr>

        </tbody>
		
    </table>
					
	        	</td>
	        </tr>
	        
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/split_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <!--<button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>-->
                    <!--<button type="submit" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>-->
				</td>
	        </tr>
	        
	        
	    </table>
	    </form> 
	    	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>