		<input type="hidden" id="jml_split" value="<?=$jmlsplit;?>">
		<input type="hidden" id="jml_detail" value="<?=count($detail);?>">			
		<input type="hidden" name="v_meja" id="v_meja" value="<?php echo $header->KdMeja; ?>">
	    <input type="hidden" name="v_totalitem" id="v_totalitem" value="<?php echo $header->TotalItem; ?>">
	    <input type="hidden" name="v_nokassa" id="v_nokassa" value="<?php echo $header->NoKassa; ?>">
			
		<table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="10"><center>No</center></th>
		        <th><center>Item</center></th>
		        <th width="30"><center>Qty</center></th>
		        <?php for($i=1;$i<=$jmlsplit;$i++){
		        	if($i==1){
						$hrf="A";
					}else 
					if($i==2){
						$hrf="B";
					}else
					if($i==3){
						$hrf="C";
					}else
					if($i==4){
						$hrf="D";
					}else
					if($i==5){
						$hrf="E";
					}else
					if($i==6){
						$hrf="F";
					}else
					if($i==7){
						$hrf="G";
					}else
					if($i==8){
						$hrf="H";
					}else
					if($i==9){
						$hrf="I";
					}else
					if($i==10){
						$hrf="J";
					}else
					if($i==11){
						$hrf="K";
					}else
					if($i==12){
						$hrf="L";
					}else
					if($i==13){
						$hrf="M";
					}else
					if($i==14){
						$hrf="N";
					}else
					if($i==15){
						$hrf="O";
					}
		        	?>
		        <th width="10"><center><?php echo $table.$hrf; ?><input type="hidden" name="v_meja_split" id="v_meja_split<?=$i;?>" value="<?php echo $table.$hrf; ?>"></center></th>
		        <?php } ?>
		        <th width="10"><center>Fix</center></th>
				<th width="10"><center>&nbsp;</center></th>
                </tr>
            
        </thead>
        <tbody>

            <?php
            if (count($detail) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($detail as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#ffffff;";
				}

				if($no<10){
							$num = '0'.$no;
						}else{
							$num = $no;
						}
				
				        /*if ($val["status_detail"] == 0) {
                        $echo_status = "<font style='color:#068725'><b>Procces</b></font>";
						} else if ($val["status_detail"] == 1) {
							$echo_status = "<font style='color:#3933ff;'><b>Complete</b></font>";
						} else if ($val["status_detail"] == 2) {
							$echo_status = "<font style='color:##FF0000;'><b>Cancel</b></font>";
						}*/
				
                ?>
                <tr title="<?php echo $val["NamaLengkap"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" id="<?php echo $num; ?>">
                    <td><h4><b><?php echo $no; ?></b></h4></td>
                    <input readonly class="form-control-new" type="hidden" name="notrans[]" id="notrans<?php echo $num;?>" value="<?php echo $val["NoTrans"]; ?>" />
                    <td style="display: none" align="center"><h4><b><input readonly class="form-control-new" size="20" type="text" name="kdmeja[]" id="kdmeja<?php echo $num;?>" value="<?php echo $val["KdMeja"]; ?>" /></b></h4></td>
                    <td style="display: none" align="center"><h4><b><input readonly class="form-control-new" size="20" type="text" name="pcode[]" id="pcode<?php echo $num;?>" value="<?php echo $val["PCode"]; ?>" /></b></h4></td>
                    <td align="left"><h4><b><?php echo $val["NamaLengkap"]; ?></b></h4></td>
                    <td align="center"><h4><b><input style="text-align: right;" readonly class="form-control-new" size="3" type="hidden" name="qty[]" id="qty<?php echo $num;?>" value="<?php echo $val["Qty"]; ?>" /><?php echo $val["Qty"]; ?></b></h4></td>
					
                    <?php for($i=1;$i<=$jmlsplit;$i++){
						$bgcolors = "";
						if ($i % 2 == 1) {
							$bgcolors = "background:#f0f0f0;";
						}
                    	?>
			        <td style="<?php echo $bgcolors; ?>" align="center"><h4><b><input style="text-align: right;" class="form-control-new" size="3" onkeyup="HitungJml(this),HitungJmlMenurun('<?=count($detail);?>','<?php echo $i;?>')" type="text" name="split<?php echo $num.$i; ?>" id="split<?php echo $num.$i;?>" value="" /></b></h4></td>
			        <?php } ?>
			        <!--<td align="center"><h4><b><input class="form-control-new" size="10" type="button" name="save[]" id="save<?php echo $no;?>" onclick="save_detail('<?=base_url();?>',this)" value="Save" /></b></h4></td>-->
                    <td style="vertical-align: middle; text-align: center;"><input style="text-align: right;" size="3" type="hidden" name="v_sJumlah[]" id="v_sJumlah_<?php echo $num; ?>" value="0"  class="form-control-new" readonly="readonly"/><div id="s_jumlah<?php echo $num; ?>">0</div></td>
					<td style="vertical-align: middle; text-align: center;"><div id="s_status<?php echo $num; ?>"><img src="../../../../public/images/cancel.png"/></div></td>
                </tr>
                <?php
                $no++;
            }
            ?>
            <!-- style="display: none" -->
			<tr style="display: none">
			<td colspan='3' align='center'>
			
			<!-- Qty -->
			<input type="hidden" name="v_sJumlah_qty" id="v_sJumlah_qty" value="<?php echo $sumdetail->sum_qty;?>" dir="rtl" class="form-control-new" readonly="readonly"/>
			
			<!-- Qty Sum Split-->
			<input type="hidden" name="v_tot_sJumlah" id="v_tot_sJumlah" value="0" dir="rtl" class="form-control-new" readonly="readonly"/>
			
			<!-- jml split yang tersave -->
			<input type="hidden" name="v_sum_save" id="v_sum_save" value="1" dir="rtl" class="form-control-new" readonly="readonly"/></td>
			
			<?php for($i=1;$i<=$jmlsplit;$i++){?>
<td align="center" >
<input style="text-align: right;" class="form-control-new" size="3" type="text" name="qty_turun<?php echo $no.$i; ?>" id="qty_turun<?php echo $no.$i;?>" value="0" />
</td>
			<?php } ?>
			</tr>

        </tbody>
		
    </table>
					
	        	</td>
	        </tr>
	        
	     <table class="table table-bordered responsive">   	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/split_order/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <!--<button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>-->
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_saves" id="btn_saves" onclick="deal_split();" value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        
	    </table>

         

 