<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Split Order";

$counter=1;

?>
<link rel="stylesheet" href="<?= base_url(); ?>public/jquery_confirm/dist/jquery-confirm.min.css">
<script src="<?= base_url(); ?>public/jquery_confirm/dist/jquery-confirm.min.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Creat <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/split_order/edit_split_order/<?php echo $header->KdMeja; ?>'>
	    <input type="hidden" name="v_trans" id="v_trans" value="<?php echo $header->NoTrans; ?>">
	    <input type="hidden" name="v_meja" id="v_meja" value="<?php echo $header->KdMeja; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <!--<tr>
	            <td class="title_table" width="150">No Transaksi</td>
	            <td><b><?php echo $header->NoTrans; ?></b></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table" width="150">No Meja</td>
	            <td><b><?php echo $header->KdMeja; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->dodate!="" && $header->dodate!="00-00-0000") { echo $header->dodate; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table">Jumlah Split</td>
	            <td><select class="form-control-new" name="v_jml_split" id="v_jml_split" style="width: 20;">
                    <option value="0">--</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>                    
                    </select>
	            
	            
	            	&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="split_order()" value="Simpan">Split Order<i class="entypo-search"></i></button>
	            </td>
	        </tr>
	        
	    </table>
	    </form>  
	</div>
	
	<!-- Detail Split -->
	<div id="getList"></div>
	
	
</div>

          <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3><div id="pesan_loading">Proses...</div></h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>

	function split_order(){
		        meja = $('#v_meja').val();
		        tgl_dokumen = $('#v_tgl_dokumen').val();
				jml = $('#v_jml_split').val();
				
				if(jml=='0'){
					$.alert({
				        title: 'Alert!',
				        content: 'Harus Ada Jumlah Split.',
				    });				    	
				}else{
					
					    $('#pleaseWaitDialog').modal('show');
						$.ajax({
				            url: "<?php echo site_url('transaksi/split_order/pecah_order') ?>/" +meja+"/"+tgl_dokumen+"/"+jml,
				            type: "GET",
				            dataType: "html",
				            success: function (data)
				            {
				                    $('#pleaseWaitDialog').modal('hide');
								  	$('#getList').html(data);								  	
								  								      		
				            }
				            ,
				            error: function (textStatus, errorThrown)
				            {
				                    $.alert({
								        title: 'Alert!',
								        content: 'Error Get Data',
								    });
				            }
				        }
				        );
					
				}
		        	
	}
	
	
	function deal_split(){
		jml_split = $('#jml_split').val();
		jml_detail= $('#jml_detail').val();
		notrans=$("#v_trans").val();
	    v_sJumlah_qty = $('#v_sJumlah_qty').val();
	    v_tot_sJumlah = $('#v_tot_sJumlah').val();
	    
		//for jumlah kolom
		if(v_sJumlah_qty==v_tot_sJumlah){
			
									hitung_nol=0;
			                        //cek apakah ada split yang jumlahnya nol
									record = parseInt(jml_detail)+1;							
									for(x=jml_split;x>=1;x--){
										if($('#qty_turun'+record+x).val()=='0'){
										//alert($('#qty_turun'+record+x).val());
										hitung_nol+=1;
										}
									}
									
			if(hitung_nol>0){
				qoute = "Ada "+hitung_nol+" Split Yang Masih Nol, Apakah Anda Ingin Tetap Lanjut Split Transaksi ini?"
			}else{
				qoute = "Apakah Anda Yakin Lanjut Split Transaksi ini?"
			}
			
		    var r=confirm(qoute)
			if (r==true)
			{
					for(x=1;x<=jml_split;x++){
						
						//for untuk baris
						kum_split="";
						kum_meja="";
						kum_pcode="";
						
						
						for(y=1;y<=jml_detail;y++){
							
							if(jml_detail<10){
								splits_ke = $('#split0'+y+x).val();
								pcode = $('#pcode0'+y).val();
							}else{
								splits_ke = $('#split'+y+x).val();
								pcode = $('#pcode'+y).val();
							}
							
							meja_split = $('#v_meja_split'+x).val();
							
							
							kum_split+=splits_ke+"-";
							kum_meja+=meja_split+"-";
							kum_pcode+=pcode+"-";
							
							if(y==jml_detail){
											//insert detail
											
											$.ajax({										
												url: "<?php echo site_url('transaksi/split_order/save_detail_new') ?>/",
												data: {no_trans_old:notrans,qty_split:kum_split,pc:kum_pcode,meja:kum_meja,jmlrecord:jml_detail,tambah_urutan_id:x},
												type: "POST",
												dataType: 'json',					
												success: function(data)
												{
													
												},
												error: function(e) 
												{
													//alert(e);
												} 
											 });
								
								//alert('Split ke '+x);
							}
							
						}			
					}
			}
			else
			{
				return false;
			}						
									$.alert({
								        title: 'Alert!',
								        content: 'Split Telah Sukses',
								    });
                                    $('#btn_save').attr('disabled',true);
                                    document.getElementById("btn_saves").style.display = "none";									
		}else{
			                         $.alert({
								        title: 'Alert!',
								        content: 'Ada Transaksi Yang Belum Ikut Di Split',
								    });
		}
		
	}
	
	
	
	
	
	
	
	
	
	function complete(namabarang,pcode,notrans,search,url)
{
	var r=confirm("Apakah Anda Menu "+namabarang+" complete?")
	if (r==true)
	{
		window.location = url+"index.php/transaksi/pending_order/complete/"+notrans+"/"+pcode+"/"+search+"/";	
	}
	else
	{
  		return false;
	}
}

function cancel(namabarang,pcode,notrans,search,url)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : url+"index.php/transaksi/pending_order/cancel/"+notrans+"/"+pcode+"/",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="NoKassa"]').val(data.NoKassa);
            $('[name="NoTrans"]').val(data.NoTrans);
            $('[name="NoUrut"]').val(data.NoUrut);
            $('[name="PCode"]').val(data.PCode);
			$('[name="NamaBarang"]').val(data.NamaBarang);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Otorisasi User'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save(url,search)
{
	var user=$("#username").val();
	var password=$("#password").val();
	var notrans=$("#notrans").val();
	var pcode=$("#pcode").val();
	
    $('#btnSave').text('confirm...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
	//alert(user+" - "+password+" - "+notrans+" - "+pcode);
    window.location = url+"index.php/transaksi/pending_order/done2/"+search+"/"+user+"/"+password+"/"+notrans+"/"+pcode+"/";    
}

function HitungJmlMenurun(jmlrecord,ke)
{   
    temp=parseInt(jmlrecord)+1;
    tot_qty=0;
	for(x=1;x<=jmlrecord;x++){
		qty_splits = parseInt($('#split'+x+ke).val());
		tot_qty+=qty_splits;
	}
	//alert(tot_qty);
	$('#qty_turun'+temp+ke).val(tot_qty);
}

function HitungJml(obj)
{
	objek = obj.id;
	id = objek.substr(5,objek.length-5);
	var res = id.substr(0, 2);
	var jmlsplit=$("#v_jml_split").val();
	var qty=$("#qty"+res).val();
	var totsplit=$("#v_sJumlah_"+res).val();
	
	/*if(qty==totsplit){
		alert("Melebihi Qty Item.");
		$("#split"+id).val("");
		}else{*/
		
	    tot_split=0;
		for(x = 0; x < jmlsplit; x++){
			y=x+1;
			var split=$("#split"+res+""+y).val();
			
				if(split==""){
					split_ = 0;
				}else{
					split_=parseInt(split);
				}
				tot_split+=split_;
			}
	//}	
		
		if(qty<tot_split){
		                             $.alert({
								        title: 'Alert!',
								        content: 'Melebihi Quantity Yang DiPesan.',
								    });
		$("#split"+id).val("0");
		}else{
			
			if(qty==tot_split){
				document.getElementById("s_status"+res).innerHTML = '<img src="../../../../public/images/accept.png"/>';
			}else{
				document.getElementById("s_status"+res).innerHTML = '<img src="../../../../public/images/cancel.png"/>';
			}
			
		$("#v_sJumlah_"+res).val(tot_split);
		document.getElementById("s_jumlah"+res).innerHTML = tot_split;
		total_jum_split();
       }
}

function total_jum_split()
{
    var lastRow = document.getElementsByName("v_sJumlah[]").length;
    var stotal = 0;//v_Jumalah atau total atas
    for (index = 0; index < lastRow; index++)
    {
        indexs = index - 1;
        nama = document.getElementsByName("v_sJumlah[]");
        temp = nama[index].id;
        temp1 = parseFloat(nama[index].value);
        stotal += temp1; 
    }
    $("#v_tot_sJumlah").val(Math.round(stotal));

}

function save_detail(url,obj)
{
	objek = obj.id;
	id = objek.substr(4,objek.length-4);
	
	var notrans=$("#v_trans").val();
	var kdmeja=$("#v_meja").val();
	var lastRow = document.getElementsByName("notrans[]").length;
		
	if($('#v_sum_save').val() == $("#v_jml_split").val()){
			if(parseInt($("#v_sJumlah_qty").val()) > parseInt($("#v_tot_sJumlah").val())){
				alert("Transaksi Terakhir Gagal, Karena Ada Item Yang Belum Ikut Dibayar.");
			}else{		
				$.ajax({
					url: url+"index.php/transaksi/split_order/save_header/",
					data: {no_trans:notrans,meja:kdmeja,id:id},
					type: "POST",
					dataType: 'json',					
					success: function(data)
					{
						notrans_ = data.notrans;
						
						
						for(x = 0; x < lastRow; x++){
							    no = x+1;
								var hasil_split=$("#split"+no+""+id).val();
								
								if(hasil_split!=""){				
									var pcode=$("#pcode"+no).val();
									var notrans=$("#notrans"+no).val();
									 //alert(pcode+" - "+hasil_split);
									 
									 //simpan dengan ajax				 
									 $.ajax({
										url: url+"index.php/transaksi/split_order/save_detail/",
										data: {no_trans_old:notrans,no_trans_new:notrans_,qty_split:hasil_split,pc:pcode,meja:kdmeja,id:id,no:no},
										type: "POST",
										dataType: 'json',					
										success: function(data)
										{
											
										},
										error: function(e) 
										{
											//alert(e);
										} 
									 });
								}
							}
						
						var sum_save = parseInt($('#v_sum_save').val());
						var sum_save_ = sum_save+1;
						$('#v_sum_save').val(sum_save_);
						
						alert("Sukses Split Order");
						$('#save'+id).val('DONE');
						$('#save'+id).attr('disabled',true);
						
						
					},
					error: function(e) 
					{
						//alert(e);
					} 
				 });
			}	 		 
		}else{		
				$.ajax({
					url: url+"index.php/transaksi/split_order/save_header/",
					data: {no_trans:notrans,meja:kdmeja,id:id},
					type: "POST",
					dataType: 'json',					
					success: function(data)
					{
						notrans_ = data.notrans;
						
						
						for(x = 0; x < lastRow; x++){
							    no = x+1;
								var hasil_split=$("#split"+no+""+id).val();
								
								if(hasil_split!=""){				
									var pcode=$("#pcode"+no).val();
									var notrans=$("#notrans"+no).val();
									 //alert(pcode+" - "+hasil_split);
									 
									 //simpan dengan ajax				 
									 $.ajax({
										url: url+"index.php/transaksi/split_order/save_detail/",
										data: {no_trans_old:notrans,no_trans_new:notrans_,qty_split:hasil_split,pc:pcode,meja:kdmeja,id:id,no:no},
										type: "POST",
										dataType: 'json',					
										success: function(data)
										{
											
										},
										error: function(e) 
										{
											//alert(e);
										} 
									 });
								}
							}
						
						var sum_save = parseInt($('#v_sum_save').val());
						var sum_save_ = sum_save+1;
						$('#v_sum_save').val(sum_save_);
						
						alert("Sukses Split Order");
						$('#save'+id).val('DONE');
						$('#save'+id).attr('disabled',true);
						
						
					},
					error: function(e) 
					{
						//alert(e);
					} 
				 });
			}
	
}

function done(kdmeja,url)
{
	var r=confirm("Order is Done?")
	if (r==true)
	{
		window.location = url+"index.php/transaksi/pending_order/done/"+kdmeja+"/";	
	}
	else
	{
  		return false;
	}
}

function deleteRow(obj)
{
	objek = obj.id;
	id = objek.substr(15,objek.length-3);
	
	var lastRow = document.getElementsByName("pcode[]").length;
	
	if( lastRow > 1)
	{
		$('#baris'+id).remove();
	}else{
			alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
	}
}


	
</script>
