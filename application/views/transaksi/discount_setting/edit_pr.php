<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Discount Setting";

?>
<script language="javascript" src="<?=base_url();?>public/js/discount_setting.js"></script>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    } 

    function add_PCode()
    {
        targetBlank("<?=base_url()."inventory/npm_master_barang.php";?>");
    }
</script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		

		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/discount_setting/save_discount'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoTrans; ?>">
	    <table class="table table-bordered responsive">

            <tr>
                <td class="title_table" width="150">No Discount</td>
                <td><b><?php echo $header->NoTrans; ?></b></td>
            </tr>
            <tr>
                <td class="title_table">Ketentuan <font color="red"><b>(*)</b></font></td>
                <td><input type="text" class="form-control-new" value="<?php echo $header->Ketentuan; ?>" name="v_ketentuan" id="v_ketentuan" maxlength="255" size="100"></td>
            </tr>
            <tr>
                <td class="title_table" width="150">Tanggal Awal<font color="red"><b>(*)</b></font></td>
                <td>
                    <input type="text" class="form-control-new datepicker" value="<?php echo $header->DateAwal; ?>" name="v_date_awal" id="v_date_awal" size="10" maxlength="10">
                </td>
            </tr>


            <tr>
                <td class="title_table" width="150">Tanggal Akhir<font color="red"><b>(*)</b></font></td><!-- Estimasi Terima -->
                <td>
                    <input type="text" class="form-control-new datepicker" value="<?php echo $header->DateAkhir; ?>" name="v_date_akhir" id="v_date_akhir" size="10" maxlength="10">
                </td>
            </tr>


            <tr>
                <td class="title_table">Total Disc (%) <font color="red"><b>(*)</b></font></td>
                <td><input type="text" class="form-control-new" value="<?php echo $header->disc; ?>" name="v_disc" id="v_disc" maxlength="255" size="100" readonly></td>
            </tr>


            <tr>
                <td class="title_table">Minimul Belanja <font color="red"><b>(*)</b></font></td>
                <td><input type="text" class="form-control-new" value="<?php echo $header->Minimum; ?>" name="v_minimum" id="v_minimum" maxlength="255" size="100" readonly></td>
            </tr>
            <tr>
                <td class="title_table">Hari Berlaku <font color="red"><b>(*)</b></font></td>
                <td>
                    <?php
                    $pisah_hari= explode("#",$header->berlaku);
                    $tanda_pager = substr_count($header->berlaku, "#");
                    $hari=[7,7,7,7,7,7,7];
                    $dayname = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
                    for($x=0; $x<($tanda_pager*1); $x++){


                        for($i=0;$i<7;$i++){
                            if($pisah_hari[$x]==$i){
                                $hari[$i]=$pisah_hari[$x];
                            }
                        }

//                        echo $pisah_hari[$x];



                    }
                    for($ix=0;$ix<7;$ix++){
                        if($hari[$ix]!==7){


                        ?>
                        <input type="checkbox" class="form-control-neww" name="day_list[]" value="<?=$ix?>" checked><label style="margin-left: 5px;margin-right: 20px;"><?=$dayname[$ix]?></label>
                        <?php
                        }else{
                            ?>
                            <input type="checkbox" class="form-control-neww" name="day_list[]" value="<?=$ix?>"><label style="margin-left: 5px;margin-right: 20px;"><?=$dayname[$ix]?></label>

                            <?php
                        }
                    }
//                    print_r($hari);

                    ?>

<!--                    <input type="checkbox" class="form-control-neww" name="day_list[]" value="0"><label style="margin-left: 5px;margin-right: 20px;">Minggu</label>-->
<!--                    <input type="checkbox" class="form-control-neww" name="day_list[]" value="1"><label style="margin-left: 5px;margin-right: 20px;">Senin</label>-->
<!--                    <input type="checkbox" class="form-control-neww" name="day_list[]" value="2"><label style="margin-left: 5px;margin-right: 20px;">Selasa</label>-->
<!--                    <input type="checkbox" class="form-control-nwew" name="day_list[]" value="3"><label style="margin-left: 5px;margin-right: 20px;">Rabu</label>-->
<!--                    <input type="checkbox" class="form-control-wnew" name="day_list[]" value="4"><label style="margin-left: 5px;margin-right: 20px;">Kamis</label>-->
<!--                    <input type="checkbox" class="form-control-nwew" name="day_list[]" value="5"><label style="margin-left: 5px;margin-right: 20px;">Jumat</label>-->
<!--                    <input type="checkbox" class="form-control-wnew" name="day_list[]" value="6"><label style="margin-left: 5px;margin-right: 20px;">Sabtu</label>-->

                </td>

            </tr>

	        <?php
			if($header->Status==0)
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->Status==1){ echo "selected='selected'"; } ?> value="1">Open</option>
		            		<option <?php if($header->Status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>  
			<?php
			}

			?>


            
            <?php
			if($header->Status==9)
			{
			?>

            <tr>
                <td colspan="100%">
                    <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Add PCode" title="" name="btn_add_pcode" id="btn_add_pcode" value="Add PCode" onClick="add_PCode()">
                        <i class="entypo-plus"></i> Add PCode
                    </button>
                </td>
            </tr>

            <?php
			}
			?>
            
			<?php
			if(count($detail)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
<!--							    <th width="50"><center>Qty</center></th>-->
<!--							    <th width="100"><center>Satuan</center></th>-->
							    
							    <?php
							    if($header->Status==0)
							    {
									echo '<th width="100"><center>Delete</center></th>';	
								}
							    ?>
							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$i=1;
						foreach($detail as $val)
						{
							?>
							<tr>
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaLengkap"]; ?></td>
<!--								<td align="right">--><?php //echo $mylib->format_number($val["Qty"],2); ?><!--</td>-->
<!--								<td align="center">--><?php //echo $val["Satuan"]; ?><!--</td>-->
								
								<?php
							    if($header->Status==0)
							    {
							    	?>
							    	<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetailpr('<?php echo $val["PCode"]; ?>','<?php echo $header->NoTrans; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
							    	<?php									
								}
							    ?>
				                
							</tr>
							<?php
							$i++;
						}
						
						?>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        
	        <?php
	        }
	        ?>
			
			<?php
			if($header->Status==0)
			{
			?>
			
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
<!--							    <th width="50"><center>Qty</center></th>-->
<!--							    <th width="100"><center>Satuan</center></th>-->
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input readonly type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>

				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/discount_setting/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/discount_setting/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada, Karena Status OPEN</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
	        
	    </table>
	    </form> 
	    

	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>