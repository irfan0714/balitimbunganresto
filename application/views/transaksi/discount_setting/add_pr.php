<?php 

$this->load->view('header'); 

$modul = "Discount Setting";

//$arr_data(unset);
?>

<script language="javascript" src="<?=base_url();?>public/js/discount_setting.js"></script>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    } 

    function add_PCode()
    {
        targetBlank("<?=base_url()."inventory/npm_master_barang.php";?>");
    }
</script>



<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		<?php
        echo date('l', strtotime(date("Y-m-d")));
        ?>
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/discount_setting/save_discount'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Discount</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
            <tr>
                <td class="title_table">Ketentuan <font color="red"><b>(*)</b></font></td>
                <td><input type="text" class="form-control-new" value="" name="v_ketentuan" id="v_ketentuan" maxlength="255" size="100"></td>
            </tr>
	        <tr>
	            <td class="title_table" width="150">Tanggal Awal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_date_awal" id="v_date_awal" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal Akhir<font color="red"><b>(*)</b></font></td><!-- Estimasi Terima -->
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_date_akhir" id="v_date_akhir" size="10" maxlength="10">
	            </td>
	        </tr>


            <tr>
                <td class="title_table">Total Disc (%) <font color="red"><b>(*)</b></font></td>
                <td><input type="text" class="form-control-new" value="0" name="v_disc" id="v_disc" maxlength="255" size="100"></td>
            </tr>

	        
	        <tr>
	            <td class="title_table">Minimul Belanja <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="0" name="v_minimum" id="v_minimum" maxlength="255" size="100"></td>
	        </tr>
            <tr>
                <td class="title_table">Hari Berlaku <font color="red"><b>(*)</b></font></td>
                <td>
                    <input type="checkbox" class="form-control-neww" name="day_list[]" value="0"><label style="margin-left: 5px;margin-right: 20px;">Minggu</label>
                    <input type="checkbox" class="form-control-neww" name="day_list[]" value="1"><label style="margin-left: 5px;margin-right: 20px;">Senin</label>
                    <input type="checkbox" class="form-control-neww" name="day_list[]" value="2"><label style="margin-left: 5px;margin-right: 20px;">Selasa</label>
                    <input type="checkbox" class="form-control-nwew" name="day_list[]" value="3"><label style="margin-left: 5px;margin-right: 20px;">Rabu</label>
                    <input type="checkbox" class="form-control-wnew" name="day_list[]" value="4"><label style="margin-left: 5px;margin-right: 20px;">Kamis</label>
                    <input type="checkbox" class="form-control-nwew" name="day_list[]" value="5"><label style="margin-left: 5px;margin-right: 20px;">Jumat</label>
                    <input type="checkbox" class="form-control-wnew" name="day_list[]" value="6"><label style="margin-left: 5px;margin-right: 20px;">Sabtu</label>

                </td>

            </tr>
            
            <!--<tr>
                <td colspan="100%">
                    <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Add PCode" title="" name="btn_add_pcode" id="btn_add_pcode" value="Add PCode" onClick="add_PCode()">
                        <i class="entypo-plus"></i> Add PCode
                    </button>
                </td>
            </tr>-->
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               

							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input readonly type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>

				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/discount_setting/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>