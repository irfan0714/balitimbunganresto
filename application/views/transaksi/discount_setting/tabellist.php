<?php $this->load->view('header'); ?>

<script language="javascript" src="<?=base_url();?>public/js/purchase_request_v2.js"></script>
<script text='javascript'>
function VoidTrans(param){
  //if (confirm("Anda yakin ingin menghapus transaksi ini?")){
  window.open(param,'','width=400,height=300,left=400,top=250');
  //}
}
</script>
</head>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/purchase_request/search'>
  <input type="hidden" name="btn_search" id="btn_search" value="y"/>
  <div class="rows">

      <!--<div class="col-md-10">-->
      <b>Search</b>&nbsp;
      <input placeholder="Kata Kunci" type="text" size="30" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" />


      &nbsp;

      &nbsp;
      <!--</div>

      <div class="col-lg-12" align="right">-->
      <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
      <!--</div>-->

    <br>
    <div class="col-md-12" align="center">
      <a href="<?php echo base_url()."index.php/transaksi/discount_setting/add_new/"; ?>" onClick="show_loading_bar(100)" class="btn btn-orange btn-icon btn-sm icon-left" title="" >Tambah Discount<i class="entypo-plus"></i></a>
    </div>

  </div>
</form>

<hr/>

<?php
if($this->session->flashdata('msg'))
{
  $msg = $this->session->flashdata('msg');

  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
  <table class="table table-bordered responsive">
    <thead class="title_table">
      <tr>
        <th width="30"><center>No</center></th>
        <th width="150"><center>No Discount</center></th>
        <th ><center>Ketentuan</center></th>
        <th width="150"><center>Tanggal Awal</center></th>
        <th width="100"><center>Tanggal Akhir</center></th>
        <th width="100"><center>Minimum</center></th>
        <th><center>Status</center></th>
        <th><center>Action</center></th>

      </tr>
    </thead>
    <tbody>

      <?php
      if(count($data)==0)
      {
        echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
      }

      $no=1;
      foreach($data as $val)
      {
        $bgcolor = "";
        if($no%2==0)
        {
          $bgcolor = "background:#f7f7f7;";
        }

        if($val["Status"]==0)
        {
          $echo_status =  "<font style='color:#ff1c1c'>Pending</font>";
        }
        else if($val["Status"]==1)
        {
          $echo_status = "<font style='color:#6bcf29'>Open</font>";
        }
        else if($val["Status"]==2)
        {
          $echo_status = "<font style='color:#ff1c1c;'>Void</font>";
        }

        ?>
        <tr title="<?php echo $val["NoTrans"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
          <td><?php echo $no; ?></td>
          <td align="center"><?php echo $val["NoTrans"]; ?></td>
          <td align="center"><?php echo $val["Ketentuan"]; ?></td>
          <td align="center"><?php echo $val["TglAwal"]; ?></td><!-- -->
          <td align="center"><?php echo $val["TglAkhir"]; ?></td>
          <td><?php echo $val["Minimum"]; ?></td>
          <td><?php echo $echo_status; ?></td>

          <td align="center">
              <?php
              if($val["Status"]!=='1'){
                  ?>
                  <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoTrans"]; ?>','<?php echo base_url(); ?>');" >
                      <i class="entypo-trash"></i>
                  </button>

                  <?php
              }
              ?>
              <a href="<?php echo base_url();?>index.php/transaksi/discount_setting/edit_disc/<?php echo $val["NoTrans"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>


          </td>

        </tr>
        <?php

        $no++;
      }
      ?>

    </tbody>
  </table>

  <div class="row">
    <div class="col-xs-6 col-left">
      <div id="table-2_info" class="dataTables_info">&nbsp;</div>
    </div>
    <div class="col-xs-6 col-right">
      <div class="dataTables_paginate paging_bootstrap">
        <?php echo $pagination; ?>
      </div>
    </div>
  </div>

</div>


<?php $this->load->view('footer'); ?>
