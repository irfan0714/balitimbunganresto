<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'terimabarang',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/terima_barang.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('terimabarang');loading()">
<div class="box-body sidebar"  style="overflow-x: auto">
<form method='post' name="terimabarang" id="terimabarang" action='<?=base_url();?>index.php/transaksi/terima_barang/save_new_terima' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Terima Barang</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="11" name="nodok" id="nodok" readonly='readonly'/></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"10","10","readonly='readonly'","text",$gantikursor,"1");
		    ?>
			<tr>
				<td nowrap>Asal Data</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="M" checked name="sumberMO" id="sumberMO" onclick="ubahsumberMO();"/>Manual
					<input type="radio" value="O" name="sumberMO" id="sumberMO" onclick="ubahsumberMO();"/>Purchase Order
				</td>
			</tr>
			<tr>
				<td nowrap>No PO</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="11" size="11" name="noorder" id="noorder" disabled onkeydown="keyShortcut(event,'order',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickOrder();" id="btnorder" disabled>
				<input type="hidden" name="hiddennoorder" id="hiddennoorder">
				</td>
			</tr>
			<?php
		    echo $mylib->write_combo("Gudang","gudang",$gudang,"","KdGudang","NamaGudang",$gantikursor,"onchange='CatatGudang()'","ya");
			?>
			<tr>
				<td nowrap>Supplier</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" >
					<input type="text" maxlength="10" size="10" name="kdsupplier" id="kdsupplier" onkeydown="keyShortcut(event,'supplier',this);" <?=$gantikursor;?> /> 
					<input type="button" value="..." onclick="pickSupplier();" id="btnsupplier">
					<input type="hidden" name="hiddensupplier" id="hiddensupplier">
					<input type="text" name="suppliername" id="suppliername" size="30" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>
					<input type="hidden" value="C" checked name="sumber" id="sumberC" onclick="ubahpayment();"/>
					<input type="hidden" value="K" name="sumber" id="sumberK" onclick="ubahpayment();"/>
				    <input type="hidden" name="top" id="top" size="3" value='0' disabled="disabled" onkeydown="keyShortcut(event,'top',this);">
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Tgl Jatuh Tempo","tgljto","","10","10","readonly='readonly'","hidden","","1");
			echo $mylib->write_textbox("Keterangan","ket","","35","30","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Invoice","jumlah","0","25","20","readonly='readonly'","hidden","","1","");
			echo $mylib->write_number("PPN Beli","ppn","0","25","20","","hidden",$gantikursor,"1","onKeyUp=\"hitungPPN()\"");
			echo $mylib->write_number("Nilai PPN Beli","nilaippn","0","25","20","readonly='readonly'","hidden","","1","");
			echo $mylib->write_number("Total Invoice","total","0","25","20","readonly='readonly'","hidden","","1","");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
			  <p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
		    </div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" id="newrow" onClick="AddNew()"></td>
					<td>KdBarang</td>
					<td>NamaBarang</td>
					<td>Ext</td>
					<td>Satuan</td>
					<td>Qty</td>
					<!--
					<td>Harga</td>
					<td>Disc1</td>
					<td>Disc2</td>
					<td>Total</td>-->
				</tr>
				<?=$mylib->write_detailFB(1,"","","",0,0,0,0,0,0.00,0.00,0,"","",0,"","",0,0,0,0,0,0,0,0,0,"","","","",0,"")?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="limitkredit" name="limitkredit">
				<input type='hidden' id="limitfaktur" name="limitfaktur">
				<input type='hidden' id="hiddengudang" name="hiddengudang">
				<input type='hidden' id="hiddensumberMO" name="hiddensumberMO" value='M'>
				<input type='hidden' id="hiddensumber" name="hiddensumber" value='C'>
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="kdgroupext" name="kdgroupext" value="">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/terima_barang/" />
			</td>
		</tr>
	</table>
</form>
</div>

<?php
$this->load->view('footer'); ?>