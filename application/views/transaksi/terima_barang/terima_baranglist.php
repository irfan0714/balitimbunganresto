<?php
$this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
	function gantiSearch()
	{
		if($("#searchby").val()=="NoDokumen"||$("#searchby").val()=="NoOrder"||$("#searchby").val()=="KdCustomer"||$("#searchby").val()=="KdGudang")
		{
			$("#normaltext").css("display","");
			$("#datetext").css("display","none");
			$("#date1").datepicker("destroy");
			$("#date1").val("");
		}
		else
		{
			$("#datetext").css("display","");
			$("#normaltext").css("display","none");
			$("#stSearchingKey").val("");
			$('#date1').Zebra_DatePicker({ format: 'd-m-Y' });
			//$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url();?>/public/images/calendar.png' });
		}
	}
	function deleteTrans(nodok,url)
	{
		var r=confirm("Apakah Anda Ingin Menghapus Transaksi "+nodok+" ?");
		if(r==true){
			$.post(url+"index.php/transaksi/terima_barang/delete_terima_barang",{ 
				kode:nodok},
			function(data){
				window.location = url+"index.php/transaksi/terima_barang";
			});
		}
	}
	function PopUpPrint(kode,baseurl)
	{
		url="index.php/transaksi/terima_barang/cetak/"+escape(kode);
		window.open(baseurl+url,'popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
	}
</script>
<form method="POST"  name="search" action="">
<table align='center'>
	<tr>
		<td id="normaltext" style=""><input type='text' size='20' name='stSearchingKey' id='stSearchingKey'></td>
		<td id="datetext" style="display:none"><input type='text' size='10' readonly='readonly' name='date1' id='date1'></td>
		<td>
			<select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()">
				<option value="NoDokumen">No Dokumen</option>
				<option value="NoOrder">No Order</option>
				<option value="KdSupplier">Kode Supplier</option>
				<option value="KdGudang">Kode Lokasi</option>
				<option value="TglDokumen">Tgl Invoice</option>
			</select>
		</td>
		<td><input type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<table align = 'center' border='1' class='table_class_list'>
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php }
		$mylib = new globallib();
		echo $mylib->write_header($header);
		?>
	</tr>
<?php
	if(count($data)==0)
	{ 
?>
	<td nowrap colspan="<?php echo count($header)+1;?>" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($data);$a++)
{
?>
	<tr>
<?php
	if($link->edit=="Y"||$link->delete=="Y"||$link->view=="Y")
	{
?>
			<td nowrap>
		<?php
			if($link->view=="Y")
			{
		?>
			<img src='<?=base_url();?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?=$data[$a]['NoDokumen'];?>','<?=base_url();?>');"/></a>
		<?php
			}
			if($link->edit=="Y"&&$data[$a]['FlagKonfirmasi']=="T")
			{
		?>
	        <a 	href="<?=base_url();?>index.php/transaksi/terima_barang/edit_terima_barang/<?=$data[$a]['NoDokumen'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php
			}
			if($link->delete=="Y"&&$data[$a]['FlagKonfirmasi']=="T")
			{
		?>
			<img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?=$data[$a]['NoDokumen'];?>','<?=base_url();?>');"/>
		<?php
			}
		?>
		</td>
<?php } ?>
		<td nowrap><?=$data[$a]['NoDokumen'];?></td>
		<td nowrap><?=$data[$a]['Tanggal'];?></td>
		<td nowrap><?=stripslashes($data[$a]['KdSupplier']);?></td>
		<td nowrap><?=stripslashes($data[$a]['Nama']);?></td>
		<td nowrap><?=stripslashes($data[$a]['NamaGudang']);?></td>
		<!--<td nowrap><?=stripslashes($data[$a]['TglJto']);?></td>-->
		<td nowrap><?=stripslashes($data[$a]['NoOrder']);?></td>
		<td nowrap><?=stripslashes($data[$a]['Keterangan']);?></td>
		<td nowrap><?=stripslashes($data[$a]['FlagKonfirmasi']);?></td>
		<!--<td nowrap class="InputAlignRight"><?=number_format($data[$a]['Jumlah'], 0, ',', '.');?></td>-->
		<!--<td nowrap class="InputAlignRight"><?=number_format($data[$a]['PPn'], 0, ',', '.');?></td>-->
		<!--<td nowrap class="InputAlignRight"><?=number_format($data[$a]['Total'], 0, ',', '.');?></td>-->
	<tr>
<?php
}
?>
</table>
<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/transaksi/terima_barang/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>