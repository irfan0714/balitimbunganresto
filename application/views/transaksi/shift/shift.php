<?php $this->load->view('header_part1')?>
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	<script>
	$(document).ready(function(){
		$('#tanggalShift').Zebra_DatePicker({format: 'Y-m-d'});
	});
	</script>
	
	<script type="text/javascript">
		document.getElementById("submit").disabled = true;

	</script>
	
</head>
<?php $this->load->view('header_part3')?>

<form method="POST" action="<?=base_url();?>/index.php/hr/shift/submitShift/">
    <div class="row">
        <table>
			<tbody>
				<tr>
					&nbsp;<td class="panel-title"> &nbsp;Tanggal&nbsp;<font color='red' title='Harus diisi'><b>(*)</b></font></td>
					<td id="normaltext" style="display">
						&nbsp;
							<input type='text' name='tanggalShift'  size='12' id="tanggalShift" value="<?php echo $ShiftDate; ?>"  class="form-control-new" readonly />
					</td>&nbsp;&nbsp;
				</tr>
				&nbsp;
				
				<tr>
					<td class="panel-title"> &nbsp;Nama Departemen&nbsp;<font color='red' title='Harus diisi'><b>(*)</b></font></td><br>
					<td style="display">
						&nbsp;<select name="kd" id="kddp" class="form-control-new">
							<option value=0 width="120">-Pilih-</option>
							
								<?php foreach ($combo->result() as $as): ?> 
								
							<option value="<?=$as->KdDepartemen?>"><?=$as->NamaDepartemen?></option>
								<?php endforeach;?>
						</select>
					</td><br>
				</tr>
				<tr>
					<td style="display" >
						&nbsp;<input type="submit" name="submit" id="submit" value="submit" class='btn btn-default'><br>
					</td>
					<td style="display" >
						&nbsp;<input type="submit" name="copy" id="copy" value="Copy" class='btn btn-default'><br>
					</td>
				</tr>
			
			</tbody>
		</table>
    </div>
</form>
&nbsp;
		
<form action="<?=base_url();?>/index.php/hr/shift/saveshift" method="post">
	<input type='hidden' name='tanggalShift'  size='12' id="tanggalShift" value="<?php echo $ShiftDate; ?>" class="form-control-new" readonly />

		<table class="table table-bordered table-hover" >
			<thead class="title_table">
			<tr>
				<th width="5"><center>No</center></th>
				<th width="200"><center>Nama</center></th>
				<th width="50"><center>Shift</center></th>
			</tr>
			</thead>
										
			<tbody>
			
				<?php
						
				$no=1;
				if($status=='yes')
				{
						for($i=0;$i<count($datacomplete);$i++)
						{
							?>
						<tr>
							<td>
								<center><?php echo $no ?></center>
							</td>
							<td align="left">
								<?php echo $datacomplete[$i]['employee_name']; ?>
								<input type="hidden" name="kodeem[]" value="<?php echo $datacomplete[$i]['KdEmployee'];?>">
								<input type="hidden" name="kddep[]" value="<?php echo $datacomplete[$i]['KdDepartemen'];?>">
							
							</td>
							
							<td>
								<center>
							
								
									<select  name="shift[]" id="kddp">
										<option value="">-Pilih Shift-</option>								
											<?php for($value=0; $value<count($list_shift); $value++) 
													{ 
														$selected = "";
														if($list_shift[$value]['shiftId']==$datacomplete[$i]['Shift'])
														{
															$selected = " selected";
														}
											?> 
											
										<option value="<?php echo $list_shift[$value]['shiftId'];?>"<?php echo $selected?>><?php echo $list_shift[$value]['shiftName'];?></option>
											
													<?php  
													}
													?>
									</select>
									<?php
							$no++;
							}
							?>
								</center>
							</td>

						</tr>
				
						<tr>
							<td colspan="3" align="center">
									<input type="submit" value="Save" name="save" class="btn btn-info btn-icon btn-sm icon-center">
								
							</td>
						</tr>
						<?php
					}
				?>
				</form>
	
			<tbody>
		</table>
	
	<hr/>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
  
</div>
	<?php 
	$this->load->view('footer'); 
//	$this->load->view('footer_part1');
	?>
<!--dua div dibawah, untuk nutup div yg ada di header-->
	</div>	
</div>
<!--eo dua div diatas-->
</body>
</html>