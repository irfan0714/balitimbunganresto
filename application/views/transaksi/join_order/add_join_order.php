<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Join Order";

$counter=1;

?>
<link rel="stylesheet" href="<?= base_url(); ?>public/jquery_confirm/dist/jquery-confirm.min.css">
<script src="<?= base_url(); ?>public/jquery_confirm/dist/jquery-confirm.min.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Creat <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/join_order/go_join_order/'>
	    <input type="hidden" name="v_trans" id="v_trans" value="<?php echo $header->NoTrans; ?>">
	    <input type="hidden" name="v_meja" id="v_meja" value="<?php echo $header->KdMeja; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <!--<tr>
	            <td class="title_table" width="150">No Transaksi</td>
	            <td><b><?php echo $header->NoTrans; ?></b></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table" width="150">No Meja</td>
	            <td><b><?php echo $header->KdMeja; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->dodate!="" && $header->dodate!="00-00-0000") { echo $header->dodate; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
				
	            </td>
	        </tr>
	        <tr>
	            <td colspan="100%">
				<span id='cek_stiker'></span>
				<table class="table table-bordered responsive" id="TabelDetail">
        				<thead>
							<tr>
							    <td width="100" class="title_table"><center>Kode Meja</center></td>
							    <td width="100" class="title_table"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </td>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<select class="form-control-new" name="nomeja[]" id="nomeja<?php echo $no;?>" style="width: 50%;">
						                <option value=""> -- Pilih Meja -- </option>
						                <?php
						                foreach ($allmeja as $vals) {
						                    ?>
						                    <option value="<?php echo $vals["NoTrans"]; ?>"><?php echo $vals["meja"]; ?></option>
						                    <?php
						                }
						                ?>
						            </select>
				                	</nobr>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
				</td>
	        </tr>
	        <tr>
	            <td class="title_table">&nbsp;</td>
	            <td><button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onClick="join_order()" value="Simpan">Join Order<i class="entypo-search"></i></button>
	            </td>
	        </tr>
	        
	    </table>
	    </form>  
	</div>
	
	<!-- Detail Split -->
	<div id="getList"></div>
	
	
</div>

          <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3><div id="pesan_loading">Proses...</div></h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>

    function join_order(){
		document.getElementById("theform").submit();		
	}
	
    function detailNew()
	{
		var clonedRow = $("#TabelDetail tr:last").clone(true);
		var intCurrentRowId = parseFloat($('#TabelDetail tr').length )-2;
		nama = document.getElementsByName("nomeja[]");
		temp = nama[intCurrentRowId].id;
		intCurrentRowId = temp.substr(6,temp.length-6);
		var intNewRowId = parseFloat(intCurrentRowId) + 1;
		$("#nomeja" + intCurrentRowId , clonedRow ).attr( { "id" : "nomeja" + intNewRowId,"value" : ""} );
		$("#btn_del_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_" + intNewRowId} );
		$("#TabelDetail").append(clonedRow);
		$("#TabelDetail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
		$("#nostiker" + intNewRowId).focus();
		ClearBaris(intNewRowId);
	}
	
	function ClearBaris(id)
	{
		$("#nomeja"+id).val("");
	}
	
	function deleteRow(obj)
{
	objek = obj.id;
	id = objek.substr(15,objek.length-3);
	
	var lastRow = document.getElementsByName("nomeja[]").length;
	
	if( lastRow > 1)
	{
		$('#baris'+id).remove();
	}else{
			alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
	}
}
</script>
