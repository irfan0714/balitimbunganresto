<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'order',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/order_barang.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('order');loading()">
<form method='post' name="order" id="order" action='<?=base_url();?>index.php/transaksi/order_barang/save_new_order' onsubmit="return false">
<div class="box-body sidebar"  style="overflow-x: auto">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Order Barang</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="11" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"20","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_combo("Gudang","gudang",$gudang,"","KdGudang","NamaGudang",$gantikursor,"","ya");
			?>
			<tr>
				<td nowrap>Supplier</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" name="kdsupplier" id="kdsupplier" onkeydown="keyShortcut(event,'supplier',this);" <?=$gantikursor;?> />
				<input type="button" value="..." onclick="pickSupplier();" id="btnsupplier">
				<input type="hidden" name="hiddensupplier" id="hiddensupplier">
				<input type="text" name="suppliername" id="suppliername" size="30" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>Payment</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="C" checked name="sumber" id="sumberC" onclick="ubahpayment();"/>Cash
					<input type="radio" value="K" name="sumber" id="sumberK" onclick="ubahpayment();"/>Credit
				    <input type="text" name="top" id="top" size="3" value='0' disabled="disabled">Hari
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Tgl Kirim","tglkirim",$tanggal,"20","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Keterangan","ket","","35","30","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Order","jumlah","0","25","20","readonly='readonly'","text","","1","");
			echo $mylib->write_number("PPN Beli","nilaippn","0","25","20","readonly='readonly'","text","","1","");
			echo $mylib->write_number("Total Order","total","0","25","20","readonly='readonly'","text","","1","");
            echo $mylib->write_textbox("Note ","note","","80","80","","text",$gantikursor,"");
			?>
			</table>
			</fieldset>
			</td>
		</tr>

		<tr>
			<td>
                <br />
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<div class="box-body sidebar"  style="overflow-x: auto">
			<table class="table_class_list" id="detail" style="width: auto">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="detailNew()"></td>
					<td>Kode</td>
					<td>Deskripsi</td>
					<td>Ext</td>
					<td>Satuan</td>
					<td>Qty</td>
					<td>Harga</td>
					<td>Disc1</td>
					<td>Disc2</td>
                    <td>Potongan</td>
					<td>Jumlah</td>
					<td>PPn</td>
					<td>Total</td>
				</tr>
				<?=$mylib->write_detailPO(1,"","","",0,0,0,0,0,0,0,0,0,"","",0,"","",0,0,0,0,0,0,0,0,0,"","","","","")?>
			</table>
			</div>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="hidesupplier" name="hidesupplier">
				<input type='hidden' id="limitkredit" name="limitkredit">
				<input type='hidden' id="limitfaktur" name="limitfaktur">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="ppn" name="ppn" value=0>
				<input type='hidden' id="kdgroupext" name="kdgroupext" value="">
				<input type='hidden' id="hiddensumber" name="hiddensumber" value='C'>
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/order_barang/" />
			</td>
		</tr>
	</table>
    <br />
</div>

</form>

<?php
$this->load->view('footer'); ?>