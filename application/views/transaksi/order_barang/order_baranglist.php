<?php
$this->load->view('header'); ?><!--
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/default.css" />
<script language="javascript" src="<?=base_url();?>assets/js/zebra_datepicker.js"></script>
<script language="javascript">
	function gantiSearch()
	{
		if($("#searchby").val()=="NoDokumen")
		{
			$("#normaltext").css("display","");
			$("#datetext").css("display","none");
			$("#date1").datepicker("destroy");
			$("#date1").val("");
		}
		else
		{
			$("#datetext").css("display","");
			$("#normaltext").css("display","none");
			$("#stSearchingKey").val("");
			$('#date1').Zebra_DatePicker({ format: 'd-m-Y' });
			//$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url();?>/public/images/calendar.png' });
		}
	}
	function deleteTrans(nodok,url)
	{
		var r=confirm("Apakah Anda Ingin Menghapus Transaksi "+nodok+" ?");
		if(r==true){
			$.post(url+"index.php/transaksi/order_barang/delete_order",{ 
				kode:nodok},
			function(data){
				window.location = url+"index.php/transaksi/order_barang";
			});
		}
	}
	function tutupTrans(nodok,url)
	{
		var r=confirm("Apakah Anda Ingin Menutup Transaksi "+nodok+" ?");
		if(r==true){
			$.post(url+"index.php/transaksi/order_barang/tutup_order",{ 
				kode:nodok},
			function(data){
				window.location = url+"index.php/transaksi/order_barang";
			});
		}
	}
	function bukaTrans(nodok,url)
    {
        var r=confirm("Apakah Anda Ingin Membuka Transaksi "+nodok+" ?");
        if(r==true){
            $.post(url+"index.php/transaksi/order_barang/buka_order",{
                    kode:nodok},
                function(data){
                    alert(data);
                    if(data=="sudahtutup")
                        alert("Tidak bisa dibuka lagi, barang sudah diterima semua");
                    window.location = url+"index.php/transaksi/order_barang";
                });
        }
    }
    function PopUpKirim(nodok,url)
    {
        var r=confirm("Apakah Anda Ingin Mengirim Otorisasi PO No."+nodok+" ?");
        if(r==true) {
            $.post(url + "index.php/transaksi/order_barang/kirim_otr", {
                    kode: nodok
                },
                function (data) {
                    //alert(data);
                    if (data == "S") {
                        alert("Data Sudah Terkirim");
                        window.location.reload()
                    }
                    else {
                        alert("Gagal Kirim Data Otorisasi");
                    }
                });
//            window.location.href=window.location.href;
//            location.reload();
        }
    }
	function PopUpPrint(kode,baseurl)
	{
		url="transaksi/order_barang/cetak/"+escape(kode);
		window.open(baseurl+url,'popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
	}
</script>
<form method="POST"  name="search" action="">
<table align='center'>
	<tr>
		<td id="normaltext" style=""><input type='text' size='20' name='stSearchingKey' id='stSearchingKey'></td>
		<td id="datetext" style="display:none"><input type='text' size='10' readonly='readonly' name='date1' id='date1'></td>
		<td>
			<select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()">
				<option value="NoDokumen">No Dokumen</option>
				<option value="TglDokumen">Tanggal</option>
			</select>
		</td>
		<td><input type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<table align = 'center' border='1' class='table_class_list'>
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php }
		$mylib = new globallib();
		echo $mylib->write_header($header);
		?>
	</tr>
<?php
	if(count($data)==0)
	{ 
?>
	<td nowrap colspan="<?php echo count($header)+1;?>" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($data);$a++)
{
//print_r($data);die();
?>
	<tr>
<?php
	if($link->edit=="Y"||$link->delete=="Y")
	{
?>
			<td nowrap>
		<?php
			if($link->view=="Y") {
                if($data[$a]['FlagOtorisasi']!='Y') {
                    if ($data[$a]['FlagKirimOTR'] == 'S') {
                        echo "waiting";
                    } else {
                        ?>
                        <img src='<?= base_url(); ?>public/images/select.gif' border='0' title='Kirim Otorisasi'
                             onclick="PopUpKirim('<?= $data[$a]['NoDokumen']; ?>','<?= base_url(); ?>');"/></a>
                    <?php
                    }
                }else if($data[$a]['FlagOtorisasi']!='R') {
                    echo "revision";
            }else{

		?>
		<img src='<?=base_url();?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?=$data[$a]['NoDokumen'];?>','<?=base_url();?>');"/></a>
		<?php
			}
            }
			if($link->edit=="Y"&&$data[$a]['FlagPengiriman']=="T"&&$data[$a]['FlagTutup']=="T"&&$data[$a]['FlagKirimOTR']=='B')
			{
		?>
		<a 	href="<?=base_url();?>index.php/transaksi/order_barang/edit_order/<?=$data[$a]['NoDokumen'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php
			}
			if($link->delete=="Y"&&$data[$a]['FlagPengiriman']=="T"&&$data[$a]['FlagTutup']=="T"&&$data[$a]['FlagKirimOTR']=='B')
			{
		?>
			<img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?=$data[$a]['NoDokumen'];?>','<?=base_url();?>');"/>
		<?php
			}
			if($data[$a]['FlagPengiriman']=="Y")
			{
			?>
			<img src='<?=base_url();?>public/images/accept.png' border = '0' title="terkirim"/>
		<?php
			}
			if($data[$a]['FlagTutup']=="Y")
			{
			?>
			<img src='<?=base_url();?>public/images/lock.png' border = '0' title="sudah tutup" onclick="bukaTrans('<?=$data[$a]['NoDokumen'];?>','<?=base_url();?>');"/>
		<?php
			}else
			{
			?>
			<img src='<?=base_url();?>public/images/unlock.png' border = '0' title="belum tutup" onclick="tutupTrans('<?=$data[$a]['NoDokumen'];?>','<?=base_url();?>');"/>
		<?php
			}
		?>
		</td>
<?php } ?>
		<td nowrap><?=$data[$a]['NoDokumen'];?></td>
		<td nowrap><?=$data[$a]['Tanggal'];?></td>
		<td nowrap><?=stripslashes($data[$a]['NamaGudang']);?></td>
		<td nowrap><?=stripslashes($data[$a]['Nama']);?></td>
		<td nowrap><?=stripslashes($data[$a]['Keterangan']);?></td>
		<td nowrap class="InputAlignRight"><?=number_format($data[$a]['Jumlah'], 0, ',', '.');?></td>
		<td nowrap class="InputAlignRight"><?=number_format($data[$a]['PPn'], 0, ',', '.');?></td>
		<td nowrap class="InputAlignRight"><?=number_format($data[$a]['Total'], 0, ',', '.');?></td>
		<td nowrap><?=stripslashes($data[$a]['FlagPengiriman']);?></td>
		<td nowrap><?=stripslashes($data[$a]['FlagTutup']);?></td>
	<tr>
<?php
}
?>
</table>
<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/transaksi/order_barang/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>