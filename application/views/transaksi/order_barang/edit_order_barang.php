<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'order',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/order_barang.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/css/default.css" />
<script language="javascript" src="<?=base_url();?>asset/js/zebra_datepicker.js"></script>
<body onload="loading()">
<form method='post' name="order" id="order" action='<?=base_url();?>index.php/transaksi/order_barang/save_new_order' onsubmit="return false">
<div class="box-body sidebar"  style="overflow-x: auto">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Edit Order Barang</legend>
			<table class="table_class_list">
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("No","nodok",$header->NoDokumen,"11","11","readonly='readonly'","text","","1");
			echo $mylib->write_textbox("Tanggal","tgl",$header->Tanggal,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_combo("Gudang","gudang",$mgudang,$header->KdGudang,"KdGudang","NamaGudang",$gantikursor,"onchange='CatatGudang()'","ya");
		    ?>
			<tr>
				<td nowrap>Supplier</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" name="kdsupplier" id="kdsupplier" value=<?=$header->KdSupplier?> onkeydown="keyShortcut(event,'supplier',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickSupplier();" id="btnsupplier">
				<input type="text" name="suppliername" id="suppliername" value="<?=$header->Nama?>" size="30" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>Payment</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="C" <?php echo $checkedc; ?> name="sumber" id="sumber" onclick="ubahpayment();"/>Cash
					<input type="radio" value="K" <?php echo $checkedk; ?> name="sumber" id="sumber" onclick="ubahpayment();"/>Credit
				    <input type="text" name="top" id="top" size="3" value="<?=$header->TOP?>" <?php echo $topdisabled; ?>>Hari
				</td>
			</tr>
		    <?php
			echo $mylib->write_textbox("Tgl Kirim","tglkirim",$header->TglKirim,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Keterangan","ket",$header->Keterangan,"35","30","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Order","jumlah",$header->Jumlah,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_number("PPN Beli","nilaippn",$header->NilaiPPn,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_number("Total Order","total",$header->Total,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
			  <p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
		</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
				   <td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="detailNew()"></td>
					<td>Kode</td>
					<td>Deskripsi</td>
					<td>Ext</td>
					<td>Satuan</td>
					<td>Qty</td>
					<td>Harga</td>
					<td>Disc1</td>
					<td>Disc2</td>
                    <td>Potongan</td>
					<td>Jumlah</td>
					<td>PPn</td>
					<td>Total</td>
				</tr>
			<?php
			for($z=0;$z<count($detail);$z++){
			    $select0 = "";
				$selectB = "";
				$selectT = "";
				$selectK = "";
				//$satuan = $modeller->getSatuan($detail[$z]['PCodeBarang']);
				//echo $detail[$z]['NamaSatuan'];
				//echo $satuan->Nama1;
				//echo $satuan->Nama2;
				//echo $satuan->Nama3;
				if($detail[$z]['NamaSatuan']==$detail[$z]['Nama0'])
				{ $select0 = "selected='selected'"; $nilsatuan = "1|".$detail[$z]['Satuan0'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama1'])
				{ $selectB = "selected='selected'"; $nilsatuan = "1|".$detail[$z]['Satuan1'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama2'])
				{ $selectT = "selected='selected'"; $nilsatuan = "2|".$detail[$z]['Satuan2'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama3'])
				{ $selectK = "selected='selected'"; $nilsatuan = "3|".$detail[$z]['Satuan3'];}
				$msatuan = "<option ".$select0."value='0|".$detail[$z]['Satuan0']."'>".$detail[$z]['Nama0']."</option>";
				$msatuan .= "<option ".$selectB."value='1|".$detail[$z]['Satuan1']."'>".$detail[$z]['Nama1']."</option>";
				$msatuan .= "<option ".$selectT."value='2|".$detail[$z]['Satuan2']."'>".$detail[$z]['Nama2']."</option>";
				$msatuan .= "<option ".$selectK."value='3|".$detail[$z]['Satuan3']."'>".$detail[$z]['Nama3']."</option>";
				
				/*
				if($detail[$z]['NamaSatuan']==$satuan->Nama1)
				{ $selectB = "selected='selected'"; $nilsatuan = "1|".$satuan->Satuan1;}
				else if($detail[$z]['NamaSatuan']==$satuan->Nama2)
				{ $selectT = "selected='selected'"; $nilsatuan = "2|".$satuan->Satuan2;}
				else if($detail[$z]['NamaSatuan']==$satuan->Nama3)
				{ $selectK = "selected='selected'"; $nilsatuan = "3|".$satuan->Satuan3;}
				$msatuan = "<option ".$selectB."value='1|".$satuan->Satuan1."'>".$satuan->Nama1."</option>";
				$msatuan .= "<option ".$selectT."value='2|".$satuan->Satuan2."'>".$satuan->Nama2."</option>";
				$msatuan .= "<option ".$selectK."value='3|".$satuan->Satuan3."'>".$satuan->Nama3."</option>";
				*/
				//if($detail[$z]['jenis']=="pcode"){$disabl = "";}
				//else if($detail[$z]['jenis']=="bar"){$disabl = "disabled='disabled'";}
				$mylib->write_detailPO($z+1,$detail[$z]['PCode'],$detail[$z]['NamaLengkap'],$detail[$z]['PCodeExt'],$detail[$z]['QtyInput'],round($detail[$z]['JumlahB']),round($detail[$z]['PPnB']),round($detail[$z]['TotalB']),$detail[$z]['HargaB'],$detail[$z]['Disc1'],$detail[$z]['Disc2'],$detail[$z]['Potongan'],$detail[$z]['HargaB'],$detail[$z]['PCode'],$detail[$z]['Satuan'],round($detail[$z]['HargaB']),"",$msatuan,
				$detail[$z]['harga0b'],$detail[$z]['harga1b'],$detail[$z]['harga2b'],$detail[$z]['harga3b'],
				$detail[$z]['konv0st'],
				$detail[$z]['konv1st'],$detail[$z]['konv2st'],$detail[$z]['konv3st'],$detail[$z]['konversi'],
				$detail[$z]['satuanst'],$detail[$z]['nilsatuan'],
				$detail[$z]['nilsatuanst'],$detail[$z]['kdkategori'],$detail[$z]['kdbrand']);	
			}
			?>
			<script language="javascript">
				detailNew();
			</script>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="hidesupplier" name="hidesupplier" value="<?=$header->KdSupplier?>">
				<input type='hidden' id="limitkredit" name="limitkredit">
				<input type='hidden' id="limitfaktur" name="limitfaktur">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="ppn" name="ppn" value="<?=$header->PPn?>">
				<input type='hidden' id="kdgroupext" name="kdgroupext" value="<?=$header->KdGroupext?>">
				<input type='hidden' id="hiddensumber" name="hiddensumber" value="<?=$header->Payment?>">
				<input type='hidden' id="flag" name="flag" value="edit">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/order_barang/" />
			</td>
		</tr>
	</table>
</div>
</form>

<?php
$this->load->view('footer'); ?>