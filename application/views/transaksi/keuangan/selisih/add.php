<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js">
</script>
</script>
<script language="javascript">

	function submit_this()
	{
		if(!$('#kdagent').val())
		{
			alert('Masukan Kd Stiker');
			$('#kdagent').focus();
			return false;
		}
		if (document.getElementById('nostruk1').value=="")
		{
			return false;

		}else
		{
			document.getElementById('btn_submit').submit()
		}
	}

function generateList(id) {
    pil  = $("input:radio[name=v_pilihan]:checked").val()
    tgl1 = $("#v_start_date").val();
    tgl2 = $("#v_end_date").val();
    tgl2 = $("#v_end_date").val();
    base_url = $("#baseurl").val();
    kdagent = $("#kdagent").val();
    $.post(base_url + "index.php/keuangan/konf_selisih_bank/getlist", {tgl1: tgl1,tgl2:tgl2,pil:pil},
        function (data) {
            alert(data);
            if (kdagent != "") {
                if (data != "0##")
                {
                    // alert(data);
                    sp = data.split("##");
                    ar = sp[1].split("**");
                    var totaljual = 0;
                    var totalkomisi_temp = 0;
                    var totalkomisi =0;
                    var nilai =0;
                    // alert(ar[0]);
                    id = 1;
                    $('#baris' + id).remove();
                    for (by = 0; by < sp[0]; by++) {
                        det = ar[by].split("||");
                        detailNew();
                        document.getElementById('tanggal' + id).value = det[0];
                        document.getElementById('nostruk' + id).value = det[1];
                        document.getElementById('debit' + id).value = det[2];
                        document.getElementById('kredit' + id).value = det[3];

                        id++;
                    }


                } else {
                    alert("Data Tidak Ada");
                }
                document.getElementById('transaksi').value="save";
            } else {
                alert("Masukan Kode Stiker")
            }

        })
}
function detailNew()
{
    var clonedRow = $("#detail tr:last").clone(true);
    var intCurrentRowId = parseFloat($('#detail tr').length) - 2;
    nama = document.getElementsByName("tanggal[]");
    temp = nama[intCurrentRowId].id;
    intCurrentRowId = temp.substr(7, temp.length - 7);
    var intNewRowId = parseFloat(intCurrentRowId) + 1;
$("#v_pilihan" + intCurrentRowId, clonedRow).attr({
    "id": "v_pilihan" + intNewRowId,
    "value": ""
});$("#tanggal" + intCurrentRowId, clonedRow).attr({
    "id": "tanggal" + intNewRowId,
    "value": ""
});
$("#nostruk" + intCurrentRowId, clonedRow).attr({
"id": "nostruk" + intNewRowId,
"value": ""

});
    $("#debit" + intCurrentRowId, clonedRow).attr({
        "id": "debit" + intNewRowId,
        "value": ""
    });
    $("#kredit" + intCurrentRowId, clonedRow).attr({
        "id": "kredit" + intNewRowId,
        "value": ""
    });
    $("#persentase" + intCurrentRowId, clonedRow).attr({
        "id": "del" + intNewRowId
    });
    $("#nama" + intCurrentRowId, clonedRow).attr({
        "id": "nama" + intNewRowId,
        "value": ""
    });
    $("#qty" + intCurrentRowId, clonedRow).attr({
        "id": "qty" + intNewRowId,
        "value": ""
    });
    $("#persentase" + intCurrentRowId, clonedRow).attr({
        "id": "persentase" + intNewRowId,
        "value": ""
    });
    $("#selisih" + intCurrentRowId, clonedRow).attr({
        "id": "selisih" + intNewRowId,
        "value": ""
    });

    $("#detail").append(clonedRow);
//    $("#detail tr:last").attr("id", "baris" + intNewRowId); // change id of last row
//    $("#pcode" + intNewRowId).focus();

}
</script>


<body onload="firstLoad('komisi'); //loading()">
<form method='post' name="komisi" id="komisi" action='<?= base_url(); ?>index.php/<?=$tr1;?>/<?=$tr2;?>/save_new_komisi' onsubmit="return submit_this()" class="form-horizontal">
	<div class="col-md-12">
		<div class="panel panel-gradient">
			<div class="panel-heading">
				<div class="panel-title">
					<?=$judul?>
				</div>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2">
                        Rekap
					</label>
					<div class="col-sm-5">
                        <div class="radio" style="float:left; width:100px; margin-top:0px;" >
                            <label><input type="radio" name="v_pilihan" id="v_pilihan" value='all' checked /> Semua</label>
                        </div>
                        <div class="radio" style="float:left;  width:100px; margin-top:0px;">
                            <label><input type="radio" name="v_pilihan" id="v_pilihan" value='KDebit' /> Kartu Debit</label>
                        </div>
                        <div class="radio" style="float:left;  width:100px; margin-top:0px;">
                            <label><input type="radio" name="v_pilihan" id="v_pilihan" value='KKredit' /> Kartu Kredit</label>
                        </div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">
                        Periode Tanggal
					</label>
					<div class="col-sm-5">
                        <input type="text" class="form-control-new datepicker" value="<?php echo $v_start_date; ?>" name="v_start_date" id="v_start_date" size="10" maxlength="10">
                        &nbsp;
                        s/d
                        &nbsp;
                        <input type="text" class="form-control-new datepicker" value="<?php echo $v_end_date; ?>" name="v_end_date" id="v_end_date" size="10" maxlength="10">
					</div>
				</div>
				<div class="form-group">

					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Search
								</button>
							</span>
						</div>
					</div>
				</div>

				<br>

				<div id="Layer1" style="display:none">
					<p align="center">
						<img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
					</p>
				</div>

				<table id="detail" class="table table-bordered responsive">
					<thead>
						<tr id="baris0">
							<th>
								Check
							</th>
							<th>
								Tanggal Transaksi
							</th>
							<th>
								No Struk
							</th>
							<th>
								Debit
							</th>
							<th>
								Kredit
							</th>
							<th>
								Persentase
							</th>
							<th>
								Selisih
							</th>
						</tr>
					</thead>
					<tbody id="dataTable">
						<tr id="dataTable">
							<td nowrap>
                                <input type="checkbox" name="v_pilihan[]" id="v_pilihan1" value='all' />
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="tanggal1" name="tanggal[]" size="12" readonly="readonly" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="nostruk1" name="nostruk[]" size="12" maxlength="20" readonly="readonly">
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="debit1" name="debit[]" size="10" readonly="readonly"  style="text-align: right;" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="kredit1" name="kredit[]" size="10" readonly="readonly" style="text-align: right;">
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="persentase1" name="persentase[]" size="10" readonly="readonly" style="text-align: right;">
							</td>
							<td nowrap>
								<input type="hidden" class="form-control" id="selisih1" name="selisih[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;">
							</td>
						</tr>
					</tbody>
				</table>

				<input type="hidden" id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
				<div class="form-group">
					<label class="col-sm-2 control-label">
					</label>
					<div class="col-sm-4">
						<a class="btn btn-default" href="<?= base_url(); ?>index.php/keuangan/komisi/">
							<i class="entypo-back">
							</i>Back
						</a>&nbsp;
						<button class="btn btn-primary" type="submit" onclick="submit_this();" id="btn_submit">
							<i class="entypo-drive">
							</i>Save
						</button>
					</div>
				</div>

			</div>



		</div>
	</div>
	</div>
</form>

<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>