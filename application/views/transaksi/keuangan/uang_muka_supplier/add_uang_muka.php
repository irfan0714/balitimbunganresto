<?php 

$this->load->view('header'); 

$modul = "Uang Muka Supplier";

?>
<script language="javascript" src="<?=base_url();?>public/js/uang_muka_supplier_v2.js"></script>
<script>
	function cari_supplier(){
		keyword = $("#keyword_supplier").val();
		url = $("#base_url").val(); 
		$.ajax({
			url: url+"index.php/keuangan/uang_muka_supplier/cari_supplier/",
			data: {supplier:keyword},
			type: "POST",					
			success: function(res)
			{
				$('#v_supplier').html(res);
			},
			error: function(e) 
			{
				alert(e);
			} 
		});
	}
</script>
<?php
if($this->session->flashdata('msg'))
{
  $msg = $this->session->flashdata('msg');
  
  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
}
?>	
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/keuangan/uang_muka_supplier/save_data'>
		
	    <table class="table table-bordered responsive">                        

	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Kas Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_kas_bank" id="v_kas_bank" style="width: 200px;">
	            		<option value="">Pilih Kas Bank</option>
	            		<?php
	            		foreach($KasBank as $val)
	            		{
							?><option value="<?php echo $val["KdKasBank"]; ?>"><?php echo $val["NamaKasBank"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Referensi <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_no_ref" id="v_no_ref" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Nama Supplier<font color="red"><b>(*)</b></font></td>
	            <td> 
		            <input type="text" class="form-control-new" value="" placeholder="Cari Supplier" name="keyword_supplier" id="keyword_supplier" style="width: 150px;" onchange="cari_supplier()">
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
							?><option value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">DPP <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="0" name="v_dpp" id="v_dpp" onkeyup="hitungJumlah()" maxlength="255" style="text-align: right; width: 200px;"></td>
	        </tr>
	        <tr>
	            <td class="title_table">PPN <font color="red"><b>(*) </b></font>+</td>
	            <td>
	            	<input type="text" class="form-control-new" value="0" name="v_ppn" id="v_ppn" onkeyup="hitungJumlah()" maxlength="2" style="text-align: right; width: 30px;"> % 
	            	<input type="text" class="form-control-new" value="0" name="v_nilaippn" id="v_nilaippn" onkeyup="hitungJumlah()" maxlength="255" style="text-align: right; width: 153px;" readonly="">
	            </td>
	        </tr>

	        <tr>
	            <td class="title_table">PPh <font color="red"><b>(*) </b></font>-</td>
	            <td>
	            	<!-- <input type="text" class="form-control-new" value="0" name="v_pph" id="v_pph" onkeyup="hitungJumlah()" maxlength="2" style="text-align: right; width: 30px;"> %  -->
	            	<input type="text" class="form-control-new" value="0" name="v_nilaipph" id="v_nilaipph" onkeyup="hitungJumlah()" maxlength="255" style="text-align: right; width: 200px;" >
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table">Jumlah <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="0" name="v_jumlah" id="v_jumlah" maxlength="255" style="text-align: right; width: 200px;" readonly=""></td>
	        </tr>
	        <tr>
	            <td class="title_table">Rekening PPH <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<select class="form-control-new" name="v_rekeningpph" id="v_rekeningpph" style="width: 200px;">
	            		<option value="">Pilih Rekening PPH</option>
	            		<?php
	            		foreach($rekeningpph as $val)
	            		{
							?><option value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option><?php
						}
	            		?>
	            	</select> 
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table">No. Rekening <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_no_rek" id="v_no_rek" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Sub. Divisi<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_subdivisi" id="v_subdivisi" style="width: 200px;">
	            		<option value="">Pilih Sub Divisi</option>
	            		<?php
	            		foreach($subdivisi as $val)
	            		{
							?><option value="<?php echo $val["KdSubDivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
           
            <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/keuangan/uang_muka_supplier/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

	});

	function hitungJumlah(){
		DPP = $("#v_dpp").val() != '' ? $("#v_dpp").val() : 0 ;
		PPNpersen = $("#v_ppn").val() != '' ? $("#v_ppn").val() : 0;
		PPH = $("#v_nilaipph").val() != '' ? $("#v_nilaipph").val() : 0;

		PPN = parseInt(DPP) * (parseInt(PPNpersen) / 100);
		// PPH = parseInt(DPP) * (parseInt(PPHpersen) / 100);
		Jumlah = parseInt(DPP) + PPN - PPH;
		$("#v_nilaippn").val(PPN);
		// $("#v_nilaipph").val(PPH);
		$("#v_jumlah").val(Jumlah);
	}
</script>