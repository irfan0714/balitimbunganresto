<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'pencairan',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/pencairan.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('pencairan');loading()">
<?php
if($pesan<>"new")
{
    ?>
	<script language="javascript">
	alert('Nomor giro sudah ada : '+'<?=$pesan?>');
    </script>
	<?
}
?>
<form method='post' name="pencairan" id="pencairan" action='<?=base_url();?>index.php/transaksi/pencairan/save_new_pencairan' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Pencairan</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="10" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"10","10","readonly='readonly'","text",'',"1");
			?>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="jenistr" name="jenistr" <?=$gantikursor;?> onchange="changeJenis()">
					<option value="">--Please Select--</option>
					<?php
					$jenis = "";
					$nilaijenis = array_keys($mjenis);
					for($a = 0;$a<count($mjenis);$a++){
					 	$select = "";
					 	if($jenis==$nilaijenis[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenis[$nilaijenis[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<?
			echo $mylib->write_combo("Jenis Giro","kasbank",$mkasbank,"","KdKasBank","NamaKasBank",$gantikursor,"onchange=\"simpanKasBank();\"","ya");
			echo $mylib->write_combo("Bank Cair","bankcair",$mkasbank,"","KdKasBank","NamaKasBank",$gantikursor,"onchange=\"simpanBankCair();\"","ya");
			echo $mylib->write_textbox("Keterangan","ket","","75","70","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Pencairan","jumlahpencairan","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()"></td>
					<td>Nomor Giro</td>
					<td>Terbit</td>
					<td>Jatuh Tempo</td>
					<td>Jumlah</td>
				</tr>
				<?=$mylib->write_detail_giro(1,"","","",0,"",0)?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="hidekasbank" name="hidekasbank">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/pencairan/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>