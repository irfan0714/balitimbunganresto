<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'pelunasanhutang',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/pelunasanhutang_v2.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('pelunasanhutang');loading()">
<form method='post' name="pelunasanhutang" id="pelunasanhutang" action='<?=base_url();?>index.php/transaksi/pelunasanhutang/save_new_pelunasan' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Pelunasan Hutang</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="11" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"10","10","readonly='readonly'","text",'',"1");
			?>
			<tr>
				<td nowrap>Supplier</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" name="kdsupplier" id="kdsupplier" onkeydown="keyShortcut(event,'supplier',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickSupplier();" id="btnsupplier">
				<input type="hidden" name="hiddensupplier" id="hiddensupplier">
				<input type="text" name="suppliername" id="suppliername" size="30" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>Nomor</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="11" size="11" name="noreturcndn" id="noreturcndn" onkeydown="keyShortcut(event,'order',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickNomor();" id="btnorder">
				<input type="hidden" name="hiddennoreturcndn" id="hiddennoreturcndn">
				<input type="hidden" name="jenis" id="jenis">
				</td>
			</tr>	
			<?
			echo $mylib->write_textbox("Tanggal Retur","tglretur","","25","25","","text",$gantikursor,"1");
			echo $mylib->write_number("Nilai Retur","jumlahretur","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_number("Sisa Retur","sisaretur","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_textbox("Keterangan","ket","","75","70","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Payment","jumlahpayment","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()"></td>
					<td>Jenis</td>
					<td>Nomor Bukti</td>
					<td>Nama</td>
					<td>Rekening</td>
					<td>Nama Rekening</td>
					<td>Hutang</td>
					<td>Jumlah</td>
					<td>Keterangan</td>
				</tr>
				<?=$mylib->write_detail_pelunasan(1,"","",0,"",$listjenis,1,"","",0)?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="hidekasbank" name="hidekasbank">
				<input type='hidden' id="hidecostcenter" name="hidecostcenter">
				<input type='hidden' id="hidepersonal" name="hidepersonal">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/pelunasanhutang/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>