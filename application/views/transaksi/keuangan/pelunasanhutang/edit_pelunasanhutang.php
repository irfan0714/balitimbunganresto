<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'pelunasanhutang',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/pelunasanhutang_v2.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('pelunasanhutang');loading()">
<form method='post' name="pelunasanhutang" id="pelunasanhutang" action='<?=base_url();?>index.php/transaksi/pelunasanhutang/save_new_pelunasan' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Edit Pelunasan Hutang</legend>
			<table class="table_class_list">
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("No","nodok",$header->NoDokumen,"11","11","readonly='readonly'","text","","1");
			echo $mylib->write_textbox("Tanggal","tgl",$header->Tanggal,"10","10","readonly='readonly'","text",'',"1");
			?>
			<tr>
				<td nowrap>Supplier</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" name="kdsupplier" id="kdsupplier" value="<?=$header->KdSupplier?>" readonly="readonly" <?=$gantikursor;?> /> 
				<input type="hidden" name="hiddensupplier" id="hiddensupplier" value="<?=$header->KdSupplier?>">
				<input type="text" name="suppliername" id="suppliername" size="30" readonly="readonly" value="<?=$header->Nama?>">
				</td>
			</tr>
			<tr>
				<td nowrap>Nomor</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="11" size="11" name="noreturcndn" id="noreturcndn" value="<?=$header->NoBukti?>" readonly="readonly" <?=$gantikursor;?> /> 
				<input type="hidden" name="hiddennoreturcndn" id="hiddennoreturcndn" value="<?=$header->NoBukti?>">
				<input type="hidden" name="jenis" id="jenis" value="<?=$header->Jenis?>">
				</td>
			</tr>	
			<?
			echo $mylib->write_textbox("Tanggal Retur","tglretur",$header->TglRetur,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_number("Nilai Retur","jumlahretur",$header->JumlahRetur,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_number("Sisa Retur","sisaretur",$header->SisaRetur,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_textbox("Keterangan","ket",$header->Keterangan,"75","70","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Payment","jumlahpayment",$header->JumlahPayment,"25","20","readonly='readonly'","text",$gantikursor,"1","");
		    ?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
			  <p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
		</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()"></td>
					<td>Jenis</td>
					<td>Nomor Bukti</td>
					<td>Nama</td>
					<td>Rekening</td>
					<td>Nama Rekening</td>
					<td>Hutang</td>
					<td>Jumlah</td>
					<td>Keterangan</td>
				</tr>
				<?php
				for($z=0;$z<count($detail);$z++){
					$mylib->write_detail_pelunasan($detail[$z]['Urutan'],$detail[$z]['KdRekening'],$detail[$z]['NamaRekening'],$detail[$z]['Jumlah'],$detail[$z]['Keterangan'],$listjenis,$detail[$z]['Jenis'],$detail[$z]['NoBukti'],$detail[$z]['Nama'],$detail[$z]['Hutang']);
				}
				?>
				<script language="javascript">
				   detailNew();
				   jenis=$("#jenistr").val();
				   if(jenis=='1')
				   {
					  $("#nogiro").attr("disabled",true);
					  $("#bankcair").attr("disabled",true);
					  $("#tglcair").attr("disabled",true);
				   }else
				   {
					  $("#nogiro").attr("disabled",false);
					  $("#bankcair").attr("disabled",false);
					  $("#tglcair").attr("disabled",false);
				   }
				</script>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="edit">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/pelunasanhutang/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>