<?php
$this->load->view('header_part2');
$gantikursor = "onkeydown=\"changeCursor(event,'payment',this)\"";?>
    <script language="javascript" src="<?= base_url(); ?>public/js/payment_v3.js"></script>
    <script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
    <script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.css"/>
    
    <script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
    <style type="text/css">
        <!--
        #Layer1 {
            position: absolute;
            left: 45%;
            top: 40%;
            width: 0px;
            height: 0px;
            z-index: 1;
            background-color: #FFFFFF;
        }

        -->
    </style>
<script>
	function ViewLampiran(input){
		
		window.open(input.src, "Lampiran","width=600,height=600");
	}
	
	function RefreshImage(input,lampiran){
		if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img'+lampiran)
                    .attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
	}
</script>
<script>
	var listrek = '';
	
	$(function() {
  		var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: ''}
	     }).done(function(reks){
	     	 listrek = reks.split('#');
	     });;
	});
	
	function PilihRekening(obj){
		id = obj.id;
		rek = $("#"+id).val();
		id_length = id.length;
		counter = id.substr(12,id_length-1)
				
		//var reklist = loadRekening(id);
		
		$("#"+id).autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
    			//alert(value.substr(0,7));
   				 //$("#"+id).val(value.substr(0,7)); 
   				 $("#kdrekening"+counter).val(value.substr(0,8)); 
   				 $("#tmpkdrekening"+counter).val(value.substr(0,8)); 
   				 $("#namarekening"+counter).autocomplete("destroy");
   				 $("#jumlah"+counter).focus();
			}
    	});		
		
	};
</script>

<body onload="firstLoad('payment');loading()">

        <?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		
<form method='post' name="payment" id="payment" action='<?=base_url();?>index.php/keuangan/payment/save_new_payment_jurnal' onsubmit="return false" enctype="multipart/form-data">
	<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Edit Payment ( Jurnal )</legend>
			<table class="table_class_list">
			<?php
			$mylib = new globallib();
//            print_r($header);die();
			echo $mylib->write_textbox("No","nodok",$header->NoDokumen,"11","11","readonly='readonly'","text","","1");
			echo $mylib->write_textbox_jurnal("Tanggal","tgl",$header->Tanggal,"10","10","readonly='readonly'","text",'',"1");
			/*
			?>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="jenistr" name="jenistr" <?=$gantikursor;?> onchange="changeJenis()">
					<option value="">--Please Select--</option>
					<?php
					$jenis = $header->Jenis;
					$nilaijenis = array_keys($mjenis);
					for($a = 0;$a<count($mjenis);$a++){
					 	$select = "";
					 	if($jenis==$nilaijenis[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenis[$nilaijenis[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<?
			*/
			echo $mylib->write_combo_jurnal("Kas Bank","kasbank",$mkasbank,stripslashes($header->KdKasBank),"KdKasBank","NamaKasBank",$gantikursor,"onchange=\"simpanKasBank();\"","ya");
            echo $mylib->write_textbox_jurnal("Dibayar Kepada","penerima",$header->Penerima,"25","25","","text",$gantikursor,"1");
			//echo $mylib->write_combo("Cost Center","costcenter",$mcostcenter,stripslashes($header->KdCostCenter),"KdCostCenter","NamaCostCenter",$gantikursor,"onchange=\"simpanCostCenter();\"","ya");
			//echo $mylib->write_combo("Personal","personal",$mpersonal,stripslashes($header->KdPersonal),"KdPersonal","NamaPersonal",$gantikursor,"onchange=\"simpanPersonal();\"","ya");
			//echo $mylib->write_textbox("Nomor Giro","nogiro",$header->NoGiro,"25","25","","text",$gantikursor,"1");
			//echo $mylib->write_combo("Bank Cair","bankcair",$mkasbank,stripslashes($header->KdBankCair),"KdKasBank","NamaKasBank",$gantikursor,"onchange=\"simpanBankCair();\"","ya");
			//echo $mylib->write_textbox("Tanggal Cair","tglcair",$header->TglCair,"10","10","readonly='readonly'","text",'',"1");
			echo $mylib->write_textbox_jurnal("Nomor Referensi","nobukti",$header->NoBukti,"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox_jurnal("Keterangan","ket",$header->Keterangan,"75","150","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Payment","jumlahpayment",$header->JumlahPayment,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_number("Kunci Jumlah Payment","kuncijumlahpayment",$header->JumlahPayment,"25","20","readonly='readonly'","text",$gantikursor,"1","");
			?>
			<tr style="display: none;">
				<td>Lampiran</td>
				<td>:</td>
				<td>
				<?php
					echo '<img id="img1" src="data:image/jpeg;base64,' . base64_encode($header->Lampiran1) . '" onclick="ViewLampiran(this)" style="width:100px;height:100px;"/>';
					echo '<input type="file" name="file[]" onchange="RefreshImage(this,1)" />';
				
				
					echo '<img id="img2" src="data:image/jpeg;base64,' . base64_encode($header->Lampiran2) . '" onclick="ViewLampiran(this)" style="width:100px;height:100px;"/>';
					echo '<input type="file" name="file[]" onchange="RefreshImage(this,2)" />';
				?>
				</td>
			</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<div id="Layer1" style="display:none">
			<p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()"></td>
					<td>Nama Rekening</td>
					<td>Jumlah</td>
                    <td>Sub Divisi</td>
                    <td>Departemen</td>
					<td>Keterangan</td>
				</tr>
				<?php
				for($z=0;$z<count($detail);$z++){
					$mylib->write_detail_payrec_jurnal($detail[$z]['Urutan'],$detail[$z]['KdRekening'],$detail[$z]['NamaRekening'],$detail[$z]['Jumlah'],$mSubdivisi,$detail[$z]['KdSubDivisi'],$mDept,$detail[$z]['KdDepartemen'],$detail[$z]['Keterangan'],$hasil_cek);
				}
				?>
				<script language="javascript">
				   //detailNew();
				   jenis=$("#jenistr").val();
				   if(jenis=='1')
				   {
					  $("#nogiro").attr("disabled",true);
					  $("#bankcair").attr("disabled",true);
					  $("#tglcair").attr("disabled",true);
				   }else
				   {
					  $("#nogiro").attr("disabled",false);
					  $("#bankcair").attr("disabled",false);
					  $("#tglcair").attr("disabled",false);
				   }
				</script>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap align="center">
			    <input type='hidden' id="jenistr" name="jenistr" value="<?=$header->Jenis?>">
				<input type='hidden' id="hidekasbank" name="hidekasbank" value="<?=$header->KdKasBank?>">
				<input type='hidden' id="hidecostcenter" name="hidecostcenter" value="<?=$header->KdCostCenter?>">
				<input type='hidden' id="hidepersonal" name="hidepersonal" value="<?=$header->KdPersonal?>">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' id="flag" name="flag" value="edit">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<br><br>
				<input class="form-control-new" type='button' value='Save' onclick="saveAllJurnal();"/>
				<input class="form-control-new" type="button" value="Tutup" onclick="tutup()" />
			</td>
		</tr>
	</table>
</form>

<?php
//$this->load->view('footer'); ?>

<script>
	function deleteRowJurnal(obj)
{
	var index = 0;
	var indexs = 0;
	objek = obj.id;
	id = objek.substr(3,objek.length-3);
	kdrekening = $("#kdrekening"+id).val();
	
	
	url = $("#base_url").val();
	tanggal = $("#tgl").val();
	
	   $.ajax({
			url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
			data: {tgl:tanggal,jenis:'GL'},
			type: "POST",
			dataType: 'json',					
			success: function(data)
			{
				if(data=='0'){
					alert("Gagal!!. Tanggal Dokumen sudah tutup bulan GL.");
					return false;
				}else{
								//aksi jika cocok
								
								//if($("#cek"+id).val()!="Y"){
				
									var banyakBaris = 1;
									var lastRow = document.getElementsByName("kdrekening[]").length;
									for(index=0;index<lastRow;index++){
										nama = document.getElementsByName("kdrekening[]");
										temp = nama[index].id;
										indexs = temp.substr(10,temp.length-10);
										if($("#savekdrekening"+indexs).val()!=""){
											banyakBaris++;
										}
									}
									if($("#savekdrekening"+id).val()==""&&banyakBaris>1){
										$('#baris'+id).remove();
									}
									else if($("#savekdrekening"+id).val()==""&&banyakBaris==1){
										alert("Baris ini tidak dapat dihapus\nMinimal harus ada 1 baris");
									}
									else{
										if(banyakBaris==2)
										{
											alert("Baris ini tidak dapat dihapus\nMinimal harus ada 1 baris tersimpan");
										}
										else
										{
											no = $("#nodok").val();
											if(kdrekening!=""){
												var r=confirm("Apakah Anda Ingin Menghapus Kode Rekening "+kdrekening+" ?");
												if(r==true){
													$('#baris'+id).remove();
													if(no!=""){
														deleteItemJurnal(kdrekening,id);
													}
												}
											}
										}
									}
							
							//}else{
							//	alert("Maaf tidak bisa di delete karena No. Vocher ini ada di pelunasan hutang.");
							//}
								
							}	
			},
			error: function(e) 
			{
				//alert error
			},
			async:false 
	 	});
	
}

    function tutup()
        {
        	window.close();           
        }
        
        function saveAllJurnal()
		{
			jmlpayment = parseInt($("#jumlahpayment").val());
			kuncijmlpayment = parseInt($("#kuncijumlahpayment").val());
			
			if(kuncijmlpayment==jmlpayment){
				if(cekheaderJurnal())
				if(cekDetailAllJurnal()){
			        document.getElementById("payment").submit();
					//$("#payment").submit();
				}
			}else{
				alert("Jumlah Payment Tidak Sama Dengan Jumlah Kuncian Payment");
				return false;
			}
			
		}
</script>