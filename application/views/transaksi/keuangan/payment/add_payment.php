<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'payment',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/payment_v3.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>

<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.css"/>
<script>
	var listrek = '';
	
	$(function() {
  		var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: ''}
	     }).done(function(reks){
	     	 listrek = reks.split('#');
	     });;
	});
	
	function loadRekening(id){
	    var rekSelected= $("#"+id).val();
	    var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    var rekList = "";
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: rekSelected}
	     }).done(function(reks){
	     	 rekList = reks.split(',');
	     });
	    //Returns the javascript array of sports teams for the selected sport.
	  return rekList;
	}
	function PilihRekening(obj){
		id = obj.id;
		rek = $("#"+id).val();
		id_length = id.length;
		counter = id.substr(12,id_length-1)
				
		//var reklist = loadRekening(id);
		
		$("#"+id).autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
    			//alert(value.substr(0,7));
   				 //$("#"+id).val(value.substr(0,7)); 
   				 $("#kdrekening"+counter).val(value.substr(0,8)); 
   				 $("#tmpkdrekening"+counter).val(value.substr(0,8)); 
   				 $("#namarekening"+counter).autocomplete("destroy");
   				 $("#jumlah"+counter).focus();
			}
    	});		
		
	};
	
</script>

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('payment');loading()">
<?php
if($pesan<>"new")
{
    ?>
	<script language="javascript">
	alert('Nomor giro sudah ada : '+'<?=$pesan?>');
    </script>
	<?
}
?>
<form method='post' name="payment" id="payment" action='<?=base_url();?>index.php/keuangan/payment/save_new_payment' onsubmit="return false" enctype="multipart/form-data">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Payment</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="11" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"10","10","readonly='readonly'","text",'',"1");
			/*
			?>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="jenistr" name="jenistr" <?=$gantikursor;?> onchange="changeJenis()">
					<option value="">--Please Select--</option>
					<?php
					$jenis = "";
					$nilaijenis = array_keys($mjenis);
					for($a = 0;$a<count($mjenis);$a++){
					 	$select = "";
					 	if($jenis==$nilaijenis[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenis[$nilaijenis[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<?
			*/
			echo $mylib->write_combo("Kas Bank","kasbank",$mkasbank,"","KdKasBank","NamaKasBank",$gantikursor,"onchange=\"simpanKasBank();\"","ya");
//			echo $mylib->write_combo("Departemen","dept",$mDept,"","KdDepartemen","NamaDepartemen",$gantikursor,"onchange=\"simpanCostCenter();\"","ya");
//			echo $mylib->write_combo("Sub Divisi","subdivisi",$mSubdivisi,"","kode","NamaSubDivisi",$gantikursor,"onchange=\"simpanPersonal();\"","ya");
			echo $mylib->write_textbox("Dibayar Kepada","penerima","","25","25","","text",$gantikursor,"1");
			//echo $mylib->write_combo("Bank Cair","bankcair",$mkasbank,"","KdKasBank","NamaKasBank",$gantikursor,"onchange=\"simpanBankCair();\"","ya");
			//echo $mylib->write_textbox("Tanggal Cair","tglcair",$tanggal,"10","10","readonly='readonly'","text",'',"1");
			echo $mylib->write_textbox("Nomor Referensi","nobukti","","35","35","","text",$gantikursor,"1");
			echo $mylib->write_textbox("Keterangan","ket","","75","150","","text",$gantikursor,"1");
			echo $mylib->write_number("Jumlah Payment","jumlahpayment","0","25","20","readonly='readonly'","text",$gantikursor,"1","");
			echo $mylib->write_textbox("Lampiran 1","file[]","","35","35","","file",$gantikursor,"1");
			echo $mylib->write_textbox("Lampiran 2","file[]","","35","35","","file",$gantikursor,"1");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()"></td>
					<td>Nama Rekening</td>
					<td>Jumlah</td>
                    <td>Sub Divisi</td>
                    <td>Departemen</td>
					<td>Keterangan</td>
				</tr>
				<?=$mylib->write_detail_payrec(1,"","","",$mSubdivisi,"",$mDept,"","")?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
			    <input type='hidden' id="jenistr" name="jenistr" value="1">
				<input type='hidden' id="hidekasbank" name="hidekasbank">
				<input type='hidden' id="hidecostcenter" name="hidecostcenter" value="<?=$aplikasi->DefaultKdCostCenter?>">
				<input type='hidden' id="hidepersonal" name="hidepersonal">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/keuangan/payment/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>