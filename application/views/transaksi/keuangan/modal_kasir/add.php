<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<!--<script language="javascript" src="--><?//= base_url(); ?><!--public/js/komisi.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/jquery.price_format.2.0.js"></script>

<script language="javascript">
    $('#jumlah').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    function submit_this() {
//        if(!$('#kdagent').val()){
//            alert('Masukan Kd Group');
//            $('#kdagent').focus();
//            return false;
//        }
    }
    function generateList(){
        base_url = $("#baseurl").val();
        url = base_url+"index.php/pop/user_list/index/";
        window.open(url,'popuppage','width=750,height=400,top=200,left=150');
    }

</script>

<body onload="firstLoad('komisi'); //loading()">
    <form method='post' name="komisi" id="komisi" action='<?= base_url(); ?>index.php/<?=$tr1;?>/<?=$tr2;?>/save_new' onsubmit="return submit_this();" class="form-horizontal">
        <div class="col-md-12">
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        <?=$judul;?>
                    </div>
                </div>
                <div class="panel-body">

                    <?php
                        $mylib = new globallib();
                        echo $mylib->neonwrite_textbox("Tanggal","tgl",$aplikasi->TglTrans,"15","10","readonly='readonly'","text",$gantikursor,"1");
                        echo $mylib->neonwrite_textbox("Jam","jam",date("H:i:s"),"15","10","readonly='readonly'","text",$gantikursor,"1");
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2"> NIK Kasir </label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input name="kode" type="text" class="form-control" id="kode" />
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">Go</button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <?php
                        echo $mylib->neonwrite_textbox("Nama Kasir","nama","","15","10","readonly='readonly'","text",$gantikursor,"1");
                        echo $mylib->Number_textbox("Jumlah ","jumlah","","15","10","","text",$gantikursor,"1");
                        echo $mylib->neonwrite_textbox("Keterangan","ket","","35","35","","text",$gantikursor,"1");
                    ?>


                    <div id="Layer1" style="display:none">
                        <p align="center">
                            <img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
                        </p>
                    </div> 


                    <input type='hidden' id="nodok" name="nodok" value="">
                    <input type='hidden' id="flag" name="flag" value="add">
                    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/keuangan/komisi/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit" onclick="return submit_this;"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                    
                </div>



            </div>
        </div>
    </div>
</form>

<?php $this->load->view('footer'); ?>
