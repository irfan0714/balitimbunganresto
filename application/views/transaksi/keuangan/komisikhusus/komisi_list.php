<?php
$this->load->view('header');
$searchby = $this->input->post('searchby');
$date1 = $this->input->post('date1');
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<!--<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>-->
<script language="javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script language="javascript">
    $(function () {
                $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy'
                });
            });
            
    
    function deleteTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menghapus Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/keuangan/paymentv/delete_paymentv/", {
                kode: nodok},
            function (data) {
                //                            alert("data");
                window.location = url + "index.php/keuangan/paymentv/";
            });
        }
    }
    function PopUpPrint(kode, baseurl)
    {
        //url = "index.php/keuangan/komisi/cetak/" + escape(kode);
        //window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
        
        url = "index.php/keuangan/komisikhusus/versistruk2/" + escape(kode);
        window.location = baseurl+ url;
    }
    
    function PopUpExcel(kode, baseurl)
    {
        url = "index.php/keuangan/komisikhusus/cetakexcel/" + escape(kode);
        window.location = baseurl+ url;
    }

    function PopUpVoid(kode, baseurl)
    {
        var confirmvoid = confirm('Yakin Ingin Void Pembayaran Komisi Khusus' + kode + ' ?');
        if(confirmvoid){
            url = '<?= base_url() ?>';
            alert(url);
            $.ajax({
				url: url+"index.php/keuangan/komisikhusus/voidkomisi",
				data: {tgl:tanggal,jenis:'Kas'},
				type: "POST",
				dataType: 'json',					
				success: function(data)
				{
					if(data=='0'){
						alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
						document.getElementById('tgl').focus();
					}else{
						document.getElementById('btn_submit').submit()
					}	
				},
				error: function(e) 
				{
					//alert(e);
				} 
		 	});
        url = "index.php/keuangan/komisikhusus/cetakexcel/" + escape(kode);
        window.location = baseurl+ url;
        }else{
            return false;
        }

       
    }
    function gantiSearch()
    {
        if ($("#searchby").val() == "NoTransaksi")
        {
            $("#normaltext").css("display", "");
            $("#datetext").css("display", "none");
            //$("#date1").datepicker("destroy");
            $("#date1").val("");
        }
        else
        {
            $("#datetext").css("display", "");
            $("#normaltext").css("display", "none");
            $("#stSearchingKey").val("");
            //$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>/public/images/calendar.png' });
            //$('#date1').Zebra_DatePicker({format: 'd-m-Y'});
        }
    }
	
		
		function save(url)
		{
			var komisi = $("#Total_komisi").val();
			var pemb_komisi = $("#Pembulatan_komisi").val();

			var selisih = parseInt(komisi) - parseInt(pemb_komisi);
			
			if(selisih>500){
				alert("Selisih Pembulatan Tidak Boleh Lebih Dari 500.");
				$("#Pembulatan_komisi").val("");
		        document.getElementById("Pembulatan_komisi").focus();
		        return false;
			}else if(selisih<-500){
				   	    alert("Selisih Pembulatan Tidak Boleh Lebih Dari 500.");
				   	    $("#Pembulatan_komisi").val("");
				        document.getElementById("Pembulatan_komisi").focus();
				        return false;
			}
		
			url_ = url+"index.php/keuangan/komisikhusus/bayar_payment/";
			
		    $('#btnSave').text('confirm...');
		    
			$.ajax({
					url: url_,
					data: $('#form').serialize(),
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
							$('#modalkasir').modal('hide');
							alert("Payment Berhasil Disimpan.");
							location.reload();
						
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
				
		}
   		
   		function kasir_payment(id,url){
   			
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
				
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/keuangan/komisikhusus/kasir_payment/"+id+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
			            $('[name="NoTransaksi"]').val(data.NoTransaksi);
						$('[name="Keterangan"]').val(data.Keterangan);
						$('[name="Total_komisi"]').val(data.Total);
						$('[name="Penerima"]').val("");
						$('[name="NoBukti"]').val("");
						$('[name="Pembulatan_komisi"]').val("");
			            $('#modalkasir').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
			
		}
		
</script>
<body onload="option();">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    List Pembayaran Komisi 10% Guide 
                </div>
            </div>
            <div class="panel-body">
				<?php if ($date1 != ""){
					$hide = "style='display:'";
					$hide2 = "style='display:none'";
					}else{
						$hide = "style='display:none'";					
						$hide2 = "style='display:'";
					}
				?>
               <form method="POST"  name="search" action="">
                    <div class="control-group">
                        <div class="controls" align="right">
                            <table border="0">
                                <tr>
                                    <td id="normaltext" <?=$hide2;?>><input type='text' size='20' name='stSearchingKey' id='stSearchingKey' class="form-control" value="<?php echo @$_POST['stSearchingKey']; ?>"></td>
                                    <td id="datetext" <?=$hide;?>><input type='text' size='12' name='date1' id='date1' class="form-control datepicker" data-date-format="dd-mm-yyyy" value="<?php echo @$_POST['date1']; ?>"></td>
                                    <td>
                                        <select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()" class="form-control">
                                            <option <?php if ($searchby == "NoTransaksi") echo "selected='selected'"; ?> value="NoTransaksi">No Transaksi</option>
                                            <option <?php if ($searchby == "TglTransaksi") echo "selected='selected'"; ?> value="TglTransaksi">Tanggal</option>
                                        </select>
                                    </td>
                                    <td><button type="submit" class="btn-primary">GO</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>

                <br>

                <table align = 'center' class='table table-bordered table-hover responsive'>
                    <thead>
                        <tr>
                            <th nowrap>NoTransaksi</th>    
                            <th nowrap>Tanggal</th>
                            <th nowrap>Kd Agent</th>
                            <th nowrap>No Sticker</th>
                            <th nowrap>No Payment</th>
                            <th nowrap> Kategori </th>
                            <th nowrap>Keterangan</th>
                            <th nowrap>Sales</th>
                            <th nowrap>Komisi</th>
                            <?php
                            if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <th nowrap>Action</th>
                            <?php } $mylib = new globallib(); ?>
                        </tr>
                    </thead>
                    <?php
                    if (count($data) == 0) {
                        ?>
                        <td nowrap colspan="<?php echo count($header) + 1; ?>" align="center">Tidak Ada Data</td>
                        <?php
                    }
                    $grdtotal=0;
                    for ($a = 0; $a < count($data); $a++) {
                    	$grdtotal += $data[$a]['Total'];
                        ?>
                        <tr>
                            <td nowrap><?= $data[$a]['NoTransaksi']; ?></td>
                            <td nowrap><?= $data[$a]['TglTransaksi']; ?></td>
                            <td nowrap><?= $data[$a]['KdAgent']; ?></td>
                            <td nowrap><?= $data[$a]['NoSticker']; ?></td>
                            <td nowrap><?= $data[$a]['NoDokumen']; ?></td>
                            <td nowrap>
                                <?php 
                                if($data[$a]["Kategori"]==0){
                                    echo "<span class='btn btn-warning btn-flat'>Komisi 25%</span>";
                                }else{
                                    echo "<span class='btn btn-success btn-flat'>Komisi 10%</span>";

                                }
                                ?>

                            </td>
                            <td nowrap><?= stripslashes($data[$a]['Keterangan']); ?></td>
                            <td nowrap align="right"><?= number_format($data[$a]['TotalSales'], 0, '', '.'); ?></td>
                            <td nowrap align="right"><?= number_format($data[$a]['Total'], 0, '', '.'); ?></td>
                            <?php
                            if ($link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <td nowrap>
                                    <?php
                                    if ($link->view == "Y") {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/></a>
                                        <img src='<?= base_url(); ?>public/images/ic_excel.gif' border = '0' title = 'Excel' onclick="PopUpExcel('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/></a>
                                        
                                        <?php if(empty($data[$a]['NoDokumen'])){?>
											<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="kasir_payment('<?= $data[$a]['NoTransaksi']; ?>','<?php echo base_url(); ?>')"><i class="entypo-pencil"></i></button>
                                            
                                            <?php 
                                                if($this->session->userdata('userlevel')=='-1'){?>
                                                <img src='<?= base_url(); ?>public/images/void.png' border = '0' title = 'Void' onclick="PopUpVoid('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/></a>
                                                <?
                                                }
                                            ?>
                                            
                                        <?}
                                        
                                        
                                        
                                    }
                                    if ($link->edit == "Y" && (substr($data[$a]['TglTransaksi'], 4, 2) == date('m'))) {
                                        ?>
                                        <a href="<?= base_url(); ?>index.php/keuangan/komisi/edit_komisi/<?= $data[$a]['NoTransaksi']; ?>"><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                                        <?php
                                    }
                                    if ($link->delete == "Y" && (substr($data[$a]['TglTransaksi'], 4, 2) == date('m'))) {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?= $data[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/>
                                        <?php
                                    }
                                    
                                    ?>
                                </td>
                            <?php } ?>
							</tr>
                            <?php
                        }
                        $tot = 0;
                        $tot +=$grdtotal;
                        ?>
                        <tfoot>
                        	<tr>
                        		<td colspan="8" width="77%" style="text-align: right;">
                        			<b>Total Komisi</b>
                        		</td>
                        		<td style="text-align: right;">
                        			<b><?php echo number_format($tot,0,'','.') ;?></b>
                        		</td>
                        	</tr>
                        </tfoot>
                </table>
                <table align = 'center'  >
                    <tr>
                        <td>
                            <?php echo $this->pagination->create_links(); ?>
                        </td>
                    </tr>
                    <?php
                    if ($link->add == "Y") {
                        ?>
                        <tr>
                            <td nowrap colspan="3">
                                <a href="<?= base_url(); ?>index.php/keuangan/komisikhusus/add_new/"><img src='<?= base_url(); ?>public/images/add.png' border = '0' title = 'Add'/></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php $this->load->view('footer'); ?>


<!-- Bootstrap modal -->
<div class="modal fade" id="modalkasir" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Kasir Payment</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="form-body">
					
					<div class="form-group">
					<label class="col-sm-4">
						No Transaksi
					</label>
					<div class="col-sm-6">
						<input readonly  name="NoTransaksi" type="text" class="form-control" id="NoTransaksi" size="10" maxlength="30"   />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-4">
						Keterangan
					</label>
					<div class="col-sm-6">
						<input readonly name="Keterangan" type="text" class="form-control" id="Keterangan" size="20" maxlength="30"   />
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="col-sm-4">
						Tanggal Payment
					</label>
					<div class="col-sm-6">
					<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_payment" id="v_tgl_payment" size="10" maxlength="10">
	                </div>
				</div>
				
                        <div class="form-group">
					<label class="col-sm-4">
					Kas Bank	
					</label>
					<div class="col-sm-6">
						<div class="input-group">
							<!--<input name="kdagent" type="text" class="form-control" id="kdagent" />
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Go
								</button>
							</span>-->
							<?=form_dropdownDB_initJS('KdKasBank', $kasbank, 'KdKasBank', 'NamaKasBank', '', '', '--------- Pilih ---------', "id='KdKasBank'");?>
						</div>
						</div>
						</div>
				
				<div class="form-group">
					<label class="col-sm-4">
						Komisi
					</label>
					<div class="col-sm-6">
						<input readonly="readonly" name="Total_komisi" type="text" class="form-control" id="Total_komisi" size="10" maxlength="30"   />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-4">
						Pembulatan Komisi
					</label>
					<div class="col-sm-6">
						<input name="Pembulatan_komisi" type="text" class="form-control" id="Pembulatan_komisi" size="10" maxlength="30"   />
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-4">
						Dibayar Kepada
					</label>
					<div class="col-sm-6">
						<input name="Penerima" type="text" class="form-control" id="Penerima" size="10" maxlength="30"   />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4">
						Nomor Bukti
					</label>
					<div class="col-sm-6">
						<input name="NoBukti" type="text" class="form-control" id="NoBukti"  size="10" maxlength="30"   />
					</div>
				</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save('<?php echo base_url(); ?>')" class="btn btn-info">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
