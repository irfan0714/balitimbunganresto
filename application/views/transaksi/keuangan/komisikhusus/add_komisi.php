<?php
$this->load->view('header');
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
//echo number_format("1000000",0,"","");
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js">
</script>
<script language="javascript" src="<?= base_url(); ?>public/js/komisi.js">
</script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js">
</script>

<script language="javascript">
		
	function submit_this()
	{
		if(!$('#kdagent').val())
		{
			alert('Masukan Kd Agen');
			$('#kdagent').focus();
			return false;
		}
		/*if(!$('#nostruk1').val()){
			
			alert('Silakan Tekan GO Terlebih Dahulu');

			return false;
		}*/
		else 
		{
			var confirmation = confirm("Sudah Yakin dengan Perhitungan Ini?");
			if(confirmation){

			url = $("#baseurl").val();
			tanggal = $("#tgl").val();        		
    		$.ajax({
				url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
				data: {tgl:tanggal,jenis:'Kas'},
				type: "POST",
				dataType: 'json',					
				success: function(data)
				{
					if(data=='0'){
						alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
						document.getElementById('tgl').focus();
					}else{
						document.getElementById('btn_submit').submit()
					}	
				},
				error: function(e) 
				{
					//alert(e);
				} 
		 	});

			}
			
		}
	}
	
	function generateList(id) {
    	base_url = $("#baseurl").val();
    	kdagent = $("#kdagent").val();
    	nosticker = $("#nosticker").val();
		
		$('#pleaseWaitDialog').modal('show');
				
    	$.ajax({
			type: "POST",
			url: base_url + "index.php/keuangan/komisikhusus/getlistBarang/",
			data : {kdagent: kdagent, nosticker: nosticker},
			success: function(data) {
				$('#UpdateDetail').html(data);
				
				
					$('#pleaseWaitDialog').modal('hide');
					// alert("tes");
					// alert(data[0].KdRegister);
					// if(data.KdRegister == NULL){
					// 	alert("KOSNG");
					// }
			
		
			}
		});
	}

</script>


<body onload="firstLoad('komisi'); //loading()">
<form method='post' name="komisi" id="komisi" action='<?= base_url(); ?>index.php/keuangan/komisikhusus/save_new_komisi' onsubmit="return submit_this()" class="form-horizontal">
	<div class="col-md-12">
		<div class="panel panel-gradient">
			<div class="panel-heading">
				<div class="panel-title">
					Form Pembayaran Komisi Khusus
				</div>
			</div>
			<div class="panel-body">
				<div class="form-group" style="display:none">
					<label class="col-sm-1">
						No
					</label>
					<div class="col-sm-2">
						<input name="nodok" type="text" class="form-control" id="nodok" value="" size="10" maxlength="10"   />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">
						Tanggal
					</label>
					<div class="col-sm-2">
						<input name="tgl" type="text" class="form-control" id="tgl" value="<?= $aplikasi->TglTrans ?>" size="10" maxlength="10"   />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">
						No. Stiker
					</label>
					<div class="col-sm-2">
						<input name="nosticker" type="text" class="form-control" id="nosticker" value="" size="10" maxlength="10"   />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">
						Kd Agen
					</label>
					<div class="col-sm-2">
						<div class="input-group">
							<input name="kdagent" type="text" class="form-control" id="kdagent" />
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Go
								</button>
							</span>
						</div>
					</div>
				</div>
				
				<!--<div class="form-group">
					<label class="col-sm-2">
					Kas Bank	
					</label>
					<div class="col-sm-2">
						<div class="input-group">
							<!--<input name="kdagent" type="text" class="form-control" id="kdagent" />
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Go
								</button>
							</span>-->
							<!--<?=form_dropdownDB_initJS('KdKasBank', $kasbank, 'KdKasBank', 'NamaKasBank', '', '', '--------- Pilih ---------', "id='KdKasBank'");?>-->
						<!--</div>
						</div>
						</div>
					
				<div class="form-group">
					<label class="col-sm-2">
						Dibayar Kepada
					</label>
					<div class="col-sm-2">
						<input name="Penerima" type="text" class="form-control" id="Penerima" size="10" maxlength="30"   />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">
						Nomor Bukti
					</label>
					<div class="col-sm-2">
						<input name="NoBukti" type="text" class="form-control" id="NoBukti"  size="10" maxlength="30"   />
					</div>
				</div>-->
				
				<div class="form-group">
					<label class="col-sm-2">
						Keterangan
					</label>
					<div class="col-sm-5">
						<input name="ket" type="text" class="form-control" id="ket" value="" size="35" maxlength="100"   />
					</div>
				</div>
				<br>

				<div id="Layer1" style="display:none">
					<p align="center">
						<img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
					</p>
				</div>
				<span id='UpdateDetail'></span>
				<input type="hidden" id="urutan" name="urutan" value="1">
				<input type="hidden" id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
				<div class="form-group">
					<label class="col-sm-2 control-label">
					</label>
					<div class="col-sm-4">
						<a class="btn btn-default" href="<?= base_url(); ?>index.php/keuangan/komisikhusus/">
							<i class="entypo-back">
							</i>Back
						</a>&nbsp;
						<button class="btn btn-primary" type="submit" onclick="submit_this();" id="btn_submit">
							<i class="entypo-drive">
							</i>Save
						</button>
					</div>
				</div>

			</div>



		</div>
	</div>
	</div>
	
	<div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1>Mohon Tunggu, Sedang Proses...</h1>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
</form>


<?php $this->load->view('footer'); ?>
<script type="text/javascript">
	new Spry.Widget.ValidationTextField("kdagent", "none");
	new Spry.Widget.ValidationTextField("Penerima", "none");
	new Spry.Widget.ValidationSelect("KdKasBank");
	new Spry.Widget.ValidationTextField("tgl", "date", {format:"dd-mm-yyyy", useCharacterMasking:true});
</script>
