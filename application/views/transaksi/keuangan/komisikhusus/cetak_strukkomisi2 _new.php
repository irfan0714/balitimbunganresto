<?php
$ftext = '';
$alamatPT=$store[0]['Alamat1PT'];
$tgl=$header->TglTransaksi;
$tgl_tampil=$header->TglTransaksi;
printer_dos($ftext, $reset.$elite);
printer_dos($ftext, "\r\n");
printer_dos($ftext, str_pad("PEMBAYARAN",33," ",STR_PAD_BOTH)."\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, str_pad("Member",11).": ".$header->KdAgent."\r\n");
printer_dos($ftext, str_pad("Tanggal",11).": ".date('d-m-Y', strtotime($header->TglTransaksi))."\r\n") ;
printer_dos($ftext, "\r\n");
printer_dos($ftext, str_pad("Kategori",12)."  ".str_pad("Sales",9)."  ".str_pad("Rp",9)."\r\n");
printer_dos($ftext, "=================================\r\n");
$ttlS = 0;
$ttlK = 0;
for($a=0;$a<count($detail);$a++){
	$namadivisi = $detail[$a]['NamaDivisi'] == 'Head Office' ? 'Ticket' : $detail[$a]['NamaDivisi'];
	printer_dos($ftext, str_pad(substr($namadivisi,0,10),10)."  ".str_pad(number_format($detail[$a]['total'], 0, ',', '.'),10," ",STR_PAD_LEFT)."  ".str_pad(number_format($detail[$a]['komisi'], 0, ',', '.'),10,"  ",STR_PAD_LEFT). "\r\n");
	$ttlS= $ttlS + $detail[$a]['total'];
	$ttlK= $ttlK + $detail[$a]['komisi'];
}	
$by = 0;
$pembulatan = $header->Payment - $ttlK;
printer_dos($ftext, "---------------------------------\r\n");
printer_dos($ftext, str_pad('Total',10)."  ".str_pad(number_format($ttlS, 0, ',', '.'),10,"  ",STR_PAD_LEFT)."  ".str_pad(number_format($ttlK, 0, ',', '.'),10," ",STR_PAD_LEFT). "\r\n");
printer_dos($ftext, str_pad('Pembulatan',10)."  ".str_pad(' ',10,"  ",STR_PAD_LEFT)."  ".str_pad(number_format($pembulatan, 0, ',', '.'),10," ",STR_PAD_LEFT). "\r\n");
printer_dos($ftext, str_pad('Pembayaran',10)."  ".str_pad(' ',10,"  ",STR_PAD_LEFT)."  ".str_pad(number_format($header->Payment, 0, ',', '.'),10," ",STR_PAD_LEFT). "\r\n".$ncond);
printer_dos($ftext, "=================================\r\n");
printer_dos($ftext, "        ===Terima kasih===       \r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "Penerima \r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, "\r\n");
printer_dos($ftext, $fcut);
printer_dos($ftext, $op_cash);
?>