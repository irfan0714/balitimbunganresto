<?php
$this->load->view('header');

$modul = "Revisi Pembayaran";

?>

<script>
    function submitThis()
    {
        document.getElementById("search").submit();
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb">
			<li><strong><i class="entypo-pencil"></i><?php echo $modul; ?></strong></li>
		</ol>
		
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method="POST" name="search" id="search" action="<?php echo base_url(); ?>index.php/keuangan/revisi_pembayaran/search_report/" onsubmit="return false">
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Struk</td>
	            <td>
	            	<input type="text" class="form-control-new" value="<?php if($v_no_struk){ echo $v_no_struk; } ?>" name="v_no_struk" id="v_no_struk" size="50" maxlength="30">
	            </td>
	        </tr>
	        
	        <tr>
	        	<td>&nbsp;</td>
	            <td>
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
					<input type='hidden' name="search_report" id="search_report" value="search_report">
	            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="submitThis()" name="btn_search" id="btn_search"  value="Search">Search<i class="entypo-search"></i></button>
		       	</td>
	        </tr>
	        
	    </table>
	    </form> 
	</div>
</div>

<?php

if ($tampilkanDT) 
{
    $this->load->view("transaksi/keuangan/revisi_pembayaran/tampil");
}

$this->load->view('footer');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>