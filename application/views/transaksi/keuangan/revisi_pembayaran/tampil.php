<?php
error_reporting(0);

unset($arr_data);

$mylib = new globallib();

foreach($hasil as $val)
{
	$arr_data["list_data"][$val["NoStruk"]] = $val["NoStruk"];
	$arr_data["tanggal"][$val["NoStruk"]] = $val["Tanggal"];
	$arr_data["jenis"][$val["NoStruk"]] = $val["Jenis"];
	$arr_data["nomorkkredit"][$val["NoStruk"]] = $val["NomorKKredit"];
	$arr_data["nomorkdebet"][$val["NoStruk"]] = $val["NomorKDebet"];
	$arr_data["nomorvoucher"][$val["NoStruk"]] = $val["NomorVoucher"];
	$arr_data["keterangan"][$val["NoStruk"]] = $val["Keterangan"];
	$arr_data["nilaitunai"][$val["NoStruk"]] = $val["NilaiTunai"];
	$arr_data["nilaikredit"][$val["NoStruk"]] = $val["NilaiKredit"];
	$arr_data["nilaidebet"][$val["NoStruk"]] = $val["NilaiDebet"];
	$arr_data["nilaivoucher"][$val["NoStruk"]] = $val["NilaiVoucher"];
	$arr_data["totalbayar"][$val["NoStruk"]] = $val["TotalBayar"];
	$arr_data["totalnilai"][$val["NoStruk"]] = $val["TotalNilai"];
	$arr_data["kembali"][$val["NoStruk"]] = $val["Kembali"];
	
	$tanggal = $arr_data["tanggal"][$nostruk];
	$jenis = $arr_data["jenis"][$nostruk];
	$nomorkkredit = $arr_data["nomorkkredit"][$nostruk];
	$nomorkdebet = $arr_data["nomorkdebet"][$nostruk];
	$nomorvoucher = $arr_data["nomorvoucher"][$nostruk];
	$keterangan = $arr_data["keterangan"][$nostruk];
	$nilaitunai = $arr_data["nilaitunai"][$nostruk];
	$nilaikredit = $arr_data["nilaikredit"][$nostruk];
	$nilaidebet = $arr_data["nilaidebet"][$nostruk];
	$nilaivoucher = $arr_data["nilaivoucher"][$nostruk];
	$totalbayar = $arr_data["totalbayar"][$nostruk];
	$totalnilai = $arr_data["totalnilai"][$nostruk];
	$kembali = $arr_data["kembali"][$nostruk];
}	
?>

<script>
    function submitDetailThis()
    {
        document.getElementById("searchDetail").submit();
    }

    function hitung()
    {
    	var v_nostruk = document.getElementById('v_nostruk').value;
    	var grand_total = document.getElementById('grand_total').value;
    	var net_tunai = document.getElementById('net_tunai').value;
    	var v_kembali = document.getElementById('v_kembali').value;
    	
    	var v_tunai = reform(document.getElementById('v_tunai').value);	
    	var v_kredit = reform(document.getElementById('v_kredit').value);		
    	var v_debit = reform(document.getElementById('v_debit').value);		
    	var v_voucher = reform(document.getElementById('v_voucher').value);	
    	
    	var total_pembayaran = ((v_tunai*1)+(v_kredit*1)+(v_debit*1)+(v_voucher*1))-v_kembali;
    	
    	if(total_pembayaran>grand_total)
    	{
			alert("Total Pembayaran lebih dari "+format(grand_total));
			document.getElementById('v_tunai').focus();
			return false 
		}
		else
		{
			submitDetailThis();	
		}
	}
    
</script>

<div class="row">
	<div class="col-md-12" align="left">
<form method="POST" name="searchDetail" id="searchDetail" action="<?= base_url() ?>index.php/keuangan/revisi_pembayaran/simpan_pembayaran/" onsubmit="return false">
    
    
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
		<table class="table table-bordered responsive">
	    	<thead>
                <tr>
                    <th rowspan="2" style="vertical-align: middle; text-align: center;">Divisi</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center;">Kasir</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center;">Tanggal</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center;">Waktu</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center;">No Struk</th>
                    
                    <th rowspan="2" style="text-align: center; background: #73e8e4;">Total</th>
                    <th rowspan="2" style="text-align: center; background: #73e8e4;">Disc</th>
					<th rowspan="2" style="text-align: center; background: #73e8e4;">Disc/Compliement</th>
                    <th rowspan="2" style="text-align: center; background: #73e8e4;">Netto</th>
                    <th rowspan="2" style="text-align: center; background: #73e8e4;">Charge</th>
					<th rowspan="2" style="text-align: center; background: #73e8e4;">Tax</th>
					<th rowspan="2" style="text-align: center; background: #73e8e4;">GrandTotal</th>
					
                    <th colspan="5" style="text-align: center; background: #ff7575;">Pembayaran</th>
                    
                    <th rowspan="2" style="text-align: center; background: #c1f966;">Total Bayar</th>
                    <th rowspan="2" style="text-align: center; background: #ec89fa;">Kembali</th>
                </tr>
                
                <tr>
                    <th style="text-align: center;">NetTunai</th>
					<th style="text-align: center;">Tunai</th>
                    <th style="text-align: center;">Kredit</th>
                    <th style="text-align: center;">Debit</th>
                    <th style="text-align: center;  border-right-color: #ff7575;">Voucher</th>
                </tr>
			</thead>
			<tbody>
			
			<?php
			if($hasil->NoStruk=="")
			{
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
			}
			else
			{
				$nettunai = $hasil->Tunai - $hasil->Kembali;
				
				if($hasil->KdDivisi!=3){
					$grandtotal = $hasil->Netto + $hasil->Total_Charge + $hasil->TAX;	
				}
				else{
						if($hasil->Total_Charge>0){
							$grandtotal = $hasil->Netto + $hasil->Total_Charge + $hasil->TAX;
						}
						else{
								$grandtotal = $hasil->Netto;
						}
				}
				
			?>
				<tr onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="mouseclick1(this)">
					<td><?php echo $hasil->NamaDivisi; ?></td>
					<td><?php echo $hasil->Kasir; ?></td>
					<td><?php echo $mylib->ubah_tanggal($hasil->Tanggal); ?></td>
					<td><?php echo $hasil->Waktu; ?></td>
					<td align="center"><?php echo $hasil->NoStruk; ?></td>
					<td align="right"><?php echo $mylib->format_number($hasil->Bruto); ?></td>
					<td align="right"><?php echo $mylib->format_number($hasil->DiscDetail); ?></td>
					<td align="right"><?php echo $mylib->format_number($hasil->Discount); ?></td>
					<td align="right"><?php echo $mylib->format_number($hasil->Netto); ?></td>
					<td align="right"><?php echo $mylib->format_number($hasil->Total_Charge); ?></td>
					<td align="right"><?php echo $mylib->format_number($hasil->TAX); ?></td>
					<td align="right"><?php echo $mylib->format_number($grandtotal); ?></td>
					
					<td align="right"><?php echo $mylib->format_number($nettunai); ?></td>
					<td align="right"><input type="text" class="form-control-new" style="text-align: right;" name="v_tunai" id="v_tunai" size="10" onblur="toFormat('v_tunai')" value="<?php echo $mylib->format_number($hasil->Tunai); ?>"/></td>
					<td align="right"><input type="text" class="form-control-new" style="text-align: right;" name="v_kredit" id="v_kredit" size="10" onblur="toFormat('v_kredit')" value="<?php echo $mylib->format_number($hasil->KDebit); ?>"/></td>
					<td align="right"><input type="text" class="form-control-new" style="text-align: right;" name="v_debit" id="v_debit" size="10" onblur="toFormat('v_debit')" value="<?php echo $mylib->format_number($hasil->KKredit); ?>"/></td>
					<td align="right"><input type="text" class="form-control-new" style="text-align: right;" name="v_voucher" id="v_voucher" size="10" onblur="toFormat('v_voucher')" value="<?php echo $mylib->format_number($hasil->Voucher); ?>"/></td>
				
					<td nowrap style="border-right-color: #c1f966;" title="Total Bayar = <?php echo $mylib->format_number($hasil->TotalBayar); ?>" align="right"><?php echo $mylib->format_number($hasil->TotalBayar); ?></td>
					<td nowrap title="Kembali = <?php echo $mylib->format_number($hasil->Kembali); ?>" align="right"><?php echo $mylib->format_number($hasil->Kembali); ?></td>
				</tr>
				
				 <tr>
		            <td colspan="100%" align="center">
						<input type='hidden' name="flag" id="flag" value="revisi">
						<input type='hidden' name="v_nostruk" id="v_nostruk" value="<?php echo $hasil->NoStruk; ?>">
						<input type='hidden' name="grand_total" id="grand_total" value="<?php echo $grandtotal; ?>">
						<input type='hidden' name="net_tunai" id="net_tunai" value="<?php echo $nettunai; ?>">
						<input type='hidden' name="v_kembali" id="v_kembali" value="<?php echo $hasil->Kembali; ?>">
		            	<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="hitung()" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
			        </td>
		        </tr>
			<?php
			}
			?>
			
			</tbody>
		</table>
	</div>
    
    
</form>
	</div>
</div>