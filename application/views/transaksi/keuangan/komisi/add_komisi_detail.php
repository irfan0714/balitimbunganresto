<table id="detail" class="table table-bordered responsive">
					<thead>
						<tr id="baris0">
							<th>
								No Struk
							</th>
							<th>
								Tgl Jual
							</th>
							<th>
								Kd Barang
							</th>
							<th>
								Nama Barang
							</th>
							<th>
								Jumlah
							</th>
							<th>
								Harga
							</th>
							<th>
								Potongan
							</th>
							<th>
								Netto
							</th>
							<th>
								 TTL Komisi
							</th>					
							<th>
								Komisi
							</th>
							<th>
								Office %
							</th>
							<th>
								Guide %
							</th><th>
								Tour Leader %
							</th><th>
								Driver %
							</th>
						</tr>
					</thead>
					
					<span id='UpdateDetail'></span>
					

<tbody id="dataTable">
	<?php
	$totjual=0;
	$totkomisi=0;
	for($i=0;$i<count($data);$i++){
		$totjual += $data[$i]['ttlnetto'];
		$totkomisi += $data[$i]['Nilai'];
		
	?>
		<tr id="dataTable">
			<td nowrap>
				<input type="text" class="form-control" id="nostruk1" name="nostruk[]" size="12" readonly="readonly"  value="<?=$data[$i]['NoStruk'];?>">
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="tgljual1" name="tgljual[]" size="12" readonly="readonly" value="<?=$data[$i]['TglJual'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="pcode1" name="pcode[]" size="12" maxlength="20" readonly="readonly" value="<?=$data[$i]['PCode'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="nama1" name="nama[]" size="40" readonly="readonly" value="<?=$data[$i]['NamaLengkap'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="qty1" name="qty[]" size="2" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['Qty'];?>" >
			</td>
			<td nowrap>
				<input type="hidden" class="form-control" id="harga1" name="harga[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['Harga'];?>" >
				<input type="text" class="form-control" id="harga_temp1" name="harga_temp[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=number_format($data[$i]['Harga'],0,'.',',');?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="potongan1" name="potongan[]" size="10" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['Disc'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="netto1" name="netto[]" size="10" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['ttlnetto'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="nilai1" name="nilai[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['Nilai'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="persentase1" name="persentase[]" size="10" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['Komisi'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="komisia1" name="komisia[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['komisi1'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="komisib1" name="komisib[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['komisi2'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="komisic1" name="komisic[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['komisi3'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="komisid1" name="komisid[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$data[$i]['komisi4'];?>" >
				<input type="hidden" class="form-control" id="hrgPPN1" name="hrgPPN[]" value="<?=$data[$i]['hrgPPN'];?>" >
			</td>
		</tr>
	<?php
	}
	?>
	
	<table id="detail_ticket" class="table table-bordered responsive">
					<thead>
						<tr id="baris0">
							<th>
								No Struk
							</th>
							<th>
								Tgl Jual
							</th>
							<th>
								Kd Barang
							</th>
							<th>
								Nama Barang
							</th>
							<th>
								Jumlah
							</th>
							<th>
								Harga
							</th>
							<th>
								Potongan
							</th>
							<th>
								Netto
							</th>
							<th>
								 TTL Komisi
							</th>					
							<th>
								Komisi
							</th>
							<th>
								Office %
							</th>
							<th>
								Guide %
							</th><th>
								Tour Leader %
							</th><th>
								Driver %
							</th>
						</tr>
					</thead>				
				<tbody id="dataTable">
					<?php
					$totjualticket=0;
					$totkomisiticket=0;
					for($j=0;$j<count($ticketing);$j++){
						$totjualticket += $ticketing[$j]['ttlnettos'];
						$totkomisiticket += $ticketing[$j]['komisi_ticket'];
						
					?>
						<tr id="dataTable">
							<td nowrap>
								<input type="text" class="form-control" id="notrans1" name="notrans[]" size="12" readonly="readonly"  value="<?=$ticketing[$j]['notrans'];?>">
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="tgl_berlaku1" name="tgl_berlaku[]" size="12" readonly="readonly" value="<?=$ticketing[$j]['tgl_berlaku'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="pcodeticket1" name="pcodeticket[]" size="12" maxlength="20" readonly="readonly" value="<?=$ticketing[$j]['PCode'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="namaticket1" name="namaticket[]" size="40" readonly="readonly" value="<?=$ticketing[$j]['NamaLengkap'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="qtyticket1" name="qtyticket[]" size="2" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['quantity'];?>" >
							</td>
							<td nowrap>
								<input type="hidden" class="form-control" id="hargaticket1" name="hargaticket[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['price'];?>" >
								<input type="text" class="form-control" id="harga_ticket_temp1" name="harga_ticket_temp[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=number_format($ticketing[$j]['price'],0,'.',',');?>" >
							</td>
							
						    <td nowrap>
								<input type="text" class="form-control" id="potongans1" name="potongans[]" size="10" readonly="readonly" style="text-align: right;" value="0" >
							</td>
			
							<td nowrap>
								<input type="text" class="form-control" id="nettos1" name="nettos[]" size="10" readonly="readonly" style="text-align: right;" value="<?=number_format($ticketing[$j]['ttlnettos']);?>" >
							</td>
							<td nowrap>
							<input type="hidden" class="form-control" id="nilticket1" name="nilticket[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['komisi_ticket'];?>" >
								<input type="text" class="form-control" id="nilaiticket1" name="nilaiticket[]" size="10" readonly="readonly" style="text-align: right;" value="<?=number_format($ticketing[$j]['komisi_ticket']);?>" >
							</td>
							
							<td nowrap>
								<input type="text" class="form-control" id="persentaseticket1" name="persentaseticket[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['komisi'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="komisiticketa1" name="komisiticketa[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['komisi1'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="komisiticketb1" name="komisiticketb[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['komisi2'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="komisiticketc1" name="komisiticketc[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['komisi3'];?>" >
							</td>
							<td nowrap>
								<input type="text" class="form-control" id="komisiticketd1" name="komisidticket[]" size="12" maxlength="11" readonly="readonly" style="text-align: right;" value="<?=$ticketing[$j]['komisi4'];?>" >
								<input type="hidden" class="form-control" id="hrgPPNticket1" name="hrgPPNticket[]" value="0" >
							</td>
			
						</tr>
						<?php } ?>	
					</tbody>			
				</table>
	
	<table class="table table-bordered responsive">
		<tr>
			<td colspan="6" width="77%" style="text-align: right;">
				<b>Total Sales</b>
			</td>
			<td>
				<input name="totjual" type="hidden" class="form-control" value="<?=$totjual;?>" readonly="readonly" id="totjual" value="" size="15" maxlength="15" style="text-align: right;" />
				<input name="totjual_temp" type="text" class="form-control"  value="<?=number_format($totjual,0,'.',',');;?>" readonly="readonly" id="totjual_temp" value="" size="15" maxlength="15" style="text-align: right;" />
			</td>
			<td>
				<input name="total_sales" type="hidden" class="form-control"  value="<?=$totkomisi;?>" readonly="readonly" id="total_sales" value="" size="15" maxlength="15" style="text-align: right;" />
				<input name="total_sales_temp" type="text" class="form-control" value="<?=number_format($totkomisi,0,'.',',');?>" readonly="readonly" id="total_sales_temp" value="" size="15" maxlength="15" style="text-align: right;" />

			</td>
		</tr>
		
		<tr>
			<td colspan="6" width="77%" style="text-align: right;">
				<b>Total Ticket</b>
			</td>
			<td>
				<input name="totticket" type="hidden" class="form-control"  value="<?=$totjualticket;?>" readonly="readonly" id="totticket" value="" size="15" maxlength="15" style="text-align: right;" />
				<input name="totticket_temp" type="text" class="form-control" value="<?=number_format($totjualticket,0,'.',',');?>" readonly="readonly" id="totticket_temp" value="" size="15" maxlength="15" style="text-align: right;" />

			</td>
			<td>
				<input name="total_ticket" type="hidden" class="form-control"  value="<?=$totkomisiticket;?>" readonly="readonly" id="total_ticket" value="" size="15" maxlength="15" style="text-align: right;" />
				<input name="total_ticket_temp" type="text" class="form-control" value="<?=number_format($totkomisiticket,0,'.',',');?>" readonly="readonly" id="total_ticket_temp" value="" size="15" maxlength="15" style="text-align: right;" />

			</td>
		</tr>
		
		<tr>
			<td colspan="6" width="77%" style="text-align: right;">
				<b>Total Komisi Sales & Ticket</b>
			</td>
			<td>
				<input name="totjualdanticket" type="hidden" class="form-control" value="<?=$totjual+$totjualticket;?>" readonly="readonly" id="totjualdanticket" value="" size="15" maxlength="15" style="text-align: right;" />
				<input name="totjualdanticket_temp" type="text" class="form-control"  value="<?=number_format($totjual+$totjualticket,0,'.',',');;?>" readonly="readonly" id="totjualdanticket_temp" value="" size="15" maxlength="15" style="text-align: right;" />
			</td>
			<td>
				<input name="total" type="hidden" class="form-control"  value="<?=$totkomisiticket+$totkomisi;?>" readonly="readonly" id="total" value="" size="15" maxlength="15" style="text-align: right;" />
				<input name="total_temp" type="text" class="form-control" value="<?=number_format($totkomisiticket+$totkomisi,0,'.',',');?>" readonly="readonly" id="total_temp" value="" size="15" maxlength="15" style="text-align: right;" />

			</td>
		</tr>

	</table>
</tbody>

				</table>