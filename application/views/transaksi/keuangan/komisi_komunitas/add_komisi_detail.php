<?php
$mylib = new globallib();
?>
<table id="detail" class="table table-bordered responsive">
					<thead>
						<tr id="baris0">
							<th>
								Tanggal
							</th>
							<th style="display: none">
								No Stiker
							</th>
							<th>
								Kode Tour Leader
							</th>
							<th>
								Nama Tour Leader
							</th>
							<th>
								Omset
							</th>
						</tr>
					</thead>
					
					<span id='UpdateDetail'></span>
					

<tbody id="dataTable">
	<?php
	$tot_omset=0;
	for($i=0;$i<count($data);$i++){
		
	?>
		<tr id="dataTable">
			<td nowrap>
				<input type="text" class="form-control" id="tgl1" name="tgl[]" size="12" readonly="readonly"  value="<?=$mylib->ubah_tanggal($data[$i]['Tanggal']);?>">
			</td>
			<td nowrap style="display: none">
				<input type="text" class="form-control" id="nostiker1" name="nostiker[]" size="12" readonly="readonly" value="<?=$data[$i]['NoStiker'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="kdtourleader1" name="kdtourleader[]" size="12" readonly="readonly" value="<?=$data[$i]['KdTourLeader'];?>" >
			</td>
			<td nowrap>
				<input type="text" class="form-control" id="namatl1" name="namatl[]" size="12" readonly="readonly" value="<?=$data[$i]['Nama'];?>" >
			</td>
			<td nowrap>
				<input style="text-align:right;" type="text" class="form-control" id="omset1" name="omset[]" size="12" maxlength="20" readonly="readonly" value="<?=number_format($data[$i]['Omset']);?>" >
				<input style="text-align:right;" type="hidden" class="form-control" id="omset_temp1" name="omset_temp[]" size="12" maxlength="20" readonly="readonly" value="<?=$data[$i]['Omset'];?>" >
			</td>
		</tr>
	<?php
	$tot_omset+=$data[$i]['Omset'];
	}
	?>
	
	<table class="table table-bordered responsive">
		<tr>
			<td colspan="2" width="77%" style="text-align: right;">
				<b>Total Omset</b>
			</td>
			<td>
				<input name="totomset" type="hidden" class="form-control" value="<?=$tot_omset;?>" readonly="readonly" id="totomset" size="15" maxlength="15" style="text-align: right;" />
				<input name="totomset_temp" type="text" class="form-control"  value="<?=number_format($tot_omset,0,'.',',');;?>" readonly="readonly" id="totomset_temp" size="15" maxlength="15" style="text-align: right;" />
			</td>
			
		</tr>
		<tr>
			<td colspan="2" width="77%" style="text-align: right;">
				<b>Komisi Komunitas (1.5%)</b>
			</td>
			<td>
				<input name="totkomisi" type="hidden" class="form-control" value="<?=$tot_omset*1.5/100;?>" readonly="readonly" id="totkomisi" size="15" maxlength="15" style="text-align: right;" />
				<input name="totkomisi_temp" type="text" class="form-control"  value="<?=number_format($tot_omset*1.5/100,0,'.',',');;?>" readonly="readonly" id="totkomisi_temp" size="15" maxlength="15" style="text-align: right;" />
			</td>
			
		</tr>

	</table>
</tbody>

				</table>