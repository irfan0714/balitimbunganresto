<?php
$this->load->view('header');
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
//echo number_format("1000000",0,"","");
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js">
</script>
<script language="javascript" src="<?= base_url(); ?>public/js/komisi.js">
</script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js">
</script>

<script language="javascript">
		
	function submit_this()
	{
		if(!$('#v_tourtravel').val())
		{
			alert('Pilih Kode Travel');
			$('#v_tourtravel').focus();
			return false;
		}else
		{
			document.getElementById('btn_submit').submit()

		}
	}
	
	function generateList() {
    	base_url = $("#baseurl").val();
    	kdtourtravel = $("#v_tourtravel").val();
		tglawal = $("#tgl_awal").val();
		tglakhir = $("#tgl_akhir").val();
		
		$('#pleaseWaitDialog').modal('show');
				
    	$.ajax({
			type: "POST",
			url: base_url + "index.php/keuangan/komisi_komunitas/getlistOmset/",
			data : {kdtourtravel: kdtourtravel, tglawal: tglawal, tglakhir: tglakhir},
			success: function(data) {
				$('#UpdateDetail').html(data);
				
				
					$('#pleaseWaitDialog').modal('hide');
			
		
			}
		});
	}
	
	function cari_tt(url)
		{
			var act=$("#namatourtravel").val();
			
			$.ajax({
					url: url+"index.php/keuangan/komisi_komunitas/ajax_tt/",
					data: {id:act},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#v_tourtravel').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
				}); 
			
			   	
   		}

</script>


<body onload="firstLoad('komisi'); //loading()">
<form method='post' name="komisi" id="komisi" action='<?= base_url(); ?>index.php/keuangan/komisi_komunitas/save_new_komisi' onsubmit="return submit_this()" class="form-horizontal">
	<div class="col-md-12">
		<div class="panel panel-gradient">
			<div class="panel-heading">
				<div class="panel-title">
					Form Pembayaran Komisi Komunitas
				</div>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-sm-2">
						Tanggal
					</label>
					<div class="col-sm-2">
						<input data-toggle="tooltip" data-placement="top" data-original-title="Tanggal Mulai" name="tgl_awal" type="text" class="form-control datepicker" id="tgl_awal" value="<?= $aplikasi->TglTrans ?>" size="10" maxlength="10"   />
					</div>
					<div class="col-sm-2">
						<input data-toggle="tooltip" data-placement="top" data-original-title="Tanggal Akhir" name="tgl_akhir" type="text" class="form-control datepicker" id="tgl_akhir" value="<?= $aplikasi->TglTrans ?>" size="10" maxlength="10"   />
					</div>
					
				</div>
				<div class="form-group">
					<label class="col-sm-2">
						Tour Travel
					</label>
					<div class="col-sm-2">
					<input name="tgl" data-toggle="tooltip" data-placement="top" data-original-title="Cari Kode atau Nama Tour Leader" name="namatourtarvel" type="text" class="form-control" id="namatourtravel" value="" size="10" onkeyup="cari_tt('<?php echo base_url(); ?>')" maxlength="10"   />
					</div>
					<div class="col-sm-3">
					
						<div class="input-group">
							<select class="form-control" name="v_tourtravel" id="v_tourtravel" style="width: 200px;">
								<option value="">Pilih Tour Travel</option>
								<?php
								foreach($tour_travel as $val)
								{
									?><option value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
								}
								?>
							</select>  
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Go
								</button>
							</span>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2">
						Keterangan
					</label>
					<div class="col-sm-5">
						<input name="ket" type="text" class="form-control" id="ket" value="" size="35" maxlength="100"   />
					</div>
				</div>
				<br>

				<div id="Layer1" style="display:none">
					<p align="center">
						<img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
					</p>
				</div>
				<span id='UpdateDetail'></span>
				<input type="hidden" id="urutan" name="urutan" value="1">
				<input type="hidden" id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
				<div class="form-group">
					<label class="col-sm-2 control-label">
					</label>
					<div class="col-sm-4">
						<a class="btn btn-default" href="<?= base_url(); ?>index.php/keuangan/komisi_komunitas/">
							<i class="entypo-back">
							</i>Back
						</a>&nbsp;
						<button class="btn btn-primary" type="submit" onclick="submit_this();" id="btn_submit">
							<i class="entypo-drive">
							</i>Save
						</button>
					</div>
				</div>

			</div>



		</div>
	</div>
	</div>
	
	<div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.5);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1>Mohon Tunggu, Sedang Proses...</h1>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
</form>


<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script type="text/javascript">
	new Spry.Widget.ValidationTextField("kdagent", "none");
	new Spry.Widget.ValidationTextField("Penerima", "none");
	new Spry.Widget.ValidationSelect("KdKasBank");
	new Spry.Widget.ValidationTextField("tgl", "date", {format:"dd-mm-yyyy", useCharacterMasking:true});
</script>
