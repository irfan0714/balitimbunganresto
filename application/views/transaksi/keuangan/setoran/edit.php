<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'terima',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<!--<script language="javascript" src="--><?//= base_url(); ?><!--public/js/komisi.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/jquery.price_format.2.0.js"></script>

<script language="javascript">

    function submit_this() {
//        if(!$('#kdagent').val()){
//            alert('Masukan Kd Group');
//            $('#kdagent').focus();
//            return false;
//        }
    }
    function hitung(){
        //alert("hitung sisa");
        var a;
        var b;
        var c;
        a = $("#nsetoran").val();
        b = $("#jumlah").val();
        c = b - a;
        //alert(c);
        document.getElementById('selisih').value= c;
    }

    function generateList(){
        base_url = $("#baseurl").val();
        url = base_url+"index.php/pop/setoran_list/index/";
        window.open(url,'popuppage','width=750,height=400,top=200,left=150');
    }

</script>

<body onload="firstLoad('komisi'); //loading()">
<form method='post' name="simpan" id="simpan" action='<?= base_url(); ?>index.php/<?=$tr1;?>/<?=$tr2;?>/save_new' onsubmit="return submit_this();" class="form-horizontal">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$judul;?>
                </div>
            </div>
            <div class="panel-body">

                <?php
                $mylib = new globallib();
                echo $mylib->neonwrite_textbox("Tanggal","tgl",$tgl,"15","10","readonly='readonly'","text",$gantikursor,"1");
                echo $mylib->neonwrite_textbox("Jam","jam",date("H:i:s"),"15","10","readonly='readonly'","text",$gantikursor,"1");
                echo $mylib->neonwrite_textbox("NIK","kode",$Nik,"15","10","readonly='readonly'","text",$gantikursor,"1");
                echo $mylib->neonwrite_textbox("Nama Kasir","nama",$NamaKasir,"15","10","readonly='readonly'","text",$gantikursor,"1");
                echo $mylib->Number_textbox("Jumlah Setoran ","nsetoran",$NilaiSetoran,"15","10","","text"," onkeypress='hitung();'","1");
                echo $mylib->Number_textbox("Selisih","selisih",$Selisih,"15","10","readonly='readonly","text",$gantikursor,"1");
                echo $mylib->Number_textbox("Tunai ","jumlah",$Tunai,"15","10","readonly='readonly","text",$gantikursor,"1");
                echo $mylib->Number_textbox("Kartu Kredit ","kredit",$KKredit,"15","10","readonly='readonly","text",$gantikursor,"1");
                echo $mylib->Number_textbox("Kartu Debit","debit",$KDebit,"15","10","readonly='readonly","text",$gantikursor,"1");
                echo $mylib->Number_textbox("Voucher ","voucher",$Voucher,"15","10","readonly='readonly","text",$gantikursor,"1");
                echo $mylib->neonwrite_textbox("Keterangan","ket",$Keterangan,"35","35","","text",$gantikursor,"1");
                ?>


                <div id="Layer1" style="display:none">
                    <p align="center">
                        <img src='<?= base_url(); ?>public/images/ajax-loader.gif'>
                    </p>
                </div>


                <input type='hidden' id="nodok" name="nodok" value="">
                <input type='hidden' id="flag" name="flag" value="edit">
                <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                <div class="form-group">
                    <label class="col-sm-2 control-label"> </label>
                    <div class="col-sm-4">
                        <a class="btn btn-default" href="<?= base_url(); ?>index.php/keuangan/komisi/"><i class="entypo-back"></i>Back</a>&nbsp;
                        <button class="btn btn-primary" type="submit" onclick="return submit_this;"><i class="entypo-drive"></i>Save</button>
                    </div>
                </div>

            </div>



        </div>
    </div>
    </div>
</form>

<?php $this->load->view('footer'); ?>
