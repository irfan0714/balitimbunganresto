<?php
$this->load->view('header');
$searchby = $this->input->post('searchby');
$date1 = $this->input->post('date1');
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
<!--<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>-->
<script language="javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script language="javascript">
    $(function () {
                $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy'
                });
            });
            
    
    function deleteTrans(nodok, url)
    {
        var r = confirm("Apakah Anda Ingin Menghapus Transaksi " + nodok + " ?");
        if (r == true) {
            $.post(url + "index.php/keuangan/paymentv/delete_paymentv/", {
                kode: nodok},
            function (data) {
                //                            alert("data");
                window.location = url + "index.php/keuangan/paymentv/";
            });
        }
    }
    function PopUpPrint(kode, baseurl)
    {
        url = "index.php/keuangan/komisi/cetak/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=900,height=500,top=50,left=50');
    }
    function gantiSearch()
    {
        if ($("#searchby").val() == "NoTransaksi")
        {
            $("#normaltext").css("display", "");
            $("#datetext").css("display", "none");
            //$("#date1").datepicker("destroy");
            $("#date1").val("");
        }
        else
        {
            $("#datetext").css("display", "");
            $("#normaltext").css("display", "none");
            $("#stSearchingKey").val("");
            //$("#date1").datepicker({ dateFormat: 'dd-mm-yy',showOn: 'button', buttonImageOnly: true, buttonImage: '<?php echo base_url(); ?>/public/images/calendar.png' });
            //$('#date1').Zebra_DatePicker({format: 'd-m-Y'});
        }
    }
</script>
<body onload="option();">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$judul;?>
                </div>
            </div>
            <div class="panel-body">
				<?php if ($date1 != ""){
					$hide = "style='display:'";
					$hide2 = "style='display:none'";
					}else{
						$hide = "style='display:none'";					
						$hide2 = "style='display:'";
					}
				?>
               <form method="POST"  name="search" action="">
                    <div class="control-group">
                        <div class="controls" align="right">
                            <table border="0">
                                <tr>
                                    <td id="normaltext" <?=$hide2;?>><input type='text' size='20' name='stSearchingKey' id='stSearchingKey' class="form-control" value="<?php echo @$_POST['stSearchingKey']; ?>"></td>
                                    <td id="datetext" <?=$hide;?>><input type='text' size='12' name='date1' id='date1' class="form-control datepicker" data-date-format="dd-mm-yyyy" value="<?php echo @$_POST['date1']; ?>"></td>
                                    <td>
                                        <select size="1" height="1" name ="searchby" id ="searchby" onchange="gantiSearch()" class="form-control">
                                            <option <?php if ($searchby == "NoTransaksi") echo "selected='selected'"; ?> value="NoTransaksi">No Transaksi</option>
                                            <option <?php if ($searchby == "TglTransaksi") echo "selected='selected'"; ?> value="TglTransaksi">Tanggal</option>
                                        </select>
                                    </td>
                                    <td><button type="submit" class="btn-primary">GO</button></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>

                <br>

                <table align = 'center' class='table table-bordered table-hover responsive'>
                    <thead>
                        <tr>
                            <th nowrap>NoTransaksi</th>    
                            <th nowrap>Tanggal</th>
                            <th nowrap>Nama Kasir</th>
                            <th nowrap>Setoran</th>
                            <th nowrap>Selisih</th>
                            <th nowrap>Tunai</th>
                            <th nowrap>KDebit</th>
                            <th nowrap>KKredit</th>
                            <th nowrap>Voucher</th>
                            <th nowrap>Keterangan</th>
                            <?php
                            if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <th nowrap>Action</th>
                            <?php } $mylib = new globallib(); ?>
                        </tr>
                    </thead>
                    <?php
                    if (count($datalist) == 0) {
                        ?>
                        <td nowrap colspan="11" align="center">Tidak Ada Data</td>
                        <?php
                    }
                    $grdtotal=0;
                    for ($a = 0; $a < count($datalist); $a++) {
                        ?>
                        <tr>
                            <td nowrap><?= $datalist[$a]['NoTransaksi']; ?></td>
                            <td nowrap><?= $datalist[$a]['TglTransaksi']; ?></td>
                            <td nowrap><?= $datalist[$a]['NamaKasir']; ?></td>
                            <td nowrap align="right"><?= number_format($datalist[$a]['NilaiSetoran'],0,'.',','); ?></td>
                            <td nowrap align="right"><?= number_format($datalist[$a]['Selisih'],0,'.',','); ?></td>
                            <td nowrap align="right"><?= number_format($datalist[$a]['Tunai'],0,'.',','); ?></td>
                            <td nowrap align="right"><?= number_format($datalist[$a]['KDebit'],0,'.',','); ?></td>
                            <td nowrap align="right"><?= number_format($datalist[$a]['KKredit'],0,'.',','); ?></td>
                            <td nowrap align="right"><?= number_format($datalist[$a]['Voucher'],0,'.',','); ?></td>
                            <td nowrap><?= stripslashes($datalist[$a]['Keterangan']); ?></td>
                            <?php
                            if ($link->edit == "Y" || $link->delete == "Y") {
                                ?>
                                <td nowrap>
                                    <?php
                                    if ($link->view == "Y") {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/printer.png' border = '0' title = 'Print' onclick="PopUpPrint('<?= $datalist[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/></a>
                                        <?php
                                    }
                                    if ($link->edit == "Y" && (substr($datalist[$a]['TglTransaksi'], 0, 2) == date('d'))) {
                                        ?>
                                        <a href="<?= base_url(); ?>index.php/keuangan/setor_kasir/edit_setoran/<?= $datalist[$a]['NoTransaksi']; ?>"><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                                        <?php
                                    }
                                    if ($link->delete == "Y" && (substr($datalist[$a]['TglTransaksi'], 0, 2) == date('d'))) {
                                        ?>
                                        <img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete' onclick="deleteTrans('<?= $datalist[$a]['NoTransaksi']; ?>', '<?= base_url(); ?>');"/>
                                        <?php
                                    }
                                    
                                    ?>
                                </td>
                            <?php } ?>
							</tr>
                            <?php
                        }
                        $tot = 0;
                        $tot +=$grdtotal;
                        ?>
                        <tfoot>
                        	<tr>
                        		<td colspan="4" width="77%" style="text-align: right;">
                        			<b></b>
                        		</td>
                        		<td style="text-align: right;">
                        			<b><?php echo number_format($tot,0,'','.') ;?></b>
                        		</td>
                        	</tr>
                        </tfoot>
                </table>
                <table align = 'center'  >
                    <tr>
                        <td>
                            <?php echo $this->pagination->create_links(); ?>
                        </td>
                    </tr>
                    <?php
                    if ($link->add == "Y") {
                        ?>
                        <tr>
                            <td nowrap colspan="3">
                                <a href="<?= base_url(); ?>index.php/keuangan/setor_kasir/add_new/"><img src='<?= base_url(); ?>public/images/add.png' border = '0' title = 'Add'/></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</body>
<?php $this->load->view('footer'); ?>
