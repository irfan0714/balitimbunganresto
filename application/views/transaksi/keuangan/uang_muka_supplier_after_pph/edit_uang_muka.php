<?php 

$this->load->view('header'); 

$modul = "Uang Muka Supplier";

?>

<script language="javascript" src="<?=base_url();?>public/js/uang_muka_supplier_v2.js"></script>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/keuangan/uang_muka_supplier/save_data'>
		
	    <table class="table table-bordered responsive">                        

	        <tr>
	            <td class="title_table">No. Dokumen <font color="red"><b>(*)</b></font></td>
	            <td><input readonly type="text" class="form-control-new" value="<?php echo $header->NoDokumen; ?>" name="v_no_dokumen" id="v_no_dokumen" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Payment Voucher <font color="red"><b>(*)</b></font></td>
	            <td><input readonly type="text" class="form-control-new" value="<?php echo $header->NoPV; ?>" name="v_no_pv" id="v_no_pv" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo $header->Tanggal; ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Kas Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_kas_bank" id="v_kas_bank" style="width: 200px;">
	            		<option value="">Pilih Kas Bank</option>
	            		<?php
	            	
	            		foreach($KasBank as $val)
	            		{
	            			$selected='';
	            			if($header->KdKasBank == $val['KdKasBank']){
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdKasBank"]; ?>"><?php echo $val["NamaKasBank"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Referensi <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->NoReferensi; ?>" name="v_no_ref" id="v_no_ref" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Nama Supplier<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
	            			$selected='';
	            			if($header->KdSupplier == $val['KdSupplier']){
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jumlah <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->Jumlah; ?>" onkeyup="hitung()" name="v_jumlah" id="v_jumlah" onkeyup="hitung()" maxlength="255" style="text-align: right; width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jenis PPH<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_jenis_pph" id="v_jenis_pph" style="width: 200px;">
	            		<?php
	            		foreach($rekeningpph as $val)
	            		{
	            			if($header->KdRekening==$val['KdRekening']){
								$selected = 'selected';
							}else{
								$selected = '';
							}
							?><option <?=$selected;?> value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">PPH <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->PPH; ?>" onkeyup="hitung()" name="v_pph" id="v_pph" onkeyup="hitung()" maxlength="255" style="text-align: right; width: 30px;">&nbsp;&nbsp;<input type="text" class="form-control-new" value="<?php echo ($header->Jumlah*$header->PPH/100); ?>" name="v_rp_pph" id="v_rp_pph" onkeyup="hitung()" maxlength="255" style="text-align: right; width: 165px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Total <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="<?= $header->Jumlah-($header->Jumlah*$header->PPH/100);?>" name="v_total" id="v_total" maxlength="255" style="text-align: right; width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Sisa <font color="red"><b>(*)</b></font></td>
	            <td><input readonly type="text" class="form-control-new" value="<?php echo $header->Sisa; ?>" name="v_sisa" id="v_sisa" maxlength="255" style="text-align: right; width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Rekening <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->NoRekening; ?>" name="v_no_rek" id="v_no_rek" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Sub. Divisi<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_subdivisi" id="v_subdivisi" style="width: 200px;">
	            		<option value="">Pilih Sub Divisi</option>
	            		<?php
	            		foreach($subdivisi as $val)
	            		{
	            			$selected='';
	            			if($header->KdSubDivisi == $val['KdSubDivisi']){
								$selected='selected="selected"';
							}
							
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSubDivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->Keterangan; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
           
            <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
					<?php if($otorisasi=="edit"){ ?>
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/keuangan/uang_muka_supplier/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
					<?php }else{ ?>
					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/keuangan/uang_muka_supplier/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <?php } ?>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	function hitung() {
				
				jml = parseFloat($("#v_jumlah").val());
				pph = parseFloat($("#v_pph").val());
				ttl=jml-(jml*pph/100);
				$("#v_total").val(ttl);
				$("#v_rp_pph").val(jml*pph/100);

    }
</script>