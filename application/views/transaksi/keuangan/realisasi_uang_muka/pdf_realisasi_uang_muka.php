<?php
$mylib = new globallib();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Reservasi</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 8.5pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 8.5pt;
		}
		
	</style>
    <body>
        <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
                <td colspan="100%" align="center"><img src="<?= base_url(); ?>public/images/Logosg.png" width="150" alt="Secret Garden Village"/></td>
            </tr>
            <tr>
                <td colspan="100%" align="center"><b>REALISASI UANG MUKA KARYAWAN</b></td>
            </tr>
            <tr>
                <td align="center" colspan="100%"><span id="NoDokumen"> <?= $results->NoDokumen;?></span></td>
            </tr>
            
            <tr>
                <td colspan="100%">&nbsp;</td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table width="50%" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td width="100">Tanggal</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $mylib->ubah_tanggal($results->TglDokumen);?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">No. Referensi</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->NoReferensi; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Kas Bank</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->KdRekening." ".$results->NamaKasBank; ?></span></td>

                        </tr>
                        
                    </table>
                </td>
                <td></td>
                <td colspan="100%">
                    <table width="50%" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        
                        
                        <tr>
                            <td width="100">Nama Karyawan</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->employee_name; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Keterangan</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->Keterangan; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1">&nbsp;</td>
                            <td width="150">&nbsp;</td>

                        </tr>
                       
                    </table>
                </td>
            </tr>            
            
</table>
<br>
<table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
	<tr>
         <thead>
        <th width="20">No</th>
        <th width="100">Tanggal</th>
        <th width="100">No.Dokumen</th>
        <th>Keterangan</th>
        <th width="100">Jumlah</th>
        <th width="100">Realisasi</th>
        </thead>
    </tr>
    
                                          <?php $no = 1;
                                                $tots=0;
											foreach($detail_realisasi AS $val){ ?>
												<tr>
													<td align='center'><?=$no?></td>
													<td align='center'><?=$val['TglDokumen']?></td>
													<td align='center'><?=$val['NoDokumen']?>
													<td align='left'><?=$val['Keterangan']?>
													<td align='right'><?= number_format($val['Jumlah'],0);?></td>
													<td align='right'><?= number_format($val['Realisasi'],0);?></td>
												</tr>
											<?php $no++;
											$tots+=$val['Realisasi'];} ?>
											
<tr>
	<td colspan="5" align="right">Total</td>
	<td align='right'><?= number_format($tots,0);?></td>
</tr>
											
</table>
<br>
<table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
	<tr>
         <thead>
        <th width="100">Nama Rekening</th>
        <th width="100">SubDivisi</th>
        <th width="100">Departemen</th>
        <th width="100">Keterangan</th>
        <th width="100">Jumlah</th>
        </thead>
    </tr>
    
                                          <?php 
                                          $totsx= 0;
											foreach($detail_realisasi_pv AS $val){ ?>
												<tr>
													<td align="left"><?=$val['KdRekening']." ".$val['NamaRekening']?></td>
													<td align='left'><?=$val['NamaSubDivisi']?></td>
													<td align='left'><?=$val['NamaDepartemen']?></td>
													<td align='left'><?=$val['Keterangan']?></td>
													<td align="right"><?= number_format($val['Jumlah'],0);?></td>
												</tr>
											<?php $totss+=$val['Jumlah']; } ?>
											
	<tr>
	<td colspan="4" align="right">Total</td>
	<td align='right'><?= number_format($totss,0);?></td>
</tr>
											
</table>
<br><br>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
<tr><td align="center">DiBuat Oleh,</td><td align="center">DiSetujui Oleh,</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">(....................................)</td><td align="center">(....................................)</td></tr>
</table>

</body>
</html>