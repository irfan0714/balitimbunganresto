<?php 

$this->load->view('header'); 

$modul = "Realisasi Uang Muka";

?>

<script language="javascript" src="<?=base_url();?>public/js/realisasi_uang_muka_v2.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/payment_.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>


<script>
	function cari_karyawan(){
		keyword = $("#keyword_karyawan").val();
		url = $("#base_url").val(); 
		$.ajax({
			url: url+"index.php/keuangan/realisasi_uang_muka/cari_karyawan/",
			data: {employee:keyword},
			type: "POST",					
			success: function(res)
			{
				$('#v_employee').html(res);
			},
			error: function(e) 
			{
				alert(e);
			} 
		});
	}
</script>
<?php
if($this->session->flashdata('msg'))
{
  $msg = $this->session->flashdata('msg');
  
  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
}
?>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/keuangan/realisasi_uang_muka/save_data'>
		
	    <table class="table table-bordered responsive">                        

	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Nama Karyawan<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" value="" placeholder="Cari Karyawan" name="keyword_karyawan" id="keyword_karyawan" style="width: 150px;" onchange="cari_karyawan()">
	            	<select class="form-control-new" name="v_employee" id="v_employee" style="width: 200px;" onchange="getData()">
	            		<option value="">Pilih Karyawan</option>
	            		<?php
	            		foreach($employee as $val)
	            		{
							?><option value="<?php echo $val["employee_id"]; ?>"><?php echo $val["employee_name"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Referensi <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_no_ref" id="v_no_ref" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Kas Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_kas_bank" id="v_kas_bank" style="width: 200px;">
	            		<option value="">Pilih Kas Bank</option>
	            		<?php
	            		foreach($KasBank as $val)
	            		{
							?><option value="<?php echo $val["KdKasBank"]; ?>"><?php echo $val["NamaKasBank"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%" id="TabelDetail">
					
	        	</td>
	        </tr>
           
            <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/keuangan/realisasi_uang_muka/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?= base_url();?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script>
	function getData(){
		employee_id = $("#v_employee").val();
		url = $("#base_url").val(); 
		
				$.ajax({
					url: url+"index.php/keuangan/realisasi_uang_muka/getData/",
					data: {id:employee_id},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#TabelDetail').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
	}
	
	function detailNew()
	{
		var clonedRow = $("#detail tr:last").clone(true);
		var intCurrentRowId = parseFloat($('#detail tr').length )-2;
		nama = document.getElementsByName("kdrekening[]");
		temp = nama[intCurrentRowId].id;
		intCurrentRowId = temp.substr(10,temp.length-10);
		var intNewRowId = parseFloat(intCurrentRowId) + 1;
		$("#kdrekening" + intCurrentRowId , clonedRow ).attr( { "id" : "kdrekening" + intNewRowId,"value" : ""} );
		$("#pick" + intCurrentRowId , clonedRow ).attr( { "id" : "pick" + intNewRowId} );
		$("#del" + intCurrentRowId , clonedRow ).attr( { "id" : "del" + intNewRowId} );
		$("#namarekening" + intCurrentRowId , clonedRow ).attr( { "id" : "namarekening" + intNewRowId,"value" : ""} );
		$("#jumlah" + intCurrentRowId , clonedRow ).attr( { "id" : "jumlah" + intNewRowId,"value" : ""} );
		$("#keterangan" + intCurrentRowId , clonedRow ).attr( { "id" : "keterangan" + intNewRowId,"value" : ""} );
		$("#tmpkdrekening" + intCurrentRowId , clonedRow ).attr( { "id" : "tmpkdrekening" + intNewRowId,"value" : ""} );	
		$("#savekdrekening" + intCurrentRowId , clonedRow ).attr( { "id" : "savekdrekening" + intNewRowId,"value" : ""} );
		$("#tmpjumlah" + intCurrentRowId , clonedRow ).attr( { "id" : "tmpjumlah" + intNewRowId,"value" : ""} );
		$("#subdivisi" + intCurrentRowId , clonedRow ).attr( { "id" : "subdivisi" + intNewRowId,"value" : 0} );
		$("#dept" + intCurrentRowId , clonedRow ).attr( { "id" : "dept" + intNewRowId,"value" : 0} );
		$("#urutan" + intCurrentRowId , clonedRow ).attr( { "id" : "urutan" + intNewRowId,"value" : intNewRowId} );
		$("#detail").append(clonedRow);
		$("#detail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
		$("#namarekening" + intNewRowId).focus();
		resetRow(intNewRowId);
	}

	function resetRow(id)
	{
		$("#kdrekening"+id).focus();
		$("#kdrekening"+id).val("");
		$("#namarekening"+id).val("");
		$("#jumlah"+id).val("");
		$("#keterangan"+id).val("");
		$("#tmpjumlah"+id).val(0);
	}

	function deleteRow(obj)
	{
		var index = 0;
		var indexs = 0;
		objek = obj.id;
		id = objek.substr(3,objek.length-3);
		
		var lastRow = document.getElementsByName("kdrekening[]").length;
				
		if( lastRow > 1)
			{
				$('#baris'+id).remove();
			}else{
					alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris.");
			}		
		
	}
	
    var listrek = '';
	
	$(function() {
  		var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: ''}
	     }).done(function(reks){
	     	 listrek = reks.split('#');
	     });;
	});
	
	function loadRekening(id){
	    var rekSelected= $("#"+id).val();
	    var baseurl = "<?= base_url();?>index.php/keuangan/payment/getrekening" ;
	    var rekList = "";
	    $.ajax({
	            url: baseurl,
	            type: "POST",
	            async: false,
	            data: { KdRekening: rekSelected}
	     }).done(function(reks){
	     	 rekList = reks.split(',');
	     });
	    //Returns the javascript array of sports teams for the selected sport.
	  return rekList;
	}
	/*function PilihRekening(obj){
	
		id = obj.id;
		rek = $("#"+id).val();
		id_length = id.length;
		counter = id.substr(12,id_length-1)
		//alert(id);
		//var reklist = loadRekening(id);
		//alert(listrek);
		$("#"+id).autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
    			 //alert(value.substr(0,7));
   				 //$("#"+id).val(value.substr(0,7)); 
   				 $("#kdrekening"+counter).val(value.substr(0,8)); 
   				 $("#tmpkdrekening"+counter).val(value.substr(0,8)); 
   				 $("#namarekening"+counter).autocomplete("destroy");
   				 $("#jumlah"+counter).focus();
			}
    	});		
		
	}*/
	
	function PilihRekening(obj){
		id = obj.id;
		rek = $("#"+id).val();
		id_length = id.length;
		counter = id.substr(12,id_length-1)
				
		//var reklist = loadRekening(id);
		
		$("#"+id).autocomplete({
			source: listrek,
			minLength:3,
			select: function( event, ui ) {
				var label = ui.item.label;
    			var value = ui.item.value;
    			  //alert(value.substr(0,7));
   				 //$("#"+id).val(value.substr(0,7)); 
   				 $("#kdrekening"+counter).val(value.substr(0,8)); 
   				 $("#tmpkdrekening"+counter).val(value.substr(0,8)); 
   				 $("#namarekening"+counter).autocomplete("destroy");
   				 $("#jumlah"+counter).focus();
			}
    	});		
		
	};
	
	
	function keyShortcut(e,flag,obj) {
	//var e = window.event;
	if(window.event) // IE
	{
		var code = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		var code = e.which;
	}
	
	if (code == 13) { //checks for the enter key
		
		objek = obj.id;
		if(flag=='kdrekening'){
			id = parseFloat(objek.substr(10,objek.length-10));
			findkdrekening(id);
		}
		if(flag=='namarekening'){
			id = parseFloat(objek.substr(10,objek.length-10));
			findkdrekening(id);
		}
		else if(flag=='jumlah'){
			id = parseFloat(objek.substr(6,objek.length-6));
			InputJumlah(id,'enter');
		}
		else if(flag=='keterangan'){
			id = parseFloat(objek.substr(10,objek.length-10));
			InputKeterangan(id,'enter');
		}
		else if(flag=='subdivisi'){
			id = parseFloat(objek.substr(9,objek.length-9));
			$("#dept"+id).focus();
		}
		else if(flag=='dept'){
			id = parseFloat(objek.substr(4,objek.length-4));
			$("#keterangan"+id).focus();
		}
	}
}

function calculate(obj)
	{
		
		objek = obj.id;
	    id = objek.substr(11,objek.length-11);
		var sisa = parseInt($('#v_jml_um'+id).val());
		var bayars = parseInt($('#v_realisasi'+id).val());
		
		/*if( (sisa) < (bayars) ){
			
			alert("Tidak diizinkan melebihi Jumlah Uang Muka.");
			$("#v_realisasi"+id).val("");
        	$("#v_realisasi"+id).focus();
        	return false;
			
		}else{*/
			
		    var lastRow = document.getElementsByName("v_realisasi[]").length;
		    var stotal = 0;//v_Jumalah atau total atas
		    for (index = 0; index < lastRow; index++)
		    {
		        indexs = index - 1;
		        nama = document.getElementsByName("v_realisasi[]");
		        temp = nama[index].id;
		        temp1 = parseFloat(nama[index].value);
		        if(nama[index].value != ''){
					stotal += temp1;	
				}
		    }
		    $("#v_tot_realisasi").val(Math.round(stotal));
	    
	    /*}*/
	}
	
	function calculate2(obj)
	{
		
		objek = obj.id;
	    id = objek.substr(6,objek.length-6);
		
		var lastRow = document.getElementsByName("namarekening[]").length;
		    var stotal = 0;//v_Jumalah atau total atas
		    for (index = 0; index < lastRow; index++)
		    {
		        indexs = index - 1;
		        nama = document.getElementsByName("jumlah[]");
		        temp = nama[index].id;
		        temp1 = parseFloat(nama[index].value);
		        if(temp1!=''){
					stotal += temp1;	
				}
		    }
		    $("#v_tot_pv").val(Math.round(stotal));
		
	}
</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>