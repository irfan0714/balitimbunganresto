<table class="table table-bordered responsive">
	<thead class="title_table">
		<tr>
			<th width="30"><center>No</center></th>
			<th width="120"><center>Tanggal</center></th> 
			<th width="150"><center>No. Dokumen</center></th>
			<th><center>Keterangan</center></th>
			<th width="150"><center>Jumlah</center></th>
		    <th width="150"><center>Realisasi</center></th>
		</tr>
	</thead>
	<tbody>
	<?php
  	if(count($data)==0){
  	?>
  		<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>;
  	<?php	
	}else{
		$no=0;
		foreach ($data as $var){
	?>				
			<tr>
				<td align='center'><?=$no+1;?></td>
				<td align='center'><?=$var['Tanggal'];?></td>
				<td align='center'><?=$var['NoDokumen'];?>
					<input type='hidden' class='form-control-new' name='v_no_um[]' value='<?=$var['NoDokumen'];?>'>
					<input type='hidden' class='form-control-new' name='v_no_pv[]' value='<?=$var['NoPV'];?>'>
					<input type='hidden' class='form-control-new' name='v_kd_kassbank[]' id='v_kd_kassbank<?=$no;?>' value='<?=$var['KdKasBank'];?>' style='text-align: right; width: 100%;' />
					<input type='hidden' class='form-control-new' name='v_kd_subdivisi[]' id='v_kd_subdivisi<?=$no;?>' value='<?=$var['KdSubDivisi'];?>' style='text-align: right; width: 100%;' />
				</td>
				<td align='left'><?=$var['Keterangan'];?></td>
				<td>
					<input type="hidden" class="form-control-new" name="vSisaUM[]" id="vSisaUM<?php echo $no;?>" VALUE="<?=$var['NilaiDPP'];?>" readonly style="text-align: right;">
            		<input type="text" class="form-control-new" name="SisaUM[]" id="SisaUM<?php echo $no;?>" ondblclick="CopyUM(<?php echo $no;?>)" VALUE="<?=number_format($var['NilaiDPP'],0,',','.');?>" readonly style="text-align: right;">
				</td>
				<td>
					<input type='hidden' readonly class='form-control-new' name='h_realisasi[]' id='h_realisasi<?=$no;?>' value='0' style='text-align: right; width: 100%;' />
					<input type='text' class='form-control-new'  onchange='HitungUM(<?=$no;?>)' name='v_realisasi[]' id='v_realisasi<?=$no;?>' value='0' style='text-align: right; width: 100%;' />
				</td>
			</tr>
		<?php
			$no++;
		}
		?>
			<tr>
				<td colspan='4'></td>
				<td align='Right'><b>Total</b></td>
				<td>
					<input type="hidden" readonly style='text-align:right;' type='text' class='form-control-new' name='h_tot_realisasi' id='h_tot_realisasi' value=''>
					<input readonly style='text-align:right;' type='text' class='form-control-new' name='v_tot_realisasi' id='v_tot_realisasi' value=''>
				</td>
		  	</tr>
	<?php
	}
	?>
	</tbody>
</table>
	
<?php
if(count($data) >0){  
?>
<table class="table table-bordered responsive" id="TabelDetail">
	<thead class="title_table">
		<tr>
			<th width="50"><center>No Faktur</center></th>
			<th><center>No Faktur Supplier</center></th> 
			<th width="100"><center>No RG</center></th>              
			<th width="100"><center>No PO</center></th>
			<th width="100"><center>Jatuh Tempo</center></th>
			<th width="100"><center>Nilai</center></th>
			<th width="100"><center>Bayar</center></th>
	    </tr>
	</thead>
	<tbody>
		
		<?php 
		$subtotal = 0;
		$ppn = 0;
		for($a=0 ; $a<count($row) ; $a++) {
		?>
		<tr id="baris<?php echo $a; ?>">
	        <td>
            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoFakturSupplier[]" id="NoFakturSupplier<?php echo $a;?>" value='<?=$row[$a]['NoFakturSupplier'];?>' readonly style="width: 100%;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoRG[]" id="NoRG<?php echo $a;?>" value='<?=$row[$a]['NoRG'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="NoPO[]" id="NoPO<?php echo $a;?>" value='<?=$row[$a]['NoPO'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
            </td>
            <td>
            	<input type="hidden" class="form-control-new" name="vSisa[]" id="vSisa<?php echo $a;?>" VALUE="<?=$row[$a]['Sisa'];?>" readonly style="text-align: right;">
            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" ondblclick="CopySisa(<?php echo $a;?>)" VALUE="<?=number_format($row[$a]['Sisa'], 4, '.', ',');?>" readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>" onchange="HitungBayar(<?php echo $a;?>)" style="text-align: right;">
            	<input type="hidden" readonly class="form-control-new" name="vBayar[]" id="vBayar<?php echo $a;?>" style="text-align: right;" value="">
            </td>
	    </tr>
	    <?php
	    }
	    ?>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Faktur
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vtotalfaktur" id="vtotalfaktur" VALUE="0" readonly style="text-align: right;">
	    		<input type="text" class="form-control-new" name="totalfaktur" id="totalfaktur" VALUE="0" readonly style="text-align: right;">
	    	</td>
	    </tr>
	    <tr style="display: none">
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Biaya Admin(+)
	    	</td>
	    	<td>
		    	<input type="hidden" class="form-control-new" name="vbiayaadmin" id="vbiayaadmin" VALUE="0" style="text-align: right;">
	    		<input type="text" class="form-control-new" name="biayaadmin" id="biayaadmin" VALUE="0"  onblur="BiayaAdminBlur()" style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		PPh(-)
	    	</td>
	    	<td>
		    	<input type="hidden" class="form-control-new" name="vpph" id="vpph" VALUE="0" style="text-align: right;">
	    		<input type="text" class="form-control-new" name="pph" id="pph" VALUE="0"  onblur="PPHBlur()" style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Selisih Pembulatan
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vpembulatan" id="vpembulatan" readonly style="text-align: right;">
	    		<input type="text" class="form-control-new" name="pembulatan" id="pembulatan" VALUE="0" readonly style="text-align: right;">
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Bayar
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vtotalbayar" id="vtotalbayar" VALUE="0"  style="text-align: right;">
	    		<input type="text" class="form-control-new" name="totalbayar" id="totalbayar" VALUE="0"  onblur="TotalBayarBlur()" style="text-align: right;" readonly>
	    	</td>
	    </tr>
	</tbody>
</table>
<font style="color: red; font-style: italic; font-weight: bold;">Note: Untuk pembayaran Full, double click di nilai faktur ybs.</font>
<?php
}
?>