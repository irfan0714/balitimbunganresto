<?php 

$this->load->view('header'); 

$modul = "Realisasi Uang Muka Supplier";

?>

<!--<script language="javascript" src="<?=base_url();?>public/js/realisasi_uang_muka_supplier_v2.js"></script>-->
<?php
if($this->session->flashdata('msg'))
{
  $msg = $this->session->flashdata('msg');
  
  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
}
?>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/keuangan/realisasi_uang_muka_supplier/save_data'>
		
	    <table class="table table-bordered responsive">                        

	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Nama Supplier<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" value="" placeholder="Cari Supplier" name="keyword_supplier" id="keyword_supplier" style="width: 150px;" onchange="cari_supplier()">
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;" onchange="getData()">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
							?><option value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No. Referensi <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_no_ref" id="v_no_ref" maxlength="255" style="width: 200px;"></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_kas_bank" id="v_kas_bank" style="width: 200px;">
	            		<option value="">Pilih Kas Bank</option>
	            		<?php
	            		foreach($KasBank as $val)
	            		{
							?><option value="<?php echo $val["KdKasBank"]; ?>"><?php echo $val["NamaKasBank"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%" id="TabelDetail">
					
	        	</td>
	        </tr>
           
            <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/keuangan/realisasi_uang_muka_supplier/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>


<script>
	function getData(){
		KdSupplier = $("#v_supplier").val();
		v_tgl_dokumen = $("#v_tgl_dokumen").val();
		var vdata = new Array(2);
    	vdata[0] = KdSupplier;
    	vdata[1] = v_tgl_dokumen;
    	
		url = $("#base_url").val(); 
		$.ajax({
			url: url+"index.php/keuangan/realisasi_uang_muka_supplier/getData/",
			data: {data:vdata},
			type: "POST",					
			success: function(res)
			{
				console.log(url);		
				$('#TabelDetail').html(res);
			},
			error: function(e) 
			{
				alert(e);
			} 
		});
	}
	
	function cari_supplier(){
		keyword = $("#keyword_supplier").val();
		url = $("#base_url").val(); 
		$.ajax({
			url: url+"index.php/keuangan/realisasi_uang_muka_supplier/cari_supplier/",
			data: {supplier:keyword},
			type: "POST",					
			success: function(res)
			{
				$('#v_supplier').html(res);
			},
			error: function(e) 
			{
				alert(e);
			} 
		});
	}
	
	function CopyUM(id){
		sisa =  parseFloat($("#vSisaUM" + id).val());		$("#h_realisasi"+id).val(sisa);
		$("#v_realisasi"+id).val(sisa);
		HitungUM(id);
	}
	
	function CopySisa(id){
		sisa =  parseFloat($("#vSisa" + id).val());
		$("#vBayar"+id).val(sisa);
		$("#Bayar"+id).val(sisa);
		HitungBayar(id);
	}
	
	function HitungUM(id){
		sisa =  parseFloat($("#vSisaUM" + id).val());
		bayar = $("#v_realisasi"+id).val();
		if(bayar>sisa){
			$("#v_realisasi"+id).val(0);
			$("#h_realisasi"+id).val(0);
			bayar=0;
			alert("Realisasi lebih besar dari uang muka.");
		}
		formatdec('v_realisasi'+id);
		$("#h_realisasi"+id).val(bayar);
		
		var lastRow = document.getElementsByName("v_realisasi[]").length;
    	var total = 0;
    	
    	for (index = 0; index < lastRow; index++)
    	{
			nama = document.getElementsByName("h_realisasi[]");
        	temp = nama[index].id;
        	temp1 = parseFloat(nama[index].value);
        	
        	if(temp1!='' && !isNaN(temp1)){
				total += temp1;	
			}
		}
		
		$("#v_tot_realisasi").val(total);
		$("#h_tot_realisasi").val(total);
		formatdec('v_tot_realisasi');
		$("#vtotalbayar").val(total);
		$("#totalbayar").val(total);
		formatdec('totalbayar');
		
	}
	
	function BiayaAdminBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#biayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vbiayaadmin").val(biayaadmin);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('biayaadmin');
	}
	
	function PPHBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pembulatans = parseFloat($("#vpembulatan").val());
		pph = parseFloat($("#pph").val());
		
		
		if(pph!=0){
		//pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		pembulatan = pph + pembulatans;
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		$("#vpph").val(pph);
		formatdec('pembulatan');
		formatdec('pph');
		
	    totalsbayars = 	totalfaktur - pph + pembulatan;
	    $("#vtotalbayar").val(totalsbayars);
		$("#totalbayar").val(totalsbayars);
		formatdec('totalbayar');
		
		}else{
		
		totalbayarss = $("#h_tot_realisasi").val();
			
		pembulatan = totalbayarss - totalfaktur;
		
			
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		
		$("#vtotalbayar").val(totalbayarss);
		$("#totalbayar").val(totalbayarss);
		formatdec('totalbayar');
		
		}	
	}
	
	function HitungBayar(id){
		sisa = parseFloat($("#vSisa" + id).val());
		bayar = $("#Bayar"+id).val();
		var xbayar = formatdec('Bayar'+id);
		$("#vBayar"+id).val(bayar);
		if(bayar>sisa){
			$("#Bayar"+id).val(0);
			$("#vBayar"+id).val(0);
			$("#vpembulatan").val(0);
		    $("#pembulatan").val(0);
			
			bayar=0;
			alert("Pembayaran lebih besar dari nilai faktur.");
			return false;
		}
		var lastRow = document.getElementsByName("NoFaktur[]").length;
    	var total = 0;
    	for (index = 0; index < lastRow; index++)
    	{
			nama = document.getElementsByName("vBayar[]");
        	temp = nama[index].id;
        	temp1 = parseFloat(nama[index].value);
        	
        	if(temp1!='' && !isNaN(temp1)){
				total += temp1;	
			}
		}
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		//pembulatan = totalbayar - (total + biayaadmin - pph);
		pembulatan = totalbayar - (total);
	
		
		$("#vtotalfaktur").val(total);
		$("#totalfaktur").val(total);
		
		
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('totalfaktur');
		formatdec('pembulatan');
	
	}
	
	function formatdec(nilai){
		var MataUang = 'IDR';
		if(MataUang=='IDR'){
			result = toFormat(nilai);
		}else{
			result = toFormat4(nilai);	
		}
		
		return result;
	}
	
	function detailNew()
	{
		var clonedRow = $("#detail tr:last").clone(true);
		var intCurrentRowId = parseFloat($('#detail tr').length )-2;
		nama = document.getElementsByName("kdrekening[]");
		temp = nama[intCurrentRowId].id;
		intCurrentRowId = temp.substr(10,temp.length-10);
		var intNewRowId = parseFloat(intCurrentRowId) + 1;
		$("#kdrekening" + intCurrentRowId , clonedRow ).attr( { "id" : "kdrekening" + intNewRowId,"value" : ""} );
		$("#pick" + intCurrentRowId , clonedRow ).attr( { "id" : "pick" + intNewRowId} );
		$("#del" + intCurrentRowId , clonedRow ).attr( { "id" : "del" + intNewRowId} );
		$("#namarekening" + intCurrentRowId , clonedRow ).attr( { "id" : "namarekening" + intNewRowId,"value" : ""} );
		$("#jumlah" + intCurrentRowId , clonedRow ).attr( { "id" : "jumlah" + intNewRowId,"value" : ""} );
		$("#keterangan" + intCurrentRowId , clonedRow ).attr( { "id" : "keterangan" + intNewRowId,"value" : ""} );
		$("#tmpkdrekening" + intCurrentRowId , clonedRow ).attr( { "id" : "tmpkdrekening" + intNewRowId,"value" : ""} );	
		$("#savekdrekening" + intCurrentRowId , clonedRow ).attr( { "id" : "savekdrekening" + intNewRowId,"value" : ""} );
		$("#tmpjumlah" + intCurrentRowId , clonedRow ).attr( { "id" : "tmpjumlah" + intNewRowId,"value" : ""} );
		$("#subdivisi" + intCurrentRowId , clonedRow ).attr( { "id" : "subdivisi" + intNewRowId,"value" : 0} );
		$("#dept" + intCurrentRowId , clonedRow ).attr( { "id" : "dept" + intNewRowId,"value" : 0} );
		$("#urutan" + intCurrentRowId , clonedRow ).attr( { "id" : "urutan" + intNewRowId,"value" : intNewRowId} );
		$("#detail").append(clonedRow);
		$("#detail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
		$("#namarekening" + intNewRowId).focus();
		resetRow(intNewRowId);
	}

	function resetRow(id)
	{
		$("#kdrekening"+id).focus();
		$("#kdrekening"+id).val("");
		$("#namarekening"+id).val("");
		$("#jumlah"+id).val("");
		$("#keterangan"+id).val("");
		$("#tmpjumlah"+id).val(0);
	}

	function deleteRow(obj)
	{
		var index = 0;
		var indexs = 0;
		objek = obj.id;
		id = objek.substr(3,objek.length-3);
		
		var lastRow = document.getElementsByName("kdrekening[]").length;
				
		if( lastRow > 1)
			{
				$('#baris'+id).remove();
			}else{
					alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris.");
			}		
		
	}
	
	function keyShortcut(e,flag,obj) {
	//var e = window.event;
	if(window.event) // IE
	{
		var code = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		var code = e.which;
	}
	
	if (code == 13) { //checks for the enter key
		
		objek = obj.id;
		if(flag=='kdrekening'){
			id = parseFloat(objek.substr(10,objek.length-10));
			findkdrekening(id);
		}
		if(flag=='namarekening'){
			id = parseFloat(objek.substr(10,objek.length-10));
			findkdrekening(id);
		}
		else if(flag=='jumlah'){
			id = parseFloat(objek.substr(6,objek.length-6));
			InputJumlah(id,'enter');
		}
		else if(flag=='keterangan'){
			id = parseFloat(objek.substr(10,objek.length-10));
			InputKeterangan(id,'enter');
		}
		else if(flag=='subdivisi'){
			id = parseFloat(objek.substr(9,objek.length-9));
			$("#dept"+id).focus();
		}
		else if(flag=='dept'){
			id = parseFloat(objek.substr(4,objek.length-4));
			$("#keterangan"+id).focus();
		}
	}
}

function cekTheform()
{	var tot_realisasi = parseInt($('#h_tot_realisasi').val()); 
    var totalsbayar = parseInt($('#vtotalbayar').val());
    var totalfaktur = parseInt($("#vtotalfaktur").val());
    //alert(tot_realisasi +" # "+ totalsbayar);
    if(document.getElementById("v_kas_bank").value=="")
    {
        alert("Kas Bank harus dipilih");
        document.getElementById("v_kas_bank").focus();
        return false;
    }
	else if(document.getElementById("v_no_ref").value=="")
    {
        alert("No. Referensi harus diisi.");
        document.getElementById("v_no_ref").focus();
        return false;
    }
	else if(document.getElementById("v_supplier").value=="")
    {
        alert("Nama Supplier harus dipilih");
        document.getElementById("v_supplier").focus();
        return false;
    }else if(tot_realisasi!=totalsbayar)
    {
        alert("Total Uang Muka Supplier Harus Sama Dengan Total Bayar Invoice.");
        return false;
    }else if(totalfaktur==0){
		alert("Belum ada faktur yang dibayar.");
        return false;
	}else
    {
	
    	var yesSubmit = true;
    	
        if(yesSubmit)
        {
			document.getElementById("theform").submit();	
		}  
	}
}

function deleteTrans(nodok, url)
{	
	var r=confirm("Apakah Anda Ingin Menghapus No Dokumen "+nodok+" ?")
	if (r==true)
	{
		window.location = url+"index.php/keuangan/realisasi_uang_muka/delete_trans/"+nodok+"/";	
	}
	else
	{
  		return false;
	}
}

</script>