<?php $this->load->view('header'); ?>
<head>
</head>
<script language="javascript" src="<?= base_url(); ?>public/js/realisasi_uang_muka_supplier_v2.js"></script>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/keuangan/realisasi_uang_muka_supplier/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input data-toggle="tooltip" data-placement="top" data-original-title="Cari Berdasarkan NoDokumen, NoReferensi. Keterangan." type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/keuangan/realisasi_uang_muka_supplier/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="150"><center>No. Dokumen</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th width="200"><center>Kas Bank</center></th>
		        <th><center>No Referensi</center></th>
		        <th><center>Supplier</center></th>
		        <th><center>Jumlah</center></th>
		        <th><center>Note</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

               
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $val["Tanggal"]; ?></td>
                    <td align="center"><?php echo $val["NamaKasBank"]; ?></td>
                    <td align="center"><?php echo $val["NoReferensi"]; ?></td>
                    <td align="center"><?php echo $val["Nama"]; ?></td>
                    <td align="center"><?php echo number_format($val["JmlRealisasi"],0); ?></td>
                    <td align="center"><?php echo $val["Keterangan"]; ?></td>
                    <td align="center">
                        <a href="<?php echo base_url(); ?>index.php/keuangan/realisasi_uang_muka_supplier/view_realisasi_uang_muka/<?php echo $val["NoDokumen"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                    
                    	<button type="button" class="btn btn-orange btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrint('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
			            <i class="entypo-print"></i>
			            </button>
                    
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

function PopUpPrint(kode, baseurl)
    {
        url = "index.php/keuangan/realisasi_uang_muka_supplier/create_pdf/" + escape(kode);
        window.open(baseurl + url, 'popuppage', 'scrollbars=yes, width=1280,height=800,top=50,left=50');
    }
</script>
