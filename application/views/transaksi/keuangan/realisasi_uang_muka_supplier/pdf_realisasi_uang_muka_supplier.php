<?php
$mylib = new globallib();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Reservasi</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 8.5pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 8.5pt;
		}
		
	</style>
    <body>
        <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
                <td colspan="100%" align="center"><img src="<?= base_url(); ?>public/images/Logosg.png" width="150" alt="Secret Garden Village"/></td>
            </tr>
            <tr>
                <td colspan="100%" align="center"><b>REALISASI UANG MUKA SUPPLIER</b></td>
            </tr>
            <tr>
                <td align="center" colspan="100%"><span id="NoDokumen"> <?= $results->NoDokumen;?></span></td>
            </tr>
            
            <tr>
                <td colspan="100%">&nbsp;</td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table width="50%" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td width="100">Tanggal</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $mylib->ubah_tanggal($results->TglDokumen);?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">No. Referensi</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->NoReferensi; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Kas Bank</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->NamaKasBank; ?></span></td>

                        </tr>
                        
                    </table>
                </td>
                <td></td>
                <td colspan="100%">
                    <table width="50%" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        
                        
                        <tr>
                            <td width="100">Nama Supplier</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->NamaSupplier; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Keterangan</td>
                            <td width="1">:</td>
                            <td width="150"><span id="nama_travel" name="nama_travel"><?= $results->Keterangan; ?></span></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1">&nbsp;</td>
                            <td width="150">&nbsp;</td>

                        </tr>
                       
                    </table>
                </td>
            </tr>            
            
</table>
<br>
<table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
	<tr>
         <thead>
        <th width="20">No</th>
        <th width="100">Tanggal</th>
        <th width="100">No.Dokumen</th>
        <th>Keterangan</th>
        <th width="100">Jumlah</th>
        <th width="100">Realisasi</th>
        </thead>
    </tr>
    
                                          <?php $no = 1;
                                                $tots=0;
											foreach($detail_realisasi AS $val){ ?>
												<tr>
													<td align='center'><?=$no?></td>
													<td align='center'><?=$val['TglDokumen']?></td>
													<td align='center'><?=$val['NoDokumen']?>
													<td align='left'><?=$val['Keterangan']?>
													<td align='right'><?= number_format($val['Jumlah'],0);?></td>
													<td align='right'><?= number_format($val['Realisasi'],0);?></td>
												</tr>
											<?php $no++;
											$tots+=$val['Realisasi'];} ?>
											
<tr>
	<td colspan="5" align="right">Total</td>
	<td align='right'><?= number_format($tots,0);?></td>
</tr>
											
</table>
<br>
<table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
	<thead class="title_table">
		<tr>
			<th width="50"><center>No Faktur</center></th>
			<th><center>No Faktur Supplier</center></th> 
			<th width="100"><center>No RG</center></th>              
			<th width="100"><center>No PO</center></th>
			<th width="100"><center>Jatuh Tempo</center></th>
			<th width="100"><center>Nilai</center></th>
			<th width="100"><center>Bayar</center></th>
	    </tr>
	</thead>
	<tbody>
		
		<?php 
		$subtotalfaktur = 0;
		$ppn = 0;
		for($a=0 ; $a<count($row) ; $a++) {
		?>
		<tr id="baris<?php echo $a; ?>">
	        <td align="center">
            	<?=$row[$a]['NoFaktur'];?>
            </td>
            <td align="center">
            	<?=$row[$a]['NoFakturSupplier'];?>
            </td>
            <td align="center">
            	<?=$row[$a]['NoRG'];?>
            </td>
            <td align="center">
            	<?=$row[$a]['NoPO'];?>
            </td>
            <td align="center">
            	<?=$mylib->ubah_tanggal($row[$a]['Tanggal']);?>
            </td>
            <td align="right">
                <?=number_format($row[$a]['NilaiFaktur'], 0, '.', ',');?>
            </td>
            <td align="right">
            	<?=number_format($row[$a]['Bayar'], 0, '.', ',');?>
            </td>
	    </tr>
	    <?php
	    $subtotalfaktur += $row[$a]['Bayar']; }
	    ?>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Faktur
	    	</td>
	    	<td align="right">
	    		 <?=number_format($subtotalfaktur, 0, '.', ',');?>
	    	</td>
	    </tr>
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		PPh(-)
	    	</td>
	    	<td align="right">
		    	<?=number_format($totbayar->PPh, 0, '.', ',');?>
	    	</td>
	    </tr>
	    
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Selisih Pembulatan
	    	</td>
	    	<td align="right">
	    		<?=number_format($totbayar->Pembulatan, 0, '.', ',');?>
	    	</td>
	    </tr>
	    
	    <tr>
	    	<td colspan="5">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Bayar
	    	</td>
	    	<td align="right">
	    		<?=number_format($totbayar->JmlRealisasi, 0, '.', ',');?>
	    	</td>
	    </tr>
	</tbody>
</table>
<br><br>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
<tr><td align="center">DiBuat Oleh,</td><td align="center">DiSetujui Oleh,</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">&nbsp;</td><td align="center">&nbsp;</td></tr>
<tr><td align="center">(....................................)</td><td align="center">(....................................)</td></tr>
</table>

</body>
</html>