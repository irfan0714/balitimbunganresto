<?php 

$this->load->view('header'); 

$modul = "Purchase Invoice";
//test
?>

<script language="javascript" src="<?=base_url();?>public/js/pi_marketing.js"></script>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/pi_marketing/save_data'>
		
	    <table class="table table-bordered responsive">                        
		    
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" value="" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No Recipe Goods <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input readonly type="text" class="form-control-new" name="rgno" id="rgno" value="" style="width: 180px;" onblur="detail()";/>
				    <a href="javascript:void(0)" id="get_rgm" onclick="pickThis()" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i><b>Cari</b>&nbsp;</a>
				 </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">No Proposal <font color="red"><b>(*)</b></font></td>
	            <td><input readonly type="text" class="form-control-new" value="" name="nopro" id="nopro" style="width: 233px;"></td>
	        </tr>
			
			<tr>
	            <td class="title_table">Supplier <font color="red"><b>(*)</b></font></td>
	            <td><input readonly type="text" class="form-control-new" value="" name="v_suppliernama" id="v_suppliernama" style="width: 233px;">
				<input readonly type="hidden" class="form-control-new" value="" name="v_supplier" id="v_supplier" style="width: 233px;">
				</td>
	        </tr>
			
	        
	        <!--<tr>
	            <td class="title_table">Supplier<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 233px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
							?><option value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
			
			<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" >
					   <tr>
	        	         <td colspan="100%" id="TabelDetail">
						 
						 </td>
			          </tr>
					</table>
				</td>
			</tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/pi_marketing/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>


<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/active_monthGL.js"></script>
<script>
function detail(){
var rgno=$('#rgno').val();
var url=$('#base_url').val();
	$.ajax({
		url : url+"index.php/transaksi/pi_marketing/save_detail_temp/"+rgno+"/",
		type: "GET",
		dataType: "html",
		success: function(res)
		{
		  $('#TabelDetail').html(res);
			
		},
		error: function(e) 
		{
			alert(e);
		}
	});
}

var base_url=$('#base_url').val();
active_monthGL('v_tgl_dokumen',base_url);

</script>