<?php 
$this->load->view('header_part2'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Edit Jurnal PIM Buku Besar";
?>
<script language="javascript" src="<?=base_url();?>public/js/pi_marketing.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method="post" name="theform" id="theform" action="<?=base_url();?>index.php/transaksi/pi_marketing/save_data_edit_jurnal">
	    <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->NoDokumen; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->NoDokumen; ?></b></td>
	        </tr>
	        
	        <tr style="display:none;">
	            <td class="title_table" width="150">Tanggal</td>
	            <td> 
					<input type="text" class="form-control-new datepicker" value="<?php if($header->TglDokumen_!="" && $header->TglDokumen_!="00-00-0000") { echo $header->TglDokumen_; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr style="display:none;">
	            <td class="title_table">No Recipe Goods <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input readonly type="text" class="form-control-new" name="rgno" id="rgno" value="<?php echo $header->RGNo; ?>" style="width: 180px;"/>
				    <a href="javascript:void(0)" id="get_rgm" onclick="pickThis()" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i><b>Cari</b>&nbsp;</a>
				 </td>
	        </tr>
	        
	        <tr style="display:none;">
	            <td class="title_table">No Proposal <font color="red"><b>(*)</b></font></td>
	            <td><input readonly ="text" class="form-control-new" value="<?php echo $header->NoProposal; ?>" name="nopro" id="nopro" style="width: 233px;"></td>
	        </tr>
	              
	        <tr style="display:none;">
	            <td class="title_table">Supplier</td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
	            			$selected="";
							if($header->KdSupplier == $val["KdSupplier"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        	
	        <tr style="display:none;">
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->Keterangan; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <?php
			if($header->status==0)
			{
			?>
		        <tr style="display:none;">
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->Status==1){ echo "selected='selected'"; } ?> value="1">Close</option>
		            		<option <?php if($header->Status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
			<?php
			}
			else
			{
			?>
		        <tr style="display:none;">
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            <?php
		            if($header->Status==1)
		            {
						echo "<b>Close</b>";
					}
					else if($header->Status==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr> 
			<?php
			}
			?>
            
	           <tr>
	        	<td colspan="100%">
				
				
			<div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="entypo-list"></i> Purchase Invoice Detail</a></li>
                <li><a href="#tab2" data-toggle="tab"><i class="entypo-pencil"></i> Purchase Invoice Akun</a></li>
              </ul>
              <div class="tab-content">
			  <div class="tab-pane fade in active" id="tab1">
		            <div class="table-responsive">
				
				
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="300"><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty RG</center></th>
							    <th width="30"><center>Qty</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>PPn (%)</center></th>
							    <th width="100"><center>Sub Total</center></th>
							</tr>
						</thead>
						<tbody>
						<input type="hidden" name="grdTotal" id="grdTotal" value=""/>
						  <?php 
						  $Sid=1;
						  foreach($detail_list as $val)
						  {?>
							
							  <tr>
								<td align="left"><?php echo $val["NamaBarang"]; ?><input type="hidden" name="v_nmbarang[]" value="<?php echo $val["NamaBarang"]; ?>"></td>
								<td align="center"><?php echo $val["Qty"]; ?></td>
								<td align="right"><input readonly style="text-align: right; width: 60px;" type="text" class="form-control-new" dir="rtl" name="v_Qty[]" id="v_Qty_<?php echo $Sid; ?>" value="<?php echo $val["Qty"]; ?>" ></td>
                                <td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal, total dan grand total." name="v_Harga[]" id="v_Harga_<?php echo $Sid; ?>" value="<?php echo $val["HargaSatuan"]; ?>" onkeydown="HitungHarga(event, 'harga', this);" dir="rtl"></td>
							  	<td align="right"><input style="text-align: right; width: 50px;" type="text" class="form-control-new" dir="rtl" name="v_PPn[]" id="v_PPn_<?php echo $Sid; ?>" value="<?php echo $val["PPN"]; ?>"></td>
							  	<td align="right"><input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_subtotal[]" id="v_subtotal_<?php echo $Sid; ?>" dir="rtl" value="<?php echo $val["Jumlah"]; ?>" ></td>
								<td style="display: none"><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $Sid; ?>" dir="rtl" value="<?php echo $val['Total'] ?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
							  </tr>							
							<?php  
							//style="display: none"
							$Sid++; } ?>
						</tbody>	
						
						   <tr style="color: black; font-weight: bold;">
                                <td colspan="4" rowspan="3">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL
                                
                                </td>
                                <td style="text-align: right;"><input dir="rtl" readonly style="text-align: right;" class="form-control-new" type="text" name="v_Jumlah" id="v_Jumlah" value="<?php echo $header->Jumlah;?>"></td>
                            </tr>
                            
                            <!--<tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC<!--<input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" onchange="pickThis4(this)" value="<?php echo number_format($header->DiscHarga,0);?>" > (%)--></td>
                                <!--<td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_pot_disc" id="v_pot_disc" value="<?php echo number_format($header->Diskon,0);?>"></td>
                            </tr>-->
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN<!-- <input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn_" id="v_PPn_" value="<?php echo number_format($header->PPN,0);?>" onchange="pickThis5(this)"> (%) --></td>
                                <td style="text-align: right;">
                                    <input readonly dir="rtl" style="text-align: right;" class="form-control-new" type="text" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo $header->NilaiPPn;?>">
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                </td>
                                <td style="text-align: right;">
                                    <input readonly dir="rtl" style="text-align: right;" class="form-control-new" type="text" name="v_Total" id="v_Total" value="<?php echo $header->Total;?>">
                                </td>
                            </tr>
											
					</table>
	        	
				</div>
			    </div>
				
				
				<div class="tab-pane body fade" id="tab2">
		            <div class="table-responsive">
					
					
					<?php
			if(!empty($header->EditUser_))
			{
			?>
			<table>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="196"><center>No Rekening</center></th>
							    <th width="296"><center>Nama Rekening</center></th>
							    <th width="220"><center>SubDivisi</center></th>
								<th width="335"><center>Deskripsi<center>
							    <th width="150"><center>Jumlah<center>
							    <th width="30"><center> 
								        <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete All" title="" onclick="deleteDetailEdit('<?php echo $header->NoDokumen; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
								</center></th>							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$i=1;
						foreach($detail_list_akun as $val)
						{
							?>
							<tr>
								<td align="center"><?php echo $val["KdRekening"]; ?></td>
								<td><?php echo $val["NamaRekening"]; ?></td>
								<td align="left"><?php echo $val["KdSubDivisi"]." :: ".$val["NamaSubDivisi"]; ?></td>
								<td align="left"><?php echo $val["Deskripsi"]; ?></td>
								<td align="right"><?php echo $val["Jumlah"]; ?></td>
								
							    	<td align="center">
					                	<!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail('<?php echo $val["sid"]; ?>','<?php echo $val["PCode"]; ?>','<?php echo $header->dono; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>-->
					                </td>
				                
							</tr>
							<?php
							$i++;
						}
						
						?>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        </table>
	        <?php
	        }
	        ?>
				
				 <?php if(!empty($header->EditUser_))
			{
			?>
					<table class="table table-bordered responsive" id="TabelDetail2">
        				<thead class="title_table">
							<tr>
								<th width="200"><center>No Rekening</center></th>
							    <th width="200"><center>Nama Rekening</center></th>
							    <th width="200"><center>SubDivisi</center></th>
								<th><center>Deskripsi<center>
							    <th width="80"><center>Jumlah<center>
								</th>
								<th width="30"><center>
									    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
												<i class="entypo-plus"></i>
											</button><center>
								</th>
							</tr>
						</thead>
						<tbody>
						
						<?php $no_=1; ?>
									
						  <tr id="baris<?php echo $no_; ?>">
						    <td>
							<input type="text" class="form-control-new" size="25" name="v_kd_rek[]" id="v_kd_rek<?php echo $no_;?>">
							<a href="javascript:void(0)" id="get_norek<?php echo $no_;?>" onclick="pickThis3(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
							</td>
		                    <td>
							<input type="text" class="form-control-new" size="50" name="v_nm_rek[]" id="v_nm_rek<?php echo $no_;?>">
							</td>
							<td>
										<select class="form-control-new" name="v_subdivisi[]" id="v_subdivisi<?php echo $no_;?>" style="width: 200px;">
						            		<option value="">Pilih Subdivisi</option>
						            		<?php
						            		foreach($subdivisi as $val2)
						            		{
						            			$selected="";
												if($val["KdSubDivisi"]==$val2["KdSubdivisi"])
												{
													$selected='selected="selected"';
												}
												?><option <?php echo $selected; ?> value="<?php echo $val2["KdSubdivisi"]; ?>"><?php echo $val2["NamaSubDivisi"]; ?></option><?php
											}
						            		?>
						            	</select> 
									</td>
							<td>
							<input type="text" size="58" class="form-control-new" name="v_deskripsi[]" id="v_deskripsi<?php echo $no_;?>">
							</td>
		                    <td>
							<input style="text-align: right;" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan total nilai rekening." id="v_jml<?php echo $no_;?>" name="v_jml[]" class="form-control-new" type="text" onkeydown="HitungHarga2(event, 'harga', this);" dir="rtl">
		                    </td>
							<td align="center">
						                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_target_<?php echo $no_;?>]" id="btn_del_detail_target_<?php echo $no_;?>" value="Save" onclick='deleteRow(this)'>
												<i class="entypo-trash"></i>
											</button>
						    </td>
		                  </tr>
						
						</tbody>										
									
					</table>
					<?php
					}
					?>
					<table>
					<tr style="color: black; font-weight: bold;">
                                <td width="1005">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL &nbsp;&nbsp;&nbsp;
                                
                                </td>
                                <td style="text-align: right;"><input dir="rtl" readonly style="text-align: right;" class="form-control-new" type="text" name="v_Total_rek" id="v_Total_rek" value="<?php echo $header_->total_rek;?>"></td>
                            </tr>
                    
                    <tr style="color: black; font-weight: bold;">
                                <td width="1005">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                LOCK&nbsp;&nbsp;&nbsp;
                                
                                </td>
                                <td style="text-align: right;"><input dir="rtl" readonly style="text-align: right;" class="form-control-new" type="text" name="v_Lock_Total" id="v_Lock_Total" value="<?php echo ROUND($KunciTotal->Jumlah);?>"></td>
                            </tr>
					</table>
				</div>
			    </div>
			
				
			
			
			</div>
			</div>
			</td>
	        </tr>
	       
				
		   <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/pi_marketing/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheformEditJurnal();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
			
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->NoDokumen)
        {
        ?>
   			<ol class="breadcrumb title_table" style="display:;">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive" style="display:; ">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->AddUser_." :: ".$header->AddDate_; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->EditUser_." :: ".$header->EditDate_; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
         
	</div>
</div>
 
<div style="display: none;">   	
<?php $this->load->view('footer'); ?>
</div>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	function pickThis3(obj)
{
	    base_url = $("#base_url").val();
		objek = obj.id;
		id = parseFloat(objek.substr(9,objek.length-9));
		url = base_url+"index.php/pop/pop_up_no_rek/index/0/"+id+"/";
		windowOpener(525, 600, 'Cari List Rekening', url, 'Cari List Rekening')
	
}

function deleteDetailEdit(nodok,url)
{
	var r=confirm("Apakah Anda Ingin Menghapus No Rekening tersebut dan mengulang kembali?")
	if (r==true)
	{
		window.location = url+"index.php/transaksi/pi_marketing/delete_detail_edit_jurnal/"+nodok+"";	
	}
	else
	{
  		return false;
	}
}

function cekTheformEditJurnal()
{
	Total_rek	= parseInt($('#v_Total_rek').val());
	Lock_Total 	= parseInt($('#v_Lock_Total').val());
	
	if(Total_rek<Lock_Total)
	{
		alert("Total Rekening Masih Kurang Dengan Totalan Kunci");
		return false;
	}
	else if(Total_rek>Lock_Total)
	{
		alert("Total Rekening Melebihi Dengan Totalan Kunci");
		return false;
	}
    else if(Total_rek==Lock_Total)
    {
	
    	var yesSubmit = true;
    	
        
        if(yesSubmit)
        {
			document.getElementById("theform").submit();	
		}  
	}
}
</script>