<span id="otorisasi" style="display: none">
<b>Otorisasi Supervisor</b><br><br>
<input class="form-control-new" placeholder="username" type='text' name='username' id='username' value=''>
<input class="form-control-new" placeholder="password" type='password' name='password' id='password' value=''>
<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" onClick="order_cancel(this.form)";>Approve<i class="entypo-check"></i></button>
<br><br>
</span>
<span id="otorisasi2" style="display: none">
<b>Otorisasi Supervisor</b><br><br>
<input class="form-control-new" placeholder="username" type='text' name='username2' id='username2' value=''>
<input class="form-control-new" placeholder="password" type='password' name='password2' id='password2' value=''>
<button type="button" class="btn btn-danger btn-icon btn-sm icon-left cancelmenu">Approve<i class="entypo-check"></i></button>
<br><br>
</span>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Person Form</h3>
            </div>
            <div class="modal-body form">

                    <input id="notrans" type="hidden" value="" name="NoTrans"/>
                    <input id="pcode" type="hidden" value="" name="PCode"/>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9">
                                <input id="username" name="username" placeholder="Username" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-9">
                                <input id="password" name="password" placeholder="Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="order_cancel(this.form)" class="btn btn-info">Confirm</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<!--<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="30" ><center>No</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="30"><center>Meja</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="80"><center>Start</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="100"><center>Waiters</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;"><center>Item</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="30"><center>Qty</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="30"><center>Weight</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="80"><center>Done</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="80"><center>Durasi</center></th>
                <th colspan="2" style="vertical-align: middle; text-align: center;"><center>Action</center></th>

            </tr>
            <tr>
                <th width="30"><center>
                <button type="button" class="btn btn-green btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Complete" title="Complete" onClick="displayAlert()" >
                                        <i class="entypo-check"></i>
                                    </button>
                </center></th>

                <th width="30">
                    <center>
                <button type="button" class="btn btn-red btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Cancel" title="Cancel" onClick="cancel_all()" >
                                        <i class="entypo-cancel"></i>
                                    </button>
                </center></th>


            </tr>
            <tr>
              <th colspan="2" style="vertical-align: middle; text-align: center;"><center>
                <input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'checklist[]')">
              </center></th>
            </tr>

        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f0f0f0;";
                }

                ?>
                <tr title="<?php echo $val["NamaLengkap"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><h4><b><?php echo $no; ?></b></h4></td>
                    <td align="center"><h4><b><?php echo $val["KdMeja"]; ?></b></h4></td>
                    <td align="center"><h4><b><?php echo $val["waktu_start"]; ?></b></h4></td>
                    <td align="center"><h4><b><?php echo $val["KdPersonal"]; ?></b></h4></td>
                    <td align="left"><h4><b><?php echo $val["NamaLengkap"]; ?><input type="hidden" name="nama_menu[]" id="nama_menu<?=$no;?>" value="<?php echo $val["NamaLengkap"]; ?>">
                    </b></h4></td>
                    <td align="center"><h4><b><?php echo $val["Qty"]; ?></b></h4></td>
                    <td align="center"><h4><b>
                        <input style="text-align: right;" type="text" class="form-control" size="3" name="berat[]" id="berat<?=$val["PCode"].$val["KdMeja"];?>" value="<?php echo $val["Berat"]; ?>">
                    </b></h4></td>
                    <td align="center"><h4><b><?php echo $val["waktu_finish"]; ?></b></h4></td>
                    <td align="center"><h4><b><?php echo $val["durasi"]; ?></b></h4></td>
                    <td colspan="2" align="center">

                        <?php


                        if ($val["status_detail"] == 0) {
                                if ($val["status_header"] == 0) {
                                    $uri = $val["NoTrans"]."#".$val["PCode"]."#".$this->uri->segment(4)."#".$val["KdMeja"]."#".$no."#".$val["waktu_start"]."#".$val["Berat"];
                                    ?>
                                    <!-- <?php echo $val["NoTrans"]."#".$val["PCode"]."#".$this->uri->segment(4)."#".$val["KdMeja"]."#".$no."#".$val["waktu_start"]."#".$val["Berat"] ?> -->
                                    <!--<input type="checkbox" name="checklist[]" id="checklist<?=$no;?>" value='<?=$uri;?>' >
                                    <?php

                                }
                        } else if ($val["status_detail"] == 1) {
                            echo "<button type='button' class='btn btn-blue btn-md md-new tooltip-primary'><i class='entypo-check'></i>";?>

                            <button type="button" class="btn btn-orange btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Complete" title="Complete" onClick="batal('<?php echo $val["NoTrans"];?>','<?php echo $val["PCode"];?>','<?php echo $val["KdMeja"];?>')" >
                                        <i class="entypo-pencil"></i>
                                    </button>

                        <?php
                        } else if ($val["status_detail"] == 2) {
                            echo "<button type='button' class='btn btn-orange btn-md md-new tooltip-primary'><i class='entypo-cancel'></i>";
                        }




                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>
            <tr>
            <td colspan='100%' align='center'>
            <?php if($complete==1){ ?>
            <!--<button type="button" class="btn btn-orange btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Done" title="" onclick="done('<?php echo $val["KdMeja"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-check"></i><b>Done</b>
                            </button>-->
        <!--<?php } ?>
            </td>
            </tr>

        </tbody>

    </table>



    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>

</div>-->

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="10" ><center>No</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="20"><center>PCode</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;"><center>Nama Menu</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="100"><center>Stock</center></th>
                <!--<th rowspan="3" style="vertical-align: middle; text-align: center;"><center>Item</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="30"><center>Qty</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="30"><center>Weight</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="80"><center>Done</center></th>
                <th rowspan="3" style="vertical-align: middle; text-align: center;" width="80"><center>Durasi</center></th>
                <th colspan="2" style="vertical-align: middle; text-align: center;"><center>Action</center></th>-->

            </tr>
            <!--<tr>
                <th width="30"><center>
                <button type="button" class="btn btn-green btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Complete" title="Complete" onClick="displayAlert()" >
                                        <i class="entypo-check"></i>
                                    </button>
                </center></th>

                <th width="30">
                    <center>
                <button type="button" class="btn btn-red btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Cancel" title="Cancel" onClick="cancel_all()" >
                                        <i class="entypo-cancel"></i>
                                    </button>
                </center></th>


            </tr>
            <tr>
              <th colspan="2" style="vertical-align: middle; text-align: center;"><center>
                <input type="checkbox" id="chkall" onclick="CheckAll('chkall', 'checklist[]')">
              </center></th>
            </tr>-->

        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f0f0f0;";
                }
                
                if($val["FlagReady"]=="Y" AND $val["FlagStock"]=="Y"){
					$sts="<font color='white'>Yes</font>";
					$color="green";
					$stss="Y";
				}else{
					$sts="<font color='white'>No</font>";
					$color="red";
					$stss="N";
				}

                ?>
                <tr title="<?php echo $val["NamaLengkap"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td align="center"><h4><b><?php echo $no; ?></b></h4></td>
                    <td align="left"><h4><b><?php echo $val["PCode"]; ?></b></h4></td>
                    <td align="left"><h4><b><?php echo $val["NamaLengkap"]; ?></b></h4></td>
                    <td align="center" onClick="edit_stock_ready('<?=$val["PCode"];?>','<?=$stss;?>')" bgcolor="<?=$color;?>"><h4><b><?php echo $sts; ?></b></h4></td>
                    <!--<td align="left"><h4><b><?php echo $val["NamaLengkap"]; ?><input type="hidden" name="nama_menu[]" id="nama_menu<?=$no;?>" value="<?php echo $val["NamaLengkap"]; ?>">
                    </b></h4></td>
                    <td align="center"><h4><b><?php echo $val["Qty"]; ?></b></h4></td>
                    <td align="center"><h4><b>
                        <input style="text-align: right;" type="text" class="form-control" size="3" name="berat[]" id="berat<?=$val["PCode"].$val["KdMeja"];?>" value="<?php echo $val["Berat"]; ?>">
                    </b></h4></td>
                    <td align="center"><h4><b><?php echo $val["waktu_finish"]; ?></b></h4></td>
                    <td align="center"><h4><b><?php echo $val["durasi"]; ?></b></h4></td>
                    <td colspan="2" align="center">

                        <?php


                        if ($val["status_detail"] == 0) {
                                if ($val["status_header"] == 0) {
                                    $uri = $val["NoTrans"]."#".$val["PCode"]."#".$this->uri->segment(4)."#".$val["KdMeja"]."#".$no."#".$val["waktu_start"]."#".$val["Berat"];
                                    ?>
                                    <!-- <?php echo $val["NoTrans"]."#".$val["PCode"]."#".$this->uri->segment(4)."#".$val["KdMeja"]."#".$no."#".$val["waktu_start"]."#".$val["Berat"] ?> -->
                                    <!--<input type="checkbox" name="checklist[]" id="checklist<?=$no;?>" value='<?=$uri;?>' >
                                    <?php

                                }
                        } else if ($val["status_detail"] == 1) {
                            echo "<button type='button' class='btn btn-blue btn-md md-new tooltip-primary'><i class='entypo-check'></i>";?>

                            <button type="button" class="btn btn-orange btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Complete" title="Complete" onClick="batal('<?php echo $val["NoTrans"];?>','<?php echo $val["PCode"];?>','<?php echo $val["KdMeja"];?>')" >
                                        <i class="entypo-pencil"></i>
                                    </button>

                        <?php
                        } else if ($val["status_detail"] == 2) {
                            echo "<button type='button' class='btn btn-orange btn-md md-new tooltip-primary'><i class='entypo-cancel'></i>";
                        }




                        ?>

                    </td>-->
                </tr>
                <?php
                $no++;
            }
            ?>
            <tr>
            <td colspan='100%' align='center'>
            <?php if($complete==1){ ?>
            <!--<button type="button" class="btn btn-orange btn-md md-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Done" title="" onclick="done('<?php echo $val["KdMeja"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-check"></i><b>Done</b>
                            </button>-->
        <?php } ?>
            </td>
            </tr>

        </tbody>

    </table>



    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>

</div>
