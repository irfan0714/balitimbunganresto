<?php $this->load->view('header'); ?>
<meta http-equiv="refresh" content="300" />
<head>

</head>

<link rel="stylesheet" href="<?= base_url(); ?>public/jquery_confirm/dist/jquery-confirm.min.css">
<script src="<?= base_url(); ?>public/jquery_confirm/dist/jquery-confirm.min.js"></script>
<script language="javascript" src="<?= base_url(); ?>public/js/pending_order.js"></script>

<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/cek_ready_menu/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="Y"/>
    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
    <div class="row">
        <div class="col-md-8">

            &nbsp;<b>Menu Item </b>&nbsp;
            <input type="text" class="form-control-new" name="search_meja" id="search_meja" onkeyup="cari_menu()"/>
        
        	&nbsp;<b>Type </b>&nbsp;
            <b><select class="form-control-new" name="search_type" id="search_type" value="<?=$search_type;?>"  style="width: 20%;" onchange="pilih_menu()">
                <option value="0">All</option>
                <option value="2">Food</option>
                <option value="1">Baverages</option>
                
            </select></b>
            
            &nbsp;<b>Group Food </b>&nbsp;
            <b><select class="form-control-new" name="search_group" id="search_group" style="width: 20%;" onchange="pilih_group()">
                <option value="0">All</option>
                <?php
                foreach ($mmeja as $val) {
                    $selected = "";
                    if ($search_meja) {
                        if ($val["KdSubKategori"] == $search_type) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdSubKategori"]; ?>"><?php echo $val["NamaSubKategori"]; ?></option><?php
                }
                ?>
            </select></b>

            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <!--<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>-->
            <!--<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="pilih_meja()";>Search<i class="entypo-search"></i></button>-->
            <button type="button" class="btn btn-info btn-icon btn-sm icon-left" onClick="pilih_meja()";>Refresh<i class="entypo-search"></i></button>
            <!--<button type="button" class="btn btn-primary btn-icon btn-sm icon-left" onkeyup="cari_menu()";>Change The Table<i class="entypo-cancel"></i></button>
            <button type="button" class="btn btn-warning btn-icon btn-sm icon-left" onClick="change_sticker()";>Change Sticker<i class="entypo-cancel"></i></button>
            <button type="button" class="btn btn-green btn-icon btn-sm icon-left" id="coba" onClick="coba_test()";>Coba<i class="entypo-search"></i></button>-->

        </div>
    </div>
</form>
<br>
<hr>
<!--<button class="btn btn-green btn-icon btn-sm icon-left" onClick="refresh_halaman()";>Refresh<i class="entypo-search"></i></button>-->

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div>
    <script>//alert('Perbaharui Halaman...');
    //refresh_halaman()
    </script>
    <?php
}
?>

<span id="changetable" style="display: none">
<b>Change The Table</b><br><br>
<b>From</b> &nbsp;&nbsp;&nbsp;
            <select class="form-control-new" name="meja_from" id="meja_from" style="width: 5%;">
                <option value=""> -- Pilih Meja -- </option>
                <?php
                foreach ($mmeja as $val) {
                    $selected = "";
                    if ($search_meja) {
                        if ($val["KdMeja"] == $search_meja) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdMeja"]; ?>"><?php echo $val["KdMeja"]; ?></option><?php
                }
                ?>
            </select>
&nbsp;&nbsp;&nbsp; <b>To</b> &nbsp;&nbsp;&nbsp;
<select class="form-control-new" name="meja_to" id="meja_to" style="width: 5%;">
                <option value=""> -- Pilih Meja -- </option>
                <?php
                foreach ($allmeja as $vals) {
                    ?>
                    <option value="<?php echo $vals["meja"]; ?>"><?php echo $vals["meja"]; ?></option>
                    <?php
                }
                ?>
            </select>&nbsp;&nbsp;&nbsp;
<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" onClick="changes_tables()";>Change Table<i class="entypo-check"></i></button>
<br><br>
</span>


<span id="changesticker" style="display: none">
<b>Change Sticker Number</b><br><br>
<b>From</b> &nbsp;&nbsp;&nbsp;
<input class="form-control-new" placeholder="Sticker Lama" type='text' name='sticker_lama' id='sticker_lama' value=''>
&nbsp;&nbsp;&nbsp; <b>To</b> &nbsp;&nbsp;&nbsp;
<input class="form-control-new" placeholder="Sticker Baru" type='text' name='sticker_baru' id='sticker_baru' value=''>
&nbsp;&nbsp;&nbsp;
<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" onClick="changes_stickers()";>Change Sticker<i class="entypo-check"></i></button>
<br><br>
</span>

<div id="getList"></div>

<div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>Proses...</h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php $this->load->view('footer'); ?>

<script>

	$(document).ready(function()
		{
			getData();
		});

	

	function getData(){
		base_url = $("#baseurl").val();
		keyword = $('#search_meja').val();//meja

			//$('#pleaseWaitDialog').modal('show');

	    	$.ajax({
				type: "POST",
				url: base_url + "index.php/transaksi/cek_ready_menu/getList/"+keyword,
				success: function(data) {
					$('#getList').html(data);

						//$('#pleaseWaitDialog').modal('hide');

				}
			});
	}

	function refresh_halaman(){
		location.reload();
	}
	
	function edit_stock_ready(pcode,sts){
										
										if(sts=="N"){
											sts="Y";
										}else{
											sts="T";
										}
										//$('#pleaseWaitDialog').modal('show');
	                                    $.ajax({
								            url: "<?php echo site_url('transaksi/cek_ready_menu/updateStock') ?>/" +pcode+"/"+sts,
								            type: "GET",
								            dataType: "JSON",
								            success: function (data)
								            {
								                  if(data){
												  	getData();
												  	//$('#pleaseWaitDialog').modal('hide');
												  	//$.alert('Confirmed!');
												  	/*$.alert({
												        title: 'Alert!',
												        content: 'Successfully',
												    });*/
												  }
								            }
								            ,
								            error: function (textStatus, errorThrown)
								            {
								                    getData();
								                    //$('#pleaseWaitDialog').modal('hide');
								                    $.alert({
												        title: 'Alert!',
												        content: 'Error',
												    });
								            }
								        }
								        );
	}

  function CheckAll(param, target)
		{
			var field = document.getElementsByName(target);
			var chkall = document.getElementById(param);
			if (chkall.checked == true)
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = true ;
			}else
			{
				for (i = 0; i < field.length; i++)
				field[i].checked = false ;
			}
		}

		function displayAlert(){

		var chks = document.getElementsByName('checklist[]');
		var hasChecked = false;

    // alert(chks);

		kum_pcode="";
		kum_berat="";
		kum_nourut ="";
		kum_waktu="";
		for (index = 0; index < chks.length; index++)
		{
			if (chks[index].checked)
			{
			indexs = index - 1;
	        nama = document.getElementsByName("checklist[]");
			temp = nama[index].id;
	        menupilihan = nama[index].value;
			hasChecked = true;
			//break;

			     pisah = menupilihan.split('#');
				   notrans = pisah[0];
				   pcode   = pisah[1];
				   src     = pisah[2];
				   meja    = pisah[3];
				   no_urut  = pisah[4];
				   waktu    = pisah[5];

				   berat = parseInt($('#berat'+(pcode+meja)).val());
				   // berat   = pisah[6];
				   kum_pcode+=pcode+"-";
				   kum_berat+=berat+"-";
				   kum_nourut+=no_urut+"-";
				   kum_waktu+=waktu+"-";
			}
		}

			if (hasChecked == true)
			{  //cek ada berat ga
        // alert(pcode);
			  if(parseInt(berat)*1>0){
			  	jmlbrs = chks.length;
          // alert(jmlbrs);
			  			$.confirm({
						    title: 'Pengingat!!',
						    content: 'Apakah Mau Input Berat??',
						        buttons: {
						        Ya: {
						            text: 'Ya',
						            btnClass: 'btn-warning',
							        action: function () {
							            //jika Ya
							            for (index = 0; index < jmlbrs; index++)
										{
											chks[index].checked = false;
										}
							            $('#berat'+(pcode+meja)).focus();
							        }
						        },
						        Tidak: {
						            text: 'Tidak',
						            btnClass: 'btn-red',
						            action: function () {
						            	for (index = 0; index < jmlbrs; index++)
										{
											chks[index].checked = false;
										}

						            	$.alert('Pastikan Makanan Dimeja Tersebut Sudah Keluar.');
						            }
						        },
						        Lanjut: {
						            text: 'Lanjut',
						            btnClass: 'btn-green',
						            action: function () {
						            	//Jika Lanjut
						                $('#pleaseWaitDialog').modal('show');
	                                    $.ajax({
								            url: "<?php echo site_url('transaksi/cek_ready_menu/completenew') ?>/" +notrans+"/"+kum_pcode+"/"+chks.length+"/"+kum_berat+"/"+kum_waktu+"/"+meja,
								            type: "GET",
								            dataType: "JSON",
								            success: function (data)
								            {
								                  if(data){
												  	getData();
												  	$('#pleaseWaitDialog').modal('hide');
												  	//$.alert('Confirmed!');
												  }
								            }
								            ,
								            error: function (textStatus, errorThrown)
								            {
								                    getData();
								                    $('#pleaseWaitDialog').modal('hide');
								                    /*$.alert({
												        title: 'Alert!',
												        content: 'Error Get Data',
												    });*/
								            }
								        }
								        );
						            }
						        }
						    }
						});

			    }else{

			    	jmlbrs = chks.length;
			    	$.confirm({
						    title: 'Pengingat!!',
						    content: 'Apakah Mau Input Berat?',
						        buttons: {
						        Ya: {
						            text: 'Ya',
						            btnClass: 'btn-warning',
							        action: function () {
							            //jika Ya
							            for (index = 0; index < jmlbrs; index++)
										{
											chks[index].checked = false;
										}
							            $('#berat'+(pcode+meja)).focus();
							        }
						        },
						        Tidak: {
						            text: 'Tidak',
						            btnClass: 'btn-red',
						            action: function () {
						            	for (index = 0; index < jmlbrs; index++)
										{
											chks[index].checked = false;
										}

						            	$.alert('Pastikan Makanan Dimeja Tersebut Sudah Keluar.');
						            }
						        },
						        Lanjut: {
						            text: 'Lanjut',
						            btnClass: 'btn-green',
						            action: function () {
						            	//Jika Lanjut
						                $('#pleaseWaitDialog').modal('show');
	                                    $.ajax({
								            url: "<?php echo site_url('transaksi/cek_ready_menu/completenew') ?>/" +notrans+"/"+kum_pcode+"/"+chks.length+"/"+kum_berat+"/"+kum_waktu+"/"+meja,
								            type: "GET",
								            dataType: "JSON",
								            success: function (data)
								            {
								                  if(data){
												  	getData();
												  	$('#pleaseWaitDialog').modal('hide');
												  	//$.alert('Confirmed!');
												  }
								            }
								            ,
								            error: function (textStatus, errorThrown)
								            {
								                    getData();
								                    $('#pleaseWaitDialog').modal('hide');
								                    /*$.alert({
												        title: 'Alert!',
												        content: 'Error Get Data',
												    });*/
								            }
								        }
								        );
						            }
						        }
						    }
						});


			    }


	        }

		if (hasChecked == false)
			{
			                         $.alert({
								        title: 'Alert!',
								        content: 'Belum Ada Yang Di Checklist',
								    });
			return false;
			}

		}

		
		function testing(a){
			alert(a);
		}


		function batal(notrans,pcode,meja){
                  var corporate = "<?php echo $this->session->userdata('CompanyID');?>";

                  // alert(pcode);

                  // break;
                  if(corporate == '0'){
                    document.getElementById("otorisasi2").style.display = "";
                    $('.cancelmenu').attr("onclick", "CancelMenu("+notrans+",'"+pcode+"','"+meja+"');");
              			// document.getElementById("tombol").style.display = "none";
                  }else{

                    $('#pleaseWaitDialog').modal('show');

                   $.ajax({
                           url: "<?php echo site_url('transaksi/cek_ready_menu/buka_menu') ?>/" +notrans+"/"+pcode+"/"+meja,
                           type: "GET",
                           dataType: "JSON",
                           success: function (data)
                           {
                                 if(data){
                   		  	getData();
                   		  	$('#pleaseWaitDialog').modal('hide');
                   		  	//$.alert('Confirmed!');
                   		  }
                           }
                           ,
                           error: function (textStatus, errorThrown)
                           {
                                   getData();
                                   $('#pleaseWaitDialog').modal('hide');
                                   /*$.alert({
                   		        title: 'Alert!',
                   		        content: 'Error Get Data',
                   		    });*/
                           }
                       }
                       );

                  }


		}


    // cancel menu 2
    function CancelMenu(notrans,pcode11,meja){

    var pcode1 = pcode11;

    // alert(pcode1);
    var user=$("#username2").val();
    var password=$("#password2").val();

       if(user==""){
                        $.alert({
                        title: 'Alert!',
                        content: 'Username Harus Diisi',
                    });
      $("#username2").focus();
      return false;
     }else if(password==""){
                                  $.alert({
                        title: 'Alert!',
                        content: 'Password Harus Diisi',
                    });
      $("#password2").focus();
      return false;
     }

     $('#pleaseWaitDialog').modal('show');

     $.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/buka_menu2') ?>/" +notrans+"/"+pcode1+"/"+meja+"/"+user+"/"+password,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {
                  if(data.status == '1'){
                   getData();
                   $('#pleaseWaitDialog').modal('hide');
                   //$.alert('Confirmed!');
                   $.alert({
                   title: 'Alert!',
                   content: 'Username atau Password Salah',
                   });
                 }else if(data.status == '0'){
                   getData();
                   $('#pleaseWaitDialog').modal('hide');

                   $.alert({
                   title: 'Alert!',
                   content: 'Anda Tidak Memiliki Otorisasi',
                   });
                 }else{
                   getData();
                   $('#pleaseWaitDialog').modal('hide');
                   document.getElementById("otorisasi2").style.display = "none";

                   $.alert({
                   title: 'Alert!',
                   content: 'Berhasil Batalkan Menu',
                   });

                   // location.reload();
                 }
            },
            error: function (textStatus, errorThrown)
            {
                    getData();
                    $('#pleaseWaitDialog').modal('hide');

                    /*$.alert({
               title: 'Alert!',
               content: 'Error Get Data',
           });*/
            }
        });
    // var chks = document.getElementsByName('checklist[]');
    // var hasChecked = false;
    // kum_pcode="";
    // for (index = 0; index < chks.length; index++)
    // {
    //   if (chks[index].checked)
    //   {
    //   indexs = index - 1;
    //       nama = document.getElementsByName("checklist[]");
    //   temp = nama[index].id;
    //       menupilihan = nama[index].value;
    //   hasChecked = true;
    //   //break;
    //
    //        pisah = menupilihan.split('#');
    //        notrans = pisah[0];
    //        pcode   = pisah[1];
    //        src     = pisah[2];
    //        kum_pcode+=pcode+"-";
    //   }
    // }

    if (hasChecked == true)
      {

            $('#pleaseWaitDialog').modal('show');
            $.ajax({
                    url: "<?php echo site_url('transaksi/cek_ready_menu/cancel_ordernew') ?>/" +user+"/"+password+"/"+notrans+"/"+kum_pcode+"/"+chks.length,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                          if(data.status=='1'){


                            //refresh meja aktif
                            $.ajax({
                          url: "<?php echo site_url('transaksi/cek_ready_menu/getMejaAktif') ?>/",
                          type: "GET",
                          dataType: "html",
                          success: function (data)
                          {
                             $('#search_meja').html(data);

                          }
                          ,
                          error: function (textStatus, errorThrown)
                          {
                               $.alert({
                            title: 'Alert!',
                            content: 'Error Get Data',
                        });
                          }
                      }
                      );


                    getData();
                    $('#pleaseWaitDialog').modal('hide');
                  }else if(data.status=='2'){
                    $('#pleaseWaitDialog').modal('hide');
                    $.alert({
                        title: 'Alert!',
                        content: 'Password Anda Salah.',
                    });
                    document.getElementById("otorisasi").style.display = "none";
                    $('#password').val('');
                  } else if(data.status=='3'){
                    $('#pleaseWaitDialog').modal('hide');
                    $.alert({
                        title: 'Alert!',
                        content: 'Anda Tidak Punya Otorisasi.',
                    });

                      //unchacked
                      for (index = 0; index < chks.length; index++)
                    {
                      indexs = index - 1;
                          nama = document.getElementsByName("checklist[]");
                      temp = nama[index].id;
                          menupilihan = nama[index].checked = false;
                    }

                    document.getElementById("otorisasi").style.display = "none";
                    $('#username').val('');
                    $('#password').val('');
                  }

                    }
                    ,
                    error: function (textStatus, errorThrown)
                    {
                        $.alert({
                        title: 'Alert!',
                        content: 'Error Get Data',
                    });
                    }
                }
                );

          }

    if (hasChecked == false)
      {
      $.alert({
              title: 'Alert!',
              content: 'Belum Ada Yang Di Checklist.',
          });
      return false;
      }

    }
    // cancel menu 2



		function order_cancel(){

		var user=$("#username").val();
		var password=$("#password").val();

       if(user==""){
	   	                            $.alert({
								        title: 'Alert!',
								        content: 'Username Harus Diisi',
								    });
	   	$("#username").focus();
	   	return false;
	   }else if(password==""){
	   	                            $.alert({
								        title: 'Alert!',
								        content: 'Password Harus Diisi',
								    });
	   	$("#password").focus();
	   	return false;
	   }

       var chks = document.getElementsByName('checklist[]');
		var hasChecked = false;
		kum_pcode="";
		for (index = 0; index < chks.length; index++)
		{
			if (chks[index].checked)
			{
			indexs = index - 1;
	        nama = document.getElementsByName("checklist[]");
			temp = nama[index].id;
	        menupilihan = nama[index].value;
			hasChecked = true;
			//break;

			     pisah = menupilihan.split('#');
				   notrans = pisah[0];
				   pcode   = pisah[1];
				   src     = pisah[2];
				   kum_pcode+=pcode+"-";
			}
		}

		if (hasChecked == true)
			{

						$('#pleaseWaitDialog').modal('show');
						$.ajax({
				            url: "<?php echo site_url('transaksi/cek_ready_menu/cancel_ordernew') ?>/" +user+"/"+password+"/"+notrans+"/"+kum_pcode+"/"+chks.length,
				            type: "GET",
				            dataType: "JSON",
				            success: function (data)
				            {
				                  if(data.status=='1'){


				                  	//refresh meja aktif
				                  	$.ajax({
							            url: "<?php echo site_url('transaksi/cek_ready_menu/getMejaAktif') ?>/",
							            type: "GET",
							            dataType: "html",
							            success: function (data)
							            {
							               $('#search_meja').html(data);

							            }
							            ,
							            error: function (textStatus, errorThrown)
							            {
							                 $.alert({
										        title: 'Alert!',
										        content: 'Error Get Data',
										    });
							            }
							        }
							        );


								  	getData();
								  	$('#pleaseWaitDialog').modal('hide');
								  }else if(data.status=='2'){
								  	$('#pleaseWaitDialog').modal('hide');
								  	$.alert({
								        title: 'Alert!',
								        content: 'Password Anda Salah.',
								    });
								  	document.getElementById("otorisasi").style.display = "none";
								  	$('#password').val('');
								  } else if(data.status=='3'){
								  	$('#pleaseWaitDialog').modal('hide');
								  	$.alert({
								        title: 'Alert!',
								        content: 'Anda Tidak Punya Otorisasi.',
								    });

									  	//unchacked
									  	for (index = 0; index < chks.length; index++)
										{
											indexs = index - 1;
									        nama = document.getElementsByName("checklist[]");
											temp = nama[index].id;
									        menupilihan = nama[index].checked = false;
										}

								  	document.getElementById("otorisasi").style.display = "none";
								  	$('#username').val('');
								  	$('#password').val('');
								  }

				            }
				            ,
				            error: function (textStatus, errorThrown)
				            {
				                $.alert({
								        title: 'Alert!',
								        content: 'Error Get Data',
								    });
				            }
				        }
				        );

	        }

		if (hasChecked == false)
			{
			$.alert({
			        title: 'Alert!',
			        content: 'Belum Ada Yang Di Checklist.',
			    });
			return false;
			}

		}

		function cancel_all()
		{

			var chks = document.getElementsByName('checklist[]');
		var hasChecked = false;
		for (index = 0; index < chks.length; index++)
		{
			if (chks[index].checked)
			{
			hasChecked = true;

			document.getElementById("otorisasi").style.display = "";
			document.getElementById("tombol").style.display = "none";

			}
		}

		if (hasChecked == false)
			{
			$.alert({
			        title: 'Alert!',
			        content: 'Belum Ada Yang Di Checklist.',
			    });
			return false;
			}

		}

	function pilih_meja(){
		//alert($('#search_meja').val());
		keyword = $('#search_meja').val();//meja

		$('#pleaseWaitDialog').modal('show');

		$.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/getList') ?>/" + keyword,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#meja_from').val(keyword);
               $('#pleaseWaitDialog').modal('hide');
               $("#changetable").css("display","none");
		       $("#changesticker").css("display","none");

            }
            ,
            error: function (textStatus, errorThrown)
            {
                $.alert({
			        title: 'Alert!',
			        content: 'Error Get Data!',
			    });
            }
        }
        );

	}
	
	function cari_menu(){
		//alert($('#search_meja').val());
		keyword = $('#search_meja').val();//meja
		type = $('#search_type').val();//meja
		group = $('#search_group').val();//meja

		if(keyword==""){
			keyword="xxxxxxxxx";
		}
		//$('#pleaseWaitDialog').modal('show');

		$.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/getList') ?>/" + keyword +"/"+type+"/"+group,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#meja_from').val(keyword);
               //$('#pleaseWaitDialog').modal('hide');
               $("#changetable").css("display","none");
		       $("#changesticker").css("display","none");

            }
            ,
            error: function (textStatus, errorThrown)
            {
                $.alert({
			        title: 'Alert!',
			        content: 'Error Get Data!',
			    });
            }
        }
        );

	}
	
	function pilih_menu(){
		//alert($('#search_meja').val());
		keyword = $('#search_meja').val();//meja
		type = $('#search_type').val();//meja
		group = $('#search_group').val();//meja

if(keyword==""){
			keyword="xxxxxxxxx";
		}
		
		//$('#pleaseWaitDialog').modal('show');

		$.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/getList') ?>/" + keyword +"/"+type+"/"+group,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#meja_from').val(keyword);
               //$('#pleaseWaitDialog').modal('hide');
               $("#changetable").css("display","none");
		       $("#changesticker").css("display","none");

            }
            ,
            error: function (textStatus, errorThrown)
            {
                $.alert({
			        title: 'Alert!',
			        content: 'Error Get Data!',
			    });
            }
        }
        );

	}
	
	function pilih_group(){
		//alert($('#search_meja').val());
		keyword = $('#search_meja').val();//meja
		type = $('#search_type').val();//meja
		group = $('#search_group').val();//meja

if(keyword==""){
			keyword="xxxxxxxxx";
		}
		
		//$('#pleaseWaitDialog').modal('show');

		$.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/getList') ?>/" + keyword +"/"+type+"/"+group,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               $('#getList').html(data);
               $('#meja_from').val(keyword);
              // $('#pleaseWaitDialog').modal('hide');
               $("#changetable").css("display","none");
		       $("#changesticker").css("display","none");

            }
            ,
            error: function (textStatus, errorThrown)
            {
                $.alert({
			        title: 'Alert!',
			        content: 'Error Get Data!',
			    });
            }
        }
        );

	}

	function change_table(){

		meja = $('#search_meja').val();
		if(meja==""){
			                        $.alert({
								        title: 'Alert!',
								        content: 'Pilih Meja Terlebih Dahulu...',
								    });
			return false;
		}

		$("#changetable").css("display","");
		$("#changesticker").css("display","none");
	}

	function change_sticker(){

		meja = $('#search_meja').val();
		if(meja==""){
			                        $.alert({
								        title: 'Alert!',
								        content: 'Pilih Meja Terlebih Dahulu...',
								    });
			return false;
		}

		$("#changetable").css("display","none");
		$("#changesticker").css("display","");
	}

	function changes_tables(){
		meja_from = $('#meja_from').val();
		meja_to = $('#meja_to').val();

		if(meja_from==""){
			                        $.alert({
								        title: 'Alert!',
								        content: 'Pilih Meja From Terlebih Dahulu...',
								    });
			return false;
		}

		if(meja_to==""){
									$.alert({
								        title: 'Alert!',
								        content: 'Pilih Meja To Terlebih Dahulu...',
								    });
			return false;
		}

		$('#pleaseWaitDialog').modal('show');

		$.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/change_table') ?>/" + meja_from +"/"+ meja_to,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               if(data){


			   		$.ajax({
			            url: "<?php echo site_url('transaksi/cek_ready_menu/getList') ?>/" + meja_to,
			            type: "GET",
			            dataType: "html",
			            success: function (data)
			            {
			               $('#getList').html(data);
			               $('#meja_from').val(keyword);


			            }
			            ,
			            error: function (textStatus, errorThrown)
			            {
			                $.alert({
						        title: 'Alert!',
						        content: 'Error Get Data!',
						    });
			            }
			        }
			        );

			   	    $('#pleaseWaitDialog').modal('hide');
			   	    $('#meja_from').val('');
		            $('#meja_to').val('');
		            $("#changetable").css("display","none");
		            meja_aktif();
		            $('#search_meja').val(meja_to);
		                            $.alert({
								        title: 'Alert!',
								        content: 'Berhasil Ganti Meja '+meja_from+' menjadi '+meja_to+'.',
								    });
			   }

            }
            ,
            error: function (textStatus, errorThrown)
            {
                $.alert({
			        title: 'Alert!',
			        content: 'Error Get Data!',
			    });
            }
        }
        );

	}


	function changes_stickers(){
		sticker_lama = $('#sticker_lama').val();
		sticker_baru = $('#sticker_baru').val();
		meja = $('#search_meja').val();

		if(sticker_lama==""){
			                        $.alert({
								        title: 'Alert!',
								        content: 'Isi No.Sticker Lama Terlebih Dahulu...',
								    });
			$('#sticker_lama').focus();
			return false;
		}

		if(sticker_baru==""){
			                        $.alert({
								        title: 'Alert!',
								        content: 'Isi No.Sticker Baru Terlebih Dahulu...',
								    });
			$('#sticker_baru').focus();
			return false;
		}

		$('#pleaseWaitDialog').modal('show');

		$.ajax({
            url: "<?php echo site_url('transaksi/cek_ready_menu/change_sticker') ?>/" + sticker_lama +"/"+ sticker_baru +"/"+ meja,
            type: "GET",
            dataType: "html",
            success: function (data)
            {
               if(data){

			   	$('#pleaseWaitDialog').modal('hide');
			   	    $('#sticker_lama').val('');
		            $('#sticker_baru').val('');
		            $("#changesticker").css("display","none");
		                            $.alert({
								        title: 'Alert!',
								        content: 'Sukses Ganti No. Sticker '+sticker_lama+' menjadi '+sticker_baru+' di Meja '+meja+'.',
								    });
			   }

            }
            ,
            error: function (textStatus, errorThrown)
            {
                $.alert({
			        title: 'Alert!',
			        content: 'Error Get Data!',
			    });
            }
        }
        );

	}


	function meja_aktif(){
	//refresh meja aktif
				                  	$.ajax({
							            url: "<?php echo site_url('transaksi/cek_ready_menu/getMejaAktif') ?>/",
							            type: "GET",
							            dataType: "html",
							            success: function (data)
							            {
							               $('#search_meja').html(data);
							               $('#meja_from').html(data);

							            }
							            ,
							            error: function (textStatus, errorThrown)
							            {
							                 $.alert({
										        title: 'Alert!',
										        content: 'Error Get Data',
										    });
							            }
							        }
							        );
	}




	function coba_test(){
		        $.alert({
			        title: 'Alert!',
			        content: 'Huhui!',
			    });
	}

</script>
