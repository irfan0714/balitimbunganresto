<?php $this->load->view('header'); 
$page = $this->uri->segment(4);?>
<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/tax_number/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="50" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?=$cari;?>" />
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><a href="<?php echo base_url() . "index.php/transaksi/tax_number/preview/".$msg['nofaktur']; ?>" ><h5><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div></h5></a><?php
}
?>


<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>No. Purcase Invoice</center></th>
		        <th width="100"><center>No. Purcase Order</center></th>
		        <th width="100"><center>No. Receipt Good</center></th>
		        <th width="100"><center>No. Faktur Pajak</center></th>
		        <th width="300"><center>Supplier</center></th>
        	</tr>
        </thead>
        <tbody>

            <?php
            if (count($ListPajak) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }
            
            if($page==""){
				$no = 1;
			}else{
				$no = ($page*1)+1;
			}
            
            foreach ($ListPajak as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                ?>
                <tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk detail <?php echo $val["NoFaktur"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td onclick="editing('<?php echo $val["NoFaktur"]; ?>')" align="left"><?php echo $val["NoFaktur"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoFaktur"]; ?>')" align="left"><?php echo $val["NoPO"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoFaktur"]; ?>')" align="left"><?php echo $val["NoPenerimaan"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoFaktur"]; ?>')" align="left"><?php echo $val["NoFakturPajak"]; ?></td>
                    <td onclick="editing('<?php echo $val["NoFaktur"]; ?>')" align="left"><?php echo $val["Nama"]; ?></td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<?php $this->load->view('footer'); ?>
<script>
function editing(nofaktur){
var url=$('#base_url').val();
window.location = url+"index.php/transaksi/tax_number/preview/"+nofaktur;	
}</script>
