<?php 
$this->load->view('header');
$mylib = new globallib();
 ?>
<head>
</head>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>

<form method="post" action="<?php echo base_url(); ?>index.php/transaksi/voucher_employee/import" enctype="multipart/form-data">
<div class="row">
    <div class="col-md-12" >
        <a download="template_upload_voucher_karyawan.xls"  href="<?php echo base_url(); ?>upload/excel_voucher_employee/template/template_upload_voucher_karyawan.xls" class="btn btn-success btn-icon btn-sm icon-left" style="float:left;margin-right: 100px;">Download Template Excel<i class="entypo-download"></i></a> 
     
        <b style="float:left;margin-right: 10px;">Bulan</b>
        <select class="form-control-new" name="bulan" style="float:left;margin-right: 10px;" required="">
            <option value="">Pilih</option>
            <?php 
            $arrbulan = array('0'=>'','Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                for($i=1;$i<=12;$i++){
                    echo "<option value='$i' >".$arrbulan[$i]."</option>";
                }
            ?>
        </select>  
        <b style="float:left;margin-right: 10px;">Tahun</b>
        <select class="form-control-new" name="tahun" style="float:left;margin-right: 10px;" required="">
            <option value="" >Pilih</option>
            <option value="2022" >2022</option>
            <option value="2021" >2021</option>
            <option value="2020" >2020</option>
            <option value="2019" >2019</option>
        </select>
        <input type="file" size="20" maxlength="30" name="file" id="file" class="form-control-new" style="float:left;margin-right: 10px;" required="" /> 
        <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" style="float:left;">Upload Excel<i class="entypo-upload"></i></button> 
    </div>
</div>
<hr>
<br>
</form>

<form method="POST"  name="search" id="form-search" action='<?php echo base_url(); ?>index.php/transaksi/voucher_employee/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
    <input type='hidden' value='<?= $offset ?>' id="offset" name="offset">
    <input type='hidden' value='no' id="excel" name="excel">
    <div class="row">
        <div class="col-md-12">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" /> 
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
				<option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Aktif</option>
                <option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Non Aktif</option>
            </select>  
            &nbsp;
            &nbsp;
            <b>Bulan</b>&nbsp;
            <select class="form-control-new" name="bulan" id="bulan">
                <?php 
                $arrbulan = array('0'=>'','Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                    for($i=1;$i<=12;$i++){
                        $select = ($bulan == $i) ? "selected" : "";
                        echo "<option value='$i' ".$select.">".$arrbulan[$i]."</option>";
                    }
                ?>
            </select>  
            &nbsp;
            &nbsp;
            <b>Tahun</b>&nbsp;
            <select class="form-control-new" name="tahun" id="tahun">
                <option value="">All</option>
                <option value="2022" <?php if($tahun == '2022'){echo "selected";} ?> >2022</option>
                <option value="2021" <?php if($tahun == '2021'){echo "selected";} ?> >2021</option>
                <option value="2020" <?php if($tahun == '2020'){echo "selected";} ?> >2020</option>
                <option value="2019" <?php if($tahun == '2019'){echo "selected";} ?> >2019</option>
            </select>  
            &nbsp;
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" id="btn-search">Search<i class="entypo-search"></i></button> 
            <button type="button" class="btn btn-success btn-icon btn-sm icon-left " id="export-excel">Export Excel<i class="entypo-download"></i></button> 
            <a href="<?php echo base_url();  ?>index.php/proses/tarik_data_outlet/" data-toggle="tooltip" data-placement="left"  title="Tarik data outlet 7 hari terakhir & hitung ulang voucher karyawan" target="_blank" class="btn btn-primary btn-sm  pull-right" id="">Hitung Ulang<i class="entypo-loader"></i></a> 
        </div>
    </div>
</form>
<br>
<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
   <?php  
$mylib = new globallib();
 ?>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">

<table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
                <th width="100"><center>NIK</center></th>
                <th width="100"><center>Keterangan</center></th>
                <th width="100"><center>Exp Date</center></th>
                <th width="100"><center>Status</center></th>
                <th width="100"><center>Nominal</center></th>
                <th width="100"><center>Terpakai</center></th>
                <th width="100"><center>Sisa</center></th>
                <th width="100"><center></center></th>
            </tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["status"] == 0) {
                    $echo_status = "<font style='color:#ff1c1c'><b>Non Aktif</b></font>";
                } else if ($val["status"] == 1) {
                    $echo_status = "<font style='color:#000000'><b>Aktif</b></font>";
                }
           
                ?>
                <tr title="<?php echo $val["nik"]." - ".$val["keterangan"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td bgcolor="<?=$color;?>"><?php echo $no; ?>.</td>
                    <td align="center"><?php echo $val["nik"]; ?></td>
                    <td align="left"><?php echo $val["keterangan"]; ?></td>
                    <td align="center"><?php echo date("d/m/Y",strtotime($val["expDate"])); ?></td>
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="right"><?php echo number_format($val["nominal"],0,",","."); ?></td>
                    <td align="right"><?php echo number_format($val["terpakai"],0,",","."); ?></td>
                    <td align="right"><?php echo number_format($val["nominal"] - $val["terpakai"],0,",","."); ?></td>
                    <td align="center">
                        <?php if($val["terpakai"] == 0){ ?>
                            <button class="btn btn-danger btn-xs" title="Hapus" onclick="hapus(this,'<?php echo $val["nik"]; ?>','<?php echo $val["expDate"]; ?>')"><i class="entypo-trash"></i></button>
                        <?php }else{ ?>
                            -
                        <?php } ?>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
    
</div>  

</div>

<?php $this->load->view('footer'); ?>



<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

	$(document).ready(function() {
        $("#export-excel").click(function(){
            $("#excel").val('yes');
            $("#form-search").submit();
        });

        $("#btn-search").click(function(){
            $("#excel").val('no');
            $("#form-search").submit();
        });
	});

    function hapus(e,nik,expDate){

        $.ajax({
            url : "<?php echo base_url(); ?>index.php/transaksi/voucher_employee/hapus",
            type: "post",
            data : {nik:nik,expDate:expDate},
            beforeSend : function(){
                $(e).html('.....');
            },
            success : function(res){
                if(res == 'success'){
                    $(e).parent().parent().remove();
                }else{
                    alert('Failed !');
                }
                $(e).html('<i class="entypo-trash"></i>');
            },
            error : function(e){
                alert("error "+e);
                $(e).html('<i class="entypo-trash"></i>');
            }
        });
    }
</script>
