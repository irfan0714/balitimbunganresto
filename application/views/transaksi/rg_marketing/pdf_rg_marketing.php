<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Proposal</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 10pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 9pt;
			vertical-align: top;
		}
		
	</style>
    <body>
    
   <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
            	<td colspan="5" align="left">
            		PT. Natura Pesona Mandiri<br>
					Jl. Raya Denpasar Bedugul KM.36 Tabanan<br>
					Bali 82191 - Indonesia<br>
					Phone : +62 361 4715379<br>
            	</td>
            </tr>
            
            <tr>
                <td colspan="5" align="center"><b>RECEIPT GOOD NON STOCK</b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><span><?= "No : ".$nomor; ?></span></td>
            </tr>
            
    </table> 
    
    <br>
    <table width="30%"  border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->TglDokumen; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>Supplier</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->Nama; ?></span></td>
                        </tr>
                        
                        <tr>
                            <td>P.O.M</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->PONo; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>Proposal</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->NoProposal; ?></span></td>
                        </tr>
                        
                        <!--<tr>
                            <td>P.O.M</td>
                            <td>:</td>
                            <td width="80" align="left"><span><?= $header->PONo; ?></span></td>
                            <td width="250">&nbsp;</td>
                            <td>Contact</td>
                            <td>:</td>
                            <td width="200" align="left"><span><?= $header->Telepon; ?></span></td>
                        </tr>-->
		</table>
		<br>
		
		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
			<tr>
				<td align="center" width="50">No</td>
				<td align="center" width="350">Nama Barang</td>
				<td align="center">Qty</td>
				<td align="center">Harga Satuan</td>
				<td align="center">SubTotal</td>
			</tr>
			<?php 
			$no = '1';
			foreach($detail AS $val){?>
			<tr>
				<td align="center"><?= $no;?></td>
				<td><?= $val['NamaBarang'];?></td>
				<td align="center"><?= $val['Qty'];?></td>
				<td align="right"><?= number_format($val['HargaSatuan']);?></td>
				<td align="right"><?= number_format($val['Total']);?></td>
			</tr>
			<?php 
			$no++;
			} ?>
			
			<tr>
				<td align="center">Note</td>
				<td colspan="2"><?= $header->Keterangan; ?></td>
				<td>Total</td>
				<td align="right"><?= number_format($header->Jumlah); ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>Disc</td>
				<td align="right"><?= number_format($header->DiscHarga); ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>PPN</td>
				<td align="right"><?= number_format($header->NilaiPPn); ?></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>Grand Total</td>
				<td align="right"><?= number_format($header->Total); ?></td>
			</tr>
		</table> 
		
		<br>
		
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
			<tr>
				<td align="center">Penerima,</td>
				<td align="center">Pengirim,</td>
				<td align="center">Mengetahui,</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
			</tr>
			
			<tr>
				<td align="center">..................</td>
				<td align="center">..................</td>
				<td align="center">..................</td>
			</tr>
		</table> 
    

</body>
</html>