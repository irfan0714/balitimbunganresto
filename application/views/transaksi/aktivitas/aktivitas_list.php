<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table>
	<tr>
		<td><input class="form-control-new" type='text' size'20' name='stSearchingKey' id='stSearchingKey'>
		<select class="form-control-new"  size="1" height="1" name ="searchby" id ="searchby">
				<option value="NamaAktivitas">Nama Aktivitas</option>
				<option value="NamaRekening">Nama Rekening</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search"></td>
		
	</tr>
</table>
</form>

<button class="btn btn-info pull-right" style="margin-right:10px;" onclick="form_tambah_aktivitas()"><i class="fa fa-plus"></i> &nbsp Tambah Aktivitas</button>
<br><br><br>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th><center>Nama Aktivitas</center></th>
		        <th  align="center" width="400"><center>Kode Rekening</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                
                ?>
                <tr title="<?php echo $val["KdAktivitas"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="left"><?php echo $val["NamaAktivitas"]; ?></td>
                    <td align="left"><?php echo $val["KdRekening"]." :: ".$val['NamaRekening']; ?></td>
                    <td align="center">
							<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="view_aktivitas('<?php echo $val["KdAktivitas"];?>','<?php echo base_url(); ?>')"><i class="entypo-eye"></i></button>
                    		<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="edit_aktivitas('<?php echo $val["KdAktivitas"];?>','<?php echo base_url(); ?>')"><i class="entypo-pencil"></i></button>
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="delete_aktivitas('<?php echo $val["KdAktivitas"]; ?>', '<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<?php
$this->load->view('footer'); ?>

<!-- Bootstrap modal -->
<div class="modal fade" id="modalaktivitas" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Tambah Aktivitas</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="kdaktivitas"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Aktitas</label>
                            <div class="col-md-9">
                                <input id="nmaktivitas" name="nmaktivitas" placeholder="Nama Aktivitas" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-3">Kode Rekening</label>
                        <div class="col-md-9">
                        <input type="text" class="form-control-new" size="10" maxlength="10" name="v_keyword_rekening" id="v_keyword_rekening" placeholder="Keyword" onblur="cari('<?php echo base_url(); ?>')">
                        <span id="select_rekening">
                        <select name="kdrekening" id="kdrekening" class="form-control-new" style="width:303px;">
                           
                           <option value=""> -- Pilih -- </option>
		            		<?php
		            		foreach($rekening as $val)
		            		{
		            			$selected="";
								if($rek_==$val["KdRekening"])
								{
									$selected='selected="selected"';
								}
								?><option <?php echo $selected; ?> value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["KdRekening"]." - ".$val["NamaRekening"]; ?></option><?php
							}
		            		?>
	            		</select>    
                        </span>
                        </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save('<?php echo base_url(); ?>','<?php echo $this->uri->segment(4); ?>')" class="btn btn-info">Simpan</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script type="text/javascript">
		var save_method;
		var rek_;
		var id_;
		
        function form_tambah_aktivitas(){
		    save_method = 'add';
			id_='0';
			$('#form')[0].reset(); // reset form on modals
			$('.form-group').removeClass('has-error'); // clear error class
			$('.help-block').empty(); // clear error string
			$('#modalaktivitas').modal('show'); // show modal					
		}
		
		function save(url)
		{
		
			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/aktivitas/save_aktivitas/";
			} else {
				url_ = url+"index.php/transaksi/aktivitas/edit_aktivitas/";
			}
			
			var aktivitas=$("#nmaktivitas").val();
			var kdrekening=$("#kdrekening").val();
			
		    $('#btnSave').text('confirm...'); //change button text 
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";
			
			$.ajax({
					url: url_,
					data: {id:id_, namaaktivitas:aktivitas, rekening:kdrekening},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
							$('#modalaktivitas').modal('hide');
							alert("Aktivitas Berhasil Disimpan.");
							location.reload();
						
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
				
		}
		
		function cari(url)
		{
			var rek=$("#v_keyword_rekening").val();
			$.ajax({
					url: url+"index.php/transaksi/aktivitas/ajax/",
					data: {id:rek},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#kdrekening').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
				}); 
			
			   	
   		}
   		
   		function edit_aktivitas(id,url){
   			
			save_method = 'edit';
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/aktivitas/edit/"+id+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
						id_=id;
			            $('[name="nmaktivitas"]').val(data.NamaAktivitas);
						rek_= $('[name="kdrekening"]').val(data.KdRekening);
			            $('#modalaktivitas').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}
		
		function view_aktivitas(id,url){
   			
			save_method = 'view';
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/aktivitas/edit/"+id+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
						id_=id;
			            $('[name="nmaktivitas"]').val(data.NamaAktivitas);
						rek_= $('[name="kdrekening"]').val(data.KdRekening);
						$('#btnSave').attr('disabled',true);
			            $('#modalaktivitas').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}
					
		function delete_aktivitas(id,url)
		{
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url+"index.php/transaksi/aktivitas/delete_aktivitas/",
					data: {id:id},
					type: "POST",
					dataType: "html",
					success: function(data)
					{
						    $('#modalaktivitas').modal('hide');
							alert("Aktivitas Berhasil Dihapus.");
							location.reload();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});

			}
		}

</script>