<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Purchase Return";
//test
$counter=1;
foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script language="javascript" src="<?=base_url();?>public/js/purchase_return.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_return/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->pretno; ?>">
	    <input type="hidden" name="purchasereturnno" id="purchasereturnno" value="<?php echo $header->pono; ?>">
	    <input type="hidden" name="v_gudang" id="v_gudang" value="<?php echo $header->warehouse; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->pretno; ?></b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Purchase Return No</td>
	            <td><b><?php echo $header->pono; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            
					<input type="text" class="form-control-new datepicker" value="<?php if($header->pretdate_indo!="" && $header->pretdate_indo!="00-00-0000") { echo $header->pretdate_indo; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10" onchange="reset_gudang()>
				
	            </td>
	        </tr>
	        
	        
	        <tr>
	            <td class="title_table">Gudang</td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;" onchange="cek_tgl_approve_so_type4('<?php echo base_url(); ?>')">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
	            			$selected="";
							if($header->warehouse==$val["KdGudang"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        
	        <!--<tr>
	            <td class="title_table" width="150">Warehouse</td>
	            <td><b><?php echo $header->warehouse." :: ".$header->Keterangan; ?></b></td>
	        </tr>-->
	        
	        <tr>
	            <td class="title_table">Subdivisi</td>
	            <td> 
	            	<select class="form-control-new" name="v_subdivisi" id="v_subdivisi" style="width: 200px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($subdivisi as $val)
	            		{
	            			$selected="";
							if($header->KdSubdivisi==$val["KdSubdivisi"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSubdivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	       	        
	        <tr>
	            <td class="title_table">Supplier </td>
	            <td> 
	            	<select class="form-control-new" name="v_supplier" id="v_supplier" style="width: 200px;">
	            		<option value="">Pilih Supplier</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
	            			$selected="";
							if($header->supplierid==$val["KdSupplier"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
	        <?php
			if($header->status==0)
			{
			?>
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
		            		<option <?php if($header->Status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Close</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
			<?php
			}
			else
			{
			?>
		        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            <?php
		            if($header->status==1)
		            {
						echo "<b>Close</b>";
					}
					else if($header->status==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr>
			<?php
			}
			?>
            
            
			<?php
			if(count($detail_list)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <!--<th width="200"><center>Note</center></th>-->
							    
							    <?php
							    if($header->status==0)
							    {
									echo '<th width="100"><center>Delete</center></th>';	
								}
							    ?>
							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$i=1;
						foreach($detail_list as $val)
						{
							?>
							<tr>
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaLengkap"]; ?></td>
								<td align="right"><?php echo $mylib->format_number($val["quantity"],0); ?></td>
								<td align="center"><?php echo $val["NamaSatuan"]; ?></td>
								<!--<td align="center"><?php echo $val["note"]; ?></td>-->
								
								<?php
							    if($header->status==0)
							    {
							    	?>
							    	<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail('<?php echo $val["retdetailid"]; ?>','<?php echo $val["PCode"]; ?>','<?php echo $header->pretno; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
							    	<?php									
								}
							    ?>
				                
							</tr>
							<?php
							$i++;
						}
						
						?>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        
	        <?php
	        }
	        ?>
			
			<?php
			if($header->status==0)
			{
			?>
			
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <!--<th width="200"><center>Note</center></th>-->
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="toFormat2('v_qty<?php echo $no;?>')" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
								<td>
				                <select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>">
				                	<option value=""> -- Pilih -- </option>
				                </select>
								</td>
				                <!--<td>
				                	<input type="text" class="form-control-new" name="v_note_detail[]" id="v_note_detail<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>" >
					                <?php
					                foreach($msatuan as $value)
					                {
					                	?><option value="<?php echo $value["KdSatuan"] ; ?>"><?php echo $value["NamaSatuan"] ; ?></option><?php
									}
					                ?>
				                	</select>
				                </td>-->
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_return/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	        <?php
	        }
	        else
	        {
				?>
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/purchase_return/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>
					</td>
		        </tr>
				<?php
			}
	        ?>
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->pono)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>