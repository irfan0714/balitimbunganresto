<?php 
$mylib = new globallib();
$user = $this->session->userdata('username');
function limit_words($string, $word_limit){
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}
 ?>
<div id="col-search" class="row" style="overflow:auto;height:380px;">
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="120"><center>Invoice</center></th>
		        <th width="50"><center>Tanggal</center></th>
		        <th width="50"><center>Jatuh Tempo</center></th>
		        <th width="120"><center>No. Reservasi</center></th>
		        <th width="200"><center>Keterangan</center></th>
		        <th width="80"><center>Total</center></th>
		        <th width="50"><center>Status</center></th>
                <th width="50"><center>Action</center></th>
        	</tr>
        
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Piutang</b></font>";
                } else if ($val["Status"] == 1) {
                    $echo_status = "<font style='color:#ff1c1c'><b>Close</b></font>";
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>Void</b></font>";
                }
                
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td bgcolor="<?=$color;?>"><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["Tanggal"]); ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["Jatuh_Tempo"]); ?></td>
                    <td align="center"><?php echo $val["NoReservasi"]; ?></td>
                    <td align="left"><?php echo limit_words($val["Keterangan"],6)." ..."; ?></td>
                    <td align="right"><?php echo number_format($val["Total_Nilai"]); ?></td>  
                    <td align="center"><?php echo $echo_status; ?></td>                                       
                    <td align="center">

                        <?php
                        if ($val["Status"] == 0) {
                            ?>
                            <!--<a href="<?php echo base_url(); ?>index.php/transaksi/uang_muka_beo/edit_uang_muka_beo/<?php echo $val["id"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>-->

                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Edit Bayar" title="" onclick="edit_invoice('<?php echo $val["NoDokumen"]; ?>')" >
                                <i class="entypo-pencil"></i>
                            </button>-->
                            
                            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" onclick="hapus_invoice('<?php echo $val["NoDokumen"]; ?>')" >
                                <i class="entypo-trash"></i>
                            </button>
                            
                            <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Close" title="" onclick="bayar('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo">&nbsp;<b>Rp</b>&nbsp;</i>
                            </button>
                            
                            <?php
                        }

                        if ($val["Status"] == 1) {
                            ?>
                            
                            <button type="button" class="btn btn-orange btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Export To PDF" title="" onclick="PopUpPrint('<?= $val['NoDokumen']; ?>', '<?= base_url(); ?>');">
								<i class="entypo-export"></i>
							</button>
							<?php if($user=="mechael0101"){?>
							<img src='<?= base_url(); ?>public/images/flag_orange.gif' border='0' title='Void' onclick="VoidTrans('<?= $val['NoDokumen']; ?>');"></img>
                            <?php } ?>
                            <?php
                        }

                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>
	</div>
	</div>
    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	
