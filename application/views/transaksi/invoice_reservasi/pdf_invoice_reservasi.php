<?php
$mylib = new globallib();
$this->load->helper('terbilang');

$total_tambahan=0;
foreach($getTambahan AS $valx){
$total_tambahan+=$valx['Nilai'];
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Reservasi</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 8pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 8pt;
		}
		
	</style>
    <body>
        <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
                <td colspan="5" align="center"><img src="<?= base_url(); ?>public/images/Logosgs.png" width="400" alt=""/></td>
            </tr>
            <tr>
                <td colspan="5" align="center"><b></b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><h3>INVOICE</h3></td>
            </tr>
            <br><br>
            <tr>
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td width="150">Bills To&nbsp;&nbsp;:</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150"><b><?=$getHeader->Nama;?></b></td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                                                
                        <tr>
                            <td width="150"><?=$getHeader->Alamat;?></td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                    </table>
                </td>
                <td></td>
                
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Invoice Date</td>
                            <td width="1">:</td>
                            <td width="150"><?=$mylib->ubah_tanggal($getHeader->Tanggal);?></td>

                        </tr>
                                                
                        <tr>
                            <td width="100">Invoice Number</td>
                            <td width="1">:</td>
                            <td width="150"><?=$getHeader->NoDokumen;?></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Reservasi Number</td>
                            <td width="1">:</td>
                            <td width="150"><?=$getHeader->NoReservasi;?></td>

                        </tr>
                        
                        <tr>
                            <td width="100">Due Date</td>
                            <td width="1">:</td>
                            <td width="150"><?=$mylib->ubah_tanggal($getHeader->Jatuh_Tempo);?></td>

                        </tr>
                    </table>
                </td>
                
            </tr>
            
        </table>
        <br>
        
        
        <table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="non-border-table" >
                        <tr>
                            <th width="200">Arrival Date</th>
                            <th>Descriptions</th>
							<th width="200">Total Value (Rp)</th>
                        </tr>
                         
                       
                                                
                        <?php
                        $total1=0; 
                        foreach($getDetail AS $val){?>
                        <tr>
                        <td rowspan="<?=count($getDetail) + count($getTambahan);?>" align="center"><?=$mylib->ubah_tanggal($val['TglDokumen']);?></td>
                        <td height="50" align="left"><?=$val['Keterangan'];?></td>
                        <td align="right"><?=number_format(($val['Total_Nilai']+$val['Uang_Muka'])-$total_tambahan);?></td>
                        </tr>
                        <?php
                        $total1+=($val['Total_Nilai']+$val['Uang_Muka'])-$total_tambahan;
                        } ?>
                                                
                        <?php 
                        if(!empty($getTambahan)){
                        	
	                        $total2=0;
	                        foreach($getTambahan AS $vals){?>
	                        <tr>
	                        <td height="30" align="left"><?=$vals['Keterangan'];?></td>
	                        <td align="right"><?=number_format($vals['Nilai']);?></td>
	                        </tr>
	                        <?php
	                        $total2+=$vals['Nilai'];
	                        }
                        
                        } ?>
                        <tr>
                        	<td colspan="2" align="right"><b>SubTotal</b></td>
                        	<td align="right"><?=number_format($total1+$total2);?></td>
                        </tr>
                        <tr>
                        	<td colspan="2" align="right"><b>Down Payment</b></td>
                        	<td align="right"><?=number_format($getHeader->Uang_Muka);?></td>
                        </tr>
                        <tr>
                        	<td colspan="2" align="right"><b>Total</b></td>
                        	<td align="right"><?=number_format(($total1+$total2)-$getHeader->Uang_Muka);?></td>
                        </tr>
                        <tr>
                        	<td  colspan="100%" align="left"><b>Be Regarded &nbsp;&nbsp;:&nbsp;&nbsp; <?php echo terbilang(($total1+$total2)-$getHeader->Uang_Muka)." Rupiah"; ?></b></td>
                        </tr>
                        
                    </table>
                    
  <br>                 
<table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            
            <tr>
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td colspan="100%"><b>TERM OF PAYMENT &nbsp;&nbsp;:</b></td>
                            <td width="1"></td>
                            <td colspan="100%"></td>

                        </tr>
                        
                        <tr>
                            <td colspan="100%">Payment Cash At Secret Garden Village</td>
                            <td width="1"></td>
                            <td colspan="100%"></td>

                        </tr>
                                                
                        <tr>
                            <td width="150">Or</td>
                            <td width="1"></td>
                            <td colspan="100%"></td>

                        </tr>
                        
                        <tr>
                            <td colspan="100%">Please Kindly Wire The Payment To Our Bank Account Below</td>
                            <td width="1">:</td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">BANK NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">BANK MANDIRI ( Cabang Tabanan )</td>

                        </tr>
                        
                        <tr>
                            <td width="150">ACCOUNT NUMBER</td>
                            <td width="1">:</td>
                            <td width="150">175.0000.260793</td>

                        </tr>
                        
                         <tr>
                            <td width="150">ACCOUNT NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">PT. NATURA PESONA MANDIRI</td>

                        </tr>
                        
                        
                        <tr>
                            <td width="150">&nbsp;&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="150">BANK NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">BANK BCA ( Cabang Tabanan )</td>

                        </tr>
                        
                        <tr>
                            <td width="150">ACCOUNT NUMBER</td>
                            <td width="1">:</td>
                            <td width="150">142.042.4244 ( IDR )</td>

                        </tr>
                        
                         <tr>
                            <td width="150">ACCOUNT NAME</td>
                            <td width="1">:</td>
                            <td colspan="100%">PT. NATURA PESONA MANDIRI</td>

                        </tr>
                        
                    </table>
                </td>
                <td></td>
                
                <td colspan="2">
                    <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
                    	<tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                                                
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                        
                        <tr>
                            <td width="100">&nbsp;</td>
                            <td width="1"></td>
                            <td width="150"></td>

                        </tr>
                    </table>
                </td>
                
            </tr>
            
        </table>
        
        <br><br>
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table" >
        	<tr>
        		<td align="center">Prepared By,</td>
        		<td align="center" style="display:none;"></td>
        		<td align="center">Approved By,</td>
        	</tr>
        	
        	<tr>
        		<td align="center" height="50">&nbsp;</td>
        		<td align="center" height="50" style="display:none;">&nbsp;</td>
        		<td align="center" height="50">&nbsp;</td>
        	</tr>
        	
        	<tr>
        		<td align="center">Mela Gena Yani</td>
        		<td align="center" style="display:none;"></td>
        		<td align="center">Bambang Sutrisno</td>
        	</tr>
        	<tr>
        		<td align="center">Staff Finance</td>
        		<td align="center" style="display:none;"></td>
        		<td align="center">Sr. FA & Adm Manager</td>
        	</tr>
        </table>

</body>
</html>