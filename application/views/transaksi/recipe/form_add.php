<?php 

$this->load->view('header'); 

$modul = "Recipe";

?>
<script>

	function pickThis(obj)
	{
		var	base_url = $("#base_url").val();
		
		
		objek = obj.id;
		id = parseFloat(objek.substr(9,objek.length-9));
		url = base_url+"index.php/pop/pop_up_recipe/index/"+id+"/";
		
		windowOpener(600, 1200, 'Cari PCode', url, 'Cari PCode')
	}
	
	function detailNew()
		{
			var clonedRow = $("#TabelDetail tr:last").clone(true);
			var intCurrentRowId = parseFloat($('#TabelDetail tr').length )-2;
			nama = document.getElementsByName("pcode[]");
			temp = nama[intCurrentRowId].id;
			intCurrentRowId = temp.substr(5,temp.length-5);
			var intNewRowId = parseFloat(intCurrentRowId) + 1;
			$("#pcode" + intCurrentRowId , clonedRow ).attr( { "id" : "pcode" + intNewRowId,"value" : ""} );
			$("#get_pcode" + intCurrentRowId , clonedRow ).attr( { "id" : "get_pcode" + intNewRowId} );
			$("#v_namabarang" + intCurrentRowId , clonedRow ).attr( { "id" : "v_namabarang" + intNewRowId,"value" : ""} );
			$("#v_qty" + intCurrentRowId , clonedRow ).attr( { "id" : "v_qty" + intNewRowId,"value" : 0} );
			$("#v_satuan" + intCurrentRowId , clonedRow ).attr( { "id" : "v_satuan" + intNewRowId,"value" : "Karton"});
			$("#btn_del_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_" + intNewRowId} );
			$("#TabelDetail").append(clonedRow);
			$("#TabelDetail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
			//$("#pcode" + intNewRowId).focus();
				ClearBaris(intNewRowId);
		}
		
		function ClearBaris(id)
		{
			$("#pcode"+id).val("");
			$("#v_namabarang"+id).val("");
			$("#v_qty"+id).val("");
			$("#v_satuan"+id).val("");
		}
		
		function cekTheform()
		{
			//banyaknya PCode detail dalam array
			var pcode1 = document.getElementsByName("pcode[]").length;
			
		
	    	var yesSubmit = true;
			
			//cek untuk validasi apakah sudah diisi
			for (var s = 1; s <=pcode1; s++)
	        {
	        	pcode = $("#pcode"+s).val();
	        	v_qty = $("#v_qty"+s).val();
	        	
	        	if(pcode)
	        	{
	        		if(v_qty*1==0)
		        	{
						alert("Qty PCode "+pcode+" harus diisi.");	
						document.getElementById("v_qty"+s).focus();
						
						yesSubmit = false;
						
						return false;
					}	
				}
	        }
	        
	        if(yesSubmit)
	        {
				//alert("submit");
				document.getElementById("theform").submit();	
				return false;
			} 
		}
	
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/recipe/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Resep ID</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Kode Barang</td>
	            <td>
                	<input type="text" class="form-control-new" value="<?php echo $header[0]['PCode']; ?>" name="PCode" id="PCode" size="20">
                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Nama Recipe</td>
	            <td>
	            	<input type="text" class="form-control-new" value="<?php echo $header[0]['NamaLengkap']; ?>" name="namalengkap" id="namalengkap" size="100" readonly>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Jenis <font color="red"><b>(*)</b></font></td>
	            <td colspan="3">
	            	<select class="form-control-new" name="v_jenis" id="v_jenis" style="width: 200px;">
	            		<option <?php if($header[0]['Jenis']=='C'){ echo "selected='selected'"; } ?> value="C">Compliment</option>
	            		<option <?php if($header[0]['Jenis']=='R'){ echo "selected='selected'"; } ?> value="R">Publish</option>
	            	</select>
	            </td>
	        </tr>  
	        
	        <tr>
	            <td class="title_table">Qty</td>
	            <td> 
	            	<input type="text" class="form-control-new" value="<?php echo $header[0]['Qty']; ?>" name="Qty" id="Qty">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Satuan</td>
	            <td> 
	            	<input type="readonly" class="form-control-new" value="<?php echo $header[0]['Satuan']; ?>" name="Satuan" id="Satuan">
	            </td>
	        </tr>
	        
	        <?php
			if($header[0]['Status']==0)
			{
			?>
	        <tr>
	            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
	            <td colspan="3">
	            	<select class="form-control-new" name="v_status" id="v_status" style="width: 200px;">
	            		<option <?php if($header[0]['Status']==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
	            		<option <?php if($header[0]['Status']==1){ echo "selected='selected'"; } ?> value="1">Close</option>
	            		<option <?php if($header[0]['Status']==2){ echo "selected='selected'"; } ?> value="2">Void</option>
	            	</select>
	            </td>
	        </tr>  
			<?php
			}
			else
			{
			?>
		        <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td colspan="3">
		            <?php
		            if($header[0]['Status']==1)
		            {
						echo "<b>Close</b>";
					}
					else if($header[0]['Status']==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr>  
			<?php
			}
			?>
	        
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td>
				                	<input type="readonly" class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	                <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/pengeluaran_lain/"; ?>">Keluar<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
