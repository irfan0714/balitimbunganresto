<?php 
$this->load->view('header'); 
$user = $this->session->userdata('username');
?>

<script text='javascript'>
function VoidTrans(param){
    //if (confirm("Anda yakin ingin menghapus transaksi ini?")){
        window.open(param,'','width=400,height=300,left=400,top=250');
    //}
}
</script>
</head>
<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/recipe/search'>
	<input type="hidden" name="btn_search" id="btn_search" value="y"/>
	<?php
	
	if($view_value > 0){
	?>
		<div class="row">
			<div class="col-md-9">
				<b>Search</b>&nbsp;
				<input type="text" size="60" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" />
			</div>
			
			<div class="col-md-3" align="right">
				<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
				<a href="<?php echo base_url()."index.php/transaksi/recipe/add_new/"; ?>" onClick="show_loading_bar(100)" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
				<a href="<?php echo base_url()."index.php/transaksi/recipe/recalculate/all"; ?>" onClick="show_loading_bar(100)" class="btn btn-info btn-icon btn-sm icon-left" title="" >Recalculate<i class="entypo-plus"></i></a>
			</div>
			
		</div>
	<?php
	}else{
	?>
		<div class="row">
			<div class="col-md-10">
				<b>Search</b>&nbsp;
				<input type="text" size="60" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if($search_keyword){ echo $search_keyword; } ?>" />
			</div>
			
			<div class="col-md-2" align="right">
				<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
				<a href="<?php echo base_url()."index.php/transaksi/recipe/add_new/"; ?>" onClick="show_loading_bar(100)" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
			</div>
		</div>
	<?php	
	}
	?>
</form>

<hr/>

<?php
if($this->session->flashdata('msg'))
{
  $msg = $this->session->flashdata('msg');
  
  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
}
?>
	
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
	<table class="table table-bordered responsive">
        <thead class="title_table">
			<tr>
				<th width="30"><center>No</center></th>
				<th width="100"><center>Recipe ID</center></th>
				<th width="250"><center>Nama</center></th>
				<th width="100"><center>Jenis</center></th>
				<th width="100"><center>Status</th>
				<th width="100"><center>Navigasi</th>
			</tr>
		</thead>
		<tbody>
		
		<?php
		if(count($data)==0)
		{
			echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
		}
		
		$no=1;
		foreach($data as $val)
		{
            $bgcolor = "";
            if($no%2==0)
            {
                $bgcolor = "background:#f7f7f7;";
            }
            
            if($val["Status"]==0)
        	{ 
        		$echo_status =  "<font style='color:#ff1c1c'>Pending</font>";
        	}
        	else if($val["Status"]==1)
        	{ 
        		$echo_status = "<font style='color:#6bcf29'>Close</font>";
        	}
        	else if($val["Status"]==2)
        	{ 
        		$echo_status = "<font style='color:#ff1c1c;'>Void</font>";
        	}
        	
        	if($val['Jenis']=='C')
        		$echo_jenis = 'Compliment';
        	else	
        		$echo_jenis = 'Publish';
            
			?>
			<tr style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
				<td><?php echo $no; ?></td>
                <td align="left"><?php echo $val["resep_id"]; ?></td>
                <td align="left"><?php echo $val["NamaLengkap"]; ?></td>
                <td><?php echo $echo_jenis; ?></td>
                <td align="center"><?php echo $echo_status; ?></td>
                <td align="center">
                
	                <?php
	                if($val["Status"]==0)
	        		{
	        			?>
						<a href="<?php echo base_url();?>index.php/transaksi/recipe/edit_form/<?php echo $val["resep_id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
	        			
						<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["resep_id"]; ?>','<?php echo base_url(); ?>');" >
							<i class="entypo-trash"></i>
						</button>
						<?php	
	        		}
	        		
	                if($val["Status"]==1)
	        		{
	        			?>
	        			<a href="<?php echo base_url();?>index.php/transaksi/recipe/edit_form/<?php echo $val["resep_id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
	        			
	        			<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['resep_id']; ?>', '<?= base_url(); ?>');">
							<i class="entypo-print"></i>
						</button>
	        			<?php	
	        		}
	        		
	                if($val["Status"]==2)
	        		{
	        			?>
	        			<a href="<?php echo base_url();?>index.php/transaksi/recipe/edit_form/<?php echo $val["resep_id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
	        			<?php if($user!="yuni0204"){ ;?>
						<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["resep_id"]; ?>','<?php echo base_url(); ?>');" >
							<i class="entypo-trash"></i>
						</button>
						<?php } ?>
	        			<?php	
	        		}
	                ?>
					
                </td>
            </tr>
			<?php	
			
			$no++;			
		}
		?>
		
		</tbody>
	</table>

	<div class="row">
		<div class="col-xs-6 col-left">
			<div id="table-2_info" class="dataTables_info">&nbsp;</div>
		</div>
		<div class="col-xs-6 col-right">
			<div class="dataTables_paginate paging_bootstrap">
				<?php echo $pagination; ?>
			</div>
		</div>
	</div>	
	
</div>
	

<?php $this->load->view('footer'); ?>
