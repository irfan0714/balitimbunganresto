<?php $this->load->view('header_part1')?>
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
</head>
<?php $this->load->view('header_part3')?>

<form action='<?=base_url(); ?>/index.php/hr/absensi/uploadproses' method="post" accept-charset="utf-8" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-2" align="right">
            FILE MESIN ABSENSI
		</div>
		<div class="col-md-3"  align="center">
			<input type="file" name="userfile" size="80"  id="userfile"  class="form-control" />
        </div>
		<div class="col-md-2" align="left">
			<input type="submit" value="UPLOAD" class="btn btn-default" />
        </div>
        <div class="col-md-5" align="right">
			&nbsp;
        </div>
    </div>
	<div class="row">
        <div class="col-md-10" align="right">
			<?=$errormsg?>
		</div>
		 <div class="col-md-2" align="right">
			 &nbsp;
		</div>
    </div>
</form>

<hr/>
	<?php 
	$this->load->view('footer'); 
//	$this->load->view('footer_part1');
	?>
<!--dua div dibawah, untuk nutup div yg ada di header-->
	</div>	
</div>
<!--eo dua div diatas-->
</body>
</html>