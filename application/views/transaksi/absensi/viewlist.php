<?php $this->load->view('header_part1')?>
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?= base_url();?>public/js/js.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/NotoSans.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/skins/black.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/style.css">
    <link rel="stylesheet" href="<?= base_url();?>public/css/my.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/default.css"/>
	<script language="javascript" src="<?= base_url(); ?>assets/js/zebra_datepicker.js"></script>
	<script>
	$(document).ready(function(){
		$('#tgldari').Zebra_DatePicker({format: 'Y-m-d'});
        $('#tglsampai').Zebra_DatePicker({format: 'Y-m-d'});
	});
	</script>
</head>
<?php $this->load->view('header_part3')?>

<form method="POST"  name="search" action='<?=base_url(); ?>/index.php/hr/absensi/view'>
    <div class="row">
        <div class="col-md-8">
            Range Tanggal&nbsp;&nbsp;Dari
			<input type='text' name='tgldari'  value="<?=$tgldari?>" size='12' id="tgldari"  class="form-control-new" readonly />
            &nbsp;Sampai&nbsp;
			<input type='text' name='tglsampai' value="<?=$tglsampai?>" size='12' id="tglsampai" class="form-control-new" readonly />
			&nbsp;<input type="submit" name='submit' value="TAMPIL" class='btn btn-default'>
			&nbsp;<input type="submit" name='submit' value="XLS" class='btn btn-default'>
			&nbsp;<input type="submit" name='submit' value="UPLOAD FILE" class='btn btn-default'>
        </div>
        <div class="col-md-4" align="right">
			&nbsp;
        </div>
    </div>
</form>

<hr/>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
				<th width="40"><center>No</center></th>
				<th><center>NIK Karyawan</center></th>
				<th><center>Nama Karyawan</center></th>
				<th width="120"><center>Jam Masuk</center></th>
				<th width="120"><center>Jam Keluar</center></th>
				<th width="120"><center>No Absen</center></th>
			</tr>
        </thead>
        <tbody>
		<?php 
			if(count($viewdata)==0) {
				echo "<tr><td colspan='5' align='center'>Tidak Ada Data</td></tr>";
			}
			for($i=0 ; $i<count($viewdata) ; $i++) 
			{ 
				$bgcolor	= ($i % 2 == 0)?"#C8DFE6":"WHITE";
				$tanggal	= $viewdata[$i]['Tanggal'];
				$isnewdate	= false;
				if($i<1) 
				{
					$isnewdate	= true;
				} 
				else 
				{
					if($viewdata[$i]['Tanggal']!=$viewdata[$i-1]['Tanggal']) //tanggal sekarang ga sama dengan tanggal kemaren
						$isnewdate	= true;
				}
				
				if($isnewdate) {
					echo "<tr style='background:#FFFF5A'><td colspan='6' align='left'><b><i>Tanggal ".$tanggal."</i></b></td></tr>";
					$no	= 1;
				}
			?>			
			<tr style="background:<?=$bgcolor?>">
				<td align="right"><?=$no?>&nbsp;</td>
				
				<td><?=$viewdata[$i]['employee_nik']?></td>
				<td><?=$viewdata[$i]['NamaKaryawan']?></td>	
				<td align="center"><?=$viewdata[$i]['TimeIn']?></td>
				<td align="center"><?=$viewdata[$i]['TimeOut']?></td>
				<td><?=$viewdata[$i]['NoAbsen']?></td>	
			</tr>
			<?php $no++; 
			} 
			?>         
        </tbody>
    </table>
</div>
	<?php 
	$this->load->view('footer'); 
//	$this->load->view('footer_part1');
	?>
<!--dua div dibawah, untuk nutup div yg ada di header-->
	</div>	
</div>
<!--eo dua div diatas-->
</body>
</html>