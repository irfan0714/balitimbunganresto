<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Pelunasan Piutang";

//$arr_data(unset);

?>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>View <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/purchase_invoice/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td>
	            	<input type="text" value="<?=$data['NoTransaksi']; ?>" class="form-control-new" name="notransaksi" id="notransaksi" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?=$data['TglDokumen']; ?>" name="tgldokumen" id="tgldokumen" readonly size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['NamaKasBank']; ?>" class="form-control-new" name="namabank" id="namabank" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">No Bukti <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NoBukti']; ?>" class="form-control-new" name="NoBukti" id="NoBukti" size="30" maxlength="25">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Pelanggan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NamaCustomer']; ?>" class="form-control-new" name="namapelanggan" id="namapelanggan" size="30" maxlength="25">
	            </td>
	            <td class="title_table" width="150">Credit Note <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['CNNo']; ?>" class="form-control-new" name="cnno" id="cnno" size="30" maxlength="25">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['Keterangan']; ?>" class="form-control-new" name="keterangan" id="keterangan" size="75" maxlength="100"  style="text-align: left;">
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
								<th><center>No Faktur</center></th>
								<th width="100"><center>Jatuh Tempo</center></th>
								<th width="100"><center>Nilai</center></th>
								<th width="100"><center>Bayar</center></th>
						    </tr>
						</thead>
						<tbody>
							
							<?php 
							$subtotal = 0;
							$ppn = 0;
							$num=count($row);
							$totfaktur=0;
							for($a=0 ; $a<count($row) ; $a++) {
							?>
							<tr id="baris<?php echo $a; ?>">
						        <td>
					            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" VALUE="<?=number_format($row[$a]['NilaiFaktur'], 4, ',', '.');?>" readonly style="text-align: right;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>" VALUE="<?=number_format($row[$a]['NilaiBayar'], 4, ',', '.');?>" style="text-align: right;">
					            </td>
						    </tr>
						    <?php
						    $totfaktur += $row[$a]['NilaiBayar']; 
							}
							$totbayar = $totfaktur+$data['BiayaAdmin']+$data['NilaiPPH']+$data['Pembulatan'];
						    ?>
							
							<tr>
								<td colspan="2">
									&nbsp;	    	</td>
								<td class="title_table" align="right">
									Total Faktur
								</td>
								<td>
									<input type="hidden" class="form-control-new" name="vtotalfaktur" id="vtotalfaktur" VALUE="<?=$totfaktur;?>" readonly style="text-align: right;">
									<input type="text" class="form-control-new" name="totalfaktur" id="totalfaktur" VALUE="<?=number_format($totfaktur, 4, '.', ',');?>" readonly style="text-align: right;">
								</td>
	    					</tr>
							<tr>
								<td colspan="2">
									&nbsp;	    	</td>
								<td class="title_table" align="right">
									Biaya Admin(+)
								</td>
								<td>
									<input type="hidden" class="form-control-new" name="vbiayaadmin" id="vbiayaadmin" VALUE="<?=$data['BiayaAdmin'];?>" style="text-align: right;">
									<input type="text" class="form-control-new" name="biayaadmin" id="biayaadmin" VALUE="<?=number_format($data['BiayaAdmin'], 4, '.', ',');?>"  onchange="BiayaAdminBlur()" style="text-align: right;">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									&nbsp;	    	</td>
								<td class="title_table" align="right">
									<select class="form-control-new" name="vkdrekeningpph" id="vkdrekeningpph">
										<option value="">- Rek PPH -</option>
										<?php
										foreach($rekeningpph as $val)
										{
										?>	
											<option <?=$val['KdRekening']==$data['KdRekeningPPH']?"selected" : "" ;?> value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option>
										<?php
										}
										?>
									</select>
								</td>
								<td>
									<input type="hidden" class="form-control-new" name="vpph" id="vpph" VALUE="<?=$data['NilaiPPH'];?>" style="text-align: right;">
									<input type="text" class="form-control-new" name="pph" id="pph" VALUE="<?=number_format($data['NilaiPPH'], 4, '.', ',');?>"  onchange="PPHBlur()" style="text-align: right;">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									&nbsp;	    	</td>
								<td class="title_table" align="right">
									Total Bayar
								</td>
								<td>
									<input type="hidden" class="form-control-new" name="vtotalbayar" id="vtotalbayar" VALUE="<?=$totbayar;?>"  style="text-align: right;">
									<input type="text" class="form-control-new" name="totalbayar" id="totalbayar" VALUE="<?=number_format($totbayar, 4, '.', ',');?>"  onchange="TotalBayarBlur()" style="text-align: right;" readonly>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									&nbsp;	    	</td>
								<td class="title_table" align="right">
									Selisih Pembulatan
								</td>
								<td>
									<input type="hidden" class="form-control-new" name="vpembulatan" id="vpembulatan" VALUE="<?=$data['Pembulatan'];?>" readonly style="text-align: right;">
									<input type="text" class="form-control-new" name="pembulatan" id="pembulatan" VALUE="<?=number_format($data['Pembulatan'], 4, '.', ',');?>" readonly style="text-align: right;">
								</td>
							</tr>
						</tbody>
					</table>
	        	</td>
	        </tr>
	    </table>
	    
	    </form> 
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
		</ol>
			
        <table class="table table-bordered responsive">
            <tr>
            	<td class="title_table" width="150">Author</td>
	            <td><?php echo $data['AddUser']." :: ".$data['AddDate']; ?></td>
            </tr>
            <tr>
            	<td class="title_table" width="150">Edited</td>
	            <td><?php echo $data['EditUser']." :: ".$data['EditDate']; ?></td>
            </tr>
         </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
