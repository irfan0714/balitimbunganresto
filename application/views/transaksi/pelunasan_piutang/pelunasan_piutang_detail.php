<table class="table table-bordered responsive" id="TabelDetail">
	<thead class="title_table">
		<tr>
			<th><center>No Faktur</center></th>
			<th width="100"><center>Jatuh Tempo</center></th>
			<th width="100"><center>Nilai</center></th>
			<th width="100"><center>Bayar</center></th>
	    </tr>
	</thead>
	<tbody>
		
		<?php 
		$subtotal = 0;
		$ppn = 0;
		$num=count($row);
		$totfaktur=0;
		for($a=0 ; $a<$num ; $a++) {
		?>
		<tr id="baris<?php echo $a; ?>">
	        <td>
            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
            </td>
            <td>
            	<input type="hidden" class="form-control-new" name="vSisa[]" id="vSisa<?php echo $a;?>" VALUE="<?=$row[$a]['Sisa'];?>" readonly style="text-align: right;">
            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" VALUE="<?=number_format($row[$a]['Sisa'], 4, ',', '.');?>" readonly style="text-align: right;">
            </td>
            <td>
            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>" onblur="formatdec('Bayar<?php echo $a;?>')" onchange="CekBayar(this)" style="text-align: right;">
            	<input type="hidden" class="form-control-new" name="vBayar[]" id="vBayar<?php echo $a;?>">
            </td>
	    </tr>
	    <?php
	    //$totfaktur += $row[$a]['NilaiBayar']; 
			}
			//$totbayar = $totfaktur;
				?>
			
			<tr>
	    	<td colspan="2">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Total Faktur
	    	</td>
	    	<td>
	    		<input type="hidden" class="form-control-new" name="vtotalfaktur" id="vtotalfaktur" VALUE="0" readonly style="text-align: right;">
	    		<input type="text" class="form-control-new" name="totalfaktur" id="totalfaktur" VALUE="0" readonly style="text-align: right;">
	    	</td>
	    </tr>
			<tr>
				<td colspan="2">
					&nbsp;	    	</td>
				<td class="title_table" align="right">
					<select class="form-control-new" name="vkdrekeningpph" id="vkdrekeningpph">
						<option value="">- Rek PPH -</option>
						<?php
						foreach($rekeningpph as $val)
						{
						?>	
							<option value="<?php echo $val["KdRekening"]; ?>"><?php echo $val["NamaRekening"]; ?></option>
						<?php
						}
						?>
					</select>
				</td>
				<td>
		    	<input type="hidden" class="form-control-new" name="vpph" id="vpph" VALUE="0" style="text-align: right;">
	    		<input type="text" class="form-control-new" name="pph" id="pph" VALUE="0"  onblur="PPHBlur()" style="text-align: right;">
	    	</td>
			</tr>
	    <tr>
	    	<td colspan="2">
				&nbsp;	    	</td>
	    	<td class="title_table" align="right">
	    		Biaya Admin(+)
	    	</td>
	    	<td>
		    	<input type="hidden" class="form-control-new" name="vbiayaadmin" id="vbiayaadmin" VALUE="0" style="text-align: right;">
	    		<input type="text" class="form-control-new" name="biayaadmin" id="biayaadmin" VALUE="0"  onblur="BiayaAdminBlur()" style="text-align: right;">
	    	</td>
	    </tr>
			<tr>
				<td colspan="2">
					&nbsp;	    	</td>
				<td class="title_table" align="right">
					Total Bayar
				</td>
				<td>
	    		<input type="hidden" class="form-control-new" name="vtotalbayar" id="vtotalbayar" VALUE="0"  style="text-align: right;">
	    		<input type="text" class="form-control-new" name="totalbayar" id="totalbayar" VALUE="0"  onblur="TotalBayarBlur()" style="text-align: right;">
	    	</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;	    	</td>
				<td class="title_table" align="right">
					Selisih Pembulatan
				</td>
				<td>
	    		<input type="hidden" class="form-control-new" name="vpembulatan" id="vpembulatan" readonly style="text-align: right;">
	    		<input type="text" class="form-control-new" name="pembulatan" id="pembulatan" VALUE="0" readonly style="text-align: right;">
	    	</td>
			</tr>
	</tbody>
</table>