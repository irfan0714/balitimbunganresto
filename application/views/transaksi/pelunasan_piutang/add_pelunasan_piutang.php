<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Pelunasan piutang";

//$arr_data(unset);

?>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
    
    function submitthis(){
    	jenis = $("#v_jenis").val();
    	if(jenis=='C'){
			totalbayar = $("#vTotalBayar").val();	
			nilaicn = $("#nilaicn").val();
			cnno = $("#v_cnno").val();
			if(cnno == '')
				alert("Credit Note harus diisi");
			else if(nilaicn*1 != totalbayar*1){
				alert("Nilai CN= "+nilaicn+" tidak sama dengan total bayar="+totalbayar);
			}else
				$("#theform").submit();
		}else{
			kdkasbank = $("#KdKasBank").val();
			if(kdkasbank=='')
				alert("Kas/Bank harus diisi.")
			else
				$("#theform").submit();
		}
	}
    
    function getDetail(){
    	var KdCustomer = $('#KdCustomer').val();
    	var vtype = $('#v_type').val();
    	var vdata = new Array(2);
    	vdata[0] = KdCustomer;
    	var jsonString = JSON.stringify(vdata);
    	$.ajax({
	        type: "POST",
	        url: "<?=base_url();?>index.php/transaksi/pelunasan_piutang/getDetail/",
	        data: {data: vdata, type: vtype},
	        success: function(data) {
				$('#UpdateDetail').html(data);
			}
	    });
	}
	
	function formatdec(nilai){
		var MataUang = 'IDR'
		if(MataUang=='IDR'){
			result = toFormat(nilai);
		}else{
			result = toFormat4(nilai);	
		}
		return result;
	}
	
	function CekBayar(obj) {
		objek = obj.id;
        id = parseFloat(objek.substr(5, objek.length - 5));

        bayar = parseFloat($("#Bayar" + id).val());
        sisa = parseFloat($("#vSisa" + id).val());
        
        if(bayar>sisa){
			alert("Pembayaran lebih besar dari nilai faktur.");
			$("#Bayar" + id).val(0);
		}else{
			HitungBayar(id);
		}
    }
    
    function HitungBayar(id){
		bayar = $("#Bayar"+id).val();
		var xbayar = formatdec('Bayar'+id);
		$("#vBayar"+id).val(bayar);
		
		var lastRow = document.getElementsByName("NoFaktur[]").length;
    	var total = 0;
    	for (index = 0; index < lastRow; index++)
    	{
			nama = document.getElementsByName("vBayar[]");
        	temp = nama[index].id;
        	temp1 = parseFloat(nama[index].value);
        	
        	if(temp1!='' && !isNaN(temp1)){
				total += temp1;	
			}
		}
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (total + biayaadmin + pph);
		
		$("#vtotalfaktur").val(total);
		$("#totalfaktur").val(total);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('totalfaktur');
		formatdec('pembulatan');
	}
	
	function BiayaAdminBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#biayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin + pph);
		$("#vbiayaadmin").val(biayaadmin);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('biayaadmin');
	}
	
	function PPHBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#pph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin + pph);
		$("#vpph").val(pph);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('pph');
	}
	
	function TotalBayarBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#totalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin + pph);
		$("#vtotalbayar").val(totalbayar);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('totalbayar');
	}
    
    function type_onclick(){
		type = $("#v_type").val();
		
		$.ajax({
	         url: "<?=base_url();?>index.php/transaksi/pelunasan_piutang/ajax_customer/",
	         type: "POST",
	         async: true,
	         data: {Type: type },
	         success: function(res)
				{
					$('#KdCustomer').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
      	});
	}
	
	function jenis_onclick(){
		jenis = $("#v_jenis").val();
		if(jenis=='B'){
			$("#KdKasBank").prop('disabled', false);
			$("#v_cnno").prop('disabled', 'disabled');
		}else{
			$("#KdKasBank").prop('disabled', 'disabled');
			$("#v_cnno").prop('disabled', false);
		}
	}
	
	function getCN(){
		jenis = $("#v_jenis").val();
		kdcustomer = $("#KdCustomer").val();
		if(jenis=='C'){
			$.ajax({
	         url: "<?=base_url();?>index.php/transaksi/pelunasan_piutang/getCN/",
	         type: "POST",
	         async: true,
	         data: {Customer: kdcustomer },
	         success: function(res)
				{
					$('#v_cnno').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
      		});	
		}
	}
	
	function getNilaiCN(){
		var url = $("#base_url").val();
		var cnno = $('#v_cnno').val();
		$.ajax({
			url: url+"index.php/transaksi/pelunasan_piutang/getNilaiCN/",
			data: {creditno:cnno},
			type: "POST",
			dataType: 'html',					
			success: function(res)
			{
				
				$('#nilaicn').val(res);
			},
			error: function(e) 
			{
				alert(e);
			} 
		});
	}

</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/pelunasan_piutang/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="tgldokumen" id="tgldokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Sumber <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_jenis" id="v_jenis" style="width: 100px;" onchange="jenis_onclick()">
	            		<option value="B">Kas/Bank</option>
	            		<option value="C">Credit Note</option>
		            </select>
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?=form_dropdownDB_initJS('KdKasBank', $kasbank, 'KdKasBank', 'NamaKasBank', '', '', '--------- Pilih ---------', 'id="KdKasBank" class="widthonehundred");"');?>
	            </td>
	            <td class="title_table" width="150">No Bukti <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" name="NoBukti" id="NoBukti" size="30" maxlength="25">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Type <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 100px;" onchange="type_onclick()">
	            		<option value="1">Distribusi</option>
	            		<option value="2">BEO</option>
		            </select>
	            </td>
	            <td class="title_table" width="150">Pelanggan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?=form_dropdownDB_initJS('KdCustomer', $customer, 'KdCustomer', 'Nama', '', '', '--------- Pilih ---------', 'id="KdCustomer" onchange="getCN();getDetail()" class="widthonehundred");"');?>
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new" name="keterangan" id="keterangan" size="50" maxlength="100"  style="text-align: left;">
	            </td>	
	            <td class="title_table" width="150">Credit Note <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_cnno" id="v_cnno" style="width: 200px;" onchange="getNilaiCN()">
	            		
		            </select>
		            &nbsp;
		            <input type="text" class="form-control-new" name="nilaicn" id="nilaicn" size="50" style="text-align: left;" readonly>
	            </td>	
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<span id='UpdateDetail'></span>
	        	</td>
	        </tr>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/pelunasan_piutang/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onclick="submitthis();" value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
	new Spry.Widget.ValidationSelect("KdCustomer");
	new Spry.Widget.ValidationTextField("tgldokumen", "date", {format:"dd-mm-yyyy", useCharacterMasking:true});
	new Spry.Widget.ValidationTextField("kurs", "real", {useCharacterMasking:true});
</script>
