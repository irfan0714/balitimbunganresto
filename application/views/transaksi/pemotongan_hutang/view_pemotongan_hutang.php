<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Pemotongan Hutang";

$counter=1;
foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script language="javascript" src="<?=base_url();?>public/js/pemotongan_hutang_v2.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>

<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/pemotongan_hutang/save_data'>
	    <input type="hidden" name="v_dnallocationno" id="v_dnallocationno" value="<?php echo $header->dnallocationno; ?>">
	    <input type="hidden" name="v_dnno" id="v_dnallocationno" value="<?php echo $header->dnno; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->dnallocationno; ?></b></td>
	        </tr>
	                       
	         <tr>
	            <td class="title_table" width="150">Tanggal Credit Note</td>
	            <td> 
								
					<input type="text" class="form-control-new datepicker" value="<?php if($header->dnadate_indo!="" && $header->dnadate_indo!="00-00-0000") { echo $header->dnadate_indo; }else{echo date('d-m-Y');}  ?>" name="v_tgl_pemotongan_hutang" id="v_tgl_pemotongan_hutang" size="10" maxlength="10">	
	            </td>
	        </tr>
	        
	        <tr>
				<td class="title_table">Supplier </td>
				<td nowrap>
				<select class="form-control-new" name="KdSupplier" id="KdSupplier" size="1" onchange="ambildebitno('<?=base_url();?>')" style="width: 25%;">
					<?php
						for($s=0;$s<count($msupplier);$s++)
						{
							?>
							<option <?php if($msupplier[$s]['KdSupplier']==$header->supplierid) echo "selected"; ?> value="<?=$msupplier[$s]['KdSupplier'];?>"><?=$msupplier[$s]['Nama'];?></option>
						<?php
						}
					?>
				</select></td>
			</tr>
			
			<tr>
				<td class="title_table">Debit Credit No</td>
				<td nowrap>
				<select class="form-control-new" name="debitno" id="debitno" size="1" style="width: 25%;">
				<option <?php if($header->dnno=="") echo "selected"; ?> value=""> Pilih Debit Credit No </option>
					<?php
						for($s=0;$s<count($anak);$s++)
						{
							?>
							<option <?php if($anak[$s]['nama']==$header->dnno) echo "selected"; ?> value="<?=$anak[$s]['nama'];?>"><?=$anak[$s]['nama'];?></option>
						<?php
						}
					?>
				</select></td>
			</tr>
			
			<tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Mata Uang", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	        
	        <!--<tr>
	            <td>&nbsp;</td>
	            <td> Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="<?php echo $header->invamount; ?>" name="v_amount" id="v_amount" maxlength="255" size="25%" style="text-align: right;">
	            	 &nbsp;&nbsp;&nbsp;&nbsp;
	            	 Allocation Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="<?php echo $header->allocation; ?>" name="v_amount_allocation" id="v_amount_allocation" maxlength="255" size="25%" style="text-align: right;">
	            </td>
	        </tr>
	        
	        <tr>
	            <td>&nbsp;</td>
	            <td> Amount IDR &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="<?php echo $header->invamountidr; ?>" name="v_amount_idr" id="v_amount_idr" maxlength="255" size="25%" style="text-align: right;">
	            	 &nbsp;&nbsp;&nbsp;&nbsp;
	            	 Allocation Amount IDR &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control-new" value="<?php echo $header->allocationidr; ?>" name="v_amount_allocation_idr" id="v_amount_allocation_idr" maxlength="255" size="25%" style="text-align: right;">
	            </td>
	        </tr>-->
	        
	        <tr>
		            <td class="title_table">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_status" id="v_status" style="width: 25%;">
		            		<option <?php if($header->status==0){ echo "selected='selected'"; } ?> value="0">Pending</option>
		            		<option <?php if($header->status==1){ echo "selected='selected'"; } ?> value="1">Close</option>
		            		<option <?php if($header->status==2){ echo "selected='selected'"; } ?> value="2">Void</option>
		            	</select>
		            </td>
		        </tr>
				
           
			<tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="120"><center>DN Allocation</center></th>
								<th width="120"><center>Invoice No</center></th> 
								<th><center>Supplier</center></th>
							    <th width="150"><center>Sisa</center></th>
							    <th width="150"><center>Allocation Amount</center></th>
							    <!--<th width="150"><center>Amount IDR</center></th>
							    <th width="150"><center>Allocation Amount IDR</center></th>-->
							    <?php if($header->status==0){ ?>
							    <th width="50"><center>
							    	Action</center>
							    </th>
							    <?php } ?>
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($datadna)<=0){
						 	echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
						 }else{
						 	$no=0;
						 	foreach($datadna as $val){
						 		$no		= $no + 1;
								?>
								<tr>
									<td><?php echo $val['dnallocationno']; ?>
									
										<input type="hidden" class="form-control-new" name="v_dnadetailid2[]" id="v_dnadetailid2<?php echo $no;?>" value="<?php echo $val['dnadetailid']; ?>" style="width: 100%;"/>
										<input type="hidden" class="form-control-new" name="v_dnallocationno2[]" id="v_dnallocationno2<?php echo $no;?>" value="<?php echo $val['dnallocationno']; ?>" style="width: 100%;"/>
									</td>
									<td><?php echo $val['invno']; ?></td>
									<td><?php echo $val['Nama']; ?></td>
									
									<td>
										<input type="text" class="form-control-new" name="v_invamount2[]" id="v_invamount2<?php echo $no;?>" value="<?php echo number_format($val['sisa'],0); ?>"  style="text-align: right;width: 100%;"/>
									</td>
									
									<td align='right'>
										<input type="text" class="form-control-new" name="v_allocation2[]" id="v_allocation2<?php echo $no;?>" value="<?php echo number_format($val['allocation'],0); ?>" onkeypress="hitung(event,'<?=$no;?>');" style="text-align: right; width: 100%;" />
									</td>
									
									<!--<td>
										<input type="text" class="form-control-new" name="v_invamountidr2[]" id="v_invamountidr2<?php echo $no;?>" value="<?php echo $val['invamountidr']; ?>"  style="text-align: right;width: 100%;"/>
									</td>
									
									<td align='right'>
										<input type="text" class="form-control-new" name="v_allocationidr2[]" id="v_allocationidr2<?php echo $no;?>" value="<?php echo $val['allocationidr']; ?>" onkeypress="hitung(event,'<?=$no;?>');" style="text-align: right; width: 100%;" />
									</td>-->
									
									<?php if($header->status==0){ ?>
									<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteDetail2('<?php echo $val["dnadetailid"]; ?>','<?php echo $val["dnallocationno"]; ?>','<?php echo base_url(); ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
					                <?php } ?>
								</tr>
								<?php
							}
						 }
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
		   
			
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/pemotongan_hutang/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;">View Only.</font>
					</td>
		        </tr>
				
		
	        
	    </table>
		
		
	    </form> 
	    
	    <?php
        if($header->dnno)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>