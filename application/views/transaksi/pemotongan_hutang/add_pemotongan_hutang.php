<?php 

$this->load->view('header'); 

$modul = "Debit Note";

?>

<script language="javascript" src="<?=base_url();?>public/js/pemotongan_hutang_v2.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Tambah <?php echo $modul; ?></strong></li>
		</ol>
		
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/pemotongan_hutang/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal Debit</td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tanggal_dokumen" id="v_tanggal_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
				<td class="title_table">Supplier </td>
				<td nowrap>
				<!--<select class="form-control-new" name="KdSupplier" id="KdSupplier" size="1" onchange="ambildebitno('<?=base_url();?>')" style="width: 25%;">-->
				<select class="form-control-new" name="KdSupplier" id="KdSupplier" size="1" onchange="ambilhutang()" style="width: 25%;">
					<option value=""> -- Pilih Supplier -- </option>
					<?php
						for($s=0;$s<count($msupplier);$s++)
						{
							?>
							<option <?php if($msupplier[$s]['Nama']==$spp) echo "selected"; ?> value="<?=$msupplier[$s]['KdSupplier'];?>"><?=$msupplier[$s]['Nama'];?></option>
						<?php
						}
					?>
				</select></td>
			</tr>
			
			<tr>
				<td class="title_table">Debit Note No </td>
				<td nowrap>
				<select class="form-control-new" name="debitno" id="debitno" size="1" style="width: 25%;" onchange="ambilamount()">
				<option <?php if($debitno=="") echo "selected"; ?> value=""> Pilih Debit Credit No </option>
					<?php
						for($s=0;$s<count($anak);$s++)
						{
							?>
							<option <?php if($anak[$s]['nama']==$debitno) echo "selected"; ?> value="<?=$anak[$s]['nama'];?>"><?=$anak[$s]['nama'];?></option>
						<?php
						}
					?>
				</select></td>
			</tr>
				        
	        <tr>
	           <?php
                        $mylib = new globallib();
                        $action = "onchange =\"SetKembali();\"";
                        echo $mylib->write_combo4("Mata Uang", "Uang", $mUang, "", "Kode", "Keterangan", "", $action, "ya");
                        ?>
	        </tr>
	       
	        
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				
					</table>
	        	</td>
	        </tr>
	        	<tr colspan='100%' align='center' id='amounts'></tr>        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick="batal('<?php echo base_url(); ?>');">Batal<i class="entypo-check"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                </td>
	        </tr>
	        
	    </table>
	    
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
	function ambilhutang(){
		
		 var url = $("#base_url").val();
	     var supplier = $('#KdSupplier').val();
	     var tgl = $('#v_tanggal_dokumen').val();
	     
				$.post(url+"index.php/transaksi/pemotongan_hutang/getDebitCreditNo", { KdSupplier: $("#KdSupplier").val()},
				function(data){
				   $("#debitno").empty();
				   $("#debitno").append(data);
				   $("#debitno2").empty();
				   $("#debitno2").append(data);
				});
	     
			  $.ajax({
					url: url+"index.php/transaksi/pemotongan_hutang/ambilhutang/",
					data: {spl:supplier,tanggal:tgl},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#TabelDetail').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
					   });    	
}

function ambilamount(){
	
	var url = $("#base_url").val();
	var dnno = $('#debitno').val();
	$.ajax({
					url: url+"index.php/transaksi/pemotongan_hutang/ambilamount/",
					data: {debit:dnno},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#amounts').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
					   });
}
</script>