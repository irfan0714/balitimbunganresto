<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table>
	<tr>
		<td><input class="form-control-new" type='text' size'20' name='stSearchingKey' id='stSearchingKey'>
			
		
			<select class="form-control-new" size="1" height="1" name ="searchby" id ="searchby">
				<!--<option value="NoStiker">No Stiker</option>-->
				<option value="NamaSalesman">Nama Sales</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search"></td>
	</tr>
</table>
</form>

<br>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <!--<th width="100"><center>No. Stiker</center></th>-->
		        <th><center>Nama Sales</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($gsa_data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($gsa_data as $val) {
                
                ?>
                <tr title="<?php echo $val["stk_id"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["Tanggal_"]; ?></td>
                    <!--<td align="center"><?php echo $val["NoStiker"]; ?></td>-->
                    <td align="left"><?php echo $val["KdSalesman"]." :: ".$val['NamaSalesman']; ?></td>
                    <td align="center">
                            <a href="<?php echo base_url(); ?>index.php/transaksi/stiker_sales/view_stiker_sales/<?php echo $val["stk_id"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/stiker_sales/edit_stiker_sales/<?php echo $val["stk_id"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/stiker_sales/delete_stiker_sales/<?php echo $val["stk_id"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title=""><i class="entypo-trash"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["KdSalesman"]; ?>', '<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>-->
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<!--<table align = 'center' border='1' class='table_class_list'>
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php } ?>
		<th>Kode Salesman</th>
		<th>Nama Salesman</th>
		<th>Nama Personal</th>
		<th>Nama Tipe Salesman</th>
		<th>Gudang</th>
	</tr>
<?php
	if(count($gsa_data)==0)
	{ 
?>
	<td nowrap colspan="4" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($gsa_data);$a++)
{
?>
	<tr>
<?php
	if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
	{
?>
			<td nowrap>
		<?php
			if($link->view=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/salesman/view_salesman/<?=$gsa_data[$a]['KdSalesman'];?>"><img src='<?=base_url();?>public/images/zoom.png' border = '0' title = 'View'/></a>
		<?php
			}
			if($link->edit=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/salesman/edit_salesman/<?=$gsa_data[$a]['KdSalesman'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php
			}
			if($link->delete=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/salesman/delete_salesman/<?=$gsa_data[$a]['KdSalesman'];?>"><img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete'/></a>
		<?php
			}
		?>
		</td>
<?php } ?>
		<td nowrap><?=stripslashes($gsa_data[$a]['KdSalesman']);?></td>
		<td nowrap><?=stripslashes($gsa_data[$a]['NamaSalesman']);?></td>
		<td nowrap><?=stripslashes($gsa_data[$a]['NamaPersonal']);?></td>
		<td nowrap><?=stripslashes($gsa_data[$a]['NamaTipeSalesman']);?></td>
		<td nowrap><?=stripslashes($gsa_data[$a]['KdGudang']);?></td>
	<tr>
<?php
}
?>
</table>-->
<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/transaksi/stiker_sales/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>