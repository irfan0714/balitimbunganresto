<?php $this->load->view('header'); ?>
<head>
<script>
    function PopUpVoucher(id)
    {
        base_url = "<?php echo base_url(); ?>";
        url = base_url+"index.php/report/report_kasir/pop_up_detail_voucher/index/"+id+"/";
        windowOpener(400, 600, 'Detail Voucher', url, 'Detail Voucher')
    }
</script>
<script text='javascript'>
function VoidTrans(param){
    //if (confirm("Anda yakin ingin menghapus transaksi ini?")){
        window.open(param,'','width=400,height=300,left=400,top=250');
    //}
} 
</script>
</head>
<?php
    $username = $this->session->userdata('username');
    $q = "SELECT * FROM `otorisasi_user` WHERE Tipe='gudang' AND UserName='$username'";
    $qry = mysql_query($q);
    $num_hasil = mysql_num_rows($qry);
?>
<script language="javascript" src="<?= base_url(); ?>public/js/permintaan_barang.js"></script>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/permintaan_barang/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;<b>Gudang</b>&nbsp;
            <select class="form-control-new" name="search_gudang" id="search_gudang">
                <option value="">All</option>
                <?php
                foreach ($mgudang as $val) {
                    $selected = "";
                    if ($search_gudang) {
                        if ($val["KdGudang"] == $search_gudang) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
                }
                ?>
            </select>
            &nbsp;<b>Divisi</b>&nbsp;
            <select class="form-control-new" name="search_divisi" id="search_divisi">
                <option value="">All</option>
                <?php
                foreach ($mdivisi as $val) {
                    $selected = "";
                    if ($search_divisi) {
                        if ($val["KdDivisi"] == $search_divisi) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
                }
                ?>
            </select>  
            &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status">
                <option value="">All</option>
                <option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Open</option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2">Void</option>
            </select>  
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/permintaan_barang/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="150"><center>No Dokumen</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th width="100"><center>Tgl Kebutuhan</center></th><!-- dulunya estimasi terima -->
		        <th><center>Keterangan</center></th>
		        <th><center>Gudang</center></th>
		        <th><center>Divisi</center></th>
		        <th width="100"><center>Status</center></th>
                <th width="100"><center>Navigasi</center></th>
                    <?
                    if($num_hasil>0){
                    ?>
                       <th width="100"><center>Action</center></th>
                    <?
                    }
                    ?>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == 0) {
                    $echo_status = "<font style='color:#ff1c1c'>Pending</font>";
                } else if ($val["Status"] == 1) {
                    $echo_status = "<font style='color:#6bcf29'>Open</font>";
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'>Void</font>";
                }
                ?>
                <tr title="<?php echo $val["NoDokumen"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $val["Tanggal"]; ?></td>
                    <td align="center"><?php echo $val["TglTerima"]; ?></td>
                    <td><?php echo $val["Keterangan"]; ?></td>
                    <td><?php echo $val["NamaGudang"]; ?></td>
                    <td><?php echo $val["NamaDivisi"]; ?></td>
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                        if ($val["Status"] == 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/permintaan_barang/edit_permintaan_barang/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>

                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }

                        if ($val["Status"] == 1) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/permintaan_barang/edit_permintaan_barang/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>

                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Print" title="" onclick="PopUpPrint('<?= $val['NoDokumen'];?>', '<?= base_url(); ?>');">
                                <i class="entypo-print"></i>
                            </button>

                            

                            <?php
                        }

                        if ($val["Status"] == 2) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/permintaan_barang/edit_permintaan_barang/<?php echo $val["NoDokumen"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>

                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoDokumen"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            <?php
                        }
                        ?>

                    </td>
                    <td align="center">
                         <?php 
                            if($num_hasil>0)
                            {
                              //  $link = base_url()."/inventory/form_otorisasi.php?kdtransaksi=PB&tipe=gudang&notransaksi=".$val["NoDokumen"];
                                $link = base_url().'index.php/transaksi/permintaan_barang/otoritas?NoDokumen=PB&tipe=gudang&Tanggal='.$val["Tanggal"].'&nodokumen='.$val["NoDokumen"];
                                 ?>
                             <img src='<?= base_url(); ?>public/images/flag_orange.gif' border='0' title='Void' onclick="VoidTrans('<? echo $link;?>');"></img>
                            <?php
                             }
                             ?>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
