<?php 

$this->load->view('header'); 

$modul = "Permintaan Barang";

//$arr_data(unset);

$counter=1;
foreach($market_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["market_list_name"][$counter]=$val["market_list_name"];
	$arr_data["pcode"][$counter]=$val["pcode"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	$arr_data["satuanst"][$counter]=$val["SatuanSt"];
	
	$counter++;
}
?>

<script language="javascript" src="<?=base_url();?>public/js/permintaan_barang.js"></script>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    } 

    function add_PCode()
    {
        targetBlank("<?=base_url()."inventory/npm_master_barang.php";?>");
    }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/permintaan_barang/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Tanggal Kebutuhan<font color="red"><b>(*)</b></font></td><!-- dulunya estimasi terima -->
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_est_terima" id="v_est_terima" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Divisi <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_divisi" id="v_divisi" style="width: 200px;">
	            		<option value="">Pilih Divisi</option>
	            		<?php
	            		foreach($mdivisi as $val)
	            		{
	            			
							?><option value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($mgudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["NamaGudang"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_keterangan" id="v_keterangan" maxlength="255" size="100"></td>
	        </tr>
            
            <tr>
                <td colspan="100%">
                    <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Add PCode" title="" name="btn_add_pcode" id="btn_add_pcode" value="Add PCode" onClick="add_PCode()">
                        <i class="entypo-plus"></i> Add PCode
                    </button>
                </td>
            </tr>
	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input readonly type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="toFormat2('v_qty<?php echo $no;?>')" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
				                <td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>" >
					                <option value=""> -- Pilih -- </option>
					                <?php        	                					                
					                /*foreach($msatuan as $value)
					                {
					                	?><option value="<?php echo $value["KdSatuan"] ; ?>"><?php echo $value["NamaSatuan"] ; ?></option><?php
									}*/									
					                ?>
				                	</select>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
	        		<div class="row" style="overflow:auto;height:300px;">
						<table class="table table-bordered responsive">
							<tbody>
							
								<?php
								$name_prev = "";
								foreach($arr_data["list_market"] as $counter => $val)
								{
									$market_list_name = $arr_data["market_list_name"][$counter];
									$pcode = $arr_data["pcode"][$counter];
									$namalengkap = $arr_data["namalengkap"][$counter];
									$satuanst = $arr_data["satuanst"][$counter];
									
									$name_echo = "&nbsp;";
	                                if($name_prev!=$market_list_name)
	                                {
	                                    $name_echo = $market_list_name;    
	                                } 
									
								?>
								<tr>
					                <td>
						                <input type="hidden" name="market_pcode[]" id="market_pcode<?php echo $counter;?>" value="<?php echo $pcode; ?>"/>
						                <input type="hidden" name="market_namabarang[]" id="market_namabarang<?php echo $counter;?>" value="<?php echo $namalengkap; ?>"/>
						                <input type="hidden" name="market_satuan[]" id="market_satuan<?php echo $counter;?>" value="<?php echo $satuanst; ?>"/>
						                <?php echo $name_echo; ?>
					                </td>
					                <td><?php echo $pcode; ?></td>
					                <td><?php echo $namalengkap; ?></td>
					                <td align="right">
					                	<input type="text" class="form-control-new" name="market_qty[]" onblur="toFormat2('market_qty<?php echo $counter;?>')" id="market_qty<?php echo $counter;?>" value="" style="text-align: right;"/>
					                </td>
					                <td><?php echo $satuanst; ?></td>
					            </tr>
								<?php	
								
									$name_prev = $market_list_name;							
								}
								?>
								
							</tbody>
						</table>
	        		</div>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/permintaan_barang/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>