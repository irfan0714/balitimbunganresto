<?php
$reset = chr(27) . '@';
$plength = chr(27) . 'C';
$lmargin = chr(27) . 'l';
$cond1   =chr(27).'!'.chr(15);
$cond = chr(15);
$ncond = chr(18);
$dwidth = chr(27) . '!' . chr(24);
$ndwidth = chr(27) . '!' . chr(14);
$draft = chr(27) . 'x' . chr(48);
$nlq = chr(27) . 'x' . chr(49);
$bold = chr(27) . 'E'.chr(15);
$nbold = chr(27) . 'F' .chr(15);
$uline = chr(27) . '!' . chr(129);
$nuline = chr(27) . '!' . chr(1);
$dstrik = chr(27) . 'G';
$ndstrik = chr(27) . 'H';
$elite = '';
$pica = chr(27) . 'P';
$height = chr(27) . '!' . chr(16);
$nheight = chr(27) . '!' . chr(1);
$spasi05 = chr(27) . "3" . chr(16);
$spasi1 = chr(27) . "3" . chr(24);
$fcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'i';
$pcut = chr(10) . chr(10) . chr(10) . chr(10) . chr(10) . chr(13) . chr(27) . 'm';
$op_cash = chr(27) . 'p' . chr(0) . chr(50) . chr(20) . chr(20);

//$ftext = printer_open("\\\\".$_SERVER['REMOTE_ADDR']."\\HPLaserJ");//
//$ftext = printer_open("\\\\192.168.0.75\\lcd_vpd");
//$ftext = printer_open("\\\\".$ip."\\".$nm_printer);
//$ftext = printer_open($_SERVER['REMOTE_ADDR']);
$ftext = printer_open("lcd_wpd");
printer_set_option($ftext, PRINTER_MODE, "raw");
printer_set_option($ftext, PRINTER_COPIES, "2");

	$atas = substr($nama,0,17); //Baris Atas
	$bawah = "Rp.".number_format($harga)."   (Rp.".number_format($total).")";			// baris bawah

printer_write($ftext, $reset . $elite);
printer_write($ftext, str_pad($atas, 19, " ", STR_PAD_RIGHT) . "\r\n");
//printer_write($ftext, str_pad(number_format($bawah, 0, ',', '.') , 20, " ", STR_PAD_LEFT) . "\r\n");
printer_write($ftext, $bawah);
printer_close($ftext);
?>