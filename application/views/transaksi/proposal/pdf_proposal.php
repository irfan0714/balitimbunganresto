
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Proposal</title>
    </head>
	<style>
		.border-table{
			border: 1px solid #191919;
			font-family: serif;
			border-collapse: collapse;
			font-size: 10pt;
		}
		
		.non-border-table{
			font-family: serif;
			border-collapse: collapse;
			font-size: 9pt;
			vertical-align: top;
		}
		
	</style>
    <body>
   
   <table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" >
            <tr>
                <td colspan="5" align="center"><img src="<?= base_url(); ?>public/images/Logosg.png" width="120" alt="Secret Garden Village"/></td>
            </tr>
            <tr>
                <td colspan="5" align="center"><b>PT. NATURA PESONA MANDIRI</b></td>
            </tr>
   </table>
   <hr>  
   <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
            <tr>
                <td colspan="5" align="center"><b><u>PROPOSAL PROGRAM</u></b></td>
            </tr>
            <tr>
                <td width="300" align="center" colspan="5"><span><?= "No : ".$nomor; ?></span></td>
            </tr>
    </table> <br>
    <table width="30%"  border="0" cellpadding="0" cellspacing="0" class="non-border-table">
                        <tr>
                            <td>Budget</td>
                            <td>:</td>
                            <td width="80" align="right"><span><?= number_format($getBudget[0]['budget'],0); ?></span></td>

                        </tr>
                        <tr>
                            <td>Terpakai</td>
                            <td>:</td>
                            <td align="right"><span><?= number_format($getBudget[0]['terpakai'],0); ?></span></td>

                        </tr>
                        <tr>
                            <td>Proposal</td>
                            <td>:</td>
                            <td align="right"><span><?= number_format($ProDetail[0]['jml_detail'] + $ProTarget[0]['jml_target'],0); ?></span></td>

                        </tr>
                        <tr>
                            <td>Sisa</td>
                            <td>:</td>
                            <td align="right"><span><?= number_format($getBudget[0]['budget']-($getBudget[0]['realisasi']+$ProDetail[0]['jml_detail'] + $ProTarget[0]['jml_target']),0); ?></span></td>

                        </tr>
                        <tr>
                            <td>Realisasi</td>
                            <td>:</td>
                            <td align="right"><span><?= number_format($getBudget[0]['realisasi'],0); ?></span></td>
                        </tr>
                        <tr>
                            <td>Budget - Realisasi</td>
                            <td>:</td>
                            <td align="right"><span><?= number_format($getBudget[0]['budget']-$getBudget[0]['realisasi'],0); ?></span></td>
                        </tr>
</table>
<br>
<table width="30%"  border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;" width="170">Latar Belakang</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;" width="600" height="40"><span>
                            	<?= $getBudget[0]['LatarBelakang']; ?>
                            </span></td>

                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Tujuan</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;" width="600" height="40"><span>
                            	<?= $getBudget[0]['Tujuan']; ?> 
                            </span></td>

                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><span>
                            	<?= $getBudget[0]['NamaProposal']; ?>
                            </span></td>

                        </tr>
                        <tr>
                            <td valign="top">Budget</td>
                            <td valign="top">:</td>
                            <td valign="top"><span>
                            	<?= $getBudget[0]['NamaAktivitas']; ?>
                            </span></td>

                        </tr>
                        <tr>
                            <td>Periode</td>
                            <td>:</td>
                            <td><span>
                            	<?= $getBudget[0]['Periode_awal']."/".$getBudget[0]['Periode_akhir']; ?>
                            </span></td>

                        </tr>
                         <tr>
                            <td style="vertical-align: top;" width="170">Mekanisme</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;" width="600" height="40"><span>
                            	<?= $getBudget[0]['Mekanisme']; ?>
                            </span></td>

                        </tr>
                        
</table>
<br>Detail :<br>
<table width="100%"  border="1" cellpadding="0" cellspacing="0" class="non-border-table">
					   <thead>
					   <tr>
					   	<th width="30">No</th>
					   	<th>Keterangan</th>
					   	<th width="70">Quantity</th>
					   	<th width="100">Harga</th>
					   	<th width="100">Jumlah</th>
					   	</tr>
					   </thead>
					   <tbody>
					   <?php 
					   $no=1;
					   foreach($detail as $val){?>
                        <tr>
                            <td align="center"><?= $no; ?></td>
                            <td><?= $val['Keterangan']; ?></td>
                            <td align="center"><?= $val['Qty']; ?></td>
                            <td align="right"><?= number_format($val['HargaSatuan'],0); ?></td>
                            <td align="right"><?= number_format($val['Jumlah'],0); ?></td>
                        </tr>
                        <?php $no++; }?>
                         <tr>
                            <td colspan="4" align="right"><b>Total</b></td>
                            <td align="right"><b><?= number_format($totaldetail[0]['Totaldetail'],0); ?></b></td>
                        </tr>
                        </tbody>
</table>
<?php if(!empty($target[0]['Keterangan'])){?>
<br>Target :<br>
<table width="100%"  border="1" cellpadding="0" cellspacing="0" class="non-border-table">
					   <thead>
					   <tr>
					   	<th width="30">No</th>
					   	<th>Keterangan</th>
					   	<th width="70">Quantity</th>
					   	<th width="100">Harga</th>
					   	<th width="100">Jumlah</th>
					   	</tr>
					   </thead>
					   <tbody>
					   <?php 
					   $no=1;
					   foreach($target as $val){?>
                        <tr>
                            <td align="center"><?= $no; ?></td>
                            <td><?= $val['Keterangan']; ?></td>
                            <td align="center"><?= $val['Qty']; ?></td>
                            <td align="right"><?= number_format($val['HargaSatuan'],0); ?></td>
                            <td align="right"><?= number_format($val['Jumlah'],0); ?></td>
                        </tr>
                        <?php $no++; }?>
                         <tr>
                            <td colspan="4" align="right"><b>Total</b></td>
                            <td align="right"><b><?= number_format($totaltarget[0]['Totaltarget'],0); ?></b></td>
                        </tr>
                        </tbody>
</table>
<?php } ?>
<br>
<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="non-border-table">
					  
                        <tr>
                            <td align="center">Dibuat Oleh,</td>
                            <td align="center">Disetujui Oleh,</td>
                            <td align="center">Disetujui Oleh,</td>
                            <td align="center">Mengetahui,</td>  
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>  
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>  
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>  
                        </tr>
                        <tr>
                            <td align="center"><u><?= $dibuatoleh[0]['employee_name']; ?></u></td>
                            <td align="center"><u><?= $approve1[0]['employee_name']; ?></u></td>
                            <td align="center"><u><?= $approve2[0]['employee_name']; ?></u></td>
                            <!--<td align="center"><u><?= $approve3[0]['employee_name']; ?></u></td>-->
                            <td align="center"><u>Bambang Sutirsno</u></td>  
                        </tr><tr>
                            <td align="center"><font size="1"><?= "<font color='white'>".$dibuatoleh[0]['jabatan_name']."</font>"; ?></font></td>
                            <td align="center"><font size="1"><?= $approve1[0]['jabatan_name']; ?></font></td>
                            <td align="center"><font size="1"><?= $approve2[0]['jabatan_name']; ?></font></td>
                            <!--<td align="center"><font size="1"><?= $approve3[0]['jabatan_name']; ?></font></td>--> 
                            <td align="center">&nbsp;</u></td>  
                        </tr>
                        
                      
</table>
   

</body>
</html>