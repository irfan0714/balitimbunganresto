<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'pengiriman',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/pengiriman.js"></script>
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="$('#no').focus()">
<form method='post' name="pengiriman" id="pengiriman" action='<?=base_url();?>index.php/transaksi/pengiriman/save_pengiriman' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Edit Pengiriman</legend>
			<table class="table_class_list">
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("No","nodok",$header->NoDokumen,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Tanggal","tgl",$header->TglDokumen,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Kendaraan","kendaraan",stripslashes($header->NamaKendaraan),"30","30","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Driver","driver",stripslashes($header->NamaPersonal),"30","30","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("No Polisi","nopol",stripslashes($header->NoPolisi),"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Keterangan","ket",stripslashes($header->Keterangan),"35","30","","text",$gantikursor,"1");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail </legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<div>
				<font style="font-size:15px;font-weight : bold;">No Pengambilan : </font><input type="text" id="no" name="no" onkeydown="keyShortcut(event,'no',this)">
				<font style="font-size:15px;font-weight : bold;">Scan Barcode : </font><input type="text" id="scan" name="scan" onkeydown="keyShortcut(event,'scan',this)">
				<select id="tipescan" name="tipescan" size="1">
				<option value="increase">Penambahan</option>
				<option value="decrease">Pengurangan</option>
				</select>
				</div>
				<br>
				<tr id="baris0">
					<td>NoPengambilan</td>
					<td>KdBarang</td>
					<td>NamaBarang</td>
					<td>Satuan</td>
					<td>Qty</td>
					<td>Satuan Jual</td>
					<td>Qty Sat Jual</td>
				</tr>
				<?php
				$modeller = new pengiriman_model();
				for($a=0;$a<count($detail);$a++)
				{
					$totqtykonfirm = 0;
					$var_counter_ambil = "";
					$counter_ambil = $modeller->findCounter($header->NoDokumen,$detail[$a]['PCode'],$detail[$a]['Counter'],$detail[$a]['NoPengambilan']);
					$nilai = $modeller->getQtySendEdit($header->NoDokumen,$detail[$a]['PCode'],$detail[$a]['NoPengambilan']);
					if(empty($nilai)){$kirim = 0;}
					else{
						$kirim = $nilai->Qty;
					}
					for($k=0;$k<count($counter_ambil);$k++)
					{
						$var_counter_ambil .= $counter_ambil[$k]['CounterPengambilan']."<>".$counter_ambil[$k]['Qty']."||";
						$qtykonfirm = $modeller->GetQtyConfirm($detail[$a]['NoPengambilan'],$detail[$a]['PCode'],$counter_ambil[$k]['CounterPengambilan']);
						$totqtykonfirm += (int)$qtykonfirm->QtyPcsKonfirm;
					}
					$sisa = (int)$totqtykonfirm - (int)$kirim;
					write_detail($qtybarcode,$a,$detail[$a]['NoPengambilan'],$detail[$a]['PCode'],$detail[$a]['NamaLengkap'],$detail[$a]['Qty'],$detail[$a]['QtyDisplay'],"",$detail[$a]['NamaSatuanKecil'],$detail[$a]['Satuan'],$detail[$a]['NamaSatuanJual'],"","",$detail[$a]['KdSatuanJual'],$detail[$a]['PCode'],$detail[$a]['Counter'],$detail[$a]['NoOrder'],$sisa,$var_counter_ambil);
				}
				?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="hidekendaraan" name="hidekendaraan" value="<?=$header->KdKendaraan?>">
				<input type='hidden' id="noambilkendaraan" name="noambilkendaraan" value="<?=$noambilkendaraan?>">
				<input type='hidden' id="notemp" name="notemp">
				<input type='hidden' id="pcodetemp" name="pcodetemp">
				<input type='hidden' id="flag" name="flag" value="edit">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/transaksi/pengiriman/" />
			</td>
		</tr>
	</table>
</form>

<?php
function write_detail($qtybarcode,$counter,$noambil,$pcode,$nama,$qty,$qtydisplay,$konverbk,$satuan,$nilsatuan,$satuanj,$konvertk,$konverjk,$kdsatuanj,$pcodebarang,$counters,$noorder,$qtysisa,$counterambil)
{
	$read = "";
	if($qtybarcode=="N")
	{
		$read = "readonly='readonly'";
	}
?>
<tr id="baris<?=$counter?>">
	<td nowrap><input type="text" id="noambil<?=$counter?>" name="noambil[]" size="10" maxlength="10" readonly="readonly" value="<?=$noambil?>"> </td>
	<td nowrap><input type="text" id="pcode<?=$counter?>" name="pcode[]" size="25" maxlength="20" readonly="readonly" value="<?=stripslashes($pcode)?>"> </td>
	<td nowrap><input type="text" id="nama<?=$counter?>" name="nama[]" size="25" readonly="readonly" value="<?=$nama?>"></td>
	<td nowrap><input type="text" id="satuan<?=$counter?>" name="satuan[]" size="15" readonly="readonly" value="<?=$satuan?>"></td>
	<td nowrap><input type="text" id="qty<?=$counter?>" name="qty[]" size="5" maxlength="11" <?=$read?> value="<?=$qty?>" onkeydown="keyShortcut(event,'qty',this)"></td>
	<td nowrap><input type="text" id="satuanj<?=$counter?>" name="satuanj[]" size="15" readonly="readonly"  value="<?=$satuanj?>"></td>
	<td nowrap><input type="text" id="qtydisplay<?=$counter?>" name="qtydisplay[]" size="15" readonly="readonly"  value="<?=$qtydisplay?>"></td>
	<td nowrap>
	<input type="hidden" id="counter<?=$counter?>" name="counter[]" value="<?=$counters?>">
	<input type="hidden" id="counterambil<?=$counter?>" name="counterambil[]" value="<?=$counterambil?>">
	<input type="hidden" id="konverbk<?=$counter?>" name="konverbk[]">
	<input type="hidden" id="konvertk<?=$counter?>" name="konvertk[]">
	<input type="hidden" id="konverjk<?=$counter?>" name="konverjk[]">
	<input type="hidden" id="kdsatuanj<?=$counter?>" name="kdsatuanj[]" value="<?=$kdsatuanj?>">
	<input type="hidden" id="pcodebarang<?=$counter?>" name="pcodebarang[]" value="<?=$pcodebarang?>">
	<input type="hidden" id="kdsatuan<?=$counter?>" name="kdsatuan[]" value="<?=$nilsatuan?>">
	<input type="hidden" id="qtypcsawal<?=$counter?>" name="qtypcsawal[]" value="<?=$qtysisa?>">
	<input type="hidden" id="noorder<?=$counter?>" name="noorder[]" value="<?=$noorder?>">
	<input type="hidden" id="savepcode<?=$counter?>" name="savepcode[]" value="<?=$pcode?>">
	</td>
</tr>
<?php
}
$this->load->view('footer'); ?>