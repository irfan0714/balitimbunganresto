<!DOCTYPE html>
<?php
$session_name=$this->session->userdata('username');
$gantikursor = "onkeydown=\"changeCursor(event,'pos_touch',this)\"";?>
<script src="<?php echo base_url();?>public/js/shortcuts_v1.js" type="text/javascript"></script>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<style type='text/css'>
body{
	cursor: pointer;
}
table tr td:active{
	background-color:#369;
	color:#FFF;
}
table tr td{
	border:1px solid #FFF;
}
.boxdisplay{
	color:black;
	font-size:15px;
	text-align:left;
	width:21%;
	height:60%;
	}
.sptombol{
	color:white;
	background-color:#666;
	font-size:30px;
	text-align:center;
	width:7%;
	height:12%;
	}
.angka{
	color:white;
	background-color:#666;
	font-size:30px;
	text-align:center;
	width:7%;
	height:12%;
	}
.angka2{
	color:red;
	background-color:white;
	font-size:30px;
	text-align:center;
	width:7%;
	height:12%;
	}
.subkategori{
	color:white;
	background-color:#333;
	font-size:15px;
	padding:5px;
	text-align:center;
	font-weight:bold;
	width:7%;
	height:12%;
	}
.quitbtn{
	color:white;
	background-color:#f00000;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.cancelbtn{
	color:white;
	background-color:#FF1000;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.jam{
	color:white;
	background-color:#999;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.menutengah{
	color:#369;
	background-color:#EEE;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.menutengahkosong{
	color:#FF0000;
	background-color:#EEE;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.mejaisi{
	color:red;
	background-color:#EEE;
	font-size:20px;
	font-weight: bold;
	text-align:center;
	width:7%;
	height:12%;
	}
.tabledisplay .menukanan{
	color:white;
	background-color:#999;
	font-size:17px;
	text-align:center;
	width:5%;
	height:12%;
	padding-left:1px;
	padding-right:1px;
	padding-top:2px;
	padding-bottom:2px;
	}
.tabledisplay .menukanan1{
	color:White;
	background-color:#ffa500;
	font-size:17px;
	text-align:center;
	width:5%;
	height:12%;
	padding-left:1px;
	padding-right:1px;
	padding-top:2px;
	padding-bottom:2px;
	}
.tabledisplay .menudone{
	color:White;
	background-color:#00b0c5;
	font-size:17px;
	text-align:center;
	width:5%;
	height:12%;
	padding-left:1px;
	padding-right:1px;
	padding-top:2px;
	padding-bottom:2px;
	}
.kategori{
	font-weight:bold;
	color:White;
	background-color:#666;
	font-size:15px;
	text-align:center;
	width:5%;
	height:12%;
	}
.nonaktif{
	font-weight:bold;
	color:White;
	background-color:#666;
	font-size:15px;
	text-align:center;
	width:5%;
	height:12%;
	opacity: 0.5;
	}
.kategori1{
	font-weight:bold;
	color:white;
	background-color:#ffa000;
	font-size:15px;
	text-align:center;
	width:5%;
	height:12%;
	}
.updown{
	padding-top:7px;
	padding-bottom:7px;
	color:white;
	background-color:#666666;
	font-size:20px;
	text-align:center;
	width:7%;
	height:12%;
	}
.updown1{
	color:white;
	background-color:#ffa500;
	font-size:15px;
	text-align:center;
	width:7%;
	height:12%;
	}
.column2{
	color:black;
	background-color:#CCC;
	font-weight:bold;
	font-size:30px;
	text-align:center;
	width:14%;
	height:12%;
	}
.column3{
	color:black;
	background-color:#CCC;
	font-size:20px;
	text-align:center;
	width:5%;
	height:12%;
	}
.menu1{
	color:white;
	background-color:#ff0000;
	font-size:20px;
	text-align:center;
	width:12%;
	height:21%;
	}
.tabledisplay{
	height:100%;
	width:100%;
	margin-bottom:0px;
	margin-top:0px;
	background-color:#EEE;
	border:5px solid #00b0c5;
}
.tabledisplay tr td:active{
	background-color:#EEE;
	color:black;
}
.tabledisplay tr td{
	margin:auto;
	padding:1px;
}
.displaytext{
	background-color:#FFF;
	cursor:pointer;
	color:black;
	font-size:15px;
	text-align:left;
	border:1px solid #00b0c5;
	height:22px;
	width:300px;
	text-align:left;
	}
.displaytext:active{
	background:#333;
	color:white;
	}
.displaytextatas{
	color:black;
	font-size:20px;
	width:380px;
	text-align:center;
	border:none;
	margin-left:auto;
	margin-right:auto;
	}
.boxdisplay2{
width:100;height:100;
}
</style>
<script language="javascript">
$(document).keypress(function(e) {
	if(window.event) // IE
	{
		var code = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		var code = e.which;
	}
	//alert(code);
	if (code == 13) 
	{
		$("#enter").click();
	}
	if ((code == 8) && ($("#keterangan").val()=="")&&($("#flagsearch").val()==0)) 
	{
		if($("#statusisi").val()=="menu" && ($("#keterangan").val()=="")&&($("#flagsearch").val()==0))
		{
			return false;
		}
		else
		{
			$("#del").click();
			return false;
		}
	}
	if (code == 45) 
	{
		$("#minus").click();
	}
	if (code == 43) 
	{
		$("#plus").click();
	}
	if (code == 48) 
	{
		$("#nol").click();
	}
	if (code == 49) 
	{
		$("#satu").click();
	}
	if (code == 50) 
	{
		$("#dua").click();
	}
	if (code == 51) 
	{
		$("#tiga").click();
	}
	if (code == 52) 
	{
		$("#empat").click();
	}
	if (code == 53) 
	{
		$("#lima").click();
	}
	if (code == 54) 
	{
		$("#enam").click();
	}
	if (code == 55) 
	{
		$("#tujuh").click();
	}
	if (code == 56) 
	{
		$("#delapan").click();
	}
	if (code == 57) 
	{
		$("#sembilan").click();
	}
	
});

//-----to lcd--
var start_i=0;

function toLCD(varsatu, vardua, vartiga) {
	var url	= 'http://<?=$iplokal?>/displaylcd/getlcd.php?varsatu='+varsatu+'&vardua='+vardua+'&vartiga='+vartiga+'&callback=?';	
	$.ajax({
		type: 'GET',
		url: url,
		async: false,
		jsonpCallback: 'jsonCallback',
		contentType: "application/json",
		dataType: 'jsonp'
	});
}

	function jsonCallback(dataarray) {
		//return messege nya dikirim ke console firebug 
		console.log(dataarray.pesan[0].msg);
	}
//-----eo to lcd--
function previewbill()
{
	lastRow = document.getElementsByName("pcode[]").length;
	//alert(lastRow+"*"+$("#pcode0").val()+"*"+$("#qty0").val());
	if(lastRow==1&&($("#pcode0").val()==""||$("#qty0").val()==""||$("#qty0").val()==0))
	{
	   alert("Detail barang belum ada");
	}else
	{
		kassa 		    = $('#kassa').val();
		kasir           = $('#kasir').val();
		store	        = $('#store').val();
		total_rupiah	= $('#totalsales').val();
		pb1_rupiah	    = $('#taxsales').val();
		sc_rupiah	    = $('#scsales').val();
		netto_rupiah	= $('#nettosales').val();
		kdmeja			= $('#kdmeja').val();
		totalguest		= $('#idguest').val();
		discount		= $('#idpersendiscount').val();
		voucher 		= parseFloat($("#idbayarvoucher").val());
		idagent		= $('#idagent').val();
		
		header = kassa+'_'+kasir+'_'+store+'_'+
				 total_rupiah+'_'+pb1_rupiah+'_'+netto_rupiah+'_'+sc_rupiah+'_'+discount+'_'+idagent+'_'+voucher+'_mxmxm_'+kdmeja+'_mxmxm_gxgxg_'+totalguest+'_dxdxd_end';
		
		detail = '';
		lastRow = document.getElementsByName("pcode[]").length;
		for(index=0;index<lastRow;index++)
		{
			indexs = index; 
			detail += $("#pcode"+indexs).val()+"_"+
					  $("#nama"+indexs).val()+"_"+
					  $("#qty"+indexs).val()+"_"+
					  $("#satuan"+indexs).val()+"_"+
					  $("#harga"+indexs).val()+"_"+
					  $("#disc"+indexs).val()+"_"+
					  $("#pdisc"+indexs).val()+
					  $("#pservicecharge"+indexs).val()+"_"+"axaxa";
		}
		kirim = header+'bxbxb'+detail;
		//alert(kirim);
		base_url = $("#baseurl").val();
		//alert(base_url+'index.php/transaksi/pos/preview/'+kirim);
		//$.post(base_url+'index.php/transaksi/order_touch/preview/'+kirim,{kirim:kirim},
		//function(datakode){
		//});
		document.location = base_url+'index.php/transaksi/order_touch/preview/'+kirim;
	}		
}

function clearangka()
{
   $("#nol").attr({"class":"angka"})
   $("#satu").attr({"class":"angka"})
   $("#dua").attr({"class":"angka"})
   $("#tiga").attr({"class":"angka"})
   $("#empat").attr({"class":"angka"})
   $("#lima").attr({"class":"angka"})
   $("#enam").attr({"class":"angka"})
   $("#tujuh").attr({"class":"angka"})
   $("#delapan").attr({"class":"angka"})
   $("#sembilan").attr({"class":"angka"})
}

function clearmenukanan()
{
   $("#waitress").attr({"class":"menukanan"})
   $("#table").attr({"class":"menukanan"})
   $("#guest").attr({"class":"menukanan"})
   $("#agent").attr({"class":"agent"})
}

function cekPassword(id)
{
	var lastRow = document.getElementsByName("id[]").length;
	id0 = parseInt(id.substr(0,3));
	pass0 = md5(id.substr(3,id.length-3));
	
	for(index=0;index<lastRow;index++){
		nama = document.getElementsByName("id[]");
		temp = nama[index].id;
		indexs = temp.substr(2,temp.length-2);
		//alert($("#id"+indexs).val()+'-'+$("#password"+indexs).val())
		
		if(id0==parseInt($("#id"+indexs).val()) && pass0 == $("#password"+indexs).val())
		{
		    $("#kdpersonal").val($("#id"+indexs).val())
			$("#namapersonal").val($("#username"+indexs).val())
			return true;
		}
	}
	alert("Password salah");
	clearIsian();
	clearMenuTengah();
    return false;
}

function setLokasi()
{
	kategorikassa = $("#kategorikassa").val();
	if(kategorikassa!="03")
	{
    start = parseFloat($("#menutengah").val());
	lastRow = parseFloat($("#lenlokasi").val());
	if(lastRow-start>14)
	   lastRow1=14+start;
	else
	   lastRow1=lastRow;
	//alert(start);
	//alert(lastRow);
	//alert(lastRow1);
	for(index=1;index<=15;index++){
	    if(start+index-2<lastRow)
		{
		   nama = document.getElementsByName("namalokasi[]");
		   temp = nama[start+index-2].id;
		   indexs = temp.substr(10,temp.length-10);
		//alert($("#id"+indexs).val()+$("#password"+indexs).val())
		   namalokasi = $("#namalokasi"+indexs).val();
		   kdlokasi = $("#kdlokasi"+indexs).val();
		   adaisi = $("#adaisi"+indexs).val();
		}else
		{
		   namalokasi = "";
		   kdlokasi = "";
		   adaisi = 0;
		}
		
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = namalokasi;
		if(parseFloat(adaisi)==1){
			//$("#menutengah"+urutan).css('color', 'red');
			$("#menutengah"+urutan).attr({"value":kdlokasi, "class":"mejaisi"});
		}
		else{
			//$("#menutengah"+urutan).css('color', 'blue');
			$("#menutengah"+urutan).attr({"value":kdlokasi, "class":"menutengah"});
		}
	}
	}
}

function setPayment()
{
	    idbayartunai = parseFloat($("#idbayartunai").val());
		idbayarkredit = parseFloat($("#idbayarkredit").val());
		idbayardebet = parseFloat($("#idbayardebet").val());
		idbayarvoucher = parseFloat($("#idbayarvoucher").val());
		idbayarcompliment = parseFloat($("#idbayarcompliment").val());
		idpersendiscount = parseFloat($("#idpersendiscount").val());
		nettosales = parseFloat($("#nettosales").val());
		idnilai1valas = parseFloat($("#idnilai1valas").val());
		idnilai2valas = parseFloat($("#idnilai2valas").val());
		id1kurs = parseFloat($("#id1kurs").val());
		id2kurs = parseFloat($("#id2kurs").val());
		
		totalbayar = idbayartunai + idbayarkredit + idbayardebet + idbayarvoucher + idbayarcompliment + (idnilai1valas*id1kurs) + (idnilai2valas*id2kurs);
		sisabayar = totalbayar - nettosales;
		
		var text = document.getElementById("menutengah01");
	    text.innerHTML = "Tunai*"+"<br>"+idbayartunai;
		$("#menutengah01").attr({"value":"Tunai"});
		var text = document.getElementById("menutengah02");
	    text.innerHTML = "Kartu Kredit*"+"<br>"+idbayarkredit;
		$("#menutengah02").attr({"value":"Kredit"});
		var text = document.getElementById("menutengah03");
	    text.innerHTML = "Kartu Debit*"+"<br>"+idbayardebet;
		$("#menutengah03").attr({"value":"Debit"});
		var text = document.getElementById("menutengah04");
	    text.innerHTML = "Voucher*"+"<br>"+idbayarvoucher;
		$("#menutengah04").attr({"value":"Voucher"});
		var text = document.getElementById("menutengah05");
	    text.innerHTML = "Compliment*"+"<br>"+idbayarcompliment;
		$("#menutengah05").attr({"value":"Compliment"});
		var text = document.getElementById("menutengah06");
	    text.innerHTML = "Discount*"+"<br>"+idpersendiscount;
		$("#menutengah06").attr({"value":"Discount"});
		var text = document.getElementById("menutengah07");
	    text.innerHTML = "CNY*"+"<br>"+idnilai1valas;
		$("#menutengah07").attr({"value":"CNY"});
		var text = document.getElementById("menutengah08");
		text.innerHTML = "USD*"+"<br>"+idnilai2valas;
		$("#menutengah08").attr({"value":"USD"});

			var text = document.getElementById("menutengah13");
			text.innerHTML = "Total Sales*"+"<br>"+nettosales;
			$("#menutengah13").attr({"value":"TotalSales"});
			var text = document.getElementById("menutengah14");
			text.innerHTML = "Total Bayar*"+"<br>"+totalbayar;
			$("#menutengah14").attr({"value":"TotalBayar"});
			var text = document.getElementById("menutengah15");
			text.innerHTML = "Sisa Bayar*"+"<br>"+sisabayar;
			$("#menutengah14").attr({"value":"SisaBayar"});		
}

function setKeterangan()
{
    start = parseFloat($("#menutengah").val());
	lastRow = parseFloat($("#lenketr").val());
	if(lastRow-start>14)
	   lastRow1=14+start;
	else
	   lastRow1=lastRow;
	//alert(start);
	//alert(lastRow);
	//alert(lastRow1);
	for(index=1;index<=15;index++){
	    if(start+index-2<lastRow)
		{
		   nama = document.getElementsByName("ketrdtl[]");
		   temp = nama[start+index-2].id;
		   indexs = temp.substr(7,temp.length-7);
		//alert($("#id"+indexs).val()+$("#password"+indexs).val())
		   ketrdtl = $("#ketrdtl"+indexs).val();
		}else
		{
		   ketrdtl = "";
		}
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = ketrdtl;
		$("#menutengah"+urutan).attr({"value":ketrdtl});
	}
}

function setupmenu(jenis,kode)
{   
	console.log("test "+kode);
   if(jenis==1)
   {
		index01 = "";
		index02 = "";
		index03 = "";
		subindex0101 = "";
		subindex0102 = "";
		subindex0103 = "";
		subindex0104 = "";
		subindex0105 = "";
		subindex0106 = "";
		subindex0107 = "";
		subindex0108 = "";
		subindex0109 = "";
		subindex0110 = "";
		subindex0111 = "";
		subindex0112 = "";
		subindex0113 = "";
		subindex0114 = "";
		subindex0115 = "";
		subindex0116 = "";
		subindex0201 = "";
		subindex0202 = "";
		subindex0203 = "";
		subindex0204 = "";
		subindex0205 = "";
		subindex0206 = "";
		subindex0207 = "";
		subindex0208 = "";
		subindex0209 = "";
		subindex0210 = "";
		subindex0211 = "";
		subindex0212 = "";
		subindex0213 = "";
		subindex0214 = "";
		subindex0215 = "";
		subindex0301 = "";
		subindex0302 = "";
		subindex0303 = "";
		subindex0304 = "";
		subindex0305 = "";
		subindex0306 = "";
		subindex0307 = "";
		subindex0308 = "";
		subindex0309 = "";
		subindex0310 = "";
		subindex0311 = "";
		subindex0312 = "";
		subindex0313 = "";
		subindex0314 = "";
		subindex0315 = "";
		subindex0316 = "";
		subindex0317 = "";
		subindex0318 = "";

		subindex0116 = "";
		subindex0117 = "";
		subindex0118 = "";
		subindex0119 = "";
		subindex0120 = "";
		subindex0121 = "";
		subindex0122 = "";
        
      if(kode=="00")
		{
			indexid = 0;
			lastRow = document.getElementsByName("namalengkap[]").length;
	
			for(index=0;index<lastRow;index++){
			   nama = document.getElementsByName("kdkategori[]");
			   temp = nama[index].id;
			   indexs = temp.substr(10,temp.length-10);
			   kategori = $("#kdkategori"+indexs).val();
			   subkategori = $("#kdsubkategori"+indexs).val();
			   //if(index<=5)
			   //{
			   //alert(kode);alert(kategori);
			   //}
			   if(kategori=='01')
			   {
				  index01 = index01 + indexs + "*";
			   }
			   if(kategori=='02')
			   {
				  index02 = index02 + indexs + "*";
			   }
			   if(kategori=='03')
			   {
				  index03 = index03 + indexs + "*";
			   }
			   if(subkategori=='0101')
			   {
				  subindex0101 = subindex0101 + indexs + "*";
			   }
			   if(subkategori=='0102')
			   {
				  subindex0102 = subindex0102 + indexs + "*";
			   }
			   if(subkategori=='0103')
			   {
				  subindex0103 = subindex0103 + indexs + "*";
			   }
			   if(subkategori=='0104')
			   {
				  subindex0104 = subindex0104 + indexs + "*";
			   }
			   if(subkategori=='0105')
			   {
				  subindex0105 = subindex0105 + indexs + "*";
			   }
			   if(subkategori=='0106')
			   {
				  subindex0106 = subindex0106 + indexs + "*";
			   }
			   if(subkategori=='0107')
			   {
				  subindex0107 = subindex0107 + indexs + "*";
			   }
			   if(subkategori=='0108')
			   {
				  subindex0108 = subindex0108 + indexs + "*";
			   }
			   if(subkategori=='0109')
			   {
				  subindex0109 = subindex0109 + indexs + "*";
			   }
			   if(subkategori=='0110')
			   {
				  subindex0110 = subindex0110 + indexs + "*";
			   }
			   if(subkategori=='0111')
			   {
				  subindex0111 = subindex0111 + indexs + "*";
			   }
			   if(subkategori=='0112')
			   {
				  subindex0112 = subindex0112 + indexs + "*";
			   }
			   if(subkategori=='0113')
			   {
				  subindex0113 = subindex0113 + indexs + "*";
			   }
			   if(subkategori=='0114')
			   {
				  subindex0114 = subindex0114 + indexs + "*";
			   }
			   if(subkategori=='0115')
			   {
				  subindex0115 = subindex0115 + indexs + "*";
			   }
			   if(subkategori=='0116')
			   {
				  subindex0116 = subindex0116 + indexs + "*";
			   }
			   if(subkategori=='0201')
			   {
				  subindex0201 = subindex0201 + indexs + "*";
			   }
			   if(subkategori=='0202')
			   {
				  subindex0202 = subindex0202 + indexs + "*";
			   }
			   if(subkategori=='0203')
			   {
				  subindex0203 = subindex0203 + indexs + "*";
			   }
			   if(subkategori=='0204')
			   {
				  subindex0204 = subindex0204 + indexs + "*";
			   }
			   if(subkategori=='0205')
			   {
				  subindex0205 = subindex0205 + indexs + "*";
			   }
			   if(subkategori=='0206')
			   {
				  subindex0206 = subindex0206 + indexs + "*";
			   }
			   if(subkategori=='0207')
			   {
				  subindex0207 = subindex0207 + indexs + "*";
			   }
			   if(subkategori=='0208')
			   {
				  subindex0208 = subindex0208 + indexs + "*";
			   }
			   if(subkategori=='0209')
			   {
				  subindex0209 = subindex0209 + indexs + "*";
			   }
			   if(subkategori=='0210')
			   {
				  subindex0210 = subindex0210 + indexs + "*";
			   }
			   if(subkategori=='0211')
			   {
				  subindex0211 = subindex0211 + indexs + "*";
			   }
			   if(subkategori=='0212')
			   {
				  subindex0212 = subindex0212 + indexs + "*";
			   }
			   if(subkategori=='0213')
			   {
				  subindex0213 = subindex0213 + indexs + "*";
			   }
			   if(subkategori=='0214')
			   {
				  subindex0214 = subindex0214 + indexs + "*";
			   }
			   if(subkategori=='0215')
			   {
				  subindex0215 = subindex0215 + indexs + "*";
			   }
			   if(subkategori=='0301')
			   {
				  subindex0301 = subindex0301 + indexs + "*";
			   }
			   if(subkategori=='0302')
			   {
				  subindex0302 = subindex0302 + indexs + "*";
			   }
			   if(subkategori=='0303')
			   {
				  subindex0303 = subindex0303 + indexs + "*";
			   }
			   if(subkategori=='0304')
			   {
				  subindex0304 = subindex0304 + indexs + "*";
			   }
			   if(subkategori=='0305')
			   {
				  subindex0305 = subindex0305 + indexs + "*";
			   }
			   if(subkategori=='0306')
			   {
				  subindex0306 = subindex0306 + indexs + "*";
			   }
			   if(subkategori=='0307')
			   {
				  subindex0307 = subindex0307 + indexs + "*";
			   }
			   if(subkategori=='0308')
			   {
				  subindex0308 = subindex0308 + indexs + "*";
			   }
			   if(subkategori=='0309')
			   {
				  subindex0309 = subindex0309 + indexs + "*";
			   }
			   if(subkategori=='0310')
			   {
				  subindex0310 = subindex0310 + indexs + "*";
			   }
			   if(subkategori=='0311')
			   {
				  subindex0311 = subindex0311 + indexs + "*";
			   }
			   if(subkategori=='0312')
			   {
				  subindex0312 = subindex0312 + indexs + "*";
			   }
			   if(subkategori=='0313')
			   {
				  subindex0313 = subindex0313 + indexs + "*";
			   }
			   if(subkategori=='0314')
			   {
				  subindex0314 = subindex0314 + indexs + "*";
			   }
            if(subkategori=='0315')
            {
               subindex0315 = subindex0315 + indexs + "*";
            }
            if(subkategori=='0316')
            {
               subindex0316 = subindex0316 + indexs + "*";
            }
            if(subkategori=='0317')
            {
               subindex0317 = subindex0317 + indexs + "*";
            }
            if(subkategori=='0318')
            {
               subindex0318 = subindex0318 + indexs + "*";
            }
            if(subkategori=='0116')
            {
               subindex0116 = subindex0116 + indexs + "*";
            }
            if(subkategori=='0117')
            {
               subindex0117 = subindex0117 + indexs + "*";
            }
            if(subkategori=='0118')
            {
               subindex0118 = subindex0118 + indexs + "*";
            }
            if(subkategori=='0119')
            {
               subindex0119 = subindex0119 + indexs + "*";
            }
            if(subkategori=='0120')
            {
               subindex0120 = subindex0120 + indexs + "*";
            }
            if(subkategori=='0121')
            {
               subindex0121 = subindex0121 + indexs + "*";
            }
            if(subkategori=='0122')
            {
               subindex0122 = subindex0122 + indexs + "*";
            }



			}
			$("#index01").val(index01);
			$("#index02").val(index02);
			$("#index03").val(index03);
			$("#indexa").val(index01);
			$("#subindex0101").val(subindex0101);
			$("#subindex0102").val(subindex0102);
			$("#subindex0103").val(subindex0103);
			$("#subindex0104").val(subindex0104);
			$("#subindex0105").val(subindex0105);
			$("#subindex0106").val(subindex0106);
			$("#subindex0107").val(subindex0107);
			$("#subindex0108").val(subindex0108);
			$("#subindex0109").val(subindex0109);
			$("#subindex0110").val(subindex0110);
			$("#subindex0111").val(subindex0111);
			$("#subindex0112").val(subindex0112);
			$("#subindex0113").val(subindex0113);
			$("#subindex0114").val(subindex0114);
			$("#subindex0115").val(subindex0115);
			$("#subindex0116").val(subindex0116);
			$("#subindex0201").val(subindex0201);
			$("#subindex0202").val(subindex0202);
			$("#subindex0203").val(subindex0203);
			$("#subindex0204").val(subindex0204);
			$("#subindex0205").val(subindex0205);
			$("#subindex0206").val(subindex0206);
			$("#subindex0207").val(subindex0207);
			$("#subindex0208").val(subindex0208);
			$("#subindex0209").val(subindex0209);
			$("#subindex0210").val(subindex0210);
			$("#subindex0211").val(subindex0211);
			$("#subindex0212").val(subindex0212);
			$("#subindex0213").val(subindex0213);
			$("#subindex0214").val(subindex0214);
			$("#subindex0215").val(subindex0215);
			$("#subindex0301").val(subindex0301);
			$("#subindex0303").val(subindex0303);
			$("#subindex0303").val(subindex0303);
			$("#subindex0304").val(subindex0304);
			$("#subindex0305").val(subindex0305);
			$("#subindex0306").val(subindex0306);
			$("#subindex0307").val(subindex0307);
			$("#subindex0308").val(subindex0308);
			$("#subindex0309").val(subindex0309);
			$("#subindex0310").val(subindex0310);
			$("#subindex0311").val(subindex0311);
			$("#subindex0312").val(subindex0312);
			$("#subindex0313").val(subindex0313);
			$("#subindex0314").val(subindex0314);
            $("#subindex0315").val(subindex0315);
            $("#subindex0316").val(subindex0316);
            $("#subindex0317").val(subindex0317);
            $("#subindex0318").val(subindex0318);

         $("#subindex0118").val(subindex0118);
         $("#subindex0119").val(subindex0119);
         $("#subindex0120").val(subindex0120);
         $("#subindex0121").val(subindex0121);
         $("#subindex0122").val(subindex0122);
			//alert(index01);
			//clearSubKategori();
		    //setSubKategori('01');
		}
		else
		{
		    clearSubKategori();
		    setSubKategori(kode);
		    $("#indexa").val($("#index"+kode).val());
		}
   }
   if(jenis==2)
   {
       $("#indexa").val($("#subindex"+kode).val());

   }
}

function setupfilter(kode)
{   
	indexid = 0;
	lastRow = document.getElementsByName("namalengkap[]").length;
    index01 = "";
	for(index=0;index<lastRow;index++){
	   nama = document.getElementsByName("kdkategori[]");
	   temp = nama[index].id;
	   indexs = temp.substr(10,temp.length-10);
	   namabarang = $("#namalengkap"+indexs).val();
	   posisi = namabarang.search(kode);
	   if(posisi>=0)
	   {
		  index01 = index01 + indexs + "*";
	   }
	}
	$("#indexa").val(index01);
}

function setMenu()
{
    start = parseFloat($("#menutengah").val());
	//lastRow = parseFloat($("#lenbarang").val());
	lastRow = document.getElementsByName("namalengkap[]").length;
	if(lastRow-start>14)
	{
	   lastRow1=14+start;
	}
	else
	{
		lastRow1=lastRow;
	}
	//alert(start);
	//alert(lastRow);
	//alert(lastRow1);
	indexa = $("#indexa").val();
	//alert(indexa);
    indexa = indexa.split('*');
	lastRow = indexa.length-1;
	
	for(index=1;index<=15;index++){
		//alert(start+index-2);
		//alert(lastRow);
		
	    if(start+index-2<lastRow)
		{
		   nama = document.getElementsByName("namalengkap[]");
		   temp = nama[start+index-2].id;
		   indexs = temp.substr(8,temp.length-8);
		   indexs = indexa[start+index-2];
		   namalengkap = $("#namalengkap"+indexs).val();
		   pcode = $("#pcodebrg"+indexs).val();
		   flagready = $("#flagready"+indexs).val();
		   satuan = $("#satuanbrg"+indexs).val();
		   harga = $("#hargabrg"+indexs).val();
		   pdisc = $("#discbrg"+indexs).val();
		   komisi = $("#komisibrg"+indexs).val();
		   printer = $("#printerbrg"+indexs).val();
		   //alert(harga);
		   //alert(komisi);
		}else
		{
		   namalengkap = "";
		   pcode = "";
		   flagready= "T";
		}
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    if(flagready=="T")
		{
			$("#menutengah"+urutan).attr({"class":"menutengahkosong"});
			text.innerHTML = namalengkap;
		}
		if(flagready=="Y")
		{
			$("#menutengah"+urutan).attr({"class":"menutengah"});
			text.innerHTML = namalengkap;
		}
		$("#menutengah"+urutan).attr({"value":pcode});
		$("#satuan"+urutan).attr({"value":satuan});
		$("#dharga"+urutan).attr({"value":harga});
		$("#pdisc"+urutan).attr({"value":pdisc});
		$("#printer"+urutan).attr({"value":printer});
		$("#komisi"+urutan).attr({"value":komisi});
	}
}

function setSubKategori(kode)
{
	console.log(kode);
    //start = parseFloat($("#menutengah").val());
	//lastRow = parseFloat($("#lenbarang").val());
	lastRow = document.getElementsByName("namasubkategori[]").length;
	kolom = 1;
	for(index=0;index<lastRow;index++){
    	   nama = document.getElementsByName("namasubkategori[]");
		   temp = nama[index].id;
		   indexs = temp.substr(15,temp.length-15);
		   kdsubkategori = $("#_kdsubkategori"+indexs).val();
		   kdkategori = $("#_kdkategori"+indexs).val();
		   namasubkategori = $("#namasubkategori"+indexs).val();
		   //alert(indexs);
		   //alert(namalengkap);
           
		   //alert(kode+"-"+kdkategori+"-"+kdsubkategori+namasubkategori);
		   if(kdkategori==kode)
		   {
			   urutan0 = "0"+parseFloat(kolom);
			   urutan=urutan0.substr(-2);
				//alert(urutan);
			   var text = document.getElementById("subkategori"+urutan);
			   text.innerHTML = namasubkategori;
			   $("#subkategori"+urutan).attr({"value":kdsubkategori});
			   kolom = kolom + 1;
		   }
	}
}

function clearMenuTengah()
{
	for(index=1;index<=15;index++){
	    namamenu = "";
	    kodemenu = "";
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("menutengah"+urutan);
	    text.innerHTML = namamenu;
		$("#menutengah"+urutan).attr({"value":kodemenu});
	}
	$("#menutengah").val(1);
}

function clearSubKategori()
{
	for(index=1;index<=14;index++){
	    namamenu = "";
	    kodemenu = "";
		urutan0 = "0"+parseFloat(index);
		urutan=urutan0.substr(-2);
		//alert(urutan);
		var text = document.getElementById("subkategori"+urutan);
	    text.innerHTML = namamenu;
		$("#subkategori"+urutan).attr({"value":kodemenu});
	}
}

function clearIsian()
{
    $("#hdisplay0").val("");
	$("#hdisplay1").val("");
	$("#dtext01").val("");
	$("#dtext02").val("");
	$("#dtext02").attr('disabled',true);
	$("#dtext02").attr({"placeholder":""});
}

function kosongarray()
{
	var lastRow = document.getElementsByName("pcode[]").length;
	//alert(lastRow);
	for(index=1;index<lastRow;index++){
	    //alert(index);
		//displaytext();
		nama = document.getElementsByName("pcode[]");
		temp = nama[index].id;
		//alert(index+";"+temp);
		indexs = temp.substr(5,temp.length-5);
		$('#baris'+indexs).remove();
	}
	$("#nourut0").val(1);
	$("#pcode0").val("");
	$("#tmppcode0").val("");
	$("#nama0").val("");
	$("#tmpqty0").val(0);
	$("#qty0").val(0);
	$("#harga0").val(0);
	$("#printer0").val("");
	$("#komisi0").val("");
	$("#satuan0").val("");
	$("#keterangan0").val("");
	$(".displaytext").val("");
	
	$("#idbayartunai").val(0);
	$("#idbayarkredit").val(0);
	$("#idbayardebet").val(0);
	$("#idbayarvoucher").val(0);
	$("#idbayarcompliment").val(0);
	$("#idpersendiscount").val(0);
	$("#iddiscountasli").val(0);
	$("#iduserdiscount").val("");
}

function addIt2(cKey)
{
    id = cKey.getAttribute("id");
    totalitem = document.getElementsByName("pcode[]").length;
	transaksi = $('#transaksi').val();
	idpayment = $('#idpayment').val();
	statusisi = $('#statusisi').val();
	
	
	idpersendiscount = $('#idpersendiscount').val();
	iddiscountasli = $('#iddiscountasli').val();
	
	if(transaksi=="yes")
	{
	    if(id=="commit")
		{		   
	        if(idpersendiscount!=iddiscountasli)
			{
				alert("Discount perlu otorisasi ulang!");
			}else
			{
				nilaibayar = parseFloat($('#idnilaibayar').val());
				nettosales = parseFloat($("#nettosales").val());
				jenisbayar = $("#idbayar").val();
				
				if(jenisbayar=="T")
				   $("#idbayartunai").val(nilaibayar);
				if(jenisbayar=="K")
				   $("#idbayarkredit").val(nilaibayar);
				if(jenisbayar=="D")
				   $("#idbayardebet").val(nilaibayar);
				if(jenisbayar=="V")
				   $("#idbayarvoucher").val(nilaibayar);
				if(jenisbayar=="C")
				   $("#idbayarcompliment").val(nilaibayar);
				
				idbayartunai = parseFloat($("#idbayartunai").val());
				idbayarkredit = parseFloat($("#idbayarkredit").val());
				idbayardebet = parseFloat($("#idbayardebet").val());
				idbayarvoucher = parseFloat($("#idbayarvoucher").val());
				idbayarcompliment = parseFloat($("#idbayarcompliment").val());
				nettosales = parseFloat($("#nettosales").val());
				idnilai1valas = parseFloat($("#idnilai1valas").val());
				idnilai2valas = parseFloat($("#idnilai2valas").val());
				id1kurs = parseFloat($("#id1kurs").val());
				id2kurs = parseFloat($("#id2kurs").val());
		
				totalbayar = idbayartunai + idbayarkredit + idbayardebet + idbayarvoucher + idbayarcompliment  + (id1kurs*idnilai1valas) + (id2kurs*idnilai2valas);
				sisabayar = totalbayar - nettosales;
				
				//alert(totalbayar);
				//alert(nettosales);
				//alert(nilaibayar < nettosales);
				//Konfirmasi("Proses Order ini?");
				if(confirm("Proses Order ini?"))
					{
					   $('#idnilaibayar').val(totalbayar);
					   $("#pos_touch").submit();
					}
			}
		}
		if(id=="quit")
		{
			if(confirm("Keluar dari transaksi ini?"))
			{
			   //kosongarray();
			   //displaytext();
			   $('#transaksi').val("no");
			   $("#pos_touch").submit();
			}
		}
		if(id=="cancel")
		{
			if(confirm("Hapus transaksi order ini?"))
			{
				$('#transaksi').val("no");
				kosongarray();
				clearIsian();
				clearMenuTengah();
				kategorikassa = $("#kategorikassa").val();
				if(kategorikassa=="01")
				{
					//$("#statusisi").val("table");
					$("#statusisi").val("waitress");
					var tbl = document.getElementById('table');
					var gst = document.getElementById('guest');
					var kms = document.getElementById('agent');
					var wtr = document.getElementById('waitress');
					wtr.innerHTML = "";
					tbl.innerHTML = "Table"+" </br> -";
					gst.innerHTML = "Guest"+" </br> -";
					kms.innerHTML = "";
					$("#waitress").attr({"class":"menukanan1"});
					$("#table").attr({"class":"menukanan"});
					$("#guest").attr({"class":"menukanan"});
					$("#agent").attr({"class":"menukanan"});
				}else
				{
					$("#statusisi").val("agent");
					var tbl = document.getElementById('table');
					var gst = document.getElementById('guest');
					var kms = document.getElementById('agent');
					tbl.innerHTML = "BTA";
					gst.innerHTML = "Guest"+" </br> -";
					kms.innerHTML = "";
					$("#table").attr({"class":"menudone"});
					$("#guest").attr({"class":"menukanan"});
					$("#agent").attr({"class":"menukanan1"});
				}
			}
		}
    }
	else
	{
		if(id=="quit")
		{
			if(confirm("Keluar dari transaksi order?"))
			{
			   $("#pos_touch").submit();
			}
		}
	}
}

function addIt(cKey)//iso1
{
	
    //alert(cKey.getAttribute("id"));
	console.log(cKey);    
	id = cKey.getAttribute("id");
	val = cKey.getAttribute("value");
	nclass = cKey.getAttribute("class");
  
    //alert("val "+val);
    
    
	
	prevtext = $("#prevtext").val();
	prevclass = $("#prevclass").val();
	prevmenu = $("#prevmenu").val();
	statusketr = $("#statusketr").val();
	//if((id!="plus")&&(id!="minus")&&(id!="enter")&&(id!="kategori01")&&(id!="kategori02")&&(id!="kategori03"))
	
	textawal00 = document.getElementById(id).innerHTML;
	textawal01 = textawal00.split("*");
	text = textawal01[0];
	//alert(text);


	if(id=="preview")
	{
	   previewbill();
	}
	if(nclass=="menutengah" || nclass=="mejaisi")
	{
		
	   if(text!="")
	   {
		   if(statusketr==1)
		   {
				$("#dtext02").val(text);
				$("#dtext02").focus();
				$("#keterangan").val($("#dtext02").val());
		   }
		   else
		   {
				if($("#pcode").val() == val)
				{
					$("#qty").val((parseFloat($("#qty").val()))+1);
				}else
				{
					$("#qty").val(1);
				}
			   $("#pcode").val(val);
			   $("#nmbrg").val(text);
			   satuanid = id.substr(-2);
			   satuan = $("#satuan"+satuanid).val();
			   $("#satuan").val(satuan);
			   harga = $("#dharga"+satuanid).val();
			   $("#harga").val(harga);
			   pdisc = $("#pdisc"+satuanid).val();
			   $("#pdisc").val(pdisc);
			   komisi = $("#komisi"+satuanid).val();
			   $("#komisi").val(komisi);
			   printer = $("#printer"+satuanid).val();
			   $("#printer").val(printer);
		   }
	   }else{
			$("#qty").val("");
	   }
	}
	else
	   text = prevtext;
	//alert('**'+text+'**');
				  
	//alert(nclass);
	waitress = $("#waitress").val();
	statusisi = $("#statusisi").val();
	flagangka = $("#flagangka").val();
	hdisplay0 = $("#hdisplay0").val();
	hdisplay1 = $("#hdisplay1").val();
	hdisplay0len = hdisplay0.length;
	$("#ceknilai").val(prevclass+"*"+text+"*"+id+"*"+nclass);
	
	if(flagangka==1)
	{
	   clearangka();
	   $("#flagangka").val(0);
	}
	if(id=="clear")
	{
	   clearIsian();
	   return
	}	
	flagsearch = $("#flagsearch").val();
	if(((id=="search")&&(statusisi=="menu"))||((id=="enter")&&(flagsearch==1)))
	{
	   if(flagsearch==0)
	   {
		   $("#menutengah").val(1);	
		   $("#statusketr").val(0);
		   $("#dtext02").attr('disabled',false);
		   $("#dtext02").attr({"placeholder":"Search Item"});
		   $("#dtext02").focus();
		   $("#flagsearch").val(1);
	   }else if(flagsearch==1)
	   {
	      $("#flagsearch").val(0);
		  searchtext = $("#dtext02").val().toUpperCase();
		  $("#dtext02").attr('disabled',true);
		  $("#dtext02").attr({"placeholder":""});
		  $("#statusketr").val(0);
		  $("#keterangan").val($("#dtext02").val());
		  setupfilter(searchtext);
	      setMenu();
	   }
	}
	if(id=="del")
	{
		$("#hdisplay0").val(hdisplay0.substr(0,hdisplay0len-1));
		$("#hdisplay1").val(hdisplay1.substr(0,hdisplay0len-1));
		$("#dtext01").val(hdisplay1.substr(0,hdisplay0len-1));
		return false;
	}
	if((id=="table")&&((val=="")||(val==0)))
	{
	   $("#statusisi").val("table");
	   statusisi = "table";
	}
	
	if((id=="waitress")&&((val=="")||(val==0)))
	{
	   $("#statusisi").val("waitress");
	   statusisi = "waitress";
	}
	if(nclass=='subkategori')
	{
		if((statusisi=="menu")||(statusisi=="payment"))
		{
			
			$("#menutengah").val(1);
			$("#posisisubkategori").val(val);
			setupmenu(2,val);
			setMenu();
			$("#statusisi").val("menu");
		}
	}
	if((id!=prevmenu)&&(id=="kategori01"||id=="kategori02"||id=="kategori03"))//&&(prevmenu=="kategori01"||prevmenu=="kategori02"||prevmenu=="kategori03"))
	{
	    $("#prevtext").val("");
	    $("#menutengah").val(1);
	    $("#qty").val(1);
		$("#harga").val(0);
		$("#pdisc").val(0);
		$("#komisi").val("");
		$("#printer").val("");
	    $("#dtext00").val("");
		$("#dtext01").val("");
		$("#hdisplay0").val("");
		$("#hdisplay1").val("");
		$("#statusketr").val(0);
		$("#posisisubkategori").val("");
	}
	if(id=="kategori01")
	{
	   if((statusisi=="menu")||(statusisi=="payment"))
	   {
	      $("#kategori01").attr({"class":"updown1"});
		  $("#kategori02").attr({"class":"updown1"});
		  $("#kategori03").attr({"class":"updown1"});
	      setupmenu(1,'01');
		  setMenu();
		  $("#posisi").val("01");
		  $("#statusisi").val("menu");
	   }
	}
	if(id=="kategori02")
	{
	   if((statusisi=="menu")||(statusisi=="payment"))
	   {
	      $("#kategori01").attr({"class":"updown1"});
		  $("#kategori02").attr({"class":"updown1"});
		  $("#kategori03").attr({"class":"updown1"});	
	      setupmenu(1,'02');
	      setMenu(); 
		  $("#posisi").val("02");
		  $("#statusisi").val("menu");
	   }
	}
	if(id=="kategori03")
	{
	   if((statusisi=="menu")||(statusisi=="payment"))
	   {
	      $("#kategori01").attr({"class":"updown1"});
		  $("#kategori02").attr({"class":"updown1"});
		  $("#kategori03").attr({"class":"updown1"});
	      setupmenu(1,'03');
	      setMenu();
		  $("#posisi").val("03");
		  $("#statusisi").val("menu");
	   }
	}
	if(statusketr==1)
	{
		start = parseFloat($("#menutengah").val());
		lastRow = parseFloat($("#lenlokasi").val());
		if(id=="down2")
		{
		   if(start+14<lastRow)
			 $("#menutengah").val(start+3);
		}
		if(id=="up2")
		{
		   if(start-3>0)
			 $("#menutengah").val(start-3);
		}
		if(id=="pagedown2")
		{
		   if(start+29<lastRow)
			 $("#menutengah").val(start+15);
		   else
			 $("#menutengah").val(Math.floor(lastRow/15)*15+1);
		}
		if(id=="pageup2")
		{
		   if(start-15>0)
			 $("#menutengah").val(start-15);
		   else
			 $("#menutengah").val(1);
		}
	}
	if(id=="payment")
	{
		statusisi = "payment";
		$("#statusisi").val("payment");
		//alert("masuk");
		clearMenuTengah();
		setPayment();
		//alert("lagi");//085346103310
	}
	switch(statusisi)
	{
		case "payment":
			 if(nclass=="menutengah")
			 {
				 prtext = $("#dtext02").val();
				 //text = document.getElementById(id).innerHTML;
                 $("#dtext02").val(text);
				 nxtext = $("#dtext02").val();
				 if(prtext!=nxtext)
				 {
					$("#dtext00").val("");
					$("#dtext01").val("");
					$("#hdisplay0").val("");
					$("#hdisplay1").val("");
				 }
				 if(text=="Tunai")
				 {
					 $("#idbayar").val("T");
					 hitungnetto();
				 }
				 if(text=="Kartu Kredit")
				 {
					 $("#idbayar").val("K");
				 }
				 if(text=="Kartu Debit")
				 {
					 $("#idbayar").val("D");
				 }
				 if(text=="Voucher")
				 {
					 $("#idbayar").val("V");
					 document.getElementById('idvoucher').value = "";
					 document.getElementById('jenisvoucher').value = "";
					 VoucherCustomer();
					 hitungnetto();
				 }
				 if(text=="Compliment")
				 {
					 ComplimentCustomer();
					 $("#idbayar").val("C");
					 $("#idbayartunai").val(0);
					 $("#idbayarkredit").val(0);
					 $("#idbayardebet").val(0);
					 $("#idbayarvoucher").val(0);
					 $("#idbayarcompliment").val(0);
					 $("#idnilaibayar").val(0);
					 //hitungnetto();
				 }
				 if(text=="Discount")
				 {
					 //DiscountCustomer();
					 $("#idbayar").val("P");
					 //hitungnetto();
				 }
				 if(text=="CNY")
				 {
					 $("#idbayar").val("Y");
					 $("#idnilai2valas").val(0);
				 }
				 if(text=="USD")
				 {
					 $("#idbayar").val("U");
					 $("#idnilai1valas").val(0);
				 }
				 setPayment();
			 }
			 if((nclass=="kategori01")||(nclass=="kategori02")||(nclass=="kategori03"))
			 {
			    $("#statusisi").val("menu");
			    posisi = $("#posisi").val();
				if(posisi=='01')
				$("#kategori01").attr({"class":"updown1"});
				if(posisi=='02')
				$("#kategori02").attr({"class":"updown1"});
				if(posisi=='03')
				$("#kategori03").attr({"class":"updown1"});
				clearIsian();
				clearMenuTengah();
				setupmenu(1,posisi);
				setMenu();
			 }
			 if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
			 {
			   if((hdisplay0len>0)&&((id=="commit")||(id=="enter")))
			   {  
					jenisbayar = $("#idbayar").val();
			        if(jenisbayar=="V")
					{
					        idvoucher = document.getElementById('dtext01').value;
							if (idvoucher !== "") {
								$.ajax({
									type: "POST",
									url: '<?= base_url(); ?>index.php/transaksi/order_touch/VoucherCustomer/' + idvoucher + '/',
									success: function (msg) {
										if(msg=="salah")
										{
											document.getElementById('idvoucher').value = "";
											document.getElementById('jenisvoucher').value = "";
											hitungnetto();
											alert("Kode voucher salah!");
										}
											else
										{
											var jsdata = msg;
											eval(jsdata);
											document.getElementById('idvoucher').value = datajson[0].novoucher;
											document.getElementById('idbayarvoucher').value = parseFloat(datajson[0].nominal);
											document.getElementById('ketrvoucher').value = datajson[0].keterangan;
											document.getElementById('jenisvoucher').value = datajson[0].jenis;
											document.getElementById('rupdisc').value = datajson[0].rupdisc;
											
											if(datajson[0].jenis=='3')
											{
												hitungnetto();
												text = $("#dtext02").val();
												$("#dtext02").val(text+" "+datajson[0].keterangan);
											}
										}
									}
								});
							}
							else {
								document.getElementById('idvoucher').value = "";
								document.getElementById('jenisvoucher').value = "";
								hitungnetto();
								alert("masukkan nomor voucher");
							}	
					}
					else
					{
						if(jenisbayar=="P")
						{
							idpersendiscount = parseFloat($("#dtext01").val());
							$("#idpersendiscount").val(idpersendiscount);
							if(idpersendiscount==0)
							{
								$("#iduserdiscount").val("");	
								$("#iddiscountasli").val(0);
								hitungnetto();
								setPayment();
							}else
							{
						       DiscountCustomer();
							}
							//setPayment();
						}else
						{
							nilaibayar = parseFloat($("#dtext01").val());
							nettosales = parseFloat($("#nettosales").val());
							
							if(jenisbayar=="T")
							   $("#idbayartunai").val(nilaibayar);
							if(jenisbayar=="K")
							   $("#idbayarkredit").val(nilaibayar);
							if(jenisbayar=="D")
							   $("#idbayardebet").val(nilaibayar);
							if(jenisbayar=="V")
							   $("#idbayarvoucher").val(nilaibayar);
							if(jenisbayar=="C")
							   $("#idbayarcompliment").val(nilaibayar);
						    if(jenisbayar=="Y")
							   $("#idnilai1valas").val(nilaibayar);
							if(jenisbayar=="U")
							   $("#idnilai2valas").val(nilaibayar);
							
							bayartunai = parseFloat($("#idbayartunai").val());
							bayarkredit = parseFloat($("#idbayarkredit").val());
							bayardebet = parseFloat($("#idbayardebet").val());
							bayarvoucher = parseFloat($("#idbayarvoucher").val());
							bayarcompliment = parseFloat($("#idbayarcompliment").val());
							idnilai1valas = parseFloat($("#idnilai1valas").val());
							idnilai2valas = parseFloat($("#idnilai2valas").val());
							id1kurs = parseFloat($("#id1kurs").val());
							id2kurs = parseFloat($("#id2kurs").val());
							totalbayar = bayartunai + bayarkredit + bayardebet + bayarvoucher + bayarcompliment + (id1kurs*idnilai1valas) + (id2kurs*idnilai2valas);
							
							//alert(nilaibayar);
							//alert(nettosales);
							//alert(nilaibayar < nettosales);
							$("#idnilaibayar").val(nilaibayar);
							setPayment();
							
							if(totalbayar < nettosales)
							{
								alert("Pembayaran kurang");
								clearIsian();
							}else{
								$("#idpayment").val("lunas");
							}
						}
					}
			   }
			 }else
			 {
			   dtext00 = hdisplay0+val;
			   dtext01 = hdisplay1+val;
			   $("#hdisplay0").val(dtext00);
			   $("#hdisplay1").val(dtext01);
			   $("#dtext01").val(dtext01);
			 }
			 break;
	    case "waitress":
		    //alert("masuk");
				if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
				{
					if((hdisplay0len>0)&&((id=="table")||(id=="enter")))
					{  
						if(cekPassword(hdisplay0))
						{	
							namapersonal = $("#namapersonal").val();
							kdpersonal = $("#kdpersonal").val();
							var text = document.getElementById('waitress');
					  
							text.innerHTML = namapersonal;
							$("#waitress").attr({"class":"menudone"});
							$("#table").attr({"class":"menukanan1"});
							$("#idpersonal").val(kdpersonal);
							$("#statusisi").val("table");
							setupmenu(1,"00");
							clearIsian();
							clearMenuTengah();
							setLokasi();
						}
					}
					else
					{
	
						alert("Masukkan kode anda dahulu");
						clearIsian();
						// clearMenuTengah();
						kosongarray();
						$("#waitress").attr({"class":"menukanan1"});
						$("#table").attr({"class":"menukanan"});
						$("#guest").attr({"class":"menukanan"});
						$("#agent").attr({"class":"menukanan"});
					}
				}
				else
				{
					dtext00 = hdisplay0+val;
					dtext01 = hdisplay1+"*";
					$("#hdisplay0").val(dtext00);
					$("#hdisplay1").val(dtext01);
					$("#dtext01").val(dtext01);
				}
			break;
	    case "table":
			if((nclass!="menutengah"&&nclass!="updown"&&nclass!="mejaisi")||id=="enter")
			{
				if((hdisplay0len>0)&&((id=="agent")||(id=="enter")))
				{  
				  kodetable = $("#dtext01").val();
				  
				  $("#kdmeja").val(kodetable);
				  $.ajax({
	       			type: "POST",
	        		url: "<?=base_url();?>index.php/transaksi/order_touch/cektable/",
	        		data: {data: kodetable},
	        		success: function(data) {
						var obj = JSON.parse(data);
						if(obj.NoTrans != ''){

							/*
							if(obj.Status == 'order-touch'){
									alert("Sedang ada order di meja ini");
							}else{
								alert("Sedang ada order di meja ini, atau Selesaikan transaksi di Meja ini. ");
							}
							*/
							
							var text = document.getElementById('agent');
							text.innerHTML = obj.KdAgent;
							$("#agent").attr({"class":"menudone"});
							$("#idagent").val(obj.KdAgent);
						
							var text = document.getElementById('guest');
							text.innerHTML = "Guest "+"</br>"+obj.TotalGuest;
							$("#guest").attr({"class":"menudone"});
							$("#idguest").val(obj.TotalGuest);
							$("#statusisi").val("menu")
							
							posisi = $("#posisi").val();
							if(posisi=='01')
								$("#kategori01").attr({"class":"updown1"});
							if(posisi=='02')
								$("#kategori02").attr({"class":"updown1"});
							if(posisi=='03')
								$("#kategori03").attr({"class":"updown1"});
							clearIsian();
							clearMenuTengah();
							setupmenu(1,posisi);
							setMenu();

						}
					}
	    		  });
				  var text = document.getElementById('table');
				  text.innerHTML = "Table "+"</br>"+kodetable;
				  document.title = kodetable;
				  $("#table").attr({"class":"menudone"});
				  $("#agent").attr({"class":"menukanan1"});
				  $("#idtable").val(kodetable);
				  $("#statusisi").val("agent");
				  setupmenu(1,"00");
				  clearIsian();
				  clearMenuTengah();
				}
				else
				 {
					alert("Masukkan kode table dahulu");
				 }
			}else
			{
				$("#dtext01").val(val);
				$("#hdisplay0").val(val);
				$("#hdisplay1").val(val);
				start = parseFloat($("#menutengah").val());
				lastRow = parseFloat($("#lenlokasi").val());
				if(id=="down2")
				{
				   if(start+14<lastRow)
					 $("#menutengah").val(start+3);
				}
				if(id=="up2")
				{
				   if(start-3>0)
					 $("#menutengah").val(start-3);
				}
				if(id=="pagedown2")
				{
				   if(start+29<lastRow)
					 $("#menutengah").val(start+15);
				   else
					 $("#menutengah").val(Math.floor(lastRow/15)*15+1);
				}
				if(id=="pageup2")
				{
				   if(start-15>0)
					 $("#menutengah").val(start-15);
				   else
					 $("#menutengah").val(1);
				}
				setLokasi();
			}			
			break;
		case "agent":
		    //alert("masuk");
	        if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
			{
			   if((hdisplay0len>0)&&((id=="guest")||(id=="enter")))
			   {  
			   
					kodeagent = $("#dtext01").val();
					if(((kodeagent>9999)||(kodeagent<1000))&&kodeagent!=0)
					{
						alert("Kode Agen Salah!");
						clearIsian();
					}else{
						var text = document.getElementById('agent');
						text.innerHTML = kodeagent;
						$("#agent").attr({"class":"menudone"});
						$("#idagent").val(kodeagent);
						$("#statusisi").val("guest");
						
						kategorikassa = $("#kategorikassa").val();
						iscounter = $("#iscounter").val();
						
						if(kategorikassa=="03" || iscounter==1)
						{	
						setupmenu(1,"00");
						}
						clearIsian();
						clearMenuTengah();
					}
			   }
			   else
				   alert("Masukkan kode agen")
			}else
			{
			   dtext00 = hdisplay0+val;
			   dtext01 = hdisplay1+val;
			   $("#hdisplay0").val(dtext00);
			   $("#hdisplay1").val(dtext01);
			   $("#dtext01").val(dtext01);
			}
			break;
		case "guest":
		    //alert("masuk");
	        if((nclass!="angka"&&nclass!="angka2")||(id=="enter"))
			{
			   if((hdisplay0len>0)&&((id=="table")||(id=="enter")))
			   {  
			   
					kodeguest = $("#dtext01").val();
					if(kodeguest > 999)
					{
						alert("Maksimal Guest 999 !");
						clearIsian();
					}else{
						var text = document.getElementById('guest');
						text.innerHTML = "Guest "+"</br>"+kodeguest;
						$("#guest").attr({"class":"menudone"});
						posisi = $("#posisi").val();
						if(posisi=='01')
						$("#kategori01").attr({"class":"updown1"});
						if(posisi=='02')
						$("#kategori02").attr({"class":"updown1"});
						if(posisi=='03')
						$("#kategori03").attr({"class":"updown1"});
						$("#idguest").val(kodeguest);
						$("#statusisi").val("menu");
						clearIsian();
						clearMenuTengah();
						setupmenu(1,posisi);
						setMenu();
					}
			   }
			   else
				   alert("Masukkan jumlah tamu")
			}else
			{
			   dtext00 = hdisplay0+val;
			   dtext01 = hdisplay1+val;
			   $("#hdisplay0").val(dtext00);
			   $("#hdisplay1").val(dtext01);
			   $("#dtext01").val(dtext01);
			}
			break;
		case "menu":
			var str = $("#dtext01").val();
			var res = str.split(' ',3);
			
			if((nclass!="menutengah"&&nclass!="updown"&&nclass!="kategori"&&id!="plus"&&id!="minus"&&nclass!="angka"&&nclass!="angka2"&&nclass!="angka3")||id=="void"||id=="enter")
			{
				if((hdisplay0len>0)&&(id=="enter"||id=="void"))
			    {  
				  //totalitem = parseFloat($("#totalitem").val())+1;
				  //$("#totalitem").val(totalitem);
				  saveData(id);
				  
				  totalitem = document.getElementsByName("pcode[]").length;
				  if(totalitem>15)
				     $("#posisitampil").val(totalitem-14);
				  else
				     $("#posisitampil").val(1);
				  displaytext();
				  clearIsian();
				  /*
				  posisi = $("#posisi").val();
				  if(posisi=="01")
				  {
				  $("#kategori01").attr({"class":"updown1"});
				  $("#kategori02").attr({"class":"kategori"});
				  $("#kategori03").attr({"class":"kategori"});
				  //setupmenu(1,'01');
				  //setMenu();
				  }
				  if(posisi=="02")
				  {
				  $("#kategori01").attr({"class":"kategori"});
				  $("#kategori02").attr({"class":"updown1"});
				  $("#kategori03").attr({"class":"kategori"});
				  //setupmenu(1,'02');
				  //setMenu();
				  }
				  if(posisi=="03")
				  {
				  $("#kategori01").attr({"class":"kategori"});
				  $("#kategori02").attr({"class":"kategori"});
				  $("#kategori03").attr({"class":"updown1"});
				  //setupmenu(1,'03');
				  //setMenu();
				  }
				  
					kode = $("#posisisubkategori").val();
					setupmenu(2,kode);
					setMenu();
					//clearSubKategori();
					//setSubKategori(kode);
					$("#indexa").val($("#index"+kode).val());
				  */
				  $("#qty").val(1);
				  $("#prevtext").val("");
				  $("#statusketr").val(0);
				  statusketr=0;
			    }
				if((hdisplay0len>0)&&id=="note")
				{
					$("#flagsearch").val(0);
					$("#dtext02").val("");
					$("#menutengah").val(1);
				    setKeterangan();
					
					$("#statusketr").val(1);
					$("#dtext02").attr('disabled',false);
					$("#dtext02").attr({"placeholder":"Input Keterangan Menu"});
					$("#dtext02").focus();
				}
				
				if(statusketr==1)
				    setKeterangan();
				
				if(res[1] =='PHOTO'){
					PhotoBooth();					
				};
			}else
			{
			    //alert(id);
				//alert(text);
				if(statusketr==0)
				{
					start = parseFloat($("#menutengah").val());
					lastRow = parseFloat($("#lenbarang").val());
					if(id=="down2")
					{
					   if(start+14<lastRow)
						 $("#menutengah").val(start+3);
					}
					if(id=="up2")
					{
					   if(start-3>0)
						 $("#menutengah").val(start-3);
					}
					if(id=="pagedown2")
					{
					   if(start+29<lastRow)
						 $("#menutengah").val(start+15);
					   else
						 $("#menutengah").val(Math.floor(lastRow/15)*15+1);
					}
					if(id=="pageup2")
					{
					   if(start-15>0)
						 $("#menutengah").val(start-15);
					   else
						 $("#menutengah").val(1);
					}
					
					if(id=="down1"||id=="up1"||id=="pagedown1"||id=="pageup1")
					{
						start = parseFloat($("#posisitampil").val());
						totalitem = document.getElementsByName("pcode[]").length;
						if(id=="down1")
						{
						   if(start+14<totalitem)
							 $("#posisitampil").val(start+1);
						}
						if(id=="up1")
						{
						   if(start-1>0)
							 $("#posisitampil").val(start-1);
						}
						if(id=="pagedown1")
						{
						   if(start+15<totalitem)
							 $("#posisitampil").val(start+5);
						   else
							 $("#posisitampil").val(Math.floor(totalitem/15)*15+1);
						}
						if(id=="pageup1")
						{
						   if(start-5>0)
							 $("#posisitampil").val(start-5);
						   else
							 $("#posisitampil").val(1);
						}
						displaytext();
					}
					
					if((id=="plus")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!=''))))
					{
					   qty = parseFloat($("#qty").val())+1;
					   $("#qty").val(qty);
					}
					if((id=="minus")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!=''))))
					{
					   qty = parseFloat($("#qty").val())-1;
					   if(qty>0)
						  $("#qty").val(qty);
					}
					if((nclass=="angka"||nclass=="angka2"||nclass=="angka3")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!='')))){
						xqty = $("#xQtyOrder").val();
						//alert(val);
						$("#xQtyOrder").val(xqty.concat(val));
						$("#qty").val(parseFloat($("#xQtyOrder").val()));
					}
					
					setMenu();
					if((nclass=="menutengah"||nclass=="mejaisi"||id=="plus"||id=="minus" ||nclass=="angka"||nclass=="angka2"||nclass=="angka3")&&((text!="")||(($("hdisplay0").val()!='')&&($("hdisplay1").val()!=''))))
					{
						//alert('**'+text+'**');
						qty = $("#qty").val();
						
						text1 = qty+" "+text;
						
						$("#dtext00").val(val);
						$("#dtext01").val(text1);
						$("#dtext02").val('');
						$("#hdisplay0").val(val);
						$("#hdisplay1").val(text1);
					}
				}
				
			}
			break;
	}
	
	$("#prevclass").val(nclass);
	$("#prevmenu").val(id);
	if((id!="plus")&&(id!="minus")&&(id!="enter")&&(id!="kategori01")&&(id!="kategori02")&&(id!="kategori03"))
	   $("#prevtext").val(text);
	/*
	if(id=="boxpicture")
	{
		$("#boxtext").css("display","");
		$("#boxpicture").css("display","none")
	}else if(id=="boxtext")
	{
		$("#boxtext").css("display","none")
		$("#boxpicture").css("display","")
	}*/
	if(cKey.getAttribute("class")=="angka")
	{
		$("#flagangka").val(1)
		cKey.setAttribute("class", "angka2")
	}
/*
	if(nclass=="menukanan"||nclass=="menukanan1")
		clearmenukanan()
	if(cKey.getAttribute("class")=="menukanan")
	cKey.setAttribute("class", "menukanan1")
*/
//var text = document.getElementById('dtext01');
	//text.innerHTML = dtext01;

//cKey.innerHTML= "class";
}

function displaytext()
{
    totalitem = document.getElementsByName("pcode[]").length;
	posisitampil = parseFloat($("#posisitampil").val());
	for(index=0;index<15;index++){
	    mulai = index+posisitampil-1;
		index0 = "0"+parseFloat(index+1);
		index1 = index0.substr(-2);
	    if(mulai<totalitem)
		{
		   
		   nourut = $("#nourut" + mulai).val();
		   qty = $("#qty" + mulai).val();
		   harga = $("#harga" + mulai).val();
		   nama = $("#nama" + mulai).val();
		   ketr = $("#keterangan" + mulai).val();
		   disc = $("#disc" + mulai).val();
		   netto = $("#netto" + mulai).val();
		   pdisc = $("#pdisc" + mulai).val();
		   if(nourut<10)
		   {
			nourut = "0"+nourut;
		   }
		   if(ketr!="")
		   {
			hdisplay = nourut+"| "+qty+" |"+nama+"|"+ketr+"| "+netto;
	       }
		   else
		   {
			hdisplay = nourut+"| "+qty+" |"+nama+"| "+netto;
		   }
		   $("#displaytext"+index1).val(hdisplay);
		}
		else {
		   $("#displaytext"+index1).val("");	
		   
	   }
	   // -- to lcd
	   //dibikin seperti dibawah ini, karena function ini
	   //jalan 2 * 15 (2 kali index==14) kali, tapi mau execute
	   // function toLCD sekali aja pas terakhir.
	   if(index==14) {
			this.start_i++;
			if(this.start_i==2) {
				var total = $("#nettosales").val();
				toLCD(nama, harga, total);
				this.start_i = 0;
			}
	   }
	   // -- eo to lcd
	}	
}

function saveData(id)
{
		//saveThis();
		
		pcode = $("#pcode").val();
		qty = parseFloat($("#qty").val());
		if(id=="void")qty = qty * -1;
		satuan = $("#satuan").val();
		harga = parseFloat($("#harga").val());
		disc = parseFloat($("#disc").val());
		komisi = $("#komisi").val();
		printer = $("#printer").val();
		pdisc = parseFloat($("#pdisc").val());
		keterangan = $("#keterangan").val();
		editflag =$("#EditFlg").val();
		
		disc = pdisc/100*(qty*harga);
		netto = (qty*harga)-disc;
		//alert(editflag);
		//editflag = 0;
		if(editflag==0){
			
			lastRow = document.getElementsByName("pcode[]").length;
			//alert(lastRow);
			datadouble = false;
			for(index=0;index<lastRow;index++)
			{
				indexs = index; 
				if($("#pcode"+indexs).val()==pcode&&$("#satuan"+indexs).val()==satuan)
				{
				   if(qty-Math.round(qty)==0&&$("#pcode"+indexs).val()-Math.round($("#pcode"+indexs).val())==0)
				   {
    				   datadouble = true;
	    			   break;
				   }
				}
				
			}
			
			//alert(indexs);
			if(datadouble)
			{
				qtyasli = parseFloat($("#tmpqty" + indexs).val());
				if($("#statusubah").val()==1 && id!="void")
				{
					qtybaru = qty;
				}
				else
				{
					qtybaru = qtyasli+qty;
				}
				//if(qtybaru<0)qtybaru=0;
				if(qtybaru<1)
				{
					if(indexs==0 && lastRow==1)
					{
						$("#nourut"+indexs).val("");
						$("#pcode"+indexs).val("");
						$("#nama"+indexs).val("");
						$("#qty" + indexs).val("");
						$("#harga" + indexs).val("");
						$("#disc" + indexs).val("");
						$("#netto" + indexs).val("");
						$("#pdisc" + indexs).val("");
						$("#satuan"+indexs).val("");
						$("#keterangan"+indexs).val("");
						$("#counter"+indexs).val("");
						$("#counterambil"+indexs).val("");
						$("#tmppcode"+indexs).val("");
						$("#tmpqty" + indexs).val("");
						$("#komisi" + indexs).val("");
						$("#printer" + indexs).val("");
					}
					if(indexs > 0 && indexs==(lastRow-1))
					{
						$("#baris"+indexs).remove();
					}
					if(((indexs > 0)||(indexs==0)) && (indexs < (lastRow-1)))
					{
//						alert(indexs);
						for(indexa=(indexs+1);indexa<(lastRow);indexa++)
						{
							indexb = indexa;
							//alert(indexb);
							$("#nourut"+indexb).val(($("#nourut"+indexb).val())-1);
							$("#counter"+indexb).val(($("#counter"+indexb).val())-1);
							$("#counterambil"+indexb).val(($("#counterambil"+indexb).val())-1);
						}
						
						$("#nourut"+indexs).val("");
						$("#pcode"+indexs).val("");
						$("#nama"+indexs).val("");
						$("#qty" + indexs).val("");
						$("#harga" + indexs).val("");
						$("#disc" + indexs).val("");
						$("#netto" + indexs).val("");
						$("#pdisc" + indexs).val("");
						$("#satuan"+indexs).val("");
						$("#keterangan"+indexs).val("");
						$("#counter"+indexs).val("");
						$("#counterambil"+indexs).val("");
						$("#tmppcode"+indexs).val("");
						$("#tmpqty" + indexs).val("");
						$("#komisi" + indexs).val("");
						$("#printer" + indexs).val("");
						
						for(indexc=indexs;indexc<lastRow;indexc++)
						{
							indexd = indexc;
							//alert(indexd);
							$("#nourut"+indexd).val($("#nourut"+(indexd+1)).val());
							$("#pcode"+indexd).val($("#pcode"+(indexd+1)).val());
							$("#nama"+indexd).val($("#nama"+(indexd+1)).val());
							$("#qty"+indexd).val($("#qty"+(indexd+1)).val());
							$("#harga"+indexd).val($("#harga"+(indexd+1)).val());
							
							$("#disc"+indexd).val($("#disc"+(indexd+1)).val());
							$("#netto"+indexd).val($("#netto"+(indexd+1)).val());
							$("#pdisc"+indexd).val($("#pdisc"+(indexd+1)).val());
							
							$("#satuan"+indexd).val($("#satuan"+(indexd+1)).val());
							$("#keterangan"+indexd).val($("#keterangan"+(indexd+1)).val());
							$("#counter"+indexd).val($("#counter"+(indexd+1)).val());
							$("#counterambil"+indexd).val($("#counterambil"+(indexd+1)).val());
							$("#tmppcode"+indexd).val($("#tmppcode"+(indexd+1)).val());
							$("#tmpqty"+indexd).val($("#tmpqty"+(indexd+1)).val());
							$("#komisi"+indexd).val($("#komisi"+(indexd+1)).val());
							$("#printer"+indexd).val($("#printer"+(indexd+1)).val());
						}
						
						$("#baris"+(lastRow-1)).remove();
					}
				}
				else{
					$("#tmpqty" + indexs).val(qtybaru);
					$("#qty" + indexs).val(qtybaru);
					$("#keterangan" + indexs).val(keterangan);
					disc = pdisc/100*(qtybaru*harga);
					netto = (qtybaru*harga)-disc;
					$("#disc" + indexs).val(disc);
					$("#netto" + indexs).val(netto);
				}
				
			}else
			{
			    if(id=="enter")
				{
					if(lastRow==1&&$("#pcode0").val()=="")
					{
						newid = 0;
					}else
					{
						newid = detailNew();
					}
					nourut = parseFloat($("#nourut" + newid).val());
					$("#nourut" + newid).val(newid+1);
					$("#pcode" + newid).val(pcode);
					$("#tmppcode" + newid).val(pcode);
					$("#nama" + newid).val($("#nmbrg").val());
					$("#tmpqty" + newid).val(qty);
					$("#qty" + newid).val(qty);
					$("#harga" + newid).val(harga);
					$("#disc" + newid).val(disc);
					$("#pdisc" + newid).val(pdisc);
					$("#netto" + newid).val(netto);
					$("#satuan" + newid).val(satuan);
					$("#keterangan" + newid).val(keterangan);
					$("#komisi" + newid).val(komisi);
					$("#printer" + newid).val(printer);
					$("#transaksi").val("yes");
					pcode = $("#pcode" + newid).val();
					$("#xQtyOrder").val('');
				}
			}
			$("#statusubah").val(0);
		}
		else
		{
			indexs = parseFloat($("#NoUrut").val())-1;
			//alert(indexs);
	        $("#keterangan" + indexs).val(keterangan);
			$("#tmpqty" + indexs).val(qty);
			$("#qty" + indexs).val(qty);
			disc = pdisc/100*(qty*harga);
			netto = (qty*harga)-disc;
			$("#disc" + indexs).val(disc);
			$("#netto" + indexs).val(netto);
		}
		//alert(pcode);
		hitungnetto();
		
		$("#pcode").val("");
		$("#nmbrg").val("");
		$("#qty").val("");
		$("#harga").val("");
		$("#disc").val("");
		$("#netto").val("");
		$("#pdisc").val("");
		$("#satuan").val("");
		$("#keterangan").val("");
		$("#komisi").val("");
		$("#printer").val("");
		$("#NoUrut").val("");
		$("#EditFlg").val(0);
		//$("#pcode").focus();
}

function number_format(a, b, c, d)
{
	a = Math.round(a * Math.pow(10, b)) / Math.pow(10, b);
	e = a + '';
	f = e.split('.');
	if (!f[0]) {
	f[0] = '0';
	}
	if (!f[1]) {
	f[1] = '';
	}
	if (f[1].length < b) {
	g = f[1];
	for (i=f[1].length + 1; i <= b; i++) {
	g += '0';
	}
	f[1] = g;
	}
	if(d != '' && f[0].length > 3) {
	h = f[0];
	f[0] = '';
	for(j = 3; j < h.length; j+=3) {
	i = h.slice(h.length - j, h.length - j + 3);
	f[0] = d + i +  f[0] + '';
	}
	j = h.substr(0, (h.length % 3 == 0) ? 3 : (h.length % 3));
	f[0] = j + f[0];
	}
	c = (b <= 0) ? '' : c;
	
	return f[0] + c + f[1];
}

function hitungnetto()
{
	var idbayar = $("#idbayar").val();
	var idcompliment = $("#idcompliment").val();
	var jenisvoucher = $("#jenisvoucher").val();
	var idvoucher = $("#idvoucher").val();
	
	nilaidiscount = $("#idpersendiscount").val();
	iduserdiscount = $("#iduserdiscount").val();
	iscounter = $("#iscounter").val();
	if(iscounter==1){
		scptg=0;
		taxptg=0;	
	}else{
		scptg=0.08;
		taxptg=0.1;
	}
	
	var lastRow = document.getElementsByName("pcode[]").length;
	var total = 0;
	var discount = 0;
	var bruto = 0;
	for(index=0;index<lastRow;index++)
	{
		indexs = index; 
		qty = parseFloat($("#qty"+indexs).val());
		if((idbayar=="C")&&(idcompliment!=''))
		{
			pdisc = 100
		}
		else
			if((jenisvoucher=="3")&&(idvoucher!=""))
				pdisc = parseFloat($("#pdisc"+indexs).val());
			else
				pdisc = 0;
		//alert(pdisc);
		//alert(nilaidiscount);
		//alert(iduserdiscount);
		//alert(pdisc);
		if((nilaidiscount>0)&&(iduserdiscount!=''))
			{
				discid = nilaidiscount;
				//alert(discid);
				//alert(disc);
				if(discid>pdisc)
				{
					pdisc = discid;
				}
			}
		
		harga = parseFloat($("#harga"+indexs).val());
		disc = pdisc/100*(qty*harga);
		netto = (qty*harga)-disc;
		$("#disc"+indexs).val(disc);
		$("#netto"+indexs).val(netto);
		total += netto;
		discount += disc;
		bruto += (qty*harga);
	}
	//alert(total);
	//alert(discount);
	//alert(bruto);
	totalsales = Math.round(total);
	totaldisc = Math.round(discount);
	scsales = Math.round(total*scptg);
	taxsales = Math.round((totalsales+scsales)*taxptg);
	nettosales = totalsales+scsales+taxsales;
	//alert(idbayar+jenisvoucher+idvoucher);
	
	$("#brutosales").val(bruto);
	$("#totalsales").val(totalsales);
	$("#scsales").val(scsales);
	$("#taxsales").val(taxsales);
	$("#nettosales").val(nettosales);
	$("#idnilaidiscount").val(totaldisc);
	
	nettosales = $("#nettosales").val();
	displaytotal = document.getElementById("displaytotal");
	/*displaytotal.innerHTML = "Total " + totalsales + "<br>"+
							 "SC " + scsales + "<br>"
							 "Tax " + taxsales + "<br>"+
							 "Netto " + nettosales;*/
	displaytotal.innerHTML =  number_format(nettosales, 0, ',', '.');
	/*
	$("#total_rupiah").val(number_format(total, 0, ',', '.'));
	$("#nilai_sc_rupiah").val(number_format(nilsc, 0, ',', '.'));
	$("#nilai_pb1_rupiah").val(number_format(nilaipb1, 0, ',', '.'));
	$("#netto_rupiah").val(number_format(nettobulat, 0, ',', '.'));
	total = number_format(total, 0, ',', '.');
	*/
	//setPayment();
	displaytext();
}

function detailNew()
{
	var clonedRow = $("#detail1 tr:last").clone(true);
	var intCurrentRowId = parseFloat($('#detail1 tr').length)-1;
	lastRow = document.getElementsByName("pcode[]").length;
	//alert(lastRow);
	//alert(intCurrentRowId);
	//alert(intCurrentRowId);
	var intNewRowId = parseFloat(intCurrentRowId)+1;
	$("#nourut" + intCurrentRowId , clonedRow ).attr( { "id" : "nourut" + intNewRowId, "value" : ""} );
	$("#pcode" + intCurrentRowId , clonedRow ).attr( { "id" : "pcode" + intNewRowId, "value" : ""} );
	$("#nama" + intCurrentRowId , clonedRow ).attr( { "id" : "nama" + intNewRowId, "value" : ""} );
	$("#qty" + intCurrentRowId , clonedRow ).attr( { "id" : "qty" + intNewRowId, "value" : ""} );
	$("#harga" + intCurrentRowId , clonedRow ).attr( { "id" : "harga" + intNewRowId, "value" : ""} );
	$("#disc" + intCurrentRowId , clonedRow ).attr( { "id" : "disc" + intNewRowId, "value" : ""} );
	$("#netto" + intCurrentRowId , clonedRow ).attr( { "id" : "netto" + intNewRowId, "value" : ""} );
	$("#pdisc" + intCurrentRowId , clonedRow ).attr( { "id" : "pdisc" + intNewRowId, "value" : ""} );
	$("#printer" + intCurrentRowId , clonedRow ).attr( { "id" : "printer" + intNewRowId, "value" : ""} );
	$("#komisi" + intCurrentRowId , clonedRow ).attr( { "id" : "komisi" + intNewRowId, "value" : ""} );
	$("#satuan" + intCurrentRowId , clonedRow ).attr( { "id" : "satuan" + intNewRowId, "value" : ""} );
	$("#keterangan" + intCurrentRowId , clonedRow ).attr( { "id" : "keterangan" + intNewRowId, "value" : ""} );
	$("#tmppcode" + intCurrentRowId , clonedRow ).attr( { "id" : "tmppcode" + intNewRowId, "value" : ""} );
	$("#tmpqty" + intCurrentRowId , clonedRow ).attr( { "id" : "tmpqty" + intNewRowId, "value" : ""} );
	$("#detail1").append(clonedRow);
	$("#detail1 tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
	return intNewRowId;
}

function klikdisptext(cKey)
{
	id = cKey.getAttribute("id");
	val = cKey.getAttribute("value");
	nclass = cKey.getAttribute("class");
	
	posisitampil = parseFloat($("#posisitampil").val());
	
	id2 = id.split("displaytext");
	if(posisitampil==1)
	{
		id3 = (parseFloat(id2[1])-1);
	}
	else
	{
		id3 = (parseFloat(id2[1])+posisitampil-2);
	}
	
	nourut = $("#nourut"+id3).val();
	pcode = $("#pcode"+id3).val();
	nama = $("#nama"+id3).val();
	qty = $("#qty"+id3).val();
	satuan = $("#satuan"+id3).val();
	harga = $("#harga"+id3).val();
	disc = $("#disc"+id3).val();
	pdisc = $("#pdisc"+id3).val();
	netto = $("#netto"+id3).val();
	komisi = $("#komisi"+id3).val();
	printer = $("#printer"+id3).val();
	tmppcode = $("#tmppcode"+id3).val();
	tmpqty = $("#tmpqty"+id3).val();
	counter = $("#counter"+id3).val();
	counterambil = $("#counterambil"+id3).val();
	keterangan = $("#keterangan"+id3).val();
	if(nama)
	{
	$("#NoUrut").val(nourut);	
	$("#pcode").val(pcode);	
	$("#tmppcode").val(tmppcode);	
	$("#nmbrg").val(nama);	
	$("#qty").val(qty);
	$("#harga").val(harga);
	$("#disc").val(disc);
	$("#pdisc").val(pdisc);
	$("#netto").val(netto);	
	$("#komisi").val(komisi);	
	$("#printer").val(printer);	
	$("#satuan").val(satuan);	
	$("#keterangan").val(keterangan);	
	
	text = (qty+" "+nama);
	$("#dtext01").val(text);
	$("#dtext01").focus();
	
	$("#hdisplay0").val(pcode);
	$("#hdisplay1").val(text);
	
	$("#dtext02").val(keterangan);
	
	$("#statusubah").val(1);
	$("#statusisi").val("menu");
	}
}
function text02()
{
	teks = $('#dtext02').val();
	searchtext = $("#dtext02").val().toUpperCase();
	flagsearch = $('#flagsearch').val().toUpperCase();
	if(flagsearch==1)
	{
		setupfilter(searchtext);
	   setMenu();
	}
	else
	{
		$('#keterangan').val(teks);
	}
}

function VoucherCustomer() {
		nettosales = parseFloat($("#nettosales").val());
		if(nettosales==0)
		{
		   alert("Total Belanja masih NOL");	
		}else
		{
			base_url = $("#baseurl").val();
			url = base_url+"index.php/pop/voucher2/index/";
			window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
}

function ComplimentCustomer() {
		totalitem = document.getElementsByName("pcode[]").length;
		if(totalitem==0)
		{
		   alert("Total Item Belanja masih NOL");	
		}else
		{
			base_url = $("#baseurl").val();
			url = base_url+"index.php/pop/compliment2/index/";
			window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
}

function DiscountCustomer() {
		totalitem = document.getElementsByName("pcode[]").length;
		if(totalitem==0)
		{
		   alert("Total Item Belanja masih NOL");	
		}else
		{
			base_url = $("#baseurl").val();
			url = base_url+"index.php/pop/discount2/index/";
			window.open(url,'popuppage','width=750,height=500,top=100,left=150');
		}
}

function PhotoBooth() {
		
	base_url = $("#baseurl").val();
	url = base_url+"index.php/pop/photobooth/index/";
	window.open(url,'popuppage','width=500,height=500,top=100,left=150');
}

</script>
<html style="height: 100%;">
<head>
<title>Secret Garden Village - Order Touch</title>
</head>
<!--onload="popup_barang_soldout();$('#dtext02').attr('disabled',true);"-->
<body style="height: 100%;margin:0px 0px 0px 0px;" onload="setLokasi()" >
<form method='post' name="pos_touch" id="pos_touch" action='<?=base_url();?>index.php/transaksi/order_touch/save_trans' onsubmit="return false" style="height: 100%;">
<input type="hidden" name="baseurl" id="baseurl" value="<?=base_url();?>">
<input type="hidden" name="kassa" id="kassa" value="<?=$NoKassa?>">
<input type="hidden" name="kasir" id="kasir" value="<?=$session_name?>">
<input type="hidden" name="store" id="store" value="<?=$store[0]['KdCabang']?>">
<input type="hidden" name="jenisprint" id="jenisprint" value="<?=$datakassa[0]['JenisPrint']?>">
<input type="hidden" name="iscounter" id="iscounter" value="<?=$datakassa[0]['IsCounter']?>">
<input type="hidden" name="kategorikassa" id="kategorikassa" value="<?=$datakassa[0]['KdKategori']?>">
<input type="hidden" id="ceknilai" name="ceknilai" value="">
<input type="hidden" id="transaksi" name="transaksi" value="no">
<input type="hidden" id="flagangka" name="flagangka" value=0>
<input type="hidden" id="hdisplay0" name="hdisplay0" value="">
<input type="hidden" id="hdisplay1" name="hdisplay1" value="">
<input type="hidden" id="statusubah" name="statusubah" value=0>
<input type="hidden" id="flagsearch" name="flagsearch" value=0>

<input type="hidden" id="kdpersonal" name="kdpersonal" value="">
<input type="hidden" id="namapersonal" name="namapersonal" value="">

<input type="hidden" id="listvoucher" name="listvoucher" value="">
<input type="hidden" id="listvouchket" name="listvouchket" value="">
<input type="hidden" id="listvouchsaldo" name="listvouchsaldo" value="">
<input type="hidden" id="listvouchpakai" name="listvouchpakai" value="">
<input type="hidden" id="listjenis" name="listjenis" value="">

<input type="hidden" id="listcompliment" name="listcompliment" value="">
<input type="hidden" id="listcomplket" name="listcomplket" value="">
<input type="hidden" id="listcomplsaldo" name="listcomplsaldo" value="">
<input type="hidden" id="listcomplpakai" name="listcomplpakai" value="">
<input type="hidden" id="listcompljenis" name="listcompljenis" value="">

<input type="hidden" id="idpersendiscount" name="idpersendiscount" value=0>
<input type="hidden" id="iduserdiscount" name="iduserdiscount" value="">
<input type="hidden" id="iddiscountasli" name="iddiscountasli" value=0>

<input type="hidden" id="listkodephoto" name="listkodephoto" value="">
<input type="hidden" id="kdmeja" name="kdmeja" value="">
<input type="hidden" id="xQtyOrder" name="xQtyOrder" value="">

<?if($datakassa[0]['KdKategori']=="03" || $datakassa[0]['IsCounter']==1)
{
	$statusisi = "agent";
	$isitable = "BTA";
	$classtable = "menudone";
}
else
{
	$statusisi = "waitress";
	$isitable = "";
	$classtable = "menukanan1";
}
?>
<input type="hidden" id="statusisi" name="statusisi" value="waitress">
<input type="hidden" id="menutengah" name="menutengah" value=1>

<input type="hidden" id="totalitem" name="totalitem" value=0>
<input type="hidden" id="posisitampil" name="posisitampil" value=0>
<input type="hidden" id="posisi" name="posisi" value="<?=$datakassa[0]['KdKategori']?>">
<input type="hidden" name="tgltrans" id="tgltrans" value="<?= $store[0]['TglTrans'] ?>">
<input type="hidden" id="posisisubkategori" name="posisisubkategori">


<input type="hidden" id="prevclass" name="prevclass" value="">
<input type="hidden" id="prevtext" name="prevtext" value="">
<input type="hidden" id="prevmenu" name="prevmenu" value="">
<input type="hidden" id="qty" name="qty" value=1>

<input type="hidden" id="totalsales" name="totalsales" value=0>
<input type="hidden" id="scsales" name="scsales" value=0>
<input type="hidden" id="taxsales" name="taxsales" value=0>
<input type="hidden" id="nettosales" name="nettosales" value=0>
<input type="hidden" id="brutosales" name="brutosales" value=0>

<input type="hidden" id="idbayar" name="idbayar" value="">
<input type="hidden" id="idnilaibayar" name="idnilaibayar" value=0>
<input type="hidden" id="idnilaidiscount" name="idnilaidiscount" value=0>
<input type="hidden" id="idpayment" name="idpayment" value="">
<input type="hidden" id="idbayartunai" name="idbayartunai" value=0>
<input type="hidden" id="idbayarkredit" name="idbayarkredit" value=0>
<input type="hidden" id="idbayardebet" name="idbayardebet" value=0>
<input type="hidden" id="idbayarvoucher" name="idbayarvoucher" value=0>
<input type="hidden" id="idbayarcompliment" name="idbayarcompliment" value=0>
<input type="hidden" id="iddiscount" name="iddiscount" value=0>

<input type="hidden" id="ketrvoucher" name="ketrvoucher" value="">
<input type="hidden" id="idvoucher" name="idvoucher" value="">
<input type="hidden" id="jenisvoucher" name="jenisvoucher" value="">
<input type="hidden" id="rupdisc" name="rupdisc" value="">

<input type="hidden" id="idnilai1valas" name="idnilai1valas" value=0>
<input type="hidden" id="idnilai2valas" name="idnilai2valas" value=0>
<input type="hidden" id="idvalas" name="idvalas" value="">
<input type="hidden" id="idvalasasli" name="idvalasasli" value="">
<input type="hidden" id="id1kurs" name="id1kurs" value="<?= 1 ?>">
<input type="hidden" id="id2kurs" name="id2kurs" value="<?= 1 ?>">

<input type="hidden" id="idcompliment" name="idcompliment" value="">
<input type="hidden" id="jeniscompliment" name="jeniscompliment" value="">
<input type="hidden" id="ketrcompl" name="ketrcompl" value="">

<input type="hidden" id="indexa" name="indexa" value="">
<input type="hidden" id="index01" name="index01" value="">
<input type="hidden" id="index02" name="index02" value="">
<input type="hidden" id="index03" name="index03" value="">

<input type="hidden" id="subindex0101" name="subindex0101" value="">
<input type="hidden" id="subindex0102" name="subindex0102" value="">
<input type="hidden" id="subindex0103" name="subindex0103" value="">
<input type="hidden" id="subindex0104" name="subindex0104" value="">
<input type="hidden" id="subindex0105" name="subindex0105" value="">
<input type="hidden" id="subindex0106" name="subindex0106" value="">
<input type="hidden" id="subindex0107" name="subindex0107" value="">
<input type="hidden" id="subindex0108" name="subindex0108" value="">
<input type="hidden" id="subindex0109" name="subindex0109" value="">
<input type="hidden" id="subindex0110" name="subindex0110" value="">
<input type="hidden" id="subindex0111" name="subindex0111" value="">
<input type="hidden" id="subindex0112" name="subindex0112" value="">
<input type="hidden" id="subindex0113" name="subindex0113" value="">
<input type="hidden" id="subindex0114" name="subindex0114" value="">
<input type="hidden" id="subindex0115" name="subindex0115" value="">
<input type="hidden" id="subindex0116" name="subindex0116" value="">

<input type="hidden" id="subindex0201" name="subindex0201" value="">
<input type="hidden" id="subindex0202" name="subindex0202" value="">
<input type="hidden" id="subindex0203" name="subindex0203" value="">
<input type="hidden" id="subindex0204" name="subindex0204" value="">
<input type="hidden" id="subindex0205" name="subindex0205" value="">
<input type="hidden" id="subindex0206" name="subindex0206" value="">
<input type="hidden" id="subindex0207" name="subindex0207" value="">
<input type="hidden" id="subindex0208" name="subindex0208" value="">
<input type="hidden" id="subindex0209" name="subindex0209" value="">
<input type="hidden" id="subindex0210" name="subindex0210" value="">
<input type="hidden" id="subindex0211" name="subindex0211" value="">
<input type="hidden" id="subindex0212" name="subindex0212" value="">
<input type="hidden" id="subindex0213" name="subindex0213" value="">
<input type="hidden" id="subindex0214" name="subindex0214" value="">
<input type="hidden" id="subindex0215" name="subindex0215" value="">

<input type="hidden" id="subindex0301" name="subindex0301" value="">
<input type="hidden" id="subindex0302" name="subindex0302" value="">
<input type="hidden" id="subindex0303" name="subindex0303" value="">
<input type="hidden" id="subindex0304" name="subindex0304" value="">
<input type="hidden" id="subindex0305" name="subindex0305" value="">
<input type="hidden" id="subindex0306" name="subindex0306" value="">
<input type="hidden" id="subindex0307" name="subindex0307" value="">
<input type="hidden" id="subindex0308" name="subindex0308" value="">
<input type="hidden" id="subindex0309" name="subindex0309" value="">
<input type="hidden" id="subindex0310" name="subindex0310" value="">
<input type="hidden" id="subindex0311" name="subindex0311" value="">
<input type="hidden" id="subindex0312" name="subindex0312" value="">
<input type="hidden" id="subindex0313" name="subindex0313" value="">
<input type="hidden" id="subindex0314" name="subindex0314" value="">

<input type="hidden" id="subindex0315" name="subindex0315" value="">
<input type="hidden" id="subindex0316" name="subindex0316" value="">
<input type="hidden" id="subindex0317" name="subindex0317" value="">
<input type="hidden" id="subindex0318" name="subindex0318" value="">

<input type="hidden" id="subindex0117" name="subindex0117" value="">
<input type="hidden" id="subindex0118" name="subindex0118" value="">
<input type="hidden" id="subindex0119" name="subindex0119" value="">
<input type="hidden" id="subindex0120" name="subindex0120" value="">
<input type="hidden" id="subindex0121" name="subindex0122" value="">


<input type="hidden" id="idpersonal" name="idpersonal" value="">
<input type="hidden" id="idtable" name="idtable" value="">
<input type="hidden" id="idguest" name="idguest" value="">
<input type="hidden" id="idagent" name="idagent" value="">

<input type="hidden" id="satuan01" name="satuan01" value="">
<input type="hidden" id="satuan02" name="satuan02" value="">
<input type="hidden" id="satuan03" name="satuan03" value="">
<input type="hidden" id="satuan04" name="satuan04" value="">
<input type="hidden" id="satuan05" name="satuan05" value="">
<input type="hidden" id="satuan06" name="satuan06" value="">
<input type="hidden" id="satuan07" name="satuan07" value="">
<input type="hidden" id="satuan08" name="satuan08" value="">
<input type="hidden" id="satuan09" name="satuan09" value="">
<input type="hidden" id="satuan10" name="satuan10" value="">
<input type="hidden" id="satuan11" name="satuan11" value="">
<input type="hidden" id="satuan12" name="satuan12" value="">
<input type="hidden" id="satuan13" name="satuan13" value="">
<input type="hidden" id="satuan14" name="satuan14" value="">
<input type="hidden" id="satuan15" name="satuan15" value="">

<input type="hidden" id="dharga01" name="dharga01" value="">
<input type="hidden" id="dharga02" name="dharga02" value="">
<input type="hidden" id="dharga03" name="dharga03" value="">
<input type="hidden" id="dharga04" name="dharga04" value="">
<input type="hidden" id="dharga05" name="dharga05" value="">
<input type="hidden" id="dharga06" name="dharga06" value="">
<input type="hidden" id="dharga07" name="dharga07" value="">
<input type="hidden" id="dharga08" name="dharga08" value="">
<input type="hidden" id="dharga09" name="dharga09" value="">
<input type="hidden" id="dharga10" name="dharga10" value="">
<input type="hidden" id="dharga11" name="dharga11" value="">
<input type="hidden" id="dharga12" name="dharga12" value="">
<input type="hidden" id="dharga13" name="dharga13" value="">
<input type="hidden" id="dharga14" name="dharga14" value="">
<input type="hidden" id="dharga15" name="dharga15" value="">

<input type="hidden" id="komisi01" name="komisi01" value="">
<input type="hidden" id="komisi02" name="komisi02" value="">
<input type="hidden" id="komisi03" name="komisi03" value="">
<input type="hidden" id="komisi04" name="komisi04" value="">
<input type="hidden" id="komisi05" name="komisi05" value="">
<input type="hidden" id="komisi06" name="komisi06" value="">
<input type="hidden" id="komisi07" name="komisi07" value="">
<input type="hidden" id="komisi08" name="komisi08" value="">
<input type="hidden" id="komisi09" name="komisi09" value="">
<input type="hidden" id="komisi10" name="komisi10" value="">
<input type="hidden" id="komisi11" name="komisi11" value="">
<input type="hidden" id="komisi12" name="komisi12" value="">
<input type="hidden" id="komisi13" name="komisi13" value="">
<input type="hidden" id="komisi14" name="komisi14" value="">
<input type="hidden" id="komisi15" name="komisi15" value="">

<input type="hidden" id="printer01" name="printer01" value="">
<input type="hidden" id="printer02" name="printer02" value="">
<input type="hidden" id="printer03" name="printer03" value="">
<input type="hidden" id="printer04" name="printer04" value="">
<input type="hidden" id="printer05" name="printer05" value="">
<input type="hidden" id="printer06" name="printer06" value="">
<input type="hidden" id="printer07" name="printer07" value="">
<input type="hidden" id="printer08" name="printer08" value="">
<input type="hidden" id="printer09" name="printer09" value="">
<input type="hidden" id="printer10" name="printer10" value="">
<input type="hidden" id="printer11" name="printer11" value="">
<input type="hidden" id="printer12" name="printer12" value="">
<input type="hidden" id="printer13" name="printer13" value="">
<input type="hidden" id="printer14" name="printer14" value="">
<input type="hidden" id="printer15" name="printer15" value="">

<input type="hidden" id="pdisc01" name="pdisc01" value="">
<input type="hidden" id="pdisc02" name="pdisc02" value="">
<input type="hidden" id="pdisc03" name="pdisc03" value="">
<input type="hidden" id="pdisc04" name="pdisc04" value="">
<input type="hidden" id="pdisc05" name="pdisc05" value="">
<input type="hidden" id="pdisc06" name="pdisc06" value="">
<input type="hidden" id="pdisc07" name="pdisc07" value="">
<input type="hidden" id="pdisc08" name="pdisc08" value="">
<input type="hidden" id="pdisc09" name="pdisc09" value="">
<input type="hidden" id="pdisc10" name="pdisc10" value="">
<input type="hidden" id="pdisc11" name="pdisc11" value="">
<input type="hidden" id="pdisc12" name="pdisc12" value="">
<input type="hidden" id="pdisc13" name="pdisc13" value="">
<input type="hidden" id="pdisc14" name="pdisc14" value="">
<input type="hidden" id="pdisc15" name="pdisc15" value="">

<input type="hidden" id="pservicecharge01" name="pservicecharge01" value="">
<input type="hidden" id="pservicecharge02" name="pservicecharge02" value="">
<input type="hidden" id="pservicecharge03" name="pservicecharge03" value="">
<input type="hidden" id="pservicecharge04" name="pservicecharge04" value="">
<input type="hidden" id="pservicecharge05" name="pservicecharge05" value="">
<input type="hidden" id="pservicecharge06" name="pservicecharge06" value="">
<input type="hidden" id="pservicecharge07" name="pservicecharge07" value="">
<input type="hidden" id="pservicecharge08" name="pservicecharge08" value="">
<input type="hidden" id="pservicecharge09" name="pservicecharge09" value="">
<input type="hidden" id="pservicecharge10" name="pservicecharge10" value="">
<input type="hidden" id="pservicecharge11" name="pservicecharge11" value="">
<input type="hidden" id="pservicecharge12" name="pservicecharge12" value="">
<input type="hidden" id="pservicecharge13" name="pservicecharge13" value="">
<input type="hidden" id="pservicecharge14" name="pservicecharge14" value="">
<input type="hidden" id="pservicecharge15" name="pservicecharge15" value="">


<input type="hidden" name="EditFlg" id="EditFlg" value=0>
<input type="hidden" id="NoUrut" name="NoUrut" size="2" value="" onkeydown="keyShortcut(event,'editdata',this)">	
<input type="hidden" id="pcode" name="pcode" size="15" maxlength="15" onkeydown="keyShortcut(event,'pcode',this)">
<input type="hidden" id="tmppcode" name="tmppcode" value="">
<input type="hidden" readonly id="nmbrg" name="nmbrg" size="60" value="">
<input type="hidden" id="qty" name="qty" size="9" onKeyUp="SetQty()" onkeydown="keyShortcut(event,'qty',this)" class="InputAlignRight">
<input type="hidden" id="satuan" name="satuan" value="" class="InputAlignRight">
<input type="hidden" id="harga" name="harga" value="" class="InputAlignRight">
<input type="hidden" id="disc" name="disc" value="" class="InputAlignRight">
<input type="hidden" id="pdisc" name="pdisc" value="" class="InputAlignRight">
<input type="hidden" id="pservicecharge" name="pservicecharge" value="" class="InputAlignRight">
<input type="hidden" id="netto" name="netto" value="" class="InputAlignRight">
<input type="hidden" id="komisi" name="komisi" value="" class="InputAlignRight">
<input type="hidden" id="printer" name="printer" value="" class="InputAlignRight">
<input type="hidden" id="keterangan" name="keterangan" size="60" onkeydown="keyShortcut(event,'keterangan',this)" value="">

<div style="display:none">
<table name="detail1" id="detail1"> 
<tr id="baris0">
    <td>
		<input type="hidden" size="2" id="nourut0" name="nourut[]"readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="15" id="pcode0" name="pcode[]" readonly="readonly" value="">
		<input type="hidden" size="60" id="nama0" name="nama[]" readonly="readonly" value="">		
		<input type="hidden" size="9" id="qty0" name="qty[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="harga0" name="harga[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="disc0" name="disc[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="pdisc0" name="pdisc[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="pservicecharge0" name="pservicecharge[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="netto0" name="netto[]" readonly="readonly" value="" class="InputAlignRight">
		<input type="hidden" size="9" id="satuan0" name="satuan[]" readonly="readonly" value="">
		<input type="hidden" size="2" id="printer0" name="printer[]" readonly="readonly" value="">
		<input type="hidden" size="2" id="komisi0" name="komisi[]" readonly="readonly" value="">
		<input type="hidden" size="60" id="keterangan0" name="keterangan[]"readonly="readonly" value="">
		<input type="hidden" id="counter0" name="counter[]">
		<input type="hidden" id="counterambil0" name="counterambil[]">
		<input type="hidden" id="tmppcode0" name="tmppcode[]" value="">
		<input type="hidden" id="tmpqty0" name="tmpqty[]" value="">
	</td>
</tr>
</table>
</div>

<? 
for($z=0;$z<count($barang);$z++)
{
?>
		<input type="hidden" id="namalengkap<?=$z?>" name="namalengkap[]" value="<?=$barang[$z]['namalengkap']?>">
		<input type="hidden" id="pcodebrg<?=$z?>" name="pcodebrg[]" value="<?=$barang[$z]['pcode']?>">
		<input type="hidden" id="kdkategori<?=$z?>" name="kdkategori[]" value="<?=$barang[$z]['kdkategori']?>">
		<input type="hidden" id="kdsubkategori<?=$z?>" name="kdsubkategori[]" value="<?=$barang[$z]['kdsubkategori']?>">
		<input type="hidden" id="hargabrg<?=$z?>" name="hargabrg[]" value="<?=$barang[$z]['harga']?>">
		<input type="hidden" id="discbrg<?=$z?>" name="discbrg[]" value="<?=$barang[$z]['disc']?>">
		<input type="hidden" id="satuanbrg<?=$z?>" name="satuanbrg[]" value="<?=$barang[$z]['satuan']?>">
		<input type="hidden" id="flagready<?=$z?>" name="flagready[]" value="<?=$barang[$z]['flagready']?>">
		<input type="hidden" id="komisibrg<?=$z?>" name="komisibrg[]" value="<?=$barang[$z]['komisi']?>">
		<input type="hidden" id="printerbrg<?=$z?>" name="printerbrg[]" value="<?=$barang[$z]['printer']?>">
<?
}
?>
<input type="hidden" id="lenbarang" name="lenbarang" value=<?=$z?>>
<?
for($z=0;$z<count($kategori);$z++)
{
?>
		<input type="hidden" id="kdkategori0<?=$z?>" name="kdkategori0[]" value="<?=$kategori[$z]['kdkategori']?>">
		<input type="hidden" id="namakategori0<?=$z?>" name="namakategori[]" value="<?=$kategori[$z]['namakategori']?>">
<?
}
?>
<input type="hidden" id="lenkategori" name="lenkategori" value=<?=$z?>>
<?
for($z=0;$z<count($subkategori);$z++)
{
?>
		<input type="hidden" id="_kdkategori<?=$z?>" name="_kdkategori[]" value="<?=$subkategori[$z]['kdkategori']?>">
		<input type="hidden" id="_kdsubkategori<?=$z?>" name="_kdsubkategori[]" value="<?=$subkategori[$z]['kdsubkategori']?>">
		<input type="hidden" id="namasubkategori<?=$z?>" name="namasubkategori[]" value="<?=$subkategori[$z]['namasubkategori']?>">
<?
}
?>
<input type="hidden" id="lensubkategori" name="lensubkategori" value=<?=$z?>>
<?
for($z=0;$z<count($userid);$z++)
{
?>
		<input type="hidden" id="id<?=$z?>" name="id[]" value="<?=$userid[$z]['id']?>">
		<input type="hidden" id="username<?=$z?>" name="username[]" value="<?=$userid[$z]['username']?>">
		<input type="hidden" id="password<?=$z?>" name="password[]" value="<?=$userid[$z]['password']?>">
<?
}
?>
<input type="hidden" id="lenuserid" name="lenuserid" value=<?=$z?>>
<?
for($z=0;$z<count($lokasi);$z++)
{
?>
		<input type="hidden" id="kdlokasi<?=$z?>" name="kdlokasi[]" value="<?=$lokasi[$z]['kdlokasi']?>">
		<input type="hidden" id="namalokasi<?=$z?>" name="namalokasi[]" value="<?=$lokasi[$z]['keterangan']?>">
		<input type="hidden" id="adaisi<?=$z?>" name="adaisi[]" value="<?=$lokasi[$z]['AdaIsi']?>">
<?
}
?>
<input type="hidden" id="lenlokasi" name="lenlokasi" value=<?=$z?>>
<?
for($z=0;$z<count($keterangan);$z++)
{
?>
		<input type="hidden" id="ketrdtl<?=$z?>" name="ketrdtl[]" value="<?=$keterangan[$z]['keterangan']?>">
<?
}
?>
<input type="hidden" id="lenketr" name="lenketr" value=<?=$z?>>
<input type="hidden" id="statusketr" name="statusketr" value=0>
<?php 
	$posisi_store = $datakassa[0]['KdKategori']; 

	if($posisi_store == '01'){
		$classkat1 = "updown1";
		$classkat2 = "updown1";
		$classkat3 = "updown1";
		$func_click1 = "addIt(this)";
		$func_click2 = "addIt(this)";
		$func_click3 = "addIt(this)";
	}elseif($posisi_store == '02'){
		$classkat1 = "updown1";
		$classkat2 = "updown1";
		$classkat3 = "updown1";
		$func_click1 = "addIt(this)";
		$func_click2 = "addIt(this)";
		$func_click3 = "addIt(this)";
	}elseif($posisi_store == '03'){
		$classkat1 = "updown1";
		$classkat2 = "updown1";
		$classkat3 = "updown1";
		$func_click1 = "addIt(this)";
		$func_click2 = "addIt(this)";
		$func_click3 = "addIt(this)";
	}
?>
<table border="1" style="width:100%;height:100%;">
  <tr>
    <td onClick="<?=$func_click1;?>" class="<?=$classkat1;?>" id='kategori01' value='1'><?=$kategori[0]['namakategori'];?></td>
    <td onClick="<?=$func_click2;?>" class="<?=$classkat2;?>" id='kategori02' value='0'><?=$kategori[1]['namakategori'];?></td>
	<td onClick="<?=$func_click3;?>" class="<?=$classkat3;?>" id='kategori03' value='0'><?=$kategori[2]['namakategori'];?></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori01' value='katergori01'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori02' value='katergori02'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori03' value='katergori03'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori04' value='katergori04'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori05' value='katergori05'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori06' value='katergori06'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori07' value='katergori07'></td>
	<td onClick="addIt2(this)" class="cancelbtn" id='cancel' value='1' colspan='1'>Cancel Trans</td>
	<td onClick="addIt2(this)" class="quitbtn" id='quit' value='1' colspan='1'>Quit</td>
  </tr>
  <tr>
    <td colspan="3" id="displaytext" class="subkategori">
	   <table>
	   <tr><td><input type="text" class="displaytextatas" size=30 id='dtext01' value='' readonly='readonly'></td></tr>
	   <tr><td><input type="text" class="displaytextatas" size=30 id='dtext02' value='' onkeyup="text02();" disabled='disabled'></td></tr>
	   </table>
	</td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori08' value='katergori08'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori09' value='katergori09'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori10' value='katergori10'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori11' value='katergori11'></td>
    <td onClick="addIt(this)" class="subkategori" id='subkategori12' value='katergori12'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori13' value='katergori13'></td>
	<td onClick="addIt(this)" class="subkategori" id='subkategori14' value='katergori14'></td>
    <td height="30" align="center" class="displaytotal" id="displaytotal" colspan='3'> 
	<!--<div id="jam2">
	<script language="javascript">
	jam2();
	</script>
	</div>-->	
	</td>
  </tr>
  <tr>
	<td rowspan="6" colspan="3" class="boxdisplay" id="boxtext">
		<table class='tabledisplay'>
		<tr>
		<td>
		<table>
		<tr>
		<td onClick="addIt(this)" colspan="1" class="menukanan1" id="waitress" value="">User</br>-</td>
		<td onClick="addIt(this)" colspan="1" class="menukanan" id="table" value="0">Table</br>-</td>
		<td onClick="addIt(this)" colspan="1" class="menukanan" id="agent" value="0">Agen</br>-</td>
		<td onClick="addIt(this)" colspan="1" class="menukanan" id="guest" value="0">Guest</br>-</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext01' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id='up1' value='up1'>Up</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext02' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext03' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext04' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id="down1" value="0">Down</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext05' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext06' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext07' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id='search' value='0'>Search</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext08' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext09' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext10' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt(this)" class="updown" id='void' value='0'>Void</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext11' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext12' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext13' value='' onClick="klikdisptext(this)" readonly='readonly'><td><td rowspan="3" onClick="addIt2(this)" class="column3" id='commit' value='0'>Commit</td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext14' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
		<tr><td><input type="button" colspan="3" class="displaytext" size=45 id='displaytext15' value='' onClick="klikdisptext(this)" readonly='readonly'><td></tr>
	   </table>
	   </td>
	   </tr>
	   </table>
	</td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah01' value='menutengah01'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah02' value='menutengah02'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah03' value='menutengah03'></td>
	<td onClick="addIt(this)" class="updown" id='up2' value='0'>Up</td>
	<td onClick="addIt(this)" class="updown" id='pageup2' value='0' colspan="2">Pg Up</td>
	
	</tr>
  
  <tr>
	<td onClick="addIt(this)" rowspan="5" colspan="3" class="boxdisplay1" id="boxpicture" style="display:none">
		 <img width=100% height=100% src="<?=base_url();?>public/images/barang/1101.jpg"/>
	</td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah04' value='menutengah04'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah05' value='menutengah05'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah06' value='menutengah06'></td>
	<td onClick="addIt(this)" class="updown" id='down2' value='0'>Down</td>
	<td onClick="addIt(this)" class="updown" id="pagedown2" value="0" colspan="2">Pg Down</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah07' value='menutengah04'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah08' value='menutengah05'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah09' value='menutengah06'></td>
    <td onClick="addIt(this)" class="angka" id='tujuh' value='7'>7</td>
	<td onClick="addIt(this)" class="angka" id='delapan' value='8'>8</td>
    <td onClick="addIt(this)" class="angka" id='sembilan' value='9'>9</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah10' value='menutengah10'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah11' value='menutengah11'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah12' value='menutengah12'></td>
    <td onClick="addIt(this)" class="angka" id='empat' value='4'>4</td>
	<td onClick="addIt(this)" class="angka" id='lima' value='5'>5</td>
    <td onClick="addIt(this)" class="angka" id='enam' value='6'>6</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah13' value='menutengah13'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah14' value='menutengah14'></td>
	<td onClick="addIt(this)" colspan="2" class="menutengah" id='menutengah15' value='menutengah15'></td>
    <td onClick="addIt(this)" class="angka" id='satu' value='1'>1</td>
	<td onClick="addIt(this)" class="angka" id='dua' value='2'>2</td>
    <td onClick="addIt(this)" class="angka" id='tiga' value='3'>3</td>
  </tr>
  <tr>
	<td onClick="addIt(this)" class="column2" colspan="2" id='plus' value='0'>+</td>
	<td onClick="addIt(this)" class="column2" colspan="2" id='minus' value='0'>-</td>
    <td onClick="addIt(this)" class="sptombol" id='note' value=''>Note</td>
	<td onClick="addIt(this)" class="sptombol" id='clear' value='0'>Clear</td>
    <td onClick="addIt(this)" class="sptombol" id='del' value='0'>Del</td>
	<td onClick="addIt(this)" class="angka" id='nol' value='0'>0</td>
    <td onClick="addIt(this)" class="sptombol" id='enter' value='0'>Ent</td>
  </tr>
</table>
</form>
</body>
</html>