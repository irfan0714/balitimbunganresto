<html lang="en">

<head>
	<style>
		table{
			width: 100%;
			border: 0;
			
			font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}
		
		.btn_print img{
			opacity: 0.4;
    		filter: alpha(opacity=40); /* For IE8 and earlier */
			cursor: pointer;	
		}
		
		.btn_print img:hover{
			opacity: 1.0;
    		filter: alpha(opacity=100); /* For IE8 and earlier */
		}
		
		.garis_putus{
			border-bottom: 1px dotted;	
			margin: 10px 0px;
		}
 
	</style>

	<script>
		function doPrint()
		{
			document.getElementById("theform").submit();		
		}
	</script>
</head>

<body>
<?php $nodok = $this->uri->segment(4); ?>
<form name="theform" id="theform" method="post" action="<?php echo base_url() . "index.php/transaksi/po_marketing/doPrint/".$nodok.""; ?>">
    <table>
    	<tr>
    		<td colspan="100%" align="center" class="btn_print">
    			<img src="../../../../public/images/bigprinter.png" title="Print" align="Print" onclick="doPrint()"/>
    		</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%"><hr size='1' noshade></td>
    	</tr>
    	
        <tr>
            <td colspan="100%">
                <b>
                    <?= $pt->Nama ?><br>
                    <?= $pt->Alamat1 ?><br>
                    <?= $pt->Alamat2 ?><br>
                    Phone : <?= $pt->TelpPT ?>
                </b>
            </td>
        </tr>
        
        <tr>
            <td colspan="100%" align="center">
               <b><u><?php echo $judul ?></u></b><br>
               <b>No : <?php echo $nodok; ?></b>
            </td>
        </tr>
        
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
        
        <tr>
            <td width="120">Tanggal</td>
            <td width="10">:</td>
            <td width="30%"><?php echo $header->TglDokumen_; ?></td>
            
            
            <td width="120">Supplier</td>
            <td width="10">:</td>
            <td><?php echo $header->Nama; ?></td>
        </tr>
        
        <tr>
            <td>Mata Uang</td>
            <td>:</td>
            <td><?php echo $header->currencycode_; ?></td> 
            
            <td width="120">TOP</td>
            <td width="10">:</td>
            <td><?php echo $header->top_." Hari"; ?></td> 
        </tr>
        
        <tr>
            <td>Estimasi</td>
            <td>:</td>
            <td><?php echo $header->TglTerima_; ?></td>
            
            <td width="120">Contact</td>
            <td width="10">:</td>
            <td><?php echo $header->Contact; ?></td>  
        </tr>

        <tr>
            <td>No. PRM</td>
            <td>:</td>
            <td><?php echo $header->NoPr; ?></td>
            
            <!-- <td width="120">Contact</td>
            <td width="10">:</td>
            <td><?php echo $header->Contact; ?></td>   -->
        </tr>
        
    	<tr>
    		<td colspan="100%">
    			<table>
    				<thead>
        
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
	    				<tr>
	    					<td align="center" width="30">No</td>
	    					<td align="center">Nama Barang</td>
	    					<td align="center" width="50">Qty</td>
	    					<td align="center"width="120">Harga Satuan</td>
	    					<td align="center" width="120">SubTotal</td>
	    				</tr>
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
    				</thead>
    				<tbody>
    				<?php
    				$no=1;
    				foreach($detail as $val)
    				{
    					?>
    					<tr>
    						<td><?php echo $no; ?></td>
    						<td><?php echo $val["NamaBarang"]; ?></td>
    						<td align="right"><?php echo number_format($val["QtyTerima"],2); ?></td>
    						<td align="right"><?php echo number_format($val["HargaSatuan"],2); ?></td>
    						<td align="right"><?php echo number_format($val["HargaSatuan"]*$val["Qty"],2); ?></td>
    					</tr>
    					<?php
    					$no++;
					}
    				?>
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
				    	
    				</tbody>
    			</table>
    			<table border="0">
    				<tr>
    					<td style="vertical-align: top;" rowspan="4" width="630">Note : <?= $header->Keterangan_;?></td>
				        <td>Total</td>
				        <td align="right"><?= number_format($header->Jumlah,2);?></td>
    				</tr>
    				<tr>
				        <td>Disc</td>
				        <td align="right"><?= $header->DiscHarga;?></td>
    				</tr>
    				<tr>
				        <td>PPN</td>
				        <td align="right"><?= number_format($header->NilaiPPn,2);?></td>
    				</tr>
    				<tr>
				        <td>Grand Total</td>
				        <td align="right"><?= number_format($header->Total,2);?></td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
    	
    	<?php
    	$spasi = " ";
    	for($i=0;$i<20;$i++)
    	{
			$spasi .= "&nbsp;";	
		}
		
		$spasi .=" ";
    	?>
    	
    	<tr>
    		<td colspan="100%" align="center">
    			<table>
    				<tr>
    					
    					<td align="center">
    						Dibuat Oleh,
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    					<td align="center">
    						Diketahui Oleh,
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    					<td align="center">
    						Disetujui Oleh,
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
        
    </table>
</form>
</body>
</html>
