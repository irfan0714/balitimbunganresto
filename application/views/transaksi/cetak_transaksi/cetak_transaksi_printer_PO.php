<?php
$this->load->helper('path');
$this->load->helper('file');
$this->load->library('printreportlib');
$printlib = new printreportlib();
$pathDta = set_realpath(APPPATH."Report");
$IP = $printlib->getIP();
$fileName = $pathDta.$IP.$fileName2;
$no = 0;

for($a=0;$a<$tot_hal;$a++){
	if($a==0)
	{
		$printlib->PrintJudul($judullap,"w",$fileName,$fontstyle,$spasienter);
	}
	else
	{
		$printlib->PrintJudul($judullap,"a",$fileName,$fontstyle,$spasienter);
	}
	$printlib->subjudultabel_print($judul1,$niljudul1,$judul2,$niljudul2,$fileName);
	$line 	= $printlib->print_judul_detail_kertas_PO($lebar_detail,$max_field_len,$banyakBarang,$fileName,$judul_detail,$tipe_judul_detail);
	$no 	= $printlib->print_detail_kertas_PO($lebar_detail,$no,$max_field_len,$banyakBarang,$fileName,$detail[$a],$tipe_judul_detail,$judul_detail,$panjang_per_hal);
}

if($string1!=""&&isset($string1)){
	$printlib->print_footer_cetak_PO($lebar_detail,$notes,$brs_footer,$max_field_len,$fileName,$tipe_judul_detail,$judul_detail,$footer1,$footer2);
	$printlib->persetujuan($string1,$string2,$fileName);
}

header("Content-type: application/txt");
header("Content-Disposition: attachment; filename=".$IP.$fileName2);
$content = read_file($fileName);
echo $content;
?>