<?php
if($aksi=="view")
{
?>

<html lang="en">

<head>
	<style>
		table{
			width: 100%;
			border: 0;
			
			font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}
		
		.btn_print img{
			opacity: 0.4;
    		filter: alpha(opacity=40); /* For IE8 and earlier */
			cursor: pointer;	
		}
		
		.btn_print img:hover{
			opacity: 1.0;
    		filter: alpha(opacity=100); /* For IE8 and earlier */
		}
		
		.garis_putus{
			border-bottom: 1px dotted;	
			margin: 10px 0px;
		}
 
	</style>

	<script>
		function doPrint()
		{
			document.getElementById("theform").submit();		
		}
	</script>
</head>

<body>
<form name="theform" id="theform" method="post" action="<?php echo base_url() . "index.php/transaksi/reservasi_konfirmasi/doPrintPdf/".$data->NoDokumen.""; ?>" >
	
	<table>
		<tr>
			<td colspan="100%" align="center" class="btn_print">
				<img src="../../../../public/images/bigprinter.png" title="Print" align="Print" onclick="doPrint()"/>
			</td>
		</tr>

		<tr>
			<td colspan="100%"><div class="garis_putus"></div></td>
		</tr>
	
    	<tr>
    		<td><b>PT. NATURA PESONA MANDIRI</b></td>    		
    	</tr>
    	
    	<tr>
    		<td><b>NPWP : 02.231.300.1.908.000</b></td>
    	</tr>
    	
    	<tr>
    		<td align="center" colspan="100%"><b><?php echo $judul; ?></b></td>
    	</tr>
    	
    	<tr>
    		<td align="center" colspan="100%"><b>No : <?php echo $data->NoDokumen; ?></b></td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%"><hr size='1' noshade></td>
    	</tr>
    	
    	<tr>
    		<td colspan='100%'>
    			<table>
    				<tr>
    					<td width="130"><b>Tanggal</b></td>
    					<td width="5"> : </td>
    					<td width="350"><?php echo $data->TglKonfirmasi; ?></td>
    					
    					<td width="130"><b>Tour Group</b></td>
    					<td width="5"> : </td>
    					<td><?php echo $datatravel->Nama; ?></td>
    					
    				</tr>
    				<tr>
    					<td><b>Jam</b></td>
    					<td> : </td>
    					<td><?php echo $data->JamKonfirmasi; ?></td>
    					
    					
    					<td><b>Tour Leader</b></td>
    					<td> : </td>
    					<td><?php echo $data->TourLeaderName; ?></td>
    				</tr>
    				<tr>
    					<td><b>No. Reservasi</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->NoReservasi; ?></td>
    				</tr>
    				<tr>
    					<td><b>Jumlah Rombongan</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->JumlahRombongan; ?></td>
    				</tr>
    				<tr>
    					<td><b>No Stiker</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->NoStiker; ?></td>
    				</tr>
    				<tr>
    					<td><b>No Voucher</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->NoVoucher; ?></td>
    				</tr>
    				<tr>
    					<td valign="top"><b>Detail Reservasi</b></td>
    					<td valign="top"> : </td>
    					<td colspan="3">
    						<table border="0">
    						<?php
    						$no = 0 ;
    						foreach($detailreservasi as $val)
    						{
    							if(($no % 2) == 0 ) echo "<tr>";
    							
							?>
								<td><nobr>- <?php echo $val["NamaBarang"]; ?> (<?php echo number_format($val["Qty"]); ?>)</nobr></td>
							<?php	
							
								if(($no % 2) == 6 ) echo "</tr>"; 
								
								$no++;
							}
    						?>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%"><hr size='1' noshade><b>Keterangan : <?php echo $data->keterangan; ?></b></td>
    	</tr>
    	    	
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
    	
    	<tr>
    		<td colspan='100%'>
    			<table>
			    	<tr>
						<td align="center">
							<b>Tour Leader</b>
							<br>
							<br>
							<br>
							(&nbsp;<?php echo $data->TourLeaderName; ?>&nbsp;)
						</td>
						<td align="center">
							<b>SGV Receptionist</b>
							<br>
							<br>
							<br>
							(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
						</td>
			    	</tr>
    			</table>
    		</td>
    	</tr>
    	
    </table>
</form>
</body>
</html>

<?php
}
else if($aksi=="print")
{
	?>
	<style>
		table.tb_print{
			font-size:9pt;
			width: 100%;
			font-family: "Bookman Old Style", serif;
		}
	</style>
	<?php
	
	$echo_for = array("FOR TL","FOR SGV");
	
	echo "<table class='tb_print'>";
	
	for($i=0;$i<2;$i++)
	{
	?>
    	<tr>
    		<td colspan="100%" align="left"><b>PT. NATURA PESONA MANDIRI</b></td>    		
    	</tr>
    	
    	<tr>
    		<td colspan="100%" align="left"><b>NPWP : 02.231.300.1.908.000</b></td>
    	</tr>
    	
    	<tr>
    		<td align="center" colspan="100%"><b><?php echo $judul; ?></b></td>
    	</tr>
    	
    	<tr>
    		<td align="center" colspan="100%"><b>No : <?php echo $data->NoDokumen; ?></b></td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%"><hr size='1' noshade></td>
    	</tr>
    	
    	<tr>
    		<td colspan='100%'>
    			<table width="100%">
    				<tr>
    					<td width="130"><b>Tanggal</b></td>
    					<td width="5"> : </td>
    					<td width="240"><?php echo $data->TglKonfirmasi; ?></td>
    					
    					<td width="100"><b>Tour Group</b></td>
    					<td width="5"> : </td>
    					<td><?php echo $datatravel->Nama; ?></td>
    					
    				</tr>
    				<tr>
    					<td><b>Jam</b></td>
    					<td> : </td>
    					<td><?php echo $data->JamKonfirmasi; ?></td>
    					
    					
    					<td><b>Tour Leader</b></td>
    					<td> : </td>
    					<td><?php echo $data->TourLeaderName; ?></td>
    				</tr>
    				<tr>
    					<td><b>No. Reservasi</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->NoReservasi; ?></td>
    				</tr>
    				<tr>
    					<td><b>Jumlah Rombongan</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->JumlahRombongan; ?></td>
    				</tr>
    				<tr>
    					<td><b>No. Stiker</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->NoStiker; ?></td>
    				</tr>
    				<tr>
    					<td><b>No. Voucher</b></td>
    					<td> : </td>
    					<td colspan="3"><?php echo $data->NoVoucher; ?></td>
    				</tr>
    				<tr>
    					<td valign="top"><b>Detail Reservasi</b></td>
    					<td valign="top"> : </td>
    					<td colspan="3">
    						<table border="0">
    						<?php
    						$no = 0 ;
    						foreach($detailreservasi as $val)
    						{
    							if(($no % 2) == 0 ) echo "<tr>";
    							
							?>
								<td><nobr>- <?php echo $val["NamaBarang"]; ?> (<?php echo number_format($val["Qty"]); ?>)</nobr></td>
							<?php	
							
								if(($no % 2) == 6 ) echo "</tr>"; 
								
								$no++;
							}
    						?>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%"><hr size='1' noshade><b>Keterangan : <?php echo $data->keterangan; ?></b></td>
    	</tr>
    	    	
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
    	
    	<tr>
			<td align="center" width="40%">
				<b>Tour Leader</b>
				<br>
				<br>
				<br>
				(&nbsp;<?php echo $data->TourLeaderName; ?>&nbsp;)
			</td>
			<td width="20%"></td>
			<td align="center"  width="40%">
				<b>SGV Receptionist</b>
				<br>
				<br>
				<br>
				(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
			</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%" align="right"><?php echo $echo_for[$i]; ?></td>
    	</tr>
    	
    	<?php
    	if($i==0)
    	{
		?>
    	
	    	<tr>
	    		<td colspan="100%"><hr style="border: 1px; border-style: dotted;"/></td>
	    	</tr>
		
		<?php
		}
    }
	
	echo "</table>";
}
?>
