<html lang="en">

<head>
	<style>
		table{
			width: 100%;
			border: 0;

			font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}

		.btn_print img{
			opacity: 0.4;
    		filter: alpha(opacity=40); /* For IE8 and earlier */
			cursor: pointer;
		}

		.btn_print img:hover{
			opacity: 1.0;
    		filter: alpha(opacity=100); /* For IE8 and earlier */
		}

		.garis_putus{
			border-bottom: 1px dotted;
			margin: 10px 0px;
		}

	</style>

	<script>
		function doPrint()
		{
			document.getElementById("theform").submit();
		}
	</script>
</head>

<body>
<form name="theform" id="theform" method="post" action="<?php echo base_url() . "index.php/transaksi/permintaan_khusus/doPrint/".$header->NoDokumen.""; ?>">
    <table>
    	<tr>
    		<td colspan="100%" align="center" class="btn_print">
    			<img src="../../../../public/images/bigprinter.png" title="Print" align="Print" onclick="doPrint()"/>
    		</td>
    	</tr>

    	<tr>
    		<td colspan="100%"><hr size='1' noshade></td>
    	</tr>

        <tr>
            <td colspan="100%">
                <b>
                    <?= $pt->Nama ?><br>
                    <?= $pt->Alamat1 ?><br>
                    <?= $pt->Alamat2 ?><br>
                    Phone : <?= $pt->TelpPT ?>
                </b>
            </td>
        </tr>

        <tr>
            <td colspan="100%" align="center">
               <b><?php echo $judul ?></b><br>
               <b>No : <?php echo $header->NoDokumen ?></b>
            </td>
        </tr>

    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>

        <tr>
            <td width="120">Tanggal</td>
            <td width="10">:</td>
            <td width="30%"><?php echo $header->Tanggal; ?></td>


            <td width="120">Divisi</td>
            <td width="10">:</td>
            <td><?php echo $header->NamaDivisi; ?></td>
        </tr>

        <tr>
            <td>Tanggal Terima</td>
            <td>:</td>
            <td><?php echo $header->TglTerima; ?></td>


            <td width="120">Gudang</td>
            <td width="10">:</td>
            <td><?php echo $header->NamaGudang; ?></td>
        </tr>

    	<tr>
    		<td colspan="100%">
    			<table>
    				<thead>

				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
	    				<tr>
	    					<td width="30">No</td>
	    					<td width="100">PCode</td>
	    					<td>Nama Barang</td>
	    					<td width="50">Qty</td>
	    					<td width="100">Satuan</td>
	    				</tr>
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
    				</thead>
    				<tbody>
    				<?php
    				$no=1;
    				foreach($detail as $val)
    				{
    					?>
    					<tr>
    						<td><?php echo $no; ?></td>
    						<td><?php echo $val["PCode"]; ?></td>
    						<td><?php echo $val["NamaBarang"]; ?></td>
    						<td><?php echo number_format($val["Qty"],2,',','.'); ?></td>
    						<td><?php echo $val["Satuan"]; ?></td>
    					</tr>
    					<?php
    					$no++;
					}
    				?>
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div>Note : <?php echo $header->Keterangan; ?></td>
				    	</tr>

    				</tbody>
    			</table>
    		</td>
    	</tr>

    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>

    	<?php
    	$spasi = " ";
    	for($i=0;$i<20;$i++)
    	{
			$spasi .= "&nbsp;";
		}

		$spasi .=" ";
    	?>

    	<tr>
    		<td colspan="100%" align="center">
    			<table>
    				<tr>
    					<td align="center">
    						Dibuat Oleh
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $user; ?>)
    					</td>
    					<td align="center">
    						Hormat Kami
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    					<td align="center">
    						Diketahui Oleh
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    					<td align="center">
    						Diterima Oleh
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>

    </table>
</form>
</body>
</html>
