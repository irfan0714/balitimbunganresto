<form method="POST" name="search" id="search">
    <?php
    $mylib = new globallib();
    ?>
    <br>
    <br>
    <table align="left" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td nowrap colspan="8"><strong><font face="Arial" size="5">Pembayaran Komisi</font></strong></td>
        </tr>
        <tr></tr>
        <tr>
        	<td>No Transaksi</td>
        	<td  style="text-align: left;"><?= $header[0]['NoTransaksi'] ?></td>
        </tr>
        <tr>
        	<td>Tanggal</td>
        	<td style="text-align: left;"><?= $header[0]['TglTransaksi'] ?></td>
        </tr>
        <tr>
        	<td>Keterangan</td>
        	<td nowrap style="text-align: left;"><?= $header[0]['Keterangan'] ?></td>
        </tr>
        <tr>
        	<td>Kode Agen</td>
        	<td style="text-align: left;"><?= $header[0]['KdAgent'] . '-'.$header[0]['Nama'] ?></td>
        </tr>
        <tr>
        	<td>Total Sales</td>
        	<td style="text-align: right;"><?= $mylib->ubah_format($header[0]['TotalSales']) ?></td>
        </tr>
        <tr>
        	<td>Total Komisi</td>
        	<td style="text-align: right;"><?= $mylib->ubah_format($header[0]['Total']) ?></td>
        </tr>
        
    </table>
    <br>
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table border="1" cellpadding="1" cellspacing="0" class="table table-bordered table-responsive table-hover">
        <thead>
			<tr class="title_table">
				<th>No Struk</th>
				<th>Tgl Jual</th>
				<th>Kd Barang</th>
				<th>Nama Barang</th>
				<th>Jumlah</th>
				<th>Harga</th>
				<th>Potongan</th>
				<th>Netto</th>
				<th>TTL Komisi</th>
				<th>Office</th>
				<th>Guide</th>
				<th>Tour Leader</th>
				<th>Driver</th>
			</tr>
		</thead>
        <?php
        
        $tkomisi = 0;
        $toffice = 0;
        $tguide = 0;
        $ttourleader=0;
        $tdriver = 0;
        $tnetto =0;
        $thargappn=0;
        for ($s = 0; $s < count($detail); $s++) {
        	$netto = ($detail[$s]['Qty']*$detail[$s]['Harga'])-$detail[$s]['Potongan'];
        	$komisi = $netto * $detail[$s]['Persentase']/100 ;
        	$office = $netto * $detail[$s]['Komisi1']/100;
        	$guide= $netto * $detail[$s]['Komisi2']/100;
        	$tourleader=$netto * $detail[$s]['Komisi3']/100;
        	$driver=$netto * $detail[$s]['Komisi4']/100;
        	$hargappn = $detail[$s]['hargaPPN'];
        	?>
        	<tr>
            	<td><?= $detail[$s]['NoStruk'] ?></td>
                <td><?= $detail[$s]['Tgl'] ?></td>
                <td><?= $detail[$s]['PCode'] ?></td>
                <td nowrap><?= $detail[$s]['NamaLengkap'] ?></td>
                <td style="text-align: right;"><?= $detail[$s]['Qty'] ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($detail[$s]['Harga']) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($detail[$s]['Potongan']) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($netto) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($komisi) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($office) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($guide) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($tourleader) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($driver) ?></td>
            </tr>
        <?php
        	$tkomisi += $komisi;
        	$toffice += $office;
        	$tguide += $guide;
        	$ttourleader += $tourleader;
        	$tdriver += $driver;
        	$tnetto += $netto;
        	$thargappn += $hargappn;
        }
        ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="7"><b>Total</b></td>
               <td style="text-align: right;"><?= $mylib->ubah_format($tnetto) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($tkomisi) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($toffice) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($tguide) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($ttourleader) ?></td>
                <td style="text-align: right;"><?= $mylib->ubah_format($tdriver) ?></td>
            </tr>
            
    </table>
    </div>
</form>