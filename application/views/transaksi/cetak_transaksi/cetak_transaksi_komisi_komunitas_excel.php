<form method="POST" name="search" id="search">
    <?php
    $mylib = new globallib();
    ?>
    <br>
    <br>
    <table align="left" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td nowrap colspan="8"><strong><font face="Arial" size="5">Pembayaran Komisi Komunitas</font></strong></td>
        </tr>
        <tr></tr>
        <tr>
        	<td>No Transaksi</td>
        	<td  style="text-align: left;"><?= $header[0]['NoTransaksi'] ?></td>
        </tr>
        <tr>
        	<td>Tanggal</td>
        	<td style="text-align: left;"><?= $header[0]['TglTransaksi'] ?></td>
        </tr>
        <tr>
        	<td>Keterangan</td>
        	<td nowrap style="text-align: left;"><?= $header[0]['Keterangan'] ?></td>
        </tr>
        <tr>
        	<td>Kode Travel</td>
        	<td style="text-align: left;"><?= $header[0]['KdAgent'] . '-'.$header[0]['Nama'] ?></td>
        </tr>
        <tr>
        	<td>Total Sales</td>
        	<td style="text-align: right;"><?= $mylib->ubah_format($header[0]['TotalSales']) ?></td>
        </tr>
        <tr>
        	<td>Total Komisi</td>
        	<td style="text-align: right;"><?= $mylib->ubah_format($header[0]['Total']) ?></td>
        </tr>
        
    </table>
    <br>
    <div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table border="1" cellpadding="1" cellspacing="0" class="table table-bordered table-responsive table-hover">
        <thead>
			<tr class="title_table">
				<th>Tanggal</th>
				<th>Kode Tour Leader</th>
				<th>Omset</th>
				<th>Komisi</th>
			</tr>
		</thead>
        <?php
        
        $tkomisi = 0;
        $totomset = 0;
        for ($s = 0; $s < count($detail); $s++) {
        	?>
        	<tr>
                <td><?= $detail[$s]['Tgl']; ?></td>
                <td><?= $detail[$s]['KdTourleader']; ?></td>
                <td nowrap><?= round($detail[$s]['Omset']); ?></td>
				<td nowrap><?= round($detail[$s]['Omset']*1.5/100) ;?></td>
            </tr>
        <?php
        	$tkomisi += $detail[$s]['Omset']*1.5/100;
        	$totomset+= $detail[$s]['Omset'];
        }
        ?>
            <tr>
                <td nowrap align='center' bgcolor='#f7d7bb' colspan="2"><b>Total</b></td>
               <td style="text-align: right;"><?= $mylib->ubah_format($totomset) ?></td>
			   <td style="text-align: right;"><?= $mylib->ubah_format($tkomisi) ?></td>
            </tr>
            
    </table>
    </div>
</form>