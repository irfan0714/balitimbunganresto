
<?php
$this->load->library('printreportlib');
$printlib = new printreportlib();
if (empty($pt)) {
    $pt = $printlib->getNamaPT();
}
//for ($a = 0; $a < $tot_hal; $a++) {
    ?>

<form method="POST" name="search" id="search">
    <?php
    $mylib = new globallib();
    ?>
    <br>
    <br>
    <table>
        <tr>
            <td bordercolor="#FFFFFF" colspan="4">
                <font face="Courier New"><b>
                    <?= $pt->Nama ?><br>
                    <?= $pt->Alamat1 ?><br>
                    <?= $pt->Alamat2 ?><br>
                    <?= $pt->Kota ?>
                </b></font>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table align="center" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td nowrap colspan="8"><strong><font face="Arial" size="5">Standard Menu Per Bahan</font></strong></td>
        </tr>
    </table>
    <br>
    <table align="center" border="0" cellpadding="3" cellspacing="3" >
        <tr>
            <td>Nama Menu &nbsp;&nbsp;&nbsp; :</td>
            
            <td align="left"><?= $header[0]['NamaLengkap'];?></td>
        </tr>
        <tr>
            <td>Store &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td align="left"><?= $header[0]['NamaSubDivisi'];?></td>
        </tr>
        
    </table>
    <br>
    <br>
        <table border="1"  class='table table-bordered table-hover responsive'>
            <thead>
    			<tr class="title_table" style="background-color: #00FFFF">
    				<td width="50">No</td>
    				<td width="50">PCode</td>
    				<td width="50">Nama Barang</td>
    				<td width="50">Isi /Pack</td>
    				<td width="50">Qty</td>
    				<td width="50">Satuan</td>
    				<td width="50">Harga /Pack</td>
    				<td width="50">Nilai</td>
    			</tr>
    		</thead>
            <tbody>
            <?php
                $no=1;
                $sum_nilai=0;
                for($i=0; $i<count($detail);$i++)
                {
                ?>
                    <tr>
                        <td><?= $no;?></td>
                        <td><?= $detail[$i]['Pcode'];?></td>
                        <td><?= $detail[$i]['NamaLengkap'];?></td>
                        <td><?= $detail[$i]['IsiPerPack'];?></td>
                        <td><?= number_format(stripslashes($detail[$i]['Qty']),2,',','');?></td>
                        <td><?= stripslashes($detail[$i]['Satuan']);?></td>
                        <td><?= stripslashes($detail[$i]['HargaPerPack']);?></td>
                        <td><?= number_format(((stripslashes($detail[$i]['HargaPerPack'])/(stripslashes($detail[$i]['IsiPerPack'])))*(stripslashes($detail[$i]['Qty']))),0,'','.');?></td>
                    </tr>
                <?php
                $no++;
                $harga = ((stripslashes($detail[$i]['HargaPerPack'])/(stripslashes($detail[$i]['IsiPerPack'])))*(stripslashes($detail[$i]['Qty'])));

                $sum_nilai = $sum_nilai + ($harga);

                }

                    $biaya_tambahan_20          = ((($sum_nilai*20)/100));
                    $estimasi_cog_perunit       = ($sum_nilai + $biaya_tambahan_20);


                    $hargajual                  = $header[0]['Harga1c'];
                    $komisi                     = ($hargajual*20/100);

                    $service_charge             = ($hargajual*$header[0]['Service_charge']/100);
                    $ppn                        = (($hargajual + $service_charge)*($header[0]['PPN'])/100);

                    $harga_setelah_service_cas  = $hargajual + $service_charge + $ppn;


                    $net_sales                  = $hargajual - $komisi;
                    $net_profit                 = ($net_sales - $estimasi_cog_perunit);


                    $isi_cogs1                  = ($estimasi_cog_perunit / $net_sales); 
                    $net_profit_persen          = ($net_profit / $net_sales);
                ?>
             
            </tbody>
        </table>
        <br>
        <table>
        <tbody>
                <tr>
                    <td colspan="5"></td>
                    <td colspan="2" align="right" style="background-color: #FFD700  ">Estimasi Biaya Per Unit : </td>
                    <td align="right" style="background-color: #FFD700  "><?= number_format($sum_nilai, 0, '', '.'); ?></td>
                </tr>
                 <tr>
                    <td colspan="5"></td>
                    <td colspan="2" align="right" style="background-color: #ADFF2F ">Biaya Tambahan 20 % : </td>
                    <td align="right"  style="background-color: #ADFF2F "><?= number_format($biaya_tambahan_20, 0, '', '.'); ?></td>
                </tr>
                 <tr>
                    <td colspan="5"></td>
                    <td colspan="2" align="right" style="background-color:  #FF8C00">Estimasi Estimasi COG Per Unit : </td>
                    <td align="right" style="background-color:  #FF8C00"><?= number_format($estimasi_cog_perunit, 0, '', '.'); ?></td>
                </tr>
        </tbody>
        </table>
        <table>
        <tbody>
                <tr>
                    
                </tr>
                <tr>
                    <td colspan="2">Komisi 20 % : </td> 
                    <td align="right"><?= number_format($komisi,0,'','.'); ?></td>

                    <td></td>

                    <td colspan="2" align="right">Net Sales : </td>
                    <td align="right"><?= number_format($net_sales, 0, '', '.'); ?></td>
                </tr>
                 <tr>
                    <td colspan="2" style="background-color:  #FF8C00">Harga Jual : </td>
                    <td align="right" style="background-color:  #FF8C00"><?= number_format($hargajual,0,'','.'); ?></td>

                    <td></td>

                    <td colspan="2" align="right" style="background-color:  #FF8C00">COGS : </td>
                    <td align="right" style="background-color:  #FF8C00"><?= number_format($estimasi_cog_perunit, 0, '', '.'); ?></td>


                    <td align="right" style="background-color:  #FF8C00"><?= number_format($isi_cogs1,0,'','.');?> %</td>
                </tr>
                 <tr>
                    <td colspan="2">Harga Jual Setelah Service Charge & Pajak : </td>
                    <td align="right"><?= number_format($harga_setelah_service_cas, 0, '', '.'); ?></td>

                    <td>
                        
                    </td>
                    <td colspan="2" align="right">Net Profit : </td>
                    <td align="right"><?= number_format($net_profit, 0, '', '.'); ?></td>

                    <td><?= number_format($net_profit_persen, 0, '', '.'); ?> %</td>
                </tr>
            </tbody>
        </table>

<br><br><br><br>
<table width="100%" border="0" align="center">
    
        <tr>
            <td width="10" align="center" ><font face="Courier New" size="3"><b>&nbsp;&nbsp;Dibuat Oleh</b></font><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
            </td>
             <td width="10" align="center"><font face="Courier New" size="3"><b>&nbsp;&nbsp;Mengetahui,</b></font><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
            </td>
            <td align="center" width="10" ><font face="Courier New" size="3"><b>&nbsp;&nbsp;Menyetujui</b></font><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
            </td>
        </tr>
        <tr>
        </tr>

        <tr>
        </tr>

        <tr>
        </tr>

        <tr>
        </tr>

        <tr>
            
            <td width="20" align="center"><font face="Courier New" size="3"><b>&nbsp;&nbsp;&nbsp;</b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b>Bambang Sutrisno</b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b>Sr Mgr F & A</b></font></td>
        </tr>
         <tr>
            
            <td width="20" align="center"><font face="Courier New" size="3"><b>&nbsp;&nbsp;&nbsp;Finance</b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b>&nbsp;Billy Hartono Salim</b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b>CEO</b></font></td>

        </tr>
        
</table>

        
</form>