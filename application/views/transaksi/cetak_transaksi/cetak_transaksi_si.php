<html lang="en">

<head>
	<style>
		table{
			width: 100%;
			border: 0;
			
			font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			line-height: 20px;
		}
		
		.btn_print img{
			opacity: 0.4;
    		filter: alpha(opacity=40); /* For IE8 and earlier */
			cursor: pointer;	
		}
		
		.btn_print img:hover{
			opacity: 1.0;
    		filter: alpha(opacity=100); /* For IE8 and earlier */
		}
		
		.garis_putus{
			border-bottom: 1px dotted;	
			margin: 10px 0px;
		}
 
	</style>

	<script>
		function doPrint()
		{
			document.getElementById("theform").submit();		
		}
	</script>
</head>

<body>
<form name="theform" id="theform" method="post" action="<?php echo base_url() . "index.php/transaksi/sales_invoice/doPrint/".$header->invoiceno.""; ?>">
    <table>
    	<tr>
    		<td colspan="100%" align="center" class="btn_print">
    			<img src="../../../../public/images/bigprinter.png" title="Print" align="Print" onclick="doPrint()"/>
    		</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%"><hr size='1' noshade></td>
    	</tr>
    	
        <tr>
            <td colspan="100%">
                <b>
                    <?= $pt->Nama ?><br>
                    <?= $pt->Alamat1 ?><br>
                    <?= $pt->Alamat2 ?><br>
                    Phone : <?= $pt->TelpPT ?>
                </b>
            </td>
        </tr>
        
        <tr>
            <td colspan="100%" align="center">
               <b><u><?php echo $judul ?></u></b><br>
               <b>No : <?php echo $header->invoiceno; ?></b>
            </td>
        </tr>
        
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
        <tr>
            <td width="120">Tanggal Invoice</td>
            <td width="10">:</td>
            <td width="30%"><?php echo $header->sidate; ?></td>
            
            
            <td width="120">Tanggal Jatuh Tempo</td>
            <td width="10">:</td>
            <td width="30%"><?php echo $header->duedate; ?></td>
        </tr>
        
        <tr>
            <td>Kepada</td>
            <td>:</td>
            <td><?php echo $header->Nama; ?></td>  
        </tr>
        
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?php echo $header->Alamat; ?></td>  
        </tr>
        
    	<tr>
    		<td colspan="100%">
    			<table border="0">
    				<thead>
        
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
	    				<tr>
	    					<td align="center" width="30">No</td>
	    					<td align="center"width="100">PCode</td>
	    					<td align="center">Nama Barang</td>
	    					<td align="center" width="50">Qty</td>
	    					<td align="center" width="100">Satuan</td>
	    					<td align="center" width="100">Harga</td>
	    					<td align="center" width="100">Total</td>
	    				</tr>
				    	<tr>
				    		<td colspan="100%"><div class="garis_putus"></div></td>
				    	</tr>
    				</thead>
    				<tbody>
    				<?php
    				$no=1;
    				$sum_total =0;
                    $sum_disc =0;
    				foreach($detail as $val)
    				{
    					$total=$val['gross'] * $val['quantity'];
    					?>
    					<tr>
    						<td align="center"><?php echo $no; ?></td>
    						<td><?php echo $val["inventorycode"]; ?></td>
    						<td><?php echo $val["NamaLengkap"]; ?></td>
    						<td align="center"><?php echo number_format($val["quantity"],0,',','.'); ?></td>
    						<td align="center"><?php echo $val["SatuanSt"]; ?></td>
    						<td align="right"><?php echo number_format($val["gross"],0,',','.'); ?></td>
    						<td align="right"><?php echo number_format($total,0,',','.'); ?></td>
    						
    					</tr>
    					<?php
    					$sum_total += $total;
                        $sum_disc += $val['disc'] * $val['quantity'];
    					$no++;
					}
    				?>
    				<?php
    				
    					if($val["diskon"]==0){
							$potongan_diskon=0;
						}else{
							$potongan_diskon=0;
						}

						//ppn
						$potongan_ppn=(10/100)* ($sum_total-$sum_disc);
						
						//gradtotal
						$grandtotal=($sum_total-$sum_disc)+$potongan_ppn;
    					?>
				    	<tr>
				    		<td colspan="5"><div class="garis_putus"></div>Note : <?php echo $header->note; ?></td>
				    		<td align="right"><div class="garis_putus"></div>Subtotal</td>
				    		<td align="right"><div class="garis_putus"></div><?php echo number_format($sum_total,0,',','.'); ?></td>
				    	</tr>
				    	<tr>
				    	
				    		<?php $this->load->helper('terbilang');
				    	    // $grand_tothal = ($val["total"]-$potongan_diskon)+(10/100)*$val["total"];
				    	    ?>
				    		<td colspan="5">Terbilang: <?php echo terbilang($grandtotal,$style=4)." Rupiah";?></td>
				    		<td align="right">Discount</td>
				    		<td align="right"><?php echo number_format($sum_disc,0,',','.'); ?></td>
				    	</tr>
				    	<tr>
				    		<td colspan="5"></td>
				    		<td align="right">PPN</td>
				    		<td align="right"><?php echo number_format($potongan_ppn,0,',','.'); ?></td>
				    	</tr>
				    	<tr>
				    		<td colspan="5"></td>
				    		<td align="right">Grand Total</td>
				    		<td align="right"><?php echo number_format($grandtotal,0,',','.'); ?></td>
				    	</tr>
				    	
				    	
				    	
    				</tbody>
    			</table>
    		</td>
    	</tr>
    	
    	<tr>
    		<td colspan="100%">&nbsp;</td>
    	</tr>
    	
    	<?php
    	$spasi = " ";
    	for($i=0;$i<20;$i++)
    	{
			$spasi .= "&nbsp;";	
		}
		
		$spasi .=" ";
    	?>
    	
    	<tr>
    		<td colspan="100%" align="center">
    			<table>
    				<tr>
    					
    					<td align="center">
    						Penerima
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    					<td align="center">
    						Pengirim
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    					<td align="center">
    						Mengetahui,
    						<br>
    						<br>
    						<br>
    						<br>
    						(<?php echo $spasi; ?>)
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
        
    </table>
</form>
</body>
</html>
