<!--<a href="<?= base_url() ?>index.php/transaksi/<?= $url ?>">
    <img style="border:0px" src="<?= base_url() ?>public/images/bigprinter.png">1
</a>
<a href="<?= base_url() ?>index.php/keuangan/<?= $url2 ?>">
    <img style="border:0px" src="<?= base_url() ?>public/images/kepson.png">2
</a>-->
<!--<a href="<?= base_url() ?>index.php/keuangan/<?= $url3 ?>">
    <img style="border:0px" src="<?= base_url() ?>public/images/bigprinter.png">
</a>-->
<?php
$this->load->library('printreportlib');
$printlib = new printreportlib();
if (empty($pt)) {
    $pt = $printlib->getNamaPT();
}
for ($a = 0; $a < $tot_hal; $a++) {
    ?>
    <table border="0">
        <tr>
            <td bordercolor="#FFFFFF" colspan="4">
                <font face="Courier New" size="3"><b>
                    <?= $pt->Nama ?><br>
                    <?= $pt->Alamat1 ?><br>
                    <?= $pt->Alamat2 ?><br>
                    <?= $pt->Kota ?>
                </b></font>
            </td>
        </tr>
        <tr>
            <td bordercolor="#FFFFFF" colspan="4" align="center">
                <font face="Courier New" size="5"><b><?= $judullap ?></b></font>
            </td>
        </tr>
        <?php
        $printlib->subjudultabel($judul1, $niljudul1, $judul2, $niljudul2);
        ?>
        <tr>
            <td colspan="<?= $colspan_line ?>" nowrap><font face="Courier New"><hr size='1' noshade></font></td>
        </tr>
        <tr>
            <td colspan="<?= $colspan_line ?>" nowrap>
                <table border="0" style="width:100%">
                    <?php
                    $printlib->print_judul_detail($judul_detail, $tipe_judul_detail);
                    $printlib->print_detail($detail[$a], $tipe_judul_detail);
                    //$printlib->print_footer($tot_hal, $judul_netto, $sum_harga, count($judul_detail));
                    //$printlib->print_footer(count($judul_detail),(int)$a+1,$tot_hal);
                            $tot_hal1='' ;
                            for($s=0;$s<3-strlen($tot_hal);$s++)
                            {
                                $tot_hal1 .= "0";
                            }
                            $tot_hal = $tot_hal1.$tot_hal;
                        ?>
                            <tr><td colspan="8"><hr size='1' noshade></td></tr>
                        <?php
                            if($judul_netto!=""){
                                for($t = 0;$t<count($judul_netto);$t++){ ?>
                                    
                                    <tr bordercolor="#000" style='font-weight:bold;'>
                                        <td colspan="6"></td>
                                        <td nowrap="nowrap"  width = "%" align='left'>
                                            <font face="Calibri" size="3"><?php echo $judul_netto[$t]; ?></font>
                                        </td>
                                        <td nowrap="nowrap"  align="right" width = "25%">
                                            <font face="Calibri" size="3"> <?php echo $sum_harga[$t]; ?> </font>
                                        </td>
                                    </tr>
                            <?php
                                }
                            }

                                        ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                    ?>
<!-- Bikin total netto -->
<table width="30%" align="right" border="0">
    <?php
    for ($d = 0; $d < count($judul_netto); $d++) {
        ?>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $judul_netto[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $sum_harga[$d]; ?></b></font></td>
        </tr>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $biaya_tambahan[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_biaya_tambahan[$d]; ?></b></font></td>
        </tr>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $estimasi_COGunit[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_netto[$d]; ?></b></font></td>
        </tr>
        <?php
    }
    ?>
</table>
<br><br><br><br><br>
<table width="40%" align="left" border="0">
    <?php
    for ($d = 0; $d < count($judul_netto); $d++) {
        ?>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $komisi[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_komisi[$d]; ?></b></font></td>
        </tr>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $harga_jual[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_harga_jual[$d]; ?></b></font></td>
        </tr>
        <tr>
            <td width="300" align="left"><font face="Courier New" size="3"><b><?= $harga_jual_setelah[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_harga_jual_setelah[$d]; ?></b></font></td>
        </tr>
        <?php
    }
    ?>
</table>

<table width="30%" align="right" border="0">
    <?php
    for ($d = 0; $d < count($judul_netto); $d++) {
        ?>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $net_sales[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_net_sales[$d]; ?></b></font></td>
        </tr>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $cogs[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_cogs1[$d]; ?> </b></font></td>

            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_cogs_persen[$d]; ?> % </b></font></td>
        </tr>
        <tr>
            <td width="20" align="left"><font face="Courier New" size="3"><b><?= $net_profit[$d]; ?></b></font></td>
            <td width="2" align="left"><b>:</b></td>
            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $isi_net_profit[$d]; ?></b></font></td>

            <td width="5" align="right"><font face="Courier New" size="3"><b><?= $net_profit_persen[$d];?> % </b></font></td>
        </tr>
        <?php
    }
    ?>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br>
<table width="100%" border="0" align="center">
    <?php
    for ($d = 0; $d < count($dibuat); $d++) {
        ?>
        <tr>
            <td width="10" align="center" ><font face="Courier New" size="3"><b>&nbsp;&nbsp;<?= $dibuat[$d]; ?></b></font><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
            </td>
             <td width="10" align="center"><font face="Courier New" size="3"><b>&nbsp;&nbsp;<?= $mengetahui[$d]; ?></b></font><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
            </td>
            <td align="center" width="10" ><font face="Courier New" size="3"><b>&nbsp;&nbsp;<?= $menyetujui[$d]; ?></b></font><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
            </td>
        </tr>

        <tr>
            
            <td width="20" align="center"><font face="Courier New" size="3"><b>&nbsp;&nbsp;&nbsp;</b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b><?= $nama[$d]; ?></b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b><?= $nama1[$d]; ?></b></font></td>
        </tr>
         <tr>
            
            <td width="20" align="center"><font face="Courier New" size="3"><b>&nbsp;&nbsp;&nbsp;<?= $finance[$d]; ?></b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b>&nbsp;<?= $div[$d]; ?></b></font></td>
            <td width="20" align="center"><font face="Courier New" size="3"><b><?= $div1[$d]; ?></b></font></td>

        </tr>
        
        <?php
    }
    ?>
</table>
