<?php
$this->load->view('header'); 
?>
<script>
function cekTheform()
{
	if($("#noregist").val()=="")
    {
        alert("No Regist harus diiisi");
        $("#noregist").focus();
        return false;
    }
	else if($("#alasan").val()=="")
    {
        alert("Alasan harus diisi");
        $("#alasan").focus();
        return false;
    }
    
    if($("#v_type").val()=="")
    {
        alert("Tipe Harus Dipilih");
        $("#v_type").focus();
        return false;
    }
    
	if($("#tanggalbaru").val()=="" && $("#v_type").val()=='tanggal')
    {
        alert("Tanggal Yang Baru Harus Diisi");
        $("#nostiker").focus();
        return false;
    }
    
    if($("#travelbaru").val()=="" && $("#v_type").val()=='travel')
    {
        alert("Travel Yang Baru Harus Diisi");
        $("#travelbaru").focus();
        return false;
    }
    
    
    if($("#statuskomisi").val()=="1")
    {
        alert("Tidak bisa edit karena sudah proses komisi.");
        $("#nostruk").focus();
        return false;
    }
        
    document.getElementById("theform").submit();	
    return false;
}


function getregist(obj){
	var noregist = obj.value;
	
	$.ajax({
			type: "POST",
			url: '<?= base_url(); ?>index.php/transaksi/edit_transaksi_pos/getregist/' + noregist + '/',
			success: function (data){
				obj = $.parseJSON(data);
				$("#tanggal").val(obj['Tanggal']);
				$("#tourleader").val(obj['nama_tour_leader']);
				$("#nama_travel").val(obj['nama_travel']);
				$("#KdTravelLama").val(obj['KdTravel']);
				$("#statuskomisi").val(obj['statuskomisi']);
			}, 
			async: false
		});
	
}

function ubah(){
	if($("#v_type").val()=="travel"){
    $('#travelbaru').attr('disabled',false);
	$("#tanggalbaru").val("");
	$('#tanggalbaru').attr('disabled',true);
	}else{
    $('#tanggalbaru').attr('disabled',false);
	$("#tanggalbaru").focus();
	$("#travelbaru").val("");
	$('#travelbaru').attr('disabled',true);	
	}
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add Request Edit Registrasi</strong></li>
		</ol>
		
		<form method='post' id="theform" action="<?=base_url();?>index.php/transaksi/edit_transaksi_pos/save_regist">
		
	    <table class="table table-bordered responsive">   
	        <tr>
	            <td class="title_table" width="150">
	            	No. Registrasi <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            
	            	<input type="text" class="form-control-new" name="noregist" id="noregist" size="15" maxlength="15" onblur="getregist(this)">
	            	        &nbsp;&nbsp;
	            	     
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Cek
								</button>
							
					
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">
	            	Tanggal 
	            </td>
	            <td> 
	            	<input type="text" class="form-control-new" name="tanggal" id="tanggal" size="15" maxlength="15" readonly>
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">
	            	Tour Leader 
	            </td>
	            <td> 
	            	<input type="readonly" class="form-control-new" name="tourleader" id="tourleader" size="25" maxlength="15" readonly>
	            </td>
	        </tr>
	        
	        <tr>
	        	<td class="title_table" width="150">
	            	Nama Travel 
	            </td>
	            <td> 
	            	<input type="readonly" class="form-control-new" name="nama_travel" id="nama_travel" size="50" maxlength="15" readonly>
	            </td>
	        </tr>
	                            
	        <tr>
	            <td class="title_table">Type Request<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;" onchange="ubah()">
	            		<option value="">-- Pilih --</option>
	            		<option value="tanggal">Ubah Tanggal</option>
	            		<option value="travel">Ubah Travel</option>
	            	</select>  
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">
	            	Tanggal Baru<font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="" name="tanggalbaru" id="tanggalbaru" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">
	            	Travel Baru<font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<select class="form-control-new" name="travelbaru" id="travelbaru" style="width: 200px;">
	            		<option value="">Pilih Travel Baru</option>
	            		<?php
	            		foreach($travel as $val)
	            		{
	            			
							?><option value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select> 
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">
	            	Alasan <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text" class="form-control-new" name="alasan" id="alasan" size="150" maxlength="150">
	            </td>
	        </tr>
	        
	        <tr>
	        	<td>
	        		<input type="hidden"  name="statuskomisi" id="statuskomisi" size="10" maxlength="5">
	        		<input type="hidden"  name="KdTravelLama" id="KdTravelLama" size="10" maxlength="5">
	        	</td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
					<button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	    </table>
		</form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>