<?php $this->load->view('header'); 
$this->load->model('globalmodel');
$mylib = new globallib();
?>
<head>
</head>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-6">
            <b>Search</b>&nbsp;
            <input type="text" size="20" maxlength="30" placeholder="No. Struk" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php if ($search_keyword) { echo $search_keyword;} ?>" />
      		&nbsp;
            <b>Tanggal</b>&nbsp;
            <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_awal!="" && $search_tgl_awal!="00-00-0000") { echo $search_tgl_awal; }else{echo date('d-m-Y');}  ?>" name="v_tgl_mulai" id="v_tgl_mulai" size="10" maxlength="10">
	        &nbsp;s/d&nbsp;
	        <input type="text" class="form-control-new datepicker" value="<?php if($search_tgl_akhir!="" && $search_tgl_akhir!="00-00-0000") { echo $search_tgl_akhir; }else{echo date('d-m-Y');}  ?>" name="v_tgl_akhir" id="v_tgl_akhir" size="10" maxlength="10">
	         &nbsp;
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status" style="width: 70px;">
                 <option value="">All</option>
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="0">Pending</option>
                <option <?php if($search_status=="1"){ echo 'selected="selected"'; } ?> value="1">Sending</option>
                <option <?php if($search_status=="2"){ echo 'selected="selected"'; } ?> value="2">OK</option>
                <option <?php if($search_status=="3"){ echo 'selected="selected"'; } ?> value="3">Reject</option>
            </select>  
            &nbsp;
        </div>
        <div class="col-md-6" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/transaksi/edit_transaksi_pos/add_request/"; ?>" class="btn btn-success btn-icon btn-sm icon-left" title="" >Add Request POS<i class="entypo-plus"></i></a>
            <a href="<?php echo base_url() . "index.php/transaksi/edit_transaksi_pos/add_request_regist/"; ?>" class="btn btn-warning btn-icon btn-sm icon-left" title="" >Add Request Registrasi<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>No Dokumen</center></th>
		        <th width="80"><center>Created Date</center></th>
		        <th width="80"><center>Jenis</center></th>
		        <th width="150"><center>Type Request</center></th>
		        <th><center>Alasan</center></th>
		        <th width="200"><center>User Request</center></th>
		        <th width="200"><center>Approve Request</center></th>
		        <th width="80"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($list) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($list as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }
                
                if ($val["Type"] == "hapus") {
                    $tipe = "Hapus Struk";
                }else if ($val["Type"] == "stiker"){
					$tipe = "Ubah No. Stiker";
				}else if ($val["Type"] == "tanggal"){
					$tipe = "Ubah Tgl Register";
				}else if ($val["Type"] == "travel"){
					$tipe = "Ubah Travel";
				}
				
				if($val["Edit_User_Request"]==""){
					$user_request = "Add By ".$val["Add_User_Request"]." at ".$val["Add_Date_Request"];	
				}else{
					$user_request = "Edit By ".$val["Edit_User_Request"]." at ".$val["Edit_Date_Request"];
				}
				
				if($val["Status_Request"]==0){
					$status = "<font color='black'><b>Pending</b></font>";
				}else if($val["Status_Request"]==1){
					$status = "<font color='orange'><b>Sending</b></font>";
				}else if($val["Status_Request"]==2){
					$status = "<font color='blue'><b>OK</b></font>";
				}else if($val["Status_Request"]==3){
					$status = "<font color='red'><b>Reject</b></font>";
				}
				
				if($val["Status_Request"]==0 AND $val["Status_Approve"]==0){
					$ket = "New Request";
				}else if($val["Status_Request"]==1 AND $val["Status_Approve"]==0){
					$ket = "<font color='orange'>Waiting Approve</font>";
				}else if($val["Status_Request"]==2 AND $val["Status_Approve"]==1){
					if($val["Perangkat"]==1){
					$ket = "<font color='blue'>Approved By ".$val["Approve_Request"]." at ".$val["Approve_Date"]." via PC</font>";
					}else{
					$ket = "<font color='blue'>Approved By ".$val["Approve_Request"]." at ".$val["Approve_Date"]." via Telegram</font>";	
					}
				}else if($val["Status_Request"]==3 AND $val["Status_Reject"]==1){
					if($val["Perangkat"]==1){
					$ket = "<font color='red'> Rejected By ".$val["Reject_Request"]." at ".$val["Reject_Date"].", ".$val["Reject_Remark"]." via PC</font>";
					}else{
					$ket = "<font color='red'> Rejected By ".$val["Reject_Request"]." at ".$val["Reject_Date"].", ".$val["Reject_Remark"]." via Telegram</font>";
					}
				}
                
                ?>
                <tr title="<?php echo $val["id_request"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoDokumen"]; ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["Created_Date"]); ?></td>
                    <td align="left"><?php echo $val["JenisDokumen"]; ?></td>
                    <td align="center"><?php echo $tipe; ?></td>                    
                    <td align="left"><?php echo $val["Alasan"]; ?></td>
                    <td align="center"><?php echo $user_request; ?></td>
                    <td align="center"><?php echo $ket; ?></td>
                    <td align="center"><?php echo $status; ?></td>
					<td align="center">
					<?php if($val["Status_Request"]==0){?>
					        <?php if($val["Type"]=="hapus" OR $val["Type"]=="stiker"){ ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/edit_request/<?php echo $val["id_request"]; ?>"  class="btn btn-warning btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <?php }else if($val["Type"]=="tanggal" OR $val["Type"]=="travel"){ ?>
                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/edit_request_regist/<?php echo $val["id_request"]; ?>"  class="btn btn-warning btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <?php } ?>
                            <button type="button" class="btn btn-success btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Kirim Request" title="" onclick="proses('<?php echo $val["id_request"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-mail"></i>
                            </button>
                            
                            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["id_request"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                    <?php } else if($val["Status_Request"]==1){ ?>
                            <?php if($username=="trisno1402" OR $username=="indah0203" OR $username=="tonny1205"){?>
                            		<?php if($val["Type"]=="hapus" OR $val["Type"]=="stiker"){ ?>
		                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/edit_request/<?php echo $val["id_request"]; ?>"  class="btn btn-warning btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
		                            <?php }else if($val["Type"]=="tanggal" OR $val["Type"]=="travel"){ ?>
		                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/edit_request_regist/<?php echo $val["id_request"]; ?>"  class="btn btn-warning btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
		                            <?php } ?>
                            <?php }?>
                            
                            <?php if($val["Type"]=="hapus" OR $val["Type"]=="stiker"){ ?>
		                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/view_request/<?php echo $val["id_request"]; ?>"  class="btn btn-primary btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="view" title=""><i class="entypo-eye"></i></a>
		                            <?php }else if($val["Type"]=="tanggal" OR $val["Type"]=="travel"){ ?>
		                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/view_request_regist/<?php echo $val["id_request"]; ?>"  class="btn btn-primary btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="view" title=""><i class="entypo-eye"></i></a>
		                    <?php } ?>
		                    
                    <?php }else{ ?>
                    		<?php if($val["Type"]=="hapus" OR $val["Type"]=="stiker"){ ?>
		                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/view_request/<?php echo $val["id_request"]; ?>"  class="btn btn-primary btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="view" title=""><i class="entypo-eye"></i></a>
		                            <?php }else if($val["Type"]=="tanggal" OR $val["Type"]=="travel"){ ?>
		                            <a href="<?php echo base_url(); ?>index.php/transaksi/edit_transaksi_pos/view_request_regist/<?php echo $val["id_request"]; ?>"  class="btn btn-primary btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="view" title=""><i class="entypo-eye"></i></a>
		                    <?php } ?>
                    <?php } ?>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>
	function deleteTrans(nodok,url)
    {
		var r=confirm("Apakah Anda Ingin Menghapus No Dokumen ini ?")
		if (r==true)
		{
			window.location = url+"index.php/transaksi/edit_transaksi_pos/delete/"+nodok;	
		}
		else
		{
	  		return false;
		}
    }
    
    function proses(nodok,url)
    {
		var r=confirm("Apakah Anda Yakin Ingin Kirim Request ini ?")
		if (r==true)
		{
			window.location = url+"index.php/transaksi/edit_transaksi_pos/proses/"+nodok;	
		}
		else
		{
	  		return false;
		}
    }
</script>
