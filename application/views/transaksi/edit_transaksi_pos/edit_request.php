<?php
$this->load->view('header'); 
?>
<script>
function cekTheform()
{
	if($("#nostruk").val()=="")
    {
        alert("No struk harus diiisi");
        $("#nostruk").focus();
        return false;
    }
	else if($("#alasan").val()=="")
    {
        alert("Alasan harus diisi");
        $("#alasan").focus();
        return false;
    }
	if($("#nostiker").val()=="" && $("#v_type").val()=='stiker')
    {
        alert("Stiker harus dipilih");
        $("#nostiker").focus();
        return false;
    }
    
    if($("#kasir").val()=="")
    {
        alert("No struk tidak valid");
        $("#nostruk").focus();
        return false;
    }
    
    if($("#statuskomisi").val()=="1")
    {
        alert("Tidak bisa edit karena sudah proses komisi.");
        $("#nostruk").focus();
        return false;
    }
        
    document.getElementById("theform").submit();	
    return false;
}

function confirm_reject(name)
        {
        	if($("#v_alasan_reject").val()=="")
            {
            	
            	alert("Isi Alasan Reject");
		        $("#v_alasan_reject").focus();
		        return false;
            	
            }else{
			
            var r = confirm("Anda yakin ingin Reject ini ? ");
            if(r==true)
            {
            	document.getElementById("v_edit").value = '2';
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
            }
        }

function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve ini ? ");
            
            if(r==true)
            {
            	document.getElementById("v_edit").value = '2';
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }

function getstruk(obj){
	var nostruk = obj.value;
	
	$.ajax({
			type: "POST",
			url: '<?= base_url(); ?>index.php/transaksi/edit_transaksi_pos/getstruk/' + nostruk + '/',
			success: function (data){
				obj = $.parseJSON(data);
				$("#tanggal").val(obj['tanggal']);
				$("#kasir").val(obj['kasir']);
				$("#totalsales").val(obj['totalnilai']);
				$("#status").val(obj['status']);
				$("#kdagentlama").val(obj['kdagent']);
				$("#statuskomisi").val(obj['statuskomisi']);
			}, 
			async: false
		});
	
}

function muncul_reject()
        {
            document.getElementById("btn_approve").style.display = "none";    
            document.getElementById("btn_confirm_reject").style.display = "none";    
            
            document.getElementById("v_alasan_reject").style.display = "";    
            document.getElementById("btn_reject").style.display = "";    
            
            document.getElementById("v_alasan_reject").focus();
        }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit Request</strong></li>
		</ol>
		
		<form method='post' id="theform" action="<?=base_url();?>index.php/transaksi/edit_transaksi_pos/edit">
		<input type="hidden" value="<?= $list_data->id_request;?>" name="id_request" id="id_request">
		<input type="hidden" name="v_approve" id="v_approve" value="">
		<input type="hidden" name="v_reject" id="v_reject" value="">
			<input type="hidden" name="v_edit" id="v_edit" value="1">
	    <table class="table table-bordered responsive">   
	        <tr>
	            <td class="title_table" width="150">
	            	No Struk <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            
	            	<input type="text" value="<?= $list_data->NoDokumen;?>" class="form-control-new" name="nostruk" id="nostruk" size="15" maxlength="15" onblur="getstruk(this)">
	            	        &nbsp;&nbsp;
	            	     <?php if($list_data->Status_Request==0){?>
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Cek
								</button>
						<?php } ?>	
					
	            </td>
	        </tr>
	        	<td class="title_table" width="150">
	            	Tanggal 
	            </td>
	            <td> 
	            	<input type="text" value="<?= $detail[0]['tanggal'];?>" class="form-control-new" name="tanggal" id="tanggal" size="15" maxlength="15" readonly>
	            </td>
	        <tr>
	        </tr>
	        	<td class="title_table" width="150">
	            	Kasir 
	            </td>
	            <td> 
	            	<input type="readonly" value="<?= $detail[0]['kasir'];?>" class="form-control-new" name="kasir" id="kasir" size="15" maxlength="15" readonly>
	            </td>
	        <tr>
	        </tr>
	        	<td class="title_table" width="150">
	            	Total Sales 
	            </td>
	            <td> 
	            	<input type="readonly" style="text-align: right;" value="<?= $detail[0]['totalnilai'];?>" class="form-control-new" name="totalsales" id="totalsales" size="15" maxlength="15"  readonly>
	            </td>
	        <tr>
	        </tr>                     
	        <tr>
	            <td class="title_table">Type Request<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;">
	            		<option <?php if($list_data->Type=="hapus"){ echo "selected='selected'"; } ?> value="hapus">Hapus Struk</option>
	            		<option <?php if($list_data->Type=="stiker"){ echo "selected='selected'"; } ?> value="stiker">Ubah No. Stiker</option>
	            	</select>  
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">
	            	Alasan <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text" value="<?= $list_data->Alasan;?>" class="form-control-new" name="alasan" id="alasan" size="150" maxlength="150">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">
	            	No Stiker Baru<font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text"  value="<?= $list_data->NoStiker;?>" class="form-control-new" name="nostiker" id="nostiker" size="10" maxlength="5">
	            </td>
	        </tr>
	        <tr>
	        	<td>
	        		<input type="hidden"  name="status" id="status" size="10" maxlength="5">
	        		<input type="hidden"  value="<?= $detail[0]['statuskomisi'];?>" name="statuskomisi" id="statuskomisi" size="10" maxlength="5">
	        		<input type="hidden"  value="<?= $detail[0]['kdagent'];?>" name="kdagentlama" id="kdagentlama" size="10" maxlength="5">
	        	</td>
	        </tr>
	        <?php if($list_data->Status_Request==0){ ?>
	        <tr>
	        	<td colspan="100%">
					<button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/edit_transaksi_pos/"; ?>">Keluar<i class="entypo-cancel"></i></button>
		        </td>
	        </tr>
	        <?php }else{?>
	        <tr>
	        	<td colspan="100%">
	        	    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/edit_transaksi_pos/"; ?>">Keluar<i class="entypo-cancel"></i></button>
	        	               <span style="float: right;">
						            
                                    <button type="button" name="btn_approve" id="btn_approve" class="btn btn-info btn-icon btn-sm icon-left" onclick="confirm_approve('<?php echo $list_data->id_request; ?>');" value="Approve">Approve<i class="entypo-check"></i></button>    
                                    
                                    <button type="button" name="btn_confirm_reject" id="btn_confirm_reject" class="btn btn-danger btn-icon btn-sm icon-left" value="Reject" onclick="muncul_reject()">Reject<i class="entypo-check"></i></button>    
                                    
                                    
                                    <input style="display: none; width: 150px;" type="text" class="form-control-new" name="v_alasan_reject" id="v_alasan_reject" value="" placeholder="Alasan Reject">
                                    <button style="display: none;" type="button" name="btn_reject" id="btn_reject" class="btn btn-danger btn-icon btn-sm icon-left" onclick="confirm_reject('<?php echo $list_data->id_request; ?>');" value="Reject">Reject<i class="entypo-check"></i></button>    
                               </span>
		        </td>
	        </tr>
	        <?php }?>
	    </table>
		</form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>