<?php
$this->load->view('header'); 
$this->load->library('globallib');
$mylib = new globallib();
?>
<script>
function cekTheform()
{
	if($("#noregist").val()=="")
    {
        alert("No Regist harus diiisi");
        $("#noregist").focus();
        return false;
    }
	else if($("#alasan").val()=="")
    {
        alert("Alasan harus diisi");
        $("#alasan").focus();
        return false;
    }
	if($("#tanggalbaru").val()=="" && $("#v_type").val()=='tanggal')
    {
        alert("Tanggal Yang Baru Harus Diisi");
        $("#nostiker").focus();
        return false;
    }
    
    if($("#travelbaru").val()=="" && $("#v_type").val()=='travel')
    {
        alert("Travel Yang Baru Harus Diisi");
        $("#travelbaru").focus();
        return false;
    }
    
    
    if($("#statuskomisi").val()=="1")
    {
        alert("Tidak bisa edit karena sudah proses komisi.");
        $("#nostruk").focus();
        return false;
    }
        
    document.getElementById("theform").submit();	
    return false;
}


function getregist(obj){
	var noregist = obj.value;
	
	$.ajax({
			type: "POST",
			url: '<?= base_url(); ?>index.php/transaksi/edit_transaksi_pos/getregist/' + noregist + '/',
			success: function (data){
				obj = $.parseJSON(data);
				$("#tanggal").val(obj['Tanggal']);
				$("#tourleader").val(obj['nama_tour_leader']);
				$("#nama_travel").val(obj['nama_travel']);
				$("#KdTravelLama").val(obj['KdTravel']);
				$("#statuskomisi").val(obj['statuskomisi']);
			}, 
			async: false
		});
	
}

function ubah(){
	if($("#v_type").val()=="travel"){
    $('#travelbaru').attr('disabled',false);
	$("#tanggalbaru").val("");
	$('#tanggalbaru').attr('disabled',true);
	}else{
    $('#tanggalbaru').attr('disabled',false);
	$("#tanggalbaru").focus();
	$("#travelbaru").val("");
	$('#travelbaru').attr('disabled',true);	
	}

}

function confirm_reject(name)
        {
        	if($("#v_alasan_reject").val()=="")
            {
            	
            	alert("Isi Alasan Reject");
		        $("#v_alasan_reject").focus();
		        return false;
            	
            }else{
			
            var r = confirm("Anda yakin ingin Reject ini ? ");
            if(r==true)
            {
            	document.getElementById("v_edit").value = '2';
                document.getElementById("v_reject").value = '1';
                document.getElementById("theform").submit();
            }
            }
        }

function confirm_approve(name)
        {
            var r = confirm("Anda yakin ingin Approve ini ? ");
            
            if(r==true)
            {
            	document.getElementById("v_edit").value = '2';
                document.getElementById("v_approve").value = '1';
                document.getElementById("theform").submit();
            }
        }
        
function muncul_reject()
        {
            document.getElementById("btn_approve").style.display = "none";    
            document.getElementById("btn_confirm_reject").style.display = "none";    
            
            document.getElementById("v_alasan_reject").style.display = "";    
            document.getElementById("btn_reject").style.display = "";    
            
            document.getElementById("v_alasan_reject").focus();
        }
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add Request Edit Registrasi</strong></li>
		</ol>
		
		<form method='post' id="theform" action="<?=base_url();?>index.php/transaksi/edit_transaksi_pos/edit_regist">
		<input type="hidden" value="<?= $list_data->id_request;?>" name="id_request" id="id_request">
		<input type="hidden" name="v_approve" id="v_approve" value="">
		<input type="hidden" name="v_reject" id="v_reject" value="">
			<input type="hidden" name="v_edit" id="v_edit" value="1">
		
	    <table class="table table-bordered responsive">   
	        <tr>
	            <td class="title_table" width="150">
	            	No. Registrasi <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            
	            	<input type="text" value="<?=$list_data->NoDokumen;?>" class="form-control-new" name="noregist" id="noregist" size="15" maxlength="15" onblur="getregist(this)">
	            	        &nbsp;&nbsp;
							
					
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">
	            	Tanggal 
	            </td>
	            <td> 
	            	<input type="text" value="<?= $detail[0]['Tanggal'];?>" class="form-control-new" name="tanggal" id="tanggal" size="15" maxlength="15" readonly>
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">
	            	Tour Leader 
	            </td>
	            <td> 
	            	<input type="readonly" value="<?= $detail[0]['nama_tour_leader'];?>" class="form-control-new" name="tourleader" id="tourleader" size="25" maxlength="15" readonly>
	            </td>
	        </tr>
	        
	        <tr>
	        	<td class="title_table" width="150">
	            	Nama Travel 
	            </td>
	            <td> 
	            	<input type="readonly" value="<?= $detail[0]['nama_travel'];?>" class="form-control-new" name="nama_travel" id="nama_travel" size="50" maxlength="15" readonly>
	            </td>
	        </tr>
	                            
	        <tr>
	            <td class="title_table">Type Request<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;" onchange="ubah()">
	            		<option <?php if($list_data->Type=="tanggal"){ echo "selected='selected'"; } ?> value="tanggal">Ubah Tanggal</option>
	            		<option  <?php if($list_data->Type=="travel"){ echo "selected='selected'"; } ?> value="travel">Ubah Travel</option>
	            	</select>  
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">
	            	Tanggal Baru<font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text" value="<?php if($mylib->ubah_tanggal($list_data->Tanggal)=="00-00-0000") { echo ""; }else{echo $mylib->ubah_tanggal($list_data->Tanggal);}  ?>" class="form-control-new datepicker" value="" name="tanggalbaru" id="tanggalbaru" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">
	            	Travel Baru<font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<select class="form-control-new" name="travelbaru" id="travelbaru" style="width: 200px;">
	            		<option value="">Pilih Travel Baru</option>
	            		<?php
	            		foreach($travel as $val)
	            		{
	            			$selected="";
							if($list_data->KdTravel==$val["KdTravel"])
							{
								$selected='selected="selected"';
							}
							
							?><option <?php echo $selected; ?> value="<?php echo $val["KdTravel"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select> 
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">
	            	Alasan <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text" value="<?=$list_data->Alasan;?>" class="form-control-new" name="alasan" id="alasan" size="150" maxlength="150">
	            </td>
	        </tr>
	        
	        <tr>
	        	<td>
	        		<input type="hidden"  name="statuskomisi" id="statuskomisi" size="10" maxlength="5">
	        		<input type="hidden"  name="KdTravelLama" id="KdTravelLama" size="10" maxlength="5">
	        	</td>
	        </tr>
	        
	        
	        <tr>
	        	<td colspan="100%">
					<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/edit_transaksi_pos/"; ?>">Keluar<i class="entypo-cancel"></i></button>
		        </td>
	        </tr>
	         
	    </table>
		</form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>