<?php
$this->load->view('header'); 
?>
<script>
function cekTheform()
{
	if($("#nostruk").val()=="")
    {
        alert("No struk harus diiisi");
        $("#nostruk").focus();
        return false;
    }
	else if($("#alasan").val()=="")
    {
        alert("Alasan harus diisi");
        $("#alasan").focus();
        return false;
    }
	if($("#nostiker").val()=="" && $("#v_type").val()=='stiker')
    {
        alert("Stiker harus dipilih");
        $("#nostiker").focus();
        return false;
    }
    
    if($("#kasir").val()=="")
    {
        alert("No struk tidak valid");
        $("#nostruk").focus();
        return false;
    }
    
    if($("#statuskomisi").val()=="1")
    {
        alert("Tidak bisa edit karena sudah proses komisi.");
        $("#nostruk").focus();
        return false;
    }
        
    document.getElementById("theform").submit();	
    return false;
}

function getstruk(obj){
	var nostruk = obj.value;
	
	$.ajax({
			type: "POST",
			url: '<?= base_url(); ?>index.php/transaksi/edit_transaksi_pos/getstruk/' + nostruk + '/',
			success: function (data){
				obj = $.parseJSON(data);
				$("#tanggal").val(obj['tanggal']);
				$("#kasir").val(obj['kasir']);
				$("#totalsales").val(obj['totalnilai']);
				$("#status").val(obj['status']);
				$("#kdagentlama").val(obj['kdagent']);
				$("#statuskomisi").val(obj['statuskomisi']);
			}, 
			async: false
		});
	
}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit Transaksi POS</strong></li>
		</ol>
		
		<form method='post' id="theform" action="<?=base_url();?>index.php/transaksi/edit_transaksi_pos/proses">
		
	    <table class="table table-bordered responsive">   
	        <tr>
	            <td class="title_table" width="150">
	            	No Struk <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            
	            	<input type="text" class="form-control-new" name="nostruk" id="nostruk" size="15" maxlength="15" onblur="getstruk(this)">
	            	        &nbsp;&nbsp;
	            	     
								<button class="btn btn-primary" type="button" id="btngenerate" onclick="generateList();">
									Cek
								</button>
							
					
	            </td>
	        </tr>
	        	<td class="title_table" width="150">
	            	Tanggal 
	            </td>
	            <td> 
	            	<input type="text" class="form-control-new" name="tanggal" id="tanggal" size="15" maxlength="15" readonly>
	            </td>
	        <tr>
	        </tr>
	        	<td class="title_table" width="150">
	            	Kasir 
	            </td>
	            <td> 
	            	<input type="readonly" class="form-control-new" name="kasir" id="kasir" size="15" maxlength="15" readonly>
	            </td>
	        <tr>
	        </tr>
	        	<td class="title_table" width="150">
	            	Total Sales 
	            </td>
	            <td> 
	            	<input type="readonly" class="form-control-new" name="totalsales" id="totalsales" size="15" maxlength="15"  readonly>
	            </td>
	        <tr>
	        </tr>                     
	        <tr>
	            <td class="title_table">Type Request<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;">
	            		<option value="hapus">Hapus Struk</option>
	            		<option value="stiker">Ubah No. Stiker</option>
	            	</select>  
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">
	            	Alasan <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text" class="form-control-new" name="alasan" id="alasan" size="150" maxlength="150">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">
	            	No Stiker <font color="red"><b>(*)</b></font>
	            </td>
	            <td> 
	            	<input type="text"  class="form-control-new" name="nostiker" id="nostiker" size="10" maxlength="5">
	            </td>
	        </tr>
	        <tr>
	        	<td>
	        		<input type="hidden"  name="status" id="status" size="10" maxlength="5">
	        		<input type="hidden"  name="statuskomisi" id="statuskomisi" size="10" maxlength="5">
	        		<input type="hidden"  name="kdagentlama" id="kdagentlama" size="10" maxlength="5">
	        	</td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
					<button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
		        </td>
	        </tr>
	    </table>
		</form> 
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>