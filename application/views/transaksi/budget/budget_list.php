<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table>
	<tr>
		<td><input class="form-control-new" type='text' size'20' name='stSearchingKey' id='stSearchingKey'>
		<select class="form-control-new"  size="1" height="1" name ="searchby" id ="searchby">
				<option value="NamaAktivitas">Nama Aktivitas</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search"></td>
		
	</tr>
</table>
</form>

<button class="btn btn-info pull-right" style="margin-right:10px;" onclick="form_tambah_budget()"><i class="entypo-plus"></i> &nbsp Buat Budget</button>
<br><br><br>
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th rowspan="2" width="30"><center>No</center></th>
				<th rowspan="2" width="50"><center>Tahun</center></th>
		        <th rowspan="2" width="300"><center>Nama Aktivitas</center></th>
		        <th colspan="12"><center>Bulan</center></th>
                <th rowspan="2" width="100"><center>Action</center></th>
        	</tr>
			<tr>
		        <th width="50"><center>Jan</center></th>
                <th width="50"><center>Feb</center></th>
				<th width="50"><center>Mar</center></th>
				<th width="50"><center>Apr</center></th>
				<th width="50"><center>Mei</center></th>
				<th width="50"><center>Jun</center></th>
				<th width="50"><center>Jul</center></th>
				<th width="50"><center>Ags</center></th>
				<th width="50"><center>Sep</center></th>
				<th width="50"><center>Okt</center></th>
				<th width="50"><center>Nov</center></th>
				<th width="50"><center>Des</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                
                ?>
                <tr title="<?php echo $val["KdAktivitas"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="left"><?php echo $val["Tahun"]; ?></td>
                    <td align="left"><?php echo $val["NamaAktivitas"]; ?></td>
					<td align="right"><?php echo number_format($val["Budget01"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget02"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget03"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget04"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget05"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget06"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget07"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget08"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget09"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget10"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget11"],0); ?></td>
					<td align="right"><?php echo number_format($val["Budget12"],0); ?></td>
                    <td align="center">
					        <a href="<?php echo base_url(); ?>index.php/transaksi/budget/view_budget/<?php echo $val["Tahun"]; ?>/<?php echo $val["KdAktivitas"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
							<!--<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="view_budget('<?php echo $val["Tahun"];?>','<?php echo $val["KdAktivitas"];?>','<?php echo base_url(); ?>')"><i class="entypo-eye"></i></button>-->
                    		<button type=button class="btn btn-info btn-sm sm-new tooltip-primary" onclick="edit_budget('<?php echo $val["Tahun"];?>','<?php echo $val["KdAktivitas"];?>','<?php echo base_url(); ?>')"><i class="entypo-pencil"></i></button>
                            <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="delete_budget('<?php echo $val["Tahun"]; ?>','<?php echo $val["KdAktivitas"]; ?>', '<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<?php
$this->load->view('footer'); ?>

<!-- Default modal -->
    <div id="default_modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">Buat Budget</h3>
          </div>
          <!-- New invoice template -->
          <div class="panel">
            <div class="panel-body">
              <div class="row invoice-header">
                <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="form-body">
					
                        <div class="form-group">
                            <label class="control-label col-md-3">Tahun Budget</label>
                            <div class="col-md-9">
                                <select id="tahun" name="tahun" class="form-control-new">
                                    <option value="">-- Pilih --</option>
                                    <option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        
                        <label class="control-label col-md-3">Nama Aktivitas</label>
                        <div class="col-md-9">
                        <input type="text" class="form-control-new" size="20" maxlength="10" name="v_keyword_aktivitas" id="v_keyword_aktivitas" placeholder="Keyword" onblur="cari('<?php echo base_url(); ?>')">
                        <span id="select_rekening">
                        <select name="kdaktivitas" id="kdaktivitas" class="form-control-new" style="width:400px;">
                           
                           <option value=""> -- Pilih -- </option>
		            		<?php
		            		foreach($aktivitas as $val)
		            		{
		            			$selected="";
								if($act_==$val["KdRekening"])
								{
									$selected='selected="selected"';
								}
								?><option <?php echo $selected; ?> value="<?php echo $val["KdAktivitas"]; ?>"><?php echo $val["NamaAktivitas"]; ?></option><?php
							}
		            		?>
	            		</select>    
                        </span>
                        </div>
                        </div>
                    </div>
                </form>
            </div>				  
              </div>
            </div>
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <thead class="title_table">
                  <tr>
                    <th>Bulan</th>
                    <th>Jumlah</th>
                    <th>Bulan</th>
                    <th>Jumlah</th>
					<th>Bulan</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Januari</td>
                    <td>
					<input style="text-align: right;" id="budget01" name="budget01" class="form-control" type="text">
                    </td>
                    <td>Februari</td>
                    <td><input style="text-align: right;" id="budget02" name="budget02"  class="form-control" type="text">
                    </td>
					<td>Maret</td>
                    <td><input style="text-align: right;" id="budget03" name="budget03" class="form-control" type="text">
                    </td>
                  </tr>
				  
				  <tr>
                    <td>April</td>
                    <td>
					<input style="text-align: right;" id="budget04" name="budget04" class="form-control" type="text">
                    </td>
                    <td>Mei</td>
                    <td><input style="text-align: right;" id="budget05" name="budget05" class="form-control" type="text">
                    </td>
					<td>Juni</td>
                    <td><input style="text-align: right;" id="budget06" name="budget06" class="form-control" type="text">
                    </td>
                  </tr>
				  
				  <tr>
                    <td>Juli</td>
                    <td>
					<input style="text-align: right;" id="budget07" name="budget07" class="form-control" type="text">
                    </td>
                    <td>Agustus</td>
                    <td><input style="text-align: right;" id="budget08" name="budget08" class="form-control" type="text">
                    </td>
					<td>September</td>
                    <td><input style="text-align: right;" id="budget09" name="budget09" class="form-control" type="text">
                    </td>
                  </tr>
				  
				  <tr>
                    <td>Oktober</td>
                    <td>
					<input style="text-align: right;" id="budget10" name="budget10" class="form-control" type="text">
                    </td>
                    <td>November</td>
                    <td><input style="text-align: right;" id="budget11" name="budget11" class="form-control" type="text">
                    </td>
					<td>Desember</td>
                    <td><input style="text-align: right;" id="budget12" name="budget12" class="form-control" type="text">
                    </td>
                  </tr>
              </table>
            </div>
            <div class="panel-body">
              <h6>Notes &amp; Information:</h6>
			  Budget Marketing Secret Garden Village
          </div>
          <!-- /new invoice template -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button id="btnSave" type="button" onclick="save('<?php echo base_url(); ?>')" class="btn btn-success">Save</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /default modal -->

<script type="text/javascript">
		var save_method;
		
        function form_tambah_budget(){
		    save_method = 'add';
			id_='0';
			$('#form')[0].reset(); // reset form on modals
			$('.form-group').removeClass('has-error'); // clear error class
			$('.help-block').empty(); // clear error string
			$('#default_modal').modal('show'); // show modal					
		}
		
		function save(url)
		{
		
			if(save_method == 'add') {
				url_ = url+"index.php/transaksi/budget/save_budget/";
			} else {
				url_ = url+"index.php/transaksi/budget/edit_budget/";
			}
			
			var tahun=$("#tahun").val();
			var kdaktivitas=$("#kdaktivitas").val();
			var b1=$("#budget01").val();
			var b2=$("#budget02").val();
			var b3=$("#budget03").val();
			var b4=$("#budget04").val();
			var b5=$("#budget05").val();
			var b6=$("#budget06").val();
			var b7=$("#budget07").val();
			var b8=$("#budget08").val();
			var b9=$("#budget09").val();
			var b10=$("#budget10").val();
			var b11=$("#budget11").val();
			var b12=$("#budget12").val();
			
		    $('#btnSave').text('confirm...'); //change button text 
		    //window.location = url+"index.php/transaksi/aktivitas/save_aktivitas/"+aktivitas+"/"+kdrekening+"/";
			
			$.ajax({
					url: url_,
					data: {thn:tahun, aktivitas:kdaktivitas, bud1:b1, bud2:b2, bud3:b3, bud4:b4, bud5:b5, bud6:b6, bud7:b7, bud8:b8, bud9:b9, bud10:b10, bud11:b11, bud12:b12},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
							$('#default_modal').modal('hide');
							alert("Budget Berhasil Disimpan.");
							location.reload();
						
					},
					error: function(e) 
					{
						alert(e);
					} 
				});
				
		}
		
		function cari(url)
		{
			var act=$("#v_keyword_aktivitas").val();
			$.ajax({
					url: url+"index.php/transaksi/budget/ajax/",
					data: {id:act},
					type: "POST",
					dataType: 'html',					
					success: function(res)
					{
						
						$('#kdaktivitas').html(res);
					},
					error: function(e) 
					{
						alert(e);
					} 
				}); 
			
			   	
   		}
   		
   		function edit_budget(tahun,aktivitas,url){
   			
			save_method = 'edit';
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/budget/edit/"+tahun+"/"+aktivitas+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
			            $('[name="tahun"]').val(data.Tahun);
						$('[name="kdaktivitas"]').val(data.KdAktivitas);
						$('[name="budget01"]').val(data.Budget01);
						$('[name="budget02"]').val(data.Budget02);
						$('[name="budget03"]').val(data.Budget03);
						$('[name="budget04"]').val(data.Budget04);
						$('[name="budget05"]').val(data.Budget05);
						$('[name="budget06"]').val(data.Budget06);
						$('[name="budget07"]').val(data.Budget07);
						$('[name="budget08"]').val(data.Budget08);
						$('[name="budget09"]').val(data.Budget09);
						$('[name="budget10"]').val(data.Budget10);
						$('[name="budget11"]').val(data.Budget11);
						$('[name="budget12"]').val(data.Budget12);
						$('#tahun').attr('disabled',true);
						$('#v_keyword_aktivitas').attr('disabled',true);
						$('#kdaktivitas').attr('disabled',true);
			            $('#default_modal').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}
		
		function view_budget(tahun,aktivitas,url){
   			
			save_method = 'view';
			$('.form-group').removeClass('has-error');	
			$('.error_message').css('display','none');
			
			    //Ajax Load data from ajax
			    $.ajax({
			        url : url+"index.php/transaksi/budget/edit/"+tahun+"/"+aktivitas+"/",
			        type: "GET",
			        dataType: "json",
			        success: function(data)
			        {
			            $('[name="tahun"]').val(data.Tahun);
						$('[name="kdaktivitas"]').val(data.KdAktivitas);
						$('[name="budget01"]').val(data.Budget01);
						$('[name="budget02"]').val(data.Budget02);
						$('[name="budget03"]').val(data.Budget03);
						$('[name="budget04"]').val(data.Budget04);
						$('[name="budget05"]').val(data.Budget05);
						$('[name="budget06"]').val(data.Budget06);
						$('[name="budget07"]').val(data.Budget07);
						$('[name="budget08"]').val(data.Budget08);
						$('[name="budget09"]').val(data.Budget09);
						$('[name="budget10"]').val(data.Budget10);
						$('[name="budget11"]').val(data.Budget11);
						$('[name="budget12"]').val(data.Budget12);
						$('#tahun').attr('disabled',true);
						$('#v_keyword_aktivitas').attr('disabled',true);
						$('#kdaktivitas').attr('disabled',true);
						$('#btnSave').attr('disabled',true);
			            $('#default_modal').modal('show'); // show bootstrap modal when complete loaded

			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            alert('Error get data from ajax');
			        }
			    });
		}
							
		function delete_budget(tahun,id,url)
		{
			if(confirm('Are you sure delete this data?'))
			{
				// ajax delete data to database
				$.ajax({
					url : url+"index.php/transaksi/budget/delete_budget/",
					data: {tahun:tahun,id:id},
					type: "POST",
					dataType: "html",
					success: function(data)
					{
						    $('#default_modal').modal('hide');
							alert("Aktivitas Berhasil Dihapus.");
							location.reload();
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error deleting data');
					}
				});

			}
		}

</script>