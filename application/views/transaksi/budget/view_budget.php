<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Budget";
?>
<script language="javascript" src="<?=base_url();?>public/js/rg_marketing.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>View <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/rg_marketing/save_data'>
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">Tahun</td>
	            <td><b><?php echo $header[0]['Tahun']; ?></b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Nama Aktivitas</td>
	            <td><b><?php echo $header[0]['NamaAktivitas']; ?></b></td>
	        </tr>
	        
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="50"><center>Tahun</center></th>
								<th width="50"><center>Bulan</center></th>								
							    <th width="300"><center>Nama Aktivitas</center></th>
							    <th width="100"><center>Budget</center></th>
							    <th width="100"><center>Terpakai</center></th>
							    <th width="100"><center>Realisasi</center></th>
								<th width="100"><center>Sisa</center></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($budget01 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget01'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai01'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi01'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>	
							
						<?php foreach($budget02 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget02'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai02'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi02'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget03 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget03'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai03'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi03'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget04 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget04'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai04'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi04'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget05 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget05'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai05'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi05'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget06 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget06'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai06'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi06'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget07 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget07'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai07'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi07'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						
						<?php foreach($budget08 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget08'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai08'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi08'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget09 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget09'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai09'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi09'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						
						<?php foreach($budget10 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget10'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai10'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi10'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget11 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget11'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai11'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi11'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
						<?php } ?>
						
						<?php foreach($budget12 AS $val){  ?>
							<tr>
							<td><?php echo $val['Tahun']?></td>
							<td><?php echo $val['Bulan']?></td>
							<td><?php echo $val['NamaAktivitas']?></td>
							<td align="right"><?php echo number_format($val['Budget12'],0)?></td>
							<td align="right"><?php echo number_format($val['Terpakai12'],0)?></td>
							<td align="right"><?php echo number_format($val['Realisasi12'],0)?></td>
							<td align="right"><?php echo number_format($val['sisa'],0)?></td>
							</tr>
							
							
						<?php } ?>
							
							<tr>
							<td align="right" colspan="3"><b>Total</b></td>
							<td align="right"><?php echo number_format($totbudget[0]['budget'],0);?></td>
							<td align="right"><?php echo number_format($totbudget[0]['terpakai'],0);?></td>
							<td align="right"><?php echo number_format($totbudget[0]['realisasi'],0);?></td>
							<td align="right"><?php echo number_format($totbudget[0]['budget']-$totbudget[0]['realisasi'],0);?></td>
							</tr>
						</tbody>
	        	</td>
	        </tr>
	        
				
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/budget/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<font style="color: red; font-style: italic; font-weight: bold;"></font>
					</td>
		        </tr>
			
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header[0]['AddUser'])
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header[0]['AddUser_']." :: ".$header[0]['AddDate_']; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header[0]['EditUser_']." :: ".$header[0]['EditDate_']; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>