<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'lainlain',this)\"";
//$cek = 'onchange="cek_tgl_approve_so_type3()';
?>
<script language="javascript" src="<?=base_url();?>public/js/mutasi_paket.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<script type="text/javascript">
    function cek_form() {
        if(!$('#ket').val()){
            alert('Keterangan Tidak Boleh Kosong');
            $('#ket').focus();
            return false;
        }
    }
</script>
<body>
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
					<?=$judul?>
                </div>
            </div>			
		</div>
		<div class="row">
	<div class="col-md-12">
	<form method='post' name="matauang" id="matauang" action='<?= base_url(); ?>index.php/<?=$ul1?>/<?=$ul2?>/simpan' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">

		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					Header
				</div>
            </div>
			<div class="panel-body">
                <?php 
                if (!empty($msg)) echo $msg ; 
               	?>
	      		  	<div class="form-group">
	    				<label class="col-sm-2 control-label">Tanggal</label>
	    				<div class="col-sm-5">
	        				<input name="tanggal" type="input" id="tanggal" size="10" class="control-new datepicker"/>
	    				</div>
					</div>
				<?php
				
				$mylib = new globallib();
				$url = base_url();
                echo $mylib->text_boostrap("Kode", "kode", "", "10", "10", "readonly='readonly'", "text", $gantikursor, "1");
				echo $mylib->option_boostrap_type3 ("Gudang","gd",$gudang,"","KdGudang","Keterangan","",$url);
				echo $mylib->text_boostrap("Keterangan", "ket", "", "30", "30", "", "text", $gantikursor, "1");
				?> 
			<div class="form-group">
            <label class="col-sm-2 control-label"> Jenis Paket</label>
            <div class="col-sm-2">
                <select class="form-control" size="1" id="jenis" name="jenis">
                    <option value="B">Buat Paket</option>
					<option value="P">Pecah Paket</option>
                </select>
            </div>
        </div>
			</div>
		</div>
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					Detail
				</div>
			</div>
            <div class="panel-body">
			<div id="Layer1" style="display:none">
			  <p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
		</div>
			
<div class="col-md-10">
		
		<table class="table table-bordered" id="detail">
            <thead>
				<tr id="baris0">
					<th>
					<!--<img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" id="newrow" onClick="detailNew()"> 
					-->
					</th>
					<th>Kode Paket</th>
					<th>Nama Paket</th>
					<th>Qty</th>
				</tr>
            </thead>
            <tbody id="dataTable">
                <tr id="dataTable">
                <td nowrap>
					<img src="<?=base_url();?>/public/images/del.png" width="16" height="16" id="del1" border="0" onClick="deleteRow(this)">
				</td>
                <td nowrap>
					<div class="col-sm-6">
					<input type="text" class="form-control" id="pcode1" name="pcode[]" size="25" maxlength="20" placeholder="Kode Barang" onkeydown="keyShortcut(event,'pcode',this)">
					</div>
                    <img src="<?=base_url();?>/public/images/pick.png" width="16" height="16" id="pick1" border="0" onClick="pickThis(this)"> 
					
				</td>
				<td nowrap>
					<input type="text" class="form-control" id="nama1" name="nama[]" size="35" readonly="readonly" >
				</td>
                <td nowrap>
					<input type="text" class="form-control" id="qty1" name="qty[]" size="5" maxlength="11" onkeyup="InQty(this)">
                </td>
                                </tr>
                            </tbody>
			</table>                   
		
	</div> 
	</div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
						<input type="hidden" id="baseurl" name="baseurl" value="<?=base_url();?>" >
						<input type="hidden" id="flag" name="flag" value="add" >
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/<?=$ul1?>/<?=$ul2?>/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
