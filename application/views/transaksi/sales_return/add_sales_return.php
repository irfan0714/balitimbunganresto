<?php 

$this->load->view('header'); 

$modul = "Sales Return";
//test
?>

<script language="javascript" src="<?=base_url();?>public/js/sales_return_v2.js"></script>
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/sales_return/save_data'>
		
	    <table class="table table-bordered responsive">                        

	        
	        <tr>
	            <td class="title_table" width="150">Tanggal <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Delivery No <font color="red"><b>(*)</b></font></td>
	            <td>
	            	<input type="text" class="form-control-new" name="deliveryorderno" id="deliveryorderno" value="" style="width: 180px;"/>
				    <a href="javascript:void(0)" id="get_dono<?php echo $no;?>" onclick="pickThis2(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				 </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Subdivisi <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_subdivisi" id="v_subdivisi" style="width: 200px;">
	            		<option value="">Pilih Subdivisi</option>
	            		<?php
	            		foreach($subdivisi as $val)
	            		{
							?><option value="<?php echo $val["KdSubdivisi"]; ?>"><?php echo $val["NamaSubDivisi"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Gudang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_gudang" id="v_gudang" style="width: 200px;">
	            		<option value="">Pilih Gudang</option>
	            		<?php
	            		foreach($gudang as $val)
	            		{
							?><option value="<?php echo $val["KdGudang"]; ?>"><?php echo $val["Keterangan"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Customer <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<select class="form-control-new" name="v_customer" id="v_customer" style="width: 200px;">
	            		<option value="">Pilih Customer</option>
	            		<?php
	            		foreach($customer as $val)
	            		{
							?><option value="<?php echo $val["KdCustomer"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Note <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
           	        
	        <tr>
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="200"><center>Note</center></th>
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;" onblur="UpdateItemID(this);"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="right">
				                	<input type="text" class="form-control-new" name="v_qty[]" onblur="toFormat2('v_qty<?php echo $no;?>')" id="v_qty<?php echo $no;?>" value="" style="text-align: right;"/>
				                </td>
								<td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan<?php echo $no;?>">
				                	<option value=""> -- Pilih -- </option>
				                	</select>
																			
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_note_detail[]" id="v_note_detail<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
	        		<div class="row" style="overflow:auto;height:150px;">
						<table class="table table-bordered responsive">
							<tbody>
							
								<?php
								$name_prev = "";
								foreach($arr_data["list_market"] as $counter => $val)
								{
									$market_list_name = $arr_data["market_list_name"][$counter];
									$pcode = $arr_data["pcode"][$counter];
									$namalengkap = $arr_data["namalengkap"][$counter];
									$satuanst = $arr_data["satuanst"][$counter];
									
									$name_echo = "&nbsp;";
	                                if($name_prev!=$market_list_name)
	                                {
	                                    $name_echo = $market_list_name;    
	                                } 
									
								?>
								<tr>
					                <td>
						                <input type="hidden" name="market_pcode[]" id="market_pcode<?php echo $counter;?>" value="<?php echo $pcode; ?>"/>
						                <input type="hidden" name="market_namabarang[]" id="market_namabarang<?php echo $counter;?>" value="<?php echo $namalengkap; ?>"/>
						                <input type="hidden" name="market_satuan[]" id="market_satuan<?php echo $counter;?>" value="<?php echo $satuanst; ?>"/>
						                <?php echo $name_echo; ?>
					                </td>
					                <td><?php echo $pcode; ?></td>
					                <td><?php echo $namalengkap; ?></td>
					                <td align="right">
					                	<input type="text" class="form-control-new" name="market_qty[]" onblur="toFormat2('market_qty<?php echo $counter;?>')" id="market_qty<?php echo $counter;?>" value="" style="text-align: right;"/>
					                </td>
					                <td><?php echo $satuanst; ?></td>
					            </tr>
								<?php	
								
									$name_prev = $market_list_name;							
								}
								?>
								
							</tbody>
						</table>
	        		</div>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/sales_return/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>