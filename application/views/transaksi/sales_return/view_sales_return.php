<?php 
$this->load->view('header'); 
$this->load->library('globallib');

$mylib = NEW Globallib;

$modul = "Delivery Order";
//test
$counter=1;
foreach($detail_list as $val)
{
	$arr_data["list_market"][$counter]=$counter;
	$arr_data["pcode"][$counter]=$val["inventorycode"];
	$arr_data["quantity"][$counter]=$val["quantity"];
	$arr_data["satuan"][$counter]=$val["SatuanSt"];
	$arr_data["namalengkap"][$counter]=$val["NamaLengkap"];
	
	$counter++;
}

?>
<script language="javascript" src="<?=base_url();?>public/js/sales_return_v2.js"></script>
<div class="row" >
    <div class="col-md-12" align="left">
			
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
			<span style="float: right; display: none;" id="show_image_ajax_form"><img src="images/ajax-image.gif" /></span>
		</ol>
    	
		<?php
		if($this->session->flashdata('msg'))
		{
		  $msg = $this->session->flashdata('msg');
		  
		  ?><div class="alert alert-<?php echo $msg['class'];?>"><?php echo $msg['message']; ?></div><?php
		}
		?>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/sales_return/save_data'>
	    <input type="hidden" name="v_no_dokumen" id="v_no_dokumen" value="<?php echo $header->returnno; ?>">
	    <input type="hidden" name="deliveryorderno" id="deliveryorderno" value="<?php echo $header->dono; ?>">
	    <input type="hidden" name="v_gudang" id="v_gudang" value="<?php echo $header->warehousecode; ?>">
	    <table class="table table-bordered responsive">
	                 
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b><?php echo $header->returnno; ?></b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Delivery No</td>
	            <td><b><?php echo $header->dono; ?></b></td>
	        </tr>
	                       
	        <tr>
	            <td class="title_table" width="150">Tanggal </td>
	            <td> 
	            <?php
	            if($header->status==1)
	            {
					?>
					<input type="hidden" class="form-control-new datepicker" value="<?php if($header->returndate_indo!="" && $header->returndate_indo!="0000-00-00") { echo $header->dodate; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10">
					<input type="text" class="form-control-new datepicker" value="<?php if($header->returndate_indo!="" && $header->returndate_indo!="00-00-0000") { echo $header->returndate_indo; }else{echo date('d-m-Y');}  ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10" disabled="true>	
				<?php	
				}
				else
				{
					?><input type="hidden" value="<?php echo $header->dodate; ?>" name="v_tgl_dokumen" id="v_tgl_dokumen" size="10" maxlength="10"><?php	
					echo "<b>".$header->dodate."<b>";
				}
	            ?>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Warehouse</td>
	            <td><b><?php echo $header->warehousecode." :: ".$header->Keterangan; ?></b></td>
	        </tr>
	        
	        <tr>
	            <td class="title_table">Customer </td>
	            <td> 
	            	<select class="form-control-new" name="v_customer" id="v_customer" style="width: 200px;">
	            		<option value="">Pilih Customer</option>
	            		<?php
	            		foreach($customer as $val)
	            		{
	            			$selected="";
							if($header->customerid==$val["KdCustomer"])
							{
								$selected='selected="selected"';
							}
							?><option <?php echo $selected; ?> value="<?php echo $val["KdCustomer"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>   
	            </td>
	        </tr>
	        	        
	        <tr>
	            <td class="title_table">Note</td>
	            <td><input type="text" class="form-control-new" value="<?php echo $header->note; ?>" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        
		       <tr>
		            <td class="title_table">Status <font color="red"><b>(*)</b></font></td>
		            <td>
		            <?php
		            if($header->status==0)
		            {
						echo "<b>Pending</b>";
					}
					else if($header->status==1)
		            {
						echo "<b>Close</b>";
					}else if($header->status==2)
		            {
						echo "<b>Void</b>";
					}
		            ?>
		            	
		            </td>
		        </tr> 
            
            
			<?php
			if(count($detail_list)>0)
			{
			?>
			<tr>
	        	<td colspan="100%">
					
					<table class="table table-bordered responsive">
       		 			<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="200"><center>Note</center></th>
							    
							    
							</tr>
						</thead>
						<tbody>
						
						<?php
						$i=1;
						foreach($detail_list as $val)
						{
							?>
							<tr>
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaLengkap"]; ?></td>
								<td align="right"><?php echo $mylib->format_number($val["quantity"],0); ?></td>
								<td align="center"><?php echo $val["NamaSatuan"]; ?></td>
								<td align="center"><?php echo $val["note"]; ?></td>
								
				                
							</tr>
							<?php
							$i++;
						}
						
						?>
							
						</tbody>
					</table>
	        	
	        	</td>
	        </tr>
	        
	        <?php
	        }
	        ?>
			
			
		        <tr>
		        	<td>&nbsp;</td>
		            <td colspan="100%">
						<button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Keluar" onclick=parent.location="<?php echo base_url()."index.php/transaksi/sales_return/"; ?>">Keluar<i class="entypo-cancel"></i></button>
						<!--<font style="color: red; font-style: italic; font-weight: bold;">Button Simpan tidak ada Karena Status sudah terkirim.</font>-->
						<font style="color: red; font-style: italic; font-weight: bold;">View Only.</font>
					</td>
		        </tr>
				
	        
	    </table>
	    </form> 
	    
	    <?php
        if($header->dono)
        {
        ?>
   			<ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
			</ol>
			
	         <table class="table table-bordered responsive">
	            <tr>
	            	<td class="title_table" width="150">Author</td>
		            <td><?php echo $header->adduser." :: ".$header->adddate; ?></td>
	            </tr>
	            <tr>
	            	<td class="title_table" width="150">Edited</td>
		            <td><?php echo $header->edituser." :: ".$header->editdate; ?></td>
	            </tr>
	         </table>	
        <?php 
      	}
        ?>
	    
	    <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
         
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>