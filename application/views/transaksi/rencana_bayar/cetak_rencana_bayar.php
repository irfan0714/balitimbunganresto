<!DOCTYPE html>
<html>
<head>
    <title>Cetak Rencana Bayar</title>
</head>
<style>
	.border-table{
		border: 1px solid #191919;
		font-family: serif;
		border-collapse: collapse;
		font-size: 10pt;
	}

	.non-border-table{
		font-family: serif;
		border-collapse: collapse;
		font-size: 9pt;
		vertical-align: top;
	}
</style>
<body>
<table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="non-border-table">
	<tr>
    	<td colspan="4" align="center"><b><u>RENCANA BAYAR</u></b></td>
    </tr>

    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td width="10%">No Dokumen</td>
        <td width="40%"><?=$data['NoTransaksi'];?></td>
        <td width="10%">Tanggal</td>
        <td width="40%"><?=$data['TglDokumen'];?></td>
    </tr>
    <tr>
        <td width="20%">Kas/Bank</td>
        <td><?=$data['NamaKasBank'];?></td>
        <td width="100">Mata Uang</td>
        <td><?=$data['MataUang'];?></td>
    </tr>
    <tr>
    	<td width="20%">Kurs</td>
        <td><?=$data['Kurs']; ?></td>
        <td width="100">No Bukti</td>
        <td><?=$data['NoBukti'];?></td>
    </tr>
    <tr>
        <td width="20%">Supplier</td>
        <td><?=$data['NamaSupplier'];?></td>
        <td width="100">No. Rek Supplier</td>
        <td><?=$data['NoRekeningSupplier'];?></td>
    </tr>
    <tr>
        <td width="20%">Keterangan</td>
        <td colspan="3"><?=stripslashes($data['Keterangan']);?></td>
    </tr>



   	<!-- <tr>
    	<td colspan="100%">
    		<table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
				<thead>
					<tr>
						<th colspan="100%"><b>Outstanding DP</b></th>
					</tr>
					<tr>
						<th width="150"><center>No. Dokumen</center></th>
						<th width="150"><center>Tanggal</center></th>
						<th width="350"><center>Keterangan</center></th>
						<th align="right" width="150">Sisa</th>
					</tr>
				</thead>
				<tbody>
				<?php
				foreach($dataums as $dataum ){
				?>
					<tr>
			        	<td width="150"><center><?=$dataum['NoUM'];?></center></td>
			        	<td width="150"><center><?=$dataum['TglUM'];?></center></td>
			        	<td width="350"><center><?=stripslashes($dataum['KeteranganUM']);?></center></td>
			        	<td width="150" align="right">'<?=number_format($dataum['NilaiUM'], 0, '.', ',');?></td>
			        </tr>
				<?php
				}
				?>
				</tbody>
			</table>
    	</td>
    </tr> -->



     <tr>
    	<td colspan="100%">&nbsp;</td>
    </tr>



    <tr>
		<td colspan="100%">
			<table  width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class="border-table">
				<thead class="title_table">
					<tr>
						<th width="120"><center>No Faktur</center></th>
						<th width="150"><center>No Faktur Supplier</center></th>
						<th width="120"><center>No RG</center></th>
						<th width="120"><center>No PO</center></th>
						<th width="120"><center>Tanggal</center></th>
						<th width="120"><center>Nilai</center></th>
						<th width="120"><center>Bayar</center></th>
				    </tr>
				</thead>
				<tbody>

					<?php
					$subtotal = 0;
					$ppn = 0;
					$totalfaktur = 0;
					for($a=0 ; $a<count($row) ; $a++) {
						$totalfaktur += $row[$a]['NilaiBayar'];
					?>
					<tr id="baris<?php echo $a; ?>">
				        <td><?=$row[$a]['NoFaktur'];?></td>
			            <td><?=$row[$a]['NoFakturSupplier'];?></td>
			            <td><?=$row[$a]['NoRG'];?></td>
			            <td><?=$row[$a]['NoPO'];?></td>
			            <td><?=$row[$a]['Tanggal'];?></td>
			            <td align="right"><?=number_format($row[$a]['Sisa'], 0, ',', '.');?></td>
			            <td align="right"><?=number_format($row[$a]['NilaiBayar'], 0, ',', '.');?></td>
				    </tr>
				    <?php
				    }
				    ?>
				    <tr>
				    	<td colspan="5">
				    		&nbsp;
				    	</td>
				    	<td align="right">
				    		Total Faktur
				    	</td>
				    	<td align="right"><?=number_format($totalfaktur, 0, ',', '.');?></td>
				    </tr>
				    <tr>
				    	<td colspan="5">
				    		&nbsp;
				    	</td>
				    	<td align="right">
				    		Biaya Admin
				    	</td>
				    	<td align="right"><?=number_format($data['BiayaAdmin'], 0, ',', '.');?></td>
				    </tr>
				    <tr>
				    	<td colspan="5">
				    		&nbsp;
				    	</td>
				    	<td align="right">
				    		<?=$data['NamaRekening'];?>
				    	</td>
				    	<td align="right"><?=number_format($data['NilaiPPH'], 0, ',', '.');?></td>
				    </tr>
				    <tr>
				    	<td colspan="5">
				    		&nbsp;
				    	</td>
				    	<td align="right">
				    		Total Rencana Bayar
				    	</td>
				    	<td align="right"><?=number_format($totalfaktur+$data['BiayaAdmin']+$data['Pembulatan']-$data['NilaiPPH'], 0, ',', '.');?></td>
				    </tr>
					<tr>
				    	<td colspan="5">
				    		&nbsp;
				    	</td>
				    	<td align="right">
				    		Pembulatan
				    	</td align="right">
				    	<td align="right"><?=number_format($data['Pembulatan'], 0, ',', '.');?></td>
				    </tr>
				</tbody>
			</table>
		</td>
	</tr>




    <tr><td colspan="100%">&nbsp;</td></tr>
	<tr><td colspan="100%">&nbsp;</td></tr>
	<tr>
		<td>Dibuat oleh</td>
		<td>Diperiksa oleh</td>
		<td>Disetujui oleh</td>
		<td>&nbsp;</td>
	</tr>
	<tr><td colspan="100%">&nbsp;</td></tr>
	<tr><td colspan="100%">&nbsp;</td></tr>
	<tr><td colspan="100%">&nbsp;</td></tr>
	<tr><td colspan="100%">&nbsp;</td></tr>
	<tr><td colspan="100%">&nbsp;</td></tr>
	<tr>
		<td>-----------------</td>
		<td>-----------------</td>
		<td>-----------------</td>
		<td>&nbsp;</td>
	</tr>

    </table>

</body>
</html>
