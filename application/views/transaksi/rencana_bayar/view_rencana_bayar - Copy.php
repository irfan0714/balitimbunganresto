<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Rencana Bayar";

//$arr_data(unset);

?>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>View <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/rencana_bayar/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td>
	            	<input type="text" value="<?=$data['NoTransaksi']; ?>" class="form-control-new" name="notransaksi" id="notransaksi" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?=$data['TglDokumen']; ?>" name="tgldokumen" id="tgldokumen" readonly size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['NamaKasBank']; ?>" class="form-control-new" name="namabank" id="namabank" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            <td class="title_table" width="150">Mata Uang <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['MataUang']; ?>" class="form-control-new" name="matauang" id="matauang" size="15" maxlength="15" readonly style="text-align: left;">
	            </td>
	            
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Kurs <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['Kurs']; ?>" class="form-control-new" name="kurs" id="kurs" size="15" maxlength="15" readonly style="text-align: right;">
	            </td>
	            <td class="title_table" width="150">No Bukti <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NoBukti']; ?>" class="form-control-new" name="NoBukti" id="NoBukti" readonly  size="30" maxlength="25">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">Supplier <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text"  value="<?=$data['NamaSupplier']; ?>" class="form-control-new" name="namasupplier" id="namasupplier" readonly size="30" maxlength="25">
	            </td>
	            <td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="<?=$data['Keterangan']; ?>" class="form-control-new" name="keterangan" id="keterangan" size="75" maxlength="100" readonly style="text-align: left;">
	            </td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<table class="table table-bordered responsive" id="TabelUM">
						<thead class="title_table">
							<tr>
								<th width="100"><center>No. Dokumen</center></th>
								<th width="100"><center>Tanggal</center></th> 
								<th><center>Keterangan</center></th>              
								<th width="100"><center>Sisa</center></th>
	    					</tr>
						</thead>
						<tbody>
						<?php
						foreach($dataums as $dataum ){
						?>
							<tr>
					        	<td>
					        		<input type="text" name="NoUM[]" class="form-control-new"  value='<?=$dataum['NoUM'];?>' readonly>
					        	</td>
					        	<td>
					        		<input type="text" name="TglUM[]" class="form-control-new"  value='<?=$dataum['TglUM'];?>' readonly>
					        	</td>
					        	<td>
					        		<input type="text" name="KeteranganUM[]" style="width: 100%;" class="form-control-new"  value='<?=$dataum['KeteranganUM'];?>' readonly>
					        	</td>
					        	<td>
					        		<input type="text" name="NilaiUM[]" style="text-align: right;" class="form-control-new"  value='<?=number_format($dataum['NilaiUM'], 0, '.', ',');?>' readonly>
					        	</td>
					        </tr>
						<?php	
						}
						?>
						</tbody>
					</table>
	        	</td>
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<table class="table table-bordered responsive" id="TabelDetail">
						<thead class="title_table">
							<tr>
								<th width="50"><center>No Faktur</center></th>
								<th><center>No Faktur Supplier</center></th>               
								<th width="100"><center>No RG</center></th>
								<th width="100"><center>No PO</center></th>
								<th width="100"><center>Tanggal</center></th>
								<th width="100"><center>Nilai</center></th>
								<th width="100"><center>Bayar</center></th>
						    </tr>
						</thead>
						<tbody>
							
							<?php 
							$subtotal = 0;
							$ppn = 0;
							$totalfaktur = 0;
							for($a=0 ; $a<count($row) ; $a++) {
								$totalfaktur += $row[$a]['NilaiBayar'];
							?>
							<tr id="baris<?php echo $a; ?>">
						        <td>
					            	<input type="text" class="form-control-new" name="NoFaktur[]" id="NoFaktur<?php echo $a;?>" value='<?=$row[$a]['NoFaktur'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="NoFakturSupplier[]" id="NoFakturSupplier<?php echo $a;?>" value='<?=$row[$a]['NoFakturSupplier'];?>' readonly style="width: 100%;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="NoRG[]" id="NoRG<?php echo $a;?>" value='<?=$row[$a]['NoRG'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="NoPO[]" id="NoPO<?php echo $a;?>" value='<?=$row[$a]['NoPO'];?>' readonly>
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Tanggal[]" id="Tanggal<?php echo $a;?>" value='<?=$row[$a]['Tanggal'];?>' readonly style="text-align: center;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Sisa[]" id="Sisa<?php echo $a;?>" VALUE="<?=number_format($row[$a]['NilaiFaktur'], 0, ',', '.');?>" readonly style="text-align: right;">
					            </td>
					            <td>
					            	<input type="text" class="form-control-new" name="Bayar[]" id="Bayar<?php echo $a;?>" VALUE="<?=number_format($row[$a]['NilaiBayar'], 0, ',', '.');?>" style="text-align: right;">
					            </td>
						    </tr>
						    <?php
						    }
						    ?>
						    <tr>
						    	<td colspan="5">
						    		&nbsp;
						    	</td>
						    	<td class="title_table" align="right">
						    		Total Faktur
						    	</td>
						    	<td>
						    		<input type="text" class="form-control-new" name="totalfaktur" id="totalfaktur" VALUE="<?=number_format($totalfaktur, 0, ',', '.');?>" readonly style="text-align: right;">
						    	</td>
						    </tr>
						    <tr>
						    	<td colspan="5">
						    		&nbsp;
						    	</td>
						    	<td class="title_table" align="right">
						    		Biaya Admin
						    	</td>
						    	<td>
						    		<input type="text" class="form-control-new" name="biayaadmin" id="biayaadmin" VALUE="<?=number_format($data['BiayaAdmin'], 0, ',', '.');?>" readonly style="text-align: right;">
						    	</td>
						    </tr>
						    <tr>
						    	<td colspan="5">
						    		&nbsp;
						    	</td>
						    	<td class="title_table" align="right">
						    		PPh
						    	</td>
						    	<td>
						    		<input type="text" class="form-control-new" name="pph" id="pph" VALUE="<?=number_format($data['NilaiPPH'], 0, ',', '.');?>" readonly style="text-align: right;">
						    	</td>
						    </tr>
						    <tr>
						    	<td colspan="5">
						    		&nbsp;
						    	</td>
						    	<td class="title_table" align="right">
						    		Total Bayar
						    	</td>
						    	<td>
						    		<input type="text" class="form-control-new" name="totalbayar" id="totalbayar" VALUE="<?=number_format($totalfaktur+$data['BiayaAdmin']+$data['Pembulatan']-$data['NilaiPPH'], 0, ',', '.');?>" readonly style="text-align: right;">
						    	</td>
						    </tr>
							<tr>
						    	<td colspan="5">
						    		&nbsp;
						    	</td>
						    	<td class="title_table" align="right">
						    		Pembulatan
						    	</td>
						    	<td>
						    		<input type="text" class="form-control-new" name="pembulatan" id="pembulatan" VALUE="<?=number_format($data['Pembulatan'], 0, ',', '.');?>" readonly style="text-align: right;">
						    	</td>
						    </tr>
						    
						</tbody>
					</table>
	        	</td>
	        </tr>
	    </table>
	    
	    </form> 
	    <ol class="breadcrumb title_table">
				<li><i class="entypo-vcard"></i>Information data</li>
		</ol>
			
        <table class="table table-bordered responsive">
            <tr>
            	<td class="title_table" width="150">Author</td>
	            <td><?php echo $data['AddUser']." :: ".$data['AddDate']; ?></td>
            </tr>
            <tr>
            	<td class="title_table" width="150">Edited</td>
	            <td><?php echo $data['EditUser']." :: ".$data['EditDate']; ?></td>
            </tr>
         </table>
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
