<?php 

$this->load->view('header'); 
$this->load->view('js/TextValidation');
$this->load->view('js/SelectValidation');

$modul = "Rencana Bayar";

//$arr_data(unset);

?>

<script>
    function targetBlank (url)
    {
        blankWin = window.open(url,'_blank','menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes');
    }
    
    function simpan(){
    	if(parseFloat($('#vtotalbayar').val()) > 0){
    		nilaipph = parseFloat($('#vpph').val());
    		kdrekeningpph = $('#vkdrekeningpph').val();
    		if(nilaipph>0 && kdrekeningpph==''){
				valid=false;
				alert("Rekening PPH belum dipilih.");
			}else{
	    		url = $("#base_url").val();
				tanggal = $("#tgldokumen").val();
				$.ajax({
					url: url+"index.php/transaksi/all_cek/cek_tutup_bulan/",
					data: {tgl:tanggal,jenis:'Kas'},
					type: "POST",
					dataType: 'json',					
					success: function(data)
					{
						if(data=='0'){
							alert("Gagal!!. Tanggal Dokumen sudah tutup bulan.");
							document.getElementById('tgldokumen').focus();
							valid = false;
						}else{
							valid = true;
						}	
					},
					error: function(e) 
					{
						//aler
					},
					async:false 
			 	});
			}
    		if(valid==true){
				$("#theform").submit();		
			}
		}else{
			alert("Total Bayar masih Nol.");
		}
	}
    
    function getDetail(){
    	
    	var KdSupplier = $('#KdSupplier').val();
    	var MataUang = $('#kd_uang').val();
    	var type = $('#v_type').val();
    	var tgl  = $('#tgldokumen').val();
    	
    	var vdata = new Array(4);
    	vdata[0] = KdSupplier;
    	vdata[1] = MataUang;
    	vdata[2] = type;
    	vdata[3] = tgl;
    	
    	$.ajax({
	         url: "<?=base_url();?>index.php/transaksi/rencana_bayar/ajax_rekeningsupplier/",
	         type: "POST",
	         async: true,
	         data: {KdSupplier: KdSupplier},
	         success: function(res)
				{
					$('#NoRekSupplier').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
      	});
      	
    	var jsonString = JSON.stringify(vdata);
    	$.ajax({
	        type: "POST",
	        url: "<?=base_url();?>index.php/transaksi/rencana_bayar/getDetail/",
	        data: {data: vdata},
	        success: function(data) {
				$('#UpdateDetail').html(data);
			}
	    });
	}
	
	function formatdec(nilai){
		var MataUang = $('#kd_uang').val();
		if(MataUang=='IDR'){
			result = toFormat(nilai);
		}else{
			result = toFormat4(nilai);	
		}
		
		return result;
	}
	
	function HitungBayar(id){
		bayar = $("#Bayar"+id).val();
		var xbayar = formatdec('Bayar'+id);
		$("#vBayar"+id).val(bayar);
		
		var lastRow = document.getElementsByName("NoFaktur[]").length;
    	var total = 0;
    	for (index = 0; index < lastRow; index++)
    	{
			nama = document.getElementsByName("vBayar[]");
        	temp = nama[index].id;
        	temp1 = parseFloat(nama[index].value);
        	
        	if(temp1!='' && !isNaN(temp1)){
				total += temp1;	
			}
		}
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (total + biayaadmin - pph);
		
		$("#vtotalfaktur").val(total);
		$("#totalfaktur").val(total);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('totalfaktur');
		formatdec('pembulatan');
	}
	
	function BiayaAdminBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#biayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vbiayaadmin").val(biayaadmin);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('biayaadmin');
	}
	
	function PPHBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#vtotalbayar").val());
		pph = parseFloat($("#pph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		$("#vpph").val(pph);
		formatdec('pembulatan');
		formatdec('pph');
	}
	
	function TotalBayarBlur(){
		totalfaktur = parseFloat($("#vtotalfaktur").val());
		biayaadmin = parseFloat($("#vbiayaadmin").val());
		totalbayar = parseFloat($("#totalbayar").val());
		pph = parseFloat($("#vpph").val());
		
		pembulatan = totalbayar - (totalfaktur + biayaadmin - pph);
		$("#vtotalbayar").val(totalbayar);
		$("#vpembulatan").val(pembulatan);
		$("#pembulatan").val(pembulatan);
		formatdec('pembulatan');
		formatdec('totalbayar');
	}
	
	function CopySisa(id){
		sisa =  parseFloat($("#vSisa" + id).val());
		$("#vBayar"+id).val(sisa);
		$("#Bayar"+id).val(sisa);
		HitungBayar(id);
	}
	
	function CekBayar(obj) {
		objek = obj.id;
        id = parseFloat(objek.substr(5, objek.length - 5));

        bayar = parseFloat($("#Bayar" + id).val());
        sisa = parseFloat($("#vSisa" + id).val());
        
        if(bayar>sisa){
			alert("Pembayaran lebih besar dari nilai faktur.");
			$("#Bayar" + id).val(0);
		}
    }
    
    function cari_supplier(url){
		var act=$("#v_supplier_search").val();
		$.ajax({
				url: url+"index.php/transaksi/rencana_bayar/ajax_supplier/",
				data: {id:act},
				type: "POST",
				dataType: 'html',					
				success: function(res)
				{
					
					$('#KdSupplier').html(res);
				},
				error: function(e) 
				{
					alert(e);
				} 
		}); 
	}
</script>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/transaksi/rencana_bayar/save_data'>
		
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table" width="150">No Dokumen</td>
	            <td><b>AutoGenerate</b></td>
	            <td class="title_table" width="150">Tanggal<font color="red"><b>(*)</b></font></td>
	            <td colspan="3"> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="tgldokumen" id="tgldokumen" size="10" maxlength="10">
	            </td>
	        </tr>
	        <tr>
	            <td class="title_table" width="150">Kas/Bank <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<?=form_dropdownDB_initJS('KdKasBank', $kasbank, 'KdKasBank', 'NamaKasBank', '', '', '--------- Pilih ---------', 'id="KdKasBank" class="widthonehundred");"');?>
	            </td>
	            <td class="title_table" width="150">Mata Uang <font color="red"><b>(*)</b></font></td>
	            <td colspan="3">  
	            	<?=form_dropdownDB_initJS('kd_uang', $matauang, 'kd_uang', 'keterangan', '', 'IDR', 'Rupiah Indonesia(IDR)', 'id="kd_uang" onchange="getDetail()" class="widthonehundred");"');?>
	            </td>
	            
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Kurs <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" value="1" class="form-control-new" name="kurs" id="kurs" size="15" maxlength="15"  style="text-align: right;">
	            </td>
	            <td class="title_table" width="150">No Bukti <font color="red"><b>(*)</b></font></td>
	            <td colspan="3"> 
	            	<input type="text" class="form-control-new" name="NoBukti" id="NoBukti" size="30" maxlength="25">
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="title_table" width="150">&nbsp; <font color="red"><b>(*)</b></font></td>
	            <td> 
		            	<!--<select class="form-control-new" name="v_type" id="v_type" style="width: 200px;">
		            		<option value="1">Hutang Dagang</option>
		            		<option value="2">Hutang Lain -  lain</option>
		            	</select>-->&nbsp;
		            
	            </td>
	            <td class="title_table" width="150">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td colspan="3"> 
	            	<input type="text" class="form-control-new" name="keterangan" id="keterangan" size="75" maxlength="100"  style="text-align: left;">
	            </td>
	        </tr>
	        <tr>
	        	<td class="title_table" width="150">Supplier <font color="red"><b>(*)</b></font></td>
	        	<td>
	        		<input type="text" class="form-control-new" style="width: 15%;" maxlength="30" name="v_supplier_search" id="v_supplier_search" placeholder="Cari Supplier" onkeyup="cari_supplier('<?php echo base_url(); ?>')">
          			<select class="form-control-new" name="KdSupplier" id="KdSupplier" onchange="getDetail()">
	            		<option value="">- Pilih Supplier -</option>
	            		<?php
	            		foreach($supplier as $val)
	            		{
							?><option value="<?php echo $val["KdSupplier"]; ?>"><?php echo $val["Nama"]; ?></option><?php
						}
	            		?>
	            	</select>
				</td>
				<td class="title_table" width="150">No. Rek Supplier <font color="red"><b>(*)</b></font></td>
				<td>
          			<select class="form-control-new" name="NoRekSupplier" id="NoRekSupplier">
	            		<option value="">- Pilih Rek Supplier -</option>
	            	</select>
				</td>
				
	        </tr>
	        <tr>
	        	<td colspan="100%">
	        		<span id='UpdateDetail'></span>
	        	</td>
	        </tr>
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/transaksi/rencana_bayar/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" name="btn_save" id="btn_save" onclick="simpan();" value="Simpan">Simpan<i class="entypo-check"></i></button>
		         </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
	new Spry.Widget.ValidationSelect("KdSupplier");
	new Spry.Widget.ValidationTextField("tgldokumen", "date", {format:"dd-mm-yyyy", useCharacterMasking:true});
	new Spry.Widget.ValidationTextField("kurs", "real", {useCharacterMasking:true});
</script>
