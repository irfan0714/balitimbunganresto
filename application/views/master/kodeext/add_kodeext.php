<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'kodeext',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/kodeext.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/css/default.css" />
<script language="javascript" src="<?=base_url();?>asset/js/zebra_datepicker.js"></script>
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('kodeext');loading()">
<form method='post' name="kodeext" id="kodeext" action='<?=base_url();?>index.php/master/kodeext/save_new_kodeext' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Kode External</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="10" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Nama Group","namagrp","","35","30","","text",$gantikursor,"1");
			?>
			<tr>
				<td nowrap>Status</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="A" checked name="sumber" id="sumberA" onclick="ubahstatus();"/>Aktif
					<input type="radio" value="T" name="sumber" id="sumberT" onclick="ubahstatus();"/>Tidak Aktif
				</td>
			</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="detailNew()"></td>
					<td>Kode</td>
					<td>Nama Barang</td>
					<td>Kode Ext</td>
					<td>Nama Ext</td>
				</tr>
				<?=$mylib->write_detailKodeext(1,"","","","")?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="hiddensumber" name="hiddensumber" value='A'>
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='hidden' id="bulan" name="bulan" value="<?=$bulan?>">
				<input type='hidden' id="tahun" name="tahun" value="<?=$tahun?>">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/kodeext/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>