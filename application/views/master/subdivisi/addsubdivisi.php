<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'subdivisi',this)\"";
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<body onload="firstLoad('subdivisi')">
    <form method='post' name="subdivisi" id="subdivisi" action='<?= base_url(); ?>index.php/master/subdivisi/save_new_subdivisi' class="form-horizontal">
        <table align = 'center' border="0" >
            <tr>
                <td>
                    <fieldset>
                        <legend><b>Add Sub Divisi</b></legend>
                        <?php
                        if ($msg) {
                            echo $msg;
                        }
                        ?>	
                        <div class="control-group">
                            <label class="control-label" for="kodesub"> Kode</label>
                            <div class="controls">
                                <input name="kode" class="span3" type="text" id="kode" value="<?= stripslashes($id); ?>" <?= $gantikursor; ?> onKeyUp="javascript:dodacheck(document.getElementById('kode'));"/>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="namasub"> Nama</label>
                            <div class="controls">
                                <input name="nama" class="span3" type="text" id="nama" value="<?= stripslashes($nama); ?>" <?= $gantikursor; ?> />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="divisi"> Divisi</label>
                            <div class="controls">
                                <select size="1" id="master" name="master" <?= $gantikursor; ?>>
                                    <?php
                                    for ($a = 0; $a < count($master); $a++) {
                                        $select = "";
                                        if ($master1 == $master[$a]['KdDivisi']) {
                                            $select = "selected";
                                        }
                                        ?>
                                        <option <?= $select; ?> value= "<?= $master[$a]['KdDivisi'] ?>"><?= stripslashes($master[$a]['NamaDivisi']) ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="control-group">
                            <div class="controls">
                                <input type="button" class="btn btn-default" name="button" id="button" value="Save" onclick="cekMaster2('kode', 'nama', 'subdivisi', 'Kode Sub Divisi', 'Nama Sub Divisi');" />
                                <input type="button" class="btn" value="Back" onclick=parent.location="<?= base_url(); ?>index.php/master/subdivisi/" />
                            </div>
                        </div>

                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
    <?php $this->load->view('footer'); ?>