<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'tipepersonal',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('tipepersonal')">
<form method='post' name="tipepersonal" id="tipepersonal" action='<?=base_url();?>index.php/master/tipe_personal/save_tipe_personal'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Tipe Personal</b></legend>
			<table align = 'center'>
				<tr>
					<td nowrap>Kode</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="2" size="5" readonly name='kode' id='kode' value="<?=stripslashes($tipe_personal_data->KdTipePersonal);?>" /></td>
				</tr>
				<tr>
					<td nowrap>Nama</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($tipe_personal_data->NamaTipePersonal);?>" <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap colspan="3">
					<?php if($edit){ ?>
						<input type='button' value='Save' onclick="cekMaster2('kode','nama','tipepersonal','Kode Tipe Personal','Nama Tipe Personal');"/>
					<?php } ?>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/tipe_personal/" />
					</td>
				</tr>
			</table>
		</fieldset>
		</td>
	</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>