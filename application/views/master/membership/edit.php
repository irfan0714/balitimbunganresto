<?php
    $this->load->view('header');
    $gantikursor = "onkeydown=\"changeCursor(event,'typesupp',this)\"";
    
    if($RecidenceMail=="1"){$RecidenceMail =  "checked";}else{$RecidenceMail =  "";}
    if($OfficeMail=="1"){$OfficeMail =  "checked";}else{$OfficeMail =  "";}
    
    $q = "
            SELECT
                KdTravel,
                Nama
            FROM
                tourtravel
            WHERE
                1
                AND Nama != ''
            GROUP BY 
                Nama
            ORDER BY
                Nama ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdTravel, $NamaTravel) = $row;
        
        $arr_data["list_travel"][$KdTravel] = $KdTravel;
        $arr_data["travel_Nama"][$KdTravel] = $NamaTravel;
    }
    
    $q = "
            SELECT
                *
            FROM
                `type_member`
            WHERE
                1 
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdTypeMember, $NamaTypeMember) = $row;
        
        $arr_data["list_type_leader"][$KdTypeMember] = $KdTypeMember;
        $arr_data["type_leader_KeteranganTourLeader"][$KdTypeMember] = $NamaTypeMember;
    }
    
?>

<script>
    function start_page()
    {
        document.getElementById("Nama").focus();
    }
    
    function cek_form()
    {
        if(document.getElementById("Nama").value=="")
        {
            alert("Nama harus diisi...");
            document.getElementById("Nama").focus();
            return false;
        }
        else if(document.getElementById("v_NoIdentitas").value=="")
        {
            alert("No Identitas harus diisi...");
            document.getElementById("v_NoIdentitas").focus();
            return false;
        }  else if (document.getElementById("travelleader").value=="") {
			alert("Biro Member harus dipilih...");
            document.getElementById("travelleader").focus();
            return false;
		}
        
        
    }
</script>
    <body onload="start_page()">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$label?>
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="matauang" id="matauang" action='<?= base_url(); ?>index.php/<?=$tr1."/".$tr2."/".$tr3;?>' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">
                                                    
        <div class="form-group">
            <label class="col-sm-2 control-label">Kode</label>
            <div class="col-sm-5">
                <input name="Kode" id="Kode" size="10" value="<?php echo $Kode; ?>" type="hidden">
                <?php
                    echo "<b>".$Kode."</b>";
                ?>
            </div>
        </div>
    
    
    <div class="form-group">
            <label class="col-sm-2 control-label"> Type Member</label>
            <div class="col-sm-5">
                <select class="form-control" id="type_member" name="type_member" value="">
                    <option value="">--Please Select--</option>
                    <?php 
                            foreach($arr_data["list_type_leader"] as $KdTypeTourLeader=>$val)                
                            {
                                $Nama = $arr_data["type_leader_KeteranganTourLeader"][$KdTypeTourLeader];
                                $selected = "";
                                if($typenya==$KdTypeTourLeader)
                                {
                                    $selected = "selected='selected'";
                                }
                                ?>
                                    <option <?php echo $selected;?>  value="<?php echo $KdTypeTourLeader; ?>"><?php echo $Nama; ?></option>            
                                <?php
                            }
                        ?>
                </select>

            </div>
        </div>
        
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama (*)</label>
                <div class="col-sm-5">
                    <input name="Nama" id="Nama" size="30" value="<?php echo $NamaMember; ?>" maxlength="30" onkeydown="changeCursor(event,'typesupp',this)" class="form-control" type="text">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">No Identitas (*)</label>
                <div class="col-sm-5">
                    <select class="form-control" name="v_JenisIdentitas" id="v_JenisIdentitas" style="width: 15%;">
                        <option <?php if($JenisIdentitas=="KTP") echo "selected='selected'"; ?> value="KTP">KTP</option>
                        <option <?php if($JenisIdentitas=="SIM") echo "selected='selected'"; ?> value="SIM">SIM</option>
                        <option <?php if($JenisIdentitas=="STNK") echo "selected='selected'"; ?> value="STNK">STNK</option>
                        <option <?php if($JenisIdentitas=="Pasport") echo "selected='selected'"; ?> value="Pasport">Pasport</option>
                    </select>
                   
                    <input name="v_NoIdentitas" id="v_NoIdentitas" size="30" value="<?php echo $NoIdentitas; ?>" maxlength="30" onkeydown="changeCursor(event,'typesupp',this)" class="form-control" style="width: 70%;" type="text">
                </div>
            </div>
                        
            
            <div class="form-group">
            <label class="col-sm-2 control-label">Data Of Birth</label>
            <div class="col-sm-5">
                <input name="hbd" id="hbd" size="20" value="<?php echo $hbd; ?>" maxlength="20" class="form-control datepicker" type="text">
            </div>
        </div>    
    
    	<div class="form-group">
                <label class="col-sm-2 control-label">Father's / Husband's Name</label>
                <div class="col-sm-5">
                    <input name="Nama_other" id="Nama_other" size="30" value="<?php echo $Husband_Father;?>" maxlength="30" class="form-control" type="text">
                </div>
            </div>
        
        <div class="form-group">
                <label class="col-sm-2 control-label">Nationality</label>
                <div class="col-sm-5">
                    <input name="nationality" id="nationality" size="30" value="<?php echo $Nationality;?>" maxlength="30" class="form-control" type="text">
                </div>
            </div>
            
            <div class="form-group">
            <label class="col-sm-2 control-label">Recidence Address</label>
            <div class="col-sm-5">
                <input name="AlamatRumah" id="AlamatRumah" size="40" value="<?php echo $Alamat1;?>" maxlength="30"  class="form-control" type="text">
            </div>
            <div class="col-sm-5">
                <input name="TlpRumah" id="TlpRumah" size="20" value="<?php echo $Telp1;?>" maxlength="30"  class="form-control" placeholder="Recidence Phone" type="text">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Office Address</label>
            <div class="col-sm-5">
                <input name="AlamatKantor" id="AlamatKantor" size="40" value="<?php echo $Alamat2;?>" maxlength="30" class="form-control" type="text">
            </div>
            <div class="col-sm-5">
                <input name="TlpKantor" id="TlpKantor" size="20" value="<?php echo $Telp2;?>" maxlength="30"  class="form-control" placeholder="Office Phone" type="text">
            </div>
        </div>
            
        <div class="form-group">
            <label class="col-sm-2 control-label">Mobile No.</label>
            <div class="col-sm-5">
                <input name="MobilePhone1" id="MobilePhone1" size="20" value="<?php echo $NoHP1;?>" maxlength="20" class="form-control" placeholder="Mobile Phone 1" type="text">
            </div>
            <div class="col-sm-5">
                <input name="MobilePhone2" id="MobilePhone2" size="20" value="<?php echo $NoHP2;?>" maxlength="20" class="form-control" placeholder="Mobile Phone 2" type="text">
            </div>
        </div>
            <div class="form-group" style="display: none;">
            <label class="col-sm-2 control-label">Fax</label>
            <div class="col-sm-5">
                <input name="Fax" id="Fax" size="20" value="" maxlength="20" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
        
            <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-5">
                <input name="Email" id="Email" size="25" value="<?php echo $Email;?>" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
        
        
        <div class="form-group">
            <label class="col-sm-2 control-label">All Mail To Be Sent On </label>
            <div class="col-sm-5">
                <p><input <?php echo $RecidenceMail;?> type='checkbox' id="RecidenceMail" name='RecidenceMail' value='Recidence' />&nbsp;&nbsp;Residence Address</p>
                <p><input <?php echo $OfficeMail;?> type='checkbox' id="OfficeMail" name='OfficeMail' value='Office' />&nbsp;&nbsp;Office Address</p>
            </div>
        </div>
            
        <div class="form-group">
            <label class="col-sm-12-"><b>&nbsp;&nbsp;This Membership Is Being Proposes By The Following Of PT. Natura Pesona Mandiri - Secret Garden Village</b></label>
        </div>
        
            
        <div class="form-group">
            <label class="col-sm-2 control-label">Name 1</label>
            <div class="col-sm-5">
                <input name="NamePurposed1" id="NamePurposed1" size="25" value="<?php echo $NamePurposed1;?>" maxlength="25" class="form-control" type="text">
            </div>
            <div class="col-sm-5">    
                <input name="PhonePurposed1" id="PhonePurposed1" size="25" value="<?php echo $PhonePurposed1;?>" maxlength="25" class="form-control" placeholder="Phone" type="text">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Name 2</label>
            <div class="col-sm-5">
                <input name="NamePurposed2" id="NamePurposed2" size="25" value="<?php echo $NamePurposed2;?>" maxlength="25" class="form-control" type="text">
            </div>
            <div class="col-sm-5">    
                <input name="PhonePurposed2" id="PhonePurposed2" size="25" value="<?php echo $PhonePurposed2;?>" maxlength="25" class="form-control" placeholder="Phone" type="text">
            </div>
        </div>    
            
        
        <div class="form-group">
            <label class="col-sm-2 control-label"> Aktif</label>
            <div class="col-sm-2">
                <select class="form-control" size="1" id="status" name="status" onkeydown="changeCursor(event,'typesupp',this)" onchange="simpanKontak();">
                        <option <?php if($statusnya=="A") echo "selected='selected'"; ?> value="A">Aktif</option>
                        <option <?php if($statusnya=="T") echo "selected='selected'"; ?> value="T">Tidak</option>
                </select>

            </div>
        </div>
                    
                    
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/<?=$tr1."/".$tr2;?>/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                    
                    
                    
                </form>
            </div>
        </div>
    </div>
    </body>

<?php $this->load->view('footer'); ?>