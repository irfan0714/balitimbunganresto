<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'attr',this)\"";
$pk = "";
if($viewattr->TipeAttr=="A")
{
	$tipe = "Angka";
}
else if($viewattr->TipeAttr=="T")
{
	$tipe = "Tanggal";
}
else if($viewattr->TipeAttr=="H")
{
	$tipe = "Text";
}
else if($viewattr->TipeAttr=="D")
{
	$tipe = "Pilihan";
}
if($viewattr->Mandatory=="Y")
{
	$pk = "checked='checked'";
}
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript">
function cekAttr()
{
	if(cekoption('nama','Memasukkan Nama Attribute'))
	if(cekoption('panjang','Memasukkan Panjang Attribute'))
		document.getElementById('attr').submit();
}
</script>
<body onload="firstLoad('attr')">
<form method='post' name="attr" id="attr" action='<?=base_url();?>index.php/master/attribute/save_attr'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Attribute</b></legend>
			<table align = 'center'>
				<tr>
					<td nowrap>Kode</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="2" size="5" readonly name='kode' id='kode' value="<?=$viewattr->KdAttribute;?>" /></td>
				</tr>
				<tr>
					<td nowrap>Nama</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($viewattr->NamaAttribute);?>" <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Tipe Attribute</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' size="8" id='tipe2' name='tipe2' value='<?=$tipe;?>' readonly='readonly'/>
					<input type='hidden' id='tipe' name='tipe' value='<?=$viewattr->TipeAttr;?>'/></td>
				</tr>
				<tr>
					<td nowrap>Panjang Attribute</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="2" size="3" id='panjang' name='panjang'value='<?=$viewattr->PanjangAttr;?>' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Harus Diisi</td>
					<td nowrap>:</td>
					<td nowrap><input type='checkbox' id='pk' name='pk' <?=$pk?> value="Y" <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap colspan="3">
					<?php if($edit){ ?>
						<input type='button' value='Save' onclick="cekAttr();"/>
					<?php } ?>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/attribute/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>