<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'attr',this)\"";
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript">
function cekAttr()
{
	if (document.attr.jenis[0].checked){
		if(cekoption('kode','Memasukkan Kode Attribute'))
		if(cekoption('nama','Memasukkan Nama Attribute'))
		if(cekAngka('panjang','Panjang Attribute'))
			document.getElementById('attr').submit();
	}
	else
	{
		if(cekoption('kode','Memasukkan Kode Attribute'))
			document.getElementById('attr').submit();
	}
}
function gantiPilihan(isi)
{
	if(isi=="I")
	{
		document.getElementById("barisnama").style.display = "";
		document.getElementById("baristipe").style.display = "";
		document.getElementById("barispjg").style.display = "";
		document.getElementById("barisdrop").style.display = "none";
	}
	else
	{
		document.getElementById("barisnama").style.display = "none";
		document.getElementById("baristipe").style.display = "none";
		document.getElementById("barispjg").style.display = "none";
		document.getElementById("barisdrop").style.display = "";
	}
}
</script>
<body onload="firstLoad('attr')">
<form method='post' name="attr" id="attr" action='<?=base_url();?>index.php/master/attribute/save_new_attr'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b>Add Attribute</b></legend>
			<table align = 'center'>
			<?php if($msg){ echo $msg;} ?>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type='radio' id='jenis' name='jenis' value="I" checked <?=$gantikursor;?> onclick="gantiPilihan('I');"/>Inputan
					<input type='radio' id='jenis' name='jenis' value="P" <?=$gantikursor;?> onclick="gantiPilihan('P');" />Pilihan
				</td>
			</tr>
			<tr>
				<td nowrap>Kode</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="4" size="5" id='kode' name='kode' <?=$gantikursor;?> onKeyUp="javascript:dodacheck(document.getElementById('kode'));"/></td>
			</tr>
			<tr id="barisnama" style="display:">
				<td nowrap>Nama</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' <?=$gantikursor;?>/></td>
			</tr>
			<tr id="baristipe" style="display:">
				<td nowrap>Tipe Attribute</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" name="tipe" id="tipe" <?=$gantikursor;?>>
				<option value="H">Text</option>
				<option value="A">Angka</option>
				<option value="T">Tanggal</option>
				</select></td>
			</tr>
			<tr id="barispjg" style="display:">
				<td nowrap>Panjang Attribute</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="2" size="3" id='panjang' name='panjang' <?=$gantikursor;?>/></td>
			</tr>
			<tr id="barisdrop" style="display:none">
				<td nowrap>Pilih Dari</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" name="pilihan" id="pilihan" <?=$gantikursor;?>/>
					<option value="Kontak|6">Kontak</option>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Harus Diisi</td>
				<td nowrap>:</td>
				<td nowrap><input type='checkbox' id='pk' name='pk' value="Y" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap colspan="3">
					<input type='button' value='Save' onclick="cekAttr();"/>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/attribute/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>