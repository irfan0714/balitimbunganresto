<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'member',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
	function loadDate(url)
	{
		$('#tgllahir').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: url+ "public/images/calendar.png", buttonImageOnly: true } );
	}
</script>
<body onload="firstLoad('member');loadDate('<?=base_url();?>');">
<form method='post' name="member" id="member" action='<?=base_url();?>index.php/master/member/save_new_member'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b>Add Member</b></legend>
			<table align = 'center'>
				<?php
				if($msg){ echo $msg;}?>	
				<tr>
					<td nowrap>Nomor</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="10" size="12" name='kode' value="<?=stripslashes($id);?>" id='kode' <?=$gantikursor;?> onKeyUp="javascript:dodacheck(document.getElementById('kode'));" /></td>
				</tr>
				<tr>
					<td nowrap>Nama Lengkap</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='nama' value="<?=stripslashes($nama);?>" name='nama' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Nama Panggilan</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='nick' value="<?=stripslashes($nick);?>" name='nick' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Alamat</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="60" size="65" id='alamat1' value="<?=stripslashes($alamat1);?>" name='alamat1' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap></td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="60" size="65" id='alamat2' value="<?=stripslashes($alamat2);?>" name='alamat2' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Kota</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='kota' value="<?=stripslashes($kota);?>" name='kota' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Tempat Lahir</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='kotalahir' value="<?=stripslashes($kotalahir);?>" name='kotalahir' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Tgl Lahir</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="10" size="12" id='tgllahir' value="<?=stripslashes($tgllahir);?>" name='tgllahir' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Telp / HP</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='telp' value="<?=stripslashes($telp);?>" name='telp' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Email</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='email' value="<?=stripslashes($email);?>" name='email' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Twitter</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="30" size="35" id='twitter' value="<?=stripslashes($twitter);?>" name='twitter' <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap>Tipe Member</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="tipe" name="tipe" <?=$gantikursor;?>>
					<?php
					for($a = 0;$a<count($tipe);$a++){
						$select = "";
						if($niltipe==$tipe[$a]['KdTipeMember']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$tipe[$a]['KdTipeMember']?>"><?=stripslashes($tipe[$a]['NamaTipeMember'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				<tr>
					<td nowrap colspan="3">
						<input type='button' value='Save' onclick="cekMaster2('kode','nama','member','Kode Member','Nama Member');"/>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/member/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>