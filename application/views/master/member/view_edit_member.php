<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'member',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
	function loadDate(url)
	{
		$('#tgllahir').datepicker({ dateFormat: 'dd-mm-yy',mandatory: true,showOn: "both", buttonImage: url+ "public/images/calendar.png", buttonImageOnly: true } );
	}
</script>
<body onload="firstLoad('member');loadDate('<?=base_url();?>');">
<form method='post' name="member" id="member" action='<?=base_url();?>index.php/master/member/save_member'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Member</b></legend>
			<table align = 'center'>
			<tr>
				<td nowrap>Kode</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="10" size="12" readonly name='kode' id='kode' value="<?=stripslashes($view_member->KdMember);?>" /></td>
			</tr>
			<tr>
				<td nowrap>Nama Lengkap</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='nama' name='nama' value="<?=stripslashes($view_member->NamaMember);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Nama Panggilan</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='nick' name='nick' value="<?=stripslashes($view_member->NickName);?>" <?=$gantikursor;?>/></td>
				</tr>
			<tr>
				<td nowrap>Alamat</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="60" size="65" id='alamat1' name='alamat1' value="<?=stripslashes($view_member->Alamat1);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap></td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="60" size="65" id='alamat2' name='alamat2' value="<?=stripslashes($view_member->Alamat2);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Kota</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='kota' name='kota' value="<?=stripslashes($view_member->Kota);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Tempat Lahir</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='kotalahir' name='kotalahir' value="<?=stripslashes($view_member->KotaLahir);?>" <?=$gantikursor;?>/></td>
				</tr>
			<tr>
				<td nowrap>Tgl Lahir (dd-mm-yyyy)</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="10" size="12" id='tgllahir' name='tgllahir' value="<?=stripslashes($view_member->TglLahir);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Telp / HP</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='telp' name='telp' value="<?=stripslashes($view_member->Telp);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Email</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='email' name='email' value="<?=stripslashes($view_member->Email);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Twitter</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="30" size="35" id='twitter' name='twitter' value="<?=stripslashes($view_member->Twitter);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Kategori</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" id="tipe" name="tipe" <?=$gantikursor;?>>
				<?php
				for($a = 0;$a<count($tipe);$a++){
					$select = "";
					if($view_member->KdTipeMember==$tipe[$a]['KdTipeMember']){
						$select = "selected";
					}
				?>
				<option <?=$select;?> value= "<?=stripslashes($tipe[$a]['KdTipeMember'])?>"><?=stripslashes($tipe[$a]['NamaTipeMember'])?></option>
				<?php
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input type='button' value='Save' onclick="cekMaster2('kode','nama','member','Kode Member','Nama Member');"/>
				<?php } ?>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/member/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>