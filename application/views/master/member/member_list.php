<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table align='center'>
	<tr>
		<td><input type='text' size'20' name='stSearchingKey' id='stSearchingKey'></td>
		<td>
			<select size="1" height="1" name ="searchby" id ="searchby">
				<option value="KdMember">Kode Member</option>
				<option value="NamaMember">Nama Member</option>
			</select>
		</td>
		<td><input type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<table align = 'center' border='1' class='table_class_list'>
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php } ?>
		<th>Kode Member</th>
		<th>Nama</th>
		<th>Nama Tipe Member</th>
		<th>Join Date</th>
		<th>Last Trans</th>
		<th>Total Trans</th>
	</tr>
<?php
	if(count($member_data)==0)
	{ 
?>
	<td nowrap colspan="7" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($member_data);$a++)
{
?>
	<tr>
<?php
	if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
	{
?>
			<td nowrap>
		<?php
			if($link->view=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/member/view_member/<?=$member_data[$a]['KdMember'];?>"><img src='<?=base_url();?>public/images/zoom.png' border = '0' title = 'View'/></a>
		<?php
			}
			if($link->edit=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/member/edit_member/<?=$member_data[$a]['KdMember'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php
			}
			if($link->delete=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/member/delete_member/<?=$member_data[$a]['KdMember'];?>"><img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete'/></a>
		<?php
			}
		?>
		</td>
		<?php } 
			$mylib = new globallib();
		?>        
		<td nowrap><?=stripslashes($member_data[$a]['KdMember']);?></td>
		<td nowrap><?=stripslashes($member_data[$a]['NamaMember']);?></td>
		<td nowrap><?=stripslashes($member_data[$a]['NamaTipeMember']);?></td>
		<td nowrap><?=$mylib->ubah_tanggal($member_data[$a]['JoinDate']);?></td>
		<td nowrap><?=$mylib->ubah_tanggal($member_data[$a]['LastTrans']);?></td>
		<td nowrap class="InputAlignRight"><?=number_format($member_data[$a]['TotalTrans'], 0, ',', '.');?></td>
	<tr>
<?php
}
?>
</table>
<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/master/member/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>