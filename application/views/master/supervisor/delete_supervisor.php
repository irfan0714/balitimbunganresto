<?php
$this->load->view('header');
if($cekAda==0){?>
<form name = "tabel" method="post" action="<?=base_url();?>index.php/master/supervisor/delete_This">
<table class="table table-bordered responsive">
	<tr>
		<td>
		
		<legend><b>Delete Supervisor</b></legend>
			<table class="table table-bordered responsive">
			<tr>
				<th nowrap colspan="3">Hapus Data Berikut ? </th>
			</tr>
			<tr>
				<td nowrap>Kode</td>
				<td nowrap><?=$view_supervisor->KdSupervisor;?></td>
					<input type='hidden' readonly name='kode' id='kode' value="<?=stripslashes($view_supervisor->KdSupervisor);?>" />
			</tr>
			<tr>
				<td nowrap>Nama</td>
				<td nowrap><?=stripslashes($view_supervisor->NamaSupervisor);?></td>
			</tr>
			<tr>
				<td nowrap colspan="3">
					<input class="btn btn-info btn-sm sm-new tooltip-primary" type='submit' value = "Yes"/>
					<input class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="No" ONCLICK =parent.location="<?=base_url();?>index.php/master/supervisor/" />
				</td>
			</tr>
		</table>
		
		</td>
	</tr>
</table>
</form>
<?php
}
else
{
?>
<table class="table table-bordered responsive">
	<tr>
		<td>
	
		<legend><b>Delete Salesman</b></legend>
		<table class="table table-bordered responsive">
		<tr>
			<th nowrap colspan="3">Data Tidak Dapat Dihapus. Salesman Masih Digunakan</th>
		</tr>
		<tr>
			<td nowrap colspan="3">
				<input class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/salesman/" />
			</td>
		</tr>
		</table>

		</td>
	</tr>
</table>
<?php
}
$this->load->view('footer'); ?>