<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'salesman',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('salesman')">
<form method='post' name="salesman" id="salesman" action='<?=base_url();?>index.php/master/supervisor/save_supervisor'>
<table  class="table table-bordered responsive">
	<tr>
		<td>
		
		<legend><b><?=$fieldset?> Supervisor</b></legend>
			<table  class="table table-bordered responsive">
			<tr>
				<td>Kode</td>
				<td nowrap><?=stripslashes($view_supervisor->KdSupervisor);?></td>
			</tr>
			<input class="form-control-new" type='hidden' maxlength="4" size="5" readonly name='kode' id='kode' value="<?=stripslashes($view_supervisor->KdSupervisor);?>" />
			<tr>
				<td>Nama</td>
				<td nowrap><input class="form-control-new" type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($view_supervisor->NamaSupervisor);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
					<td>Status</td>
					<td>
		            	<select class="form-control-new" name="status" id="status" style="width: 200px;">
		            		<option <?php if($view_supervisor->Status=='A'){ echo "selected='selected'"; } ?> value="A">Aktif</option>
		            		<option <?php if($view_supervisor->Status=='T'){ echo "selected='selected'"; } ?> value="T">Non Aktif</option>
		            	</select>
		            </td>
				</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input  class="btn btn-info btn-sm sm-new tooltip-primary" type='button' value='Save' onclick="cekMaster2('kode','nama','salesman','Kode Salesman','Nama Salesman');"/>
				<?php } ?>
					<input  class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/supervisor/" />
				</td>
			</tr>
			</table>

	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>