<?php $this->load->view('header'); ?>
<head>
</head>
<!--<script language="javascript" src="<?= base_url(); ?>public/js/master_barang.js"></script>-->


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/master/master_barang/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    <div class="row">
        <div class="col-md-9">  
            <b>Pencarian</b>&nbsp;
            <input type="text" size="20" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" />
            &nbsp;<b>Divisi</b>&nbsp;
            <select class="form-control-new" name="search_divisi" id="search_divisi" style="width: 120px;">
            	<option <?php echo $selected ?>  value="">Semua</option>
                <?php
                foreach ($divisi as $val) {
                    $selected = "";
                    if ($search_divisi) {
                        if ($val["KdDivisi"] == $search_divisi) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?>  value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
                }
                ?>
            </select>
            &nbsp;<b>Kategori</b>&nbsp; 
            <select class="form-control-new" name="search_kategori" id="search_kategori" style="width: 120px;">
                <option value="">Semua</option>
                <?php
				
                foreach ($kategori as $val) {
                    $selected = "";
                    if ($search_kategori) {
                        if ($val["KdKategori"] == $search_kategori) {
                            $selected = 'selected="selected"';
                        }
                    }
                    ?><option <?php echo $selected ?> value="<?php echo $val["KdKategori"]; ?>"><?php echo $val["NamaKategori"]; ?></option><?php
                }
                ?>
            </select>  
            &nbsp;
            <b>Urut By</b>&nbsp;
            <select class="form-control-new" name="search_urut" id="search_urut" style="width: 120px;">
				<option <?php if($search_urut=="0"){ echo 'selected="selected"'; } ?> value="0">Date</option>
                <option <?php if($search_urut=="1"){ echo 'selected="selected"'; } ?> value="1">Nama</option>
                <option <?php if($search_urut=="2"){ echo 'selected="selected"'; } ?> value="2">PCode</option>
            </select>  
				
            <b>Status</b>&nbsp;
            <select class="form-control-new" name="search_status" id="search_status" style="width: 120px;">
				<option <?php if($search_status=="0"){ echo 'selected="selected"'; } ?> value="">Semua</option>
                <option <?php if($search_urut=="1"){ echo 'selected="selected"'; } ?> value="1">Pending</option>
                <option <?php if($search_urut=="2"){ echo 'selected="selected"'; } ?> value="2">Waiting Approve</option>
                <option <?php if($search_urut=="2"){ echo 'selected="selected"'; } ?> value="3">Approve</option>
                <option <?php if($search_urut=="2"){ echo 'selected="selected"'; } ?> value="4">Reject</option>
            </select>  
            &nbsp;
        </div>

        <div class="col-md-3" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/master/master_barang/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
            <!--<a href="<?php echo base_url() . "index.php/master/master_barang/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >DownloaD Excel<i class="entypo-download"></i></a>-->
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>
<!--<table class="table table-bordered responsive">
        <thead class="title_table">
                <th width="20">No</th>
                <th width="73">PCode</th>
                <th width="87">Barcode</th>
                <th>Nama Barang</th>
                <th>Satuan</th>
                <th>Divisi</th>
                <th>Kategori</th>    
                <th>Add Date</th>      
                <th>Status</th>   
            
        </thead>
</table>-->
<div id="col-search" class="row" style="overflow:auto;height:430px;">
<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
                <th width="30">No</th>
                <th>PCode</th>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <th>Satuan</th>
                <th>Divisi</th>
                <th>Kategori</th>    
                <th>Add Date</th>      
                <th>Approval</th>
                <th>Status</th>   
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }
                $status='';
                if($val["Status"]=="A"){
                    $status='Aktif';
                }else{
                    $status='NonAktif';
                }
                
                $addDate = date("Y-m-d",strtotime($val["AddDate"]));
                $dateCut = date("Y-m-d",strtotime("2019-12-02"));

                if($addDate < $dateCut){
                    if($val["Approval_Status"]==0 AND $val["status_mail"]==0){
                        $sts = "Pending";
                    }else if($val["Approval_Status"]==0 AND $val["status_mail"]==1){
                        $sts = "Waiting Approve";
                    }else if($val["Approval_Status"]==1 AND ( $val["status_mail"]==1 OR $val["status_mail"]==0)){
                        $sts = "<font color='blue'>Approved By ".$val["Approval_By"]." ".$val["Approval_Date"]."</font>";
                    }else if($val["Approval_Status"]==2 AND ( $val["status_mail"]==1 OR $val["status_mail"]==0)){
                        $sts = "<font color='red'>Rejected By ".$val["Approval_By"]." ".$val["Approval_Date"]."</font>";
                    }else{
                        $sts = "";
                    }
                }else{
                    if($val["Approval_Status"]==0 AND $val["status_mail"]==0){
                        $sts = "Pending";
                    }else if($val["Approval_Status"]==0 AND $val["status_mail"]==1){
                        $sts = "Menunggu Approval ".$val["Approval_By"];
                    }else if($val["Approval_Status"]==1 AND $val["Approval_Status_2"]==0 AND $val["status_mail"]== 1){
                        $sts = "Menunggu Approval ".$val["Approval_By_2"];
                    }else if($val["Approval_Status"]==1 AND $val["Approval_Status_2"]==1 AND ( $val["status_mail"]==1 OR $val["status_mail"]==0)){
                        $sts = "<font color='blue'>Approved By ".$val["Approval_By_2"]." ".$val["Approval_Date_2"]."</font>";
                    }else if($val["Approval_Status"]==2 AND ( $val["status_mail"]==1 OR $val["status_mail"]==0)){
                        $sts = "<font color='red'>Rejected By ".$val["Approval_By"]." ".$val["Approval_Date"]."</font>";
                    }else if($val["Approval_Status_2"]==2 AND ( $val["status_mail"]==1 OR $val["status_mail"]==0)){
                        $sts = "<font color='red'>Rejected By ".$val["Approval_By_2"]." ".$val["Approval_Date_2"]."</font>";
                    }else{
                        $sts = "";
                    }
                }
                
                
				if($val["PCode"]!=""){
                ?>
                <tr data-toggle="tooltip" data-placement="top" data-original-title="Klik untuk detail <?php echo $val["PCode"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="center"><?php echo $val["PCode"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="center"><?php echo $val["Barcode1"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $val["NamaLengkap"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $val["SatuanSt"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $val["NamaDivisi"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $val["NamaKategori"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $val["AddDate"]; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $sts; ?></td>
                    <td onclick="editing('<?php echo $val["PCode"]; ?>')" align="left"><?php echo $status; ?></td>
                    
                </tr>
                <?php
                }
                $no++;
            }
            ?>

        </tbody>
    </table>

    <!--<div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>-->	

</div>
</div>


<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
function editing(pcode){
var url=$('#base_url').val();
window.location = url+"index.php/master/master_barang/edit_data/"+pcode;	
}</script>