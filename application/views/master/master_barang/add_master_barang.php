<?php 

$this->load->view('header'); 

$modul = "Master Barang";

?>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/master/master_barang/save_data'>
		
	    <table class="table table-bordered responsive">                        
        <input type='hidden' name='v_otorisasi' id='v_otorisasi' value='<?php echo $otorisasi; ?>'>    
            <tr>
                <td class="title_table" width="200">PCode</td>
                <td colspan="5"> 
                     <input type="text" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_PCode" id="v_PCode" maxlength="50" value="" onkeyup="CallAjaxForm('cek_pcode', this.value)">
                
	            	   <select class="form-control-new" name="v_Auto" id="v_Auto" style="width: 80px;" onchange="buka()">
		            		<option value="Y">Auto</option>
		            		<option value="N">Manual</option>
		            	</select> 	
	            </td>
                
            </tr>
	        
	        <tr>
	            <td class="title_table" width="200">Nama Barang</td>
	            <td colspan="5"> 
	            	<input type="text" class="form-control-new" style="width: 400px; text-transform: uppercase;" name="v_NamaLengkap" id="v_NamaLengkap" maxlength="50" onkeyup="CallAjaxForm('cek_barang', this.value)">
	            	<button type="button" name="btn_check" id="btn_check" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check()" value="Check">Check<i class="entypo-check"></i></button> 
	            </td>
	        </tr>
	        
	    	<tr>
                <td class="title_table" width="200">Barcode</td>
                <td colspan="5"> 
                     <input type="text" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_Barcode1" id="v_Barcode1" maxlength="50" onkeyup="CallAjaxForm('cek_barcode', this.value)">
                </td>
            </tr>
	        
	        <tr>
                <td class="title_table" width="200">Divisi</td>
                <td width="500">
                    <select name="v_KdDivisi" id="v_KdDivisi" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_divisi', this.value)">  
                        <option value=""> -- Pilih Divisi -- </option>
                        <?php 
                            foreach($divisi as $KdDivisi=>$val)
                            {
                                ?>
                                <option value="<?php echo $val['KdDivisi']; ?>"><?php echo $val['NamaDivisi']; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                    
                    <span id="td_KdSubDivisi"></span>
                    
                    
                    
                </td> 
                
                <td class="title_table" width="150">Stock</td>
	            <td colspan="3"> 
	            	   <select class="form-control-new" name="v_Stock" id="v_Stock" style="width: 70px;">
		            		<option value="Y">Ya</option>
		            		<option value="N">Tidak</option>
		            	</select>
	            </td>
                
            </tr> 
            
            <tr>
                <td class="title_table">Kategori</td>
                <td id="td_Kategori" width="500">
                    <select name="v_KdKategori" id="v_KdKategori" class="form-control-new" style="width: 200px;" onchange="CallAjaxForm('ajax_kategori', this.value)">
                        <option value=""> -- Pilih Kategori -- </option>
                        <?php 
                            foreach($kategori as $KdKategori=>$val)
                            {
                                ?>
                                <option value="<?php echo $val['KdKategori']; ?>"><?php echo $val['NamaKategori']; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                    
                    <span id="td_KdSubKategori"></span>
                </td>                 
                
                <td class="title_table" width="150">Pembelian</td>
	            <td colspan="3"> 
	            	   <select class="form-control-new" name="v_Pembelian" id="v_Pembelian" style="width: 70px;">
		            		<option value="Y">Ya</option>
		            		<option value="N">Tidak</option>
		            	</select>
	            </td>
                
            </tr>
            
            <tr id="tampil_kode_group_barang" style="display: none;">
		            <td class="title_table">Kode Group Barang</td>
		            <td colspan="5"> 
		            	<select name="v_groupbarang" id="v_groupbarang" class="form-control-new" style="width: 200px;">  
                        <option value=""> -- Pilih Group Barang -- </option>
                        <?php 
                            foreach($kdgroupbarang as $val)
                            {
                                ?>
                                <option value="<?php echo $val['KdGroupBarang']; ?>"><?php echo $val['NamaGroupBarang']; ?></option>
                                <?php
                            }
                        ?>
                    </select>
		            </td>
		            
		    </tr>
            
            <tr>
                <td class="title_table">Satuan</td>      
                <td width="500">
                     <select name="v_SatuanSt" id="v_SatuanSt" class="form-control-new" style="width: 200px;">  
                        <option value=""> -- Pilih Satuan -- </option>
                        <?php 
                            foreach($satuan as $val)
                            {
                                ?>
                                <option value="<?php echo $val['KdSatuan']; ?>"><?php echo $val['NamaSatuan']."  (".$val['KdSatuan'].")"; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
                
                <td class="title_table" width="150">Penjualan POS</td>
	            <td colspan="3"> 
	            	   <select class="form-control-new" name="v_penjualan_pos" id="v_penjualan_pos" style="width: 70px;">
		            		<option value="N">Tidak</option>
		            		<option value="Y">Ya</option>
		            	</select>
	            </td>
               
            </tr>
			
			    <tr>
		            <td class="title_table">Status</td>
		            <td width="500">
		            	<select class="form-control-new" name="v_Status" id="v_Status" style="width: 200px;">
		            		<option value="T">Non Aktif</option>
		            	</select>
		            </td>
		            
		            <td class="title_table" width="150">Penjualan POS Touch</td>
	                <td> 
	            	   <select class="form-control-new" name="v_penjualan_pos_touch" id="v_penjualan_pos_touch" style="width: 70px;" onchange="muncul()">
		            		<option value="N">Tidak</option>
		            		<option value="Y">Ya</option>		            		
		            	</select>
	                </td>
	                
	                <td id="td_subkategori_pos_touch1" class="title_table" width="100">Sub Kategori</td>
	                <td id="td_subkategori_pos_touch2"> 
	            	   <select class="form-control-new" name="v_subkategori_pos_touch" id="v_subkategori_pos_touch" style="width: 150px;">
		            		<?php foreach($subkategoripos as $val){?>
		            		     <option value="<?php echo $val['KdSubKategori']; ?>"><?php echo $val['NamaSubKategori']; ?></option>
		            		<?php }?>
		            	</select>
	                </td>
		        </tr>
		        
		        
	        
	    </table>
	    
	    <?php if($otorisasi=="Y"){?>
	    <ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Finance</strong></li>
		</ol>
		
		
		<table class="table table-bordered responsive">                        

	        
	               <tr id="td_muncul1">
                        <td class="title_table" width="200">Harga</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_Harga1c')" name="v_Harga1c" id="v_Harga1c" value="" onKeyUp="getHarga1c(),toFormat('dp')">
                       		 <input type='hidden' name="v_hidden_harga1c" id="v_hidden_harga1c" value="">
                        </td>
                        
                    </tr>
                    
                    <tr id="td_muncul2">
                        <td class="title_table" width="200">PPN</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_PPN')" name="v_PPN" id="v_PPN" value="">
                        </td>
                        
                    </tr>
                    
                    <tr id="td_muncul3">
                        <td class="title_table" width="200">Service Charge</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_Service_charge')" name="v_Service_charge" id="v_Service_charge" value="">
                        </td>
                        
                    </tr>
                    
                    <tr id="td_muncul4">
                        <td class="title_table" width="200">Komisi</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_komisi')" name="v_komisi" id="v_komisi" value="">
                        </td>
                    </tr>
                    
                    <tr id="td_muncul5">
                        <td class="title_table" width="200">Disc Internal</td>
                        <td>
                             <input type="text" class="form-control-new" style="width: 200px; text-align: right" onblur="toFormat2('v_DiscInternal')" name="v_DiscInternal" id="v_DiscInternal" value="">
                        </td>
                    </tr>
                    
            <tr>
                <td class="title_table" width="200">Nomor Rekening Persediaan</td>
                <td>
                     <input type="text" placeholder="Nama Rekening" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaRekening1" id="v_NamaRekening1" maxlength="50" value="">
                     <input type="hidden"  name="v_KdRekening1" id="v_KdRekening1" maxlength="50" value="">
                     <button type="button" name="btn_check1" id="btn_check1" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check_rekening(this)" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
            
            
            <tr id="td_muncul6">
                <td class="title_table" width="200">Nomor Rekening HPP</td>
                <td>
                     <input type="text" placeholder="Nama Rekening" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaRekening2" id="v_NamaRekening2" maxlength="50" value="">
                     <input type="hidden"  name="v_KdRekening2" id="v_KdRekening2" maxlength="50" value="">
                     <button type="button" name="btn_check2" id="btn_check2" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check_rekening(this)" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
            
            <tr id="td_muncul7">
                <td class="title_table" width="200">Nomor Rekening Penjualan</td>
                <td>
                     <input type="text" placeholder="Nama Rekening" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaRekening3" id="v_NamaRekening3" maxlength="50" value="">
                     <input type="hidden"  name="v_KdRekening3" id="v_KdRekening3" maxlength="50" value="">
                     <button type="button" name="btn_check3" id="btn_check3" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check_rekening(this)" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
            
            <tr id="td_muncul8">
                <td class="title_table" width="200">Nomor Rekening Return</td>
                <td>
                     <input type="text" placeholder="Nama Rekening" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaRekening4" id="v_NamaRekening4" maxlength="50" value="">
                     <input type="hidden"  name="v_KdRekening4" id="v_KdRekening4" maxlength="50" value="">
                     <button type="button" name="btn_check4" id="btn_check4" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check_rekening(this)" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
            
            <tr id="td_muncul9">
                <td class="title_table" width="200">Nomor Rekening Disc</td>
                <td>
                     <input type="text" placeholder="Nama Rekening" class="form-control-new" style="width: 400px;  text-transform: uppercase;" name="v_NamaRekening5" id="v_NamaRekening5" maxlength="50" value="">
                     <input type="hidden"  name="v_KdRekening5" id="v_KdRekening5" maxlength="50" value="">
                     <button type="button" name="btn_check5" id="btn_check5" class="btn btn-info btn-icon btn-sm icon-left" onclick="pop_up_check_rekening(this)" value="Check">Check<i class="entypo-check"></i></button>    
                </td>
                
            </tr>
                 
                 
                 <tr id="td_muncul10">
		            <td class="title_table" width="200">Status</td>
		            <td>
		            	<select class="form-control-new" name="v_Konsinyasi" id="v_Konsinyasi" style="width: 200px;">
		            		<option  value="0">Non Konsinyasi</option>
		            		<option  value="1">Konsinyasi</option>
		            	</select>
		            </td>
		        </tr>
		     <?php } ?>   
		        <tr>
		            <td colspan="50%">&nbsp;</td>
		            <td colspan="50%">
		            	&nbsp;
		            </td>
		        </tr>
	        	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/master/master_barang/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    
	    </form> 
        
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

		$(document).ready(function()
		{
			$('#v_PCode').attr('disabled',true);
			$('#v_Barcode1').attr('disabled',true);
			
			document.getElementById("td_subkategori_pos_touch1").style.display = "none";
			document.getElementById("td_subkategori_pos_touch2").style.display = "none";
			
			    document.getElementById("td_muncul1").style.display = "none";
				document.getElementById("td_muncul2").style.display = "none";
				document.getElementById("td_muncul3").style.display = "none";
				document.getElementById("td_muncul4").style.display = "none";
				document.getElementById("td_muncul5").style.display = "none";
				document.getElementById("td_muncul6").style.display = "none";
				document.getElementById("td_muncul7").style.display = "none";
				document.getElementById("td_muncul8").style.display = "none";
				document.getElementById("td_muncul9").style.display = "none";
				document.getElementById("td_muncul10").style.display = "none";
				
		});
		
		function CallAjaxForm(tipenya,param1,param2,param3,param4,param5)
		{
			try
			{
				if (!tipenya) return false;
				//document.getElementById("show_image_ajax").style.display='block';

				if (param1 == undefined) param1 = '';
				if (param2 == undefined) param2 = '';
				if (param3 == undefined) param3 = '';
				if (param4 == undefined) param4 = '';
				if (param5 == undefined) param5 = '';

				var variabel;
				var arr_data;
				variabel = "";

				if(tipenya=='ajax_divisi')
				{
					v_KdDivisi = document.getElementById("v_KdDivisi").value;
					
					document.getElementById("td_KdSubDivisi").innerHTML = "Loading...";

					 var url = $("#base_url").val();
				     
						  $.ajax({
								url: url+"index.php/master/master_barang/getDataSubDivisi/",
								data: {divisi:v_KdDivisi},
								type: "POST",
								dataType: 'html',					
								success: function(res)
								{
									$('#td_KdSubDivisi').html(res);
								},
								error: function(e) 
								{
									alert(e);
								} 
					   });
				}
                else if(tipenya=='ajax_kategori')
                {
                    v_KdKategori = document.getElementById("v_KdKategori").value;
                                        
                    document.getElementById("td_KdSubKategori").innerHTML = "Loading...";

                     var url = $("#base_url").val();
				     
						  $.ajax({
								url: url+"index.php/master/master_barang/getDataSubKategori/",
								data: {kategori:v_KdKategori},
								type: "POST",
								dataType: 'html',					
								success: function(res)
								{
									$('#td_KdSubKategori').html(res);
								},
								error: function(e) 
								{
									alert(e);
								} 
					   });
					   
					oto = document.getElementById("v_otorisasi").value;
					if(oto=="Y"){
					   //muncul finance
					    cek = document.getElementById("v_KdKategori").value;
						if(cek=="3" || cek=="8"){				
							document.getElementById("td_muncul1").style.display = "";
							document.getElementById("td_muncul2").style.display = "";
							document.getElementById("td_muncul3").style.display = "";
							document.getElementById("td_muncul4").style.display = "";
							document.getElementById("td_muncul5").style.display = "";
							document.getElementById("td_muncul6").style.display = "";
							document.getElementById("td_muncul7").style.display = "";
							document.getElementById("td_muncul8").style.display = "";
							document.getElementById("td_muncul9").style.display = "";
							document.getElementById("td_muncul10").style.display = "";
							
							//kode group barang
							document.getElementById("tampil_kode_group_barang").style.display = "";
							
						}else{					
								document.getElementById("td_muncul1").style.display = "none";
								document.getElementById("td_muncul2").style.display = "none";
								document.getElementById("td_muncul3").style.display = "none";
								document.getElementById("td_muncul4").style.display = "none";
								document.getElementById("td_muncul5").style.display = "none";
								document.getElementById("td_muncul6").style.display = "none";
								document.getElementById("td_muncul7").style.display = "none";
								document.getElementById("td_muncul8").style.display = "none";
								document.getElementById("td_muncul9").style.display = "none";
								document.getElementById("td_muncul10").style.display = "none";
								
								//kode group barang
							    document.getElementById("tampil_kode_group_barang").style.display = "none";
							    $('[name="v_groupbarang"]').val('');
						}
					}
					   
                }
                else if(tipenya=='ajax_subdivisi')
                {
                    v_KdSubDivisi = document.getElementById("v_KdSubDivisi").value;
                                       
                    document.getElementById("td_Kategori").innerHTML = "Loading...";
                    
                    var url = $("#base_url").val();
				     
						  $.ajax({
								url: url+"index.php/master/master_barang/getDataKategori/",
								data: {subdivisi:v_KdSubDivisi},
								type: "POST",
								dataType: 'html',					
								success: function(res)
								{
									$('#td_Kategori').html(res);
								},
								error: function(e) 
								{
									alert(e);
								} 
					   });
                }
                
                else if(tipenya=='ajax_kategori_pos')
                {
                    v_KdKategori = document.getElementById("v_KdKategori").value;
                    
                    document.getElementById("td_KdSubKategori").innerHTML = "Loading...";
                    
                    var url = $("#base_url").val();
				     
						  $.ajax({
								url: url+"index.php/master/master_barang/getDataSubKategoriPos/",
								data: {kategori:v_KdKategori},
								type: "POST",
								dataType: 'html',					
								success: function(res)
								{
									$('#td_KdSubKategori').html(res);
								},
								error: function(e) 
								{
									alert(e);
								} 
					   });
                    
                }
                else if (tipenya == 'cek_pcode')
	            {
		            	pcode = $('#v_PCode').val();
		            	var url = $("#base_url").val();
		            			            	
		            	$.ajax({
						url: url+"index.php/master/master_barang/cek_duplicate_pcode/",
						data: {kdbrg:pcode},
						type: "POST",
						dataType: 'json',					
						success: function(data)
						{
							if(data=='1'){
								alert("PCode Duplicate.");
								document.getElementById('v_PCode').focus();
								return false;
							}	
						    						
						},
						error: function(e) 
						{
							//alert(e);
						} 
				     });	            	
	            }
	            else if (tipenya == 'cek_barang')
	            {
		            	barang = $('#v_NamaLengkap').val();
		            	var url = $("#base_url").val();
		            	
		            	$.ajax({
						url: url+"index.php/master/master_barang/cek_duplicate_barang/",
						data: {brg:barang},
						type: "POST",
						dataType: 'json',					
						success: function(data)
						{
							if(data=='1'){
								alert("Nama Barang Duplicate.");
								document.getElementById('v_NamaLengkap').focus();
								return false;
							}	
						    						
						},
						error: function(e) 
						{
							//alert(e);
						} 
				     });	            	
	            }
	            else if (tipenya == 'cek_barcode')
	            {
		            	barcode= $('#v_Barcode1').val();
		            	var url = $("#base_url").val();
		            	
		            	$.ajax({
						url: url+"index.php/master/master_barang/cek_duplicate_barcode/",
						data: {brc:barcode},
						type: "POST",
						dataType: 'json',					
						success: function(data)
						{
							if(data=='1'){
								alert("Barcode Duplicate.");
								document.getElementById('v_Barcode1').focus();
								return false;
							}	
						    						
						},
						error: function(e) 
						{
							//alert(e);
						} 
				     });	            	
	            }
                
                
				     
                     
			}
			catch(err)
			{
				txt  = "There was an error on this page.\n\n";
				txt += "Error description : "+ err.message +"\n\n";
				txt += "Click OK to continue\n\n";
				alert(txt);
			}
		}
		
		function pop_up_check()
        {
        	if($('#v_NamaLengkap').val()==""){
				alert("Isi Nama Barang Terlebih Dahulu.");
				$('#v_NamaLengkap').focus();
				return false;
			}
			
            try{
                v_NamaLengkap = document.getElementById("v_NamaLengkap").value;
                var base_url = $("#base_url").val();
                
                url = base_url+"index.php/pop/pop_up_cari_nama_barang/index/"+v_NamaLengkap+"/";
		        windowOpener(600, 800, 'Check Nama Barang', url, 'Check Nama Barang')   
            
            }
            catch(err)
            {
                txt  = "There was an error on this page.\n\n";
                txt += "Error description : "+ err.message +"\n\n";
                txt += "Click OK to continue\n\n";
                alert(txt);
            }
        }
        
        function muncul(){
			cek = document.getElementById("v_penjualan_pos_touch").value;
			if(cek=="Y"){
				document.getElementById("td_subkategori_pos_touch1").style.display = "";
				document.getElementById("td_subkategori_pos_touch2").style.display = "";
			}else{
				document.getElementById("td_subkategori_pos_touch1").style.display = "none";
				document.getElementById("td_subkategori_pos_touch2").style.display = "none";
			}      
		}
		
		function buka(){
			cek = document.getElementById("v_Auto").value;
			if(cek=="N"){
				$('#v_PCode').attr('disabled',false);
			    $('#v_Barcode1').attr('disabled',false);
			    $('#v_PCode').val("");
			    $('#v_Barcode1').val("");
			    $('#v_PCode').focus();
			}else{
				$('#v_PCode').attr('disabled',true);
			    $('#v_Barcode1').attr('disabled',true);
			}
		}
		
		function pop_up_check_rekening(obj)
        {
		    base_url = $("#base_url").val();		    

		    objek = obj.id;
			id = parseFloat(objek.substr(9,objek.length-9));
			nama = $("#v_NamaRekening"+id).val();
			if(nama==""){
				alert("Isi Nama Rekening Terlebih Dahulu.");
				$('#v_NamaRekening'+id).focus();
				return false;
			}else{
			url = base_url+"index.php/pop/pop_up_no_rek_master_barang/index/"+id+"/"+nama;
			windowOpener(500, 600, 'Cari Rekening', url, 'Cari Rekening')
			}
		
        }
        
        function getHarga1c(){
			var harga1c = reform(document.getElementById("v_Harga1c").value)*1;
			document.getElementById("v_Harga1c").value = format(harga1c);
			document.getElementById("v_hidden_harga1c").value = harga1c;
		}
		
		function toFormat(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format(document.getElementById(id).value);
		}
		
		function toFormat2(id)
		{
			//alert(document.getElementById(id).value);
			if ((!isFinite(reform(document.getElementById(id).value)))||(document.getElementById(id).value.length==0))
			{
				//alert("That's not a number.")
				document.getElementById(id).value=0;
				//document.getElementById(id).focus();
			}
			document.getElementById(id).value=reform(document.getElementById(id).value);
			document.getElementById(id).value=format2(document.getElementById(id).value);
		}
		
		function cekTheform()
		{			
				if($("#v_NamaLengkap").val()==""){
					alert("Nama Barang harus diisi...");
					$('#v_NamaLengkap').focus();
					return false;
				}
				
				if($("#v_KdDivisi").val()==""){
					alert("Divisi harus dipilih...");
					$('#v_KdDivisi').focus();
					return false;
				}
				
				if($("#v_KdSubDivisi").val()==""){
					alert("SubDivisi harus dipilih...");
					$('#v_KdSubDivisi').focus();
					return false;
				}
				
				if($("#v_KdKategori").val()==""){
					alert("Kategori harus dipilih...");
					$('#v_KdKategori').focus();
					return false;
				}
				
				if($("#v_KdSubKategori").val()==""){
					alert("SubKategori harus dipilih...");
					$('#v_KdSubKategori').focus();
					return false;
				}
				
				if($("#v_SatuanSt").val()==""){
					alert("Satuan harus dipilih...");
					$('#v_SatuanSt').focus();
					return false;
				}
						
				ceks = document.getElementById("v_KdKategori").value;
				
				if(ceks=="3"){
					
					/*if(parseInt($("#v_Harga1c").val())==0){
					alert("Harga Harus Diisi...");
					$('#v_NamaRekening1').focus();
					return false;
				    }*/
					
					if($("#v_NamaRekening1").val()=="-"){
					alert("Rekening Persediaan Harus dipilih...");
					$("#v_NamaRekening1").val("");
					$('#v_NamaRekening1').focus();
					return false;
				    }
				    
				    if($("#v_NamaRekening2").val()=="-"){
					alert("Rekening HPP Harus dipilih...");
					$("#v_NamaRekening2").val("");
					$('#v_NamaRekening2').focus();
					return false;
				    }
				    
				    if($("#v_NamaRekening3").val()=="-"){
					alert("Rekening Penjualan Harus dipilih...");
					$("#v_NamaRekening3").val("");
					$('#v_NamaRekening3').focus();
					return false;
				    }
				    
				    if($("#v_NamaRekening4").val()=="-"){
					alert("Rekening Return Harus dipilih...");
					$("#v_NamaRekening4").val("");
					$('#v_NamaRekening4').focus();
					return false;
				    }
				    
				    if($("#v_NamaRekening5").val()=="-"){
					alert("Rekening Disc Harus dipilih...");
					$("#v_NamaRekening5").val("");
					$('#v_NamaRekening5').focus();
					return false;
				    }
				    
				    if($("#v_groupbarang").val()==""){
					alert("Kode Group Barang Harus dipilih...");
					$("#v_groupbarang").val("0");
					$('#v_groupbarang').focus();
					return false;
				    }
				    
				}else{
					
					if($("#v_NamaRekening1").val()=="-"){
					alert("Rekening Persediaan Harus dipilih...");
					$("#v_NamaRekening1").val("");
					$('#v_NamaRekening1').focus();
					return false;
				    }
				    
				}	
											
			    var yesSubmit = true;
		    	
		        if(yesSubmit)
		        {
					document.getElementById("theform").submit();	
				}  
			
		}
</script>