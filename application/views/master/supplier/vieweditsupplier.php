<?php
$this->load->view('header');
?>
<script>
    function start_page()
    {
        document.getElementById("nama").focus();
    }
    
    function cek_form()
    {
        if(document.getElementById("nama").value=="")
        {
            alert("Nama harus diisi...");
            document.getElementById("nama").focus();
            return false;
        }
        else if(document.getElementById("contact").value=="")
        {
            alert("Contact harus diisi...");
            document.getElementById("contact").focus();
            return false;
        } else if (document.getElementById("alm").value=="") {
			alert("Alamat harus dipilih...");
            document.getElementById("alm").focus();
            return false;
        } else if (document.getElementById("kota").value=="") {
			alert("Kota harus dipilih...");
            document.getElementById("kota").focus();
            return false;
		} else if (document.getElementById("telp").value=="") {
			alert("Telp harus dipilih...");
            document.getElementById("telp").focus();
            return false;
		} else if (document.getElementById("npwp").value=="") {
			alert("NPWP harus dipilih...");
            document.getElementById("npwp").focus();
            return false;
		} else if (document.getElementById("top").value=="") {
			alert("Jatuh Tempo harus dipilih...");
            document.getElementById("top").focus();
            return false;
        } else if (document.getElementById("ppn").value=="") {
			alert("PPN harus dipilih...");
            document.getElementById("ppn").focus();
            return false;
		}
    }
</script>

<body onload="start_page()">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$label?>
                </div>
            </div>
            <div class="panel-body">
                <form method='post' name="supplier" id="supplier" action='<?= base_url(); ?>index.php/master/supplier/save_supplier' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()"">
					<div class="form-group">
			            <label class="col-sm-2 control-label">Kode</label>
			            <div class="col-sm-5">
			                <input name="kode" id="kode" size="10" value="<?=$viewsupplier->KdSupplier;?>" maxlength="10" readonly="readonly" class="form-control" type="text">
			            </div>
			        </div>
			        
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Nama (*)</label>
		                <div class="col-sm-5">
		                    <input name="nama" id="nama" size="30" value="<?=$viewsupplier->Nama;?>" maxlength="30"  class="form-control" type="text">
		                </div>
		            </div>
		            
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Contact (*)</label>
		                <div class="col-sm-5">
		                    <input name="contact" id="contact" size="30" value="<?=$viewsupplier->Contact;?>" maxlength="30" class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Alamat (*)</label>
		                <div class="col-sm-5">
		                    <textarea name="alm" id="alm" row="5" col="27"  class="form-control"><?=$viewsupplier->Alamat;?> </textarea>
		                </div>
		            </div>
					<div class="form-group">
		                <label class="col-sm-2 control-label">Kota (*)</label>
		                <div class="col-sm-5">
		                    <input name="kota" id="kota" size="30" value="<?=stripslashes($viewsupplier->Kota);?>" maxlength="30" class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">No Telp (*)</label>
		                <div class="col-sm-5">
		                    <input name="telp" id="telp" size="50" value="<?=stripslashes($viewsupplier->Telepon);?>" maxlength="50" class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Nama Pajak</label>
		                <div class="col-sm-5">
		                    <input name="namapajak" id="namapajak" size="30" value="<?=$viewsupplier->NamaPajak;?>" maxlength="30"  class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Alamat Pajak</label>
		                <div class="col-sm-5">
		                    <textarea name="alamatpajak" id="alamatpajak" row="5" col="27"  class="form-control"><?=$viewsupplier->AlamatPajak;?> </textarea>
		                </div>
		            </div>
					<div class="form-group">
		                <label class="col-sm-2 control-label">Kota Pajak</label>
		                <div class="col-sm-5">
		                    <input name="kotapajak" id="kotapajak" size="30" value="<?=stripslashes($viewsupplier->KotaPajak);?>" maxlength="30" class="form-control" type="text">
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-2 control-label">NPWP (*)</label>
		                <div class="col-sm-5">
		                    <input name="npwp" id="npwp" size="30" value="<?=stripslashes($viewsupplier->NPWP);?>" maxlength="30" class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Tempo Pembayaran (Hari)</label>
		                <div class="col-sm-1">
		                    <input name="top" id="top" size="5" value="<?=$viewsupplier->TOP;?>" maxlength="5" style="text-align: right" class="form-control" type="text"> 
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">PPN</label>
		                <div class="col-sm-1">
		                    <input name="ppn" id="ppn" size="5" value="<?=$viewsupplier->PPn;?>" maxlength="5" style="text-align: right" class="form-control" type="text">
		                </div>
		            </div>
		            
		             <div class="form-group">
		                <label class="col-sm-2 control-label">Email</label>
		                <div class="col-sm-5">
		                    <input name="email" id="email" size="30" value="<?=$viewsupplier->Email;?>" maxlength="30"  class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Notes</label>
		                <div class="col-sm-5">
		                    <input name="notes" id="notes" size="50" value="<?=$viewsupplier->Notes;?>" maxlength="50"  class="form-control" type="text">
		                </div>
		            </div>
		            
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Status</label>
		                <div class="col-sm-5">
					        <select size="1" id="stataktif" name="stataktif">
								<option <?= $viewsupplier->StatAktif=='Y'? "selected":"" ;?> value="Y">Ya</option>
								<option <?= $viewsupplier->StatAktif=='T'? "selected":"" ;?> value="T">Tidak</option>
							</select>
		                </div>
		            </div>
				
					<div class="form-group" align="center">
						<input type='submit' value='Save'>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/supplier/" />
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
<?php
$this->load->view('footer'); ?>