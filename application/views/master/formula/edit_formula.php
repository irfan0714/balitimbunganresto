<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'formula',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/formula.js"></script>
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body>
<form method='post' name="formula" id="formula" action='<?=base_url();?>index.php/master/formula/save_new_formula' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Edit Formula</legend>
			<table class="table_class_list">
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("No","nodok",$header->kdformula,"10","10","readonly='readonly'","text","","1");
			echo $mylib->write_textbox("Tanggal","tgl",$header->tglformula,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Nama Formula","namaformula",$header->namaformula,"35","30","","text",$gantikursor,"1");
		    ?>
			<tr>
				<td nowrap>Kode Barang</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" name="pcodeatas" id="pcodeatas" value=<?=$header->pcode?> onkeydown="keyShortcut(event,'pcodeatas',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickBarang();" id="btnbarang">
				<input type="hidden" name="hiddenbarang" id="hiddenbarang">
				<input type="text" name="barangname" id="barangname" value="<?=$header->namabarang?>" size="30" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>Status</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="D" <?php echo $checkedd; ?> name="sumber" id="sumberD" onclick="ubahstatus();"/>Default-Aktif
					<input type="radio" value="A" <?php echo $checkeda; ?> name="sumber" id="sumberA" onclick="ubahstatus();"/>Aktif
					<input type="radio" value="T" <?php echo $checkedt; ?> name="sumber" id="sumberT" onclick="ubahstatus();"/>Tidak Aktif
				</td>
			</tr>
		    <?php
			echo $mylib->write_number("Qty","qtyatas",$header->qty,"20","15","","text",$gantikursor,"1","");
			echo $mylib->write_combo("Satuan","satuanatas",$msatuan,$header->satuan,"KdSatuan","NamaSatuan",$gantikursor,"","ya");
		  
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
			  <p align="center">
			  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
			</p>
		</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="AddNew()"></td>
					<td>KdBarang</td>
					<td>NamaBarang</td>
					<td>Satuan</td>
					<td>Qty</td>
				</tr>
			<?php
			//$modeller = new sales_ordermodel();
			for($z=0;$z<count($detail);$z++){
				$selectB = "";
				$selectT = "";
				$selectK = "";
				if($detail[$z]['NamaSatuan']==$detail[$z]['Nama1'])
				{ $selectB = "selected='selected'"; $nilsatuan = "1|".$detail[$z]['Satuan1'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama2'])
				{ $selectT = "selected='selected'"; $nilsatuan = "2|".$detail[$z]['Satuan2'];}
				else if($detail[$z]['NamaSatuan']==$detail[$z]['Nama3'])
				{ $selectK = "selected='selected'"; $nilsatuan = "3|".$detail[$z]['Satuan3'];}
				$msatuan = "<option ".$selectB."value='".$detail[$z]['Satuan1']."'>".$detail[$z]['Nama1']."</option>";
				$msatuan .= "<option ".$selectT."value='".$detail[$z]['Satuan2']."'>".$detail[$z]['Nama2']."</option>";
				$msatuan .= "<option ".$selectK."value='".$detail[$z]['Satuan3']."'>".$detail[$z]['Nama3']."</option>";
		
				$mylib->write_detailFormula($z+1,$detail[$z]['PCode'],$detail[$z]['NamaInitial'],$detail[$z]['Qty'],$detail[$z]['PCode'],$detail[$z]['Satuan'],"",$msatuan);
			}
			?>
			<script language="javascript">
				detailNew();
			</script>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
			    <input type='hidden' id="hiddensumber" name="hiddensumber" value="<?=$header->status?>">
				<input type='hidden' id="hidepcodeatas" name="hidepcodeatas" value="<?=$header->pcode?>">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="flag" name="flag" value="edit">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/formula/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>