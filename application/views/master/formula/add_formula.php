<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'formula',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/formula.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	left:45%;
	top:40%;
	width:0px;
	height:0px;
	z-index:1;
	background-color:#FFFFFF;
}
-->
</style>
<body onload="firstLoad('formula');loading()">
<form method='post' name="formula" id="formula" action='<?=base_url();?>index.php/master/formula/save_new_formula' onsubmit="return false">
	<table align = 'center' >
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Add Formula</legend>
			<table class="table_class_list">
			<tr id="nodokumen" style="display:none">
				<td nowrap>No</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" size="10" readonly="readonly" name="nodok" id="nodok" /></td>
			</tr>
			<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Tanggal","tgl",$tanggal,"10","10","readonly='readonly'","text",$gantikursor,"1");
			echo $mylib->write_textbox("Nama Formula","namaformula","","35","30","","text",$gantikursor,"1");
			?>
			<tr>
				<td nowrap>Kode Barang</td>
				<td nowrap>:</td>
				<td nowrap colspan="1" ><input type="text" maxlength="10" size="10" name="pcodeatas" id="pcodeatas" onkeydown="keyShortcut(event,'pcodeatas',this);" <?=$gantikursor;?> /> 
				<input type="button" value="..." onclick="pickBarang(this);" id="btnbarang">
				<input type="hidden" name="hiddenbarang" id="hiddenbarang">
				<input type="text" name="barangname" id="barangname" size="30" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td nowrap>Status</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="D" checked name="sumber" id="sumberD" onclick="ubahstatus();"/>Default-Aktif
					<input type="radio" value="A" name="sumber" id="sumberA" onclick="ubahstatus();"/>Aktif
					<input type="radio" value="T" name="sumber" id="sumberT" onclick="ubahstatus();"/>Tidak Aktif
				</td>
			</tr>
			<?php
			echo $mylib->write_number("Qty","qtyatas","1","20","15","","text",$gantikursor,"1","");
			echo $mylib->write_combo("Satuan","satuanatas",$satuanatas,"","KdSatuan","NamaSatuan",$gantikursor,"","ya");
			?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td>
			<fieldset class="disableMe">
			<legend class="legendStyle">Detail</legend>
			<div id="Layer1" style="display:none">
				<p align="center">
				  <img src='<?=base_url();?>public/images/ajax-loader.gif'>
				</p>
			</div>
			<table class="table_class_list" id="detail">
				<tr id="baris0">
					<td><img src="<?=base_url();?>/public/images/table_add.png" width="16" height="16" border="0" onClick="detailNew()"></td>
					<td>KdBarang</td>
					<td>NamaBarang</td>
					<td>Satuan</td>
					<td>Qty</td>
				</tr>
				<?=$mylib->write_detailFormula(1,"","",0,"","","","")?>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td nowrap>
				<input type='hidden' id="hidepcodeatas" name="hidepcodeatas">
				<input type='hidden' id="transaksi" name="transaksi" value="no">
				<input type='hidden' id="hiddensumber" name="hiddensumber" value='D'>
				<input type='hidden' id="flag" name="flag" value="add">
				<input type='hidden' value='<?=base_url()?>' id="baseurl" name="baseurl">
				<input type='button' value='Save' onclick="saveAll();"/>
				<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/formula/" />
			</td>
		</tr>
	</table>
</form>

<?php
$this->load->view('footer'); ?>