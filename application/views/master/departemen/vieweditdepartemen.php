<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'departemen',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('departemen')">
<form method='post' name="departemen" id="departemen" action='<?=base_url();?>index.php/master/departemen/save_departemen'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Departemen</b></legend>
			<table align = 'center'>
				<tr>
					<td nowrap>Kode</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="2" size="5" readonly name='kode' id='kode' value="<?=stripslashes($viewdepartemen->KdDepartemen);?>" /></td>
				</tr>
				<tr>
					<td nowrap>Nama</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($viewdepartemen->NamaDepartemen);?>" <?=$gantikursor;?>/></td>
				</tr>
				<tr>
					<td nowrap colspan="3">
					<?php if($edit){ ?>
						<input type='button' value='Save' onclick="cekMaster('kode','nama','departemen','Kode Departemen','Nama Departemen');"/>
					<?php } ?>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/departemen/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>