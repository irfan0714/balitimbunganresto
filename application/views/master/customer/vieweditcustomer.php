<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'customer',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
function ubahpayment()
{
	sumber = $("input[@name='sumber']:checked").val();
	$("#hiddensumber").val(sumber);
	if(sumber=="C")
	{
	$("#top").attr("disabled", "disabled");
	$("#top").val("0");
	}
    else
	$("#top").attr("disabled", "");
}
function loading()
{
	base_url = $("#baseurl").val();
	$('#tglpkp').datepicker({ dateFormat: 'dd-mm-yyyy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
}
</script>
<body onload="firstLoad('customer');loading()">
<form method='post' name="customer" id="customer" action='<?=base_url();?>index.php/master/customer/save_customer'>
<table class="table table-bordered responsive">
	<tr>
		<td>
		<legend><b><?=$fieldset?> Customer</b></legend>
			<table class="table table-bordered responsive">
		<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Kode","kode",stripslashes($viewcustomer->KdCustomer),"8","6","readonly","text",$gantikursor,"1");
			echo $mylib->write_textbox("Nama","nama",stripslashes($viewcustomer->Nama),"55","50","","text",$gantikursor,"1");
			echo $mylib->write_textbox("Contact","contact",stripslashes($viewcustomer->Contact),"55","50","","text",$gantikursor,"1");
		?>
			<tr>
				<td>Alamat</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="alm" name="alm"><?=stripslashes($viewcustomer->Alamat)?></textarea>
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Kota","kota",stripslashes($viewcustomer->Kota),"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox("No. Telp","telp",$viewcustomer->Telepon,"25","20","","text",$gantikursor,"1");
		?>
			<tr>
				<td>Alamat Kirim</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="almkirim" name="almkirim"><?=stripslashes($viewcustomer->AlamatKirim)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Kirim","kotakirim",stripslashes($viewcustomer->KotaKirim),"35","30","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("No. Telp Kirim","telpkirim",$viewcustomer->TelpKirim,"25","20","","text",$gantikursor,"1");?>
			<?$mylib->write_textbox("Nama Pajak","namapajak",stripslashes($viewcustomer->NamaPajak),"55","50","","text",$gantikursor,"1");?>
			<tr>
				<td>Alamat Pajak</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="almpajak" name="almpajak"><?=stripslashes($viewcustomer->AlamatPajak)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Pajak","kotapajak",stripslashes($viewcustomer->KotaPajak),"35","30","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("NPWP","npwp",$viewcustomer->NPWP,"25","20","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("Tanggal PKP","tglpkp",$viewcustomer->Tanggal,"15","10","","text",$gantikursor,"1");?>
			<?=$mylib->write_combo("Tipe","tipe",$tipecustomer,$viewcustomer->KdTypecust,"KdTypecust","NamaTypecust",$gantikursor,"","ya");?>
			<?=$mylib->write_combo("Group","group",$groupcustomer,$viewcustomer->KdGroupcust,"KdGroupcust","NamaGroupcust",$gantikursor,"","ya");?>
			<!--<?=$mylib->write_combo("Group Bayar","groupbayar",$groupbayar,$viewcustomer->KdGroupbayar,"KdGroupbayar","NamaGroupbayar",$gantikursor,"","ya");?>-->
			<?=$mylib->write_combo("Area","area",$area,$viewcustomer->KdArea,"KdArea","NamaArea",$gantikursor,"","ya");?>
			<tr>
				<td>Payment</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="C" <?php echo $checkedc; ?> name="sumber" id="sumber" onclick="ubahpayment();"/>Cash
					<input type="radio" value="K" <?php echo $checkedk; ?> name="sumber" id="sumber" onclick="ubahpayment();"/>Credit
				    <input type="text" name="top" id="top" maxlength="3" size="3" value="<?=$viewcustomer->TOP?>" <?php echo $topdisabled; ?>>Hari
				</td>
			</tr>
			<?php
			echo $mylib->write_number("Limit Kredit","limitkredit",$viewcustomer->LimitKredit,"25","20","","text",$gantikursor,"1","");
			echo $mylib->write_number("Limit Faktur","limitfaktur",$viewcustomer->LimitFaktur,"5","2","","text",$gantikursor,"1","");
			echo $mylib->write_number("PPn","ppn",$viewcustomer->PPn,"5","5","","text",$gantikursor,"1","");
			?>
			<tr>
				<td>Status Aktif</td>
				<td nowrap>:</td>
				<td nowrap colspan="5">
				<select class="form-control-new"  size="1" id="stataktif" name="stataktif" <?=$gantikursor;?>>
				<option <?php if($viewcustomer->StatAktif=="Y") echo "selected";?> value="Y">Ya</option>
				<option <?php if($viewcustomer->StatAktif=="N") echo "selected";?> value="N">Tidak</option>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input class="btn btn-info btn-sm sm-new tooltip-primary" type='button' value='Save' onclick="cekcustomer();"/>
				<?php } ?>
					<input class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/customer/" />
				</td>
			</tr>
			</table>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>