<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'customer',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<script language="javascript">
function ubahpayment()
{
	sumber = $("input[@name='sumber']:checked").val();
	$("#hiddensumber").val(sumber);
	if(sumber=="C")
	{
	$("#top").attr("disabled", "disabled");
	$("#top").val("0");
	}
    else
	$("#top").attr("disabled", "");
}
function loading()
{
	base_url = $("#baseurl").val();
	$('#tglpkp').datepicker({ dateFormat: 'dd-mm-yyyy',mandatory: true,showOn: "both", buttonImage: base_url+ "public/images/calendar.png", buttonImageOnly: true } );
}
</script>
<body onload="firstLoad('customer');loading()">
<form method='post' name="customer" id="customer" action='<?=base_url();?>index.php/master/customer/save_new_customer'>
<table class="table table-bordered responsive">
	<tr>
		<td>
			<legend><b>Add Customer</b></legend>
			<table class="table table-bordered responsive">
			<?php
			if($msg){ echo $msg;}
			$mylib = new globallib();
			//echo $mylib->write_textbox("Kode","kode",$id,"8","6","readonly='readonly'","text",$gantikursor."onKeyUp=\"javascript:dodacheck(document.getElementById('kode'));\"","1","no");
			echo $mylib->write_textbox("Nama","nama",stripslashes($nama),"55","50","","text",$gantikursor,"1","ya");
			echo $mylib->write_textbox("Contact","contact",stripslashes($contact),"35","30","","text",$gantikursor,"3");
			?>
			<tr>
				<td>Alamat</td>
				<td><b>:</b></td>
				<td>
				<textarea class="form-control-new" colspan="3" id="alm" name="alm"><?=stripslashes($alm)?></textarea>
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Kota","kota",stripslashes($kota),"35","30","","text",$gantikursor,"1","no");
			echo $mylib->write_textbox("No. Telp","telp",$telp,"25","20","","text",$gantikursor,"1","ya");
			?>
			<tr>
				<td>Alamat Kirim</td>
				<td><b>:</b></td>
				<td>
				<textarea class="form-control-new" colspan="3" id="almkirim" name="almkirim"><?=stripslashes($almkirim)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Kirim","kotakirim",stripslashes($kotakirim),"35","30","","text",$gantikursor,"1","no");?>
			<?=$mylib->write_textbox("No. Telp Kirim","telpkirim",$telpkirim,"25","20","","text",$gantikursor,"1","ya");?>
			<?$mylib->write_textbox("Nama Pajak","namapajak",stripslashes($namapajak),"55","50","","text",$gantikursor,"3");?>
			<tr>
				<td>Alamat Pajak</td>
				<td><b>:</b></td>
				<td>
				<textarea class="form-control-new" colspan="3" id="almpajak" name="almpajak"><?=stripslashes($almpajak)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Pajak","kotapajak",stripslashes($kotapajak),"35","30","","text",$gantikursor,"3");?>	
			<?=$mylib->write_textbox("NPWP","npwp",$npwp,"25","20","","text",$gantikursor,"1","no");?>
			<?=$mylib->write_textbox("Tanggal PKP","tglpkp",$tglpkp,"15","10","","text",$gantikursor,"1","ya");?>
			<?=$mylib->write_combo("Tipe","tipe",$tipecustomer,$niltipe,"KdTypecust","NamaTypecust",$gantikursor,"","ya");?>
			<?=$mylib->write_combo("Group Cust","group",$groupcustomer,$nilgroup,"KdGroupcust","NamaGroupcust",$gantikursor,"","ya");?>
			<?=$mylib->write_combo("Area","area",$area,$nilarea,"KdArea","NamaArea",$gantikursor,"","ya");?>
			<tr>
				<td>Payment</td>
				<td nowrap>:</td>
				<td nowrap colspan="3">
					<input class="form-control-new" type="radio" value="C" checked name="sumber" id="sumber" onclick="ubahpayment();"/>Cash
					<input class="form-control-new" type="radio" value="K" name="sumber" id="sumber" onclick="ubahpayment();"/>Credit
				    <input class="form-control-new" type="text" name="top" id="top" maxlength="3" size="3" value='0' disabled="disabled">Hari
				</td>
			</tr>
			<?php
			echo $mylib->write_number("Limit Kredit","limitkredit","0","25","20","","text",$gantikursor,"3","");
			echo $mylib->write_number("Limit Faktur","limitfaktur","0","5","2","","text",$gantikursor,"3","");
			echo $mylib->write_number("PPn","ppn","0","5","5","","text",$gantikursor,"1","");
			?>
			<tr>
				<td>Status Aktif</td>
				<td nowrap>:</td>
				<td nowrap colspan="3">
				<select class="form-control-new" size="1" id="stataktif" name="stataktif" <?=$gantikursor;?>>
				<option <?php echo "selected";?> value="Y">Ya</option>
				<option value="N">Tidak</option>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="5">
					<input class="btn btn-info btn-sm sm-new tooltip-primary" type='button' value='Save' onclick="cekcustomer();"/>
					<input class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/customer/" />
				</td>
			</tr>
			</table>

	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>