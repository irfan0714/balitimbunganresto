<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table>
	<tr>
		<td><input class="form-control-new"  type='text' size'20' name='stSearchingKey' id='stSearchingKey'>
			<select class="form-control-new"  size="1" height="1" name ="searchby" id ="searchby">
				<option value="KdCustomer">Kode Customer</option>
				<option value="Keterangan">Keterangan</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br><br>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th><center>Kode Customer</center></th>
		        <th><center>Tipe Customer</center></th>
		        <th><center>Nama Customer</center></th>
		        <th><center>Alamat</center></th>
		        <th><center>Kota</center></th>
		        <th><center>Telp.</center></th>
		        <th><center>Payment</center></th>
		        <th><center>Top</center></th>
		        <th><center>PPN</center></center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($customerdata) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($customerdata as $val) {
                
                ?>
                <tr title="<?php echo $val["KdCustomer"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["KdCustomer"]; ?></td>
                    <td align="center"><?php echo $val["NamaTypecust"]; ?></td>
                    <td align="center"><?php echo $val["Nama"]; ?></td>
                    <td align="center"><?php echo $val["Alamat"]; ?></td>
                    <td align="center"><?php echo $val["Kota"]; ?></td>
                    <td align="center"><?php echo $val["Telepon"]; ?></td>
                    <td align="center"><?php echo $val["Payment"]; ?></td>
                    <td align="center"><?php echo $val["TOP"]; ?></td>
                    <td align="center"><?php echo $val["PPn"]; ?></td>
                    <td align="center">
                            <a href="<?php echo base_url(); ?>index.php/master/customer/view_customer/<?php echo $val["KdCustomer"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/master/customer/edit_customer/<?php echo $val["KdCustomer"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/master/customer/delete_customer/<?php echo $val["KdCustomer"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title=""><i class="entypo-trash"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["KdSalesman"]; ?>', '<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>-->
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>




<!--<table class="table table-bordered responsive" align="center">
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php } 
		$mylib = new globallib();
		echo $mylib->write_header($header);
		?>	
	</tr>
<?php
	if(count($customerdata)==0)
	{ 
?>
	<td nowrap colspan="<?php echo count($header)+1;?>" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($customerdata);$a++)
{
?>
	<tr>
<?php
	if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
	{
?>
			<td nowrap>
		<?php
			if($link->view=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/customer/view_customer/<?=$customerdata[$a]['KdCustomer'];?>"><img src='<?=base_url();?>public/images/zoom.png' border = '0' title = 'View'/></a>
		<?php
			}
			if($link->edit=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/customer/edit_customer/<?=$customerdata[$a]['KdCustomer'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php
			}
			if($link->delete=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/customer/delete_customer/<?=$customerdata[$a]['KdCustomer'];?>"><img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete'/></a>
		<?php
			}
		?>
		</td>
<?php } ?>
		<td nowrap><?=stripslashes($customerdata[$a]['KdCustomer']);?></td>
		<td nowrap><?=stripslashes($customerdata[$a]['NamaTypecust']);?></td>
		<td nowrap><?=stripslashes($customerdata[$a]['Nama']);?></td>
		<td nowrap><?=stripslashes($customerdata[$a]['Alamat']);?></td>
		<td nowrap><?=stripslashes($customerdata[$a]['Kota']);?></td>
		<td nowrap><?=$customerdata[$a]['Telepon'];?></td>
		<td nowrap><?=$customerdata[$a]['Payment'];?></td>
		<td nowrap><?=$customerdata[$a]['TOP'];?></td>
		<td nowrap><?=$customerdata[$a]['PPn'];?></td>
	<tr>
<?php
}
?>
</table>-->
<table align = 'center'>
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/master/customer/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>