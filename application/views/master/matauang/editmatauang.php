<?php
$this->load->view('header');
?>

<body>
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    Edit Mata Uang 
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="matauang" id="matauang" action='<?= base_url(); ?>index.php/master/mata_uang/save_matauang' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kode</label>
                        <div class="col-sm-2">
                            <input name="kdmatauang" type="text" id="kdmatauang" value="<?php echo $viewmatauang->Kd_Uang; ?>" size="10" maxlength="10" class="form-control" readonly="readonly" /> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Mata Uang</label>
                        <div class="col-sm-5">
                            <input name="nmmatauang" type="text" id="nmmatauang" value="<?php echo $viewmatauang->Keterangan; ?>" size="30" maxlength="30" class="form-control"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Nilai</label>
                        <div class="col-sm-2">
                            <input name="nilai" type="text" id="nilai" value="<?php echo $viewmatauang->NilaiTukar; ?>" size="12" maxlength="12" class="form-control"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Status</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="status" name="status">
                                <option <?php if($viewmatauang->FlagAktif=="A"){ echo "selected";}?> value='A'>Aktif</option>
                                <option <?php if($viewmatauang->FlagAktif=="T"){ echo "selected";}?> value='T'>Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/master/mata_uang/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>