<?php $this->load->view('header'); ?>
<div class="col-md-12">
    <div class="panel panel-gradient">
        <div class="panel-heading">
            <div class="panel-title">
                List Mata Uang 
            </div>
        </div>
        <div class="panel-body">
            <form method="POST"  name="search" action="">
                <div class="control-group">
                    <div class="col-md-8">
                        <?php
                        if ($link->add == "Y") {
                            ?>
                            <a class="btn btn-primary" href="<?= base_url(); ?>index.php/master/mata_uang/add_new/"><i class="entypo-plus"></i>Tambah</a>
                        <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <table align="right">
                            <tr>
                                <td>
                                    <select size="1" height="1" name ="searchby" id ="searchby" class="form-control">
                                        <option value="Kd_Uang">Kode</option>
                                        <option value="Keterangan">Mata Uang</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type='text' name='stSearchingKey' id='stSearchingKey' class="form-control" value="<?php echo @$_POST['stSearchingKey']; ?>" placeholder="search.." />
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">GO</button>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
            <br>
            <br>
            <br>

            <table align = 'center' class='table table-bordered responsive table-hover'>
                <thead>
                    <tr>
                        <th nowrap>Kode</th>    
                        <th nowrap>Nama Mata Uang</th>
                        <th nowrap>Nilai</th>
                        <th nowrap>Terakhir di Update</th>
                        <?php
                        if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                            ?>
                            <th nowrap>Action</th>
                        <?php } $mylib = new globallib(); ?>
                    </tr>
                </thead>
                <?php
                if (count($matauangdata) == 0) {
                    ?>
                    <td nowrap colspan="4" align="center">Tidak Ada Data</td>
                    <?php
                }
                for ($a = 0; $a < count($matauangdata); $a++) {
                    ?>
                    <tr>

                        <td nowrap><?= $matauangdata[$a]['Kd_Uang']; ?></td>
                        <td nowrap><?= $matauangdata[$a]['Keterangan']; ?></td>
                        <td nowrap><?= $matauangdata[$a]['NilaiTukar']; ?></td>
                        <td nowrap><?= $matauangdata[$a]['EditDate']; ?></td>
                        <?php
                        if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                            ?>
                            <td nowrap>
                                <?php
                                if ($link->edit == "Y") {
                                    ?>
                                    <a 	href="<?= base_url(); ?>index.php/master/mata_uang/edit_matauang/<?= $matauangdata[$a]['Kd_Uang']; ?>"><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                                    <?php
                                }
                                if ($link->delete == "Y") {
                                    ?>
            <!--                                    <a 	href="<?= base_url(); ?>index.php/master/matauang/delete_matauang/<?= $matauangdata[$a]['Kd_Uang']; ?>"><img src='<?= base_url(); ?>public/images/cancel.png' border = '0' title = 'Delete'/></a>-->
                                    <?php
                                }
                                ?>
                            </td>
                        <?php } ?>
                    <tr>
                        <?php
                    }
                    ?>
            </table>
            <table align = 'center'  >
                <tr>
                    <td>
                        <?php echo $this->pagination->create_links(); ?>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>


<?php $this->load->view('footer'); ?>