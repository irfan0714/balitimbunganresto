<?php
$this->load->view('header');
?>
<script type="text/javascript">
    function cek_form() {
        if(!$('#kdmatauang').val()){
            alert('Kode Mata Uang Kosong');
            $('#kdmatauang').focus();
            return false;
        }
        if(!$('#nmmatauang').val()){
            alert('Nama Mata Uang Kosong');
            $('#nmmatauang').focus();
            return false;
        }
        if(!$('#nilai').val()){
            alert('Nilai Mata Uang Kosong');
            $('#nilai').focus();
            return false;
        }
    }
</script>
<body>
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    Add Mata Uang 
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="matauang" id="matauang" action='<?= base_url(); ?>index.php/master/mata_uang/save_new_matauang' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kode</label>
                        <div class="col-sm-2">
                            <input name="kdmatauang" type="text" id="kdmatauang" size="10" maxlength="10" class="form-control" /> 
                        </div>
                        <?php if (!empty($msg)) echo $msg ; ?>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Mata Uang</label>
                        <div class="col-sm-5">
                            <input name="nmmatauang" type="text" id="nmmatauang" size="30" maxlength="30" class="form-control"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Nilai</label>
                        <div class="col-sm-2">
                            <input name="nilai" type="text" id="nilai" size="12" maxlength="12" class="form-control"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Status</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="status" name="status">
                                <option value="A">Aktif</option>
                                <option value="T">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/master/mata_uang/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>