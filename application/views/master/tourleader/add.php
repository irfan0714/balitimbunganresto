<?php
    $this->load->view('header');
    $gantikursor = "onkeydown=\"changeCursor(event,'typesupp',this)\"";
    
    $q = "
            SELECT
                KdTravel,
                Nama
            FROM
                tourtravel
            WHERE
                1
                AND Nama != ''
            GROUP BY 
                Nama
            ORDER BY
                Nama ASC
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdTravel, $Nama) = $row;
        
        $arr_data["list_travel"][$KdTravel] = $KdTravel;
        $arr_data["travel_Nama"][$KdTravel] = $Nama;
    }
    
    $q = "
            SELECT
                KdTypeTourLeader,
                KeteranganTourLeader
            FROM
                `typetour_leader`
            WHERE
                1
                AND KeteranganTourLeader != ''
            ORDER BY
                KeteranganTourLeader ASC 
    ";
    $qry = mysql_query($q);
    while($row = mysql_fetch_array($qry))
    {
        list($KdTypeTourLeader, $KeteranganTourLeader) = $row;
        
        $arr_data["list_type_leader"][$KdTypeTourLeader] = $KdTypeTourLeader;
        $arr_data["type_leader_KeteranganTourLeader"][$KdTypeTourLeader] = $KeteranganTourLeader;
    }
    
    
?>
<script>
    function start_page()
    {
        document.getElementById("Nama").focus();
    }
    
    function cek_form()
    {
        if(document.getElementById("Nama").value=="")
        {
            alert("Nama harus diisi...");
            document.getElementById("Nama").focus();
            return false;
        }
        else if(document.getElementById("v_NoIdentitas").value=="")
        {
            alert("No Identitas harus diisi...");
            document.getElementById("v_NoIdentitas").focus();
            return false;
        } else if (document.getElementById("travelleader").value=="") {
			alert("Biro Member harus dipilih...");
            document.getElementById("travelleader").focus();
            return false;
		}
        
        
    }
</script>

<body onload="start_page()">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$label?>
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="matauang" id="matauang" action='<?= base_url(); ?>index.php/<?=$tr1."/".$tr2;?>/save' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">

                        <?php if (!empty($msg)) echo $msg ; ?>

                    <?php
                     /*
                    $mylib = new globallib();
//                    print_r($TypeTour);
                    echo $mylib->text_boostrap("Kode","Kode",$Kode,"10","10","readonly='readonly'","text",$gantikursor,"1");
                    echo $mylib->option_boostrap("Type Member", "typeleader", $TypeLeader, $TypeLeadernya, "KdTypeTourLeader", "KeteranganTourLeader", $gantikursor, "onchange=\"simpanKontak();\"");
                    echo $mylib->option_boostrap("Biro Member", "travelleader", $BiroTour, $BiroTournya, "KdTravel", "Nama", $gantikursor, "onchange=\"simpanKontak();\"");
                        echo $mylib->text_boostrap("Nama (*)","Nama",$Nama,"30","30","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Phone","Phone",$Phone,"20","20","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Fax","Fax",$Fax,"20","20","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Alamat","Alamat",$Alamat,"40","30","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Email","Email",$Email,"25","25","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Kota","Kota",$Kota,"25","25","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Telepon","Telepon",$Telepon,"25","25","","text",$gantikursor,"1");
                        echo $mylib->option_boostrap("Aktif", "status", $Aktif, "", "Kd", "Ket", $gantikursor, "onchange=\"simpanKontak();\"");
                     */
                    ?>
                    
                    
        <div class="form-group">
            <label class="col-sm-2 control-label">Kode</label>
            <div class="col-sm-5">
                <input name="Kode" id="Kode" size="10" value="" maxlength="10" readonly="readonly" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
    
        
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama (*)</label>
                <div class="col-sm-5">
                    <input name="Nama" id="Nama" size="30" value="" maxlength="30" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">No Identitas (*)</label>
                <div class="col-sm-5">
                    <select class="form-control" name="v_JenisIdentitas" id="v_JenisIdentitas" style="width: 15%;">
                        <option value="KTP">KTP</option>
                        <option value="SIM">SIM</option>
                        <option value="STNK">STNK</option>
                        <option value="Pasport">Pasport</option>
                    </select>
                   
                    <input name="v_NoIdentitas" id="v_NoIdentitas" size="30" value="" maxlength="30" onkeydown="changeCursor(event,'typesupp',this)" class="form-control" style="width: 70%;" type="text">
                </div>
            </div>
            
            
            <div class="form-group">
            <label class="col-sm-2 control-label"> Type Member</label>
            <div class="col-sm-5">
                <select class="form-control" id="typeleader" name="typeleader" onkeydown="changeCursor(event,'typesupp',this)" onchange="simpanKontak();">
                    <option value="">--Please Select--</option>
                    <?php 
                            foreach($arr_data["list_type_leader"] as $KdTypeTourLeader=>$val)                
                            {
                                $Nama = $arr_data["type_leader_KeteranganTourLeader"][$KdTypeTourLeader];
                                ?>
                                    <option value="<?php echo $KdTypeTourLeader; ?>"><?php echo $Nama; ?></option>            
                                <?php
                            }
                        ?>
                </select>

            </div>
        </div>
    
        <div class="form-group">
            <label class="col-sm-2 control-label"> Biro Member</label>
            <div class="col-sm-5">
                <select class="form-control" id="travelleader" name="travelleader" onkeydown="changeCursor(event,'typesupp',this)" onchange="simpanKontak();">
                    <option value="">--Please Select--</option>
                        <?php 
                            foreach($arr_data["list_travel"] as $KdTravel=>$val)                
                            {
                                $Nama = $arr_data["travel_Nama"][$KdTravel];
                                ?>
                                    <option value="<?php echo $KdTravel; ?>"><?php echo $Nama; ?></option>            
                                <?php
                            }
                        ?>
                    </select>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-5">
                <input name="Phone" id="Phone" size="20" value="" maxlength="20" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Fax</label>
            <div class="col-sm-5">
                <input name="Fax" id="Fax" size="20" value="" maxlength="20" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-5">
                <input name="Alamat" id="Alamat" size="40" value="" maxlength="30" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-5">
                <input name="Email" id="Email" size="25" value="" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Kota</label>
            <div class="col-sm-5">
                <input name="Kota" id="Kota" size="25" value="" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Telepon</label>
            <div class="col-sm-5">
                <input name="Telepon" id="Telepon" size="25" value="" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">No. Rekening</label>
            <div class="col-sm-5">
                <input name="NoRekening" id="NoRekening" size="25" value="" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Bank</label>
            <div class="col-sm-5">
                <input name="Bank" id="Bank" size="25" value="" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Nama Penerima</label>
            <div class="col-sm-5">
                <input name="Penerima" id="Penerima" size="25" value="" maxlength="25" onkeydown="changeCursor(event,'typesupp',this)" 1="" class="form-control" type="text">
            </div>
        </div>
    
        <div class="form-group">
            <label class="col-sm-2 control-label"> Aktif</label>
            <div class="col-sm-2">
                <select class="form-control" size="1" id="status" name="status" onkeydown="changeCursor(event,'typesupp',this)" onchange="simpanKontak();">
                        <option value="A">Aktif</option>
                        <option value="T">Tidak</option>
                </select>

            </div>
        </div>
                        
                          


                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/<?=$tr1."/".$tr2;?>/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="submit"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>