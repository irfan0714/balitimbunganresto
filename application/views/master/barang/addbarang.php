<?php
$this->load->view('header'); 

$gantikursor = "onkeydown=\"changeCursor(event,'barang',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/barang.js"></script>
<body onload="firstLoad('barang');">
<form method='post' name="barang" id="barang" action='<?=base_url();?>index.php/master/barang/save_new_barang'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b>Add Barang</b></legend>
			<table align = 'center' width="80%">
				<?php
				if($msg){ echo $msg;}
				$mylib = new globallib();
				echo $mylib->write_textbox("Kode Barang <font style='color: red;'>(*)</font>","kode",$id,"20","15","readonly='readonly'","text",$gantikursor."onKeyUp=\"javascript:dodacheck(document.getElementById('kode'));\"","5");
				//echo $mylib->write_textbox("Nama Lengkap*","nlengkap",$nlengkap,"80","75","","text",$gantikursor,"5");
				echo $mylib->write_textbox("Nama Barang <font style='color: red;'>(*)</font>","nstruk",$nstruk,"35","30","","text",$gantikursor,"5");
				
				?>
				<tr>
					<td nowrap>Nama Initial</td>
					<td nowrap>:</td>
					<td nowrap><input type="text" class="" name="nama_initial" id="nama_initial" value="" size="35" maxlength="20" <?php echo $gantikursor; ?>/></td>
				</tr>
				<?php
				
                $action = "onchange =\"getSubDiv('".base_url()."');\"";
                echo $mylib->write_combo("Divisi <font style='color: red;'>(*)</font>","divisi",$mdivisi,$divisi,"KdDivisi","NamaDivisi",$gantikursor,$action,"tidak");

                echo $mylib->write_plain_combo("Sub Divisi <font style='color: red;'>(*)</font>","subdivisi",$gantikursor,$subdiv,"","ya");

                //echo $mylib->write_combo("Kategori*","kategori",$mkategori,$kategori,"KdKategori","NamaKategori",$gantikursor,"","tidak");
                
                ?>
				<tr>
					<td nowrap>Kategori <font style="color: red;">(*)</font></td>
					<td nowrap>:</td>
					<td nowrap>
						<select size="1" id="kategori" name="kategori" <?php echo $gantikursor ?> onchange ="getSubKat('<?php echo base_url(); ?>')">
						<option value="">--Please Select--</option>
						<?php
						foreach($mkategori as $val)
						{
							?>
								<option value= "<?php echo $val["KdKategori"]; ?>"><?php echo $val["NamaKategori"]; ?></option>
							<?php
						}
						?>
						</select>
					</td>
					
					<td nowrap>Sub Kategori <font style="color: red;">(*)</font></td>
					<td nowrap>
						<select size="1" id="subkategori" name="subkategori" <?php echo $gantikursor ?>>
						<option value="">--Please Select--</option>
						</select>
					</td>
				</tr>
				
				
				<tr>
					<td nowrap>Brand <font style="color: red;">(*)</font></td>
					<td nowrap>:</td>
					<td nowrap>
						<select size="1" id="brand" name="brand" <?php echo $gantikursor ?> onchange ="getSubBrand('<?php echo base_url(); ?>')">
						<option value="">--Please Select--</option>
						<?php
						foreach($mbrand as $val)
						{
							?><option value= "<?php echo $val["KdBrand"]; ?>"><?php echo $val["NamaBrand"]; ?></option><?php
						}
						?>
						</select>
					</td>
					
					<td nowrap>Sub Brand <font style="color: red;">(*)</font></td>
					<td nowrap>
						<select size="1" id="subbrand" name="subbrand" <?php echo $gantikursor ?>>
						<option value="">--Please Select--</option>
						</select>
					</td>
				</tr>
                <?php
                
				//echo $mylib->write_combo("Brand*","brand",$mbrand,$brand,"KdBrand","NamaBrand",$gantikursor,"","tidak");
				
				echo $mylib->write_combo("Satuan <font style='color: red;'>(*)</font>","satuanst",$msatuan,$satuanst,"KdSatuan","NamaSatuan",$gantikursor,"","tidak");
				
				/*
				?>
				<tr>
				    <td><td>
					<td nowrap align="center">Satuan</td>
					<td nowrap align="center">Konversi</td>
					<td nowrap align="center">Harga Jual</td>
					<td nowrap align="center">Harga Beli</td>
				</tr>
				<?
				echo $mylib->write_combo("Satuan*","satuanbl",$msatuan,$satuanbl,"KdSatuan","NamaSatuan",$gantikursor,"","tidak");
				*/
				echo $mylib->write_number("Harga Jual <font style='color: red;'>(*)</font>","harga1c",$harga1c,"25","20","","text",$gantikursor,"1","");
				echo $mylib->write_number("Harga Beli <font style='color: red;'>(*)</font>","harga1b",$harga1b,"25","20","","text",$gantikursor,"1","");

				echo $mylib->write_number2("","konvblst",$konvblst,"6","4","","hidden",$gantikursor,"","5");
				
//				echo $mylib->write_combo("Satuan 1*","satuan1",$msatuan,$satuan1,"KdSatuan","NamaSatuan",$gantikursor,"","tidak");
//                $act1 = onkeydown=\"keyShortcut(event,'konv1st',this)\">";
//				echo $mylib->write_number2("Konversi 1","konv1st",$konv1st,"6","4","","text",$act1,"1","");
//				echo $mylib->write_number2("Jual","harga1c",$harga1c,"25","20","","text",$gantikursor,"1","");
//				echo $mylib->write_number2("Beli","harga1b",$harga1b,"25","20","","text",$gantikursor,"1","");
				echo $mylib->write_combo("Satuan 2","satuan2",$msatuan,$satuan2,"KdSatuan","NamaSatuan",$gantikursor,"","tidak");
                $act2 = "onkeydown=\"keyShortcut(event,'konv2st',this)\"";
				echo $mylib->write_number2("Konversi 2","konv2st",$konv2st,"6","4","","text",$act2,"","5");
				echo $mylib->write_number2("Jual","harga2c",$harga2c,"25","20","","text",$gantikursor,"1","");
				echo $mylib->write_number2("Beli","harga2b",$harga2b,"25","20","","text",$gantikursor,"1","");
				echo $mylib->write_combo("Satuan 3","satuan3",$msatuan,$satuan3,"KdSatuan","NamaSatuan",$gantikursor,"","tidak");
                $act3 = "onkeydown=\"keyShortcut(event,'konv3st',this)\"";
				echo $mylib->write_number2("Konversi 3","konv3st",$konv3st,"6","4","","text",$act3,"","5");
				echo $mylib->write_number2("Jual","harga3c",$harga3c,"25","20","","text",$gantikursor,"1","");
				echo $mylib->write_number2("Beli","harga3b",$harga3b,"25","20","","text",$gantikursor,"1","");

				echo $mylib->write_number("Persen Pajak","persenpajak",$persenpajak,"7","5","","text",$gantikursor,"1","");
				//echo $mylib->write_textbox("Barcode","barcodek",$barcodek,"20","20","","text",$gantikursor,"5");
				?>
				<!--
				<tr>
					<td nowrap>Jenis Barang*</td>
					<td nowrap>:</td>
					<td nowrap colspan="6">
						<select size="1" id="jenisbarang" name="jenisbarang" <?=$gantikursor;?>>
						<option value="">--Please Select--</option>
						<?php
						$nilaijenis = array_keys($mjenisbarang);
						for($a = 0;$a<count($mjenisbarang);$a++){
							$select = "";
							if($jenisbarang==$nilaijenis[$a]){
								$select = "selected";
							}
						?>
						<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenisbarang[$nilaijenis[$a]]?></option>
						<?php
						}
						?>
						</select>
					</td>
				</tr>
				<?php
				echo $mylib->write_combo("Parent Code","parents",$mparent,$parent,"PCode","NamaLengkap",$gantikursor,"","ya");
				?>
				-->
				<tr>
					<td nowrap>Tipe Stock</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="tipe" name="tipe" <?=$gantikursor;?>>
					<?php
					for($a = 0;$a<count($tipe);$a++){
						$select = "";
						if($niltipe==$tipe[$a]['KdTipeStock']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$tipe[$a]['KdTipeStock']?>"><?=stripslashes($tipe[$a]['NamaTipeStock'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				
				<tr>
					<td nowrap>Status</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="status" name="status" <?=$gantikursor;?>>
					<?php
					for($a = 0;$a<count($status);$a++){
						$select = "";
						if($nilstatus==$status[$a]['KdStatus']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$status[$a]['KdStatus']?>"><?=stripslashes($status[$a]['NamaStatus'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				
				<tr>
					<td nowrap>Kode Komisi</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="komisi" name="komisi" <?php echo $gantikursor ?>>
					<?php
					foreach($mkomisi as $val)
					{
						?>
							<option value= "<?php echo $val["Kd_Komisi"]; ?>"><?php echo $val["Nama_Komisi"]; ?></option>
						<?php
					}
					?>
					</select>
					</td>
				</tr>
				
				<?
                    //echo $mylib->write_combo("Kode Komisi","komisi",$mkomisi,$komisi,"Kd_Komisi","Nama_Komisi",$gantikursor,"","tidak");
                    echo $mylib->write_textbox("Service Charge %","service_charge",$service_charge,"35","30","","text",$gantikursor,"5");
				//echo $mylib->write_combo("Formula*","formula",$mformula,$formula,"KdFormula","NamaFormula",$gantikursor,"","tidak");
				?>
				<tr>
					<td nowrap colspan="4">
						<input type='hidden' name="flag" id="flag" value="add">
						<input type='hidden' name="base_url" id="base_url" value="<?=base_url()?>">
						<input type='button' value='Save' onclick="cekbarang();"/>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/barang/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>
