<?php $this->load->view('header'); 
$this->load->library('globallib');
$mylib = new globallib();?>
<head>
</head>
<script language="javascript" src="<?= base_url(); ?>public/js/ketentuan_diskon_v2.js"></script>


<form method="POST"  name="search" action='<?php echo base_url(); ?>index.php/master/ketentuan_diskon/search'>
    <input type="hidden" name="btn_search" id="btn_search" value="y"/>
    <div class="row">
        <div class="col-md-8">
            <b>Search</b>&nbsp;
            <input type="text" size="50" maxlength="30" name="search_keyword" id="search_keyword" class="form-control-new" value="<?php
            if ($search_keyword) {
                echo $search_keyword;
            }
            ?>" /> 
            &nbsp;
        </div>

        <div class="col-md-4" align="right">
            <button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
            <a href="<?php echo base_url() . "index.php/master/ketentuan_diskon/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
        </div>
    </div>
</form>

<hr/>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="150"><center>No. Transaksi</center></th>
		        <th width="100"><center>Tanggal</center></th>
		        <th><center>Keterangan</center></th>
		        <th width="200"><center>Periode</center></th>
		        <th width="100"><center>Pembelian Minimun</center></th>
		        <th width="100"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == 0) {
                    $echo_status = "<font style='color:#000000'><b>Pending</b></font>";
                } else if ($val["Status"] == 1) {
                    $echo_status = "<font style='color:#6bcf29'><b>Close</b></font>";
                } else if ($val["Status"] == 2) {
                    $echo_status = "<font style='color:#ff1c1c;'><b>Void</b></font>";
                }
                ?>
                <tr title="<?php echo $val["NoTrans"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["NoTrans"]; ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["TglTrans"]); ?></td>
                    <td align="left"><?php echo $val["Ketentuan"]; ?></td>
                    <td align="center"><?php echo $mylib->ubah_tanggal($val["TglAwal"])." s.d ".$mylib->ubah_tanggal($val["TglAkhir"]); ?></td>
                    <td align="right"><?php echo $val["Minimum"]; ?></td>
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                        if ($val["Status"] == 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/master/ketentuan_diskon/edit_data/<?php echo $val["NoTrans"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>

                            <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["NoTrans"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                            </button>
                            
                             <a href="<?php echo base_url(); ?>index.php/master/ketentuan_diskon/close_data/<?php echo $val["NoTrans"]; ?>"  class="btn btn-orange btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Close" title=""><i class="entypo-lock"></i></a>
                            <?php
                        }

                        if ($val["Status"] == 1) {
                            ?>
                            
                            <a href="<?php echo base_url(); ?>index.php/master/ketentuan_diskon/view_data/<?php echo $val["NoTrans"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
							
                            <?php
                        }
                        ?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
<script>
	function deleteTrans(nodok,url)
{
	var r=confirm("Apakah Anda Ingin Menghapus No Dokumen "+nodok+" ?")
	if (r==true)
	{
		window.location = url+"index.php/master/ketentuan_diskon/delete_trans/"+nodok;	
	}
	else
	{
  		return false;
	}
}
</script>
