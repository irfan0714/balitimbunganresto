<?php 

$this->load->view('header'); 

$modul = "Ketentuan Diskon";

?>

<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Add <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name="theform" id="theform" action='<?=base_url();?>index.php/master/ketentuan_diskon/save_data'>
		<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
	    <table class="table table-bordered responsive" border="1">                        

	        
	        <tr>
	            <td class="title_table" width="150">Periode <font color="red"><b>(*)</b></font></td>
	            <td> 
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_periode1" id="v_tgl_periode1" size="10" maxlength="10">
	            	&nbsp;s.d&nbsp;
	            	<input type="text" class="form-control-new datepicker" value="<?php echo date('d-m-Y'); ?>" name="v_tgl_periode2" id="v_tgl_periode2" size="10" maxlength="10">
	            </td>
	        </tr>
			
			<tr>
	            <td class="title_table">Diskon <font color="red"><b>(*)</b></font></td>
	            <td><input style="text-align: right" type="text" class="form-control-new" value="" name="v_diskon" id="v_diskon" maxlength="20" size="5">&nbsp;%</td>
	        </tr>
	        
	        <tr>
		        <td class="title_table">Minimum Pembelian</td>
		        <td>
		            <select class="form-control-new" name="v_min_pembelian" id="v_min_pembelian" style="width: 75px;" onchange="view_nim_pem()">
		            	<option value="T">Tidak</option>
		            	<option value="Y">Ya</option>
		            </select>
		            &nbsp;&nbsp;
		            <span id="mins_buy" style="display: none">
		            Rp. &nbsp;<input  style="text-align: right; width: 100px;" type="text" class="form-control-new" value="" name="minimal_pembelian" id="minimal_pembelian" maxlength="255" size="100">
		            </span>
		        </td>
		    </tr>
		    
	        <tr>
	            <td class="title_table">Keterangan <font color="red"><b>(*)</b></font></td>
	            <td><input type="text" class="form-control-new" value="" name="v_note" id="v_note" maxlength="255" size="100"></td>
	        </tr>
	        		    
		    <tr>
		        <td class="title_table">Pilihan Input</td>
		        <td>
		            <select class="form-control-new" name="v_pil_input" id="v_pil_input" style="width: 100px;" onchange="pilihan_input()">
		            	<option value="M">Manual</option>
		            	<option value="O">Otomatis</option>
		            </select>
		            &nbsp;&nbsp;
		            <span id="by" style="display: none">
		            <select class="form-control-new" name="v_pil_by" id="v_pil_by" style="width: 100px;" onchange="pilihan_by()">
		            	<option value="K">By Kategori</option>
		            	<option value="D">By Divisi</option>
		            </select>
		            </span>
		            &nbsp;&nbsp;
		     		<span id="by_kategori" style="display: none">
		            <select class="form-control-new" name="v_pil_by_kategori" id="v_pil_by_kategori" style="width: 100px;">
		            	<?php
	            		foreach($bykategori as $val)
	            		{
							?><option value="<?php echo $val["KdKategori"]; ?>"><?php echo $val["NamaKategori"]; ?></option><?php
						}
	            		?>
		            </select>
					</span>
		            &nbsp;&nbsp;
		       		<span id="by_divisi" style="display: none">
		            <select class="form-control-new" name="v_pil_by_divisi" id="v_pil_by_divisi" style="width: 100px;">
		            	<?php
	            		foreach($bydivisi as $val)
	            		{
							?><option value="<?php echo $val["KdDivisi"]; ?>"><?php echo $val["NamaDivisi"]; ?></option><?php
						}
	            		?>
		            </select>
		            </span>
		        </td>
		    </tr>
		    
		    <tr id="include" style="display: none">
	            <td class="title_table" >&nbsp;</td>
	            <td><input type="checkbox" name="excludepromo" id="excludepromo" value="Y">&nbsp;&nbsp;Exclude Promo Yang Sedang Berlangsung</td>
	        </tr>
           	        
	        <tr id="tabel" style="display: ">
	        	<td colspan="100%">
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>        
							    <th width="100"><center>
							    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
										<i class="entypo-plus"></i>
									</button></center>
							    </th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no=1; ?>
							
							<tr id="baris<?php echo $no; ?>">
				                <td>
				                	<nobr>
				                	<input type="text" class="form-control-new" name="pcode[]" id="pcode<?php echo $no;?>" value="" style="width: 100px;"/>
				                	<a href="javascript:void(0)" id="get_pcode<?php echo $no;?>" onclick="pickThis(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
				                	</nobr>
				                </td>
				                <td>
				                	<input type="text" class="form-control-new" name="v_namabarang[]" id="v_namabarang<?php echo $no;?>" value="" style="width: 100%;"/>
				                </td>				                
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
				            </tr>
							
						</tbody>
					</table>
	        	</td>
	        </tr>
	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="add">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <button type="button" class="btn btn-danger btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/master/ketentuan_diskon/"; ?>">Batal<i class="entypo-cancel"></i></button>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
				</td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
        <font style="color: red; font-style: italic; font-weight: bold;">(*) Harus diisi.</font>
        
	</div>
</div>
    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script>
	function pilihan_input(){
		pil = $('#v_pil_input').val();
		if(pil=="O"){
		   $("#by").css("display","");
		   $("#by_kategori").css("display","");
		   $("#include").css("display","");
		   $("#tabel").css("display","none");
		}else{
			$("#by").css("display","none");
			$("#by_kategori").css("display","none");
			$("#by_divisi").css("display","none");
			$("#include").css("display","none");
			$("#tabel").css("display","");
		}
	}
	
	function pilihan_by(){
		pil = $('#v_pil_by').val();
		if(pil=="K"){
		   $("#by_kategori").css("display","");
		   $("#by_divisi").css("display","none");
		}else{
			 $("#by_kategori").css("display","none");
		   $("#by_divisi").css("display","");
		}
	}
	
	function pickThis(obj)
	{
		base_url = $("#base_url").val();

		    objek = obj.id;
			id = parseFloat(objek.substr(9,objek.length-9));
			url = base_url+"index.php/pop/pop_up_masterbarang/index/"+id+"/";
			windowOpener(550, 650, 'Cari PCode', url, 'Cari PCode')
		
	}
	
	function detailNew()
	{
		var clonedRow = $("#TabelDetail tr:last").clone(true);
		var intCurrentRowId = parseFloat($('#TabelDetail tr').length )-2;
		nama = document.getElementsByName("pcode[]");
		temp = nama[intCurrentRowId].id;
		intCurrentRowId = temp.substr(5,temp.length-5);
		var intNewRowId = parseFloat(intCurrentRowId) + 1;
		$("#pcode" + intCurrentRowId , clonedRow ).attr( { "id" : "pcode" + intNewRowId,"value" : ""} );
		$("#get_pcode" + intCurrentRowId , clonedRow ).attr( { "id" : "get_pcode" + intNewRowId} );
		$("#v_namabarang" + intCurrentRowId , clonedRow ).attr( { "id" : "v_namabarang" + intNewRowId,"value" : ""} );
		$("#btn_del_detail_" + intCurrentRowId , clonedRow ).attr( { "id" : "btn_del_detail_" + intNewRowId} );
		$("#TabelDetail").append(clonedRow);
		$("#TabelDetail tr:last" ).attr( "id", "baris" +intNewRowId ); // change id of last row
		$("#pcode" + intNewRowId).focus();
		ClearBaris(intNewRowId);
	}

	function ClearBaris(id)
	{
		$("#pcode"+id).val("");
		$("#v_namabarang"+id).val("");
	}
	
	function deleteRow(obj)
	{
		objek = obj.id;
		id = objek.substr(15,objek.length-3);
		
		var lastRow = document.getElementsByName("pcode[]").length;
		
		if( lastRow > 1)
		{
			$('#baris'+id).remove();
		}else{
				alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
		}
	}
	
	function cekTheform()
	{
		
	    if(document.getElementById("v_diskon").value=="")
	    {
	        alert("Diskon Harus Diisi.");
	        document.getElementById("v_diskon").focus();
	        return false;
	    }
		else if(document.getElementById("v_note").value=="")
	    {
	        alert("Keterangan Harus Diisi.");
	        document.getElementById("v_note").focus();
	        return false;
	    }
	    else
	    {
		
	    	var yesSubmit = true;
	    	
	    }
	        
	        if(yesSubmit)
	        {
				document.getElementById("theform").submit();	
			}  
		
	}
	
	function view_nim_pem(){
		pil = $('#v_min_pembelian').val();
		if(pil=="Y"){
			$("#mins_buy").css("display","");
			$("#minimal_pembelian").focus();
		}else{
			$("#mins_buy").css("display","none");
		}
	}
</script>