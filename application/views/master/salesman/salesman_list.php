<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table >
	<tr>
		<td><input class="form-control-new" type='text' size'20' name='stSearchingKey' id='stSearchingKey'></td>
		<td>&nbsp;&nbsp;
			<select class="form-control-new" size="1" height="1" name ="searchby" id ="searchby">
				<option value="KdSalesman">Kode Salesman</option>
				<option value="NamaSalesman">Nama Salesman</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>Kode Sales</center></th>
		        <th><center>Nama Sales</center></th>
		        <th width="150"><center>Kode Supervisor</center></th>
		        <th><center>Nama Supervisor</center></th>
		        <th width="50"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($salesman_data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($salesman_data as $val) {
                
                ?>
                <tr title="<?php echo $val["KdSalesman"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["KdSalesman"]; ?></td>
                    <td align="center"><?php echo $val["NamaSalesman"]; ?></td>
                    <td align="left"><?php echo $val["KdSupervisor"]; ?></td>
                    <td align="left"><?php echo $val["NamaSupervisor"]; ?></td>
                    <td align="center"><?php echo $val["Status"]; ?></td>
                    <td align="center">
                            <a href="<?php echo base_url(); ?>index.php/master/salesman/view_salesman/<?php echo $val["KdSalesman"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/master/salesman/edit_salesman/<?php echo $val["KdSalesman"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/master/salesman/delete_salesman/<?php echo $val["KdSalesman"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title=""><i class="entypo-trash"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["KdSalesman"]; ?>', '<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>-->
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<!--<table align = 'center' border='1' class='table_class_list'>
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php } ?>
		<th>Kode Salesman</th>
		<th>Nama Salesman</th>
		<th>Nama Personal</th>
		<th>Nama Tipe Salesman</th>
		<th>Gudang</th>
	</tr>
<?php
	if(count($salesman_data)==0)
	{ 
?>
	<td nowrap colspan="4" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($salesman_data);$a++)
{
?>
	<tr>
<?php
	if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
	{
?>
			<td nowrap>
		<?php
			if($link->view=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/salesman/view_salesman/<?=$salesman_data[$a]['KdSalesman'];?>"><img src='<?=base_url();?>public/images/zoom.png' border = '0' title = 'View'/></a>
		<?php
			}
			if($link->edit=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/salesman/edit_salesman/<?=$salesman_data[$a]['KdSalesman'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php
			}
			if($link->delete=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/salesman/delete_salesman/<?=$salesman_data[$a]['KdSalesman'];?>"><img src='<?=base_url();?>public/images/cancel.png' border = '0' title = 'Delete'/></a>
		<?php
			}
		?>
		</td>
<?php } ?>
		<td nowrap><?=stripslashes($salesman_data[$a]['KdSalesman']);?></td>
		<td nowrap><?=stripslashes($salesman_data[$a]['NamaSalesman']);?></td>
		<td nowrap><?=stripslashes($salesman_data[$a]['NamaPersonal']);?></td>
		<td nowrap><?=stripslashes($salesman_data[$a]['NamaTipeSalesman']);?></td>
		<td nowrap><?=stripslashes($salesman_data[$a]['KdGudang']);?></td>
	<tr>
<?php
}
?>
</table>-->
<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/master/salesman/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>