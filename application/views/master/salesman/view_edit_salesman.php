<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'salesman',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('salesman')">
<form method='post' name="salesman" id="salesman" action='<?=base_url();?>index.php/master/salesman/save_salesman'>
<table  class="table table-bordered responsive">
	<tr>
		<td>
		
		<legend><b><?=$fieldset?> Salesman</b></legend>
			<table  class="table table-bordered responsive">
			<tr>
				<td nowrap>Kode</td>
				<td nowrap><?=stripslashes($view_salesman->KdSalesman);?>
				<input class="form-control-new" type='hidden' maxlength="4" size="5" readonly name='kode' id='kode' value="<?=stripslashes($view_salesman->KdSalesman);?>" /></td>
			</tr>
			<tr>
				<td nowrap>Nama</td>
				<td nowrap><input class="form-control-new" type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($view_salesman->NamaSalesman);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
					<td nowrap>Supervisor</td>
					<td nowrap>
					<select class="form-control-new" size="1" id="supervisor" name="supervisor" <?=$gantikursor;?>>
					<?php
					for($a = 0;$a<count($supervisor);$a++){
						$select = "";
						if($view_salesman->KdSupervisor==$supervisor[$a]['KdSupervisor']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$supervisor[$a]['KdSupervisor']?>"><?=stripslashes($supervisor[$a]['NamaSupervisor'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>
		            	<select class="form-control-new" name="status" id="status" style="width: 200px;">
		            		<option <?php if($view_salesman->Status=='A'){ echo "selected='selected'"; } ?> value="A">Aktif</option>
		            		<option <?php if($view_salesman->Status=='T'){ echo "selected='selected'"; } ?> value="T">Non Aktif</option>
		            	</select>
		            </td>
				</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input  class="btn btn-info btn-sm sm-new tooltip-primary" type='button' value='Save' onclick="cekMaster2('kode','nama','salesman','Kode Salesman','Nama Salesman');"/>
				<?php } ?>
					<input  class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/salesman/" />
				</td>
			</tr>
			</table>

	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>