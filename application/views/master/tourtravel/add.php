<?php
    $this->load->view('header');
    $gantikursor = "onkeydown=\"changeCursor(event,'typesupp',this)\"";
?>
<body>
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$label?>
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="matauang" id="matauang" action='<?= base_url(); ?>index.php/<?=$tr1."/".$tr2;?>/save' class="form-horizontal form-groups-bordered" onsubmit="cek_form()">

                        <?php if (!empty($msg)) echo $msg ; ?>

                    <?php
                    $mylib = new globallib();
//                    print_r($TypeTour);
                    echo $mylib->text_boostrap("Kode","Kode",$Kode,"10","10","readonly='readonly'","text",$gantikursor,"1");
                    echo $mylib->option_boostrap("Type Tour", "type", $TypeTour, "", "KdTypeTour", "KeteranganTour", $gantikursor, "onchange=\"simpanKontak();\"");
                        echo $mylib->text_boostrap("Nama","Nama",$Nama,"30","30","","text",$gantikursor,"1");
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size='2'>Contact Person boleh lebih 1. Exmp : Mr. Smith, Mrs. Brown, Etc.</font>";
                        echo $mylib->text_boostrap("Contact Person","Contact",$Contact,"20","20","","text",$gantikursor,"1");
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size='2'>Phone boleh lebih 1. Exmp : 021848373, 08987654321, Etc.</font>";
                        echo $mylib->text_boostrap("Phone","Phone",$Phone,"20","20","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Fax","Fax",$Fax,"20","20","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Alamat","Alamat",$Alamat,"100","100","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Email","Email",$Email,"25","25","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Kota","Kota",$Kota,"25","25","","text",$gantikursor,"1");
                        echo $mylib->text_boostrap("Telepon","Telepon",$Telepon,"25","25","","text",$gantikursor,"1");
                        echo $mylib->option_boostrap("Aktif", "status", $Aktif, "", "Kd", "Ket", $gantikursor, "onchange=\"simpanKontak();\"");

                    ?>


                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <input type="hidden" id="base_url" name="base_url" value="<?= base_url(); ?>" />
                            <a class="btn btn-default" href="<?= base_url(); ?>index.php/<?=$tr1."/".$tr2;?>/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-primary" type="button" onclick="cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>
<script>
	function cek_form(){
		
	
    	var url = $("#base_url").val();
    	Phone = $('#Phone').val();
    	
    	if(Phone==""){
    		alert("Isi Phone ...");
			$('#Phone').focus();
			return false;
		}else{
		
			$.ajax({
					url: url+"index.php/master/biro_tour_travel/cek_double/",
					data: {tlp:Phone},
					async:false,
					type: "POST",
					dataType: 'json',					
					success: function(res)
					{
						if(res.cek){
							alert("Biro Tour Travel Ini Sudah Ada Didalam Database. Silahkan Klik Tombol Back.");
							return false;
						}else{
							document.getElementById("matauang").submit();	
						}
					},
					error: function(e) 
					{
						alert(e);
					} 
			});
		}
    	 
	}
</script>