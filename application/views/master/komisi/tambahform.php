<?php
    $this->load->view('header');	
	$this->load->view('js/TextValidation');
	$this->load->view('js/SelectValidation');
?>
<style>
.widthonehundred{
	width:150px;
}
.widthtwohundred{
	width:220px;
}
.RTL {
	text-align:right;
}

</style>
<script>
	$(document).ready(function(){
		addRowToTable(0);
	});
	
	var start_i = 1;
	
	function addRowToTable(trid) {
		var i = this.start_i++;
		
		$('#tr'+trid).after("<tr id='tr"+i+"'></tr>");
		if(i < 2){
			var kurangbutton = "&nbsp;";
		}
		else {				
			var kurangbutton = "<a onclick='removeRowFromTable("+i+")'> - </a>";
		}

		var selectpcode 	= '<?=form_dropdownDB_initJS('PCode[]', $masterbarang, 'PCode', 'NamaLengkap', '', '', '------------ Pilih --------', 'id="PCode\'+i+\'" onchange="isUnique(\'+i+\')"; class="widthtwohundred"');?>';
		$('#tr'+i).append("<td>"+selectpcode+"</td>");

		var inputkomisi1 = "<input type='text' name='Komisi1[]' id='Komisi1"+i+"' value='0' size='10' class='RTL form-control-new' onKeyUp='getTotal("+i+")'>"
		$('#tr'+i).append("<td align='right'>"+ inputkomisi1 +"</td>");
		
		var inputkomisi4 = "<input type='text' name='Komisi2[]' id='Komisi2"+i+"' value='0' size='10' class='RTL form-control-new' onKeyUp='getTotal("+i+")'>"
		$('#tr'+i).append("<td align='right'>"+ inputkomisi4 +"</td>");
		
		var inputkomisi4 = "<input type='text' name='Komisi3[]' id='Komisi3"+i+"' value='0' size='10' class='RTL form-control-new' onKeyUp='getTotal("+i+")'>"
		$('#tr'+i).append("<td align='right'>"+ inputkomisi4 +"</td>");
		
		var inputkomisi4 = "<input type='text' name='Komisi4[]' id='Komisi4"+i+"' value='0' size='10' class='RTL form-control-new' onKeyUp='getTotal("+i+")'>"
		$('#tr'+i).append("<td align='right'>"+ inputkomisi4 +"</td>");
		
		var inputkomisi4b = "<input type='text' name='Komisi4bag[]' id='Komisi4bag"+i+"' value='0' size='10' class='RTL form-control-new' onKeyUp='getTotal("+i+")'>"
		$('#tr'+i).append("<td align='right'>"+ inputkomisi4b +"</td>");
		
		var totalsamping = "<span id='totalsamping"+i+"'>0&nbsp;</span>"
		$('#tr'+i).append("<td align='right'>"+ totalsamping +"</td>");		

		$('#tr'+i).append("<td align='center'>"+ kurangbutton +"</td>");
		
		var tambahbutton = "<a onclick='addRowToTable("+i+")'> + </a>";
		$('#tr'+i).append("<td align='center'>"+ tambahbutton +"</td>");
		
		new Spry.Widget.ValidationSelect("PCode"+i);
		new Spry.Widget.ValidationTextField("Komisi1"+i, "integer", {useCharacterMasking:true});
		new Spry.Widget.ValidationTextField("Komisi2"+i, "integer", {useCharacterMasking:true});
		new Spry.Widget.ValidationTextField("Komisi3"+i, "integer", {useCharacterMasking:true});
		new Spry.Widget.ValidationTextField("Komisi4"+i, "integer", {useCharacterMasking:true});
		new Spry.Widget.ValidationTextField("Komisi4bag"+i, "integer", {useCharacterMasking:true});
		
		$('#Komisi1'+i).focus();
	}
	
	function removeRowFromTable(n) {
		Spry.Widget.Utils.destroyWidgets('PCode' + n);
		Spry.Widget.Utils.destroyWidgets('Komisi1' + n);
		Spry.Widget.Utils.destroyWidgets('Komisi2' + n);
		Spry.Widget.Utils.destroyWidgets('Komisi3' + n);
		Spry.Widget.Utils.destroyWidgets('Komisi4' + n);
		Spry.Widget.Utils.destroyWidgets('Komisi4bag' + n);
		$('#tr'+n).remove();
	}
	
	function getTotal(i) {
		var kom1	= $('#Komisi1'+i).val();
		var kom2	= $('#Komisi2'+i).val();
		var kom3	= $('#Komisi3'+i).val();
		var kom4	= $('#Komisi4'+i).val();
		var kom4b	= $('#Komisi4bag'+i).val();
		
		var totalsam	= Number(kom1) + Number(kom2) + Number(kom3) + Number(kom4) + Number(kom4b);
		$('#totalsamping'+i).html(totalsam);
	}
	
	
	Array.prototype.indexOf = function( v, b, s ) {
		for( var i = +b || 0, l = this.length; i < l; i++ ) {
			if( this[i]===v || s && this[i]==v ) { return i; }
		}
		return -1;
	};

	Array.prototype.unique = function(b) {
		var a = [], i, len = this.length;
		for(i=0; i<len; i++ ) {
			if( a.indexOf( this[i], 0, b ) < 0 ) {
				a.push( this[i] );
			}
		}
		return a;
	};

	function isUnique(n){
		var item = document.getElementsByName('PCode[]');
		var originalArray = [];
		for(i=0 ; i< item.length; i++){
			originalArray[i] = item[i].value;
		}
		var uniqueArray = originalArray.unique();

		if(uniqueArray.length == originalArray.length) {
			$('#dataunique').val(1);
		}
		else{
			var itemname = $('#PCode'+n+' option:selected').text();
			var itemid =  $('#PCode'+n+' option:selected').val();
			$('#PCode'+n+' :selected').removeAttr("selected");
			$('#PCode'+n).val('');
			$('#dataunique').val('');
			alert(itemname+" sudah dipilih, \ntidak boleh ada yg sama dalam satu Travel");
			$("#PCode"+n).focus();
		}
	}
</script>
<body>
	<form action='<?=base_url()?>index.php/komisi/komisiagent/tambahproses' method='post' id='form1' name='form1'>
		<input type='hidden' value='' name='dataunique' id='dataunique'>
	<div class="row">
		<div class="col-md-1">
			Travel
		</div>
		<div class="col-md-2">
			<?=form_dropdownDB_init('KdTravel', $mastertravel, 'KdTravel', 'Nama', '', '', '------------ Pilih --------', 'id="KdTravel" class="widthonehundred" class="form-control-new"');?>
		</div>
		<div class="col-md-1">
			Keterangan
		</div>
		<div class="col-md-2">
			<input type="text" name="Keterangan" id="Keterangan" value="" class="form-control-new" />
		</div>
		<div class="col-md-6">
			&nbsp;
		</div>
	</div>
		<br/>
	<div class="row">
		<div class="col-md-1">
			Komisi I
		</div>
		<div class="col-md-2">
			<select name="Komisi1H" id="Komisi1H" />
				<option value="">-- Pilih Jenis Komisi --</option>
				<option value="OFFICE">OFFICE</option>
				<option value="GUIDE">GUIDE</option>
				<option value="TOUR LEADER">TOUR LEADER</option>
				<option value="DRIVER">DRIVER</option>
			</select>
		</div>
		<div class="col-md-1">
			Komisi II
		</div>
		<div class="col-md-2">
			<select name="Komisi2H" id="Komisi2H" />
				<option value="">-- Pilih Jenis Komisi --</option>					
				<option value="GUIDE">GUIDE</option>
				<option value="TOUR LEADER">TOUR LEADER</option>
				<option value="DRIVER">DRIVER</option>
				<option value="OFFICE">OFFICE</option>
			</select>
		</div>
		<div class="col-md-6">
			&nbsp;
		</div>
	</div>
		<br/>
	<div class="row">	
		<div class="col-md-1">
			Komisi III
		</div>
		<div class="col-md-2">
			<select name="Komisi3H" id="Komisi3H" />
				<option value="">-- Pilih Jenis Komisi --</option>					
				<option value="TOUR LEADER">TOUR LEADER</option>
				<option value="DRIVER">DRIVER</option>
				<option value="OFFICE">OFFICE</option>
				<option value="GUIDE">GUIDE</option>
			</select>
		</div>
		<div class="col-md-1">
			Komisi IV
		</div>
		<div class="col-md-2">
			<select name="Komisi4H" id="Komisi4H" />
				<option value="">-- Pilih Jenis Komisi --</option>					
				<option value="DRIVER">DRIVER</option>
				<option value="OFFICE">OFFICE</option>
				<option value="GUIDE">GUIDE</option>
				<option value="TOUR LEADER">TOUR LEADER</option>
			</select>
		</div>
		<div class="col-md-6">
			&nbsp;
		</div>
	</div>
		<br/>
	<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
	<table class="table table-bordered responsive">
		<thead class="title_table">
			<tr id="tr0">
				<th>Barang</th>
				<th>Komisi I</th>
				<th>Komisi II</th>
				<th>Komisi III</th>
				<th>Komisi IV</th>
				<th>Komisi IVbag</th>
				<th>Total</th>
				<th colspan="2">&nbsp;</th>
			</tr>	
		</thead>
		<tbody></tbody>
			<tr>
				<td align='center' colspan='9'>
					<input type="submit" name='submit' value="Simpan" class='button'>
				</td>
			</tr>
	</table>
	</div>
	</form>	
</div>
	<script>
		new Spry.Widget.ValidationTextField("KdTravel", "none");
		new Spry.Widget.ValidationTextField("Keterangan", "none");
		new Spry.Widget.ValidationSelect("Komisi1H");
		new Spry.Widget.ValidationTextField("dataunique");
	</script>
</body>

<?php $this->load->view('footer')?>