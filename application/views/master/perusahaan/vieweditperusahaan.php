<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'perusahaan',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript">
function cekperusahaan()
{
	if(cekoption("kode","Memasukkan Kode"))
	if(cekoption("nama","Memasukkan Nama"))
	if(cekoption("alm1","Memasukkan Alamat 1"))
	if(cekoption("alm2","Memasukkan Alamat 2"))
	if(cekoption("kota","Memasukkan Kota"))
	if(cekoption("telp","Memasukkan No Telp"))
	if(cekoption("npwp","Memasukkan NPWP"))
		$("#perusahaan").submit();
}
</script>
<body onload="firstLoad('perusahaan');">
<form method='post' name="perusahaan" id="perusahaan" action='<?=base_url();?>index.php/master/perusahaan/save_perusahaan'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Perusahaan</b></legend>
			<table align = 'center'>
		<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Kode","kode",stripslashes($viewperusahaan->KdPerusahaan),"8","6","readonly","text",$gantikursor,"1");
			echo $mylib->write_textbox("Nama","nama",stripslashes($viewperusahaan->Nama),"55","50","","text",$gantikursor,"1");
			echo $mylib->write_textbox("Alamat 1","alm1",stripslashes($viewperusahaan->Alamat1),"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox("Alamat 2","alm2",stripslashes($viewperusahaan->Alamat2),"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox("Kota","kota",stripslashes($viewperusahaan->Kota),"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox("No. Telp","telp",$viewperusahaan->Telepon,"25","20","","text",$gantikursor,"1");
			echo $mylib->write_textbox("NPWP","npwp",$viewperusahaan->NPWP,"25","20","","text",$gantikursor,"1");
		?>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input type='button' value='Save' onclick="cekperusahaan();"/>
				<?php } ?>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/perusahaan/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>