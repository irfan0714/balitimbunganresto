<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'saldoawal',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('saldoawal')">
<form method='post' name="saldoawal" id="saldoawal" action='<?=base_url();?>index.php/master/saldoawal/save_new_saldo'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b>Saldo Awal GL  <?=substr($tahunbulan,-2)." - ".substr($tahunbulan,0,4)?></b></legend>
			<table align = 'center'>
				<?php
				$mylib = new globallib();
				if($msg){ echo $msg;}?>	
				
				<tr>
					<td nowrap>Departemen</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="departemen" name="departemen" <?=$gantikursor;?>>
					<?php
					for($a = 0;$a<count($departemen);$a++){
						$select = "";
						if($nildepartemen==$departemen[$a]['KdDepartemen']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$departemen[$a]['KdDepartemen']?>"><?=stripslashes($departemen[$a]['NamaDepartemen'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
			
				<tr>
					<td nowrap>Rekening</td>
					<td nowrap>:</td>
					<td nowrap>
					<select size="1" id="rekening" name="rekening" <?=$gantikursor;?>>
					<option value="">--Please Select--</option>
					<?php
					for($a = 0;$a<count($rekening);$a++){
						$select = "";
						if($nilrekening==$rekening[$a]['kdrekening']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$rekening[$a]['kdrekening']?>"><?=stripslashes($rekening[$a]['namarekening'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				
				<?php echo $mylib->write_number("Saldo Awal","nilaisaldoawal","0","25","20","","text",$gantikursor,"1",""); ?>
			
				<tr>
					<td nowrap colspan="3">
						<input type='button' value='Save' onclick="cekMaster3('departemen','rekening','nilaisaldoawal','saldoawal','Departemen','Kode Rekening','Saldo Awal');"/>
						<input type="button" value="Back" onclick=parent.location="<?=base_url();?>index.php/master/saldoawal/" />
						<input type='hidden' id="tahunbulan" name="tahunbulan" value='<?=$tahunbulan?>'>
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>