<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<table align='center'>
	<tr>
		<td><input type='text' size'20' name='stSearchingKey' id='stSearchingKey'></td>
		<td>
			<select size="1" height="1" name ="searchby" id ="searchby">
				<option value="KdRekening">Kode Rekening</option>
				<option value="NamaRekening">Nama Rekening</option>
			</select>
		</td>
		<td><input type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>

<br>

<table align = 'center' border='1' class='table_class_list'>
	<tr>
	<?php
		if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
		{
		?>
		<th></th>
	<?php } ?>
		<th>Departemen</th>
		<th>Rekening</th>
		<th>Nama Rekening</th>
		<th>Saldo Awal</th>
	</tr>
<?php
	if(count($saldoawal_data)==0)
	{ 
?>
	<td nowrap colspan="6" align="center">Tidak Ada Data</td>
<?php		
	}
for($a = 0;$a<count($saldoawal_data);$a++)
{
?>
	<tr>
<?php
	if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
	{
?>
		<td nowrap>
		<?php
			if($link->view=="Y")
			{
		?>
		<a  href="<?=base_url();?>index.php/master/saldoawal/view_saldoawal/<?=$saldoawal_data[$a]['Departemen'];?>/<?=$saldoawal_data[$a]['KdRekening'];?>"><img src='<?=base_url();?>public/images/zoom.png' border = '0' title = 'View'/></a>
		<?php
			}
			if($link->edit=="Y")
			{
		?>
		<a 	href="<?=base_url();?>index.php/master/saldoawal/edit_saldoawal/<?=$saldoawal_data[$a]['Departemen'];?>/<?=$saldoawal_data[$a]['KdRekening'];?>"><img src='<?=base_url();?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
		<?php } ?>
		</td>
		<?php } ?>
		<td nowrap><?=stripslashes($saldoawal_data[$a]['Departemen']);?></td>
		<td nowrap><?=stripslashes($saldoawal_data[$a]['KdRekening']);?></td>
		<td nowrap><?=stripslashes($saldoawal_data[$a]['NamaRekening']);?></td>
		<td nowrap class="InputAlignRight"><?=number_format($saldoawal_data[$a]['SaldoAwal'], 2, ',', '.');?></td>
	<tr>
<?php
}
?>
</table>
<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
	<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/master/saldoawal/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>