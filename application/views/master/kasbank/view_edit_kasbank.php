<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'kasbank',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('kasbank')">
<form method='post' name="kasbank" id="kasbank" action='<?=base_url();?>index.php/master/kasbank/save_kasbank'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Kas Bank</b></legend>
			<table align = 'center'>
			<tr>
				<td nowrap>Kode</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="4" size="5" readonly name='kode' id='kode' value="<?=stripslashes($view_kasbank->KdKasBank);?>" /></td>
			</tr>
			<tr>
				<td nowrap>Nama</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($view_kasbank->NamaKasBank);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="jenistr" name="jenistr" <?=$gantikursor;?>>
					<option value="">--Please Select--</option>
					<?php
					$jenis = $view_kasbank->Jenis;
					$nilaijenis = array_keys($mjenis);
					for($a = 0;$a<count($mjenis);$a++){
					 	$select = "";
					 	if($jenis==$nilaijenis[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenis[$nilaijenis[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Rekening</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" id="tipe" name="tipe" <?=$gantikursor;?>>
				<?php
				for($a = 0;$a<count($tipe);$a++){
					$select = "";
					if($view_kasbank->KdRekening==$tipe[$a]['KdRekening']){
						$select = "selected";
					}
				?>
				<option <?=$select;?> value= "<?=stripslashes($tipe[$a]['KdRekening'])?>"><?=stripslashes($tipe[$a]['NamaRekening'])?></option>
				<?php
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input type='button' value='Save' onclick="cekMaster2('kode','nama','kasbank','Kode Kas Bank','Nama Kas Bank');"/>
				<?php } ?>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/kasbank/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>