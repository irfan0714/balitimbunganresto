<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'discount',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/discount.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>public/css/ui.datepicker.css" />
<body onload="firstLoad('discount');loadDate('<?=base_url();?>');">
<form method='post' name="discount" id="discount" action='<?=base_url();?>index.php/master/discount/save_new_discount'>
<table>
	<tr>
		<td>
		<input type="hidden" id="kode" name="kode" value="<?=$id;?>">
		<input type="hidden" id="tempnama" name="tempnama">
		<fieldset>
		<legend>Header</legend>
		<table>
			<?php
			if($msg){ echo $msg;}
			$mylib = new globallib();
			echo $mylib->write_textbox("Nama Discount","nama",$nama,"20","15","","text",$gantikursor,"6");
		?>
			<tr>
				<td nowrap>Jenis</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="jenis" name="jenis" <?=$gantikursor;?> onchange="changeJenis();setBeban();">
					<option value="">--Please Select--</option>
					<?php
					$nilaijenis = array_keys($mjenis);
					for($a = 0;$a<count($mjenis);$a++){
					 	$select = "";
					 	if($jenis==$nilaijenis[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaijenis[$a]?>"><?=$mjenis[$nilaijenis[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Rupiah / Barang</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="rup" name="rup" <?=$gantikursor;?> onchange="perhitungan();setBeban();hadiahbonus();">
					<option value="">--Please Select--</option>
					<?php
					$nilairup = array_keys($mrup);
					for($a = 0;$a<count($mrup);$a++){
					 	$select = "";
					 	if($rup==$nilairup[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilairup[$a]?>"><?=$mrup[$nilairup[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Perhitungan</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="hitung" name="hitung" <?=$gantikursor;?>>
					<option value="">--Please Select--</option>
					<?php
					$nilaihitung = array_keys($mhitung);
					for($a = 0;$a<count($mhitung);$a++){
					 	$select = "";
					 	if($hitung==$nilaihitung[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaihitung[$a]?>"><?=$mhitung[$nilaihitung[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>

			<tr>
				<td nowrap>Periode</td>
				<td nowrap>:</td>
				<td nowrap width="10%"><input type="text" readonly id="periode1" name="periode1" size="15" value="<?=$period1;?>">&nbsp - &nbsp</td>

				<td nowrap><input type="text" readonly id="periode2" name="periode2" size="15" value="<?=$period2;?>"></td>
			</tr>
			<?php
				for($s=1;$s<4;$s++){
					$namabeban  = "beban".$s;
					$namarek    = "rek".$s;
					$namapersen = "persen".$s;
				?>
			<tr>
				<td nowrap>Beban <?=$s;?></td>
				<td nowrap>:</td>
				<td nowrap>
					<select size="1" id="<?=$namabeban;?>" name="<?=$namabeban;?>" <?=$gantikursor;?>>
					<option value="">--Please Select--</option>
					<?php
					$nilaibeban = array_keys($mbeban);
					for($a = 0;$a<count($mrup);$a++){
					 	$select = "";
					 	if($beban[$s-1]==$nilaibeban[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaibeban[$a]?>"><?=$mbeban[$nilaibeban[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
				<td nowrap>
					<select size="1" id="<?=$namarek;?>" name="<?=$namarek;?>" <?=$gantikursor;?>>
					<option value="">--Please Select--</option>
					<?php
					for($a = 0;$a<count($mrek);$a++){
					 	$select = "";
					 	if($rek[$s-1]==$mrek[$a]['KdRekening']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$mrek[$a]['KdRekening']?>"><?=$mrek[$a]['Keterangan']?></option>
					<?php
					}
					?>
					</select>
				</td>
				<td nowrap>
					<input type='text' value="<?=$persen[$s-1];?>" size="5" id="<?=$namapersen;?>" name="<?=$namapersen;?>"/>&nbsp; %
				</td>
			</tr>
		<?php } ?>
			<tr>
				<td nowrap>Jumlah Diskon</td>
				<td nowrap>:</td>
				<td nowrap colspan="6"
				<input type="text" id="nilai" name="nilai" size="10" maxlength="9" value="<?=$nilai;?>" <?=$gantikursor;?>>
				<input type="text" value="<?=$satuan;?>" readonly id="satuan" name="satuan" size="10"></td>
			</tr>
			<tr>
				<td nowrap>Footer / Detail</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="footerdetail" name="footerdetail" <?=$gantikursor;?> >
					<option value="">--Please Select--</option>
					<?php
					$afooterdetail = array_keys($mfooterdetail);
					for($a = 0;$a<count($mfooterdetail);$a++){
					 	$select = "";
					 	if($footerdetail==$afooterdetail[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$afooterdetail[$a]?>"><?=$mfooterdetail[$afooterdetail[$a]]?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Hadiah Barang</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="bonus" name="bonus" disabled <?=$gantikursor;?> onchange="ambilHadiah();">
					<option value="">--Please Select--</option>
					<?php
					$nilaihadiah = array_keys($mhadiah);
					for($a = 0;$a<count($mhadiah);$a++){
					 	$select = "";
					 	if($hadiah==$nilaihadiah[$a]){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$nilaihadiah[$a]?>"><?=$mhadiah[$nilaihadiah[$a]]?></option>
					<?php
					}
					?>
					</select>
					<input type="text" id="brghadiah" readonly name="brghadiah" size="20" maxlength="15" onkeydown="getItem(event,'<?=base_url();?>')" ><input type='button' value='...' disabled name="btnbrg" id="btnbrg" onclick="popbrg('<?=base_url();?>')"/>
				</td>
			</tr>
			<tr>
				<td nowrap>
				<div id="myHiddenDiv" style="visibility:none">
					<select name="brg[]" id="brg[]" multiple size = "5" <?=$gantikursor;?>>
					</select>
					<input type='button' value='Remove' disabled name="delbrg" id="delbrg" onclick="deletebrg();"/>
				</div>
				</td>
			</tr>
		</table>
		</fieldset>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
		<fieldset>
		<legend>Detail</legend>
		<table>
			<tr>
				<th bgcolor="#414141"><font color="white"></font></th>
				<th bgcolor="#414141"><font color="white"></font></th>
				<th bgcolor="#414141"><font color="white">Termasuk</font></th>
				<th bgcolor="#414141"><font color="white">Kecuali</font></th>
				<th bgcolor="#414141"><font color="white">Opr 1</font></th>
				<th bgcolor="#414141"><font color="white">Nilai 1</font></th>
				<th bgcolor="#414141"><font color="white">Opr 2</font></th>
				<th bgcolor="#414141"><font color="white">Nilai 2</font></th>
				<th bgcolor="#414141"><font color="white">Campur</font></th>
			</tr>
		<?php write_detail($judul,$opr,$mcampur,$detaildisc); ?>
		</table>
		</fieldset>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td nowrap colspan="6">
		<?php
		if($edit)
		{?>
			<input type='button' value='Save' onclick="cekdiscount('<?=base_url();?>');"/>
		<?php
		}?>
			<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/discount/" />
		</td>
	</tr>
</table>
</form>
<?php
$this->load->view('footer');

function write_detail($judul,$opr,$mcampur,$detaildisc)
{
  	$strOpr    = "";
 	$strCampur = "";
 	$nilaicampur = array_keys($mcampur);
 	$jmljudul = count($judul);
 	for($s=0;$s<count($opr);$s++)
 	{
		$strOpr .= "<option value='".$opr[$s]."'>".$opr[$s]."</option>";
	}
 	for($s=0;$s<count($mcampur);$s++)
 	{
		$strCampur .= "<option value='".$nilaicampur[$s]."'>".$mcampur[$nilaicampur[$s]]."</option>";
	}

 	for($i=0;$i<$jmljudul;$i++)
 	{
 	 	$str 	   = "";
 	 	$ukuranBox = "";
		$namacek  = "all".$i;
		$namasel  = "sel".$i."[]";
		$namasel1 = "kec".$i."[]";
		$namaopr1 = "opr1".$i;
		$namanil1 = "nil1".$i;
		$namaopr2 = "opr2".$i;
		$namanil2 = "nil2".$i;
		$campur   = "campur".$i;
		$namajudul = array_keys($judul);
		$data = new discountmodel();
		$field1 = $judul[$namajudul[$i]][0];
		$field2 = $judul[$namajudul[$i]][1];
		$hasil = $data->getList($field1,$judul[$namajudul[$i]][1],$judul[$namajudul[$i]][2]);
		$jmlhasil = count($hasil);
		for($c=0;$c<$jmlhasil;$c++)
		{
		    $datacari = '0'.$i.$hasil[$c][$field1];
			echo $datacari.'<br>';
			$str .= "<option 'selected' value='".$hasil[$c][$field1]."'>".$hasil[$c][$field2]."</option>";
		}
		if($jmlhasil>5)
		{
			$ukuranBox = 5;
		}
?>
  	<tr bordercolor="#FFFFFF">
	  	<td nowrap><?=$namajudul[$i];?></td>
		<td nowrap><input type="checkbox" name="<?=$namacek;?>" id="<?=$namacek;?>" onclick ="cekall('<?=$namacek ?>','<?=$namasel?>')" /> Semua</td>
		<td nowrap>
			<select id="<?=$namasel;?>" name="<?=$namasel;?>" multiple size = "<?=$ukuranBox?>" onchange = "ubah1('<?=$namasel;?>','<?=$namacek;?>')">
			<?=$str;?>
			</select>
		</td>
		<td nowrap>
			<select id="<?=$namasel1;?>" name="<?=$namasel1;?>" multiple size = "<?=$ukuranBox?>" onchange = "ubah1('<?=$namasel1;?>','<?=$namacek;?>')">
			<?=$str;?>
			</select>
		</td>
		<td nowrap>
			<select id="<?=$namaopr1;?>" name ="<?=$namaopr1;?>"  onchange = "pilih('<?=$namaopr1;?>','<?=$namanil1;?>','<?=$namaopr2;?>','1','<?=$namanil2;?>')">
			  <?=$strOpr;?>
		  </select>
		</td>
		<td nowrap>
			<input type ="text" id ="<?=$namanil1;?>" name ="<?=$namanil1;?>" size ='12'maxlength='9' readonly/>
		</td>
		<td nowrap>
			<select id = "<?=$namaopr2;?>" name = "<?=$namaopr2;?>"  onchange = "pilih('<?=$namaopr1;?>','<?=$namanil1;?>','<?=$namaopr2;?>','2','<?=$namanil2;?>')" disabled>
			  <?=$strOpr;?>
		  </select>
		</td>
		<td nowrap>
			<input type ="text" id="<?=$namanil2;?>" name="<?=$namanil2;?>" size ='12'maxlength='9' readonly/>
		</td>
		<td nowrap>
			<select id="<?=$campur;?>" name="<?=$campur;?>" disabled>
			  <?=$strCampur;?>
		  </select>
		</td>
	</tr>
<?php
	}
}
?>