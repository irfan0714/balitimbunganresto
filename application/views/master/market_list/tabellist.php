<?php $this->load->view('header'); ?>

<script language="javascript" src="<?= base_url(); ?>public/js/market_list.js"></script>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div class="row">
    <div class="col-md-8">
        
    </div>

    <div class="col-md-4" align="right">
        <a href="<?php echo base_url() . "index.php/master/market_list/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
    </div>
</div>

<hr/>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th><center>Market List Name</center></th>
		        <th width="150"><center>Total User Level</center></th>
		        <th width="100"><center>Total Pcode</center></th>
		        <th width="100"><center>Navigasi</center></th>
        	</tr>
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }
                ?>
                <tr title="<?php echo $val["market_list_name"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td><?php echo $val["market_list_name"]; ?></td>
                    <td align="right"><?php echo $val["total_pair"]; ?></td>
                    <td align="right"><?php echo $val["total_pcode"]; ?></td>
                    <td align="center">
                        <a href="<?php echo base_url(); ?>index.php/master/market_list/form_edit/<?php echo $val["market_list_id"]; ?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>

                        <button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["market_list_id"]; ?>', '<?php echo base_url(); ?>');" >
                        <i class="entypo-trash"></i>
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
