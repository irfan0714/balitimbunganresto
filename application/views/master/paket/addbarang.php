<?php
$this->load->view('header');
?>
<link href="<?= base_url() ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.12.0.custom.css" rel="stylesheet">
 <style>
     .error{
      color:red;
     }
    </style>
<script src="<?= base_url() ?>assets/js/jquery-ui/js/jquery-1.9.1.js"></script>
<script src="<?= base_url() ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.custom.js"></script>

<script type="text/javascript">
    function cek_form() {
       // return true;
        if (!$('#itemNo_1').val()) {
            alert('Kode Barang Tidak Boleh Kosong');
            $('#itemNo_1').focus();
            return false;
        }

    }
</script>

<script>
    function change_id() {
        pcode = $("#kode").val();
		nama = $("#kode option:selected").text();
        $("#kdmpcode").val(pcode);
		$("#nama").val(nama);
        $("#barcode").val(pcode);
    }
</script>
<body>
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    Add Barang Paket 
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="barangpaket" id="barangpaket" action='<?= base_url(); ?>index.php/master/paket/save_new_barang' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">
                
                    <?php
                    
                    //echo validation_errors();
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Paket</label>
                        <div class="col-sm-4">
                            <?=form_dropdownDB_init('kode',$masterpkt,'PCode1','NamaLengkap', '','','---Pilih Barang Paket---','id="kode" class="form-control" onchange="change_id()"')?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Nama Paket</label>
                        <div class="col-sm-4">
                            <input name="kdmpcode" type="hidden" id="kdmpcode" size="10" maxlength="10" class="form-control" /> 
                            <input name="nama" type="text" id="nama" size="20" maxlength="20" class="form-control" readonly="readonly"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Barcode</label>
                        <div class="col-sm-2">
                            <input name="barcode" type="text" id="barcode" size="12" maxlength="12" class="form-control" readonly="readonly"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="2%"><input id="check_all" class="formcontrol" type="checkbox"/></th>
                                        <th width="15%">PCode</th>
                                        <th width="38%">Nama Barang</th>
                                        <th width="15%">Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input class="case" type="checkbox"/></td>
                                        <td><input type="text" data-type="PCode" name="itemNo[]" id="itemNo_1" class="form-control autocomplete_txt" autocomplete="off"><?php echo form_error('itemNo_1', '<div class="error">', '</div>'); ?></td>
                                        <td><input type="text" data-type="NamaLengkap" name="itemName[]" id="itemName_1" class="form-control autocomplete_txt" autocomplete="off"></td>
                                        <td><input type="text" name="quantity[]" id="quantity_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" dir="rtl"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
                            <button class="btn btn-default btn-icon icon-left delete" type="button"><i class="entypo-minus"></i>Delete</button>
                            <button class="btn btn-default btn-icon icon-left addmore" type="button"><i class="entypo-plus"></i>Add</button>
                        </div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                            <a class="btn btn-default btn-icon btn-lg icon-left" href="<?= base_url(); ?>index.php/master/paket/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-default btn-icon btn-lg icon-left" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>
<script src="<?= base_url() ?>public/js/barang_paket.js"></script>

