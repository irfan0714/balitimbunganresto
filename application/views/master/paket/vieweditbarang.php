<?php
$this->load->view('header');
?>
<link href="<?= base_url() ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.12.0.custom.css" rel="stylesheet">
<style>
    .error{
        color:red;
    }
</style>
<script src="<?= base_url() ?>assets/js/jquery-ui/js/jquery-1.9.1.js"></script>
<script src="<?= base_url() ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.custom.js"></script>

<script type="text/javascript">
    function cek_form() {
        // return true;
        if (!$('#itemNo_1').val()) {
            alert('Kode Barang Tidak Boleh Kosong');
            $('#itemNo_1').focus();
            return false;
        }

    }
</script>

<script>
    $(function () {

        $("#kode").autocomplete({
            minlength: 0,
            autoFocus: true,
            source: "<?php echo site_url('master/paket/getPaketByPCode'); ?>",
            select: function (event, ui) {
                var names = ui.item.label.split(" ");
                // $(this).val(ui.item.pcode);
                //$("#kode").val(ui.item.pcode);
                $("#kode").val(names);
                $("#kdmpcode").val(ui.item.pcode);
                $("#nama").val(ui.item.nama);
                $("#barcode").val(ui.item.barcode);
            }
        });

    })

    function change_id() {
        pcode = $("#kdmpcode").val();
        $("#kode").val(pcode);
    }
</script>
<body>
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    Edit Barang Paket 
                </div>
            </div>
            <div class="panel-body">

                <form method='post' name="barangpaket" id="barangpaket" action='<?= base_url(); ?>index.php/master/paket/save_barang' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()">
                    <?php
                    foreach ($getPaket->result() as $row) {
                        $MPCode = $row->MPCode;
                        $nl = $row->nl;
                        $bMaster = $row->bMaster;
                    }
                    ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Paket</label>
                        <div class="col-sm-4">
                            <input name="kode" type="text" id="kode" maxlength="20" onblur="change_id();" value="<?php echo $MPCode; ?>" autocomplete="off" class="form-control typeahead" placeholder="Masukan Kode atau Nama Paket" /> 
                            <?php echo form_error('kode', '<div class="error">', '</div>'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Nama Paket</label>
                        <div class="col-sm-4">
                            <input name="kdmpcode" type="hidden" id="kdmpcode" size="10" maxlength="10" class="form-control" /> 
                            <input name="nama" type="text" id="nama" size="20" value="<?php echo $nl; ?>" maxlength="20" class="form-control" readonly="readonly"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> Barcode</label>
                        <div class="col-sm-2">
                            <input name="barcode" type="text" id="barcode" value="<?php echo $bMaster; ?>" class="form-control" readonly="readonly"/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="2%"><input id="check_all" class="formcontrol" type="checkbox"/></th>
                                        <th width="15%">PCode</th>
                                        <th width="38%">Nama Barang</th>
                                        <th width="15%">Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $i=0;
                                        foreach ($getPaket->result() as $value) {
                                            $i++;
                                        ?>
                                        <tr>
                                            <td><input class="case" type="checkbox"/></td>
                                            <input type="hidden" data-type="id" name="id[]" id="id_<?=$i;?>" value="<?php echo $value->id; ?>">
                                            <td><input type="text" data-type="PCode" name="itemNo[]" id="itemNo_<?=$i;?>" class="form-control autocomplete_txt" value="<?php echo $value->DPcode; ?>" autocomplete="off"><?php echo form_error('itemNo_1', '<div class="error">', '</div>'); ?></td>
                                            <td><input type="text" data-type="NamaLengkap" name="itemName[]" id="itemName_<?=$i;?>" value="<?php echo $value->nmdet; ?>" class="form-control autocomplete_txt" autocomplete="off"></td>
                                            <td><input type="text" name="quantity[]" id="quantity_<?=$i;?>" value="<?php echo $value->Qty; ?>" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" dir="rtl"></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
                            <button class="btn btn-default btn-icon icon-left delete" type="button"><i class="entypo-minus"></i>Delete</button>
                            <button class="btn btn-default btn-icon icon-left addmore" type="button"><i class="entypo-plus"></i>Add</button>
                        </div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"> </label>
                        <div class="col-sm-4">
                            <input type='hidden' value='<?= base_url() ?>' id="baseurl" name="baseurl">
                            <a class="btn btn-default btn-icon btn-lg icon-left" href="<?= base_url(); ?>index.php/master/paket/"><i class="entypo-back"></i>Back</a>&nbsp;
                            <button class="btn btn-default btn-icon btn-lg icon-left" type="submit" onclick="return cek_form();"><i class="entypo-drive"></i>Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('footer'); ?>
<script src="<?= base_url() ?>public/js/barang_paket.js"></script>

