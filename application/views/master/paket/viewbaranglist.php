<?php $this->load->view('header'); ?>
<style>
    tr.group,
    tr.group:hover {
        background-color:  #fdfbf5 !important;
    }
</style>
<script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
//        $('#table_mt').DataTable({
//             
//            'order': [0, 'desc']
//            //'order': [[0, 'desc'], [1, 'desc'], [2, 'desc'],[3, 'desc'],[4, 'desc'],[5, 'desc']]
//        });

        var table = $('#table_mt').DataTable({
            columnDefs: [
                {visible: false, targets: 0, bSortable: false},
                { targets: 3, bSortable: false},
                { targets: 4, bSortable: false, searchable: false},
                { targets: 5, bSortable: false, searchable: false}
            ],
            order: [[0, 'asc']],
            displayLength: 25,
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(0, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                                );
                        last = group;
                    }
                });
            }
        });

    });

</script>

<div class="col-md-12">
    <div class="panel panel-gradient">
        <div class="panel-heading">
            <div class="panel-title">
                List Barang Paket 
            </div>
            <div class="panel-options">

                <?php
                if ($link->add == "Y") {
                    ?>
                    <a class="btn btn-primary btn-block" href="<?= base_url(); ?>index.php/master/paket/add_new/"><font color="#fff">Tambah</font></a>
                <?php } ?>

            </div>
        </div>
        <div class="panel-body">

            <table class='table table-bordered table-hover' id='table_mt'>
                <thead>
                    <tr>
<!--                        <th>Action</th> -->
                        <th>Kode</th>    
                        <th>Paket</th>
                        <th></th>
                        <th>Isi Paket</th>
                        <th>Barcode Detail</th>
                        <th>Nama Lengkap</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($barangpaket) == 0) {
                        echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
                    }
                    $temp = "";
                    foreach ($barangpaket as $key => $value) {
                        ?>
                        <tr>

                            <td nowrap> 
                                <?php
                                if ($link->view == "Y" || $link->edit == "Y" || $link->delete == "Y") {
                                    ?>
                                    <a 	href="<?= base_url(); ?>index.php/master/paket/edit_barang/<?= $value['MPCode']; ?>"><img src='<?= base_url(); ?>public/images/pencil.png' border = '0' title = 'Edit'/></a>
                                    <?php
                                }
                                ?><b><?= $value['MPCode'] . '  ' . $value['nl']; ?></b>
                            </td>
                            <td></td>
                            <td></td>
                            <td nowrap><?= $value['DPcode']; ?></td>
                            <td nowrap><?= $value['bDetail']; ?></td>
                            <td nowrap><?= $value['nmdet']; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>

        </div>
    </div>
</div>


<?php $this->load->view('footer'); ?>