<?php
$this->load->view('header'); ?>

<!--<form method="POST"  name="search" action="">
<table >
	<tr>
		<td><input class="form-control-new" type='text' size'20' name='stSearchingKey' id='stSearchingKey'></td>
		<td>&nbsp;&nbsp;
			<select class="form-control-new" size="1" height="1" name ="searchby" id ="searchby">
			    <option value="NamaGsa">Nama GSA</option>
				<option value="employee_id">Employee ID GSA</option>
			</select>
		</td>
		<td>&nbsp;&nbsp;&nbsp;<input class="btn btn-info btn-md md-new tooltip-primary" type="submit" value="Search (*)"></td>
	</tr>
</table>
</form>-->

<br>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th width="100"><center>Employee ID</center></th>
		        <th><center>Nama GSA</center></th>
		        <th width="100"><center>Warna</center></th>
		        <th width="50"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($gsa_data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($gsa_data as $val) {
                if($val["status"]=="Y"){
					$sts = "Aktif";
				}else{
					$sts = "Non Aktif";
				}
				
                ?>
                <tr title="<?php echo $val["KdGsa"]; ?>" style="cursor:pointer;" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="center"><?php echo $val["employee_id"]; ?></td>
                    <td align="left"><?php echo $val["nama"]; ?></td>
                    <td align="center" bgcolor="<?=$val["hex"];?>"><?php echo $val["warna"]; ?></td>
                    <td align="center"><?php echo $sts; ?></td>
                    <td align="center">
                            <a href="<?php echo base_url(); ?>index.php/master/gsa/view_gsa/<?php echo $val["KdGsa"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/master/gsa/edit_gsa/<?php echo $val["KdGsa"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
                            <a href="<?php echo base_url(); ?>index.php/master/gsa/delete_gsa/<?php echo $val["KdGsa"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title=""><i class="entypo-trash"></i></a>
                            
                            <!--<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="deleteTrans('<?php echo $val["KdSalesman"]; ?>', '<?php echo base_url(); ?>');" >
                            <i class="entypo-trash"></i>
                            </button>-->
                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>

<table align = 'center'  >
	<tr>
	<td>
	<?php echo $this->pagination->create_links(); ?>
	</td>
	</tr>
<?php
	if($link->add=="Y")
	{
?>
	<tr>
	<td nowrap colspan="3">
		<a 	href="<?=base_url();?>index.php/master/gsa/add_new/"><img src='<?=base_url();?>public/images/add.png' border = '0' title = 'Add'/></a>
	</td>
<?php } ?>
</table>
<?php
$this->load->view('footer'); ?>