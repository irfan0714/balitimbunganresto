<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'gsa',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('gsa')">
<form method='post' name="gsa" id="gsa" action='<?=base_url();?>index.php/master/gsa/save_gsa'>
<table  class="table table-bordered responsive">
	<tr>
		<td>
		
		<legend><b><?=$fieldset?> Guide Services Attendent</b></legend>
			<table  class="table table-bordered responsive">
			
				<input class="form-control-new" type='hidden' maxlength="4" size="5" readonly name='KdGsa' id='KdGsa' value="<?=stripslashes($view_gsa->KdGsa);?>" /></td>
			
					<td nowrap>GSA Name</td>
					<td nowrap>
					<select class="form-control-new" size="1" id="gsa" name="gsa" <?=$gantikursor;?>>
					<option> -- Pilih -- </option>
					<?php
					for($a = 0;$a<count($gsa);$a++){
						    $selected="";
							if($view_gsa->employee_id==$gsa[$a]['employee_id'])
							{
								$selected='selected="selected"';
							}
					?>
					<option <?php echo $selected; ?> value= "<?=$gsa[$a]['employee_id']?>"><?=stripslashes($gsa[$a]['employee_name'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
				
				<tr>
					<td nowrap>Warna</td>
					<td nowrap>
					<select class="form-control-new" size="1" id="warna" name="warna" <?=$gantikursor;?>>
					<option> -- Pilih -- </option>
					<?php
					for($a = 0;$a<count($warna);$a++){
						$selected="";
							if($view_gsa->wrn==$warna[$a]['id_warna'])
							{
								$selected='selected="selected"';
							}
					?>
					<option <?php echo $selected; ?> value= "<?=$warna[$a]['id_warna']?>"><?=stripslashes($warna[$a]['warna'])?></option>
					<?php
					}
					?>
					</select>
					</td>
				</tr>
			<tr>
					<td>Status</td>
					<td>
		            	<select class="form-control-new" name="status" id="status" style="width: 200px;">
		            		<option <?php if($view_gsa->status=='A'){ echo "selected='selected'"; } ?> value="A">Aktif</option>
		            		<option <?php if($view_gsa->status=='T'){ echo "selected='selected'"; } ?> value="T">Non Aktif</option>
		            	</select>
		            </td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input  class="btn btn-info btn-sm sm-new tooltip-primary" type='submit' value='Save'/>
				<?php } ?>
					<input  class="btn btn-info btn-sm sm-new tooltip-primary" type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/gsa/" />
				</td>
			</tr>
			</table>

	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>