<?php
$this->load->view('header');
?>
<script>
    function start_page()
    {
        document.getElementById("nama").focus();
    }
    
    function cek_form()
    {
        if(document.getElementById("Supplier").value=="")
        {
            alert("Supplier harus diisi...");
            document.getElementById("Supplier").focus();
            return false;
        }
        else if(document.getElementById("NoRekening").value=="")
        {
            alert("No Rekening harus diisi...");
            document.getElementById("NoRekening").focus();
            return false;
        } else if (document.getElementById("NamaBank").value=="") {
			alert("Nama Bank harus dipilih...");
            document.getElementById("NamaBank").focus();
            return false;
        } else if (document.getElementById("NamaPemilik").value=="") {
			alert("Nama Pemilik harus dipilih...");
            document.getElementById("NamaPemilik").focus();
            return false;
		}
    }
    
</script>

<body onload="start_page()">
    <div class="col-md-12">
        <div class="panel panel-gradient">
            <div class="panel-heading">
                <div class="panel-title">
                    <?=$label?>
                </div>
            </div>
            <div class="panel-body">
                <form method='post' name="supplier" id="supplier" action='<?= base_url(); ?>index.php/master/rekening_supplier/save' class="form-horizontal form-groups-bordered" onsubmit="return cek_form()"">
					<div class="form-group">
		                <label class="col-sm-2 control-label">Supplier</label>
		                <div class="col-sm-5">
		                	<input type="hidden" value="<?=$data['KdSupplier'];?>" class="form-control-new" style="width: 15%;" maxlength="30" name="KdSupplier" id="KdSupplier">
		          			<input name="Nama" id="Nama" value="<?=$data['Nama'];?>" size="30" maxlength="30" class="form-control" type="text" readonly>
		                </div>
		            </div>
			        
		            <div class="form-group">
		                <label class="col-sm-2 control-label">No Rekening (*)</label>
		                <div class="col-sm-5">
		                    <input name="NoRekening" id="NoRekening" value="<?=$data['NoRekening'];?>" size="30" maxlength="30"  class="form-control" type="text" readonly>
		                </div>
		            </div>
		            
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Nama Bank (*)</label>
		                <div class="col-sm-5">
		                    <input name="NamaBank" id="NamaBank" value="<?=$data['NamaBank'];?>" size="30" maxlength="30" class="form-control" type="text">
		                </div>
		            </div>
		            
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Nama Pemilik (*)</label>
		                <div class="col-sm-5">
		                    <input name="NamaPemilik" id="NamaPemilik" value="<?=$data['NamaPemilik'];?>" size="30" maxlength="30" class="form-control" type="text">
		                </div>
		            </div>
					
		            <div class="form-group">
		                <label class="col-sm-2 control-label">Status</label>
		                <div class="col-sm-1">
					        <select size="1" id="StatAktif" name="StatAktif">
								<option <?= $data['StatAktif']=='Y' ? 'selected' : '' ;?> value="Y">Ya</option>
								<option <?=$data['StatAktif']=='N' ? 'selected' : '';?> value="N">Tidak</option>
							</select>
		                </div>
		            </div>
				
					<div class="form-group" align="center">
						<input type='submit' value='Save'>
						<input type="button" value="Back" ONCLICK=parent.location="<?=base_url().'index.php/master/rekening_supplier/';?>" />
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
<?php
$this->load->view('footer'); ?>