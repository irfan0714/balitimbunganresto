<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'kategori',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('kategori')">
<form method='post' name="kategori" id="kategori" action='<?=base_url();?>index.php/master/kategori/save_kategori'>
<legend><b><?=$fieldset?> Kategori</b></legend>
<table width="50%" align="center" border="0" cellpadding="0" cellspacing="0" class="table responsive">
	<tr>
		<td nowrap>Kode</td>
		<td nowrap>:</td>
		<td nowrap><input type='text' class="form-control-new" maxlength="3" size="5" name='kode' id='kode' readonly='readonly' value="<?=stripslashes($viewkategori->KdKategori);?>"/></td>
	</tr>
	<tr>
		<td nowrap>Nama</td>
		<td nowrap>:</td>
		<td nowrap><input type='text' class="form-control-new" maxlength="25" size="35" name='nama' id='nama' value="<?=stripslashes($viewkategori->NamaKategori);?>" <?=$gantikursor;?>/></td>
	</tr>
	<tr>
		<td nowrap colspan="3">
			<?php if($edit){ ?>
				<button type="button" class="btn btn-info btn-icon btn-sm icon-left" onclick="cekMaster('kode','nama','kategori','Kode Kategori','Nama Kategori');" name="btn_save" id="btn_save"  value="Save">Simpan<i class="entypo-check"></i></button>
			<?php } ?>
			<button type="button" class="btn btn-orange btn-icon btn-sm icon-left" name="btn_close" id="btn_close"  value="Batal" onclick=parent.location="<?php echo base_url()."index.php/master/kategori/"; ?>">Keluar<i class="entypo-cancel-circled"></i></button></td>
		</td>
	</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>