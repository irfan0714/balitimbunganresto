<?php
$this->load->view('header'); ?>

<form method="POST"  name="search" action="">
<div class="row">
	<div class="col-md-8">
		<b>Search</b>&nbsp;
		<input type="text" size="20" maxlength="30" name="stSearchingKey" id="stSearchingKey" class="form-control-new" />
		&nbsp;<b>Gudang</b>&nbsp;
		<select class="form-control-new" name="searchby" id="searchby">
			<option value="KdKategori">Kode Kategori</option>
			<option value="NamaKategori">Keterangan</option>
		</select>
		&nbsp;
		<button type="submit" class="btn btn-info btn-icon btn-sm icon-left" onClick="show_loading_bar(100)">Search<i class="entypo-search"></i></button>
		<?php
			if($link->add=="Y")
			{
		?>
			<a href="<?php echo base_url() . "index.php/master/kategori/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left" title="" >Tambah<i class="entypo-plus"></i></a>
		<?php } ?>
	</div>
</div>
</form>

<br>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table width="50%" class="table table-bordered responsive">
	<thead>
		<tr>
		<th style="text-align:center;">Kode Kategori</th>
		<th style="text-align:center;">Keterangan</th>
		<?php
				if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
				{
			?>
		<th style="text-align:center;">Navigasi</th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
			<?php
			if(count($kategoridata)==0)
			{ 
			?>
		<tr><td nowrap colspan="3" align="center">Tidak Ada Data</td></tr>
			<?php		
			}
			for($a = 0;$a<count($kategoridata);$a++)
			{
			?>
		<tr>
			<td nowrap align="center" width="10%"><?=$kategoridata[$a]['KdKategori'];?></td>
			<td nowrap><?=stripslashes($kategoridata[$a]['NamaKategori']);?></td>
			<?php
				if($link->view=="Y"||$link->edit=="Y"||$link->delete=="Y")
				{
				?>
			<td nowrap align="center">
				<?php
					if($link->edit=="Y")
					{
				?>
				<a href="<?php echo base_url(); ?>index.php/master/kategori/edit_kategori/<?=$kategoridata[$a]['KdKategori'];?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>
				<?php
					}
					if($link->delete=="Y")
					{
				?>
				<a href="<?php echo base_url(); ?>index.php/master/kategori/delete_kategori/<?=$kategoridata[$a]['KdKategori'];?>" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Delete" title=""><i class="entypo-trash"></i></a>
				<?php
					}
				?>
				<?php
					}
				?>
			</td>
		</tr>
			<?php
			}
			?>
		</tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>	

</div>
<?php
$this->load->view('footer'); ?>