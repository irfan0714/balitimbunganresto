<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'personal',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('personal')">
<form method='post' name="personal" id="personal" action='<?=base_url();?>index.php/master/personal/save_personal'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Personal</b></legend>
			<table align = 'center'>
			<tr>
				<td nowrap>Kode</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="4" size="5" readonly name='kode' id='kode' value="<?=stripslashes($view_personal->KdPersonal);?>" /></td>
			</tr>
			<tr>
				<td nowrap>Nama</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($view_personal->NamaPersonal);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Kategori</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" id="tipe" name="tipe" <?=$gantikursor;?>>
				<?php
				for($a = 0;$a<count($tipe);$a++){
					$select = "";
					if($view_personal->KdTipePersonal==$tipe[$a]['KdTipePersonal']){
						$select = "selected";
					}
				?>
				<option <?=$select;?> value= "<?=stripslashes($tipe[$a]['KdTipePersonal'])?>"><?=stripslashes($tipe[$a]['NamaTipePersonal'])?></option>
				<?php
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input type='button' value='Save' onclick="cekMaster2('kode','nama','personal','Kode Personal','Nama Personal');"/>
				<?php } ?>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/personal/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>