<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'divisi',this)\"";
if ($edit) {
    $fieldset = "Edit";
} else {
    $fieldset = "View";
}
?>
<script language="javascript" src="<?= base_url(); ?>public/js/global.js"></script>
<body onload="firstLoad('divisi')">
    <form method='post' name="divisi" id="divisi" action='<?= base_url(); ?>index.php/master/divisi/save_divisi' class="form-horizontal">
        <table align = 'center'>
            <tr>
                <td>
                    <fieldset>
                        <legend><b><?= $fieldset ?> Divisi</b></legend>
                        <div class="control-group">
                            <label class="control-label" for="kodesub"> Kode</label>
                            <div class="controls">
                                <input name="kode" class="span3" type="text" id="kode" value="<?= stripslashes($viewdivisi->KdDivisi); ?>" <?= $gantikursor; ?> />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="namasub"> Nama</label>
                            <div class="controls">
                                <input name="nama" class="span3" type="text" id="nama" value="<?= stripslashes($viewdivisi->NamaDivisi); ?>" <?= $gantikursor; ?> />
                            </div>
                        </div>

                        <br>
                        <div class="control-group">
                            <div class="controls">
                                <?php if($edit){ ?>
                                <input type="button" class="btn btn-default" name="button" id="button" value="Save" onclick="cekMaster('kode', 'nama', 'divisi', 'Kode Divisi', 'Nama Divisi');" />
                                <?php } ?>
                                <input type="button" class="btn" value="Back" onclick=parent.location="<?= base_url(); ?>index.php/master/divisi/" />
                            </div>
                        </div>

                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
    <?php $this->load->view('footer'); ?>