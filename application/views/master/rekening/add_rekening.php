<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'rekening',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('rekening')">
<form method='post' name="rekening" id="rekening" action='<?=base_url();?>index.php/master/rekening/save_new_rekening'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b>Add Rekening</b></legend>
			<table align = 'center'>
				<?php
				if($msg){ echo $msg;}?>	
				<tr>
					<td nowrap>Rekening</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="8" size="10" name='kode' value="<?=stripslashes($id);?>" id='kode' <?=$gantikursor;?> onKeyUp="javascript:dodacheck(document.getElementById('kode'));" /></td>
				</tr>
				<tr>
					<td nowrap>Nama Rekening</td>
					<td nowrap>:</td>
					<td nowrap><input type='text' maxlength="25" size="35" id='nama' value="<?=stripslashes($nama);?>" name='nama' <?=$gantikursor;?>/></td>
				</tr>
		
			<tr>
				<td nowrap>Posisi</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" id="posisi" name="posisi" <?=$gantikursor;?>>
				<?php
				for($a = 0;$a<count($posisi);$a++){
					$select = "";
					if($nilposisi==$posisi[$a]['posisi']){
						$select = "selected";
					}
				?>
				<option <?=$select;?> value= "<?=$posisi[$a]['posisi']?>"><?=stripslashes($posisi[$a]['namaposisi'])?></option>
				<?php
				}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Tingkat</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" id="tingkat" name="tingkat" <?=$gantikursor;?>>
				<?php
				for($a = 0;$a<count($tingkat);$a++){
					$select = "";
					if($niltingkat==$tingkat[$a]['tingkat']){
						$select = "selected";
					}
				?>
				<option <?=$select;?> value= "<?=$tingkat[$a]['tingkat']?>"><?=stripslashes($tingkat[$a]['namatingkat'])?></option>
				<?php
				}
				?>
				</select>
				</td>
			</tr>


			<tr>
				<td nowrap>Parent</td>
				<td nowrap>:</td>
				<td nowrap>
				<select size="1" id="rekparent" name="rekparent" <?=$gantikursor;?>>
				<option value="">--Please Select--</option>
				<?php
				for($a = 0;$a<count($rekparent);$a++){
					$select = "";
					if($nilparent==$rekparent[$a]['KdRekening']){
						$select = "selected";
					}
				?>
				<option <?=$select;?> value= "<?=$rekparent[$a]['KdRekening']?>"><?=$rekparent[$a]['KdRekening'].'-'.stripslashes($rekparent[$a]['NamaRekening'])?></option>
				<?php
				}
				?>
				</select>
				</td>
			</tr>
				<tr>
					<td nowrap colspan="3">
						<input type='button' value='Save' onclick="cekMaster4('kode','nama','posisi','tingkat','rekening','Kode Rekening','Nama Rekening','Posisi','Tingkat');"/>
						<input type="button" value="Back" onclick=parent.location="<?=base_url();?>index.php/master/rekening/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>