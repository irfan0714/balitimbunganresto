<?php
$this->load->view('header'); 
$gantikursor = "onkeydown=\"changeCursor(event,'rekening',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<body onload="firstLoad('rekening')">
<form method='post' name="rekening" id="rekening" action='<?=base_url();?>index.php/master/rekening/save_rekening'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Rekening</b></legend>
			<table align = 'center'>
			<tr>
				<td nowrap>Kode</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="8" size="10" readonly name='kode' id='kode' value="<?=stripslashes($view_rekening->KdRekening);?>" /></td>
			</tr>
			<tr>
				<td nowrap>Nama</td>
				<td nowrap>:</td>
				<td nowrap><input type='text' maxlength="25" size="35" id='nama' name='nama' value="<?=stripslashes($view_rekening->NamaRekening);?>" <?=$gantikursor;?>/></td>
			</tr>
			<tr>
				<td nowrap>Posisi</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="posisi" name="posisi" <?=$gantikursor;?>>
					<?php
					$nilposisi = $view_rekening->Posisi;
					for($a = 0;$a<count($posisi);$a++){
					 	$select = "";
					 	if($nilposisi==$posisi[$a]['posisi']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$posisi[$a]['posisi']?>"><?=stripslashes($posisi[$a]['namaposisi'])?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Tingkat</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="tingkat" name="tingkat" <?=$gantikursor;?>>
					<?php
					$niltingkat = $view_rekening->Tingkat;
					for($a = 0;$a<count($tingkat);$a++){
					 	$select = "";
					 	if($niltingkat==$tingkat[$a]['tingkat']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$tingkat[$a]['tingkat']?>"><?=stripslashes($tingkat[$a]['namatingkat'])?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap>Parent</td>
				<td nowrap>:</td>
				<td nowrap colspan="6">
					<select size="1" id="rekparent" name="rekparent" <?=$gantikursor;?>>
					<?php
					$nilparent = $view_rekening->RekParent;
					for($a = 0;$a<count($rekparent);$a++){
					 	$select = "";
					 	if($nilparent==$rekparent[$a]['KdRekening']){
							$select = "selected";
						}
					?>
					<option <?=$select;?> value= "<?=$rekparent[$a]['KdRekening']?>"><?=stripslashes($rekparent[$a]['NamaRekening'])?></option>
					<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input type='button' value='Save' onclick="cekMaster4('kode','nama','posisi','tingkat','rekening','Kode Rekening','Nama Rekening','Posisi','Tingkat');"/>
				<?php } ?>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/rekening/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>