<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'contact',this)\"";?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript">
function ubahpayment()
{
	sumber = $("input[@name='sumber']:checked").val();
	$("#hiddensumber").val(sumber);
	if(sumber=="C")
	{
	$("#top").attr("disabled", "disabled");
	$("#top").val("0");
	}
    else
	$("#top").attr("disabled", "");
}
</script>
<body onload="firstLoad('contact');">
<form method='post' name="contact" id="contact" action='<?=base_url();?>index.php/master/contact/save_new_contact'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b>Add Kontak</b></legend>
			<table align = 'center'>
			<?php
			if($msg){ echo $msg;}
			$mylib = new globallib();
			echo $mylib->write_textbox("Kode","kode",$id,"8","6","readonly='readonly'","text",$gantikursor."onKeyUp=\"javascript:dodacheck(document.getElementById('kode'));\"","1");
			echo $mylib->write_combo("Tipe","tipe",$tipekontak,$niltipe,"KdTipeContact","NamaTipeContact",$gantikursor,"","ya");
			echo $mylib->write_textbox("Nama","nama",stripslashes($nama),"55","50","","text",$gantikursor,"1");
			?>
			<tr>
				<td>Alamat</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="alm" name="alm"><?=stripslashes($alm)?></textarea>
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Kota","kota",stripslashes($kota),"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox("No. Telp","telp",$telp,"25","20","","text",$gantikursor,"1");
			?>
			<tr>
				<td>Alamat Kirim</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="almkirim" name="almkirim"><?=stripslashes($almkirim)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Kirim","kotakirim",stripslashes($kotakirim),"35","30","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("No. Telp Kirim","telpkirim",$telpkirim,"25","20","","text",$gantikursor,"1");?>
			<?$mylib->write_textbox("Nama Pajak","namapajak",stripslashes($namapajak),"55","50","","text",$gantikursor,"1");?>
			<tr>
				<td>Alamat Pajak</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="almpajak" name="almpajak"><?=stripslashes($almpajak)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Pajak","kotapajak",stripslashes($kotapajak),"35","30","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("NPWP","npwp",$npwp,"25","20","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("Tanggal PKP","tglpkp",$tglpkp,"15","10","","text",$gantikursor,"1");?>
			<tr>
				<td nowrap>Payment</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="C" checked name="sumber" id="sumber" onclick="ubahpayment();"/>Cash
					<input type="radio" value="K" name="sumber" id="sumber" onclick="ubahpayment();"/>Credit
				    <input type="text" name="top" id="top" maxlength="3" size="3" value='0' disabled="disabled">Hari
				</td>
			</tr>
			<?php
			echo $mylib->write_number("Limit Kredit","limitkredit","0","25","20","","text",$gantikursor,"1","");
			echo $mylib->write_number("Limit Faktur","limitfaktur","0","5","2","","text",$gantikursor,"1","");
			?>
			<tr>
				<td nowrap>Status Aktif</td>
				<td nowrap>:</td>
				<td nowrap colspan="5">
				<select size="1" id="stataktif" name="stataktif" <?=$gantikursor;?>>
				<option <?php echo "selected";?> value="Y">Ya</option>
				<option value="N">Tidak</option>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
					<input type='button' value='Save' onclick="cekcontact();"/>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/contact/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>