<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'contact',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/cek.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript">
function ubahpayment()
{
	sumber = $("input[@name='sumber']:checked").val();
	$("#hiddensumber").val(sumber);
	if(sumber=="C")
	{
	$("#top").attr("disabled", "disabled");
	$("#top").val("0");
	}
    else
	$("#top").attr("disabled", "");
}
</script>
<body onload="firstLoad('contact');">
<form method='post' name="contact" id="contact" action='<?=base_url();?>index.php/master/contact/save_contact'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Kontak</b></legend>
			<table align = 'center'>
		<?php
			$mylib = new globallib();
			echo $mylib->write_textbox("Kode","kode",stripslashes($viewcontact->KdContact),"8","6","readonly","text",$gantikursor,"1");
			echo $mylib->write_combo("Tipe","tipe",$tipekontak,$viewcontact->KdTipeContact,"KdTipeContact","NamaTipeContact",$gantikursor,"","ya");
			echo $mylib->write_textbox("Nama","nama",stripslashes($viewcontact->Nama),"55","50","","text",$gantikursor,"1");
		?>
			<tr>
				<td>Alamat</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="alm" name="alm"><?=stripslashes($viewcontact->Alamat)?></textarea>
				</td>
			</tr>
			<?php
			echo $mylib->write_textbox("Kota","kota",stripslashes($viewcontact->Kota),"35","30","","text",$gantikursor,"1");
			echo $mylib->write_textbox("No. Telp","telp",$viewcontact->Telepon,"25","20","","text",$gantikursor,"1");
		?>
			<tr>
				<td>Alamat Kirim</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="almkirim" name="almkirim"><?=stripslashes($viewcontact->AlamatKirim)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Kirim","kotakirim",stripslashes($viewcontact->KotaKirim),"35","30","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("No. Telp Kirim","telpkirim",$viewcontact->TelpKirim,"25","20","","text",$gantikursor,"1");?>
			<?$mylib->write_textbox("Nama Pajak","namapajak",stripslashes($viewcontact->NamaPajak),"55","50","","text",$gantikursor,"1");?>
			<tr>
				<td>Alamat Pajak</td>
				<td><b>:</b></td>
				<td>
				<textarea rows="5" cols="27" id="almpajak" name="almpajak"><?=stripslashes($viewcontact->AlamatPajak)?></textarea>
				</td>
			</tr>
			<?=$mylib->write_textbox("Kota Pajak","kotapajak",stripslashes($viewcontact->KotaPajak),"35","30","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("NPWP","npwp",$viewcontact->NPWP,"25","20","","text",$gantikursor,"1");?>
			<?=$mylib->write_textbox("Tanggal PKP","tglpkp",$viewcontact->TglPKP,"15","10","","text",$gantikursor,"1");?>
			<tr>
				<td nowrap>Payment</td>
				<td nowrap>:</td>
				<td nowrap>
					<input type="radio" value="C" <?php echo $checkedc; ?> name="sumber" id="sumber" onclick="ubahpayment();"/>Cash
					<input type="radio" value="K" <?php echo $checkedk; ?> name="sumber" id="sumber" onclick="ubahpayment();"/>Credit
				    <input type="text" name="top" id="top" maxlength="3" size="3" value="<?=$viewcontact->TOP?>" <?php echo $topdisabled; ?>>Hari
				</td>
			</tr>
			<?php
			echo $mylib->write_number("Limit Kredit","limitkredit",$viewcontact->LimitKredit,"25","20","","text",$gantikursor,"1","");
			echo $mylib->write_number("Limit Faktur","limitfaktur",$viewcontact->LimitFaktur,"5","2","","text",$gantikursor,"1","");
			?>
			<tr>
				<td nowrap>Status Aktif</td>
				<td nowrap>:</td>
				<td nowrap colspan="5">
				<select size="1" id="stataktif" name="stataktif" <?=$gantikursor;?>>
				<option <?php if($viewcontact->StatAktif=="Y") echo "selected";?> value="Y">Ya</option>
				<option <?php if($viewcontact->StatAktif=="N") echo "selected";?> value="N">Tidak</option>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap colspan="3">
				<?php if($edit){ ?>
					<input type='button' value='Save' onclick="cekcontact();"/>
				<?php } ?>
					<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/contact/" />
				</td>
			</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer'); ?>