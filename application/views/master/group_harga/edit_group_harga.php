<?php
$this->load->view('header'); 
$modul = "Group Harga";
?>
<body class="page-body skin-black" onload="start_page()">
<div class="row">
    <div class="col-md-12" align="left">
    
    	<ol class="breadcrumb title_table">
			<li><strong><i class="entypo-pencil"></i>Edit <?php echo $modul; ?></strong></li>
		</ol>
		
		<form method='post' name='theform' id='theform'' action='<?=base_url();?>index.php/master/group_harga/save_data'>
		<input type="hidden" class="form-control-new" value="<?=$id;?>" name="v_id" id="v_id" maxlength="255" size="48%">
		<input type="hidden" class="form-control-new" value="" name="excel" id="excel">
	    <table class="table table-bordered responsive">                        
	        
	        <tr>
	            <td class="title_table">Nama Group Harga </td>
	            <td>
	            	<input type="text" class="form-control-new" value="<?=$header->GroupHargaName;?>" name="v_namagroup" id="v_namagroup" maxlength="255" size="48%">
	       
	            		<button type="button" class="btn btn-primary btn-icon btn-sm icon-left pull-right"  value="add_item" onclick="add_item('<?php echo base_url(); ?>');">Tambah Item Barang<i class="entypo-plus"></i></button>
	         
	            </td>
	        </tr>
	        
	        <tr>
	            <td>&nbsp;</td>
	            <td>
	            	<input type="button" class="btn btn-info" onclick="cekTheform_excel();" name="btn_excel" id="btn_excel" value="Export To Excel">
	            </td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
	        		<div id="konten"></div>
	        	</td>
	        </tr>
	        
	        <tr>
	        	<td colspan="100%">
	        	    <b>Detail Menu</b>
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="10"><center>No.</center></th>
								<th width="80"><center>PCode</center></th>
								<th><center>Nama Barang</center></th> 
							    <th width="100"><center>Harga</center></th>
							    <th width="30"><center>Action</center></th>
							</tr>
						</thead>
						<tbody>
						<?php
						 	$no=1;
						 	foreach($detail AS $val){
								?>
								<tr id="baris<?php echo $no; ?>">
									<td>
									<?=$no;?>										
									</td>
									
									<td>
										<input type="hidden" class="form-control-new" name="v_pcode[]" id="v_pcode<?php echo $no;?>" value="<?=$val['PCode'];?>"/>
										<?=$val['PCode'];?>										
									</td>
									
									<td>
									<?=$val['NamaLengkap'];?>										
									</td>
									
									<td>
										<input type="text" class="form-control-new" name="v_harga[]" id="v_harga<?php echo $no;?>" value="<?=$val['Harga'];?>" style="text-align: right; width: 100%;" />
									</td>
									<td align="center">
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick="delete_detail('<?=$id;?>','<?=$val['PCode'];?>')">
										<i class="entypo-trash"></i>
									</button>
					                </td>
								</tr>
								<?php
						     $no++;}
						?>	
						</tbody>
					</table>
	        	</td>
	        </tr>
	        	        
	        <tr>
	            <td colspan="100%" align="center">
					<input type='hidden' name="flag" id="flag" value="edit">
					<input type='hidden' name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                    <a href="<?php echo base_url() . "index.php/master/group_harga/"; ?>" class="btn btn-danger btn-icon btn-sm icon-left" title="" >Close<i class="entypo-check"></i></a>
                    <button type="button" class="btn btn-green btn-icon btn-sm icon-left" onclick="cekTheform();" name="btn_save" id="btn_save"  value="Simpan">Simpan<i class="entypo-check"></i></button>
                </td>
	        </tr>
	        
	    </table>
	    
	    </form> 
        
	</div>
</div>

           <div id="pleaseWaitDialog" class="modal" data-keyboard="false" data-backdrop="false" style="background-color: rgba(0, 0, 0, 0.2);">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>Loading...</h3>
                        </div>
                        <div class="modal-body">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</body>    	
<?php $this->load->view('footer'); ?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>

<script>

	function start_page(){
		//alert("Start Page");
		//getData();
	}
	
	function cekTheform(){
		$("#excel").val("");
		document.getElementById("theform").submit();
	}
	
	function cekTheform_excel(){
		$("#excel").val("1");
		document.getElementById("theform").submit();
	}
	
	function getData(){
		base_url = $("#base_url").val();	
		$('#pleaseWaitDialog').modal('show');
		
	    	$.ajax({
				type: "POST",
				url: base_url + "index.php/master/group_harga/getList/",
				success: function(data) {
					$('#pleaseWaitDialog').modal('hide');
					$('#konten').html(data);
				}
			});
	}
	
	function add_item(base_url){
		
		url = base_url+"index.php/pop/pop_up_item_barang/";
		windowOpener(650, 600, 'Cari Item Barang', url, 'Cari Item Barang')
	}
	
	function deleteRow(obj)
	{
		objek = obj.id;
		id = objek.substr(15,objek.length-3);

		var lastRow = document.getElementsByName("v_harga[]").length;
		
		if( lastRow > 1)
		{
			$('#baris'+id).remove();
		}else{
				alert("Baris ini tidak dapat dihapus \n Minimal harus ada 1 baris tersimpan");
		}
	}
	
	function delete_detail(id,pcode){
		
		var r=confirm("Apakah Anda Ingin Menghapus PCode "+pcode+" ?")
		if (r==true)
		{
		
				base_url = $("#base_url").val();	
				$('#pleaseWaitDialog').modal('show');
				
			    	$.ajax({
						type: "GET",
						url: base_url + "index.php/master/group_harga/delete_detail/"+id+"/"+pcode,
						success: function(data) {
							$('#pleaseWaitDialog').modal('hide');
							window.location.reload();
						}
					});
		}
		else
		{
	  		return false;
		}
	}
</script>