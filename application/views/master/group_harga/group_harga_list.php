<?php $this->load->view('header'); ?>
<head>
</head>

<a href="<?php echo base_url() . "index.php/master/group_harga/add_new/"; ?>" class="btn btn-info btn-icon btn-sm icon-left pull-right" title="" >Tambah<i class="entypo-plus"></i></a>
<br><hr>

<?php
if ($this->session->flashdata('msg')) {
    $msg = $this->session->flashdata('msg');
    ?><div class="alert alert-<?php echo $msg['class']; ?>"><?php echo $msg['message']; ?></div><?php
}
?>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered responsive">
        <thead class="title_table">
            <tr>
                <th width="30"><center>No</center></th>
		        <th><center>Nama Group Harga</center></th>
		        <th width="100"><center>Status</center></th>
                <th width="100"><center>Action</center></th>
        	</tr>
            
        </thead>
        <tbody>

            <?php
            if (count($data) == 0) {
                echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
            }

            $no = 1;
            foreach ($data as $val) {
                $bgcolor = "";
                if ($no % 2 == 0) {
                    $bgcolor = "background:#f7f7f7;";
                }

                if ($val["Status"] == "A") {
                    $echo_status = "<font style='color:black'><b>Aktif</b></font>";
                } else if ($val["Status"] == "T") {
                    $echo_status = "<font style='color:red'><b>Tidak Aktif</b></font>";
                }else{
					$echo_status = "<font style='color:green'><b>Pending</b></font>";
				}
                ?>
                <tr title="<?php echo $val["GroupHargaName"]; ?>" style="cursor:pointer; <?php echo $bgcolor; ?>" onmouseover="change_onMouseOver('<?php echo $no; ?>')" onmouseout="change_onMouseOut('<?php echo $no; ?>')" id="<?php echo $no; ?>">
                    <td><?php echo $no; ?></td>
                    <td align="left"><?php echo $val["GroupHargaName"]; ?></td>
                    <td align="center"><?php echo $echo_status; ?></td>
                    <td align="center">

                        <?php
                        if ($val["Status"] == "O") {
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/master/group_harga/edit_data/<?php echo $val["GroupHargaID"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit" title=""><i class="entypo-pencil"></i></a>

							<a href="<?php echo base_url(); ?>index.php/master/group_harga/aktif/<?php echo $val["GroupHargaID"]; ?>"  class="btn btn-warning btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Aktif" title=""><i class="entypo-check"></i></a>
                            <?php
                        }

                        if ($val["Status"] == "A") {
                            ?>
                            
                            <a href="<?php echo base_url(); ?>index.php/master/group_harga/view_data/<?php echo $val["GroupHargaID"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>

                            <a href="<?php echo base_url(); ?>index.php/master/group_harga/nonaktif/<?php echo $val["GroupHargaID"]; ?>"  class="btn btn-danger btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Non Aktif" title=""><i class="entypo-key"></i></a>
                            <?php
                        }
                        
                        if ($val["Status"] == "T") {
                            ?>
                            
                            <a href="<?php echo base_url(); ?>index.php/master/group_harga/view_data/<?php echo $val["GroupHargaID"]; ?>"  class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="View" title=""><i class="entypo-eye"></i></a>

                            <?php
                        }?>

                    </td>
                </tr>
                <?php
                $no++;
            }
            ?>

        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-6 col-left">
            <div id="table-2_info" class="dataTables_info">&nbsp;</div>
        </div>
        <div class="col-xs-6 col-right">
            <div class="dataTables_paginate paging_bootstrap">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>	

</div>


<?php $this->load->view('footer'); ?>
