<?php

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="Group_Harga_'.$header->GroupHargaName.'.xls"');
    
?>
<body class="page-body skin-black" onload="start_page()">
<div class="row">
    <div class="col-md-12" align="left">
    		
		<?php
		$echo = '';
		$echo .= '<h2><b>List Group Harga '.$header->GroupHargaName.'</b></h2>';
		$echo .='                       
	        
					<table border="1">
        				<thead class="title_table">
							<tr>
							    <th width="30"><center>No.</center></th>
								<th width="80"><center>PCode</center></th>
								<th width="500"><center>Nama Barang</center></th> 
							    <th width="100"><center>Harga</center></th>
							</tr>
						</thead>
						<tbody>';?>
						<?php
						 	$no=1;
						 	foreach($detail AS $val){
								?>
								<?php $echo.='<tr>
									<td>'.$no.'</td>
									
									<td>
										'.$val['PCode'].'										
									</td>
									
									<td>
									    '.$val['NamaLengkap'].'										
									</td>
									
									<td align="right">
										'.$val['Harga'].'
									</td>
								</tr>';?>
								<?php
						     $no++;}
						?>	
						
						<?php $echo.='</tbody>
					</table>';
					
					echo $echo;?>
					
        
	</div>
</div>

