<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'lokasi',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script language="javascript" src="<?=base_url();?>public/js/lokasi.js"></script>
<body onload="firstLoad('lokasi')">
<form method='post' name="lokasi" id="lokasi" action='<?=base_url();?>index.php/master/lokasi/save_lokasi'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Lokasi</b></legend>
			<table align = 'center'>
			<?php
				$mylib = new globallib();
				echo $mylib->write_textbox("Kode","kode",$viewlokasi->KdLokasi,"15","15","readonly","text",$gantikursor,"5");
				echo $mylib->write_textbox("Nama","nama",stripslashes($viewlokasi->Keterangan),"35","25","","text",$gantikursor,"5");
				echo $mylib->write_textbox("Tingkat","tingkat",$viewlokasi->Tingkat,"5","3","","text",$gantikursor,"5");
				echo $mylib->write_combo("Tipe Lokasi","tipe",$tipe,$viewlokasi->KdTipeLokasi,"KdTipeLokasi","Keterangan",$gantikursor,"","ya");
				?>
				<tr>
					<td nowrap>Lokasi Aktif</td>
					<td nowrap>:</td>
					<td nowrap colspan="5">
					<select size="1" id="statlokasi" name="statlokasi" <?=$gantikursor;?>>
					<option <?php if($viewlokasi->StatusLokasiKecil=="Y") echo "selected";?> value="Y">Ya</option>
					<option <?php if($viewlokasi->StatusLokasiKecil=="N") echo "selected";?> value="N">Tidak</option>
					</select>
					</td>
				</tr>
				<!--
				<?php
				echo $mylib->write_number("Order","order",$viewlokasi->Status,"5","3","0","text",$gantikursor,"1","");
				?>-->
			    <tr>
					<td nowrap colspan="3">
					<input type="hidden" value="<?=base_url()?>" id="baseurl" name="baseurl">
					<?php if($edit){ ?>
						<input type='button' value='Save' onclick="ceklokasi();"/>
					<?php } ?>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/lokasi/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer');
?>