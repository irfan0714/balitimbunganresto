<?php
$this->load->view('header');
$gantikursor = "onkeydown=\"changeCursor(event,'kendaraan',this)\"";
if($edit){ $fieldset = "Edit"; }
else{ $fieldset = "View";}
?>
<script language="javascript" src="<?=base_url();?>public/js/global.js"></script>
<script language="javascript">
function cekKendaraan(){
	if(cekoption("nama","Memasukkan Nama"))
	if(cekoption("nopol","Memasukkan No Polisi"))
	if(cekoption("driver","Memasukkan Supir"))
		document.getElementById("kendaraan").submit();
}
</script>
<body onload="firstLoad('kendaraan')">
<form method='post' name="kendaraan" id="kendaraan" action='<?=base_url();?>index.php/master/kendaraan/save_kendaraan'>
<table align = 'center'>
	<tr>
		<td>
		<fieldset class="fieldsetUmum">
		<legend><b><?=$fieldset?> Kendaraan</b></legend>
			<table align = 'center'>
			<?php
				$mylib = new globallib();
				echo $mylib->write_textbox("Kode","kode",$data_kendaraan->KdKendaraan,"11","10","readonly","text",$gantikursor,"5");
				echo $mylib->write_textbox("Nama","nama",$data_kendaraan->NamaKendaraan,"35","25","","text",$gantikursor,"1");
				echo $mylib->write_textbox("No Polisi","nopol",$data_kendaraan->NoPolisi,"15","10","","text",$gantikursor,"1");
				echo $mylib->write_combo("Supir","driver",$mdriver,$data_kendaraan->KdPersonal,"KdPersonal","NamaPersonal",$gantikursor,"","ya");
			?>
				<tr>
					<td nowrap colspan="3">
					<input type="hidden" value="<?=base_url()?>" id="baseurl" name="baseurl">
					<?php if($edit){ ?>
						<input type='button' value='Save' onclick="cekKendaraan();"/>
					<?php } ?>
						<input type="button" value="Back" ONCLICK =parent.location="<?=base_url();?>index.php/master/kendaraan/" />
					</td>
				</tr>
			</table>
		</fieldset>
	</td>
</tr>
</table>
</form>
<?php
$this->load->view('footer');
?>