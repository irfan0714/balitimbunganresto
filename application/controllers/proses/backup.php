<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class backup extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->helper('path');
        $this->load->library('email'); 
    }

    function index(){
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
//        print_r($sign);die();
    	if($sign=="Y")
		{
	        $data['track'] = $mylib->print_track();
			$data['msg'] = "";
	        $this->load->view('proses/backup/backup', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	
	
	
	function doThis()
	{   
            $mylib 		= new globallib();
            $tgl1   	= $mylib->ubah_tanggal($this->input->post("tglawal"));
            $tgl2   	= $mylib->ubah_tanggal($this->input->post("tglakhir"));
            $isiattc 	= $this->eksekusi_database();
            
            $query = $this->db->query("select NamaPT,Alamat1PT from aplikasi limit 0,1");
			$result_query = $query->row();
			$query->free_result();
			
			
            //Buat Data Folder di Application BackupNMP
            $pathDta	= set_realpath(APPPATH."BackupNPM");
            $judulattc	= "backupNPM-".date("Y-m-d~H-i-s");
            $this->createAttach("$judulattc",$isiattc);

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://mx.vci.co.id';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '300';
            									 
            $config['smtp_user']    = 'automail@secretgarden.co.id';
            $config['smtp_pass']    = 'secret2016';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'text'; // or html
            $config['validation']   = TRUE; // bool whether to validate email or not
 
            $this->email->initialize($config);
 	
            
		    //*Konfigurasi email keluar melalui mail server*//
        	$judul_email = 'Auto Backup Database';
        	$sub = 'Auto Backup Database '.$result_query->NamaPT;
            $this->email->from('automail@secretgarden.co.id',$judul_email); 
            $this->email->to("backup@secretgarden.co.id;");  //diisi dengan alamat tujuan
            $this->email->cc("irf@vci.co.id;");  //diisi dengan alamat tujuan
            $this->email->Subject($sub); 
            $this->email->message("BackupRetail : ".$judulattc.", NamaPT : ".$result_query->NamaPT.", Alamat : ".$result_query->Alamat1PT); 
            $this->email->attach($pathDta.$judulattc.".zip");

            //$this->email->send();

			if (!$this->email->send()) {
			   $data['msg'] = show_error($this->email->print_debugger()); }
			  else {
				//unlink($pathDta.$judulattc.".zip");
			    $data['msg'] = "Backup berhasil di kirim";
			  }

					$data['msg'] = "Backup berhasil di kirim";
					$this->load->view('proses/backup/backup', $data);
	}
	
	//===========================================================================
        
        function eksekusi_database()
		{
			ini_set('memory_limit', '2048M');
			$mylib = new globallib();
			$sql = $this->db->query("select PathAplikasi from aplikasi limit 0,1");
			$path_backup = $sql->row();
			$sql->free_result();
	                //print_r($path_backup);die();
			$this->load->dbutil();
			$this->load->helper('file');
			$name = "backupNPM-".date("Y-m-d~H-i-s");
			$prefs = array(
				'format'      => 'zip',             // gzip, zip, txt
				'filename'    => $name.".sql",    // File name - NEEDED ONLY WITH ZIP FILES
			);
			$backup =& $this->dbutil->backup($prefs);
			$output = write_file($path_backup->PathAplikasi.$name.".zip", $backup);
			 
			return $output;
		}
        
        
        
        function createAttach($subjek,$message){
			$pathDta	= set_realpath(APPPATH."BackupNPM");
			$handle = fopen($pathDta.$subjek.".zip", "a");
			fwrite($handle, $message);
			fclose($handle);
		}
		
		function mail()
	{   
            $mylib 		= new globallib();
            $isiattc 	= $this->eksekusi_database();
         
            

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://mx.vci.co.id';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '300';
            									 
            $config['smtp_user']    = 'automail@secretgarden.co.id';
            $config['smtp_pass']    = 'secret2016';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'text'; // or html
            $config['validation']   = TRUE; // bool whether to validate email or not
 
            $this->email->initialize($config);
 
            
		    //*Konfigurasi email keluar melalui mail server*//
        	$judul_email = 'Test';
            $this->email->from('automail@secretgarden.co.id',$judul_email); 
            $this->email->to("irf@vci.co.id;");  //diisi dengan alamat tujuan
            $this->email->cc("irf@vci.co.id;");  //diisi dengan alamat tujuan
            $this->email->Subject('Test'); 
            $this->email->message("Test"); 

            //$this->email->send();

			if (!$this->email->send()) {
			   $data['msg'] = show_error($this->email->print_debugger()); }
			  else {
			
			    $data['msg'] = "Backup berhasil di kirim";
			  }

					$data['msg'] = "Backup berhasil di kirim";
					$this->load->view('proses/backup/backup', $data);
	}
	
}
?>