<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Tutup_bulan_gl extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        //error_reporting(0);                              
        $this->load->library('globallib');
		$this->load->model('proses/tutup_bulan_gl_model');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$user = $this->session->userdata('username');
        	
        	$data['periodesekarang'] = $this->tutup_bulan_gl_model->getPeriodeGL();
			$data['periodeberikut'] = $this->tutup_bulan_gl_model->getBulanDepan();
			$data['periodelalu'] = $this->tutup_bulan_gl_model->getBulanLalu();
			$data['otorisasiuser'] = $this->tutup_bulan_gl_model->OtorisasiUser($user);
			
            $this->load->view('proses/tutupbulan/tutup_bulan_gl', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }     

    
	function simpan(){
		$mylib = new globallib();
		
		$user = $this->session->userdata('username');
		$prdlalu = $mylib->ubah_tanggal($this->input->post('periodelalu'));
		$prdsekarang = $mylib->ubah_tanggal($this->input->post('periodesekarang'));
		$prdberikut = $mylib->ubah_tanggal($this->input->post('periodeberikut'));
		$submit = $this->input->post('submit');
		
		if($submit=='Tutup')	
			$tgl = $prdberikut;
		else
			$tgl = $prdlalu;
			
		
		$this->tutup_bulan_gl_model->updateperiode($tgl);		
		redirect('proses/tutup_bulan_gl');
	}
	
	function test($pcode){
		$hasil = $this->getharga($pcode);
		print_r($hasil);
	}
}

?>