<?php
class kirim_otomatis extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        error_reporting(0);
        $this->load->model('proses/kirim_data_model');
        $this->load->helper('path');
        $this->load->library('email'); 
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
	        $data['track'] = $mylib->print_track();
			$data['msg'] = "";
	        $this->load->view('proses/kirim_oto', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
	function doThis()
	{   
            $mylib 		= new globallib();
            //$tgl2 = date('d-m-Y');
			//list($vtgl, $bulan, $tahun) = explode('-',$tgl2);
			//$tgl1 = '01-'.$bulan.'-'.$tahun;
			
			$tgl1   		= $mylib->ubah_tanggal($this->input->post("tglawal"));
            $tgl2   		= $mylib->ubah_tanggal($this->input->post("tglakhir"));
            
            $isiattc 	= $this->isinya($tgl1,$tgl2);
            
            $query = $this->db->query("select NamaPT,Alamat1PT from aplikasi limit 0,1");
			$result_query = $query->row();
			$query->free_result();
			
			
            //Buat Data Folder di Application BackupNMP
            $pathDta	= set_realpath(APPPATH."emailtemp");
            $judulattc	= "backupCSV-".date("Y-m-d~H-i-s");
            $this->createAttach("$judulattc",$isiattc);

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://mx.vci.co.id';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '300';
            									 
            $config['smtp_user']    = 'automail@secretgarden.co.id';
            $config['smtp_pass']    = 'secret2016';
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'text'; // or html
            $config['validation']   = TRUE; // bool whether to validate email or not
 
            $this->email->initialize($config);
 	
            
		    //*Konfigurasi email keluar melalui mail server*/
        	$judul_email = 'Auto Mail Sales CSV';
        	$sub = 'Auto Mail Sales CSV '.$result_query->NamaPT;
            $this->email->from('automail@secretgarden.co.id',$judul_email); 
            $this->email->to("backup@secretgarden.co.id;");  //diisi dengan alamat tujuan
            $this->email->cc("irf@vci.co.id;");  //diisi dengan alamat tujuan
            $this->email->Subject($sub); 
            $this->email->message("BackupSalesCSV : ".$judulattc.", NamaPT : ".$result_query->NamaPT.", Alamat : ".$result_query->Alamat1PT); 
            $this->email->attach($pathDta.$judulattc.".csv");

            //$this->email->send();

			if (!$this->email->send()) {
			   $data['msg'] = show_error($this->email->print_debugger()); }
			  else {
				unlink($pathDta.$judulattc.".csv");
			    $data['msg'] = "Backup berhasil di kirim";
			  }

			
					$data['msg'] = "Backup berhasil di kirim";
					//$this->load->view('proses/kirim_oto/', $data);
					redirect('/proses/kirim_otomatis/sukses/');
	}
	
	function sukses()
	{   
            $mylib = new globallib();
            $data['track'] = $mylib->print_track();
			$data['msg'] = "Backup berhasil di kirim";
	        $this->load->view('proses/kirim_oto', $data);
	}
	
	function doThis_()
	{   
             $from = 'autoemail95@gmail.com';
			 $to   = 'irf@vci.co.id';
			 $cc   = 'irf@vci.co.id';
			 $subject = 'Auto Email';
			 $message = 'Hello World';

			 $this->load->library('email');

			 $config['mailtype'] = 'html';
			 $config['smtp_port']='465';
			 $config['smtp_timeout']='30';
			 $config['charset']='utf-8';
			 $config['protocol'] = 'smtp';
			 $config['mailpath'] = '/usr/sbin/sendmail';
			 $config['charset'] = 'iso-8859-1';
			 $config['wordwrap'] = TRUE;

			 $this->email->initialize($config);
			 $this->email->from($from);
			 $this->email->to($to);
			 $this->email->cc($cc);
			 $this->email->subject($subject);
			 $this->email->message($message);

			 if (!$this->email->send()) {
			    echo show_error($this->email->print_debugger()); }
			  else {
			    echo "berhasil di kirim";
			  }
	}
	
	
	/*function doThis(){
		
		    $this->load->library('email');
		    
		    $config['protocol'] = "smtp";
			$config['smtp_host'] = "ssl://smtp.gmail.com";
			$config['smtp_port'] = "465";
			$config['smtp_user'] = "autoemail95@gmail.com"; 
			$config['smtp_pass'] = "autoEmail2018";
			$config['charset']  = "utf-8";
			$config['mailtype'] = "html";
			$config['newline'] = "\r\n";
			
			$from = 'autoemail95@gmail.com';
			$to   = 'irf@vci.co.id';
			$subject = 'your subject';
			$message = 'your message';
			
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			
			  if (!$this->email->send()) {
			    echo show_error($this->email->print_debugger()); }
			  else {
			    echo "berhasil di kirim";
			  }
			 
	}*/
	
	
	
	
	//===========================================================================
        
        function isinya($sPrevStart,$sPrevEnd){
            $mylib = new globallib();
            $csv_terminated = "\n";
            $csv_separator  = "#";
            $csv_enclosed   = '';
            $csv_escaped    = "\"";
            $schema_insert  = "";
            $out            = '';
            $batasjudul     = ";";

                $this->load->model('proses/kirim_data_model','chat');
				$kg = '';
                $tabel = array('transaksi_header','transaksi_detail','trans_penerimaan_lain','trans_order_barang_header','opname_header','trans_pengeluaran_lain');
				$kondisi = array('Tanggal','Tanggal','TglDokumen','TglDokumen','TglDokumen','TglDokumen');// kondisi untuk where
                $output  = "";
                $outputD ='';
                for ($i=0;$i<count($tabel);){
                    $outputD .= $this->chat->getReportsDetail($tabel[$i],$kondisi[$i],$sPrevStart,$sPrevEnd,$csv_terminated,$csv_separator,$csv_enclosed,$csv_escaped,$schema_insert,$out,$batasjudul,$kg);
                    $i++;
                }
                $output .= $outputD;
                $tabelH      = array('trans_penerimaan_lain','trans_order_barang_header','opname_header','trans_pengeluaran_lain','trans_pengeluaran_lain');
                $tabelD      = array('trans_penerimaan_lain_detail','trans_order_barang_detail','opname_detail','trans_pengeluaran_lain_detail','trans_pengeluaran_lain_detail');
                $key    = array('NoDokumen','NoDokumen','NoDokumen','NoDokumen','NoDokumen');// kondisi untuk where
                $tgl    = array('TglDokumen','TglDokumen','TglDokumen','TglDokumen','TglDokumen');// kondisi untuk where
                $outputTrxD ='';
                
                for ($c=0;$c<count($tabelH);){
                    $outputTrxD .= $this->chat->getDetailTrx($tabelD[$c],$tabelH[$c],$key[$c],$tgl[$c],$sPrevStart,$sPrevEnd,$csv_terminated,$csv_separator,$csv_enclosed,$csv_escaped,$schema_insert,$out,$batasjudul,$kg);
                    $c++;
                    //echo count($tabelH);die();
                }
                $output .= $outputTrxD;

                return $output;
        }
        
        function isinya_old($sPrevStart,$sPrevEnd){
            $mylib = new globallib();
            $csv_terminated = "\n";
            $csv_separator  = "#";
            $csv_enclosed   = '';
            $csv_escaped    = "\"";
            $schema_insert  = "";
            $out            = '';
            $batasjudul     = ";";

                $this->load->model('proses/kirim_data_model','chat');

             
                $kdC = $this->kirim_data_model->FindCabang();
                $kg = $kdC->KdGU;
                $tabel = array('transaksi_header','transaksi_detail','trans_terima_header','trans_order_header','trans_opname_header','trans_retur_header','trans_lainlain_header');
//                $tabel = array('trans_lainlain_header');
//                $kondisi = array('TglDokumen');// kondisi untuk where
                $kondisi = array('Tanggal','Tanggal','TglDokumen','TglDokumen','TglDokumen','TglDokumen','TglDokumen');// kondisi untuk where
                $output  = "";
                $outputD ='';
                for ($i=0;$i<count($tabel);){
                    $outputD .= $this->chat->getReportsDetail($tabel[$i],$kondisi[$i],$sPrevStart,$sPrevEnd,$csv_terminated,$csv_separator,$csv_enclosed,$csv_escaped,$schema_insert,$out,$batasjudul,$kg);
                    $i++;
                }
                $output .= $outputD;
                // ----- +++ -----
                //$tabelH      = array('trans_terima_header','trans_order_header','trans_opname_header','trans_retur_header','trans_lainlain_header');
                //$tabelD      = array('trans_terima_detail','trans_order_detail','trans_opname_detail','trans_retur_detail','trans_lainlain_detail');
                $tabelH      = array('trans_penerimaan_lain','trans_order_barang_header','opname_header','trans_pengeluaran_lain','trans_pengeluaran_lain');
                $tabelD      = array('trans_penerimaan_lain_detail','trans_order_barang_detail','opname_detail','trans_pengeluaran_lain_detail','trans_pengeluaran_lain_detail');
                $key    = array('NoDokumen','NoDokumen','NoDokumen','NoDokumen','NoDokumen');// kondisi untuk where
                $tgl    = array('TglDokumen','TglDokumen','TglDokumen','TglDokumen','TglDokumen');// kondisi untuk where
                $outputTrxD ='';
                
                for ($c=0;$c<count($tabelH);){
                    $outputTrxD .= $this->chat->getDetailTrx($tabelD[$c],$tabelH[$c],$key[$c],$tgl[$c],$sPrevStart,$sPrevEnd,$csv_terminated,$csv_separator,$csv_enclosed,$csv_escaped,$schema_insert,$out,$batasjudul,$kg);
                    $c++;//echo count($tabelH);die();
                }
                $output .= $outputTrxD;

                return $output;
        }
        
        
        
        function createAttach($subjek,$message){
			$pathDta	= set_realpath(APPPATH."emailtemp");
			$handle = fopen($pathDta.$subjek.".csv", "a");
			fwrite($handle, $message);
			fclose($handle);
		}
		
		/**
		$mylib 		= new globallib();
            $tgl1   		= $mylib->ubah_tanggal($this->input->post("tglawal"));
            $tgl2   		= $mylib->ubah_tanggal($this->input->post("tglakhir"));
            //$isiattc 	= $this->isinya($tgl1,$tgl2);// buat attc nya
            $isiattc 	= '';// buat attc nya
            $pathDta		= set_realpath(APPPATH."emailtemp");
            $judulattc	= "BackupRetail";
            $this->createAttach("$judulattc",$isiattc);

            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '30';
            									 
            //$config['smtp_user']    = 'herborist2014@gmail.com';
            //$config['smtp_pass']    = 'omah2017';
            
            $config['smtp_user']    = 'sumber.herbal.rezeki.bali@gmail.com';
            $config['smtp_pass']    = 'sumber2018';
            
            $config['charset']      = 'utf-8';
            $config['newline']      = "\r\n";
            $config['mailtype']     = 'text'; // or html
            $config['validation']   = TRUE; // bool whether to validate email or not
 
            $this->email->initialize($config);
 
            
		    //*Konfigurasi email keluar melalui mail server
            $kdC = $this->kirim_data_model->FindCabang();
            //$kg = $kdC->KdGU;
            $kg = '';
            //$cb = $kdC->Keterangan;
            $cb = '';
            //$this->email->from('herborist2014@gmail.com','Omah Herborist'); 
            //$this->email->to("dataomah@vci.co.id");  //diisi dengan alamat tujuan
            //$this->email->cc("irf@vci.co.id");  //diisi dengan alamat tujuan
            
            $this->email->from('sumber.herbal.rezeki.bali@gmail.com','SUMBER HERBAL REZEKI BALI'); 
            $this->email->to("irf@vci.co.id");  //diisi dengan alamat tujuan
            $this->email->cc("irf@vci.co.id");  //diisi dengan alamat tujuan
            
            
            $this->email->Subject('Backup Otomatis'); 
            $this->email->message("BackupRetail : ".$kg."-".$cb.$tgl1."s/d".$tgl2); 
            $this->email->attach($pathDta.$judulattc.".csv");

            //$this->email->send();

			if (!$this->email->send()) {
			   $data['msg'] = show_error($this->email->print_debugger()); }
			  else {
				 unlink($pathDta.$judulattc.".csv");
			    $data['msg'] = "Backup berhasil di kirim";
			  }

					$data['msg'] = "Backup berhasil di kirim";
					$this->load->view('proses/kirim_oto', $data);
		*/
}
?>