<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class bulanaktif extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('proses/bulanaktif_model');
		$this->load->model('globalmodel');
    }
	
	function getTahun(){
      $result = $this->globalmodel->getTahun();
      echo json_encode($result);
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		$userid     = $this->session->userdata('userid');
		$bulanaktif     = $this->session->userdata('bulanaktif');
		$tahunaktif     = $this->session->userdata('tahunaktif');
		$tanggaltrans   = $this->session->userdata('Tanggal_Trans');
    	if($sign=="Y")
		{
		    $data['bulanaktif'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['tanggaltrans'] = $tanggaltrans;
			$data['tahun'] = $this->bulanaktif_model->getListTahun();
			$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['track'] = $mylib->print_track();
			$this->load->view('proses/bulanaktif/bulanaktif',$data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function save()
	{
	    $mylib = new globallib();
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$userid  = $this->session->userdata('userid');		
		$this->db->update('user', array("Bulan"=>$bulan,"Tahun"=>$tahun),array('id'=>$userid));
		$sessiondata = array(
				   'bulanaktif' => $bulan,
				   'tahunaktif' => $tahun
               );
		$this->session->set_userdata($sessiondata);
		redirect("start/");
	}
}
?>