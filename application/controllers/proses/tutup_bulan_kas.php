<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Tutup_bulan_kas extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        //error_reporting(0);                              
        $this->load->library('globallib');
		$this->load->model('proses/tutup_bulan_kas_model');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$user = $this->session->userdata('username');
        	
        	$data['periodesekarang'] = $this->tutup_bulan_kas_model->getPeriodeKasBank();
			$data['periodeberikut'] = $this->tutup_bulan_kas_model->getBulanDepan();
			
            $this->load->view('proses/tutupbulan/tutup_bulan_kas', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }     

    
	function simpan(){
		$mylib = new globallib();
		
		$prdsekarang = $mylib->ubah_tanggal($this->input->post('periodesekarang'));
		$prdberikut = $mylib->ubah_tanggal($this->input->post('periodeberikut'));
			
		$this->tutup_bulan_kas_model->updatesaldo($prdsekarang, $prdberikut);		
		$this->tutup_bulan_kas_model->updateperiode($prdberikut);	
		
		redirect('proses/tutup_bulan_kas');
	}
	
	function test($pcode){
		$hasil = $this->getharga($pcode);
		print_r($hasil);
	}
}

?>