<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tarik_data_outlet extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->model('proses/tarik_data_outlet_model');
    }
    
    function index(){
        ini_set('display_errors', 1);
    	$date=date('Y-m-d');
    	$kdstore = '';
    	$store = $this->tarik_data_outlet_model->getOutlet($kdstore);
    	$err_text  ='';
    	for($i=0;$i<count($store);$i++){
			$sourcedb = $this->load->database($store[$i]['DbName'], TRUE);
			$connected = $sourcedb->initialize();
			if (!$connected){
    			echo 'Tidak dapat terhubung ke database ' . $store[$i]['NamaOutlet'] .'<br>';
    			$err_text .=  "<pre>" . 'Tidak dapat terhubung ke database ' . $store[$i]['NamaOutlet'] ."</pre>\n";
    			$status='Gagal';
    		}else{
				if($store[$i]['SkipImport']==0){
					echo 'Start Import data  ' . $store[$i]['NamaOutlet'] . '  ' . date('Y-m-d h:i:s') .'<br>';
					$sourcedb->conn_id = ($sourcedb->pconnect == FALSE) ? $sourcedb->db_connect() : $sourcedb->db_pconnect();
                    echo '      data Header <br>';
					$this->tarik_data_outlet_model->TransaksiHeader($store[$i]['Kassa'],$sourcedb);  
                    echo '      data Detail <br>';
					$this->tarik_data_outlet_model->TransaksiDetail($store[$i]['Kassa'],$sourcedb);
                    echo '      data Detail Voucher <br>';
					$this->tarik_data_outlet_model->TransaksiDetailVoucher($store[$i]['Kassa'],$sourcedb);
                    echo '      data Detail Bayar <br>';
					$this->tarik_data_outlet_model->TransaksiDetailBayar($store[$i]['Kassa'],$sourcedb);	

                    // update voucher karyawan all counter  data vocuher blm bener
                    $this->SinkronVoucherKaryawan($date,$sourcedb);

					$status = 'Berhasi';
					echo 'End data  ' . $store[$i]['NamaOutlet'] . '  ' . date('Y-m-d h:i:s') .'<br>';
				}
			}
			// $this->tarik_data_outlet_model->InsertStatus($store[$i]['NamaOutlet'], $status);
		}
    	
    	echo 'Finish..' . date('Y-m-d h:i:s');
		/*
    	if($err_text !=''){
			//kirim telegram ke support
			$this->load->library('telegramlib');
    		$telegram = new telegramlib();
    		$telegram->chat_id('504750510'); //elly
			$telegram->text($err_text);
			$telegram->send();
    		
    		$telegram->chat_id('188790806'); //tonny
			$telegram->text($err_text);
			$telegram->send();
			
			
			// $telegram->chat_id('523261461'); //krisna
			// $telegram->text($err_text);
			// $telegram->send();
		}
		*/
    }
    
    function tanggal($date,$kdstore = ''){
    	$store = $this->tarik_data_outlet_model->getOutlet($kdstore);
    	$err_text  ='';
    	for($i=0;$i<count($store);$i++){
			$sourcedb = $this->load->database($store[$i]['DbName'], TRUE);
			$connected = $sourcedb->initialize();
			if (!$connected){
    			echo 'Tidak dapat terhubung ke database ' . $store[$i]['NamaOutlet'] .'<br>';
    			$err_text .=  "<pre>" . 'Tidak dapat terhubung ke database ' . $store[$i]['NamaOutlet'] ."</pre>\n";
    			$status='Gagal';
    		}else{
    			echo 'Import data  ' . $store[$i]['NamaOutlet'] .'<br>';
				$sourcedb->conn_id = ($sourcedb->pconnect == FALSE) ? $sourcedb->db_connect() : $sourcedb->db_pconnect();
				$this->tarik_data_outlet_model->TransaksiHeaderPerTgl($date,$store[$i]['Kassa'],$sourcedb);   
	    		$this->tarik_data_outlet_model->TransaksiDetailPerTgl($date,$store[$i]['Kassa'],$sourcedb);
	    		$this->tarik_data_outlet_model->TransaksiDetailVoucherperTgl($date,$store[$i]['Kassa'],$sourcedb);
	    		$this->tarik_data_outlet_model->TransaksiDetailBayarperTgl($date,$store[$i]['Kassa'],$sourcedb);	
	    		$status = 'Berhasi';
			}
			$this->tarik_data_outlet_model->InsertStatus($store[$i]['NamaOutlet'], $status);
		}
    	
    	echo 'Finish..' . date('Y-m-d h:i:s');
    	if($err_text !=''){
			//kirim telegram ke support
			$this->load->library('telegramlib');
    		$telegram = new telegramlib();
    		$telegram->chat_id('504750510'); //elly
			$telegram->text($err_text);
			$telegram->send();
    		
    		$telegram->chat_id('188790806'); //tonny
			$telegram->text($err_text);
			$telegram->send();
		}
    }

    function peroutlet_perbulan($kdOutlet='',$tahun='',$bulan=''){
        if($kdOutlet == ''){
            echo "Silahkan isi kode outlet lihat di table outlet ambil ID, <br> contoh http://natura/index.php/proses/tarik_data_outlet/peroutlet_perbulan/3/2020/07";die();
        }else{
            $cek = $this->db->get_where('outlet',array('ID'=>$kdOutlet))->num_rows();

            if($cek > 0){
                $kode_outlet = $kdOutlet;
            }else{
              echo "Kode outlet salah lihat di table outlet ambil ID, <br> contoh http://natura/index.php/proses/tarik_data_outlet/peroutlet_perbulan/3/2020/07";die();  
            }
        }

        if($tahun == ''){
            echo "Silahkan isi tahun, <br> contoh http://natura/index.php/proses/tarik_data_outlet/peroutlet_perbulan/3/2020/07";die();
        }

        if($bulan == ''){
            echo "Silahkan isi bulan, <br> contoh http://natura/index.php/proses/tarik_data_outlet/peroutlet_perbulan/3/2020/07";die();
        }

        $store = $this->tarik_data_outlet_model->getOutlet($kode_outlet);
        $err_text  ='';
        for($i=0;$i<count($store);$i++){
            $sourcedb = $this->load->database($store[$i]['DbName'], TRUE);
            $connected = $sourcedb->initialize();
            if (!$connected){
                echo 'Tidak dapat terhubung ke database ' . $store[$i]['NamaOutlet'] .'<br>';
                $err_text .=  "<pre>" . 'Tidak dapat terhubung ke database ' . $store[$i]['NamaOutlet'] ."</pre>\n";
                $status='Gagal';
            }else{
                if($store[$i]['SkipImport']==0){
                    echo 'Import data  ' . $store[$i]['NamaOutlet'] .'<br>';
                    $sourcedb->conn_id = ($sourcedb->pconnect == FALSE) ? $sourcedb->db_connect() : $sourcedb->db_pconnect();
                    $this->tarik_data_outlet_model->TransaksiHeaderPerOutletPerBulan($tahun,$bulan,$store[$i]['Kassa'],$sourcedb);   
                    $this->tarik_data_outlet_model->TransaksiDetailPerOutletPerBulan($tahun,$bulan,$store[$i]['Kassa'],$sourcedb);
                    $this->tarik_data_outlet_model->TransaksiDetailVoucherPerOutletPerBulan($tahun,$bulan,$store[$i]['Kassa'],$sourcedb);
                    $this->tarik_data_outlet_model->TransaksiDetailBayarPerOutletPerBulan($tahun,$bulan,$store[$i]['Kassa'],$sourcedb);

                    // update voucher karyawan all counter  data vocuher blm bener
                    $this->SinkronVoucherKaryawan($date,$sourcedb);

                    $status = 'Berhasi';
                }
            }
            $this->tarik_data_outlet_model->InsertStatus($store[$i]['NamaOutlet'], $status);
        }
        
        echo 'Finish..' . date('Y-m-d h:i:s');
    }
    
    public function SinkronVoucherKaryawan($date,$conn){
          $bulan = date('m',strtotime($date));
          $tahun = date('Y',strtotime($date));

          $tahunBulan = $tahun."-".$bulan;
          $Query1 = "
                    SELECT tbl.NomorVoucher,SUM(tbl.total_voucher) AS TotalVoucherPakai FROM 
                    (
                      SELECT
                      'sgv' AS flag,
                      a.NomorVoucher,
                      SUM(a.NilaiVoucher) AS total_voucher
                      FROM
                      transaksi_detail_voucher a
                      INNER JOIN transaksi_header b ON a.NoStruk = b.NoStruk
                      WHERE a.Jenis = '3' AND DATE_FORMAT(a.Tanggal,'%Y-%m') = '$tahunBulan' AND b.Status = '1'
                      GROUP BY a.NomorVoucher
                      UNION ALL
                      SELECT
                      'sunset' AS flag,
                      c.NomorVoucher,
                      SUM(c.NilaiVoucher) AS total_voucher
                      FROM
                      transaksi_detail_voucher_sunset c
                      INNER JOIN transaksi_header_sunset d ON c.NoStruk = d.NoStruk
                      WHERE c.Jenis = '3'  AND DATE_FORMAT(c.Tanggal,'%Y-%m') = '$tahunBulan' AND d.Status = '1' 
                      GROUP BY c.NomorVoucher
                    ) tbl GROUP BY tbl.NomorVoucher
                  ";

          $excuteQuery1 = $this->db->query($Query1)->result_array();

          if(!empty($excuteQuery1)){

              foreach ($excuteQuery1 as $key => $value) {
                  $nik = $value['NomorVoucher'];
                  $totalVoucherPakai = $value['TotalVoucherPakai'];

                  $Query2 = "UPDATE voucher_employee SET terpakai = $totalVoucherPakai WHERE nik = '$nik' AND DATE_FORMAT(expDate,'%Y-%m') = '$tahunBulan'  ";
                  $this->db->query($Query2);
              }  
          }

          //update ke outlet
          $Query3 = "SELECT * FROM voucher_employee WHERE  DATE_FORMAT(expDate,'%Y-%m') = '$tahunBulan'  " ; 
          $excuteQuery3 = $this->db->query($Query3)->result_array();
          foreach ($excuteQuery3 as $key => $value) {
              $nik = $value['nik'];
              $nominal = $value['nominal'];
              $expDate = $value['expDate'];
              $addDate = $value['addDate'];
              $userAdd = $value['userAdd'];
              $status = $value['status'];
              $keterangan = $value['keterangan'];
              $terpakai = $value['terpakai'];

              $Query4 = "SELECT * FROM voucher_employee WHERE nik = '$nik' AND DATE_FORMAT(expDate,'%Y-%m') = '".date('Y-m',strtotime($expDate))."'  ";
              $cek4 = $conn->query($Query4)->num_rows();
              if($cek4 > 0){
                    $Query5 = "UPDATE voucher_employee SET nominal = '$nominal',addDate = '$addDate',userAdd = '$userAdd',status = '$status',keterangan = '$keterangan',terpakai = '$terpakai' WHERE nik = '$nik' AND DATE_FORMAT(expDate,'%Y-%m') = '".date('Y-m',strtotime($expDate))."' ";
                    $conn->query($Query5);
              }else{
                    $Query6 = "INSERT INTO voucher_employee SET nik = '$nik',nominal = '$nominal', expDate = '$expDate',addDate = '$addDate',userAdd = '$userAdd',status = '$status',keterangan = '$keterangan',terpakai = '$terpakai' ";
                    $conn->query($Query6);
              }

              //insert ke voucher yg jenis 3 (buat diskon karyawan)
              $Query7 = "SELECT * FROM voucher WHERE novoucher = '$nik' AND jenis = '3' ";
              $cek7 = $conn->query($Query7)->num_rows();
              if($cek7 <= 0){
                    $Query6 = "INSERT INTO voucher SET novoucher = '$nik',nominal = '0',keterangan = '$keterangan',jenis = '3',rupdisc = 'D'  ";
                    $conn->query($Query6);
              }
          }
    }

    public function hitung_ulang_voucher_employee($date){
        // $date = date("Y-m-d");
          $bulan = date('m',strtotime($date));
          $tahun = date('Y',strtotime($date));

          $tahunBulan = $tahun."-".$bulan;
          $Query = "
                    SELECT tbl.NomorVoucher,SUM(tbl.total_voucher) AS TotalVoucherPakai FROM 
                    (
                      SELECT
                      'sgv' AS flag,
                      a.NomorVoucher,
                      SUM(a.NilaiVoucher) AS total_voucher
                      FROM
                      transaksi_detail_voucher a
                      INNER JOIN transaksi_header b ON a.NoStruk = b.NoStruk
                      WHERE a.Jenis = '3' AND DATE_FORMAT(a.Tanggal,'%Y-%m') = '$tahunBulan' AND b.Status = '1'
                      GROUP BY a.NomorVoucher
                      UNION ALL
                      SELECT
                      'sunset' AS flag,
                      c.NomorVoucher,
                      SUM(c.NilaiVoucher) AS total_voucher
                      FROM
                      transaksi_detail_voucher_sunset c
                      INNER JOIN transaksi_header_sunset d ON c.NoStruk = d.NoStruk
                      WHERE c.Jenis = '3'  AND DATE_FORMAT(c.Tanggal,'%Y-%m') = '$tahunBulan' AND d.Status = '1' 
                      GROUP BY c.NomorVoucher
                    ) tbl GROUP BY tbl.NomorVoucher
                  ";

          $excuteQuery = $this->db->query($Query)->result_array();

          if(!empty($excuteQuery)){

              foreach ($excuteQuery as $key => $value) {
                  $nik = $value['NomorVoucher'];
                  $totalVoucherPakai = $value['TotalVoucherPakai'];

                  $Query = "UPDATE voucher_employee SET terpakai = $totalVoucherPakai WHERE nik = '$nik' AND DATE_FORMAT(expDate,'%Y-%m') = '$tahunBulan'  ";
                  echo $Query."<br>";
                  $excuteQuery = $this->db->query($Query);
              }  
          }

          echo "selesai";
    }   
}
?>