<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Tutup_bulan extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        //error_reporting(0);                              
        $this->load->library('globallib');
		$this->load->model('proses/tutup_bulan_model');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$user = $this->session->userdata('username');
        	
        	$data['periodesekarang'] = $this->tutup_bulan_model->getPeriode();
			$data['periodeberikut'] = $this->tutup_bulan_model->getBulanDepan();
			$data['periodelalu'] = $this->tutup_bulan_model->getBulanLalu();
			$data['otorisasiuser'] = $this->tutup_bulan_model->OtorisasiUser($user);
			
            $this->load->view('proses/tutupbulan/tutup_bulan', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }     

    
	function simpan($type){
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');
		
		$mylib = new globallib();
		$this->load->library('stocklib');
		
		$user = $this->session->userdata('username');
			
		$prdlalu = $mylib->ubah_tanggal($this->tutup_bulan_model->getBulanLalu());
		$prdsekarang = $mylib->ubah_tanggal($this->tutup_bulan_model->getPeriode());
		$prdberikut = $mylib->ubah_tanggal($this->tutup_bulan_model->getBulanDepan());
		$bukatutup = $type;
		
		list($thnsekarang,$blnsekarang,$tglsekarang) = explode('-',$prdsekarang);
		list($thnberikut,$blnberikut,$tglberikut) = explode('-',$prdberikut);
		
		if($bukatutup=='T'){
			$tgl = $prdberikut;
			print 'data:Mulai' . PHP_EOL . PHP_EOL;		
			flush();
			print 'data:Proses Kas/Bank' . PHP_EOL . PHP_EOL;
			flush();
			$this->tutup_bulan_model->updatesaldokas($prdsekarang, $prdberikut);
			print 'data:Proses Hutang' . PHP_EOL . PHP_EOL;
			flush();
			$this->tutup_bulan_model->HitungHutang($prdsekarang, $prdberikut);
			print 'data:Hitung Stock Periode Sekarang' . PHP_EOL . PHP_EOL;
			flush();
			$this->stocklib->recalculate_stock_bulanan($thnsekarang, $blnsekarang);
			print 'data:Hitung Stock Periode Berikut' . PHP_EOL . PHP_EOL;
			flush();
			$this->stocklib->recalculate_stock_bulanan($thnberikut, $blnberikut);
		}else{
			$tgl = $prdlalu;
		}	
		print 'data:Selesai' . PHP_EOL . PHP_EOL;
		flush();
		$this->tutup_bulan_model->updateperiode($tgl);		
		redirect('proses/tutup_bulan');
	}
	
	function test($prdsekarang,$prdberikut){
		$mylib = new globallib();
		$this->load->library('stocklib');
		
		$user = $this->session->userdata('username');
		
		$bukatutup = 'T';
		
		list($thnsekarang,$blnsekarang,$tglsekarang) = explode('-',$prdsekarang);
		list($thnberikut,$blnberikut,$tglberikut) = explode('-',$prdberikut);
		
		if($bukatutup=='T'){
			$tgl = $prdberikut;
			print 'data:Mulai' . PHP_EOL . PHP_EOL;		
			flush();
			print 'data:Proses Kas/Bank' . PHP_EOL . PHP_EOL;
			flush();
			$this->tutup_bulan_model->updatesaldokas($prdsekarang, $prdberikut);
			print 'data:Proses Hutang' . PHP_EOL . PHP_EOL;
			flush();
			$this->tutup_bulan_model->HitungHutang($prdsekarang, $prdberikut);
			print 'data:Hitung Stock Periode Sekarang' . PHP_EOL . PHP_EOL;
			flush();
			$this->stocklib->recalculate_stock_bulanan($thnsekarang, $blnsekarang);
			print 'data:Hitung Stock Periode Berikut' . PHP_EOL . PHP_EOL;
			flush();
			$this->stocklib->recalculate_stock_bulanan($thnberikut, $blnberikut);
		}else{
			$tgl = $prdlalu;
		}	
		print 'data:Selesai' . PHP_EOL . PHP_EOL;
		flush();
		$this->tutup_bulan_model->updateperiode($tgl);		
		redirect('proses/tutup_bulan');
	}
	
	function test2($prdsekarang,$prdberikut){
		$this->tutup_bulan_model->HitungHutang($prdsekarang, $prdberikut);
	}
}

?>