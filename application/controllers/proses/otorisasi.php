<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class otorisasi extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('proses/otorisasi_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);
            $id = $this->input->post('stSearchingKey');
            $id2 = $this->input->post('date1');
            $with = $this->input->post('searchby');
            if ($with == "TglDokumen") {
                $id = $mylib->ubah_tanggal($id2);
            }
            $this->load->library('pagination');

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = '</span>';
            $config['per_page'] = '12';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['base_url'] = base_url() . 'index.php/transaksi/order_barang/index/';
            $page = $this->uri->segment(4);
            $config['uri_segment'] = 4;
            $flag1 = "";
            if ($with != "") {
                if ($id != "" && $with != "") {
                    $config['base_url'] = base_url() . 'index.php/transaksi/order_barang/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                } else {
                    $page = "";
                }
            } else {
                if ($this->uri->segment(5) != "") {
                    $with = $this->uri->segment(4);
                    $id = $this->uri->segment(5);
                    if ($with == "TglDokumen") {
                        $id = $mylib->ubah_tanggal($id);
                    }
                    $config['base_url'] = base_url() . 'index.php/transaksi/order_barang/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                }
            }
            //$data['bulan'] = $this->session->userdata('bulanaktif');
            //$data['tahun'] = $this->session->userdata('tahunaktif');
            //echo $thnbln = $data['tahun'].$data['bulan'];die();
            $data['header'] = array("No Dokumen", "Tanggal", "Gudang", "Supplier", "Keterangan", "Jumlah", "PPn %", "Total", "Otorisasi");
            $config['total_rows'] = $this->otorisasi_model->num_order_row($id, $with);
            $data['data'] = $this->otorisasi_model->getorderList($config['per_page'], $page, $id, $with);

            $data['track'] = $mylib->print_track();
            $this->pagination->initialize($config);
            $this->load->view('proses/otorisasi/otr_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $aplikasi = $this->otorisasi_model->getDate();
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $tgl = '01' . '-' . $data['bulan'] . '-' . $data['tahun'];
            $data['tanggal'] = $aplikasi->TglTrans;
            if (($data['tahun'] == substr($data['tanggal'], -4)) && ($data['bulan'] == substr($data['tanggal'], 3, 2)))
                $data['tanggal'] = $aplikasi->TglTrans;
            else
                $data['tanggal'] = $tgl;
            $data['gudang'] = $this->otorisasi_model->getGudang();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/order_barang/add_order_barang', $data);
        }
        else {
            $this->load->view('denied');
        }
    }

    function view_product_tag($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['viewproduct_tag'] = $this->otorisasi_model->getDetail($id);
            $data['edit'] = false;
            $this->load->view('master/product_tag/vieweditproduct_tag', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function view_otorisasi($id) {
        $data['header'] = $this->otorisasi_model->getHeader($id);
        $this->load->view('proses/otorisasi/otr_proses', $data);
    }

    function save_otorisasi() {
        $id = $this->input->post('nodok');
        $flag = $this->input->post('rbtotorisasi');
        $ketOto = $this->input->post('ketreject');
        $data = array(
            'FlagOtorisasi' => $flag,
            'KetOtorisasi' => $ketOto
        );

//        $this->db->where('NoDokumen', $id);


        $dsn = "mysql://natura:dbnatura@192.168.7.3/gl_natura";
//        $dsn = "mysql://natura:dbnatura@localhost/gl_natura";
        $db_obj = $this->load->database($dsn,TRUE);
        if($db_obj->conn_id) {
            //do something
            //$this->dba = $this->load->database($dsn, true);
            //$this->dba->delete($table, $where);

            $nil = 0;
            $sukses = $db_obj->update('otorisasi_header', $data, array('NoDokumen' => $id));
            $sukses2 = $db_obj->update('trans_order_barang_header', array('FlagOtorisasi' => $flag), array('NoDokumen' => $id));
            if ($sukses) {
                $nil = $nil + 0;//echo "berhasil".$dsn ."<br>";
            } else {
                $nil =  $nil + 1;//echo "gagal".$dsn."<br>";
            }
            if ($sukses2) {
                $nil =  $nil +  0;//echo "berhasil".$dsn ."<br>";
            } else {
                $nil =  $nil + 1;//echo "gagal".$dsn."<br>";
            }
            if($nil== 0){ //jika update berhasil maka update ke diri sendiri
                $this->db->update('otorisasi_header', $data, array('NoDokumen' => $id));
//                $this->db->update('trans_order_barang_header', array('FlagOtorisasi' =>$flag), array('NoDokumen' => $id));
            }
        }
            //$this->dba->close();
            $db_obj->close();
    }

    function cetak() {
        $data = $this->varCetak();
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
    }

    function printThis() {
        $data = $this->varCetak();
        $id = $this->uri->segment(4);
        $data['fileName2'] = "order_barang.sss";
        $data['fontstyle'] = chr(27) . chr(80); //$data['fontstyle'] = chr(27).chr(80);
        //$data['fontstylejudul'] =  chr(27).chr(107).chr(0);
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_PO', $data);
    }

    function varCetak() {
        $this->load->library('printreportlib');
        $mylib = new printreportlib();
        $id = $this->uri->segment(4);
        $header = $this->otorisasi_model->getHeader($id);
//print_r($header);
        $data['header'] = $header;
        $data['alamat'] = $this->otorisasi_model->getalamat();
        $detail = $this->otorisasi_model->getDetail($id);
        $data['judul1'] = array("Tanggal", "No PO", "Tanggal Kirim", "Keterangan");
        $data['niljudul1'] = array($header->Tanggal, $header->NoDokumen, $header->TglKirim, stripslashes($header->Keterangan)); //wsc
        $data['judul2'] = array("Supplier", "", "", "");
        $data['niljudul2'] = array(stripslashes($header->Nama), stripslashes($header->kontak), stripslashes($header->Kota), stripslashes($header->TelpSupp));
        $data['judullap'] = "PURCHASE ORDER";
        $data['url'] = "order_barang/printThis/" . $id;
        $data['colspan_line'] = 4;
        $data['tipe_judul_detail'] = array("normal", "normal", "kanan", "normal", "normal", "kanan", "kanan", "kanan", "kanan");
        $data['lebar_detail'] = array(11, 32, 8, 10, 15, 8, 15, 8, 16); // total 95
        $data['judul_detail'] = array("Kode Barang", "Nama Barang", "Qty", "Satuan", "Harga", "Disc", "Potongan", "PPn", "Jumlah");
        $data['panjang_kertas'] = 60;
        $default_page_written = 28;
        $data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $default_page_written;
        if ($data['panjang_per_hal'] != 0) {
            $data['tot_hal'] = ceil((int) count($detail) / (int) $data['panjang_per_hal']);
        } else {
            $data['tot_hal'] = 1;
        }

        $list_detail = array();
        $detail_page = array();
        $counterRow = 0;
        $max_field_len = array(0, 0, 0, 0, 0, 0, 0, 0, 0); //8 kolom
        //$max_field_len = array(0,0,0,0,0,0,0,0);
        for ($m = 0; $m < count($detail); $m++) {
            unset($list_detail);
            $counterRow++;

            //wsc - 211014
            $dataext = $this->otorisasi_model->getExtCode($header->KdSupplier, $detail[$m]['PCode']);
            if ($dataext) {
                if ($dataext->Pcode == $detail[$m]['PCode']) {
                    $list_detail[] = stripslashes($dataext->PCodeExt);
                    $list_detail[] = stripslashes($dataext->NamaExt);
                }
            } else {
                $list_detail[] = stripslashes($detail[$m]['PCode']);
                $list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
            }

            $list_detail[] = $detail[$m]['QtyInput'];
            $list_detail[] = $detail[$m]['NamaSatuan'];
            $list_detail[] = number_format($detail[$m]['HargaB'], 2, ",", ".");
            $list_detail[] = ($detail[$m]['Disc1'] != 0 || $detail[$m]['Disc2'] != 0 ) ? number_format($detail[$m]['Disc1'], 0, ",", ".") . "%" . " + " . number_format($detail[$m]['Disc2'], 0, ",", ".") . "%" : "0%";
            $list_detail[] = number_format($detail[$m]['Potongan'], 0, ",", ".");
            $list_detail[] = number_format($detail[$m]['PPnB'], 0, ",", ".") . "%";
            $list_detail[] = number_format($detail[$m]['TotalB'], 2, ",", ".");
            $detail_page[] = $list_detail;
            $max_field_len = $mylib->get_max_field_len($max_field_len, $list_detail);
            if ($data['panjang_per_hal'] != 0) {
                if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
                    $data['detail'][] = $detail_page;
                    if ($m != count($detail) - 1) {
                        unset($detail_page);
                    }
                }
            }
        }
        $data['detail'][] = $detail_page;

        /* perhitungan footer */
        $footjumlah = round($header->Jumlah);
//    $DiscHarga      = round($header->DiscHarga);
        $nilPPN = round($header->PPn / 100 * ($header->Jumlah - $header->DiscHarga));
        $ttlBI = round($footjumlah + $nilPPN);
        /* end hitung */

        $data['footer1'] = array("Total", "PPn", "Total Order");
        $data['footer2'] = array(number_format($footjumlah, 2, ",", "."), number_format($nilPPN, 2, ",", "."), number_format($ttlBI, 2, ",", "."));
        $data['brs_footer'] = array(15, 3, 20); // 'judul' ,':', nilai
//nb : total karakter 80 tanpa $cond=chr(15);
        $data['max_field_len'] = $max_field_len;
        $data['banyakBarang'] = $counterRow;
        $data['fontstylejudul'] = chr(27) . chr(71);
        $data['fontstyleketeranganfooter'] = chr(27) . chr(71);
        $data['nfontstyle'] = chr(27) . chr(71);
        $data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
        $data['keterangan'] = "Alamat Pengiriman"; //wsc
        $data['note1'] = "1. Setiap Pengiriman Barang Harap Disertakan Copy PO"; //wsc - 110914
        $data['note2'] = "2. Setiap <b>Hari Libur Tidak</b> Menerima Pengiriman Barang Dari Supplier";
        $data['string1'] = "     Dibuat Oleh               Mengetahui             Menyetujui";
        $data['string2'] = "(                     ) (                     )  (                      )";

        //$data['fileName'] = "order_barang.sss";
        return $data;
    }

    function kirim_otr() {
        $id = $this->input->post('kode'); //die();
//        $this->otorisasi_model->locktables('trans_order_barang_header,trans_order_barang_detail');
//        $cabang = $this->Global_cabang_model->getField("select * from  cabang_depo WHERE id_cd='$c'");
        $cabang = ""; // injek ip terlebih dahulu
        $hdr = $this->Insert_OTR_Header($id, $cabang);
        $dtl = $this->Insert_OTR_Detail($id, $cabang);
        $hsl = $hdr + $dtl;
        if ($hsl == 0) {
            $sts = 'S';
        } else { // ada gagal
            $sts = 'B';
        }

        $this->db->update('trans_order_barang_header', array('FlagKirimOTR' => '$sts'), array('NoDokumen' => $id));
        echo $sts;
//        $this->otorisasi_model->unlocktables();
        //echo $StatusOrder->Item."*".$StatusOrder->Item."*".$StatusOrder->Status;
    }

    function edit_order($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $aplikasi = $this->otorisasi_model->getDate();
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->otorisasi_model->getHeader($id);
            $data['detail'] = $this->otorisasi_model->getDetail($id);
            $data['mgudang'] = $this->otorisasi_model->getGudang();
            $data['gudang'] = $data['header']->KdGudang;
            if ($data['header']->Payment == "C") {
                $data['checkedc'] = "checked='checked'";
                $data['checkedk'] = "";
                $data['topdisabled'] = "disabled";
            } else {
                $data['checkedk'] = "checked='checked'";
                $data['checkedc'] = "";
                $data['topdisabled'] = "";
            }
            $data['track'] = $mylib->print_track();
            if ($data['header']->FlagPengiriman == 'T' && $data['header']->FlagTutup == 'T') {
                $this->load->view('transaksi/order_barang/edit_order_barang', $data);
            } else {
                redirect('/transaksi/order_barang/');
            }
        } else {
            $this->load->view('denied');
        }
    }

    function getSupplier() {
        $kode = strtoupper($this->input->post('kdsupplier'));
        $detail = $this->otorisasi_model->getSupplier($kode);
        if (count($detail) != 0) {
            $nilai = $detail->Nama . "*_*" . $detail->Payment . "*_*" . $detail->TOP . "*_*" . $detail->LimitKredit . "*_*" . $detail->LimitFaktur . "*_*" . $detail->PPn . "*_*" . $detail->KdGroupext;
        } else {
            $nilai = "";
        }
        echo $nilai;
    }


    function save_new_item() {
        $mylib = new globallib();
        $user = $this->session->userdata('userid');
        $flag = $this->input->post('flag');
        $no = $this->input->post('no');
        $tgl = $this->input->post('tgl');
        $kdsupplier = strtoupper($this->input->post('kdsupplier'));
        $kdgroupext = strtoupper($this->input->post('kdgroupext'));
        $kdgudang = strtoupper($this->input->post('gudang'));
        $payment = $this->input->post('sumber');
        $top = $this->input->post('top');
        $tglkirim = $this->input->post('tglkirim');
        $ket = trim(strtoupper(addslashes($this->input->post('ket'))));
        $jumlah = $this->input->post('jumlah');
        $ppn = $this->input->post('ppn');
        $nilaippn = $this->input->post('nilaippn');
        $total = $this->input->post('total');
        $pcode = strtoupper(addslashes(trim($this->input->post('pcode'))));
        $qty = trim($this->input->post('qty'));
        $disc1 = $this->input->post('disc1');
        $disc2 = $this->input->post('disc2');
        $potongan = $this->input->post('potongan');
        $satuan = $this->input->post('satuan');
        $extcode = $this->input->post('extcode');
        $hargab = $this->input->post('hargab');
        $jumlahb = $this->input->post('jumlahb');
        $ppnb = $this->input->post('ppnb');
        $totalb = $this->input->post('totalb');
        $konversi = $this->input->post('konversi');
        $satuanst = $this->input->post('satuanst');
        $nilsatuan = $this->input->post('nilsatuan');
        $nilsatuanst = $this->input->post('nilsatuanst');
        $kdkategori = $this->input->post('kdkategori');
        $kdbrand = $this->input->post('kdbrand');
        $pcodesave = $this->input->post('pcodesave');
        if ($no == "") {
            $counter = "1";
            $no = $this->insertNewHeader($flag, $mylib->ubah_tanggal($tgl), $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $kdgudang, $mylib->ubah_tanggal($tglkirim));
        } else {
            $counter = $this->updateHeader($flag, $no, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $kdgudang, $mylib->ubah_tanggal($tgl), $mylib->ubah_tanggal($tglkirim));
        }
        $this->insertDetail($flag, $no, $counter, $pcode, $qty, $satuan, $extcode, $user, $pcodesave, $hargab, $disc1, $disc2, $jumlahb, $ppnb, $totalb, $konversi, $satuanst, $nilsatuan, $nilsatuanst, $kdkategori, $kdbrand, $tgl, $kdsupplier, $kdgudang);
        echo $no;
    }

    function insertNewHeader($flag, $tgl, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $kdgudang, $tglkirim) {
        $this->otorisasi_model->locktables('counter,trans_order_barang_header');
        $new_no = $this->otorisasi_model->getNewNo($tgl);
        $no = $new_no->NoOrderBarang;
        $data = array(
            'NoDokumen' => $no,
            'TglDokumen' => $tgl,
            'TglKirim' => $tglkirim,
            'Keterangan' => $ket,
            'KdSupplier' => $kdsupplier,
            'KdGroupext' => $kdgroupext,
            'KdGudang' => $kdgudang,
            'Payment' => $payment,
            'Top' => $top,
            'AddDate' => $tgl,
            'AddUser' => $user,
            'Jumlah' => $jumlah,
            'PPn' => $ppn,
            'NilaiPPn' => $nilaippn,
            'Total' => $total,
            'Status' => ' '
        );
        $this->db->insert('trans_order_barang_header', $data);
        $this->otorisasi_model->unlocktables();
        return $no;
    }

    function updateHeader($flag, $no, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $kdgudang, $tgl, $tglkirim) {
        $this->otorisasi_model->locktables('trans_order_barang_header');
        $count = $this->otorisasi_model->getCounter($no);
        $counter = (empty($count)) ? 0 : (int) $count->Counter + 1;
        $data = array(
            'TglDokumen' => $tgl,
            'TglKirim' => $tglkirim,
            'KdSupplier' => $kdsupplier,
            'KdGroupext' => $kdgroupext,
            'KdGudang' => $kdgudang,
            'Payment' => $payment,
            'Top' => $top,
            'Keterangan' => $ket,
            'Jumlah' => $jumlah,
            'PPn' => $ppn,
            'NilaiPPn' => $nilaippn,
            'Total' => $total
        );
        if ($flag == "edit") {
            $data['EditDate'] = $tgl;
            $data['EditUser'] = $user;
        }
        $this->db->update('trans_order_barang_header', $data, array('NoDokumen' => $no));
        $data = array(
            'TglDokumen' => $tgl
        );
        $this->db->update('trans_order_barang_detail', $data, array('NoDokumen' => $no));
        $this->otorisasi_model->unlocktables();
        return $counter;
    }

    function insertDetail($flag, $no, $counter, $pcode, $qty, $satuan, $extcode, $user, $pcodesave, $hargab, $disc1, $disc2, $potongan, $jumlahb, $ppnb, $totalb, $konversi, $satuanst, $nilsatuan, $nilsatuanst, $kdkategori, $kdbrand, $tgl, $kdsupplier, $kdgudang) {
        //echo $flag;die();
        $tgl = $this->session->userdata('Tanggal_Trans');
        $this->otorisasi_model->locktables('trans_order_barang_detail');
        $data = array(
            'NoDokumen' => $no,
            'TglDokumen' => $tgl,
            'KdSupplier' => $kdsupplier,
            'KdGudang' => $kdgudang,
            'PCode' => $pcode,
            'PCodeExt' => $extcode,
            'Counter' => $counter,
            'QtyInput' => $qty,
            'QtyPcs' => $qty * $konversi,
            'Satuan' => $satuan,
            'Harga' => $hargab,
            'Disc1' => $disc1,
            'Disc2' => $disc2,
            'Potongan' => $potongan,
            'Jumlah' => $jumlahb,
            'PPn' => $ppnb,
            'Total' => $totalb,
            'NamaSatuan' => $nilsatuan,
            'SatuanSt' => $satuanst,
            'NamaSatuanSt' => $nilsatuanst,
            'KonversiSt' => $konversi,
            'KdKategori' => $kdkategori,
            'KdBrand' => $kdbrand,
            'Status' => ' '
        );
        if ($flag == "add") {
            $data['AddDate'] = $tgl;
            $data['AddUser'] = $user;
        } else {
            $data['EditDate'] = $tgl;
            $data['EditUser'] = $user;
        }
        $this->db->insert('trans_order_barang_detail', $data);
        $this->otorisasi_model->unlocktables();
    }

    function delete_item() {
        $id = $this->input->post('no');
        $pcode = $this->input->post('pcode');
        $jumlah = $this->input->post('jumlah');
        $ppn = $this->input->post('ppn');
        $nilaippn = $this->input->post('nilaippn');
        $total = $this->input->post('total');
        $this->otorisasi_model->locktables('trans_order_barang_detail');
        $this->db->delete('trans_order_barang_detail', array('NoDokumen' => $id, 'PCode' => $pcode));
        $this->otorisasi_model->unlocktables();
        $this->otorisasi_model->locktables('trans_order_barang_header');
        $this->db->update('trans_order_barang_header', array('Jumlah' => $jumlah, 'PPn' => $ppn, 'NilaiPPn' => $nilaippn, 'Total' => $total), array('NoDokumen' => $id));
        $this->otorisasi_model->unlocktables();
    }

    function hapusDetail($id) {
        $jumlah = $this->input->post('jumlah');
        $ppn = $this->input->post('ppn');
        $nilaippn = $this->input->post('nilaippn');
        $total = $this->input->post('total');
        $this->otorisasi_model->locktables('trans_order_barang_detail');
        $this->db->delete('trans_order_barang_detail', array('NoDokumen' => $id));
        $this->otorisasi_model->unlocktables();
        $this->otorisasi_model->locktables('trans_order_barang_header');
        $this->db->update('trans_order_barang_header', array('Jumlah' => $jumlah, 'PPn' => $ppn, 'NilaiPPn' => $nilaippn, 'Total' => $total), array('NoDokumen' => $id));
        $this->otorisasi_model->unlocktables();
    }

    function Insert_OTR_Detail($kd, $cb) {

        $hrgD = $this->otorisasi_model->getData("SELECT * FROM trans_order_barang_detail WHERE NoDokumen='$kd'");
        $where = "NoDokumen = '$kd'";
        $table = "otorisasi_detail";
//        $dsn	= "mysql://".$cb->User.":".$cb->Password."@".$cb->Server."/".$cb->Database;
        $dsn = "mysql://natura:dbnatura@192.168.0.219/gl_naturaho";
        $db_obj = $this->load->database($dsn, TRUE);
        if ($db_obj->conn_id) {
            //do something
            $this->dba = $this->load->database($dsn, true);
            $this->dba->delete($table, $where);

            foreach ($hrgD as $d) {

                $dt = array(
                    'NoDokumen' => $kd,
                    'TglDokumen' => $d['TglDokumen'],
                    'KdSupplier' => $d['KdSupplier'],
                    'KdGudang' => $d['KdGudang'],
                    'PCode' => $d['PCode'],
                    'PCodeExt' => $d['PCodeExt'],
                    'Counter' => $d['Counter'],
                    'QtyInput' => $d['QtyInput'],
                    'QtyPcs' => $d['QtyPcs'],
                    'Satuan' => $d['Satuan'],
                    'Harga' => $d['Harga'],
                    'Disc1' => $d['Disc1'],
                    'Disc2' => $d['Disc2'],
                    'Potongan' => $d['Potongan'],
                    'Jumlah' => $d['Jumlah'],
                    'PPn' => $d['PPn'],
                    'Total' => $d['Total'],
                    'NamaSatuan' => $d['NamaSatuan'],
                    'SatuanSt' => $d['SatuanSt'],
                    'NamaSatuanSt' => $d['NamaSatuanSt'],
                    'KonversiSt' => $d['KonversiSt'],
                    'KdKategori' => $d['KdKategori'],
                    'KdBrand' => $d['KdBrand'],
                    'Status' => ' '
                );
                $this->dba->insert($table, $dt);
            }
            $this->dba->close();
            $nil = 0;
        } else {
            //echo 'Unable to connect with database with given db details.';
            $nil = 1;
        }
        return $nil;
    }

    function Insert_OTR_Header($kd, $cb) {
        $hrgH = $this->otorisasi_model->getData("SELECT * FROM trans_order_barang_header WHERE NoDokumen='$kd'");
        $where = "NoDokumen='$kd'";
        $table = "otorisasi_header";
        //mysql://users3cabang:usersukasukses@192.168.0.251/vcidist;
//        $dsn	= "mysql://".$cb->User.":".$cb->Password."@".$cb->Server."/".$cb->Database;
        $dsn = "mysql://natura:dbnatura@192.168.0.219/gl_naturaho";
        $db_obj = $this->load->database($dsn, TRUE);
        if ($db_obj->conn_id) {
            //do something
            //$this->dba = $this->load->database($dsn, true);
            $db_obj->delete($table, $where);
            //$this->dba->delete($table, $where);
            foreach ($hrgH as $h) {
                $dt = array(
                    'NoDokumen' => $kd,
                    'TglDokumen' => $h['TglDokumen'],
                    'TglKirim' => $h['TglKirim'],
                    'Keterangan' => $h['Keterangan'],
                    'KdSupplier' => $h['KdSupplier'],
                    'KdGroupext' => $h['KdGroupext'],
                    'KdGudang' => $h['KdGudang'],
                    'Payment' => $h['Payment'],
                    'TOP' => $h['TOP'],
                    'AddDate' => $h['AddDate'],
                    'AddUser' => $h['AddUser'],
                    'Jumlah' => $h['Jumlah'],
                    'PPn' => $h['PPn'],
                    'NilaiPPn' => $h['NilaiPPn'],
                    'Total' => $h['Total'],
                    'Status' => ' '
                );
                $sukses = $db_obj->insert($table, $dt);
                if ($sukses) {
                    $nil = 0; //echo "berhasil".$dsn ."<br>";
                } else {
                    $nil = 1; //echo "gagal".$dsn."<br>";
                }
            }
            //$this->dba->close();
            $db_obj->close();
        } else {
            //echo 'Unable to connect with database with given db details.';
            $nil = 1;
        }
        return $nil;
    }

}

?>