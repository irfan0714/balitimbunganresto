<?php 
include(BASEPATH.'application/controllers/auth/authcontroller'.EXT);
class Sync_cabang extends Authcontroller {

	function __construct(){
		parent::Controller();
		//$this->load->helper(array('form', 'url'));
		$this->load->helper('path');
		$this->load->helper('bantu');
		$this->load->helper('date');
		$this->load->model('Global_cabang_model');
	}
	
	function index(){
		
		$this->load->library('Menugen');
		$IdRole				= $this->session->userdata('IdRole');
		$data['mnu']		= $this->getMenu($IdRole);		
		//$data['list'] 		= $this->Global_cabang_model->getData("SELECT * from target_sales_header order by notrans desc ; ");		//ArrPrint($data['list']);die();		
		$data['list'] 		= $this->Global_cabang_model->getData("SELECT a.notrans,a.tahun,a.bulan,a.status,SUM(b.`jumoff`) AS  totoff,SUM(b.`jumnon`) AS  totnon FROM target_sales_header a INNER JOIN target_sales_detail b ON a.`notrans`=b.`notrans` 
								
								GROUP BY a.notrans
								ORDER BY a.notrans DESC ; ");		//ArrPrint($data['list']);die();
		$par						= $this->uri->segment(4);
		if(!empty($par)){
			$data['excel']			= "excel";
		}else{
			$data['excel']			= "";
		}		
		$data['msg']		= "";		
		$this->load->view('utility/targetsales/list_target', $data);
	}
	
	function list_log($kdhrg){
		
		$this->load->library('Menugen');
		$IdRole				= $this->session->userdata('IdRole');
		$data['mnu']		= $this->getMenu($IdRole);		
		$data['list'] 		= $this->Global_cabang_model->getData("SELECT a.notrans,a.tahun,a.bulan,a.status,SUM(b.`jumoff`) AS  totoff,SUM(b.`jumnon`) AS  totnon FROM target_sales_header a INNER JOIN target_sales_detail b ON a.`notrans`=b.`notrans` 
								GROUP BY a.notrans
								ORDER BY a.notrans DESC ; ");
		//ArrPrint($data['list']);die();
		$par						= $this->uri->segment(4);
		if(!empty($par)){
			$data['excel']			= "excel";
		}else{
			$data['excel']			= "";
		}		
		$data['msg']		= "";		
		$this->load->view('utility/targetsales/list_target', $data);
		
	}
	
	function formadd(){
		$this->load->library('Menugen');
		$IdRole				= $this->session->userdata('IdRole');
		$data['mnu']		= $this->getMenu($IdRole);	

		$thn_now			= date("Y");
    	$bln_now			= date("m");
		$data['sublist']	= $this->Global_cabang_model->getData("SELECT * FROM cabang WHERE sts_pair='N' AND KdCabang NOT IN('34') ORDER BY KdCabang;");
		$data['CabangDB']	= $this->Global_cabang_model->getData("SELECT * FROM cabang_depo WHERE pair='Y' AND `Database`='vcidist' and Flag='Y'");
		$data['DepoDB']		= $this->Global_cabang_model->getData("SELECT * FROM cabang_depo WHERE pair='N' AND `Database`='vcidist' and Flag='Y'");
		$data['getHarga']	= $this->Global_cabang_model->getData("SELECT * FROM grupharga_header");
		$data['action']		= "simpan";
		$this->load->view('ka/from_sync_HargaKA', $data);
	}
	
	function simpan(){
		$id					= $this->input->post('notrans');
		if(!empty($id)){
		
			$data_dtl = array(
				'varcab'        => $this->input->post('kdcabang'),  
				'vartof'        => fungsi_replace($this->input->post('tof')),
				'varton'      	=> fungsi_replace($this->input->post('ton'))
				  
			   );
			$this->Insert_Detail($data_dtl,$id);			
			
			$totoff			= fungsi_replace($this->input->post('tottof'));
			$totnon			= fungsi_replace($this->input->post('totnon'));
		
			$data			= array(
				"notrans" 	=> $id,
				"tahun" 	=> $this->input->post('KodeTahun'),
				"bulan" 	=> $this->input->post('KodeBulan'),
				"totoff"	=> $totoff,
				"totnon"	=> $totnon,
				"adddate" 	=> $this->Global_cabang_model->getNow(),
				"adduser" 	=> $this->session->userdata('UserID')				
			);
			
			$this->Global_cabang_model->addData("target_sales_header",$data);

			redirect('utility/targetsales', 'refresh');
		}
			
	}
	
	function save_log($kdhrg,$keterangan,$status,$kdcabang){ //delete insert log
		$data	= array(
			'KdHarga'		=> $kdhrg,
			'SysDate'		=> date("Y-m-d H:i:s"),
			'Keterangan'	=> $keterangan,
			'IdUser'		=> $this->session->userdata('UserID'),
			'Status_Kirim'	=> $status,
			'KdCabang'		=> $kdcabang
			);
		$where = "KdHarga='$kdhrg' and KdCabang='$kdcabang'";
		$this->Global_cabang_model->deleteData("log_group_harga",$where);
		$this->Global_cabang_model->addData("log_group_harga",$data);
	}
	
	function Insert_HRG_Detail($kd,$cb){
		
		$hrgD 	= $this->Global_cabang_model->getData("SELECT * FROM grupharga WHERE KdHarga='$kd'");
		$where	= "KdHarga = '$kd'";
		$table	= "grupharga";
		$dsn	= "mysql://".$cb->User.":".$cb->Password."@".$cb->Server."/".$cb->Database;
		$db_obj = $this->load->database($dsn,TRUE);
		if($db_obj->conn_id) {
					//do something
				$this->dba = $this->load->database($dsn, true);
				$this->dba->delete($table, $where);
			
			foreach($hrgD as $d){
	
				$dt	= array(
					'KdHarga'	=> $d['KdHarga'],
					'Keterangan'=> $d['Keterangan'],
					'AddDate'	=> $d['AddDate'],
					'EditDate'	=> $d['EditDate'],
					'PCode'		=> $d['PCode'],
					'HrgKecil'	=> $d['HrgKecil'],
					'HrgTengah'	=> $d['HrgTengah'],
					'HrgBesar'	=> $d['HrgBesar'],
					'KdPLU'		=> $d['KdPLU']
				);
				$this->dba->insert($table, $dt);
			}
			$this->dba->close();
			$nil = 0;
		} else {
			//echo 'Unable to connect with database with given db details.';
			$nil = 1;
		}
		return $nil;
	}
	
	function Insert_HRG_Header($kd,$cb){
		$hrgH 	= $this->Global_cabang_model->getData("SELECT * FROM grupharga_header WHERE KdHarga='$kd'");
		$where	= "KdHarga = '$kd'";
		$table	= "grupharga_header";
		//mysql://users3cabang:usersukasukses@192.168.0.251/vcidist;
		$dsn	= "mysql://".$cb->User.":".$cb->Password."@".$cb->Server."/".$cb->Database;
		$db_obj = $this->load->database($dsn,TRUE);
		if($db_obj->conn_id) {
					//do something
					//$this->dba = $this->load->database($dsn, true);
					$db_obj->delete($table, $where);
					//$this->dba->delete($table, $where);
			foreach($hrgH as $h){
				$dt	= array(
					'KdHarga'	=> $h['KdHarga'],
					'Keterangan'=> $h['Keterangan']			
				);
				$sukses = $db_obj->insert($table, $dt);
				if($sukses){
					$nil = 0;//echo "berhasil".$dsn ."<br>";
				}else{
					$nil = 1;//echo "gagal".$dsn."<br>";
				}
			}
			//$this->dba->close();
			$db_obj->close();
		} else {
				//echo 'Unable to connect with database with given db details.';
				$nil = 1;
		}
		return $nil ;
	}

	
	function broadcast(){
		//ArrPrint($_POST);
		//( [kdHrg] => KC [pil] => Array ( [0] => 33 ) [simpan] => broadcast ) 
		//$cabang = $this->Global_cabang_model->getData("select * from  cabang WHERE KdCabang IN(30,31,32,33,40,41,42,43)");
		//if(!empty())
		$allcb 	= $this->input->post('all_cb');
		$alldp 	= $this->input->post('all_dp');
		$cbg 	= $this->input->post('cbg');
		$dpo 	= $this->input->post('dpo');
		$kdHrg 	= $this->input->post('kdHrg');
		
		//echo count($cbg);//die();
		
		if(!empty($kdHrg)){
			if(!empty($allcb)){				// ini untuk all Cabanf
				$CabangDB	= $this->getData2("SELECT * FROM cabang_depo WHERE pair='Y' AND `Database`='vcidist' and Flag='Y'");
				foreach($CabangDB as $c){
					//$cabang = $this->Global_cabang_model->getField("select * from  cabang_depo WHERE id_cd='$c['id_cd']'");
					$hdr	= $this->Insert_HRG_Header($kdHrg,$c);
					$dtl	= $this->Insert_HRG_Detail($kdHrg,$c);
					$hsl	= $hdr + $dtl;
					if($hsl == 0){
						$this->save_log($kdHrg,"Terkirim",'Y',$c->id_cd);
					}else{ 
						$this->save_log($kdHrg,"gagal kirim",'T',$c->id_cd);
					}
				}
			}else if(!empty($cbg)){
				foreach($cbg as $c){
					$cabang = $this->Global_cabang_model->getField("select * from  cabang_depo WHERE id_cd='$c'");
					$hdr	= $this->Insert_HRG_Header($kdHrg,$cabang);
					$dtl	= $this->Insert_HRG_Detail($kdHrg,$cabang);
					$hsl	= $hdr + $dtl;
					if($hsl == 0){
						$this->save_log($kdHrg,"Terkirim",'Y',$c);
					}else{ // ada gagal
						$this->save_log($kdHrg,"gagal kirim",'T',$c);
					}
				}
			}
			if(!empty($alldp)){				// all depo
				$DepoDB	= $this->getData2("SELECT * FROM cabang_depo WHERE pair='N' AND `Database`='vcidist' and Flag='Y'");
				//ArrPrint($DepoDB);die();
				foreach($DepoDB as $d){
					//$cabang = $this->Global_cabang_model->getField("select * from  cabang_depo WHERE id_cd='$c['id_cd']'");
					$hdr	= $this->Insert_HRG_Header($kdHrg,$d);
					$dtl	= $this->Insert_HRG_Detail($kdHrg,$d);
					$hsl	= $hdr + $dtl;
					if($hsl == 0){
						$this->save_log($kdHrg,"Terkirim",'Y',$d->id_cd);
					}else{ // ada gagal
						$this->save_log($kdHrg,"gagal kirim",'T',$d->id_cd);
					}
				}
				// ini untuk all cabang
			}else if(!empty($dpo)){ //depo tertentu
				foreach($dpo as $d){
					$depo = $this->Global_cabang_model->getField("select * from  cabang_depo WHERE id_cd='$d'");
					$hdr	= $this->Insert_HRG_Header($kdHrg,$depo);
					$dtl	= $this->Insert_HRG_Detail($kdHrg,$depo);
					$hsl	= $hdr + $dtl;
					if($hsl == 0){
						$this->save_log($kdHrg,"Terkirim",'Y',$d);
					}else{ // ada gagal
						$this->save_log($kdHrg,"gagal kirim",'T',$d);
					}
				}
			}
		
		}
		
		$this->load->library('Menugen');
		$IdRole				= $this->session->userdata('IdRole');
		$data['mnu']		= $this->getMenu($IdRole);	
		$data['msg']		= "";
		$data['list'] 		= $this->Global_cabang_model->getData("
				SELECT log_group_harga.*,g.Keterangan  AS NamaHarga,c.NamaCabang FROM log_group_harga
				INNER  JOIN grupharga_header g ON g.KdHarga=log_group_harga.KdHarga
				INNER JOIN cabang_depo c ON c.id_cd=log_group_harga.KdCabang
				WHERE log_group_harga.KdHarga='$kdHrg'
			");
		//ArrPrint($data['list']);die();
		$this->load->view('ka/listlog_grp_harga', $data);
		
		//redirect('utility/sync_cabang/list_log', '3');
	}
	
	function cek_sts_dtl($id){
		$getlist	= $this->Global_cabang_model->getData("SELECT status from target_sales_detail where notrans='$id' ; ");		
		
		$cek_off		= search($getlist, 'status', 'O');	//echo "<pre>";print_r($cek_dt);echo "</pre>";die();
		if(empty($cek_off)){
			$cek_dt		= search($getlist, 'status', 'N');	//echo "<pre>";print_r($cek_dt);echo "</pre>";die();
			if(empty($cek_dt)){
				//ho			
				$data_ho	= array("status"=>"Y");
				$where_ho	= array("notrans"=>$id);
				$this->Global_cabang_model->editData("target_sales_header",$data_ho,$where_ho);			
			}
		}
	}
	
	function getData2($sql){
	//	 echo "<pre>$sql</pre>";die;
         $query = $this->db->query($sql);
		 return $query->result();
    }

}
?>