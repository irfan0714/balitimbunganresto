<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Create_Jurnal extends authcontroller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('proses/create_jurnal_model');
    }

    function index($msg = "")
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $userid = $this->session->userdata('userid');
        $periodgl = $this->create_jurnal_model->getPeriodeGL();
        list($tahunaktif,$bulanaktif,$tglaktif) = explode('-',$periodgl);
        $tanggaltrans = $this->session->userdata('Tanggal_Trans');
        
        if ($sign == "Y") {
            $data['bulanaktif'] = $bulanaktif;
            $data['tahunaktif'] = $tahunaktif;
            $data['tanggaltrans'] = $tanggaltrans;
            $data['track'] = $mylib->print_track();
            $data['msg'] = $msg;
            $this->load->view('proses/create_jurnal/view', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function proses()
    {
        $mylib = new globallib();
        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
		$module = $this->input->post('module');
        $userid = $this->session->userdata('userid');
        $interface = $this->create_jurnal_model->getinterface();
        if($module=='All'){
			if(($tahun==2017 && $bulan>=11) || $tahun>2017){
				$this->posting_payment($tahun,$bulan,$userid);//pv
				$this->posting_receipt($tahun,$bulan,$userid);//RV
				
				$this->posting_pembelian_non_stok($tahun,$bulan,$userid);//PIM
			    $this->posting_penjualan($tahun,$bulan,$userid);//R
				$this->posting_distribusi($tahun,$bulan,$userid);//SI
				$this->posting_cndn_penjualan($tahun,$bulan,$userid); //RB
			    $this->posting_cndn_pembelian($tahun,$bulan,$userid); //RB
                $this->posting_realisasi_um($tahun,$bulan,$userid); //RUM
                $this->posting_invoice_sanitasi($tahun,$bulan,$userid); //IS

				// if($tahun>2018){	
				// 	$this->posting_pembelian($tahun,$bulan,$userid);//PI
				// 	$this->posting_penerimaan_lain($tahun,$bulan,$userid);//DL
				// 	$this->posting_pengeluaran_lain($tahun,$bulan,$userid);//PL
				// 	$this->posting_produksi($tahun,$bulan,$userid);//LL
				// }
			}
			
			$this->posting_JRN_ADJ($tahun,$bulan,$userid);//JRN	
		}else{
			switch ($module) {
				case "PV":
					$this->posting_payment($tahun,$bulan,$userid);
					break;
				case "RV":
					$this->posting_receipt($tahun,$bulan,$userid);//RV
					break;
				case "PI":
					$this->posting_pembelian($tahun,$bulan,$userid);//PI
					break;
				case "PIM":
					$this->posting_pembelian_non_stok($tahun,$bulan,$userid);//PIM
					break;
				case "R":
					$this->posting_penjualan($tahun,$bulan,$userid);//R
					break;
				case "SI":
					$this->posting_distribusi($tahun,$bulan,$userid);//SI
					break;
				case "RJ":
					$this->posting_cndn_penjualan($tahun,$bulan,$userid);//RJ
					break;
				case "RB":
					$this->posting_cndn_pembelian($tahun,$bulan,$userid);//RB
					break;
				case "RUM":
					$this->posting_realisasi_um($tahun,$bulan,$userid);//RUM
					break;
				case "DL":
					$this->posting_penerimaan_lain($tahun,$bulan,$userid);//DL
					break;
				case "PL":
					$this->posting_pengeluaran_lain($tahun,$bulan,$userid);//PL
					break;
				case "LL":
					$this->posting_produksi($tahun,$bulan,$userid);//
					break;
				case "ADJ":
					$this->posting_JRN_ADJ($tahun,$bulan,$userid);//
					break;
                case "IS":
                    $this->posting_invoice_sanitasi($tahun,$bulan,$userid);//IS
                    break;
			}
		}
		
        $msg = "Buat Jurnal Module ".$module." bulan " . $bulan . " - " . $tahun . " selesai.  Jangan lupa untuk melakukan Posting!";
        $this->index($msg);
    }

	function posting_pembelian_non_stok($tahun,$bulan,$userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'PIM', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PIM', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $noposting1 = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 1;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getPembelianNonStok($tahun, $bulan);
        				
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $tgldokumen = $header[$m]['TglDokumen'];
            $kdsubdivisi = $header[$m]['KdSubdivisi'];
            $deskripsi = $header[$m]['Deskripsi'];
            $ketheader = $header[$m]['Keterangan'];
            $jumlah  = $header[$m]['Jumlah'];
            $addUser  = $header[$m]['AddUser'];
            						
            if ($nodok <> $nodokumen) {
        		$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
            	$noposting = $new_no->counter;
                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $ketheader,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PIM',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PIM",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $jumlah,
                "Kredit" => 0,
                "KeteranganDetail" => $deskripsi,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $totdebit += $jumlah;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi"  => $tgldokumen,
	                "KodeJurnal"    => "PIM",
	                "KdDepartemen"  => "00",
	                "Project"       => "00",
	                "CostCenter"    => "00",
					"KdSubDivisi"	=> '25',
	                "KdRekening"    => $rekening[0]['HutangLain'],
	                "Debit"         => 0,
	                "Kredit"        => $totdebit,
	                "KeteranganDetail" => $ketheader,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
		            );
		            
		            $this->db->insert('jurnaldetail', $datadtl);
	                $nilkredit = 0;
	                $counterid = 1;
	                $totdebit = 0;
	                $totkredit = 0;
		        }	
			}	
		}
		
        if ($m > 0) {
            $datadtl = array(
				"NoReferensi"	=> $noposting,
                "TglTransaksi"	=> $tgldokumen,
                "KodeJurnal"	=> "PIM",
                "KdDepartemen"	=> "00",
                "Project"		=> "00",
                "CostCenter"	=> "00",
                "KdRekening"	=>  $rekening[0]['HutangLain'],
				"KdSubDivisi"	=> '25',
                "Debit"		=> 0,
                "Kredit"	=> $totdebit,
                "KeteranganDetail" => $ketheader,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
	}
	
    function posting_payment($tahun, $bulan, $userid)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'PV', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PV', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $noposting1 = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 1;

        $header = $this->create_jurnal_model->getpayment($tahun, $bulan);
        				
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$divisiheader = $header[$m]['divisiheader'];
			$divisidetail = $header[$m]['divisidetail'];
			
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $rekeningheader = $header[$m]['RekeningHeader'];
            $debit	= $header[$m]['Debit'];
            $valdebit = $debit >0 ? $debit : 0;
            $valkredit = $debit >0 ? 0 : $debit*-1;
            $ketdetail = $header[$m]['KetDetail'];
            $ketheader = $header[$m]['KetHeader'];
            $addUser = $header[$m]['AddUser'];
									
            if ($nodok <> $nodokumen) {
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('trans_payment_header', array('NoPosting' => $noposting), array('NoDokumen' => $nodokumen));
                }else
                	$noposting = $header[$m]['NoPosting'];
                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $ketheader,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PV',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $divisidetail,
                "Debit" => $valdebit,
                "Kredit" => $valkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $nilkredit = $nilkredit + $debit;
            $totdebit += $valdebit;
            $totkredit += $valkredit;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi"  => $tgldokumen,
	                "KodeJurnal"    => "PV",
	                "KdDepartemen"  => "00", 
	                "Project"       => "00",
	                "CostCenter"    => "00",
					"KdSubDivisi"	=> $divisiheader,
	                "KdRekening"    => $rekeningheader,
	                "Debit"         => 0,
	                "Kredit"        => $nilkredit,
	                "KeteranganDetail" => $ketdetail,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
		            );
		            $this->db->insert('jurnaldetail', $datadtl);
	                $nilkredit = 0;
	                $counterid = 1;
	                $totdebit = 0;
	                $totkredit = 0;
		        }	
			}	
		}
		
        if ($m > 0) {
            $datadtl = array(
				"NoReferensi"	=> $noposting,
                "TglTransaksi"	=> $tgldokumen,
                "KodeJurnal"	=> "PV",
                "KdDepartemen"	=> "00",
                "Project"		=> "00",
                "CostCenter"	=> "00",
                "KdRekening"	=> $rekeningheader,
				"KdSubDivisi"	=> $divisiheader,
                "Debit"		=> 0,
                "Kredit"	=> $nilkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
    }

    function posting_receipt($tahun, $bulan, $userid)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'RV', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'RV', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $header = $this->create_jurnal_model->getreceipt($tahun, $bulan);
        $nodok = '';
        $noposting1 = '';
        $nildebit = 0;
        $nilkredit = 0;
        $counterid = 1;

        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
            $divisiheader = $header[$m]['divisiheader'];
			$divisidetail = $header[$m]['divisidetail'];
			
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $rekeningheader = $header[$m]['RekeningHeader'];
            $kredit = $header[$m]['Kredit'];
            $ketdetail = $header[$m]['KetDetail'];
            
            $valkredit = $kredit >0 ? $kredit : 0;
            $valdebit  = $kredit >0 ? 0 : $kredit*-1;
            
            $ketheader = $header[$m]['KetHeader'];
            $addUser = $header[$m]['AddUser'];
            
            if ($nodok <> $nodokumen) {
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('trans_receipt_header', array('NoPosting' => $noposting), array('NoDokumen' => $nodokumen));
                }else
                	
                	$noposting = $header[$m]['NoPosting'];
            	                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "JenisJurnal" => 'U',
                    "Keterangan" => $ketheader,
                    "Project" => '00',
                    "CostCenter" => '00',
                    "KodeJurnal" => 'RV',
                    "NoTransaksi" => $nodokumen,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }

            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "RV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $divisidetail,
                "Debit" => $valdebit,
                "Kredit" => $valkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $nildebit = $nildebit + $valdebit;
            $nilkredit = $nilkredit + $valkredit;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < count($header)-1){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "KodeJurnal" => "RV",
                    "KdDepartemen" => "00",
                    "Project" => "00",
                    "CostCenter" => "00",
                    "KdRekening" => $rekeningheader,
					"KdSubDivisi"	=> $divisiheader,
                    "Debit" => $nilkredit-$nildebit,
                    "Kredit" => 0,
                    "KeteranganDetail" => $ketdetail,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "JenisJurnal" => "U",
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser);
	                $this->db->insert('jurnaldetail', $datadtl);
	                $nildebit = 0;
	                $nilkredit=0;
	                $counterid = 1;
	            }   
			}
        }
        if ($m > 0) {
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "RV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekeningheader,
				"KdSubDivisi"	=> $divisiheader,
                "Debit" =>  $nilkredit-$nildebit,
                "Kredit" => 0,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
    }

    function posting_JRN_ADJ($tahun, $bulan, $userid)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'JRN', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'JRN', 'Bulan' => $bulan, 'Tahun' => $tahun));

        /* Journal */
        $header = $this->create_jurnal_model->getjurnal($tahun, $bulan);
        $nodok = '';
        $noposting1 = '';
        $nildebit = 0;
        $counterid = 1;
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
            $divisidetail = $header[$m]['divisidetail'];
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $debit = $header[$m]['Debit'];
            $kredit = $header[$m]['Kredit'];
            $ketdetail = $header[$m]['KetDetail'];
            $ketheader = $header[$m]['KetHeader'];
            $addUser = $header[$m]['AddUser'];
			

            if ($nodok <> $nodokumen) {
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('trans_jurnal_header', array('NoPosting' => $noposting), array('NoDokumen' => $nodokumen));
                }else
                	
                	$noposting = $header[$m]['NoPosting'];
                	
                $data = array("KdDepartemen" => '00',
                    "NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "JenisJurnal" => 'U',
                    "Keterangan" => $ketheader,
                    "Project" => '00',
                    "CostCenter" => '00',
                    "KodeJurnal" => 'JRN',
                    "NoTransaksi" => $nodokumen,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
                $counterid = 1;
            }
            
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "JRN",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $divisidetail,
                "Debit" => $debit,
                "Kredit" => $kredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $counterid = $counterid + 1;
			$nodok = $nodokumen;            
        }
    }

	function posting_pembelian($tahun, $bulan, $userid)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'PI', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PI', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;
        $valppn = 0;
        $valtotal = 0;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getpembelian($tahun, $bulan);
        	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoFaktur'];
			$nofakturpajak = $header[$m]['NoFakturPajak'];
			$keterangan = $header[$m]['Keterangan'];
			$tgldokumen = $header[$m]['Tanggal'];
			$nama = $header[$m]['Nama'];
            $ppn = $header[$m]['PPN'];
            $qty = $header[$m]['Qty'];
            $harga = $header[$m]['Harga'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekpersediaan = $header[$m]['KdRekeningPersediaan'];
            $supplier = $header[$m]['Nama'];
            $addUser = $header[$m]['AddUser'];
									
            if ($nodok <> $nodokumen) {
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('invoice_pembelian_header', array('NoPosting' => $noposting), array('NoFaktur' => $nodokumen));
                }else
                	$noposting = $header[$m]['NoPosting'];
                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => 'Pembelian dari ' .$supplier,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PI',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            $counterid++;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PI",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekpersediaan,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $qty*$harga,
                "Kredit" => 0,
                "KeteranganDetail" => $keterangan,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $valppn += ($qty*$harga)*$ppn/100;
            $valtotal += ($qty*$harga)+(($qty*$harga)*$ppn/100);
            
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoFaktur']){
		            if($ppn>0){
		            	$counterid++;
						$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "PI",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekening[0]['PPnBeli'],
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => $valppn,
			                "Kredit" => 0,
			                "KeteranganDetail" => 'PPN 1 '. $nama.'-'.$nofakturpajak,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid,
                            "AddUserTransaksi" => $addUser

		            	);
		            	$this->db->insert('jurnaldetail', $datadtl);
					}
					
					$counterid++;
					$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "PI",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekening[0]['HutangDagang'],
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => 0,
			                "Kredit" => $valtotal,
			                "KeteranganDetail" => $nama.'-'.$nofakturpajak,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid,
                            "AddUserTransaksi" => $addUser
		            	);
		            $this->db->insert('jurnaldetail', $datadtl);
		            $valppn =0;
		        	$valtotal=0;
		        }
		    }	
		   	$nodok = $nodokumen;
		}
		
        if ($m > 0) {
            if($ppn>0){
            	
            	$counterid++;
				$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "PI",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['PPnBeli'],
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => $valppn,
	                "Kredit" => 0,
	                "KeteranganDetail" => $nama.'-'.$nofakturpajak,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
            	);
            	$this->db->insert('jurnaldetail', $datadtl);
			}
					
			$counterid++;
			$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "PI",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['HutangDagang'],
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $valtotal,
	                "KeteranganDetail" => $nama.'-'.$nofakturpajak,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
            	);
            $this->db->insert('jurnaldetail', $datadtl);
        
        }
    }
    
    function posting_penerimaan_lain($tahun, $bulan, $userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'DL', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'DL', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getpenerimaanlain($tahun, $bulan);
        	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$keterangan = $header[$m]['Keterangan'];
			$keterangandetail = $header[$m]['KeteranganDetail'];
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekpersediaan = $header[$m]['KdRekeningPersediaan'];
            $addUser = $header[$m]['AddUser'];
            if(empty($header[$m]['Nilai'])){
                $nilai = 0;
            }else{
                $nilai = $header[$m]['Nilai'];
            }
            $rekpurpose = $header[$m]['coano'];
									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'DL',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $counterid++;
            $totdebit += $nilai;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "DL",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekpersediaan,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $nilai,
                "Kredit" => 0,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$counterid++;
					$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "DL",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekpurpose,
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => 0,
			                "Kredit" => $totdebit,
			                "KeteranganDetail" => $keterangan,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid,
                            "AddUserTransaksi" => $addUser
		            	);
		            $this->db->insert('jurnaldetail', $datadtl);
		            $totdebit=0;
		        }
		    }	
		    $nodok = $nodokumen;
		}
		
        if ($m > 0) {
			$counterid++;
			$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "DL",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekpurpose,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $totdebit,
	                "KeteranganDetail" => $keterangan,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
            	);
            $this->db->insert('jurnaldetail', $datadtl);
            
        }
    }
   
    function posting_pengeluaran_lain($tahun, $bulan, $userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'PL', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PL', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getpengeluaranlain($tahun, $bulan);
        	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$keterangan = $header[$m]['Keterangan'];
			$keterangandetail = $header[$m]['KeteranganDetail'];
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekpersediaan = $header[$m]['KdRekeningPersediaan'];
            $addUser = $header[$m]['AddUser'];
            if(empty($header[$m]['Nilai'])){
                $nilai =0;
            }else{
                $nilai = $header[$m]['Nilai'];

            }
            $rekpurpose = $header[$m]['coano'];
									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PL',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $counterid++;
            $totkredit += $nilai;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PL",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekpersediaan,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => 0,
                "Kredit" => $nilai,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$counterid++;
					$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "PL",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekpurpose,
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => $totkredit,
			                "Kredit" => 0,
			                "KeteranganDetail" => $keterangan,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid,
                            "AddUserTransaksi" => $addUser
		            	);
		            $this->db->insert('jurnaldetail', $datadtl);
		            $totkredit=0;
		        }
		    }	
		    $nodok = $nodokumen;
		}
		
        if ($m > 0) {
			$counterid++;
			$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "PL",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekpurpose,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => $totkredit,
	                "Kredit" => 0,
	                "KeteranganDetail" => $keterangan,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
            	);
            $this->db->insert('jurnaldetail', $datadtl);
        }
    }
    
    function posting_penjualan($tahun, $bulan, $userid)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'R', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'R', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
        $noposting = $new_no->counter;
        $awalbulan = $tahun.'-'. $bulan .'-01';
        $tgldokumen = date('Y-m-t', strtotime($awalbulan));
        
        $counterid = 0;
		$rekening = $this->create_jurnal_model->getinterface();

        #UPDATE SC 8% PER 2019-08 ISO
        $tahunbulan = $tahun.$bulan;
        if($tahunbulan < 201908){
            $data = $this->create_jurnal_model->getpenjualan($tahun, $bulan);
        }else{
            $data = $this->create_jurnal_model->getpenjualanSC8($tahun, $bulan);
        }
			  				
        for ($m = 0; $m < count($data); $m++) {
            $kddivisi		= $data[$m]['KdDivisi'];
            $kdsubdivisi	= $data[$m]['KdSubDivisi'];
            $kddivisireport = $data[$m]['KdDivisiReport'];
            $namadivisireport = $data[$m]['NamaDivisiReport'];
            $jenispajak = $data[$m]['JenisPajak'];
            $nett = $data[$m]['Nett'];
            $gross = $data[$m]['Gross'];
            $rounding = $data[$m]['Rounding'];
            
            $discoffice = $data[$m]['DiscOffice'];
            $discmkt = $data[$m]['DiscMKT'];
            $discoutlet = $data[$m]['DiscOutlet'];
            $disckk = $data[$m]['DiscKK'];
            $discmember = $data[$m]['DiscMember'];
            $disckaryawan = $data[$m]['DiscKaryawan'];
            $disctot = $data[$m]['Disc'];
            $hpp     = $data[$m]['HPP'];
            $kdrekpersediaan = $data[$m]['KdRekeningPersediaan'];
            
            $discoutlet = $disctot - $discmkt - $discoffice - $disckk - $discmember - $disckaryawan;
            
            $servicecharge = $data[$m]['ServiceCharge'];
            $tax = $data[$m]['Tax'];
            $sales = $data[$m]['Sales'];
            $voucher = $data[$m]['Voucher'];
            $kdreksales = $data[$m]['KdRekeningSales'];
            $kdrekdisc = $data[$m]['KdRekeningDisc'];
            if($jenispajak=='PPN'){
				$taxacct = $rekening[0]['PPNJual'];	
			}else{
				$taxacct = $rekening[0]['PB1'];
			}
			$gross = $sales-$tax-$servicecharge+$disctot;
			
			if($tahun < 2018){
				//Penjualan				
	            $counterid ++;				
	            $datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $kdreksales,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $nett,
	                "KeteranganDetail" => 'Penjualan '. $namadivisireport,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
	            $this->db->insert('jurnaldetail', $datadtl);	
			}else{
				//Gross
	            $counterid ++;				
	            $datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $kdreksales,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $gross,
	                "KeteranganDetail" => 'Penjualan '. $namadivisireport,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );

                
	            $this->db->insert('jurnaldetail', $datadtl);	
	            
	            //disc office
	            if($discoffice>0){
					$counterid ++;				
		            $datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "R",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $kdrekdisc,
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $discoffice,
		                "Kredit" => 0,
		                "KeteranganDetail" => 'Discount Penjualan '. $namadivisireport,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);		
				}
				
				//disc mkt
	            if($discmkt>0){
					$counterid ++;				
		            $datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "R",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $kdrekdisc,
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $discmkt,
		                "Kredit" => 0,
		                "KeteranganDetail" => 'Discount Tambahan Marketing '. $namadivisireport,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);		
				}
				
				//disc outlet
	            if($discoutlet>0){
					$counterid ++;				
		            $datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "R",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $kdrekdisc,
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $discoutlet,
		                "Kredit" => 0,
		                "KeteranganDetail" => 'Discount Tambahan Outlet '. $namadivisireport,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);		
				}
				
				if($disckk>0){
					$counterid ++;				
		            $datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "R",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $kdrekdisc,
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $disckk,
		                "Kredit" => 0,
		                "KeteranganDetail" => 'Discount Kartu Kredit '. $namadivisireport,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);		
				}
				
				if($discmember>0){
					$counterid ++;				
		            $datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "R",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $kdrekdisc,
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $discmember,
		                "Kredit" => 0,
		                "KeteranganDetail" => 'Discount Kartu Anggota SGV '. $namadivisireport,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);		
				}
				
				if($disckaryawan>0){
					$counterid ++;				
		            $datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "R",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $rekening[0]['RekDiscKaryawan'],
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $disckaryawan,
		                "Kredit" => 0,
		                "KeteranganDetail" => 'Discount Karyawan '. $namadivisireport,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);		
				}
			}

            //Rounding
            if($rounding>0){
                
                $counterid ++;              
                $datadtl = array("NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "KodeJurnal" => "R",
                    "KdDepartemen" => "00",
                    "Project" => "00",
                    "CostCenter" => "00",
                    "KdRekening" => $rekening[0]['LabaSelisihBayar'],
                    "KdSubDivisi"   => $kdsubdivisi,
                    "Debit" => 0,
                    "Kredit" => $rounding,
                    "KeteranganDetail" => 'Rounding '. $namadivisireport,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "JenisJurnal" => "U",
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "Counter" => $counterid
                );
                $this->db->insert('jurnaldetail', $datadtl);    
            }else{
                $counterid ++;              
                $datadtl = array("NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "KodeJurnal" => "R",
                    "KdDepartemen" => "00",
                    "Project" => "00",
                    "CostCenter" => "00",
                    "KdRekening" => $rekening[0]['RugiSelisihBayar'],
                    "KdSubDivisi"   => $kdsubdivisi,
                    "Debit" => $rounding*-1,
                    "Kredit" => 0,
                    "KeteranganDetail" => 'Rounding '. $namadivisireport,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "JenisJurnal" => "U",
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "Counter" => $counterid
                );
                $this->db->insert('jurnaldetail', $datadtl);   
            }
            
            //Service Charge
            if($servicecharge>0){
            	
				$counterid ++;				
	            $datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['ServiceCharge'],
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $servicecharge,
	                "KeteranganDetail" => 'Service Charge '. $namadivisireport,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
	            $this->db->insert('jurnaldetail', $datadtl);	
			}
           
            //Tax
            if($tax>0){
				$counterid ++;				
	            $datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $taxacct,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $tax,
	                "KeteranganDetail" => 'Pajak '. $namadivisireport,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
	            $this->db->insert('jurnaldetail', $datadtl);	
			}
            
            //Piutang
            $counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekening[0]['PiutangDagang'],
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $sales+$rounding,
                "Kredit" => 0,
                "KeteranganDetail" => 'Piutang Dagang '. $namadivisireport,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
            
            //Voucher
            if($voucher>0){
				$counterid ++;				
	            $datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['Voucher'],
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => $voucher,
	                "Kredit" => 0,
	                "KeteranganDetail" => 'Pemakaian Voucher '. $namadivisireport,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
	            $this->db->insert('jurnaldetail', $datadtl);	
	            
	            $counterid ++;				
	            $datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['PiutangDagang'],
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => 0,
	                "Kredit" => $voucher,
	                "KeteranganDetail" => 'Pemakaian Voucher '. $namadivisireport,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
	            $this->db->insert('jurnaldetail', $datadtl);	
			}
			/*
			//HPP
			$counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekening[0]['HPP'],
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $hpp,
                "Kredit" => 0,
                "KeteranganDetail" => 'Harga Pokok Penjualan '. $namadivisireport,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
            
            $counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekpersediaan,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => 0,
                "Kredit" => $hpp,
                "KeteranganDetail" => 'Harga Pokok Penjualan '. $namadivisireport,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);		
			*/
		}
		// Penjualan Tiket
		// pembaharuan jurnal untuk coa penjualan tickting
		// per september 2018 penjualan ticketing masuk ke coa pajak hiburan bukan pajak pembangunan
		
		$datatiket = $this->create_jurnal_model->getpenjualan_tiket($tahun, $bulan);
		$RpTiket = $datatiket[0]['RpTiket'];
		$disctiket = $datatiket[0]['RpDisc'];
		$nettiket = $datatiket[0]['NetTiket'];
		$rkgn = $rekening[0]['RekPenjualanTiket'];
		
		if($RpTiket>0){
			$counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rkgn,
				"KdSubDivisi"	=> '25',
                "Debit" => 0,
                "Kredit" => $RpTiket/1.1,
                "KeteranganDetail" => 'Penjualan Tiket..',
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
            
            if($disctiket>0){
				$counterid ++;				
            	$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['RekDiscTambahan'],
					"KdSubDivisi"	=> '25',
	                "Debit" => $disctiket/1.1,
	                "Kredit" => 0,
	                "KeteranganDetail" => 'Discount Penjualan Tiket',
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
            $this->db->insert('jurnaldetail', $datadtl);	
			}
            
            $counterid ++;	
            
            //dibawah sepetem 2018
			if(($tahun*1)<=2018 AND ($bulan*1)<=8 ){
					$taxacct = $taxacct;
			}else{
					$taxacct = $rekening[0]['RekPenjualanTiketHiburan'];
			}
					
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $taxacct,
				"KdSubDivisi"	=> '25',
                "Debit" => 0,
                "Kredit" => ($RpTiket-$disctiket)*0.1/1.1,
                "KeteranganDetail" => 'Pajak Penjualan Tiket_',
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
            
            $counterid ++;
            $kdsubdivisi_='25';				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekening[0]['PiutangDagang'],
				"KdSubDivisi"	=> $kdsubdivisi_,
                "Debit" => $nettiket,
                "Kredit" => 0,
                "KeteranganDetail" => 'Penjualan Tiket.',
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
        }
        
        //swing

        $datatiketSwing = $this->create_jurnal_model->getpenjualan_tiket_swing($tahun, $bulan);
		$RpTiketSwing = $datatiketSwing[0]['RpTiket'];
		$disctiketSwing = $datatiketSwing[0]['RpDisc'];
		$nettiketSwing = $datatiketSwing[0]['NetTiket'];
		$rkgnSwing = $rekening[0]['RekPenjualanTiketSwing'];
		
		if($RpTiketSwing>0){
			$counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rkgnSwing,
				"KdSubDivisi"	=> '96',
                "Debit" => 0,
                "Kredit" => $RpTiketSwing/1.1,
                "KeteranganDetail" => 'Penjualan Tiket Swing..',
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
            //pajak penjulan tiket swing
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $taxacct,
                "KdSubDivisi"   => '96',
                "Debit" => 0,
                "Kredit" => ($RpTiketSwing/1.1) * 0.1,
                "KeteranganDetail" => 'Pajak Penjualan Tiket Swing',
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);    

            if($disctiketSwing>0){
				$counterid ++;				
            	$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "R",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekening[0]['RekDiscTambahan'],
					"KdSubDivisi"	=> '25',
	                "Debit" => $disctiketSwing/1.1,
	                "Kredit" => 0,
	                "KeteranganDetail" => 'Discount Penjualan Tiket Swing',
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
	            );
            $this->db->insert('jurnaldetail', $datadtl);	
			}	
            
            $counterid ++;
            // $kdsubdivisi_='25';				
            $kdsubdivisi_='96';             
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekening[0]['PiutangDagang'],
				"KdSubDivisi"	=> $kdsubdivisi_,
                "Debit" => $nettiketSwing,
                "Kredit" => 0,
                "KeteranganDetail" => 'Penjualan Tiket Swing.',
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);	
        }

        //end ticket swing
		
		$data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => 'Jurnal Penjualan',
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'R',
                    "NoTransaksi"   => '',
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid);
                $this->db->insert('jurnalheader', $data);
                
        //$this->compliment($tahun, $bulan, $userid, $counterid, $noposting,$tgldokumen);
    }
    
    function posting_cndn_penjualan($tahun, $bulan, $userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'CJ', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'CJ', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getcndnpenjualan($tahun, $bulan);
        $nilaivat = 0;	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['cnno'];
			$keterangan = $header[$m]['note'];
			$keterangandetail = $header[$m]['description'];
			$tgldokumen = $header[$m]['cndate'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekdetail = $header[$m]['coano'];
            $nilai = $header[$m]['value'];
            $nama = $header[$m]['Nama'];
            $dntype = $header[$m]['cntype'];
            $kdrekeningheader = $header[$m]['KdRekening'];
            $vatpercent = $header[$m]['VatPercent'];
            $addUser = $header[$m]['adduser'];
            									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'CJ',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            if($dntype=='D' || $dntype=='DR'){
            	if($nilai>0){
					$debit = 0;
					$credit = $nilai;
				}else{
					$debit = $nilai*-1;
					$credit = 0;
				}
			}else{
				if($nilai>0){
					$debit = $nilai;
					$credit = 0;
				}else{
					$debit = 0;	
					$credit = $nilai*-1;
				}
				
			}
			
            $counterid++;
            $totkredit += $credit;
            $totdebit += $debit;
            $nilaivat += $nilai * $vatpercent/100;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "CJ",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekdetail,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $debit,
                "Kredit" => $credit,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['cnno']){
					
					if($kdrekeningheader== ''){
						$rekhutang = $rekening[0]['PiutangDagang'];
					}else{
						$rekhutang = $kdrekeningheader;	
					}	
					//echo 'vat '.$nilaivat;
					if($nilaivat>0){
						if($dntype=='D' || $dntype=='DR'){
							$vatdebet = 0;
							$vatcredit = $nilaivat;
						}else{
							$vatdebet = $nilaivat;
							$vatcredit = 0;
						}
					
						$counterid++;
						$datadtl = array("NoReferensi" => $noposting,
				                "TglTransaksi" => $tgldokumen,
				                "KodeJurnal" => "CJ",
				                "KdDepartemen" => "00",
				                "Project" => "00",
				                "CostCenter" => "00",
				                "KdRekening" => $rekening[0]['PPNJual'],
								"KdSubDivisi"	=> $kdsubdivisi,
				                "Debit" => $vatdebet,
				                "Kredit" => $vatcredit,
				                "KeteranganDetail" => $keterangan,
				                "Bulan" => $bulan,
				                "Tahun" => $tahun,
				                "JenisJurnal" => "U",
				                "AddDate" => $tgldokumen,
				                "AddName" => $userid,
				                "Counter" => $counterid,
                                "AddUserTransaksi" => $addUser
			            	);
			            $this->db->insert('jurnaldetail', $datadtl);	
					}
					
					if($dntype=='D' || $dntype=='DR'){
						$nilaihutangdb = $totkredit-$totdebit+$nilaivat;
						$nilaihutangcr = 0;
					}else{
						$nilaihutangdb = 0;
						$nilaihutangcr = $totdebit-$totkredit+$nilaivat;
					}
					$counterid++;
					$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "CJ",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekhutang,
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => $nilaihutangdb,
			                "Kredit" => $nilaihutangcr,
			                "KeteranganDetail" => $keterangan,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid,
                            "AddUserTransaksi" => $addUser
		            	);
		            $this->db->insert('jurnaldetail', $datadtl);
		            $totkredit=0;
		            $totdebit=0;
		            $nilaivat = 0;
		        }
		    }	
		    $nodok = $nodokumen;
		}
		
        if($m > 0){
            if($kdrekeningheader== ''){
                $rekhutang = $rekening[0]['PiutangDagang'];
            }else{
                $rekhutang = $kdrekeningheader; 
            }
            
			if($nilaivat>0){
				if($dntype=='D' || $dntype=='DR'){
					$vatdebet = 0;
					$vatcredit = $nilaivat;
				}else{
					$vatdebet = $nilaivat;
					$vatcredit = 0;
				}
					
				$counterid++;
				$datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "CJ",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $rekening[0]['PPNJual'],
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $vatdebet,
		                "Kredit" => $vatcredit,
		                "KeteranganDetail" => $keterangan,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid,
                        "AddUserTransaksi" => $addUser
	            	);
	            $this->db->insert('jurnaldetail', $datadtl);	
			}
			
			if($dntype=='D' || $dntype=='DR'){
				$nilaihutangdb = $totkredit-$totdebit+$nilaivat;
				$nilaihutangcr = 0;
			}else{
				$nilaihutangdb = 0;
				$nilaihutangcr = $totdebit-$totkredit+$nilaivat;
			}
			
			$counterid++;
			$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "CJ",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekhutang,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => $nilaihutangdb,
	                "Kredit" => $nilaihutangcr,
	                "KeteranganDetail" => $keterangan,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
            	);
            $this->db->insert('jurnaldetail', $datadtl);
        }
	}

    
    function posting_distribusi($tahun, $bulan, $userid)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'SI', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'SI', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
        $noposting = $new_no->counter;
        $awalbulan = $tahun.'-'. $bulan .'-01';
        $tgldokumen = date('Y-m-t', strtotime($awalbulan));
        
        $counterid = 0;
		$rekening = $this->create_jurnal_model->getinterface();
        $data = $this->create_jurnal_model->getdistribusi($tahun, $bulan); 
        
        if(count($data) > 0) {
            for ($m = 0; $m < count($data); $m++) {
            	$invoiceno	= $data[$m]['InvoiceNo'];
            	$tgldokumen	= $data[$m]['sidate'];
            	$kdreksales	= $data[$m]['KdRekeningSales'];
                $kdrekdistribusi = $data[$m]['KdRekeningDist'];
                $kdsubdivisi	= $data[$m]['KdSubDivisi'];
                $gross	= $data[$m]['Gross'];
                $tax	= $data[$m]['PPN'];
                $nama = $data[$m]['Nama'];
                $addUser = $data[$m]['adduser'];
                
                $taxacct = $rekening[0]['PPNJual'];
                
                //Penjualan				
                $counterid ++;				
                $datadtl = array("NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "KodeJurnal" => "SI",
                    "KdDepartemen" => "00",
                    "Project" => "00",
                    "CostCenter" => "00",
                    "KdRekening" => $kdrekdistribusi,
    				"KdSubDivisi"	=> $kdsubdivisi,
                    "Debit" => 0,
                    "Kredit" => $gross,
                    "KeteranganDetail" => 'Penjualan ke  '. $nama,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "JenisJurnal" => "U",
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
                );
                $this->db->insert('jurnaldetail', $datadtl);
                
                //Tax
                if($tax>0){
    				$counterid ++;				
    	            $datadtl = array("NoReferensi" => $noposting,
    	                "TglTransaksi" => $tgldokumen,
    	                "KodeJurnal" => "SI",
    	                "KdDepartemen" => "00",
    	                "Project" => "00",
    	                "CostCenter" => "00",
    	                "KdRekening" => $taxacct,
    					"KdSubDivisi"	=> $kdsubdivisi,
    	                "Debit" => 0,
    	                "Kredit" => $tax,
    	                "KeteranganDetail" => 'PPN Keluaran '. $nama,
    	                "Bulan" => $bulan,
    	                "Tahun" => $tahun,
    	                "JenisJurnal" => "U",
    	                "AddDate" => $tgldokumen,
    	                "AddName" => $userid,
    	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
    	            );
    	            $this->db->insert('jurnaldetail', $datadtl);	
    			}
                
                //Piutang
                $counterid ++;				
                $datadtl = array("NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "KodeJurnal" => "SI",
                    "KdDepartemen" => "00",
                    "Project" => "00",
                    "CostCenter" => "00",
                    "KdRekening" => $rekening[0]['PiutangDagang'],
    				"KdSubDivisi"	=> $kdsubdivisi,
                    "Debit" => $gross+$tax,
                    "Kredit" => 0,
                    "KeteranganDetail" => 'Piutang Dagang '. $nama,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "JenisJurnal" => "U",
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
                );
                $this->db->insert('jurnaldetail', $datadtl);	
                	
    		}
		
		    $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => 'Jurnal Distribusi',
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'SI',
                    "NoTransaksi"   => '',
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser
                );
                $this->db->insert('jurnalheader', $data);
        }               
    }

	function posting_cndn_pembelian($tahun, $bulan, $userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'CB', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'CB', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getcndnpembelian($tahun, $bulan);
        $nilaivat = 0;	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['dnno'];
			$keterangan = $header[$m]['note'];
			$keterangandetail = $header[$m]['description'];
			$tgldokumen = $header[$m]['dndate'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekdetail = $header[$m]['coano'];
            $nilai = $header[$m]['value'];
            $nama = $header[$m]['Nama'];
            $dntype = $header[$m]['dntype'];
            $kdrekeningheader = $header[$m]['KdRekening'];
            $vatpercent = $header[$m]['VatPercent'];
            $addUser = $header[$m]['adduser'];
            									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'CB',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
            if($dntype=='D' || $dntype=='DM'){
            	if($nilai>0){
					$debit = 0;
					$credit = $nilai;
				}else{
					$debit = $nilai*-1;
					$credit = 0;	
				}
			}else{
				if($nilai>0){
					$debit = $nilai;
					$credit = 0;
				}else{
					$debit = 0;
					$credit = $nilai*-1;	
				}
				
			}
			
            $counterid++;
            $totkredit += $credit;
            $totdebit += $debit;
            $nilaivat += $nilai * $vatpercent/100;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "CB",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekdetail,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $debit,
                "Kredit" => $credit,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['dnno']){
					
					if($kdrekeningheader== ''){
						if($dntype=='D' || $dntype=='C'){
							$rekhutang = $rekening[0]['HutangDagang'];
						}else{
							$rekhutang = $rekening[0]['HutangLain'];
						}
					}else{
						$rekhutang = $kdrekeningheader;	
					}	
					
					if($nilaivat>0){
						if($dntype=='D' || $dntype=='DM'){
							$vatdebet = 0;
							$vatcredit = $nilaivat;
						}else{
							$vatdebet = $nilaivat;
							$vatcredit = 0;
						}
					
						$counterid++;
						$datadtl = array("NoReferensi" => $noposting,
				                "TglTransaksi" => $tgldokumen,
				                "KodeJurnal" => "CB",
				                "KdDepartemen" => "00",
				                "Project" => "00",
				                "CostCenter" => "00",
				                "KdRekening" => $rekening[0]['PPnBeli'],
								"KdSubDivisi"	=> $kdsubdivisi,
				                "Debit" => $vatdebet,
				                "Kredit" => $vatcredit,
				                "KeteranganDetail" => $keterangan,
				                "Bulan" => $bulan,
				                "Tahun" => $tahun,
				                "JenisJurnal" => "U",
				                "AddDate" => $tgldokumen,
				                "AddName" => $userid,
				                "Counter" => $counterid,
                                "AddUserTransaksi" => $addUser
			            	);
			            //kata pak herman harusnya tidak ada pak, semua ke Biaya, sempat revisi CN (PPN dihapus)
			            //$this->db->insert('jurnaldetail', $datadtl);	
					}
					
					if($dntype=='D' || $dntype=='DM'){
						$nilaihutangdb = $totkredit-$totdebit+$nilaivat;
						$nilaihutangcr = 0;
					}else{
						$nilaihutangdb = 0;
						$nilaihutangcr = $totdebit-$totkredit+$nilaivat;
					}
					$counterid++;
					$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "CB",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekhutang,
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => $nilaihutangdb,
			                "Kredit" => $nilaihutangcr,
			                "KeteranganDetail" => $keterangan,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid,
                            "AddUserTransaksi" => $addUser
		            	);
		            $this->db->insert('jurnaldetail', $datadtl);
		            $totkredit=0;
		            $totdebit=0;
		            $nilaivat = 0;
		        }
		    }	
		    $nodok = $nodokumen;
		}
		
        if($m > 0){
        	if($kdrekeningheader== ''){
				if($dntype=='D' || $dntype=='C'){
					$rekhutang = $rekening[0]['HutangDagang'];
				}else{
					$rekhutang = $rekening[0]['HutangLain'];
				}
			}else{
				$rekhutang = $kdrekeningheader;	
			}
        	
			if($nilaivat>0){
				if($dntype=='D' || $dntype=='DM'){
					$vatdebet = 0;
					$vatcredit = $nilaivat;
				}else{
					$vatdebet = $nilaivat;
					$vatcredit = 0;
				}
					
				$counterid++;
				$datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "CB",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $rekening[0]['PPnBeli'],
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $vatdebet,
		                "Kredit" => $vatcredit,
		                "KeteranganDetail" => $keterangan,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid,
                        "AddUserTransaksi" => $addUser
	            	);
	            $this->db->insert('jurnaldetail', $datadtl);	
			}
			
			if($dntype=='D' || $dntype=='DM'){
				$nilaihutangdb = $totkredit-$totdebit+$nilaivat;
				$nilaihutangcr = 0;
			}else{
				$nilaihutangdb = 0;
				$nilaihutangcr = $totdebit-$totkredit+$nilaivat;
			}
			
			$counterid++;
			$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "CB",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekhutang,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => $nilaihutangdb,
	                "Kredit" => $nilaihutangcr,
	                "KeteranganDetail" => $keterangan,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid,
                    "AddUserTransaksi" => $addUser
            	);
            $this->db->insert('jurnaldetail', $datadtl);
        }
	}

	function compliment($tahun, $bulan, $userid, $counterid, $noposting,$tgldokumen){
		$rekening = $this->create_jurnal_model->getinterface();
		$data = $this->create_jurnal_model->getCompliment($tahun, $bulan);
		
        $awalbulan = $tahun.'-'. ($bulan>=10 ? $bulan : '0'.$bulan) .'-01';		
        
        for ($m = 0; $m < count($data); $m++) {
            $kddivisi		= $data[$m]['KdDivisi'];
            $kdsubdivisi	= $data[$m]['KdSubDivisi'];
            $kddivisireport = $data[$m]['KdDivisiReport'];
            $namadivisireport = $data[$m]['NamaDivisiReport'];
            $nett = $data[$m]['total'];
            $kdrekpersediaan = $data[$m]['KdRekeningPersediaan'];
            
            //DB Promo CR Persediaan				
            $counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekpersediaan,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => 0,
                "Kredit" => $nett,
                "KeteranganDetail" => 'Compliment '. $namadivisireport,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            
            $counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "R",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekening[0]['Compliment'],
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $nett,
                "Kredit" => 0,
                "KeteranganDetail" => 'Compliment '. $namadivisireport,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
	}

	function posting_produksi($tahun, $bulan, $userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'PD', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PD', 'Bulan' => $bulan, 'Tahun' => $tahun));
		
		$rekening = $this->create_jurnal_model->getinterface();
		$data = $this->create_jurnal_model->getproduksi($tahun, $bulan);
		$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
        $noposting = $new_no->counter;
		
        $awalbulan = $tahun.'-'. $bulan .'-01';
        $tgldokumen = date('Y-m-t', strtotime($awalbulan));
        $tot = 0;
        
        $counterid = 0;
        for ($m = 0; $m < count($data); $m++) {
            $jenis		= $data[$m]['Jenis'];
            $kdrekpersediaan	= $data[$m]['KdRekeningPersediaan'];
            $namarekening = $data[$m]['NamaRekening'];
            $kdsubdivisi = $data[$m]['KdSubDivisi'];
            $nilai = $data[$m]['Nilai'];
            $addUser = $data[$m]['adduser'];
            if($jenis=='Mix')
            	$ket = 'Pemakaian bahan baku';
            else
            	$ket = 'Pemakaian bahan kemas';
            
            //DB FG CR Baku+Kemas
            $counterid ++;				
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PD",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekpersediaan,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => 0,
                "Kredit" => $nilai,
                "KeteranganDetail" => $ket,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $tot += $nilai;	
            $this->db->insert('jurnaldetail', $datadtl);
        }
        
        $counterid ++;
        $datadtl = array("NoReferensi" => $noposting,
            "TglTransaksi" => $tgldokumen,
            "KodeJurnal" => "PD",
            "KdDepartemen" => "00",
            "Project" => "00",
            "CostCenter" => "00",
            "KdRekening" => $rekening[0]['PersediaanFGKopi'],
			"KdSubDivisi"	=> $kdsubdivisi,
            "Debit" => $tot,
            "Kredit" => 0,
            "KeteranganDetail" => 'Persediaan Barang Jadi',
            "Bulan" => $bulan,
            "Tahun" => $tahun,
            "JenisJurnal" => "U",
            "AddDate" => $tgldokumen,
            "AddName" => $userid,
            "Counter" => $counterid,
            "AddUserTransaksi" => $addUser
        );
        $this->db->insert('jurnaldetail', $datadtl);
        
        $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => 'Produksi Kopi',
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PD',
                    "NoTransaksi"   => '',
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
        
	}
	
	function posting_realisasi_um($tahun, $bulan, $userid){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'RUM', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'RUM', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getrealisasi_UM($tahun, $bulan);
        	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$keterangan = 'Realisasi Uang Muka ';
			$keterangandetail = $header[$m]['Keterangan'];
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekdetail = $header[$m]['KdRekening'];
            $debit = $header[$m]['Debet'];
            $credit = $header[$m]['Kredit'];
            $addUser = $header[$m]['adduser'];
									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'RUM',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $AddUser);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $counterid++;
            $totkredit += $debit;
            $totdebit += $credit;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "RUM",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekdetail,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $debit,
                "Kredit" => $credit,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $AddUser
            );
            $this->db->insert('jurnaldetail', $datadtl);
            	
		    $nodok = $nodokumen;
		}
	}

    function posting_invoice_sanitasi($tahun, $bulan, $userid){
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'IS', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'IS', 'Bulan' => $bulan, 'Tahun' => $tahun));
        
       
        $rekening = $this->create_jurnal_model->getinterface();
        $header = $this->create_jurnal_model->getinvoice_sanitasi($tahun, $bulan);
        //$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
        //$noposting = $new_no->counter;
        
        $awalbulan = $tahun.'-'. $bulan .'-01';
        $tgldokumen = date('Y-m-t', strtotime($awalbulan));
        $tot = 0; 
        $nodok = '';        
        
        $counterid = 0;
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
            $tgldokumen = $header[$m]['Tanggal'];
            $keterangan = $header[$m]['Keterangan'];
            $Total_Nilai = $header[$m]['Total_Nilai'];
            $Diskon = $header[$m]['Diskon_Nilai'];
            $Total_DPP = $header[$m]['Total_DPP'];
            $Uang_Muka = $header[$m]['Uang_Muka'];
            $PPN = $header[$m]['PPN'];
            $PPH = $header[$m]['PPH'];
            $PPH_Nilai = $header[$m]['PPH_Nilai'];
            $Total = $header[$m]['Total'];
            $harga = $header[$m]['harga'];
            $barang = $header[$m]['barang'];
            $addUser = $header[$m]['AddUser'];
            $AddDate = $header[$m]['AddDate'];
            $nama = $header[$m]['Nama'];
            $pem = $header[$m]['Pembulatan'];
            $pembulatan = $Total - $pem;
            
            if ($nodok <> $nodokumen) {
            	$new_no = $this->create_jurnal_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => 'Jurnal Sanitasi '. $nama,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'IS',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);
            }   
             //Penjualan				
             $counterid ++;				
             $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "IS",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => '40010710',
                "KdSubDivisi"   => '106',
                "Debit" => 0,
                "Kredit" => $harga,
                "KeteranganDetail" => 'Pendapatan Jasa Sanitasi'. $nama,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
           
            $this->db->insert('jurnaldetail', $datadtl);            
             
             //Piutang
             $counterid ++;				
             $datadtl = array("NoReferensi" => $noposting,
                 "TglTransaksi" => $tgldokumen,
                 "KodeJurnal" => "IS",
                 "KdDepartemen" => "00",
                 "Project" => "00",
                 "CostCenter" => "00",
                 "KdRekening" => '11040000',
                 "KdSubDivisi"   => '106',
                 "Debit" => $pem,
                 "Kredit" => 0,
                 "KeteranganDetail" => 'Piutang Dagang '. $nama,
                 "Bulan" => $bulan,
                 "Tahun" => $tahun,
                 "JenisJurnal" => "U",
                 "AddDate" => $tgldokumen,
                 "AddName" => $userid,
                 "Counter" => $counterid,
                 "AddUserTransaksi" => $addUser
             );
             $this->db->insert('jurnaldetail', $datadtl);
             
             //disc sanitasi
            $counterid ++;              
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "IS",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => '40030700',
                "KdSubDivisi"   => '106',
                "Debit" => $Diskon,
                "Kredit" => 0,
                "KeteranganDetail" => 'Discount Penjualan Jasa Sanitasi'. $nama,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);  

            //ppn sanitasi
            $counterid ++;              
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "IS",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => '21060090',
                "KdSubDivisi"   => '106',
                "Debit" => 0,
                "Kredit" => $PPN,
                "KeteranganDetail" => 'PPN Keluaran - Jasa Sanitasi '. $nama,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);        
        
            //pph sanitasi
            $counterid ++;              
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "IS",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => '11100020',
                "KdSubDivisi"   => '106',
                "Debit" => $PPH_Nilai,
                "Kredit" => 0,
                "KeteranganDetail" => 'PPH Jasa Sanitasi '. $nama,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);   
            
            //pembulatan sanitasi
            $counterid ++;              
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "IS",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => '44010025',
                "KdSubDivisi"   => '106',
                "Debit" => $pembulatan,
                "Kredit" => 0,
                "KeteranganDetail" => 'Pembulatan Jasa Sanitasi '. $nama,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid,
                "AddUserTransaksi" => $addUser
            );
            $this->db->insert('jurnaldetail', $datadtl);   

            
            //Uang muka sanitasi
           /* $counterid ++;              
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "IS",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => '21020010',
                "KdSubDivisi"   => '106',
                "Debit" => $Uang_Muka,
                "Kredit" => 0,
                "KeteranganDetail" => 'Uang_Muka Jasa Sanitasi'. $nama,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl); */       
        
		    $nodok = $nodokumen;
                 
        }
        
       /*$data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => 'Jurnal Sanitasi'. $nama,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'IS',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid,
                    "AddUserTransaksi" => $addUser);
                $this->db->insert('jurnalheader', $data);*/
    }


	function test(){
		$awalbulan='2016-11-01';
		$tgldokumen = date('Y-m-t', strtotime($awalbulan));
		echo $tgldokumen;
	}
}

?>