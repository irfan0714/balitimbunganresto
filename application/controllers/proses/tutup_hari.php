<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tutup_hari extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('proses/tutup_hari_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
	        $data['track'] = $mylib->print_track();
			$data['msg'] = "";
			$last_update_query = $this->tutup_hari_model->getLastDate();
			$data['tanggal']= $last_update_query;
	        $this->load->view('proses/tutup_hari', $data);
		}else{
			$this->load->view('denied');
		}
	}

    /**
     *
     */
    function doThis()
	{

		$mylib = new globallib();
		$id = $this->session->userdata('userid');
		$tgl = $this->tutup_hari_model->getLastDate();//echo $tgl->TglTrans;die();
		$tahun = substr($tgl->TglTrans,0,4);
//                $this->cetak($tgl->TglTrans,$tahun); // cetak rekap harian hanya untuk counter
		$this->db->update("aplikasi",array("FlagTutupHari"=>"Y"),array("Tahun"=>$tahun));
	//	$usergantung = $this->tutup_hari_model->FindUser($id);
        $nextdates = $this->getNewDate($tahun,1);
//        print_r($nextdates);die;
		$bulanini = substr($nextdates->TglTrans,5,2);
		$tahunini = substr($nextdates->TglTrans,0,4);
		$bulanbesok = substr($nextdates->nextdate,5,2);
		$tahunbesok = substr($nextdates->nextdate,0,4);
		$session_name=$this->session->userdata('username');
		$tanggalbaru = $mylib->ubah_tanggal($nextdates->TglTrans);
//        print $tanggalbaru;die;
		$validTrans="";
		if($bulanini==$bulanbesok)
		{
			$validTrans="";
		}
		else
		{
			$order = $this->tutup_hari_model->cekOrder();

			if($order!=0)
			{
				$validTrans .= "<font color = 'red'><b> Masih Ada Order Barang Yang Belum Diproses</b></font><br>";
			}
			
		}
		
		if(!empty($usergantung))
		{
			$msg = "<font color = 'red'><b> Proses Tutup Hari Tidak Bisa Dilakukan
					Karena Masih Ada User Yang Mengakses Program<br>Gunakan Menu Hapus User Gantung
					Pada Menu Proses Untuk Menghapus User Gantung
					</b></font>";
					
		}
		$validTrans = "";
		if($validTrans!="")
		{
			$msg = $validTrans;
		}
		
		if(empty($usergantung)&&$validTrans=="")
		{
			//$this->HitungHutang($tahunini, $bulanini);
			//$this->HitungStok($tahunini, $bulanini);
			
			$fieldakhir_ini = "GAkhir".$bulanini;
			$fieldakhir_besok = "GAkhir".$bulanbesok;
			$fieldawal_besok = "GAwal".$bulanbesok;
//                        echo $nextdates->TglTrans;die();
			if($tahunini!=$tahunbesok)
			{ 
				$getAplikasi = $this->tutup_hari_model->getAllAplikasi($tahunini);
				//$getSetupPerusahaan = $this->tutup_hari_model->getSetupNo($tahunini);
				$prefix = substr($tahunbesok,2,2);
				$new_no = $prefix."000001";
				                //echo "setup no ok<br>";


				$stoksimpan = $this->tutup_hari_model->getStokSimpan($tahunini,$fieldakhir_ini);
				for($s=0;$s<count($stoksimpan);$s++)
				{
					$data = array(
						"Tahun"             =>$tahunbesok,
						"KdGudang"            => $stoksimpan[$s]['KdGudang'],
						"PCode"		        => $stoksimpan[$s]['PCode'],
						$fieldawal_besok    => $stoksimpan[$s][$fieldakhir_ini],
						$fieldakhir_besok   => $stoksimpan[$s][$fieldakhir_ini]
					);
					$this->db->insert("stock",$data);
				}
                                //echo "stock ok<br>";
//					$this->db->update("aplikasi",array("FlagTutupHari"=>"T"),array("Tahun"=>$tahunini));
					$this->db->update("aplikasi",array("FlagTutupHari"=>"T","TglTrans"=>$nextdates->nextdate,"Tahun"=>$tahunbesok),array("Tahun"=>$tahunini));
					
					$this->tambahCounter($tahunbesok,$bulanbesok);
                    $this->pindah_compliment($tahunini,$bulanini);
                    $this->db->update("compliment",array("terpakai"=>"0"));
			}
			else
			{
				if($bulanini!=$bulanbesok)
				{
					//$this->db->query("update stock set $fieldawal_besok=$fieldakhir_ini,$fieldakhir_besok=$fieldakhir_ini where Tahun='$tahunini'");
					$this->db->update("aplikasi",array("FlagTutupHari"=>"T","TglTrans"=>$nextdates->nextdate),array("Tahun"=>$tahunini));

                    $this->tambahCounter($tahunbesok,$bulanbesok);
                    $this->pindah_compliment($tahunini,$bulanini);
                    $this->db->update("compliment",array("terpakai"=>"0"));

                    //UPDATE compliment SET terpakai=0
				}
				else
				{
					$this->db->update("aplikasi",array("FlagTutupHari"=>"T","TglTrans"=>$nextdates->nextdate),array("Tahun"=>$tahunini));
				}
			}
			
			/*$sessiondata = array(
				'Tanggal_Trans' => $nextdates->nextdate
			);
			$this->session->set_userdata($sessiondata);*/
			$this->session->set_userdata('Tanggal_Trans', $nextdates->nextdate2);
			$msg = "<b> Proses Tutup Hari Berhasil</b>";
			$tanggalbaru = $mylib->ubah_tanggal($nextdates->nextdate);
		}
		$this->db->update("aplikasi",array("FlagTutupHari"=>"T"),array("Tahun"=>$tahun));
		$head_sess = "<b>$session_name/".$tanggalbaru."</b>";
		echo $msg."&&&".$head_sess;
	}
	
	function HitungHutang($tahun, $bulan){
		//echo 'in';
		$bulan = $bulan * 1;
		$next_bulan = $bulan+1;		
		$next_tahun = $tahun;
		for($i=2;$i<=$bulan;$i++){
			$lalu= $i-1;
			$sql = "delete from mutasi_hutang where Tahun='$next_tahun' and Bulan='$i' and TipeTransaksi in('O','OM')";
			$qry = $this->db->query($sql);
							
			$sql ="insert into mutasi_hutang  (Tahun, Bulan, NoDokumen, Tanggal, MataUang, Kurs, KdSupplier, TipeTransaksi, Jumlah)
					SELECT '$next_tahun', '$i', NoDokumen, Tanggal, MataUang, Kurs, KdSupplier,  if(substring(NoDokumen,3,1)='M','OM','O'), sum(Jumlah)
					FROM(
						SELECT NoDokumen, Tanggal, MataUang, Kurs, KdSupplier, Jumlah 
						FROM mutasi_hutang 
						WHERE Tahun='$tahun' AND Bulan='$lalu' AND TipeTransaksi IN('O','I','C','OM','IM','CM')
										UNION ALL
						SELECT apd.NoFaktur, '', ap.MataUang, ap.Kurs, ap.KdSupplier, (apd.NilaiBayar)*-1 
						FROM mutasi_hutang ap INNER JOIN pelunasan_hutang_detail apd ON ap.NoDokumen=apd.NoTransaksi
						WHERE Tahun='$tahun' AND Bulan='$lalu' AND TipeTransaksi in('P','PM') 
										UNION ALL
						SELECT dnad.InvNo, '', ap.MataUang, ap.Kurs, ap.KdSupplier, dnad.Allocation*-1 
						FROM mutasi_hutang ap INNER JOIN dnallocation dna ON ap.NoDokumen=dna.DNNo
						INNER JOIN dnallocationdetail dnad ON dna.dnallocationno = dnad.dnallocationno
						WHERE Tahun='$tahun' AND Bulan='$lalu' AND TipeTransaksi in('D','DM') AND status=1
					) tbl
					GROUP BY NoDokumen, MataUang, KdSupplier HAVING SUM(Jumlah)<>0";
			$qry = $this->db->query($sql);
		}
		//echo 'out';
	}
	
	function HitungStok($tahun, $bulan){
		
	}

    function pindah_compliment($tahun,$bulan)
    {
        $sql ="
        INSERT INTO compliment_ok (
        tahun, bulan, nik, nominal, expDate, ADDDATE, userAdd, STATUS, keterangan, terpakai, jenis
        )
        (
        SELECT
          '$tahun', '$bulan', nik, nominal, expDate, ADDDATE, userAdd, STATUS, keterangan, terpakai, jenis
        FROM
          compliment
          )
        ";
        $this->db->query($sql);

    }

    function tambahCounter($tahunbesok,$bulanbesok){
        $datacounter = array(
            'Tahun' => $tahunbesok,
            'Bulan' => $bulanbesok,
            'NoJurnal' => substr($tahunbesok, 2, 2) . $bulanbesok . '2400001',
            'NoReceipt' => substr($tahunbesok, 2, 2) . $bulanbesok . '2500001',
            'NoPayment' => substr($tahunbesok, 2, 2) . $bulanbesok . '2600001',
            'NoStruk' => substr($tahunbesok, 2, 2) . $bulanbesok . '2900001',
            'NoKomisi' => substr($tahunbesok, 2, 2) . $bulanbesok . '3100001',
            'NoStruk01' => substr($tahunbesok, 2, 2) . $bulanbesok . '100001',
            'NoStruk02' => substr($tahunbesok, 2, 2) . $bulanbesok . '200001',
            'NoStruk03' => substr($tahunbesok, 2, 2) . $bulanbesok . '300001'
        );

        $this->db->insert('counter', $datacounter);
    }
	function getNewDate($tahun,$interval)
	{
		$apl = $this->tutup_hari_model->getAllAplikasi($tahun);
		$nextdates = $this->tutup_hari_model->FindNextDate($tahun,$interval);
		$bulanbesok = substr($nextdates->nextdate,5,2);
		$tahunbesok = substr($nextdates->nextdate,0,4);
		$tanggalbesok = substr($nextdates->nextdate,8,2);
		$hari = date("w", mktime(0, 0, 0, $bulanbesok, $tanggalbesok, $tahunbesok));
		//$sabtumasuk = $apl->SabtuMasuk;
//		if($sabtumasuk=="N"&&$hari==6)  // 0 = minggu, 1 = senin
//		{
//			$interval++;
//			$nextdates = $this->getNewDate($tahun,$interval);
//		}
//		if($hari==0)
//		{
//			$interval++;
//			$nextdates = $this->getNewDate($tahun,$interval);
//		}
		return $nextdates;
	}
        
        function cetak($tgl,$tahun)
	{
               // $no	= $this->uri->segment(4);

                $printer = $this->tutup_hari_model->NamaPrinter($_SERVER['REMOTE_ADDR']);

                        $data['ip']             = $printer[0]['ip'];
                        $data['nm_printer']     = $printer[0]['nm_printer'];
                        $data['store']		= $this->tutup_hari_model->aplikasi();
			$data['header']		= $this->tutup_hari_model->all_trans($tgl);
//print_r($data['header']);
//			$data['detail']		= $this->tutup_hari_model->det_trans($no);
//die();
//                        $data['ip']    = "\\\\".."\LQ-2170s";

            if(!empty($data['header'])){
//		$this->load->view('proses/cetak_tutup',$data); // jika untuk tes
		$this->load->view('proses/cetak_transaksi',$data); // jika ada printernya
            }
        }
}
?>