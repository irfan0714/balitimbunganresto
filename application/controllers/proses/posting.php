<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class posting extends authcontroller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('proses/posting_model');
    }

    function index($msg = "")
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $userid = $this->session->userdata('userid');
        $periodgl = $this->posting_model->getPeriodeGL();
        list($tahunaktif,$bulanaktif,$tglaktif) = explode('-',$periodgl);
        $tanggaltrans = $this->session->userdata('Tanggal_Trans');
        
        if ($sign == "Y") {
            $data['bulanaktif'] = $bulanaktif;
            $data['tahunaktif'] = $tahunaktif;
            $data['tanggaltrans'] = $tanggaltrans;
            $data['tahun'] = $this->posting_model->getListTahun();
            $data['bulan'] = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            $data['track'] = $mylib->print_track();
            $data['msg'] = $msg;
            $this->load->view('proses/posting/posting', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save()
    {
        $mylib = new globallib();
        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $userid = $this->session->userdata('userid');
        $interface = $this->posting_model->getinterface();
        
        $this->posting_to_GL($tahun,$bulan,$userid);//GL
        
        $msg = "Posting data bulan " . $bulan . " - " . $tahun . " selesai";
        $this->index($msg);
    }

	function posting_to_GL($tahun,$bulan,$userid){
		$this->db->delete('jurnaldetail', array('Debit' => 0, 'Kredit' => 0, 'Bulan' => $bulan, 'Tahun' => $tahun));

        /* posting jurnaldetail ke saldoGL */
        $BulanAktif1 = $bulan;
        $TahunAktif1 = $tahun;
        $BulanAktif2 = $bulan;
        $TahunAktif2 = $tahun;
        $Tahun = $TahunAktif1;
        for ($j = $BulanAktif1; $j <= $BulanAktif2; $j++) {
            $Bulan = $j;
            if (strlen($Bulan) == 1) {
                $Bulan = "0" . $Bulan;
            }
            // echo $Bulan;
            // echo $Tahun;
            if ($Bulan == "01") {
                $TahunAwal = $Tahun - 1;
                //echo $TahunAwal;
                $ListAwal = $this->posting_model->AkhirSaldoGL("12", $TahunAwal);
                
                if (!empty($ListAwal)) {
                    $this->posting_model->KosongSaldoAwalGL($Bulan, $Tahun);
                    for ($i = 0; $i < count($ListAwal); $i++) {
                        $KdDepartemen = $ListAwal[$i]['Departemen'];
                        $KdRekening = $ListAwal[$i]['KdRekening'];
                        $KdSubDivisi = $ListAwal[$i]['KdSubDivisi'];
                        $Saldo = $ListAwal[$i]['Saldo'];
                        $wherecek = array("Departemen" => $KdDepartemen,
                            "Tahun" => $Tahun,
                            "KdRekening" => $KdRekening,
                            "KdSubDivisi" => $KdSubDivisi); 
                        $AdaGak = $this->posting_model->cekdata("saldo_gl", $wherecek);
                        if ($AdaGak == 0) {
                            $this->posting_model->addData('saldo_gl', $wherecek);
                        }
                        if ($this->posting_model->posisirek($KdRekening) <= "3") {
                            $this->posting_model->UpdateSaldoAwal01($Saldo, $Tahun, $KdDepartemen, $KdRekening, $KdSubDivisi);
                        }
                    }
	                
	                //Laba Ditahan
	                $ListSaldoRL = $this->posting_model->AkhirSaldoRL("12", $TahunAwal, $this->posting_model->LabaPeriode());
                    
                    for ($i = 0; $i < count($ListSaldoRL); $i++) {
                        $KdDepartemen = $ListSaldoRL[$i]['Departemen'];
                        $KdRekening = $ListSaldoRL[$i]['KdRekening'];
                        $KdSubDivisi = $ListSaldoRL[$i]['KdSubDivisi'];
                        $Saldo = $ListSaldoRL[$i]['Saldo'];
                        $LabaDiTahan = $this->posting_model->LabaDiTahan();
                        $wherecek = array("Departemen" => $KdDepartemen,
                            "Tahun" => $Tahun,
                            "KdRekening" => $LabaDiTahan,
                            'KdSubDivisi'=> $KdSubDivisi,
                            'Departemen' => $KdDepartemen); //
                        $AdaGak = $this->posting_model->cekdata("saldo_gl", $wherecek);
                        if ($AdaGak == 0) {
                            $this->posting_model->addData('saldo_gl', $wherecek);
                        }
                        $this->posting_model->UpdateSaldoRL($Saldo, $Tahun, $KdDepartemen, $LabaDiTahan, $KdSubDivisi);
                        $this->posting_model->UpdateSaldoAwal01(0, $Tahun, $KdDepartemen, $KdRekening, $KdSubDivisi);
                    }
                }
            } else {
                $BulanAwal = $Bulan - 1;
                if (strlen($BulanAwal) == 1) {
                    $BulanAwal = "0" . $BulanAwal;
                }
                $this->posting_model->KosongSaldoAwalGL($Bulan, $Tahun);
                $this->posting_model->UpdateSaldoAwal2($Bulan, $Tahun, $BulanAwal);
            }
            $this->posting_model->KosongSaldoGL($Bulan, $Tahun);
            $ListDtl = $this->posting_model->getDataJur($Bulan, $Tahun);
            // echo "<pre>";print_r($ListDtl);
            if (!empty($ListDtl)) {
                for ($i = 0; $i < count($ListDtl); $i++) {
                    $KdDepartemen = $ListDtl[$i]['KdDepartemen'];
                    $KdRekening = $ListDtl[$i]['KdRekening'];
                    $KdSubDivisi = $ListDtl[$i]['KdSubDivisi'];
                    $Debit = $ListDtl[$i]['Debit'];
                    $Kredit = $ListDtl[$i]['Kredit'];
                    $DebitAdj = $ListDtl[$i]['DebitAdj'];
                    $KreditAdj = $ListDtl[$i]['KreditAdj'];
                    // echo $i." => ".$KdSubDivisi."<br>";
                    $wherecek = array("Departemen" => $KdDepartemen,
                        "Tahun" => $Tahun,
                        "KdRekening" => $KdRekening,
                        "KdSubDivisi" => $KdSubDivisi);  //
                    // echo "<pre>";print_r($wherecek);
                    $AdaGak = $this->posting_model->cekdata("saldo_gl", $wherecek);

                    if ($AdaGak == 0) {
                        $this->posting_model->addData('saldo_gl', $wherecek);
                    }
                    $this->posting_model->UpdateSaldoGL($KdDepartemen, $Bulan, $Tahun, $KdRekening, $Debit, $Kredit, $DebitAdj, $KreditAdj, $KdSubDivisi);
                    if ($this->posting_model->posisirek($KdRekening) > "3") {

                        $wherecek = array("Departemen" => $KdDepartemen,
                            "Tahun" => $Tahun,
                            "KdRekening" => $this->posting_model->LabaPeriode(),
                            "KdSubDivisi" => $KdSubDivisi); //
                        $AdaGak = $this->posting_model->cekdata("saldo_gl", $wherecek);
                        if ($AdaGak == 0) {
                            $this->posting_model->addData('saldo_gl', $wherecek);
                        }
                        $this->posting_model->UpdateSaldoGL($KdDepartemen, $Bulan, $Tahun, $this->posting_model->LabaPeriode(), $Debit, $Kredit, $DebitAdj, $KreditAdj, $KdSubDivisi);
                    }
                }
            }
        }
        
	}
	
	
	function test(){
		$awalbulan='2016-11-01';
		$tgldokumen = date('Y-m-t', strtotime($awalbulan));
		echo $tgldokumen;
	}
}

?>