<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class hitungstock extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

	function index(){
		$mylib = new globallib();
		
		$data['tahun'] = '';
		$data['bulan'] = '';
		$this->load->view('proses/hitungstock', $data);
	}

	function proses($tahun, $bulan){
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');

		$this->load->library('stocklib');

		$this->stocklib->recalculate_stock_bulanan($tahun, $bulan);
		print 'data:Selesai' . PHP_EOL . PHP_EOL;
		flush();
	}

	function prosesbulanan($tahun, $bulan){
		//header('Content-Type: text/event-stream');
		//header('Cache-Control: no-cache');

		$this->load->library('stocklib');

		$this->stocklib->recalculate_stock_bulanan($tahun, $bulan);
		print 'data:Selesai' . PHP_EOL . PHP_EOL;
		flush();
	}

    function konversiawal(){
    	$this->InitData();
    	$rec = $this->GetTempData();
    	for($i=0;$i<count($rec);$i++){
			$tahun = $rec[$i]['Tahun'];
			$kdgudang = $rec[$i]['KdGudang'];
			$pcode = $rec[$i]['PCode'];
			$gakhir01 = $rec[$i]['GAkhir01'];
			$gnakhir01 = $rec[$i]['GNAkhir01'];

			$data = array(
        		'Tahun' => $tahun,
        		'KdGudang' => $kdgudang,
        		'PCode'	=> $pcode,
        		'Status' => 'G',
        		'GAkhir01' => $gakhir01,
        		'GNAkhir01' => $gnakhir01,
        		'GAwal02'	=> $gakhir01,
        		'GNAwal02' => $gnakhir01,
        		'GAkhir02'	=> $gakhir01,
        		'GNAkhir02' => $gnakhir01
				);

			$this->db->replace('stock', $data);
		}
		echo 'Finish';
    }

    function InitData(){
		$sql = "Update stock set GAkhir01=0, GNAkhir01=0, GAWal02=0, GNAwal02=0 where Tahun=2017";
		$qry = $this->db->query($sql);
	}

    function GetTempData(){
		$sql = "SELECT Tahun, KdGudang, PCode, GAkhir01, GNAkhir01 FROM temp_stockawal";
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        return $row;
	}

	function test(){
		$tgl='2017-08-09';
		$this->load->library('stocklib');
		echo 'a';
		$this->stocklib->_FinishGoods($tgl);
		echo 'b';
	}
}
?>
