<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tarik_data extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
//	$this->load->library('koneksi_oracle');
        $this->load->model('proses/tarikdata_model');
    }
    
    function index(){
        $mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
        {
//            $data['listTerima'] = $this->tarikdata_model->getlistTerima();
            $data['track']       = $mylib->print_track();
                $this->load->view('proses/download/tarik_data', $data);
            }
            else{
                $this->load->view('denied');
            }
    }
    function eksekusi()
    {
        $mylib = new globallib();
        //print_r($_POST);die();
        $tglnya = $this->input->post('tglawal');


//        $mylib = new globallib();
//        $tgl2   = $this->session->userdata('Tanggal_Trans');
//        $tgl    = $mylib->ubah_tanggal($tgl2);
//        $bulan = substr($tgl,5,2);
//        $tahun = substr($tgl,0,4);

        //$tarikdata      = $this->tarikdata_model->getPLCabang($tglawal);
        //print_r($tarikdata);die();

    if(!empty($tglnya)){
        list ($bulan, $tahun) = explode ('-', $tglnya);
        $hdr    = 0;
        $dtl    = 0;
        $kodeCBG = $this->tarikdata_model->getCabang('NB');// NB= Natura Bedugul
        //print_r($kodeCBG);die();
            //$cabang = $this->Global_cabang_model->getField("select * from  cabang_depo WHERE id_cd='$c['id_cd']'");
            echo $hdr	= $this->downloadPV($kodeCBG,$bulan,$tahun);//die();
            //$hdr	= $this->Insert_HRG_Header($kdHrg,$c);
//            $dtl	= $this->Insert_HRG_Detail($kdHrg,$c);
            $hsl	= $hdr + $dtl;
            if($hsl == 0){
                $data['msg'] ="Data telah di download";
            }else{
                $data['msg']  ="gagal Download";
            }
        }
        $data['track']       = $mylib->print_track();
        $this->load->view('proses/download/tarik_data', $data);

    }
    function downloadPV($cb,$bulan,$tahun){
        $sql    = "SELECT * FROM trans_payment_header
                    WHERE MONTH(TglDokumen)='$bulan' AND YEAR(TglDokumen)='$tahun'
                    AND (STATUS <> 'B' or Status is NULL)
                    ORDER BY NoDokumen";
        //    $table	= "grupharga";

//        Array ( [0] => Array ( [KdCabang] => 99 [NamaCabang] => Natura Bedugul
//        [Ip] => 192.168.0.193 [Initial] => NB [User] => natura [Kode] => dbnatura [NamaDB] => dbnatura ) )
            $dsn	= "mysql://".$cb[0]['User'].":".$cb[0]['Kode']."@".$cb[0]['Ip']."/".$cb[0]['NamaDB'];
            $db_obj = $this->load->database($dsn,TRUE);
            if($db_obj->conn_id) {
                $this->dba = $this->load->database($dsn, true);
                $hd =  $this->dba->query($sql);
                $tt   = $hd->result_array();
                //echo "<pre>";print_r($hd);echo "</pre>";//die();
                $this->db->delete('trans_payment_header_cbg', array('MONTH(TglDokumen)' => $bulan, 'YEAR(TglDokumen)' =>$tahun ));
                foreach($tt as $d ){
                    $dt	= array(
                        'NoDokumen'     => $d['NoDokumen'],
                        'TglDokumen'    => $d['TglDokumen'],
                        'KdKasBank'     => $d['KdKasBank'],
                        'KdCostCenter'  => $d['KdCostCenter'],
                        'KdPersonal'    => $d['KdPersonal'],
                        'keterangan'    => $d['keterangan'],
                        'JumlahPayment' => $d['JumlahPayment'],
                        'AddUser'       => $d['AddUser'],
                        'EditUser'      => $d['EditUser'],
                        'Jenis'         => $d['Jenis'],
                        'NoBukti'       => $d['NoBukti'],
                        'AddDate'	    => $d['AddDate'],
                        'EditDate'	    => $d['EditDate']
                    );
                    $this->db->insert('trans_payment_header_cbg', $dt);
                }
                $this->dba->close();
                $nil = 0;
            } else {
                //echo 'Unable to connect with database with given db details.';
                $nil = 1;
            }
            return $nil;

    }
}
?>
