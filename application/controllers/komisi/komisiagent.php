<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class KomisiAgent extends authcontroller {

    function __construct() {
        parent::__construct();
		$this->load->model('master/komisimodel');
		$this->load->library('globallib');
    }

    function index() {
//		$mylib			= new globallib();
//		$data['track']	= $mylib->print_track();
		
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		if($sign =='Y') {
			
			$this->load->library('pagination');

			$urisegment	= 6;
			$keyword	= $this->uri->segment($urisegment -2);
			$keywordurl = $keyword;
			$keywordurl2 = $this->uri->segment($urisegment -1);

			if(($keyword=='') || ($keyword=='nokeyword')){
					$keyword	= '';
					$keywordurl = 'nokeyword';
					$keywordurl2 = 'nokeyword';
			}
			if($this->input->post('submit')=='Cari'){
				$keyword    = $this->input->post('optionValue');
				$keywordurl2 = $this->input->post('option');
				if($keyword ==''){
					$keywordurl = 'nokeyword';
					$keywordurl2 = 'nokeyword';
				}
				else{
					$keywordurl = $keyword;
				}
			}
			$dataperpage				= 10;
			$config['per_page']         = $dataperpage;
			$config['base_url']         = site_url()."/komisi/komisiagent/index/$keywordurl/$keywordurl2/";
			$config['uri_segment']      = $urisegment;
			$config['total_rows']       = $this->komisimodel->getNumRowsDataView($keyword, $keywordurl2);

			$config['full_tag_open']	= '<ul class="pagination pagination-sm">';
			$config['full_tag_close']	= '</ul>';
			$config['cur_tag_open']		= '<li class="active"><a href="javascript:void(0);">';
			$config['cur_tag_close']	= '</a></li>';
			$config['first_link']		= 'First';
			$config['last_link']		= 'Last';
			$config['num_links']		= 5;

			$this->pagination->initialize($config);
			$fromurisegment				= $this->uri->segment($urisegment);
			$data['viewdata']			= $this->komisimodel->viewData($dataperpage, $fromurisegment, $keyword, $keywordurl2);

			$this->load->view('master/komisi/viewlist', $data);
		}
		else {
			$this->load->view('denied');
		}
    }
	
	function tambah() {
		$this->load->library('globallib');
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		if($sign =='Y') {
			$data['mastertravel']	= $this->komisimodel->getMasterTravel();
			$data['masterbarang']	= $this->komisimodel->getMasterBarang();
			$this->load->view('master/komisi/tambahform', $data);
		}
		else {
			$this->load->view('denied');
		}
	}	
	
	function tambahproses() {
		$submit		= $this->input->post('submit');
		$kdtravel	= $this->input->post('KdTravel');
		
		if($submit) {
			$dataheader	= array(
							'KdTravel'		=> $kdtravel,
							'Keterangan'	=> $this->input->post('Keterangan'),
							'Status'		=> 1,
							'Komisi1'		=> $this->input->post('Komisi1H'),
							'Komisi2'		=> $this->input->post('Komisi2H'),
							'Komisi3'		=> $this->input->post('Komisi3H'),
							'Komisi4'		=> $this->input->post('Komisi4H')
						);
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			$this->db->insert('komisi_header', $dataheader);
			
			$pcode_arr		= $this->input->post('PCode');
			$komisi1_arr	= $this->input->post('Komisi1');
			$komisi2_arr	= $this->input->post('Komisi2');
			$komisi3_arr	= $this->input->post('Komisi3');
			$komisi4_arr	= $this->input->post('Komisi4');
			$komisi4bag_arr	= $this->input->post('Komisi4bag');
			
			for($i=0;$i<count($pcode_arr);$i++) {
				$total		= $komisi1_arr[$i] + $komisi2_arr[$i] + $komisi3_arr[$i] + $komisi4_arr[$i] + $komisi4bag_arr[$i];
				$datadetail	= array(
								'KdTravel'		=> $kdtravel,  
								'PCode'			=> $pcode_arr[$i],
								'Komisi1'		=> $komisi1_arr[$i],
								'Komisi2'		=> $komisi2_arr[$i],
								'Komisi3'		=> $komisi3_arr[$i],
								'Komisi4'		=> $komisi4_arr[$i],
								'Komisi4bag'	=> $komisi4bag_arr[$i],
								'Total'			=> $total
							);
				$this->db->insert('komisi_detail', $datadetail);
			}
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
			redirect('komisi/komisiagent/');
		}		
	}
	
	function edit($id) {
		$data['masterbarang']	= $this->komisimodel->getMasterBarang();
		$data['dataheader']		= $this->komisimodel->getKomisiHeader($id);
		$data['datadetail']		= $this->komisimodel->getKomisiDetail($id);
		$data['komdat']			= array(
									array('OpVal'	=> 'OFFICE'),
									array('OpVal'	=> 'GUIDE'),
									array('OpVal'	=> 'TOUR LEADER'),
									array('OpVal'	=> 'DRIVER')
								);
		$this->load->view('master/komisi/editform', $data);
	}
	
	function editproses($id) {
		$submit		= $this->input->post('submit');
		$kdtravel	= $this->input->post('KdTravel');
		
		if($submit) {
			$dataheader	= array(
								'Keterangan'	=> $this->input->post('Keterangan'),
								'Komisi1'		=> $this->input->post('Komisi1H'),
								'Komisi2'		=> $this->input->post('Komisi2H'),
								'Komisi3'		=> $this->input->post('Komisi3H'),
								'Komisi4'		=> $this->input->post('Komisi4H')
							);
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			$this->db->update('komisi_header', $dataheader, array('KdTravel'	=> $kdtravel));		
			$this->db->delete('komisi_detail', array('KdTravel' => $kdtravel));
			
			$pcode_arr		= $this->input->post('PCode');
			$komisi1_arr	= $this->input->post('Komisi1');
			$komisi2_arr	= $this->input->post('Komisi2');
			$komisi3_arr	= $this->input->post('Komisi3');
			$komisi4_arr	= $this->input->post('Komisi4');
			$komisi4bag_arr	= $this->input->post('Komisi4bag');

			for($i=0;$i<count($pcode_arr);$i++) {
				$total		= $komisi1_arr[$i] + $komisi2_arr[$i] + $komisi3_arr[$i] + $komisi4_arr[$i] + $komisi4bag_arr[$i];
				$datadetail	= array(
								'KdTravel'		=> $kdtravel,  
								'PCode'			=> $pcode_arr[$i],
								'Komisi1'		=> $komisi1_arr[$i],
								'Komisi2'		=> $komisi2_arr[$i],
								'Komisi3'		=> $komisi3_arr[$i],
								'Komisi4'		=> $komisi4_arr[$i],
								'Komisi4bag'	=> $komisi4bag_arr[$i],
								'Total'			=> $total
							);
				$this->db->insert('komisi_detail', $datadetail);
			}
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
			redirect('komisi/komisiagent/');
		}
	}
	
	function _index() {
		echo 'test';
	}
}
