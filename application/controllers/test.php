<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Test extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
	 	$this->load->library('session');
		$this->load->model('Globalmodel');

    }

    function index() 
    {
    
    	$this->test_send_email('test mail', 'test email', 'tny@vci.co.id', 'tny', 'auto'); 
    }
    
    function test_send_email($subject, $body, $to, $to_name, $author)
	{
		
	 	$this->load->library('email');
   		$user = $this->session->userdata('username');
   		
   		$rowMail = $this->Globalmodel->getEmail();
   		
   		$arr_to      = explode(";", $to);
	    $arr_to_name = explode(";", $to_name);
	    
	    $this->email->subject($subject);
	    $this->email->from($rowMail->email_address, $rowMail->subject); 
	    
	    //for($i=0;$i<(count($arr_to)-1);$i++)
	    //{
	    //	$this->email->to($arr_to[$i]);
	    //}
        $this->email->to($arr_to);
        $this->email->message($body); 
   
        //Send mail 
        if($this->email->send()) 
        {
        	$msg = $this->session->set_flashdata('msg', array('message' => 'Email sent successfully.', 'class' => 'success'));
        	$results = "Successfull";
        	print_r($this->email->_debug_msg);	
		}
        else
        {
        	$msg = $this->session->set_flashdata('msg', array('message' => 'Error in sending Email.', 'class' => 'danger'));
        	$results = "Error message";
         	
         	print_r($this->email->_debug_msg);	
		} 
		
		$data = array(
            'host' => $rowMail->host,
            'email_from' => $rowMail->email_address,
            'email_to' => $to,
            'subject' => $subject,
            'message' => $body,
            'status' => $results,
            'author' => $user." - ".date('d/m/Y H:i:s'),
            'email_date' => date('Y-m-d H:i:s')
        );
        
       	echo $msg;
	}
	
}

?>