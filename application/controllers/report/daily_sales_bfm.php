<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class daily_sales_bfm extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/daily_sales_bfm_model','daily_sales_model');
    }

    function index(){
     	//$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['jenis'] = 'A';
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/daily_sales_bfm/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->daily_sales_model->aplikasi();
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		$jenis = $this->input->post("jenis");
		
		$data['jenis'] = $jenis;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$data['kassa'] = $this->daily_sales_model->getkassa();
		$data['guest'] = $this->daily_sales_model->getGuest($tgldari,$tglsampai, $jenis);
		$data['struck'] = $this->daily_sales_model->getStruck($tgldari,$tglsampai, $jenis);
		$data['salesrec'] = $this->daily_sales_model->getsales($tgldari,$tglsampai, $jenis);
		$data['bruto'] = $this->daily_sales_model->getBruto($tgldari,$tglsampai, $jenis);
		
		
		$data['listemail'] = $this->daily_sales_model->getlistemail();
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Laporan Penjualan Harian';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/daily_sales_bfm/views', $data);
		}
		else
		{
			$this->load->view("report/daily_sales_bfm/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail()
	{
		$mylib = new globallib();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		$data['store']		= $this->daily_sales_model->aplikasi();
		$data['excel'] = '';
		$data['print'] = '';
		$data['jenis'] = 'A';
		$data['v_date_from'] = $awalbulan;
		$data['v_date_to'] = date('d-m-Y');
		$judul = array();
		$jenis = 'A';
		
		$tgldari = $mylib->ubah_tanggal($awalbulan);
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['tampilkanDT'] = true;
		$data['kassa'] = $this->daily_sales_model->getkassa();
		$data['guest'] = $this->daily_sales_model->getGuest($tgldari,$tglsampai, $jenis);
		$data['struck'] = $this->daily_sales_model->getStruck($tgldari,$tglsampai, $jenis);
		$data['salesrec'] = $this->daily_sales_model->getsales($tgldari,$tglsampai, $jenis);
		$data['bruto'] = $this->daily_sales_model->getBruto($tgldari,$tglsampai, $jenis);
		$data['listemail'] = $this->daily_sales_model->getlistemail();
		
		$data['kirim_email']='Y';
				
		$data['judul'] = 'Laporan Penjualan Harian';
		
		$this->load->view("report/daily_sales_bfm/reportRT", $data);
	}
	
}
?>