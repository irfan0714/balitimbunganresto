<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Insentif_gsa extends authcontroller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('report/report_insentif_gsa');
	}
 

	public function index()
	{  	

		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {

			$today					= date('Y-m-d');
			
			$data['bulan']			= "";
		    $data['tahun']			= "";
			$data['tampilkanDT'] 	= false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_insentif_gsa/view_insentif_gsa', $data);	
//		} else {
//			$this->load->view('denied');
//		}
		
	}

	public function tampil() 
	{ 

		$mylib = new globallib();
		
		$submit			= $this->input->post('submit');
		
		$bulan		= $this->input->post('bulan');
		$tahun		= $this->input->post('tahun');
		//tanggal_awal dan akhir
		$tgldari = $tahun."-".$bulan."-01";
		$jmlhari = $this->report_insentif_gsa->jmlhari($tgldari);
		$tglsampai = $jmlhari[0]['tgl'];
		
		$today			= date('Y-m-d');
		$tanggaldari	= strtotime($tgldari);
		$tanggalsampai	= strtotime($tglsampai);
		
		//menghitung jumlah hari minggu pada bulan tertentu
		$date1 = $mylib->ubah_tanggal($tgldari);
		$date2 = $mylib->ubah_tanggal($tglsampai);
		
		$pecahtgl1=explode("-",$date1);
		$tgl1=$pecahtgl1[0];
		$bln1=$pecahtgl1[1];
		$thn1=$pecahtgl1[2];
		
		$i=0;
		$sum=0;
		$jmlhari=0;
		
		do{
		$tanggal = date('d-m-Y',mktime(0,0,0,$bln1,$tgl1+$i,$thn1));

		if(date('w',mktime(0,0,0,$bln1,$tgl1+$i,$thn1))==0)
		{
		$sum++;
		}

		$jmlhari++;
		$i++;
		}
		while ($tanggal!=$date2);
		$jml_hraktif = $jmlhari - $sum;
		$data['jmlhraktif']	= $jml_hraktif;

			if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai)) ) {
				$tgldari		= $today;
				$tglsampai		= $today;
				$namadep		= "";
			}
			
		$data['bulan']				= $bulan;
		$data['tahun']				= $tahun;
		$data['submit']				= $submit;
		$data['tanggaldari']		= $tgldari;
		$data['tanggalsampai']		= $tglsampai;
		$data['jmlhari']			= $this->report_insentif_gsa->datediff($tglsampai, $tgldari);
		$data['tampilkanDT'] 		= true;
		$data['viewdata']			= $this->report_insentif_gsa->viewData($tgldari,$tglsampai);
		$data['viewdatahr']			= $this->report_insentif_gsa->viewDataHr($tgldari,$tglsampai);
		$data['viewdatatot']			= $this->report_insentif_gsa->viewDataTot($tgldari,$tglsampai);
		$data['listemp']			= $this->report_insentif_gsa->getListEmp($tgldari,$tglsampai);
		$data['listemp2']			= $this->report_insentif_gsa->getListEmp2();
		$data['listemp3']			= $this->report_insentif_gsa->getListEmp3();
		$data['track'] = $mylib->print_track();
		//echo "<pre>";print_r($data['viewdata']);echo "</pre>";die;

		 if($submit=='TAMPIL')
		{
			$this->load->view('report/report_insentif_gsa/view_insentif_gsa', $data);	
		}
		
		else if($submit=='XLS')
		{
			//$this->_printxls($dataresult);
		
				//$data['track'] = $mylib->print_track();
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="reportgsa.xls"');
                
                $this->load->view('report/report_insentif_gsa/tampil_insentif_gsa', $data);	
		}        
		
		
	}

}

/* End of file absensi.php */
/* Location: ./application/controllers/absensi.php */