<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class bukubesar extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/bukubesar_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->bukubesar_model->getDate();
			list($tahunaktif, $bulanaktif, $tglaktif)  = explode('-',$tanggal);
			
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['listdept'] = $this->bukubesar_model->getDept();
			$data['dept'] = "00";
			$data['listrekening'] = $this->bukubesar_model->getRekening();
			$data['rekening'] = "";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['bulanaktif1'] = $bulanaktif;
			$data['bulanaktif2'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['listtahun'] = $this->bukubesar_model->getListTahun();
			$data['rekeningaktif1'] ='';
			$data['rekeningaktif2'] ='';
			$data['listdivisi'] = $this->bukubesar_model->getListDivisi();
			$data['listbulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['tahun2'] = $this->bukubesar_model->getListTahun();
			$data['track'] = $mylib->print_track();
			//print_r($data);
			$this->load->view('report/bukubesar/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
        //print_r($_POST);die();
		$mylib = new globallib();
		$data['store']		= $this->bukubesar_model->aplikasi();
		$bulanaktif1     = $this->input->post("bulan1");
		$bulanaktif2    = $this->input->post("bulan2");
		$tahunaktif     = $this->input->post("tahun1");
		$kddivisi     = $this->input->post("divisi");
		$kdrekening1   = $this->input->post("KdRekening1");
		$kdrekening2   = $this->input->post("KdRekening2");
		
		$excel      = $this->input->post("excel");
		$print      = $this->input->post("print");
		$userlevel  = $this->input->post("userlevel");
		
		$data['listrekening'] = $this->bukubesar_model->getRekening();
		$data['listtahun'] = $this->bukubesar_model->getListTahun();
		$data['listdivisi'] = $this->bukubesar_model->getListDivisi();
		$data['listbulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$data['userlevel'] = $userlevel;
		$data['divisi'] = $kddivisi;
		$data['rekeningaktif1'] = $kdrekening1;
		$data['rekeningaktif2'] = $kdrekening2;
		
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif1'] = $bulanaktif1;
		$data['bulanaktif2'] = $bulanaktif2;
		$data['tahunaktif'] = $tahunaktif;
		$judul = array();
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Periode = $bulanaktif1 s/d $bulanaktif2 - $tahunaktif";

		$data['tampilkanDT'] = true;

		$data['hasil'] = $this->bukubesar_model->getDetailTrans($tahunaktif,$bulanaktif1,$bulanaktif2,$kddivisi,$kdrekening1, $kdrekening2);
        
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/bukubesar/views', $data);
		}
		else
		{
			$this->load->view("report/bukubesar/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function ajax_rekening(){
		$kdrekening = $this->input->post('KdRekening');
		$listrekening = $this->bukubesar_model->getRekening($kdrekening);
		
		echo "<option value=''> -- Pilih Rekening --</option>";
     	foreach ($listrekening as $var) {
	 		echo "<option value=".$var['KdRekening'].">".$var['NamaRekening']."</option>";
	    }	
	}
}
?>