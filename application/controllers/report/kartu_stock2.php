<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class kartu_stock2 extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/kartu_stock2_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->kartu_stock2_model->getDate();
			$bulanaktif     = $this->session->userdata('bulanaktif');
			$tahunaktif     = $this->session->userdata('tahunaktif');
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['listgudang'] = $this->kartu_stock2_model->getGudang();
			$data['gudang'] = "";
			$data['listbarang'] = $this->kartu_stock2_model->getBarang();
			$data['barang'] = "";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['bulanaktif'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['tahun'] = $this->kartu_stock2_model->getListTahun();
			$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['track'] = $mylib->print_track();
			$this->load->view('report/kartu_stock2/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->kartu_stock2_model->aplikasi();
		$bulanaktif = $this->input->post("bulan");
		$tahunaktif = $this->input->post("tahun");
		$gudang = $this->input->post("gudang");
		$barang = $this->input->post("barang");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['gudang'] = $gudang;
		$data['barang'] = $barang;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif'] = $bulanaktif;
		$data['tahunaktif'] = $tahunaktif;
		$data['tahun'] = $this->mutasi_bulanan_model->getListTahun();
		$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$judul = array();
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Periode = $bulanaktif - $tahunaktif";
		$judul[] = "Gudang  = $gudang";
		
		$data['tampilkanRT'] = true;

		$data['hasil0'] = $this->kartu_stock2_model->getRekapTrans($tahunaktif,$bulanaktif,$gudang,$barang);
		$data['hasil1'] = $this->kartu_stock2_model->getDetailTrans($tahunaktif,$bulanaktif,$gudang,$barang);
		$judul[] = "Barang  = $barang - ".$data['hasil0']->NamaBarang;
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listgudang'] = $this->kartu_stock2_model->getGudang();
			$data['listbarang'] = $this->kartu_stock2_model->getBarang();
			$this->load->view('report/kartu_stock2/views', $data);
		}
		else
		{

			$this->load->view("report/kartu_stock2/reportRT", $data);
			$data['excel'] = "";
		}
	}
}
?>