<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class daily_sales_non_guide extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/daily_sales_non_guide_model');
    }

    function index(){
     	//$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/daily_sales_non_guide/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->daily_sales_non_guide_model->aplikasi();
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$data['kassa'] = $this->daily_sales_non_guide_model->getkassa();
		$data['salesrec'] = $this->daily_sales_non_guide_model->getsales($tgldari,$tglsampai);
		$data['listemail'] = $this->daily_sales_non_guide_model->getlistemail();
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Laporan Penjualan Harian';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/daily_sales_non_guide/views', $data);
		}
		else
		{
			$this->load->view("report/daily_sales_non_guide/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail()
	{
		$mylib = new globallib();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		$data['store']		= $this->daily_sales_non_guide_model->aplikasi();
		$data['excel'] = '';
		$data['print'] = '';
		$data['v_date_from'] = $awalbulan;
		$data['v_date_to'] = date('d-m-Y');
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($awalbulan);
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['tampilkanDT'] = true;
		$data['salesrec'] = $this->daily_sales_non_guide_model->getsales($tgldari,$tglsampai);
		$data['listemail'] = $this->daily_sales_non_guide_model->getlistemail();
		
		$data['kirim_email']='Y';
				
		$data['judul'] = 'Laporan Penjualan Harian';
		
		$this->load->view("report/daily_sales_non_guide/reportRT", $data);
	}
}
?>