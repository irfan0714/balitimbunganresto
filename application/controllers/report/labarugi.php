<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class labarugi extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/labarugi_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->labarugi_model->getDate();
			list($tahunaktif, $bulanaktif, $tglaktif)  = explode('-',$tanggal);
			$data['userlevel'] = $this->session->userdata('userlevel');
			
			$data['listdivisi'] = $this->labarugi_model->getDivisi();
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['bulanaktif'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['tampilkanDT'] = false;
			$data['bulan1'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['tahun1'] = $this->labarugi_model->getListTahun();
			$data['track'] = $mylib->print_track();
			$data['type1'] = '';
			$this->load->view('report/labarugi/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->labarugi_model->aplikasi();
		$bulanaktif = $this->input->post("bulan1");
		$tahunaktif = $this->input->post("tahun1");
		$divisi = $this->input->post("divisi");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$type = $this->input->post("type");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['divisi'] = $divisi;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif'] = $bulanaktif;
		$data['tahunaktif'] = $tahunaktif;
		$data['namabulan'] = array( "Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des" );
		$data['bulan1'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$data['tahun1'] = $this->labarugi_model->getListTahun();
		$judul = array();
		
		$judul[] = $data['store'][0]['NamaPT'];
		$judul[] = "Laporan Laba Rugi";
		$judul[] = "Periode = $bulanaktif - $tahunaktif";
		if($divisi!='' && $divisi!='No'){ 
			$judul[] = 'Divisi = ' . $this->labarugi_model->getNamaDivisi($divisi);
		}
		
		$data['tampilkanDT'] = true;
		if($type=='YTD'){
			$data['data'] = $this->labarugi_model->getReport($tahunaktif,$bulanaktif,$divisi);
		}
		elseif($type=='Divisi'){
			$data['data'] = $this->labarugi_model->getReportByDivisi($tahunaktif,$bulanaktif);
		}else{
			$data['data'] = $this->labarugi_model->getReportTrend($tahunaktif,$bulanaktif,$divisi);
		}
		
		$data['netsales'] = $this->labarugi_model->getNetSales($tahunaktif,$bulanaktif,$divisi);	
		$data['netsales1'] = $this->labarugi_model->getNetSales($tahunaktif,$bulanaktif,1);
		$data['netsales2'] = $this->labarugi_model->getNetSales($tahunaktif,$bulanaktif,2);
		$data['netsales3'] = $this->labarugi_model->getNetSales($tahunaktif,$bulanaktif,3);
		$data['netsales4'] = $this->labarugi_model->getNetSales($tahunaktif,$bulanaktif,5);
		$data['judul'] = $judul;
		$data['type1'] = $type;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listdivisi'] = $this->labarugi_model->getDivisi();
			$this->load->view('report/labarugi/views', $data);
		}
		else
		{
			if($type=='YTD'){
				$this->load->view("report/labarugi/reportRT", $data);
			}elseif($type=='Divisi'){
				$this->load->view("report/labarugi/reportRTDivisi", $data);
			}else{
				$this->load->view("report/labarugi/reportTrend", $data);
			}
			$data['excel'] = "";
		}
	}
}
?>