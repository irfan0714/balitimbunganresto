<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_compliment extends authcontroller {

    function __construct() {
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_compliment_model', 'compliment');
    }

    function index() {
        $mylib = new globallib();
        //$sign = $mylib->getAllowList("all");
        //if ($sign == "Y") 
        //{
            $tanggal = $this->compliment->getDate();
            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['v_pilihan'] = "";
			
			$data['v_start_date'] = '01-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
            
            $data['excel'] = "";
            $data['print'] = "";
            $data['v_nostruk'] = "";
            
            $dataType = "'1','2','3','6'";
            $data['mtype'] = $this->compliment->getType($dataType);
            
            $dataKasir = "'8', '9', '10'";
            $data['mkasir'] = $this->compliment->getKasir($dataKasir);
            
            $data['mkasirfromheader'] = $this->compliment->getKasirFromHeader();
            
            $data['tampilkanDT'] = false;
            $data['tampilkanDR'] = false;
            $data['tampilkanRR'] = false;
            $data['tampilkanRT'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/compliment/views', $data);
        //} 
        //else 
        //{
        //    $this->load->view('denied');
        //}
    }

	function search_report()
	{
		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$v_pilihan = $this->input->post("v_pilihan");
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_divisi = $this->input->post("v_divisi");
        $v_kasir = $this->input->post("v_kasir");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        
        $data['v_pilihan'] = $v_pilihan;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['v_divisi'] = $v_divisi;
        $data['v_kasir'] = $v_kasir;
        $data['excel'] = $excel;
        $data['print'] = $print;
            
        $dataType = "'1','2','3'";
        $data['mtype'] = $this->compliment->getType($dataType);
        
        $dataKasir = "'8', '9', '10'";
        $data['mkasir'] = $this->compliment->getKasir($dataKasir);
        
        $data['mkasirfromheader'] = $this->compliment->getKasirFromHeader();
        
        $data['judul'] = "Reprot Compliment $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
        $data['tampilkanDT'] = true;
        
        $data['hasil'] = $this->compliment->getReport($v_pilihan,$v_start_date,$v_end_date,$v_divisi,$v_kasir);
        
        if ($excel == "") 
        {
            if ($print == "print") 
            {
                $data['fileName'] = 'harian.sss';
            } 
            else 
            {
                $data['track'] = $mylib->print_track();
                
                $this->load->view('report/compliment/views', $data);
            }
        } 
        else 
        {
			if($v_pilihan=="transaksi")
			{
                $this->load->view("report/compliment/tampil", $data);
			}
			elseif($v_pilihan=="detail")
			{
                $this->load->view("report/compliment/tampil_detail", $data);
			}
			elseif($v_pilihan=="barang")
			{
                $this->load->view("report/compliment/tampil_barang", $data);
			}
			
        }
	}
	
	function ajax_detail_voucher($id)
	{
		$mylib = new globallib();
		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$id] = $id;
		
		$where_nostruk = $mylib->where_array($arr_data['list_data'], "transaksi_detail_voucher.NoStruk", "in");
		
		$data = $this->kasir->getVoucher($v_start_date,$v_end_date,$where_nostruk);
		 
		/*$sql = "SELECT * FROM `transaksi_detail_voucher` WHERE NoStruk = '".$id."'";
		$qry = $this->db->query($sql);
        $row = $qry->row();*/
	
		echo json_encode($data); 
		
	}
	
	function pop_up_detail_voucher()
	{
		$mylib = new globallib();
		$nostruk 	= $this->uri->segment(5);
		
		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$nostruk] = $nostruk;
		
		$where_nostruk = $mylib->where_array($arr_data['list_data'], "transaksi_detail_voucher.NoStruk", "in");
		
		$data["detail_voucher"] = $this->kasir->getVoucher($v_start_date,$v_end_date,$where_nostruk);
		$data["nostruk"] = $nostruk;
		
        $this->load->view('pop/pop_up_detail_voucher', $data);
	}
}

?>