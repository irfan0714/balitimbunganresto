<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_cn_dn_piutang extends authcontroller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('report/report_cn_dn_piutang_model');
	}
 

	public function index()
	{  	

		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {

			$today					= date('Y-m-d');
			
			$data['tgldari']		= $today;
			$data['tglsampai']		= $today;
			$data['tampilkanDT'] 	= false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_cn_dn_piutang/view_cn_dn_piutang', $data);	
//		} else {
//			$this->load->view('denied');
//		}
		
	}

	public function tampil() 
	{ 

		$mylib = new globallib();
		
		$submit			= $this->input->post('submit');
		$tgldari		= $this->input->post('tgldari');
		$tglsampai		= $this->input->post('tglsampai');
		$today			= date('Y-m-d');
		$tanggaldari	= strtotime($tgldari);
		$tanggalsampai	= strtotime($tglsampai);


			if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai)) ) {
				$tgldari		= $today;
				$tglsampai		= $today;
				$namadep		= "";
			}
		
		$data['tgldari']			= $tgldari;
		$data['tglsampai']			= $tglsampai;
		$data['submit']				= $submit;
		$data['tanggaldari']		= $tgldari;
		$data['tanggalsampai']		= $tglsampai;
		$data['tampilkanDT'] 		= true;
		$data['viewdata']			= $this->report_cn_dn_piutang_model->viewData($tgldari,$tglsampai);

		 if($submit=='TAMPIL')
		{
			$this->load->view('report/report_cn_dn_piutang/view_cn_dn_piutang', $data);	
		}
		
		else if($submit=='XLS')
		{
			//$this->_printxls($dataresult);
		
				//$data['track'] = $mylib->print_track();
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="report_cn_dn_piutang.xls"');
                
                $this->load->view('report/report_cn_dn_piutang/tampil_cn_dn_piutang', $data);	
		}        
		
		
	}

}

/* End of file absensi.php */
/* Location: ./application/controllers/absensi.php */