<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_komisi extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rpt_komisimodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $tanggal = $this->rpt_komisimodel->getDate();
            $notrans = $this->input->post("notrans");
            $data['listjenis'] = $this->rpt_komisimodel->getJenis();
//$data['listrekening'] = $this->paymentmodel->getRekening();
            $data['idjenis'] = "";
            //$data['kdrekening'] = "";
            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['tgl1'] = '01' . '-' . $bulan . '-' . $tahun;
            $data['tgl2'] = $maxtgl . '-' . $bulan . '-' . $tahun;
            $data['notrans'] = "";
            $data['cekdt'] = "checked='checked'";
            $data['cektr'] = "";
            $data['cektl'] = "";
            $data['cektd'] = "";
            $data['excel'] = "";
            $data['print'] = "";
            $data['tampilkanDT'] = false;
            $data['tampilkanTR'] = false;
            $data['tampilkanTL'] = false;
            $data['tampilkanTD'] = false;
            $data['track'] = $mylib->print_track();
            $this->load->view('report/komisi/views', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function cari() {
        $mylib = new globallib();
        $tgl1 = $this->input->post("tgl1");
        $tgl2 = $this->input->post("tgl2");
        $notrans = $this->input->post("notrans");
        $idjenis = $this->input->post("idjenis");
//        $kdrekening = $this->input->post("kdrekening");
        $opt = $this->input->post("opt");
//        $opt1 = $this->input->post("opt1");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $data['notrans'] = $notrans;
        $data['idjenis'] = $idjenis;
        //   $data['kdrekening'] = $kdrekening;
        $data['tgl1'] = $tgl1;
        $data['tgl2'] = $tgl2;
//        $data['cekrr'] = "";
        $data['cektr'] = "";
        $data['cekdt'] = "";
        $data['cektl'] = "";
        $data['cektd'] = "";
//        $data['cekdr'] = "";
//        $data['cekyy'] = "";
//        $data['cektt'] = "";
        $data['excel'] = $excel;
        $data['print'] = $print;

        if ($opt == "DT") {
            $data['cekdt'] = "checked='checked'";
        }
        if ($opt == "TR") {
            $data['cektr'] = "checked='checked'";
        }
        if ($opt == "TL") {
            $data['cektl'] = "checked='checked'";
        }
        if ($opt == "TD") {
            $data['cektd'] = "checked='checked'";
        }
//        
//        if ($opt1 == "YY") {
//            $data['cekyy'] = "checked='checked'";
//            $orderby = "KdRekening,Tanggal,--NoDokumen";
//        }
//        if ($opt1 == "TT") {
//            $data['cektt'] = "checked='checked'";
//            $orderby = "Tanggal,--NoDokumen";
//        }
        $data['tampilkanDT'] = false;
        $data['tampilkanTR'] = false;
        $data['tampilkanTL'] = false;
        $data['tampilkanTD'] = false;
        
        $judul = array();
        $judul[] = "Tanggal = $tgl1 s/d $tgl2";
        $tgl1 = $mylib->ubah_tanggal($tgl1);
        $tgl2 = $mylib->ubah_tanggal($tgl2);
        if ($opt == "DT"){
            $wheredtl = "a.TglTransaksi >= '$tgl1' AND a.TglTransaksi <= '$tgl2'";
        }else{
            $wheredtl = "TglTransaksi >= '$tgl1' AND TglTransaksi <= '$tgl2'";
        }
//        if (!empty($kdrekening) && $opt <> "RT") {
//            $wheredtl.=" and KdRekening='$kdrekening'";
//            $judul[] = "Rekening = $kdrekening";
//        }
        $where1 = "";

        if (!empty($notrans)) {
            $where2 = "AND a.NoTransaksi = '$notrans'";
            $judul[] = "No Transaksi = $notrans";
        } else {
            $where2 = "";
            $judul[] = "";
        }

        if (!empty($idjenis)) {
            $where1.=" and d.jenis='$idjenis'";
            $jenis = $idjenis == 1 ? "ASING" : "LOCAL";
            $judul[] = "Jenis = $jenis";
        }
        if ($opt == "DT") {
            $sql1 = "SELECT a.TglTransaksi, a.NoTransaksi, b.NoStruk, b.PCode, d.NamaLengkap, b.Qty,b.Harga,b.Persentase as Komisi, IFNULL(ROUND(b.Qty * b.Persentase * b.Harga / 100),0) AS Nilai
                    FROM finance_komisi_header a
                    INNER JOIN finance_komisi_detail b
                    ON a.`NoTransaksi` = b.`NoTransaksi`
                    INNER JOIN masterbarang d
                    ON b.PCode = d.PCode
                    WHERE $wheredtl $where2 ORDER BY a.TglTransaksi, a.NoTransaksi";
            //echo $sql1;
            $data['tampilkanDT'] = true;
        }
        if ($opt == "TR") {
            $sql1 ="SELECT TglTransaksi,NoTransaksi,Total AS TotalKomisi, TotalSales FROM finance_komisi_header
                   WHERE $wheredtl $where2 ORDER BY TglTransaksi DESC, NoTransaksi";
            $data['tampilkanTR'] = true;
        }
        if ($opt == "TL") {
            $sql1 ="SELECT 
                        a.KdAgent, b.Nama, b.Phone, SUM(a.Total) AS TotalKomisi, SUM(a.TotalSales) AS TotalSales 
                      FROM
                        finance_komisi_header a 
                        INNER JOIN tourleader b 
                          ON a.`KdAgent` = b.`KdTourLeader` 
                      WHERE $wheredtl
                      GROUP BY a.`KdAgent` 
                      ORDER BY a.TglTransaksi DESC";
            $data['tampilkanTL'] = true;
        }
        if ($opt == "TD") {
            $sql1 ="SELECT 
                        a.KdAgent, b.Nama, b.Phone, a.Total AS TotalKomisi, a.TotalSales, c.KdRegister, c.NoStiker
                      FROM
                        finance_komisi_header a 
                        INNER JOIN tourleader b 
                          ON a.`KdAgent` = b.`KdTourLeader` 
                        INNER JOIN register c
                          ON b.KdTourLeader = c.KdTourLeader
                      WHERE $wheredtl
                      ORDER BY c.Tanggal DESC";
            $data['tampilkanTD'] = true;
        }
        $data['hasil'] = $this->rpt_komisimodel->getReport($sql1);
        $data['judul'] = $judul;
        if ($excel == "") {
            if ($print == "print") {
                $data['fileName'] = 'harian.sss';
                // $this->load->view('report/ticketing/reportRHprint', $data);
            } else {
                $data['track'] = $mylib->print_track();
                $data['listjenis'] = $this->rpt_komisimodel->getJenis();
//                $data['listrekening'] = $this->paymentmodel->getRekening();
                $this->load->view('report/komisi/views', $data);
            }
        } else {
            if ($opt == "DT") {
                $this->load->view("report/komisi/tampil", $data);
            }
            if ($opt == "TR") {
                $this->load->view("report/komisi/tampil_TR", $data);
            }
            if ($opt == "TL") {
                $this->load->view("report/komisi/tampil_TL", $data);
            }
            if ($opt == "TD") {
                $this->load->view("report/komisi/tampil_TD", $data);
            }
        }
    }

}

?>