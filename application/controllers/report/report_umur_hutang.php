<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_umur_hutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_umur_hutangmodel');
    }

    function index(){
    	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['v_date'] = date('d-m-Y');
			$data['type'] = 'D';
			$data['excel'] = "";
			$data['tampilkan'] = false;
			$data['rekening'] = $this->report_umur_hutangmodel->getrekening();
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_umur_hutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari(){
		$mylib = new globallib();
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$tgl = $this->input->post("v_date");
		$date = $mylib->ubah_tanggal($tgl);
		$excel = $this->input->post("excel");
		$type = $this->input->post("type");
		
		if($type=='D')
			$data['hasil'] = $this->report_umur_hutangmodel->getReport($date);
		else
			$data['hasil'] = $this->report_umur_hutangmodel->getReportRekap($date);
			
		$data['judul'] = "Reprot Umur Hutang Per $tgl";
		$data['v_date'] = $tgl;
		$data['type'] = $type;
		$data['excel'] = $excel;
        
        $data['tampilkan'] = true;
  
		if ($excel == "") 
        {
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_umur_hutang/views', $data);
        } 
        else 
        {
        	if($type=='D')
				$this->load->view('report/report_umur_hutang/reportdt', $data);
			else
				$this->load->view('report/report_umur_hutang/reportrkp', $data);
        }	
	}
		
}
?>