<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
//purwanto on sept 16
class Report_rencana_terima_barang extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rpt_mutasipaketmodel');
        $this->load->model('report/rpt_mutasibarangmodel');
        $this->load->model('report/report_bpermintaan_model');
        $this->load->model('report/rpt_rencana_terima_barang_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {


			$data['listgudang'] = $this->report_bpermintaan_model->getGudang();
			$data['gudang'] = "";
			$data['tgl_cari']="";
			
            $data['excel'] = "";
            $data['print'] = "";
            $data['tampilkandata'] = false;


            $data['track'] = $mylib->print_track();
            $this->load->view('report/persediaan/view_rencana_terima_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    
    function search_report() {
        //print_r($_POST);die();
        $mylib = new globallib();
        
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        
        //$kdtransaksi = $this->input->post("kdtransaksi");
        //$tahun = $this->input->post("tahun");
        //$bulan = $this->input->post("bulan");
        $tgl = $this->input->post("v_start_date");
        $tgl_=$mylib->ubah_tanggal($tgl);
        $gudang = $this->input->post("gudang");
		//$divisi = $this->input->post("divisi");
        
        $data['listgudang'] = $this->report_bpermintaan_model->getGudang();
		$data['gudang'] = $gudang;

        $data['excel'] = $excel;
        $data['print'] = $print;
        
		
        
        $data['hasil_cari'] = $this->rpt_rencana_terima_barang_model->getReport_detail($tgl_, $gudang);
        $data['judul'] = "Report Rencana Terima Barang " .$data['hasil_cari'][0]['Keterangan']." - ".$tgl;
		//print_r($data['hasil_cari']);die;
		
		if ($excel == "") {
            if ($print == "print") {
                $data['fileName'] = 'harian.sss';
            } else {
                $data['track'] = $mylib->print_track();
				$data['tampilkandata'] = true;
				$data['tgl_cari']=$tgl;
                $this->load->view('report/persediaan/view_rencana_terima_barang', $data);
            }
        } else {
        	 $this->load->view('report/persediaan/tampil_rencana_terima_barang', $data);
            
        }    
        
    }
    

}

?>