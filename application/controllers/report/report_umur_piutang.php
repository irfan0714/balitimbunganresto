<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_umur_piutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_umur_piutangmodel');
    }

    function index(){
    	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['v_date'] = date('d-m-Y');
			$data['type'] = 'D';
			$data['excel'] = "";
			$data['tampilkan'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_umur_piutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari(){
		$mylib = new globallib();
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$tgl = $this->input->post("v_date");
		$date = $mylib->ubah_tanggal($tgl);
		$excel = $this->input->post("excel");
		$type = $this->input->post("type");
		
		if($type=='D')
			$data['hasil'] = $this->report_umur_piutangmodel->getReport($date);
		else
			$data['hasil'] = $this->report_umur_piutangmodel->getReportRekap($date);
			
		$data['judul'] = "Reprot Umur Hutang Per $tgl";
		$data['v_date'] = $tgl;
		$data['type'] = $type;
		$data['excel'] = $excel;
        
        $data['tampilkan'] = true;
  
		if ($excel == "") 
        {
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_umur_piutang/views', $data);
        } 
        else 
        {
        	if($type=='D')
				$this->load->view('report/report_umur_piutang/reportdt', $data);
			else
				$this->load->view('report/report_umur_piutang/reportrkp', $data);
        }	
	}
		
}
?>