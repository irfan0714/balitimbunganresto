<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_voucher_employee extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_voucher_employee_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y")
        {
            $tanggal = $this->report_voucher_employee_model->getDate();
            $bulan = date('m');//$this->session->userdata('bulanaktif');
            $tahun = date('Y');//$this->session->userdata('tahunaktif');

        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');

            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;

            $data['v_nostruk'] = "";

            $data['ArrDivisi'] = $this->report_voucher_employee_model->getDivisi();
            $data['ArrEmployee'] = $this->report_voucher_employee_model->getEmployee();

            // echo "<pre>";print_r($data['ArrEmployee']);die();

            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_voucher_employee/views', $data);
        }
        else
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		$mylib = new globallib();

    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');

        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_nostruk = $this->input->post("v_nostruk");
        $v_divisi = $this->input->post("v_divisi");
        $v_nik = $this->input->post("v_nik");

        $data['judul'] = "Reprot Voucher Karyawan $v_start_date s/d $v_end_date";

        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);

        $data['hasil'] = $this->report_voucher_employee_model->getReport($v_start_date,$v_end_date,$v_nostruk,$v_divisi,$v_nik);
        $data['ArrVoucherEmployee'] = $this->report_voucher_employee_model->getVoucherDetailEmployee($v_start_date,$v_end_date,$v_nostruk);
        $data['ArrEmployee'] = $this->report_voucher_employee_model->getEmployee();

  		// echo "<pre>";print_r($data['ArrVoucherEmployee']);echo "</pre>";die;
        $arr_data["list_nostruk"] = array();
	    $this->load->view("report/report_voucher_employee/tampil_detail", $data);
	}

	function export_excel($param){
		$arr_param = explode('#',base64_decode($param));

		$mylib = new globallib();

    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');

        $v_start_date = $arr_param[1];
        $v_end_date = $arr_param[2];
        $v_nostruk = $arr_param[3];
        $v_divisi = $arr_param[4];
        $v_nik = $arr_param[5];


        $data['judul'] = "Reprot Voucher Karyawan $v_start_date s/d $v_end_date";

        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);

        $data['hasil'] = $this->report_voucher_employee_model->getReport($v_start_date,$v_end_date,$v_nostruk,$v_divisi,$v_nik);
        $data['ArrVoucherEmployee'] = $this->report_voucher_employee_model->getVoucherDetailEmployee($v_start_date,$v_end_date,$v_nostruk);
        $data['ArrEmployee'] = $this->report_voucher_employee_model->getEmployee();

  		// echo "<pre>";print_r($data['ArrVoucherEmployee']);echo "</pre>";die;
        $arr_data["list_nostruk"] = array();
         header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Report Voucher Karyawan.xls"');
	    $this->load->view("report/report_voucher_employee/tampil_detail", $data);
	}

	function divisi($v_divisi=''){
		$userlevel = $this->session->userdata('userlevel');
		$divisi = $this->report_voucher_employee_model->getDivisi();

     	if($userlevel==-1 || $userlevel==13)
     		echo "<option value=''>All</option>";
     	for($i=0;$i<count($divisi);$i++) {
     		$selected="";
     		if($v_divisi==$divisi[$i]['KdDivisi'])
			{
				$selected = "selected='selected'";
			}
			echo "<option $selected value=" .$divisi[$i]['KdDivisi'].">".$divisi[$i]['NamaDivisi']."</option>";
      	}
	}

	function subdivisi($v_divisi=''){
		$userlevel = $this->session->userdata('userlevel');
		$divisi = $this->report_voucher_employee_model->getType();

     	if($userlevel==-1 || $userlevel==13)
     		echo "<option value=''>All</option>";
     	for($i=0;$i<count($divisi);$i++) {
     		$selected="";
     		if($v_divisi==$divisi[$i]['KdDivisi'])
			{
				$selected = "selected='selected'";
			}
	 		echo "<option $selected value=" .$divisi[$i]['KdDivisi'].">".$divisi[$i]['NamaDivisi']."</option>";
      	}
	}

	function ajax_detail_voucher($id)
	{
		$mylib = new globallib();
		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$id] = $id;

		$where_nostruk = $mylib->where_array($arr_data['list_data'], "transaksi_detail_voucher.NoStruk", "in");

		$data = $this->report_voucher_employee_model->getVoucher($v_start_date,$v_end_date,$where_nostruk);

		/*$sql = "SELECT * FROM `transaksi_detail_voucher` WHERE NoStruk = '".$id."'";
		$qry = $this->db->query($sql);
        $row = $qry->row();*/

		echo json_encode($data);

	}

	function pop_up_detail_voucher()
	{
		$mylib = new globallib();
		$nostruk 	= $this->uri->segment(5);

		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$nostruk] = $nostruk;

		$where_nostruk = $mylib->where_array($arr_data['list_data'], "v.NoStruk", "in");

		$data["detail_voucher"] = $this->report_voucher_employee_model->getVoucher($v_start_date,$v_end_date,$where_nostruk);
		$data["nostruk"] = $nostruk;

        $this->load->view('pop/pop_up_detail_voucher', $data);
	}
}

?>
