<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_reservasi extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_reservasi_model', 'reservasi');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y")
        {
            $tanggal = $this->reservasi->getDate();
            $bulan = date('m');//$this->session->userdata('bulanaktif');
            $tahun = date('Y');//$this->session->userdata('tahunaktif');

        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');

            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['v_pilihan'] = "";

			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;

            $data['excel'] = "";
            $data['print'] = "";
            $data['v_nostruk'] = "";
            $data['v_tipe_cari']='struk';

            //$dataType = "'1','2','3'";
            $data['mtype'] = $this->reservasi->getDivisi();

            $dataKasir = "'8', '9', '10'";
            $data['mkasir'] = $this->reservasi->getKasir($dataKasir);

            $data['mkasirfromheader'] = $this->reservasi->getKasirFromHeader();

            $data['tampilkanDT'] = false;
            $data['tampilkanDR'] = false;
            $data['tampilkanRR'] = false;
            $data['tampilkanRT'] = false;

            $data['track'] = $mylib->print_track();
            $this->load->view('report/penjualan/reservasi/views', $data);
        }
        else
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;

		$mylib = new globallib();

    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');

		$v_pilihan = $this->input->post("v_pilihan");
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_nobeo = $this->input->post("v_nobeo");
        $v_nosticker = $this->input->post("v_nosticker");
        $v_username = $this->input->post("v_username");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");

        $data['v_pilihan'] = $v_pilihan;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['v_nobeo'] = $v_nobeo;
        $data['v_nosticker'] = $v_nosticker;
        $data['v_username'] = $v_username;
        $data['excel'] = $excel;
        $data['print'] = $print;

        $data['judul'] = "Reprot Penjualan Reservasi $v_start_date s/d $v_end_date";

        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);

        //$data['tampilkanDT'] = true;

        $data['hasil'] = $this->reservasi->getReport($v_pilihan,$v_start_date,$v_end_date,$v_nobeo,$v_nosticker,$v_username);
  		//echo "<pre>";print_r($data['hasil']);echo "</pre>";die;
        $arr_data["list_noreservasi"] = array();


        if(is_array($data['hasil']))
        {
	        foreach($data['hasil'] as $val)
	        {
	        	$arr_data["list_noreservasi"][$val["NoDokumen"]]=$val["NoDokumen"];
			}
		}

		if(is_array($arr_data['list_noreservasi']))
        {
        	$where_noreservasi = $mylib->where_array($arr_data['list_noreservasi'], "r.`NoDokumen`", "in");
        }

        //$data['hasil_voucher'] = $this->reservasi->getVoucher($v_start_date,$v_end_date,$where_noreservasi);
        
		if ($excel == "")
        {
            if ($print == "print")
            {
                $data['fileName'] = 'harian.sss';
            }
            else
            {
                $data['track'] = $mylib->print_track();
                $this->load->view('report/penjualan/reservasi/views', $data);
            }
        }
        else
        {
			if($v_pilihan=="transaksi"){
				
				$this->load->view("report/penjualan/reservasi/tampil", $data);
			}
			elseif($v_pilihan=="detail"){
                $this->load->view("report/penjualan/reservasi/tampil_detail", $data);
			}
			elseif($v_pilihan=="barang"){
                $this->load->view("report/penjualan/reservasi/tampil_barang", $data);
			}
        }
	}

	function divisi($v_divisi=''){
		$userlevel = $this->session->userdata('userlevel');
		$divisi = $this->reservasi->getDivisi();

     	if($userlevel==-1 || $userlevel==13)
     		echo "<option value=''>All</option>";
     	for($i=0;$i<count($divisi);$i++) {
     		$selected="";
     		if($v_divisi==$divisi[$i]['KdDivisi'])
			{
				$selected = "selected='selected'";
			}
			echo "<option $selected value=" .$divisi[$i]['KdDivisi'].">".$divisi[$i]['NamaDivisi']."</option>";
      	}
	}

	function subdivisi($v_divisi=''){
		$userlevel = $this->session->userdata('userlevel');
		$divisi = $this->reservasi->getType();

     	if($userlevel==-1 || $userlevel==13)
     		echo "<option value=''>All</option>";
     	for($i=0;$i<count($divisi);$i++) {
     		$selected="";
     		if($v_divisi==$divisi[$i]['KdDivisi'])
			{
				$selected = "selected='selected'";
			}
	 		echo "<option $selected value=" .$divisi[$i]['KdDivisi'].">".$divisi[$i]['NamaDivisi']."</option>";
      	}
	}

	function ajax_detail_voucher($id)
	{
		$mylib = new globallib();
		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$id] = $id;

		$where_nostruk = $mylib->where_array($arr_data['list_data'], "transaksi_detail_voucher.NoStruk", "in");

		$data = $this->reservasi->getVoucher($v_start_date,$v_end_date,$where_nostruk);

		/*$sql = "SELECT * FROM `transaksi_detail_voucher` WHERE NoStruk = '".$id."'";
		$qry = $this->db->query($sql);
        $row = $qry->row();*/

		echo json_encode($data);

	}

	function pop_up_detail_voucher()
	{
		$mylib = new globallib();
		$nostruk 	= $this->uri->segment(5);

		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$nostruk] = $nostruk;

		$where_nostruk = $mylib->where_array($arr_data['list_data'], "v.NoStruk", "in");

		$data["detail_voucher"] = $this->reservasi->getVoucher($v_start_date,$v_end_date,$where_nostruk);
		$data["nostruk"] = $nostruk;

        $this->load->view('pop/pop_up_detail_voucher', $data);
	}
}

?>
