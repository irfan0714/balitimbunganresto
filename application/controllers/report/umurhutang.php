<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class umurhutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/umurhutangmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['listsupplier'] = $this->umurhutangmodel->getSupplier();
			$data['listjenis'] = Array ( Array ( 'Jenis' => 'F', 'NamaJenis'=>'Faktur' ), 
                                         Array ( 'Jenis' => 'R', 'NamaJenis'=>'Retur' ),
										 Array ( 'Jenis' => 'D', 'NamaJenis'=>'Nota Debet' ), 
                                         Array ( 'Jenis' => 'K', 'NamaJenis'=>'Nota Kredit'));
			$data['kdsupplier'] = "";
			$data['jenis'] = "F";
			$data['tgl1a'] = '00-00-0000';
			$data['tgl2a'] = '00-00-0000';
			$data['tgl1b'] = '00-00-0000';
			$data['tgl2b'] = '00-00-0000';
			$data['tglcetak'] = date("d-m-Y");
			$data['tglcetak1'] = date("d-m-Y");
			$data['cekdt'] = "checked='checked'";
			$data['cekrk'] = "";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkan'] = false;
			$data['opt'] = "";
			$data['track'] = $mylib->print_track();
			$this->load->view('report/umurhutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$tgl1a = $this->input->post("tgl1a");
		$tgl2a = $this->input->post("tgl2a");
		$tgl1b = $this->input->post("tgl1b");
		$tgl2b = $this->input->post("tgl2b");
		$tglcetak = $this->input->post("tglcetak");
		$tglcetak1 = $mylib->ubah_tanggal($tglcetak);
		$kdsupplier = $this->input->post("kdsupplier");
		$jenis = $this->input->post("jenis");
		$opt = $this->input->post("opt");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$data['kdsupplier'] = $kdsupplier;
		$data['jenis'] = $jenis;
		$data['tgl1a'] = $tgl1a;
		$data['tgl2a'] = $tgl2a;
		$data['tgl1b'] = $tgl1b;
		$data['tgl2b'] = $tgl2b;
		$data['tglcetak'] = $tglcetak;
		$data['cekdt'] = "";
		$data['cekrk'] = "";
		$data['ceksp'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['opt'] = $opt;
		if($opt=="DT"){
			$data['cekdt'] = "checked='checked'";
		}
		if($opt=="RK"){
			$data['cekrk'] = "checked='checked'";
		}
		$data['tampilkan'] = true;
		$judul = array();
		$tgl1a = $mylib->ubah_tanggal($tgl1a);
		$tgl2a = $mylib->ubah_tanggal($tgl2a);
		$tgl1b = $mylib->ubah_tanggal($tgl1b);
		$tgl2b = $mylib->ubah_tanggal($tgl2b);
		
		$where1 = "TotalHutang<>TotalBayar";
		if($tgl1a!='0000-00-00'){ $where1.=" and TglTransaksi between '$tgl1a' and '$tgl2a'"; $judul[] = "Tanggal Terima = '$tgl1a' s/d '$tgl2a'"; }
		if($tgl1b!='0000-00-00'){ $where1.=" and TglJto between '$tgl1b' and '$tgl2b'"; $judul[] = "Tanggal JTO = '$tgl1b' s/d '$tgl2b'"; }
		if(!empty($kdsupplier)){ $where1.=" and a.KdSupplier='$kdsupplier'"; $judul[] = "Customer = $kdsupplier"; }
		if(!empty($jenis)){ $where1.=" and Jenis='$jenis'"; $judul[] = "Jenis = $jenis"; }
		$judul[] = "Tanggal Cetak = $tglcetak";   
		
		if($opt=="DT")
	    $sql1 = "Select a.*,b.Nama,datediff('$tglcetak1',tgljto) as umur from hutang a,supplier b where a.KdSupplier=b.KdSupplier and ".$where1;
	    else
		$sql1 = "SELECT a.KdSupplier,b.Nama,
				SUM(TotalHutang) AS TotalHutang,
				SUM(TotalBayar) AS TotalBayar,
				SUM(IF((DATEDIFF('$tglcetak1',tgljto)<=0),TotalHutang-TotalBayar,0)) AS SaatIni,
				SUM(IF((DATEDIFF('$tglcetak1',tgljto)>=1) AND (DATEDIFF('$tglcetak1',tgljto)<=30),TotalHutang-TotalBayar,0)) AS Umur0130,
				SUM(IF((DATEDIFF('$tglcetak1',tgljto)>=31) AND (DATEDIFF('$tglcetak1',tgljto)<=60),TotalHutang-TotalBayar,0)) AS Umur3160,
				SUM(IF((DATEDIFF('$tglcetak1',tgljto)>=61) AND (DATEDIFF('$tglcetak1',tgljto)<=90),TotalHutang-TotalBayar,0)) AS Umur6190,
				SUM(IF((DATEDIFF('$tglcetak1',tgljto)>=91),TotalHutang-TotalBayar,0)) AS Umur91
				FROM hutang a,supplier b 
				WHERE a.KdSupplier=b.KdSupplier AND ".$where1." GROUP BY a.KdSupplier";
		
		//echo $sql1;
		
		$data['hasil'] = $this->umurhutangmodel->getReport($sql1);
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listsupplier'] = $this->umurhutangmodel->getSupplier();
			$data['listjenis'] = Array ( Array ( 'Jenis' => 'F', 'NamaJenis'=>'Faktur' ), 
                                         Array ( 'Jenis' => 'R', 'NamaJenis'=>'Retur' ),
										 Array ( 'Jenis' => 'D', 'NamaJenis'=>'Nota Debet' ), 
                                         Array ( 'Jenis' => 'K', 'NamaJenis'=>'Nota Kredit'));
			$this->load->view('report/umurhutang/views', $data);
		}
		else
		{
		    if($opt=="DT")
			    $this->load->view('report/umurhutang/reporthutang', $data);
		    else
			    $this->load->view('report/umurhutang/reporthutangrekap', $data);
			
		}
	}
}
?>