<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
//purwanto on sept 16
class Report_mutasibarang extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rpt_mutasibarangmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {


			$data['listgudang'] = $this->rpt_mutasibarangmodel->getGudang();
			$data['listdivisi'] = $this->rpt_mutasibarangmodel->getDivisi();
			$data['gudang'] = "";
			$data['divisi'] = "";
			
			$data['excel'] = "";
            $data['print'] = "";
            $data['tampilkandata'] = false;
            list($tahun, $bulan, $tgl) = explode('-',DATE('Y-m-d'));
            $data['tahun'] = $tahun;
            $data['bulan'] = $bulan;
            $data['search_by']="";
            
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/persediaan/view_mutasi_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    
    function search_report() {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $search_by = $this->input->post("search_by");
        
        $kdtransaksi = $this->input->post("kdtransaksi");
        $tahun = $this->input->post("tahun");
        $bulan = $this->input->post("bulan");
        
        $gudang = $this->input->post("gudang");
		$divisi = $this->input->post("divisi");
        
        $data['listgudang'] = $this->rpt_mutasibarangmodel->getGudang();
        $data['listdivisi'] = $this->rpt_mutasibarangmodel->getDivisi();
		$data['gudang'] = $gudang;
		$data['divisi'] = $divisi;
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$data['search_by'] = $search_by;
		

        $data['excel'] = $excel;
        $data['print'] = $print;
        $tabel_field = 'GAWAl'.$bulan;
        $tabel_field_val = 'GNAWAl'.$bulan;
        
		$data['judul'] = "Report Mutasi Barang Bulan " .$this->input->post("bulan"). " Tahun " .$this->input->post("tahun");
        //$data['hasil_cari'] = $this->rpt_mutasibarangmodel->getReport_detail($bulan, $tahun, $gudang,$divisi, $tabel_field, $search_by);
		$hasil_cari = $this->rpt_mutasibarangmodel->getReport_detail($bulan, $tahun, $gudang,$divisi, $tabel_field, $tabel_field_val, $search_by);
		
		foreach($hasil_cari AS $key){
				
				$arr_data["Kategori_PCode"][$key["KdKategori"]][$key["PCode"]] = $key["PCode"];
				$arr_data["NamaLengkap"][$key["PCode"]] = $key["NamaLengkap"];
				$arr_data["satuan"][$key["PCode"]] = $key["SatuanSt"];            
				$arr_data["list_KdKategori"][$key["KdKategori"]] = $key["KdKategori"];
				$arr_data["NamaKategori"][$key["KdKategori"]] = $key["NamaKategori"];
				$arr_data["list_pcode"][$key["PCode"]] = $key["PCode"];
				
				$arr_data["list_saldo_awal"][$key["PCode"]] = $key["saldo_awal"];
				$arr_data["list_jml_R"][$key["PCode"]] = $key["jml_R"];
				$arr_data["list_jml_RG"][$key["PCode"]] = $key["jml_RG"];
				$arr_data["list_jml_MM_PL"][$key["PCode"]] = $key["jml_MM_PL"];
				$arr_data["list_jml_MC_DL"][$key["PCode"]] = $key["jml_MC_DL"];
				$arr_data["list_jml_SO"][$key["PCode"]] = $key["jml_SO"];
				$arr_data["list_jml_MX"][$key["PCode"]] = $key["jml_MX"];
				$arr_data["list_jml_MP_IN"][$key["PCode"]] = $key["jml_MP_IN"];
				$arr_data["list_jml_MP_OUT"][$key["PCode"]] = $key["jml_MP_OUT"];
				$arr_data["list_jml_FG"][$key["PCode"]] = $key["jml_FG"];
				$arr_data["list_jml_RB"][$key["PCode"]] = $key["jml_RB"];
				$arr_data["list_jml_SR"][$key["PCode"]] = $key["jml_SR"];
				$arr_data["list_jml_ADJ"][$key["PCode"]] = $key["jml_ADJ"];
				
				$arr_data["list_saldo_awal_val"][$key["PCode"]] = $key["saldo_awal_val"];
				$arr_data["list_jml_R_val"][$key["PCode"]] = $key["jml_R_val"];
				$arr_data["list_jml_RG_val"][$key["PCode"]] = $key["jml_RG_val"];
				$arr_data["list_jml_MM_PL_val"][$key["PCode"]] = $key["jml_MM_PL_val"];
				$arr_data["list_jml_MC_DL_val"][$key["PCode"]] = $key["jml_MC_DL_val"];
				$arr_data["list_jml_SO_val"][$key["PCode"]] = $key["jml_SO_val"];
				$arr_data["list_jml_MX_val"][$key["PCode"]] = $key["jml_MX_val"];
				$arr_data["list_jml_MP_IN_val"][$key["PCode"]] = $key["jml_MP_IN_val"];
				$arr_data["list_jml_MP_OUT_val"][$key["PCode"]] = $key["jml_MP_OUT_val"];
				$arr_data["list_jml_FG_val"][$key["PCode"]] = $key["jml_FG_val"];
				$arr_data["list_jml_RB_val"][$key["PCode"]] = $key["jml_RB_val"];
				$arr_data["list_jml_SR_val"][$key["PCode"]] = $key["jml_SR_val"];
				$arr_data["list_jml_ADJ_val"][$key["PCode"]] = $key["jml_ADJ_val"];
			}
			
			$data['Kategori_PCode'] = $arr_data["Kategori_PCode"];
			$data['NamaLengkaps'] = $arr_data["NamaLengkap"];
			$data['satuans'] = $arr_data["satuan"];
			$data['list_KdKategori']=$arr_data["list_KdKategori"];
			$data['NamaKategoris'] = $arr_data["NamaKategori"];
		    $data['list_saldo_awal'] = $arr_data["list_saldo_awal"];
		    $data['list_jml_R'] =$arr_data["list_jml_R"];
		    $data['list_jml_RG'] =$arr_data["list_jml_RG"];
		    $data['list_jml_MM_PL'] =$arr_data["list_jml_MM_PL"];
		    $data['list_jml_MC_DL'] =$arr_data["list_jml_MC_DL"];
		    $data['list_jml_SO'] =$arr_data["list_jml_SO"];
		    $data['list_jml_MX'] =$arr_data["list_jml_MX"];
		    $data['list_jml_MP_IN'] =$arr_data["list_jml_MP_IN"];
		    $data['list_jml_MP_OUT'] =$arr_data["list_jml_MP_OUT"];
		    $data['list_jml_FG'] =$arr_data["list_jml_FG"];
		    $data['list_jml_RB'] =$arr_data["list_jml_RB"];
			$data['list_jml_SR'] =$arr_data["list_jml_SR"];
		    $data['list_jml_ADJ'] =$arr_data["list_jml_ADJ"];
		    
		    $data['list_saldo_awal_val'] = $arr_data["list_saldo_awal_val"];
		    $data['list_jml_R_val'] =$arr_data["list_jml_R_val"];
		    $data['list_jml_RG_val'] =$arr_data["list_jml_RG_val"];
		    $data['list_jml_MM_PL_val'] =$arr_data["list_jml_MM_PL_val"];
		    $data['list_jml_MC_DL_val'] =$arr_data["list_jml_MC_DL_val"];
		    $data['list_jml_SO_val'] =$arr_data["list_jml_SO_val"];
		    $data['list_jml_MX_val'] =$arr_data["list_jml_MX_val"];
		    $data['list_jml_MP_IN_val'] =$arr_data["list_jml_MP_IN_val"];
		    $data['list_jml_MP_OUT_val'] =$arr_data["list_jml_MP_OUT_val"];
		    $data['list_jml_FG_val'] =$arr_data["list_jml_FG_val"];
		    $data['list_jml_RB_val'] =$arr_data["list_jml_RB_val"];
			$data['list_jml_SR_val'] =$arr_data["list_jml_SR_val"];
		    $data['list_jml_ADJ_val'] =$arr_data["list_jml_ADJ_val"];
		    $data['userlevel'] = $this->session->userdata('userlevel');
		    
		if ($excel == "") {
            if ($print == "print") {
                $data['fileName'] = 'harian.sss';
            } else {
                $data['track'] = $mylib->print_track();
				$data['tampilkandata'] = true;
                $this->load->view('report/persediaan/view_mutasi_barang', $data);
            }
        } else {
        	$userlevel = $this->session->userdata('userlevel');
        	if($userlevel==-1 || $userlevel==1){
				$this->load->view('report/persediaan/tampil_mutasi_barang_val', $data);	
			}else{
				$this->load->view('report/persediaan/tampil_mutasi_barang', $data);	
			}
        	
            
        }    
        
    }
    

}

?>