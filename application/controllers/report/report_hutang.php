<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_hutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_hutangmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['v_date'] = date('d-m-Y');
			$data['excel'] = "";
			$data['tampilkan'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_hutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$tgl = $this->input->post("v_date");
		$date = $mylib->ubah_tanggal($tgl);
		$excel = $this->input->post("excel");
		
		$data['hasil'] = $this->report_hutangmodel->getReport($date);
		$data['judul'] = "Reprot Hutang Per $tgl";
		$data['v_date'] = $tgl;
		$data['excel'] = $excel;
        
        $data['tampilkan'] = true;
  
		if ($excel == "") 
        {
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_hutang/views', $data);
        } 
        else 
        {
			$this->load->view('report/report_hutang/reporthutang', $data);
        }
	}
}
?>