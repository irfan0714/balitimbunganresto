<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_tourleader extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rpt_tourleadermodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $tanggal = $this->rpt_tourleadermodel->getDate();
            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['dtpicker'] = $this->input->post("dtpicker");
            $data['alt_date'] = $this->input->post("alt_date");
          //  $data['v_month'] = date('MM yy');
            
            $data['excel'] = "";
            $data['print'] = "";

            $data['tampilkanDT'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/analisa/view_omzet_tourleader', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search_report() {
       // print_r($_POST);die();
        $mylib = new globallib();
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $data['dtpicker'] = $this->input->post("dtpicker");
        $data['alt_date'] = $this->input->post("alt_date");
        $tglnya = $this->input->post("alt_date");
        $bulan = substr($tglnya,0,2);
        $tahun = substr($tglnya,3,4);//die();
        $data['excel'] = $excel;
        $data['print'] = $print;

        $data['judul'] = "Report Omzet Tour Leader ".$this->input->post("dtpicker");

        $data['tampilkanDT'] = true;

        $data['hasil'] = $this->rpt_tourleadermodel->getReport($bulan, $tahun);

        
        if ($excel == "") {
            if ($print == "print") {
                $data['fileName'] = 'harian.sss';
            } else {
                $data['track'] = $mylib->print_track();

                $this->load->view('report/analisa/view_omzet_tourleader', $data);
            }
        } else {
            $this->load->view("report/analisa/tampil_omzet_tourleader", $data);
        }
    }

}

?>