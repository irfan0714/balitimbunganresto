<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class jurnal extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/jurnalmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$tanggal = $this->jurnalmodel->getDate();
			$data['listrekening'] = $this->jurnalmodel->getRekening();
			$data['listjenis'] = Array ( Array ( 'Jenis' => '1', 'NamaJenis' => 'Umum' ), 
                                           Array ( 'Jenis' => '2', 'NamaJenis' => 'Adjustment' ));
			$data['kdrekening'] = "";
			$data['jenis'] = "";
			$bulan = $this->session->userdata('bulanaktif');
			$tahun = $this->session->userdata('tahunaktif');
			$date = $tahun."-".$bulan."-1";
            $maxtgl = date("t", strtotime($date));
			
			$data['tgl1'] = '01'.'-'.$bulan.'-'.$tahun;
			$data['tgl2'] = $maxtgl.'-'.$bulan.'-'.$tahun;
			$data['cekrh'] = "checked='checked'";
			$data['cekrt'] = "";
			$data['cekdt'] = "";
			$data['cekyy'] = "";
			$data['cektt'] = "checked='checked'";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRH'] = false;
			$data['tampilkanRT'] = false;
			$data['tampilkanDT'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/jurnal/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$tgl1 = $this->input->post("tgl1");
		$tgl2 = $this->input->post("tgl2");
		$kdrekening = $this->input->post("kdrekening");
		$jenis = $this->input->post("jenis");
		$opt = $this->input->post("opt");
		$opt1 = $this->input->post("opt1");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$data['kdrekening'] = $kdrekening;
		$data['jenis'] = $jenis;
		$data['tgl1'] = $tgl1;
		$data['tgl2'] = $tgl2;
		$data['cekrh'] = "";
		$data['cekrt'] = "";
		$data['cekdt'] = "";
		$data['cekyy'] = "";
		$data['cektt'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		if($opt=="RH"){
			$data['cekrh'] = "checked='checked'";
		}
		if($opt=="RT"){
			$data['cekrt'] = "checked='checked'";
		}
		if($opt=="DT"){
			$data['cekdt'] = "checked='checked'";
		}
		if($opt1=="YY"){
			$data['cekyy'] = "checked='checked'";
			$orderby = "KdRekening,Tanggal,--NoDokumen";
		}
		if($opt1=="TT"){
			$data['cektt'] = "checked='checked'";
			$orderby = "Tanggal,--NoDokumen";
		}
		$data['tampilkanRH'] = false;
		$data['tampilkanRT'] = false;
		$data['tampilkanDT'] = false;
		$judul = array();
		$judul[] = "Tanggal = $tgl1 s/d $tgl2";
		$tgl1 = $mylib->ubah_tanggal($tgl1);
		$tgl2 = $mylib->ubah_tanggal($tgl2);
		$wheredtl = "TglDokumen between '$tgl1' and '$tgl2' and (STATUS <> 'B' OR STATUS IS NULL)";
		if(!empty($kdrekening)&&$opt<>"RT"){ $wheredtl.=" and KdRekening='$kdrekening'"; $judul[] = "Rekening = $kdrekening"; }
		$where1 = "h.Jenis=h.Jenis";
        if(!empty($jenis)){ $where1.=" and h.Jenis='$jenis'"; $judul[] = "Jenis = $jenis"; }
		if($opt=="RH")
		{
			$sql1 = "select KdRekening,NamaRekening,sum(Debit) as Debit,sum(Kredit) as Kredit,Jenis from
					(SELECT d.KdRekening,r.NamaRekening,d.Debit,d.Kredit,d.TglDokumen as Tanggal,h.Jenis
					FROM(SELECT * FROM trans_jurnal_detail WHERE ".$wheredtl.")d
					LEFT JOIN
					(SELECT NoDokumen,Jenis FROM trans_jurnal_header)h
					ON h.NoDokumen=d.NoDokumen
					LEFT JOIN
					(SELECT KdRekening,NamaRekening FROM rekening)r
					ON r.KdRekening=d.KdRekening
					where ".$where1.")u
					group by KdRekening order by KdRekening";
			$data['tampilkanRH'] = true;
		}
		
		
		if($opt=="RT")
		{
			$sql1 = "select NoDokumen,Tanggal,Debit,Kredit,NoTransaksi,Keterangan,Jenis from
					(SELECT h.NoDokumen,h.TglDokumen as Tanggal,
					h.JumlahDebit as Debit,h.JumlahKredit as Kredit,h.Keterangan,h.NoTransaksi,h.Jenis
					FROM(SELECT * FROM trans_jurnal_header WHERE ".$wheredtl.")h
					where ".$where1.")u
					order by --NoDokumen";
			$data['tampilkanRT'] = true;
		}
		if($opt=="DT")
		{
			$sql1 = "select NoDokumen,Tanggal,NoTransaksi,KetHeader,
					KdRekening,NamaRekening,Debit,Kredit,Keterangan,Jenis from
					(SELECT h.NoDokumen,d.KdRekening,r.NamaRekening,d.Debit,d.Kredit,d.TglDokumen as Tanggal,
					h.NoTransaksi,h.KetHeader,d.Keterangan,h.Jenis
					FROM(SELECT * FROM trans_jurnal_detail WHERE ".$wheredtl.")d
					LEFT JOIN
					(SELECT NoDokumen,Keterangan as KetHeader,NoTransaksi,Jenis FROM trans_jurnal_header)h
					ON h.NoDokumen=d.NoDokumen
					LEFT JOIN
					(SELECT KdRekening,NamaRekening FROM rekening)r
					ON r.KdRekening=d.KdRekening
					where ".$where1.")u
					order by ".$orderby;
			$data['tampilkanDT'] = true;
		}
		$data['hasil'] = $this->jurnalmodel->getReport($sql1);
		$data['judul'] = $judul;
		if($excel=="")
		{
		    if($print=="print")
			{
			   $data['fileName'] = 'harian.sss';
			   $this->load->view('report/jurnal/reportRHprint', $data);
			}
			else
			{
			$data['track'] = $mylib->print_track();
			$data['listrekening'] = $this->jurnalmodel->getRekening();
			$data['listjenis'] = Array ( Array ( 'Jenis' => '1', 'NamaJenis' => 'Umum' ), 
                                           Array ( 'Jenis' => '2', 'NamaJenis' => 'Adjustment' ));
			$this->load->view('report/jurnal/views', $data);
			}
		}
		else
		{
			if($opt=="RH")
			{
				$this->load->view('report/jurnal/reportRH', $data);
			}
			if($opt=="RT")
			{
				$this->load->view("report/jurnal/reportRT", $data);
			}
			if($opt=="DT")
			{
				$this->load->view("report/jurnal/reportDT", $data);
			}
		}
	}
}
?>