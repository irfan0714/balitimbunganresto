<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class payment extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/paymentmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$tanggal = $this->paymentmodel->getDate();
			$data['listkasbank'] = $this->paymentmodel->getKasBank();
			$data['listrekening'] = $this->paymentmodel->getRekening();
			$data['kdkasbank'] = "";
			$data['kdrekening'] = "";
			$bulan = $this->session->userdata('bulanaktif');
			$tahun = $this->session->userdata('tahunaktif');
			$date = $tahun."-".$bulan."-1";
            $maxtgl = date("t", strtotime($date));
			
			$data['tgl1'] = '01'.'-'.$bulan.'-'.$tahun;
			$data['tgl2'] = $maxtgl.'-'.$bulan.'-'.$tahun;
			$data['cekrh'] = "checked='checked'";
			$data['cekrt'] = "";
			$data['cekdt'] = "";
			$data['cekyy'] = "";
			$data['cektt'] = "checked='checked'";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRH'] = false;
			$data['tampilkanRT'] = false;
			$data['tampilkanDT'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/payment/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$tgl1 = $this->input->post("tgl1");
		$tgl2 = $this->input->post("tgl2");
		$kdkasbank = $this->input->post("kdkasbank");
		$kdrekening = $this->input->post("kdrekening");
		$opt = $this->input->post("opt");
		$opt1 = $this->input->post("opt1");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$data['kdkasbank'] = $kdkasbank;
		$data['kdrekening'] = $kdrekening;
		$data['tgl1'] = $tgl1;
		$data['tgl2'] = $tgl2;
		$data['cekrh'] = "";
		$data['cekrt'] = "";
		$data['cekdt'] = "";
		$data['cekyy'] = "";
		$data['cektt'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		if($opt=="RH"){
			$data['cekrh'] = "checked='checked'";
		}
		if($opt=="RT"){
			$data['cekrt'] = "checked='checked'";
		}
		if($opt=="DT"){
			$data['cekdt'] = "checked='checked'";
		}
		if($opt1=="YY"){
			$data['cekyy'] = "checked='checked'";
			$orderby = "KdRekening,Tanggal,--NoDokumen";
		}
		if($opt1=="TT"){
			$data['cektt'] = "checked='checked'";
			$orderby = "Tanggal,--NoDokumen";
		}
		$data['tampilkanRH'] = false;
		$data['tampilkanRT'] = false;
		$data['tampilkanDT'] = false;
		$judul = array();
		$judul[] = "Tanggal = $tgl1 s/d $tgl2";
		$tgl1 = $mylib->ubah_tanggal($tgl1);
		$tgl2 = $mylib->ubah_tanggal($tgl2);
		$wheredtl = "TglDokumen between '$tgl1' and '$tgl2' and status<>'B'";
		if(!empty($kdrekening)&&$opt<>"RT"){ $wheredtl.=" and KdRekening='$kdrekening'"; $judul[] = "Rekening = $kdrekening"; }
		$where1 = "h.KdKasBank=h.KdKasBank";
		if(!empty($kdkasbank)){ $where1.=" and h.KdKasBank='$kdkasbank'"; $judul[] = "Kas Bank = $kdkasbank"; }

		if($opt=="RH")
		{
			$sql1 = "select KdKasBank,NamaKasBank,KdRekening,NamaRekening,sum(Jumlah) as Jumlah from
					(SELECT d.KdRekening,r.NamaRekening,d.Jumlah,d.TglDokumen as Tanggal,h.KdKasBank,k.NamaKasBank
					FROM(SELECT * FROM trans_payment_detail WHERE ".$wheredtl.")d
					LEFT JOIN
					(SELECT NoDokumen,KdKasBank FROM trans_payment_header)h
					ON h.NoDokumen=d.NoDokumen
					LEFT JOIN
					(SELECT KdKasBank,NamaKasBank FROM kasbank)k
					ON k.KdKasBank=h.KdKasBank
					LEFT JOIN
					(SELECT KdRekening,NamaRekening FROM rekening)r
					ON r.KdRekening=d.KdRekening
					where ".$where1.")u
					group by KdKasBank,KdRekening order by KdKasBank,KdRekening";
			$data['tampilkanRH'] = true;
		}
		
		
		if($opt=="RT")
		{
			$sql1 = "select NoDokumen,Tanggal,KdKasBank,NamaKasBank,Jumlah,NoBukti,Keterangan from
					(SELECT h.NoDokumen,h.TglDokumen as Tanggal,h.KdKasBank,k.NamaKasBank,
					h.JumlahPayment as Jumlah,h.Keterangan,h.NoBukti
					FROM(SELECT * FROM trans_payment_header WHERE ".$wheredtl.")h
					LEFT JOIN
					(SELECT KdKasBank,NamaKasBank FROM kasbank)k
					ON k.KdKasBank=h.KdKasBank
					where ".$where1.")u
					order by NoDokumen";
			$data['tampilkanRT'] = true;
		}
		if($opt=="DT")
		{
			$sql1 = "select NoDokumen,Tanggal,KdKasBank,NamaKasBank,NoBukti,KetHeader,
					KdRekening,NamaRekening,Jumlah,Keterangan,
					NamaSubDivisi
					from
					(SELECT h.NoDokumen,d.KdRekening,r.NamaRekening,d.Jumlah,d.TglDokumen as Tanggal,
					h.KdKasBank,k.NamaKasBank,h.NoBukti,h.KetHeader,d.Keterangan,d.KdSubDivisi,SubDivisi.NamaSubDivisi
					FROM(SELECT * FROM trans_payment_detail WHERE ".$wheredtl.")d
					LEFT JOIN subdivisi ON subdivisi.KdSubDivisi=d.KdSubDivisi
					LEFT JOIN
					(SELECT NoDokumen,KdKasBank,Keterangan as KetHeader,NoBukti FROM trans_payment_header)h
					ON h.NoDokumen=d.NoDokumen
					LEFT JOIN
					(SELECT KdKasBank,NamaKasBank FROM kasbank)k
					ON k.KdKasBank=h.KdKasBank
					LEFT JOIN
					(SELECT KdRekening,NamaRekening FROM rekening)r
					ON r.KdRekening=d.KdRekening
					where ".$where1.")u
					order by ".$orderby;
			$data['tampilkanDT'] = true;
		}
		$data['hasil'] = $this->paymentmodel->getReport($sql1);
		$data['judul'] = $judul;
		if($excel=="")
		{
		    if($print=="print")
			{
			   $data['fileName'] = 'harian.sss';
			   $this->load->view('report/payment/reportRHprint', $data);
			}
			else
			{
			$data['track'] = $mylib->print_track();
			$data['listkasbank'] = $this->paymentmodel->getKasBank();
			$data['listrekening'] = $this->paymentmodel->getRekening();
			$this->load->view('report/payment/views', $data);
			}
		}
		else
		{
			if($opt=="RH")
			{
				$this->load->view('report/payment/reportRH', $data);
			}
			if($opt=="RT")
			{
				$this->load->view("report/payment/reportRT", $data);
			}
			if($opt=="DT")
			{
				$this->load->view("report/payment/reportDT", $data);
			}
		}
	}
}
?>