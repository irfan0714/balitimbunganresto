<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_sales_persediaan extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_sales_persediaan_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			//$data['track'] = '';//$mylib->print_track();
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_sales_persediaan/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$mylib = new globallib();
		$tglsystem		= $this->report_sales_persediaan_model->aplikasi();
		
		list($tahun, $bulan, $vtgl) = explode('-',$tglsystem->TglTrans);
		
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['store']		= $this->report_sales_persediaan_model->aplikasi();
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
				
		$data['tampilkanDT'] = true;
	
		$data['hasil'] = $this->report_sales_persediaan_model->getReport($tgldari,$tglsampai);
		$data['stock'] =  $this->report_sales_persediaan_model->getStockAwalBulan($bulan,$tahun);
		$data['mutasi'] =  $this->report_sales_persediaan_model->getHitungMutasi($tgldari);
		$data['listemail'] = $this->report_sales_persediaan_model->getlistemail();
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Report Sales dan Persediaan';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_sales_persediaan/views', $data);
		}
		else
		{
			$this->load->view("report/report_sales_persediaan/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail()
	{
		$mylib = new globallib();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		$data['store']		= $this->report_sales_persediaan_model->aplikasi();
		$data['excel'] = '';
		$data['print'] = '';
		$data['v_date_from'] = $awalbulan;
		$data['v_date_to'] = date('d-m-Y');
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($awalbulan);
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['tampilkanDT'] = true;

		$data['hasil'] = $this->report_sales_persediaan_model->getReport($tgldari,$tglsampai);
		$data['stock'] =  $this->report_sales_persediaan_model->getStockAwalBulan($bulan,$tahun);
		$data['mutasi'] =  $this->report_sales_persediaan_model->getHitungMutasi($tgldari);
		$data['listemail'] = $this->report_sales_persediaan_model->getlistemail();
		
		$data['kirim_email']='Y';
				
		$data['judul'] = 'Report Sales dan Persediaan';
		
		$this->load->view("report/report_sales_persediaan/reportRT", $data);
	}
}
?>