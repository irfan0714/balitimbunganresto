<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_kasir extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_kasir_model', 'kasir');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y")
        {
            $tanggal = $this->kasir->getDate();
            $bulan = date('m');//$this->session->userdata('bulanaktif');
            $tahun = date('Y');//$this->session->userdata('tahunaktif');

        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');

            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['v_pilihan'] = "";

			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;

            $data['excel'] = "";
            $data['print'] = "";
            $data['v_nostruk'] = "";
            $data['v_tipe_cari']='struk';

            //$dataType = "'1','2','3'";
            $data['mtype'] = $this->kasir->getDivisi();

            $dataKasir = "'8', '9', '10'";
            $data['mkasir'] = $this->kasir->getKasir($dataKasir);

            $data['mkasirfromheader'] = $this->kasir->getKasirFromHeader();

            $data['tampilkanDT'] = false;
            $data['tampilkanDR'] = false;
            $data['tampilkanRR'] = false;
            $data['tampilkanRT'] = false;

            $data['track'] = $mylib->print_track();
            $this->load->view('report/penjualan/kasir/views', $data);
        }
        else
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		$nostiker = 'T';
		if($this->input->post("nostiker")){
			$nostiker = 'Y';
		}
		//echo "<pre>";print_r($_POST);echo "</pre>";die;

		$mylib = new globallib();

    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');

		$v_pilihan = $this->input->post("v_pilihan");
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_nostruk = $this->input->post("v_nostruk");
        $v_sticker = $this->input->post("v_sticker");
        $v_divisi = $this->input->post("v_divisi");
        $v_tipe_cari = $this->input->post("v_tipe_cari");
        $v_username = $this->input->post("v_username");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");

        $data['v_pilihan'] = $v_pilihan;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['v_nostruk'] = $v_nostruk;
        $data['v_divisi'] = $v_divisi;
        $data['v_tipe_cari'] = $v_tipe_cari;
        $data['v_username'] = $v_username;
        $data['excel'] = $excel;
        $data['print'] = $print;
		$data['nostiker']	= $nostiker;

        $otorisasi = $this->kasir->cekOtorisasi('report_kasir_diskon', $this->session->userdata('username'));
        if(!empty($otorisasi)){
            $data['otorisasi_diskon_view']="Y";
        }else{
            $data['otorisasi_diskon_view']="T";
        } 


        $dataType = "'1','2','3'";

        $data['mtype'] = $this->kasir->getDivisi();
        $dataKasir = "'8', '9', '10'";
        $data['mkasir'] = $this->kasir->getKasir($dataKasir);

        $data['mkasirfromheader'] = $this->kasir->getKasirFromHeader();

        $data['judul'] = "Reprot Penjualan kasir $v_start_date s/d $v_end_date";

        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);

        $data['tampilkanDT'] = true;

        $data['hasil'] = $this->kasir->getReport($v_pilihan,$v_tipe_cari,$v_start_date,$v_end_date,$v_nostruk,$v_divisi,$v_username,$nostiker,$v_sticker);
  		//echo "<pre>";print_r($data['hasil']);echo "</pre>";die;
        $arr_data["list_nostruk"] = array();


        if(is_array($data['hasil']))
        {
	        foreach($data['hasil'] as $val)
	        {
	        	$arr_data["list_nostruk"][$val["NoStruk"]]=$val["NoStruk"];
			}
		}

		if(is_array($arr_data['list_nostruk']))
        {
        	$where_nostruk = $mylib->where_array($arr_data['list_nostruk'], "v.NoStruk", "in");
        }

        $data['hasil_voucher'] = $this->kasir->getVoucher($v_start_date,$v_end_date,$where_nostruk);

		if ($excel == "")
        {
            if ($print == "print")
            {
                $data['fileName'] = 'harian.sss';
            }
            else
            {
                $data['track'] = $mylib->print_track();
                $this->load->view('report/penjualan/kasir/views', $data);
            }
        }
        else
        {
			if($v_pilihan=="transaksi"){
				if($nostiker =='Y'){
	                $this->load->view("report/penjualan/kasir/tampilAgen", $data);
				}else{
					$this->load->view("report/penjualan/kasir/tampil", $data);
				}
			}
			elseif($v_pilihan=="detail"){
                $this->load->view("report/penjualan/kasir/tampil_detail", $data);
			}
			elseif($v_pilihan=="barang"){
                $this->load->view("report/penjualan/kasir/tampil_barang", $data);
			}
			elseif($v_pilihan=="dpp"){
                $this->load->view("report/penjualan/kasir/tampil_dpp", $data);
			}
			elseif($v_pilihan=="tanggal"){
                $this->load->view("report/penjualan/kasir/tampil_tanggal", $data);
			}
      elseif($v_pilihan=="sticker"){
        if($nostiker =='Y'){
                  $this->load->view("report/penjualan/kasir/tampilAgen", $data);
        }else{
          $this->load->view("report/penjualan/kasir/tampil", $data);
        }
			}
        }
	}

	function divisi($v_divisi=''){
		$userlevel = $this->session->userdata('userlevel');
		$divisi = $this->kasir->getDivisi();

     	if($userlevel==-1 || $userlevel==13)
     		echo "<option value=''>All</option>";
     	for($i=0;$i<count($divisi);$i++) {
     		$selected="";
     		if($v_divisi==$divisi[$i]['KdDivisi'])
			{
				$selected = "selected='selected'";
			}
			echo "<option $selected value=" .$divisi[$i]['KdDivisi'].">".$divisi[$i]['NamaDivisi']."</option>";
      	}
	}

	function subdivisi($v_divisi=''){
		$userlevel = $this->session->userdata('userlevel');
		$divisi = $this->kasir->getType();

     	if($userlevel==-1 || $userlevel==13)
     		echo "<option value=''>All</option>";
     	for($i=0;$i<count($divisi);$i++) {
     		$selected="";
     		if($v_divisi==$divisi[$i]['KdDivisi'])
			{
				$selected = "selected='selected'";
			}
	 		echo "<option $selected value=" .$divisi[$i]['KdDivisi'].">".$divisi[$i]['NamaDivisi']."</option>";
      	}
	}

	function ajax_detail_voucher($id)
	{
		$mylib = new globallib();
		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$id] = $id;

		$where_nostruk = $mylib->where_array($arr_data['list_data'], "transaksi_detail_voucher.NoStruk", "in");

		$data = $this->kasir->getVoucher($v_start_date,$v_end_date,$where_nostruk);

		/*$sql = "SELECT * FROM `transaksi_detail_voucher` WHERE NoStruk = '".$id."'";
		$qry = $this->db->query($sql);
        $row = $qry->row();*/

		echo json_encode($data);

	}

	function pop_up_detail_voucher()
	{
		$mylib = new globallib();
		$nostruk 	= $this->uri->segment(5);

		$v_start_date="";
		$v_end_date="";
		$arr_data["list_data"][$nostruk] = $nostruk;

		$where_nostruk = $mylib->where_array($arr_data['list_data'], "v.NoStruk", "in");

		$data["detail_voucher"] = $this->kasir->getVoucher($v_start_date,$v_end_date,$where_nostruk);
		$data["nostruk"] = $nostruk;

        $this->load->view('pop/pop_up_detail_voucher', $data);
	}
}

?>
