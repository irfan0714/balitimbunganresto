<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_mutasi_hutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_mutasi_hutang_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($sign);die;
    	if($sign=="Y")
		{
			$tanggal = $this->report_mutasi_hutang_model->getDate();
			$bulanaktif     = date('m');
			$tahunaktif     = date('Y');
			$kdrekeningaktif='';
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['excel'] = "Excel";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['bulanaktif'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['kdrekeningaktif'] = $kdrekeningaktif;
			$data['tahun'] = array( "2016","2017","2018","2019","2020" );
			$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['kdrekening'] = $this->report_mutasi_hutang_model->geRekeningHutang();
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_mutasi_hutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		
		$mylib = new globallib();
		//print_r($_POST);
		$data['store']		= $this->report_mutasi_hutang_model->aplikasi();
		$bulanaktif = $this->input->post("bulan");
		$tahunaktif = $this->input->post("tahun");
		$kdrekeningaktif = $this->input->post("kdrekening");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif'] = $bulanaktif;
		$data['tahunaktif'] = $tahunaktif;
		$data['kdrekeningaktif'] = $kdrekeningaktif;
		$data['tahun'] = array( "2016","2017","2018","2019","2020" );
		$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$data['kdrekening'] = $this->report_mutasi_hutang_model->geRekeningHutang();
		$judul = array();
		$judul[] = "Laporan Mutasi " . $this->report_mutasi_hutang_model->getNamaRekening($kdrekeningaktif);
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Periode = $bulanaktif - $tahunaktif";
		
		$data['tampilkanRT'] = true;

		$data['hasil0'] = $this->report_mutasi_hutang_model->getdata($tahunaktif,$bulanaktif,$kdrekeningaktif);
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_mutasi_hutang/views', $data);
		}else{
			$this->load->view("report/report_mutasi_hutang/reportRT", $data);
			$data['excel'] = "";
		}
	}
}
?>