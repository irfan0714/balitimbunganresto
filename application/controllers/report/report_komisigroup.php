<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_komisigroup extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_komisigroup_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->report_komisigroup_model->getDate();
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['listgroup'] = $this->report_komisigroup_model->getKomisiGroup();
			$data['group'] = "";
			$data['nosticker'] = "";
			$data['tgl1'] = $mylib->ubah_tanggal(substr($tanggal->TglTrans,0,8)."01"); //$mylib->ubah_tanggal($tanggal->TglTrans);
			$data['tgl2'] = $mylib->ubah_tanggal($tanggal->TglTrans);
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_komisigroup/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']	= $this->report_komisigroup_model->aplikasi();
		$tgl1 = $this->input->post("tgl1");
		$group = $this->input->post("group");
		$nosticker = $this->input->post("nosticker");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['group'] = $group;
		$data['nosticker'] = $nosticker;
		$data['tgl1'] = $tgl1;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['jenis'] = "";
		$judul = array();
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Tanggal = $tgl1";
		$judul[] = "Sticker = $nosticker";
		$judul[] = "Group   = $group";
		
		$tgl1 = $mylib->ubah_tanggal($tgl1);
		
		$data['hasil'] = $this->report_komisigroup_model->getKomisi($tgl1,$nosticker,$group);
		$data['tampilkanDT'] = true;
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listgroup'] = $this->report_komisigroup_model->getKomisiGroup();
			$this->load->view('report/report_komisigroup/views', $data);
		}
		else
		{
			$this->load->view("report/report_komisigroup/reportDT", $data);
			$data['excel'] = "";
		}
	}
}
?>