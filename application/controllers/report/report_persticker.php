<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_PerSticker extends authcontroller {

    function __construct() {
        parent::__construct();
		error_reporting(0);        
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_persticker_model', 'report_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        //$sign = 'Y';
        if ($sign == "Y") 
        {            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
            $data['v_pilihan'] = "";
            $data['excel'] = "";
            $data['print'] = "";
            $data['v_date'] = '';
            $data['v_nostiker'] = '';
            
            $data['tampilkanDT'] = false;
            $data['tampilkanDD'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/penjualan/sticker/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		
		$v_nostiker = $this->input->post("v_nostiker");
		$v_date = $this->input->post("v_date");
		$v_pilihan = $this->input->post("v_pilihan");
		$excel = $this->input->post("excel");
        $print = $this->input->post("print");
        
		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
		
        $data['v_pilihan'] = $v_pilihan;
		$data['v_date'] = $v_date;
        $data['v_nostiker'] = $v_nostiker;
        $data['excel'] = $excel;
        $data['print'] = $print;
        $x_date = $mylib->ubah_tanggal($v_date);
        
        $hasil = $this->report_model->getReport($v_pilihan,$x_date,$v_nostiker);
        if(count($hasil>0)){
			$travel = $hasil[0]['Nama'];
		}
		else{
			$travel = '';
		}
        $data['hasil'] = $hasil;
 
        $data['judul'] = "Reprot Penjualan $travel No Sticker $v_nostiker  Tanggal $v_date";
        
        $data['tampilkanDT'] = true;
  
        
  
		if ($excel == "") 
        {
            if ($print == "print") 
            {
                $data['fileName'] = 'harian.sss';
            } 
            else 
            {
                $data['track'] = $mylib->print_track();
                $this->load->view('report/penjualan/sticker/views', $data);
            }
        } 
        else 
        {
			if($v_pilihan=="transaksi"){
				$this->load->view("report/penjualan/sticker/tampil", $data);
			}
			elseif($v_pilihan=="barang"){
                $this->load->view("report/penjualan/sticker/tampil_barang", $data);
			}
        }
	}
}

?>