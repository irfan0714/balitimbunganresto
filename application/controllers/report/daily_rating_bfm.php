<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class daily_rating_bfm extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/daily_rating_bfm_model','daily_sales_model');
    }

    function index(){
     	//$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/daily_rating_bfm/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->daily_sales_model->aplikasi();
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$data['makanan'] = $this->daily_sales_model->getMakanan($tgldari,$tglsampai, '');
		$data['minuman'] = $this->daily_sales_model->getMinuman($tgldari,$tglsampai, '');
		
		
		$data['listemail'] = $this->daily_sales_model->getlistemail();
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Laporan Rating Harian';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/daily_rating_bfm/views', $data);
		}
		else
		{
			$this->load->view("report/daily_rating_bfm/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail()
	{
		$mylib = new globallib();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		$data['store']		= $this->daily_sales_model->aplikasi();
		$data['excel'] = '';
		$data['print'] = '';
		$data['jenis'] = 'A';
		$data['v_date_from'] = $awalbulan;
		$data['v_date_to'] = date('d-m-Y');
		$judul = array();
		$jenis = 'A';
		
		$tgldari = $mylib->ubah_tanggal($awalbulan);
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['tampilkanDT'] = true;
		$data['makanan'] = $this->daily_sales_model->getMakanan($tgldari,$tglsampai, '');
		$data['minuman'] = $this->daily_sales_model->getMinuman($tgldari,$tglsampai, '');
		$data['listemail'] = $this->daily_sales_model->getlistemail();
		
		$data['kirim_email']='Y';
				
		$data['judul'] = 'Laporan Rating Harian';
		
		$this->load->view("report/daily_rating_bfm/reportRT", $data);
	}
	
}
?>