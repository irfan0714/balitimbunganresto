<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_konfirmasi_kedatangan extends authcontroller {

    function __construct() {
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('globalmodel');
        $this->load->model('report/report_konfirmasi_kedatangan_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	unset($arr_data);
        	
            $bulan = date('m');
            $tahun = date('Y');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            $data['mail'] = false;
        	$data['status_konfirmasi'] = 0;
        	$data['status_sort']=0;
			$user = $this->session->userdata('username');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
         
           
            $data['v_tourtravel'] = $this->report_konfirmasi_kedatangan_model->getTourtravel();
        	
        	$data["bulan"] =date('m');
        	$data["tahun"] =date('Y');
        	
        	$data['btn_excel'] = "";
        	$data['flag'] = "";
            
            $data['analisa'] = FALSE;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_konfirmasi_kedatangan/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		//print_r($_POST);die;

		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
       
        $bulan		= $this->input->post('bulan');
		$tahun		= $this->input->post('tahun');
		$travel		= $this->input->post('v_travel');
		$status		= $this->input->post('v_status');
		$sort		= $this->input->post('v_sort');

		//tanggal_awal dan akhir
		$tgldari = $tahun."-".$bulan."-01";
		$jmlhari = $this->report_konfirmasi_kedatangan_model->jmlhari($tgldari);
		$tglsampai = $jmlhari[0]['tgl']; 
		
        
        $data['flag'] = $this->input->post("flag");
        $data['base_url'] = $this->input->post("base_url");
        $data['btn_excel'] = $this->input->post("btn_excel");
        //$excel_btn = $this->input->post("btn_excel");
        
        $data['v_tourtravel'] = $this->report_konfirmasi_kedatangan_model->getTourtravel();
        
        $data['judul'] = "Report Konfirmasi Kedatangan Bulan".$bulan." s.d ".$tahun."";
        $data['mail'] = false;
        
        /*if($excel_btn=="Excel"){
		$data['btn_excel'] = true;	
		}*/
        
        
       $data['header'] = $this->report_konfirmasi_kedatangan_model->getArrayHeader(
			$tgldari,
			$tglsampai,
			$travel,
			$status,
			$sort
		);
				
		$user = $this->session->userdata('username');
		
		$data['bulan']=$bulan;
		$data['tahun']=$tahun;
		$data['travel']=$travel;
		$data['status_konfirmasi']=$status;
		$data['status_sort']=$sort;
		
        $data['analisa'] = TRUE;
			
		$data['track'] = $mylib->print_track();
  
		$this->load->view('report/report_konfirmasi_kedatangan/views', $data);
        
	}
	
	/*function mail(){
		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
       
        $bulan		= $this->input->post('bulan');
		$tahun		= $this->input->post('tahun');
		$travel		= $this->input->post('v_travel');
		$status		= $this->input->post('v_status');
		$sort		= $this->input->post('v_sort');

		//tanggal_awal dan akhir
		$tgldari = $tahun."-".$bulan."-01";
		$jmlhari = $this->report_konfirmasi_kedatangan_model->jmlhari($tgldari);
		$tglsampai = $jmlhari[0]['tgl']; 
		
        
        $data['flag'] = '';
        $data['base_url'] = '';
        $data['btn_excel'] = false;
        $data['mail'] = true;
        $data['listemail'] = $this->report_konfirmasi_kedatangan_model->getlistemail();
        
        $data['v_tourtravel'] = '';
        
        $data['judul'] = "Report Konfirmasi Kedatangan ";
        
       	$data['header'] = $this->report_konfirmasi_kedatangan_model->getmaildata();
				
		$user = $this->session->userdata('username');
		
		$data['bulan']=$bulan;
		$data['tahun']=$tahun;
		$data['travel']='';
		$data['status_konfirmasi']='';
		$data['status_sort']='';
		
        $data['analisa'] = TRUE;
			
		$data['track'] = $mylib->print_track();
  
		$this->load->view('report/report_konfirmasi_kedatangan/views', $data);
	}*/
}

?>