<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_mutasi_antargudang extends authcontroller {

    function __construct() {
        parent::__construct();
		error_reporting(0);        
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_mutasi_antargudang_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        //$sign = 'Y';
        if ($sign == "Y") 
        {   
        	$username = $this->session->userdata('username');
        	$userlevel = $this->session->userdata('userlevel');         
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
                        
            $data['excel'] = "";
            $data['v_date1'] = '';
            $data['v_date2'] = '';
            $data['gudangasal'] = '';
            $data['gudang'] = $this->report_mutasi_antargudang_model->listgudang($username, $userlevel);
            
            $data['tampilkan'] = false;
            $data['track'] = $mylib->print_track();
            $this->load->view('report/mutasi_antargudang/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		$mylib = new globallib();
		
		$username = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');  
        
		$v_date1 = $this->input->post("v_date1");
		$v_date2 = $this->input->post("v_date2");
		$gudangasal = $this->input->post("gudangasal");
		$jenis = $this->input->post("jenis");
		$excel = $this->input->post("excel");
        
		$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
		$data['gudang'] = $this->report_mutasi_antargudang_model->listgudang($username,$userlevel);
		$data['gudangasal'] = $gudangasal;
        $data['v_date1'] = $v_date1;
        $data['v_date2'] = $v_date2;
        $data['excel'] = $excel;
        $data['jenis'] = $jenis;
        
        $date1 = $mylib->ubah_tanggal($v_date1);
        $date2 = $mylib->ubah_tanggal($v_date2);
        
        $hasil = $this->report_mutasi_antargudang_model->getReport($date1,$date2,$gudangasal, $jenis);
        $data['hasil'] = $hasil;
 
        $data['judul'] = "Reprot Mutasi Antar Gudang Tanggal $v_date1 s/d $v_date2";
        
        $data['tampilkan'] = true;
  
		if ($excel == "") 
        {
            $data['track'] = $mylib->print_track();
            $this->load->view('report/mutasi_antargudang/views', $data);
        } 
        else 
        {
			$this->load->view("report/mutasi_antargudang/tampil", $data);
        }
	}
}

?>