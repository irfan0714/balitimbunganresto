<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_lain_lain extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_lainlain_model', 'lainlain');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            //$tanggal = $this->lainlain->getDate();
            $bulan = date('m');//$this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_start_date'] = date('d') . '-' . date('m') . '-' . date('Y');
            $data['v_end_date'] = date('d') . '-' . date('m') . '-' . date('Y');
            
            $data["mtype"] = array("Penerimaan","Pengeluaran");
            $data['gudang'] = $this->lainlain->getgudang($data["username"]);
            $data['status']='';
            
            $data['excel'] = "";
            $data['print'] = "";
            $data['v_nostruk'] = "";
            
            $data['analisa'] = FALSE;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_lainlain/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		//print_r($_POST);die;

		$mylib = new globallib();
		
		$data["mtype"] = array("Penerimaan","Pengeluaran");
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$v_type = $this->input->post("v_type");
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $flag = $this->input->post("flag");
        $base_url = $this->input->post("base_url");
        $btn_excel = $this->input->post("btn_excel");
        $gudang = $this->input->post("gudang");
        $status = $this->input->post("status");
        
        $data['v_type'] = $v_type;
        $data['v_gudang'] = $gudang;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['flag'] = $flag;
        $data['btn_excel'] = $btn_excel;
        $data['gudang'] = $this->lainlain->getgudang($data["username"]);
        $data['status'] = $status;
        
        $data['judul'] = "Reprot ".$v_type." Lain-Lain $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
		$data['header'] = $this->lainlain->getHeader($v_type,$v_start_date,$v_end_date, $gudang, $status);
		
        $arr_data["list_nodok"] = array();

        if(is_array($data['header']))
        {
	        foreach($data['header'] as $val)
	        {
	        	$arr_data["list_nodok"][$val["NoDokumen"]]=$val["NoDokumen"];
			}
		}
		
		$data['detail'] = "";
		$where_nodok = "";		
		if(count($arr_data["list_nodok"])*1>0)
		{
			$where_nodok = $mylib->where_array($arr_data['list_nodok'], "NoDokumen", "in");	
			
			$data['detail'] = $this->lainlain->getDetail($v_type,$where_nodok);
		}
		
        $data['analisa'] = TRUE;
			
		$data['track'] = $mylib->print_track();
  
		if ($flag == "analisa") 
        {
            $this->load->view('report/report_lainlain/views', $data);
        } 
        else if ($flag == "excel") 
        {
        	$this->load->view('report/report_lainlain/views', $data);
        }
	}
}

?>