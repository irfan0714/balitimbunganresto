<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_pemakaian_bahan extends authcontroller {

    function __construct() {
        parent::__construct();
		error_reporting(0);        
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_pemakaian_bahan_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        //$sign = 'Y';
        if ($sign == "Y") 
        {            
        	$username = $this->session->userdata('username');
        	
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
                        
            $data['excel'] = "";
            $data['v_date1'] = '';
            $data['v_date2'] = '';
            $data['listgudang'] = $this->report_pemakaian_bahan_model->listgudang($username);
            $data['gudang'] = '';
            
            $data['tampilkan'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/pemakaian_bahan/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		
		$v_date1 = $this->input->post("v_date1");
		$v_date2 = $this->input->post("v_date2");
		$gudang = $this->input->post("gudang");
		$excel = $this->input->post("excel");
        
		$mylib = new globallib();
		$username = $this->session->userdata('username');
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
		
        $data['v_date1'] = $v_date1;
        $data['v_date2'] = $v_date2;
        $data['gudang'] = $gudang;
        $data['listgudang'] = $this->report_pemakaian_bahan_model->listgudang($username);
        
        $data['excel'] = $excel;
        $date1 = $mylib->ubah_tanggal($v_date1);
        $date2 = $mylib->ubah_tanggal($v_date2);
        
        $hasil = $this->report_pemakaian_bahan_model->getReport($date1,$date2,$gudang);
        $data['hasil'] = $hasil;
 
        $data['judul'] = "Reprot Pemakaian Bahan Tanggal $v_date1 s/d $v_date2";
        
        $data['tampilkan'] = true;
  
		if ($excel == "") 
        {
            $data['track'] = $mylib->print_track();
            $this->load->view('report/pemakaian_bahan/views', $data);
        } 
        else 
        {
			$this->load->view("report/pemakaian_bahan/tampil", $data);
        }
	}
}

?>