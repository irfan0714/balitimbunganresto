<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_hutang_supplier extends authcontroller {

    function __construct(){
        parent::__construct();
		$this->load->library('globallib');
		$this->load->model('report/report_hutang_supplier_model');
    }
	
	function index()
	{
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {		
 

		$submit			= $this->input->post('submit');
		
		
		$data['submit']			= $submit;

		$data['tampilkanDT'] 	= false;
		$data['viewdata']		= $this->report_hutang_supplier_model->viewData();
		$this->load->view('report/report_hutang_supplier/view',$data); 

//		} else {
//			$this->load->view('denied');
//		}

		/*if($submit=='TAMPIL')
		{

			$data['viewdata']		= $this->report_hutang_supplier_model->viewData();

			$this->load->view('report/report_hutang_supplier/view',$data);
		}

		*/
		
	}

	function view()
	{

		$submit			= $this->input->post('submit');

		$data['submit']			= $submit;
		$data['tampilkanDT'] 	= TRUE;
		$data['viewdata']		= $this->report_hutang_supplier_model->viewData();

		if($submit=='TAMPIL')
		{
			$this->load->view("report/report_hutang_supplier/view", $data);
		}
		else if($submit=='XLS')
			{ 
				//$this->_printxls($dataresult);
			
					//$data['track'] = $mylib->print_track();
					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: attachment; filename="report_hutang_supplier.xls"');
	                
	               $this->load->view('report/report_hutang_supplier/tampil',$data);
			} 
		
	}
	

	
	function testing()
	{
		$asdf	=  $this->report_hutang_supplier_model->viewData();
		echo "<pre>";
		print_r($asdf);
		echo "</pre>";
	}
	
	
}