<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_Voucher extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/m_voucher');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            $tanggal = $this->m_voucher->getDate();
            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['v_start_date'] = '01' . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
            
            $data['excel'] = "";
            $data['print'] = "";
            
            $data['tampilkanDT'] = false;
            $data['tampilkanDR'] = false;
            $data['tampilkanRR'] = false;
            $data['tampilkanRT'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/r_voucher/views', $data);
        } else {
            $this->load->view('denied');
        }
    }

	// febri
	function search_report()
	{
		$mylib = new globallib();
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_jenis = $this->input->post("v_jenis");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        
        $data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['v_jenis'] = $v_jenis;
        $data['excel'] = $excel;
        $data['print'] = $print;
        
        $data['judul'] = "Reprot Voucher $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
        $data['tampilkanDT'] = true;
        
       	$data['listjenis'] = $this->m_voucher->getJenis();
        $data['hasil'] = $this->m_voucher->getReport($v_start_date,$v_end_date);
        
        $data['jenis_bayar'] =$this->m_voucher->getTicketHead($v_start_date,$v_end_date);
          
        if ($excel == "") {
            if ($print == "print") 
            {
                $data['fileName'] = 'harian.npm';
            } 
            else 
            {
                $data['track'] = $mylib->print_track();
                
                $this->load->view('report/r_voucher/views', $data);
            }
        } 
        else 
        {
                $this->load->view("report/r_voucher/tampil", $data);
        }
	}

}

?>