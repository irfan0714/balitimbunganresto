<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class giro extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/giromodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['listkasbank'] = $this->giromodel->getKasBank();
			$data['listjenis'] = Array ( Array ( 'Jenis' => 'P', 'NamaJenis'=>'Hutang Giro' ), 
                                         Array ( 'Jenis' => 'R', 'NamaJenis'=>'Piutang Giro' ));
			$data['kdkasbank'] = "";
			$data['jenis'] = "";
			$data['tgl1a'] = '00-00-0000';
			$data['tgl2a'] = '00-00-0000';
			$data['tgl1b'] = '00-00-0000';
			$data['tgl2b'] = '00-00-0000';
			$data['tgl1c'] = '00-00-0000';
			$data['tgl2c'] = '00-00-0000';
			$data['cekbc'] = "checked='checked'";
			$data['ceksc'] = "";
			$data['cekjt'] = "";
			$data['ceksg'] = "";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkan'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/giro/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$tgl1a = $this->input->post("tgl1a");
		$tgl2a = $this->input->post("tgl2a");
		$tgl1b = $this->input->post("tgl1b");
		$tgl2b = $this->input->post("tgl2b");
		$tgl1c = $this->input->post("tgl1c");
		$tgl2c = $this->input->post("tgl2c");
		$kdkasbank = $this->input->post("kdkasbank");
		$jenis = $this->input->post("jenis");
		$opt = $this->input->post("opt");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$data['kdkasbank'] = $kdkasbank;
		$data['jenis'] = $jenis;
		$data['tgl1a'] = $tgl1a;
		$data['tgl2a'] = $tgl2a;
		$data['tgl1b'] = $tgl1b;
		$data['tgl2b'] = $tgl2b;
		$data['tgl1c'] = $tgl1c;
		$data['tgl2c'] = $tgl2c;
		$data['cekbc'] = "";
		$data['ceksc'] = "";
		$data['cekjt'] = "";
		$data['ceksg'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		if($opt=="BC"){
			$data['cekbc'] = "checked='checked'";
		}
		if($opt=="SC"){
			$data['ceksc'] = "checked='checked'";
		}
		if($opt=="JT"){
			$data['cekjt'] = "checked='checked'";
		}
		if($opt=="SG"){
			$data['ceksg'] = "checked='checked'";
		}
		$data['tampilkan'] = true;
		$judul = array();
		$tgl1a = $mylib->ubah_tanggal($tgl1a);
		$tgl2a = $mylib->ubah_tanggal($tgl2a);
		$tgl1b = $mylib->ubah_tanggal($tgl1b);
		$tgl2b = $mylib->ubah_tanggal($tgl2b);
		$tgl1c = $mylib->ubah_tanggal($tgl1c);
		$tgl2c = $mylib->ubah_tanggal($tgl2c);
		
		$wheredtl = "TglDokumen between '$tgl1a' and '$tgl2a'";
		$where1 = "Jenis=Jenis";
		if($tgl1a!='0000-00-00'){ $where1.=" and TglTerima between '$tgl1a' and '$tgl2a'"; $judul[] = "Tanggal Terima = '$tgl1a' s/d '$tgl2a'"; }
		if($tgl1b!='0000-00-00'){ $where1.=" and TglJTo between '$tgl1b' and '$tgl2b'"; $judul[] = "Tanggal JTO = '$tgl1b' s/d '$tgl2b'"; }
		if($tgl1c!='0000-00-00'){ $where1.=" and TglCair between '$tgl1c' and '$tgl2c'"; $judul[] = "Tanggal Cair = '$tgl1c' s/d '$tgl2c'"; }
		if(!empty($kdkasbank)){ $where1.=" and KdBankCair='$kdkasbank'"; $judul[] = "Bank Cair = $kdkasbank"; }
		if(!empty($jenis)){ $where1.=" and Jenis='$jenis'"; $judul[] = "Jenis = $jenis"; }
		if($opt=="BC")
		   $where1.=" and Status='B'";
		if($opt=="SC")
		   $where1.=" and Status='C'";
        if($opt=="JT")
		   $where1.=" and TglJTo<='$tgl1'";
        if($opt=="SG")
		   $where1.="";		   
		
	    $sql1 = "Select * from bukugiro where ".$where1;
		//echo $sql1;
		
		$data['hasil'] = $this->giromodel->getReport($sql1);
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listkasbank'] = $this->giromodel->getKasBank();
			$data['listjenis'] = Array ( Array ( 'Jenis' => 'P', 'NamaJenis' => 'Hutang Giro' ), 
                                         Array ( 'Jenis' => 'R', 'NamaJenis' => 'Piutang Giro' ));
			$this->load->view('report/giro/views', $data);
		}
		else
		{
			$this->load->view('report/giro/reportgiro', $data);
		}
	}
}
?>