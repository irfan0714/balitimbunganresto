<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_Food_Cost extends authcontroller {

    function __construct(){
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_food_costmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['Jenis'] = 'R';
			$data['tampilkanDT'] = true;
			$data['excel'] = '';
			$data['print'] = '';
			$data['hasil'] = array();
		
			$this->load->view('report/report_food_cost/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$jenis = $this->input->post("Jenis");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['Jenis'] = $jenis;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$judul = array();
		
		$data['tampilkanDT'] = true;
	
		$data['hasil'] = $this->report_food_costmodel->getReport($jenis);
				
		$data['judul'] = 'Report Food Cost';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_food_cost/views', $data);
		}
		else
		{
			$this->load->view("report/report_food_cost/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function popup($resepid){
		$data['header']	= $this->report_food_costmodel->getheader($resepid);
		$data['detail']	= $this->report_food_costmodel->getdetail($resepid);
		
		$this->load->view("report/report_food_cost/popup", $data);
	}
}
?>