<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class hutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/hutangmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['listsupplier'] = $this->hutangmodel->getSupplier();
			$data['listjenis'] = Array ( Array ( 'Jenis' => 'F', 'NamaJenis'=>'Faktur' ), 
                                         Array ( 'Jenis' => 'R', 'NamaJenis'=>'Retur' ),
										 Array ( 'Jenis' => 'D', 'NamaJenis'=>'Nota Debet' ), 
                                         Array ( 'Jenis' => 'K', 'NamaJenis'=>'Nota Kredit'));
			$data['kdsupplier'] = "";
			$data['jenis'] = "";
			$data['tgl1a'] = '00-00-0000';
			$data['tgl2a'] = '00-00-0000';
			$data['tgl1b'] = '00-00-0000';
			$data['tgl2b'] = '00-00-0000';
			$data['cekbl'] = "checked='checked'";
			$data['ceksl'] = "";
			$data['ceksp'] = "";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkan'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/hutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$tgl1a = $this->input->post("tgl1a");
		$tgl2a = $this->input->post("tgl2a");
		$tgl1b = $this->input->post("tgl1b");
		$tgl2b = $this->input->post("tgl2b");
		$kdsupplier = $this->input->post("kdsupplier");
		$jenis = $this->input->post("jenis");
		$opt = $this->input->post("opt");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$data['kdsupplier'] = $kdsupplier;
		$data['jenis'] = $jenis;
		$data['tgl1a'] = $tgl1a;
		$data['tgl2a'] = $tgl2a;
		$data['tgl1b'] = $tgl1b;
		$data['tgl2b'] = $tgl2b;
		$data['cekbl'] = "";
		$data['ceksl'] = "";
		$data['ceksp'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		if($opt=="BL"){
			$data['cekbl'] = "checked='checked'";
		}
		if($opt=="SL"){
			$data['ceksl'] = "checked='checked'";
		}
		if($opt=="SP"){
			$data['ceksp'] = "checked='checked'";
		}
		$data['tampilkan'] = true;
		$judul = array();
		$tgl1a = $mylib->ubah_tanggal($tgl1a);
		$tgl2a = $mylib->ubah_tanggal($tgl2a);
		$tgl1b = $mylib->ubah_tanggal($tgl1b);
		$tgl2b = $mylib->ubah_tanggal($tgl2b);
		
		$wheredtl = "TglDokumen between '$tgl1a' and '$tgl2a'";
		$where1 = "Jenis=Jenis";
		if($tgl1a!='0000-00-00'){ $where1.=" and TglTransaksi between '$tgl1a' and '$tgl2a'"; $judul[] = "Tanggal Terima = '$tgl1a' s/d '$tgl2a'"; }
		if($tgl1b!='0000-00-00'){ $where1.=" and TglJto between '$tgl1b' and '$tgl2b'"; $judul[] = "Tanggal JTO = '$tgl1b' s/d '$tgl2b'"; }
		if(!empty($kdsupplier)){ $where1.=" and a.KdSupplier='$kdsupplier'"; $judul[] = "Customer = $kdsupplier"; }
		if(!empty($jenis)){ $where1.=" and Jenis='$jenis'"; $judul[] = "Jenis = $jenis"; }
		if($opt=="BL")
		   $where1.=" and TotalHutang<>TotalBayar";
		if($opt=="SL")
		   $where1.=" and TotalHutang=TotalBayar";
        if($opt=="SP")
		   $where1.="";		   
		
	    $sql1 = "Select a.*,b.Nama from hutang a,supplier b where a.KdSupplier=b.KdSupplier and ".$where1;
		//echo $sql1;
		
		$data['hasil'] = $this->hutangmodel->getReport($sql1);
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listsupplier'] = $this->hutangmodel->getSupplier();
			$data['listjenis'] = Array ( Array ( 'Jenis' => 'F', 'NamaJenis'=>'Faktur' ), 
                                         Array ( 'Jenis' => 'R', 'NamaJenis'=>'Retur' ),
										 Array ( 'Jenis' => 'D', 'NamaJenis'=>'Nota Debet' ), 
                                         Array ( 'Jenis' => 'K', 'NamaJenis'=>'Nota Kredit'));
			$this->load->view('report/hutang/views', $data);
		}
		else
		{
			$this->load->view('report/hutang/reporthutang', $data);
		}
	}
}
?>