<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class cetakstruk_store extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
       
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");

        $where = array('1','2','3','4','5','6','7');
    	$data['store'] = $this->db
    							->select('')
    							->from('store')
                                ->where_in('KodeStore',$where)
    							->get()->result_array();
		if($sign=="Y")
		{
			$this->load->view('report/cetakstruk_store/cetakstruk_store',$data);
		}
		else{
			$this->load->view('denied');
		}
	}

    function cek_struk(){

        $nostruk = $this->input->post('nostruk');

        $Q = "SELECT * FROM transaksi_header WHERE NoStruk = '".$nostruk."'";

        $Ex = $this->db->query($Q);

        if($Ex->num_rows() > 0){
            $result = "valid";
        }else{
            $result = "invalid";
        }

        echo $result;
    }

	function cetak()
    {

        $store      = $this->input->post('v_store');
        $start_date = $this->input->post('v_start_date');
        $end_date   = $this->input->post('v_end_date');
        $struk_terakhir    = $this->input->post("struk_terakhir");

        $date_start = date("Y-m-d",strtotime($start_date));
        $date_end   = date("Y-m-d",strtotime($end_date));

        $data['store'] = $this->db->get('aplikasi')->result_array();


        $sql_cari_kassa = "SELECT * FROM kassa WHERE KdStore = '".$store."'";
        $cari_kassa = $this->db->query($sql_cari_kassa)->result_array();

        if(count($cari_kassa) < 0){
        	echo "ID kassa dari storeID ".$store." Kosong";die();
        }


        $kassa = "(";
        foreach ($cari_kassa as $key => $val) {
        	$kassa .= "'".$val['id_kassa']."',";
        }
        $IDkassa = substr($kassa,0,-1).")";
        
        $WhereStruk = "";
        if($struk_terakhir !=""){
            $WhereStruk = "AND h.NoStruk > $struk_terakhir";
        }

        $sql_header = "SELECT h.*, k.IsCounter, m.JumlahPoint, COALESCE(gh.NamaGroupDisc,'') AS NamaGroupDisc, 
    				COALESCE(m.NamaMember,'') as NamaMember, tm.NilaiPoint
    				FROM transaksi_header h inner join kassa k on h.NoKassa = k.id_kassa 
        			LEFT JOIN member m on h.KdMember=m.KdMember
        			LEFT JOIN type_member tm on m.KdTipeMember=tm.KdTipeMember
        			LEFT JOIN group_disc_header gh on h.NamaCard=gh.KdGroupDisc
        			WHERE 1  
                    AND h.NoKassa IN $IDkassa 
                    AND h.Tanggal BETWEEN '$date_start' AND '$date_end' 
                    $WhereStruk 
                    ORDER BY h.NoStruk, h.NoKassa ASC";

        $header = $this->db->query($sql_header)->result_array();

        if(count($header) < 1){
        	echo "<script>alert('Data Kosong');window.history.go(-1);</script>";die();
        }

        $data['header'] = $header;
        
		$html =  $this->load->view('report/cetakstruk_store/cetak_struk_wireness_dos', $data);
		
		$filename	='pos';
		$ext		='ctk';
		
		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;
		
    }
}
?>