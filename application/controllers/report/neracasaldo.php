<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class neracasaldo extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/neracasaldo_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->neracasaldo_model->getDate();
			$bulanaktif     = $this->session->userdata('bulanaktif');
			$tahunaktif     = $this->session->userdata('tahunaktif');
			$bulanaktif2    = $this->session->userdata('bulanaktif');
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['listdept'] = $this->neracasaldo_model->getDept();
			$data['dept'] = "00";
			$data['listtingkat'] = array( "Tiga","Dua","Satu" );
			$data['tingkat'] = "Tiga";
			$data['listmutasi'] = array( "Ya","Tidak" );
			$data['mutasi'] = "Ya";
			$data['listbudget'] = array( "Ya","Tidak" );
			$data['budget'] = "Tidak";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['bulanaktif'] = $bulanaktif;
			$data['bulanaktif2'] = $bulanaktif2;
			$data['tahunaktif'] = $tahunaktif;
			$data['bulan1'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['tahun2'] = $this->neracasaldo_model->getListTahun();
			$data['bulan2'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['track'] = $mylib->print_track();
			$this->load->view('report/neracasaldo/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$user = $this->session->userdata('username');
		$data['store']		= $this->neracasaldo_model->aplikasi();
		$bulanaktif = $this->input->post("bulan1");
		$bulanaktif2 = $this->input->post("bulan2");
		$tahunaktif = $this->input->post("tahun2");
		$dept = $this->input->post("dept");
		$tingkat = $this->input->post("tingkat");
		$mutasi = $this->input->post("mutasi");
		$budget = $this->input->post("budget");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['dept'] = $dept;
		$data['tingkat'] = $tingkat;
		$data['mutasi'] = $mutasi;
		$data['budget'] = $budget;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif'] = $bulanaktif;
		$data['bulanaktif2'] = $bulanaktif2;
		$data['tahunaktif'] = $tahunaktif;
		$data['bulan1'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$data['tahun2'] = $this->neracasaldo_model->getListTahun();
		$data['bulan2'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$judul = array();
		$where1 = "AND Tahun='$tahunaktif'";
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Periode = $bulanaktif s/d $bulanaktif2 - $tahunaktif";
		if(!empty($dept)){ $where1.=" and a.Departemen='$dept'"; $judul[] = "Departemen = $dept"; }
		
		$data['tampilkanRT'] = true;
		
		$colspan = 3;
		if($budget=="Ya")$colspan+=1;
		if($mutasi=="Ya")$colspan+=3;
		$data['colspan'] = $colspan;
		
		$sdebet = " ";
		$skredit = " ";
		$sadjdebet = " ";
		$sadjkredit = " ";
		$sbudget = " ";
		for($i=$bulanaktif;$i<=$bulanaktif2;$i++){
		   $bl = $i;
		   if(strlen($bl)==1){ 
			  $bl="0".$bl;
		   }
		   $sdebet = $sdebet."debet".$bl;
		   $skredit = $skredit."kredit".$bl;
		   $sadjdebet = $sadjdebet."adjdebet".$bl;
		   $sadjkredit = $sadjkredit."adjkredit".$bl;
		   $sbudget = $sbudget."budget".$bl;
		   if($i<$bulanaktif2){
			 $sdebet = $sdebet."+";
			 $skredit = $skredit."+";
			 $sadjdebet = $sadjdebet."+";
			 $sadjkredit = $sadjkredit."+";
			 $sbudget = $sbudget."+";
		   }
		}
		$sdebet0 = "sum(".$sdebet.")";
		$skredit0 = "sum(".$skredit.")";
		$sadjdebet0 = "sum(".$sadjdebet.")";
		$sadjkredit0 = "sum(".$sadjkredit.")";
		$sawal0 = "sum(awal".$bulanaktif.")";
		$sbud0 = "sum(".$sbudget.")";
		$sbud1 = "sum(budget".$bulanaktif.")";
		
		$sql = "SELECT c.*, d.namarekening AS namaparent0,d.posisi FROM 
				(SELECT a.*, b.namarekening AS namaparent1,b.parent AS parent0 FROM
				(SELECT a.kdrekening,namarekening,$sawal0 AS awal,$sdebet0 AS debit,
				$skredit0 AS kredit,$sadjdebet0 AS adjdebit,$sadjkredit0 AS adjkredit,
				$sbud0 as budget,$sbud1 as budget0, parent AS parent1 FROM saldo_gl a, rekening b 
				WHERE a.kdrekening=b.kdrekening and a.kdrekening<>'31030000' $where1
				group by a.kdrekening)a, rekening b
				WHERE a.parent1=b.kdrekening ORDER BY parent0,parent1,kdrekening)c, rekening d
				WHERE c.parent0=d.kdrekening";
		if($user=='yuri0717' || $user=='jamal3108'){
			echo $sql;
		}
		
		$data['hasil1'] = $this->neracasaldo_model->getReport($sql);

		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listdept'] = $this->neracasaldo_model->getDept();
			$data['listtingkat'] = array( "Tiga","Dua","Satu" );
			$data['listmutasi'] = array( "Ya","Tidak" );
			$data['listbudget'] = array( "Ya","Tidak" );
			$this->load->view('report/neracasaldo/views', $data);
		}
		else
		{

			$this->load->view("report/neracasaldo/reportRT", $data);
			$data['excel'] = "";
		}
	}
}
?>