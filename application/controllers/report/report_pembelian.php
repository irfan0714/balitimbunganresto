<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_pembelian extends authcontroller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('report/report_pembelian_model');
		$this->load->library('globallib');
	}

	public function index()
	{
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {		
 

		$submit			= $this->input->post('submit');
		$tgldari		= $this->input->post('tgldari');
		$tglsampai		= $this->input->post('tglsampai');
		
		
		$data['submit']			= $submit;
		$today					= date('Y-m-d');
		$data['tgldari']		= $today;
		$data['tglsampai']		= $today;
		$data['tampilkanDT'] 	= false;
		$dataresult				= $this->report_pembelian_model->viewdata($today,$today);
		$data['viewdata']		= $dataresult; 
		
		$this->load->view('report/pembelian/view',$data); 

		
	}
	//a24

	public function view()
	{
		$submit		= $this->input->post('submit');
		$tgldari	= $this->input->post('tgldari');
		$tglsampai	= $this->input->post('tglsampai');
		$today		= date('Y-m-d');

		if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai)) ) {
			$tgldari	= $today;
			$tglsampai	= $today;
		}

		$data['tgldari']			= $tgldari;
		$data['tglsampai']			= $tglsampai;
		$data['submit']				= $submit;
		$data['tanggaldari']		= $tgldari;
		$data['tanggalsampai']		= $tglsampai;
		$data['tampilkanDT'] 		= true;
		$data['viewdata']			= $this->report_pembelian_model->viewdata($tgldari,$tglsampai);

		if($submit=='TAMPIL')
		{
			$this->load->view('report/pembelian/view',$data);
		}
		
		else if($submit=='XLS')
		{
			//$this->_printxls($dataresult);
		
				//$data['track'] = $mylib->print_track();
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="report_pembelian.xls"');
                
                $this->load->view('report/pembelian/tampil', $data);
		}       
	}

}

/* End of file report_pembelian.php */
/* Location: ./application/controllers/report_pembelian.php */