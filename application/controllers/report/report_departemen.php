<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_departemen extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_departemen_model');
    }

    function index() {
		
		
	
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {			
			$today					= date('Y-m-d');
			
			$namadep = '';
			$data['tgldari']		= $today;
			$data['tglsampai']		= $today;
			$data['namadep']		= '';
			$data['tampilkanDT'] 	= false;
			$dataresult				= $this->report_departemen_model->viewData($today,$today, $namadep);
			$data['viewdata']		= $dataresult; 
			$this->load->view('report/departemen/views', $data);
//		} else {
//			$this->load->view('denied');
//		}

    }

	function view() {
		$mylib = new globallib();
		
		$submit			= $this->input->post('submit');
		$tgldari		= $this->input->post('tgldari');
		$tglsampai		= $this->input->post('tglsampai');
		$namadep		= $this->input->post('namadep');
		$today			= date('Y-m-d');
		$tanggaldari	= strtotime($tgldari);
		$tanggalsampai	= strtotime($tglsampai);
		
		
    
		
			if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai) OR ($namadep=='' OR is_null($namadep)) )) {
				$tgldari		= $today;
				$tglsampai		= $today;
				$namadep		= "";
			}
		
		$data['tgldari']			= $tgldari;
		$data['tglsampai']			= $tglsampai;
		$data['namadep']			= $namadep;
		$data['submit']				= $submit;
		$data['tanggaldari']		= $tgldari;
		$data['tanggalsampai']		= $tglsampai;
		$data['jmlhari']			= $this->report_departemen_model->datediff($tglsampai, $tgldari);
		$data['tampilkanDT'] 		= true;
		$data['viewdata']			= $this->report_departemen_model->viewData($tgldari,$tglsampai, $namadep);
		$data['listemp']			= $this->report_departemen_model->getListEmp($tgldari,$tglsampai, $namadep);
		          
        if($submit=='TAMPIL')
		{
			$this->load->view('report/departemen/views', $data);
		}
		
		else if($submit=='XLS')
		{
			//$this->_printxls($dataresult);
		
				//$data['track'] = $mylib->print_track();
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="reportshift.xls"');
                
                $this->load->view('report/departemen/tampil', $data);
		}        
		
		
		/*else if($submit=='XLS')
		{	$this->_printxls($dataresult);
                
		}       
		
		*/
		//else
			//$this->tambah();

	}

}

?>