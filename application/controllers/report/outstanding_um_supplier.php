<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Outstanding_UM_Supplier extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/outstanding_um_supplier_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	
    	$excel = $this->input->post('excel');
    	if($sign=="Y")
		{
			$data['hasil'] = $this->outstanding_um_supplier_model->getData();
			$data['excel'] = $excel;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/outstanding_um_supplier/view', $data);
		}
		else{
			$this->load->view('denied');
		}
	}
}
?>