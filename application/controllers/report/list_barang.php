<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class list_barang extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/m_barang');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            $tanggal= $this->m_barang->getDate();
            $bulan 	= $this->session->userdata('bulanaktif');
            $tahun 	= $this->session->userdata('tahunaktif');
            $date 	= $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['divisi']	= $this->m_barang->getDivisi();
					  
            $data['excel'] = "";
            $data['print'] = "";
            $data['judul_r'] = "List Barang";
			$data['ul1'] =  $this->uri->segment(1);
            $data['ul2'] =  $this->uri->segment(2);
            $data['tampilkanDT'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/l_barang/views', $data);
        } else {
            $this->load->view('denied');
        }
    }

	// febri
	function search_report()
	{
		//print_r($_POST);
		$mylib = new globallib();
        $data['ul1'] =  $this->uri->segment(1);
            $data['ul2'] =  $this->uri->segment(2);
        $div = $this->input->post("div"); // divisi
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $data['judul_r'] = "List Barang";
        
        $data['excel'] = $excel;
        $data['print'] = $print;
        
        $data['judul'] = "List Barang";
        
       
        
        $data['tampilkanDT'] = true;
        
        $data['divisi']	= $this->m_barang->getDivisi();
        $data['hasil'] = $this->m_barang->getReport($div);
          
        if ($excel == "") {
            if ($print == "print") 
            {
                $data['fileName'] = 'harian.npm';
            } 
            else 
            {
                $data['track'] = $mylib->print_track();
                
                $this->load->view('report/l_barang/views', $data);
            }
        } 
        else 
        {
                $this->load->view("report/l_barang/tampil", $data);
        }
	}

}

?>