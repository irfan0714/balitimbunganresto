<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_Cost_Of_Food extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_cost_of_food_model', 'cost_of_food');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            
            $bulan = date('m');
            $tahun = $this->session->userdata('tahunaktif');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_start_date'] = date('d') . '-' . date('m') . '-' . date('Y');
            $data['v_end_date'] = date('d') . '-' . date('m') . '-' . date('Y');
            
            $data['gudang'] = $this->cost_of_food->getgudang($data["username"]);
            $data['subkategori'] = $this->cost_of_food->getsubkategori();
            $data['tampilkan'] = false;
            
            $data['excel'] = "";
            $data['print'] = "";
            $data['v_nostruk'] = "";
            
            $data['analisa'] = FALSE;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/cost_of_food/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		//print_r($_POST);die;

		$mylib = new globallib();
			
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$v_subkategori = $this->input->post("v_subkategori");
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $flag = true;
        $base_url = $this->input->post("base_url");
        $excel = $this->input->post("excel");
        $gudang = $this->input->post("gudang");
        
        $data['v_subkategori'] = $v_subkategori;
        $data['v_gudang'] = $gudang;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['flag'] = $flag;
        $data['excel'] = $excel;
        $data['gudang'] = $this->cost_of_food->getgudang($data["username"]);
        $data['subkategori'] = $this->cost_of_food->getsubkategori();
        $data['tampilkan'] = true;
        
        $data['judul'] = "Report Cost Of Food $v_start_date s/d $v_end_date";
                
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
		$data['data'] = $this->cost_of_food->getdata($v_subkategori,$v_start_date,$v_end_date, $gudang);
		
        $data['analisa'] = TRUE;
			
		$data['track'] = $mylib->print_track();
  
		if ($excel=="") 
        {
            $this->load->view('report/cost_of_food/views', $data);
        } 
        else 
        {
        	$this->load->view('report/cost_of_food/tampil', $data);
        }
	}
}

?>