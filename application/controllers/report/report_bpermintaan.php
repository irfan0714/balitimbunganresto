<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_bpermintaan extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_bpermintaan_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->report_bpermintaan_model->getDate();
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['listgudang'] = $this->report_bpermintaan_model->getGudang();
			$data['gudang'] = "";
			$data['listdivisi'] = $this->report_bpermintaan_model->getDivisi();
			$data['divisi'] = "";
			$data['listbarang'] = $this->report_bpermintaan_model->getBarang();
			$data['barang'] = "";
			$data['tgl1'] = $mylib->ubah_tanggal(substr($tanggal->TglTrans,0,8)."01"); //$mylib->ubah_tanggal($tanggal->TglTrans);
			$data['tgl2'] = $mylib->ubah_tanggal($tanggal->TglTrans);
			$data['cekrt'] = "checked='checked'";
			$data['cekdt'] = "";
			$data['excel'] = "";
			$data['cekf1'] = "";
			$data['cekf0'] = "";
			$data['cekf2'] = "checked='checked'";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['tampilkanDT'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_bpermintaan/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->report_bpermintaan_model->aplikasi();
		$tgl1 = $this->input->post("tgl1");
		$tgl2 = $this->input->post("tgl2");
		$gudang = $this->input->post("gudang");
		$divisi = $this->input->post("divisi");
		$barang = $this->input->post("barang");
		$opt = $this->input->post("opt");
		$opt1 = $this->input->post("opt1");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['gudang'] = $gudang;
		$data['divisi'] = $divisi;
		$data['barang'] = $barang;
		$data['tgl1'] = $tgl1;
		$data['tgl2'] = $tgl2;
		$data['cekrt'] = "";
		$data['cekdt'] = "";
		$data['cekf1'] = "";
		$data['cekf0'] = "";
		$data['cekf2'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		if($opt=="RT"){
			$data['cekrt'] = "checked='checked'";
		}
		if($opt=="DT"){
			$data['cekdt'] = "checked='checked'";
		}
		//echo $opt1;
		$data['jenis'] = "";
		if($opt1=="F0"){
			$data['cekf0'] = "checked='checked'";
			$data['jenis'] = "Open";
		}
		if($opt1=="F1"){
			$data['cekf1'] = "checked='checked'";
			$data['jenis'] = "Pending";
		}
		if($opt1=="F2"){
			$data['cekf2'] = "checked='checked'";
			$data['jenis'] = "Semua";
		}
		$data['tampilkanRT'] = false;
		$data['tampilkanDT'] = false;
		$judul = array();
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Tanggal = $tgl1 s/d $tgl2";
		$tgl1 = $mylib->ubah_tanggal($tgl1);
		$tgl2 = $mylib->ubah_tanggal($tgl2);
		$wheretgl = "TglDokumen between '$tgl1' and '$tgl2'";
		$wheretgl1 = "b.TglDokumen between '$tgl1' and '$tgl2'";
		$wheregudang = "";
		$wheregudang1 = "";
		$wheredivisi = "";
		$wheredivisi1 = "";
		$wherebarang = "";
		$wherebarang1 = "";
		$whereopt1 = "";
		$whereopt11 = "";

		if($divisi!="")
		{
			$wheredivisi = "and KdDivisi='$divisi'";
			$wheredivisi1 = "and a.KdDivisi='$divisi'";
			$judul[] = "Divisi = ".$divisi; 
		}
		if($gudang!="")
		{
			$wheregudang = "and KdGudang='$gudang'";
			$wheregudang1 = "and a.KdGudang='$gudang'";
			$judul[] = "Gudang = ".$gudang; 
		}
		if($barang!="")
		{
			$wherebarang = "and PCode='$barang'";
			$wherebarang1 = "and a.PCode='$barang'";
			$judul[] = "Barang = ".$barang; 
		}
		if($opt1=="F1")
		{
		   $whereopt1 = "and Status='1'";
		   $whereopt11 = "and b.Status='1'";
		}
		if($opt1=="F0")
		{
		   $whereopt1 = "and Status='0'";
		   $whereopt11 = "and b.Status='0'";
		}
		if($opt=="RT")
		{
			$data['hasil'] = $this->report_bpermintaan_model->getRekapTrans($wheretgl,$wheredivisi,$wheregudang,$wherebarang,$whereopt1);
			$data['tampilkanRT'] = true;
		}
		if($opt=="DT")
		{
			$data['hasil'] = $this->report_bpermintaan_model->getDetailTrans($wheretgl1,$wheredivisi1,$wheregudang1,$wherebarang1,$whereopt11);
			$data['tampilkanDT'] = true;
		}
		$data['judul'] = $judul;
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$data['listgudang'] = $this->report_bpermintaan_model->getGudang();
			$data['listdivisi'] = $this->report_bpermintaan_model->getDivisi();
			$data['listbarang'] = $this->report_bpermintaan_model->getBarang();
			$this->load->view('report/report_bpermintaan/views', $data);
		}
		else
		{
			if($opt=="RT")
			{
				$this->load->view("report/report_bpermintaan/reportRT", $data);
			}
			if($opt=="DT")
			{
				$this->load->view("report/report_bpermintaan/reportDT", $data);
			}
			$data['excel'] = "";
		}
	}
}
?>