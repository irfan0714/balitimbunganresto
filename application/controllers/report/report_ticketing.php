<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_ticketing extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('transaksi/ticketingmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            $tanggal = $this->ticketingmodel->getDate();
            //$bulan = $this->session->userdata('bulanaktif');
            //$tahun = $this->session->userdata('tahunaktif');
            $bulan = date('m');
            $tahun = date('Y');
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['v_start_date'] = '01' . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
            $data['tipe_report'] = '1';
            
            $data['excel'] = "";
            $data['print'] = "";
            
            $data['tampilkanDT'] = false;
            $data['tampilkanDR'] = false;
            $data['tampilkanRR'] = false;
            $data['tampilkanRT'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/ticketing/views', $data);
        } else {
            $this->load->view('denied');
        }
    }

	// febri
	function search_report()
	{
		$mylib = new globallib();
        $v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_jenis = $this->input->post("v_jenis");
        $tipe_report = $this->input->post("tipe_report");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        
        $data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['v_jenis'] = $v_jenis;
        $data['tipe_report'] = $tipe_report;
        $data['excel'] = $excel;
        $data['print'] = $print;
        
        $data['judul'] = "Reprot Ticketing $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
        $data['tampilkanDT'] = true;
        
       	$data['listjenis'] = $this->ticketingmodel->getJenis();
       	$data['total_ticket'] = $this->ticketingmodel->getTotalTicket($v_start_date,$v_end_date);
       	if($tipe_report=='1'){
			$data['hasil'] = $this->ticketingmodel->getReport($v_start_date,$v_end_date);	
		}else if($tipe_report=='2'){
			$data['hasil'] = $this->ticketingmodel->getReport_ticket($v_start_date,$v_end_date);	
		}else if($tipe_report=='3'){
			$data['hasil'] = $this->ticketingmodel->getReport_wilayah($v_start_date,$v_end_date);	
			$data['hasil_kota'] = $this->ticketingmodel->getReport_kota($v_start_date,$v_end_date);
		}else if($tipe_report=='4'){
			$data['hasil'] = $this->ticketingmodel->getReport_customer($v_start_date,$v_end_date);	
		}
        
        
        $data['jenis_bayar'] =$this->ticketingmodel->getTicketHead($v_start_date,$v_end_date);
          
        if ($excel == "") {
            if ($print == "print") 
            {
                $data['fileName'] = 'harian.sss';
            } 
            else 
            {
                $data['track'] = $mylib->print_track();
                
                $this->load->view('report/ticketing/views', $data);
            }
        } 
        else 
        {
        	if($tipe_report=='1'){
				$this->load->view("report/ticketing/tampil", $data);
			}else if($tipe_report=='2'){
				//penjualan ticket
				$this->load->view("report/ticketing/tampil_ticket", $data);
			}else if($tipe_report=='3'){
				//sebaran wilayah
				$this->load->view("report/ticketing/tampil_wilayah", $data);
			}else if($tipe_report=='4'){
				//loyalitas customer
				$this->load->view("report/ticketing/tampil_customer", $data);
			}
                
        }
	}

}

?>