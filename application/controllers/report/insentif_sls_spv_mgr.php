<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Insentif_sls_spv_mgr extends authcontroller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('report/report_insentif_sls_spv_mgr');
	}
 

	public function index()
	{  	

		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {

			$data['bulan']			= "";
		    $data['tahun']			= "";
			$data['tampilkanDT'] 	= false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_insentif_sls_spv_mgr/view_insentif_sls_spv_mgr', $data);	
//		} else {
//			$this->load->view('denied');
//		}
		
	}

	public function tampil() 
	{ 
		//print_r($_POST);
		$mylib = new globallib();
		
		$submit		= $this->input->post('submit');
		$bulan		= $this->input->post('bulan');
		$tahun		= $this->input->post('tahun');
		
		//tanggal_awal dan akhir
		$tglawal = $tahun."-".$bulan."-01";
		$jmlhari = $this->report_insentif_sls_spv_mgr->jmlhari($tglawal);
		$tglakhir = $jmlhari[0]['tgl'];
		
		$data['bulan']				= $bulan;
		$data['tahun']				= $tahun;
		$data['submit']				= $submit;
		$data['tampilkanDT'] 		= true;
		$data['viewdata']			= $this->report_insentif_sls_spv_mgr->viewData($tglawal, $tglakhir);
		$data['viewdatakunj']		= $this->report_insentif_sls_spv_mgr->viewDataKunj($tglawal, $tglakhir);
		$data['listemp']			= $this->report_insentif_sls_spv_mgr->getListEmp($bulan, $tahun);
		$data['listempspv']			= $this->report_insentif_sls_spv_mgr->getListEmpSpv($bulan, $tahun);
		$data['track'] = $mylib->print_track();

		 if($submit=='TAMPIL')
		{
			$this->load->view('report/report_insentif_sls_spv_mgr/view_insentif_sls_spv_mgr', $data);	
		}
		
		else if($submit=='XLS')
		{
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="report_insentif_sls_spv_mgr.xls"');
                
                $this->load->view('report/report_insentif_sls_spv_mgr/tampil_insentif_sls_spv_mgr', $data);	
		}        
		
		
	}

}

/* End of file absensi.php */
/* Location: ./application/controllers/absensi.php */