<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class neraca extends authcontroller {

    function __construct(){
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/neraca_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
    	if($sign=="Y")
		{
			$tanggal = $this->neraca_model->getDate();
			list($tahunaktif, $bulanaktif, $tglaktif)  = explode('-',$tanggal);
			$data['userlevel'] = $this->session->userdata('userlevel');
			
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['bulanaktif'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['tampilkanDT'] = false;
			$data['bulan1'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['tahun1'] = $this->neraca_model->getListTahun();
			$data['track'] = $mylib->print_track();
			$data['type1'] = '';
			$data['rekapdetail1'] = 'R';
			$this->load->view('report/neraca/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->neraca_model->aplikasi();
		$bulanaktif = $this->input->post("bulan1");
		$tahunaktif = $this->input->post("tahun1");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$type = $this->input->post("type");
		$rekapdetail = $this->input->post("rekapdetail");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif'] = $bulanaktif;
		$data['tahunaktif'] = $tahunaktif;
		$data['namabulan'] = array( "Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des" );
		$data['bulan1'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$data['tahun1'] = $this->neraca_model->getListTahun();
		$judul = array();
		
		$judul[] = $data['store'][0]['NamaPT'];
		$judul[] = "Laporan Neraca";
		$judul[] = "Periode = $bulanaktif - $tahunaktif";
		
		$data['tampilkanDT'] = true;
		if($type=='Perbandingan'){
			if($rekapdetail=='R'){
				$data['data'] = $this->neraca_model->getReportPerbandinganRekap($tahunaktif,$bulanaktif);		
			}else{
				$data['data'] = $this->neraca_model->getReportPerbandingan($tahunaktif,$bulanaktif);
			}
		}
		else{
			if($rekapdetail=='R'){
				$data['data'] = $this->neraca_model->getReportTrendRekap($tahunaktif,$bulanaktif);
			}else{
				$data['data'] = $this->neraca_model->getReportTrend($tahunaktif,$bulanaktif);
			}
			
		}
		
		$data['judul'] = $judul;
		$data['type1'] = $type;
		$data['rekapdetail1'] = $rekapdetail;
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/neraca/views', $data);
		}
		else
		{
			if($type=='Perbandingan'){
				$this->load->view("report/neraca/reportperbandingan", $data);	
			}else{
				$this->load->view("report/neraca/reporttrend", $data);
			}
			$data['excel'] = "";
		}
	}
}
?>