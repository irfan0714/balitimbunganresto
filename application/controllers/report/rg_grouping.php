<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
//purwanto on sept 16
class Rg_grouping extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rpt_rggroupingmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {


			$data['listgudang'] = $this->rpt_rggroupingmodel->getGudang();
			$data['listdivisi'] = $this->rpt_rggroupingmodel->getDivisi();
			$data['gudang'] = "";
			$data['divisi'] = "";
			
			$data['excel'] = "";
            $data['print'] = "";
            $data['tampilkandata'] = false;
            list($tahun, $bulan, $tgl) = explode('-',DATE('Y-m-d'));
            $data['tahun'] = $tahun;
            $data['bulan'] = $bulan;
            $data['search_by']="";
            
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/rg/view_rg', $data);
        } else {
            $this->load->view('denied');
        }
    }

    
    function search_report() {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $search_by = $this->input->post("search_by");
        
        $kdtransaksi = $this->input->post("kdtransaksi");
        $tahun = $this->input->post("tahun");
        $bulan = $this->input->post("bulan");
        
        $gudang = $this->input->post("gudang");
		$divisi = $this->input->post("divisi");
        
        $data['listgudang'] = $this->rpt_rggroupingmodel->getGudang();
        $data['listdivisi'] = $this->rpt_rggroupingmodel->getDivisi();
		$data['gudang'] = $gudang;
		$data['divisi'] = $divisi;
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$data['search_by'] = $search_by;
		

        $data['excel'] = $excel;
        $data['print'] = $print;
        $tabel_field = 'GAWAl'.$bulan;
        
		$data['judul'] = "Report Terima Barang Bulan " .$this->input->post("bulan"). " Tahun " .$this->input->post("tahun");
        //$data['hasil_cari'] = $this->rpt_rggroupingmodel->getReport_detail($bulan, $tahun, $gudang,$divisi, $tabel_field, $search_by);
		$hasil_cari = $this->rpt_rggroupingmodel->getReport_detail($bulan, $tahun, $gudang,$divisi, $tabel_field, $search_by);
		if(empty($hasil_cari)){
			$data['ada_data'] = "NO";
			
			$data['SubKategori_PCode'] = "";
			$data['NamaLengkaps'] = "";
			$data['satuans'] = "";
			$data['quantity'] = "";
			$data['list_hargas'] = "";
			$data['list_KdSubKategori']="";
			$data['NamaSubKategoris'] = "";
			$data['NamaSuppliers'] = "";
			
		}else{
		$data['ada_data'] = "YES";	
		foreach($hasil_cari AS $key){
				
				$arr_data["SubKategori_PCode"][$key["KdSubKategori"]][$key["PCode"]] = $key["PCode"];
				$arr_data["NamaLengkap"][$key["PCode"]] = $key["NamaLengkap"];
				$arr_data["NamaSupplier"][$key["PCode"]] = $key["Nama"];
				$arr_data["satuan"][$key["PCode"]] = $key["SatuanSt"];
				$arr_data["list_qty"][$key["PCode"]] = $key["Qty"];		
				$arr_data["list_harga"][$key["PCode"]] = $key["Harga"];
				$arr_data["list_KdSubKategori"][$key["KdSubKategori"]] = $key["KdSubKategori"];
				$arr_data["NamaSubKategori"][$key["KdSubKategori"]] = $key["NamaSubKategori"];
				
				$arr_data["list_pcode"][$key["PCode"]] = $key["PCode"];
				
			}
			
			$data['SubKategori_PCode'] = $arr_data["SubKategori_PCode"];
			$data['NamaLengkaps'] = $arr_data["NamaLengkap"];
			$data['satuans'] = $arr_data["satuan"];
			$data['quantity'] = $arr_data["list_qty"];
			$data['list_hargas'] = $arr_data["list_harga"];
			$data['list_KdSubKategori']=$arr_data["list_KdSubKategori"];
			$data['NamaSubKategoris'] = $arr_data["NamaSubKategori"];
			$data['NamaSuppliers'] = $arr_data["NamaSupplier"];
		    
		    }
			
			if ($excel == "") {
				if ($print == "print") {
					$data['fileName'] = 'harian.sss';
				} else {
					$data['track'] = $mylib->print_track();
					$data['tampilkandata'] = true;
					$this->load->view('report/rg/view_rg', $data);
				}
			} else {
				 $this->load->view('report/rg/tampil_rg', $data);
				
			}  
			
		
        
    }
    

}

?>