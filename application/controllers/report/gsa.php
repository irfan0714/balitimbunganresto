<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gsa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('report/report_gsa');
	}
 

	public function index()
	{  	

		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {

			$today					= date('Y-m-d');
			
			$data['bulan']			= "";
		    $data['tahun']			= "";
			$data['tampilkanDT'] 	= false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_gsa/view_gsa', $data);	
//		} else {
//			$this->load->view('denied');
//		}
		
	}

	public function tampil() 
	{ 

		$mylib = new globallib();
		
		$submit			= $this->input->post('submit');
		
		
		$bulan		= $this->input->post('bulan');
		$tahun		= $this->input->post('tahun');
		//tanggal_awal dan akhir
		$tgldari = $tahun."-".$bulan."-01";
		$jmlhari = $this->report_gsa->jmlhari($tgldari);
		$tglsampai = $jmlhari[0]['tgl'];
		
		
		$today			= date('Y-m-d');
		$tanggaldari	= strtotime($tgldari);
		$tanggalsampai	= strtotime($tglsampai);


			if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai)) ) {
				$tgldari		= $today;
				$tglsampai		= $today;
				$namadep		= "";
			}
			
		$data['bulan']				= $bulan;
		$data['tahun']				= $tahun;
		$data['submit']				= $submit;
		$data['tanggaldari']		= $tgldari;
		$data['tanggalsampai']		= $tglsampai;
		$data['jmlhari']			= $this->report_gsa->datediff($tglsampai, $tgldari);
		$data['tampilkanDT'] 		= true;
		$data['viewdata']			= $this->report_gsa->viewData($tgldari,$tglsampai);
		$data['listemp']			= $this->report_gsa->getListEmp($tgldari,$tglsampai);
		$data['track'] = $mylib->print_track();
		//echo "<pre>";print_r($data['viewdata']);echo "</pre>";die;

		 if($submit=='TAMPIL')
		{
			$this->load->view('report/report_gsa/view_gsa', $data);	
		}
		
		else if($submit=='XLS')
		{
			//$this->_printxls($dataresult);
		
				//$data['track'] = $mylib->print_track();
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="reportgsa.xls"');
                
                $this->load->view('report/report_gsa/tampil_gsa_', $data);	
		}        
		
		
	}

}

/* End of file absensi.php */
/* Location: ./application/controllers/absensi.php */