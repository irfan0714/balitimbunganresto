<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
//purwanto on sept 16
class Report_mutasipaket extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rpt_mutasipaketmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $tanggal = $this->rpt_mutasipaketmodel->getDate();
            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

            $data['dtpicker1'] = $this->input->post("dtpicker1");
            $data['dtpicker2'] = $this->input->post("dtpicker1");

            $data['excel'] = "";
            $data['print'] = "";

            $data['tampilkanTR'] = false;
            $data['tampilkanDT'] = false;
            $data['cektr'] = "checked='checked'";
            $data['cekdt'] = "";


            $data['track'] = $mylib->print_track();
            $this->load->view('report/persediaan/view_mutasi_paket', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search_report() {
        //print_r($_POST);die();
        $mylib = new globallib();
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $opt = $this->input->post("opt");
        $data['dtpicker1'] = $this->input->post("dtpicker1");
        $data['dtpicker2'] = $this->input->post("dtpicker2");
        $tglawal = $mylib->ubah_tanggal($this->input->post("dtpicker1"));
        $tglakhir = $mylib->ubah_tanggal($this->input->post("dtpicker2"));

        $data['excel'] = $excel;
        $data['print'] = $print;

        $data['cektr'] = "";
        $data['cekdt'] = "";

        $data['judul'] = "Report Mutasi Paket " .$this->input->post("dtpicker1"). " s/d " .$this->input->post("dtpicker2");
        
        if ($opt == "DT") {
            $data['cekdt'] = "checked='checked'";
            $data['tampilkanDT'] = true;
            $data['tampilkanTR'] = false;
            $data['hasil'] = $this->rpt_mutasipaketmodel->getReport_Detail($tglawal, $tglakhir);
        }
        if ($opt == "TR") {
            $data['cektr'] = "checked='checked'";
            $data['tampilkanTR'] = true;
            $data['tampilkanDT'] = false;
            $data['hasil'] = $this->rpt_mutasipaketmodel->getReport_header($tglawal, $tglakhir);
        }

        if ($excel == "") {
            if ($print == "print") {
                $data['fileName'] = 'harian.sss';
            } else {
                $data['track'] = $mylib->print_track();

                $this->load->view('report/persediaan/view_mutasi_paket', $data);
            }
        } else {
            if ($opt == "TR") {
                $this->load->view("report/persediaan/tampil_mutasi_paket_tr", $data);
            } else if ($opt == "DT") {
//                $data['hasil'] = $this->rpt_mutasipaketmodel->getReport_detail($bulan, $tahun);
//                $data['tampilkanDT'] = true;
                $this->load->view("report/persediaan/tampil_mutasi_paket_dt", $data);
            }
        }
    }

}

?>