<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_mutasi_piutang extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_mutasi_piutang_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		//print_r($sign);die;
    	if($sign=="Y")
		{
			$tanggal = $this->report_mutasi_piutang_model->getDate();
			$bulanaktif     = $this->session->userdata('bulanaktif');
			$tahunaktif     = $this->session->userdata('tahunaktif');
			$data['userlevel'] = $this->session->userdata('userlevel');
			$data['listcustomer'] = $this->report_mutasi_piutang_model->getCustomer();
			$data['customer'] = "";
			$data['excel'] = "Excel";
			$data['print'] = "";
			$data['tampilkanRT'] = false;
			$data['bulanaktif'] = $bulanaktif;
			$data['tahunaktif'] = $tahunaktif;
			$data['tahun'] = array( "2016","2017","2018","2019","2020" );
			$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_mutasi_piutang/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		//print_r($_POST);
		$data['store']		= $this->report_mutasi_piutang_model->aplikasi();
		$bulanaktif = $this->input->post("bulan");
		$tahunaktif = $this->input->post("tahun");
		$data['listcustomer'] = $this->report_mutasi_piutang_model->getCustomer();
		
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$userlevel = $this->input->post("userlevel");
		$data['userlevel'] = $userlevel;
		$data['customer'] = $customer;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['bulanaktif'] = $bulanaktif;
		$data['tahunaktif'] = $tahunaktif;
		$data['tahun'] = array( "2016","2017","2018","2019","2020" );
		$data['bulan'] = array( "01","02","03","04","05","06","07","08","09","10","11","12" );
		$judul = array();
		$judul[] = $data['store'][0]['Alamat1PT'];
		$judul[] = "Periode = $bulanaktif - $tahunaktif";
		
		$data['tampilkanRT'] = true;

		$data['hasil'] = $this->report_mutasi_piutang_model->getNama($customer);
		$data['hasil0'] = $this->report_mutasi_piutang_model->getRekapTrans($tahunaktif,$bulanaktif);
		$data['hasil1'] = $this->report_mutasi_piutang_model->getDetailTrans($tahunaktif,$bulanaktif);
		//$judul[] = "Customer  = $customer - ".$data['hasil']->NamaCustomer;
		$data['judul'] = $judul;
		if($print=="screen")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_mutasi_piutang/views', $data);
		}
		else
		{
			$this->load->view("report/report_mutasi_piutang/reportRT", $data);
		}
	}
}
?>