<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class SalesPerStruk extends authcontroller {

    function __construct(){
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/salesperstruk_model');
    }

    function index(){
     	$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/salesperstruk/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->salesperstruk_model->aplikasi();
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$salesrec = $this->salesperstruk_model->getsales($tgldari,$tglsampai);
		
		
		$arr_sales = array();
		$arr_guest = array();
		$arr_struk = array();
		foreach($salesrec as $row){
			$tanggal = $row['Tanggal'];
			$kdstore = $row['KdStore'];
			$sales = $row['TotSales'];
			$guest = $row['JmlGuest'];
			$struk = $row['JmlStruk'];
			
			if(isset($arr_sales['$tanggal']['$kdstore'])){
				$arr_sales[$tanggal][$kdstore] += $sales;	
				$arr_guest[$tanggal][$kdstore] += $guest;	
				$arr_struk[$tanggal][$kdstore] += $struk;	
			}else{
				$arr_sales[$tanggal][$kdstore] = $sales;	
				$arr_guest[$tanggal][$kdstore] = $guest;	
				$arr_struk[$tanggal][$kdstore] = $struk;	
			}
			
		}
		
		$data['liststore'] = $this->salesperstruk_model->ListStore();
		$data['salesrec'] = $arr_sales;
		$data['guestrec'] = $arr_guest;
		$data['strukrec'] = $arr_struk;
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Laporan Analisa Store';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/salesperstruk/views', $data);
		}
		else
		{
			$this->load->view("report/salesperstruk/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail(){
		$mylib = new globallib();
		$data['store']		= $this->salesperstruk_model->aplikasi();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		$v_date_from = $awalbulan;
		$v_date_to = date('d-m-Y');
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['excel'] = '';
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$salesrec = $this->salesperstruk_model->getsales($tgldari,$tglsampai);
		
		$arr_sales = array();
		$arr_guest = array();
		$arr_struk = array();
		foreach($salesrec as $row){
			$tanggal = $row['Tanggal'];
			$kdstore = $row['KdStore'];
			$sales = $row['TotSales'];
			$guest = $row['JmlGuest'];
			$struk = $row['JmlStruk'];
			
			if(isset($arr_sales['$tanggal']['$kdstore'])){
				$arr_sales[$tanggal][$kdstore] += $sales;	
				$arr_guest[$tanggal][$kdstore] += $guest;	
				$arr_struk[$tanggal][$kdstore] += $struk;	
			}else{
				$arr_sales[$tanggal][$kdstore] = $sales;	
				$arr_guest[$tanggal][$kdstore] = $guest;	
				$arr_struk[$tanggal][$kdstore] = $struk;	
			}
			
		}
		
		$data['liststore'] = $this->salesperstruk_model->ListAllStore();
		$data['salesrec'] = $arr_sales;
		$data['guestrec'] = $arr_guest;
		$data['strukrec'] = $arr_struk;
		
		$data['tampilkanDT'] = true;
		$data['kirim_email']='Y';
		$data['listemail'] = $this->salesperstruk_model->getlistemail();
				
		$data['judul'] = 'Laporan Analisa Store';
		
		$this->load->view("report/salesperstruk/reportRT", $data);
	}
}
?>