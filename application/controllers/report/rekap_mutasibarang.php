<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
//purwanto on sept 16
class Rekap_mutasibarang extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/rekap_mutasibarangmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {			$data['excel'] = "";
            $data['print'] = "";
            $data['tampilkandata'] = false;
            list($tahun, $bulan, $tgl) = explode('-',DATE('Y-m-d'));
            $data['tahun'] = $tahun;
            $data['bulan'] = $bulan;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/persediaan/view_rekap_mutasi', $data);
        } else {
            $this->load->view('denied');
        }
    }

    
    function search_report() {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $tahun = $this->input->post("tahun");
        $bulan = $this->input->post("bulan");
        
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		
        $data['excel'] = $excel;
        $data['print'] = $print;
        $tabel_field = 'GAWAl'.$bulan;
        
		$data['judul'] = "Rekap Mutasi Barang Bulan " .$bulan. " Tahun " .$tahun;
		
		$detail = $this->rekap_mutasibarangmodel->getReport_detail($bulan, $tahun);
		$sawal  = $this->rekap_mutasibarangmodel->getSaldoAwal($bulan, $tahun);
		
		$data['detail'] = $detail;
		$data['sawal'] = $sawal;
		    
		if ($excel == "") {
            if ($print == "print") {
                $data['fileName'] = 'harian.sss';
            } else {
                $data['track'] = $mylib->print_track();
				$data['tampilkandata'] = true;
                $this->load->view('report/persediaan/view_rekap_mutasi', $data);
            }
        } else {
        	 $this->load->view('report/persediaan/tampil_rekap_mutasi', $data);
            
        }    
        
    }
    

}

?>