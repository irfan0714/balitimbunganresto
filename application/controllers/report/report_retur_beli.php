<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_retur_beli extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_retur_beli_model');
    }

    function index() {
        $mylib = new globallib();
        //$sign = $mylib->getAllowList("all");
        $sign='Y';
        if ($sign == "Y") 
        {
            $bulan = date('m');
            $tahun = date('Y');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));
            
			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            
            $data['excel'] = "";
            $data['print'] = "";
            
            $data['tampilkanDT'] = false;
            $data['kdcustomer'] = '';
            $data['customer'] = $this->report_retur_beli_model->getCustomer();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/returbeli/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		$mylib = new globallib();
		//echo "<pre>";print_r($_POST);echo "</pre>";
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $kdcustomer = $this->input->post("KdCustomer");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
      
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['kdcustomer'] = $kdcustomer;
        $data['v_username'] = $this->session->userdata('username');
        $data['excel'] = $excel;
        $data['print'] = $print;
        $data['customer'] = $this->report_retur_beli_model->getCustomer();
        
        $data['judul'] = "Reprot Sales Return $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
        $data['tampilkanDT'] = true;
  
        $data['hasil'] = $this->report_retur_beli_model->getReport($v_start_date,$v_end_date,$kdcustomer);
       
		if ($excel == "") 
        {
            if ($print == "print") 
            {
            	$data['fileName'] = 'rptdo.sss';
            } 
            else 
            {
            	$data['track'] = $mylib->print_track();
                $this->load->view('report/returbeli/views', $data);
            }
        } 
        else 
        {
        	//echo "<pre>";print_r($data);echo "</pre>";die();
			$this->load->view("report/returbeli/tampil", $data);
        }
	}
	
}

?>