<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_buku_bank extends authcontroller {

    function __construct() {
        parent::__construct();
		error_reporting(0);        
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_bukubank_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        //$sign = 'Y';
        if ($sign == "Y") 
        {            
        	$username = $this->session->userdata('username');
        	
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
                        
            $data['excel'] = "";
            $data['v_tahun'] = '';
            $data['v_bulan'] = '';
            $data['listbank'] = $this->report_bukubank_model->listbank($username);
            $data['v_bank'] = '';
            
            $data['tampilkan'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/bukubank/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		
		$v_tahun = $this->input->post("v_tahun");
		$v_bulan = $this->input->post("v_bulan");
		$v_bank = $this->input->post("v_bank");
		$excel = $this->input->post("excel");
        
		$mylib = new globallib();
		$username = $this->session->userdata('username');
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
		
        $data['v_tahun'] = $v_tahun;
        $data['v_bulan'] = $v_bulan;
        $data['v_bank'] = $v_bank;
        $data['listbank'] = $this->report_bukubank_model->listbank($username);
        
        $data['excel'] = $excel;
        
        $hasil = $this->report_bukubank_model->getReport($v_tahun, $v_bulan, $v_bank);
        $saldoawal = $this->report_bukubank_model->getSaldoAwal($v_tahun, $v_bulan, $v_bank);
        $data['data'] = $hasil;
        $data['saldoawal'] = $saldoawal;
 
        $data['judul'] = "Laporan Buku Bank Bulan $v_bulan/$v_tahun";
        
        $data['tampilkan'] = true;
  
		if ($excel == "") 
        {
            $data['track'] = $mylib->print_track();
            $this->load->view('report/bukubank/views', $data);
        } 
        else 
        {
			$this->load->view("report/bukubank/reportRT", $data);
        }
	}
}

?>