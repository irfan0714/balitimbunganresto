<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_Sales_Invoice extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_sales_invoice_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
    	
        if ($sign == "Y") 
        {
            $bulan = date('m');
            $tahun = date('Y');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));
            
			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            
            $data['excel'] = "";
            $data['print'] = "";
            $data['v_pilihan'] = 'transaksi';
            
            $data['tampilkanDT'] = false;
            $data['customer'] = $this->report_sales_invoice_model->getCustomer();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/salesinvoice/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $kdcustomer = $this->input->post("KdCustomer");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
        $v_pilihan = $this->input->post("v_pilihan");
        
        $data['v_pilihan'] = $v_pilihan;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['kdcustomer'] = $kdcustomer;
        $data['v_username'] = $this->session->userdata('username');
        $data['excel'] = $excel;
        $data['print'] = $print;
        $data['customer'] = $this->report_sales_invoice_model->getCustomer();
        
        $data['judul'] = "Report Sales Invoice $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
        $data['tampilkanDT'] = true;
  
        $data['hasil'] = $this->report_sales_invoice_model->getReport($v_start_date,$v_end_date,$kdcustomer);
        
		if ($excel == "") 
        {
            if ($print == "print") 
            {
            	$data['fileName'] = 'rptinvoice.sss';
            } 
            else 
            {
            	if($v_pilihan=="transaksi"){
            		$data['hasil'] = $this->report_sales_invoice_model->getReport($v_start_date,$v_end_date,$kdcustomer);
            	}else{
					$data['hasil'] = $this->report_sales_invoice_model->getReportBarang($v_start_date,$v_end_date,$kdcustomer);
				}
            	$data['track'] = $mylib->print_track();
                $this->load->view('report/salesinvoice/views', $data);
            }
        } 
        else 
        {
        	if($v_pilihan=="transaksi"){
				$data['hasil'] = $this->report_sales_invoice_model->getReport($v_start_date,$v_end_date,$kdcustomer);
				$this->load->view("report/salesinvoice/tampil", $data);
			}else{
				$data['hasil'] = $this->report_sales_invoice_model->getReportBarang($v_start_date,$v_end_date,$kdcustomer);
				$this->load->view("report/salesinvoice/tampil_barang", $data);
			}
        }
	}
	
}

?>