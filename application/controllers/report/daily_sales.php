<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class daily_sales extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/daily_sales_model');
    }

    function index(){
     	//$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['jenis'] = 'A';
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/daily_sales/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->daily_sales_model->aplikasi();
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		$jenis = $this->input->post("jenis");
		
		$data['jenis'] = $jenis;
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$data['kassa'] = $this->daily_sales_model->getkassa();
		$data['ticket'] = $this->daily_sales_model->gettiket($tgldari,$tglsampai, $jenis);
		$data['voucher'] = $this->daily_sales_model->getvoucher($tgldari,$tglsampai, $jenis);
		$data['salesrec'] = $this->daily_sales_model->getsales($tgldari,$tglsampai, $jenis);
		$data['compliment'] = $this->daily_sales_model->getcompliment($tgldari,$tglsampai, $jenis);
		
		if($jenis!='N'){
			$data['komisi'] = $this->daily_sales_model->getkomisi($tgldari,$tglsampai, $jenis);
		}
		
		if($jenis=='A' ){
			
			$data['distribusikopi'] = $this->daily_sales_model->getdistribusikopi($tgldari,$tglsampai);
			$data['distribusioh'] = $this->daily_sales_model->getdistribusiOH($tgldari,$tglsampai);
			$data['distribusivci'] = $this->daily_sales_model->getdistribusivci($tgldari,$tglsampai);	
		}
		
		$data['listemail'] = $this->daily_sales_model->getlistemail();
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Laporan Penjualan Harian';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/daily_sales/views', $data);
		}
		else
		{
			$this->load->view("report/daily_sales/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail()
	{
		$mylib = new globallib();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		$data['store']		= $this->daily_sales_model->aplikasi();
		$data['excel'] = '';
		$data['print'] = '';
		$data['jenis'] = 'A';
		$data['v_date_from'] = $awalbulan;
		$data['v_date_to'] = date('d-m-Y');
		$judul = array();
		$jenis = 'A';
		
		$tgldari = $mylib->ubah_tanggal($awalbulan);
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['tampilkanDT'] = true;
		$data['kassa'] = $this->daily_sales_model->getkassa();
		$data['ticket'] = $this->daily_sales_model->gettiket($tgldari,$tglsampai, $jenis);
		$data['voucher'] = $this->daily_sales_model->getvoucher($tgldari,$tglsampai, $jenis);
		$data['salesrec'] = $this->daily_sales_model->getsales($tgldari,$tglsampai, $jenis);
		$data['compliment'] = $this->daily_sales_model->getcompliment($tgldari,$tglsampai, $jenis);
		
		if($jenis!='N'){
			$data['komisi'] = $this->daily_sales_model->getkomisi($tgldari,$tglsampai);
		}
		
		if($jenis=='A' ){
			
			$data['distribusikopi'] = $this->daily_sales_model->getdistribusikopi($tgldari,$tglsampai);
			$data['distribusioh'] = $this->daily_sales_model->getdistribusiOH($tgldari,$tglsampai);
			$data['distribusivci'] = $this->daily_sales_model->getdistribusivci($tgldari,$tglsampai);	
		}
		$data['listemail'] = $this->daily_sales_model->getlistemail();
		
		$data['kirim_email']='Y';
				
		$data['judul'] = 'Laporan Penjualan Harian';
		
		$this->load->view("report/daily_sales/reportRT", $data);
	}
	
	function voucher($tgl,$bulan,$tahun){
		$vtgl = $tgl.'-'.$bulan.'-'.$tahun;
		$data['voucher'] = $this->daily_sales_model->voucher($vtgl);
		
		$this->load->view("report/daily_sales/voucher", $data);
	}
}
?>