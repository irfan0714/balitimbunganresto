<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_rencana_bayar extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_rencana_bayarmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkan'] = false;
			$data['v_date'] = date('d-m-Y');
			$data['v_date2'] = date('d-m-Y');
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_rencana_bayar/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		
		$v_date = $this->input->post("v_date");
		$v_date2 = $this->input->post("v_date2");
		$excel = $this->input->post("excel");
		$data['excel'] = $excel;
		$data['tampilkan'] = true;
		$data['v_date'] = $v_date;
		$data['v_date2'] = $v_date2;
		$tgl = $mylib->ubah_tanggal($v_date);
		$tgl2 = $mylib->ubah_tanggal($v_date2);
		
		$data['hasil'] = $this->report_rencana_bayarmodel->getReport($tgl, $tgl2);
		$data['judul'] = 'Report Rencana Bayar';
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_rencana_bayar/views', $data);
		}
		else
		{
			$this->load->view('report/report_rencana_bayar/reportdtl', $data);
		}
	}
}
?>