<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_penggunaan_diskon extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_penggunaan_diskon_model', 'MyQuery');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            $tanggal = $this->MyQuery->getDate();
            $bulan = date('m');
            $tahun = date('Y');
            $data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            $data['v_pilihan'] = "";
			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['excel'] = "";
            $data['print'] = "";
            $data['v_tipe_cari']='karyawan';
            
            $data['tmp_nama_kary']='1';
            $data['tmp_jns_mem']='0';
            $data['tmp_nama_mem']='0';
            $data['tmp_jns_cards']='0';
            
            $data['members'] = $this->MyQuery->getMembers();
            $data['namamembers'] = $this->MyQuery->getNamaMembers();  
            $data['employee'] = $this->MyQuery->getEmployee(); 
            $data['cards'] = $this->MyQuery->getCards();        
            $data['tampilkanDT'] = false;
            $data['track'] = $mylib->print_track();
            $this->load->view('report/penggunaan_diskon/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		
		//echo "<pre>";print_r($_POST);echo "</pre>";die;

		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$v_start_date = $this->input->post("v_start_date");
        $v_end_date = $this->input->post("v_end_date");
        $v_pilihan = $this->input->post("v_tipe_cari");
        $v_employee = $this->input->post("v_employee");
        $v_jenis_members = $this->input->post("v_jenis_members");
        $v_nama_members = $this->input->post("v_nama_members");
        $v_cards = $this->input->post("v_cards");
        $excel = $this->input->post("excel");
        $print = $this->input->post("print");
                
        $data['v_pilihan'] = $v_pilihan;
		$data['v_start_date'] = $v_start_date;
        $data['v_end_date'] = $v_end_date;
        $data['v_tipe_cari'] = $v_pilihan;
        $data['v_employee'] = $v_employee;
        
        $data['v_jenis_members'] = $v_jenis_members;
        $data['v_nama_members'] = $v_nama_members;
        $data['v_cards'] = $v_cards;
        
        $data['excel'] = $excel;
        $data['print'] = $print;
        
        if($v_pilihan=="karyawan"){
			$data['tmp_nama_kary']='1';
            $data['tmp_jns_mem']='0';
            $data['tmp_nama_mem']='0';
            $data['tmp_jns_cards']='0';
		}else if($v_pilihan=="members"){
			$data['tmp_nama_kary']='0';
            $data['tmp_jns_mem']='1';
            $data['tmp_nama_mem']='1';
            $data['tmp_jns_cards']='0';
		}else if($v_pilihan=="cards"){
			$data['tmp_nama_kary']='0';
            $data['tmp_jns_mem']='0';
            $data['tmp_nama_mem']='0';
            $data['tmp_jns_cards']='1';
		}
        
        $data['members'] = $this->MyQuery->getMembers();
        $data['namamembers'] = $this->MyQuery->getNamaMembers();  
        $data['employee'] = $this->MyQuery->getEmployee(); 
        $data['cards'] = $this->MyQuery->getCards(); 
        
        $data['judul'] = "Reprot Penggunaan Diskon $v_start_date s/d $v_end_date";
        
        $v_start_date = $mylib->ubah_tanggal($v_start_date);
        $v_end_date = $mylib->ubah_tanggal($v_end_date);
        
        $data['tampilkanDT'] = true;
  
        $data['hasil'] = $this->MyQuery->getReport($v_pilihan,$v_tipe_cari,$v_start_date,$v_end_date,$v_employee,$v_jenis_members,$v_nama_members,$v_cards);
  		//echo "<pre>";print_r($data['hasil']);echo "</pre>";die;
        
		if ($excel == "") 
        {
            if ($print == "print") 
            {
                $data['fileName'] = 'harian.sss';
            } 
            else 
            {
                $data['track'] = $mylib->print_track();
                $this->load->view('report/penggunaan_diskon/views', $data);
            }
        } 
        else 
        {
			if($v_pilihan=="karyawan"){
                $this->load->view("report/penggunaan_diskon/tampil_diskon_karyawan", $data);
			}
			elseif($v_pilihan=="members"){
                $this->load->view("report/penggunaan_diskon/tampil_diskon_members", $data);
			}
			elseif($v_pilihan=="cards"){
                $this->load->view("report/penggunaan_diskon/tampil_diskon_cards", $data);
			}
        }
	}
	
	
}

?>