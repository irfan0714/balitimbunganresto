<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_stock_outlet extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_stock_outlet_model');
    }

    function index(){
     	//$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/report_stock_outlet/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$v_date = $this->input->post("tanggal");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date;
		$judul = array();
		
		$tgl = $mylib->ubah_tanggal($v_date);
		$data['tampilkanDT'] = true;
		
		$tglsekarang = $v_date;
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		$awalbulan = $tahun.'-'.$bulan.'-01';
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		//cek penjualan bebek kemarin
		$data_penjualan 		= $this->report_stock_outlet_model->getPenjualan();
		$data_safety_stock 		= $this->report_stock_outlet_model->getSafetyStock('28');
		$data_sa_bebek			= $this->report_stock_outlet_model->getSaldoAwalStockBebek($bulan,'28');
		$data_sa_bumbu			= $this->report_stock_outlet_model->getSaldoAwalStockBumbu($bulan,'28');
		$data_mutasi_bebek		= $this->report_stock_outlet_model->getMutasiBebek('28',$awalbulan,$tglsampai);
		$data_mutasi_jual_bebek	= $this->report_stock_outlet_model->getMutasiJualBebek('25',$awalbulan,$tglsampai);
		$data_mutasi_bumbu		= $this->report_stock_outlet_model->getMutasiBumbu('28',$awalbulan,$tglsampai);
		
		foreach($data_penjualan AS $val){
			
			    //dapetin menu di BEO
                $data_beo 	= $this->report_stock_outlet_model->getDataBEO($val['PCode']);
                $arr_data["beo"][$val["PCode"]] = $data_beo->Qty;
        
		}
		
		foreach($data_penjualan AS $vals){
			
			    //dapetin Resep Bebek
                $data_bebek	= $this->report_stock_outlet_model->getResepBebek($vals['PCode']);
                $arr_data["bebek"][$vals["PCode"]] = $data_bebek->Qty;
        
		}
		
		$jml_bebek_keluar = 0;
		foreach($data_mutasi_jual_bebek AS $vale){
			
			    //dapetin Resep Bebek
                $data_jual_bebek	= $this->report_stock_outlet_model->getResepBebek($vale['KodeBarang']);
                $jml_bebek_keluar+= $vale['Qty']*$data_jual_bebek->Qty;
        
		}
		
		foreach($data_penjualan AS $valb){
			
			    //dapetin Resep Bebek
                $data_bumbu	= $this->report_stock_outlet_model->getBumbuBebek($valb['PCode']);
                $arr_data["bumbu"][$vals["PCode"]] = $data_bumbu->Qty;
        
		}
		
		$jml_bumbu_keluar = 0;
		foreach($data_mutasi_jual_bebek AS $vala){
			
			    //dapetin Resep Bebek
                $data_jual_bumbu	= $this->report_stock_outlet_model->getBumbuBebek($vala['KodeBarang']);
                $jml_bumbu_keluar+= $vala['Qty']*$data_jual_bumbu->Qty;
        
		}
				
		$in_bebek=0;
		$out_bebek=0;
		foreach($data_mutasi_bebek AS $valz){
			
			  $Qty_bebek = $valz['Qty'];
			  $Jns_bebek = $valz['Jenis'];
			  if($Jns_bebek=="I"){
			  	$in_bebek+=$Qty_bebek;
			  }else{
			  	$out_bebek+=$Qty_bebek;
			  }  
        
		}
		$total_stock_bebek = ($data_sa_bebek->saldo_awal+$in_bebek)-$out_bebek;
		
		$in_bumbu=0;
		$out_bumbu=0;
		foreach($data_mutasi_bumbu AS $valk){
			
			  $Qty_bumbu = $valk['Qty'];
			  $Jns_bumbu = $valk['Jenis'];
			  if($Jns_bumbu=="I"){
			  	$in_bumbu+=$Qty_bumbu;
			  }else{
			  	$out_bumbu+=$Qty_bumbu;
			  }  
        
		}
		$total_stock_bumbu= ($data_sa_bumbu->saldo_awal+$in_bumbu)-$out_bumbu;
		
		$data['datanya'] = $data_penjualan;
		$data['cek_safety_stock'] = $data_safety_stock;
		$data['stock_bebek_di_gudang_bts'] = $total_stock_bebek - $jml_bebek_keluar;
		$data['stock_bumbu_di_gudang_bts'] = $total_stock_bumbu - $jml_bumbu_keluar;
		$data['saldo_awal_bumbu'] = $data_sa_bumbu->saldo_awal;
		$data['cek_beo'] = $arr_data["beo"];
		$data['cek_bebek'] = $arr_data["bebek"];
		$data['cek_bumbu'] = $arr_data["bumbu"];
		
		$data['listemail'] = $this->report_stock_outlet_model->getlistemail();
		
		$data['kirim_email']='';
				
		$data['judul'] = 'Laporan Stock Harian Outlet';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_stock_outlet/views', $data);
		}
		else
		{
			$this->load->view("report/report_stock_outlet/reportRT", $data);
			$data['excel'] = "";
		}
	}
	
	function mail()
	{
		$mylib = new globallib();
		$v_date = date('d-m-Y');
		
		
		$judul = array();
		$tgl = 
		$data['tampilkanDT'] = true;
		
		$tglsekarang = $v_date;
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		
		$awalbulan = $tahun.'-'.$bulan.'-01';
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['v_date_from'] = $awalbulan;
		$data['v_date_to'] = $tglsampai;
		//cek penjualan bebek kemarin
		$data_penjualan 	= $this->report_stock_outlet_model->getPenjualan();
		$data_safety_stock 	= $this->report_stock_outlet_model->getSafetyStock('28');
		$data_sa_bebek		= $this->report_stock_outlet_model->getSaldoAwalStockBebek($bulan,'28');
		$data_sa_bumbu		= $this->report_stock_outlet_model->getSaldoAwalStockBumbu($bulan,'28');
		$data_mutasi_bebek	= $this->report_stock_outlet_model->getMutasiBebek('28',$awalbulan,$tglsampai);
		$data_mutasi_bumbu	= $this->report_stock_outlet_model->getMutasiBumbu('28',$awalbulan,$tglsampai);
		
		foreach($data_penjualan AS $val){
			
			    //dapetin menu di BEO
                $data_beo 	= $this->report_stock_outlet_model->getDataBEO($val['PCode']);
                $arr_data["beo"][$val["PCode"]] = $data_beo->Qty;
        
		}
		
		foreach($data_penjualan AS $vals){
			
			    //dapetin Resep Bebek
                $data_bebek	= $this->report_stock_outlet_model->getResepBebek($vals['PCode']);
                $arr_data["bebek"][$vals["PCode"]] = $data_bebek->Qty;
        
		}
		
		foreach($data_penjualan AS $valb){
			
			    //dapetin Resep Bebek
                $data_bumbu	= $this->report_stock_outlet_model->getBumbuBebek($valb['PCode']);
                $arr_data["bumbu"][$vals["PCode"]] = $data_bumbu->Qty;
        
		}
				
		$in_bebek=0;
		$out_bebek=0;
		foreach($data_mutasi_bebek AS $valz){
			
			  $Qty_bebek = $valz['Qty'];
			  $Jns_bebek = $valz['Jenis'];
			  if($Jns_bebek=="I"){
			  	$in_bebek+=$Qty_bebek;
			  }else{
			  	$out_bebek+=$Qty_bebek;
			  }  
        
		}
		$total_stock_bebek = ($data_sa_bebek->saldo_awal+$in_bebek)-$out_bebek;
		
		$in_bumbu=0;
		$out_bumbu=0;
		foreach($data_mutasi_bumbu AS $valk){
			
			  $Qty_bumbu = $valk['Qty'];
			  $Jns_bumbu = $valk['Jenis'];
			  if($Jns_bumbu=="I"){
			  	$in_bumbu+=$Qty_bumbu;
			  }else{
			  	$out_bumbu+=$Qty_bumbu;
			  }  
        
		}
		$total_stock_bumbu= ($data_sa_bumbu->saldo_awal+$in_bumbu)-$out_bumbu;
		
		$data['datanya'] = $data_penjualan;
		$data['cek_safety_stock'] = $data_safety_stock;
		$data['stock_bebek_di_gudang_bts'] = $total_stock_bebek;
		$data['stock_bumbu_di_gudang_bts'] = $total_stock_bumbu;
		$data['saldo_awal_bumbu'] = $data_sa_bumbu->saldo_awal;
		$data['cek_beo'] = $arr_data["beo"];
		$data['cek_bebek'] = $arr_data["bebek"];
		$data['cek_bumbu'] = $arr_data["bumbu"];
		$data['listemail'] = $this->report_stock_outlet_model->getlistemail();
				
		$data['judul'] = 'Laporan Stock Harian Outlet';
		
		$data['kirim_email']='Y';
		
		$this->load->view("report/report_stock_outlet/reportRT", $data);
	}
	
}
?>