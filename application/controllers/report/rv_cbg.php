<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Rv_cbg extends authcontroller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_finance_model');
    }
    
    function index()
	{
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if(!empty($_POST)){
//                    print_r($_POST);
//                }
		if($sign == "Y")
		{	
			$data['aa']             = "checked";
			$data['bb']		= "";
			$data['cc']		= "";
			$data['dd']		= "";
			$data['display1']	= "display:none";
			$data['display2']	= "display:none";
			$data['display3']	= "display:none";
			$data['display4']	= "display:none";
			$data['pilihan']	= $this->input->post('pilihan');
			
			$pilihan= $this->input->post('pilihan');
			$no1	= $this->input->post('no1');
			$no2	= $this->input->post('no2');
			$tgl_1	= $this->input->post('tgl_1');
			$tgl_2	= $this->input->post('tgl_2');
			$kode1	= $this->input->post('kode1');
			$kode2	= $this->input->post('kode2');
			$jenis	= $this->input->post('jenis');
			$par	= $this->input->post('par');
                        $data['tgl_1']= $tgl_1;
                        $data['tgl_2']= $tgl_2;

			if(!empty($no1))
                        {
					$NoDokumen = "and NoDokumen between '$no1' and '$no2'";
                        }else{
					$NoDokumen = "";
                        }

                        
			
			if(!empty($tgl_1) and !empty($tgl_2))
				{
					$tgl1	= $mylib->ubah_tanggal($tgl_1);
					$tgl2	= $mylib->ubah_tanggal($tgl_2);
					$Tgltrans = "and TglDokumen between '$tgl1' and '$tgl2'";
				}	
			elseif(!empty($tgl_1) and empty($tgl_2))
				{
					$tgl1	= $mylib->ubah_tanggal($tgl_1);
					$Tgltrans = "and a.TglDokumen>='$tgl1'";
				}
			elseif(empty($tgl_1) and !empty($tgl_2))
				{
					$tgl2	= $mylib->ubah_tanggal($tgl_2);
					$Tgltrans = "and a.TglDokumen <='$tgl2'";	
				}
			else{
					$Tgltrans = "";	
				}	
				
			if(!empty($pilihan))
				{
					if($pilihan=="detail")
					 	{
				 			$data['temp_so_last'] 	= "";	
							$data['temp_so1'] 	= "";
							$data['temp_so2'] 	= "";
							$data['temp_so3'] 	= "";
							$data['temp_so4'] 	= "";
							$data['temp_so5'] 	= "";
							$data['temp_so6'] 	= "";
							$data['temp_so7'] 	= "";
							$data['temp_so8'] 	= "";
							$data['temp_so9'] 	= "";
							$data['temp_so11'] 	= "";
							$data['total_netto'] 	= "";
							$data['aa']		= "checked";
							$data['bb']		= "";
							$data['cc']		= "";
							$data['dd']		= "";
							$data['display1']	= "";
							$data['display2']	= "display:none";
							$data['display3']	= "display:none";
							$data['display4']	= "display:none";
							$data['detail']		= $this->report_finance_model->detailRV($NoDokumen,$Tgltrans);
						
							//$this->load->view('transaksi/report/viewdetail',$data);
						}
					elseif($pilihan=="detailRekening")
					 	{
				 			$data['temp_so_last'] 	= "";	
							$data['temp_so1'] 	= "";
							$data['temp_so2'] 	= "";
							$data['temp_so3'] 	= "";
							$data['temp_so4'] 	= "";
							$data['temp_so5'] 	= "";
							$data['temp_so6'] 	= "";
							$data['temp_so7'] 	= "";
							$data['temp_so8'] 	= "";
							$data['temp_so9'] 	= "";
							$data['temp_so11'] 	= "";
							$data['total_netto'] 	= "";
							$data['aa']		= "";
							$data['bb']		= "checked";
							$data['cc']		= "";
							$data['dd']		= "";
							$data['display1']	= "display:none";
							$data['display2']	= "display:none";
							$data['display3']	= "";
							$data['display4']	= "display:none";
							$data['detailRk']		= $this->report_finance_model->detailRekeningRV($NoDokumen,$Tgltrans);
						
							//$this->load->view('transaksi/report/viewdetail',$data);
						}
					elseif($pilihan=="RekapTRX")
						{
                                                        
                                                        $data['total_trans'] 	= "";
							$data['aa']		= "";
							$data['bb']		= "";
							$data['cc']		= "checked";
							$data['dd']		= "";
							$data['display1']	= "display:none";
							$data['display2']	= "";
							$data['display3']	= "display:none";
							$data['display4']	= "display:none";
							$data['transaksi']	= $this->report_finance_model->transaksiRV($NoDokumen,$Tgltrans);
//		print_r($data['transaksi']);
							//$this->load->view('transaksi/report/viewtransaksi',$data);
							
						}	
					elseif($pilihan=="RekapRekening")
						{
							$data['total_barang'] 	= "";
							$data['aa']		= "";
							$data['bb']		= "";
							$data['cc']		= "";
							$data['dd']		= "checked";
							$data['display1']	= "display:none";
							$data['display2']	= "display:none";
							$data['display3']	= "display:none";
							$data['display4']	= "";
							$data['rekening']	= $this->report_finance_model->RekapRekeningRV($NoDokumen,$Tgltrans);
					
							//$this->load->view('transaksi/report/viewbarang',$data);
						}
                                                
				}
			if(empty($par)){
                            $data['excel']="";
                        }else{
                            $data['excel']=$par;
                        }
                        $data['judul'] = "Report Receipt Voucher";
			$this->load->view('report/pv/viewreport',$data);
	    }
		else
			{
				$this->load->view('denied');
			}
    }
    
	
}
?>