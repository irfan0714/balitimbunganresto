<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class alljurnal extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/alljurnalmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$tanggal = $this->alljurnalmodel->getDate();
			list($tahun, $bulan, $tglaktif)  = explode('-',$tanggal);
			
			$data['listrekening'] = $this->alljurnalmodel->getRekening();
			$arrkodejurnal = array('PV','RV','PI','PIM','R','SI','RJ','RB','RUM','DL','PL','LL','ADJ','IS');
			$arrasockodejurnal = array();
			foreach ($arrkodejurnal as $key => $value) {
				$arrasockodejurnal[] = array('KodeJurnal'=>$value) ; 	
			} 
			$data['listkodejurnal'] = $arrasockodejurnal;
			$data['listjenis'] = Array ( Array ( 'Jenis' => '1', 'NamaJenis' => 'Umum' ), 
                                           Array ( 'Jenis' => '2', 'NamaJenis' => 'Adjustment' ));
			$data['kdrekening'] = "";
			$data['kdjurnal'] = "";
			$data['jenis'] = "";
			
			$date = $tahun."-".$bulan."-1";
            $maxtgl = date("t", strtotime($date));
			
			$data['tgl1'] = '01'.'-'.$bulan.'-'.$tahun;
			$data['tgl2'] = $maxtgl.'-'.$bulan.'-'.$tahun;
			$data['cekrh'] = "checked='checked'";
			$data['cekrt'] = "";
			$data['cekdt'] = "";
			$data['cekyy'] = "";
			$data['cektt'] = "checked='checked'";
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanRH'] = false;
			$data['tampilkanRT'] = false;
			$data['tampilkanDT'] = false;
			$data['track'] = $mylib->print_track();
			$this->load->view('report/alljurnal/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$user = $this->session->userdata('username');
		$tgl1 = $this->input->post("tgl1");
		$tgl2 = $this->input->post("tgl2");
		$kdrekening = $this->input->post("kdrekening");
		$kdjurnal = $this->input->post("kdjurnal");
		$jenis = $this->input->post("jenis");
		$opt = $this->input->post("opt");
		$opt1 = $this->input->post("opt1");
		$excel = $this->input->post("excel");
		$print = $this->input->post("print");
		$data['kdrekening'] = $kdrekening;
		$data['kdjurnal'] = $kdjurnal;
		$data['jenis'] = $jenis;
		$data['tgl1'] = $tgl1;
		$data['tgl2'] = $tgl2;
		$data['cekrh'] = "";
		$data['cekrt'] = "";
		$data['cekdt'] = "";
		$data['cekyy'] = "";
		$data['cektt'] = "";
		$data['excel'] = $excel;
		$data['print'] = $print;
		if($opt=="RH"){
			$data['cekrh'] = "checked='checked'";
		}
		if($opt=="RT"){
			$data['cekrt'] = "checked='checked'";
		}
		if($opt=="DT"){
			$data['cekdt'] = "checked='checked'";
		}
		if($opt1=="YY"){
			$data['cekyy'] = "checked='checked'";
			$orderby = "KdRekening,Tanggal,--NoReferensi";
		}
		if($opt1=="TT"){
			$data['cektt'] = "checked='checked'";
			$orderby = "Tanggal,--NoReferensi";
		}
		$data['tampilkanRH'] = false;
		$data['tampilkanRT'] = false;
		$data['tampilkanDT'] = false;
		$judul = array();
		$judul[] = "Tanggal = $tgl1 s/d $tgl2";
		$tgl1 = $mylib->ubah_tanggal($tgl1);
		$tgl2 = $mylib->ubah_tanggal($tgl2);
		$wheredtl = "h.TglTransaksi between '$tgl1' and '$tgl2'";
		$wheredtl1 = "h.TglTransaksi between '$tgl1' and '$tgl2'";
        $wheredtl2 = "and a.TglTransaksi between '$tgl1' and '$tgl2'";
		if(!empty($kdrekening)&&$opt<>"RT"){ $wheredtl.=" and r.KdRekening='$kdrekening'"; $judul[] = "r.Rekening = $kdrekening"; }
		$where1 = "";
        if(!empty($jenis)){ $where1.=" and h.JenisJurnal='$jenis'"; $judul[] = "Jenis = $jenis"; }

        if($kdjurnal != ''  &&  $opt == "DT"){ $wheredtl2 .=" and a.KodeJurnal='$kdjurnal' "; }

		if($opt=="RH")
		{
			$sql1 = "SELECT d.KdRekening,NamaRekening,SUM(Debit) AS Debit,SUM(Kredit) AS Kredit, h.JenisJurnal 
						FROM jurnaldetail d INNER JOIN jurnalheader h ON h.`KdDepartemen`=d.`KdDepartemen` AND h.`NoReferensi`=d.`NoReferensi` AND h.`Tahun`=d.`Tahun` AND h.`Bulan`=d.`Bulan`
						INNER JOIN rekening r ON d.`KdRekening`=r.`KdRekening`
						WHERE $wheredtl $where1
						GROUP BY d.`KdRekening` order by d.KdRekening";
			$data['tampilkanRH'] = true;
			if($user=='jamal3108' || $user=='yuri0717'){
				echo $sql1;
			}
		}
		
		
		if($opt=="RT")
		{
			$sql1 ="Select h.NoReferensi,h.TglTransaksi as Tanggal,h.NoTransaksi,h.Keterangan,h.KodeJurnal,h.JenisJurnal,sum(Debit) as Debit,sum(Kredit) as Kredit,
						CONCAT(COALESCE(p.Penerima,''),COALESCE(rc.TerimaDari,'')) AS KepadaDari
						FROM jurnaldetail d INNER JOIN jurnalheader h ON h.`KdDepartemen`=d.`KdDepartemen` AND h.`NoReferensi`=d.`NoReferensi` AND h.`Tahun`=d.`Tahun` AND h.`Bulan`=d.`Bulan`
						INNER JOIN rekening r ON d.`KdRekening`=r.`KdRekening`
						left join trans_payment_header p on h.NoTransaksi=p.NoDokumen
						left join trans_receipt_header rc on h.NoTransaksi=rc.NoDokumen
						WHERE $wheredtl1 $where1
						GROUP BY h.`noreferensi` order by h.noreferensi";
			$data['tampilkanRT'] = true;
			if($user=='jamal3108' || $user=='yuri0717'){
				echo $sql1;
			}
		}
		if($opt=="DT")
		{
			if($this->session->userdata('userid') == 'mechael0101') {
			}else{
				$orderBy = "order by Tanggal,a.Noreferensi";
			}
				$orderBy = "order by a.kdsubdivisi,a.KdRekening";
            $sql1 ="
                    SELECT
					  a.TglTransaksi as Tanggal,
					  a.JenisJurnal,
					  a.KodeJurnal,
					  b.NoTransaksi,
					  b.Keterangan as KetHeader,
					  a.KeteranganDetail as Keterangan,
					  IF(u.Id IS NULL, a.AddUserTransaksi,u.username) AS UserTrans,
					  IF(emp.employee_name IS NULL, emp2.employee_name, emp.employee_name) AS employee_name,
					  CONCAT(COALESCE(p.Penerima,''),COALESCE(r.TerimaDari,'')) AS KepadaDari,
					  a.KdDepartemen, a.NoReferensi, a.TglTransaksi, c.NamaRekening, a.Debit, a.Kredit, a.CostCenter, a.KdRekening, s.NamaSubDivisi
					FROM
					  jurnaldetail a inner join jurnalheader b ON a.KdDepartemen=b.KdDepartemen 
					  AND a.NoReferensi=b.NoReferensi AND a.Tahun=b.Tahun AND a.Bulan=b.Bulan
					  inner join rekening c on a.KdRekening = c.KdRekening
					  inner join subdivisi s on a.KdSubDivisi = s.KdSubDivisi 
					  left join trans_payment_header p on b.NoTransaksi=p.Nodokumen
					  left join trans_receipt_header r on b.NoTransaksi=r.Nodokumen
					  LEFT JOIN `user` u ON a.AddUserTransaksi = u.Id
					  LEFT JOIN employee emp ON a.AddUserTransaksi = emp.username
					  LEFT JOIN employee emp2 ON u.username = emp2.username
					WHERE 
					 1
					 $wheredtl2
					 $orderBy";
					  // order by Tanggal,a.Noreferensi";
//order by ".$orderby;
			$data['tampilkanDT'] = true;
			if($user=='jamal3108' || $user=='yuri0717'){
				echo $sql1;
			}
		}

		$data['hasil'] = $this->alljurnalmodel->getReport($sql1);
		$data['judul'] = $judul;
		if($excel=="")
		{
		    if($print=="print")
			{
			   $data['fileName'] = 'harian.sss';
			   $this->load->view('report/alljurnal/reportRHprint', $data);
			}
			else
			{
			$data['track'] = $mylib->print_track();
			$data['listrekening'] = $this->alljurnalmodel->getRekening();
			$arrkodejurnal = array('PV','RV','PI','PIM','R','SI','RJ','RB','RUM','DL','PL','LL','ADJ','IS');
			$arrasockodejurnal = array();
			foreach ($arrkodejurnal as $key => $value) {
				$arrasockodejurnal[] = array('KodeJurnal'=>$value) ; 	
			} 
			$data['listkodejurnal'] = $arrasockodejurnal;
			$data['listjenis'] = Array ( Array ( 'Jenis' => '1', 'NamaJenis' => 'Umum' ), 
                                           Array ( 'Jenis' => '2', 'NamaJenis' => 'Adjustment' ));
			$this->load->view('report/alljurnal/views', $data);
			}
		}
		else
		{
			if($opt=="RH")
			{
				$this->load->view('report/alljurnal/reportRH', $data);
			}
			if($opt=="RT")
			{
				$this->load->view("report/alljurnal/reportRT", $data);
			}
			if($opt=="DT")
			{
				$this->load->view("report/alljurnal/reportDT", $data);
			}
		}
	}
}
?>