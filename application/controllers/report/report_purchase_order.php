<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_purchase_order extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('globalmodel');
        $this->load->model('report/report_purchase_order_model', 'po_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	unset($arr_data);
        	
            $bulan = date('m');
            $tahun = $this->session->userdata('tahunaktif');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
			$user = $this->session->userdata('username');
            $gudangadmin = $this->globalmodel->getGudangAdmin($user);
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
            
            $arr_data['gudang_admin'] = array();
            foreach($gudangadmin as $key)
			{
				$arr_data["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
            
            $where_gudangadmin = $mylib->where_array($arr_data['gudang_admin'], "KdGudang", "in");	
            
            $data['mgudang'] = $this->po_model->getArrayGudang($where_gudangadmin);
            $data['msupplier'] = $this->po_model->getArraySupplier();
            $data['mcurrency'] = $this->po_model->getArrayCurrency();
           
        	$data['v_type'] = "";
        	$data['v_gudang'] = "";
        	$data['v_supplier'] = "";
        	$data['v_currency'] = "";
        	$data['v_pono'] = "";
        	$data['v_prno'] = "";
        	$data['v_status'] = "";
        	$data['v_orderby'] = "";
        	$data['btn_excel'] = "";
        	$data['flag'] = "";
            
            $data['analisa'] = FALSE;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_purchase_order/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		//print_r($_POST);die;

		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
        
		$data['v_type'] = $this->input->post("v_type");
        $data['v_start_date'] = $this->input->post("v_start_date");
        $data['v_end_date'] = $this->input->post("v_end_date");
        $data['v_gudang'] = $this->input->post("v_gudang");
        $data['v_supplier'] = $this->input->post("v_supplier");
        $data['v_currency'] = $this->input->post("v_currency");
        $data['v_pono'] = $this->input->post("v_pono");
        $data['v_prno'] = $this->input->post("v_prno");
        $data['v_status'] = $this->input->post("v_status");
        $data['v_orderby'] = $this->input->post("v_orderby");
        
        $data['flag'] = $this->input->post("flag");
        $data['base_url'] = $this->input->post("base_url");
        $data['btn_excel'] = $this->input->post("btn_excel");
        
        $data['judul'] = "Reprot ".$data['v_type']." purchse order ".$data['v_start_date']." s/d ".$data['v_end_date']."";
        
       $data['header'] = $this->po_model->getArrayHeader(
			$data['v_type'],
			$data['v_start_date'],
			$data['v_end_date'],
			$data['v_gudang'],
			$data['v_supplier'],
			$data['v_currency'],
			$data['v_pono'],
			$data['v_prno'],
			$data['v_status'],
			$data['v_orderby']
		);
		
		if($data['v_type']=="detail")
		{
			$arr_data["list_pono"] = array();

	        if(is_array($data['header']))
	        {
		        foreach($data['header'] as $val)
		        {
		        	$arr_data["list_pono"][$val["NoDokumen"]]=$val["NoDokumen"];
				}
			}
			
			$data['detail'] = "";
			$where_nodok = "";		
			if(count($arr_data["list_pono"])*1>0)
			{
				$where_nodok = $mylib->where_array($arr_data['list_pono'], "NoDokumen", "in");	
				
				$data['detail'] = $this->po_model->getArrayDetail($where_nodok);
			}
		}
		
		$user = $this->session->userdata('username');
        $gudangadmin = $this->globalmodel->getGudangAdmin($user);
            
		$arr_data['gudang_admin'] = array();
        foreach($gudangadmin as $key)
		{
			$arr_data["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
		}
        
        $where_gudangadmin = $mylib->where_array($arr_data['gudang_admin'], "KdGudang", "in");	
        
        $data['mgudang'] = $this->po_model->getArrayGudang($where_gudangadmin);
        $data['msupplier'] = $this->po_model->getArraySupplier();
        $data['mcurrency'] = $this->po_model->getArrayCurrency();
		
        $data['analisa'] = TRUE;
			
		$data['track'] = $mylib->print_track();
  
		$this->load->view('report/report_purchase_order/views', $data);
        
	}
}

?>