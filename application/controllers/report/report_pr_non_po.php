<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Report_pr_non_po extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('globalmodel');
        $this->load->model('report/report_pr_non_po_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	unset($arr_data);
        	
            $bulan = date('m');
            $tahun = date('Y');
            
        	$data["username"] = $this->session->userdata('username');
            $data["userlevel"] = $this->session->userdata('userlevel');
            
			$user = $this->session->userdata('username');
            
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
         
            $data['msupplier'] = $this->report_pr_non_po_model->getArraySupplier();
           
            $data['v_supplier'] = "";
        
        	$data['btn_excel'] = "";
        	$data['flag'] = "";
            
            $data['analisa'] = FALSE;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('report/report_pr_non_po/views', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		//print_r($_POST);die;

		$mylib = new globallib();
		
    	$data["username"] = $this->session->userdata('username');
        $data["userlevel"] = $this->session->userdata('userlevel');
       
        $data['v_start_date'] = $this->input->post("v_start_date");
        $data['v_end_date'] = $this->input->post("v_end_date");
        $data['v_supplier'] = $this->input->post("v_supplier");
        
        $data['flag'] = $this->input->post("flag");
        $data['base_url'] = $this->input->post("base_url");
        $data['btn_excel'] = $this->input->post("btn_excel");
        
        $data['judul'] = "Reprot purchase return tanpa purchse order ".$data['v_start_date']." s/d ".$data['v_end_date']."";
        
       $data['header'] = $this->report_pr_non_po_model->getArrayHeader(
			$data['v_start_date'],
			$data['v_end_date'],
			$data['v_supplier']
		);
				
		$user = $this->session->userdata('username');
		
        $data['msupplier'] = $this->report_pr_non_po_model->getArraySupplier();
		
        $data['analisa'] = TRUE;
			
		$data['track'] = $mylib->print_track();
  
		$this->load->view('report/report_pr_non_po/views', $data);
        
	}
}

?>