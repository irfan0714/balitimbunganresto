<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class report_pendapatan_kasir extends authcontroller {

    function __construct(){
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->library('report_lib');
        $this->load->model('report/report_pendapatan_kasirmodel');
    }

    function index(){
     	//$mylib = new globallib();
    	//$sign  = $mylib->getAllowList("all");
		//print_r($mylib);die;
		$sign='Y';
    	if($sign=="Y")
		{
			$data['v_date_from'] = date('d-m-Y');
			$data['v_date_to'] = date('d-m-Y');
			$data['excel'] = "";
			$data['print'] = "";
			$data['tampilkanDT'] = false;
			$data['track'] = '';//$mylib->print_track();
			$this->load->view('report/report_pendapatan_kasir/views', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		$mylib = new globallib();
		$data['store']		= $this->report_pendapatan_kasirmodel->aplikasi();
		$v_date_from = $this->input->post("v_date_from");
		$v_date_to = $this->input->post("v_date_to");
		$excel = $this->input->post("btn_excel");
		$print = $this->input->post("print");
		
		$data['excel'] = $excel;
		$data['print'] = $print;
		$data['v_date_from'] = $v_date_from;
		$data['v_date_to'] = $v_date_to;
		$judul = array();
		
		$tgldari = $mylib->ubah_tanggal($v_date_from);
		$tglsampai = $mylib->ubah_tanggal($v_date_to);
		
		$data['tampilkanDT'] = true;
		$data['data'] = $this->report_pendapatan_kasirmodel->getData($tgldari, $tglsampai);
				
		$data['judul'] = 'Laporan Pendapatan Kasir';
		
		if($excel=="")
		{
			$data['track'] = $mylib->print_track();
			$this->load->view('report/report_pendapatan_kasir/views', $data);
		}
		else
		{
			$this->load->view("report/report_pendapatan_kasir/reportRT", $data);
			$data['excel'] = "";
		}
	}
}
?>