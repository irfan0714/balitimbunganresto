<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Fixed_asset extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0); 
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('fixed_asset/fixed_asset_model');
    }

    function index() 
    {
        $mylib 		= new globallib();
        $sign 		= $mylib->getAllowList("all");
        
        $arr_search["search"] = array();
        
        if ($sign == "Y") {
        	
        	$id = $this->uri->segment(4);
        	$user = $this->session->userdata('username');
            
            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            $data['cari']="";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "fixed_asset", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]);   
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                    
                    $data['cari']=$data["search_keyword"];
                }
            }
            
        	// pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '100';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['total_rows'] = $this->fixed_asset_model->num_list_asset_row($arr_search["search"]);
            
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/fixed_asset/fixed_asset/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/fixed_asset/fixed_asset/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
           
        	$data['data'] = $this->fixed_asset_model->getListAsset($config['per_page'],$page, $arr_search["search"]);
        	
        	$data['track'] = $mylib->print_track();
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('fixed_asset/fixed_asset/fixed_asset_list',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'fixed_asset', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "fixed_asset",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/fixed_asset/fixed_asset/index/' . $query_id . '');
    }
    
    function add_new() 
    {
        $mylib 		= new globallib();
        $sign 		= $mylib->getAllowList("all");
        $user = $this->session->userdata('username');
        
        if ($sign == "Y") {
        	
        	$data['item_asset'] = $this->fixed_asset_model->getItemAsset();
        	$data['type_asset'] = $this->fixed_asset_model->getTypeAsset();
        	$data['lokasi'] 	= $this->fixed_asset_model->getLokasiFixedAsset();
        	$data['departemen'] = $this->fixed_asset_model->getDepartemenFixedAsset();
        	$data['employee'] 	= $this->fixed_asset_model->getEmployee();
        	$data['otorisasi'] 	= $this->fixed_asset_model->getOtorisasi($user);
        	
            $this->load->view('fixed_asset/fixed_asset/add_fixed_asset',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function add_new_other() 
    {
        $mylib 		= new globallib();
        $sign 		= $mylib->getAllowList("all");
        $user = $this->session->userdata('username');
        
        if ($sign == "Y") {
        	
        	$data['item_asset'] = $this->fixed_asset_model->getItemAsset();
        	$data['type_asset'] = $this->fixed_asset_model->getTypeAssetOtherAsset();
        	$data['lokasi'] 	= $this->fixed_asset_model->getLokasiFixedAsset();
        	$data['departemen'] = $this->fixed_asset_model->getDepartemenFixedAsset();
        	$data['employee'] 	= $this->fixed_asset_model->getEmployee();
        	$data['otorisasi'] 	= $this->fixed_asset_model->getOtorisasi($user);
        	
            $this->load->view('fixed_asset/fixed_asset/add_other_fixed_asset',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_asset($noasset) 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['data'] = $this->fixed_asset_model->getDataAsset($noasset);
            
            $data['item_asset'] = $this->fixed_asset_model->getItemAsset();
            $data['code_asset'] = $this->fixed_asset_model->getCodeItemAsset();
        	$data['type_asset'] = $this->fixed_asset_model->getTypeAsset();
        	$data['sub_type_asset'] = $this->fixed_asset_model->getSubTypeAsset();
        	$data['lokasi'] 	= $this->fixed_asset_model->getLokasiFixedAsset();
        	$data['departemen'] = $this->fixed_asset_model->getDepartemenFixedAsset();
        	$data['employee'] 	= $this->fixed_asset_model->getEmployee();
            $data['otorisasi'] 	= $this->fixed_asset_model->getOtorisasi($user);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('fixed_asset/fixed_asset/edit_fixed_asset', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_other_asset($noasset) 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['data'] = $this->fixed_asset_model->getDataAsset($noasset);
            
            $data['item_asset'] = $this->fixed_asset_model->getItemAsset();
            $data['code_asset'] = $this->fixed_asset_model->getCodeItemAsset();
        	$data['type_asset'] = $this->fixed_asset_model->getTypeAssetOtherAsset();
        	$data['sub_type_asset'] = $this->fixed_asset_model->getSubTypeAsset();
        	$data['lokasi'] 	= $this->fixed_asset_model->getLokasiFixedAsset();
        	$data['departemen'] = $this->fixed_asset_model->getDepartemenFixedAsset();
        	$data['employee'] 	= $this->fixed_asset_model->getEmployee();
            $data['otorisasi'] 	= $this->fixed_asset_model->getOtorisasi($user);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('fixed_asset/fixed_asset/edit_other_fixed_asset', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function save_data(){
		    //echo "<pre>";print_r($_POST);echo "</pre>";die();
		
		    $mylib 					= new globallib();
		    $user 					= $this->session->userdata('username');
		    $No_Asset				= $this->input->post('No_Asset');
		    $v_ItemAsset 			= $this->input->post('v_ItemAsset');
	        $v_CodeAsset 			= $this->input->post('v_CodeAsset');
	        $v_TypeAsset 			= $this->input->post('v_TypeAsset');
	        $v_SubTypeAsset 		= $this->input->post('v_SubTypeAsset');
	        $v_nama_asset 			= $this->input->post('v_nama_asset');
	        $v_brand 				= $this->input->post('v_brand');
	        $v_Volume 				= $this->input->post('v_Volume');
	        $v_Size 				= $this->input->post('v_Size');
	        $v_Qty 					= $this->input->post('v_Qty');
	        $v_Satuan 				= $this->input->post('v_Satuan');
	        $v_Qty_Detail_Set		= $this->input->post('v_Qty_Detail_Set');
	        $v_Detail_set 			= $this->input->post('v_Detail_set');
	        $v_Kondisi 				= $this->input->post('v_Kondisi');
	        $v_Keterangan 			= $this->input->post('v_Keterangan');
	        $filegambar 			= $this->input->post('filegambar');
	        $v_tgl_beli 			= $this->input->post('v_tgl_beli');
	        $v_Nominal 				= $this->input->post('v_Nominal');
	        $v_NoProposal 			= $this->input->post('v_NoProposal');
	        $v_Departemen_proposal 	= $this->input->post('v_Departemen_proposal');
	        $filegambar_pro 		= $this->input->post('filegambar_pro');
	        $v_MetodePenyusutan 	= $this->input->post('v_MetodePenyusutan');
	        $v_Lokasi 				= $this->input->post('v_Lokasi');
	        $v_Departemen 			= $this->input->post('v_Departemen');
	        $v_PenanggungJawab 		= $this->input->post('v_PenanggungJawab');
	        $v_Pegawai 				= $this->input->post('v_Pegawai');
	        $sts_photo 				= $this->input->post('sts_photo');
	        $sts_photo_pro			= $this->input->post('sts_photo_pro');
	        $other_asset 			= $this->input->post('other_asset');
	        $flag 					= $this->input->post('flag');
	        $otorisasi					= $this->input->post('otorisasi');
	        $tahun 					= substr(date('Y'),-2);
	        
		if($flag =="add"){
			
			$qty_looping = ($v_Qty*1)*($v_Qty_Detail_Set*1);
			
			//looping sesuai dengan jumlah unit yang di input
			//echo $v_Qty*$v_Qty_Detail_Set;die();
			for($x=1;$x<=$qty_looping;$x++){
			
			        $v_NoAsset = $this->GenerateNoAsset($v_TypeAsset,$v_SubTypeAsset,$tahun);
			            
			            if($v_PenanggungJawab=="umum"){
						   $v_Pegawai				="-";
						   $v_dept_tanggungjawab	=$v_Departemen;
						}else{
						   $v_Pegawai				=$v_Pegawai;
						   $v_dept_tanggungjawab	="-";
						}
						
						if($v_Satuan=="Set"){
						   $v_Satuan="Unit";
						   $v_Qty=$x;
						}
			   
						$data=array(
			        			'NoAsset' 		    	=> $v_NoAsset,
			        			'ItemAsset' 			=> $v_ItemAsset,
							    'CodeAsset' 			=> $v_CodeAsset,
							    'TypeAsset' 			=> $v_TypeAsset,
							    'SubTypeAsset' 			=> $v_SubTypeAsset,
							    'nama_asset' 			=> $v_nama_asset,
							    'brand' 				=> $v_brand,
							    'Volume' 				=> $v_Volume,
							    'Size' 					=> $v_Size,
							    'Qty' 					=> $v_Qty,
							    'Satuan' 				=> $v_Satuan,
							    'Kondisi' 				=> $v_Kondisi,
							    'Keterangan' 			=> $v_Keterangan,
							    'filegambar' 			=> $filegambar,
							    'tgl_beli' 				=> $mylib->ubah_tanggal($v_tgl_beli),
							    'Nominal' 				=> $v_Nominal,
							    'NoProposal' 			=> $v_NoProposal,
							    'Departemen_Proposal'	=> $v_Departemen_proposal,
							    'filegambar_Proposal'	=> $filegambar_pro,
							    'MetodePenyusutan' 		=> $v_MetodePenyusutan,
							    'Lokasi' 				=> $v_Lokasi,
							    'Departemen' 			=> $v_Departemen,
							    'PenanggungJawab' 		=> $v_PenanggungJawab,
							    'Pegawai' 				=> $v_Pegawai,
							    'Dept_tanggungjawab'	=> $v_dept_tanggungjawab,
							    'AddDate'				=> date('Y-m-d'),
							    'AddUser'				=> $user
			        			);
					
			        
			        $this->db->insert('master_fixed_asset',$data);
			        
			              $datax=array(
						        'NoAsset' 		    	=> $v_NoAsset,
							    'Departemen' 			=> $v_Departemen,
							    'Lokasi' 				=> $v_Lokasi,
							    'Qty' 					=> $v_Qty,
							    'Satuan' 				=> $v_Satuan,
							    'Keterangan' 			=> $v_Keterangan,
							    'pic' 					=> $v_Pegawai,
							    'tgl_mutasi'			=> date('Y-m-d'),
							    'AddDate'				=> date('Y-m-d'),
							    'AddUser'				=> $user
							  );
							  
					$this->db->insert('mutasi_fixed_asset',$datax);
			        
	        }
			
	        
		}else if($flag =="add_other"){
			
	        	$qty_looping = ($v_Qty*1)*($v_Qty_Detail_Set*1);
	   			for($x=1;$x<=$qty_looping;$x++){
	   			        $v_NoAsset = $this->GenerateNoAsset($v_TypeAsset,$v_SubTypeAsset,$tahun);
	   			
	   			        if($v_PenanggungJawab=="umum"){
						   $v_Pegawai				="-";
						   $v_dept_tanggungjawab	=$v_Departemen;
						}else{
						   $v_Pegawai				=$v_Pegawai;
						   $v_dept_tanggungjawab	="-";
						}
						
						if($v_Satuan=="Set"){
						   $v_Satuan="Unit";
						   $v_Qty=$x;
						}
						
							$data=array(
				        			'NoAsset' 		    => $v_NoAsset,
				        			'ItemOtherAsset' 	=> $v_ItemAsset,
								    'CodeAsset' 		=> $v_CodeAsset,
								    'TypeAsset' 		=> $v_TypeAsset,
								    'SubTypeAsset' 		=> $v_SubTypeAsset,
								    'nama_asset' 		=> $v_nama_asset,
								    'brand' 			=> $v_brand,
								    'Volume' 			=> $v_Volume,
								    'Size' 				=> $v_Size,
								    'Qty' 				=> $v_Qty,
								    'Satuan' 			=> $v_Satuan,
								    'Kondisi' 			=> $v_Kondisi,
								    'Keterangan' 		=> $v_Keterangan,
								    'filegambar' 		=> $filegambar,
								    'tgl_beli' 			=> $mylib->ubah_tanggal($v_tgl_beli),
								    'Nominal' 			=> $v_Nominal,
								    'NoProposal' 		=> $v_NoProposal,
								    'MetodePenyusutan' 	=> $v_MetodePenyusutan,
								    'Lokasi' 			=> $v_Lokasi,
								    'Departemen' 		=> $v_Departemen,
								    'PenanggungJawab' 	=> $v_PenanggungJawab,
								    'Pegawai' 			=> $v_Pegawai,
								    'Other_Asset'		=>'1',
								    'AddDate'			=> date('Y-m-d'),
								    'AddUser'			=> $user
				        			);
						
				        
				            $this->db->insert('master_fixed_asset',$data);
				        
				        
				            $datax=array(
						        'NoAsset' 		    	=> $v_NoAsset,
							    'Departemen' 			=> $v_Departemen,
							    'Lokasi' 				=> $v_Lokasi,
							    'Qty' 					=> $v_Qty,
							    'Satuan' 				=> $v_Satuan,
							    'Keterangan' 			=> $v_Keterangan,
							    'pic' 					=> $v_Pegawai,
							    'tgl_mutasi'			=> date('Y-m-d'),
							    'AddDate'				=> date('Y-m-d'),
							    'AddUser'				=> $user
							  );
							  
					       $this->db->insert('mutasi_fixed_asset',$datax);
	        
			}
			
		}else if($flag =="edit"){
			
			    if($v_PenanggungJawab=="umum"){
				   $v_Pegawai				="-";
				   $v_dept_tanggungjawab	=$v_Departemen;
				}else{
				   $v_Pegawai				=$v_Pegawai;
				   $v_dept_tanggungjawab	="-";
				}
			
			 if($filegambar!="" OR $filegambar_pro!=""){
			 	$datax=array(
	        			'ItemAsset' 			=> $v_ItemAsset,
					    'CodeAsset' 			=> $v_CodeAsset,
					    'TypeAsset' 			=> $v_TypeAsset,
					    'SubTypeAsset' 			=> $v_SubTypeAsset,
					    'nama_asset' 			=> $v_nama_asset,
					    'brand' 				=> $v_brand,
					    'Volume' 				=> $v_Volume,
					    'Size' 					=> $v_Size,
					    'Qty' 					=> $v_Qty,
					    'Satuan' 				=> $v_Satuan,
					    'Kondisi' 				=> $v_Kondisi,
					    'Keterangan' 			=> $v_Keterangan,
					    'filegambar' 			=> $filegambar,
					    'tgl_beli' 				=> $mylib->ubah_tanggal($v_tgl_beli),
					    'Nominal' 				=> $v_Nominal,
					    'NoProposal' 			=> $v_NoProposal,
					    'Departemen_Proposal'	=> $v_Departemen_proposal,
					    'filegambar_Proposal'	=> $filegambar_pro,
					    'MetodePenyusutan' 		=> $v_MetodePenyusutan,
					    'Lokasi' 				=> $v_Lokasi,
					    'Departemen' 			=> $v_Departemen,
					    'PenanggungJawab' 		=> $v_PenanggungJawab,
					    'Pegawai' 				=> $v_Pegawai,
					    'Dept_tanggungjawab'	=> $v_dept_tanggungjawab,
					    'EditDate'				=> date('Y-m-d'),
					    'EditUser'				=> $user
	        			);
			 }else{
			 	$datax=array(
	        			'ItemAsset' 			=> $v_ItemAsset,
					    'CodeAsset' 			=> $v_CodeAsset,
					    'TypeAsset' 			=> $v_TypeAsset,
					    'SubTypeAsset' 			=> $v_SubTypeAsset,
					    'nama_asset' 			=> $v_nama_asset,
					    'brand' 				=> $v_brand,
					    'Volume' 				=> $v_Volume,
					    'Size' 					=> $v_Size,
					    'Qty' 					=> $v_Qty,
					    'Satuan' 				=> $v_Satuan,
					    'Kondisi' 				=> $v_Kondisi,
					    'Keterangan' 			=> $v_Keterangan,
					    'tgl_beli' 				=> $mylib->ubah_tanggal($v_tgl_beli),
					    'Nominal' 				=> $v_Nominal,
					    'NoProposal' 			=> $v_NoProposal,
					    'Departemen_Proposal'	=> $v_Departemen_proposal,
					    'MetodePenyusutan' 		=> $v_MetodePenyusutan,
					    'Lokasi' 				=> $v_Lokasi,
					    'Departemen' 			=> $v_Departemen,
					    'PenanggungJawab' 		=> $v_PenanggungJawab,
					    'Pegawai' 				=> $v_Pegawai,
					    'Dept_tanggungjawab'	=> $v_dept_tanggungjawab,
					    'EditDate'				=> date('Y-m-d'),
					    'EditUser'				=> $user
	        			);
			 }
			 
	        			
	        $this->db->update('master_fixed_asset',$datax,array('NoAsset' => $No_Asset));
	        
	        if($otorisasi=="Y"){
			//update Nominal dan Status Accounting
				$like_noAsset = substr($No_Asset,0,9);
				$belinya = $mylib->ubah_tanggal($v_tgl_beli); 
				$qry = "UPDATE
								  master_fixed_asset a
								SET
								  a.`Nominal` = '$v_Nominal',
								  a.`tgl_beli`='$belinya',
								  a.`Sts_accounting`='Y'
								WHERE a.`NoAsset` LIKE '%$like_noAsset%'
								  AND a.`Departemen` = '$v_Departemen'
								  AND a.`Lokasi` = '$v_Lokasi'
								  AND a.`nama_asset`='$v_nama_asset';";
				//echo $qry;die();
				$this->db->query($qry);		   	
			}
	        
		}else if($flag =="edit_other"){
			
			 if($filegambar!=""){
			 	$datax=array(
	        			'ItemOtherAsset' 		=> $v_ItemAsset,
					    'CodeAsset' 		=> $v_CodeAsset,
					    'TypeAsset' 		=> $v_TypeAsset,
					    'SubTypeAsset' 		=> $v_SubTypeAsset,
					    'nama_asset' 		=> $v_nama_asset,
					    'brand' 			=> $v_brand,
					    'Volume' 			=> $v_Volume,
					    'Size' 				=> $v_Size,
					    'Qty' 				=> $v_Qty,
					    'Satuan' 			=> $v_Satuan,
					    'Kondisi' 			=> $v_Kondisi,
					    'Keterangan' 		=> $v_Keterangan,
					    'filegambar' 		=> $filegambar,
					    'tgl_beli' 			=> $mylib->ubah_tanggal($v_tgl_beli),
					    'Nominal' 			=> $v_Nominal,
					    'NoProposal' 		=> $v_NoProposal,
					    'MetodePenyusutan' 	=> $v_MetodePenyusutan,
					    'Lokasi' 			=> $v_Lokasi,
					    'Departemen' 		=> $v_Departemen,
					    'PenanggungJawab' 	=> $v_PenanggungJawab,
					    'Pegawai' 			=> $v_Pegawai,
					    'EditDate'			=> date('Y-m-d'),
					    'EditUser'			=> $user
	        			);
			 }else{
			 	$datax=array(
	        			'ItemOtherAsset' 		=> $v_ItemAsset,
					    'CodeAsset' 		=> $v_CodeAsset,
					    'TypeAsset' 		=> $v_TypeAsset,
					    'SubTypeAsset' 		=> $v_SubTypeAsset,
					    'nama_asset' 		=> $v_nama_asset,
					    'brand' 			=> $v_brand,
					    'Volume' 			=> $v_Volume,
					    'Size' 				=> $v_Size,
					    'Qty' 				=> $v_Qty,
					    'Satuan' 			=> $v_Satuan,
					    'Kondisi' 			=> $v_Kondisi,
					    'Keterangan' 		=> $v_Keterangan,
					    'tgl_beli' 			=> $mylib->ubah_tanggal($v_tgl_beli),
					    'Nominal' 			=> $v_Nominal,
					    'NoProposal' 		=> $v_NoProposal,
					    'MetodePenyusutan' 	=> $v_MetodePenyusutan,
					    'Lokasi' 			=> $v_Lokasi,
					    'Departemen' 		=> $v_Departemen,
					    'PenanggungJawab' 	=> $v_PenanggungJawab,
					    'Pegawai' 			=> $v_Pegawai,
					    'EditDate'			=> date('Y-m-d'),
					    'EditUser'			=> $user
	        			);
			 }
			 
	        			
	        $this->db->update('master_fixed_asset',$datax,array('NoAsset' => $No_Asset));
	        
	        if($otorisasi=="Y"){
			//update Nominal dan Status Accounting
				$like_noAsset = substr($No_Asset,0,9);
				$belinya = $mylib->ubah_tanggal($v_tgl_beli); 
				$qry = "UPDATE
								  master_fixed_asset a
								SET
								  a.`Nominal` = '$v_Nominal',
								  a.`tgl_beli`='$belinya',
								  a.`Sts_accounting`='Y'
								WHERE a.`NoAsset` LIKE '%$like_noAsset%'
								  AND a.`Departemen` = '$v_Departemen'
								  AND a.`Lokasi` = '$v_Lokasi'
								  AND a.`nama_asset`='$v_nama_asset';";
				//echo $qry;die();
				$this->db->query($qry);		   	
			}
	        
		}
		
		redirect('/fixed_asset/fixed_asset/index/');
	}
	
	function sychDepart(){
		 $dept 	= $this->input->post('dept');
		 $list_departemen = $this->fixed_asset_model->getDepartemenFixedAsset();
		 
	     echo "<select name='v_Departemen' id='v_Departemen' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
	     <? echo " <option value=''> -- Pilih Departement ".$dept." -- </option>
	     ";
	     foreach ($list_departemen AS $cetak) {
	     	 $selected="";
			 if($dept==$cetak["KdDepartemen"])
				{
					$selected='selected="selected"';
				}
		 echo "<option $selected value=$cetak[KdDepartemen]>$cetak[NamaDepartemen]</option>";
	      } 
	     echo "</select>
	           "; 
	}
	
    function getCodeItem(){
		 $itemasset 	= $this->input->post('itemasset');
	     $query 		= $this->fixed_asset_model->getDataCodeItemAsset($itemasset);
	     
	     echo "<select name='v_CodeAsset' id='v_CodeAsset' class='form-control-new' style='border-color: #aeb0b2;width: 200px;' onchange='gol_pajak()'>";?>
	     <? echo " <option value=''> -- Pilih Code Asset -- </option>
	     ";
	     foreach ($query->result_array() as $cetak) {
		 echo "<option value=$cetak[Code]>$cetak[NamaCode]</option>";
	      } 
	     echo "</select>
	           <span id='span_loading_TypeAsset' style='border-color: #aeb0b2;display: none;'><img src='../../../../public/images/ajax-image.gif'/></span>
	           "; 
	}
	
	function getTypeItem(){
		 $typeasset 	= $this->input->post('typeasset');
		 $code_asset 	= $this->input->post('code_asset');
	     $query 		= $this->fixed_asset_model->getDataTypeAsset($typeasset,$code_asset);
	     
	        if($code_asset=="B6" OR $code_asset=="B10" OR $code_asset=="E2"){
			     echo "<select name='v_SubTypeAsset' id='v_SubTypeAsset' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
			     <? echo " <option value=''> -- Pilih Type Asset -- </option>
			     ";
			     foreach ($query->result_array() as $cetak) {
				 echo "<option value=$cetak[KdSubTypeAsset]>$cetak[NamaSubTypeAsset]</option>";
			      } 
			     echo "</select>
	            "; 
			}else if($code_asset=="B7" OR $code_asset=="B9" OR $code_asset=="E1" OR $code_asset=="E5" OR $code_asset=="G4"){
				 echo "<select name='v_SubTypeAsset' id='v_SubTypeAsset' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
			     <? echo "
			     ";
			     foreach ($query->result_array() as $cetak) {
				 echo "<option value=$cetak[KdSubTypeAsset]>$cetak[NamaSubTypeAsset]</option>";
			      } 
			     echo "</select>
	            "; 
			}else{
				 echo "<select name='v_SubTypeAsset' id='v_SubTypeAsset' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
			     <? echo "
			     ";
			     foreach ($query->result_array() as $cetak) {
				 echo "<option value=$cetak[KdSubTypeAsset]>$cetak[NamaSubTypeAsset]</option>";
			      } 
			     echo "</select>
	            "; 
			}
	     
			     
	}
	
	function getTypeItemOtherAsset(){
		 $typeasset 	= $this->input->post('typeasset');
	     $query 		= $this->fixed_asset_model->getDataTypeAssetOtherAsset($typeasset);
	     
	     echo "<select name='v_SubTypeAsset' id='v_SubTypeAsset' onchange='gol_pajak_other_asset()' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
	     <? echo " <option value=''> -- Pilih SubType Asset -- </option>
	     ";
	     foreach ($query->result_array() as $cetak) {
		 echo "<option value=$cetak[KdSubTypeAsset]>$cetak[NamaSubTypeAsset]</option>";
	      } 
	     echo "</select>
	           "; 
	}
	
	function getGolPajak(){
		 $code_asset 	= $this->input->post('code_asset');
	     $query 		= $this->fixed_asset_model->getGolPajakModel($code_asset);
	     
	     echo "<input readonly type='text' class='form-control-new' style='border-color: #aeb0b2; width: 80px; text-align: right; text-transform: uppercase;' name='v_MetodePenyusutan' id='v_MetodePenyusutan' value='$query->Gol'>
	     	   <span id='span_loading_gol_pajak1' style='border-color: #aeb0b2;display: none;'><img src='../../../../public/images/ajax-image.gif'/></span>
	     	   <span id='span_loading_accept' style='border-color: #aeb0b2;display: ;'><img src='../../../../public/images/accept.png'/></span>"; 
	}
	
	function getTypeAsset(){
		 $code_asset 	= $this->input->post('code_asset');
	     $query 		= $this->fixed_asset_model->getTypeAssetModel($code_asset);
	     
	     echo "<select name='v_TypeAsset' id='v_TypeAsset' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
	     <? echo "
	     ";
	     foreach ($query->result_array() as $cetak) {
		 echo "<option value=$cetak[KdTypeAsset]>$cetak[NamaTypeAsset]</option>";
	      } 
	     echo "</select>
	           ";
	}
	
	function getGolPajakOtherAsset(){
		 $KdTypeAsset 	= $this->input->post('type_asset');
		 $KdSubTypeAsset= $this->input->post('sub_type_asset');
	     $query 		= $this->fixed_asset_model->getGolPajakOtherAssetModel($KdTypeAsset,$KdSubTypeAsset);
	     
	     if($query[0]['Gol']!=""){
	     	 $gol = $query[0]['Gol'];
		     echo "<input readonly type='text' class='form-control-new' style='border-color: #aeb0b2; width: 80px; text-align: right; text-transform: uppercase;' name='v_MetodePenyusutan' id='v_MetodePenyusutan' value='$gol'>
		     	   <span id='span_loading_gol_pajak1' style='border-color: #aeb0b2;display: none;'><img src='../../../../public/images/ajax-image.gif'/></span>
		     	   <span id='span_loading_accept' style='border-color: #aeb0b2;display: ;'><img src='../../../../public/images/accept.png'/></span>"; 
		 }else{
			echo "<select name='v_MetodePenyusutan' id='v_MetodePenyusutan' class='form-control-new' style='border-color: #aeb0b2;width: 200px;'>";?>
			     <? echo " <option value=''> -- Pilih Golongan -- </option>
			               <option value='I'> I </option>
			               <option value='II'> II </option>
			      </select>
			     ";
		 }
	}
	
	function uploadgambar(){
		
		$config['upload_path'] 		= './public/fixed_assets';
        $config['allowed_types'] 	= 'gif|jpg|png';
        $config['max_size']  		= '204800';
        $config['max_width']  		= '128000';
        $config['max_height']  		= '76800';
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload('file')){
            $status 		= "error";
            $msg 			= $this->upload->display_errors();
            $nama_file 		= "";
        }
        else{
            $dataupload 	= $this->upload->data();
            $status 		= "success";
            $msg 			= $dataupload['file_name']." berhasil diupload";
            $nama_file 		= $dataupload['file_name'];
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg,'nama_file'=>$nama_file));
        //$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>$status,'msg'=>$msg)));
    }
    
    function uploadgambar_pro(){
		
		$config['upload_path'] 		= './public/proposal_asset';
        $config['allowed_types'] 	= 'gif|jpg|png';
        $config['max_size']  		= '204800';
        $config['max_width']  		= '128000';
        $config['max_height']  		= '76800';
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload('file')){
            $status 		= "error";
            $msg 			= $this->upload->display_errors();
            $nama_file 		= "";
        }
        else{
            $dataupload 	= $this->upload->data();
            $status 		= "success";
            $msg 			= $dataupload['file_name']." berhasil diupload";
            $nama_file 		= $dataupload['file_name'];
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg,'nama_file'=>$nama_file));
        //$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>$status,'msg'=>$msg)));
    }
    
    function viewImages(){
		 $images = $this->input->post('img');
	     echo "<span style='border-color: #aeb0b2;display: ;'><img src='../../../../public/fixed_assets/".$images."' width='500' height='350'/></span>"; 
	}
	
	function viewImages_pro(){
		 $images = $this->input->post('img');
	     echo "<span style='border-color: #aeb0b2;display: ;'><img src='../../../../public/proposal_asset/".$images."' width='500' height='350'/></span>"; 
	}
	
	function GenerateNoAsset($v_TypeAsset,$v_SubTypeAsset,$tahun){
		 $Asset = $v_TypeAsset.".".$v_SubTypeAsset.".".$tahun;
		 $q = "
	                SELECT
					  RIGHT(master_fixed_asset.NoAsset, 4) AS last_counter
					FROM
					  master_fixed_asset
					WHERE 1
					  AND LEFT(master_fixed_asset.NoAsset, 8) = '$Asset'
					ORDER BY master_fixed_asset.NoAsset DESC
					LIMIT 0, 1
	        ";
	        $qry = mysql_query($q);
	        $row = mysql_fetch_array($qry);
	        list($last_counter) = $row; 
	        
	        $counternya = ($last_counter*1) + 1;
	        $NoAsset  = $v_TypeAsset.".".$v_SubTypeAsset.".".$tahun.".".sprintf("%04s", $counternya);
	       
	        return $NoAsset;
	}
		
}

?>