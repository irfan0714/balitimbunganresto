<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Mutasi_asset extends CI_Controller{
	
	function __construct(){
        parent::__construct();
        error_reporting(0);
		$this->load->library('globallib');
        $this->load->model('fixed_asset/mutasi_asset_model','mutasi_asset_model');
    }
	
	function index(){
		//$this->load->view('barcodeview');
		$data['action'] = "tampilkan_asset";
		$data['Departement'] = $this->mutasi_asset_model->getDepartement();
		$data['Lokasi'] = $this->mutasi_asset_model->getLokasi();
		$data['Data_Buku'] = $this->mutasi_asset_model->getDataBuku();
		$data['Sort_Data_Buku'] = $this->mutasi_asset_model->getSortDataBuku();
		$data['Detail']= $this->mutasi_asset_model->getDataAssest();
		$data['Asset']= $this->mutasi_asset_model->getDataAssestDetail('01010','01010','');
		$this->load->view('fixed_asset/mutasi_asset/mutasi_fixed_assets_list',$data);
	}
	
	function tampilkan_asset(){
		//print_r($_POST);die();
		$Departement = $this->input->post('Departement');
		$Lokasi = $this->input->post('Lokasi');
		$Nama = $this->input->post('Nama');
		
		$data['action'] = "tampilkan_asset";
		$data['Departement'] = $this->mutasi_asset_model->getDepartement();
		$data['Lokasi'] = $this->mutasi_asset_model->getLokasi();
		$data['Data_Buku'] = $this->mutasi_asset_model->getDataBuku();
		$data['Sort_Data_Buku'] = $this->mutasi_asset_model->getSortDataBuku();
		$data['Detail']= $this->mutasi_asset_model->getDataAssest();
		$data['Asset']= $this->mutasi_asset_model->getDataAssestDetail($Departement,$Lokasi,$Nama);
		$this->load->view('fixed_asset/mutasi_asset/mutasi_fixed_assets_list',$data);
	}
	
	function view_asset($noasset){
		//print_r($_POST);die();
		$data['action'] = "mutasikan_assetnya";
		$data['Departement'] = $this->mutasi_asset_model->getDepartement();
		$data['Lokasi'] = $this->mutasi_asset_model->getLokasi();
		$data['History']= $this->mutasi_asset_model->getMutasiAssestDetail($noasset);
		$data['Employee']= $this->mutasi_asset_model->getEmployee();
		$this->load->view('fixed_asset/mutasi_asset/view_mutasi_fixed_asset',$data);
	}
	
	function mutasikan_assetnya(){
		//print_r($_POST);die();
		$mylib = new globallib();
		$NoAsset = $this->input->post('NoAsset');
		$Departement = $this->input->post('Departement');
		$Lokasi = $this->input->post('Lokasi');
		$tanggal_mutasi = $this->input->post('tanggal_mutasi');
		$sts_photo = $this->input->post('sts_photo');
		$filegambar = $this->input->post('filegambar');
		$qty = $this->input->post('qty');
		$ket = $this->input->post('ket');
		$pic = $this->input->post('PIC');
		
		$data =  array (
						'NoAsset'		=>$NoAsset,
					    'Departemen'	=>$Departement,
					    'Lokasi'		=>$Lokasi,
					    'Qty'			=>$qty,
					    'Satuan'		=>'Unit',
					    'Keterangan'	=>$ket,
					    'pic'			=>$pic,
					    'tgl_mutasi'	=>$mylib ->ubah_tanggal($tanggal_mutasi),
					    'file_dokumen'	=>$filegambar
						);
						
		$this->db->insert('mutasi_fixed_asset',$data);
	
		$data['action'] = "mutasikan_assetnya";
		$data['Departement'] = $this->mutasi_asset_model->getDepartement();
		$data['Lokasi'] = $this->mutasi_asset_model->getLokasi();
		$data['History']= $this->mutasi_asset_model->getMutasiAssestDetail($NoAsset);
		$data['Employee']= $this->mutasi_asset_model->getEmployee();
		$this->load->view('fixed_asset/mutasi_asset/view_mutasi_fixed_asset',$data);
		
	}
	
	function delete_asset($noasset,$dep,$lok){
		$this->db->delete('mutasi_fixed_asset',array('NoAsset'=>$noasset,'Departemen'=>$dep,'Lokasi'=>$lok));
		
		$data['action'] = "mutasikan_assetnya";
		$data['Departement'] = $this->mutasi_asset_model->getDepartement();
		$data['Lokasi'] = $this->mutasi_asset_model->getLokasi();
		$data['History']= $this->mutasi_asset_model->getMutasiAssestDetail($noasset);
		$data['Employee']= $this->mutasi_asset_model->getEmployee();
		$this->load->view('fixed_asset/mutasi_asset/view_mutasi_fixed_asset',$data);
	}
	
	function uploadgambar(){
		
		$config['upload_path'] 		= './public/mutasi_asset';
        $config['allowed_types'] 	= 'gif|jpg|png';
        $config['max_size']  		= '2048';
        $config['max_width']  		= '1280';
        $config['max_height']  		= '768';
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload('file')){
            $status 		= "error";
            $msg 			= $this->upload->display_errors();
            $nama_file 		= "";
        }
        else{
            $dataupload 	= $this->upload->data();
            $status 		= "success";
            $msg 			= $dataupload['file_name']." berhasil diupload";
            $nama_file 		= $dataupload['file_name'];
        }
        echo json_encode(array('status'=>$status,'msg'=>$msg,'nama_file'=>$nama_file));
        //$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>$status,'msg'=>$msg)));
    }
    
    function viewImages(){
		 $images = $this->input->post('img');
	     echo "<span style='border-color: #aeb0b2;display: ;'><img src='../../../../public/mutasi_asset/".$images."' width='500' height='350'/></span>"; 
	}
	
}
?>