<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class lainlain extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/lainlain_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);
            $id = $this->input->post('stSearchingKey');
            $id2 = $this->input->post('date1');
            $with = $this->input->post('searchby');
            if ($with == "TglDokumen") {
                $id = $mylib->ubah_tanggal($id2);
            }
            $this->load->library('pagination');

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = '</span>';
            $config['per_page'] = '14';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['base_url'] = base_url() . 'index.php/transaksi/lainlain/index/';
            $page = $this->uri->segment(4);
            $config['uri_segment'] = 4;
            $flag1 = "";
            if ($with != "") {
                if ($id != "" && $with != "") {
                    $config['base_url'] = base_url() . 'index.php/transaksi/lainlain/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                } else {
                    $page = "";
                }
            } else {
                if ($this->uri->segment(5) != "") {
                    $with = $this->uri->segment(4);
                    $id = $this->uri->segment(5);
                    if ($with == "TglDokumen") {
                        $id = $mylib->ubah_tanggal($id);
                    }
                    $config['base_url'] = base_url() . 'index.php/transaksi/lainlain/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                }
            }
            $data['header'] = array("No Dokumen", "Tanggal", "Tipe", "Keterangan");
            $config['total_rows'] = $this->lainlain_model->num_row(addslashes($id), $with);
            $tanggal = $this->lainlain_model->getDate();
            $data['tanggal'] = $tanggal->TglTrans2;
            $this->pagination->initialize($config);
            $data['data'] = $this->lainlain_model->getList($config['per_page'], $page, addslashes($id), $with);
            $data['qtyctk'] = "";
            $data['extract'] = "";
            $this->load->view('transaksi/lainlain/lainlain_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['track'] = $mylib->print_track();
            $data['tanggal'] = $this->lainlain_model->getDate();
            $data['mgudang'] = $this->lainlain_model->getGudang();
            //$data['mcndn'] = $this->lainlain_model->getCNDN();
            $this->load->view('transaksi/lainlain/lainlain_add', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_lainlain() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->lainlain_model->getHeader($id);
            $data['detail'] = $this->lainlain_model->getDetail($id);
            $data['session_tgl'] = $this->session->userdata('Tanggal_Trans');
            $this->load->view('transaksi/lainlain/lainlain_edit', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function print_barcode() {
        $id = $this->uri->segment(4);
        $data = $this->varBarcode($id);
        $data['NoDokumen'] = $id;
        $this->load->view('transaksi/cetak_transaksi/print_barcode', $data);
    }

    function printThisBarcode() {
        $id = $this->uri->segment(4);
        $data = $this->varBarcode($id);
        $data['fileName2'] = "barcode_simpan.ccc";
        $qtycetak = $this->lainlain_model->getQtyCetak($id);
        for ($m = 0; $m < count($qtycetak); $m++) {
            $qtyctk = $qtycetak[$m]['QtyCetak'];
            $data['QtyCetak'][] = $qtyctk;
        }
//		$data['fileName2'] = "barcode_lain.sss";
        $this->load->view('transaksi/cetak_transaksi/print_barcode_printer', $data);
    }

    function varBarcode($id) {
        $det = $this->lainlain_model->getBasedBarcode($id);
//                print_r($det);
        $getIdPerusahaan = $this->lainlain_model->getDate();
        $perusahaan = $getIdPerusahaan->DefaultKodePerusahaan;
        $this->load->library('printreportlib');
        $printreport = new printreportlib();
        $mylib = new globallib();
        for ($s = 0; $s < count($det); $s++) {
            $nonya = $det[$s]['NoDokumen'];
            $nopenerimaan = $perusahaan . "8" . $nonya;
            $nilai = $printreport->findSatuanQtyCetak($det[$s]['QtyKonversi'], $det[$s]['KonversiBesarKecil'], $det[$s]['KonversiTengahKecil'], "B", "T", "K");
            $nilai_exp = explode(" ", $nilai);
            for ($m = 0; $m < count($nilai_exp); $m++) {
                $qty = $nilai_exp[$m];
                $m++;
                $satuan = $nilai_exp[$m];
                if ($satuan == "B") {
                    $kode_sat = "1";
                } else if ($satuan == "T") {
                    $kode_sat = "2";
                } else if ($satuan == "K") {
                    $kode_sat = "3";
                }
                if (strlen($det[$s]['Counter']) == 1) {
                    $det[$s]['Counter'] = "0" . $det[$s]['Counter'];
                }
                $asaldata = "1";
                $barcode = $kode_sat . $asaldata . $nopenerimaan . $det[$s]['Counter'];
                $data['Barcode'][] = $barcode;
                $data['Qty'][] = $qty;
                if (strlen($det[$s]['PCode']) == 13) {
                    $hasil = $mylib->findStructureBarcode($det[$s]['PCode'], "PCode", "distinct");
                    $pcode_hasil = $hasil['nilai'];
                    $properties_barang = $this->lainlain_model->getPCodeName($pcode_hasil[0]['PCode']);
                    $pcode = $pcode_hasil[0]['PCode'];
                } else {
                    $properties_barang = $this->lainlain_model->getPCodeName($det[$s]['PCode']);
                    $pcode = $det[$s]['PCode'];
                }
                $cari_attr_lot = $mylib->getLot("L", $det[$s]['NoDokumen'], $pcode, $det[$s]['Counter'], "2");
                $data['Attr'][] = substr($cari_attr_lot->NilAttr, strlen($cari_attr_lot->NilAttr) - 6, 6);
//                                                                print_r($cari_attr_lot);
                if ($satuan == "B") {
                    $data['Satuan'][] = $properties_barang->SatuanBesar;
                } else if ($satuan == "T") {
                    $data['Satuan'][] = $properties_barang->SatuanTengah;
                } else if ($satuan == "K") {
                    $data['Satuan'][] = $properties_barang->SatuanKecil;
                }
                $data['Nama'][] = $properties_barang->NamaLengkap;
                $data['noterima'][] = $det[$s]['NoDokumen'];
                $data['asaldata'][] = "L";
                $data['counter'][] = $det[$s]['Counter'];
                $data['pcode'][] = $pcode;
                $data['konversibesarkecil'][] = $det[$s]['KonversiBesarKecil'];
                $data['konversitengahkecil'][] = $det[$s]['KonversiTengahKecil'];
            }
        }
        $data['url'] = "transaksi/lainlain/printThisBarcode/" . $id;
        return $data;
    }

    function cetak() {
        $data = $this->varCetak();
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_lain', $data);
    }

    function printThis() {
        $data = $this->varCetak();
        $id = $this->uri->segment(4);
        $data['fileName2'] = "lainlain.sss";
        $data['fontstyle'] = chr(27) . chr(80);
        $data['nfontstyle'] = "";
        $data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
        $data['pindah_hal'] = "\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
        $data['string1'] = "     Dibuat Oleh,                     Disetujui Oleh,";
        $data['string2'] = "(                     )         (                      )";
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_lain', $data);
    }

    function varCetak() {
        $this->load->library('printreportlib');
        $mylib = new globallib();
        $printreport = new printreportlib();
        $id = $this->uri->segment(4);
        $header = $this->lainlain_model->getHeader($id);
        $data['header'] = $header;
        $detail = $this->lainlain_model->getDetailForPrint($id);
        $data['judul1'] = array("NoDokumen", "TglDokumen", "Keterangan");
        $data['niljudul1'] = array($header->NoDokumen, $header->TglDokumen, stripslashes($header->Keterangan));
        $data['judul2'] = "";
        $data['niljudul2'] = "";
        $data['judullap'] = $header->NamaKeterangan . " LAIN LAIN";
        $data['colspan_line'] = 4;
        $data['url'] = "lainlain/printThis/" . $id;
        $data['tipe_judul_detail'] = array("normal", "normal", "kanan", "normal", "kanan", "kanan");
        $data['judul_detail'] = array("Kode", "Nama Barang", "Qty", "", "Harga", "Total");
        $data['panjang_kertas'] = 30;
        $jmlh_baris_lain = 19;
        $data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
        $jml_baris_detail = count($detail) + $this->lainlain_model->getCountDetail($id);
        if ($data['panjang_per_hal'] == 0) {
            $data['tot_hal'] = 1;
        } else {
            $data['tot_hal'] = ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
        }
        $list_detail = array();
        $detail_attr = array();
        $list_detail_attr = array();
        $detail_page = array();
        $new_array = array();
        $gudangBaris = 0;
        $gudangRow = 0;
        $max_field_len = array(0, 0, 0, 0, 0, 0);
        $sum_netto = 0;
//                print_r($detail);
        for ($m = 0; $m < count($detail); $m++) {
//			$attr = $this->lainlain_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
            unset($list_detail);
            $gudangRow++;
            $list_detail[] = stripslashes($detail[$m]['PCode']);
            $list_detail[] = stripslashes($detail[$m]['NamaInitial']);
            $list_detail[] = number_format($detail[$m]['QtyPcs'], 0, '', '.');
            $list_detail[] = "pcs";
            $list_detail[] = number_format($detail[$m]['Harga'], 0, '', '.');
            $list_detail[] = number_format(($detail[$m]['QtyPcs']) * ($detail[$m]['Harga']), 0, '', '.');
            $detail_page[] = $list_detail;
            $max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
            if ($data['panjang_per_hal'] != 0) {
                if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
                    $data['detail'][] = $detail_page;
                    if ($m != count($detail) - 1) {
                        unset($detail_page);
                    }
                }
            }
            $netto = $detail[$m]['QtyPcs'] * $detail[$m]['Harga'];
            $sum_netto = $sum_netto + ($netto);
        }
        $data['judul_netto'] = array("Total", "PPN 10%", "Nett");
        $data['isi_netto'] = array(number_format($sum_netto, 0, ',', '.'), number_format(($sum_netto * 0.1), 0, ',', '.'), number_format($sum_netto + ($sum_netto * 0.1), 0, ',', '.'));
        $data['detail'][] = $detail_page;
        $data['max_field_len'] = $max_field_len;
        $data['banyakBarang'] = $gudangRow;
        return $data;
    }

    function getPCode() {
        $mylib = new globallib();
        $kode = $this->input->post('pcode');
        $tgl = $mylib->ubah_tanggal($this->input->post('tgl'));
        $bulan = substr($tgl, 5, 2);
        $tahun = substr($tgl, 0, 4);
        $fieldmasuk = "QtyMasuk" . $bulan;
        $fieldakhir = "QtyAkhir" . $bulan;
        $fieldkeluar = "QtyKeluar" . $bulan;
        $jml = "";
        $noterima = "";
        $gudang = "";
        $asaldata = "";

        $valpcode = $kode;
        if (count($valpcode) != 0) {
            $pcode = $valpcode;
//			$jenis_kode = $valpcode->Jenis;
            $detail = $this->lainlain_model->getPCodeDet($pcode);
            if (!empty($detail)) {
                $nilai = $detail->NamaLengkap . "*&^%" . $detail->Harga . "*&^%" . $detail->PCode;
            } else {
                $nilai = "";
            }
        } else {
            $nilai = "";
        }
        echo $nilai;
    }

    function getstringstock() {
        $pcodebarang = $this->input->post('pcodebarang');
        $pickingmethod = $this->input->post('pickmethod');
        $qtypcs = $this->input->post('qtypcs');
        $tgltrans = $this->session->userdata('Tanggal_Trans');
        $tahun = substr($tgltrans, 0, 4);
        $bulan = substr($tgltrans, 5, 2);
        $strstok = $this->getStok($pcodebarang, $pickingmethod, $tahun, $bulan, $qtypcs);
        if ($strstok != "picking method not defined") {
            $ambil_total = explode("@", $strstok);
            $string_stok = $ambil_total[0];
            if ($ambil_total[1] >= $qtypcs) {
                $qtyterambil = $qtypcs;
            } else {
                $qtyterambil = $ambil_total[1];
            }
            $str = "##" . $qtyterambil . "##" . $ambil_total[1] . "##" . $ambil_total[2] . "**" . $string_stok;
        } else {
            $str = $strstok;
        }
        echo $str;
    }

    function save_new_lainlain() {
         //   echo "<pre>";print_r($_POST);echo "</pre>";//die();
        $mylib = new globallib();
        $user = $this->session->userdata('userid');
        $no = $this->input->post('nodok');
        $tgl = $this->input->post('tgl');
        $tipe = $this->input->post('hidetipe');
        $kontak = $this->input->post('kontak');
        //$cndn = $this->input->post('cndn');
        $gudang = $this->input->post('gudang');
        $ket = trim(strtoupper(addslashes($this->input->post('ket'))));
        $flag = $this->input->post('flag');
        $pcode1 = $this->input->post('pcode');
        $qty1 = $this->input->post('qty');
        $hrg1 = $this->input->post('hrg');
        $nil1 = $this->input->post('nil');
        $qtypcs1 = $this->input->post('qtypcs');
        $qtydisplay1 = $this->input->post('qtydisplay');
        if ($flag == "add") {
            $no = $this->InsertHeader($no, $mylib->ubah_tanggal($tgl), $tipe, $kontak, $ket, $user,  $gudang);
        } else {
            $this->updateHeader($flag, $no, $ket, $user,  $gudang);
            //hapus detail lama dan update stock
            $bulan = substr($tgl, 5, 2);
            $tahun = substr($tgl, 0, 4);
            $fieldmasuk = "QtyMasuk" . $bulan;
            $fieldakhir = "QtyAkhir" . $bulan;
            $fieldkeluar = "QtyKeluar" . $bulan;
            $this->deleteAll($tipe, $no, $tahun, $fieldmasuk, $fieldkeluar, $fieldakhir); //update dan delete
        }
        for ($x = 0; $x < count($pcode1); $x++) {
            if ($pcode1[$x] != "") {
                $pcode = strtoupper(addslashes($pcode1[$x]));
                $qty = trim($qty1[$x]);
                $hrg = trim($hrg1[$x]);
                $nil = trim($nil1[$x]);
                $this->InsertAllDetail($flag, $tipe, $no, $pcode, $qty, $hrg, $nil, $user, $tgl, $ket, $gudang);
            }
        }
        redirect('/transaksi/lainlain/');
    }

    function InsertHeader($no, $tgl, $tipe, $kontak, $ket, $user,  $gudang) {
        $this->lainlain_model->locktables('counter,trans_lainlain_header');
        $new_no = $this->lainlain_model->getNewNo($tgl);
        $no = $new_no->NoLainLain;
        echo $no;
      //  $this->db->update('counter', array("NoLainLain" =>  $no + 1), array("Tahun" => substr($tgl, 0, 4), "Bulan" => substr($tgl, 5 , 2)));
        if ($tipe == "masuk") {
            $tipe = "M";
        } else if ($tipe == "keluar") {
            $tipe = "K";
        }
        $data = array(
            'NoDokumen' => $no,
            'TglDokumen' => $tgl,
            'KdPerusahaan' => "01",
            'Tipe' => $tipe,
            'KdGudang' => $gudang,
            'KdContact' => $kontak,
            'Keterangan' => $ket,
            'AddDate' => $tgl,
            'AddUser' => $user
        );
        $this->db->insert('trans_lainlain_header', $data);
        $this->lainlain_model->unlocktables();
        return $no;
    }

    function updateHeader($flag, $no, $ket, $user,  $gudang) {
        $this->lainlain_model->locktables('trans_lainlain_header');
        $tgltrans = $this->session->userdata('Tanggal_Trans');
        $data = array(
            'Keterangan' => $ket,
        );
        if ($flag == "edit") {
            $data['EditDate'] = $tgltrans;
            $data['EditUser'] = $user;
        }
        $this->db->update('trans_lainlain_header', $data, array("NoDokumen" => $no));
        $this->lainlain_model->unlocktables();
    }

    function InsertAllDetail($flag, $tipe, $no, $pcode, $qty, $hrg, $nil, $user, $tgl, $ket, $gudang) { //echo $qty."/".$qtypcs."/".$qtydisplay."/".$flag."/".$pcodesave."=".$pcode;
        /* insert to mutasi
         * Update/insert stock
         * insert to trans_lainlain_detail
         */
        $bulan = substr($tgl, 5, 2);
        $tahun = substr($tgl, 0, 4);
        $fieldmasuk = "QtyMasuk" . $bulan;
        $fieldakhir = "QtyAkhir" . $bulan;
        $fieldkeluar = "QtyKeluar" . $bulan;
        $adddateapl = date('Y-m-d H:i:s');
        $adddatestok = $tgl . " " . date('H:i:s');

        //$lokasi_dulu = $this->lainlain_model->getPastLocation($no,$pcodesave,$gudang);
        $this->doAll($flag, $tipe, $no, $pcode, $qty, $hrg, $nil, $user, $tgl, $ket, $fieldmasuk, $fieldkeluar, $fieldakhir, $adddateapl, $adddatestok, $tahun, $gudang);
    }

    function doAll($flag, $tipe, $no, $pcode, $qty, $hrg, $nil, $user, $tgl, $ket, $fieldmasuk, $fieldkeluar, $fieldakhir, $adddateapl, $adddatestok, $tahun, $gudang) {
        if ($tipe == "masuk") {
          //  $this->updateStokMasuk($no, $pcode, $qty, $hrg, $fieldmasuk, $fieldakhir, $tahun, $adddateapl, $adddatestok, $gudang);
        } else if ($tipe == "keluar") {//echo "keluar";
         //   $this->updateStokKeluar($pcode, $qty, $fieldkeluar, $fieldakhir, $tahun, $gudang);
        }
    //    echo $no;
//            }
        $this->insertTransDetail($flag, $no, $tipe, $pcode, $qty, $hrg, $nil, $user, $tgl, $adddateapl, $adddatestok, $gudang);
       // $this->insertMutasi($no, $tipe, $pcode, $qty, $hrg, $user, $tgl, $ket, $gudang);
    }

    function updateStokMasuk($no, $pcode, $qty, $hrg, $fieldmasuk, $fieldakhir, $tahun, $adddateapl, $adddatestok, $gudang) {
        $this->lainlain_model->locktables('stock,stock_detail');
        $stokawal = $this->lainlain_model->CekStock($fieldmasuk, $fieldakhir, $pcode, $tahun);
        if (!empty($stokawal)) {
            $data = array(
                $fieldmasuk => (int) $stokawal->$fieldmasuk + (int) $qty,
                $fieldakhir => (int) $stokawal->$fieldakhir + (int) $qty
            );
            $this->db->update('stock', $data, array("Tahun" => $tahun, "KodeBarang" => $pcode, "Gudang" => $gudang));
        } else {
            $data = array(
                'Tahun' => $tahun,
                'Gudang' => $gudang,
                'KodeBarang' => $pcode,
                $fieldmasuk => $qty,
                $fieldakhir => $qty
            );
            $this->db->insert('stock', $data);
        }
        $this->lainlain_model->unlocktables();
    }

    function updateStokKeluar($pcode, $qty, $fieldkeluar, $fieldakhir, $tahun, $gudang) {
//            print $fieldkeluar."/".$fieldakhir."/".$pcodebarang."/".$tahun."/".$lokasi."/".$qtypcs;
        $this->lainlain_model->locktables('stock');
        $stokawal = $this->lainlain_model->StockKeluarAwal($fieldkeluar, $fieldakhir, $pcode, $tahun);
        if (!empty($stokawal)) {
            $data = array(
                $fieldkeluar => (int) $stokawal->$fieldkeluar + (int) $qty,
                $fieldakhir => (int) $stokawal->$fieldakhir - (int) $qty
            );
            $this->db->update('stock', $data, array("Tahun" => $tahun, "KodeBarang" => $pcode, "Gudang" => $gudang));
        } else {
            $data = array(
                'Tahun' => $tahun,
                'Gudang' => $gudang,
                'KodeBarang' => $pcode,
                $fieldkeluar => $qty,
                $fieldakhir => $qty * -1
            );
            $this->db->insert('stock', $data);
        }
        $this->lainlain_model->unlocktables();
    }

    function insertTransDetail($flag, $no, $tipe, $pcode, $qty, $hrg, $nil, $user, $tgl, $adddateapl, $adddatestok, $gudang) {//print "detailnya";
        $this->lainlain_model->locktables('trans_lainlain_detail');

        $data = array(
            'NoDokumen' => $no,
            'PCode' => $pcode,
            'KdGudang' => $gudang,
            'QtyPcs' => $qty,
            'Harga' => $hrg,
            'Netto' => $nil,
            'TglStockSimpan' => $adddatestok,
            'TglAplikasiStokSimpan' => $adddateapl,
            'AddDate' => $tgl,
            'AddUser' => $user
        );
        if ($flag == "edit") {
            $tgltrans = $this->session->userdata('Tanggal_Trans');
            $data['EditDate'] = $tgltrans;
            $data['EditUser'] = $user;
        }
        $this->db->insert('trans_lainlain_detail', $data);
        $this->lainlain_model->unlocktables();
    }

    function insertMutasi($no, $tipe, $pcode, $qty, $hrg, $user, $tgl, $ket, $gudang) {
        $this->lainlain_model->locktables('mutasi');
        if ($tipe == "masuk") {
            $jenismutasi = "I";
            $kodetransaksi = "TL";
        }
        if ($tipe == "keluar") {
            $jenismutasi = "O";
            $kodetransaksi = "KL";
        }
        $dataekonomis = array(
            'KdTransaksi' => $kodetransaksi,
            'NoTransaksi' => $no,
            'Tanggal' => $tgl,
            'Kasir' => $user,
            'KodeBarang' => $pcode,
            'Gudang' => $gudang,
            'Qty' => $qty,
            'Nilai' => $hrg,
            'Jenis' => $jenismutasi,
            'Keterangan' => $ket
        );
        $this->db->insert('mutasi', $dataekonomis);
        $this->lainlain_model->unlocktables();
    }

    function deleteAll($tipe, $no, $tahun, $fieldmasuk, $fieldkeluar, $fieldakhir) {//print $fieldmasuk."/".$fieldakhir."/".$pcode."/".$tahun."/".$lokasi;
//           echo $tipe.$qty;
        $this->lainlain_model->unlocktables();

        $lama = $this->lainlain_model->getdatalama($no);
//                print_r($lama);
        for ($x = 0; $x < count($lama); $x++) {
            $pcode = $lama->PCode;
            $qty = $lama->QtyPcs;
//		$this->lainlain_model->locktables('stock,mutasi,trans_lainlain_detail');
            if ($tipe == "masuk") {
                $kdtransaksi = "TL";
                $stokawal = $this->lainlain_model->CekStock($fieldmasuk, $fieldakhir, $pcode, $tahun);
//                        print_r($stokawal);
                $data = array(
                    $fieldmasuk => (int) $stokawal->$fieldmasuk - (int) $qty,
                    $fieldakhir => (int) $stokawal->$fieldakhir - (int) $qty
                );
                $this->db->update('stock', $data, array("Tahun" => $tahun, "KodeBarang" => $pcode));
            } else if ($tipe == "keluar") {
                $kdtransaksi = "KL";
                $stokawal = $this->lainlain_model->StockKeluarAwal($fieldkeluar, $fieldakhir, $pcode, $tahun);
                $data = array(
                    $fieldkeluar => (int) $stokawal->$fieldkeluar - (int) $qty,
                    $fieldakhir => (int) $stokawal->$fieldakhir + (int) $qty
                );
                $this->db->update('stock', $data, array("Tahun" => $tahun, "KodeBarang" => $lama->PCode));
            }
            $this->db->delete('trans_lainlain_detail', array("NoDokumen" => $no, "PCode" => $pcode));
            $this->db->delete('mutasi', array("KdTransaksi" => $kdtransaksi, "NoTransaksi" => $no, "KodeBarang" => $pcode));
        }
        $this->lainlain_model->unlocktables();
    }

    function delete_itemnya($tipe, $no, $pcode, $pcodebarang, $lokasi, $qty, $gudang, $tahun, $noterima, $asaldata, $gudangterima, $fieldmasuk, $fieldkeluar, $fieldakhir) {//print $fieldmasuk."/".$fieldakhir."/".$pcode."/".$tahun."/".$lokasi;
        $this->lainlain_model->locktables('stock,stock_detail,mutasi,trans_lainlain_detail');
        if ($tipe == "masuk") {
            $kdtransaksi = "TL";
            $stokawal = $this->lainlain_model->CekStock($fieldmasuk, $fieldakhir, $pcodebarang, $tahun);
            $data = array(
                $fieldmasuk => (int) $stokawal->$fieldmasuk - (int) $qty,
                $fieldakhir => (int) $stokawal->$fieldakhir - (int) $qty
            );
            $this->db->update('stock', $data, array("Tahun" => $tahun, "PCode" => $pcodebarang));
        } else if ($tipe == "keluar") {
            $kdtransaksi = "KL";
            $stokawal = $this->lainlain_model->StockKeluarAwal($fieldkeluar, $fieldakhir, $pcodebarang, $tahun);
            $data = array(
                $fieldkeluar => (int) $stokawal->$fieldkeluar - (int) $qty,
                $fieldakhir => (int) $stokawal->$fieldakhir + (int) $qty
            );
            $this->db->update('stock', $data, array("Tahun" => $tahun, "PCode" => $pcodebarang));
        }
        $this->db->delete('mutasi', array("KdTransaksi" => $kdtransaksi, "NoTransaksi" => $no, "KodeBarang" => $pcodebarang, 'Counter' => $gudang));
        $this->lainlain_model->unlocktables();
    }

    function delete_item() {
        $no = $this->input->post('kode');
        $mylib = new globallib();
//            $no = $this->uri->segment(4);
        $header = $this->lainlain_model->getHeader($no);
        $detail = $this->lainlain_model->getDetail($no);
//                print_r($detail);
        $tipe = $header->Tipe;
        $tgl = $mylib->ubah_tanggal($header->TglDokumen);
        $bulan = substr($tgl, 5, 2);
        $tahun = substr($tgl, 0, 4);
        $fieldmasuk = "QtyMasuk" . $bulan;
        $fieldakhir = "QtyAkhir" . $bulan;
        $fieldkeluar = "QtyKeluar" . $bulan;
        for ($x = 0; $x < count($detail); $x++) {
            $pcode = $detail[$x]['PCode'];
            $lokasi_dulu = $this->lainlain_model->getPastLocation($no, $pcode, $gudang);
//                    print_r($lokasi_dulu);
            $this->delete_itemnya($tipe, $no, $pcode, $pcode, $lokasi_dulu->QtyPcs, $gudang, $tahun, $lokasi_dulu->NoPenerimaan, $lokasi_dulu->AsalData, $lokasi_dulu->CounterStockPenerimaan, $fieldmasuk, $fieldkeluar, $fieldakhir);
        }
        $datanya = array(
            'NoDokumen' => $no . "D",
            'FlagDelete' => "Y"
        );
        $this->db->update('trans_lainlain_header', $datanya, array("NoDokumen" => $no));
//                $this->index();
    }

    function doThis($sPrevStart = null, $sPrevEnd = null) {
//            $noorder			= $this->uri->segment(4);die();
        $nodok = $this->input->post('hdok');
        $mylib = new globallib();
        $csv_terminated = "\n";
        $csv_separator = "#";
        //$csv_enclosed   = '"';
        $csv_enclosed = '';
        $csv_escaped = "\"";
        $schema_insert = "";
        $out = '';
        $batasjudul = ";";
        //$sPrevStart = $mylib->ubah_tanggal($this->input->post('tgl_1'));
        //$sPrevEnd   = $mylib->ubah_tanggal($this->input->post('tgl_2'));
        // Load your model - rename the "your_model_name"
//                $this->load->model('proses/kirim_data_model','chat');
//                $kdC = $this->cabang();
        $kdC = $this->lainlain_model->FindCabang($nodok); //print_r($kdC);
        $kg = $kdC->KdCounter;
//                $outputH = $this->lainlain_model->getReports($no);
        $tabel = array('trans_lainlain_header');
//                $tabel = array('trans_lainlain_header');
        $kondisi = array('NoDokumen'); // kondisi untuk where
        $output = "";
        $outputD = '';
        for ($i = 0; $i < count($tabel);) {
            $outputD .= $this->lainlain_model->getReportsDetail($tabel[$i], $kondisi[$i], $nodok, $csv_terminated, $csv_separator, $csv_enclosed, $csv_escaped, $schema_insert, $out, $batasjudul, $kg);
            $i++;
        }
        $output .= $outputD;
        // ----- +++ -----
        $tabelH = array('trans_lainlain_header');
        $tabelD = array('trans_lainlain_detail');
        $key = array('NoDokumen'); // kondisi untuk where
        $outputTrxD = '';

        for ($c = 0; $c < count($tabelH);) {
            $outputTrxD .= $this->lainlain_model->getDetailTrx($tabelD[$c], $tabelH[$c], $key[$c], $nodok, $csv_terminated, $csv_separator, $csv_enclosed, $csv_escaped, $schema_insert, $out, $batasjudul, $kg);
            $c++; //echo count($tabelH);die();
        }
        $output .= $outputTrxD;

        $filename = 'lainlain_' . $nodok . '.csv';

        $mylib = new globallib();
        $mylib->to_excel($output, $filename);
        //$this->index();
//redirect('/transaksi/lainlain/');
        //  return ;
    }

    function save_cetak_attr() {
        $no = $this->input->post('noterima');
        $qtyctk = $this->input->post('qtycetak');
        $attr_temp = explode("~", $qtyctk);
        $data = $this->varBarcode($no);
        $this->db->delete('trans_simpan_cetak', array("NoDokumen" => $no));
        for ($k = 0; $k < count($attr_temp) - 1; $k++) {
            $qty = $attr_temp[$k];
            $this->db->insert('trans_simpan_cetak', array("qtycetak" => $qty, "NoDokumen" => $no, "Counter" => $k));
        }
    }

}

?>
