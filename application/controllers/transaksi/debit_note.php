<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Debit_note extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->helper('terbilang');
        $this->load->model('transaksi/debit_note_model');
        $this->load->model('proses/posting_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "debit_note", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            
	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/debit_note/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/debit_note/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
			$data['msupplier'] = $this->debit_note_model->getSupplier();
			
            $config['total_rows'] = $this->debit_note_model->num_debit_note_row($arr_search["search"]);
			$data['data'] = $this->debit_note_model->getDebitNoteList($config['per_page'], $page, $arr_search["search"]);

			//untuk cek apakah di user tersebut pernah input tapi ngeback atau ngeclose paksa
			$data['datado'] = $this->debit_note_model->getDebitNoteDetailList($user);
			if(!empty($data['datado'])){
				$data['param']='1';
			}else{
				$data['param']='0';
			}

			
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/debit_note/debit_note_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'debit_note', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "debit_note",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/debit_note/index/' . $query_id . '');
    }
	
	function batal() 
    {
    	    $user = $this->session->userdata('username');
			$this->db->delete('debitnotedtl_temp',array('dnno'=>'00000','adduser'=>$user));
	
			redirect('/transaksi/debit_note/');	
			
    }
    
    	
	function add_new($param) 
    {
    	if($param=='1'){
    					
			        $mylib = new globallib();
			        $sign = $mylib->getAllowList("add");
		        	if ($sign == "Y") {
		            $data['msg'] = "";
		            
		            $user = $this->session->userdata('username');
		            $userlevel = $this->session->userdata('userlevel');
		            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
		            
		            //hapus dulu ditemporari
				     $this->db->delete('debitnotedtl_temp',array('adduser'=>$user));
		            
					$data['mUang']	= $this->debit_note_model->getCurrency();
		            $data['msupplier'] = $this->debit_note_model->getSupplier();
		            $data['subdivisi'] = $this->debit_note_model->getSubdivisi();
		            $data['datado'] = $this->debit_note_model->getDebitNoteDetailList($user);
								
		            $data['track'] = $mylib->print_track();
		            //$this->load->view('transaksi/debit_note/add_debit_note', $data);
		            redirect('/transaksi/debit_note/add_new/');
		        } else {
		            $this->load->view('denied');
		        }
		        
		        }else{
					
					$mylib = new globallib();
			        $sign = $mylib->getAllowList("add");
		        	if ($sign == "Y") {
		            $data['msg'] = "";
		            
		            $user = $this->session->userdata('username');
		            $userlevel = $this->session->userdata('userlevel');
		            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
		            
					$data['mUang']	= $this->debit_note_model->getCurrency();
		            $data['msupplier'] = $this->debit_note_model->getSupplier();
		            $data['subdivisi'] = $this->debit_note_model->getSubdivisi();
		            $data['datado'] = $this->debit_note_model->getDebitNoteDetailList($user);
								
		            $data['track'] = $mylib->print_track();
		            $this->load->view('transaksi/debit_note/add_debit_note', $data);
		        } else {
		            $this->load->view('denied');
		        }
					
				}
    }

    function edit_debit_note($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->debit_note_model->getCurrency();
            $data['header'] = $this->debit_note_model->getHeader($id);
			
            $data['msupplier'] = $this->debit_note_model->getSupplier();
            $data['subdivisi'] = $this->debit_note_model->getSubdivisi();
			$data['hitungdo'] = $this->debit_note_model->hitungDebitNoteDetailList2($id);
			$data['hitungdodetail'] = $this->debit_note_model->hitungDebitNoteDetailList3($id);
            $data['datado'] = $this->debit_note_model->getDebitNoteDetailList2($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/debit_note/edit_debit_note', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
     function view_debit_note($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->debit_note_model->getCurrency();
            $data['header'] = $this->debit_note_model->getHeader($id);
			
            $data['msupplier'] = $this->debit_note_model->getSupplier();
            $data['subdivisi'] = $this->debit_note_model->getSubdivisi();
			$data['hitungdo'] = $this->debit_note_model->hitungDebitNoteDetailList2($id);
			$data['hitungdodetail'] = $this->debit_note_model->hitungDebitNoteDetailList3($id);
            $data['datado'] = $this->debit_note_model->getDebitNoteDetailList2($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/debit_note/view_debit_note', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function jurnal_edit_debit_note($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->debit_note_model->getCurrency();
            $data['header'] = $this->debit_note_model->getHeaderJurnal($id);
            $data['msupplier'] = $this->debit_note_model->getSupplier();
            $data['subdivisi'] = $this->debit_note_model->getSubdivisi();
			$data['hitungdo'] = $this->debit_note_model->hitungDebitNoteDetailList2($id);
			$data['hitungdodetail'] = $this->debit_note_model->hitungDebitNoteDetailList3($id);
            $data['datado'] = $this->debit_note_model->getDebitNoteDetailList2($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/debit_note/jurnal_edit_debit_note', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
   
	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $dndate = $this->input->post('v_tanggal_dokumen');
        $v_subdivisi = $this->input->post('v_subdivisi');
        $v_supplier = $this->input->post('v_supplier');
        $v_type = $this->input->post('v_type');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
		$currencycode = $this->input->post('Uang');
		$diskon = $this->input->post('diskon');
		$ppn = $this->input->post('ppn');
		$v_rekening = $this->input->post('v_rekening');
		$pisah = explode('-',$v_rekening);
		$norekening = $pisah[0];
	
		//detail
		$v_dndid1 = $this->input->post('v_dndid');
		$v_coano1 = $this->input->post('v_coano');
		$v_subdivisi1 = $this->input->post('v_subdivisi');
		$v_dekripsi1 = $this->input->post('v_dekripsi');
		$v_amount1 = $this->input->post('v_amount');
		
		//summary
		$cnamount = $this->input->post('grandtotalhidden');		
        
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($dndate));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
		
        if ($flag == "add")
		{
		//generate dnno
		switch ($v_type) {
		    case 'D':
        		$v_dnno = $mylib->get_code_counter($this->db->database, "debitnote","dnno", "DN", $data['bulan'], $data['tahun']); 	
        		break;
        	case 'C':
        		$v_dnno = $mylib->get_code_counter($this->db->database, "debitnote","dnno", "CN", $data['bulan'], $data['tahun']); 	
        		break;
        	case 'DM':
        		$v_dnno = $mylib->get_code_counter($this->db->database, "debitnote","dnno", "DM", $data['bulan'], $data['tahun']); 	
        		break;
        	case 'CM':
        		$v_dnno = $mylib->get_code_counter($this->db->database, "debitnote","dnno", "CM", $data['bulan'], $data['tahun']); 	
        		break;
		}
		//pertama insertheader terlebih dahulu
		$this->insertNewHeader($v_dnno, $mylib->ubah_tanggal($dndate),$v_type,$norekening, $v_supplier, "1", $v_note, $user, $cnamount, $currencycode, $diskon, $ppn);
		
		for ($x = 0; $x < count($v_coano1); $x++)
			{
            $coano = $v_coano1[$x];
            $v_subdivisi2 = $v_subdivisi1[$x];
            $deskripsi = $v_dekripsi1[$x];
            $amount = $v_amount1[$x];
            
            $data_detail=array(
			  'dnno' => $v_dnno,
			  'coano' => $coano,
			  'KdSubdivisi'=>$v_subdivisi2,
			  'value' => $amount,
			  'description' => $deskripsi,
			  'adddate' => date('Y-m-d'),
			  'adduser' => $user
            );
			
				if($coano!=""){
					$this->db->insert('debitnotedtl', $data_detail);
				}	
			
			}	
				
		} 
		
		else if ($flag == "edit") 
		{
			$v_dnno = $this->input->post('v_dnno');
			//update header
			$this->updateHeader($v_dnno,$mylib->ubah_tanggal($dndate),$v_type, $v_supplier, $v_status, $v_note, $user, $this->input->post('grandtotalhidden'), $currencycode, $diskon, $ppn);	
			
			//insert baru detail
			for ($x = 0; $x < count($v_coano1); $x++)
			{
            $coano = $v_coano1[$x];
            $v_subdivisi2 = $v_subdivisi1[$x];
            $deskripsi = $v_dekripsi1[$x];
            $amount = $v_amount1[$x];
            
            $data_detail=array(
			  'dnno' => $v_dnno,
			  'coano' => $coano,
			  'KdSubdivisi'=>$v_subdivisi2,
			  'value' => $amount,
			  'description' => $deskripsi,
			  'adddate' => date('Y-m-d'),
			  'adduser' => $user
            );
			
				if($coano!=""){
				$this->db->insert('debitnotedtl', $data_detail);
				}
			}
			
			$sum_value= $this->debit_note_model->getValue($v_dnno);
			$data_update = array(
								'dnamount'=>$sum_value->total,
								'dnamountremain'=>$sum_value->total
								);
			$this->db->update('debitnote',$data_update,array('dnno'=>$v_dnno));
			
				if($v_status=='1'){
					//cek total value yang sudah diinsert di creditnotedetail diatas
					$cek_total_amount = $this->debit_note_model->cekDataDebitNoteDetail($v_dnno);
					$grandtotal = $cek_total_amount[0]['grandtotal'];
				
					//update juga ke tabel hutang
					$cek_ditabel_hutang = $this->debit_note_model->cekTabelHutang($v_dnno);
					if(!empty($cek_ditabel_hutang)){
						$this->updateNewHutang($v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier,$v_type, $grandtotal, $norekening);
					}else{
						$this->insertNewHutang($v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier,$v_type,$currencycode, $grandtotal,$norekening);
					}
					
					
				
					//update juga ke tabel mutasi hutang
					$cek_ditabel_mutasi_hutang = $this->debit_note_model->cekTabelMutasiHutang($v_dnno);
					if(!empty($cek_ditabel_mutasi_hutang)){
						$this->updateNewMutasiHutang($v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier, $grandtotal,$norekening,$v_type);	
					}else{
						$this->insertNewMutasiHutang($data['tahun'], $data['bulan'], $v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier,$currencycode, $grandtotal,$norekening,$v_type);
					}
					
					
				
				}			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_dnno . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/debit_note/edit_debit_note/'.$v_dnno.'');
    }
    
    function save_data_jurnal() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $userid = $this->session->userdata('userid');
        
        $dndate = $this->input->post('v_tanggal_dokumen');
        $v_subdivisi = $this->input->post('v_subdivisi');
        $v_supplier = $this->input->post('v_supplier');
        $v_type = $this->input->post('v_type');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
		$currencycode = $this->input->post('Uang');
		$diskon = $this->input->post('diskon');
		$ppn = $this->input->post('ppn');
		$v_rekening = $this->input->post('v_rekening');
		$pisah = explode('-',$v_rekening);
		$norekening = $pisah[0];
	
		//detail
		$v_dndid1 = $this->input->post('v_dndid');
		$v_coano1 = $this->input->post('v_coano');
		$v_subdivisi1 = $this->input->post('v_subdivisi');
		$v_dekripsi1 = $this->input->post('v_dekripsi');
		$v_amount1 = $this->input->post('v_amount');
		
		//summary
		$cnamount = $this->input->post('grandtotalhidden');		
        
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($dndate));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
		
		
			$v_dnno = $this->input->post('v_dnno');
			//update header
			$this->updateHeader($v_dnno,$mylib->ubah_tanggal($dndate),$v_type, $v_supplier, $v_status, $v_note, $user, $this->input->post('grandtotalhidden'), $currencycode, $diskon, $ppn);	
			
			//insert baru detail
			for ($x = 0; $x < count($v_coano1); $x++)
			{
            $coano = $v_coano1[$x];
            $v_subdivisi2 = $v_subdivisi1[$x];
            $deskripsi = $v_dekripsi1[$x];
            $amount = $v_amount1[$x];
            
            $data_detail=array(
			  'dnno' => $v_dnno,
			  'coano' => $coano,
			  'KdSubdivisi'=>$v_subdivisi2,
			  'value' => $amount,
			  'description' => $deskripsi,
			  'adddate' => date('Y-m-d'),
			  'adduser' => $user
            );
			
				if($coano!=""){
				$this->db->insert('debitnotedtl', $data_detail);
				
				
				//ambil no referensi di jurnal header
                $noreferensi = $this->debit_note_model->getReferensi($v_dnno);
        
				//insert juga ke jurnal
				$data_jurnal1 = array('NoReferensi'=>$noreferensi->NoReferensi,
							        'TglTransaksi'=>$mylib->ubah_tanggal($dndate),
							        'KdRekening'=>$coano,
							        'KeteranganDetail'=>$deskripsi,
							        'KodeJurnal'=>'CB',
							        'KdDepartemen'=>'00',
							        'KdSubDivisi'=>$v_subdivisi2,
							        'Project'=>'00',
							        'CostCenter'=>'00',
							        'Debit'=>0,
							        'Kredit'=>$amount,
							        'Bulan'=>$xbulan,
							        'Tahun'=>$xtahun,
							        'Counter'=>$x,
							        'EditDate'=>date('Y-m-d'),
							        'EditName'=>$user);
				//$this->db->insert('jurnaldetail',$data_jurnal1);
				//untuk kredit itu lewat skema postingan
							        
				$data_jurnal2 = array('NoReferensi'=>$noreferensi->NoReferensi,
							        'TglTransaksi'=>$mylib->ubah_tanggal($dndate),
							        'KdRekening'=>$coano,
							        'KeteranganDetail'=>$deskripsi,
							        'KodeJurnal'=>'CB',
							        'KdDepartemen'=>'00',
							        'KdSubDivisi'=>$v_subdivisi2,
							        'Project'=>'00',
							        'CostCenter'=>'00',
							        'Debit'=>$amount,
							        'Kredit'=>0,
							        'Bulan'=>$xbulan,
							        'Tahun'=>$xtahun,
							        'Counter'=>$x+1,
							        'EditDate'=>date('Y-m-d'),
							        'EditName'=>$user);
				//$this->db->insert('jurnaldetail',$data_jurnal2);
				//untuk debit itu lewat skema postingan
				
				$this->posting_cndn_pembelian_edit_jurnal($xtahun,$xbulan,$userid,$v_dnno,$noreferensi->NoReferensi);
				
				}
			}
			
			$sum_value= $this->debit_note_model->getValue($v_dnno);
			$data_update = array(
								'dnamount'=>$sum_value->total,
								'dnamountremain'=>$sum_value->total
								);
			$this->db->update('debitnote',$data_update,array('dnno'=>$v_dnno));
			
				if($v_status=='1'){
					//cek total value yang sudah diinsert di creditnotedetail diatas
					$cek_total_amount = $this->debit_note_model->cekDataDebitNoteDetail($v_dnno);
					$grandtotal = $cek_total_amount[0]['grandtotal'];
				
					//update juga ke tabel hutang
					$cek_ditabel_hutang = $this->debit_note_model->cekTabelHutang($v_dnno);
					if(!empty($cek_ditabel_hutang)){
						$this->updateNewHutang($v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier,$v_type, $grandtotal, $norekening);
					}else{
						$this->insertNewHutang($v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier,$v_type,$currencycode, $grandtotal,$norekening);
					}
					
					
				
					//update juga ke tabel mutasi hutang
					$cek_ditabel_mutasi_hutang = $this->debit_note_model->cekTabelMutasiHutang($v_dnno);
					if(!empty($cek_ditabel_mutasi_hutang)){
						$this->updateNewMutasiHutang($v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier, $grandtotal,$norekening,$v_type);	
					}else{
						$this->insertNewMutasiHutang($data['tahun'], $data['bulan'], $v_dnno, $mylib->ubah_tanggal($dndate), $v_supplier,$currencycode, $grandtotal,$norekening,$v_type);
					}
					
				}	
						
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_dnno . '</strong> berhasil', 'class' => 'success'));

        
        redirect('/transaksi/debit_note/jurnal_edit_debit_note/'.$v_dnno.'');
    }

    function insertNewHeader($v_dnno, $dndate,$v_type, $norekening, $v_supplier, $v_status, $v_note, $user, $cnamount, $currencycode, $diskon, $ppn) 
    {
        $this->debit_note_model->locktables('debitnote');

        $data = array(
				  'dnno' => $v_dnno,
				  'dndate' => $dndate,
				  'dntype'=>$v_type,
				  'supplierid' => $v_supplier,
				  'note' => $v_note,
				  'status' => 0,
				  'currencycode' => $currencycode,
				  'vatpercent' => $ppn,
				  'discpercent' => $diskon,
				  'dnamount' => $cnamount,
				  'dnamountremain' => $cnamount,
				  'KdRekening'=>$norekening,
				  'adddate' => date('Y-m-d'),
				  'adduser' => $user
				);

        $this->db->insert('debitnote', $data);

        $this->debit_note_model->unlocktables();

    }
    
    function insertNewHutang($dnno, $tgl, $v_supplier,$v_type,$currencycode, $grandtotal, $norekening) 
    {
        $this->debit_note_model->locktables('hutang');

        $data = array(
				  'NoDokumen' => $dnno,
				  'NoFaktur' => $dnno,
				  'KdSupplier' => $v_supplier,
				  'TipeTransaksi' => $v_type,
				  'Tanggal' => $tgl,
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal,
				  'MataUang'=>$currencycode,
				  'KdRekening'=> $norekening
				);

        $this->db->insert('hutang', $data);

        $this->debit_note_model->unlocktables();

    }
    
    function insertNewMutasiHutang($tahun, $bulan, $dnno, $tgl, $v_supplier ,$currencycode, $grandtotal, $norekening, $v_type )
    {
        $this->debit_note_model->locktables('mutasi_hutang');

        $data = array(
        		  'Tahun'=>$tahun,
        		  'Bulan'=>$bulan,
        		  'Tanggal' => $tgl,
				  'NoDokumen' => $dnno,
				  'KdSupplier' => $v_supplier,
				  'MataUang'=>$currencycode,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => $v_type,
				  'KdRekening' => $norekening
				);

        $this->db->insert('mutasi_hutang', $data);

        $this->debit_note_model->unlocktables();

    }
    
    function updateNewHutang($dnno, $tgl, $v_supplier,$v_type, $grandtotal,$norekening) 
    {
        $this->debit_note_model->locktables('hutang');

        $data = array(
				  'KdSupplier' => $v_supplier,
				  'TipeTransaksi' => $v_type,
				  'Tanggal' => $tgl,
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $dnno,
				  'NoFaktur' => $dnno
				);

        $this->db->update('hutang', $data, $where);

        $this->debit_note_model->unlocktables();

    }
    
    function updateNewMutasiHutang($dnno, $tgl, $v_supplier, $grandtotal,$norekening,$v_type) 
    {
        $this->debit_note_model->locktables('mutasi_hutang');

        $data = array(
        		  'Tanggal' => $tgl,
				  'KdSupplier' => $v_supplier,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => $v_type,
				  'KdRekening' => $norekening
				);
				
		$where=array(
		          'NoDokumen' => $dnno
					);

        $this->db->update('mutasi_hutang', $data, $where);

        $this->debit_note_model->unlocktables();

    }
    
    function updateNewHutang2($dnno, $grandtotal) 
    {
        $this->debit_note_model->locktables('hutang');

        $data = array(
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $dnno,
				  'NoFaktur' => $dnno
				);

        $this->db->update('hutang', $data, $where);

        $this->debit_note_model->unlocktables();

    }
  
	function updateHeader($dnno,$dndate,$v_type, $v_supplier, $v_status, $v_note, $user, $cnamount, $currencycode, $diskon, $ppn) 
    {
    	//echo $cnno." - ".$dndate." - ". $v_customer." - ". $v_status." - ". $v_note." - ". $user." - ". $cnamount." - ". $currencycode." - ". $diskon." - ". $ppn;die;
        $this->debit_note_model->locktables('debitnote');

        $data2 = array
				(
				  'dndate' => $dndate,
				  'dntype'=>$v_type,
				  'supplierid' => $v_supplier,
				  'note' => $v_note,
				  'status' => $v_status,
				  'currencycode' => $currencycode,
				  'vatpercent' => $ppn,
				  'discpercent' => $diskon,
				  'dnamount' => $cnamount,
				  'dnamountremain' => $cnamount,
				  'editdate' => date('Y-m-d'),
				  'edituser' => $user
				);
				
		$where2=array(
		'dnno'=>$dnno
					);

        $this->db->update('debitnote', $data2, $where2);
        
        $this->debit_note_model->unlocktables();
    }

	
	
	function delete_trans($dnno) 
    {
	       
			$this->db->delete("debitnote",array('dnno'=>$dnno));
			$this->db->delete("debitnotedtl",array('dnno'=>$dnno));
			$this->db->delete("hutang",array('NoDokumen'=>$dnno));
			$this->db->delete("mutasi_hutang",array('NoDokumen'=>$dnno));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $dnno . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/debit_note/');
    }

    function delete_detail() //delete detail add
    {
    	
        $dndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $user = $this->uri->segment(6);
        
        $data=array(
        			'dndid'=>$dndid,
        			'coano'=>$coano,
        			'adduser'=>$user
        			);
		
        $this->db->delete("debitnotedtl_temp",$data);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/debit_note/add_new/');
    } 
    
    function delete_detail2() //delete detail add
    {
    	
        $dndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $dnno = $this->uri->segment(6);
        $user = $this->session->userdata('username');
        
                    /*$data=array(
        			'dndid'=>$dndid,
        			'coano'=>$coano,
        			'adduser'=>$user
        			);*/
        			
        			$data=array(
        			'dndid'=>$dndid,
        			'coano'=>$coano
        			);
		
        $this->db->delete("debitnotedtl",$data);
        
        //cek total value yang sudah diinsert di creditnotedetail diatas
		$cek_total_amount = $this->debit_note_model->cekDataDebitNoteDetail($dnno);
		$grandtotal = $cek_total_amount[0]['grandtotal'];
			
		//insert juga ke tabel piutang
		$this->updateNewHutang2($dnno, $grandtotal);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/debit_note/edit_debit_note/' .$dnno.'');
    }
    
    function delete_detail_jurnal() //delete detail add
    {
    	
        $dndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $dnno = $this->uri->segment(6);
        $user = $this->session->userdata('username');
        			
        			$data=array(
        			'dndid'=>$dndid,
        			'coano'=>$coano
        			);
		
        $this->db->delete("debitnotedtl",$data);
        
        //hapus juga di jurnal
        //ambil no referensi di jurnal header
        $noreferensi = $this->debit_note_model->getReferensi($dnno);
        $this->db->delete("jurnaldetail",array('NoReferensi'=>$noreferensi->NoReferensi));
        
        //cek total value yang sudah diinsert di creditnotedetail diatas
		$cek_total_amount = $this->debit_note_model->cekDataDebitNoteDetail($dnno);
		$grandtotal = $cek_total_amount[0]['grandtotal'];
			
		//insert juga ke tabel piutang
		$this->updateNewHutang2($dnno, $grandtotal);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/debit_note/jurnal_edit_debit_note/' .$dnno.'');
    }


	
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "I N V O I C E";
		$data["header"] 	= $this->debit_note_model->getHeader($nodok);
		$data["detail"] 	= $this->debit_note_model->getDetail_cetak($nodok);
		$data["hitungsi"] 	= $this->debit_note_model->hitungSalesInvoice_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_si', $data);
	}
	
	
	function doPrint()
	{
		$this->load->helper('terbilang');
		$this->load->library('globallib');
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 120;
	    $total_spasi_header = 80;
	    $jml_detail  = 8;
	    $ourFileName = "sales-invoice.txt";
		
		$header 	= $this->debit_note_model->getHeader($nodok);
		$detail 	= $this->debit_note_model->getDetail_cetak($nodok);
		$hitungsi 	= $this->debit_note_model->hitungSalesInvoice_cetak($nodok);
		
		/*echo "<pre>";
		print_r($data["hitungsi"]);
		echo "</pre>";
		die;*/
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
            $arr_data["detail_harga"][$counter] = $val["Harga1c"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "I N V O I C E";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->invoiceno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->invoiceno;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->sidate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->sidate));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->duedate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->duedate));$i++)
                {
                    $echo.=" ";
                }
               
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Pelanggan";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Pelanggan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                                
                $echo.="\r\n";    
            }
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 60;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 5;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Total";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            $harga = $arr_data["detail_harga"][$i_detail];
	            $total = $qty*$harga;
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 60;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,0,',','.');
                    $limit_spasi = 5;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=number_format($harga,0,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_harga));$i++)
                    {
                        $echo.=" ";
                    }
                    
					    if(strlen(number_format($total,0,',','.'))==1){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==2){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==3){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==4){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==5){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==6){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==7){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==8){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==9){
						$jarak_total=5;
						}				
					if(strlen(number_format($total,0,',','.'))==10){
                    $echo.=number_format($total,0,',','.');
					}else{
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($total,0,',','.'))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($total,0,',','.');
					}
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
			
			
            
			foreach($hitungsi as $val)
			{
				$tothal = $val['total'];
				$diskon = $val['diskon'];
				
				if($diskon ==0){
				$potongan_diskon = 0;
				}else{
				$potongan_diskon =($diskon/100)*$total;
				}
				
				$ppn=(10/100)*$tothal;
				
				$grand_total=($tothal-$potongan_diskon)+$ppn;
				
			}
			
											
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Subtotal";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Subtotal"));$i++)
						{
							$echo.=" ";
						}
						if(strlen(number_format($tothal,0,',','.'))==1){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==2){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==3){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==4){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==5){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==6){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==7){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==8){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==9){
						$jarak_tothal=5;
						}
						if(strlen(number_format($tothal,0,',','.'))==10){
						$echo.=number_format($tothal,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($tothal,0,',','.'))+$jarak_tothal));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($tothal,0,',','.');
						}
						
				
						$echo .= "\r\n";						
						
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Diskon";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Diskon"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($potongan_diskon,0,',','.'))==1){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==2){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==3){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==4){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==5){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==6){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==7){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==8){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==9){
						$jarak_diskon=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($potongan_diskon,0,',','.'))+$jarak_diskon));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($potongan_diskon,0,',','.');
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="PPN";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("PPN"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($ppn,0,',','.'))==1){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==2){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==3){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==4){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==5){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==6){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==7){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==8){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==9){
						$jarak_ppn=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($ppn,0,',','.'))+$jarak_ppn));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($ppn,0,',','.');
						
						
						
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Grand Total";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Grand Total"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==1){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total0,',','.'))==2){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==3){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==4){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==5){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==6){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==7){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==8){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==9){
						$jarak_grand_total=5;
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==10){
						$echo.=number_format($grand_total,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($grand_total,0,',','.'))+$jarak_grand_total));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($grand_total,0,',','.');
						}
						
						
						$echo .= "\r\n";						
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Terbilang: ".terbilang($grand_total,$style=4)." Rupiah";
						
						$echo .= "\r\n";
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Note : ".$header->note;
			
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			
		        $data = array(
		            'form_data' => "sales-invoice",
		            'noreferensi' => $header->invoiceno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        
		}

		$paths = "path/to/";
	    $name_text_file='sales-invoice-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function cekrekening(){
		$kdrekening = $this->input->post('kdrekening');
		$query = $this->debit_note_model->cekRekening($kdrekening);
		
		if(!empty($query)){
			$data['success']=TRUE;
		}else{
			$data['success']=FALSE;
		}
		
		echo json_encode($data);
	}
	
	function posting_cndn_pembelian_edit_jurnal($tahun, $bulan, $userid, $dnno, $noref){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'CB', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref, 'NoTransaksi' => $dnno));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'CB', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref));
        
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->posting_model->getinterface();
        $header = $this->posting_model->getcndnpembelianEditJurnal($tahun, $bulan,$dnno);
        $nilaivat = 0;	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['dnno'];
			$keterangan = $header[$m]['note'];
			$keterangandetail = $header[$m]['description'];
			$tgldokumen = $header[$m]['dndate'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekdetail = $header[$m]['coano'];
            $nilai = $header[$m]['value'];
            $nama = $header[$m]['Nama'];
            $dntype = $header[$m]['dntype'];
            $kdrekeningheader = $header[$m]['KdRekening'];
            $vatpercent = $header[$m]['VatPercent'];
            									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->posting_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'CB',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid);
                $this->db->insert('jurnalheader', $data);
            }   
            if($dntype=='D' || $dntype=='DM'){
            	if($nilai>0){
					$debit = 0;
					$credit = $nilai;
				}else{
					$debit = $nilai*-1;
					$credit = 0;	
				}
			}else{
				if($nilai>0){
					$debit = $nilai;
					$credit = 0;
				}else{
					$debit = 0;
					$credit = $nilai*-1;	
				}
				
			}
			
            $counterid++;
            $totkredit += $credit;
            $totdebit += $debit;
            $nilaivat += $nilai * $vatpercent/100;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "CB",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekdetail,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $debit,
                "Kredit" => $credit,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['dnno']){
					
					if($kdrekeningheader== ''){
						if($dntype=='D' || $dntype=='C'){
							$rekhutang = $rekening[0]['HutangDagang'];
						}else{
							$rekhutang = $rekening[0]['HutangLain'];
						}
					}else{
						$rekhutang = $kdrekeningheader;	
					}	
					
					if($nilaivat>0){
						if($dntype=='D' || $dntype=='DM'){
							$vatdebet = 0;
							$vatcredit = $nilaivat;
						}else{
							$vatdebet = $nilaivat;
							$vatcredit = 0;
						}
					
						$counterid++;
						$datadtl = array("NoReferensi" => $noposting,
				                "TglTransaksi" => $tgldokumen,
				                "KodeJurnal" => "CB",
				                "KdDepartemen" => "00",
				                "Project" => "00",
				                "CostCenter" => "00",
				                "KdRekening" => $rekening[0]['PPnBeli'],
								"KdSubDivisi"	=> $kdsubdivisi,
				                "Debit" => $vatdebet,
				                "Kredit" => $vatcredit,
				                "KeteranganDetail" => $keterangan,
				                "Bulan" => $bulan,
				                "Tahun" => $tahun,
				                "JenisJurnal" => "U",
				                "AddDate" => $tgldokumen,
				                "AddName" => $userid,
				                "Counter" => $counterid
			            	);
			            $this->db->insert('jurnaldetail', $datadtl);	
					}
					
					if($dntype=='D' || $dntype=='DM'){
						$nilaihutangdb = $totkredit-$totdebit+$nilaivat;
						$nilaihutangcr = 0;
					}else{
						$nilaihutangdb = 0;
						$nilaihutangcr = $totdebit-$totkredit+$nilaivat;
					}
					$counterid++;
					$datadtl = array("NoReferensi" => $noposting,
			                "TglTransaksi" => $tgldokumen,
			                "KodeJurnal" => "CB",
			                "KdDepartemen" => "00",
			                "Project" => "00",
			                "CostCenter" => "00",
			                "KdRekening" => $rekhutang,
							"KdSubDivisi"	=> $kdsubdivisi,
			                "Debit" => $nilaihutangdb,
			                "Kredit" => $nilaihutangcr,
			                "KeteranganDetail" => $keterangan,
			                "Bulan" => $bulan,
			                "Tahun" => $tahun,
			                "JenisJurnal" => "U",
			                "AddDate" => $tgldokumen,
			                "AddName" => $userid,
			                "Counter" => $counterid
		            	);
		            $this->db->insert('jurnaldetail', $datadtl);
		            $totkredit=0;
		            $totdebit=0;
		            $nilaivat = 0;
		        }
		    }	
		    $nodok = $nodokumen;
		}
		
        if($m > 0){
        	if($dntype=='D' || $dntype=='C'){
				$rekhutang = $rekening[0]['HutangDagang'];
			}else{
				$rekhutang = $rekening[0]['HutangLain'];
			}
			if($nilaivat>0){
				if($dntype=='D' || $dntype=='DM'){
					$vatdebet = 0;
					$vatcredit = $nilaivat;
				}else{
					$vatdebet = $nilaivat;
					$vatcredit = 0;
				}
					
				$counterid++;
				$datadtl = array("NoReferensi" => $noposting,
		                "TglTransaksi" => $tgldokumen,
		                "KodeJurnal" => "CB",
		                "KdDepartemen" => "00",
		                "Project" => "00",
		                "CostCenter" => "00",
		                "KdRekening" => $rekening[0]['PPnBeli'],
						"KdSubDivisi"	=> $kdsubdivisi,
		                "Debit" => $vatdebet,
		                "Kredit" => $vatcredit,
		                "KeteranganDetail" => $keterangan,
		                "Bulan" => $bulan,
		                "Tahun" => $tahun,
		                "JenisJurnal" => "U",
		                "AddDate" => $tgldokumen,
		                "AddName" => $userid,
		                "Counter" => $counterid
	            	);
	            $this->db->insert('jurnaldetail', $datadtl);	
			}
			
			if($dntype=='D' || $dntype=='DM'){
				$nilaihutangdb = $totkredit-$totdebit+$nilaivat;
				$nilaihutangcr = 0;
			}else{
				$nilaihutangdb = 0;
				$nilaihutangcr = $totdebit-$totkredit+$nilaivat;
			}
			
			$counterid++;
			$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi" => $tgldokumen,
	                "KodeJurnal" => "CB",
	                "KdDepartemen" => "00",
	                "Project" => "00",
	                "CostCenter" => "00",
	                "KdRekening" => $rekhutang,
					"KdSubDivisi"	=> $kdsubdivisi,
	                "Debit" => $nilaihutangdb,
	                "Kredit" => $nilaihutangcr,
	                "KeteranganDetail" => $keterangan,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
            	);
            $this->db->insert('jurnaldetail', $datadtl);
        }
	}

}

?>