<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Voucher_employee extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/voucher_employee_model');
    }

    public function index() 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        
        if ($sign == "Y") {
            
			 $data['data'] = $this->voucher_employee_model->getVoucherEmployee();

            $data['bulan'] = date('m');
            $data['tahun'] = date('Y');
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/voucher_employee/voucher_employee_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 

    public function search(){
        $search_keyword = $this->input->post('search_keyword');
        $search_status = $this->input->post('search_status');
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $data['search_keyword'] = $search_keyword;
        $data['search_status'] = $search_status;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['data'] = $this->voucher_employee_model->searchData($search_keyword,$search_status,$bulan,$tahun);

        
        if($this->input->post('excel') == 'yes'){
          $this->export($search_keyword,$search_status,$bulan,$tahun);
        }
        
          $this->load->view('transaksi/voucher_employee/voucher_employee_list', $data);

    }
    

   public function export($search_keyword,$search_status,$bulan,$tahun){
    error_reporting(1);
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel.php';

        $arrbulan = array('0'=>'','Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $bulanName = $arrbulan[$bulan];
        // Panggil class PHPExcel nya
        $excel = new PHPExcel();
        // Settingan awal fil excel
        $excel->getProperties()->setCreator('')
                     ->setLastModifiedBy('')
                     ->setTitle("Data Voucher Karyawan ".$bulanName."-".$tahun)
                     ->setSubject("Voucher Karyawan")
                     ->setDescription("Data Voucher Karyawan ".$bulanName."-".$tahun)
                     ->setKeywords("Data Voucher Karyawan");
        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
          'font' => array('bold' => true), // Set font nya jadi bold
          'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
          ),
          'borders' => array(
            'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
            'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
            'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
            'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
          )
        );
        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
          'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
          ),
          'borders' => array(
            'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
            'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
            'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
            'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
          )
        );
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA VOUCHER KARYAWAN "); // Set kolom A1 dengan tulisan "DATA SISWA"
        $excel->getActiveSheet()->mergeCells('A1:H1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); 
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "NIK");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "KETERANGAN");
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "EXPIRED DATE"); 
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "STATUS"); 
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "NOMINAL");
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "TERPAKAI"); 
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "SISA"); 
        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);

        $arr =  $this->voucher_employee_model->searchData($search_keyword,$search_status,$bulan,$tahun);
        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
        foreach($arr as $data){ // 

          $status = ($data['status'] == '1') ? "Aktif" : "Non Aktif";
          $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
          $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data['nik']);
          $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data['keterangan']);
          $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data['expDate']);
          $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $status);
          $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data['nominal']);
          $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data['terpakai']);
          $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data['nominal'] - $data['terpakai']);
          
          // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
          $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
          
          $no++; // Tambah 1 setiap kali looping
          $numrow++; // Tambah 1 setiap kali looping
        }
        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(30); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(30); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(30); // Set width kolom E
        
        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Data Voucher Karyawan ".$bulan."-".$tahun);
        $excel->setActiveSheetIndex(0);
        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data_voucher_karyawan_'.$bulanName.'_'.$tahun.'.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');
        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
        redirect('transaksi/voucher_employee');
      }

      public function import(){ 
        $filename = $_FILES['file']['name'];
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $expDate = date('Y-m-t',strtotime($tahun."-".$bulan."-01"));
        
        $data = array(); // Buat variabel $data sebagai array
        
          // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
          $upload = $this->voucher_employee_model->upload_file($filename);
          if($upload['result'] == "success"){ // Jika proses upload sukses
            // Load plugin PHPExcel nya
            $this->load->library('excel_reader');
            $this->excel_reader->setOutputEncoding('CP1251');

            $dir    = $_SERVER['DOCUMENT_ROOT'].'/natura/upload/excel_voucher_employee/';

            $file   =  $dir.$filename;
            $tse  =  $this->excel_reader->read($file);
            error_reporting(E_ALL ^ E_NOTICE);
            $hasil        = $this->excel_reader->sheets[0];

            foreach ($hasil['cells'] as $row => $colum) {
              if($row != 1){

                $data['nik'] = $colum[1];
                $data['keterangan'] = $colum[2];
                $data['expDate'] = $expDate;
                $data['nominal'] = $colum[3];
                $data['addDate'] = date('Y-m-d H:i:s');
                $data['userAdd'] = $this->session->userdata('username');;
                $data['status'] = ($colum[4] == 'Aktif') ? '1':'0';

                $cek = $this->db->get_where('voucher_employee',array('nik'=>$data['nik'],'expDate'=>$data['expDate']))->num_rows();

                if($cek > 0){
                    $this->db->update('voucher_employee',
                                      array('keterangan'=>$data['keterangan'],'nominal'=>$data['nominal'],'addDate'=>$data['addDate'],'userAdd'=>$data['userAdd'],'status'=>$data['status']),
                                      array('nik'=>$data['nik'],'expDate'=>$data['expDate'])
                                      );
                }else{
                    $this->db->insert('voucher_employee',$data);
                }

                //insert ke voucher yg jenis 3 (buat diskon karyawan)

                $cek = $this->db->get_where('voucher',array('novoucher'=>$data['nik'],'jenis'=>'3'))->num_rows();

                if($cek == 0){
                    $this->db->insert('voucher',array('novoucher'=>$data['nik'],'nominal'=>'0','keterangan'=>$data['keterangan'],'jenis'=>'3','rupdisc'=>'D')); 
                }

              }
            }

            unlink($file);

            redirect('transaksi/voucher_employee');

          }else{ // Jika proses upload gagal
            $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
          }
        
      }

      public function hapus(){
          $nik = $this->input->post('nik');
          $expDate = $this->input->post('expDate');

          $del = $this->db->delete('voucher_employee',array('nik'=>$nik,'expDate'=>$expDate));

          if($del){
              echo "success";
          }else{
              echo "failed";
          }
      }
}

?>