<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Ticketingtest extends authcontroller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/pos_model');
		$this->load->model('transaksi/ticketingmodel');
        $this->load->model('globalmodel');
    }
	
    function index(){
        $mylib	= new globallib();
        $sign	= $mylib->getAllowList("add");
        $nm		= $mylib->getUser();
		$getDateApp	= $this->globalmodel->getGenereteNumberTicket(" TglTrans from aplikasi ");
		$date		= $getDateApp[0]['TglTrans'];
		$ipkassa	= $_SERVER['REMOTE_ADDR'];
		$todaytrans	= $this->session->userdata('Tanggal_Trans');
		list($thntrans, $blntrans, $tgltrans)	= explode("-",$todaytrans);
		$todayid	= date("w", mktime(0, 0, 0, $blntrans, $tgltrans, $thntrans));
				
        if ($sign == "Y") {
        	
        	$jenis = $this->uri->segment(4);
        	$data["search_jenis"] = $jenis;
        	
            $data['TotalNetto']			= 0;
            $data['Charge']				= 0;
            $data['ttlall']				= 0;
            $data['TotalQty']			= 0;
            $data['store']				= $this->ticketingmodel->aplikasi();
            $data['mUang']				= $this->ticketingmodel->getCurrency();
            $data['NoKassa']			= $this->ticketingmodel->cekidkassa($ipkassa); // cek no kasir
            $data['track']				= $mylib->print_track();
			$data['list']				= $this->ticketingmodel->getTicketList($todayid,$jenis);
			$data['subkatticket']		= $this->globalmodel->getQuery(" * FROM `ticket_subkategori` a ORDER BY a.`idsubkategori` ASC;");
			$getTotal		 			= $this->ticketingmodel->totalTicketPerhari($date);
			$data['ttlticket']			= $getTotal[0]['total'];
			$ip				= $ipkassa;
			$getKassa		= $this->globalmodel->getGenereteNumberTicket(" id_kassa from kassa where ip='$ip'");
			$getIP			= $getKassa[0]['id_kassa'];
			if(strlen($getIP)=='1'){
				$getLastIP = "0".$getIP;
			}else{
				$getLastIP = $getIP;
			}
			$data['NoTrans']			= $this->getGenereteNotransTicket($getLastIP);
			//PETUGAS STICKER
			$data['PetugasSticker']		= $this->globalmodel->getQuery(" a.`username`,a.`employee_name` FROM employee a LEFT JOIN `user` b ON a.`username`=b.`UserName` ORDER BY a.`employee_name` ASC");
			$data['TourTravel']			= $this->globalmodel->getQuery(" KdTravel,Nama FROM tourtravel ORDER BY Nama ASC");
			$data['Country']			= $this->globalmodel->getQuery(" country_code,country_name FROM apps_countries ORDER BY id ASC");
			$data['City']				= $this->globalmodel->getQuery(" id_kota,nama_kota FROM kota ORDER BY nama_kota ASC");
            $this->load->view('transaksi/ticket/tampil', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function insert_trans(){
	    //echo "<pre>";print_r($_POST);echo "</pre>";die;
		$NoTrans = $this->input->post('NoTrans');
		$NoIdent = $this->input->post('NoIdentitas');
		$QtyHead = $this->input->post('TotalItem2');
		$TtlAmt = $this->input->post('ttlall2');
		$TtlByr = $this->input->post('total_bayar_hide');
		$TtlKbl = $this->input->post('cash_kembali_hide');
		$TtlChg = $this->input->post('SerCharge2');
		$NilaiTunai = $this->input->post('cash_bayar');
		$NilaiDebit = $this->input->post('debet_bayar');
		$NilaiKredit = $this->input->post('kredit_bayar');
		$NilaiVoucher = $this->input->post('voucher_bayar');
		$SessionName = $this->session->userdata('username');
		
		$CustomerName = $this->input->post('NamaRombongan');
		$StaffSticker = $this->input->post('PetugasId');
		$TourTravel = $this->input->post('TourTravel');
		$TourGuide = $this->input->post('TourGuide');
		$Country = $this->input->post('Country');
		$City = $this->input->post('City');
		
		$Currency = $this->input->post('Uang');
		$getDateApp	= $this->globalmodel->getQuery(" TglTrans from aplikasi ");
		$Date		= $getDateApp[0]['TglTrans'];
		$DateV		= date($Date." H:i:s");
		$jml		= $this->input->post('jml');
		$expDate	= $getDateApp[0]['TglTrans'];
		$ip				= $_SERVER['REMOTE_ADDR'];
		$ipLast			= substr($_SERVER['REMOTE_ADDR'],-2);
		$getKassa		= $this->globalmodel->getQuery(" id_kassa from kassa where ip='$ip'");
		$getIP			= $getKassa[0]['id_kassa'];
		if(strlen($getIP)=='1'){
			$getLastIP = "0".$getIP;
		}else{
			$getLastIP = $getIP;
		}
		
		$cekNoTrans = $this->globalmodel->getQuery(" notrans from ticket_head where notrans='".$NoTrans."' and last_ip='".$getLastIP."' ");
		if(!empty($cekNoTrans)){
			$NoTransNew = $this->getGenereteNotransTicket($getLastIP);
			// echo $NoTransNew;die;
		}else{
			$NoTransNew = $NoTrans;
		}
		//INSERT HEAD
		$data_head = array(
		"notrans" => $NoTransNew,
		"qty" => $QtyHead,
		"ttl_amount" => $TtlAmt,
		"ttl_bayar" => $TtlByr,
		"ttl_kembali" => $TtlKbl,
		"ttl_charge" => $TtlChg,
		"nilai_tunai" => $NilaiTunai,
		"nilai_debit" => $NilaiDebit,
		"nilai_kredit" => $NilaiKredit,
		"nilai_voucher" => $NilaiVoucher,
		"tanggal" => $DateV,
		"currency" => $Currency,
		"noidentitas" => $NoIdent,
		"user" => $SessionName,
		"last_ip" => $getLastIP
		);
		$this->db->insert("ticket_head",$data_head);
		
		//INSER CUSTOMER NAME AND STAFF STICKER
		$data_customer = array(
		"notrans" => $NoTransNew,
		"customer_name" => $CustomerName,
		"staff_id" => $StaffSticker,
		"add_date" => $Date,
		"add_user" => $SessionName,
		"no_identitas" => $NoIdent,
		"kdtravel" => $TourTravel,
		"kdtourleader" => $TourGuide,
		"country" => $Country,
		"city" => $City
		);
		$this->db->insert("ticket_customer",$data_customer);
		
		//INSERT NO TICKET
		for($i=0;$i<$jml;$i++){
		$qty	=	$this->input->post('qty1'.$i);
		$harga	=	$this->input->post('jualm1'.$i);
		$tipe	=	$this->input->post('tipe'.$i);
		$PCode	=	$this->input->post('nmticket'.$i);
		$jenis	=	$this->input->post('jenis'.$i);
		$nominal =	$this->input->post('nominalVouch'.$i);
			if($qty!=0){
				for($j=0;$j<$qty;$j++){
					$noticket	=	$this->getGenereteNumberTicket($getLastIP);
					$data = array(
					"noticket" => $noticket,
					"PCode"=>$PCode,
					"jenis" => $jenis,
					"tipe" => $tipe,
					"tgl_berlaku" => $expDate,
					"qty" => 1,
					"harga" => $harga,
					"cashback" => $nominal,
					"notrans" => $NoTransNew,
					"noidentitas" => $NoIdent,
					"user" => $SessionName,
					"add_date" => $Date,
					"last_ip" => $getLastIP
					);
					$this->db->insert("ticket",$data);
					
					//INSERT VOUCHER
					if($NoIdent!="1234"){
						$data_voucher = array(
						"novoucher" => $noticket,
						"nominal" => $nominal,
						"expDate" => $expDate,
						"addDate" => $DateV,
						"userDate" => $SessionName,
						"notrans" => $NoTransNew,
						"jenis" => $jenis
						);
						$this->db->insert("voucher",$data_voucher);
					}
				}
			}
		}
		
		$this->ticket_print($getLastIP,$Date);
		//redirect('/transaksi/ticketing/');
	}
	
	function ticket_print($getLastIP,$Date){
		$data['lastip'] = $getLastIP;
		$data['date'] = $Date;
		
		$this->load->view('transaksi/ticket/ticket_print', $data);
	}
	
	function getGenereteNumberTicket($getLastIP){
		$getTgl			= $this->globalmodel->getQuery(" TglTrans from aplikasi");
		$tglApp			= $getTgl[0]['TglTrans'];
		$thn			= substr($tglApp,2,2);
		$bln			= substr($tglApp,5,2);
		$rand			= rand(11,99);
		$dataTicket		= $this->globalmodel->getQuery(" noticket,add_date from ticket where last_ip='$getLastIP' order by noticket desc limit 1");
		
		if(!empty($dataTicket)){
			$getblnticket	= substr($dataTicket[0]['add_date'],5,2);
			if($bln!=$getblnticket){
				$counter	= "0000001";
				$noskg		= $thn.$bln.$getLastIP.$counter.$rand;
			}else{
				$dtlastno		= $dataTicket[0]['noticket'];
				$getlastno		= substr($dtlastno,6,7);
				$noskg			= $getlastno + 1;
				if (strlen($noskg)=='1'){
					$counter	= "000000".$noskg;
				}elseif (strlen($noskg)=='2'){
					$counter	= "00000".$noskg;
				}elseif (strlen($noskg)=='3'){
					$counter	= "0000".$noskg;
				}elseif (strlen($noskg)=='4'){
					$counter	= "000".$noskg;
				}elseif (strlen($noskg)=='5'){
					$counter	= "0".$noskg;
				}elseif (strlen($noskg)=='6'){
					$counter	= $noskg;
				}else{
					$counter	= "0000001";
				}
			}
		}else{
			$counter	= "0000001";
		}
			$noskg		= $thn.$bln.$getLastIP.$counter.$rand;
			$cekNoticket = $this->globalmodel->getQuery(" * from ticket where noticket = '".$noskg."'");
			if(!empty($cekNoticket)){
				$this->getGenereteNumberTicket($getLastIP);
			}else{
				$noskg;
			}
		return $noskg;
	}
	
	function getGenereteNotransTicket($getLastIP){	
		$getTgl			= $this->globalmodel->getQuery(" TglTrans from aplikasi");
		$tglApp			= $getTgl[0]['TglTrans'];
		$thn			= substr($tglApp,2,2);
		$bln			= substr($tglApp,5,2);
		$dataTransTicket= $this->globalmodel->getQuery(" notrans,tanggal from ticket_head where last_ip='$getLastIP' order by id desc limit 1");
		
		//echo $this->db->last_query();die;
		if (!empty($dataTransTicket)){
			$getblntrans	= substr($dataTransTicket[0]['tanggal'],5,2);
			if($bln!=$getblntrans){
				$counter	= "00001";
				$noskg		= $thn.$bln.$getLastIP.$counter;
			}else{
				$dtlastno	= $dataTransTicket[0]['notrans'];
				$getlastno	= substr($dtlastno,6,5);
				$noskg		= $getlastno + 1;
				if (strlen($noskg)=='1'){
					$counter	= "0000".$noskg;
				}elseif (strlen($noskg)=='2'){
					$counter	= "000".$noskg;
				}elseif (strlen($noskg)=='3'){
					$counter	= "00".$noskg;
				}elseif (strlen($noskg)=='4'){
					$counter	= "0".$noskg;
				}elseif (strlen($noskg)=='5'){
					$counter	= $noskg;
				}else{
					$counter	= "00001";
				}
			}
		}else {
			$counter	= "00001";
		}
			$noskg		= $thn.$bln.$getLastIP.$counter;
			$cekNoticket = $this->globalmodel->getQuery(" * from ticket_head where notrans = '".$noskg."'");
			if(!empty($cekNoticket)){
				$this->getGenereteNotransTicket($getLastIP);
			}else{
				$noskg;
			}
		return $noskg;
	}
	
	function create_ticket(){
		$getDisplay = $this->globalmodel->getQuery(" * FROM ticket_display");
		$data['baris_1'] = $getDisplay[0]['baris_1'];
		$data['baris_2'] = $getDisplay[0]['baris_2'];
		$data['baris_3'] = $getDisplay[0]['baris_3'];
		$data['baris_4'] = $getDisplay[0]['baris_4'];
		$data['baris_5'] = $getDisplay[0]['baris_5'];

		$listTicket = $this->globalmodel->getQuery(" * from ticket where notrans='CR1707363' order by noticket ASC");
		$filename	='tiket';
		$ext		='tkt';
		$this->load->helper('print');
		
		$html = '';
		for($i=0;$i<count($listTicket);$i++){
			$data['no_ticket'] = $listTicket[$i]['noticket'];
			$data['harga_ticket'] = $listTicket[$i]['harga'];
			$data['cashback'] = $listTicket[$i]['cashback'];
			$data['Date'] = $Date;
			$noticket = $data['no_ticket'];
			
			//$html .= $this->load->view('transaksi/ticket/cetak_ticket_dos', $data, TRUE);
			
			
			$dataTicket = array(
			"status" => 1
			);
			$whereTicket = array(
			"noticket" => $noticket
			);
			$this->ticketingmodel->editStatusTicket("ticket",$dataTicket,$whereTicket);
		}
		$data['listtiket'] = $listTicket;
		$data['Date'] = $Date;
		$html = $this->load->view('transaksi/ticket/cetak_ticket_dos_test', $data, TRUE);
		header('Content-Type: application/tkt');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;
	}
	
	function get_tour_guide($KdTravel){
		if(empty($KdTravel)){
			$where = "";
		}else{
			$where = "WHERE KdTravel = '".$KdTravel."' ";
		}
		$cek_query	= $this->globalmodel->getQuery(" KdTourLeader,Nama FROM tourleader ".$where." ORDER BY Nama ASC");
		if(empty($cek_query)){
			$query = $this->globalmodel->getQuery(" KdTourLeader,Nama FROM tourleader ORDER BY Nama ASC");
		}else{
			$query = $this->globalmodel->getQuery(" KdTourLeader,Nama FROM tourleader ".$where." ORDER BY Nama ASC");
		}
		echo json_encode($query);
	}
	
	function get_jenis_ticket()
    {        
	     $idsubkategori = $this->input->post('tck');
	     $NoKassa = $this->input->post('kssa');
	     $session_name = $this->input->post('sess');
	     $store = $this->input->post('str');
	     
	     $sql = " 
	     		 m.*,
				  t.cashback 
				FROM
				  masterbarang m,
				  ticketlistconf t 
				WHERE m.PCode = t.PCode 
				  AND t.DayID = 1 
				  AND m.JenisBarang = 'TCK' 
				  AND m.`KdSubKategori`='$idsubkategori'
				ORDER BY m.KdSubDivisi,m.KdKategori ASC  ;
	     		";
	     
	     $list = $this->globalmodel->getQuery($sql);
	   
	     echo '
	       <table  width="100%" align="center" class="table table-bordered responsive" name="detail">
            <thead>
            <tr>
                <th width="30%">Categories</th>
                <th width="20%">Qty</th>
                <th width="15%">Harga</th>
                <th width="15%">Netto</th>
            </tr>
            </thead>
			';
			if(empty($list)){
					echo '<tr>
                      <td colspan="100%" align="center">
                         Jenis Ticket Tersebut Tidak Tersedia.
                      </td>
                    </tr>
                    ';
            }
				
			for($i=0;$i<count($list);$i++){
			echo '
			<tr>
                <td>
					<div style="font-size:20px;">'.$list[$i]['NamaLengkap'].'</div>
					<input type="hidden" name="NamaLengkap'.$i.'" id="NamaLengkap'.$i.'" value="'.$list[$i]['NamaLengkap'].'">
				</td>
				<td>
					<div style="float: left;" >
						<button type="button" class="btn btn-danger" style="float: left;" id="kurang'.$i.'" onclick="kurang_pesanan(this)">-</button>
							<input type="text" class="form-control size-1" style="width:55px; float: left; font-size: 15px;" name="qty1'.$i.'" id="qty1'.$i.'" value="0" onKeyUp="getTotal('.$i.');" onKeyDown="MoveNext(event, '.$i.', '.base_url().')"/>
						<button type="button" class="btn btn-green" style="float: left;" id="tambah'.$i.'" onclick="tambah_pesanan(this)">+</button>
					</div>&nbsp;
					<button type="button" class="btn btn-gold" onClick="resetQty('.$i.');"><i class="fa fa-refresh"></i></button>
                </td>
				<td>
                    <input type="text" class="form-control" style="font-size: 16px;text-align: right;" id="jualm1'.$i.'" name="jualm1'.$i.'" size="12" value="'.$list[$i]['Harga1c'].'" class="InputAlignRight">
                </td>
                <td>
                    <input type="text" class="form-control" style="font-size: 16px;text-align: right;" readonly id="netto1'.$i.'" name="netto1'.$i.'" size="12" value="0" class="InputAlignRight">
                </td>
                </tr>
				<input type="hidden" name="nmticket'.$i.'" id="nmticket'.$i.'" value="'.$list[$i]['PCode'].'">
				<input type="hidden" name="jenis'.$i.'" id="jenis'.$i.'" value="'.$list[$i]['KdSubDivisi'].'">
				<input type="hidden" name="tipe'.$i.'" id="tipe'.$i.'" value="'.$list[$i]['Tipe'].'">
				<input type="hidden" name="charge'.$i.'" id="charge'.$i.'" value="'.$list[$i]['Service_charge'].'">
				<input type="hidden" name="nominalVouch'.$i.'" id="nominalVouch'.$i.'" value="'.$list[$i]['cashback'].'">
                ';
               }
             echo '
             	<input type="hidden" name="jml" id="jml" value="'.count($list).'">
	            <input type="hidden" name="kassa" id="kassa" value="'.$NoKassa.'">
	            <input type="hidden" name="kasir" id="kasir" value="'.$session_name.'">
	            <input type="hidden" name="store" id="store" value="'.$store.'">
	            <input type="hidden" name="brgDetail" id="brgDetail" value="">
             </table>';
	      
    }
	
	function test(){
		$getDisplay = $this->globalmodel->getQuery(" * FROM ticket_display");
		$data['baris_1'] = $getDisplay[0]['baris_1'];
		$data['baris_2'] = $getDisplay[0]['baris_2'];
		$data['baris_3'] = $getDisplay[0]['baris_3'];
		$data['baris_4'] = $getDisplay[0]['baris_4'];
		$data['baris_5'] = $getDisplay[0]['baris_5'];

		$data['no_ticket'] = '1605000000002';
		$data['harga_ticket'] = 50000;
		$data['Date'] = '2016-12-01';
		$this->load->helper('print');
		$html = $this->load->view('transaksi/ticket/cetak_ticket_test',$data, TRUE); // jika ada printernya
		
		$filename	='cetak_tiket';
		$ext		='ctk';
		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		
		print $html;
	}
	
	function test2(){
        $mylib	= new globallib();
        $sign	= $mylib->getAllowList("add");
        $nm		= $mylib->getUser();
		$getDateApp	= $this->globalmodel->getGenereteNumberTicket(" TglTrans from aplikasi ");
		$date		= $getDateApp[0]['TglTrans'];
		$ipkassa	= $_SERVER['REMOTE_ADDR'];
		$todaytrans	= $this->session->userdata('Tanggal_Trans');
		list($thntrans, $blntrans, $tgltrans)	= explode("-",$todaytrans);
		$todayid	= date("w", mktime(0, 0, 0, $blntrans, $tgltrans, $thntrans));
		echo ' a ';		
        if ($sign == "Y") {
            $data['TotalNetto']			= 0;
            $data['Charge']				= 0;
            $data['ttlall']				= 0;
            $data['TotalQty']			= 0;
            echo ' b ';
            $data['store']				= $this->ticketingmodel->aplikasi();
            $data['mUang']				= $this->ticketingmodel->getCurrency();
            $data['NoKassa']			= $this->ticketingmodel->cekidkassa($ipkassa); // cek no kasir
            $data['track']				= $mylib->print_track();
            echo ' c ';
			$data['list']				= $this->ticketingmodel->getTicketList($todayid);
			$getTotal		 			= $this->ticketingmodel->totalTicketPerhari($date);
			$data['ttlticket']			= $getTotal[0]['total'];
			echo ' d ';
			
			$ip				= $ipkassa;
			$getKassa		= $this->globalmodel->getGenereteNumberTicket(" id_kassa from kassa where ip='$ip'");
			$getIP			= $getKassa[0]['id_kassa'];
			
			if(strlen($getIP)=='1'){
				$getLastIP = "0".$getIP;
			}else{
				$getLastIP = $getIP;
			}
			echo $getLastIP;
			
			$data['NoTrans']			= $this->getGenereteNotransTicket($getLastIP);
			echo ' f ';
			die;
			//PETUGAS STICKER
			$data['PetugasSticker']		= $this->globalmodel->getQuery(" a.`username`,a.`employee_name` FROM employee a LEFT JOIN `user` b ON a.`username`=b.`UserName` ORDER BY a.`employee_name` ASC");
			$data['TourTravel']			= $this->globalmodel->getQuery(" KdTravel,Nama FROM tourtravel ORDER BY Nama ASC");
			echo ' g ';
            $this->load->view('transaksi/ticket/tampil', $data);
        } else {
            $this->load->view('denied');
        }
    }

}
?>