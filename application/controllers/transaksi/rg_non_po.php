<?php
// Febri
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Rg_non_po extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        error_reporting(0);                              
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/persediaan/rg_non_po_model', 'penerimaanlain');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$id = $this->uri->segment(4);	
			$user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			
			$data["search_keyword"] = "";
			$data["search_gudang"] = "";
            $data["search_status"] = "";
			$resSearch = "";
			$arr_search["search"]= array();
			$id_search = "";
			if($id*1>0)
			{	
				$resSearch = $this->globalmodel->getSearch($id,"rg_non_po",$user);	
				$arrSearch = explode("&",$resSearch->query_string);
				
				$id_search = $resSearch->id;
				
				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					$search_gudung = explode("=", $arrSearch[1]); // search gudang
					$arr_search["search"]["gudang"] = $search_gudung[1];
                    $search_status = explode("=", $arrSearch[2]); // search status
                    $arr_search["search"]["status"] = $search_status[1];
					
					$data["search_keyword"] = $search_keyword[1];
					$data["search_gudang"] = $search_gudung[1];
                    $data["search_status"] = $search_status[1];
				}
			}
            
			foreach($gudang_admin as $key)
			{
				$arr_search["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
            
            $arr_search["search"]["gudang_admin"] = $arr_search["gudang_admin"];
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if($id_search) 
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/rg_non_po/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/rg_non_po/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}
			
            $data['mgudang'] = $this->penerimaanlain->getGudang($arr_search["gudang_admin"]);
            $data['msupplier'] = $this->penerimaanlain->getsupplier();
            $data['minmut'] = $this->penerimaanlain->getIntMut();
            
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            
            $thnbln = $data['tahun'] . $data['bulan'];
            
            $config['total_rows'] = $this->penerimaanlain->num_tabel_row($arr_search["search"]);
            $data['data'] = $this->penerimaanlain->getTabelList($config['per_page'], $page, $arr_search["search"]);
            
            $data['track'] = $mylib->print_track();
            
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
            $this->load->view('transaksi/persediaan/rg_non_po/tabellist', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }   

    function otoritas()
    {
        $this->load->view('transaksi/persediaan/rg_non_po/form_otoritas');
    }  

    function search()
    {
        $mylib = new globallib();
        
		$user = $this->session->userdata('username'); 
		
		// hapus dulu yah
		$this->db->delete('ci_query', array('module' => 'rg_non_po','AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "rg_non_po",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/transaksi/rg_non_po/index/'.$query_id.'');
		
	} 
	
    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            
            $user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            
			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
            
            $data['msupplier'] = $this->penerimaanlain->getsupplier();
			$data['view_proposal'] = "style='display:none'";
			$data['noproposal'] 	= $this->penerimaanlain->getProposal();
            $data['mgudang'] 	= $this->penerimaanlain->getGudang($arr_search["list_gudang_admin"]);
            $data['minmut'] 	= $this->penerimaanlain->getIntMut();
            $data['msatuan'] 	= $this->penerimaanlain->getSatuan();
            $data['track'] 		= $mylib->print_track();
            
	    	$this->load->view('transaksi/persediaan/rg_non_po/form_add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function edit_form($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] 		= $this->session->userdata('bulanaktif');
            $data['tahun'] 		= $this->session->userdata('tahunaktif');
            $data['header'] 	= $this->penerimaanlain->getHeader($id);
            $data['detail'] 	= $this->penerimaanlain->getDetail($id);
            $cek = $this->penerimaanlain->getIntmutpurpose($data['header']->PurposeId);
            
            if($cek->NeedProposal==0){
            	$data['view_proposal'] = "style='display:none'";
            }
            
			$user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            
			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
			
            $data['mgudang'] 	= $this->penerimaanlain->getGudang($arr_search["list_gudang_admin"]);
            $data['msupplier'] 	= $this->penerimaanlain->getsupplier();
            $data['noproposal'] 	= $this->penerimaanlain->getProposal();
            $data['minmut'] 	= $this->penerimaanlain->getIntMut();
            $data['msatuan']	= $this->penerimaanlain->getSatuan();
            $data['track'] 		= $mylib->print_track();
            
            $this->load->view('transaksi/persediaan/rg_non_po/form_edit', $data);

        } 
        else 
        {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
               
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_jumlah =  $this->input->post('v_ntotal');
        $v_ppn =  $this->input->post('v_ppn');
        $v_pppn =  $this->input->post('n_pppn');
        $v_nilaippn =  $this->input->post('v_nppn');
        $v_totala =  $this->input->post('v_gtotal');
        $v_supplier = $this->input->post('v_supplier');
        // $v_proposal = $this->input->post('v_proposal');
        $v_suratjalan = $this->input->post('v_suratjalan');
        $v_gudang = $this->input->post('v_gudang');
        $v_keterangan = $mylib->save_char($this->input->post('v_keterangan'));
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');
        // $status1 = $this->input->post('v_status');

        
        // detail pcode
		$pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_harga2 = $this->input->post('v_harga');
        $v_total1 = $this->input->post('v_total');
        $v_total2 = $this->input->post('v_ttotal');
        // $v_keterangan1 = $this->input->post('v_keterangan_detail');
		$v_qty1 = $this->input->post('v_qty');
		$v_satuan1 = $this->input->post('v_satuan');
		$v_keterangan_pcode1 = $this->input->post('v_keterangan_pcode');
		
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
		
		if($flag=="add")
        {
            
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_terima_header","NoDokumen", "RG", $xbulan, $xtahun);
            
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_type, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user, $v_proposal,$v_suratjalan,$v_supplier,$v_jumlah,$v_ppn,$v_nilaippn,$v_totala);
       		 
       		$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       	} 
        else if($flag=="edit")
        {
           
            // if($v_ppn !== ""){

            // }

        $sql    = "Select * from trans_terima_detail where NoDokumen = '$v_no_dokumen'";
        // print_r($sql);
        $qry = $this->db->query($sql);
        $result = $qry->result_array();
        // print_r($result);
        // die();
        $total = 0;
        $jumlah = 0;
        // $tes = $this->db->delete('mutasi', array('NoTransaksi' => $v_no_dokumen));
            // print_r($tes);
            // die();
        foreach($result as $row){
            $total+= floatval($row['Total']);
            $jumlah+= floatval($row['Jumlah']);
			// $data = array('NoTransaksi' => $row['NoDokumen'],
            //               'Jenis' => 'I',
            //               'KdTransaksi' => 'RG',
            //               'Gudang' => $row['KdGudang'],
            //               'Tanggal' => $mylib->ubah_tanggal($v_tgl_dokumen),
            //               'KodeBarang' => $row['PCode'],
            //               'Qty' => $row['Qty'],
            //               'Nilai' => $row['Harga'],
            //               'Status' =>1
			// 			);
						
			// //insert ke natura
            // $this->db->insert('mutasi', $data);
            //  $this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang, $pcode , $v_qty, $v_harga12, "RG", "I");
        }
        $ptotal = 0;
        $rtotal = 0;
        
            $datappn = $this->db->get_where('trans_terima_header', array('NoDokumen' => $v_no_dokumen));
            $ppn = $datappn->row_array();
            $pppn = floatval($ppn["PPn"]);
            
            $ptotal = $total * ($pppn/100);

            $rtotal = $ptotal+$total;
            
        	$v_no_dokumen = $this->input->post('v_no_dokumen');
        	
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_type, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user, $v_proposal,$v_suratjalan,$v_supplier);
        	
        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }
        
    	// hapus detail jika ada
    	//$this->db->delete('trans_pengeluaran_lain_detail', array('NoDokumen' => $v_no_dokumen)); 
        
        
                
        
        $counter=1;
        for($x=0;$x< count($pcode1);$x++)
		{
			$pcode 				= strtoupper(addslashes(trim($pcode1[$x])));
			$v_namabarang 		= $mylib->save_char($v_namabarang1[$x]);
            $v_qty 				= $mylib->save_int($v_qty1[$x]);
            $v_harga12          = $v_harga2[$x];
            $vpppn             = $v_pppn[$x];
            // $v_keterangan       = $v_keterangan1[$x];
            $v_total            = $v_total1[$x];
            $v_ttotal            = $v_total2[$x];
			$v_satuan			= $v_satuan1[$x];
            // $v_keterangan_pcode	= $mylib->save_char($v_keterangan_pcode1[$x]);
            $v_keterangan_pcode	= $v_keterangan_pcode1[$x];

           

			 
			if($pcode!="")
			{   
			
				//cari nilai atau harga1c di masterbarang untuk setiap PCodenya
				$harga = $this->penerimaanlain->getHarga($pcode);
				$nilai=$harga->harga1c;
                
                // if($v_status == '1' && $flag == 'edit'){

                //     $this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang, $pcode , $v_qty, $nilai, "RG", "I");
    
                // }

				//insert atau update ke mutasi dan stock
				// $this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang, $pcode , $v_qty, $v_harga12, "RG", "I");
				//$this->_update_stock($mylib->ubah_tanggal($v_tgl_dokumen), $v_gudang, $pcode, $v_qty, $nilai, "I");
								
				$detail 	= $this->penerimaanlain->cekGetDetail($pcode,$v_no_dokumen);
				
				//cocokan konversi terlebih dahulu
				$konversi = $this->penerimaanlain->getKonversi($pcode,$v_satuan);
				
				//jika konversi ada datanya
				if(!empty($konversi)){
					//jika Satuan_To sama dengan Satuan yang dipilih
					if($konversi->Satuan_To==$v_satuan){
						$QtyPcs = $v_qty;
					//jika tidak sama
					}else{
						$QtyPcs = $v_qty * $konversi->amount;
					}
				//jika tidak ada datanya konversi
				}else{
					$QtyPcs = $v_qty;
				}
				
				
				if($detail->PCode==$pcode)
				{
					$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_qty,$v_satuan,$QtyPcs,$v_keterangan_pcode,$detail->Qty,$detail->QtyPcs);
					$counter++;
				}
				else
				{
                    
					$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs,$v_keterangan_pcode,$v_supplier,$mylib->ubah_tanggal($v_tgl_dokumen),$v_gudang,$v_harga12,$v_total,$vpppn,$v_ttotal);
					$counter++;
				}
			}
        }
        if($v_status == '1'){
        // $this->db->select(*);
        
        
            // die();
        $sql    = "Select * from trans_terima_detail where NoDokumen = '$v_no_dokumen'";
        // print_r($sql);
        $qry = $this->db->query($sql);
        $result = $qry->result_array();
        // print_r($result);
        // die();
        $total = 0;
        $jumlah = 0;
        $tes = $this->db->delete('mutasi', array('NoTransaksi' => $v_no_dokumen));
            // print_r($tes);
            // die();
        foreach($result as $row){
            $total+= floatval($row['Total']);
            $jumlah+= floatval($row['Jumlah']);
			$data = array('NoTransaksi' => $row['NoDokumen'],
                          'Jenis' => 'I',
                          'KdTransaksi' => 'RG',
                          'Gudang' => $row['KdGudang'],
                          'Tanggal' => $mylib->ubah_tanggal($v_tgl_dokumen),
                          'KodeBarang' => $row['PCode'],
                          'Qty' => $row['Qty'],
                          'Nilai' => $row['Harga'],
                          'Status' =>1
						);
						
			//insert ke natura
            $this->db->insert('mutasi', $data);
            //  $this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang, $pcode , $v_qty, $v_harga12, "RG", "I");
        }
        $ptotal = 0;
        $rtotal = 0;
        
            $datappn = $this->db->get_where('trans_terima_header', array('NoDokumen' => $v_no_dokumen));
            $ppn = $datappn->row_array();
            $pppn = floatval($ppn["PPn"]);
            
            $ptotal = $total * ($pppn/100);

            $rtotal = $ptotal+$total;
            // echo'total + pajak : ';
            // print_r($rtotal);
            // echo'total :';
            // print_r($total);
            // echo'jumlah :';
            // print_r($jumlah);
            
            // die();
            
        $data1 = array(
            
            'Total' => $rtotal,
            'Jumlah' => $total,
            'NilaiPPn' => $ptotal
        );
            
        // $data1 = array(
            
        //     'Total' => $total,
        //     'Jumlah' => $jumlah
        // );
        
        $this->db->update('trans_terima_header', $data1, array('NoDokumen' => $v_no_dokumen));

        }
        

        // $this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang, $pcode , $v_qty, $v_harga12, "RG", "I");
        
       redirect('/transaksi/rg_non_po/edit_form/'.$v_no_dokumen.'');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $intmut, $gudang, $keterangan, $status, $flag, $base_url, $user, $proposal, $suratjalan, $supplier,$v_jumlah,$v_ppn,$v_nilaippn,$v_total)
    {   
        $this->penerimaanlain->locktables('trans_terima_header');
        
        $data = array(
            'NoDokumen' => $no_dokumen,
            'TglDokumen' => $tgl_dokumen,
            'KdGudang' => $gudang,
            'PoNo' => "",
            'KdSupplier' => $supplier,
            'NoSuratJalan' => $suratjalan,
            'Keterangan' => $keterangan,
            'Jumlah' => $v_jumlah,
            'PPn'   => $v_ppn,
            'NilaiPPn' => $v_nilaippn,
            'Total'=> $v_total,
            'Status' => "0",
            'AddDate' => date("Y-m-d H:i:s"),
            'AddUser' => $user,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
            
        );
        
        $this->db->insert('trans_terima_header', $data);
        
        $this->penerimaanlain->unlocktables();
        
    }

    function updateHeader($no_dokumen, $tgl_dokumen, $intmut, $gudang, $keterangan, $status, $flag, $base_url, $user, $proposal, $suratjalan, $supplier)
    {
        $this->penerimaanlain->locktables('trans_terima_header');

        $data = array(
            'NoDokumen' => $no_dokumen,
            'TglDokumen' => $tgl_dokumen,
            'KdGudang' => $gudang,
            'PoNo' => "",
            'KdSupplier' => $supplier,
            'NoSuratJalan' => $suratjalan,
            'Keterangan' => $keterangan,
            'Jumlah' => $vjumlah,
            // 'PPn'   => $v_ppn,
            // 'NilaiPPn' => $v_nilaippn,
            // 'Total'=> $v_total,
            'Status' => $status,
            'AddDate' => date("Y-m-d H:i:s"),
            'AddUser' => $user,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
        );
        
        if($status=="1")
        {
        	$mylib = new globallib();
        	
            $this->penerimaanlain->get_mutasi_stock($no_dokumen); 
        	
        	$to = "";
            $to_name = "";
            
            $header = $this->penerimaanlain->getHeader($no_dokumen);
            
            $sql = "
                	employee.email,        
                    employee.employee_name
                FROM
                    employee
                WHERE
                    1
                    AND employee.username IN('".$header->AddUser."', '".$header->EditUser."')
                ORDER BY
                    employee.employee_name ASC
            ";
            $arrData = $this->globalmodel->getQuery($sql);
            foreach($arrData as $val)
            {
                $to .= $val["email"].";";
                $to_name .= $val["employee_name"].";";
			}
                                
        	$subject = "Notifikasi DL ".$no_dokumen;
            $author  = "Auto System";
            
            $to .= "samsul@secretgarden.co.id;";
            $to_name .= "Samsul Hidayat;";
            
            $body  = "DL dengan nomor <b>'.$no_dokumen.'</b> telah berhasil dikirim";
            
			$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		}
        
        $this->db->update('trans_terima_header', $data, array('NoDokumen' => $no_dokumen));
        
    }

    function insertDetail($flag,$no_dokumen,$pcode,$namabarang,$qty,$satuan,$QtyPcs,$keterangan,$supplier,$tgldokumen,$v_gudang,$v_harga12,$v_total,$vppn,$v_ttotal)
    {
        $this->penerimaanlain->locktables('trans_terima_detail');

        if ($pcode) 
        {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'TglDokumen' => $tgldokumen,
                'KdSupplier' => $supplier,
                'PCode' => $pcode,
                'Qty' => $qty,
                'PPn' => $vppn,
                'Satuan' => $satuan,
                'Kdgudang' => $v_gudang,
                'Harga' => $v_harga12,
                'Total' => $v_total,
                'Jumlah' => $v_ttotal,
                'Keterangan' => $keterangan,
                'QtyPcs'=>$QtyPcs
            );
            
            $this->db->insert('trans_terima_detail', $data);
        } 
        
        $this->penerimaanlain->unlocktables();
    }

    function updateDetail($flag,$no_dokumen,$pcode,$qty,$satuan,$QtyPcs,$keterangan,$qty_tbl, $qtypcs_tbl)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);
    	$new_qtypcs = $mylib->save_int($QtyPcs)+$mylib->save_int($qtypcs_tbl);
    	 
        $this->penerimaanlain->locktables('trans_terima_detail');

        if ($pcode) 
        {
            $data = array(
                'Qty' => $new_qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$new_qtypcs,
                'Keterangan' => $keterangan
            );
            
            $this->db->update('trans_terima_detail', $data, array('NoDokumen' => $no_dokumen,'PCode' => $pcode));
        } 
        
        $this->penerimaanlain->unlocktables();
    }
    
    function _insert_mutasi($tgl, $notrans, $gudang, $kdbarang, $qty, $nilai, $kdtransaksi, $jenis){
		
		$data = array(
                    'NoTransaksi' => $notrans ,
                    'Jenis' => $jenis,
                    'KdTransaksi' => $kdtransaksi,
                    'Gudang' => $gudang,
                    'Tanggal' => $tgl,
                    'KodeBarang' => $kdbarang,
                    'Qty' => $qty,
                    'Nilai' => $nilai,
                    'Status' =>1
                );
        
        $this->db->insert('mutasi', $data);
	}
	
	function _update_stock($tgl, $gudang, $kdbarang, $qty, $nilai, $jenis){
		
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;
        
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $bulan; 
			$fieldnupdate = "GNKeluar" . $bulan; 
			$vqty = $qty*-1;
			$vnilai = $qty*$nilai*-1;
		}else{
			$fieldupdate = "GMasuk" .$bulan;
			$fieldnupdate = "GNMasuk" .$bulan;
			$vqty = $qty;
			$vnilai = $qty*$nilai;
		}
        
		$query = $this->db->get_where('stock', array(
		            'Tahun' => $tahun,
		            'KdGudang' => $gudang,
		            'PCode' => $kdbarang,
		            'Status' => 'G'
        			));

        $count = $query->num_rows(); 
        if($count>0){
			$sql = "Update stock set $fieldupdate = $fieldupdate+$qty, $fieldakhir=$fieldakhir+$vqty,
					$fieldnupdate = $fieldnupdate+$nilai, $fieldnakhir=$fieldnakhir+$vnilai	
					 where Tahun='$tahun' and KdGudang='$gudang' and PCode='$kdbarang'";
			$this->db->query($sql);
		}else{
			$data = array(
					'Tahun' => $tahun,
		            'KdGudang' => $gudang,
		            'PCode' => $kdbarang,
		            'Status' => 'G',
		            $fieldupdate => $qty,
		            $fieldnupdate => $nilai,
		            $fieldakhir => $vqty,
		            $fieldnakhir => $vnilai);
		    $this->db->insert('stock', $data);
		}
	}
	
	function _update_stock_jika_hapus_detail($tahun,$KdGudang,$pcode,$Qty,$nilai_lama_di_stock1,$field_sasaran1,$nilai_lama_di_stock2,$field_sasaran2)
    {
		
    	$jml_update_untuk_field1=$nilai_lama_di_stock1-$Qty;
		$jml_update_untuk_field2=$nilai_lama_di_stock2-$Qty;
        $this->penerimaanlain->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $field_sasaran1=>$jml_update_untuk_field1,
				$field_sasaran2=>$jml_update_untuk_field2
            );
            $where = array
		            (
		            'Tahun' => $tahun,
		            'KdGudang' => $KdGudang,
		            'PCode' => $pcode
		            ); 
			
            $this->db->update('stock', $data,$where );
        } 
        
        $this->penerimaanlain->unlocktables();
    }

	function delete_trans($id)
	{
		$detail = $this->penerimaanlain->ambilDetail($id);
			/*echo "<pre>";
			print_r($detail);
			echo "</pre>";die;*/
		//khusus update stock
		foreach($detail as $val){
		    $nodok = $val['NoDokumen'];
			$pcode=  $val['PCode'];
			
			$ambilQty = $this->penerimaanlain->ambilQty($pcode,$nodok);
		
			$Qty = $ambilQty->Qty;
			$KdGudang = $ambilQty->KdGudang;
			$TglDokumen = $ambilQty->TglDokumen;
			
			$pisah_periode			= explode("-",$TglDokumen);
			$tahun					= $pisah_periode[0];
			$bulan					= $pisah_periode[1];
			$hari					= $pisah_periode[2];
			
			$fieldakhir = "GAkhir" . $bulan;
			$fieldnakhir = "GNAkhir" . $bulan;
		
			//jenis untuk penerimaan lain adalah I (Input)
			$jenis='I';
			
			if($jenis=='O'){
				$fieldupdate = "GKeluar" . $bulan; 
				$fieldnupdate = "GNKeluar" . $bulan; 
				$vqty = $qty*-1;
				$vnilai = $qty*$nilai*-1;
			}else{
				$fieldupdate = "GMasuk" .$bulan;
				$fieldnupdate = "GNMasuk" .$bulan;
				$vqty = $qty;
				$vnilai = $qty*$nilai;
			}
			
				//ambil Qty di stock
				$field_stock = $this->penerimaanlain->cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir);	
				/*echo "<pre>";
				print_r($field_stock);
				echo "</pre>";die;*/
				$this->_update_stock_jika_hapus_detail($tahun,$KdGudang,$pcode,$Qty,$field_stock->$fieldupdate,$fieldupdate,$field_stock->$fieldakhir,$fieldakhir);
				
				//hapus mutasi
				$this->db->delete('mutasi', array('Tanggal'=>$TglDokumen,'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
		} 
		
		$this->db->delete('trans_penerimaan_lain', array('NoDokumen' => $id));
		$this->db->delete('trans_penerimaan_lain_detail', array('NoDokumen' => $id));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen '.$id.'</strong> berhasil','class' => 'success'));
	
		redirect('/transaksi/penerimaan_lain/'); 
	}

	function delete_detail()
	{
		$sid = $this->uri->segment(4);	
		$pcode = $this->uri->segment(5);	
		$nodok = $this->uri->segment(6);

		//pertama harus kurangi angka di stock
		//ambil Qty dari penerimaan_lain_detail
		// $ambilQty = $this->penerimaanlain->ambilQty($pcode,$nodok);
		
		// $Qty = $ambilQty->Qty;
		// $KdGudang = $ambilQty->KdGudang;
		// $TglDokumen = $ambilQty->TglDokumen;
		
		//     $pisah_periode					= explode("-",$TglDokumen);
		// 	$tahun					= $pisah_periode[0];
		// 	$bulan					= $pisah_periode[1];
		// 	$hari					= $pisah_periode[2];
			
		// $fieldakhir = "GAkhir" . $bulan;
		// $fieldnakhir = "GNAkhir" . $bulan;
		
		//jenis untuk penerimaan lain adalah I (Input)
        // $jenis='I';
		
        // if($jenis=='O'){
		// 	$fieldupdate = "GKeluar" . $bulan; 
		// 	$fieldnupdate = "GNKeluar" . $bulan; 
		// 	$vqty = $qty*-1;
		// 	$vnilai = $qty*$nilai*-1;
		// }else{
		// 	$fieldupdate = "GMasuk" .$bulan;
		// 	$fieldnupdate = "GNMasuk" .$bulan;
		// 	$vqty = $qty;
		// 	$vnilai = $qty*$nilai;
		// }
		
			//ambil Qty di stock
			// $field_stock = $this->penerimaanlain->cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir);	
			/*echo "<pre>";
			print_r($field_stock);
			echo "</pre>";die;*/
			// $this->_update_stock_jika_hapus_detail($tahun,$KdGudang,$pcode,$Qty,$field_stock->$fieldupdate,$fieldupdate,$field_stock->$fieldakhir,$fieldakhir);
		
			//Hapus Mutasi
			// $this->db->delete('mutasi', array('Tanggal' => $TglDokumen, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
            
            

			//Hapus Di trans_penerimaan_lain_detail
            $this->db->delete('trans_terima_detail', array('sid' => $sid,'PCode' => $pcode,'NoDokumen' => $nodok));
            
            //update_header
            $sql    = "Select * from trans_terima_detail where NoDokumen = '$nodok'";
            // print_r($sql);
            $qry = $this->db->query($sql);
            $result = $qry->result_array();
            // print_r($result);
            // die();
            $total = 0;
            $jumlah = 0;
            // $tes = $this->db->delete('mutasi', array('NoTransaksi' => $v_no_dokumen));
                // print_r($tes);
                // die();
            foreach($result as $row){
                $total+= floatval($row['Total']);
                $jumlah+= floatval($row['Jumlah']);
                // $data = array('NoTransaksi' => $row['NoDokumen'],
                //               'Jenis' => 'I',
                //               'KdTransaksi' => 'RG',
                //               'Gudang' => $row['KdGudang'],
                //               'Tanggal' => $mylib->ubah_tanggal($v_tgl_dokumen),
                //               'KodeBarang' => $row['PCode'],
                //               'Qty' => $row['Qty'],
                //               'Nilai' => $row['Harga'],
                //               'Status' =>1
                //             );
                            
                //insert ke natura
                // $this->db->insert('mutasi', $data);
                //  $this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang, $pcode , $v_qty, $v_harga12, "RG", "I");
            }
            $ptotal = 0;
            $rtotal =0;
            
                $datappn = $this->db->get_where('trans_terima_header', array('NoDokumen' => $nodok));
                $ppn = $datappn->row_array();
                $pppn = floatval($ppn["PPn"]);
                $ptotal = $total * ($pppn/100);

                $rtotal = $ptotal+$total;
                // print_r($rtotal);
                // echo'<br>';
                // print_r($total);
                // echo'<br>';
                // print_r($jumlah);
                
                
                
            $data1 = array(
                
                'Total' => $rtotal,
                'Jumlah' => $total,
                'NilaiPPn' => $ptotal
            );
            
            $cek=$this->db->update('trans_terima_header', $data1, array('NoDokumen' => $nodok));
            // print_r($cek);
            //end

			$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode '.$pcode.'</strong> berhasil','class' => 'success'));
			
		redirect('/transaksi/rg_non_po/edit_form/'.$nodok.''); 
	}
	    
    function satuan()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->penerimaanlain->getSatuanDetail($pcode);
     
     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }     
    }

    function harga()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->penerimaanlain->getHarga($pcode);
     print_r($query);
     return $query;
    }

    function intmutpurpose()
    {        
     $tipe = $this->input->post('tipes');
     $cek = $this->penerimaanlain->getIntmutpurpose($tipe);

     echo $cek->NeedProposal;
        
    }
	
	function viewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "TERIMA BARANG NON PO";
		
		$data["header"] 	= $this->penerimaanlain->getHeader($nodok);
		$data["detail"] 	= $this->penerimaanlain->getDetail($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_rg_non_po', $data);
	}
	
	function doPrint()
	{
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "penerimaan-lain.txt";
		
		$header = $this->penerimaanlain->getHeader($nodok);
		$detail = $this->penerimaanlain->getDetail($nodok);
		
		$note_header = substr($header->Keterangan,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,40);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            $arr_data["detail_satuan"][$counter] = $val["Satuan"];
            $arr_data["detail_keterangan"][$counter] = substr($val["Keterangan"],0,35);
			
			 $counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "TERIMA BARANG NON PO";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                                                   
            $echo .= $arr_epson["cond"].$nama_dokumen;
            $echo.="\r\n";
            
            $echo.=$arr_epson["ncond"];        
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."No : ".$header->NoDokumen;
            $echo.="\r\n";       
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->TglDokumen;
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglDokumen));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Gudang";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                $echo.=$arr_epson["cond"].$header->nama_gudang;
               
                $echo.="\r\n";    
            }

            // baris 2
            // {
            //     $echo.=$arr_epson["cond"]."Type";
            //     $limit_spasi = (20-2);
            //     for($i=0;$i<($limit_spasi-strlen("Type"));$i++)
            //     {
            //         $echo.=" ";
            //     }
            //     $echo.=": ";
                
            //     $echo.=$arr_epson["cond"].$header->purpose;
                
            //     $limit_spasi = 65;
            //     for($i=0;$i<($limit_spasi-strlen($header->purpose));$i++)
            //     {
            //         $echo.=" ";
            //     }
                
            //     $echo.="\r\n";    
            // }
            
            $echo.=$arr_epson["cond"];  
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 45;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 14;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Keterangan";
            $limit_spasi = 35;
            for($i=0;$i<($limit_spasi-strlen("Keterangan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
            	$detail_keterangan = $arr_data["detail_keterangan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 45;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty);
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($qty));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$detail_keterangan;
                    $limit_spasi = 35;
                    for($i=0;$i<($limit_spasi-strlen($detail_keterangan));$i++)
                    {
                        $echo.=" ";
                    }
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->Keterangan;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Dibuat Oleh";

            $limit_spasi = 27;
            for($i=0;$i<($limit_spasi-strlen("Dibuat Oleh"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Hormat Kami";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Hormat Kami"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Diketahui Oleh";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Diketahui Oleh"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Diterima Oleh";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Diterima Oleh"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (  $user  )         (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"penerimaan-lain");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n"; 
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"penerimaan-lain");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "penerimaan-lain",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='penerimaan-lain-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
}

?>