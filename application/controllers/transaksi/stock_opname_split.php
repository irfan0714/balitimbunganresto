<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Stock_opname_split extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/persediaan/stock_opname_split_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
			
            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "stock_opname", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $tgl1 = explode("=", $arrSearch[2]); 
                    $arr_search["search"]["tgl1"] = $mylib->ubah_tanggal($tgl1[1]);
                    $tgl2 = explode("=", $arrSearch[3]); 
                    $arr_search["search"]["tgl2"] = $mylib->ubah_tanggal($tgl2[1]);

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_tgl1"] = $tgl1[1];
                    $data["search_tgl2"] = $tgl2[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/stock_opname_split/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/stock_opname_split/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->stock_opname_split_model->getGudang();

            $config['total_rows'] = $this->stock_opname_split_model->num_stock_opname_row($arr_search["search"]);
            $data['data'] = $this->stock_opname_split_model->getStockOpnameList($config['per_page'], $page, $arr_search["search"]);
			
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/persediaan/stock_opname_split/stock_opname_split_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'stock_opname', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&v_tgl_dokumen1=".$this->input->post('v_tgl_dokumen1');
		$search_value .= "&v_tgl_dokumen2=".$this->input->post('v_tgl_dokumen2');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "stock_opname",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/stock_opname_split/index/' . $query_id . '');
    }

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			$data['gudang'] = $this->stock_opname_split_model->getGudang();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/persediaan/stock_opname_split/add_stock_opname_split', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_stock_opname_split($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
		$user = $this->session->userdata('username');
        if ($sign == "Y") {
            $id = $this->uri->segment(4);

			//pengambilan dari opname_detail
			$opname_detail = $this->stock_opname_split_model->getOpnameDetail($id);
			foreach($opname_detail AS $row){
				
				$arr_curr["data_QtyFisik"][$row["PCode"]] = $row["QtyFisik"];
				$arr_curr["data_QtyProgram"][$row["PCode"]] = $row["QtyProgram"];
			}
			
			$data['header'] = $this->stock_opname_split_model->getHeader($id);
			
			//get stock
			$stocks = $this->stock_opname_split_model->getStocks($data['header']->KdGudang);
			foreach($stocks AS $key){
				
				$arr_data["SubKategori_PCode"][$key["KdSubKategori"]][$key["PCode"]] = $key["PCode"];
				$arr_data["NamaLengkap"][$key["PCode"]] = $key["NamaLengkap"];
				$arr_data["satuan"][$key["PCode"]] = $key["SatuanSt"];            
				$arr_data["list_KdSubKategori"][$key["KdSubKategori"]] = $key["KdSubKategori"];
				$arr_data["NamaSubKategori"][$key["KdSubKategori"]] = $key["NamaSubKategori"];
				$arr_data["list_pcode"][$key["PCode"]] = $key["PCode"];
			}
			
			$data['SubKategori_PCode'] = $arr_data["SubKategori_PCode"];
			$data['NamaLengkaps'] = $arr_data["NamaLengkap"];
			$data['satuans'] = $arr_data["satuan"];
			$data['list_KdSubKategori']=$arr_data["list_KdSubKategori"];
			$data['NamaSubKategoris'] = $arr_data["NamaSubKategori"];
			
			$where_pcode = $mylib->where_array($arr_data["list_pcode"], "PCode", "in");
			$where_KdBarang = $mylib->where_array($arr_data["list_pcode"], "KodeBarang", "in");
			
			$data['get_stock'] = $mylib->get_stock($data['header']->KdGudang, $data['header']->dates, $arr_data["list_pcode"], $where_pcode, $where_KdBarang);
			$data['datafisik'] = $arr_curr;
            $data['gudang'] = $this->stock_opname_split_model->getGudang();
			$data['ses_login']=$user;
			if(empty($opname_detail)){
            $data['view_action_button'] = "style='display:none'";
			}else{
			$data['view_action_button'] = "style='display:'";
			}
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/persediaan/stock_opname_split/edit_stock_opname_split', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
	
	function excel_stock_opname_split($id,$jenis)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);

			//pengambilan dari opname_detail
			$opname_detail = $this->stock_opname_split_model->getOpnameDetail($id);
			foreach($opname_detail AS $row){
				
				$arr_curr["data_Sid"][$row["PCode"]] = $row["Sid"];
				$arr_curr["data_QtyFisik"][$row["PCode"]] = $row["QtyFisik"];
				$arr_curr["akhir"][$row["PCode"]] = $row["QtyProgram"];
			}
			
			$data['header'] = $this->stock_opname_split_model->getHeader($id);
			
			//get stock
			$stocks = $this->stock_opname_split_model->getStocks($data['header']->KdGudang);
			foreach($stocks AS $key){
				
				$arr_data["SubKategori_PCode"][$key["KdSubKategori"]][$key["PCode"]] = $key["PCode"];
				$arr_data["NamaLengkap"][$key["PCode"]] = $key["NamaLengkap"];
				$arr_data["satuan"][$key["PCode"]] = $key["SatuanSt"];            
				$arr_data["list_KdSubKategori"][$key["KdSubKategori"]] = $key["KdSubKategori"];
				$arr_data["NamaSubKategori"][$key["KdSubKategori"]] = $key["NamaSubKategori"];
				$arr_data["list_pcode"][$key["PCode"]] = $key["PCode"];
			}
			
			$data['SubKategori_PCode'] = $arr_data["SubKategori_PCode"];
			$data['NamaLengkaps'] = $arr_data["NamaLengkap"];
			$data['satuans'] = $arr_data["satuan"];
			$data['list_KdSubKategori']=$arr_data["list_KdSubKategori"];
			$data['NamaSubKategoris'] = $arr_data["NamaSubKategori"];
			
			$where_pcode = $mylib->where_array($arr_data["list_pcode"], "PCode", "in");
			$where_KdBarang = $mylib->where_array($arr_data["list_pcode"], "KodeBarang", "in");
			
			if($jenis=="1"){
			$data['get_stock'] = $mylib->get_stock($data['header']->KdGudang, $data['header']->dates, $arr_data["list_pcode"], $where_pcode, $where_KdBarang);
			}else{
			$data['get_stock']=$arr_curr;
			}
			$data['datafisik'] = $arr_curr;
			$data['sid'] = $arr_curr;
            $data['gudang'] = $this->stock_opname_split_model->getGudang();	
            $data['view_action_button'] = "style='display:'";
            $data['track'] = $mylib->print_track();
			
			header('Content-Type: application/vnd.ms-excel');
		    header('Content-Disposition: attachment; filename="stock-opname-split.xls";');
					
            $this->load->view('transaksi/persediaan/stock_opname_split/export_excel_split', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
        
    function view_stock_opname_split($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);

			//pengambilan dari opname_detail
			$opname_detail = $this->stock_opname_split_model->getOpnameDetail($id);
			foreach($opname_detail AS $row){
				
				$arr_curr["data_QtyFisik"][$row["PCode"]] = $row["QtyFisik"];
				$arr_curr["akhir"][$row["PCode"]] = $row["QtyProgram"];
			}
			
			$data['header'] = $this->stock_opname_split_model->getHeader($id);
			
			//get stock
			$stocks = $this->stock_opname_split_model->getStocks($data['header']->KdGudang);
			foreach($stocks AS $key){
				
				$arr_data["SubKategori_PCode"][$key["KdSubKategori"]][$key["PCode"]] = $key["PCode"];
				$arr_data["NamaLengkap"][$key["PCode"]] = $key["NamaLengkap"];
				$arr_data["satuan"][$key["PCode"]] = $key["SatuanSt"];            
				$arr_data["list_KdSubKategori"][$key["KdSubKategori"]] = $key["KdSubKategori"];
				$arr_data["NamaSubKategori"][$key["KdSubKategori"]] = $key["NamaSubKategori"];
				$arr_data["list_pcode"][$key["PCode"]] = $key["PCode"];
			}
			
			$data['SubKategori_PCode'] = $arr_data["SubKategori_PCode"];
			$data['NamaLengkaps'] = $arr_data["NamaLengkap"];
			$data['satuans'] = $arr_data["satuan"];
			$data['list_KdSubKategori']=$arr_data["list_KdSubKategori"];
			$data['NamaSubKategoris'] = $arr_data["NamaSubKategori"];
			
			$where_pcode = $mylib->where_array($arr_data["list_pcode"], "PCode", "in");
			$where_KdBarang = $mylib->where_array($arr_data["list_pcode"], "KodeBarang", "in");
			
			$data['get_stock'] = $arr_curr;
			$data['datafisik'] = $arr_curr;
            $data['gudang'] = $this->stock_opname_split_model->getGudang();
			if(empty($opname_detail)){
            $data['view_action_button'] = "style='display:none'";
			}else{
			$data['view_action_button'] = "style='display:'";
			}
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/persediaan/stock_opname_split/view_stock_opname_split', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_no_dokumen = $this->input->post('v_no_dokumen');
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_warehouse = $this->input->post('v_gudang');
        $v_keterangan = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $v_export_excel = $this->input->post('v_export_excel');
        $v_approve = $this->input->post('v_approve');
        $v_reject = $this->input->post('v_reject');
        $v_Approval_Remarks = $this->input->post('v_Approval_Remarks');
        $user = $this->session->userdata('username');
        
		$data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        list($tgl, $bln, $thn) = explode('-',$v_tgl_dokumen);

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Delivery Order
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "opname_header_split","NoDokumen", "SO", $bln, $thn);
			
			//kedua masukkan di Delivery Order Header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen),$v_warehouse, $v_keterangan,$user);
		   	
		   	redirect('/transaksi/stock_opname_split/edit_stock_opname_split/' . $v_no_dokumen . '');			
		} 
		
		
		else if ($flag == "edit") 
		{
		  if($v_export_excel==1)
	        {
	           redirect('/transaksi/stock_opname_split/excel_stock_opname_split/' . $v_no_dokumen . '/1');
			}else if($v_export_excel==2){
	           redirect('/transaksi/stock_opname_split/excel_stock_opname_split/' . $v_no_dokumen . '/2');
			}else{
					
				
				
				
			$this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen),$v_warehouse, $v_keterangan,$user, $v_status);
		   	//hapus terlebuh dahulu
		   	$this->db->delete('opname_detail_split',array('NoDokumen' => $v_no_dokumen));
		   	//ambil PCode
		   	$PCode= $this->stock_opname_split_model->getDetailList($v_warehouse);
            for ($x = 0; $x < count($PCode); $x++) 
			{
				$PCode1 = $PCode[$x]['PCode'];
				$data=array(
						'NoDokumen' => $v_no_dokumen,
		                'PCode' => $PCode1,
		                'QtyFisik' => $this->input->post('v_QtyFisik_'.$PCode1),
		                'QtyProgram' => $this->input->post('v_QtyProgram_'.$PCode1)
						);
				$this->db->insert('opname_detail_split',$data);
				
			}
			
			if($v_approve=='1'){
				
				$data=array(
				'Approval_By'=>$user,
	            'Approval_Date'=>date('Y-m-d'),
	            'Approval_Status'=>1
				);
				$this->db->update('opname_header_split',$data,array('NoDokumen' => $v_no_dokumen));
				
				//insert ke mutasi
				//ambil PCode
			   	$PCode= $this->stock_opname_split_model->getDetailList($v_warehouse);
	            for ($x = 0; $x < count($PCode); $x++) 
				{
					$PCode1 = $PCode[$x]['PCode'];
					$fisik = $this->input->post('v_QtyFisik_'.$PCode1);
					$program = $this->input->post('v_QtyProgram_'.$PCode1);
					
					if($fisik > $program){
						$selisih = $fisik - $program;
						$jenis = "I";
						$gudang = $v_warehouse;
						$gudang_Tujuan = $v_warehouse;
					}else{
						$selisih = $program - $fisik;
						$jenis = "O";
						$gudang = $v_warehouse;
						$gudang_Tujuan = "";
					}
					
					$data_mutasi=array(
							  'NoKassa' => '0',
		                      'NoTransaksi' => $v_no_dokumen,
		                      'Jenis' => $jenis,
		                      'KdTransaksi' => 'SO',
		                      'Gudang' => $gudang,
		                      'GudangTujuan' => $gudang_Tujuan,
		                      'Tanggal' => $mylib->ubah_tanggal($v_tgl_dokumen),
		                      'KodeBarang' => $PCode1,
		                      'Qty' => $selisih,
		                      'Nilai' => '0',
		                      'Status' => '1',
		                      'Kasir' => '',
		                      'Keterangan' => '',
		                      'HPP' => '0',
		                      'PPN' => '0',
		                      'Service_charge' => '0'
							);
					$this->db->insert('mutasi',$data_mutasi);
					
					
					//insert ke stock
					$pecah = explode('-',$v_tgl_dokumen);
					$bulan = $pecah[1];
					$tahun = $pecah[2];
					
					if($jenis == "I"){
						$in = $selisih;
						$out = 0;
					}else{
						$out = $selisih;
						$in = 0;
					}
					
					$masuk  = "GMasuk".$bulan;
					$keluar = "GKeluar".$bulan;
					$awal = "GKeluar".$bulan;
					$akhir = "GAkhir".$bulan;
					
					//ambil nilai stock untuk ditambah
					$stock = $this->stock_opname_split_model->getStock($tahun,$v_warehouse,$PCode1,$bulan);
					
					$data_stock=array(
							  $masuk => $stock[0]['masuk'] + $in,
							  $keluar => $stock[0]['keluar'] + $out,
							  $akhir => $stock[0]['akhir']+$in-$out
							);
							
					$where_stock=array(
							  'Tahun' => $tahun,
							  'KdGudang' =>$v_warehouse,
							  'PCode'=>$PCode1
							);
							
					$this->db->update('stock',$data_stock,$where_stock);
					
					if($bulan==12){
						$blndepan = 1;
						$thndepan = $tahun+1;
					}else{
						$blndepan = $bulan+1;
						$thndepan = $tahun;
					}
					if($blndepan<10)
						$blndepan = '0'.$blndepan;
					
					$stockdepan = $this->stock_opname_split_model->getStock($tahun,$v_warehouse,$PCode1,$blndepan);
					
					$awaldepan  = "GAwal".$blndepan;
					$akhirdepan = "GAkhir".$blndepan;
					$data_stock_depn =array(
							  $awaldepan => $stock[0]['akhir']+$in-$out,
							  $akhirdepan => $stockdepan[0]['akhir']+$in-$out
							);
							
					$where_stock=array(
							  'Tahun' => $thndepan,
							  'KdGudang' =>$v_warehouse,
							  'PCode'=>$PCode1
							);
							
					$this->db->update('stock',$data_stock_depn,$where_stock);
				}
			}
			
			if($v_reject=='1'){
				
				$data=array(
				'Status'=>0,
				'Approval_By'=>$user,
	            'Approval_Date'=>date('Y-m-d'),
	            'Approval_Status'=>2,
	            'Approval_Remarks'=>$v_Approval_Remarks
				);
				$this->db->update('opname_header_split',$data,array('NoDokumen' => $v_no_dokumen));
			
			}
            
            /*if($v_status==1)
	            {
					$mylib = new globallib();
	        		
	        		$url = "http://sys.bebektimbungan.com/index.php/transaksi/stock_opname/edit_stock_opname/".$v_no_dokumen;
	        		            	                                
		        	$subject = "Notifikasi Stock Opname ".$v_no_dokumen;
		            $author  = "Auto System";
		            
		            $to .= "irf@vci.co.id;tny@vci.co.id;william@secretgarden.co.id;";
		            $to_name .= "";
		            
		            $body  = "Dear ".$to_name.",<br><br>";
            		$body .= "Mohon Approval untuk SO No.".$v_no_dokumen."<br><br>";
            		$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat SO Atau Copy Paste Link ini di broswer anda : ".$url;
            
					$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
				}*/
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        	redirect('/transaksi/stock_opname_split/edit_stock_opname_split/' . $v_no_dokumen . '');
        }

       } 
        
    }

    function insertNewHeader($v_no_dokumen, $v_tgl_dokumen ,$v_warehouse, $v_keterangan,$user) 
    {
        
        $this->stock_opname_split_model->locktables('opname_header_split');
        $datas = array(
                            'NoDokumen' => $v_no_dokumen,
			                'TglDokumen' => $v_tgl_dokumen,
			                'KdGudang' => $v_warehouse,
                            'Keterangan' => $v_keterangan,
                            'Status' => '0',
                            'AddDate' => date('Y-m-d'),
			                'AddUser' => $user,
			                'EditDate' => date('Y-m-d'),
			                'EditUser' => $user
        );
  
        $this->db->insert('opname_header_split', $datas);

        $this->stock_opname_split_model->unlocktables();

    }
    
  
    function updateHeader($v_no_dokumen, $v_tgl_dokumen ,$v_warehouse, $v_keterangan,$user, $v_status) 
    {
        $this->stock_opname_split_model->locktables('opname_header');

        $datas = array(
        					'TglDokumen' => $v_tgl_dokumen,
			                'KdGudang' => $v_warehouse,
                            'Keterangan' => $v_keterangan,
                            'Status' => $v_status,
                            'AddDate' => date('Y-m-d'),
			                'AddUser' => $user,
			                'EditDate' => date('Y-m-d'),
			                'EditUser' => $user
        );
  
        $this->db->update('opname_header', $datas, array('NoDokumen' => $v_no_dokumen));
        
        $this->stock_opname_split_model->unlocktables();
    }

    function insertDetail($flag, $no_dokumen, $pcode, $qty, $user , $v_satuan, $QtyPcs) 
    {
        $this->stock_opname_split_model->locktables('deliveryorderdetail');

        if ($pcode) {
            $data = array(
                'dono' => $no_dokumen,
                'inventorycode' => $pcode,
                'quantity' => $qty,
				'satuan'=>$v_satuan,
				'QtyPcs'=>$QtyPcs,
                'adddate'=>date('Y-m-d'),
                'adduser'=>$user
            );

            $this->db->insert('deliveryorderdetail', $data);
        }

        $this->stock_opname_split_model->unlocktables();
    }
	
	function insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field) 
    {
        $this->stock_opname_split_model->locktables('stock');
        if ($pcode) {
            $data = array(
                'Tahun' => $thn_periode,
                'KdGudang' => $v_warehouse,
                'PCode' => $pcode,
                $tabel_field=>$v_qty
            );

            $this->db->insert('stock', $data);
        }

        $this->stock_opname_split_model->unlocktables();
    }
   	
	function insertMutasi($v_no_dokumen, $v_warehouse, $v_tgl_dokumen, $pcode, $v_qty, $user, $v_note)
    {
        $this->stock_opname_split_model->locktables('mutasi');

        if ($pcode) {
            $data = array(
                'NoTransaksi' => $v_no_dokumen,
                'Jenis' =>'I',
				'KdTransaksi' => 'R',
				'Gudang' => $v_warehouse,
				'Tanggal'=>$v_tgl_dokumen,
				'KodeBarang'=>$pcode,
				'Qty'=>$v_qty,
				'Status'=>1,
                'Kasir'=>$user,
				'Keterangan'=> $v_note
            );

            $this->db->insert('mutasi', $data);
        }

        $this->stock_opname_split_model->unlocktables();
    }
	
    function updateDetail($flag,$no_dokumen,$pcode,$qty,$qty_tbl,$user, $satuan, $QtyPcs)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);
    	 
        $this->stock_opname_split_model->locktables('deliveryorderdetail');

        if ($pcode) 
        {
            $data = array(
                'quantity' => $new_qty,
				'satuan'=>$satuan,
				'QtyPcs'=>$QtyPcs,
                'editdate'=>date('Y-m-d'),
                'edituser'=>$user
            );
            
            $this->db->update('deliveryorderdetail', $data, array('dono' => $no_dokumen,'inventorycode' => $pcode));
        } 
        
        $this->stock_opname_split_model->unlocktables();
    }
	
	function updateStock($thn_periode,$v_warehouse,$pcode,$jml_stock,$v_qty,$tabel_field)
    {
    	$jml_tambah= $jml_stock + $v_qty;
        $this->stock_opname_split_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_tambah
            );
            
            $this->db->update('stock', $data, array('Tahun' => $thn_periode,'KdGudang' => $v_warehouse,'PCode' => $pcode));
        } 
        
        $this->stock_opname_split_model->unlocktables();
    }
    
    function updateStock2($thn_periode,$v_warehouse,$market_pcode,$detail_quantity,$stock_tabel_field,$market_qty,$tabel_field)
    {
    	$jml_update=($stock_tabel_field-$detail_quantity)+$market_qty;
        $this->stock_opname_split_model->locktables('stock');

        if ($market_pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $market_pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->stock_opname_split_model->unlocktables();
    }
    
    function updateMutasi($v_no_dokumen,$v_warehouse,$v_tgl_dokumen,$market_pcode,$market_qty)
    {
    	$this->stock_opname_split_model->locktables('mutasi');

        if ($market_pcode) 
        {
            $data = array(
                'Qty'=>$market_qty
            );
            
            $where= array(
            'NoTransaksi'=>$v_no_dokumen,
            'Jenis'=>'I',
            'KdTransaksi'=>'R',
            'Gudang'=>$v_warehouse,
            'Tanggal'=>$v_tgl_dokumen,
            'KodeBarang'=>$market_pcode            
            );
            
            $this->db->update('mutasi', $data, $where);
        } 
        
        $this->stock_opname_split_model->unlocktables();
    }
	
	function delete_trans($id) 
    {
	        //hapus opname_header
			$this->db->delete('opname_header',array('NoDokumen'=>$id));
			//hapus opname_detail
			$this->db->delete('opname_detail',array('NoDokumen'=>$id));
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/stock_opname_split/');
    }

    function delete_detail() 
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);
		
        //pertama harus kurangi angka di stock
		 //ambil qty dari deliveryorderdetail
			$detail2 = $this->stock_opname_split_model->cekGetDetail2($pcode,$nodok);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  if($bln_periode=='01'){
				$tabel_field='GKeluar01';
			}else if($bln_periode=='02'){
				$tabel_field='GKeluar02';
			}else if($bln_periode=='03'){
				$tabel_field='GKeluar03';
			}else if($bln_periode=='04'){
				$tabel_field='GKeluar04';
			}else if($bln_periode=='05'){
				$tabel_field='GKeluar05';
			}else if($bln_periode=='06'){
				$tabel_field='GKeluar06';
			}else if($bln_periode=='07'){
				$tabel_field='GKeluar07';
			}else if($bln_periode=='08'){
				$tabel_field='GKeluar08';
			}else if($bln_periode=='09'){
				$tabel_field='GKeluar09';
			}else if($bln_periode=='10'){
				$tabel_field='GKeluar10';
			}else if($bln_periode=='11'){
				$tabel_field='GKeluar11';
			}else if($bln_periode=='12'){
				$tabel_field='GKeluar12';
			}			
			//ambil GKeluar di stock
			$stock2 = $this->stock_opname_split_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			
		   //hapus dimutasi
			$this->db->delete('mutasi', array('Tanggal' => $v_tgl_dokumen, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
			
		    //baru hapus di deliveryorder
			$this->db->delete('deliveryorderdetail', array('sid' => $detail2->sid, 'inventorycode' => $pcode, 'dono' => $nodok));
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/delivery_order/edit_delivery_order/' . $nodok . '');
    }           

	function updateStock3($thn_periode,$v_warehouse,$pcode,$detail_quantity,$stock_tabel_field,$tabel_field)
    {
    	$jml_update=$stock_tabel_field-$detail_quantity;
        $this->stock_opname_split_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->stock_opname_split_model->unlocktables();
    }
    
    function upload($dokumen) {
		$data['NoDokumen']=$dokumen;
		$this->load->view('transaksi/persediaan/stock_opname_split/tambahform', $data);

	}
	
	function uploadproses() {
		$config['upload_path']		= './uploads/';
		$config['allowed_types']	= 'txt|csv';
		$config['max_size']			= '8192';
		$config['overwrite']		= true;
		$NoDokumen = $this->input->post('NoDokumen');
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()) {
			$data['errormsg']	= $this->upload->display_errors();
			$this->load->view('transaksi/persediaan/stock_opname_split/tambahform', $data);
		} else {
			$dataupload		= $this->upload->data();
			$filelocation	= $dataupload['full_path'];
			$string			= read_file($filelocation);
			$string_arr		= explode("\n",$string);
			
			//echo "<pre>";print_r($string_arr);echo "</pre>";die;
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			foreach ($string_arr as $sebaris) {				
			
				
					list($a, $pcode, $c, $QtyFisik, $e, $f, $g)	= explode(";",$sebaris);
					$kodebarang =explode('#',$pcode);
					$datadetail	= array(
									'QtyFisik'	=> $QtyFisik
								);
					$this->db->update('opname_detail_split', $datadetail,array('NoDokumen'=>$NoDokumen,'PCode'=>$kodebarang[1]));										
								
			}	
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
			
			$this->db->update('opname_header_split', array('Status'=>0,'Approval_Status'=>0),array('NoDokumen'=>$NoDokumen));
			
			redirect('/transaksi/stock_opname_split/edit_stock_opname_split/' . $NoDokumen . '');
		}	
	}
	
	function test(){
		$mylib = new globallib();
		
		$kdgudang='16';
		$tgl = '2017-06-30';
		$pcode = array('201820017');
		
		$where_pcode = $mylib->where_array($pcode, "PCode", "in");
		$where_KdBarang = $mylib->where_array($pcode, "KodeBarang", "in");
		
		$data = $mylib->get_stock($kdgudang, $tgl, $pcode, $where_pcode, $where_KdBarang);
		print_r($data);
	}
		
}

?>