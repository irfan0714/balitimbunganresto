<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Uang_muka_beo extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/uang_muka_beo_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $offset = $this->uri->segment(4);
        
        if ($sign == "Y") {
        	
        	
        	$data['kasbank'] = $this->uang_muka_beo_model->getKasBank();
			$data['kdtravel'] = $this->uang_muka_beo_model->getTourtravel();
			$data['beo'] = $this->uang_muka_beo_model->getBeo();
			
			$data['offset'] = $offset;
			
			
        	$data['track'] = $mylib->print_track();
        	
            $this->load->view('transaksi/uang_muka_beo/uang_muka_beo_list',$data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'uang_muka_beo', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "uang_muka_beo",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/uang_muka_beo/index/' . $query_id . '');
    }
    
    function getList()
    {	
    	    $keyword = $this->uri->segment(5);
            $status = $this->uri->segment(6);
            $page = $this->uri->segment(4); 
            //echo  $keyword." - ".$status." - ".$page;die;
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '25';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['total_rows'] = $this->uang_muka_beo_model->num_uang_muka_beo_row();
            $config['base_url'] = base_url() . 'index.php/transaksi/uang_muka_beo/index/';
            $config['uri_segment'] = 4;
                  

			$data['kasbank'] = $this->uang_muka_beo_model->getKasBank();
			$data['kdtravel'] = $this->uang_muka_beo_model->getTourtravel();
			
			if($page=="" || $page=='0'){
				$data['startnum']=1;
			}else{
				$data['startnum']=$page+1;
			}
			
            $data['data'] = $this->uang_muka_beo_model->getUangMukaBeoList($keyword,$status,$page,$config['per_page']);
			
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
		$this->load->view('transaksi/uang_muka_beo/uang_muka_beo_getlist', $data);
	}
    
    public function ajax_add() {    	
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $tourtravel = $this->input->post('v_tourtravel');
        $Nilai = $this->input->post('Nilai');
        $BiayaAdmin = $this->input->post('BiayaAdmin');
        $KdKasBank = $this->input->post('v_KdKasBank');
        $keterangan = $this->input->post('v_ket');
        $beo = $this->input->post('v_beo');
        $no_bukti = $this->input->post('v_no_bukti');
        $tanggal = $this->input->post('v_date_payment');
                
       		
		$data = array(
            'Tanggal'=>$mylib->ubah_tanggal($tanggal),
            'KdBank'=>$KdKasBank,
            'KdTravel'=>$tourtravel,
            'Nilai'=>$Nilai,
            'BiayaAdmin'=>$BiayaAdmin,
            'NoBukti'=>$no_bukti,
            'Keterangan'=>$keterangan,
            'no_receipt'=>'',
            'BEO'=>$beo,
            'AddUser'=>$user,
            'AddDate'=>date('Y-m-d')         
        );        
		
        $this->db->insert("uang_muka_beo", $data);
    
        echo json_encode(array("status" => TRUE));
    }

    
    public function ajax_update_bayar() {    	
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $id = $this->input->post('id');
        $no = $this->input->post('no_receipt');
        $tourtravel = $this->input->post('v_tourtravel');
        $Nilai = $this->input->post('Nilai');
        $BiayaAdmin = $this->input->post('BiayaAdmin');
        $KdKasBank = $this->input->post('v_KdKasBank');
        $keterangan = $this->input->post('v_ket');
        $beo = $this->input->post('v_beo');
        $no_bukti = $this->input->post('v_no_bukti');
        $tanggal = $this->input->post('v_date_payment');
        
        $data_trans_receipt_header = array(
            'TglDokumen' => $mylib->ubah_tanggal($tanggal),
            'KdKasBank' => $KdKasBank,
            'Keterangan'=>'Uang Muka '.$id,
            'JumlahReceipt' => $Nilai-$BiayaAdmin,
            'TerimaDari' =>$Nama,
            'EditDate' => date('Y-m-d'),
            'EditUser'=> $user,
            'Jenis'=>1,
            'NoBukti'=>$no_bukti   
        );
        
        $kd_no = $this->uang_muka_beo_model->getKodeBank($KdKasBank);
        $data_trans_receipt_detail = array(
            'TglDokumen' => date('Y-m-d'),
            'KdRekening' => $kd_no->KdRekening,
            'Jumlah'=>$Nilai,
            'KdDepartemen'=>'00',
            'KdSubDivisi'=>$kd_no->KdSubDivisi,
            'Keterangan'=>'Uang Muka '.$id,
            'Urutan'=>1,
            'EditDate' => date('Y-m-d'),
            'EditUser'=> $user,
            'NoBukti'=> $no_bukti,
            'Nama' =>$Nama,
            'Status' => 1   
        );
        
        $data = array(
            'Tanggal'=>$mylib->ubah_tanggal($tanggal),
            'KdBank'=>$KdKasBank,
            'KdTravel'=>$tourtravel,
            'Nilai'=>$Nilai,
            'BiayaAdmin'=>$BiayaAdmin,
            'NoBukti'=>$no_bukti,
            'Keterangan'=>$keterangan,
            'no_receipt'=>$no,
            'BEO'=>$beo,
            'EditUser'=>$user,
            'EditDate'=>date('Y-m-d')         
        );
		
        $this->db->update("trans_receipt_header",$data_trans_receipt_header,array('NoDokumen' => $no));
        $this->db->update("trans_receipt_detail",$data_trans_receipt_detail,array('NoDokumen' => $no));
        $this->db->update("uang_muka_beo", $data,array('id'=>$id));
       
        echo json_encode(array("status" => TRUE));
    }
        
    public function ajax_edit_bayar($id) {
        $nodok = str_replace("-", "/", $id);
        $data = $this->uang_muka_beo_model->get_by_id($nodok);
        echo json_encode($data);
    }
    
    public function ajax_list_beo(){
		$kdtravel = $this->input->post('KdTravel');
		$rec_beo = $this->uang_muka_beo_model->getBeo($kdtravel);
		
		echo "<option value=''> -- Pilih BEO --</option>";
	    foreach ($rec_beo as $beo) {
		  echo "<option value=$beo[NoDokumen]>$beo[NoDokumen]</option>";
		} 
	}
    
    function lock($id) 
    {
		$mylib = new globallib(); 
		//kirim data ke voucher_beo dan voucher
		//generate no.voucher
		
    	$uang_beo = $this->uang_muka_beo_model->get_by_id($id);
    	list($tgl, $bulan, $tahun)= explode('-',$uang_beo->Tanggal);
    	$day = date('D', strtotime($mylib->ubah_tanggal($uang_beo->Tanggal)));
    	$kode = substr(strtoupper($day),2,1)."".substr(strtoupper($day),0,1);
    	
    	$no_voucher = $this->get_no_counter_voucher( $kode ,"voucher_beo", "no_voucher",$tahun,$bulan,"id");
	    	
		//insert ke voucher_beo		
		//jenis 1 piutang sedangkan jenis 2 lunas
		$data_voucher_beo = array(
		            'expDate'=>$mylib->ubah_tanggal($uang_beo->Tanggal_beo),
		            'no_voucher'=>$no_voucher,
		            'tourtravel'=>$uang_beo->KdTravel,
		            'jenis'=>2,					            
		            'KdBank'=>$uang_beo->KdBank,
		            'nilai'=>$uang_beo->Nilai,
		            'NoBukti'=>$uang_beo->NoBukti,
		            'Keterangan'=>$uang_beo->Keterangan,
		            'status'=>1,
		            'no_receipt'=>$uang_beo->no_receipt,
		            'BEO'=>$uang_beo->BEO,
		            'uang_muka_beo'=>'Y',
		            'AddUser'=>$user,
		            'AddDate'=>date('Y-m-d')         
			        );
        $this->db->insert("voucher_beo", $data_voucher_beo);
        
        //insert ke voucher
        $data_voucher = array('novoucher'=>$no_voucher,
								        'nominal'=>$uang_beo->Nilai,
								        'expDate'=>$mylib->ubah_tanggal($uang_beo->Tanggal_beo),
								        'addDate'=>date('Y-m-d'),
								        'userDate'=>'',
								        'status'=>0,
								        'notrans'=>'',
								        'keterangan'=>$uang_beo->Keterangan,
								        'terpakai'=>0,
								        'jenis'=>5,
								        'rupdisc'=>'',
								        'ketentuan'=>'');
        $this->db->insert("voucher", $data_voucher);
    	
    	
        $kd_no = $this->uang_muka_beo_model->getKodeBank($uang_beo->KdBank);
		$no = $this->get_no_counter( $kd_no->KdPenerimaan,"trans_receipt_header", "NoDokumen",$tahun,$bulan);
		
		$data_trans_receipt_header = array(
            'NoDokumen' => $no,
            'TglDokumen' => $mylib->ubah_tanggal($uang_beo->Tanggal),
            'KdKasBank' => $uang_beo->KdBank,
            'Keterangan'=>$uang_beo->Keterangan,
            'JumlahReceipt' => $uang_beo->Nilai-$uang_beo->BiayaAdmin,
            'TerimaDari' =>$uang_beo->Nama,
            'AddDate' => date('Y-m-d'),
            'AddUser'=> $user,
            'Jenis'=>1,
            'NoBukti'=>$no_voucher,
            'Status'=>'1'
        );
        
        $interface = $this->uang_muka_beo_model->getinterface();
        $data_trans_receipt_detail = array(
            'NoDokumen' => $no,
            'TglDokumen' => $mylib->ubah_tanggal($uang_beo->Tanggal),
            'KdRekening' => $interface['UMPenjualan'],
            'Jumlah'=> $uang_beo->Nilai,
            'KdDepartemen'=>'00',
            'KdSubDivisi'=>$kd_no->KdSubDivisi,
            'Keterangan'=>$uang_beo->Keterangan,
            'Urutan'=>1,
            'AddDate' => date('Y-m-d'),
            'AddUser'=> $user,
            'NoBukti'=> $no_voucher,
            'Nama' =>$uang_beo->Nama,
            'Status' => 1   
        );
        
    	$this->db->insert("trans_receipt_header",$data_trans_receipt_header);
        $this->db->insert("trans_receipt_detail",$data_trans_receipt_detail);
        
        if($uang_beo->BiayaAdmin>0){
			$data_biaya_admin = array(
	            'NoDokumen' => $no,
	            'TglDokumen' => $mylib->ubah_tanggal($uang_beo->Tanggal),
	            'KdRekening' => $interface['AdminBank'],
	            'Jumlah'=> $uang_beo->BiayaAdmin*-1,
	            'KdDepartemen'=>'00',
	            'KdSubDivisi'=>$kd_no->KdSubDivisi,
	            'Keterangan'=>$uang_beo->Keterangan,
	            'Urutan'=>2,
	            'AddDate' => date('Y-m-d'),
	            'AddUser'=> $user,
	            'NoBukti'=> $no_voucher,
	            'Nama' =>$uang_beo->Nama,
	            'Status' => 1   
	        );	
	        $this->db->insert("trans_receipt_detail",$data_biaya_admin);
		}
                
    	//update di tabel uang_muka_beo
    	$this->db->update("uang_muka_beo", array('Status'=>1,'no_receipt'=>$no,'NoVoucher'=>$no_voucher),array('id'=>$id));
    	echo json_encode(array("status" => TRUE));
    }
    
    function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
    	$thn-=2000;
        $query = "SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        $col_primary_ok = '';
        if(count($row)>0){
			list($col_primary_ok) = $row;	
		}

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
    function get_no_counter_voucher( $kode,$table_name, $col_primary, $thn,$bln, $orderby )
    {
    	$thn -= 2000;
    	
        $query = "SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
        AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
        ORDER BY
            " .$table_name . "." . $orderby . " DESC
        LIMIT
            0,1
        ";
        //echo $query;die();
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        $col_primary_ok = '';
        if(count($row)>0){
			list($col_primary_ok) = $row;	
		}
		$counter = (substr($col_primary_ok, 6, 4) * 1)+3;
        //$counter = ((substr($col_primary_ok, 6, 4) * 1)+1)+8;
        //$counter = (substr($col_primary_ok, 10, 1) * 3) + 2 * 3;
        $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
        
        return $counter_fa;

    }
    
    function test()
    {
    	$kode='ET';
    	$table_name = 'voucher_beo';
    	$col_primary = 'no_voucher';
    	$thn=2017;
    	$bln=10;
    	
    	$thn -= 2000;
    	
        $query = "SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
        AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;die();
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        $col_primary_ok = '';
        if(count($row)>0){
			list($col_primary_ok) = $row;	
		}
		echo 'old : '.substr($col_primary_ok, 6, 4) .'<br>';
		
        $counter = ((substr($col_primary_ok, 6, 4) * 1)+1)*3 + 2 * 3;
        $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
        echo 'new : '.$counter_fa .'<br>';
    }

    function getPeriodeTutupBulan(){
        $sql ='SELECT DATE_FORMAT(DATE_SUB(PeriodeKasBank, INTERVAL 1 MONTH),"%d-%m-%Y") as TglTrans FROM aplikasi';
        $row = $this->db->query($sql);
        $tgltrans = $row->row_array();
        echo json_encode($tgltrans);
    }
}

?>