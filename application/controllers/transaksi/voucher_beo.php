<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Voucher_beo extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/voucher_beo_model');
    }

    function index() 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $offset = $this->uri->segment(4);
        
        if ($sign == "Y") {
            
			$data['kasbank'] = $this->voucher_beo_model->getKasBank();
			$data['kdtravel'] = $this->voucher_beo_model->getTourtravel();
			$data['beo'] = $this->voucher_beo_model->getBeo();
			$data['offset'] = $offset;
			
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/voucher_beo/voucher_beo_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'voucher_beo', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "voucher_beo",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/voucher_beo/index/' . $query_id . '');
    }
    
    
    function getList()
    {
    		
    	    $keyword = $this->uri->segment(5);
            $status = $this->uri->segment(6);
            
            //echo  $keyword." - ".$status;die;
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '25';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['total_rows'] = $this->voucher_beo_model->num_voucher_beo_row();
            $config['base_url'] = base_url() . 'index.php/transaksi/voucher_beo/index/';
            $config['uri_segment'] = 4;
            $page = $this->uri->segment(4);
            
            
			$data['kasbank'] = $this->voucher_beo_model->getKasBank();
			$data['kdtravel'] = $this->voucher_beo_model->getTourtravel();
			
			if($page==""){
				$data['startnum']=1;
			}else{
				$data['startnum']=$page+1;
			}
			
            $data['data'] = $this->voucher_beo_model->getVoucherBeoList($keyword,$status,$page,$config['per_page']);
			
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
		$this->load->view('transaksi/voucher_beo/voucher_beo_getlist', $data);
	}
    
    
    public function ajax_add() {    	
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $tanggal = $this->input->post('v_date_payment');
        $no_voucher = $this->input->post('v_no_voucher'); //no_voucher hasil json ketika add dan save new.
        $tourtravel = $this->input->post('v_tourtravel');
        $bts = $this->input->post('v_bts');

        // print_r($bts); die();


        $no_voucher_travel = $this->input->post('v_no_voucher_travel');
        $pisah = explode('-',$no_voucher_travel);
        
        if($pisah[1]!=""){
		    $voucherawal = $pisah[0];
		    $voucherakhir = $pisah[1];
		    $generate = "Y";
		}else{
			$voucherawal = $pisah[0];
		    $voucherakhir = $pisah[0];
		    $generate = "T";
		}
			
		
        $Nilai = $this->input->post('Nilai');    
        $no_bukti = $this->input->post('v_no_bukti');    
        $keterangan = $this->input->post('v_ket');  
        $beo = $this->input->post('v_beo'); 
        $v_type_beo = $this->input->post('v_type_beo');  
         
         
         if($generate=="T"){
			//$voucher_travel_fix = $voucherawal;
			//$no_voucher_fix = $no_voucher;
			
			$no_voucher_fix  = $this->get_generate_number_voucher();
			//jenis 1 piutang sedangkan jenis 2 lunas	
			$data = array(
	            'expDate'=>$mylib->ubah_tanggal($tanggal),
	            'no_voucher'=>$no_voucher_fix,
	            'tourtravel'=>$tourtravel,
	            'no_voucher_travel'=>$voucher_travel_fix,
	            'jenis'=>1,
	            'nilai'=>$Nilai,
	            'NoBukti'=> $no_bukti,
	            'no_receipt'=>$no,
	            'Keterangan'=>$keterangan,
	            'BEO'=>$beo,            
	            'AddUser'=>$user,
                'bts'=>$bts,
	            'AddDate'=>date('Y-m-d')         
	        );
	        
	        $this->db->insert("voucher_beo", $data);
        	
		}else{

			$formatvoucher = substr($voucherawal,0,6);
			$angkaawal = substr($voucherawal,-4);
			$angkaakhir = substr($voucherakhir,-4);
			
			for($x=1;$x<=intval($angkaakhir);$x++){
				
				if($x<=9){
					$nol ="000";
				}else if($x<=99){
					$nol ="00";
				}else if($x<=999){
					$nol ="0";
				}else {
					$nol ="";
				}
				
				
				if($x==1){
					$tampil='Y';
					$kets = $no_voucher_travel;
				}else{
					$tampil='T';
					$kets = '';
				}
				
					$no_voucher_fix = "V".$formatvoucher.$nol.$x;
					$voucher_travel_fix = $formatvoucher.$nol.$x;
					
					//jenis 1 piutang sedangkan jenis 2 lunas	
					$data = array(
			            'expDate'=>$mylib->ubah_tanggal($tanggal),
			            'no_voucher'=>$no_voucher_fix,
			            'tourtravel'=>$tourtravel,
			            'no_voucher_travel'=>$voucher_travel_fix,
			            'jenis'=>1,
			            'nilai'=>$Nilai,
			            'NoBukti'=> $no_bukti,
			            'no_receipt'=>$no,
			            'Keterangan'=>$keterangan,
			            'BEO'=>$beo,
			            'status'=>1,            
			            'AddUser'=>$user,
			            'AddDate'=>date('Y-m-d'),
			            'Voc_Glondongan'=>$kets,
                        'bts'=>$bts,
			            'Tampil'=>$tampil        
			        );			        
			        $this->db->insert("voucher_beo", $data);
			        
			        
			          //insert juga ke tabel Voucher
	    	          $data_voucher = array('novoucher'=>$no_voucher_fix,
	    	          						'novoucher_travel'=>$voucher_travel_fix,
									        'nominal'=>$Nilai,
									        'expDate'=>$mylib->ubah_tanggal($tanggal),
									        'addDate'=>date('Y-m-d'),
									        'userDate'=>'',
									        'status'=>0,
									        'notrans'=>'',
									        'keterangan'=>$keterangan,
									        'terpakai'=>0,
									        'jenis'=>5,
									        'rupdisc'=>'',
									        'ketentuan'=>'');
    	               $this->db->insert("voucher", $data_voucher);    	          	        
		        
			}
			
		}       
        
        
        
        echo json_encode(array("status" => TRUE));
    }

    
    public function ajax_update_bayar() {    	
        
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $id = $this->input->post('id');
        $no = $this->input->post('no_receipt');
        $tanggal = $this->input->post('v_date_payment');
        $no_voucher = $this->input->post('v_no_voucher');
        $tourtravel = $this->input->post('v_tourtravel');
        $no_voucher_travel = $this->input->post('v_no_voucher_travel');
        $Nilai = $this->input->post('Nilai');    
        $no_bukti = $this->input->post('v_no_bukti');    
        $keterangan = $this->input->post('v_ket'); 
        $beo = $this->input->post('v_beo'); 
        $v_type_beo = $this->input->post('v_type_beo');    
        
        //jenis 1 piutang sedangkan jenis 2 lunas	
		$data = array(
            'expDate'=>$mylib->ubah_tanggal($tanggal),
            'no_voucher'=>$no_voucher,
            'tourtravel'=>$tourtravel,
            'no_voucher_travel'=>$no_voucher_travel,
            'jenis'=>1,
            'nilai'=>$Nilai,
            'NoBukti'=> $no_bukti,
            'no_receipt'=>$no,
            'Keterangan'=>$keterangan,
            'BEO'=>$beo,              
            'EditUser'=>$user,
            'EditDate'=>date('Y-m-d')         
        );
        
        $this->db->update("voucher_beo", $data,array('id'=>$id));
        //$this->cetakStruk($no);
        echo json_encode(array("status" => TRUE,"NoDokumen" =>$no));
    }
        
    public function ajax_edit_bayar($id) {
        $nodok = str_replace("-", "/", $id);
        $data = $this->voucher_beo_model->get_by_id($nodok);
        echo json_encode($data);
    }
    
    public function generate_number_voucher() {
    	
    	$tgl=date('Y-m-d');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
				
		$day = date('D', strtotime($tgl));
    	$kode = substr(strtoupper($day),2,1)."".substr(strtoupper($day),0,1);
    	$no = $this->get_no_counter( $kode ,"voucher_beo", "no_voucher",$tahun,$bulan,"id");
		    	
        //$data = $this->voucher_beo_model->get_by_id($nodok);
        $data = array('v_no_voucher'=>$no);
        echo json_encode($data);
    }
    
    public function get_generate_number_voucher() {
    	
    	$tgl=date('Y-m-d');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);		
		$day = date('D', strtotime($tgl));
    	$kode = substr(strtoupper($day),2,1)."".substr(strtoupper($day),0,1);
    	$no = $this->get_no_counter( $kode ,"voucher_beo", "no_voucher",$tahun,$bulan,"id");   	
        return $no;
    }
    
    function lock($id) 
    {
    			  $mylib = new globallib(); 
    			  $data = $this->voucher_beo_model->get_by_id($id);
    				
    	              //insert juga ke tabel Voucher
	    	          $data_voucher = array('novoucher'=>$data->no_voucher,
	    	          						'novoucher_travel'=>$data->no_voucher_travel,
									        'nominal'=>$data->nilai,
									        'expDate'=>$mylib->ubah_tanggal($data->expDate),
									        'addDate'=>date('Y-m-d'),
									        'userDate'=>'',
									        'status'=>0,
									        'notrans'=>'',
									        'keterangan'=>$data->Keterangan,
									        'terpakai'=>0,
									        'jenis'=>5,
									        'rupdisc'=>'',
									        'ketentuan'=>'');
    	               $this->db->insert("voucher", $data_voucher);
    	          
    	          
    	$this->db->update("voucher_beo", array('status'=>1),array('id'=>$id));
    	echo json_encode(array("status" => TRUE));
    }
    
    function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln, $orderby )
    {
        $query = "SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
        AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
        ORDER BY
            " .$table_name . "." . $orderby . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        $col_primary_ok = '';
        if(count($row)>0){
			list($col_primary_ok) = $row;	
		}
		$counter = (substr($col_primary_ok, 6, 4) * 1)+3;
        //$counter = ((substr($col_primary_ok, 6, 4) * 1)+1)+8;
        //$counter = (substr($col_primary_ok, 10, 1) * 3) + 2 * 3;
        $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
        return $counter_fa;

    }

    function doUpload($id){

        // echo "<script>alert('TES');</script>";

        $nodok = str_replace("-", "/", $id);
        // $data = $this->voucher_beo_model->get_by_id($nodok);
        $mylib = new globallib(); 

        // $nodok = $this->uri->segment(4);

        $header = $this->voucher_beo_model->getHeader($nodok);

        // print_r($header); die();
        $data = "Proses Gagal";
        $sourcedb = $this->load->database('timbungandb', TRUE);
        $connected = $sourcedb->initialize();
        if(!$connected){
            $data = "Tidak Terhubung ke BTS";
            // echo "Tidak Konek ke BTS";
        }else{
        $sourcedb->conn_id = ($sourcedb->pconnect == FALSE) ? $sourcedb->db_connect() : $sourcedb->db_pconnect();


            $data_voucher = array(  'novoucher'=>$header->no_voucher,
                                    'novoucher_travel'=>$header->no_voucher_travel,
                                    'nominal'=>$header->nilai,
                                    'expDate'=>$header->expDate,
                                    'addDate'=>date('Y-m-d'),
                                    'userDate'=>'',
                                    'status'=>0,
                                    'notrans'=>'',
                                    'keterangan'=>$header->Keterangan,
                                    'terpakai'=>0,
                                    'jenis'=>5,
                                    'rupdisc'=>'',
                                    'ketentuan'=>'');

            $this->dbtimbungan=$sourcedb;
            $hasil = $this->dbtimbungan->insert("voucher", $data_voucher);
            

            if($hasil){

            $this->voucher_beo_model->updateFlag($nodok);
            $this->voucher_beo_model->updatediVoucher($header->no_voucher,$header->nilai);

                $data = "sukses";
                // echo "Sukses Upload Voucher ke BTS";
                echo json_encode($data);

                // print_r($hasil);
            }else{
                // echo "Gagal Upload Voucher ke BTS";
                $data = "gagal";

                echo json_encode($data);

            }
            // $this->$sourcedb->insert();
        // $this->voucher_beo_model->($date,$store[$i]['Kassa'],$sourcedb, $interval); 


        }


    }
    
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "voucher-beo.txt";
		
		$header = $this->voucher_beo_model->getHeader($nodok);
		$detail = $this->voucher_beo_model->getDetail_cetak($nodok);
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			$arr_data["detail_namasatuan"][$counter] = $val["NamaSatuan"];
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "VOUCHER";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			$echo .= $arr_epson["cond"].$nama_dokumen;
            $echo.="\r\n"; 
            $echo.="\r\n"; 
            
            // No. Voucher
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."No. Voucher";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("No. Voucher"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->no_voucher;         
                
                // -----------------------------------------------------
                $echo.="\r\n"; 
            } 
            
            // No. Voucher Travel
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."No. Vouc. Travel";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("No. Vouc. Travel"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->no_voucher_travel;         
                
                // -----------------------------------------------------
                $echo.="\r\n"; 
            }  
            
            // Tanggal
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Exp. Date";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Exp. Date"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$mylib->ubah_tanggal($header->expDate);         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->expDate));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                $echo.="\r\n"; 
            }
            
            // Travel
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Travel";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Travel"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                $echo.="\r\n"; 
            }
            
            // Nilai
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Nilai";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Nilai"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].number_format($header->nilai);         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->nilai));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                $echo.="\r\n"; 
            }
            
            // BEO
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."BEO";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("BEO"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->BEO;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->BEO));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                $echo.="\r\n"; 
            }
            
            // ket
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Keterangan";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Keterangan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Keterangan;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Keterangan));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                $echo.="\r\n"; 
            }

            $echo .= "\r\n";  
			
			if($user!="mechael0101")
	        {
		        $data = array(
		            'form_data' => "delivery-order",
		            'noreferensi' => $header->no_voucher,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='voucher-beo-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
    		
}

?>