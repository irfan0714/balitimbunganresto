<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Budget extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/budgetmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/budget/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/budget/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/transaksi/budget/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->budgetmodel->num_budget_row($id,$with);
	        $data['data']  = $this->budgetmodel->get_budget_List($config['per_page'],$page,$id,$with);
	        $data['aktivitas'] = $this->budgetmodel->getAktivitasList();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/budget/budget_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	
	function view_budget($tahun,$kdaktivitas)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            //$id = $this->uri->segment(4);			
            $data['header'] = $this->budgetmodel->getHeader($tahun,$kdaktivitas);
			$data['budget01'] = $this->budgetmodel->getBudget01($tahun,$kdaktivitas);
			$data['budget02'] = $this->budgetmodel->getBudget02($tahun,$kdaktivitas);
			$data['budget03'] = $this->budgetmodel->getBudget03($tahun,$kdaktivitas);
			$data['budget04'] = $this->budgetmodel->getBudget04($tahun,$kdaktivitas);
			$data['budget05'] = $this->budgetmodel->getBudget05($tahun,$kdaktivitas);
			$data['budget06'] = $this->budgetmodel->getBudget06($tahun,$kdaktivitas);
			$data['budget07'] = $this->budgetmodel->getBudget07($tahun,$kdaktivitas);
			$data['budget08'] = $this->budgetmodel->getBudget08($tahun,$kdaktivitas);
			$data['budget09'] = $this->budgetmodel->getBudget09($tahun,$kdaktivitas);
			$data['budget10'] = $this->budgetmodel->getBudget10($tahun,$kdaktivitas);
			$data['budget11'] = $this->budgetmodel->getBudget11($tahun,$kdaktivitas);
			$data['budget12'] = $this->budgetmodel->getBudget12($tahun,$kdaktivitas);
			$data['totbudget'] = $this->budgetmodel->getTotalBudget($tahun,$kdaktivitas);

            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/budget/view_budget', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function save_budget(){
    	
    	$tahun = $this->input->post('thn');
		$kdaktivitas = $this->input->post('aktivitas');
		$budget01 = $this->input->post('bud1');
		$budget02 = $this->input->post('bud2');
		$budget03 = $this->input->post('bud3');
		$budget04 = $this->input->post('bud4');
		$budget05 = $this->input->post('bud5');
		$budget06 = $this->input->post('bud6');
		$budget07 = $this->input->post('bud7');
		$budget08 = $this->input->post('bud8');
		$budget09 = $this->input->post('bud9');
		$budget10 = $this->input->post('bud10');
		$budget11 = $this->input->post('bud11');
		$budget12 = $this->input->post('bud12');
        
    	$user = $this->session->userdata('username');
    	$data = array(
    		  'Tahun'	=> $tahun,
			  'KdAktivitas' => $kdaktivitas,
			  'Budget01' => $budget01,
			  'Budget02' => $budget02,
			  'Budget03' => $budget03,
			  'Budget04' => $budget04,
			  'Budget05' => $budget05,
			  'Budget06' => $budget06,
			  'Budget07' => $budget07,
			  'Budget08' => $budget08,
			  'Budget09' => $budget09,
			  'Budget10' => $budget10,
			  'Budget11' => $budget11,
			  'Budget12' => $budget12,
              'AddDate'		=> date('Y-m-d'),
              'AddUser'		=> $user
			);
		$this->db->insert('budget', $data);
    	
    }
	
	function edit_budget(){
    	
    	$tahun = $this->input->post('thn');
		$kdaktivitas = $this->input->post('aktivitas');
		$budget01 = $this->input->post('bud1');
		$budget02 = $this->input->post('bud2');
		$budget03 = $this->input->post('bud3');
		$budget04 = $this->input->post('bud4');
		$budget05 = $this->input->post('bud5');
		$budget06 = $this->input->post('bud6');
		$budget07 = $this->input->post('bud7');
		$budget08 = $this->input->post('bud8');
		$budget09 = $this->input->post('bud9');
		$budget10 = $this->input->post('bud10');
		$budget11 = $this->input->post('bud11');
		$budget12 = $this->input->post('bud12');
        
    	$user = $this->session->userdata('username');
    	$data = array(
			  'Budget01' => $budget01,
			  'Budget02' => $budget02,
			  'Budget03' => $budget03,
			  'Budget04' => $budget04,
			  'Budget05' => $budget05,
			  'Budget06' => $budget06,
			  'Budget07' => $budget07,
			  'Budget08' => $budget08,
			  'Budget09' => $budget09,
			  'Budget10' => $budget10,
			  'Budget11' => $budget11,
			  'Budget12' => $budget12,
              'EditDate'		=> date('Y-m-d'),
              'EditUser'		=> $user
			);
		$this->db->update('budget', $data,array('Tahun'=>$tahun,'KdAktivitas'=>$kdaktivitas));
    	
    }
	
	function delete_budget(){
    	$Tahun = $this->input->post('tahun');
    	$kdaktivitas = $this->input->post('id');
		$this->db->delete('budget',array('Tahun'=>$Tahun,'KdAktivitas'=>$kdaktivitas));
    	
    }
    
    function ajax(){
		$KdAktivitas = $this->input->post('id');
		$query = $this->budgetmodel->getAktivitas($KdAktivitas);
		 
	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[KdAktivitas]>$cetak[NamaAktivitas]</option>";
	 
	    }
    }
    
    function edit($tahun,$aktivitas) 
    {
		$data = $this->budgetmodel->get_by_budget($tahun,$aktivitas);
		echo json_encode($data);			
    }
    
  }
?>