<?php
// Febri
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Request_return extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        error_reporting(0);                              
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/persediaan/request_return_model', 'reqreturn');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$id = $this->uri->segment(4);	
			$user = $this->session->userdata('username');
			
			$data["search_keyword"] = "";
			$data["search_gudang"] = "";
			$data["search_supplier"] = "";
			$resSearch = "";
			$arr_search["search"]= array();
			$id_search = "";
			if($id*1>0)
			{	
				$resSearch = $this->globalmodel->getSearch($id,"request_return",$user);	
				$arrSearch = explode("&",$resSearch->query_string);
				
				$id_search = $resSearch->id;
				
				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					$search_gudang = explode("=", $arrSearch[1]); // search gudang
					$arr_search["search"]["gudang"] = $search_gudang[1];
					$search_supplier = explode("=", $arrSearch[2]); // search gudang
					$arr_search["search"]["supplier"] = $search_supplier[1];
					
					$data["search_keyword"] = $search_keyword[1];
					$data["search_gudang"] = $search_gudang[1];
					$data["search_supplier"] = $search_supplier[1];
				}
			}
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if($id_search)
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/request_return/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/request_return/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}
			
            $data["ses_login"] 	= $this->session->userdata('username');
            $data['mgudang'] = $this->reqreturn->getGudang();
            $data['msupplier'] = $this->reqreturn->getSupplier();
            
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            
            $thnbln = $data['tahun'] . $data['bulan'];
            
            $config['total_rows'] = $this->reqreturn->num_tabel_row($arr_search["search"]);
            $data['data'] = $this->reqreturn->getTabelList($config['per_page'], $page, $arr_search["search"]);
            
            $data['track'] = $mylib->print_track();
            
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
            $this->load->view('transaksi/persediaan/request_return/tabellist', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }    

    function otoritas()
    {
        $this->load->view('transaksi/permintaan_barang/form_otoritas');
    } 



    function search()
    {
        $mylib = new globallib();
        
		$user = $this->session->userdata('username');
		
		// hapus dulu yah
		$this->db->delete('ci_query', array('module' => 'request_return','AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_supplier=".$this->input->post('search_supplier');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "request_return",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/transaksi/request_return/index/'.$query_id.'');
		
	} 
	
    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            
            $user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            
			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
			
            $data['mgudang'] 	= $this->reqreturn->getGudang($arr_search["list_gudang_admin"]);
            $data['msatuan'] 	= $this->reqreturn->getSatuan();
            $data['track'] 		= $mylib->print_track();
            
	    	$this->load->view('transaksi/persediaan/request_return/form_add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function edit_form($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data["ses_login"] 	= $this->session->userdata('username');
            $data['bulan'] 		= $this->session->userdata('bulanaktif');
            $data['tahun'] 		= $this->session->userdata('tahunaktif');
            $data['header'] 	= $this->reqreturn->getHeader($id);
            //$data['detail'] 	= $this->reqreturn->getDetail($id);
            
	        $data["detailunion"] = $this->reqreturn->getDetailUnion($id,$data['header']->RgNo);
            
            $data['track'] 		= $mylib->print_track();
            
            $this->load->view('transaksi/persediaan/request_return/form_edit', $data);

        } 
        else 
        {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
               
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_rg_no = $this->input->post('v_rg_no');
        $v_rg_gudang = $this->input->post('v_rg_gudang');
        $v_rg_supplier = $this->input->post('v_rg_supplier');
        $v_keterangan = $mylib->save_char($this->input->post('v_keterangan'));
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');
		
        //$data['bulan'] = $this->session->userdata('bulanaktif');
        //$data['tahun'] = $this->session->userdata('tahunaktif');
        
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
		
		if($flag=="add")
        {
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_request_retur_header","NoDokumen", "RR", $data['bulan'], $data['tahun']);
            
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_rg_no, $v_rg_supplier, $v_rg_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);
       		 
       		$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       		
       		$detail = $this->reqreturn->getDetailRg($v_rg_no);
       		
       		foreach($detail as $val)
       		{
				$this->insertDetail($flag,$v_no_dokumen,$val["PCode"],$val["Qty"],$val["QtyPcs"],"0",$val["Satuan"],"");
			}
       	} 
        else if($flag=="edit")
        {
	        // detail pcode
			$pcode1 = $this->input->post('pcode');
			$v_qty1 = $this->input->post('v_qty');
			$v_qtypcs1 = $this->input->post('v_qtypcs');
			$v_qty_return1 = $this->input->post('v_qty_return');
			$v_satuan1 = $this->input->post('v_satuan');
			$v_keterangan_pcode1 = $this->input->post('v_keterangan_pcode');
			
        	$v_no_dokumen = $this->input->post('v_no_dokumen');
        	
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_rg_no, $v_rg_supplier, $v_rg_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);
        	
        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        
    		$this->db->delete('trans_request_retur_detail', array('NoDokumen' => $v_no_dokumen)); 
    		
	        for($x=0;$x< count($pcode1);$x++)
			{
				$pcode 				= strtoupper(addslashes(trim($pcode1[$x])));
				$v_qty 				= $mylib->save_int($v_qty1[$x]);
				$v_qtypcs 			= $mylib->save_int($v_qtypcs1[$x]);
				$v_qty_return 		= $mylib->save_int($v_qty_return1[$x]);
				$v_satuan			= $v_satuan1[$x];
				$v_keterangan_pcode	= $mylib->save_char($v_keterangan_pcode1[$x]);
				
				if($pcode!="")
				{   
					if($v_qty_return*1>0)
					{
						//$detail 	= $this->reqreturn->cekGetDetail($pcode,$v_no_dokumen);
						
						$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$v_qtypcs,$v_qty_return,$v_satuan,$v_keterangan_pcode);
					}
				}
			}
        }
        else if($flag=="approval")
        {
        	$v_no_dokumen = $this->input->post('v_no_dokumen');
        	$v_approval = $this->input->post('v_approval');
        	
        	$v_keterangan_reject="";
        	if($v_approval==2)
        	{
        		$v_keterangan_reject = $mylib->save_char($this->input->post('v_keterangan_reject'));	
			}
		
			$this->updateApproval($v_no_dokumen, $v_approval, $v_keterangan_reject, $flag, $base_url, $user);
			
			$this->session->set_flashdata('msg', array('message' => 'Proses approval <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'warning'));
        
        }
        
       redirect('/transaksi/request_return/edit_form/'.$v_no_dokumen.'');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $rgno, $kdsupplier, $kdgudang, $keterangan, $status, $flag, $base_url, $user)
    {   
        $this->reqreturn->locktables('trans_request_retur_header');
        
        $data = array(
            'NoDokumen' => $no_dokumen,
            'TglDokumen' => $tgl_dokumen,
            'RgNo' => $rgno,
            'KdSupplier' => $kdsupplier,
            'KdGudang' => $kdgudang,
            'Keterangan' => $keterangan,
            'Status' => "0",
            'Approval_By' => "",
            'Approval_Status' => "",
            'Approval_Remarks' => "",
            'AddUser' => $user,
            'AddDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user,
            'EditDate' => date("Y-m-d H:i:s")
        );
        
        $this->db->insert('trans_request_retur_header', $data);
        
        $this->reqreturn->unlocktables();
    }

    function updateHeader($no_dokumen, $tgl_dokumen, $rgno, $kdsupplier, $kdgudang, $keterangan, $status, $flag, $base_url, $user)
    {
        $this->reqreturn->locktables('trans_request_retur_header');

        $data = array(
            'TglDokumen' => $tgl_dokumen,
            'Keterangan' => $keterangan,
            'Status' => $status,
            'Approval_By' => "",
            'Approval_Status' => "",
            'Approval_Remarks' => "",
            'EditUser' => $user,
            'EditDate' => date("Y-m-d H:i:s")
        );
        $this->db->update('trans_request_retur_header', $data, array('NoDokumen' => $no_dokumen));
        
        $this->reqreturn->unlocktables();
        
        if($status=="1")
        {
        	$mylib = new globallib();
        	
        	$to = "";
            $to_name = "";
            
            $sql = "
				  trans_order_barang_header.addUser,
				  trans_order_barang_header.EditUser,
  				  emp_add.employee_name AS employee_name_add,
				  emp_add.email AS email_add,
  				  emp_edit.employee_name AS employee_name_edit,
				  emp_edit.email AS email_edit 
				FROM
				  trans_order_barang_header 
				  INNER JOIN trans_terima_header 
				    ON trans_order_barang_header.NoDokumen = trans_terima_header.PoNo 
				  INNER JOIN employee AS emp_add 
				    ON trans_order_barang_header.AddUser = emp_add.username 
				  INNER JOIN employee AS emp_edit 
				    ON trans_order_barang_header.AddUser = emp_edit.username 
				WHERE 1 
				  AND trans_terima_header.NoDokumen = '".$no_dokumen."'
				LIMIT 0,01
            ";
            $arrData = $this->globalmodel->getQuery($sql);
            foreach($arrData as $val)
            {
            	if($val["addUser"]==$val["EditUser"])
            	{
	               $to .= $val["email_add"].";";
	                $to_name .= $val["employee_name_add"].";";
				}
				else
				{
	                $to .= $val["email_add"].";";
	                $to_name .= $val["employee_name_add"].";";
	                
	                $to .= $val["email_edit"].";";
	                $to_name .= $val["employee_name_edit"].";";
				}
			}
                                
        	$subject = "Notifikasi RR ".$no_dokumen;
            $author  = "Auto System";
            
        	$to .= "sari@secretgarden.co.id;";
            $to_name .= "Kadek Sariasih;";
            
        	$to .= "hnd@vci.co.id;";
            $to_name .= "Hendri;";
            
        	$to .= "fyt@vci.co.id;";
            $to_name .= "Febriyanto;";
            
            $body = "";
            $body  .= "REQUEST RETURN barang yang telah di ajukan oleh ".$user.", pada tanggal ".date("d-m-Y H:i:s")." ";
            $body  .= "<br/>";
            $body  .= "Untuk lebih jelas dan detail nya silahkan copy link dibawah ini : ";
            $body  .= "<br/>";
            $body  .= $base_url."index.php/transaksi/request_return/edit_form/".$no_dokumen;
            
			$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		}
       
    }

    function updateApproval($no_dokumen, $approval, $keterangan_reject, $flag, $base_url, $user)
    {
        $this->reqreturn->locktables('trans_request_retur_header');

        $data = array(
            'Approval_By' => $user,
            'Approval_Date' => date("Y-m-d H:i:s"),
            'Approval_Status' => $approval,
            'Approval_Remarks' => $keterangan_reject,
            'EditUser' => $user,
            'EditDate' => date("Y-m-d H:i:s")
        );
        $this->db->update('trans_request_retur_header', $data, array('NoDokumen' => $no_dokumen));
        
        $this->reqreturn->unlocktables();
        
        $header = $this->reqreturn->getHeader($no_dokumen);
        
        if($approval==1)
        {
        	$this->reqreturn->get_mutasi_stock($no_dokumen);
        	
			$echo_approval = '<b style="color: #3C763D; background-color: #DFF0D8; border-color: #D6E9C6;">APPROVE</b>';
			$echo_ket = "";
		}
		else
		{
			$echo_approval = '<b style="color: #A94442; background-color: #F2DEDE; border-color: #EBCCD1;">REJECT</b>';
			$echo_ket = " dengan keterangan ".$keterangan_reject;
		}
        
    	$mylib = new globallib();
    	
    	$to = "";
        $to_name = "";
               
    	$subject = "Notifikasi RR ".$no_dokumen;
        $author  = "Auto System";
        
        $to .= "samsul@secretgarden.co.id;";
        $to_name .= "Samsul Hidayat;";
        
    	$to .= "hnd@vci.co.id;";
        $to_name .= "Hendri;";
        
    	$to .= "fyt@vci.co.id;";
        $to_name .= "Febriyanto;";
        
        $body = "";
        $body  .= "REQUEST RETURN barang dengan nomor dokumen <b>".$no_dokumen."</b>, telah ".$echo_approval." pada tanggal ".$header->Approval_Date.$echo_ket." oleh ".$header->Approval_By;

		$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);       
    }

    function insertDetail($flag,$no_dokumen,$pcode,$qty,$qtypcs,$qtyreturn,$satuan,$keterangan)
    {
        $this->reqreturn->locktables('trans_request_retur_detail');

        if ($pcode) 
        {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'PCode' => $pcode,
                'Qty' => $qty,
                'QtyPcs' => $qtypcs,
                'QtyReturn' => $qtyreturn,
                'Satuan' => $satuan,
                'Keterangan' => $keterangan
            );
            
            $this->db->insert('trans_request_retur_detail', $data);
        } 
        
        $this->reqreturn->unlocktables();
    }

    function updateDetail($flag,$no_dokumen,$pcode,$qty,$qtypcs,$qtyreturn,$satuan,$keterangan,$qty_tbl)
    {
    	$new_qty =$qtyreturn+$qty_tbl;
    	 
        $this->reqreturn->locktables('trans_request_retur_detail');

        if ($pcode) 
        {
            $data = array(
                'Qty' => $qty,
                'QtyPcs' => $qtypcs,
                'QtyReturn' => $new_qty,
                'Satuan' => $satuan,
                'Keterangan' => $keterangan
            );
            
            $this->db->update('trans_request_retur_detail', $data, array('NoDokumen' => $no_dokumen,'PCode' => $pcode));
        } 
        
        $this->reqreturn->unlocktables();
    }

	function delete_trans($id)
	{
		$this->db->delete('trans_request_retur_header', array('NoDokumen' => $id));
		$this->db->delete('trans_request_retur_detail', array('NoDokumen' => $id));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen '.$id.'</strong> berhasil','class' => 'success'));
	
		redirect('/transaksi/request_return/'); 
	}

	function delete_detail()
	{
		$sid = $this->uri->segment(4);	
		$pcode = $this->uri->segment(5);	
		$nodok = $this->uri->segment(6);	
		
		$this->db->delete('trans_request_retur_detail', array('sid' => $sid,'PCode' => $pcode,'NoDokumen' => $nodok));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode '.$pcode.'</strong> berhasil','class' => 'success'));
		
		redirect('/transaksi/request_return/edit_form/'.$nodok.''); 
	}
	
	function viewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		$data["judul"]			= "REQUEST RETURN";
		
		$data["header"] 		= $this->reqreturn->getHeader($nodok);
		$data["detailunion"] 	= $this->reqreturn->getDetailUnion($nodok,$data['header']->RgNo);
		$data["pt"] 			= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_request_return', $data);
	}
	
	function doPrint()
	{
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
	
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 145;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "request-return.txt";
		
		$header = $this->reqreturn->getHeader($nodok);
		$detail = $this->reqreturn->getDetail($nodok);
		
		$note_header = substr($header->Keterangan,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,30);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            $arr_data["detail_qtyreturn"][$counter] = $val["QtyReturn"];
            $arr_data["detail_satuan"][$counter] = $val["Satuan"];
            $arr_data["detail_keterangan"][$counter] = substr($val["Keterangan"],0,30);
			
			 $counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "REQUEST RETURN";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            //$echo .= chr(27).chr(119).chr(1);
            $echo .= $arr_epson["cond"].$nama_dokumen;
            $echo.="\r\n";
            $echo.=$arr_epson["ncond"];
            
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."No : ".$header->NoDokumen;
            $echo.="\r\n";
            $echo.="\r\n";
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$header->TglDokumen;
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglDokumen));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="Surat Jalan";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Surat Jalan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                $echo.=$header->NoSuratJalan;
               
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.="RG No";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("RG No"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$header->RgNo;
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->RgNo));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="Supplier";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Supplier"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                $echo.=$header->suppliername;
                
                $echo.="\r\n";    
            }

            // baris 3
            {
                $echo.="PO No";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("PO No"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$header->PoNo;
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->PoNo));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.="\r\n";    
            }
            
            $echo.=chr(15);
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo.="NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="PCode";
            $limit_spasi = 25;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Nama Barang";
            $limit_spasi = 50;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Qty RG";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty RG"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Qty Return";
            $limit_spasi = 14;
            for($i=0;$i<($limit_spasi-strlen("Qty Return"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Keterangan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Keterangan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
            	$qry_return = $arr_data["detail_qtyreturn"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
            	$detail_keterangan = $arr_data["detail_keterangan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$pcode;
                    $limit_spasi = 25;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=substr($namabarang,0,70);
                    $limit_spasi = 50;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$mylib->format_number($qty,2);
                    $limit_spasi = 13;
                    for($i=0;$i<($limit_spasi-strlen($qty));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$mylib->format_number($qry_return,2);
                    $limit_spasi = 13;
                    for($i=0;$i<($limit_spasi-strlen($qry_return));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$detail_keterangan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($detail_keterangan));$i++)
                    {
                        $echo.=" ";
                    }
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo.=$arr_epson["cond"];
	        if($i_page==$jml_page)
	        {
	        	$echo .= $spasi_awal;
            	$echo .= "Note : ".$header->Keterangan;
            
	            $echo .= "\r\n";
	            $echo .= "\r\n";
	            
                $echo.="       Dibuat Oleh     ";
                $echo.="          ";
                $echo.="       Diketahui Oleh      ";
                $echo.="          ";
                $echo.="       Disetujui Oleh      ";
                $echo.="\r\n";    
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="     (             )     ";
                $echo.="          ";
                $echo.="     (             )      ";
                $echo.="          ";
                $echo.="     (             )      ";
                $echo.="\r\n";
	        	
	        }
	        else
	        {
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
                $echo.="\r\n";
			}
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"request-return");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 143;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 143;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"request-return");
			
			if($user!="hendri1003")
	        {
		        $data = array(
		            'form_data' => "request-return",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='request-return-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
}

?>