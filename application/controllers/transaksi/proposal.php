<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Proposal extends authcontroller {

    function __construct(){
        parent::__construct();
        //error_reporting(0);
        //global function
        $this->load->library('globallib');

        //email
        $this->load->library('email');
        $this->load->model('globalmodel');
        //model
        $this->load->model('transaksi/proposalmodel');
    }

    function index(){

     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");

    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
		 	$id = $this->uri->segment(4);
            $user = $this->session->userdata('username');


	        $data["search_keyword"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "proposal", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_status = explode("=", $arrSearch[1]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_by"] = $search_status[1];
                }
            }



	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '5';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/proposal/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('search_by');
			$id   					  = "";
			$flag1					  = "";

			 if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/proposal/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/proposal/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

			//cek akses approval
    	    $user = $this->session->userdata('username');
			$data['username']=$user;

			$config['total_rows']  = $this->proposalmodel->num_proposal_row($id,$with);
	        $data['data']  = $this->proposalmodel->get_proposal_List($config['per_page'],$page,$arr_search["search"]);
	        $data['aktivitas'] = $this->proposalmodel->getAktivitasList();
			$data['divisi'] = $this->proposalmodel->getDivisi();
			$data['employee_pic'] = $this->proposalmodel->getEmployeePic();
			$data['employee_approval1'] = $this->proposalmodel->getEmployeePic();
			$data['employee_approval2'] = $this->proposalmodel->getEmployeeApproval2();
			$data['employee_approval3'] = $this->proposalmodel->getEmployeeApproval3();

			$data['status_pending'] = $this->proposalmodel->getProposalPending();
			$data['status_waiting_approve1'] = $this->proposalmodel->getProposalWaitingApprove1();
			$data['status_waiting_approve2'] = $this->proposalmodel->getProposalWaitingApprove2();
			$data['status_waiting_approve3'] = $this->proposalmodel->getProposalWaitingApprove3();
			$data['status_waiting_approved'] = $this->proposalmodel->getProposalWaitingApproved();
			$data['status_canceled'] = $this->proposalmodel->getProposalCanceled();
			$data['status_rejected'] = $this->proposalmodel->getProposalRejected();
			$cek_boleh_lihat = $this->proposalmodel->boleh_lihat($user);
			if(empty($cek_boleh_lihat)){
				$data['boleh_lihat']="T";
			}else{
				$data['boleh_lihat']="Y";
			}
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
			$data["pagination"] = $this->pagination->create_links();
	        $this->load->view('transaksi/proposal/proposal_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function search()
    {
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'proposal', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_status=".$this->input->post('search_by');

		$data = array(
            'query_string' => $search_value,
            'module' => "proposal",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/proposal/index/' . $query_id . '');
    }

    function save_proposal(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
		$NoProposal = $this->input->post('noproposal');
		$NamaProposal = $this->input->post('namaproposal');
		$Tanggal = $this->input->post('v_tgl_dokumen');
		$Divisi = $this->input->post('divisi');
		$KdAktivitas = $this->input->post('kdaktivitas');
		$KdAktivitas = $this->input->post('kdaktivitas');
		$PeriodeAwal = $this->input->post('v_tgl_periode_awal');
		$PeriodeAkhir = $this->input->post('v_tgl_periode_akhir');
		$LatarBelakang = $this->input->post('latar_blk');
		$Tujuan = $this->input->post('tujuan');
		$Mekanisme = $this->input->post('mekanisme');
		$sts = $this->input->post('v_status');
		$pic = $this->input->post('employee_pic');
		$Approval1Name= $this->input->post('employee_approval1');
		$Status_approve1= $this->input->post('v_approve1');
		$Approval2Name= $this->input->post('employee_approval2');
		$Status_approve2= $this->input->post('v_approve2');
		$Approval3Name= $this->input->post('employee_approval3');
		$Status_approve3= $this->input->post('v_approve3');
		$v_status= $this->input->post('v_status');

		if(!empty($Status_approve1)){
		$approve_date = date('Y-m-d');
		$sts="1";
		}else if(!empty($Status_approve2)){
		$approve_date = date('Y-m-d');
		$sts="1";
		}else{
		$approve_date = '0000-00-00';
		}

		// detail
        $Keterangan1= $this->input->post('v_ket_detail');
        $HargaSatuan1 = $this->input->post('v_harga_detail');
        $Qty1 = $this->input->post('v_qty_detail');

        // target
        $Keterangan2= $this->input->post('v_ket_target');
        $HargaSatuan2 = $this->input->post('v_harga_target');
        $Qty2 = $this->input->post('v_qty_target');

        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($Tanggal));

		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;

    	$user = $this->session->userdata('username');

    	//nodokumen
        $v_no_dokumen = $mylib->get_code_counter2($this->db->database, "proposal","NoProposal", "PPS", $data['bulan'], $data['tahun']);

    	//insert header
    	$data = array
    	     ('NoProposal'=> $v_no_dokumen,
             'NamaProposal'=> $NamaProposal,
             'Tanggal'=> $mylib->ubah_tanggal($Tanggal),
             'KdDivisi'=>$Divisi,
             'KdAktivitas'=>$KdAktivitas,
             'PeriodeAwal'=> $mylib->ubah_tanggal($PeriodeAwal),
             'PeriodeAkhir' => $mylib->ubah_tanggal($PeriodeAkhir),
             'LatarBelakang' => $LatarBelakang,
             'Tujuan'=>$Tujuan,
             'Mekanisme'=>$Mekanisme,
             'PIC'=>$pic,
			 'Status'=>$v_status,
             'Approval1Name'=>$Approval1Name,
			 'Approval1Date'=>$approve_date,
			 'Status_approve1'=>$Status_approve1,
             'Approval2Name'=>$Approval2Name,
			 'Approval2Date'=>$approve_date,
			 'Status_approve2'=>$Status_approve2,
			 'Approval3Name'=>$Approval3Name,
			 'Approval3Date'=>$approve_date,
			 'Status_approve3'=>$Status_approve3,
             'AddDate'=>date('Y-m-d'),
             'AddUser'=>$user
             );
        $this->db->insert('proposal', $data);

        //insert proposal detail
        for ($x = 0; $x < count($Keterangan1); $x++)
			{
            $Keterangan = $Keterangan1[$x];
            $HargaSatuan = $HargaSatuan1[$x];
            $Qty = $mylib->save_int($Qty1[$x]);
				if($Keterangan!=""){
					$data_detail=array
					(
					'NoProposal'=>$v_no_dokumen,
					'NoUrut'=>$x+1,
					'Keterangan'=>$Keterangan,
					'HargaSatuan'=>$HargaSatuan,
					'Qty'=>$Qty
					);
					$this->db->insert('proposal_detail', $data_detail);
				}
			}

        //insert proposal target
    	for ($y = 0; $y < count($Keterangan2); $y++)
			{
            $Keterangan_ = $Keterangan2[$y];
            $HargaSatuan_ = $HargaSatuan2[$y];
            $Qty_ = $mylib->save_int($Qty2[$y]);

				if($Keterangan_!=""){
					$data_target=array
					(
					'NoProposal'=>$v_no_dokumen,
					'NoUrut'=>$y+1,
					'Keterangan'=>$Keterangan_,
					'HargaSatuan'=>$HargaSatuan_,
					'Qty'=>$Qty_
					);
					$this->db->insert('proposal_target', $data_target);
				}
			}

    }


	function edit_proposal(){

    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$user = $this->session->userdata('username');
		$NoProposal = $this->input->post('noproposal');
		$NamaProposal = $this->input->post('namaproposal');
		$Tanggal = $this->input->post('v_tgl_dokumen');
		$Divisi = $this->input->post('divisi');
		$KdAktivitas = $this->input->post('kdaktivitas');
		$KdAktivitas = $this->input->post('kdaktivitas');
		$PeriodeAwal = $this->input->post('v_tgl_periode_awal');
		$PeriodeAkhir = $this->input->post('v_tgl_periode_akhir');
		$LatarBelakang = $this->input->post('latar_blk');
		$Tujuan = $this->input->post('tujuan');
		$Mekanisme = $this->input->post('mekanisme');
		$pic = $this->input->post('employee_pic');
		$Approval1Name= $this->input->post('employee_approval1');
		$Status_approve1= $this->input->post('v_approve1');
		$Approval2Name= $this->input->post('employee_approval2');
		$Status_approve2= $this->input->post('v_approve2');
		$Approval3Name= $this->input->post('employee_approval3');
		$Status_approve3= $this->input->post('v_approve3');
		$v_status= $this->input->post('v_status');
		$v_alasan_reject= $this->input->post('alasan_reject');
		$v_email_approve1= $this->input->post('v_email_approve1');

		/*if(!empty($Status_approve1)){
		$approve_date = date('Y-m-d');
		$sts1="1";
		$v_status="1";

		//cek status approve 2
		$cek_aja2 = $this->proposalmodel->cek_aja2($NoProposal);
		$cek_aja3 = $this->proposalmodel->cek_aja3($NoProposal);
		if((!empty($cek_aja2)) AND (!empty($cek_aja3))){
		$sts2="1";
		$sts3="1";
		$v_status="1";
		}else if((!empty($cek_aja2)) AND (empty($cek_aja3))){
		$sts2="1";
		$sts3="0";
		$v_status="1";
		}else if((empty($cek_aja2)) AND (!empty($cek_aja3))){
		$sts2="0";
		$sts3="1";
		$v_status="1";
		}else{
			$sts2="0";
			$sts3="0";
		$v_status="1";
		}





		}else if(!empty($Status_approve2)){
		$approve_date = date('Y-m-d');
		$sts2="1";
		$v_status="1";

		//cek status approve 1
		$cek_aja1 = $this->proposalmodel->cek_aja1($NoProposal);
		$cek_aja3 = $this->proposalmodel->cek_aja3($NoProposal);
		if(!empty($cek_aja1) AND !empty($cek_aja3)){
		$sts1="1";
		$sts3="1";
		$v_status="1";
		}else if(!empty($cek_aja1) AND empty($cek_aja3)){
		$sts1="1";
		$sts3="0";
		$v_status="1";
		}else if(empty($cek_aja1) AND !empty($cek_aja3)){
		$sts1="0";
		$sts3="1";
		$v_status="1";
		}else{
		$sts1="0";
		$sts3="0";
		$v_status="1";
		}
		}




		else if(!empty($Status_approve3)){
		$approve_date = date('Y-m-d');
		$sts3="1";
		$v_status="1";

		//cek status approve 1
		$cek_aja1 = $this->proposalmodel->cek_aja1($NoProposal);
		$cek_aja2 = $this->proposalmodel->cek_aja2($NoProposal);
		if(!empty($cek_aja1) AND !empty($cek_aja2) ){
		$sts1="1";
		$sts2="1";
		$v_status="1";
		}else if(!empty($cek_aja1) AND empty($cek_aja2) ){
		$sts1="1";
		$sts2="0";
		$v_status="1";
		}else if(empty($cek_aja1) AND !empty($cek_aja2) ){
		$sts1="0";
		$sts2="1";
		$v_status="1";
		}else{
		$sts1="0";
		$sts2="0";
		$v_status="1";
		}
		}else{
		$approve_date = '0000-00-00';
		}*/

		// detail
        $Keterangan1= $this->input->post('v_ket_detail');
        $HargaSatuan1 = $this->input->post('v_harga_detail');
        $Qty1 = $this->input->post('v_qty_detail');

        // target
        $Keterangan2= $this->input->post('v_ket_target');
        $HargaSatuan2 = $this->input->post('v_harga_target');
        $Qty2 = $this->input->post('v_qty_target');

        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
    	$user = $this->session->userdata('username');
    	if($user=="sulis0603" OR $user=="dicky0707" OR $user=="trisno1402"){
    	}else {
    		$tgl_edit = date('Y-m-d');
			$user_edit = $user;
		}

    	//insert header proposal
    	$data = array
    	     ('NamaProposal'=> $NamaProposal,
             'Tanggal'=> $mylib->ubah_tanggal($Tanggal),
             'KdDivisi'=>$Divisi,
             'KdAktivitas'=>$KdAktivitas,
             'PeriodeAwal'=> $mylib->ubah_tanggal($PeriodeAwal),
             'PeriodeAkhir' => $mylib->ubah_tanggal($PeriodeAkhir),
             'LatarBelakang' => $LatarBelakang,
             'Tujuan'=>$Tujuan,
             'Mekanisme'=>$Mekanisme,
             'PIC'=>$pic,
			 'Status'=>$v_status,
             'Approval1Name'=>$Approval1Name,
			 'Status_approve1'=>0,
			 'Status_approve2'=>0,
			 'Status_approve3'=>0,
             'EditDate'=>$tgl_edit,
             'EditUser'=>$user_edit
             );
        $this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

	            //insert proposal detail
				for ($x = 0; $x < count($Keterangan1); $x++)
				{
	            $Keterangan = $Keterangan1[$x];
	            $HargaSatuan = $HargaSatuan1[$x];
	            $Qty = $mylib->save_int($Qty1[$x]);

					$sql = "SELECT * FROM proposal_detail a WHERE a.`NoProposal`='$NoProposal' AND a.Keterangan='$Keterangan';";
					$qry = $this->db->query($sql);
			        $row = $qry->result_array();

			        if(empty($row)){

						if ($Keterangan !=""){
							$data_detail=array
							(
							'NoProposal'=>$NoProposal,
							'NoUrut'=>$x+1,
							'Keterangan'=>$Keterangan,
							'HargaSatuan'=>$HargaSatuan,
							'Qty'=>$Qty
							);

						    $this->db->insert('proposal_detail', $data_detail);
					    }

				    }
				}



			//insert proposal target
			for ($y = 0; $y < count($Keterangan2); $y++)
			{
            $Keterangan_ = $Keterangan2[$y];
            $HargaSatuan_ = $HargaSatuan2[$y];
            $Qty_ = $mylib->save_int($Qty2[$y]);

			if ($Keterangan_ !=""){
				$data_target=array
				(
				'NoProposal'=>$NoProposal,
				'NoUrut'=>$y+1,
				'Keterangan'=>$Keterangan_,
				'HargaSatuan'=>$HargaSatuan_,
				'Qty'=>$Qty_
				);

			$this->db->insert('proposal_target', $data_target);
				}
			}



			if($v_status=="1"){

				//jika approve -----------------------------------------------------------------
				if($Status_approve1=="1" AND $Status_approve2=="0" AND $Status_approve3=="0"){

					$data = array
						 (
						 'Status_approve1'=>1,
						 'Approval1Date'=>date('Y-m-d')
						 );
					$this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

					//di kirim ke approval2
					$sql = "  a.`employee_name`,a.`email` FROM employee a WHERE a.`username`='$Approval2Name';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					//$to = $email_approval1[0]['email'];
					$to_name = $email_approval1[0]['employee_name'];
					$to = $v_email_approve1;
					$to_name = "Team SGV / ".$Approval1Name.".";

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Mohon Approval untuk Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal."</b><br><br>";
					$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;

				}else if($Status_approve1=="1" AND $Status_approve2=="1" AND $Status_approve3=="0"){

					$data = array
						 (
						 'Status_approve1'=>1,
						 'Status_approve2'=>1,
						 'Approval2Date'=>date('Y-m-d')
						 );
					$this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

					//di kirim ke approval3
					$sql = "  a.`employee_name`,a.`email` FROM employee a WHERE a.`username`='$Approval3Name';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					$to = $email_approval1[0]['email'];
					$to_name = $email_approval1[0]['employee_name'];

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Mohon Approval untuk Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal."</b><br><br>";
					$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;

				}else if($Status_approve1=="1" AND $Status_approve2=="1" AND $Status_approve3=="1"){

					$data = array
						 (
						 'Status_approve1'=>1,
						 'Status_approve2'=>1,
						 'Status_approve3'=>1,
						 'Approval3Date'=>date('Y-m-d')
						 );
					$this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

					//di kirim ke team purchasing
					$sql = "  * FROM `function_email` a WHERE a.`func_name`='proposal';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					$to = $email_approval1[0]['email_address'];
					$to_name = "All Team Purchasing";

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal." Telah di Approve 3 Atasan. Mohon Cetak Proposal tersebut.</b><br><br>";
		            $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;


				//jika reject ---------------------------------------------------------------------
				}else if($Status_approve1=="2" AND $Status_approve2=="0" AND $Status_approve3=="0"){
					$data = array
						 (
						 'Status_approve1'=>0,
						 'Status_approve2'=>0,
						 'Status_approve3'=>0,
						 'whoisreject'=>$user,
						 'RejectDate'=>date('Y-m-d'),
						 'Status_Reject'=>1,
						 'Ket_Reject'=>$v_alasan_reject
						 );
					$this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

					// Pembuat
					$sql_by = " a.AddUser FROM proposal a WHERE a.NoProposal='$NoProposal';";
					$email_add = $this->globalmodel->getQuery($sql_by );

					//di kirim ke Pembuat
					$sql = "  a.`employee_name`,a.`email` FROM employee a WHERE a.`username`='".$email_add[0]['AddUser']."';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					$to = $email_approval1[0]['email'];
					$to_name = $email_approval1[0]['employee_name'];

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Reject Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal." Telah DiReject Oleh ".$user.", Karena ".$v_alasan_reject."</b><br><br>";
					$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;

				}else if($Status_approve1=="1" AND $Status_approve2=="2" AND $Status_approve3=="0"){
					$data = array
						 (
						 'Status_approve1'=>1,
						 'Status_approve2'=>0,
						 'Status_approve3'=>0,
						 'whoisreject'=>$user,
						 'RejectDate'=>date('Y-m-d'),
						 'Status_Reject'=>1,
						 'Ket_Reject'=>$v_alasan_reject
						 );
					$this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

					// Pembuat
					$sql_by = " a.AddUser FROM proposal a WHERE a.NoProposal='$NoProposal';";
					$email_add = $this->globalmodel->getQuery($sql_by );

					//di kirim ke Pembuat
					$sql = "  a.`employee_name`,a.`email` FROM employee a WHERE a.`username`='".$email_add[0]['AddUser']."';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					$to = $email_approval1[0]['email'];
					$to_name = $email_approval1[0]['employee_name'];

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Reject Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal." Telah DiReject Oleh ".$user.", Karena ".$v_alasan_reject."</b><br><br>";
					$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;

				}else if($Status_approve1=="1" AND $Status_approve2=="1" AND $Status_approve3=="2"){
					$data = array
						 (
						 'Status_approve1'=>1,
						 'Status_approve2'=>1,
						 'Status_approve3'=>0,
						 'whoisreject'=>$user,
						 'RejectDate'=>date('Y-m-d'),
						 'Status_Reject'=>1,
						 'Ket_Reject'=>$v_alasan_reject
						 );
					$this->db->update('proposal', $data, array('NoProposal'=> $NoProposal));

					// Pembuat
					$sql_by = " a.AddUser FROM proposal a WHERE a.NoProposal='$NoProposal';";
					$email_add = $this->globalmodel->getQuery($sql_by );

					//di kirim ke Pembuat
					$sql = "  a.`employee_name`,a.`email` FROM employee a WHERE a.`username`='".$email_add[0]['AddUser']."';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					$to = $email_approval1[0]['email'];
					$to_name = $email_approval1[0]['employee_name'];

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Reject Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal." Telah DiReject Oleh ".$user.", Karena ".$v_alasan_reject."</b><br><br>";
					$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;

				}else if($Status_approve1=="0" AND $Status_approve2=="0" AND $Status_approve3=="0"){
					//di kirim ke approval1
					$sql = "  a.`employee_name`,a.`email` FROM employee a WHERE a.`username`='$Approval1Name';";
					$email_approval1 = $this->globalmodel->getQuery($sql);

					$to = $email_approval1[0]['email'];
					$to_name = $email_approval1[0]['employee_name'];

					$url = "http://sys.bebektimbungan.com/index.php/transaksi/proposal/index/NoProposal/".$NoProposal;
					$subject = "Notifikasi Proposal Marketing ".$NoProposal;
					$author  = "Auto System";

					$body  = "Dear, ".$to_name."<br><br>";

					$body .= "Mohon Approval untuk Proposal ".$NoProposal." dengan Nama Proposal ".$NamaProposal."</b><br><br>";
					$body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat Proposal Atau Copy Paste Link ini di broswer anda : ".$url;
				}
				$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
			}




			//cek apakah approve1 dan approve2 dan 3 sudah terisi
			$cek_approve = $this->proposalmodel->getCekApprove($NoProposal);

			if(!empty($cek_approve)){
				//update budget terpakai
				//pertama ambil nilai total dari proposal_detail dan proposal_target
				$total_nil_proposal = $this->proposalmodel->getNilaiPro($NoProposal);
				$bln = date('m');
				$thn = date('Y');
				$var  = "Terpakai".$bln;

				//ambil nilai terpakai di bulan tertentu dan tambahkan dengan yang baru
				$budgeting = $this->proposalmodel->getBudgeting($total_nil_proposal[0]['KdAktivitas'],$var);
				$tmbh_jmlh = $total_nil_proposal[0]['total_proposal'] + $budgeting[0]['jml_terpakai'];

				$data_bgt=array(
				$var=>$tmbh_jmlh
				);
				$where_bgt=array(
				'Tahun'=>$thn,
				'KdAktivitas'=>$total_nil_proposal[0]['KdAktivitas']
				);
				//update budget terpakai
				$this->db->update('Budget',$data_bgt,$where_bgt);
			}

    }

	function pulihkan_proposal($proposal){
		$data=array(
					'Status'=>0,
					'Approval1Date'=>'',
					'Status_approve1'=>0,
					'Approval2Date'=>'',
					'Status_approve2'=>0,
					'Approval3Date'=>'',
					'Status_approve3'=>0,
					'whoisreject'=>'',
					'RejectDate'=>'',
					'Status_Reject'=>0,
					'Ket_Reject'=>'',
					);
    	$this->db->update('proposal',$data,array('NoProposal'=>$proposal));
		redirect('/transaksi/proposal/');
    }

	function delete_proposal(){
    	$proposal = $this->input->post('id');
		$this->db->delete('proposal',array('NoProposal'=>$proposal));
    	$this->db->delete('proposal_detail',array('NoProposal'=>$proposal));
    	$this->db->delete('proposal_target',array('NoProposal'=>$proposal));
    }

    function ajax_aktivitas(){
		$KdAktivitas = $this->input->post('id');
		$query = $this->proposalmodel->getAktivitas($KdAktivitas);

	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[KdAktivitas]>$cetak[NamaAktivitas]</option>";

	    }
    }

    function ajax_employee_pic(){
		$var = $this->input->post('id');
		$query = $this->proposalmodel->getSrcEmplPic($var);

	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[username]>$cetak[employee_name]</option>";

	    }
    }

    function ajax_employee_approval1(){
		$var = $this->input->post('id');
		$query = $this->proposalmodel->getSrcEmplApr1($var);

	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[username]>$cetak[employee_name]</option>";

	    }
    }

    function cari_email(){
		$username = $this->input->post('user');
		$query = $this->proposalmodel->getEmailEmployee($username);

	   if(empty($query)){
	   	  $data['email']= '';
	   }else{
	   	  $data['email']= $query->email;
	   }
       echo json_encode($data);
    }

    function ajax_employee_approval2(){
		$var = $this->input->post('id');
		$query = $this->proposalmodel->getSrcEmplApr2($var);

	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[username]>$cetak[employee_name]</option>";

	    }
    }

    function edit($nopro)
    {
		$cek= $this->proposalmodel->get_by_proposal($nopro);
		$data=array(
		'NoProposal'=>$cek[0]['NoProposal'],
		'NamaProposal'=>$cek[0]['NamaProposal'],
		'Tanggal'=>$cek[0]['Tanggal_'],
		'KdDivisi'=>$cek[0]['KdDivisi'],
		'KdAktivitas'=>$cek[0]['KdAktivitas'],
		'PeriodeAwal'=>$cek[0]['PeriodeAwal_'],
		'PeriodeAkhir'=>$cek[0]['PeriodeAkhir_'],
		'LatarBelakang'=>$cek[0]['LatarBelakang'],
		'Tujuan'=>$cek[0]['Tujuan'],
		'Mekanisme'=>$cek[0]['Mekanisme'],
		'PIC'=>$cek[0]['PIC'],
		'Status'=>$cek[0]['Status'],
		'Approval1Name'=>$cek[0]['Approval1Name'],
		'Status_approve1'=>$cek[0]['Status_approve1'],
		'Approval2Name'=>$cek[0]['Approval2Name'],
		'Status_approve2'=>$cek[0]['Status_approve2'],
		'Approval3Name'=>$cek[0]['Approval3Name'],
		'Status_approve3'=>$cek[0]['Status_approve3'],
		'v_email_approve1'=>$cek[0]['emails']
		);

		echo json_encode($data);
    }

	function edit_proposal_detail($nopro)
    {
    	$user = $this->session->userdata('username');
		$query = $this->proposalmodel->getPrDetail($nopro);

		if (empty($query)){
		?>

							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data Proposal Detail</td>

							</tr>";

		<?php
		}else{
      $gtotal = 0;
	    foreach ($query as $cetak) {
        $total = floatval($cetak['Qty'])*floatval($cetak['HargaSatuan']);
        $gtotal+=$total;
	    ?>
							<tr>
								<td align='left'><?php echo $cetak['Keterangan'];?></td>
								<td align='right'>Rp. <?php echo number_format( $cetak['HargaSatuan'] , 2 , ',' , '.' );?></td>
								<td align='center'><?php echo $cetak['Qty'];?></td>
                <td align='center'>Rp. <?php echo number_format( $total , 2 , ',' , '.' ); ?></td>

							    	<td align='center'><?php if($user=='dicky0707' OR $user=='sulis0603' OR $user=='trisno1402'){?>
					                	<?php }else{ ?>
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="delete_detail('<?php echo $nopro; ?>','<?php echo $cetak['Keterangan']; ?>');" >
											<i class="entypo-trash"></i>
										</button>
										<?php } ?>
					                </td>

							</tr>";


		<?php }
      ?>
      <tr style="font-weight: bold;">
        <td colspan="3" align=right>
          Grand Total
        </td colspan="2">
        <td>
          <center>
          Rp. <?= number_format( $gtotal , 2 , ',' , '.' );?>
        </center>
        </td>
      </tr>
      <?php
		}

    }


	function edit_proposal_target($nopro)
    {
    	$user = $this->session->userdata('username');
		$query = $this->proposalmodel->getPrTarget($nopro);

		if (empty($query)){
		?>

							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data Proposal Target</td>

							</tr>";

		<?php
		}else{
	    foreach ($query as $cetak) {
	    ?>
							<tr>
								<td align='left'><?php echo $cetak['Keterangan'];?></td>
								<td align='right'><?php echo $cetak['HargaSatuan'];?></td>
								<!--<td align='center'><?php echo $cetak['Qty'];?></td>-->
							    	<td align='center'><?php if($user=='dicky0707' OR $user=='grammy' OR $user=='trisno1402'){?>
					                	X
										<?php }else{ ?>
										<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="delete_target('<?php echo $nopro; ?>','<?php echo $cetak['Keterangan']; ?>');" >
											<i class="entypo-trash"></i>
										</button>
										<?php } ?>
					                </td>

							</tr>";

		<?php }
		}

    }


	public function delete_detail()
	{
		$NoProposal = $this->input->post('nopro');
		$Keterangan = $this->input->post('barang');
		$this->db->delete('proposal_detail',array('NoProposal'=>$NoProposal,'Keterangan'=>$Keterangan));
		echo json_encode(array("status" => TRUE));
	}


	public function delete_target()
	{
		$NoProposal = $this->input->post('nopro');
		$Keterangan = $this->input->post('barang');
		$this->db->delete('proposal_target',array('NoProposal'=>$NoProposal,'Keterangan'=>$Keterangan));
		echo json_encode(array("status" => TRUE));
	}

	function create_pdf() {
        $id = $this->uri->segment(4);
        //cari budget dan budget terpakai
        $data['getBudget'] = $this->proposalmodel->getBudget($id);

        //jml proposal_detail
        $data['ProDetail'] = $this->proposalmodel->getProDetail($id);

        //jml proposal_target
        $data['ProTarget'] = $this->proposalmodel->getProTarget($id);

        //proposal_detail
        $data['detail'] = $this->proposalmodel->getdetailproposal($id);
        $data['totaldetail'] = $this->proposalmodel->gettotaldetail($id);

        //proposal_detail
        $data['target'] = $this->proposalmodel->gettargetproposal($id);
        $data['totaltarget'] = $this->proposalmodel->gettotaltarget($id);

        //approve1
        $data['approve1'] = $this->proposalmodel->getapprove1($id);

        //approve2
        $data['approve2'] = $this->proposalmodel->getapprove2($id);

        //approve3
        $data['approve3'] = $this->proposalmodel->getapprove3($id);

        //dibuat oleh
        //$user = $this->session->userdata('username');
        //$data['dibuatoleh'] = $this->proposalmodel->getdibuatoleh($user);
        $data['dibuatoleh'] = $this->proposalmodel->getdibuatoleh($id);

		$data['nomor']=$id;
        $html = $this->load->view('transaksi/proposal/pdf_proposal',$data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_proposal.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }



  }
?>
