<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Delivery_Order extends authcontroller {

    function __construct() {
        parent::__construct();
//        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/delivery_ordermodel');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
                
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "delivery_order", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                }
            }
            
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/delivery_order/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/delivery_order/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->delivery_ordermodel->num_deliveryorder_row($arr_search["search"]);
            $data['data'] = $this->delivery_ordermodel->getDeliveryOrderList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('transaksi/delivery_order/list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'pelunasan_hutang', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pelunasan_hutang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pelunasan_hutang/index/' . $query_id . '');
    }

    function add_new() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['warehouse'] = $this->delivery_ordermodel->getWarehouse();
            $data['customer'] = $this->delivery_ordermodel->getCustomer();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/add_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function popupbarang(){
    	$searchby = $this->input->post('searchby');
    	$searchstring = $this->input->post('stSearchingKey');
    	
		if($searchstring != ''){
			$detail = $this->delivery_ordermodel->getBarang($searchby, $searchstring);	
		}else{
			$detail[0]['PCode']='';
			$detail[0]['NamaLengkap']='';
			$detail[0]['BatchNumber']='';
			$detail[0]['ProductionDate']='';
			$detail[0]['RemainQuantity']='';
			$detail[0]['productionid']='';
			$detail[0]['productionid2']='';
		}
		$aDetail['detail']=$detail;
		
		$this->load->view('transaksi/delivery_order/popupbarang', $aDetail);
	}
    
    function getbarang(){
		$data = $this->input->post('data');
		$searchby = $data[0];
		$searchstring = $data[1];
		$barang = $this->delivery_ordermodel->getBarang($searchby, $searchstring);
	}
	
    function view($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['data'] = $this->delivery_ordermodel->getDataHeader($notransaksi);
            $data['row'] = $this->delivery_ordermodel->getDataDetail($notransaksi);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pelunasan_hutang/view_pelunasan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
	
	function getDetail(){
		$data = $this->input->post('data');
		
		$KdSupplier = $data[0];
		$MataUang = $data[1];
		$row = $this->delivery_ordermodel->getDetail($KdSupplier,$MataUang);
		
		$data['num'] = count($row);
		$data['row'] = $row;
		 	
		$this->load->view('transaksi/pelunasan_hutang/pelunasan_hutang_detail', $data);	
	}

   
    function save_data() 
    {
        $mylib = new globallib();
        $tgl = $mylib->ubah_tanggal($this->input->post('tgldokumen'));
        $kdkasbank = $this->input->post('KdKasBank');
        $kd_uang = $this->input->post('kd_uang');
        $kurs = $this->input->post('kurs');
        $nobukti = $this->input->post('NoBukti');
        $kdSupplier = $this->input->post('KdSupplier');
        $keterangan = $this->input->post('keterangan');
        
        // detail
        $nofaktur = $this->input->post('NoFaktur');
        $sisa = $this->input->post('Sisa');
        $bayar = $this->input->post('Bayar');
        
        $user = $this->session->userdata('username');

        $notransaksi = $this->delivery_ordermodel->getNoTransaksi($tgl);
        $nopv = $this->delivery_ordermodel->getNoPV($kdkasbank,$tgl);

        $this->delivery_ordermodel->insertHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan, $user, $nopv);
		$total = $this->delivery_ordermodel->insertDetail($notransaksi,$nofaktur,$sisa,$bayar);
		$this->delivery_ordermodel->insertHutang($tgl,$notransaksi, $nofaktur, $kdSupplier,$kd_uang,$kurs, $bayar);	
		$this->delivery_ordermodel->UpdateHutang($nofaktur,$bayar);	
		$this->delivery_ordermodel->insertMutasiHutang($tgl,$notransaksi, $kdSupplier,$kd_uang,$kurs, $total);	
		$this->delivery_ordermodel->insertPVHeader($nopv,$tgl,$kdkasbank,$kdSupplier,$keterangan,$total,$nobukti,$user);
		$this->delivery_ordermodel->insertPVDetail($nopv,$tgl,$kdSupplier,$bayar,$nobukti, $nofaktur, $user);
		
        redirect('/transaksi/pelunasan_hutang');
    }
}

?>