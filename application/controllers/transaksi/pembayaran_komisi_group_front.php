<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pembayaran_komisi_group_front extends authcontroller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');

		//semua meanggunakan model komisi_model
		$this->load->model('transaksi/pembayaran_komisi_group_model');
		
		//ini untuk ambil kas bank aja
		$this->load->model('transaksi/keuangan/paymentmodel');
	}

	function index()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("all");


		$notrans  = $this->input->post('NoTransaksi');

		//if($sign == "Y"){
			$segs 			= $this->uri->segment_array();
			$arr  			= "index.php/" . $segs[1] . "/" . $segs[2] . "/";
			$id   			= addslashes(trim($this->input->post('stSearchingKey')));
			$id2  			= $this->input->post('date1');
			$with 			= $this->input->post('searchby');

			$data['link']	= $mylib->restrictLink($arr);

			if($with == "TanggalTransaksi"){
				$id = $mylib->ubah_tanggal($id2);
			}

			$this->load->library('pagination');

			$config['full_tag_open'] 	= '<div class="pagination">';
			$config['full_tag_close'] 	= '</div>';
			$config['cur_tag_open'] 	= '<span class="current">';
			$config['cur_tag_close'] 	= '</span>';
			$config['per_page'] 		= '10';
			$config['first_link'] 		= 'First';
			$config['last_link'] 		= 'Last';
			$config['num_links'] 		= 2;
			$config['base_url'] 		= base_url() . 'index.php/transaksi/pembayaran_komisi_group_front/index';
			$page = $this->uri->segment(4);
			$config['uri_segment'] 		= 4;
			$flag1 						= "";



			if($with != ""){
				if($id != "" && $with != ""){
					
					$config['base_url'] = base_url() . 'index.php/transaksi/pembayaran_komisi_group_front/index' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
				else
				{
					$page = "";

				}
			}

			else
			{

				if($this->uri->segment(5) != ""){
					$with = $this->uri->segment(4);
					$id   = $this->uri->segment(5);

					if($with == "TanggalTransaksi"){
						$id = $mylib->ubah_tanggal($id);
						
					}

					$config['base_url'] = base_url() . 'index.php/transaksi/pembayaran_komisi_group_front/index' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
				
			}

			//$notrans="";
			//$data['notrans'] = $notrans;
			
			$config['total_rows'] = $this->pembayaran_komisi_group_model->num_komisi_row(addslashes($id), $with);
			$data['data'] = $this->pembayaran_komisi_group_model->getKomisiList($config['per_page'], $page, addslashes($id), $with);

			$data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
			$this->load->view('transaksi/Pembayaran_komisi_group/Pembayaran_komisi_group_list', $data);

		//}
		//else
		//{
		//	$this->load->view('denied');
		//}
	}

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_pembayaran_komisi_group', $data);
	}


	function bayar()
	{

		//$tgl = $this->session->userdata('Tanggal_Trans');

		$penerima = $this->input->post('penerima');
		

		$this->paymentmodel->locktables('counter,trans_payment_header');
		
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
		if (date('Y-m-d',strtotime($tgl)) > date('Y-m-d',strtotime('2016-10-31'))){
		    $KdKasBank2='01';
		    $kd_no = $this->paymentmodel->getKodeBank($KdKasBank2);
		    $NoDokumen = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun,$bulan);
		}else{
		    $new_no = $this->paymentmodel->getNewNo($tgl);
		    $NoDokumen = $new_no->NoPayment;
		}


	$tgl1 = $this->session->userdata('Tanggal_Trans');

		$data = array(
			'NoDokumen'	    => $NoDokumen,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'JumlahPayment' => $totaljual,
			'Status' => '1'
		);
		

		//print_r($data);
		//die;

		$this->db->insert('trans_payment_header', $data);
		$this->db->update('finance_komisi_header',$data=array('NoDokumen'=>$NoDokumen),$data=array('NoTransaksi'=>$no));
		$this->paymentmodel->unlocktables();
		return $no;
	}


	function add_new()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("add");
		if($sign == "Y"){
			$aplikasi = $this->komisi_model->getDate();
			//$data['mperusahaan'] = $this->retur_barangmodel->getPerusahaan();
			//$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			//$user = $this->session->userdata('username');
			$user = $this->session->userdata('username');
			$data['kasbank']   = $this->paymentmodel->getKasBank($user);
			$data['aplikasi'] = $aplikasi;
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/keuangan/komisi/add_komisi', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	function save_new_komisi()
	{
		$mylib       = new globallib();
		//$mylib->arrprint($_POST);
//		echo "<pre>";print_r($_POST);echo "</pre>";die();

		$user        = $this->session->userdata('userid');
		/*$flag        = $this->input->post('flag');
		$notrans     = $this->input->post('NoTransaksi');
		$tgl         = $this->input->post('tgl');
		$nama        = $this->input->post('namak');
		$kdagent     = $this->input->post('kdagent');
		$ket         = trim(strtoupper(addslashes($this->input->post('ket'))));
		$total       = $this->input->post('total');
		$totaljual   = $this->input->post('totjual');
		$nostruk1    = $this->input->post('nostruk');
		$tgljual1    = $this->input->post('tgljual');
		$pcode1      = $this->input->post('pcode');
		$potongan1   = $this->input->post('potongan');
		$harga1      = $this->input->post('harga');
		$qty1        = $this->input->post('qty');
		$persentase1 = $this->input->post('persentase');
		$pcodesave1  = $this->input->post('savepcode');
		$komisia1  = $this->input->post('komisia');
		$komisib1  = $this->input->post('komisib');
		$komisic1  = $this->input->post('komisic');
		$komisid1  = $this->input->post('komisid');
		$hrgPPN1  = $this->input->post('hrgPPN');*/

		$penerima  = $this->input->post('penerima');
		$NoBukti  = $this->input->post('NoBukti');
		$NoDokumen  = $this->input->post('NoDokumen');
		$komisi1  = $this->input->post('Komisi1');
		$komisi2  = $this->input->post('Komisi2');
		$komisi3  = $this->input->post('Komisi3');
		$komisi4  = $this->input->post('Komisi4');
		$notrans  = $this->input->post('NoTransaksi');
		$tgl  = $this->input->post('TglTransaksi');
		$kdtravel  = $this->input->post('KdTravel');
		$keterangan  = $this->input->post('Keterangan');
		$kdkasbank  = $this->input->post('KdKasBank');
		$bayar 		= $this->input->post('bayar');
		$flag = '';


		
		// 4 field tambahan 
		//$KdKasBank  = $this->input->post('KdKasBank');
		//didatabase untuk 42040050
		//$Penerima  = $this->input->post('Penerima');
		//$NoBukti  = $this->input->post('NoBukti');
				
		$waktu       = date("h:i:s");//$this->input->post('waktu');


		
		//ini adalah untuk insert di finance_komisi_header dan finance_komisi_detail ------------------------------------------------------------
		//if($notrans == ""){
			//insert new header komisi

			
			//$no = $this->insertNewHeader($notrans, $flag, $tgl, $NoBukti, $keterangan, $user, $penerima, $kdkasbank, $NoDokumen);
						
			//insert new header payment	
			$no = $this->insertNewHeaderPayment($notrans,$flag,$mylib->ubah_tanggal($tgl),$NoBukti,$keterangan,$user,$penerima,$kdkasbank, $NoDokumen);

			       
			//insert new detail komisi
			/*for($x = 0; $x < count($pcode1); $x++){
				$pcode      = strtoupper(addslashes(trim($pcode1[$x])));
				$nostruk    = trim($nostruk1[$x]);
				$tgljual    = trim($tgljual1[$x]);
				$harga      = trim($harga1[$x]);
				$qty        = trim($qty1[$x]);
				$komisia    = trim($komisia1[$x]);
				$komisib    = trim($komisib1[$x]);
				$komisic    = trim($komisic1[$x]);
				$komisid    = trim($komisid1[$x]);
				$hrgPPN     = trim($hrgPPN1[$x]);
				$potongan   = trim($potongan1[$x]);
				
				$persentase = trim($persentase1[$x]);
				$pcodesave  = $pcodesave1[$x];

				echo"masuk for";
				die;
				if($pcode != ""){
					$this->insertNewDetail($notrans, $nostruk, $tgljual, $pcode, $qty, $persentase, $harga, $user,$komisia,$komisib,$komisic,$komisid,$hrgPPN,$potongan);
					
				}
			}
*/
			//$sql = "SELECT b.`NoDokumen`,b.`NoBukti`,b.`Keterangan`,d.`KdSubDivisi`,c.Qty, c.Harga, c.`Persentase` FROM `finance_komisi_header` b INNER JOIN `finance_komisi_detail` c ON b.`NoTransaksi`=c.`NoTransaksi` INNER JOIN masterbarang d ON c.`PCode`=d.`PCode` 
            //WHERE b.`NoTransaksi`='$no' GROUP BY d.`KdSubDivisi`";
            
            //$sql="SELECT b.`NoDokumen`,b.`NoBukti`,b.`Keterangan`,d.`KdSubDivisi`,(c.Qty * c.Harga)/(c.`Persentase`/100) AS jumlah FROM `finance_komisi_header` b 
                  //INNER JOIN `finance_komisi_detail` c ON b.`NoTransaksi`=c.`NoTransaksi` INNER JOIN masterbarang d ON c.`PCode`=d.`PCode` 
                 // WHERE b.`NoTransaksi`='$no' GROUP BY d.`KdSubDivisi`";
                 
        	$sql="SELECT b.`NoDokumen`,b.`NoBukti`,b.`Keterangan`,d.`KdSubDivisi` FROM `finance_komisi_group_header` b 
                  INNER JOIN `finance_komisi_group_detail` c ON b.`NoTransaksi`=c.`NoTransaksi` INNER JOIN masterbarang d ON c.`PCode`=d.`PCode` 
                  WHERE b.`NoTransaksi`='$no' GROUP BY d.`KdSubDivisi`;";
			
			$rek = $this->pembayaran_komisi_group_model->Querydata($sql);	

			
			
			$savekdrekening='';
			$no=1;
			foreach($rek as $v){
				$counter     =$no;
				//$Qty 		=$v['Qty'];//echo $Qty."<br>";
				//$Harga 		=$v['Harga'];//echo $Harga."<br>";
				//$Persentase =$v['Persentase'];//echo $Persentase."<br>";die;
				$NoBukti 	=$v['NoBukti'];
				$keterangan =$v['Keterangan'];
				$subdivisi 	=$v['KdSubDivisi'];
				$NoDokumen 	=$v['NoDokumen'];
				
				$this->insertDetail("add", $NoDokumen,$mylib->ubah_tanggal($tgl),$NoBukti,$counter,$keterangan,$user,$savekdrekening,$subdivisi);
				$no=$no+1;	
			}



			//echo $Qty."<br>";
			//echo $Harga."<br>";
			//echo $Persentase."<br>";
			//echo $NoBukti."<br>";
			//echo $NoDokumen."<br>";
			//echo $keterangan;
			//$sum = ($Qty * $Harga)/( $Persentase/100 );
			//echo $sum;die;
				
		    //$this->insertDetail("add",$NoDokumen,$mylib->ubah_tanggal($tgl),$NoBukti,($Qty*$Harga)/($Persentase/100),$keterangan,$user,$savekdrekening,$subdivisi);
			
		 
		
		//}
		//else
		//{

		/*	$this->updateHeader($flag, $notrans, $NoBukti, $keterangan, $tgl, $user);

			for($x = 0; $x < count($notrans); $x++){
				$pcode     = strtoupper(addslashes(trim($notrans[$x])));
				$ketdet    = trim($keterangan[$x]);
				//$qty       = trim($qty1[$x]);
				//$pcodesave = $pcodesave1[$x];
				if ($notrans != "") {
				$this->updateDetail($flag, $no, $pcode, $ketdet, $qty, $tgl, $user);
				}

				echo $notrans;
				echo '<br>';
				echo $ketdet;
				echo '<br>';
				echo $qty;
				die;
			}
			echo "else ga masuk for";
			die;*/
		//}

		redirect('/transaksi/pembayaran_komisi_group_front/');
	}
	
	
	function insertDetail($flag,$NoDokumen,$tgl,$NoBukti,$counter,$keterangan,$user,$savekdrekening,$subdivisi)
	{
		
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_detail');
		if($savekdrekening==""){
			$data = array(
				'NoDokumen'	=> $NoDokumen,
				'TglDokumen' => $tgl,
				'KdRekening' => '42040050',//komisi marketing
				//jumlah dihapus
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => '00',
				'Keterangan' => $keterangan,
				'Urutan'	=> $counter,
				'NoBukti' => $NoBukti,
				'Status' => '1'

			);

			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->insert('trans_payment_detail', $data);
		}
		else 
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				//jumlah dihapus
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => $dept,
				'Keterangan' => $keterangan
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_payment_detail', $data, array('NoDokumen' => $no,'Urutan'=>'1'));
		}
		$this->paymentmodel->unlocktables();
	}
	
	//untuk mendapatkan NoDokumen
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }

	// fungsi insertNewHeader yang ada diatas
	function insertNewHeader($notrans, $flag, $tgl, $NoBukti, $keterangan, $user, $penerima, $kdkasbank, $NoDokumen)

	{
		//$this->komisi_model->locktables('keuangan_pvheader');
		$mylib = new globallib();
		$bulan = substr($tgl, 3, 2);
		$tahun = substr($tgl, - 4);


     
     	$bulan2 = substr($tgl, 5, 2);
		$tahun2 = substr($tgl, 2, 2);

		if (date('Y-m-d',strtotime($tgl)) > date('Y-m-d',strtotime('2016-10-31'))){
		    $KdKasBank2='01';
		    $kd_no = $this->paymentmodel->getKodeBank($KdKasBank2);
		    $NoDokumen = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun2,$bulan2);

		    echo "masuk if";
		    die;
		   
		}else{
		    $new_no = $this->paymentmodel->getNewNo($tgl);
		    $NoDokumen = $new_no->NoPayment;

		    
		}

		$new_no= $this->pembayaran_komisi_group_model->getNewNo($tahun,$bulan);
		$no    = $new_no->NoKomisi;
		$this->db->update('counter', array("NoKomisi"=> $new_no->NoKomisi + 1), array("Tahun"=> $tahun,"Bulan"=> $bulan));
		$data = array(
			'NoTransaksi' => $no,
			'TglTransaksi'=> $mylib->ubah_tanggal($tgl),
			'KdAgent'     => $kdagent,
			'Nama'        => $nama,
			'Keterangan'  => $ket,
			'Total'       => $total,
			'AddDate'     => date("Y-m-d"),
			'AddUser'     => $user,
			'Waktu'       => $waktu,
			'TotalSales'  => $totaljual,
			'KdKasBank'   => $KdKasBank,
			'Penerima'    => $Penerima,
			'NoBukti'     => $NoBukti,
			'NoDokumen'   => $NoDokumen
		);

		$this->db->insert('finance_komisi_group_header', $data);
		$this->db->update('register', array('statuskomisi'=>'1'),array('KdTourLeader'=> $kdagent));
		//$this->komisi_model->unlocktables();
		return $no;
	
	}
	
	
	function insertNewHeaderPayment($notrans, $flag, $tgl, $NoBukti, $keterangan, $user, $penerima, $kdkasbank, $NoDokumen)
	{
		$this->pembayaran_komisi_group_model->locktables('counter,trans_payment_header');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);

		if (date('Y-m-d',strtotime($tgl)) > date('Y-m-d',strtotime('2016-10-31'))){
			
		    $KdKasBank2='01';
		    $kd_no = $this->paymentmodel->getKodeBank($KdKasBank2);
		    $NoDokumen = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun,$bulan);
		   
		}else{
		    $new_no = $this->paymentmodel->getNewNo($tgl);
		    $NoDokumen = $new_no->NoPayment;
		   
		}
		//die;
		$tgl1 = $this->session->userdata('Tanggal_Trans');

		$data1 = array(
			'NoDokumen'	    => $NoDokumen,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $penerima,
			'KdKasBank' 	=> $kdkasbank,
			'KdCostCenter' 	=> '0',
			'KdPersonal' 	=> '0',
			'NoGiro' 		=> ' ',
			'KdBankCair' 	=> ' ',
			'TglCair' 		=> ' ',
			'NoBukti' 		=> $NoBukti,
			'Keterangan' 	=> $keterangan,
			'AddDate'    	=> $tgl1,
			'AddUser'    	=> $user,
			'JumlahPayment' => '',
			'Status' 		=> '1'
		);

		//update

		$data = array(
			'NoDokumen'	    => $NoDokumen,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $penerima,
			'KdKasBank' 	=> $kdkasbank,
			'KdCostCenter' 	=> '0',
			'KdPersonal' 	=> '0',
			'NoGiro' 		=> ' ',
			'KdBankCair' 	=> ' ',
			'TglCair' 		=> ' ',
			'NoBukti' 		=> $NoBukti,
			'Keterangan' 	=> $keterangan,
			'AddDate'    	=> $tgl1,
			'AddUser'    	=> $user,
			'Status' 		=> '1'
		);
		/*print_r($data);
		print_r($data1);
		echo $NoDokumen;
		echo'<br>';
		echo $notrans;
		die;*/

		$this->db->insert('trans_payment_header', $data);
		
		$this->db->update('finance_komisi_group_header',$data1=array('NoDokumen'=>$NoDokumen),$data1=array('NoTransaksi'=>$notrans));

		$this->paymentmodel->unlocktables();
	
		return $notrans;

	}
	
	

	function updateHeader($flag, $notrans, $NoBukti, $keterangan, $tgl, $user)
	{
		// $tgl = $this->session->userdata('Tanggal_Trans');
		// $this->komisi_model->locktables('keuangan_pvheader,keuangan_pvdetail');
		$data = array(
			'NoTransaksi'=> $notrans,
			'NoBukti'    => $NoBukti,
			'Keterangan' => $keterangan,
			//total dihapus
			'EditDate'   => date("Y-m-d"),
			'EditUser'   => $user
		);
		if($flag == "edit"){
			$data['EditDate'] = date("Y-m-d");
			$data['EditUser'] = $user;
			$this->db->update('finance_komisi_group_detail', array('EditDate'=> date("Y-m-d"),'EditUser'=> $user), array('NoTransaksi'=> $no));
		}
		$this->db->update('finance_komisi_group_header', $data, array('NoTransaksi'=> $notrans));
		$this->pembayaran_komisi_group_model->unlocktables();
	}

	
	function updateHeaderPayment($flag,$no,$tgl,$KdKasBank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$tglcair,$bankcair,$penerima)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_header');
		if($jenis=='1')
		{
		   $nogiro = "";
		   $bankcair = "";
		   $tglcair = "0000-00-00";
		}
		$data = array(
		    'TglDokumen'    => $tgl,
			'Jenis'         => $jenis,
            'Penerima'      => $penerima,
		    'KdKasBank' => $kasbank,
			'KdCostCenter' => $costcenter,
			'KdPersonal' => $personal,
			'NoGiro' => $nogiro,
			'KdBankCair' => $bankcair,
			'TglCair' => $tglcair,
			'NoBukti' => $nobukti,
			'Keterangan' => $ket,
			'JumlahPayment' => $jumlahpayment
		);
		if($flag=="edit")
		{
			$data['EditDate'] = $tgl1;
			$data['EditUser'] = $user;
			$this->db->update('trans_payment_detail', array('EditDate'=> $tgl1,'EditUser'=>$user), array('NoDokumen' => $no));
		}
		$this->db->update('trans_payment_header', $data, array('NoDokumen' => $no));

		$this->paymentmodel->unlocktables();
	}
	
	function insertDetailPayment($flag,$no,$tgl,$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening,$dept,$subdivisi)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_detail');
		if($savekdrekening==""){
			$data = array(
				'NoDokumen'	=> $no,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => $dept,
				'Keterangan' => $keterangan,
				'Urutan'	=> $counter,
				'Status' => ' '
			);
			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->insert('trans_payment_detail', $data);
		}
		else 
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => $dept,
				'Keterangan' => $keterangan
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_payment_detail', $data, array('NoDokumen' => $no,'Urutan'=>$counter));
		}
		$this->paymentmodel->unlocktables();
	}

	//ini yang insertNewDetail yang diatas
	function insertNewDetail($no, $nostruk, $tgljual, $pcode, $qty, $persentase, $harga, $user,$komisia,$komisib,$komisic,$komisid,$hrgPPN,$potongan)
	{
		$data = array(
			'NoTransaksi'=> $no,
			'PCode'      => $pcode,
			'NoStruk'    => $nostruk,
			'TglJual'    => $tgljual,
			'Qty'        => $qty,
			'Harga'      => $harga,
			'Persentase' => $persentase,
			//'Komisi4bag' => $persentase,
			'Potongan' => $potongan,
			'Komisi1' => $komisia,
			'Komisi2' => $komisib,
			'Komisi3' => $komisic,
			'Komisi4' => $komisid,
			'hargaPPN' => $hrgPPN,
			'AddDate'    => date("Y-m-d"),
			'AddUser'    => $user
		);
		$this->db->insert('finance_komisi_detail', $data);
		$this->db->update('transaksi_header', array('statuskomisi'=>'1'), array('NoStruk'=> $nostruk));
		//$this->komisi_model->unlocktables();
		return $no;
	}

function getlistBarang()
	{
		$this->load->model('proses/tutup_pos_model');
		$field   = $this->input->post('kdagent');
		$tgl     = $this->tutup_pos_model->getLastDate();
		$tgl2    = $tgl->TglTrans;
		//ambil data register yang blm diambil
		$req = $this->komisi_model->ambilReq($field,$tgl2);// ambil tanggal transaksi dan nomor stiker
		$cekagen = $this->komisi_model->CekAgent($field);//cek group harga ada / tidak
		
		$tglagen ="";
		if(!empty($req)){
			$bb = 1;
			foreach ($req as $k => $v){
				$tglagen .= "( a.Tanggal = '".$v['Tanggal']."'and a.KdAgent = '".$v['NoStiker']."')";
				if($bb != count($req)){
					$tglagen .= "or";
				}
				$bb ++;
			}
		}
		
		
		if(!empty($cekagen)){
			//echo "masuk";die;
			$detail= $this->komisi_model->getKomisiTravel($field,$tglagen);
		}else{
			$detail= $this->komisi_model->getKomisi($field,$tglagen);
		}

		$nilai = "";
		for($a = 0; $a < count($detail); $a++){

			$nilai .= $detail[$a]['NoStruk'] . "||" . $detail[$a]['TglJual'] . "||" . $detail[$a]['PCode'] . "||" . $detail[$a]['NamaLengkap'] . "||" . $detail[$a]['Qty'] . "||" . $detail[$a]['Komisi'] . "||" . $detail[$a]['Harga'] . "||" . $detail[$a]['Nilai'] . "||" . $detail[$a]['ttlnetto']. "||" . $detail[$a]['Disc']. "||" . $detail[$a]['komisi1']. "||" . $detail[$a]['komisi2']. "||" . $detail[$a]['komisi3']. "||" . $detail[$a]['komisi4']. "||" . $detail[$a]['hrgPPN']. "**";
		}
		echo count($detail) . "##" . $nilai;


	}

	function getPCode()
	{
		$kode   = $this->input->post('pcode');
		$name   = $this->input->post('nama');
		$tgl    = $this->input->post('tgl');
		$bulan  = substr($tgl, 3, 2);
		$tahun  = substr($tgl, 6, 4);
		//$fieldmasuk = "QtyMasuk" . $bulan;
		//$fieldakhir = "QtyAkhir" . $bulan;
		//$fieldkeluar = "QtyKeluar" . $bulan;
		$detail = $this->komisi_model->getPCodeDet($kode, $name);
		//                print_r($detail);
		if(!empty($detail)){
			$nilai = $detail->NamaRekening . "*&^%" . $detail->KdRekening; // . "*&^%" . $detail->HargaBeliAkhir;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}

	function getRealPCode()
	{
		$kode = $this->input->post('pcode');
		if(strlen($kode) == 13){
			$mylib = new globallib();
			$hasil = $mylib->findBarcode($kode);
			print_r($hasil);
			die();
			$pcode_hasil = $hasil['nilai'];
			if(count($pcode_hasil) != 0){
				$pcode = $pcode_hasil[0]['PCode'];
			}
			else
			{
				$pcode = "";
			}
		}
		else
		{
			$valpcode = $this->komisi_model->ifPCodeBarcode($kode);
			if(count($valpcode) != 0){
				$pcode = $valpcode->KdRekening;
			}
			else
			{
				$pcode = "";
			}
		}
		echo $pcode;
	}

	function edit_komisi($id)
	{	

		$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
		//if($sign == "Y"){
			$id = $this->uri->segment(4);
			$data['header'] = $this->pembayaran_komisi_group_model->getHeader($id); //udh diganti modelnya


			$data['detail'] = $this->pembayaran_komisi_group_model->getDetail($id);


			$this->load->view('transaksi/pembayaran/pembayaran_komisi_group_front/edit_komisi', $data);
		//}
		//else
		//{
		//	$this->load->view('denied');
		//}
	}

	//    function delete_komisi() {
	//        $mylib = new globallib();
	//        $id = $this->input->post('kode');
	//        //$header = $this->retur_barangmodel->getSumber($id);
	//        $user = $this->session->userdata('userid');
	//        $tgl2 = $this->session->userdata('Tanggal_Trans');
	//        $tgl = $mylib->ubah_tanggal($tgl2);
	//        //$getHeader = $this->retur_barangmodel->getHeader($id);
	//        $getDetail = $this->komisi_model->getDetail($id);
	//        $tahun = substr($getHeader->TglTransaksi, 6, 4);
	//        $lastNo = $this->komisi_model->getNewNo($tahun);
	//        $NoDelete = $id;
	//        $pcode1 = $this->input->post('pcode');
	//        $qty1 = $this->input->post('qty');
	//        $pcodesave1 = $this->input->post('savepcode');
	//
	//        if ((int) $lastNo->NoPaymentv == (int) $NoDelete + 1) {
	//            $this->db->update("setup_no", array("NoPaymentv" => $NoDelete[1]), array("Tahun" => $tahun));
	//        }
	//        $this->komisi_model->locktables('keuangan_pvheader,keuangan_pvdetail');
	//
	//        for ($x = 0; $x < count($pcode1); $x++) {
	//            $pcode = strtoupper(addslashes(trim($pcode1[$x])));
	//            $qty = trim($qty1[$x]);
	//            $pcodesave = $pcodesave1[$x];
	//        }
	//
	//        $this->db->delete('keuangan_pvheader', array('NoTransaksi' => $id));
	//        $this->db->delete('keuangan_pvdetail', array('NoTransaksi' => $id));
	//        $this->komisi_model->unlocktables();
	//    }

	
	function versistruk()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip         = "192.168.0.193";
		$printer    = $this->pembayaran_komisi_group_model->NamaPrinter($ip);
		
		$data['store'] = $this->pembayaran_komisi_group_model->aplikasi();
		$data['header'] = $this->pembayaran_komisi_group_model->getHeader($no);
		$data['detail'] = $this->pembayaran_komisi_group_model->getHeaderForPrint2($no);
		
		/*print_r($data['header']);die;
		if(!empty($data['header'])){
			$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi', $data); // jika ada printernya
		}*/
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id   = $this->uri->segment(4);
		$data['fileName2'] = "lainlain.sss";
		$data['fontstyle'] = chr(27) . chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['pindah_hal'] = "\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['string1'] = "     Dibuat Oleh,                     Disetujui Oleh,";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_lain', $data);
	}
	function versistruk2()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		$data['store'] = $this->pembayaran_komisi_group_model->aplikasi();
		$data['header'] = $this->pembayaran_komisi_group_model->getHeader($no);
		$data['detail'] = $this->pembayaran_komisi_group_model->getHeaderForPrint2($no);
		
		$data['reset']  =chr(27).'@';
		$data['plength']=chr(27).'C';
		$data['lmargin']=chr(27).'l';
		$data['cond']   =chr(15);
		$data['ncond']  =chr(18);
		$data['dwidth'] =chr(27).'!'.chr(24);
		$data['ndwidth']=chr(27).'!'.chr(14);
		$data['draft']  =chr(27).'x'.chr(48);
		$data['nlq']    =chr(27).'x'.chr(49);
		$data['bold']   =chr(27).'E';
		$data['nbold']  =chr(27).'F';
		$data['uline']  =chr(27).'!'.chr(129);
		$data['nuline'] =chr(27).'!'.chr(1);
		$data['dstrik'] =chr(27).'G';
		$data['ndstrik']=chr(27).'H';
		$data['elite']  ='';
		$data['pica']   =chr(27).'P';
		$data['height'] =chr(27).'!'.chr(16);
		$data['nheight']=chr(27).'!'.chr(1);
		$data['spasi05']=chr(27)."3".chr(16);
		$data['spasi1'] =chr(27)."3".chr(24);
		$data['fcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';
		$data['pcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';
		$data['op_cash']=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);
		
		//if(!empty($data['header'])){
	//		//$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
	//		$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi2_new', $data); // jika ada printernya
	//	}
		
		$this->load->helper('text');
		$data['nl']	= "\r\n";
		$html		= $this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi2 _new', $data,TRUE);
		$filename	='komisi'.$no;
		$ext		='ctk';
		header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		header("Content-Transfer-Encoding: binary");
		header('Expires: 0');
		header('Pragma: no-cache');
		print $html;
	}


	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib       = new globallib();
		$printreport = new printreportlib();
		$id          = $this->uri->segment(4);
		$header      = $this->pembayaran_komisi_group_model->getHeader($id);

		$komisi1 	 = $this->input->post('Komisi1');
		$komisi2 	 = $this->input->post('Komisi2');
		$komisi3 	 = $this->input->post('Komisi3');
		$komisi4 	 = $this->input->post('Komisi4');
		$total 	 	 = $this->input->post('total');
		

		$data['header'] = $header;

		$detail = $this->pembayaran_komisi_group_model->getDetailForPrint($id);

		$data['judul1'] = array("NoTransaksi","Tanggal","Group","Penerima","Keterangan","NoDokumen");


		$data['niljudul1'] = array($header[0]['NoTransaksi'],$header[0]['TglTransaksi'],stripslashes($header[0]['KdTravel']),$header[0]['Penerima'],$header[0]['Keterangan'],$header[0]['NoDokumen']);
		

		$data['judul2'] = "";
		$data['niljudul2'] = "";
		$data['judullap'] = "Pembayaran Komisi Group";
		$data['colspan_line'] = 4;
		$data['url'] = "pembayaran_komisi_group_front/printThis/" . $id;
		$data['url2'] = "pembayaran_komisi_group_front/versistruk/" . $id;
		$data['url3'] = "pembayaran_komisi_group_front/versistruk2/" . $id;
		$data['tipe_judul_detail'] = array("normal","normal","kanan","kanan", "kanan", "kanan", "kanan");
		$data['judul_detail'] = array("PCode", "Nama Barang", "Komisi 1", "Komisi 2", "Komisi 3", "Komisi 4" ,"Total");
		$data['panjang_kertas'] = 30;
		$jmlh_baris_lain = 19;
		$data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
		$jml_baris_detail = count($detail) + $this->pembayaran_komisi_group_model->getCountDetail($id);
		if($data['panjang_per_hal'] == 0){
			$data['tot_hal'] = 1;
		}
		else
		{
			$data['tot_hal'] = 1;//ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
		}
		$list_detail = array();
		$detail_attr = array();
		$list_detail_attr = array();
		$detail_page = array();
		$new_array = array();
		$counterBaris = 0;
		$counterRow   = 0;
		$max_field_len= array(0,0,0);
		$sum_netto = 0;
		//                print_r($detail);
		  for ($m = 0; $m < count($detail); $m++) {
		//			$attr = $this->komisi_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
		unset($list_detail);
		$counterRow++;
						 $komisi1 = (float)$detail[$m]['komisi1'];
                            $repkomisi1=str_replace(",","", $komisi1);
                            $hslkomisi1 = str_replace(".", "", $repkomisi1);


                        $komisi2 = $detail[$m]['komisi2'];
                            $repkomisi2=str_replace(",","", $komisi2);
                            $hslkomisi2 = str_replace(".", "", $repkomisi2);


                        $komisi3 = $detail[$m]['komisi3'];
                            $repkomisi3=str_replace(",","", $komisi3);
                            $hslkomisi3 = str_replace(".", "", $repkomisi3);

                         $komisi4 = $detail[$m]['komisi4'];
                            $repkomisi4=str_replace(",","", $komisi4);
                            $hslkomisi4 = str_replace(".", "", $repkomisi4);

                            $total = $hslkomisi1+$hslkomisi2+$hslkomisi3+$hslkomisi4;


		$list_detail[] = stripslashes($detail[$m]['PCode']);
		$list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
		$list_detail[] = stripslashes($hslkomisi1);
		$list_detail[] = stripslashes($hslkomisi2);
		$list_detail[] = stripslashes($hslkomisi3);
		$list_detail[] = stripslashes($hslkomisi4);
		$list_detail[] = stripslashes($detail[$m]['komisi1'])+stripslashes($detail[$m]['komisi2'])+stripslashes($detail[$m]['komisi3'])+stripslashes($detail[$m]['komisi4']);
		//$list_detail[] = stripslashes($detail[$m]['Qty']);
		//$list_detail[] = number_format($detail[$m]['Harga'], 0, '', '.');
		$detail_page[] = $list_detail;
		$max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
		if ($data['panjang_per_hal'] != 0) {
		if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
		$data['detail'][] = $detail_page;
		if ($m != count($detail) - 1) {
		unset($detail_page);
		}
		}
		}
		//$netto = $detail[$m]['Harga'];
		//$sum_netto = $sum_netto + ($netto);
		$total = stripslashes($detail[$m]['komisi1'])+stripslashes($detail[$m]['komisi2'])+stripslashes($detail[$m]['komisi3'])+stripslashes($detail[$m]['komisi4']);
		}
		$data['judul_netto'] = array("Total");
		$data['isi_netto'] = array(number_format($total, 0, '', '.'));
		$data['detail'][] = $detail_page;
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}

}

?>
