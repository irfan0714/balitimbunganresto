<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tutup_pos extends authcontroller {

    function __construct(){
		parent::__construct();
		error_reporting(1);
        $this->load->library('globallib');
         $this->load->model('proses/tutup_pos_model');
    }


    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
	        $data['track'] = $mylib->print_track();
			$data['msg'] = "";
			$last_update_query = $this->tutup_pos_model->getLastDate();
			$data['tanggal']= $last_update_query;
	        $this->load->view('proses/rekap', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	function doThis()
	{
		$mylib = new globallib();
		$id = $this->session->userdata('userid');
		
		$tgl = $this->tutup_pos_model->getLastDate();//echo $tgl->TglTrans;die();
		
		$tahun = substr($tgl->TglTrans,0,4);
        $this->cetak($tgl->TglTrans,$tahun); // cetak rekap harian hanya untuk counter
		
        return "Selesai";
	}
	
        
    function cetak($tgl,$tahun)
	{

		$id = $this->session->userdata('username');
		
        $printer = $this->tutup_pos_model->NamaPrinter($_SERVER['REMOTE_ADDR']);
        $data['nm_printer']     = $printer[0]['nm_printer'];
        $data['store']		= $this->tutup_pos_model->aplikasi();
		$data['header']		= $this->tutup_pos_model->all_trans($tgl,$id);
		$data['kurs']		= $this->tutup_pos_model->kurs();
		
        if(!empty($data['header'])){
//			$this->load->view('proses/cetak_tutup',$data); // jika untuk tes
			$this->load->helper('print');
			$html = $this->load->view('proses/cetak_transaksi',$data, TRUE); // jika ada printernya
			
			$filename	='tutup_pos';
			$ext		='ctk';
			header('Content-Type: application/ctk');
			header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
			header('Cache-Control: private, max-age=0, must-revalidate');
			header('Pragma: public');
			
			print $html;
        }else {
          	echo 'no data';
        }
    }
}
?>