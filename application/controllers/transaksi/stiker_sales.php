<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Stiker_sales extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/stiker_sales_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/stiker_sales/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/stiker_sales/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/transaksi/stiker_sales/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->stiker_sales_model->num_gsa_row($id,$with);
	        $data['gsa_data']  = $this->stiker_sales_model->get_gsa_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/stiker_sales/stiker_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	//$data['tipe']  = $this->stiker_sales_model->getTipe();
	     	$data['niltipe'] = "";
			//$data['personal']  = $this->stiker_sales_model->getPersonal();
			$data['nilsupervisor'] = "";
			//$data['supervisor']  = $this->stiker_sales_model->getSupervisor();
	     	$data['nilpersonal'] = "";
			//$data['gudang']  = $this->stiker_sales_model->getGudang();
	     	$data['nilgudang'] = "";
			$data['track'] = $mylib->print_track();
			$data['sales'] = $this->stiker_sales_model->getSales();
	    	$this->load->view('transaksi/stiker_sales/add_stiker',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_stiker_sales($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_salesman'] = $this->stiker_sales_model->getDetail($id);
	    	$data['supervisor']  = $this->stiker_sales_model->getSupervisor();
	    	//$data['tipe'] 	  = $this->stiker_sales_model->getTipe();
			//$data['personal']  = $this->stiker_sales_model->getPersonal();
			//$data['gudang']	  = $this->stiker_sales_model->getGudang();
			$data['salesman'] = $this->stiker_sales_model->getSales();
			$data['detail_gsa']= $this->stiker_sales_model->getDetailGSA($id);
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/stiker_sales/view_edit_stiker', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_stiker_sales($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$this->db->delete('stiker_sales',array('stk_id'=>$id));
	    	$this->db->delete('stiker_sales_detail',array('stk_id'=>$id));
	    	redirect('/transaksi/stiker_sales/');
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$gsa_id = $this->input->post('id');
     	//header
     	$this->db->delete('gsa', array('gsa_id' => $gsa_id));
     	
     	//detail
     	$this->db->delete('gsa_detail', array('gsa_id' => $gsa_id));
     	
		redirect('/transaksi/gsa/');
	}

    function edit_stiker_sales($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_salesman'] = $this->stiker_sales_model->getDetail($id);
	    	//$data['tipe'] 	  = $this->stiker_sales_model->getTipe();
			//$data['personal']  = $this->stiker_sales_model->getPersonal();
			//$data['gudang']	  = $this->stiker_sales_model->getGudang();
			$data['salesman'] = $this->stiker_sales_model->getSales();
			$data['detail_gsa']= $this->stiker_sales_model->getDetailGSA($id);
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/stiker_sales/view_edit_stiker', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_stiker_sales(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$stk_id	 = $this->input->post('id');
    	$mylib = new globallib();
    	$KdSalesman	 = $this->input->post('employeegsa');
    	$v_tgl_dokumen	 = $this->input->post('v_tgl_dokumen');
    	$tgl = $mylib->ubah_tanggal($v_tgl_dokumen);
    	
    	//detail
    	$nostiker1 = $this->input->post('nostiker');		
    	$user = $this->session->userdata('username');
		
    	
    	$data = array(
    		  'Tanggal'	=> $tgl,
			  'KdSalesman' => $KdSalesman,
              'EditDate'		=> date('Y-m-d'),
              'EditUser'		=> $user
			);
		$this->db->update('Stiker_sales', $data, array('stk_id' => $stk_id));
		
		    //detail
            for ($x = 0; $x < count($nostiker1); $x++) 
			{
            $nostiker = $nostiker1[$x];
            
            $data_stk=array(
            'stk_id'=>$stk_id,
            'NoStiker'=>$nostiker
            );
            
            $this->db->insert('stiker_sales_detail',$data_stk);
            }
		
    	redirect('/transaksi/stiker_sales/edit_stiker_sales/' . $stk_id . '');
    }
    
    
    /*function save_new_supervisor(){
    	
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
		$personal = strtoupper(trim($this->input->post('personal')));
		$gudang = strtoupper(trim($this->input->post('gudang')));
    	$num 	 = $this->stiker_sales_model->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'Kdsupervisor'   => $id,
               'Namasupervisor' => $nama,
			   'KdPersonal' => $personal,
               'KdTipesupervisor' => $tipe,
			   'KdGudang' => $gudang,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('supervisor', $data);
			redirect('master/supervisor');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilpersonal'] = $personal;
			$data['personal']  = $this->stiker_sales_model->getPersonal();
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->stiker_sales_model->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/supervisor/add_supervisor', $data);
		}
	}*/
	
	function save_new_stiker_sales_new(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$KdSalesman	 = $this->input->post('employeegsa');
    	$v_tgl_dokumen	 = $this->input->post('v_tgl_dokumen');
    	$tgl = $mylib->ubah_tanggal($v_tgl_dokumen);
    	
    	//detail
    	$nostiker1 = $this->input->post('nostiker');		
    	$user = $this->session->userdata('username');
    	
    	$data['bulan'] = date('m');
        $data['tahun'] = date('Y');
    	
    	$stk_id = $mylib->get_code_counter2($this->db->database, "stiker_sales","stk_id", "STK", $data['bulan'], $data['tahun']);
    	
			//header
		 	$data = array(
		 	   'stk_id'=>$stk_id,
		 	   'Tanggal' =>$tgl,             
               'KdSalesman' => $KdSalesman,
               'AddDate' 	=> date('Y-m-d'),
               'AddUser' 	=> $user
            );
            $this->db->insert('stiker_sales', $data);
            
            //detail
            for ($x = 0; $x < count($nostiker1); $x++) 
			{
            $nostiker = $nostiker1[$x];
            
            $data_stk=array(
            'stk_id'=>$stk_id,
            'NoStiker'=>$nostiker
            );
            
            $this->db->insert('stiker_sales_detail',$data_stk);
            }
            
            
			redirect('transaksi/stiker_sales');
		
	}
	
	function cek_beauty()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getbeauty($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_coffee()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getcoffee($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_chamber()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getchamber($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_oh()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getoh($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_resto()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getresto($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_be()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getbe($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
	
	function cek_stikeroh()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl =$this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getCekStikerOH($nostiker,$tgl);
	
	 if(empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_stikerresto()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getCekStikerResto($nostiker,$tgl);
	
	 if(empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_stikerbe()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getCekStikerBe($nostiker,$tgl);
	
	 if(empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_stiker_sales()
    {  
     $mylib = new globallib();     
     $nostiker = $this->input->post('id');
	 $tgl_ = $this->input->post('tanggal');
	 $tgl= $mylib->ubah_tanggal($tgl_);
	 
	 $hasilcek = $this->stiker_sales_model->getStikerSales($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_transaksi()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getTransaksi($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_beauty()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getAdaGaBeauty($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_coffee()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getAdaGaCoffee($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_chamber()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getAdaGaChamber($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_std_oh()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getAdaGaStdOh($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_std_res()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getAdaGaStdRes($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_std_be()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->stiker_sales_model->getAdaGaStdBe($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function delete_detail_sales() 
    {
        $id = $this->uri->segment(4);
        $stk_id = $this->uri->segment(5);
        $nostiker = $this->uri->segment(6);
		
		$this->db->delete('stiker_sales_detail',array('id'=>$id));

        redirect('/transaksi/stiker_sales/edit_stiker_sales/' . $stk_id . '');
    }
	
}
?>