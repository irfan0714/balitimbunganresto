<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Adjus extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/adjus_model');
        $this->load->model('proses/posting_model');
    }
    
    
    function test_stock(){
		$mylib_ = new globallib();
		$stock = $mylib_->cek_keseluruhan_stock('770001','05','2018-08-13'); 
		echo $stock;
	}
	
	function getValueItem($thn,$bln){
		
		$tgl = $thn."-".$bln."-31";
		
		//ambil Qty Saldo Awal
		$cekstock = $this->adjus_model->cekSaldoAja($thn);
		
		//ambil Value Saldo Awal
		$no=1;
		foreach( $cekstock AS $val ){
			$cek = $this->adjus_model->cekSaldoAwal($val['Tahun'],$val['PCode']);
			$qty_saldo_awal = $cek->Qty_Saldo_Awal;
			$value_saldo_awal = $cek->Value_Saldo_Awal;
			
			//cari dari PCode itu Qty Penerimaannya selama satu bulan, lalu di tambah dengan saldo awal Qty nya
			$res = $this->adjus_model->cekRG($val['PCode'],$tgl);
			$qty = $res->QtyPcs;
			$total_quantitas = $qty_saldo_awal + $qty;
			
			//cari dari PCode itu Value Penerimaannya selama satu bulan, lalu di tambah dengan saldo awal Value nya
			$values = $rez->ValueRataRata;
			$total_values = $value_saldo_awal + $values;
			
			$harga_rata_rata = $total_values/$total_quantitas;
			if($harga_rata_rata!="" OR $harga_rata_rata<0){
				//echo $no.". ".$val['PCode']." => ".$harga_rata_rata."<br>";
				//simpan
				$data = array('PCode'=>$val['PCode'], 'Harga'=>$harga_rata_rata);
				$this->db->insert('rata_rata_harga',$data);
			}
			
			$no++;
		}
		
		echo "Done";die();
		
	}
	
	function cek_mutasi_janggal($thn,$bln){
		
		$tgl = $thn."-".$bln."-31";
		
		//cek stock
		$cekstock = $this->adjus_model->cekSaldoAja($thn);
		
		foreach( $cekstock AS $val ){
			
		
					//cek saldo awal
					$cek = $this->adjus_model->cekSaldoAwal($val['Tahun'],$val['KdGudang'],$val['PCode']);
					$saldo_awal = $cek->GAwal08;
				
					//cek dimutasi 
					$res = $this->adjus_model->cekMutasi($val['PCode'],$val['KdGudang'],$tgl);
					$in=0;
					$out=0;
					
					for($a=0;$a<count($res);$a++){
					
						if($res[$a]['Jenis']=="I"){
						   $in+=$in + $res[$a]['Qty']; 
						}else{
						   $out+=$res[$a]['Qty'];	
						}
						
						$stock = ($saldo_awal+$in)-$out;
						
						echo $saldo_awal." ## ".$res[$a]['NoTransaksi']." => ".$res[$a]['Tanggal']." = ".$in." - ".$out." = ".$stock." OK... <br>";
						if($stock<0 AND $res[$a]['Jenis']=="O" AND $res[$a]['Gudang']!="15"){
							$data=array(
											'NoMutasi'=>$res[$a]['NoMutasi'],
											'NoKassa'=>$res[$a]['NoKassa'],
											'NoTransaksi'=>$res[$a]['NoTransaksi'],
											'Jenis'=>$res[$a]['Jenis'],
											'KdTransaksi'=>$res[$a]['KdTransaksi'],
											'Gudang'=>$res[$a]['Gudang'],
											'GudangTujuan'=>$res[$a]['GudangTujuan'],
											'Tanggal'=>$res[$a]['Tanggal'],
											'KodeBarang'=>$res[$a]['KodeBarang'],
											'Qty'=>$res[$a]['Qty'],
											'Nilai'=>$res[$a]['Nilai'],
											'Status'=>$res[$a]['Status'],
											'Kasir'=>$res[$a]['Kasir'],
											'Keterangan'=>$res[$a]['Keterangan'],
											'HPP'=>$res[$a]['HPP'],
											'PPN'=>$res[$a]['PPN'],
											'Service_charge'=>$res[$a]['Service_charge']
											);
								
								$this->db->insert('mutasi_karantina',$data);
						}
						
					}
					
			}
		echo "Done";die();
	}
	
	function cari_mutasi_janggal(){
		$mylib_ = new globallib();
		
		//cek saldo awal
		$cek = $this->adjus_model->cekSaldoAwal();
		$saldo_awal = $cek->GAwal08;
		
		//cek dimutasi 
		$res = $this->adjus_model->cekMutasi();
		
		//cek apakah ada kejanggalan
		for($a=0;$a<count($res);$a++){
			//echo $a."->".$res[$a]['Tanggal']." ## ".$res[$a]['Jenis']." ## ".$res[$a]['Qty']."<br>";
			
			//cek apakah di baris pertamanya Out? cek punya saldo awal ga, kalau ga itu masalah.
			if($a==0 AND $res[$a]['Jenis']=="O"){
				
				//cek apakah punya saldo awal, kalau tidak punya stock tapi kok Out, itu masalah
				if($saldo_awal<=0){
					
					//ini masalah,. masa tidak ada stock tau tau bisa Out, karantina dulu.
					$data=array(
								'NoMutasi'=>$res[$a]['NoMutasi'],
								'NoKassa'=>$res[$a]['NoKassa'],
								'NoTransaksi'=>$res[$a]['NoTransaksi'],
								'Jenis'=>$res[$a]['Jenis'],
								'KdTransaksi'=>$res[$a]['KdTransaksi'],
								'Gudang'=>$res[$a]['Gudang'],
								'GudangTujuan'=>$res[$a]['GudangTujuan'],
								'Tanggal'=>$res[$a]['Tanggal'],
								'KodeBarang'=>$res[$a]['KodeBarang'],
								'Qty'=>$res[$a]['Qty'],
								'Nilai'=>$res[$a]['Nilai'],
								'Status'=>$res[$a]['Status'],
								'Kasir'=>$res[$a]['Kasir'],
								'Keterangan'=>$res[$a]['Keterangan'],
								'HPP'=>$res[$a]['HPP'],
								'PPN'=>$res[$a]['PPN'],
								'Service_charge'=>$res[$a]['Service_charge']
								);
					
					$this->db->insert('mutasi_karantina',$data);
				}			
			}
			//kebalikan dari IF yang atas
			else{
				//jika jenis Out, cek dulu apakah punya stock di tanggal sebelumnya?
				if($res[$a]['Jenis']=="O"){
					
					$sql1 = " 
							  SUM(a.`Qty`) AS Qty
							FROM
							  mutasi a 
							WHERE a.`Jenis` = 'I' 
							  AND a.`KodeBarang` = '770001' 
							  AND (
							    a.`Gudang` = '05' 
							    OR a.`GudangTujuan` = '05'
							  ) 
							  AND a.`Tanggal` BETWEEN '".$res[$a]['Tanggal']."'
							  AND '".$res[$a]['Tanggal']."' ;
		            ";
		            $in = $this->globalmodel->getQuery($sql1);
		            $qty_in = $in[0]['Qty'];
		            
		            $sql2 = " 
							  SUM(a.`Qty`) AS Qty
							FROM
							  mutasi a 
							WHERE a.`Jenis` = 'O' 
							  AND a.`KodeBarang` = '770001' 
							  AND (
							    a.`Gudang` = '05' 
							    OR a.`GudangTujuan` = '05'
							  ) 
							  AND a.`Tanggal` BETWEEN '".$res[$a]['Tanggal']."'
							  AND '".$res[$a]['Tanggal']."' ;
		            ";
		            $out = $this->globalmodel->getQuery($sql2);
		            $qty_out = $out[0]['Qty'];
		            
		            $stock = $qty_in - $qty_out;
		            
		            //tidak punya stock, kok bisa out?
		            if($stock<0){
		            	//karantina dulu
						$datax=array(
								'NoMutasi'=>$res[$a]['NoMutasi'],
								'NoKassa'=>$res[$a]['NoKassa'],
								'NoTransaksi'=>$res[$a]['NoTransaksi'],
								'Jenis'=>$res[$a]['Jenis'],
								'KdTransaksi'=>$res[$a]['KdTransaksi'],
								'Gudang'=>$res[$a]['Gudang'],
								'GudangTujuan'=>$res[$a]['GudangTujuan'],
								'Tanggal'=>$res[$a]['Tanggal'],
								'KodeBarang'=>$res[$a]['KodeBarang'],
								'Qty'=>$res[$a]['Qty'],
								'Nilai'=>$res[$a]['Nilai'],
								'Status'=>$res[$a]['Status'],
								'Kasir'=>$res[$a]['Kasir'],
								'Keterangan'=>$res[$a]['Keterangan'],
								'HPP'=>$res[$a]['HPP'],
								'PPN'=>$res[$a]['PPN'],
								'Service_charge'=>$res[$a]['Service_charge']
								);
					
					  $this->db->insert('mutasi_karantina',$datax);
					}
		            
				}
				//jika jenis nya In
				else{
					
					//pertama cek apakah ada out sebelumnya sama dengan In yang sekarang/hari ini?
					list($tahun, $bulan, $tanggal) = explode('-',$res[$a]['Tanggal']);
					
				    $cek_tgl_sebelumnya = $this->adjus_model->cekMutasiAnehSebelumnya($res[$a]['Tanggal']);
					
					//cocokan dengan Qty keluar sebelumnya tapi keluar ga ada stock, kan aneh.
					$QtySebelumnya = ($cek_tgl_sebelumnya->Qty)*1;
					$QtySekarang = ($res[$a]['Qty'])*1;
					
					//bandingkan
					if($QtySebelumnya==$QtySekarang){
						//kalau sama, nah itu in disini aneh, masa Out dulu baru in, aneh kan. maka kita karantina.
						
						$datay=array(
								'NoMutasi'=>$res[$a]['NoMutasi'],
								'NoKassa'=>$res[$a]['NoKassa'],
								'NoTransaksi'=>$res[$a]['NoTransaksi'],
								'Jenis'=>$res[$a]['Jenis'],
								'KdTransaksi'=>$res[$a]['KdTransaksi'],
								'Gudang'=>$res[$a]['Gudang'],
								'GudangTujuan'=>$res[$a]['GudangTujuan'],
								'Tanggal'=>$res[$a]['Tanggal'],
								'KodeBarang'=>$res[$a]['KodeBarang'],
								'Qty'=>$res[$a]['Qty'],
								'Nilai'=>$res[$a]['Nilai'],
								'Status'=>$res[$a]['Status'],
								'Kasir'=>$res[$a]['Kasir'],
								'Keterangan'=>$res[$a]['Keterangan'],
								'HPP'=>$res[$a]['HPP'],
								'PPN'=>$res[$a]['PPN'],
								'Service_charge'=>$res[$a]['Service_charge']
								);
					
					  $this->db->insert('mutasi_karantina',$datay);
					  $this->db->update('mutasi',array('FlagScanMutasi'=>'1'),array('Tanggal'=>$cek_tgl_sebelumnya->Tanggal,'NoTransaksi'=>$cek_tgl_sebelumnya->NoTransaksi,'KodeBarang'=>$cek_tgl_sebelumnya->KodeBarang));
					}
					
					//$this->db->update('mutasi',array('FlagScanMutasi'=>'1'),array('Tanggal'=>$res[$a]['Tanggal'],'NoTransaksi'=>$res[$a]['NoTransaksi'],'KodeBarang'=>$res[$a]['KodeBarang']));
				}
				
			}
			
		}
		echo "Done";die();
		//simpan yang kejanggalan tadi
		
		//benerin yang janggal tadi
		
	}
    
    
    function jurnal(){
    	
    	$data = $this->adjus_model->getRepairJurnal();
    	$no=1;
    	foreach($data AS $val){
    		echo $no."<br>";
    		$NoTransaksi = $val['kodenya']; 
    		$kode = substr($val['kodenya'],0,3);
			$tahun = '2017';
	        $bulan = substr($val['kodenya'],4,2);
	    	$userid = $this->session->userdata('userid');
	    
	    	if($kode!="PI0"){
	    		echo $NoTransaksi." Processing...<br>";
	    		//cek dulu apakah dia PV atau EV
	    		$res = $this->adjus_model->cekRV($NoTransaksi);
	    		if(!empty($res)){
					$this->posting_payment($tahun,$bulan,$userid,$NoTransaksi);
				}else{
					$this->posting_receipt($tahun,$bulan,$userid,$NoTransaksi);
				}
	    		
	    		
			}else{
				
				//jika ADJ ambil dari database lama
				//cari dahulu di jurnalheader_2017nya
				$header2017 = $this->adjus_model->getHeaderJurnal2017($NoTransaksi);
				
				//Lalu cari dahulu di jurnaldetail_2017nya
				$bulanx = substr($val['kodenya'],7,2);				
				$detail2017 = $this->adjus_model->getDetailJurnal2017($header2017[0]['NoReferensi'],$tahun,'09');
				if(!empty($detail2017)){
					
						    echo $NoTransaksi."...<br>";
							foreach($detail2017 AS $value){
								if($value['NoReferensi']!=""){
							$data_detail = array(
												'NoReferensi'=>$value['NoReferensi'],
										        'TglTransaksi'=>$value['TglTransaksi'],
										        'KdRekening'=>$value['KdRekening'],
										        'KeteranganDetail'=>$value['KeteranganDetail'],
										        'KodeJurnal'=>$value['KodeJurnal'],
										        'KdDepartemen'=>$value['KdDepartemen'],
										        'KdSubDivisi'=>$value['KdSubDivisi'],
										        'Project'=>$value['Project'],
										        'CostCenter'=>$value['CostCenter'],
										        'Debit'=>$value['Debit'],
										        'Kredit'=>$value['Kredit'],
										        'Bulan'=>$value['Bulan'],
										        'Tahun'=>$value['Tahun'],
										        'Counter'=>$value['Counter'],
										        'AddDate'=>$value['AddDate'],
										        'EditDate'=>$value['EditDate'],
										        'DeleteDate'=>$value['DeleteDate'],
										        'AddName'=>$value['AddName'],
										        'EditName'=>$value['EditName'],
										        'DeleteName'=>$value['DeleteName'],
										        'JenisJurnal'=>$value['JenisJurnal']
												);  
							$this->db->insert('jurnaldetail',$data_detail);             
						}
				
			    }
			
			
				}
			}
			echo $NoTransaksi."OK <br><br>";
			$no++;
			
		}
		
		echo "Done";die();
    	
	}
	
	
	function posting_payment($tahun, $bulan, $userid, $kodenya)
    {
        $this->db->delete('jurnalheader', array('NoTransaksi' => $kodenya,'KodeJurnal' => 'PV', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $header2017x = $this->adjus_model->getHeaderJurnal($kodenya);
        print_r($header2017x);die();
        foreach($header2017x AS $ref){
			$this->db->delete('jurnaldetail', array('NoReferensi' => $ref['NoReferensi'],'KodeJurnal' => 'PV', 'Bulan' => $bulan, 'Tahun' => $tahun));
		}        
        
        $nodok = '';
        $noposting1 = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 1;

        $header = $this->posting_model->getpaymentCekUlang($tahun, $bulan, $kodenya);
        //echo "<pre>";print_r($header);echo "</pre>";		
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$divisiheader = $header[$m]['divisiheader'];
			$divisidetail = $header[$m]['divisidetail'];
			
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $rekeningheader = $header[$m]['RekeningHeader'];
            $debit	= $header[$m]['Debit'];
            $valdebit = $debit >0 ? $debit : 0;
            $valkredit = $debit >0 ? 0 : $debit*-1;
            $ketdetail = $header[$m]['KetDetail'];
            $ketheader = $header[$m]['KetHeader'];
									
            if ($nodok <> $nodokumen) {
            	
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->posting_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('trans_payment_header', array('NoPosting' => $noposting), array('NoDokumen' => $nodokumen));
                }else
                $noposting = $header[$m]['NoPosting'];
                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $ketheader,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PV',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid);
                //echo "Datanya : <pre>";print_r($data);echo "</pre>";
                $this->db->insert('jurnalheader', $data);
            }   
            
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $divisidetail,
                "Debit" => $valdebit,
                "Kredit" => $valkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $nilkredit = $nilkredit + $debit;
            $totdebit += $valdebit;
            $totkredit += $valkredit;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi"  => $tgldokumen,
	                "KodeJurnal"    => "PV",
	                "KdDepartemen"  => "00",
	                "Project"       => "00",
	                "CostCenter"    => "00",
					"KdSubDivisi"	=> $divisiheader,
	                "KdRekening"    => $rekeningheader,
	                "Debit"         => 0,
	                "Kredit"        => $nilkredit,
	                "KeteranganDetail" => $ketdetail,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);
	                $nilkredit = 0;
	                $counterid = 1;
	                $totdebit = 0;
	                $totkredit = 0;
		        }	
			}	
		}
		
        if ($m > 0) {
            $datadtl = array(
				"NoReferensi"	=> $noposting,
                "TglTransaksi"	=> $tgldokumen,
                "KodeJurnal"	=> "PV",
                "KdDepartemen"	=> "00",
                "Project"		=> "00",
                "CostCenter"	=> "00",
                "KdRekening"	=> $rekeningheader,
				"KdSubDivisi"	=> $divisiheader,
                "Debit"		=> 0,
                "Kredit"	=> $nilkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
    }
    
    
    function posting_receipt($tahun, $bulan, $userid, $kodenya)
    {
        $this->db->delete('jurnalheader', array('NoTransaksi' => $kodenya,'KodeJurnal' => 'RV', 'Bulan' => $bulan, 'Tahun' => $tahun));
        $header2017X = $this->adjus_model->getHeaderJurnal($kodenya);
        foreach($header2017X AS $ref){
			$this->db->delete('jurnaldetail', array('NoReferensi' => $ref['NoReferensi'],'KodeJurnal' => 'RV', 'Bulan' => $bulan, 'Tahun' => $tahun));
		}
        
        $header = $this->posting_model->getreceiptCekUlang($tahun, $bulan, $kodenya);
        $nodok = '';
        $noposting1 = '';
        $nildebit = 0;
        $counterid = 1;

        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
            $divisiheader = $header[$m]['divisiheader'];
			$divisidetail = $header[$m]['divisidetail'];
			
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $rekeningheader = $header[$m]['RekeningHeader'];
            $kredit = $header[$m]['Kredit'];
            $ketdetail = $header[$m]['KetDetail'];
            $ketheader = $header[$m]['KetHeader'];
            
            if ($nodok <> $nodokumen) {
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->posting_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('trans_receipt_header', array('NoPosting' => $noposting), array('NoDokumen' => $nodokumen));
                }else
                	
                	$noposting = $header[$m]['NoPosting'];
            	                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "JenisJurnal" => 'U',
                    "Keterangan" => $ketheader,
                    "Project" => '00',
                    "CostCenter" => '00',
                    "KodeJurnal" => 'RV',
                    "NoTransaksi" => $nodokumen,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid);
                $this->db->insert('jurnalheader', $data);
            }

            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "RV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $divisidetail,
                "Debit" => 0,
                "Kredit" => $kredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $nildebit = $nildebit + $kredit;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < count($header)-1){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
                    "TglTransaksi" => $tgldokumen,
                    "KodeJurnal" => "RV",
                    "KdDepartemen" => "00",
                    "Project" => "00",
                    "CostCenter" => "00",
                    "KdRekening" => $rekeningheader,
					"KdSubDivisi"	=> $divisiheader,
                    "Debit" => $nildebit,
                    "Kredit" => 0,
                    "KeteranganDetail" => $ketdetail,
                    "Bulan" => $bulan,
                    "Tahun" => $tahun,
                    "JenisJurnal" => "U",
                    "AddDate" => $tgldokumen,
                    "AddName" => $userid,
                    "Counter" => $counterid);
	                $this->db->insert('jurnaldetail', $datadtl);
	                $nildebit = 0;
	                $counterid = 1;
	            }   
			}
        }
        if ($m > 0) {
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "RV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $rekeningheader,
				"KdSubDivisi"	=> $divisiheader,
                "Debit" => $nildebit,
                "Kredit" => 0,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
    }
    
	//harga beli untuk paket di masterbarang
    function index() 
    {
     $MPcode = $this->adjus_model->getMPcode();
	 //echo "<pre>";print_r($MPcode);echo "</pre>";die;
	 
	 foreach($MPcode as $v){
		 $DPcode = $this->adjus_model->getDPcode($v['MPCode']);
		 //echo "<pre>";print_r($DPcode);echo "</pre>";die;
		$tot = 0;
		 foreach($DPcode AS $k){
			 $HrgBeli = $this->adjus_model->getHargaBeli($k['DPcode']);
			 $tot += $HrgBeli->HargaBeli * $k['Qty'];
		 }
		 $total = $tot;
		 //echo $total;die;
		 $this->db->update('masterbarang',array('HargaBeli'=>$total),array('PCode'=>$v['MPCode']));
	 }
    } 
    
    function adjus_QtyPO_di_pr(){
		//pertama cari PR dan PCodenya dengan range tanggal di trans_order_barang_detail
		$tgl1 = '2017-05-01';
		$tgl2 = '2017-06-16';
		$dataz = $this->adjus_model->getTransOrder($tgl1, $tgl2);
		
		foreach($dataz AS $v){
			 $this->db->update('trans_pr_detail',array('QtyPO'=>$v['QtyPcs']),array('NoDokumen'=>$v['NoPr'],'PCode'=>$v['PCode']));
		 }
		echo "DONE";die;
	}
    
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'delivery_order', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "delivery_order",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/delivery_order/index/' . $query_id . '');
    }

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			$data['gudang'] = $this->delivery_order_model->getGudang();
            $data['customer'] = $this->delivery_order_model->getCustomer();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/add_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_delivery_order($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->delivery_order_model->getHeader($id);
            $data['gudang'] = $this->delivery_order_model->getGudang();			
            $data['customer'] = $this->delivery_order_model->getCustomer();
            $data["detail_list"] = $this->delivery_order_model->getDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/edit_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_delivery_order($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->delivery_order_model->getHeader($id);
            $data['customer'] = $this->delivery_order_model->getCustomer();
            $data["detail_list"] = $this->delivery_order_model->getDetailList($id);
            $data['msatuan'] = $this->delivery_order_model->getSatuan();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/view_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_warehouse = $this->input->post('v_gudang');
        $v_customer = $this->input->post('v_customer');
        $v_contact_person = $this->input->post('v_contactperson');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
		$pisah_periode					= explode("-",$v_tgl_dokumen);
		$hri_periode					= $pisah_periode[0];
		$bln_periode					= $pisah_periode[1];
		$thn_periode					= $pisah_periode[2];
		
			  //cari stock
			  if($bln_periode=='01'){
			$tabel_field='GKeluar01';
		}else if($bln_periode=='02'){
			$tabel_field='GKeluar02';
		}else if($bln_periode=='03'){
			$tabel_field='GKeluar03';
		}else if($bln_periode=='04'){
			$tabel_field='GKeluar04';
		}else if($bln_periode=='05'){
			$tabel_field='GKeluar05';
		}else if($bln_periode=='06'){
			$tabel_field='GKeluar06';
		}else if($bln_periode=='07'){
			$tabel_field='GKeluar07';
		}else if($bln_periode=='08'){
			$tabel_field='GKeluar08';
		}else if($bln_periode=='09'){
			$tabel_field='GKeluar09';
		}else if($bln_periode=='10'){
			$tabel_field='GKeluar10';
		}else if($bln_periode=='11'){
			$tabel_field='GKeluar11';
		}else if($bln_periode=='12'){
			$tabel_field='GKeluar12';
		}
		
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        // detail
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_qty1 = $this->input->post('v_qty');
        $v_satuan1 = $this->input->post('v_satuan');
        
        //echo $market_pcode1." - ".$market_qty1." - ".$market_satuan1;die;

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Delivery Order
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "deliveryorder","dono", "DO", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di Delivery Order Header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_contact_person, $v_warehouse, $v_note, $user, $v_customer);
        
			
			for ($x = 0; $x < count($v_pcode1); $x++) 
			{
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];	
			
			//ketiga masukkan di Delivery Order Detail
			//$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$v_satuan,$user);
			
			//cocokan konversi terlebih dahulu
				$konversi = $this->delivery_order_model->getKonversi($pcode,$v_satuan);
				
				//jika konversi ada datanya
				if(!empty($konversi)){
					//jika Satuan_To sama dengan Satuan yang dipilih
					if($konversi->Satuan_To==$v_satuan){
						$QtyPcs = $v_qty;
					//jika tidak sama
					}else{
						$QtyPcs = $v_qty * $konversi->amount;
					}
				//jika tidak ada datanya konversi
				}else{
					$QtyPcs = $v_qty;
				}
			
			$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$user, $v_satuan, $QtyPcs);

			//keempat Masukkan di Mutasi
			$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
			
			//kelima masukkan di stock GMasuk[bulan]
			//a. cek dulu apakah di stok udah ada? kalau belum ada insert kalau udah ada update
			$stock = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
			//print_r($stock);die;
			        if($stock->Tahun==$thn_periode and $stock->KdGudang==$v_warehouse and  $stock->PCode==$pcode )
					{
						$this->updateStock($thn_periode,$v_warehouse,$pcode,$stock->$tabel_field,$v_qty,$tabel_field);
					}
					else
					{
						
						$this->insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field);
					}
			
			}			
		} 
		
		
		else if ($flag == "edit") 
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			
			//update deliveyorder
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_contact_person, $v_warehouse, $v_note, $v_status, $user, $v_customer);
            
				        // ubah detail
				        for ($x = 0; $x < count($v_pcode1); $x++) 
				        {
				        	$v_no_dokumen = $this->input->post('v_no_dokumen');
				            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
							$v_namabarang = trim($v_namabarang1[$x]);
							$v_qty = $mylib->save_int($v_qty1[$x]);
							$v_satuan = $v_satuan1[$x];

				            if ($pcode != "") 
				            {
				            	if($v_qty*1>0)
				            	{   
				            	    
									//cek mutasi
									$mutasi = $this->delivery_order_model->cekGetMutasi($v_no_dokumen,$v_warehouse,$mylib->ubah_tanggal($v_tgl_dokumen),$pcode);
							        
							        if($mutasi->NoTransaksi==$v_no_dokumen and $mutasi->KdTransaksi=='R' and $mutasi->Gudang==$v_warehouse and  $mutasi->Tanggal==$mylib->ubah_tanggal($v_tgl_dokumen) and $mutasi->KodeBarang==$pcode )
									{
										$this->updateMutasi($v_no_dokumen,$v_warehouse,$mylib->ubah_tanggal($v_tgl_dokumen),$pcode,$v_qty);
									}
									else
									{
										$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
									}
									
									//ambil qty dari deliveryorderdetail
									$detail2 = $this->delivery_order_model->cekGetDetail($market_pcode,$v_no_dokumen);
									
									//ambil GKeluar di stock
									$stock2 = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
						
									$this->updateStock2($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$v_qty,$tabel_field);
									
									
									//cek delivery order detail
				            		$detail = $this->delivery_order_model->cekGetDetail($pcode,$v_no_dokumen);
									
									//cocokan konversi terlebih dahulu
									$konversi = $this->delivery_order_model->getKonversi($pcode,$v_satuan);
									
									//jika konversi ada datanya
									if(!empty($konversi)){
										//jika Satuan_To sama dengan Satuan yang dipilih
										if($konversi->Satuan_To==$v_satuan){
											$QtyPcs = $v_qty;
										//jika tidak sama
										}else{
											$QtyPcs = $v_qty * $konversi->amount;
										}
									//jika tidak ada datanya konversi
									}else{
										$QtyPcs = $v_qty;
									}
				            		
				            		if($detail->inventorycode==$pcode)
									{
										$this->updateDetail($flag, $v_no_dokumen, $pcode, $v_qty, $market_satuan,$detail->Qty,$user, $v_satuan, $QtyPcs);
									}
									else
									{
										$this->insertDetail($flag, $v_no_dokumen, $pcode, $v_qty, $v_satuan, $user , $v_satuan, $QtyPcs);
									}
									
									
								}
				            }
				        }
				        
				        
			//update di deliveyorder dan deliveryorderdetail
			for ($x = 0; $x < count($v_pcode1); $x++) 
	        {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];

            if ($pcode != "") 
            {
            	if($v_qty*1>0)
            	{
            		//cek apakah sudah ada di deliveryorder
            		$detail = $this->delivery_order_model->cekGetDetail($pcode,$v_no_dokumen);
            		
            		if($detail->inventorycode==$pcode)
					{
						//update deliveryorderdetail
						$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_qty,$detail->Qty, $user, $v_satuan, $QtyPcs);
					}
					else
					{
						//insert jika itu record baru
						$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$user , $v_satuan, $QtyPcs);
					}				
				}
            }
			
			//keempat Masukkan di Mutasi
			$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
			
			//kelima masukkan di stock GMasuk[bulan]
			//a. cek dulu apakah di stok udah ada? kalau belum ada insert kalau udah ada update
			$stock = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
			
			        if($stock->Tahun==$thn_periode and $stock->KdGudang==$v_warehouse and  $stock->PCode==$pcode )
					{
						$this->updateStock($thn_periode,$v_warehouse,$pcode,$stock->$tabel_field,$v_qty,$tabel_field);
					}
					else
					{
						
						$this->insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field);
					}
            		
        }
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/delivery_order/edit_delivery_order/' . $v_no_dokumen . '');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $v_contact_person, $v_warehouse, $v_note, $user, $v_customer) 
    {
        $this->delivery_order_model->locktables('deliveryorder');

        $data = array(
            'dono' => $no_dokumen,
            'dodate' => $tgl_dokumen,
            'contactperson' => $v_contact_person,
            'warehousecode' => $v_warehouse,
            'status' => 0,
            'note' => $v_note,
            'adddate'=>date('Y-m-d'),
            'adduser' => $user,
            'customerid' => $v_customer
        );

        $this->db->insert('deliveryorder', $data);

        $this->delivery_order_model->unlocktables();

    }
    
  
    function updateHeader($no_dokumen, $tgl_dokumen, $v_contact_person, $v_warehouse, $v_note, $v_status,$user, $v_customer) 
    {
        $this->delivery_order_model->locktables('deliveryorder');

        $data = array(
            'dodate' => $tgl_dokumen,
            'contactperson' => $v_contact_person,
            'warehousecode'=>$v_warehouse,
			'status'=>$v_status,
            'note' => $v_note,
            'editdate' => date('Y-m-d'),
            'edituser' => $user,
            'customerid' => $v_customer
        );

        $this->db->update('deliveryorder', $data, array('dono' => $no_dokumen));
        
        $this->delivery_order_model->unlocktables();
    }

    function insertDetail($flag, $no_dokumen, $pcode, $qty, $user , $v_satuan, $QtyPcs) 
    {
        $this->delivery_order_model->locktables('deliveryorderdetail');

        if ($pcode) {
            $data = array(
                'dono' => $no_dokumen,
                'inventorycode' => $pcode,
                'quantity' => $qty,
				'satuan'=>$v_satuan,
				'QtyPcs'=>$QtyPcs,
                'adddate'=>date('Y-m-d'),
                'adduser'=>$user
            );

            $this->db->insert('deliveryorderdetail', $data);
        }

        $this->delivery_order_model->unlocktables();
    }
	
	function insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field) 
    {
        $this->delivery_order_model->locktables('stock');
        if ($pcode) {
            $data = array(
                'Tahun' => $thn_periode,
                'KdGudang' => $v_warehouse,
                'PCode' => $pcode,
                $tabel_field=>$v_qty
            );

            $this->db->insert('stock', $data);
        }

        $this->delivery_order_model->unlocktables();
    }
   	
	function insertMutasi($v_no_dokumen, $v_warehouse, $v_tgl_dokumen, $pcode, $v_qty, $user, $v_note)
    {
        $this->delivery_order_model->locktables('mutasi');

        if ($pcode) {
            $data = array(
                'NoTransaksi' => $v_no_dokumen,
                'Jenis' =>'I',
				'KdTransaksi' => 'R',
				'Gudang' => $v_warehouse,
				'Tanggal'=>$v_tgl_dokumen,
				'KodeBarang'=>$pcode,
				'Qty'=>$v_qty,
				'Status'=>1,
                'Kasir'=>$user,
				'Keterangan'=> $v_note
            );

            $this->db->insert('mutasi', $data);
        }

        $this->delivery_order_model->unlocktables();
    }
	
    function updateDetail($flag,$no_dokumen,$pcode,$qty,$qty_tbl,$user, $satuan, $QtyPcs)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);
    	 
        $this->delivery_order_model->locktables('deliveryorderdetail');

        if ($pcode) 
        {
            $data = array(
                'quantity' => $new_qty,
				'satuan'=>$satuan,
				'QtyPcs'=>$QtyPcs,
                'editdate'=>date('Y-m-d'),
                'edituser'=>$user
            );
            
            $this->db->update('deliveryorderdetail', $data, array('dono' => $no_dokumen,'inventorycode' => $pcode));
        } 
        
        $this->delivery_order_model->unlocktables();
    }
	
	function updateStock($thn_periode,$v_warehouse,$pcode,$jml_stock,$v_qty,$tabel_field)
    {
    	$jml_tambah= $jml_stock + $v_qty;
        $this->delivery_order_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_tambah
            );
            
            $this->db->update('stock', $data, array('Tahun' => $thn_periode,'KdGudang' => $v_warehouse,'PCode' => $pcode));
        } 
        
        $this->delivery_order_model->unlocktables();
    }
    
    function updateStock2($thn_periode,$v_warehouse,$market_pcode,$detail_quantity,$stock_tabel_field,$market_qty,$tabel_field)
    {
    	$jml_update=($stock_tabel_field-$detail_quantity)+$market_qty;
        $this->delivery_order_model->locktables('stock');

        if ($market_pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $market_pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->delivery_order_model->unlocktables();
    }
    
    function updateMutasi($v_no_dokumen,$v_warehouse,$v_tgl_dokumen,$market_pcode,$market_qty)
    {
    	$this->delivery_order_model->locktables('mutasi');

        if ($market_pcode) 
        {
            $data = array(
                'Qty'=>$market_qty
            );
            
            $where= array(
            'NoTransaksi'=>$v_no_dokumen,
            'Jenis'=>'I',
            'KdTransaksi'=>'R',
            'Gudang'=>$v_warehouse,
            'Tanggal'=>$v_tgl_dokumen,
            'KodeBarang'=>$market_pcode            
            );
            
            $this->db->update('mutasi', $data, $where);
        } 
        
        $this->delivery_order_model->unlocktables();
    }
	
	function delete_trans($id) 
    {
	        
			//stock
			//cek
			$detail2 = $this->delivery_order_model->cekGetDetail3($id);
			
			foreach($detail2 as $val){
			$sid = $val['sid'];
			$nodok=  $val['dono'];
			$quantity = $val['quantity'];
			$pcode=  $val['inventorycode'];
			$warehousecode = $val['warehousecode'];
			$adddate = $val['adddate'];
			
			$detail2 = $this->delivery_order_model->cekGetDetail2($pcode,$nodok);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  if($bln_periode=='01'){
				$tabel_field='GKeluar01';
			}else if($bln_periode=='02'){
				$tabel_field='GKeluar02';
			}else if($bln_periode=='03'){
				$tabel_field='GKeluar03';
			}else if($bln_periode=='04'){
				$tabel_field='GKeluar04';
			}else if($bln_periode=='05'){
				$tabel_field='GKeluar05';
			}else if($bln_periode=='06'){
				$tabel_field='GKeluar06';
			}else if($bln_periode=='07'){
				$tabel_field='GKeluar07';
			}else if($bln_periode=='08'){
				$tabel_field='GKeluar08';
			}else if($bln_periode=='09'){
				$tabel_field='GKeluar09';
			}else if($bln_periode=='10'){
				$tabel_field='GKeluar10';
			}else if($bln_periode=='11'){
				$tabel_field='GKeluar11';
			}else if($bln_periode=='12'){
				$tabel_field='GKeluar12';
			}			
			//ambil GKeluar di stock
			$stock2 = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			}
			
			//mutasi
			$detail3 = $this->delivery_order_model->cekGetDetail3($id);
			foreach($detail3 as $val){
			$sid = $val['sid'];
			$nodok=  $val['dono'];
			$quantity = $val['quantity'];
			$pcode=  $val['inventorycode'];
			$warehousecode = $val['warehousecode'];
			$adddate = $val['adddate'];
			$this->db->delete('mutasi', array('Tanggal' => $adddate, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));			
			}
			
			//deliveryorderdetail
            $this->db->delete('deliveryorderdetail', array('dono' => $id));	

			//deliveryorder
            $this->db->delete('deliveryorder', array('dono' => $id));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/delivery_order/');
    }

    function delete_detail() 
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);
		
        //pertama harus kurangi angka di stock
		 //ambil qty dari deliveryorderdetail
			$detail2 = $this->delivery_order_model->cekGetDetail2($pcode,$nodok);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  if($bln_periode=='01'){
				$tabel_field='GKeluar01';
			}else if($bln_periode=='02'){
				$tabel_field='GKeluar02';
			}else if($bln_periode=='03'){
				$tabel_field='GKeluar03';
			}else if($bln_periode=='04'){
				$tabel_field='GKeluar04';
			}else if($bln_periode=='05'){
				$tabel_field='GKeluar05';
			}else if($bln_periode=='06'){
				$tabel_field='GKeluar06';
			}else if($bln_periode=='07'){
				$tabel_field='GKeluar07';
			}else if($bln_periode=='08'){
				$tabel_field='GKeluar08';
			}else if($bln_periode=='09'){
				$tabel_field='GKeluar09';
			}else if($bln_periode=='10'){
				$tabel_field='GKeluar10';
			}else if($bln_periode=='11'){
				$tabel_field='GKeluar11';
			}else if($bln_periode=='12'){
				$tabel_field='GKeluar12';
			}			
			//ambil GKeluar di stock
			$stock2 = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			
		   //hapus dimutasi
			$this->db->delete('mutasi', array('Tanggal' => $v_tgl_dokumen, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
			
		    //baru hapus di deliveryorder
			$this->db->delete('deliveryorderdetail', array('sid' => $detail2->sid, 'inventorycode' => $pcode, 'dono' => $nodok));
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/delivery_order/edit_delivery_order/' . $nodok . '');
    }           

	function updateStock3($thn_periode,$v_warehouse,$pcode,$detail_quantity,$stock_tabel_field,$tabel_field)
    {
    	$jml_update=$stock_tabel_field-$detail_quantity;
        $this->delivery_order_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->delivery_order_model->unlocktables();
    }
	
	function satuan()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->delivery_order_model->getSatuanDetail($pcode);
     
     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }     
    }
    
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "S U R A T   J A L A N";
		$data["header"] 	= $this->delivery_order_model->getHeader($nodok);
		$data["detail"] 	= $this->delivery_order_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_do', $data);
	}
	
	
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "delivery-order.txt";
		
		$header = $this->delivery_order_model->getHeader($nodok);
		$detail = $this->delivery_order_model->getDetail_cetak($nodok);
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			$arr_data["detail_namasatuan"][$counter] = $val["NamaSatuan"];
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "S U R A T  J A L A N";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->dono)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->dono;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->adddate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->adddate));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Gudang";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Keterangan; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Kepada";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Kepada"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                
                
                $echo.="\r\n";
                
                $echo.=$arr_epson["cond"]."Alamat";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Alamat"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Alamat;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Alamat));$i++)
                {
                    $echo.=" ";
                }
                $echo.="\r\n";             
                   
            }          
           
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
				$namasatuan = $arr_data["detail_namasatuan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namasatuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($namasatuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->note;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->dono,"delivery-order");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->dono,"delivery-order");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "delivery-order",
		            'noreferensi' => $header->dono,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='delivery-order-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
		
}

?>