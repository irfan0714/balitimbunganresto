<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class rencana_bayar extends authcontroller {

    function __construct() {
        parent::__construct();
//        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/rencana_bayarmodel');
    }

    function index() 
    {   
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
                
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            
            $user = $this->session->userdata('username');
            

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "rencana_bayar", $user);
                if(!empty($resSearch)){
					$arrSearch = explode("&", $resSearch->query_string);
                $id_search = $resSearch->id;
				}
                
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_keyword = explode("=", $arrSearch[1]); // tgldari
                    $arr_search["search"]["tgldari"] = $search_keyword[1];
                    $search_keyword = explode("=", $arrSearch[2]); // search keyword
                    $arr_search["search"]["tglsampai"] = $search_keyword[1];
                    $search_keyword = explode("=", $arrSearch[3]); // search keyword
                    $arr_search["search"]["status"] = $search_keyword[1];
                }
            }
            
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/rencana_bayar/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/rencana_bayar/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];
           

            $config['total_rows'] = $this->rencana_bayarmodel->num_rencana_bayar_row($arr_search["search"]);
            $data['data'] = $this->rencana_bayarmodel->getRencanaBayarList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('transaksi/rencana_bayar/rencana_bayar_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'rencana_bayar', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_tgldari=".$mylib->ubah_tanggal($this->input->post('tgldari'));
		$search_value .= "&search_tglsampai=".$mylib->ubah_tanggal($this->input->post('tglsampai'));
		$search_value .= "&search_status=".$this->input->post('Jenis');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "rencana_bayar",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/rencana_bayar/index/' . $query_id . '');
    }

    function add_new() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['supplier'] = $this->rencana_bayarmodel->getSupplier();
            $data['matauang'] = $this->rencana_bayarmodel->getMataUang();
            $data['kasbank'] = $this->rencana_bayarmodel->getKasBank();
            $data['rekeningpph'] = $this->rencana_bayarmodel->getRekeningPPH();
            $data['status']='add';
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/rencana_bayar/add_rencana_bayar', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $data['data'] = $this->rencana_bayarmodel->getDataHeader($notransaksi);
            $data['RekSupplier'] = $this->rencana_bayarmodel->getNoRekSupplier($data['data']['KdSupplier']);
            $seri = substr($notransaksi, 0,2);
            if($seri=="PM"){
				$data['row'] = $this->rencana_bayarmodel->getDataDetail2($notransaksi);	
			}else{
            	$data['row'] = $this->rencana_bayarmodel->getDataDetail($notransaksi);
            }
            $data['dataums'] = $this->rencana_bayarmodel->getDataOutStandingUM($notransaksi);
            $data['kasbank'] = $this->rencana_bayarmodel->getKasBank();
            $data['track'] = $mylib->print_track();
            $data['rekeningpph'] = $this->rencana_bayarmodel->getRekeningPPH();
            
            $data['status']='view';
            $this->load->view('transaksi/rencana_bayar/view_rencana_bayar', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $header = $this->rencana_bayarmodel->getDataHeader($notransaksi);
            $data['RekSupplier'] = $this->rencana_bayarmodel->getNoRekSupplier($header['KdSupplier']);
            $KdSupplier = $header['KdSupplier'];
            $data['data'] = $header;
            $data['status']='edit';
            $seri = substr($notransaksi, 0,2);
            if($seri=="PM"){
				$data['row'] = $this->rencana_bayarmodel->getDataDetail2($notransaksi);	
			}else{
            	$data['row'] = $this->rencana_bayarmodel->getDataDetail($notransaksi);
            }
            $data['dataums'] = $this->rencana_bayarmodel->getOutStandingUM($KdSupplier);
            $data['kasbank'] = $this->rencana_bayarmodel->getKasBank();
            $data['rekeningpph'] = $this->rencana_bayarmodel->getRekeningPPH();
            $data['status']='edit';
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/rencana_bayar/view_rencana_bayar', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function approve($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("del");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $header = $this->rencana_bayarmodel->getDataHeader($notransaksi);
            $KdSupplier = $header['KdSupplier'];
            $data['RekSupplier'] = $this->rencana_bayarmodel->getNoRekSupplier($header['KdSupplier']);
            $data['data'] = $header;
            $data['status']='approve';
            $seri = substr($notransaksi, 0,2);
            if($seri=="PM"){
				$data['row'] = $this->rencana_bayarmodel->getDataDetail2($notransaksi);	
			}else{
            	$data['row'] = $this->rencana_bayarmodel->getDataDetail($notransaksi);
            }
            $data['dataums'] = $this->rencana_bayarmodel->getOutStandingUM($KdSupplier);
            $data['kasbank'] = $this->rencana_bayarmodel->getKasBank();
            $data['rekeningpph'] = $this->rencana_bayarmodel->getRekeningPPH();
            $data['status']='approve';
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/rencana_bayar/view_rencana_bayar', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function void($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $header = $this->rencana_bayarmodel->getDataHeader($notransaksi);
            $KdSupplier = $header['KdSupplier'];
            $data['RekSupplier'] = $this->rencana_bayarmodel->getNoRekSupplier($header['KdSupplier']);
            $data['data'] = $header;
            $data['status']='Void';
            $seri = substr($notransaksi, 0,2);
            if($seri=="PM"){
				$data['row'] = $this->rencana_bayarmodel->getDataDetail2($notransaksi);	
			}else{
            	$data['row'] = $this->rencana_bayarmodel->getDataDetail($notransaksi);
            }
            $data['dataums'] = $this->rencana_bayarmodel->getOutStandingUM($KdSupplier);
            $data['kasbank'] = $this->rencana_bayarmodel->getKasBank();
            $data['rekeningpph'] = $this->rencana_bayarmodel->getRekeningPPH();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/rencana_bayar/view_rencana_bayar', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function print_pdf($notransaksi) 
    {
        $mylib = new globallib();
        $data['msg'] = "";
        
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        
        $data['data'] = $this->rencana_bayarmodel->getDataHeader($notransaksi);
        $seri = substr($notransaksi, 0,2);
        if($seri=="PM"){
			$data['row'] = $this->rencana_bayarmodel->getDataDetail2($notransaksi);	
		}else{
        	$data['row'] = $this->rencana_bayarmodel->getDataDetail($notransaksi);
        }
        //\/ TOLONG HILANGKAN INI KALO RIKA SUDAH SELESAI PRINT
        //$data['dataums'] = $this->rencana_bayarmodel->getDataOutStandingUM($notransaksi);
        $data['track'] = $mylib->print_track();
        $html = $this->load->view('transaksi/rencana_bayar/cetak_rencana_bayar', $data, true);
    	$this->load->library('m_pdf');
	
    	$pdfFilePath = "rencanabayar.pdf";
    	$pdf = $this->m_pdf->load();
    	$pdf->WriteHTML($html);

    	$pdf->Output();
    	//echo $html;
    	exit;
    }
    
	
	function getDetail(){
		$mylib = new globallib();
		$data = $this->input->post('data');
		
		$KdSupplier = $data[0];
		$MataUang = $data[1];
		$type = $data[2];
		$tgl = $mylib->ubah_tanggal($data[3]);
		
		//akan ada filter jika type 1 / Hutang Datang atau 2 / Hutang Lain lain
		//if($type=="1"){
		$row = $this->rencana_bayarmodel->getDetail($KdSupplier,$MataUang, $tgl);
		//}else{
		//$row = $this->rencana_bayarmodel->getDetail2($KdSupplier,$MataUang, $tgl);	
		//}
		$data['num'] = count($row);
		$data['row'] = $row;
		$data['dataums'] = $this->rencana_bayarmodel->getOutStandingUM($KdSupplier);
		$data['data']=array('BiayaAdmin'=>0,'NilaiPPH'=>0,'Pembulatan'=>0);
		$data['rekeningpph'] = $this->rencana_bayarmodel->getRekeningPPH();
		$data['status']='add';
				 	
		$this->load->view('transaksi/rencana_bayar/rencana_bayar_detail', $data);	
	}
	
	function ajax_rekeningsupplier(){
		$kdsupplier = $this->input->post('KdSupplier');
		$noreksupplier = $this->rencana_bayarmodel->getNoRekSupplier($kdsupplier);
		echo "<option value=''> -- Pilih Rek Supplier --</option>";
     	foreach ($noreksupplier as $var) {
	 		echo "<option value='".$var['NoRekSupplier']."'>".$var['NoRekSupplier']."</option>";
	    }	
	}

   
    function save_data() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $notransaksi = $this->input->post('notransaksi');
        $tgl = $mylib->ubah_tanggal($this->input->post('tgldokumen'));
        $kdkasbank = $this->input->post('KdKasBank');
        $kd_uang = $this->input->post('kd_uang');
        $kurs = $this->input->post('kurs');
        $nobukti = $this->input->post('NoBukti');
        $kdSupplier = $this->input->post('KdSupplier');
        $keterangan = addslashes($this->input->post('keterangan'));
        $v_type = $this->input->post('v_type');
        $biayaadmin = $this->input->post('vbiayaadmin');
        $pembulatan = $this->input->post('vpembulatan');
        $pph = $this->input->post('vpph');
        $flag = $this->input->post('flag');
        $noreksupplier = $this->input->post('NoRekSupplier');
        $kdrekeningpph = $this->input->post('vkdrekeningpph');
        
        // detail
        $nofaktur = $this->input->post('NoFaktur');
        $sisa = $this->input->post('Sisa');
        $bayar = $this->input->post('Bayar');
        
        $noum = $this->input->post('NoUM');
        $nilaium = $this->input->post('NilaiUM');
        
        $user = $this->session->userdata('username');
        
        try {
	       $this->db->trans_begin();
	        if($flag=='add'){
	        	
				//if($v_type=="1"){
		        	$notransaksi = $this->rencana_bayarmodel->getNoTransaksi($tgl);
		        //}else{
				//	$notransaksi = $this->rencana_bayarmodel->getNoTransaksi2($tgl);	
				//}
				
					
				$this->rencana_bayarmodel->insertHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan, $user, $biayaadmin,$pembulatan, $pph, $noreksupplier,$kdrekeningpph);
				
			}else{
				$this->rencana_bayarmodel->UpdateHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan, $user, $biayaadmin,$pembulatan, $pph,$flag, $noreksupplier, $kdrekeningpph);
				
			}
			$this->rencana_bayarmodel->insertDetail($notransaksi,$nofaktur,$sisa,$bayar);
			$this->rencana_bayarmodel->insertDetailUM($notransaksi,$noum,$nilaium);
			$this->rencana_bayarmodel->UpdateHutang($nofaktur,$bayar, $flag);	
			$this->db->trans_commit();
		}catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
			$this->db->trans_rollback();
		}
        redirect('/transaksi/rencana_bayar');
    }
    
    function ajax_supplier(){
		$var = $this->input->post('id');
		$supplier = $this->rencana_bayarmodel->getSupplier($var);
		 
	 	echo "<option value=''> -- Pilih Supplier --</option>";
     	foreach ($supplier as $var) {
	 		echo "<option value=".$var['KdSupplier'].">".$var['Nama']."</option>";
	    }
    }
    
    function test(){
    	$tgl='2017-10-05';
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoTransaksi FROM pelunasan_hutang_header WHERE NoTransaksi LIKE 'PH%$blnthn' ORDER BY NoTransaksi DESC";
		
		$result =  $this->rencana_bayarmodel->getArrayResult($sql);
		print_r($result);
		if(count($result)==0){
			$notransaksi = 'PH00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoTransaksi'],2,5);
			echo 'a : '.$nourut ."<br>";
			$nourut = 100001 + ($nourut*1);
			echo 'b : '.$nourut ."<br>";
			$nourut = substr($nourut,-5);
			echo 'c : '.$nourut ."<br>";
			$notransaksi = 'PH' . $nourut .'-'.$blnthn;
		}
	}
}

?>