<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class order_touch extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/order_touch_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
			//print_r($data);die();
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/order_touch/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/order_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/order_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("No Kassa","No Transaksi","Tanggal","Total Item","Table","Person");
	        $config['total_rows']		= $this->order_touch_model->num_order_row($id,$with);
	        $data['data']	= $this->order_touch_model->getList($config['per_page'],$page,$id,$with);
			$data['tanggal'] = $this->order_touch_model->getDate();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/order_touch/order_touch_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
		    $ipaddres                   = $this->session->userdata('ip_address');
			$nokassa                    = $this->order_touch_model->getnokassa($ipaddres);
			$datakassa                  = $this->order_touch_model->getkassa($ipaddres);
			$data['NoKassa'] 			= $nokassa;
			$data['msg'] 				= "";
			$data['datakassa'] 			= $datakassa;			
			
			$data['barang']	 		    = $this->order_touch_model->getBarang();
			$data['kategori']	 		= $this->order_touch_model->getKategori();
			$data['subkategori']	 	= $this->order_touch_model->getSubKategori();
			$data['userid']	 	        = $this->order_touch_model->getUserId();
			$data['lokasi']	 	        = $this->order_touch_model->getLokasi();
			$data['keterangan']	 	    = $this->order_touch_model->getKeterangan();
			$data['store']				= $this->order_touch_model->aplikasi();
			$aplikasi = $this->order_touch_model->getDate();
			$data['tanggal'] = $aplikasi->TglTrans2;
			$this->load->view('transaksi/order_touch/add_order_touch',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	function edit($no){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$aplikasi = $this->order_touch_model->getDate();
			$data['header'] = $this->order_touch_model->getHeader($no);
			if($data['header']->FlagKonfirmasi=='T'&&$data['header']->TglDokumen2==$aplikasi->TglTrans){
				$data['detail'] = $this->order_touch_model->getDetail($no);
				$data['qtybarcode'] = $aplikasi->QtyBarcode;
				$hasil = $this->order_touch_model->FindKendaraan($data['header']->KdKendaraan,"edit",$no);
				$string = "";
				if(!empty($hasil)){
					for($p=0;$p<count($hasil);$p++)
					{
						$string .= $hasil[$p]['NoAmbil']."**";
					}
				}
				$data['noambilkendaraan'] = $string;
				$this->load->view('transaksi/order_touch/edit_order_touch',$data);
			}
			else
			{
				redirect('/transaksi/order_touch/');
			}
		}
		else{
			$this->load->view('denied');
		}
    }
	
	function findBarcode()
	{
		$barc = $this->input->post("barcode");
		$hasil = $this->order_touch_model->FindBar($barc);
		$string = "";
		if(!empty($hasil))
		{
			$string = $hasil->PCode;
		}
		echo $string;
	}
	function getRealPCode()
	{
		$kode = $this->input->post('pcode');
		//echo $kode;
		$valpcode = $this->order_touch_model->ifPCodeBarcode($kode);
		//echo count($valpcode);
		if(count($valpcode)!=0)
		{
			$pcode = $valpcode->PCode.'*&^%'.$valpcode->Jenis.'*&^%'.$valpcode->NamaInitial."*&^%".$valpcode->KonversiJualKecil."*&^%".$valpcode->KonversiBesarKecil."*&^%".$valpcode->KonversiTengahKecil."*&^%".$valpcode->KdSatuanJual."*&^%".$valpcode->NamaSatuan;
		}
		else
		{
			$pcode = "";
		}
		echo $pcode;
	}
	
	function getPCode()
	{
		$kode = $this->input->post('pcode');
		$valpcode = $this->order_touch_model->ifPCodeBarcode($kode);
		if(count($valpcode)!=0)
		{
			$detail = $this->order_touch_model->getPCodeDet($pcode);
			$nilai = $detail->NamaInitial."-".$detail->KonversiJualKecil."-".$detail->KonversiBesarKecil."-".$detail->KonversiTengahKecil."-".$detail->KdSatuanJual."-".$detail->NamaSatuan."-".$detail->PCode."-".$jenis_kode;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}
	function getcontact()
	{
		$kode = $this->input->post('pelanggan');
		$detail = $this->order_touch_model->getcontact($kode);
		if(count($detail)!=0)
		{
			$nilai = $detail->Nama;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}
	
	function getpersonal()
	{
		$kode = $this->input->post('personal');
		$detail = $this->order_touch_model->getpersonal($kode);
		if(count($detail)!=0)
		{
			$nilai = $detail->NamaPersonal;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}
	
	function insert_temporary()
	{
		$EditFlg    = $this->input->post('EditFlg');
		$NoUrut     = $this->input->post('NoUrut');
		$pcode      = $this->input->post('pcode');
		$qty        = $this->input->post('qty');
		$satuan     = $this->input->post('satuan');
		$keterangan = $this->input->post('keterangan');
		$notrans    = $this->input->post('notrans');
		$kassa   = $this->input->post('kassa');
		$kasir   = $this->input->post('kasir');
		$store	 = $this->input->post('store');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		//echo "aaaaa";
				
		if($notrans == "--")
		{
		   //print_r($NoTrans);
		   $Trans = $this->order_touch_model->no_trans($kassa);
		   $notrans = $Trans[0]['NoTrans'];
		   //print_r($NoTrans);die();
		}	
		if($EditFlg == 1)
		{
			$this->order_touch_model->DeleteRecord($NoUrut,$kassa);
		}
		$hasil = $this->order_touch_model->order_temp_cek($pcode,$kassa);		
		if($hasil==0) // or $qty<1)
		{
				if($EditFlg == 0)
				{
					$sql = "select NoUrut+1 as NoUrut from order_temp where NoKassa='$kassa' order by NoUrut desc limit 1";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					
					if($qry->num_rows() == 0)
					{
						$NoUrut = 1;
					}
					else
					{
						$NoUrut = $row[0]['NoUrut'];
					}
				}
				$data = array(
						'NoUrut'       => $NoUrut,
						'NoTrans'	   => $notrans,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Satuan'       => $satuan,
						'Keterangan'   => $keterangan,
						'NoKassa'	   => $kassa,
						'Kasir'		   => $kasir,
						'Tanggal'	   => $tgltrans,
						'Waktu'		   => date('H:i:s'),
						'KdStore'	   => $store,
						'Status'	   => 1
						);
						
						$this->db->insert('order_temp', $data);		
		}
		else
		{
			$this->order_touch_model->order_temp_add($qty,$pcode,$notrans,$kassa,$keterangan);
		}	
		echo $notrans;
	}
	function cetak()
	{
		$data = $this->varCetak();	
		$this->load->view('transaksi/order_touch/cetak',$data);
	}
	function cetak2()
	{
		$data = $this->varCetak2();	
		$this->load->view('transaksi/order_touch/cetak',$data);
	}
	function printThis()
	{
		$nokassa = $this->uri->segment(4);
		$notrans = $this->uri->segment(5);
		$hasil = $this->order_touch_model->getprintjob($nokassa);	
		$PrintLangsung = true;
		if(count($hasil)>0)
		{
		    if($hasil[0]['jenisprint']=='1')
			{
			    $PrintLangsung = false;
				for($s=count($hasil)-1;$s>=0;$s--)
				{
					switch($hasil[$s]['jenis'])
						{
							case ($hasil[$s]['jenis'] == 1) : print("Ini Makanan!");
							break;
							case ($hasil[$s]['jenis'] == 2) : print("Ini Minuman!");
							break;
							case ($hasil[$s]['jenis'] == 3) : print("Ini Semua!");
							break;
							default : print("None!");
						}
						/*if(($hasil[$s]['jenis'])=='3')
						{
							$data = array(
								'nokassa'       => $hasil[$s]['kassa2print'],
								'lpt'	   => $hasil[$s]['lpt'],
								'jenis'        => $hasil[$s]['jenis'],
								'nokassadokumen'  => $nokassa,
								'nodokumen'       => $notrans,
								'jenisdokumen'       => "SO"
							);			
							$this->db->insert('printjob', $data);
						}
						if(($hasil[$s]['jenis'])=='1')
						{
							print("Ini Makanan!");
							//$data = $this->varCetakMakanan($nokassa,$notrans);
							//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so3', $data);
						}
						if(($hasil[$s]['jenis'])=='2')
						{
							print("Ini Minuman!");
							//$data = $this->varCetakMakanan($nokassa,$notrans);
							//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so3', $data);
						}*/
				}
			}
		}
		if($PrintLangsung)
		{
			$data = $this->varCetak();
			$data['fileName'] = "order.sss";   
			$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so', $data);
		}
	}
	function printThis2()
	{
		$data = $this->varCetak2();
		$data['fileName'] = "order.ccc";   
        //echo "masuk";die();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so', $data);
		//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so2', $data);
	}
	function varCetak()
	{
	    $nokassa = $this->uri->segment(4);
		$notrans = $this->uri->segment(5);
	    $data['store']		= $this->order_touch_model->aplikasi();
		$data['header']		= $this->order_touch_model->all_trans($nokassa,$notrans);
		$data['detail']		= $this->order_touch_model->det_trans($nokassa,$notrans);
		$data['url'] = "transaksi/order_touch/printThis/".$nokassa."/".$notrans;
		return $data;
	}
	
	function varCetakMakanan($kassa,$notrans)
	{
	    //$nokassa = $this->uri->segment(4);
		//$notrans = $this->uri->segment(5);
	    $data['store']		= $this->order_touch_model->aplikasi();
		$data['header']		= $this->order_touch_model->all_trans($kassa,$notrans);
		$data['detail']		= $this->order_touch_model->det_trans_food($kassa,$notrans);
		$data['url'] = "transaksi/order_touch/printThis/".$kassa."/".$notrans;
		return $data;
	}
	
	function varCetak2()
	{
	    $nokassa = $this->uri->segment(4);
		$notrans = $this->uri->segment(5);
	    $data['store']		= $this->order_touch_model->aplikasi();
		$data['header']		= $this->order_touch_model->all_trans($nokassa,$notrans);
		$data['detail']		= $this->order_touch_model->det_trans($nokassa,$notrans);
		$data['url'] = "transaksi/order_touch/printThis2/".$nokassa."/".$notrans;
		return $data;
	}
	function save_trans()
	{
	    $transaksi 		= $this->input->post('transaksi');
		if($transaksi=="yes")
		{
			$EditFlg    = $this->input->post('EditFlg');
			$nourut1    = $this->input->post('nourut');
			$pcode1     = $this->input->post('pcode');
			$qty1       = $this->input->post('qty');
			$harga1     = $this->input->post('harga');
			$satuan1    = $this->input->post('satuan');
			$keterangan1= $this->input->post('keterangan');
			$printer1    = $this->input->post('printer');
			$komisi1    = $this->input->post('komisi');
			$notrans    = $this->input->post('notrans');
			$kassa   = $this->input->post('kassa');
			$kasir   = $this->input->post('kasir');
			$store	 = $this->input->post('store');
			$kategorikassa	 = $this->input->post('kategorikassa');
			$tgltrans = $this->session->userdata('Tanggal_Trans');
			$tahun = substr($tgltrans,0,4);
			$bulan = substr($tgltrans,5,2);
			$Trans = $this->order_touch_model->no_trans($bulan,$tahun,$kategorikassa);
		    $notrans = $Trans[0]['NoTrans'];
			
			//$nokassa 		= $this->input->post('kassa');
			//$totalnya 		= $this->input->post('total_biaya');
			//$pelanggan	    = $this->input->post('pelanggan');
			$idmeja	        = $this->input->post('idtable');
			$idguest        = $this->input->post('idguest');
			$idagent       = $this->input->post('idagent');
			if($idguest=="")$idguest=0;
			$personal	    = $this->input->post('idpersonal');
			$jenisprint	    = $this->input->post('jenisprint');
			$adddate        = date('y-m-d');
			$total	    = $this->input->post('totalsales');
			$sc	    = $this->input->post('scsales');
			$tax	    = $this->input->post('taxsales');
			$netto	    = $this->input->post('nettosales');
			
			for($x0=0;$x0<count($pcode1);$x0++)
			{
				$pcode = strtoupper(addslashes(trim($pcode1[$x0])));
				$nourut = $nourut1[$x0];
				$qty = trim($qty1[$x0]);
				$harga = trim($harga1[$x0]);
				$satuan = $satuan1[$x0];
				$keterangan = $keterangan1[$x0];
				$komisi = $komisi1[$x0];
				$printer = $printer1[$x0];
				if($pcode!="" && $qty>0){
					$data = array(
						'NoUrut'       => $nourut,
						'NoTrans'	   => $notrans,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Harga'        => round($harga,2),
						'Jumlah'       => round($qty*$harga,2),
						'Satuan'       => $satuan,
						'Keterangan'   => $keterangan,
						'komisi'       => $komisi,
						'printer'      => $printer,
						'NoKassa'	   => $kassa,
						'Kasir'		   => $kasir,
						'Tanggal'	   => $tgltrans,
						'Waktu'		   => date('H:i:s'),
						'KdStore'	   => $store,
						'KdPersonal'   => $personal,
						'KdMeja'	   => $idmeja,
						'KdAgent'	   => $idagent,
						'Status'	   => 1
					);
				    $this->db->insert('trans_order_detail', $data);
				}
			}
			
			$data = array(
						'NoKassa'     => $kassa,
						'NoTrans'	  => $notrans,
						'Tanggal'     => $tgltrans,
						'Waktu'       => date('H:i:s'),
						'Kasir'       => $kasir,
						'TotalItem'   => $x0,
						'KdStore'	  => $store,
						'Status'	  => 1,
						'KdPersonal'  => $personal,
						'KdMeja'	  => $idmeja,
						'TotalGuest'  => $idguest,
						'KdAgent'	   => $idagent,
						'Total'       => $total,
						'SC'          => $sc,
						'Tax'         => $tax,
						'Netto'       => $netto,
						'AddDate' => $adddate
					);
			$this->db->insert('trans_order_header', $data);
			
			/*
			$idmeja1 = explode("~",$idmeja);
      		for($x=0;$x<count($idmeja1)-1;$x++)
			{ 
			    if($idmeja1[$x]<>'999')
			       $this->order_touch_model->save_order_table($idmeja1[$x]);
			}

			if($jenisprint=='1')
			{
				$hasil = $this->order_touch_model->getprintjob($kassa);	
				if(count($hasil)>0)
				{
					for($s=count($hasil)-1;$s>=0;$s--)
					{
						switch($hasil[$s]['jenis'])
						{
							case ($hasil[$s]['jenis'] == 1) : print("Ini Makanan!");
							break;
							case ($hasil[$s]['jenis'] == 2) : print("Ini Minuman!");
							break;
							case ($hasil[$s]['jenis'] == 3) : print("Ini Semua!");
							break;
							default : print("None!");
						}
						/*if(($hasil[$s]['jenis'])=='3')
						{
							$data = array(
								'nokassa'       => $hasil[$s]['kassa2print'],
								'lpt'	   => $hasil[$s]['lpt'],
								'jenis'        => $hasil[$s]['jenis'],
								'nokassadokumen'  => $nokassa,
								'nodokumen'       => $notrans,
								'jenisdokumen'       => "SO"
							);			
							$this->db->insert('printjob', $data);
						}
						if(($hasil[$s]['jenis'])=='1')
						{
							print("Ini Makanan!");
							//$data = $this->varCetakMakanan($nokassa,$notrans);
							//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so3', $data);
						}
						if(($hasil[$s]['jenis'])=='2')
						{
							print("Ini Minuman!");
							//$data = $this->varCetakMakanan($nokassa,$notrans);
							//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so3', $data);
						}
					}
				}
			}
			*/
		   
		    $this->add_new();
		}  
		else
	       $this->index();
	}
	function clear_trans()
	{
	    $notrans 		= $this->input->post('notrans');
		$nokassa 		= $this->input->post('kassa1');
		//echo $notrans;
		//echo $nokassa;
		$this->order_touch_model->clear_trans($notrans,$nokassa);
		$this->add_new();
	}
}