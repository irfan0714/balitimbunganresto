<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class order_touch extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/order_touch_model');
    }

    function index(){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("all");
		$ipaddress			= $_SERVER['REMOTE_ADDR'];
		$username = $this->session->userdata('username');
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
			//print_r($data);die();
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        // $config['full_tag_open']  = '<div class="pagination">';
	        // $config['full_tag_close'] = '</div>';
	        // $config['cur_tag_open']   = '<span class="current">';
	        // $config['cur_tag_close']  = '</span>';
	        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/order_touch/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/order_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/order_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("No Kassa","No Transaksi","No Order","Tanggal","Waktu","Total Item","Table","Person", "Aksi");
	        $config['total_rows']		= $this->order_touch_model->num_order_row($id,$with);
	        $data['data']	= $this->order_touch_model->getList($config['per_page'],$page,$id,$with);
			$data['tanggal'] = $this->order_touch_model->getDate();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/order_touch/order_touch_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
		    $ipaddres                   = $this->session->userdata('ip_address');
			$nokassa                    = $this->order_touch_model->getnokassa($ipaddres);
			$datakassa                  = $this->order_touch_model->getkassa($ipaddres);
			$data['NoKassa'] 			= $nokassa;
			$data['msg'] 				= "";
			$data['datakassa'] 			= $datakassa;	
			$user_id = $this->session->userdata('userid');
			$username = $this->session->userdata('username');	
			$data['user_id'] 			= $user_id;
			$data['username'] 			= $username;
			$kddivisi                   = $datakassa[0]['KdKategori'];
			$kdsubdivisi                   = $datakassa[0]['SubDivisi'];

			$kode_divisi                = $datakassa[0]['KdDivisi'];
			
			$data['userid']	 	        = $this->order_touch_model->getUserId();
			$data['barang']	 		    = $this->order_touch_model->getBarang($kode_divisi);
			$data['kategori']	 		= $this->order_touch_model->getKategori();
			$data['subkategori']	 	= $this->order_touch_model->getSubKategori();
			$data['userid']	 	        = $this->order_touch_model->getUserId();
			$data['lokasi']	 	        = $this->order_touch_model->getLokasi($kddivisi);
			$data['keterangan']	 	    = $this->order_touch_model->getKeterangan();
			$data['store']				= $this->order_touch_model->aplikasi();
			$aplikasi = $this->order_touch_model->getDate();
			$data['tanggal'] = $aplikasi->TglTrans2;
			
			//----------------untuk keperluan lcd----
			$ipaddress			= $_SERVER['REMOTE_ADDR']; 
			$data['iplokal']	= $ipaddress;
			//---------------eo untuk keper....------
			$this->load->view('transaksi/order_touch/add_order_touch',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	
	function save_trans()
	{
		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";
		
		// die();

	    $transaksi 		= $this->input->post('transaksi');
		if($transaksi=="yes")
		{
			$EditFlg    = $this->input->post('EditFlg');
			$nourut1    = $this->input->post('nourut');
			$pcode1     = $this->input->post('pcode');
			$qty1       = $this->input->post('qty');
			$keterangan1= $this->input->post('keterangan');
			$kassa   = $this->input->post('kassa');
			$kasir   = $this->input->post('kasir');
			$store	 = $this->input->post('store');
			
			$kategorikassa	 = $this->input->post('kategorikassa');
			$tgltrans = $this->session->userdata('Tanggal_Trans');
			$userid = $this->session->userdata('userid');
			$username = $this->session->userdata('username');
			$tahun = substr($tgltrans,0,4);
			$bulan = substr($tgltrans,5,2);
			
			$idmeja	        = $this->input->post('idtable');
			$idguest        = $this->input->post('idguest');
			$idagent       = $this->input->post('idagent');
			if($idguest=="")$idguest=0;
			$personal	    = $this->input->post('idpersonal');
			$namapersonal = $this->input->post('namapersonal');
			$adddate        = date('y-m-d');
			$Jamsekarang = date("H:i:s");
			
			$data = array(
						'NoKassa'	   => $kassa,
						'NoStruk'	   => '',
						'Tanggal'	   => $adddate,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'TotalItem'    => count($pcode1),
						'Status'	  => "0",
						'KdMeja'	   => $idmeja,
						'KdAgent'	   => $idagent,
						'KdPersonal' => $namapersonal,
						'TotalGuest'   => $idguest
					);
			$this->db->insert('trans_order_header', $data);
			$notrans = $this->db->insert_id();
				
			for($x0=0;$x0<count($pcode1);$x0++)
			{
				$pcode = strtoupper(addslashes(trim($pcode1[$x0])));
				$nourut = $nourut1[$x0];
				$qty = trim($qty1[$x0]);
				$keterangan = $keterangan1[$x0];
				
				if($pcode!="" && $qty>0){
					$data = array(
						'NoKassa'	   => $kassa,
						'NoTrans'	   => $notrans,
						'NoUrut'       => $nourut,
						'Tanggal'	   => $adddate,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Status'       => "0",
						'Keterangan'	=> $keterangan,
						'KdMeja' 		=> $idmeja
					);
				    $this->db->insert('trans_order_detail', $data);
				}
			}
	   		
			$this->cetakall($notrans);
		} 
		else
	       $this->index();
	    
	}
	
	function cetakmeja($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		//print_r($data);
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		redirect('transaksi/order_touch','refresh');
	}
	
	function cetakminum($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		redirect('transaksi/order_touch','refresh');
	}
	
	function cetakmakan($notrans){
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		redirect('transaksi/order_touch','refresh');
	}
	
	function cetakall($notrans){
		// Meja
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		
		//Minuman
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		
		//Makanan
		$store = $this->order_touch_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->order_touch_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->order_touch_model->all_trans($notrans);
		$data['detail'] = $this->order_touch_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		$this->load->view('transaksi/order_touch/cetak_transaksi_wireness', $data);
		
		redirect('transaksi/order_touch/add_new/','refresh');
	}
	
	
	function clear_trans()
	{
	    $notrans 		= $this->input->post('notrans');
		$nokassa 		= $this->input->post('kassa1');
		//echo $notrans;
		//echo $nokassa;
		$this->order_touch_model->clear_trans($notrans,$nokassa);
		$this->add_new();
	}

	function cektable(){
		$kdmeja = $this->input->post('data');
		$tgl = date('y-m-d');
		$OpenOrder = $this->order_touch_model->getOpenOrder($tgl, $kdmeja);
		echo json_encode($OpenOrder);
	}

	function orderWeb() {
		try {
			$this->load->helper('apin_helper');
			$API_APIN =  apiApin();
			$url = $API_APIN."/orderPending";
		    $ch = curl_init(); 
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		    $output = curl_exec($ch); 
		    curl_close($ch);      
		    $data = json_decode($output, true);
	    	for ($x=0; $x < count($data); $x++) { 
	    		if(count($data[$x]['detail'])){
	    			$dataHeader = array();
			    	$dataDetail = array();

			    	//mapping by sequence
		    		$tempUrut = 0;
			    	$tempSequence = "-1";
			    	for($i = 0;$i < count($data[$x]['detail']); $i++) {
			    		if($tempSequence != $data[$x]['detail'][$i]['sequence']) {
			    			$dataHeader[] = array(
								'NoTransApps' => $data[$x]['invoice_number'],
								'NoKassa' => '1',
								'Tanggal' => date('Y-m-d',strtotime($data[$x]['created_at'])),
								'Waktu' => date('H:i:s',strtotime($data[$x]['created_at'])),
								'Status' => '0',
								'KdPersonal' => '0',
								'KdAgent' => '11111',
								'TotalGuest' => '1',
								'KdMeja' => $data[$x]['table_code'],
								'AddDate' => $data[$x]['created_at'],
								'NamaPelanggan' => $data[$x]['customer_name'],
								'Sequence' => $data[$x]['detail'][$i]['sequence'],
			    			);
			    		}

			    		for($loopSequence = 0; $loopSequence < 50; $loopSequence++ ) {
			    			if($loopSequence == $data[$x]['detail'][$i]['sequence']) {
			    				$tempUrut++;
			    				$dataDetail[$data[$x]['detail'][$i]['sequence']][] = array(
			    					'NoUrut' => $tempUrut,
			    					'NoKassa' => '1',
			    					'Tanggal' => date('Y-m-d',strtotime($data[$x]['created_at'])),
			    					'Waktu' => date('H:i:s',strtotime($data[$x]['created_at'])),
			    					'Kasir' => '',
			    					'KdStore' => '00',
									'Status' => '0',
									'KdMeja' => $data[$x]['table_code'],
			    					'PCode' => $data[$x]['detail'][$i]['code'],
			    					'Qty' => $data[$x]['detail'][$i]['qty'],
			    					'Keterangan' => $data[$x]['detail'][$i]['note'],
			    					'Qty' => $data[$x]['detail'][$i]['qty'],
			    					'Sequence' => $data[$x]['detail'][$i]['sequence']
			    				);
			    			}
			    		}
			    		$tempSequence = $data[$x]['detail'][$i]['sequence'];
			    	}
			    	//insert
			    	foreach ($dataHeader as $header) {
			    		$whereHeader = array(
			    				'NoTransApps' => $header['NoTransApps'],
			    				'Sequence' => $header['Sequence'],
		    			);
		    			$checkDataHeader = $this->db->get_where('trans_order_header', $whereHeader)->result_array();
		    			if(count($checkDataHeader) <= 0) {
		    				$header['TotalItem'] = count($dataDetail[$header['Sequence']]);
				    		$this->db->insert('trans_order_header', $header);
				    		$noTrans = $this->db->insert_id();
				    		$insertId = $this->db->insert_id();
				    		foreach ($dataDetail[$header['Sequence']] as $detail) {
				    			$detail['NoTrans'] = $insertId;
				    			$this->db->insert('trans_order_detail', $detail);
				    		}
				    		$this->cetakall($noTrans);
		    			}
			    	}
			    }
	    	}
		    $message = '<div class="alert alert-success"><strong>Sukses !</strong>.</div>';
		    $this->session->set_flashdata('alertOrderWeb', $message);

		} catch (Exception $e) {
			$message = '<div class="alert alert-danger"><strong>Gagal !, </strong> '.$e.'.</div>';
			$this->session->set_flashdata('alertOrderWeb', $message);
		}
		redirect('transaksi/order_touch/index','refresh');

	}

	function orderWebDone() {
		$this->load->helper('apin_helper');
		$API_APIN =  apiApin();
		try {
			$url = $API_APIN."/orderdone";
	        $body_content = array(array('invoice_number' => 'ORD-2022-03-L01-00003'));

	        $curl = curl_init();
	        curl_setopt_array($curl, array(
	            CURLOPT_URL => $url,
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_ENCODING => "",
	            CURLOPT_MAXREDIRS => 10,
	            CURLOPT_TIMEOUT => 0,
	            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	            CURLOPT_CUSTOMREQUEST => "POST",
	            CURLOPT_POSTFIELDS => json_encode($body_content),
	            CURLOPT_HTTPHEADER => array(
	                "Content-type: application/json",
	            ),
	        ));

	        $response = curl_exec($curl);
	        $err = curl_error($curl);
	        $this->session->set_flashdata('alertOrderWeb', 'Sukses.');
		} catch (Exception $e) {
	        $this->session->set_flashdata('alertOrderWeb', 'Error. '.$e);
			
		}
		redirect('transaksi/order_touch','refresh');
	}

	function orderWebDoneAll() {
		$this->load->helper('apin_helper');
		$url = $API_APIN."/orderdoneall";
        $body_content = array();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body_content),
            CURLOPT_HTTPHEADER => array(
                "Content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        echo "<pre>";
        print_r($response);
        echo "</pre>";
	}

}