<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->api = "http://app.bebektimbungan.com/api/";
	}

	public function index()
	{
		echo "working";
	}

	public function masterbarang() {
		$this->db->select('Pcode, NamaLengkap, NamaInitial, Harga1c, Status, SubKategoriPOSTouch AS SubKategori', TRUE);
		$this->db->from('masterbarang');
		$this->db->where('KdDivisi','1');
		$this->db->where('KdKategori','3');
		$masterbarang = $this->db->get()->result_array();
		$kategori = $this->db->get_where('kategoripos')->result_array();
		$subKategori = $this->db->get_where('subkategoripos')->result_array();

		$data = array(
			'masterbarang' => $masterbarang,
			'kategori' => $kategori,
			'subKategori' => $subKategori,

		);
		echo "<pre>";
	    print_r($data); 
		echo "</pre>";
		$url = $this->api."productnew";
		$ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch); 
	    curl_close($ch);   
	    echo "sukses";  
	    return $output;
	}

	public function orderWebPending() {
		try {
			$this->load->helper('apin_helper');
			$API_APIN =  apiApin();
			$url = $API_APIN."/orderPending";
		    $ch = curl_init(); 
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		    $output = curl_exec($ch); 
		    curl_close($ch);      
		    $data = json_decode($output, true);
	    	for ($x=0; $x < count($data); $x++) { 
	    		if(count($data[$x]['detail'])){
	    			$dataHeader = array();
			    	$dataDetail = array();

			    	//mapping by sequence
		    		$tempUrut = 0;
			    	$tempSequence = "-1";
			    	for($i = 0;$i < count($data[$x]['detail']); $i++) {
			    		if($tempSequence != $data[$x]['detail'][$i]['sequence']) {
			    			$dataHeader[] = array(
								'NoTransApps' => $data[$x]['invoice_number'],
								'NoKassa' => '1',
								'Tanggal' => date('Y-m-d',strtotime($data[$x]['created_at'])),
								'Waktu' => date('H:i:s',strtotime($data[$x]['created_at'])),
								'Status' => '0',
								'KdPersonal' => '0',
								'KdAgent' => '11111',
								'TotalGuest' => '1',
								'KdMeja' => $data[$x]['table_code'],
								'AddDate' => $data[$x]['created_at'],
								'NamaPelanggan' => $data[$x]['customer_name'],
								'Sequence' => $data[$x]['detail'][$i]['sequence'],
			    			);
			    		}

			    		for($loopSequence = 0; $loopSequence < 50; $loopSequence++ ) {
			    			if($loopSequence == $data[$x]['detail'][$i]['sequence']) {
			    				$tempUrut++;
			    				$dataDetail[$data[$x]['detail'][$i]['sequence']][] = array(
			    					'NoUrut' => $tempUrut,
			    					'NoKassa' => '1',
			    					'Tanggal' => date('Y-m-d',strtotime($data[$x]['created_at'])),
			    					'Waktu' => date('H:i:s',strtotime($data[$x]['created_at'])),
			    					'Kasir' => '',
			    					'KdStore' => '00',
									'Status' => '0',
									'KdMeja' => $data[$x]['table_code'],
			    					'PCode' => $data[$x]['detail'][$i]['code'],
			    					'Qty' => $data[$x]['detail'][$i]['qty'],
			    					'Keterangan' => $data[$x]['detail'][$i]['note'],
			    					'Qty' => $data[$x]['detail'][$i]['qty'],
			    					'Sequence' => $data[$x]['detail'][$i]['sequence']
			    				);
			    			}
			    		}
			    		$tempSequence = $data[$x]['detail'][$i]['sequence'];
			    	}
			    	//insert
			    	foreach ($dataHeader as $header) {
			    		$whereHeader = array(
			    				'NoTransApps' => $header['NoTransApps'],
			    				'Sequence' => $header['Sequence'],
		    			);
		    			$checkDataHeader = $this->db->get_where('trans_order_header', $whereHeader)->result_array();
		    			if(count($checkDataHeader) <= 0) {
		    				$header['TotalItem'] = count($dataDetail[$header['Sequence']]);
				    		$this->db->insert('trans_order_header', $header);
				    		$insertId = $this->db->insert_id();
				    		foreach ($dataDetail[$header['Sequence']] as $detail) {
				    			$detail['NoTrans'] = $insertId;
				    			$this->db->insert('trans_order_detail', $detail);
				    		}
		    			}
			    	}
			    }
	    	}
		    $message = '<div class="alert alert-success"><strong>Sukses !</strong>.</div>';
		    $this->session->set_flashdata('alertOrderWeb', $message);

		} catch (Exception $e) {
			$message = '<div class="alert alert-danger"><strong>Gagal !, </strong> '.$e.'.</div>';
			$this->session->set_flashdata('alertOrderWeb', $message);
		}
		redirect('transaksi/order_touch','refresh');
	}

	public function orderWebDone() {
		$data = json_encode(array(array("invoice_number" => 'ORD-2022-03-L22-00019')));
		$url = $this->api."orderdone";
		$ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch); 
	    curl_close($ch);     
	    print_r($data); 
	    // return $output;
	}

	public function orderWebDoneAll() {
		$data = array();
		$url = $this->api."orderdoneall";
		$ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch); 
	    curl_close($ch);     
	    print_r($output); 
	    // return $output;
	}

}

/* End of file order_test.php */
/* Location: ./application/controllers/transaksi/order_test.php */