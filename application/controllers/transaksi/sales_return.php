<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Sales_return extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/sales_return_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "delivery_order", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/sales_return/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/sales_return/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->sales_return_model->getGudang();
			$data['mcustomer'] = $this->sales_return_model->getCustomer();

            $config['total_rows'] = $this->sales_return_model->num_delivery_order_row($arr_search["search"]);
            $data['data'] = $this->sales_return_model->getDeliveryOrderList($config['per_page'], $page, $arr_search["search"]);
			//$data['data'] = $this->sales_return_model->getDeliveryOrderList($config['per_page'], 0, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/sales_return/sales_return_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'sales_order', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "delivery_order",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/sales_return/index/' . $query_id . '');
    }

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			$data['gudang'] = $this->sales_return_model->getGudang();
            $data['customer'] = $this->sales_return_model->getCustomer();
            $data['subdivisi'] = $this->sales_return_model->getSubdivisi();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/sales_return/add_sales_return', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_sales_return($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->sales_return_model->getHeader($id);			
            $data['customer'] = $this->sales_return_model->getCustomer();
            $data['subdivisi'] = $this->sales_return_model->getSubdivisi();            
			$data['gudang'] = $this->sales_return_model->getGudang();
            //echo "<pre>";print_r($data['subdivisi']);echo "</pre>";die;
            $data["detail_list"] = $this->sales_return_model->getDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/sales_return/edit_sales_return', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_sales_return($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->sales_return_model->getHeader($id);
            $data['customer'] = $this->sales_return_model->getCustomer();
            $data["detail_list"] = $this->sales_return_model->getDetailList($id);
            $data['msatuan'] = $this->sales_return_model->getSatuan();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/sales_return/view_sales_return', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $deliveryorderno = $this->input->post('deliveryorderno');
        $v_subdivisi = $this->input->post('v_subdivisi');
        $v_warehouse = $this->input->post('v_gudang');
        $v_customer = $this->input->post('v_customer');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
		$pisah_periode					= explode("-",$v_tgl_dokumen);
		$hri_periode					= $pisah_periode[0];
		$bln_periode					= $pisah_periode[1];
		$thn_periode					= $pisah_periode[2];
		
			  //cari stock
			  if($bln_periode=='01'){
			$tabel_field='GMasuk01';
		}else if($bln_periode=='02'){
			$tabel_field='GMasuk02';
		}else if($bln_periode=='03'){
			$tabel_field='GMasuk03';
		}else if($bln_periode=='04'){
			$tabel_field='GMasuk04';
		}else if($bln_periode=='05'){
			$tabel_field='GMasuk05';
		}else if($bln_periode=='06'){
			$tabel_field='GMasuk06';
		}else if($bln_periode=='07'){
			$tabel_field='GMasuk07';
		}else if($bln_periode=='08'){
			$tabel_field='GMasuk08';
		}else if($bln_periode=='09'){
			$tabel_field='GMasuk09';
		}else if($bln_periode=='10'){
			$tabel_field='GMasuk10';
		}else if($bln_periode=='11'){
			$tabel_field='GMasuk11';
		}else if($bln_periode=='12'){
			$tabel_field='GMasuk12';
		}
		
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
        
        // detail
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_qty1 = $this->input->post('v_qty');
        $v_satuan1 = $this->input->post('v_satuan');
        $v_note_detail1 = $this->input->post('v_note_detail');
      

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Sales Return
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "salesreturn","returnno", "SR", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di Sales Return Header
            $this->insertNewHeader($v_no_dokumen, $deliveryorderno, $mylib->ubah_tanggal($v_tgl_dokumen), $v_warehouse, $v_note, $user, $v_customer);
                        
			for ($x = 0; $x < count($v_pcode1); $x++) 
			{
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];
            $v_note_detail = $v_note_detail1[$x];	
			
			//cocokan konversi terlebih dahulu
				$konversi = $this->sales_return_model->getKonversi($pcode,$v_satuan);
				
				//jika konversi ada datanya
				if(!empty($konversi)){
					//jika Satuan_To sama dengan Satuan yang dipilih
					if($konversi->Satuan_To==$v_satuan){
						$QtyPcs = $v_qty;
					//jika tidak sama
					}else{
						$QtyPcs = $v_qty * $konversi->amount;
					}
				//jika tidak ada datanya konversi
				}else{
					$QtyPcs = $v_qty;
				}
			
			//ketiga masukkan di Sales return detail
	        $this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$v_note_detail,$user,$v_satuan,$QtyPcs);

			//keempat Masukkan di Mutasi
	        $this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
			
			//kelima masukkan di stock GMasuk[bulan]
			//a. cek dulu apakah di stok udah ada? kalau belum ada insert kalau udah ada update
	        $stock = $this->sales_return_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
			//print_r($stock);die;
			        if($stock->Tahun==$thn_periode and $stock->KdGudang==$v_warehouse and  $stock->PCode==$pcode )
					{
						$this->updateStock($thn_periode,$v_warehouse,$pcode,$stock->$tabel_field,$v_qty,$tabel_field);
					}
					else
					{
						
						$this->insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field);
					}
			
			}  
			
			//------------------------------------ insert juga ke credit note -----------------------------------------
			//simpen juga ke creditnote header
			//generate cnno
		    $v_cnno = $mylib->get_code_counter($this->db->database, "creditnote","cnno", "CS", $data['bulan'], $data['tahun']);
		    
			for ($x = 0; $x < count($v_pcode1); $x++) 
			{
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];
            $v_note_detail = $v_note_detail1[$x];	
			
			//cek harga barang di masterbarang dan divisi untuk mendapatkan KdRekeningRetur
			$stock = $this->sales_return_model->cekDataBarang($pcode,$v_customer);
			foreach($stock as $val)
					{
						$value = $val['Harga1c'];
						$coano = $val['KdRekeningRetur'];
						
						//menghitung $v_qty dengan harga di masterbarang
						$total_harga = $v_qty * $value;
						$this->insertCreditNoteDetail($v_cnno,$coano,$v_subdivisi,$total_harga,$pcode,$v_note_detail,$user);
					} 		       
			}
			
			//cek total value yang sudah diinsert di detail diatas
			$cek_total_amount = $this->sales_return_model->cekDataCreditNoteDetail($v_cnno);
			$grandtotal = $cek_total_amount[0]['grandtotal'];
			
			$this->insertCreditNote($v_cnno, $mylib->ubah_tanggal($v_tgl_dokumen),$v_no_dokumen, $v_customer, $v_note, $user, $grandtotal);
			
			//------------------------------------ insert juga ke piutang ----------------------------------------- 
			//insert juga ke tabel piutang
		    $this->insertNewPiutang($v_cnno, $mylib->ubah_tanggal($v_tgl_dokumen), $v_customer, $grandtotal);
		    
		    //insert juga ke tabel mutasi_piutang
		    $this->insertNewMutasiPiutang($data['tahun'],$data['bulan'],$v_cnno, $mylib->ubah_tanggal($v_tgl_dokumen), $v_customer, $grandtotal);
										
		} 
		
		
		else if ($flag == "edit") 
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			
			//update deliveyorder
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_warehouse, $v_note, $v_status, $user, $v_customer);
            
				        // ubah detail
				        for ($x = 0; $x < count($v_pcode1); $x++) 
				        {
				        	$v_no_dokumen = $this->input->post('v_no_dokumen');
				            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
							$v_namabarang = trim($v_namabarang1[$x]);
							$v_qty = $mylib->save_int($v_qty1[$x]);
							$v_satuan = $v_satuan1[$x];
							$v_note_detail = $v_note_detail1[$x];

				            if ($pcode != "") 
				            {
				            	if($v_qty*1>0)
				            	{   
				            	    
									
									
									//cek mutasi
									$mutasi = $this->sales_return_model->cekGetMutasi($v_no_dokumen,$v_warehouse,$mylib->ubah_tanggal($v_tgl_dokumen),$pcode);
							        
							        if($mutasi->NoTransaksi==$v_no_dokumen and $mutasi->KdTransaksi=='FG' and $mutasi->Gudang==$v_warehouse and  $mutasi->Tanggal==$mylib->ubah_tanggal($v_tgl_dokumen) and $mutasi->KodeBarang==$pcode )
									{
										$this->updateMutasi($v_no_dokumen,$v_warehouse,$mylib->ubah_tanggal($v_tgl_dokumen),$pcode,$v_qty);
									}
									else
									{
										for ($x = 0; $x < count($v_pcode1); $x++) 
										{
							            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
							            $v_namabarang = trim($v_namabarang1[$x]);
							            $v_qty = $mylib->save_int($v_qty1[$x]);
							            $v_satuan = $v_satuan1[$x];
										$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
										}
									}
									
									//ambil qty dari deliveryorderdetail
									$detail2 = $this->sales_return_model->cekGetDetail($pcode,$v_no_dokumen);
									
									//ambil GKeluar di stock
									$stock2 = $this->sales_return_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
																
									$this->updateStock2($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$v_qty,$tabel_field);
									
								}
				            }
				        }
				        
				        
			//update di deliveyorder dan deliveryorderdetail
			for ($x = 0; $x < count($v_pcode1); $x++) 
	        {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];
            $v_note_detail = $v_note_detail1[$x];

            if ($pcode != "") 
            {
            	if($v_qty*1>0)
            	{
            		//cek apakah sudah ada di deliveryorder
            		$detail = $this->sales_return_model->cekGetDetail($pcode,$v_no_dokumen);
					
					//cocokan konversi terlebih dahulu
						$konversi = $this->sales_return_model->getKonversi($pcode,$v_satuan);
						
						//jika konversi ada datanya
						if(!empty($konversi)){
							//jika Satuan_To sama dengan Satuan yang dipilih
							if($konversi->Satuan_To==$v_satuan){
								$QtyPcs = $v_qty;
							//jika tidak sama
							}else{
								$QtyPcs = $v_qty * $konversi->amount;
							}
						//jika tidak ada datanya konversi
						}else{
							$QtyPcs = $v_qty;
						}
            		
            		if($detail->inventorycode==$pcode)
					{
						//update deliveryorderdetail
						$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_qty,$detail->Qty, $user,$v_satuan,$QtyPcs);
					}
					else
					{
						//insert jika itu record baru
						$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$v_note_detail,$user,$v_satuan,$QtyPcs);
					}				
				}
            }
			
			//keempat Masukkan di Mutasi
			$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
			
			//kelima masukkan di stock GMasuk[bulan]
			//a. cek dulu apakah di stok udah ada? kalau belum ada insert kalau udah ada update
			$stock = $this->sales_return_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
			
			        if($stock->Tahun==$thn_periode and $stock->KdGudang==$v_warehouse and  $stock->PCode==$pcode )
					{
						$this->updateStock($thn_periode,$v_warehouse,$pcode,$stock->$tabel_field,$v_qty,$tabel_field);
					}
					else
					{
						
						$this->insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field);
					}
            		
        }
		$returnno = $this->input->post('v_no_dokumen');
		//ambil cnno di creditnote
		$ambilcnno = $this->sales_return_model->cekGetCnno($returnno);
		
		//update ke creditnote
		for ($x = 0; $x < count($v_pcode1); $x++) 
	        {
				
				$pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
				$v_namabarang = trim($v_namabarang1[$x]);
				$v_qty = $mylib->save_int($v_qty1[$x]);
				$v_satuan = $v_satuan1[$x];
				$v_note_detail = $v_note_detail1[$x];
							
				//samakan dengan creditnotrdetail
				$samakan = $this->sales_return_model->cekGetCnnoDetail($ambilcnno->cnno,$pcode);
				
			        if(!empty($samakan))
					{
					
						$stock_ = $this->sales_return_model->cekDataBarang($pcode, $v_customer);
					foreach($stock_ as $val)
					{
						$value = $val['Harga1c'];
						$coano = $val['KdRekeningRetur'];
						
						//menghitung $v_qty dengan harga di masterbarang
						$total_harga = $v_qty * $value;
						$this->updateCrediteNoteDetail($ambilcnno->cnno,$coano,$v_subdivisi,$total_harga,$pcode,$v_note_detail,$user);
					}
					}
					else
					{
						
						//cek harga barang di masterbarang dan divisi untuk mendapatkan KdRekeningRetur
					$stock_ = $this->sales_return_model->cekDataBarang($pcode,$v_customer);
					foreach($stock_ as $val)
					{
						$value = $val['Harga1c'];
						$coano = $val['KdRekeningRetur'];
						
						//menghitung $v_qty dengan harga di masterbarang
						$total_harga = $v_qty * $value;
						$this->insertCreditNoteDetail($ambilcnno->cnno,$coano,$v_subdivisi,$total_harga,$pcode,$v_note_detail,$user);
					}
					}
				
			
			}
			
			//cek total value yang sudah diinsert di detail diatas
			$cek_total_amount2 = $this->sales_return_model->cekDataCreditNoteDetail($ambilcnno->cnno);
			$grandtotal2 = $cek_total_amount2[0]['grandtotal'];
			
			$this->updateCreditNote($ambilcnno->cnno, $mylib->ubah_tanggal($v_tgl_dokumen),$v_no_dokumen, $v_customer, $v_note, $user, $grandtotal2);
			
			//------------------------------------ update juga ke piutang ----------------------------------------- 
			//insert juga ke tabel piutang
		    $this->updateNewPiutang($ambilcnno->cnno, $mylib->ubah_tanggal($v_tgl_dokumen), $v_customer, $grandtotal2);
		    
		    //insert juga ke tabel mutasi piutang
		    $this->updateNewMutasiPiutang($ambilcnno->cnno, $mylib->ubah_tanggal($v_tgl_dokumen), $v_customer, $grandtotal2);			
										
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/sales_return/edit_sales_return/' . $v_no_dokumen . '');
    }

    function insertNewHeader($no_dokumen, $deliveryorderno, $tgl_dokumen, $v_warehouse, $v_note, $user, $v_customer) 
    {
    	//echo $no_dokumen." - ".$deliveryorderno." - ".$tgl_dokumen." - ".$v_warehouse." - ".$v_note." - ".$user." - ".$v_customer;die; 
        $this->sales_return_model->locktables('salesreturn');

        $data = array
			        ('returnno' => $no_dokumen,
			        'dono' => $deliveryorderno,
			        'returndate' => $tgl_dokumen,
			        'customerid' => $v_customer,
			        'warehousecode' => $v_warehouse,
			        'status' => 0,
			        'note' => $v_note,
			        'adddate' =>date('Y-m-d'),
			        'adduser' => $user);

        $this->db->insert('salesreturn', $data);

        $this->sales_return_model->unlocktables();

    }
    
    function insertCreditNote($v_cnno, $cndate,$returnno, $v_customer, $v_note, $user, $cnamount) 
    {
        $this->sales_return_model->locktables('creditnote');

        $data = array(
				  'cnno' => $v_cnno,
				  'cndate' => $cndate,
				  'cntype' => 'CS',
				  'returnno'=>$returnno,
				  'KdCustomer' => $v_customer,
				  'note' => $v_note,
				  'status' => '1',
				  'currencycode' => 'IDR-1',
				  'vatpercent' => '10',
				  'cnamount' => $cnamount,
				  'cnamountremain' => $cnamount,
				  'KdRekening' => '',
				  'adddate' => date('Y-m-d'),
				  'adduser' => $user
				);

        $this->db->insert('creditnote', $data);

        $this->sales_return_model->unlocktables();

    }
	
	function updateCreditNote($v_cnno, $cndate,$returnno, $v_customer, $v_note, $user, $cnamount) 
    {
        $this->sales_return_model->locktables('creditnote');

        $data = array(
				  'cndate' => $cndate,
				  'KdCustomer' => $v_customer,
				  'note' => $v_note,
				  'status' => '1',
				  'currencycode' => 'IDR-1',
				  'vatpercent' => '10',
				  'cnamount' => $cnamount,
				  'cnamountremain' => $cnamount,
				  'editdate' => date('Y-m-d'),
				  'edituser' => $user
				);
				
		$where = array(
				  'cnno' => $v_cnno,
				  'returnno'=>$returnno
				);

        $this->db->update('creditnote', $data, $where);

        $this->sales_return_model->unlocktables();

    }
	
	function updateCreditNote2($v_cnno, $cndate,$returnno, $user, $cnamount) 
    {
        $this->sales_return_model->locktables('creditnote');

        $data = array(
				  'cndate' => $cndate,
				  'status' => '1',
				  'currencycode' => 'IDR-1',
				  'vatpercent' => '10',
				  'cnamount' => $cnamount,
				  'cnamountremain' => $cnamount,
				  'editdate' => date('Y-m-d'),
				  'edituser' => $user
				);
				
		$where = array(
				  'cnno' => $v_cnno,
				  'returnno'=>$returnno
				);

        $this->db->update('creditnote', $data, $where);

        $this->sales_return_model->unlocktables();

    }
    
    function insertCreditNoteDetail($cnno,$coano,$v_subdivisi,$value,$pcode,$description,$user) 
    {
    	$this->sales_return_model->locktables('creditnotedtl');
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
			$desc="from pcode ".$pcode." : ".$description;
			$data=array(
						'cnno'=>$cnno,
						'KdSubdivisi'=>$v_subdivisi,
				        'coano'=>$coano,
				        'value'=>$value,
				        'description'=>$desc,
				        'adddate'=>date('Y-m-d'),
				        'adduser'=>$user
				        );
			
			$this->db->insert('creditnotedtl',$data);
		$this->sales_return_model->unlocktables();
    }
	
	function updateCrediteNoteDetail($cnno,$coano,$value,$v_subdivisi,$pcode,$description,$user) 
    {
    	$this->sales_return_model->locktables('creditnotedtl');
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
			$desc="from pcode ".$pcode." : ".$description;
			
			$data=array(
						'KdSubdivisi'=>$v_subdivisi,
				        'value'=>$value,
				        'description'=>$desc,
				        'editdate'=>date('Y-m-d'),
				        'edituser'=>$user
				        );
			
			$where=array(
						'cnno'=>$cnno,
				        'coano'=>$coano
				        );
			
			$this->db->update('creditnotedtl',$data,$where);
		$this->sales_return_model->unlocktables();
    }
    
    function insertNewPiutang($cnno, $tgl, $v_customer, $grandtotal) 
    {
        $this->sales_return_model->locktables('piutang');

        $data = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno,
				  'TglTransaksi' => $tgl,
				  'KdCustomer' => $v_customer,
				  'TipeTransaksi' => 'CS',
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);

        $this->db->insert('piutang', $data);

        $this->sales_return_model->unlocktables();

    }
    
    function insertNewMutasiPiutang($tahun, $bulan, $v_invoiceno, $sidate, $v_customer , $grandtotal ) 
    {
    	//echo $sidate;die;
        $this->sales_return_model->locktables('mutasi_piutang');

        $data = array(
        		  'Tahun'=>$tahun,
        		  'Bulan'=>$bulan,
        		  'Tanggal' => $sidate,
				  'NoDokumen' => $v_invoiceno,
				  'KdCustomer' => $v_customer,
				  'MataUang'=>'IDR-1',
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'CS'
				);

        $this->db->insert('mutasi_piutang', $data);

        $this->sales_return_model->unlocktables();

    }
	
	function updateNewPiutang($cnno, $tgl, $v_customer, $grandtotal) 
    {
        $this->sales_return_model->locktables('piutang');

        $data = array(
				  'TglTransaksi' => $tgl,
				  'KdCustomer' => $v_customer,
				  'TipeTransaksi' => 'CS',
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno
				);

        $this->db->update('piutang', $data, $where);

        $this->sales_return_model->unlocktables();

    }
    
    function updateNewMutasiPiutang($cnno, $sidate,  $v_customer, $grandtotal) 
    {
    	//echo $sidate;die;
        $this->sales_return_model->locktables('mutasi_piutang');

        $data = array(
        		  'Tanggal' => $sidate,
				  'KdCustomer' => $v_customer,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'CS'
				);
				
		$where=array(
		          'NoDokumen' => $cnno
					);

        $this->db->update('mutasi_piutang', $data, $where);
        
        $this->sales_return_model->unlocktables();
    }
	
	function updateNewPiutang2($cnno, $tgl, $grandtotal) 
    {
        $this->sales_return_model->locktables('piutang');

        $data = array(
				  'TglTransaksi' => $tgl,
				  'TipeTransaksi' => 'CS',
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno
				);

        $this->db->update('piutang', $data, $where);

        $this->sales_return_model->unlocktables();

    }
    
    function updateNewMutasiPiutang2($cnno, $tgl, $grandtotal) 
    {
        $this->sales_return_model->locktables('mutasi_piutang');

        $data = array(
        		  'Tanggal' => $tgl,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'CS'
				);
				
		$where = array(
				  'NoDokumen' => $cnno
				);

        $this->db->update('mutasi_piutang', $data, $where);

        $this->sales_return_model->unlocktables();

    }
  
    function updateHeader($no_dokumen, $tgl_dokumen, $v_warehouse, $v_note, $v_status,$user, $v_customer) 
    {
        $this->sales_return_model->locktables('salesreturn');

        $data = array(  'returndate' => $tgl_dokumen,
				        'customerid' => $v_customer,
				        'warehousecode' => $v_warehouse,
				        'status' => $v_status,
				        'note' => $v_note,
				        'editdate' => date('Y-m-d'),
				        'edituser'=> $user
				        );

        $this->db->update('salesreturn', $data, array('returnno' => $no_dokumen));
        
        $this->sales_return_model->unlocktables();
    }

    function insertDetail($flag, $no_dokumen, $pcode, $qty,$v_note_detail, $user,$v_satuan,$QtyPcs) 
    {
        $this->sales_return_model->locktables('salesreturndetail');

        if ($pcode) {
            $data = array(
                'returnno' => $no_dokumen,
                'inventorycode' => $pcode,
                'quantity' => $qty,
				'satuan'=>$v_satuan,
				'QtyPcs'=>$QtyPcs,
                'note' => $v_note_detail,
                'adddate'=>date('Y-m-d'),
                'adduser'=>$user
            );
            
            //echo "<pre>";
            //print_r($data);
            //echo "</pre>";die;

            $this->db->insert('salesreturndetail', $data);
        }

        $this->sales_return_model->unlocktables();
    }
	
	function insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field) 
    {
        $this->sales_return_model->locktables('stock');
        if ($pcode) {
            $data = array(
                'Tahun' => $thn_periode,
                'KdGudang' => $v_warehouse,
                'PCode' => $pcode,
                $tabel_field=>$v_qty
            );

            $this->db->insert('stock', $data);
        }

        $this->sales_return_model->unlocktables();
    }
   	
	function insertMutasi($v_no_dokumen, $v_warehouse, $v_tgl_dokumen, $pcode, $v_qty, $user, $v_note)
    {
        $this->sales_return_model->locktables('mutasi');

        if ($pcode) {
            $data = array(
                'NoTransaksi' => $v_no_dokumen,
                'Jenis' => 'I',
				'KdTransaksi' => 'SR',
				'Gudang' => $v_warehouse,
				'Tanggal'=>$v_tgl_dokumen,
				'KodeBarang'=>$pcode,
				'Qty'=>$v_qty,
				'Status'=>1,
                'Kasir'=>$user,
				'Keterangan'=> $v_note
            );

            $this->db->insert('mutasi', $data);
        }

        $this->sales_return_model->unlocktables();
    }
	
    function updateDetail($flag,$no_dokumen,$pcode,$qty,$qty_tbl,$user,$v_satuan,$QtyPcs)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);
    	 
        $this->sales_return_model->locktables('deliveryorderdetail');

        if ($pcode) 
        {
            $data = array(
                'quantity' => $new_qty,
				'satuan'=>$v_satuan,
				'QtyPcs'=>$QtyPcs,
                'editdate'=>date('Y-m-d'),
                'edituser'=>$user
            );
            
            $this->db->update('deliveryorderdetail', $data, array('dono' => $no_dokumen,'inventorycode' => $pcode));
        } 
        
        $this->sales_return_model->unlocktables();
    }
	
	function updateStock($thn_periode,$v_warehouse,$pcode,$jml_stock,$v_qty,$tabel_field)
    {
    	$jml_tambah= $jml_stock + $v_qty;
        $this->sales_return_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_tambah
            );
            
            $this->db->update('stock', $data, array('Tahun' => $thn_periode,'KdGudang' => $v_warehouse,'PCode' => $pcode));
        } 
        
        $this->sales_return_model->unlocktables();
    }
    
    function updateStock2($thn_periode,$v_warehouse,$market_pcode,$detail_quantity,$stock_tabel_field,$market_qty,$tabel_field)
    {
    	$jml_update=($stock_tabel_field-$detail_quantity)+$market_qty;
        $this->sales_return_model->locktables('stock');

        if ($market_pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $market_pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->sales_return_model->unlocktables();
    }
    
    function updateMutasi($v_no_dokumen,$v_warehouse,$v_tgl_dokumen,$market_pcode,$market_qty)
    {
    	$this->sales_return_model->locktables('mutasi');

        if ($market_pcode) 
        {
            $data = array(
                'Qty'=>$market_qty
            );
            
            $where= array(
            'NoTransaksi'=>$v_no_dokumen,
            'KdTransaksi'=>'SR',
            'Gudang'=>$v_warehouse,
            'Tanggal'=>$v_tgl_dokumen,
            'KodeBarang'=>$market_pcode            
            );
            
            $this->db->update('mutasi', $data, $where);
        } 
        
        $this->sales_return_model->unlocktables();
    }
	
	function delete_trans($id) 
    {
	        
			//stock
			//cek
			$detail2 = $this->sales_return_model->cekGetDetail3($id);
			
			foreach($detail2 as $val){
			$returndetailid = $val['returndetailid'];
			$returnno=  $val['returnno'];
			$quantity = $val['quantity'];
			$pcode=  $val['inventorycode'];
			$warehousecode = $val['warehousecode'];
			$adddate = $val['adddate'];
			
			$detail2 = $this->sales_return_model->cekGetDetail2($pcode,$returnno);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  //hello
				  //cari stock
			  if($bln_periode=='01'){
			$tabel_field='GMasuk01';
		}else if($bln_periode=='02'){
			$tabel_field='GMasuk02';
		}else if($bln_periode=='03'){
			$tabel_field='GMasuk03';
		}else if($bln_periode=='04'){
			$tabel_field='GMasuk04';
		}else if($bln_periode=='05'){
			$tabel_field='GMasuk05';
		}else if($bln_periode=='06'){
			$tabel_field='GMasuk06';
		}else if($bln_periode=='07'){
			$tabel_field='GMasuk07';
		}else if($bln_periode=='08'){
			$tabel_field='GMasuk08';
		}else if($bln_periode=='09'){
			$tabel_field='GMasuk09';
		}else if($bln_periode=='10'){
			$tabel_field='GMasuk10';
		}else if($bln_periode=='11'){
			$tabel_field='GMasuk11';
		}else if($bln_periode=='12'){
			$tabel_field='GMasuk12';
		}			
			//ambil GKeluar di stock
			$stock2 = $this->sales_return_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			}
			
			//mutasi
			$detail3 = $this->sales_return_model->cekGetDetail3($id);
			foreach($detail3 as $val){
			$returndetailid = $val['returndetailid'];
			$returnno=  $val['returnno'];
			$quantity = $val['quantity'];
			$pcode=  $val['inventorycode'];
			$warehousecode = $val['warehousecode'];
			$adddate = $val['adddate'];
			$this->db->delete('mutasi', array('Tanggal' => $adddate, 'KodeBarang' => $pcode, 'NoTransaksi' => $returnno));			
			}
			
			//salesretundetail
            $this->db->delete('salesreturndetail', array('returndetailid' => $returndetailid));	

			//deliveryorder
            $this->db->delete('salesreturn', array('returnno' => $id));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/sales_return/');
    }

    function delete_detail() 
    {
	
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);
		
        //pertama harus kurangi angka di stock
		 //ambil qty dari deliveryorderdetail
			$detail2 = $this->sales_return_model->cekGetDetail2($pcode,$nodok);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  if($bln_periode=='01'){
				$tabel_field='GMasuk01';
			}else if($bln_periode=='02'){
				$tabel_field='GMasuk02';
			}else if($bln_periode=='03'){
				$tabel_field='GMasuk03';
			}else if($bln_periode=='04'){
				$tabel_field='GMasuk04';
			}else if($bln_periode=='05'){
				$tabel_field='GMasuk05';
			}else if($bln_periode=='06'){
				$tabel_field='GMasuk06';
			}else if($bln_periode=='07'){
				$tabel_field='GMasuk07';
			}else if($bln_periode=='08'){
				$tabel_field='GMasuk08';
			}else if($bln_periode=='09'){
				$tabel_field='GMasuk09';
			}else if($bln_periode=='10'){
				$tabel_field='GMasuk10';
			}else if($bln_periode=='11'){
				$tabel_field='GMasuk11';
			}else if($bln_periode=='12'){
				$tabel_field='GMasuk12';
			}			
			//ambil GKeluar di stock
			$stock2 = $this->sales_return_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			
		   //hapus dimutasi
			$this->db->delete('mutasi', array('Tanggal' => $v_tgl_dokumen, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
			
		    //baru hapus di salesreturndetail
			$this->db->delete('salesreturndetail', array('returndetailid' => $detail2->returndetailid, 'inventorycode' => $pcode, 'returnno' => $nodok));
			
			//hapus di creditnotedetail
			//ambil cnno
			$ambilcnno = $this->sales_return_model->cekGetCnno($nodok);
			//hapus di creditnotedetail
			$this->db->query("DELETE
								FROM `creditnotedtl`
								WHERE `cnno` = '".$ambilcnno->cnno."' 
								AND `description` LIKE '%".$pcode."%';" );
					
			//cek total value di creditnotedetail
			$cek_total_amount2 = $this->sales_return_model->cekDataCreditNoteDetail($ambilcnno->cnno);
			$grandtotal2 = $cek_total_amount2[0]['grandtotal'];
			$user = $this->session->userdata('username');
			
			$this->updateCreditNote2($ambilcnno->cnno, $v_tgl_dokumen, $nodok, $user, $grandtotal2);
			
			//------------------------------------ update juga ke piutang ----------------------------------------- 
			//insert juga ke tabel piutang
		    $this->updateNewPiutang2($ambilcnno->cnno, $v_tgl_dokumen, $grandtotal2);
		    
		    //insert juga ke tabel piutang
		    $this->updateNewMutasiPiutang2($ambilcnno->cnno, $v_tgl_dokumen, $grandtotal2);
			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/sales_return/edit_sales_return/' . $nodok . '');
    }           

	function updateStock3($thn_periode,$v_warehouse,$pcode,$detail_quantity,$stock_tabel_field,$tabel_field)
    {
    	$jml_update=$stock_tabel_field-$detail_quantity;
        $this->sales_return_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->sales_return_model->unlocktables();
    }
	
	
	function satuan()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->sales_return_model->getSatuanDetail($pcode);
     
     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }     
    }
    
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "R E T U R N";
		$data["header"] 	= $this->sales_return_model->getHeader($nodok);
		$data["detail"] 	= $this->sales_return_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_sr', $data);
	}
	
	
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "sales-return.txt";
		
		$header 	= $this->sales_return_model->getHeader($nodok);
		$detail     = $this->sales_return_model->getDetail_cetak($nodok);
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["NamaSatuan"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "R E T U R N";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->returnno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->returnno;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->adddate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->adddate));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Gudang";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Keterangan; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Kepada";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Kepada"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                
                
                $echo.="\r\n";
                
                $echo.=$arr_epson["cond"]."Alamat";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Alamat"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Alamat;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Alamat));$i++)
                {
                    $echo.=" ";
                }
                $echo.="\r\n";             
                   
            }          
           
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->note;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->retunno,"sales-return");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->retunno,"sales-return");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "sales-return",
		            'noreferensi' => $header->retunno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='sales-return-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
		
}

?>