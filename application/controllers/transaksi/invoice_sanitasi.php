<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Invoice_sanitasi extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/invoice_sanitasi_model');
    }

    function index() 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            
			$data['kasbank'] = $this->invoice_sanitasi_model->getKasBank();
			$data['kdtravel'] = $this->invoice_sanitasi_model->getTourtravel();
			$data['beo'] = $this->invoice_sanitasi_model->getBeo();
			
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/invoice_sanitasi/invoice_sanitasi_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'invoice_sanitasi', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "invoice_sanitasi",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/invoice_sanitasi/index/' . $query_id . '');
    }
    
    
    function getList()
    {
    	
    	    $keyword = $this->uri->segment(4);
            $status = $this->uri->segment(5);
            
            //echo  $keyword." - ".$status;die;
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '100';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

			$data['kasbank'] = $this->invoice_sanitasi_model->getKasBank();
			$data['kdtravel'] = $this->invoice_sanitasi_model->getTourtravel();
			
            $data['data'] = $this->invoice_sanitasi_model->getInvoiceSanitasiList($keyword,$status,$config['per_page']);
			
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
		$this->load->view('transaksi/invoice_sanitasi/invoice_sanitasi_getlist', $data);
	}
    
    
    public function ajax_cek() {    	
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $beo = $this->input->post('v_beo');
        $no_invoice = $this->input->post('v_no_invoice');
        $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
        $tglApp = $getTgl[0]['TglTrans'];
            
        //pertama dapatkan NoStiker BEO di Konfirmasi sanitasi
        $nosticker = $this->invoice_sanitasi_model->getStikerBEO($beo);
        $nostkr="";
        $tglkon="";
        for($x=0; $x<count($nosticker); $x++){
        	if(($x+1)==count($nosticker)){
				$koma="";
			}else{
				$koma=",";
			}
			$nostkr.="'".$nosticker[$x]['NoSticker']."'".$koma;
			$tglkon.="'".$nosticker[$x]['TglKonfirmasi']."'".$koma;
		} 
        //echo $nostkr."   #####   ".$tglkon;die;
        											 
        //detail item makanan
                                           $zql = "a.NoDokumen,
												  (b.`NamaLengkap`) AS menu,
												  SUM(a.`Qty`) AS Qty,
												  a.`Harga`,
												  SUM(a.`Qty`) * a.`Harga` AS Total 
												FROM
												  trans_reservasi_detail a 
												  INNER JOIN masterbarang b 
													ON a.`PCode` = b.`PCode` 
												  INNER JOIN `trans_reservasi_konfirmasi` k ON a.`NoDokumen` = k.NoReservasi
												  INNER JOIN trans_reservasi_konfirmasi_sticker s ON s.`NoDokumen` = k.`NoDokumen`
												    
												WHERE s.`NoSticker` IN ($nostkr) 
												  AND k.`TglKonfirmasi` IN ($tglkon)  
												GROUP BY a.PCode 
												ORDER BY k.`TglKonfirmasi`,
												  s.`NoSticker` ASC ;
        										   ";
        //echo $zql;die;
        $menu = $this->globalmodel->getQuery($zql);
        
        
        if (empty($menu)){
		?>
		
							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data</td>
							    	
							</tr>
							<tr class="title_table">
							    <td colspan="4" align="left">Total</td>
								<td align='right'> <?php echo number_format(0);?> <input type="hidden" id="totalan" name="totalan" value="<?php echo number_format(0);?>"> <input type="hidden" id="totalan2" name="totalan2" value="<?php echo '0';?>"></td>	
							    	
							</tr>
							";
		
		<?php }else{ 
		$no=1;
		$grnttl=0;
	    foreach ($menu as $cetak) {
	    	
	    	
	    ?>
	    					<tr>
							    <td align='center'><?php echo $no;?></td>
								<td align='left'> <input type="text" size="50" class="form-control-new" readonly  id="menu<?php echo $no;?>" name="menu[]" value="<?php echo strtoupper($cetak['menu']);?>"> </td>
								<td align='center'> <input type="text" class="form-control-new" readonly  id="qty<?php echo $no;?>" name="qty[]" style="text-align: right" value="<?php echo number_format($cetak['Qty']);?>"></td>
								<td align="right"> <input style="text-align: right" type="text" class="form-control-new" readonly  id="harga<?php echo $no;?>" name="harga[]" value="<?php echo number_format($cetak['Harga']);?>"> </td>		
							    <td align="right"> <input style="text-align: right" type="text" class="form-control-new" readonly  id="total<?php echo $no;?>" name="total[]" value="<?php echo number_format($cetak['Harga']*$cetak['Qty']);?>"> <input style="text-align: right" type="hidden" class="form-control-new" readonly  id="total_hidden<?php echo $no;?>" name="total_hidden[]" value="<?php echo round($cetak['Harga']*$cetak['Qty']);?>"> </td>	
							    	
							</tr>";
	    
		<?php $grnttl+=round($cetak['Harga']*$cetak['Qty']);$no++;} ?>
		
		                    <tr class="title_table">
							    <td colspan="4" align="left">Total</td>
								<td align='right'> <?php echo number_format($grnttl);?> <input type="hidden" id="totalan" name="totalan" value="<?php echo number_format($grnttl);?>"> <input type="hidden" id="totalan2" name="totalan2" value="<?php echo $grnttl;?>"></td>	
							    	
							</tr>
		<?php }
		
    }
    
    
    public function ajax_get_uang_muka() {    	
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $beo = $this->input->post('beoz');
        $uang_muka = $this->globalmodel->getQuery("a.`Nilai` FROM uang_muka_beo a INNER JOIN voucher_beo b ON a.`BEO` = b.`BEO` WHERE a.BEO='$beo'");
        
        if(empty($uang_muka)){
			$data['uang_muka']=0;
		}else{
			$data['uang_muka']=round($uang_muka[0]['Nilai']);
		}
		
		echo json_encode($data);
    }
    
    
    public function ajax_view_tambahan() {    	
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $beo = $this->input->post('v_beo');
        $no_invoice = $this->input->post('v_no_invoice');
        											 
        //detail item tambahan
        $menu_tambahan = $this->globalmodel->getQuery("     * FROM invoice_sanitasi_detail a WHERE a.NoDokumen='$no_invoice' AND a.Tambahan='Y'        										
        										");
        
        
        if (empty($menu_tambahan)){
		?>
		
							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data Item Tambahan</td>
							    <input type="hidden" id="totalan_tambahan_detail" name="totalan_tambahan_detail" value="0">	
							</tr>";
		
		<?php
		}else{
		$no=1;
		$grnttl=0;
	    foreach ($menu_tambahan as $cetak) {
	    ?>
								
								<tr>
								<td align='left'><?=$cetak['Keterangan'];?> </td>
								<td align="right"><?=number_format($cetak['Nilai']);?></td>		
							    <td align="center">
							    
							    <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Close" title="" onclick="delete_detail_tambahan('<?php echo $cetak["id"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                                </button>
							    
							    </td>	
							    	
							</tr>";
	    
		<?php $grnttl+=$cetak['Nilai'];$no++;} ?>
		
		                    <tr class="title_table">
							    <td colspan="2" align="left">Total</td>
								<td align='right'> <?php echo number_format($grnttl);?> <input type="hidden" id="totalan_tambahan_detail" name="totalan_tambahan_detail" value="<?php echo $grnttl;?>"></td>	
							    	
							</tr>
		<?php }
		
    }
    
    
    public function ajax_add() {    	
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $beo = $this->input->post('v_beo');
        $no_invoice = $this->input->post('v_no_invoice');
        
        //grantotal
        $grand_totals = $this->input->post('grand_totals');
		
		
        $diskon = $this->input->post('diskon');
        $diskon_nilai = $this->input->post('diskon_nilai');
        $pembulatan = $this->input->post('pembulatan');
        $diskon_nominal = $this->input->post('diskon_nominal');
        $totalanDPP = $this->input->post('totalanDPP');
        $pph = $this->input->post('pph');
        $pph_nilai = $this->input->post('pph_nilai');
        $ppn = $this->input->post('ppn');
        $totalsanitasi = $this->input->post('totalsanitasi');
        
        $v_ket_tambahan1 = $this->input->post('v_ket_tambahan');
        $v_harga_tambahan1 = $this->input->post('v_harga_tambahan');
        
        $v_menu1 = $this->input->post('menu');
        $v_total_hidden1 = $this->input->post('total_hidden');
        
        $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
        //$tglApp = $getTgl[0]['TglTrans'];
            
        //pertama dapatkan NoStiker BEO di Konfirmasi sanitasi
        $nosticker = $this->invoice_sanitasi_model->getStikerBEO($beo);
        $nostkr="";
        $tglkon="";
        for($x=0; $x<count($nosticker); $x++){
        	if(($x+1)==count($nosticker)){
				$koma="";
			}else{
				$koma=",";
			}
			$nostkr.="'".$nosticker[$x]['NoSticker']."'".$koma;
			$tglkon.="'".$nosticker[$x]['TglKonfirmasi']."'".$koma;
		} 
        //echo $nostkr."   #####   ".$tglkon;die;
        //jumlah belanja
        $jumlah_belanja = $this->globalmodel->getQuery(" a.`TglDokumen`, SUM(b.`Harga`) AS Total_Belanja 
														FROM
														  trans_reservasi a INNER JOIN trans_reservasi_detail b ON a.`NoDokumen` = b.`NoDokumen`
														  INNER JOIN trans_reservasi_konfirmasi k ON a.`NoDokumen` = k.`NoReservasi`
														  INNER JOIN trans_reservasi_konfirmasi_sticker s ON s.`NoDokumen` = k.`NoDokumen`
														WHERE s.`NoSticker` IN ($nostkr) 
														  AND k.`TglKonfirmasi` IN ($tglkon) 
														ORDER BY a.`TglDokumen`,
														  s.`NoSticker` ASC ;
        											 ");
        											 
        //detail item makanan
        $menu = $this->globalmodel->getQuery("  a.NoDokumen,
												  (b.`NamaLengkap`) AS menu,
												  SUM(a.`Qty`) AS Qty,
												  a.`Harga`,
												  SUM(a.`Qty`) * a.`Harga` AS Total 
												FROM
												  trans_reservasi_detail a 
												  INNER JOIN masterbarang b 
													ON a.`PCode` = b.`PCode` 
												  INNER JOIN `trans_reservasi_konfirmasi` k ON a.`NoDokumen` = k.NoReservasi
												  INNER JOIN trans_reservasi_konfirmasi_sticker s ON s.`NoDokumen` = k.`NoDokumen`
												   
												WHERE s.`NoSticker` IN ($nostkr) 
												  AND k.`TglKonfirmasi` IN ($tglkon)  
												GROUP BY a.PCode 
												ORDER BY k.`TglKonfirmasi`,
												  s.`NoSticker` ASC ;
        											 ");
       
        $list_menu="";											
        for($y=0; $y<count($menu); $y++){
        	if(($y+1)==count($menu)){
				$koma=".";
			}else{
				$koma=",";
			}
			$list_menu.=$menu[$y]['menu'].$koma;
		}
        
        $tglApp = $nosticker[0]['TglKonfirmasi'];											 
        //dapatkan uang muka menjadi voucher dengan BEO ini
        $nilai_voucher = $this->invoice_sanitasi_model->getUangMuka($beo);
        $jatuh_tempo = $this->globalmodel->getQuery(" DATE_ADD('$tglApp', INTERVAL 14 DAY) AS due_date;");
        $info_beo = $this->globalmodel->getQuery(" * FROM trans_reservasi a WHERE a.NoDokumen='$beo'");
        
        $nomor_invoice = $no_invoice;
        $tanggal_invoice = $tglApp;
        $NoReservasi = $beo;
        $keterangan = $info_beo[0]['adultParticipants']+$info_beo[0]['childParticipants']+$info_beo[0]['kidsParticipants']." PAX. CODE GROUP : ".$info_beo[0]['GroupCode'];
        $totalnilaibelanja = $jumlah_belanja[0]['Total_Belanja'];
        $uang_muka = $nilai_voucher[0]['nilai'];
        $due_date = $jatuh_tempo[0]['due_date'];
        //$piutang = $totalnilaibelanja - $uang_muka;
        //$piutang = $grand_totals - $uang_muka;
        $piutang = $grand_totals;
           
          //insert ke invoice_sanitasi	
		  $data = array('NoDokumen'=>$nomor_invoice,
				        'Tanggal'=>$tanggal_invoice,
				        'Jatuh_Tempo'=>$due_date,
				        'NoReservasi'=>$NoReservasi,
				        'Keterangan'=>strtoupper($list_menu)."".$keterangan,
				        'Diskon'=>$diskon,
				        'Diskon_Nilai'=>$diskon_nominal,
				        'Total_DPP'=>$totalanDPP,
				        'PPN'=>$ppn,
				        'PPH'=>$pph,
				        'PPH_Nilai'=>$pph_nilai,
				        'Pembulatan'=>$pembulatan,
				        'Total'=>$totalsanitasi,
				        'Total_Nilai'=>$piutang,
				        'Uang_Muka'=>$uang_muka,
				        'AddUser'=>$user,
				        'AddDate'=>date('Y-m-d'));
        
        $this->db->insert("invoice_sanitasi", $data);
        
                       // detail list transaksi
				        for ($y = 0; $y < count($v_menu1); $y++) 
				        {
				        	$v_menu = $v_menu1[$y];
				        	$v_total_hidden = $v_total_hidden1[$y];
				        	
				        	//data
				        	$data_detail_list = array(
				        								'NoDokumen'=>$nomor_invoice,
				        								'Keterangan'=>$v_menu,
				        								'Nilai'=>$v_total_hidden
				        								);
				        	
				        	//insert
				        	$this->db->insert("invoice_sanitasi_detail", $data_detail_list);
				        } 
        
                        // detail tambahan
				        for ($x = 0; $x < count($v_ket_tambahan1); $x++) 
				        {
				        	$v_ket_tambahan = $v_ket_tambahan1[$x];
				        	$v_harga_tambahan = $v_harga_tambahan1[$x];
				        	
				        	//data
				        	$data_detail_invoice = array(
				        								'NoDokumen'=>$nomor_invoice,
				        								'Keterangan'=>$v_ket_tambahan,
				        								'Nilai'=>$v_harga_tambahan,
				        								'Tambahan'=>'Y'
				        								);
				        	
				        	//insert
				        	if($v_ket_tambahan!=""){
				        	$this->db->insert("invoice_sanitasi_detail", $data_detail_invoice);
				        	}
				        }        
        
        echo json_encode(array("status" => TRUE));
    }

    
    public function ajax_update_invoice() {    	
        
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die; 
        $user = $this->session->userdata('username');
        $mylib = new globallib(); 
        
        $beo = $this->input->post('v_beo');
        $no_invoice = $this->input->post('v_no_invoice');
        $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
        $tglApp = $getTgl[0]['TglTrans'];
            
        //pertama dapatkan NoStiker BEO di Konfirmasi sanitasi
        $nosticker = $this->invoice_sanitasi_model->getStikerBEO($beo);
        $nostkr="";
        $tglkon="";
        for($x=0; $x<count($nosticker); $x++){
        	if(($x+1)==count($nosticker)){
				$koma="";
			}else{
				$koma=",";
			}
			$nostkr.="'".$nosticker[$x]['NoSticker']."'".$koma;
			$tglkon.="'".$nosticker[$x]['TglKonfirmasi']."'".$koma;
		} 
        
        //jumlah belanja
        $jumlah_belanja = $this->globalmodel->getQuery("  SUM(b.`Harga`) AS Total_Belanja 
														FROM
														  trans_reservasi a INNER JOIN trans_reservasi_detail b ON a.`NoDokumen` = b.`NoDokumen`
														  INNER JOIN trans_reservasi_konfirmasi k ON a.`NoDokumen` = k.`NoReservasi`
															INNER JOIN trans_reservasi_konfirmasi_sticker s 
															  ON k.`NoDokumen` = s.`NoDokumen` 
														WHERE s.`NoSticker` IN ($nostkr) 
														  AND k.`TglKonfirmasi` IN ($tglkon) 
														ORDER BY a.`TglDokumen`,
														  s.`NoSticker` ASC ;
        											 ");
        											 
        //detail item makanan
        $menu = $this->globalmodel->getQuery("  DISTINCT(b.`NamaLengkap`) AS menu
														FROM
														  trans_reservasi_detail a 
														INNER JOIN masterbarang b ON a.`PCode` = B.`PCode`
														INNER JOIN `trans_reservasi_konfirmasi` c ON a.`NoDokumen` = c.NoReservasi 
														INNER JOIN trans_reservasi_konfirmasi_sticker s ON s.`NoDokumen` = c.`NoDokumen`
														WHERE s.`NoSticker` IN ($nostkr) 
														  AND k.`TglKonfirmasi` IN ($tglkon) 
														GROUP BY a.PCode 
														ORDER BY k.`TglKonfirmasi`,
														  s.`NoSticker` ASC ;
        											 ");
       
        $list_menu="";											
        for($y=0; $y<count($menu); $y++){
        	if(($y+1)==count($menu)){
				$koma=".";
			}else{
				$koma=",";
			}
			$list_menu.=$menu[$y]['menu'].$koma;
		}
        											 
        //dapatkan uang muka menjadi voucher dengan BEO ini
        $nilai_voucher = $this->invoice_sanitasi_model->getUangMuka($beo);
        $jatuh_tempo = $this->globalmodel->getQuery(" DATE_ADD('$tglApp', INTERVAL 14 DAY) AS due_date;");
        $info_beo = $this->globalmodel->getQuery(" * FROM trans_reservasi a WHERE a.NoDokumen='$beo'");
        
        $nomor_invoice = $no_invoice;
        $tanggal_invoice = $tglApp;
        $NoReservasi = $beo;
        $keterangan = $info_beo[0]['adultParticipants']+$info_beo[0]['childParticipants']+$info_beo[0]['kidsParticipants']." PAX. CODE GROUP : ".$info_beo[0]['GroupCode'];
        $totalnilaibelanja = $jumlah_belanja[0]['Total_Belanja'];
        $uang_muka = $nilai_voucher[0]['nilai'];
        $due_date = $jatuh_tempo[0]['due_date'];
        $piutang = $totalnilaibelanja - $uang_muka;
           
          //insert ke invoice_reservasi	
		  $data = array('Tanggal'=>$tanggal_invoice,
				        'Jatuh_Tempo'=>$due_date,
				        'NoReservasi'=>$NoReservasi,
				        'Keterangan'=>strtoupper($list_menu)."".$keterangan,
				        'Total_Nilai'=>$piutang,
				        'Uang_Muka'=>$uang_muka,
				        'AddUser'=>$user,
				        'AddDate'=>date('Y-m-d'));
        
        $this->db->update("invoice_reservasi", $data ,array('NoDokumen'=>$nomor_invoice));
        
        echo json_encode(array("status" => TRUE));
    }
        
    public function ajax_edit_invoice() {
        $sgv= $this->uri->segment(4);
        $inv = $this->uri->segment(5);
        $ar= $this->uri->segment(6);
        $thn= $this->uri->segment(7);
        $rmw= $this->uri->segment(8);
        $cont = $this->uri->segment(9);
        $nodok = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;
        $data = $this->invoice_sanitasi_model->get_by_id($nodok);
        echo json_encode($data);
    }
    
    public function generate_number_invoice() {
    	
    		$cekInv="InvoiceReservasi AS Invoice FROM setup";
            $getInv = $this->globalmodel->getQuery($cekInv);
            $inv=$getInv[0]['Invoice'];
    	
    	    $no = $inv."/INV/AR/";
    	    $beo = $this->input->post('beoz');
    	    $getTgl = $this->invoice_sanitasi_model->getTglKonfrmBEO($beo);
            //$getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
            $tglApp = $getTgl[0]['TglTrans'];
            $pisah_tanggal = explode('-',$tglApp);
            $thn = substr($tglApp, 0, 4);
            $bln = $this->dateRomawi(substr($tglApp, 5, 2));
			$zql=" NoDokumen from invoice_sanitasi WHERE YEAR(Tanggal)='".$pisah_tanggal[0]."' AND MONTH(Tanggal)='".$pisah_tanggal[1]."' ORDER BY invoice_sanitasi.NoDokumen DESC limit 1";
            $getNoDokumen = $this->globalmodel->getQuery($zql);

            if (!empty($getNoDokumen)) {
                $nodokumen = $getNoDokumen[0]['NoDokumen'];
                $noLast = (substr($nodokumen, -4) *1) +1;
                if (strlen($noLast) == "1") {
                    $noDokNew = "0000" . $noLast;
                } elseif (strlen($noLast) == "2") {
                    $noDokNew = "000" . $noLast;
                } elseif (strlen($noLast) == "3") {
                    $noDokNew = "00" . $noLast;
                } elseif (strlen($noLast) == "4") {
                    $noDokNew = "0" . $noLast;
                } elseif (strlen($noLast) == "5") {
                    $noDokNew = $noLast;
                } else {
                    $noDokNew = "00001";
                }
            } else {
                $noDokNew = "00001";
            }
            
        $invoice = $no . $thn . "/" . $bln . "/" . $noDokNew;
		
        $data = array('v_no_invoice'=>$invoice);
        echo json_encode($data);
    }
    
    
    function dateRomawi($bulan) {
        switch ($bulan) {
            case 1:
                $blnRom = 'I';
                break;
            case 2:
                $blnRom = 'II';
                break;
            case 3:
                $blnRom = 'III';
                break;
            case 4:
                $blnRom = 'IV';
                break;
            case 5:
                $blnRom = 'V';
                break;
            case 6:
                $blnRom = 'VI';
                break;
            case 7:
                $blnRom = 'VII';
                break;
            case 8:
                $blnRom = 'VIII';
                break;
            case 9:
                $blnRom = 'IX';
                break;
			case 10:
                $blnRom = 'X';
                break;
            case 11:
                $blnRom = 'XI';
                break;
            case 12:
                $blnRom = 'XII';
                break;
        }
        return $blnRom;
    }
    
    
    function bayar() 
    {
    			  $mylib = new globallib(); 
    			  
    			    $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;
    			  
    			  //update status di invoice sanitasi
    			  $this->db->update('invoice_sanitasi',array('Status'=>1),array('NoDokumen'=>$id));
    			  
    			  $data = $this->invoice_sanitasi_model->get_by_id($id);
    			 
    			  $info_beo = $this->globalmodel->getQuery(" * FROM trans_reservasi a WHERE a.NoDokumen='$data->NoReservasi'");
    			  
    			  //update trans_reservasi status_invoice
    			  $this->db->update('trans_reservasi',array('status_invoice'=>1),array('NoDokumen'=>$data->NoReservasi));
    				
    	              //insert juga ke tabel Piutang
	    	          $data_piutang = array('NoDokumen'=>$data->NoDokumen,
									        'NoFaktur'=>$data->NoDokumen,
									        'TglTransaksi'=>$data->Tanggal,
									        'KdCustomer'=>$info_beo[0]['KdTravel'],
									        'TipeTransaksi'=>'IR',
									        'JatuhTempo'=>$data->Jatuh_Tempo,
									        'NilaiTransaksi'=>$data->Total_Nilai,
									        'Sisa'=>$data->Total_Nilai);
    	               $this->db->insert("piutang", $data_piutang);
    	               
    	               $pisah = explode('-',$data->Tanggal);
    	               //insert juga ke tabel Mutasi Piutang
	    	          $data_mutasi_piutang = array('Tahun'=>$pisah[0],
									        'Bulan'=>$pisah[1],
									        'NoDokumen'=>$data->NoDokumen,
									        'Tanggal'=>$data->Tanggal,
									        'KdCustomer'=>$info_beo[0]['KdTravel'],
									        'Jumlah'=>$data->Total_Nilai,
									        'TipeTransaksi'=>'IR',
									        'MataUang'=>'IDR-1',
									        'Kurs'=>'1');
    	               $this->db->insert("mutasi_piutang", $data_mutasi_piutang);
    	
    	echo json_encode(array("status" => TRUE));
    }
    
    
     function void_invoice() 
    {
    			  $mylib = new globallib(); 
    			  
    			    $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;
    			  
    			  
    			  
    			  $this->db->delete('piutang',array('NoDokumen'=>$id));
    			  $this->db->delete('mutasi_piutang',array('NoDokumen'=>$id));
    	
    			  $getBEO= $this->globalmodel->getQuery(" * FROM invoice_sanitasi a WHERE a.NoDokumen='$id'");
    			  $this->db->update('trans_reservasi',array('status_invoice'=>0),array('NoDokumen'=>$getBEO[0]['NoReservasi']));
    			  
    			  $this->db->delete('invoice_sanitasi',array('NoDokumen'=>$id));
    			  $this->db->delete('invoice_sanitasi_detail',array('NoDokumen'=>$id));
           
    	echo json_encode(array("status" => TRUE));
    }
    
    
    function hapus_invoice() 
    {
    			  $mylib = new globallib(); 
    			  
    			    $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;
    			  
    			  //update status di invoice sanitasi
    			  $this->db->delete('invoice_sanitasi',array('NoDokumen'=>$id));
    			  $this->db->delete('invoice_sanitasi_detail',array('NoDokumen'=>$id));
    			  
    			  echo json_encode(array("status" => TRUE));
    }
    
    
    function delete_detail_tambahan() 
    {
    			  $mylib = new globallib(); 
    			  
    			    $id_tambahan = $this->uri->segment(4);
    			  
					$this->db->delete('invoice_sanitasi_detail',array('id'=>$id_tambahan));
    	
    	echo json_encode(array("status" => TRUE));
    }
    
    function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln )
    {
        $query = "SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
        AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        $col_primary_ok = '';
        if(count($row)>0){
			list($col_primary_ok) = $row;	
		}

        $counter = ((substr($col_primary_ok, 7, 4) * 1)+1)*3 + 2 * 3;
        //$counter = (substr($col_primary_ok, 10, 1) * 3) + 2 * 3;
        $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
    function ajax_travel(){
		$var = $this->input->post('id');
		$query = $this->invoice_sanitasi_model->getTravelList($var);
		 
	 echo "<option value=''> -- Pilih Travel --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[KdTravel]>$cetak[Nama]</option>";
	 
	    }
    }
    
    
    function getDataSanitasi()
    {        
	     $travel = $this->input->post('trv');
	     $query = $this->invoice_sanitasi_model->getDataSanitasi($travel);
	     
	      echo "<option value=''> -- Pilih BEO --</option>";
	      foreach ($query as $cetak) {
		  echo "<option value=$cetak[NoDokumen]>$cetak[NoDokumen]</option>";
		  }   
    }
    
	function create_pdf() {
		
		            $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;
			        
        $NoTransaksi = $id;
        //echo $NoTransaksi;die;
        
        $data['getHeader'] = $this->invoice_sanitasi_model->getTravelDetail($NoTransaksi);
		$data['getDetail'] = $this->invoice_sanitasi_model->getDetailInvoice($NoTransaksi);
		$data['getTambahan'] = $this->invoice_sanitasi_model->getBiayaTambahan($NoTransaksi);
        $data['dibuatoleh'] = $this->invoice_sanitasi_model->getdibuatoleh($id);
        $html = $this->load->view('transaksi/invoice_sanitasi/pdf_invoice_sanitasi', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_komisi.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }
    		
}

?>