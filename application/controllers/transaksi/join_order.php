<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Join_order extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
		$this->load->model('globalmodel');
        $this->load->model('transaksi/join_order_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
			//print_r($data);die();
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/join_order/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/join_order/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/join_order/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("No Kassa","No Transaksi","Tanggal","Waktu","Total Item","Table","Person", "Aksi");
	        $config['total_rows']		= $this->join_order_model->num_order_row($id,$with);
	        $data['data']	= $this->join_order_model->getList($config['per_page'],$page,$id,$with);
			$data['tanggal'] = $this->join_order_model->getDate();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/join_order/join_order_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	
	function add_join_order($meja)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
        	$data['allmeja'] = $this->join_order_model->getAllMeja($meja);
        	$data['header'] = $this->join_order_model->getHeader($meja);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/join_order/add_join_order', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
    function go_join_order(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die();
		$notrans = $this->input->post('v_trans');
		$meja_utama = $this->input->post('v_meja');
		$tgl_dokumen= $this->input->post('v_tgl_dokumen');
		$meja1= $this->input->post('nomeja');
		
		 //ambil menu di meja tujuan
            
		    
		for ($x = 0; $x < count($meja1); $x++) 
		{
            $no_trans_meja_sasaran = $meja1[$x];
            //ambil menu di meja tujuan
            $sql="SELECT * FROM trans_order_detail a WHERE a.NoTrans='$no_trans_meja_sasaran'";
		    $res = $this->join_order_model->Querydata($sql);
		    $no = 0;
		    for($i=0;$i<count($res);$i++){
		    	
		    	$zql="SELECT NoUrut FROM trans_order_detail a WHERE a.NoTrans='$notrans' ORDER BY NoUrut DESC LIMIT 0,1";
			    $rez = $this->join_order_model->Querydata($zql);
			    $norut = ($rez[0]['NoUrut']*1) + 1;
		    
		        $no = $i+$x+$norut;
		       
		        $sql = array (
		                  'NoTrans' => $notrans,
						  'NoUrut' => $no,
						  'NoKassa' => $res[$i]['NoKassa'],
						  'Tanggal' => $res[$i]['Tanggal'],
						  'Waktu' => $res[$i]['Waktu'],
						  'Kasir' => $res[$i]['Kasir'],
						  'KdStore' => $res[$i]['KdStore'],
						  'PCode' => $res[$i]['PCode'],
						  'Qty' => $res[$i]['Qty'],
						  'Berat' => $res[$i]['Berat'],
						  'Satuan' => $res[$i]['Satuan'],
						  'Keterangan' => $res[$i]['Keterangan'],
						  'Note_split' => $res[$i]['Note_split'],
						  'Status' => '1',
						  'KdPersonal' => $res[$i]['KdPersonal'],
						  'KdMeja' => $meja_utama,
						  'KdContact' => $res[$i]['KdContact'],
						  'MenuBaru' => $res[$i]['MenuBaru']
		                     );
		    			
		    		$this->db->insert('trans_order_detail',$sql);
		    		
		    		$this->db->update('trans_order_header',array('Status'=>2),array('NoTrans'=>$res[$i]['NoTrans']));
		    		$this->db->update('trans_order_detail',array('Status'=>2),array('NoTrans'=>$res[$i]['NoTrans']));
			}
			
			    $zkl="SELECT COUNT(PCode) AS TotalItem FROM trans_order_detail a WHERE a.NoTrans='$notrans'";
			    $rek = $this->join_order_model->Querydata($zkl);
			    $total_item = ($rek[0]['TotalItem']*1);
		        $this->db->update('trans_order_header',array('TotalItem'=>$total_item),array('NoTrans'=>$notrans));
		}
		
		redirect('/transaksi/join_order/');
	}
	
}