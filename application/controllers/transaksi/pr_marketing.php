<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pr_marketing extends authcontroller {

    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('transaksi/pr_marketing_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');

	        $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '5';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

			$config['base_url']       = base_url().'index.php/transaksi/pr_marketing/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/pr_marketing/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/transaksi/pr_marketing/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->pr_marketing_model->num_pr_marketing_row($id,$with);
	        $data['data']  = $this->pr_marketing_model->get_pr_marketing_List($config['per_page'],$page,$id,$with);
	        $data['aktivitas'] = $this->pr_marketing_model->getAktivitasList();
			$data['divisi'] = $this->pr_marketing_model->getDivisi();
			$data['proposal'] = $this->pr_marketing_model->getProposal();
	        $data['track'] = $mylib->print_track();
	        
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
	        $this->load->view('transaksi/pr_marketing/pr_marketing_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function save_pr(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$NoDokumen = $this->input->post('nopr');
		$NoProposal = $this->input->post('noproposal');
		$TglDokumen = $this->input->post('v_tgl_dokumen');
		$TglTerima = $this->input->post('v_tgl_dokumen_terima');
		$KdDivisi = $this->input->post('divisi');
		$Keterangan = $this->input->post('keterangan');
		$Status= $this->input->post('v_status');
		
		//DETAIL
		$NamaBarang1= $this->input->post('nmbrg');
		$Qty1= $this->input->post('qty');
        
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($TglDokumen));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
        
    	$user = $this->session->userdata('username');
    	//nodokumen
        $v_no_dokumen = $mylib->get_code_counter2($this->db->database, "pr_marketing","NoDokumen", "PRM", $data['bulan'], $data['tahun']);
		
		//insert header
    	$data_header = array
    	     ('NoDokumen'=> $v_no_dokumen,
             'NoProposal'=> $NoProposal,
             'TglDokumen'=> $mylib->ubah_tanggal($TglDokumen),
             'TglTerima' => $mylib->ubah_tanggal($TglTerima),
             'FlgKonfirmPO'=> 0,
             'KdDivisi'=>$KdDivisi,
             'Keterangan'=> $Keterangan,
             'Status' => $Status,
             'AddDate'=>date('Y-m-d'),
             'AddUser'=>$user
             );
    	
        //echo "<pre>";print_r($data_header);echo "</pre>";die;     
        $this->db->insert('pr_marketing', $data_header);
        
        //insert detail
        for ($x = 0; $x < count($NamaBarang1); $x++) 
			{
            $NamaBarang = $NamaBarang1[$x];
            $Qty = $mylib->save_int($Qty1[$x]);	
					if($NamaBarang !=""){
						$data_detail=array
						(
						'NoDokumen'=>$v_no_dokumen,
						'NoUrut'=>$x+1,
						'NamaBarang'=>$NamaBarang,
						'Qty'=>$Qty
						);
					$this->db->insert('pr_marketing_detail', $data_detail);
					}
			}
        $data['status']=true;
    	echo json_encode($data);
    }
	
	function edit_pr_(){
    	
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$NoDokumen = $this->input->post('nopr');
		$NoProposal = $this->input->post('noproposal');
		$TglDokumen = $this->input->post('v_tgl_dokumen');
		$TglTerima = $this->input->post('v_tgl_dokumen_terima');
		$KdDivisi = $this->input->post('divisi');
		$Keterangan = $this->input->post('keterangan');
		$Status= $this->input->post('v_status');
		
		//DETAIL
		$NamaBarang1= $this->input->post('nmbrg');
		$Qty1= $this->input->post('qty');
        
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
    	$user = $this->session->userdata('username');
    	
		//insert header
    	$data_header = array
    	     (
             'NoProposal'=> $NoProposal,
             'TglDokumen'=> $mylib->ubah_tanggal($TglDokumen),
             'TglTerima' => $mylib->ubah_tanggal($TglTerima),
             'FlgKonfirmPO'=> 0,
             'KdDivisi'=>$KdDivisi,
             'Keterangan'=> $Keterangan,
             'Status' => $Status,
             'AddDate'=>date('Y-m-d'),
             'AddUser'=>$user
             );
    	//print_r($data_header);die;
            
        $this->db->update('pr_marketing', $data_header, array('NoDokumen'=> $NoDokumen));
        die;
        //insert detail
        for ($x = 0; $x < count($NamaBarang1); $x++) 
			{
            $NamaBarang = $NamaBarang1[$x];
            $Qty = $mylib->save_int($Qty1[$x]);	
					if($NamaBarang != ""){
						$data_detail=array
						(
						'NoDokumen'=>$NoDokumen,
						'NoUrut'=>$x+1,
						'NamaBarang'=>$NamaBarang,
						'Qty'=>$Qty
						);
					$this->db->insert('pr_marketing_detail', $data_detail);
					}
			}
			
        $data['status']=true;
    	echo json_encode($data);
    	
    }
	
	function delete_pr_marketing(){
    	$NoDokumen = $this->input->post('id');
		$this->db->delete('pr_marketing',array('NoDokumen'=>$NoDokumen));
    	$this->db->delete('pr_marketing_detail',array('NoDokumen'=>$NoDokumen));
    }
    
    function ajax_proposal(){
		$NoProposal = $this->input->post('id');
		$query = $this->pr_marketing_model->getNoProposal($NoProposal);
	 
	 if(!empty($query)){	 
	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[NoProposal]>$cetak[NamaProposal]</option>";
	 
	    }
	  }
    }
	    
    function edit_pr($nodok) 
    {
		$cek = $this->pr_marketing_model->get_by_pr($nodok);
		//print_r($cek);die;
		$data=array(		
		'NoDokumen'=>$cek[0]['NoDokumen'],
		'NoProposal'=>$cek[0]['NoProposal'],
		'TglDokumen'=>$cek[0]['TglDokumen_'],
		'TglTerima'=>$cek[0]['TglTerima_'],
		'KdDivisi'=>$cek[0]['KdDivisi'],
		'Keterangan'=>$cek[0]['Keterangan'],
		'Status'=>$cek[0]['Sts']
		);
		echo json_encode($data);			
    }
	
	function edit_pr_detail($nodok){
		$query = $this->pr_marketing_model->getPrDetail($nodok);
		
		if (empty($query)){
		?>
		
							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data Purchase Request Detail</td>
							    	
							</tr>";
		
		<?php
		}else{
	    foreach ($query as $cetak) {
	    ?>
							<tr>
								<td align='left'><?php echo $cetak['NamaBarang'];?></td>
								<td align='center'><?php echo $cetak['Qty'];?></td>		
							    	<td align='center'>
					                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Delete" title="" onclick="delete_detail('<?php echo $nodok; ?>','<?php echo $cetak['NamaBarang']; ?>');" >
											<i class="entypo-trash"></i>
										</button>
					                </td>
							    	
							</tr>";
	    
		<?php }
		}
    }
	
	public function delete_detail()
	{
		$NoDokumen = $this->input->post('nodok');
		$NamaBarang = $this->input->post('barang');
		$this->db->delete('pr_marketing_detail',array('NoDokumen'=>$NoDokumen,'NamaBarang'=>$NamaBarang));
		echo json_encode(array("status" => TRUE));
	}
	
	function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "PURCHASE REQUSET";
		$data["header"] 	= $this->pr_marketing_model->getHeader($nodok);
		
		$data["detail"] 	= $this->pr_marketing_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_prm', $data);
	}
	
	function doPrint_old()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "purchase-request.txt";
		
		$header 	= $this->pr_marketing_model->getHeader($nodok);	
		$detail 	= $this->pr_marketing_model->getDetail_cetak($nodok);
	    
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            //$arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,60);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            //$arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			//$arr_data["detail_namasatuan"][$counter] = $val["NamaSatuan"];
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "PURCHASE REQUEST";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
           
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$nodok)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$nodok;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header[0]['TglDokumen_'];         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header[0]['TglDokumen_']));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Divisi";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Divisi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header[0]['NamaDivisi']; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Tanggal Terima";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal Terima"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header[0]['TglTerima_'];  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header[0]['TglTerima_']));$i++)
                {
                    $echo.=" ";
                }
                
                
                $echo.="\r\n";           
                   
            }          
           
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            /*$echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }*/
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            /*$echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }*/
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            //$pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            //$satuan = $arr_data["detail_satuan"][$i_detail];
				//$namasatuan = $arr_data["detail_namasatuan"][$i_detail];
	            
	            if($namabarang)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    /*$echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }*/
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    /*$echo.=$arr_epson["cond"].$namasatuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($namasatuan));$i++)
                    {
                        $echo.=" ";
                    }*/
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header[0]['Keterangan'];
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($nodok,"purchase-request");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($nodok,"purchase-request");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "purchase-request",
		            'noreferensi' => $nodok,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='purchase-request-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "purchase-request.txt";
		
		$header 	= $this->pr_marketing_model->getHeader($nodok);	
		$detail 	= $this->pr_marketing_model->getDetail_cetak($nodok);
	    
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            //$arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,60);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            //$arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			//$arr_data["detail_namasatuan"][$counter] = $val["NamaSatuan"];
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "PURCHASE REQUEST";
        
        $grand_total = 0;
        

		$paths = "path/to/";
	    $name_text_file='purchase-request-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function create_pdf() {
        $id = $this->uri->segment(4);
        
        $data['header']= $this->pr_marketing_model->getHeaderPDF($id);	
        $data['detail']= $this->pr_marketing_model->getDetail_cetak($id);
        
		$data['nomor']=$id;
        $html = $this->load->view('transaksi/pr_marketing/pdf_pr_marketing',$data, true);
        $this->load->library('m_pdf');
		
        $pdfFilePath = "the_pdf_proposal.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }
    
  }
?>