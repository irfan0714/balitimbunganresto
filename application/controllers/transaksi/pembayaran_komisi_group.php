<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pembayaran_komisi_group extends authcontroller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('transaksi/pembayaran_komisi_group_model');
		$this->load->model('transaksi/keuangan/paymentmodel');
	}

	public function index()
	{
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");

    	$tanggal 	= $this->input->post('tgldari');
    	$group 		= $this->input->post('group');
    	$kas		= $this->input->post('kas');
    	$topaid		= $this->input->post('topaid');
    	$nobukti	= $this->input->post('nobukti');
    	$keterangan	= $this->input->post('keterangan');



		//if($sign =='Y') {		

    	$data['combo']		= $this->pembayaran_komisi_group_model->getgroupcombo();	
    	$data['combokas']	= $this->pembayaran_komisi_group_model->getkas();
    
		$data['paidto']			= "";
		$data['nobukti']		= "";
		$data['keterangan']		= "";
		
		$today					= date('Y-m-d');
		$data['tgl']			= $today;
		$data['tampilkanDT']	= false;
		

		$this->load->view('transaksi/pembayaran_komisi_group/view', $data);
		//} else {
//			$this->load->view('denied');
//		}
	}

	public function submit()
	{
		$submit 	= $this->input->post('submit');
		$kas		= $this->input->post('kas');
		$tgl 		= $this->input->post('tgldari');
		$group 		= $this->input->post('group');
		$paidto		= $this->input->post('topaid');
		$nobukti	= $this->input->post('nobukti');
		$keterangan	= $this->input->post('keterangan');

		$mylib	= new globallib();
    	$sign  	= $mylib->getAllowList("all");



		//if($sign =='Y') {	

		if($submit)
		{
			$data['combo']			= $this->pembayaran_komisi_group_model->getgroupcombo();	
    		$data['combokas']		= $this->pembayaran_komisi_group_model->getkas();
			$today					= date('Y-m-d');
			$data['tgl']			= $tgl;
			$data['kas']			= $kas;
			$data['paidto']			= $paidto;
			$data['nobukti']		= $nobukti;
			$data['keterangan']		= $keterangan;
			$data['group']			= $group;
			$data['tampilkanDT']	= true;




			$data['data']	= $this->pembayaran_komisi_group_model->getdata($tgl, $group);
		

			$this->load->view('transaksi/pembayaran_komisi_group/view', $data);
			

		}
		//} else {
//			$this->load->view('denied');
//		}
	}

	public function save()
	{

		$tgl			= $this->input->post('tgl');
		$group			= $this->input->post('group');
		$kas			= $this->input->post('kas');
		$paidto			= $this->input->post('paidto');
		$nobukti		= $this->input->post('nobukti');
		$keterangan		= $this->input->post('keterangan');
		$nostruk		= $this->input->post('nostruk');
		$pcode			= $this->input->post('pcode');
		$namalengkap	= $this->input->post('namalengkap');
		$qty			= $this->input->post('qty');
		$netto			= $this->input->post('netto');
		$office			= $this->input->post('office');
		$guide			= $this->input->post('guide');
		$tl 			= $this->input->post('tl');
		$driver			= $this->input->post('driver');
		$user        = $this->session->userdata('userid');

		//$no=1;

		$mylib = new globallib();
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl,0, 4);

     
     
     	$bulan2 = substr($tgl, 5, 2);
		$tahun2 = substr($tgl, 2, 2);
		/*if (date('Y-m-d',strtotime($tgl)) > date('Y-m-d',strtotime('2016-10-31'))){
			
		    $KdKasBank2='01';
		    $kd_no = $this->paymentmodel->getKodeBank($KdKasBank2);
		    $NoDokumen = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun2,$bulan2);
		}else{
			
		    $new_no = $this->paymentmodel->getNewNo($tgl);
		    $NoDokumen = $new_no->NoPayment;
		}*/

		/*echo $tgl;
		echo '<br>';
		echo $tahun;
		echo'<br>';
		echo $bulan;
		die;*/	

		//echo $new_no;
	


		$tgl1 = $this->session->userdata('Tanggal_Trans');

		
		
		for ($i=0;$i<count($pcode); $i++) 
		{

		$new_no= $this->pembayaran_komisi_group_model->getNewNo($tahun,$bulan);
		$no    = $new_no[0]['NoKomisi'];
/*
		echo $no;
		echo '<br>';
		die;*/
		
		//echo $no;
		$this->db->update('counter', array("NoKomisi"=> $new_no[0]['NoKomisi'] + 1), array("Tahun"=> $tahun,"Bulan"=> $bulan));
		
		//$this->db->trans_start();						//-----------------------------------------------------START TRANSAKSI
		
			
			$data	= array(
							'NoTransaksi'			=> $no,
							'TanggalTransaksi' 		=> $tgl,
							'KdTravel' 				=> $group,
							'keterangan' 			=> $keterangan,
							'KdKasBank' 			=> $kas,
							'Penerima'				=> $paidto,
							'NoBukti'				=> $nobukti,
							'Komisi1'				=> $office[$i],
							'Komisi2'				=> $guide[$i],
							'Komisi3'				=> $tl[$i],
							'Komisi4'				=> $driver[$i],
							'AddUser'				=> $user,
							'AddDate'				=> $tgl1,
								);


			$datadetail	= array(
							'NoTransaksi'			=> $no,
							'Pcode'					=> $pcode[$i],
							'Komisi1'				=> $office[$i],
							'Komisi2'				=> $guide[$i],
							'Komisi3'				=> $tl[$i],
							'Komisi4'				=> $driver[$i],
								);
			/*			
			print_r($data);
			echo '<br>';
			echo '<br>';
			print_r($datadetail);
			//echo $no;

		
			die;*/

			$this->db->insert('finance_komisi_group_header', $data);


			$this->db->insert('finance_komisi_group_detail', $datadetail);

			$no++;
		
		//$this->db->trans_complete();				//----------------------------------------------------END TRANSAKSI
		
		} 	

		redirect('transaksi/pembayaran_komisi_group');
	}
	//untuk mendapatkan NoDokumen
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }

}

/* End of file pembayaran_komisi_group.php */
/* Location: ./application/controllers/pembayaran_komisi_group.php */