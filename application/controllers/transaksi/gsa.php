<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Gsa extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/gsamodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/gsa/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/gsa/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/transaksi/gsa/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->gsamodel->num_gsa_row($id,$with);
	        $data['gsa_data']  = $this->gsamodel->get_gsa_List($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/gsa/gsa_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
     		$data['msg']	 = "";
	     	$data['id']		 = "";
	     	$data['nama']	 = "";
	     	//$data['tipe']  = $this->gsamodel->getTipe();
	     	$data['niltipe'] = "";
			//$data['personal']  = $this->gsamodel->getPersonal();
			$data['nilsupervisor'] = "";
			//$data['supervisor']  = $this->gsamodel->getSupervisor();
	     	$data['nilpersonal'] = "";
			//$data['gudang']  = $this->gsamodel->getGudang();
	     	$data['nilgudang'] = "";
			$data['track'] = $mylib->print_track();
			$data['gsa'] = $this->gsamodel->getGSA();
	    	$this->load->view('transaksi/gsa/add_gsa',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function view_gsa($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("view");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_gsa'] = $this->gsamodel->getDetail($id);
	    	$data['supervisor']  = $this->gsamodel->getSupervisor();
	    	//$data['tipe'] 	  = $this->gsamodel->getTipe();
			//$data['personal']  = $this->gsamodel->getPersonal();
			//$data['gudang']	  = $this->gsamodel->getGudang();
			$data['gsa'] = $this->gsamodel->getGSA();
			$data['detail_gsa']= $this->gsamodel->getDetailGSA($id);
	    	$data['edit'] 		  = false;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/gsa/view_edit_gsa', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_gsa($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("del");
    	if($sign=="Y"){
	     	$id 				  = $this->uri->segment(4);
	    	$data['view_gsa'] = $this->gsamodel->getDetail($id);
			$data['cekAda'] = $this->gsamodel->cekDelete($id);
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/gsa/delete_gsa', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function delete_This(){
     	$gsa_id = $this->input->post('id');
     	//header
     	$this->db->delete('gsa', array('gsa_id' => $gsa_id));
     	
     	//detail
     	$this->db->delete('gsa_detail', array('gsa_id' => $gsa_id));
     	
		redirect('/transaksi/gsa/');
	}

    function edit_gsa($id){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id 				  = $this->uri->segment(4);
	    	$data['view_gsa'] = $this->gsamodel->getDetail($id);
	    	//$data['tipe'] 	  = $this->gsamodel->getTipe();
			//$data['personal']  = $this->gsamodel->getPersonal();
			//$data['gudang']	  = $this->gsamodel->getGudang();
			$data['gsa'] = $this->gsamodel->getGSA();
			$data['detail_gsa']= $this->gsamodel->getDetailGSA($id);
	    	$data['edit'] 		 = true;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/gsa/view_edit_gsa', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function save_gsa(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$gsa_id	 = $this->input->post('id');
    	$Tanggal	 = $this->input->post('tanggal');    	
    	//$NoStiker 	 = strtoupper(addslashes(trim($this->input->post('nostiker'))));
    	$employee_id 	 = strtoupper(addslashes(trim($this->input->post('employeegsa'))));
		$user = $this->session->userdata('username');
		
		//check
    	$cekbeauty = $this->input->post('cekbeauty');
    	$cekcoffe = $this->input->post('cekcoffe');
    	$cekchamber = $this->input->post('cekchamber');
    	$cekoh = $this->input->post('cekoh');
    	$cekresto = $this->input->post('cekresto');
    	$cekbe = $this->input->post('cekbe');
    	
    	//detail_gsa
    	$nostiker1 = $this->input->post('nostiker');
    	
    	$data = array(
    		  'Tanggal'	=> $Tanggal,
			  'employee_id' => $employee_id,
			  'beauty_tour'=>$cekbeauty,
               'coffee_tour'=>$cekcoffe,
               'chamber'=>$cekchamber,
               'standby_oh'=>$cekoh,
               'standby_resto'=>$cekresto,
               'standby_be'=>$cekbe,
              'EditDate'		=> date('Y-m-d'),
              'EditUser'		=> $user
			);
		$this->db->update('gsa', $data, array('gsa_id' => $gsa_id));
		
		    //detail
            for ($x = 0; $x < count($nostiker1); $x++) 
			{
            $nostiker = $nostiker1[$x];
            
            $data_gsa=array(
            'gsa_id'=>$gsa_id,
            'NoStiker'=>$nostiker
            );
            
            $this->db->insert('gsa_detail',$data_gsa);
            }
		
    	redirect('/transaksi/gsa/edit_gsa/' . $gsa_id . '');
    }
    
    
    /*function save_new_supervisor(){
    	
		$id 	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	$nama 	 = strtoupper(addslashes(trim($this->input->post('nama'))));
    	$tipe = strtoupper(trim($this->input->post('tipe')));
		$personal = strtoupper(trim($this->input->post('personal')));
		$gudang = strtoupper(trim($this->input->post('gudang')));
    	$num 	 = $this->gsamodel->get_id($id);
    	if($num == 0)
		{
			$tgltrans = $this->session->userdata('Tanggal_Trans');
		 	$data = array(
               'Kdsupervisor'   => $id,
               'Namasupervisor' => $nama,
			   'KdPersonal' => $personal,
               'KdTipesupervisor' => $tipe,
			   'KdGudang' => $gudang,
               'AddDate' 	=> $tgltrans
            );
            $this->db->insert('supervisor', $data);
			redirect('master/supervisor');
		}
		else
		{
			$data['id'] 	 = $this->input->post('kode');
			$data['nama'] 	 = $this->input->post('nama');
			$data['nilpersonal'] = $personal;
			$data['personal']  = $this->gsamodel->getPersonal();
			$data['niltipe'] = $tipe;
			$data['tipe']  = $this->gsamodel->getTipe();
			$data['msg'] 	 = "<font color='red'><b>Error : Data Dengan Kode $id Sudah Ada</b></font>";
			$this->load->view('master/supervisor/add_supervisor', $data);
		}
	}*/
	
	function save_new_gsa_new(){
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
		//$KdSupervisor	 = strtoupper(addslashes(trim($this->input->post('kode'))));
    	//$NoStiker 	 = strtoupper(addslashes(trim($this->input->post('nostiker'))));
    	$mylib = new globallib();
    	$gsa 	 = strtoupper(addslashes(trim($this->input->post('employeegsa'))));
    	
    	//check
    	$cekbeauty = $this->input->post('cekbeauty');
    	$cekcoffe = $this->input->post('cekcoffe');
    	$cekchamber = $this->input->post('cekchamber');
    	$cekoh = $this->input->post('cekoh');
    	$cekresto = $this->input->post('cekresto');
    	$cekbe = $this->input->post('cekbe');
    	
    	//detail
    	$nostiker1 = $this->input->post('nostiker');
    	
    	$tgltrans = $this->session->userdata('Tanggal_Trans');		
    	$user = $this->session->userdata('username');
    	
    	$data['bulan'] = date('m');
        $data['tahun'] = date('Y');
    	
    	$gsa_id = $mylib->get_code_counter3($this->db->database, "GSA","gsa_id", "GSA", $data['bulan'], $data['tahun']);
    	
			//header
		 	$data = array(
		 	   'gsa_id'=>$gsa_id,
		 	   'Tanggal' =>$tgltrans,             
               'employee_id' => $gsa,
               'beauty_tour'=>$cekbeauty,
               'coffee_tour'=>$cekcoffe,
               'chamber'=>$cekchamber,
               'standby_oh'=>$cekoh,
               'standby_resto'=>$cekresto,
               'standby_be'=>$cekbe,
               'AddDate' 	=> date('Y-m-d'),
               'AddUser' 	=> $user
            );
            $this->db->insert('gsa', $data);
            
            //detail
            for ($x = 0; $x < count($nostiker1); $x++) 
			{
            $nostiker = $nostiker1[$x];
            
            $data_gsa=array(
            'gsa_id'=>$gsa_id,
            'NoStiker'=>$nostiker
            );
            
            $this->db->insert('gsa_detail',$data_gsa);
            }
            
            
			redirect('transaksi/gsa');
		
	}
	
	function cek_beauty()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getbeauty($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_coffee()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getcoffee($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_chamber()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getchamber($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_oh()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getoh($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_resto()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getresto($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_be()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getbe($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
	
	function cek_stikeroh()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl =$this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getCekStikerOH($nostiker,$tgl);
	
	 if(empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_stikerresto()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getCekStikerResto($nostiker,$tgl);
	
	 if(empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_stikerbe()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getCekStikerBe($nostiker,$tgl);
	
	 if(empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    
    function cek_transaksi()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getTransaksi($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_beauty()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getAdaGaBeauty($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_coffee()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getAdaGaCoffee($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_chamber()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getAdaGaChamber($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_std_oh()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getAdaGaStdOh($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_std_res()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getAdaGaStdRes($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function cek_ada_ga_std_be()
    {  
          
     $nostiker = $this->input->post('id');
	 $tgl = $this->session->userdata('Tanggal_Trans');
	 
	 $hasilcek = $this->gsamodel->getAdaGaStdBe($nostiker,$tgl);
	
	 if(!empty($hasilcek)){
	 $data=array('status'=>TRUE);
	 }
     echo json_encode($data);
	
    }
    
    function delete_detail_gsa() 
    {
        $id = $this->uri->segment(4);
        $gsa_id = $this->uri->segment(5);
        $nostiker = $this->uri->segment(6);
		
		$this->db->delete('gsa_detail',array('id'=>$id));

        redirect('/transaksi/gsa/edit_gsa/' . $gsa_id . '');
    }
	
}
?>