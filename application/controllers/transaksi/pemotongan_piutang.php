<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pemotongan_piutang extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->helper('terbilang');
        $this->load->model('transaksi/pemotongan_piutang_model');
    }

    function index() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "pemotongan_piutang", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            
	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/pemotongan_piutang/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/pemotongan_piutang/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
			$data['mcustomer'] = $this->pemotongan_piutang_model->getCustomerList();
			
            $config['total_rows'] = $this->pemotongan_piutang_model->num_pemotongan_piutang_row($arr_search["search"]);
            
			$data['data'] = $this->pemotongan_piutang_model->getCnaList($config['per_page'], $page, $arr_search["search"]);
					
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/pemotongan_piutang/pemotongan_piutang_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'pemotongan_piutang', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pemotongan_piutang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pemotongan_piutang/index/' . $query_id . '');
    }
	
	function batal() 
    {
    	    $user = $this->session->userdata('username');
			$this->db->delete('debitnotedtl_temp',array('cnno'=>'00000','adduser'=>$user));
	
			redirect('/transaksi/pemotongan_piutang/');	
			
    }
    
    	
	function add_new() 
    {
    				
			        $mylib = new globallib();
			        $sign = $mylib->getAllowList("add");
		        	if ($sign == "Y") {
		            $data['msg'] = "";
		            
		            $user = $this->session->userdata('username');
		            $userlevel = $this->session->userdata('userlevel');
		            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
		            
		            $data['spp'] 	= "";
		            $data['creditno'] = "";
					$data['mUang']	= $this->pemotongan_piutang_model->getCurrency();
		            $data['mcustomer'] = $this->pemotongan_piutang_model->getCustomer();
								
		            $data['track'] = $mylib->print_track();
		            $this->load->view('transaksi/pemotongan_piutang/add_pemotongan_piutang', $data);
		        } else {
		            $this->load->view('denied');
		        }
    }

    function edit_pemotongan_piutang($id)
    {
    	//echo $id;die;
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->pemotongan_piutang_model->getCurrency();
            $data['header'] = $this->pemotongan_piutang_model->getHeader($id);
            $data['mcustomer'] = $this->pemotongan_piutang_model->getCustomer();
            $data['anak'] = $this->pemotongan_piutang_model->getCnno();
            $data['datacna'] = $this->pemotongan_piutang_model->getCreditNoteAllocationDetail($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/pemotongan_piutang/edit_pemotongan_piutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    function view_pemotongan_piutang($id)
    {
    	//echo $id;die;
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->pemotongan_piutang_model->getCurrency();
            $data['header'] = $this->pemotongan_piutang_model->getHeader($id);
            $data['mcustomer'] = $this->pemotongan_piutang_model->getCustomer();
            $data['anak'] = $this->pemotongan_piutang_model->getCnno();
            $data['datacna'] = $this->pemotongan_piutang_model->getCreditNoteAllocationDetail($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/pemotongan_piutang/view_pemotongan_piutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function getCreditNoteNo()
	{
		
	 	$KdCustomer	= $this->input->post('KdCustomer');
	 	
	 	$key	= $this->pemotongan_piutang_model->getRequirement($KdCustomer);
	 	$customerid = $key->customerid;
		$data = $this->pemotongan_piutang_model->getSibling($customerid);
		
		for($a=0;$a<count($data);$a++)
		{
			echo "<option value='".$data[$a]['cnno']."'>".$data[$a]['cnno'].",  Total ".$data[$a]['sisa_']."</option>";
		}
	}

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $cnadate = $this->input->post('v_tanggal_dokumen');
        $v_customer = $this->input->post('KdCustomer');
        $v_creditno = $this->input->post('creditno');
		$currencycode = $this->input->post('Uang');
		$v_amount = $this->input->post('v_amount');
		$v_amount_allocation = $this->input->post('v_amount_allocation');
		$v_amount_idr = $this->input->post('v_amount_idr');
		$v_amount_allocation_idr = $this->input->post('v_amount_allocation_idr');
		
		$v_cnadetailid2 = $this->input->post('v_cnadetailid2');
		$v_cnallocationno2 = $this->input->post('v_cnallocationno2');
		$v_invamount2 = $this->input->post('v_invamount2');
		$v_allocation2 = $this->input->post('v_allocation2');
		$v_invamountidr2 = $this->input->post('v_invamountidr2');
		$v_allocationidr2 = $this->input->post('v_allocationidr2');
		$v_status = $this->input->post('v_status');
		$v_sisa2 = $this->input->post('v_sisa');
		$v_bayar2 = $this->input->post('v_bayar2');
        
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
		$data['bulan'] = date('m');
        $data['tahun'] = date('Y');
		
        if ($flag == "add")
		{
		//generate cnno
		$v_cna = $mylib->get_code_counter($this->db->database, "cnallocation","cnallocationno", "CA", $data['bulan'], $data['tahun']); 
		
		//pertama insertheader terlebih dahulu
		$this->insertNewHeader($v_cna, $mylib->ubah_tanggal($cnadate), $v_customer, $v_creditno, $user, $currencycode, $v_amount, $v_amount_allocation, $v_amount_idr, $v_amount_allocation_idr );
		
		//ambil NoFaktur di table hutang
		$ambil_no_fak = $this->pemotongan_piutang_model->ambilNoFak($v_customer);
		/*echo "<pre>";
		print_r($ambil_no_fak);
		echo "</pre>";die;*/
		foreach($ambil_no_fak as $val)
		{
			
            $invono = $val['NoFaktur'];
            
            $data_detail=array(
					  'cnallocationno' => $v_cna,
					  'invno' => $invono,
					  'invamount' => $v_amount,
					  'allocation' => $v_amount_allocation,
					  'invamountidr' => $v_amount_idr,
					  'allocationidr' => $v_amount_allocation_idr,
					  'adddate' => date('Y-m-d'),
					  'adduser' => $user
            );
			
			$this->db->insert('cnallocationdetail', $data_detail);
			
			}	
					
		} 
		
		else if ($flag == "edit") 
		{
			$v_cna = $this->input->post('v_cnallocationno');
			$v_tgl_pemotongan_piutang = $this->input->post('v_tgl_pemotongan_piutang');
			$cnno = $this->input->post('v_cnno');
			
			
		
			//update header
			$this->updateHeader($v_cna,$mylib->ubah_tanggal($v_tgl_pemotongan_piutang), $cnno , $v_status, $v_customer , $currencycode, $user);	
			
			//update detail
			for ($x = 0; $x < count($v_cnadetailid2); $x++)
			{
			$v_cnadetailid = $v_cnadetailid2[$x];	
            $v_cnallocationno = $v_cnallocationno2[$x];
            $v_invamount = $v_invamount2[$x];
            $v_allocation = $v_allocation2[$x];
            $v_invamountidr = $v_invamountidr2[$x];
            $v_allocationidr = $v_allocationidr2[$x];
            $v_sisa = $v_sisa2[$x];
            $v_bayar = $v_bayar2[$x];
             
            $data_detail_edit=array(
			  'invamount' => $v_sisa,
			  'allocation' => $v_bayar,
			  'editdate' => date('Y-m-d'),
			  'edituser' => $user
            );
            /*echo "<pre>";
            print_r($data_detail_edit);
            echo "</pre>";die;*/
            
            $where_detail_edit=array(
              'cnadetailid' => $v_cnadetailid,
			  'cnallocationno' => $v_cnallocationno
            						);
			
			$this->db->update('cnallocationdetail', $data_detail_edit,$where_detail_edit);
			
			
			
			//update ke header
			$data_jml=array(
			'allocationtotal'=>$v_bayar
			);
			$dimana=array('cnallocationno'=>$v_cnallocationno);
			
			$this->db->update('cnallocation', $data_jml,$dimana);
			}
			
			
			$sisa = $v_sisa - $v_bayar;
			$data_jml_=array(
			'Sisa'=>$sisa 
			);
			$data_dimana=array(
			'NoDokumen'=>$cnno 
			);
			$this->db->update('piutang', $data_jml_,$data_dimana);			
			
						
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_cna . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/pemotongan_piutang/edit_pemotongan_piutang/'.$v_cna.'');
    }

    function insertNewHeader($v_cna, $cnadate, $v_customer, $v_creditno, $user, $currencycode, $v_amount, $v_amount_allocation, $v_amount_idr, $v_amount_allocation_idr ) 
    {
        $this->pemotongan_piutang_model->locktables('cnallocation');
        
		$allocationtotal = $v_amount + $v_amount_allocation;
		$allocationidrtotal = $v_amount_idr + $v_amount_allocation_idr;
		
        $data = array(
				  'cnallocationno' => $v_cna,
				  'cnadate' => $ccadate,
				  'cnno' => $v_creditno,
				  'customerid' => $v_customer,
				  'currencycode' => $currencycode,
				  'allocationtotal' => $allocationtotal,
				  'allocationidrtotal' => $allocationidrtotal,
				  'adddate' => date('Y-m-d'),
				  'adduser' => $user,
				  'status' => 0
				);

        $this->db->insert('cnallocation', $data);

        $this->pemotongan_piutang_model->unlocktables();

    }
    
    
    function updateNewHutang($cnno, $tgl, $v_supplier,$v_type, $grandtotal) 
    {
        $this->pemotongan_piutang_model->locktables('hutang');

        $data = array(
				  'KdSupplier' => $v_supplier,
				  'TipeTransaksi' => $v_type,
				  'Tanggal' => $tgl,
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno
				);

        $this->db->update('hutang', $data, $where);

        $this->pemotongan_piutang_model->unlocktables();

    }
    
    function updateNewMutasiHutang($cnno, $tgl, $v_supplier, $grandtotal) 
    {
        $this->pemotongan_piutang_model->locktables('mutasi_hutang');

        $data = array(
        		  'Tanggal' => $tgl,
				  'KdSupplier' => $v_supplier,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'I'
				);
				
		$where=array(
		          'NoDokumen' => $cnno
					);

        $this->db->update('mutasi_hutang', $data, $where);

        $this->pemotongan_piutang_model->unlocktables();

    }
    
    function updateNewHutang2($cnno, $grandtotal) 
    {
        $this->pemotongan_piutang_model->locktables('hutang');

        $data = array(
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno
				);

        $this->db->update('hutang', $data, $where);

        $this->pemotongan_piutang_model->unlocktables();

    }
  
	function updateHeader($v_cnallocationno, $v_tgl_pemotongan_piutang, $cnno ,$v_status, $v_customer , $currencycode, $user) 
    {
    	
        $this->pemotongan_piutang_model->locktables('cnallocation');

        $data2 = array
				(
				  'cnadate' => $v_tgl_pemotongan_piutang,
				  'cnno' => $cnno,
				  'customerid' => $v_customer,
				  'currencycode' => $currencycode,
				  'editdate' => date('Y-m-d'),
				  'edituser' => $user,
				  'status' => $v_status
				);
				
		$where2=array(
		'cnallocationno'=>$v_cnallocationno
					);

        $this->db->update('cnallocation', $data2, $where2);
        
        $this->pemotongan_piutang_model->unlocktables();
    }

	
	
	function delete_trans($cnallocationno) 
    {
	       
			$this->db->delete("cnallocation",array('cnallocationno'=>$cnallocationno));
			$this->db->delete("cnallocationdetail",array('cnallocationno'=>$cnallocationno));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $cnallocationno . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/pemotongan_piutang/');
    }

    function delete_detail() //delete detail add
    {
    	
        $dndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $user = $this->uri->segment(6);
        
        $data=array(
        			'dndid'=>$dndid,
        			'coano'=>$coano,
        			'adduser'=>$user
        			);
		
        $this->db->delete("debitnotedtl_temp",$data);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/pemotongan_piutang/add_new/');
    } 
    
    function delete_detail2() //delete detail add
    {
    	
        $cnadetailid = $this->uri->segment(4);
        $cnallocationno = $this->uri->segment(5);
        $user = $this->session->userdata('username');
        
        $data=array(
        			'cnadetailid'=>$cnadetailid,
        			'cnallocationno'=>$cnallocationno
        			);
		
        $this->db->delete("cnallocationdetail",$data);
        
        //update total yang ada di cnallocation
			$ambil_total = $this->pemotongan_piutang_model->ambilTotal($v_cna);
			
			$data_jml=array(
			'allocationtotal'=>$ambil_total->jml_allocation,
			'allocationidrtotal'=>$ambil_total->jml_allocationidr
			);
			$dimana=array('cnallocationno'=>$v_cnallocationno);
			
			$this->db->update('cnallocation', $data_jml,$dimana);
			
						
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>Dokumen ' . $cnallocationno . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/pemotongan_piutang/edit_pemotongan_piutang/' .$cnallocationno.'');
    }


	
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "I N V O I C E";
		$data["header"] 	= $this->pemotongan_piutang_model->getHeader($nodok);
		$data["detail"] 	= $this->pemotongan_piutang_model->getDetail_cetak($nodok);
		$data["hitungsi"] 	= $this->pemotongan_piutang_model->hitungSalesInvoice_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_si', $data);
	}
	
	
	function doPrint()
	{
		$this->load->helper('terbilang');
		$this->load->library('globallib');
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 120;
	    $total_spasi_header = 80;
	    $jml_detail  = 8;
	    $ourFileName = "sales-invoice.txt";
		
		$header 	= $this->pemotongan_piutang_model->getHeader($nodok);
		$detail 	= $this->pemotongan_piutang_model->getDetail_cetak($nodok);
		$hitungsi 	= $this->pemotongan_piutang_model->hitungSalesInvoice_cetak($nodok);
		
		/*echo "<pre>";
		print_r($data["hitungsi"]);
		echo "</pre>";
		die;*/
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
            $arr_data["detail_harga"][$counter] = $val["Harga1c"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "I N V O I C E";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->invoiceno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->invoiceno;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->sidate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->sidate));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->duedate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->duedate));$i++)
                {
                    $echo.=" ";
                }
               
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Pelanggan";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Pelanggan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                                
                $echo.="\r\n";    
            }
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 60;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 5;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Total";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            $harga = $arr_data["detail_harga"][$i_detail];
	            $total = $qty*$harga;
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 60;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,0,',','.');
                    $limit_spasi = 5;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=number_format($harga,0,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_harga));$i++)
                    {
                        $echo.=" ";
                    }
                    
					    if(strlen(number_format($total,0,',','.'))==1){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==2){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==3){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==4){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==5){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==6){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==7){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==8){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==9){
						$jarak_total=5;
						}				
					if(strlen(number_format($total,0,',','.'))==10){
                    $echo.=number_format($total,0,',','.');
					}else{
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($total,0,',','.'))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($total,0,',','.');
					}
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
			
			
            
			foreach($hitungsi as $val)
			{
				$tothal = $val['total'];
				$diskon = $val['diskon'];
				
				if($diskon ==0){
				$potongan_diskon = 0;
				}else{
				$potongan_diskon =($diskon/100)*$total;
				}
				
				$ppn=(10/100)*$tothal;
				
				$grand_total=($tothal-$potongan_diskon)+$ppn;
				
			}
			
											
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Subtotal";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Subtotal"));$i++)
						{
							$echo.=" ";
						}
						if(strlen(number_format($tothal,0,',','.'))==1){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==2){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==3){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==4){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==5){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==6){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==7){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==8){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==9){
						$jarak_tothal=5;
						}
						if(strlen(number_format($tothal,0,',','.'))==10){
						$echo.=number_format($tothal,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($tothal,0,',','.'))+$jarak_tothal));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($tothal,0,',','.');
						}
						
				
						$echo .= "\r\n";						
						
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Diskon";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Diskon"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($potongan_diskon,0,',','.'))==1){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==2){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==3){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==4){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==5){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==6){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==7){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==8){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==9){
						$jarak_diskon=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($potongan_diskon,0,',','.'))+$jarak_diskon));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($potongan_diskon,0,',','.');
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="PPN";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("PPN"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($ppn,0,',','.'))==1){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==2){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==3){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==4){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==5){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==6){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==7){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==8){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==9){
						$jarak_ppn=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($ppn,0,',','.'))+$jarak_ppn));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($ppn,0,',','.');
						
						
						
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Grand Total";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Grand Total"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==1){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total0,',','.'))==2){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==3){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==4){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==5){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==6){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==7){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==8){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==9){
						$jarak_grand_total=5;
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==10){
						$echo.=number_format($grand_total,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($grand_total,0,',','.'))+$jarak_grand_total));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($grand_total,0,',','.');
						}
						
						
						$echo .= "\r\n";						
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Terbilang: ".terbilang($grand_total,$style=4)." Rupiah";
						
						$echo .= "\r\n";
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Note : ".$header->note;
			
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			
		        $data = array(
		            'form_data' => "sales-invoice",
		            'noreferensi' => $header->invoiceno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        
		}

		$paths = "path/to/";
	    $name_text_file='sales-invoice-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function ajax_customer(){
		$type = $this->input->post('Type');
		if($type=='CS' || $type == 'DS'){
			$rows = $this->pemotongan_piutang_model->getCustomer();
		}else{
			$rows = $this->pemotongan_piutang_model->getTravel();
		}
		$select = '<select class="form-control-new" name="KdCustomer" id="KdCustomer" onchange="getDetail()">';
		foreach($rows as $row){
			$select .= "<option value='".$row['KdCustomer']."'>".$row['Nama']."</option>";
		}
		$select .= "</select>";
		
		echo $select;
		
	}
	
	function getDebitCreditNo()
	{
		
	 	$kdcustomer	= $this->input->post('KdCustomer');
	 	$key	= $this->pemotongan_piutang_model->getRequirement($kdcustomer);
		$supplierid = $key->supplierid;
		$data = $this->pemotongan_piutang_model->getSibling($kdcustomer);
		echo "<option value=''> -- Pilih -- </option>";
		for($a=0;$a<count($data);$a++)
		{
			echo "<option value='".$data[$a]['dnno']."'>".$data[$a]['dnno'].", Total ".$data[$a]['sisa_']."</option>";
			
		}
		
			
	}
	
	function ambilpiutang()
    {  
     $mylib = new globallib();     
     $customer = $this->input->post('customer');
     $tgl = $this->input->post('tanggal');
     
     $query = $this->pemotongan_piutang_model->getAmbilPiutang($customer,$mylib->ubah_tanggal($tgl));
     
      echo '
	 		<thead class="title_table">
							<tr>
								<th width="30"><center>No</center></th>
								<th width="120"><center>Invoice No</center></th> 
								<th><center>Supplier</center></th>
								<th width="150"><center>Jatuh Tempo</center></th>
							    <th width="150"><center>Nilai Transaksi</center></th>
								<th width="150"><center>Sisa Bayar</center></th>
							    <th width="150"><center>Bayar</center></th>
								<th width="50"><center>Delete</center></th>
							</tr>
						</thead>
						<tbody>
	 	  ';
	 if(empty($query)){
		echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
		}else{
			 $no=1;
		     foreach ($query->result_array() as $cetak) {
			 echo "
			 		            <tr id='baris$no'>									
									<td align='center'>$no</td>
									<td align='center'>$cetak[NoFaktur]
									<input type='hidden' name='v_no_debit_note[]' id='v_no_debit_note$no' value='$cetak[NoFaktur]'/></td>
									<td>$cetak[Nama]</td>
									<td align='center'>$cetak[JatuhTempo_]
									<input type='hidden' name='v_jthtempo[]' id='v_jthtempo$no' value='$cetak[JatuhTempo]'/>
									</td>
									<td align='right'>$cetak[NilaiTransaksi1]
									<input type='hidden' name='v_nilaitransaksi[]' id='v_nilaitransaksi$no' value='$cetak[NilaiTransaksi2]'/>
									</td>
									<td align='right'>$cetak[sisa1]
									<input type='hidden' name='v_sisa[]' id='v_sisa$no' value='$cetak[sisa2]'/></td>
									<td><input type='text' class='form-control-new' name='v_bayar[]' id='v_bayar$no' value='0'  onblur='calculate(this)' style='text-align: right; width: 100%;' /></td>
									<td align='center'>
					                	<button type='button' class='btn btn-info btn-sm sm-new tooltip-primary'  data-toggle='tooltip' data-placement='top' data-original-title='Delete' title='' title='' name='btn_del_detail_$no' id='btn_del_detail_$no' onclick='deleteRow(this)' >
											<i class='entypo-trash'></i>
										</button>
					                </td>
								</tr>
			 	  ";
				$no++;
		      }
		    echo "<tr style='display:none'><td colspan='100%' align='center' >
		              <input style='display:none' name='jmlh' id='jmlh' value='0' /></td></tr>
		              <tr style='display:none'  colspan='100%' align='center' id='amounts'></tr> ";    
		}   
    }

}

?>