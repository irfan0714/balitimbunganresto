<?php
// create Febri
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class purchase_request extends authcontroller {
    function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/pembelian/purchase_request_model', 'pr');
    }

    function index()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y")
        {
        	$id = $this->uri->segment(4);
			$user = $this->session->userdata('username');

			$data["search_keyword"] = "";
			$data["search_gudang"] = "";
			$data["search_divisi"] = "";
            $data["search_status"] = "";
			$resSearch = "";
			$arr_search["search"]= array();
			$id_search = "";
			if($id*1>0)
			{
				$resSearch = $this->globalmodel->getSearch($id,"purchase_request",$user);
				$arrSearch = explode("&",$resSearch->query_string);

				$id_search = $resSearch->id;

				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					$search_gudung = explode("=", $arrSearch[1]); // search gudang
					$arr_search["search"]["gudang"] = $search_gudung[1];
					$search_divisi = explode("=", $arrSearch[2]); // search divisi
					$arr_search["search"]["divisi"] = $search_divisi[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

					$data["search_keyword"] = $search_keyword[1];
					$data["search_gudang"] = $search_gudung[1];
					$data["search_divisi"] = $search_divisi[1];
                    $data["search_status"] = $search_status[1];
				}
			}

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if($id_search)
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/purchase_request/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/purchase_request/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}

            $data['mgudang'] = $this->pr->getGudang();
            $data['mdivisi'] = $this->pr->getDivisi();

            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->pr->num_tabel_row($arr_search["search"]);
            $data['data'] = $this->pr->getTabelList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/pembelian/purchase_request/tabellist', $data);
        }
        else
        {
            $this->load->view('denied');
        }
    }

    function otoritas()
    {
        $this->load->view('transaksi/pembelian/purchase_request/form_otoritas');
    }

    function search()
    {
		$mylib = new globallib();
		$user = $this->session->userdata('username');

		// hapus dulu yah
		$this->db->delete('ci_query', array('module' => 'purchase_request','AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_divisi=".$this->input->post('search_divisi');
		$search_value .= "&search_status=".$this->input->post('search_status');

		$data = array(
            'query_string' => $search_value,
            'module' => "purchase_request",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

		$query_id = $this->db->insert_id();

        redirect('/transaksi/purchase_request/index/'.$query_id.'');

	}

    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            $data['mgudang'] 	= $this->pr->getGudang();
            $data['mdivisi'] 	= $this->pr->getDivisi();
            $data['mpb'] 		= $this->pr->getNopb();
            $data['msatuan'] 	= $this->pr->getSatuan();
            $data['track'] 		= $mylib->print_track();

	    	$this->load->view('transaksi/pembelian/purchase_request/form_add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function add_new_ml()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            $data['mgudang'] 	= $this->pr->getGudang();
            $data['mdivisi'] 	= $this->pr->getDivisi();
            $data['mpb'] 		= $this->pr->getNopb();
            $data['msatuan'] 	= $this->pr->getSatuan();
            $data['track'] 		= $mylib->print_track();

	    	$this->load->view('transaksi/pembelian/purchase_request/form_add_ml',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function add_new_non_pb()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            $data['mgudang'] 	= $this->pr->getGudang();
            $data['mdivisi'] 	= $this->pr->getDivisi();
            $data['mpb'] 		= $this->pr->getNopb();
            $data['msatuan'] 	= $this->pr->getSatuan();
            $data['track'] 		= $mylib->print_track();

	    	$this->load->view('transaksi/pembelian/purchase_request/add_pr',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

    function edit_form($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->pr->getHeader($id);
            //$data['detail'] = $this->pr->getDetail($id);
            $data['mgudang'] = $this->pr->getGudang();
            $data['mdivisi'] = $this->pr->getDivisi();
            $data['msatuan'] = $this->pr->getSatuan();
            $data["detailpb"] = $this->pr->getDetailPb($data['header']->NoPermintaan);

	        $data["detailunion"] = $this->pr->getDetailUnion($id,$data['header']->NoPermintaan);

            $data["ceknodok"] = $this->pr->cekNodok($id);
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/pembelian/purchase_request/form_edit', $data);

        }
        else
        {
            $this->load->view('denied');
        }
    }

    function edit_form_ml($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->pr->getHeader($id);
            //$data['detail'] = $this->pr->getDetail($id);
            $data['mgudang'] = $this->pr->getGudang();
            $data['mdivisi'] = $this->pr->getDivisi();
            $data['msatuan'] = $this->pr->getSatuan();
            $data["detailpb"] = $this->pr->getDetailML($data['header']->NoPermintaan);

	        $data["detailunion"] = $this->pr->getDetailUnionML($id,$data['header']->NoPermintaan);

            $data["ceknodok"] = $this->pr->cekNodok($id);
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/pembelian/purchase_request/form_edit_ml', $data);

        }
        else
        {
            $this->load->view('denied');
        }
    }

    function pop_edit_form($id,$type)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->pr->getHeader($id);
            //$data['detail'] = $this->pr->getDetail($id);
            $data['mgudang'] = $this->pr->getGudang();
            $data['mdivisi'] = $this->pr->getDivisi();
            $data['msatuan'] = $this->pr->getSatuan();
            $data["detailpb"] = $this->pr->getDetailPb($data['header']->NoPermintaan);

	        $data["detailunion"] = $this->pr->getDetailUnion($id,$data['header']->NoPermintaan);
	        $data["type"] = $type;
            $data["ceknodok"] = $this->pr->cekNodok($id);
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/pembelian/purchase_request/pop_form_edit', $data);

        }
        else
        {
            $this->load->view('denied');
        }
    }

    function edit_pr_non_pb($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->pr->getHeader($id);
            $data['detail'] = $this->pr->getDetail($id);
            $data['mgudang'] = $this->pr->getGudang();
            $data['mdivisi'] = $this->pr->getDivisi();
            $data['msatuan'] = $this->pr->getSatuan();
            $data["detailpb"] = $this->pr->getDetailPb($data['header']->NoPermintaan);

	        $data["detailunion"] = $this->pr->getDetailUnion($id,$data['header']->NoPermintaan);

            $data["ceknodok"] = $this->pr->cekNodok($id);
            $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/pembelian/purchase_request/edit_pr', $data);

        }
        else
        {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();

        $v_no_pb = $this->input->post('v_no_pb');
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_est_terima = $this->input->post('v_est_terima');
        $v_tgl_butuh = $this->input->post('pb_tgl_butuh');
        $v_gudang = $this->input->post('v_gudang');
        $v_keterangan = $this->input->post('v_keterangan');
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');

        $data["row"] = $this->pr->getDetailPb($v_no_pb);

        $v_divisi = $data["row"]->KdDivisi;

        // detail pcode pb
		$pb_pcode1 = $this->input->post('pb_pcode');
		$pb_namabarang1 = $this->input->post('pb_namabarang');
		$pb_qty1 = $this->input->post('pb_qty');
		$pb_satuan1 = $this->input->post('pb_satuan');

		$v_qty1 = $this->input->post('v_qty');

        //$data['bulan'] = $this->session->userdata('bulanaktif');
        //$data['tahun'] = $this->session->userdata('tahunaktif');

        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));

		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;

		if($flag=="add")
        {
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_pr_header","NoDokumen", "PR", $data['bulan'], $data['tahun']);

            $this->insertNewHeader($v_no_dokumen, $v_no_pb, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

       		$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       	}
        else if($flag=="edit")
        {
        	$v_no_dokumen = $this->input->post('v_no_dokumen');

            $this->updateHeader($v_no_dokumen, $v_no_pb, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }

    	// hapus detail jika ada
    	$this->db->delete('trans_pr_detail', array('NoDokumen' => $v_no_dokumen));

        for($x=0;$x< count($pb_pcode1);$x++)
		{
			$pb_pcode 		= strtoupper(addslashes(trim($pb_pcode1[$x])));
			$pb_namabarang 	= $mylib->save_char($pb_namabarang1[$x]);
			$pb_qty 		= $mylib->save_int($pb_qty1[$x]);
			$pb_satuan 		= $pb_satuan1[$x];

			$v_qty 			= $mylib->save_int($v_qty1[$x]);

			if($pb_pcode!="")
			{
				    //cocokan konversi terlebih dahulu
					$konversi = $this->pr->getKonversi($pcode,$v_satuan);

					//jika konversi ada datanya
					if(!empty($konversi)){
						//jika Satuan_To sama dengan Satuan yang dipilih
						if($konversi->Satuan_To==$v_satuan){
							$QtyPcs = $v_qty;
						//jika tidak sama
						}else{
							$QtyPcs = $v_qty * $konversi->amount;
						}
					//jika tidak ada datanya konversi
					}else{
						$QtyPcs = $v_qty;
					}

            	if($v_qty*1>0)
            	{
					$this->insertDetail($flag,$v_no_dokumen,$pb_pcode,$pb_namabarang,$pb_qty,$QtyPcs,$pb_satuan,$v_qty);
				}
			}
		}

		$guddang = $this->pr->getGudang2($v_gudang);

		if($v_status=="1"){
			//kirim email reminder ke bagian Purchasing

			$to = "";
            $to_name = "";

            $sql = "
                	  a.`email_address` AS email,
					  a.`email_name` AS employee_name
					FROM
					  `function_email` a
					WHERE a.`func_name` = 'purchase_request' ;
            ";

            $arrData = $this->globalmodel->getQuery($sql);


            foreach($arrData as $val)
            {
                $to .= $val["email"].";";
                $to_name .= $val["employee_name"].";";
			}

			$mylib = new globallib();
        	//$to 	 = "wirawati@secretgarden.co.id;irf@vci.co.id;";
		    //$to_name = "Team Purchasing SGV";

			$url = $base_url."/index.php/transaksi/purchase_request/edit_form/".$v_no_dokumen;
			$url_doc = $base_url."/index.php/transaksi/purchase_request/doprint/".$v_no_dokumen;

        	$subject = "Notifikasi Purchase Request No. ".$v_no_dokumen." Telah Dibuat.";
            $author  = "SGV Auto Email";

            $body  = "Dear Team Purchasing SGV,<br><br>";

			$body .= "Purchase Request No. <b>".$v_no_dokumen."</b> Telah Dibuat. Dengan rincian sebagai berikut :<br><br>";
            $body .= '
						<table border="0" style="border-collapse:collapse;">
						<tr>
						<td width="150" align="left">No. PR</td>
						<td>:</td>
						<td align="left">'.$v_no_dokumen.'</td>
						</tr>

						<tr>
						<td width="150" align="left">No. PB</td>
						<td>:</td>
						<td align="left">'.$v_no_pb.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Tanggal PR</td>
						<td>:</td>
						<td align="left">'.$v_tgl_dokumen.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Tanggal Kebutuhan</td>
						<td>:</td>
						<td align="left">'.$v_est_terima.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Gudang</td>
						<td>:</td>
						<td align="left">'.$guddang->NamaGudang.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Keterangan</td>
						<td>:</td>
						<td align="left">'.$v_keterangan.'</td>
						</tr>

						</table>
						<br>
						<b>Detail Barang</b>
						<table border="1" style="border-collapse:collapse;">
						<tr style="background:#f7f7f7;">
						<td width="100" align="center">PCode</td>
						<td width="300" align="center">Nama Barang</td>
						<td width="80" align="center">Qty PR</td>
						<td width="80" align="center">Satuan</td>
						</tr>';

			$data_detil = $this->pr->getDetailUnion($v_no_dokumen,$v_no_pb);
			foreach($data_detil as $y){
			$body .='<tr>
						<td align="center">'.$y[PCode].'</td>
						<td>&nbsp;'.$y[NamaBarang].'</td>
						<td align="center">'.$y[Qty].'</td>
						<td align="center">'.$y[Satuan].'</td>
						</tr>';

			}
			$body .='</table>
						<br><br>
					 ';

			$body .= "Untuk Melihat Detail <a href='".$url."' target='_blank'>KLIK DISINI</a> Atau Copy Paste Link ini di broswer anda : ".$url."<br><br>";


            $body  .= "<br><br>Terima Kasih";
			$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		}

        redirect('/transaksi/purchase_request/edit_form/'.$v_no_dokumen.'');
    }


    function save_data_ml()
    {
        // echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();

        $v_no_pb = $this->input->post('v_no_pb');
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_est_terima = $this->input->post('v_est_terima');
        $v_tgl_butuh = $this->input->post('pb_tgl_butuh');
        $v_gudang = $this->input->post('v_gudang');
        $v_keterangan = $this->input->post('v_keterangan');
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');

        $data["row"] = $this->pr->getDetailML($v_no_pb);

        $v_divisi = $data["row"]->KdDivisi;

        // echo $v_divisi;
        //
        // die();

        // detail pcode pb
    $pb_pcode1 = $this->input->post('pb_pcode');
    $pb_namabarang1 = $this->input->post('pb_namabarang');
    $pb_qty1 = $this->input->post('pb_qty');
    $pb_satuan1 = $this->input->post('pb_satuan');

    $v_qty1 = $this->input->post('v_qty');

        //$data['bulan'] = $this->session->userdata('bulanaktif');
        //$data['tahun'] = $this->session->userdata('tahunaktif');

        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));

    $data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;

    if($flag=="add")
        {
          $v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_pr_header","NoDokumen", "PR", $data['bulan'], $data['tahun']);

            $this->insertNewHeader($v_no_dokumen, $v_no_pb, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

          $this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }
        else if($flag=="edit")
        {
          $v_no_dokumen = $this->input->post('v_no_dokumen');

            $this->updateHeader($v_no_dokumen, $v_no_pb, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

          $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }

      // hapus detail jika ada
      $this->db->delete('trans_pr_detail', array('NoDokumen' => $v_no_dokumen));

        for($x=0;$x< count($pb_pcode1);$x++)
    {
      $pb_pcode 		= strtoupper(addslashes(trim($pb_pcode1[$x])));
      $pb_namabarang 	= $mylib->save_char($pb_namabarang1[$x]);
      $pb_qty 		= $mylib->save_int($pb_qty1[$x]);
      $pb_satuan 		= $pb_satuan1[$x];

      $v_qty 			= $mylib->save_int($v_qty1[$x]);

      if($pb_pcode!="")
      {
            //cocokan konversi terlebih dahulu
          $konversi = $this->pr->getKonversi($pcode,$v_satuan);

          //jika konversi ada datanya
          if(!empty($konversi)){
            //jika Satuan_To sama dengan Satuan yang dipilih
            if($konversi->Satuan_To==$v_satuan){
              $QtyPcs = $v_qty;
            //jika tidak sama
            }else{
              $QtyPcs = $v_qty * $konversi->amount;
            }
          //jika tidak ada datanya konversi
          }else{
            $QtyPcs = $v_qty;
          }

              if($v_qty*1>0)
              {
          $this->insertDetail($flag,$v_no_dokumen,$pb_pcode,$pb_namabarang,$pb_qty,$QtyPcs,$pb_satuan,$v_qty);
        }
      }
    }

    $guddang = $this->pr->getGudang2($v_gudang);

    if($v_status=="1"){
      //kirim email reminder ke bagian Purchasing

      $to = "";
            $to_name = "";

            $sql = "
                    a.`email_address` AS email,
            a.`email_name` AS employee_name
          FROM
            `function_email` a
          WHERE a.`func_name` = 'purchase_request' ;
            ";

            $arrData = $this->globalmodel->getQuery($sql);


            foreach($arrData as $val)
            {
                $to .= $val["email"].";";
                $to_name .= $val["employee_name"].";";
      }

      $mylib = new globallib();
          //$to 	 = "wirawati@secretgarden.co.id;irf@vci.co.id;";
        //$to_name = "Team Purchasing SGV";

      $url = $base_url."/index.php/transaksi/purchase_request/edit_form_ml/".$v_no_dokumen;
      $url_doc = $base_url."/index.php/transaksi/purchase_request/doprint/".$v_no_dokumen;

          $subject = "Notifikasi Purchase Request No. ".$v_no_dokumen." Telah Dibuat.";
            $author  = "SGV Auto Email";

            $body  = "Dear Team Purchasing SGV,<br><br>";

      $body .= "Purchase Request No. <b>".$v_no_dokumen."</b> Telah Dibuat. Dengan rincian sebagai berikut :<br><br>";
            $body .= '
            <table border="0" style="border-collapse:collapse;">
            <tr>
            <td width="150" align="left">No. PR</td>
            <td>:</td>
            <td align="left">'.$v_no_dokumen.'</td>
            </tr>

            <tr>
            <td width="150" align="left">No. PB</td>
            <td>:</td>
            <td align="left">'.$v_no_pb.'</td>
            </tr>

            <tr>
            <td width="150" align="left">Tanggal PR</td>
            <td>:</td>
            <td align="left">'.$v_tgl_dokumen.'</td>
            </tr>

            <tr>
            <td width="150" align="left">Tanggal Kebutuhan</td>
            <td>:</td>
            <td align="left">'.$v_est_terima.'</td>
            </tr>

            <tr>
            <td width="150" align="left">Gudang</td>
            <td>:</td>
            <td align="left">'.$guddang->NamaGudang.'</td>
            </tr>

            <tr>
            <td width="150" align="left">Keterangan</td>
            <td>:</td>
            <td align="left">'.$v_keterangan.'</td>
            </tr>

            </table>
            <br>
            <b>Detail Barang</b>
            <table border="1" style="border-collapse:collapse;">
            <tr style="background:#f7f7f7;">
            <td width="100" align="center">PCode</td>
            <td width="300" align="center">Nama Barang</td>
            <td width="80" align="center">Qty PR</td>
            <td width="80" align="center">Satuan</td>
            </tr>';

      $data_detil = $this->pr->getDetailUnion($v_no_dokumen,$v_no_pb);
      foreach($data_detil as $y){
      $body .='<tr>
            <td align="center">'.$y[PCode].'</td>
            <td>&nbsp;'.$y[NamaBarang].'</td>
            <td align="center">'.$y[Qty].'</td>
            <td align="center">'.$y[Satuan].'</td>
            </tr>';

      }
      $body .='</table>
            <br><br>
           ';

      $body .= "Untuk Melihat Detail <a href='".$url."' target='_blank'>KLIK DISINI</a> Atau Copy Paste Link ini di broswer anda : ".$url."<br><br>";


            $body  .= "<br><br>Terima Kasih";
      $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
    }

        redirect('/transaksi/purchase_request/edit_form_ml/'.$v_no_dokumen.'');
    }


    function save_data_non_pb()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();

        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_est_terima = $this->input->post('v_est_terima');
        $v_divisi = $this->input->post('v_divisi');
        $v_gudang = $this->input->post('v_gudang');
        $v_keterangan = $this->input->post('v_keterangan');
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');

        // detail pcode pr
		$pb_pcode1 = $this->input->post('pcode');
		$pb_namabarang1 = $this->input->post('v_namabarang');
		$pb_qty1 = $this->input->post('v_qty');
		$pb_satuan1 = $this->input->post('v_satuan');

		//$data['bulan'] = $this->session->userdata('bulanaktif');
        //$data['tahun'] = $this->session->userdata('tahunaktif');

        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');

		if($flag=="add")
        {
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_pr_header","NoDokumen", "PR", $data['bulan'], $data['tahun']);

            $this->insertNewHeader($v_no_dokumen,"Non PB", $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

       		$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       	}
        else if($flag=="edit")
        {
        	$v_no_dokumen = $this->input->post('v_no_dokumen');

            $this->updateHeader($v_no_dokumen, "Non PB", $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }

    	 //hapus detail jika ada
    	 //$this->db->delete('trans_pr_detail', array('NoDokumen' => $v_no_dokumen));

        for($x=0;$x< count($pb_pcode1);$x++)
		{
			$pb_pcode 		= strtoupper(addslashes(trim($pb_pcode1[$x])));
			$pb_namabarang 	= $mylib->save_char($pb_namabarang1[$x]);
			$pb_qty 		= $mylib->save_int($pb_qty1[$x]);
			$pb_satuan 		= $pb_satuan1[$x];

			if($pb_pcode!="")
			{
				    //cocokan konversi terlebih dahulu
					$konversi = $this->pr->getKonversi($pcode,$v_satuan);

					//jika konversi ada datanya
					if(!empty($konversi)){
						//jika Satuan_To sama dengan Satuan yang dipilih
						if($konversi->Satuan_To==$v_satuan){
							$QtyPcs = $pb_qty;
						//jika tidak sama
						}else{
							$QtyPcs = $pb_qty * $konversi->amount;
						}
					//jika tidak ada datanya konversi
					}else{
						$QtyPcs = $pb_qty;
					}

            	if($pb_qty*1>0)
            	{

					$this->insertDetail($flag,$v_no_dokumen,$pb_pcode,$pb_namabarang,"",$QtyPcs,$pb_satuan,$pb_qty);
				}
			}
		}

			$guddang = $this->pr->getGudang2($v_gudang);

		if($v_status=="1"){
			//kirim email reminder ke bagian Purchasing

			$to = "";
            $to_name = "";

            $sql = "
                	  a.`email_address` AS email,
					  a.`email_name` AS employee_name
					FROM
					  `function_email` a
					WHERE a.`func_name` = 'purchase_request' ;
            ";

            $arrData = $this->globalmodel->getQuery($sql);


            foreach($arrData as $val)
            {
                $to .= $val["email"].";";
                $to_name .= $val["employee_name"].";";
			}


			$mylib = new globallib();
        	//$to 	 = "wirawati@secretgarden.co.id;irf@vci.co.id;";
		    //$to_name = "Team Purchasing SGV";

			$url = $base_url."/index.php/transaksi/purchase_request/edit_form/".$v_no_dokumen;
			$url_doc = $base_url."/index.php/transaksi/purchase_request/doprint/".$v_no_dokumen;

        	$subject = "Notifikasi Purchase Request No. ".$v_no_dokumen." Telah Dibuat.";
            $author  = "SGV Auto Email";

            $body  = "Dear Team Purchasing SGV,<br><br>";

			$body .= "Purchase Request No. <b>".$v_no_dokumen."</b> Telah Dibuat. Dengan rincian sebagai berikut :<br><br>";
            $body .= '
						<table border="0" style="border-collapse:collapse;">
						<tr>
						<td width="150" align="left">No. PR</td>
						<td>:</td>
						<td align="left">'.$v_no_dokumen.'</td>
						</tr>

						<tr>
						<td width="150" align="left">No. PB</td>
						<td>:</td>
						<td align="left">Non PB</td>
						</tr>

						<tr>
						<td width="150" align="left">Tanggal PR</td>
						<td>:</td>
						<td align="left">'.$v_tgl_dokumen.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Tanggal Kebutuhan</td>
						<td>:</td>
						<td align="left">'.$v_est_terima.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Gudang</td>
						<td>:</td>
						<td align="left">'.$guddang->NamaGudang.'</td>
						</tr>

						<tr>
						<td width="150" align="left">Keterangan</td>
						<td>:</td>
						<td align="left">'.$v_keterangan.'</td>
						</tr>

						</table>
						<br>
						<b>Detail Barang</b>
						<table border="1" style="border-collapse:collapse;">
						<tr style="background:#f7f7f7;">
						<td width="100" align="center">PCode</td>
						<td width="300" align="center">Nama Barang</td>
						<td width="80" align="center">Qty PR</td>
						<td width="80" align="center">Satuan</td>
						</tr>';
			$data_detil = $this->pr->getDetailUnion($v_no_dokumen,"Non PB");
			foreach($data_detil as $y){
			$body .='<tr>
						<td align="center">'.$y[PCode].'</td>
						<td>&nbsp;'.$y[NamaBarang].'</td>
						<td align="center">'.$y[Qty].'</td>
						<td align="center">'.$y[Satuan].'</td>
						</tr>';

			}
			$body .='</table>
						<br><br>
					 ';

			$body .= "Untuk Melihat Detail <a href='".$url."' target='_blank'>KLIK DISINI</a> Atau Copy Paste Link ini di broswer anda : ".$url."<br><br>";


            $body  .= "<br><br>Terima Kasih";
			$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		}

        redirect('/transaksi/purchase_request/edit_pr_non_pb/'.$v_no_dokumen.'');
    }

    function insertNewHeader($no_dokumen, $no_pb, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $status, $flag, $base_url, $user)
    {
        $this->pr->locktables('trans_pr_header');

        $data = array(
            'NoDokumen' => $no_dokumen,
            'NoPermintaan' => $no_pb,
            'TglDokumen' => $tgl_dokumen,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('trans_pr_header', $data);

        $this->pr->unlocktables();

        // return $no;
    }

    function updateHeader($no_dokumen, $no_pb, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $status, $flag, $base_url, $user)
    {
        $this->pr->locktables('trans_pr_header');

        $data = array(
            'NoPermintaan' => $no_pb,
            'TglDokumen' => $tgl_dokumen,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'Status' => $status,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->update('trans_pr_header', $data, array('NoDokumen' => $no_dokumen));

        $this->pr->unlocktables();

        //return $counter;
    }

    function insertDetail($flag,$no_dokumen,$pb_pcode,$pb_namabarang,$pb_qty,$QtyPcs,$pb_satuan,$qty)
    {
        $this->pr->locktables('trans_pr_detail');

        if ($pb_pcode)
        {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'PCode' => $pb_pcode,
                'NamaBarang' => $pb_namabarang,
                'QtyPermintaan' => $pb_qty,
                'Qty' => $qty,
                'QtyPcs'=>$QtyPcs,
                'Satuan' => $pb_satuan,
            );

            $this->db->insert('trans_pr_detail', $data);
        }

        $this->pr->unlocktables();
    }

	function delete_trans($id)
	{
		$data["cekdata"] = $this->pr->cekNodok($id);

		if ($data["cekdata"]->NoDokumen != "")
		{
			$this->session->set_flashdata('msg', array('message' => 'Proses hapus data gagal, karena sudah dibuat PO','class' => 'danger'));
		}
		else
		{
			$this->db->delete('trans_pr_header', array('NoDokumen' => $id));
			$this->db->delete('trans_pr_detail', array('NoDokumen' => $id));
			$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen '.$id.'</strong> berhasil','class' => 'success'));
		}

		redirect('/transaksi/purchase_request/');
	}

	function delete_detail()
	{
		$sid = $this->uri->segment(4);
		$pcode = $this->uri->segment(5);
		$nodok = $this->uri->segment(6);

		$this->db->delete('trans_pr_detail', array('sid' => $sid,'PCode' => $pcode,'NoDokumen' => $nodok));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode '.$pcode.'</strong> berhasil','class' => 'success'));

		redirect('/transaksi/purchase_request/edit_form/'.$nodok.'');
	}

	function delete_detail_non_pb()
	{
		$sid = $this->uri->segment(4);
		$pcode = $this->uri->segment(5);
		$nodok = $this->uri->segment(6);

		$this->db->delete('trans_pr_detail', array('sid' => $sid,'PCode' => $pcode,'NoDokumen' => $nodok));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode '.$pcode.'</strong> berhasil','class' => 'success'));

		redirect('/transaksi/purchase_request/edit_pr_non_pb/'.$nodok.'');
	}

	function getAjax()
	{
		 $mylib	= new globallib();
		 $ajax	= $this->input->post('ajax');

		 if($ajax=="search_keyword_pb")
		 {
		 	$v_keyword 		= $mylib->save_char($this->input->post('v_keyword'));
	        $data['mpb']	= $this->pr->getNopb($v_keyword);

			?>
	    	<select name="v_no_pb" id="v_no_pb" class="form-control-new" style="width: 200px;" onchange="CallAjax('ajax_nopb', this.value)">
	    		<option value="">Pilih No PB</option>
	    		<?php
	    		foreach($data['mpb'] as $val)
	    		{
					?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
				}
	    		?>
	    	</select>
	    	<?php
		 }
     else if($ajax=="ajax_ml")
		 {
		 	$v_keyword 		= $mylib->save_char($this->input->post('v_keyword'));
	        $data['mpb']	= $this->pr->getNoML($v_keyword);

			?>
	    	<select name="v_no_pb" id="v_no_pb" class="form-control-new" style="width: 200px;" onchange="CallAjax('ajax_noml', this.value)">
	    		<option value="">Pilih No ML</option>
	    		<?php
	    		foreach($data['mpb'] as $val)
	    		{
					?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
				}
	    		?>
	    	</select>
	    	<?php
		 }

     else if($ajax=="ajax_noml")
		 {
		 	    $v_nopb	= $mylib->save_char($this->input->post('v_nopb'));
	        $row	= $this->pr->getDetailML($v_nopb);
	        $result	= $this->pr->getDetailPCodeML($v_nopb);

	        if($v_nopb)
	        {
	        	echo $row->AddUser." :: ".$row->Tanggal;
	        	echo "||";
	        	echo $row->TglTerima;
	        	echo "||";
	        	echo $row->NamaDivisi;
	        	echo "||";
	        	echo $row->nama_gudang;
	        	echo "||";
	        	echo $row->Keterangan;
	        	echo "||";
	        	?>
	        	<td colspan="100%" >
					<table class="table table-bordered responsive">
						<thead>
							<tr>
							    <th width="30"><center>No</center></th>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>
							    <th width="100"><center>Qty PB</center></th>
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Delete</center></th>
							</tr>
						</thead>
						<tbody>

						<?php
						$no=1;
						foreach($result as $val)
						{
							?>
							<tr id="baris<?php echo $no ?>">
								<td align="center">
									<center>
									<?php echo $no; ?>
									<input type="hidden" name="no[]" value="<?php echo $val["PCode"]; ?>" />
									<input type="hidden" name="pb_pcode[]" id="pb_pcode<?php echo $no; ?>" value="<?php echo $val["PCode"]; ?>" />
									<input type="hidden" name="pb_namabarang[]" id="pb_namabarang<?php echo $no; ?>" value="<?php echo $val["NamaBarang"]; ?>"/>
									<input type="hidden" name="pb_qty[]" id="pb_qty<?php echo $no; ?>" value="<?php echo $val["Qty"]; ?>"/>
									<input type="hidden" name="pb_satuan[]" id="pb_satuan<?php echo $no; ?>" value="<?php echo $val["Satuan"]; ?>"/>
									</center>
								</td>
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaBarang"]; ?></td>
								<td align="right"><?php echo $mylib->format_number($val["Qty"],2); ?></td>
								<td align="right"><input type="text" name="v_qty[]" id="v_qty<?php echo $no; ?>" onblur="toFormat2('v_qty<?php echo $no;?>')"  value="" class="form-control-new" size="10" style="text-align: right;" /></td>
								<td align="center"><?php echo $val["NamaSatuan"]; ?></td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
						    </tr>
							<?php
							$no++;
						}

						?>

						</tbody>
					</table>
	        	</td>
	        	<?php
			}
		 }

		 else if($ajax=="ajax_nopb")
		 {
		 	$v_nopb	= $mylib->save_char($this->input->post('v_nopb'));
	        $row	= $this->pr->getDetailPb($v_nopb);
	        $result	= $this->pr->getDetailPCodePb($v_nopb);

	        if($v_nopb)
	        {
	        	echo $row->AddUser." :: ".$row->Tanggal;
	        	echo "||";
	        	echo $row->TglTerima;
	        	echo "||";
	        	echo $row->NamaDivisi;
	        	echo "||";
	        	echo $row->nama_gudang;
	        	echo "||";
	        	echo $row->Keterangan;
	        	echo "||";
	        	?>
	        	<td colspan="100%" >
					<table class="table table-bordered responsive">
						<thead>
							<tr>
							    <th width="30"><center>No</center></th>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>
							    <th width="100"><center>Qty PB</center></th>
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Delete</center></th>
							</tr>
						</thead>
						<tbody>

						<?php
						$no=1;
						foreach($result as $val)
						{
							?>
							<tr id="baris<?php echo $no ?>">
								<td align="center">
									<center>
									<?php echo $no; ?>
									<input type="hidden" name="no[]" value="<?php echo $val["PCode"]; ?>" />
									<input type="hidden" name="pb_pcode[]" id="pb_pcode<?php echo $no; ?>" value="<?php echo $val["PCode"]; ?>" />
									<input type="hidden" name="pb_namabarang[]" id="pb_namabarang<?php echo $no; ?>" value="<?php echo $val["NamaBarang"]; ?>"/>
									<input type="hidden" name="pb_qty[]" id="pb_qty<?php echo $no; ?>" value="<?php echo $val["Qty"]; ?>"/>
									<input type="hidden" name="pb_satuan[]" id="pb_satuan<?php echo $no; ?>" value="<?php echo $val["Satuan"]; ?>"/>
									</center>
								</td>
								<td align="center"><?php echo $val["PCode"]; ?></td>
								<td><?php echo $val["NamaBarang"]; ?></td>
								<td align="right"><?php echo $mylib->format_number($val["Qty"],2); ?></td>
								<td align="right"><input type="text" name="v_qty[]" id="v_qty<?php echo $no; ?>" onblur="toFormat2('v_qty<?php echo $no;?>')"  value="" class="form-control-new" size="10" style="text-align: right;" /></td>
								<td align="center"><?php echo $val["NamaSatuan"]; ?></td>
				                <td align="center">
				                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_<?php echo $no;?>]" id="btn_del_detail_<?php echo $no;?>" value="Save" onclick='deleteRow(this)'>
										<i class="entypo-trash"></i>
									</button>
				                </td>
						    </tr>
							<?php
							$no++;
						}

						?>

						</tbody>
					</table>
	        	</td>
	        	<?php
			}
		 }
	}

	function viewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();

		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');


		$data["judul"]		= "PURCHASE REQUEST";

		$data["header"] 	= $this->pr->getHeader($nodok);
		$data["detail"] 	= $this->pr->getDetail($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
    $jenis = $data["header"]->NoPermintaan;

      if(substr($jenis,0,2)=="ML"){
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_pr_ml', $data);

      }else{
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_pr', $data);
        
      }
	}

	function doPrint()
	{
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();

		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";

		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();

		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();


		$total_spasi = 135;
	    $total_spasi_header = 80;


	    $jml_detail  = 8;
	    $ourFileName = "purchase-request.txt";

		$header = $this->pr->getHeader($nodok);
		$detail = $this->pr->getDetail($nodok);

		$note_header = substr($header->Keterangan,0,40);

		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,45);
            $arr_data["detail_qty_pb"][$counter] = $val["QtyPermintaan"];
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            $arr_data["detail_satuan"][$counter] = $val["Satuan"];

			 $counter++;
		}

        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);

        $nama_dokumen = "PURCHASE REQUEST";

        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n";
                $echo.="\r\n";
            }

            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n";
            }
            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo .= $arr_epson["cond"].$nama_dokumen;

            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."No : ".$header->NoDokumen;
            $echo.="\r\n";

            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Tanggal;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Tanggal));$i++)
                {
                    $echo.=" ";
                }

                $echo.="Divisi";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Divisi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                $echo.=$arr_epson["cond"].$header->NamaDivisi;

                $echo.="\r\n";
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."No PB";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("No PB"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->NoPermintaan;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->NoPermintaan));$i++)
                {
                    $echo.=" ";
                }

                $echo.="Gudang";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                $echo.=$arr_epson["cond"].$header->NamaGudang;

                $echo.="\r\n";
            }

            // baris 3
            {
                $echo.="Tanggal Terima";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal Terima"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglTerima;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglTerima));$i++)
                {
                    $echo.=" ";
                }


                $echo.="\r\n";
            }

            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 65;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty PB";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty PB"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }

            $echo .= $spasi_awal;
            $echo.="\r\n";

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }

            $echo.="\r\n";

            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;

            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty_pb = $arr_data["detail_qty_pb"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];

	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 65;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty_pb,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($qty_pb,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty_pb,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }

				}
				$echo.="\r\n";
				$no++;

            }

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";

            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->Keterangan;

            $echo .= "\r\n";
            $echo .= "\r\n";

            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.="Dibuat Oleh";

            $limit_spasi = 27;
            for($i=0;$i<($limit_spasi-strlen("Dibuat Oleh"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Hormat Kami";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Hormat Kami"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Diketahui Oleh";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Diketahui Oleh"));$i++)
            {
                $echo.=" ";

            }

            $echo.="Diterima Oleh";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Diterima Oleh"));$i++)
            {
                $echo.=" ";
            }

            $limit_enter = 3;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }

            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=" (  $user  )         (               )             (                 )         (                )";

            $echo .= "\r\n";
            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"purchase-request");

			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);

			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);

			}

            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"purchase-request");

			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "purchase-request",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='purchase-request-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);

		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;

	}
}

?>
