<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Purchase_order extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/delivery_order_model');
        $this->load->model('transaksi/purchase_order_model');
        
    }

    function GetHistoryBeli(){
    $result = $this->globalmodel->GetHistoryBeli();
    echo json_encode($result);
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
			
            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_supplier"] = "";
            $data["search_status"] = "";
            $data["search_tgl_awal"] = "01-".date("m-Y");
            $data["search_tgl_akhir"] = date("t-m-Y");
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "purchase_order", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_supplier = explode("=", $arrSearch[2]); // search supplier
                    $arr_search["search"]["supplier"] = $search_supplier[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];
                    $search_tgl_awal= explode("=", $arrSearch[4]); // search tgl_awal
                    $arr_search["search"]["tgl_awal"] = $search_tgl_awal[1];
                    $search_tgl_akhir= explode("=", $arrSearch[5]); // search tgl_ahkir
                    $arr_search["search"]["tgl_akhir"] = $search_tgl_akhir[1];
                    $search_total= explode("=", $arrSearch[6]); // search total
                    $arr_search["search"]["total"] = $search_total[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_supplier"] = $search_supplier[1];
                    $data["search_status"] = $search_status[1];
                    $data["search_tgl_awal"] = $mylib->ubah_tanggal($search_tgl_awal[1]);
                    $data["search_tgl_akhir"] = $mylib->ubah_tanggal($search_tgl_akhir[1]);
                    $data["search_total"] = $search_total[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '50';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/purchase_order/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/purchase_order/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->purchase_order_model->getGudang();
			$data['supplier'] = $this->purchase_order_model->getSupplier();

            $config['total_rows'] = $this->purchase_order_model->num_purchase_order_row($arr_search["search"]);
            $data['data'] = $this->purchase_order_model->getPurchaseOrderList($config['per_page'], $page, $arr_search["search"]);
			$data['pending'] = $this->purchase_order_model->getPending();
		    $data['approved'] = $this->purchase_order_model->getApproved();
		    $data['void'] = $this->purchase_order_model->getVoid();
		    $data['waiting'] = $this->purchase_order_model->getWaiting();
		    $data['rejected'] = $this->purchase_order_model->getReject();
		    $data['request'] = $this->purchase_order_model->getRequest();
		    $data['po5'] = $this->purchase_order_model->getPO5();
		    $data['po510'] = $this->purchase_order_model->getPO510();
		    $data['po10'] = $this->purchase_order_model->getPO10();
		    
		    
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/purchase_order/purchase_order_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	// echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'purchase_order', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_supplier=".$this->input->post('search_supplier');
		$search_value .= "&search_status=".$this->input->post('search_status');
		$search_value .= "&search_tgl_awal=".$mylib->ubah_tanggal($this->input->post('v_tgl_mulai'));
		$search_value .= "&search_tgl_akhir=".$mylib->ubah_tanggal($this->input->post('v_tgl_akhir'));
		$search_value .= "&search_total=".$this->input->post('search_total');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "purchase_order",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/purchase_order/index/' . $query_id . '/');
    }

    function add_new() 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			$data['gudang'] = $this->purchase_order_model->getGudang();
            $data['supplier'] = $this->purchase_order_model->getSupplier();
            $data['currency'] = $this->purchase_order_model->getCurrency();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/purchase_order/add_purchase_order', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function get_konversi_new()
    {
        $pcode = $this->input->post('pcode');
        $satuan_from = $this->input->post('satuan_from');
        $satuan_to = $this->input->post('satuan_to');
        $qty = $this->input->post('qty');

        if($satuan_from == $satuan_to){
            $nilaiKonfersi = $qty;
        }else{
            $arr_konversi = $this->purchase_order_model->getKonversi_new($pcode,$satuan_to,$satuan_from);

            if(!empty($arr_konversi)){
                $nilaiKonfersi = $qty / $arr_konversi->amount;
            }else{
                $nilaiKonfersi = $qty;
            }
            
        }

		echo $nilaiKonfersi;
        /*
        //titik
		$konversi = $this->purchase_order_model->getKonversi($pcode,$v_satuan); //print_r($konversi);
            //jika konversi ada datanya 
        if(!empty($konversi)){
            //jika Satuan_To sama dengan Satuan yang dipilih 
            if($konversi->Satuan_From==$satuan_pr){
                $v_QtyPcs = $qty;
                //jika tidak sama
            }else{
                $v_QtyPcs = $qty * $konversi->amount;
            }
        //jika tidak ada datanya konversi
        }else{
            $v_QtyPcs = $qty;
        }

        echo $v_QtyPcs;
        */

    }
    function edit_purchase_order($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->purchase_order_model->getHeader($id);
            $data['gudang'] = $this->purchase_order_model->getGudang();			
            $data['supplier'] = $this->purchase_order_model->getSupplier();
            $data['currency'] = $this->purchase_order_model->getCurrency();
            $data['satuan'] = $this->purchase_order_model->getSatuan();
            $data["detail_list"] = $this->purchase_order_model->getDetailList($id);
            $data['username'] = $this->session->userdata('username');
            
            $tot = $data['header']->Total * 1;

            if($tot <=10000000) {
                $otorisasi = $this->purchase_order_model->cekOtorisasi('po_level_1');
                $cek_apprv = $this->purchase_order_model->cekApprovalPO($otorisasi->UserName,$id);  
                if(!empty($cek_apprv)){
                    $data['keterangan'] = $otorisasi->UserName;
                }
            }else {
                $oto = $this->purchase_order_model->cekOtorisasi('po_level_2');
                $data['keterangan'] = $oto->UserName;
            }
            
			$hak_otorisasi =$this->purchase_order_model->HakOtorisasi($data['username']);
			
			if(!empty($hak_otorisasi)){
				$data['hak_otorisasi']="Y";
				$data['level_otorisasi']=$hak_otorisasi[0]['Tipe'];
			}else{
				$data['hak_otorisasi']="T";
				$data['level_otorisasi']="";
			} 
			//cek apakah sudah punya RG
			$cek = $this->purchase_order_model->cekRG($id);
			if(!empty($cek)){
				$data['cekrg']="1";
				$data['norg']=$cek->NoDokumen;
			}else{
				$data['cekrg']="0";
			}

            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/purchase_order/edit_purchase_order', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_delivery_order($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->purchase_order_model->getHeader($id);
            $data['customer'] = $this->purchase_order_model->getCustomer();
            $data["detail_list"] = $this->purchase_order_model->getDetailList($id);
            $data['msatuan'] = $this->purchase_order_model->getSatuan();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/view_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
    	// echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_tgl_terima = $this->input->post('v_tgl_terima');
        $v_gudang = $this->input->post('v_gudang');
        $v_kurs = $this->input->post('kurs');
        $v_supplier = $this->input->post('v_supplier');
        $v_top = $this->input->post('v_top');
        $v_nopr = $this->input->post('v_nopr');
        $namadivisi = $this->input->post('v_namadivisi');
        $v_note = $this->input->post('v_note');
		$v_currencycode = $this->input->post('v_currencycode');
		$v_grdTotal = $this->input->post('grdTotal');
		$v_status = $this->input->post('v_status');
		
		$v_Jumlah = $this->input->post('v_Jumlah');
		$v_DiscHarga = $this->input->post('v_DiscHarga');
		$v_pot_disc = $this->input->post('v_pot_disc');
		$v_PPn = $this->input->post('v_PPn');
		$v_NilaiPPn = $this->input->post('v_NilaiPPn');
		$v_Total = $this->input->post('v_Total');
		
		$base_url= $this->input->post('base_url');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        //ambil post $v_no_dokumen
		$v_no_dokumen = $this->input->post('v_no_dokumen');
		
		$v_approve= $this->input->post('v_approve');
		$v_approve_edit= $this->input->post('v_approve_edit');
		$v_edit_after_reject= $this->input->post('v_edit_after_reject');
		$v_reject= $this->input->post('v_reject');
		$v_Approval_Remarks= $this->input->post('v_Approval_Remarks');
		$v_request_edit= $this->input->post('v_request_edit');
		$v_request_Remarks= $this->input->post('v_request_Remarks');
		
		
		list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
        
        // detail
        $v_pcode1 = $this->input->post('v_pcode');
        $v_nmbarang1 = $this->input->post('v_nmbarang');
        $v_qty_request1 = $this->input->post('v_qty_request');
        $v_satuan1 = $this->input->post('v_satuan');
        $v_Qty1 = $this->input->post('v_Qty');
        $v_Harga1 = $this->input->post('v_Harga');
        $v_Disc1 = $this->input->post('v_Disc');
        $v_Potongan1 = $this->input->post('v_Potongan');
        $v_subtotal1 = $this->input->post('v_subtotal');
        $v_sJumlah1 = $this->input->post('v_sJumlah');
       

        if ($flag == "add")
		{   //Header
		    //pertama generate No Dokument di trans_order_barang_header
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_order_barang_header","NoDokumen", "PO", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di trans_order_barang_header
            $this->insertNewHeader( $v_no_dokumen,
                                    $mylib->ubah_tanggal($v_tgl_dokumen),
                                    $v_nopr,
                                    $namadivisi, 
                                    $mylib->ubah_tanggal($v_tgl_terima), 
                                    $v_supplier,
                                    $v_note, 
                                    $v_gudang,
                                    $v_kurs,
                                    $v_top,
                                    $v_currencycode,
                                    $v_Jumlah,
                                    $v_DiscHarga,
                                    $v_PPn,
                                    $v_NilaiPPn,
                                    $v_Total,
                                    $user );
        
			
			//Detail
			for ($x = 0; $x < count($v_pcode1); $x++) 
			{
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_nmbarang = trim($v_nmbarang1[$x]);
            $v_Qty = $mylib->save_int($v_Qty1[$x]);
            $v_satuan = $v_satuan1[$x];
            $v_Harga = $mylib->save_int($v_Harga1[$x]);
			$v_Disc = $v_Disc1[$x];
			$v_Potongan = $v_Potongan1[$x];
			$v_subtotal = $v_subtotal1[$x];
			$v_sJumlah = $v_sJumlah1[$x];
			
			//cocokan konversi terlebih dahulu
			
			$konversi = $this->purchase_order_model->getKonversi($pcode,$v_satuan);
								
			//jika konversi ada datanya
			if(!empty($konversi)){
				//jika Satuan_To sama dengan Satuan yang dipilih
				if($konversi->Satuan_To==$v_satuan){
					$v_QtyPcs = $v_Qty;
					//jika tidak sama
				}else{
					$v_QtyPcs = $v_Qty * $konversi->amount;
				}
			//jika tidak ada datanya konversi
			}else{
				$v_QtyPcs = $v_Qty;
			}
			
		    if ($v_Qty != ""){
		   	    if($v_Qty*1>0){
					//insertDetail
                    
					$this->insertDetail($v_no_dokumen,
	                                    $pcode,
	                                    $v_Qty, 
	                                    $v_QtyPcs,
	                                    $v_satuan, 
	                                    $v_Harga,
	                                    $v_Disc,
	                                    $v_Potongan,
	                                    $v_subtotal,
	                                    $v_sJumlah);
                    
					
					//update juga yang ada di QtyPO PR
			        //kemudian kurang angka yang ada di QtyPO PR
                    
		            $qty_pox = $this->purchase_order_model->getQtyPoDetail($v_no_dokumen,$pcode);
		            $qty_po_prx = $this->purchase_order_model->getQtyPoPr($v_nopr, $pcode); 
		            
		            if($qty_po_prx->QtyPO==0){
						$qtynya = $v_QtyPcs;
					}else{
						$qtynya = $qty_po_prx->QtyPO + $qty_pox->QtyPcs;
					}
		               					
					//update ke detail pr
					$this->db->update('trans_pr_detail',
									   array('QtyPO'=>$qtynya),
									   array('NoDokumen'=>$v_nopr,'PCode'=>$pcode)
									   );
                    
	           }
            }
			
			}
				// die("test");		
		} 
		
		
		else if ($flag == "edit") 
		{
			   //update header
               $this->updateHeader( $v_no_dokumen,
                                    $mylib->ubah_tanggal($v_tgl_dokumen),
                                    $v_nopr,
                                    $divisi, 
                                    $mylib->ubah_tanggal($v_tgl_terima), 
                                    $v_supplier,
                                    $v_note, 
                                    $v_gudang,
                                    $v_kurs,
                                    $v_top,
                                    $v_currencycode,
                                    $v_Jumlah,
                                    $v_DiscHarga,
                                    $v_PPn,
                                    $v_NilaiPPn,
                                    $v_Total,
                                    $v_status,
                                    $user,
                                    $v_edit_after_reject);
            
				    //Detail
					for ($x = 0; $x < count($v_pcode1); $x++)  {
    		            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
    		            $v_nmbarang = trim($v_nmbarang1[$x]);
    		            $v_Qty = $mylib->save_int($v_Qty1[$x]);
    		            $v_satuan = $v_satuan1[$x];
    		            $v_Harga = $mylib->save_int($v_Harga1[$x]);
    					$v_Disc = $v_Disc1[$x];
    					$v_Potongan = $v_Potongan1[$x];
    					$v_subtotal = $v_subtotal1[$x];
    					$v_sJumlah = $v_sJumlah1[$x];
    					
    					//cocokan konversi terlebih dahulu
    					$konversi = $this->purchase_order_model->getKonversi($pcode,$v_satuan);
    											
    					//jika konversi ada datanya
    					if(!empty($konversi)){
    						//jika Satuan_To sama dengan Satuan yang dipilih
    						if($konversi->Satuan_To==$v_satuan){
    							$v_QtyPcs = $v_Qty;
    							//jika tidak sama
    						}else{
    							$v_QtyPcs = $v_Qty * $konversi->amount;
    						}
    					//jika tidak ada datanya konversi
    					}else{
    						$v_QtyPcs = $v_Qty;
    					}
    					
    				    if ($v_Qty != ""){
    							$this->updateDetail($v_no_dokumen,
    			                                    $pcode,
    			                                    $v_Qty, 
    			                                    $v_QtyPcs,
    			                                    $v_satuan, 
    			                                    $v_Harga,
    			                                    $v_Disc,
    			                                    $v_Potongan,
    			                                    $v_subtotal,
    			                                    $v_sJumlah);
    				           
    	                }
    			                
    			    }
					
					
           //update juga yang ada di QtyPO PR
           //kemudian kurang angka yang ada di QtyPO PR
            $qty_po = $this->purchase_order_model->getQtyPo($v_no_dokumen);
            
            foreach($qty_po AS $v){
				$PCode_PO = $v['PCode'];
				$QtyPcs_PO = $v['QtyPcs'];
				
				//ambil QtyPO di PR
				$qty_po_pr = $this->purchase_order_model->getQtyPoPr($v_nopr, $PCode_PO);
				if($qty_po_pr->QtyPO == $QtyPcs_PO){
					$updateQtyPoPr = $qty_po_pr->QtyPO;	
				}else if($qty_po_pr->QtyPO > $QtyPcs_PO){
					$selisih = $qty_po_pr->QtyPO - $QtyPcs_PO;
					$updateQtyPoPr = $qty_po_pr->QtyPO - $selisih;
				}else if($qty_po_pr->QtyPO < $QtyPcs_PO){
					$selisih = $QtyPcs_PO - $qty_po_pr->QtyPO;
					$updateQtyPoPr = $qty_po_pr->QtyPO + $selisih;
				}
				
				//update ke PR
				$this->db->update('trans_pr_detail',array('QtyPO'=>$updateQtyPoPr),array('NoDokumen'=>$v_nopr,'PCode'=>$PCode_PO));
             }
					           
					
			//kirim email jika status = 1
			if($v_status=="1"){
				
			}else if($v_status=="2"){
				//2 untuk void
				//pertama ubah status menjadi void
				    date_default_timezone_set("Asia/Jakarta");
				    $data=array( 'Status' => "2",
                                 'EditDate' => date('Y-m-d h:i:s'),
	                             'EditUser' => $user,
	                             'Approval_By' => $user,
	                             'Approval_Date' => date('Y-m-d h:i:s'));
                    $this->db->update('trans_order_barang_header',$data,array('NoDokumen'=>$v_no_dokumen));
                
                //kemudian kurang angka yang ada di QtyPO PR
                $qty_po = $this->purchase_order_model->getQtyPo($v_no_dokumen);
                
                foreach($qty_po AS $v){
					$PCode_PO = $v['PCode'];
					$QtyPcs_PO = $v['QtyPcs'];
					
					//ambil QtyPO di PR
					$qty_po_pr = $this->purchase_order_model->getQtyPoPr($v_nopr, $PCode_PO);
					$updateQtyPoPr = $qty_po_pr->QtyPO - $QtyPcs_PO;
					
					//update ke PR
					$this->db->update('trans_pr_detail',array('QtyPO'=>$updateQtyPoPr),array('NoDokumen'=>$v_nopr,'PCode'=>$PCode_PO));
					
				}
				
			}else if($v_status=="3"){
				//3 untuk unvoid
				//pertama ubah status menjadi void
				date_default_timezone_set("Asia/Jakarta");
				    $data=array( 'Status' => "0",
                                 'EditDate' => date('Y-m-d h:i:s'),
	                             'EditUser' => $user,
	                             'Approval_By' => '',
	                             'Approval_Date' => '');
                    $this->db->update('trans_order_barang_header',$data,array('NoDokumen'=>$v_no_dokumen));
                
                //kemudian kurang angka yang ada di QtyPO PR
                $qty_po = $this->purchase_order_model->getQtyPo($v_no_dokumen);
                
                foreach($qty_po AS $v){
					$PCode_PO = $v['PCode'];
					$QtyPcs_PO = $v['QtyPcs'];
					
					//ambil QtyPO di PR
					$qty_po_pr = $this->purchase_order_model->getQtyPoPr($v_nopr, $PCode_PO);
					$updateQtyPoPr = $qty_po_pr->QtyPO + $QtyPcs_PO;
					
					//update ke PR
					$this->db->update('trans_pr_detail',array('QtyPO'=>$updateQtyPoPr),array('NoDokumen'=>$v_nopr,'PCode'=>$PCode_PO));
					
				}
				
			}
			
			$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }
        
   		//proses approve
		if($v_approve=="1"){
			
		    date_default_timezone_set("Asia/Jakarta");
		    $remarks = "Done";
		    $data=array( 'Approval_By' => $user,
                         'Approval_Date' => date('Y-m-d h:i:s'),
                         'Approval_Status' => '1',
                         'Approval_Remarks'=>$remarks);
                         
            $this->db->update('trans_order_barang_header',$data,array('NoDokumen'=>$v_no_dokumen));
		}
		
		//proses reject
		if($v_reject=="1"){
		    //update header trans_order_barang_header
		    date_default_timezone_set("Asia/Jakarta");
		    $data=array( 'Approval_By' => $user,
                         'Approval_Date' => date('Y-m-d h:i:s'),
                         'Approval_Status' => '2',
                         'Approval_Remarks' => $v_Approval_Remarks);
                         
            $this->db->update('trans_order_barang_header',$data,array('NoDokumen'=>$v_no_dokumen));
		}
		
		//proses request edit
		if($v_request_edit=="1"){
		    //update header trans_order_barang_header
		    date_default_timezone_set("Asia/Jakarta");
		    $data=array(    'Status' => '4',    
                            'Request_By' => $user,
                            'Request_Date' => date('Y-m-d h:i:s'),
                            'Request_Status' => '1',
                            'Request_Remarks' => $v_request_Remarks);
                         
            $this->db->update('trans_order_barang_header',$data,array('NoDokumen'=>$v_no_dokumen));
		}
		
		//proses approve edit po
		if($v_approve_edit=="1"){
			    //update header trans_order_barang_header
			    	$data=array( 'Status' => '0',
								 'Approval_By'=>'',
								 'Approval_Status'=>0,
                                 'Request_Status' => '0');
                             
                $this->db->update('trans_order_barang_header',$data,array('NoDokumen'=>$v_no_dokumen));
		}

        
        redirect('/transaksi/purchase_order/edit_purchase_order/' . $v_no_dokumen . '');
    }

    function insertNewHeader( $v_no_dokumen,
                                    $v_tgl_dokumen,
                                    $v_nopr, 
                                    $namadivisi,
                                    $v_tgl_terima, 
                                    $v_supplier,
                                    $v_note, 
                                    $v_gudang,
                                    $v_kurs,
                                    $v_top,
                                    $v_currencycode,
                                    $v_Jumlah,
                                    $v_DiscHarga,
                                    $v_PPn,
                                    $v_NilaiPPn,
                                    $v_Total,
                                    $user ) 
    {  
        date_default_timezone_set("Asia/Jakarta");         
        $data = array(  'NoDokumen'		=> $v_no_dokumen,
				        'TglDokumen'	=> $v_tgl_dokumen,
				        'NoPr'			=> $v_nopr,
				        'TglTerima'		=> $v_tgl_terima,
				        'KdSupplier'	=> $v_supplier,
				        'Keterangan'	=> $v_note,
				        'KdGudang'		=> $v_gudang,
				        'TOP'			=> $v_top,
				        'currencycode'	=> $v_currencycode,
				        'kurs'			=> $v_kurs,
				        'Jumlah'		=> $v_Jumlah,
				        'DiscHarga'		=> $v_DiscHarga,
				        'PPn'			=> $v_PPn,
				        'NilaiPPn'		=> $v_NilaiPPn,
				        'Total'			=> $v_Total,
				        'Status'				=> 0,
				        'AddDate'		=> date('Y-m-d h:i:s'),
				        'AddUser'		=> $user,
				        'notes'		    => $namadivisi );
	
        $this->db->insert('trans_order_barang_header', $data);

    }
    
  
    function updateHeader( $v_no_dokumen,
                                    $v_tgl_dokumen,
                                    $v_nopr,
                                    $namadivisi, 
                                    $v_tgl_terima, 
                                    $v_supplier,
                                    $v_note, 
                                    $v_gudang,
                                    $v_kurs,
                                    $v_top,
                                    $v_currencycode,                                    
                                    $v_Jumlah,
                                    $v_DiscHarga,
                                    $v_PPn,
                                    $v_NilaiPPn,
                                    $v_Total,
                                    $v_status,
                                    $user,
                                    $v_edit_after_reject ) 
    {
    	date_default_timezone_set("Asia/Jakarta");
        $this->purchase_order_model->locktables('trans_order_barang_header');


		if($v_edit_after_reject=="1"){
			$data = array(  'NoDokumen'		=> $v_no_dokumen,
				        'TglDokumen'	=> $v_tgl_dokumen,
				        'NoPr'			=> $v_nopr,
				        'TglTerima'		=> $v_tgl_terima,
				        'KdSupplier'	=> $v_supplier,
				        'Keterangan'	=> $v_note,
				        'KdGudang'		=> $v_gudang,
				        'TOP'			=> $v_top,
				        'currencycode'	=> $v_currencycode,
				        'kurs'			=> $v_kurs,
				        'Jumlah'		=> $v_Jumlah,
				        'DiscHarga'		=> $v_DiscHarga,
				        'PPn'			=> $v_PPn,
				        'NilaiPPn'		=> $v_NilaiPPn,
				        'Total'			=> $v_Total,
				        'Status'		=> 1,
				        'Approval_By'	=>'',
				        'Approval_Date' =>'',
				        'Approval_Status'=>0,
				        'Approval_Remarks'=>'',
				        'EditDate'		=> date('Y-m-d h:i:s'),
				        'EditUser'		=> $user,
				        'notes'		=>$namadivisi );
		}else{
			$data = array(  'NoDokumen'		=> $v_no_dokumen,
				        'TglDokumen'	=> $v_tgl_dokumen,
				        'NoPr'			=> $v_nopr,
				        'TglTerima'		=> $v_tgl_terima,
				        'KdSupplier'	=> $v_supplier,
				        'Keterangan'	=> $v_note,
				        'KdGudang'		=> $v_gudang,
				        'TOP'			=> $v_top,
				        'currencycode'	=> $v_currencycode,
				        'kurs'			=> $v_kurs,
				        'Jumlah'		=> $v_Jumlah,
				        'DiscHarga'		=> $v_DiscHarga,
				        'PPn'			=> $v_PPn,
				        'NilaiPPn'		=> $v_NilaiPPn,
				        'Total'			=> $v_Total,
				        'Status'		=> $v_status,
				        'EditDate'		=> date('Y-m-d h:i:s'),
				        'EditUser'		=> $user );
		}
        
		
        $this->db->update('trans_order_barang_header', $data, array('NoDokumen' => $v_no_dokumen));
        
        $this->purchase_order_model->unlocktables();
    }

    function insertDetail( $v_no_dokumen,
			               $pcode,
			               $v_Qty,
			               $v_QtyPcs, 
			               $v_satuan, 
			               $v_Harga,
			               $v_Disc,
			               $v_Potongan,
			               $v_subtotal,
			               $v_sJumlah) 
    {
        $this->purchase_order_model->locktables('trans_order_barang_detail');

        if ($pcode) {
            $data = array(  'NoDokumen'		=> $v_no_dokumen,
					        'PCode'			=> $pcode,
					        'Qty'			=> $v_Qty,
					        'QtyPcs'		=> $v_QtyPcs,
					        'Satuan'		=> $v_satuan,
					        'Harga'			=> $v_Harga,
					        'Disc1'			=> $v_Disc,
					        'Potongan'		=> $v_Potongan,
					        'Jumlah'		=> $v_subtotal,
					        'Total'			=> $v_sJumlah );

            $this->db->insert('trans_order_barang_detail', $data);
        }

        $this->purchase_order_model->unlocktables();
    }
	
	function updateDetail($v_no_dokumen,
					      $pcode,
					      $v_Qty, 
					      $v_QtyPcs,
					      $v_satuan, 
					      $v_Harga,
					      $v_Disc,
					      $v_Potongan,
					      $v_subtotal,
					      $v_sJumlah)
    {
    	    	 
        $this->purchase_order_model->locktables('trans_order_barang_detail');

        if ($pcode) 
        {
            $data = array(  'Qty'			=> $v_Qty,
					        'QtyPcs'		=> $v_QtyPcs,
					        'Satuan'		=> $v_satuan,
					        'Harga'			=> $v_Harga,
					        'Disc1'			=> $v_Disc,
					        'Potongan'		=> $v_Potongan,
					        'Jumlah'		=> $v_subtotal,
					        'Total'			=> $v_sJumlah );
            
            $this->db->update('trans_order_barang_detail', $data, array('NoDokumen' => $v_no_dokumen,'PCode' => $pcode));
        } 
        
        $this->purchase_order_model->unlocktables();
    }
	
	    
    function satuan()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->purchase_order_model->getSatuanDetail($pcode);
     
     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }     
    }
    
    
    function save_detail_temp($nopr,$supplier) 
    {
		
		//kan sudah dapet no Purchase Request
			$user = $this->session->userdata('username');
			$detail_purchase_request = $this->purchase_order_model->getPRDetail($nopr);
			
		    ?>
			<thead class="title_table">
							<tr>
								<th width="30"><center>No</center></th>
								<th width="50"><center>PCode</center></th>
							    <th width="300"><center>Nama Barang</center></th>               
							    <th width="100"><center>Qty Request</center></th>
							    <th width="50"><center>Konvert Satuan</center></th>
							    <th width="30"><center>Qty<br><i></i></center></th><!-- (double klik copy Qty) -->
							    <th width="100"><center>Harga</center></th>
								<th width="100"><center>History</center></th>
							    <th width="150"><center>Disc (%)</center></th>
							    <th width="100"><center>Potongan (IDR)</center></th>
							    <th width="100"><center>Sub Total</center></th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden" name="grdTotal" id="grdTotal" value=""/>
						  <?php 
						  
						  if(empty($detail_purchase_request)){?>
						  	<tr>
							  	<td colspan="100%" align="center">Tidak Ada Data /  PR sudah DiGunakan di PO lain</td>
							</tr>
						  <?php }else{
						  $Sid=1;
						  //foreach($detail_list as $val)
						  foreach($detail_purchase_request as $val)
						  {?>
							 <script>hitungSubtotal(<?php echo $Sid; ?>)</script>
							  <tr>
							  	<td align="center"><?php echo $Sid; ?></td>
							  	<td align="center"><?php echo $val["PCode"]; ?><input type="hidden" name="v_pcode[]" id="v_pcode<?php echo $Sid; ?>" value="<?php echo $val["PCode"]; ?>"></td>
								<td align="left"><?php echo $val["NamaBarang"]; ?><input type="hidden" name="v_nmbarang[]" value="<?php echo $val["NamaBarang"]; ?>"></td>
								<td align="right"><?php echo number_format($val["Qty"],2)." ".$val["Satuan"]; ?>
                                    <input type="hidden" id="v_qty_request_hidden_<?php echo $Sid; ?>" value="<?php echo $val["Qty"]; ?>">
                                    <input type="hidden" name="v_qty_request[]" value="<?php echo $val["Qty"]; ?>">
                                    <input type="hidden" id="v_satuan_from<?php echo $Sid; ?>" value="<?php echo $val["Satuan"]; ?>">
                                </td>
								<td>
				                	<select class="form-control-new" name="v_satuan[]" id="v_satuan_to<?php echo $Sid;?>" onchange="konversiSatuan('<?php echo $val["PCode"]; ?>','<?php echo $Sid; ?>')">
                                        <option value="">- Pilih Satuan -</option>
				                	<?php
				                	$satuan_pcode = $this->purchase_order_model->getSatuanPcode($val["PCode"]);
				            		foreach($satuan_pcode as $vals)
				            		{ 
                                        $select = ($vals["Satuan"] == $val["Satuan"])? "selected": "";
										?><option value="<?php echo $vals["Satuan"]; ?>" <?php echo $select; ?> ><?php echo $vals["NamaSatuan"]; ?></option><?php
									}
				            		?>
				                	</select>
                                    <input type="hidden" name="v_satuan_konvert[]" id="v_satuan_konvert<?php echo $Sid; ?>" value="<?php echo $val["Satuan"]; ?>">
				                </td>
								<td align="right">
								<input style="text-align: right; width: 60px;" type="text" title="" class="form-control-new" id="v_Qty_dec<?php echo $Sid; ?>" value="<?php echo number_format($val["Qty"],2); ?>" onkeyup="hitungSubtotal('<?php echo $Sid; ?>'),formatRupiah('v_Qty_dec<?php echo $Sid; ?>')">

								<input style="text-align: right; width: 60px;" type="hidden"  name="v_Qty[]" id="v_Qty_<?php echo $Sid; ?>" value="<?php echo $val["Qty"]; ?>">	
								</td>
								
								<?php
                                $datenow=date('Y-m-d');
								$harga_kontrak = $this->purchase_order_model->getHargaKontrak($supplier,$datenow,$val["PCode"]);
                                // $harga_kontrak = $this->purchase_order_model->getHargaKontrak($supplier,$val["TglDokumen"],$val["PCode"]);
								if(empty($harga_kontrak)){
									$harga_po = "";
									$read = "";
								}else{
									$harga_po = $harga_kontrak->HargaContract*1;
									$read = "readonly";
								}
								?>
								
                                <td align="right">
                                <input style="text-align: right; width: 100px;" <?=$read;?> type="text" class="form-control-new" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal, total dan grand total" id="v_Harga_dec<?php echo $Sid; ?>" value="<?php echo number_format($harga_po,0,".",","); ?>"  onkeyup="hitungSubtotal('<?php echo $Sid; ?>'),formatRupiah('v_Harga_dec<?php echo $Sid; ?>')" >
                                <input style="text-align: right; width: 100px;" type="hidden" name="v_Harga[]" id="v_Harga_<?php echo $Sid; ?>" value="<?php echo $harga_po; ?>" >	
                                </td>   

								<td align="center">
							  	<span id="history" onclick="ShowHistory('<?php echo $val['PCode']; ?>','<?php echo $Sid; ?>')" class="btn btn-orange">Show</span>
							  	</td>
                                
							  	<td align="right">
							  	<input style="text-align: right; width: 50px;" type="text" class="form-control-new"  id="v_Disc_dec<?php echo $Sid; ?>"  onkeyup="hitungSubtotal('<?php echo $Sid; ?>')" value="0">
                                <input style="text-align: right; width: 50px;" type="hidden"  name="v_Disc[]" id="v_Disc_<?php echo $Sid; ?>"   value="0">
                                <div id="view_disc<?php echo $Sid; ?>"></div>
							  	</td>
							  	
							  	<td align="right">
							  	<input style="text-align: right; width: 100px;" type="text" class="form-control-new" id="v_Potongan_dec<?php echo $Sid; ?>"  onkeyup="hitungSubtotal('<?php echo $Sid; ?>'),formatRupiah('v_Potongan_dec<?php echo $Sid; ?>')" value="0" >
							  	<input style="text-align: right; width: 100px;" type="hidden"  name="v_Potongan[]" id="v_Potongan_<?php echo $Sid; ?>"  value="0" >
							  		
							  	</td>
							  	
							  	<td align="right">
							  	<input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" id="v_subtotal_dec<?php echo $Sid; ?>" value="0" >
							  	<input readonly style="text-align: right; width: 100px;" type="hidden"  name="v_subtotal[]" id="v_subtotal_<?php echo $Sid; ?>" value="0" >
							  		
							  	</td>
							  	
							  </tr>							
							<?php  $Sid++; } ?>
						</tbody>
						
						<tr style="color: black; font-weight: bold;">
                                <td colspan="8" rowspan="4">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL
                                
                                </td>
                                <td colspan="2" style="text-align: right;">
                                <input readonly style="text-align: right;width: 100%;" class="form-control-new" type="text" id="v_Jumlah_dec" value="<?php echo '0';?>" >
                                <input readonly style="text-align: right;" type="hidden" name="v_Jumlah" id="v_Jumlah" value="<?php echo '0';?>">	
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total." style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" onkeyup="potongDiscTotal(),formatRupiah('v_pot_disc_dec')" value="<?php echo '0';?>" > (%)</td>
                                <td colspan="2" style="text-align: right;">
                                <input readonly style="text-align: right;width: 100%;" class="form-control-new" type="text" id="v_pot_disc_dec" value="<?php echo '0';?>">
                                <input readonly style="text-align: right;"  type="hidden" name="v_pot_disc" id="v_pot_disc" value="<?php echo '0';?>">
                                	
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total."style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn" id="v_PPn" value="<?php echo '0';?>" onkeyup="tambahPpn(),formatRupiah('v_NilaiPPn_dec')"> (%)</td>
                                <td colspan="2" style="text-align: right;">
                                    <input readonly style="text-align: right;width: 100%;" class="form-control-new" type="text" id="v_NilaiPPn_dec" value="<?php echo '0';?>">
                                     <input readonly style="text-align: right;"  type="hidden" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo '0';?>">
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                </td>
                                <td colspan="2" style="text-align: right;">
                                    <input readonly style="text-align: right;width: 100%;" class="form-control-new" type="text" id="v_Total_dec" value="<?php echo '0';?>">
                                    <input readonly style="text-align: right;" type="hidden" name="v_Total" id="v_Total" value="<?php echo '0';?>">
                                </td>
                            </tr>
			<?php
            }
    }
    
    
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "PURCHASE ORDER";
		$data["header"] 	= $this->purchase_order_model->getHeader($nodok);
		$data["detail"] 	= $this->purchase_order_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_po_ci', $data);
	}
	
	
	function doPrint()
	{
		// echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 100;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "delivery-order.txt";
		
		$header = $this->purchase_order_model->getHeader($nodok);
		$detail = $this->purchase_order_model->getDetail_cetak($nodok);

        if($user == 'irfan1407'){
            echo "<pre>";print_r($header);
            echo "<pre>";print_r($detail);
        }


		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["Qtys"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			$arr_data["detail_namasatuan"][$counter] = $val["KdSatuan"];
			$arr_data["detail_harga"][$counter] = $val["Harga"];
			$arr_data["detail_subtotal"][$counter] = $val["Jumlah"];
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "PURCHASE ORDER";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                //$echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->NoDokumen;    
			$echo.="\r\n";			
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->TglDokumen;         
                
                $limit_spasi = 30;
                for($i=0;$i<($limit_spasi-strlen($header->TglDokumen));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Supplier";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Supplier"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Mata Uang";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Mata Uang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->currencycode;  
                 
                $limit_spasi = 30;
                for($i=0;$i<($limit_spasi-strlen($header->currencycode));$i++)
                {
                    $echo.=" ";
                }
                
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."TOP";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("TOP"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->TOP; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
                                
                $echo.=$arr_epson["cond"]."Estimasi";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Estimasi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->TglTerima;  
                 
                $limit_spasi = 30;
                for($i=0;$i<($limit_spasi-strlen($header->TglTerima));$i++)
                {
                    $echo.=" ";
                }
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Contact";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Contact"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Contact; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------   

                 // -----------------------------------------------------
                       
                   
            } 
            
            
            // baris 3
            {
                $echo.=$arr_epson["cond"]."Divisi";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Divisi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->notes;  
                 
                $limit_spasi = 30;
                for($i=0;$i<($limit_spasi-strlen($header->notes));$i++)
                {
                    $echo.=" ";
                }
                
                 $echo.=$arr_epson["cond"]."No. PR";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("No. PR"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->NoPr; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------   
                          
                
                // $echo.="\r\n";             
                   
            }
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            /*$echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }*/
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 45;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
			$echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
			
            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 15;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."SubTotal";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("SubTotal"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            $subtotal = 0;
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
				$namasatuan = $arr_data["detail_namasatuan"][$i_detail];
				$harga = $arr_data["detail_harga"][$i_detail];
			    //$jumlah = $arr_data["detail_subtotal"][$i_detail];
			    $jumlah = $qty * $harga;
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    /*$echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }*/
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 45;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$qty;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty)));$i++)
                    {
                        $echo.=" ";
                    }
					
					$echo.=$arr_epson["cond"].$namasatuan;
                    $limit_spasi = 5;
                    for($i=0;$i<($limit_spasi-strlen($namasatuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    if(strlen(number_format($harga))==1){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==2){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==3){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==4){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==5){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==6){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==7){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==8){
						$jarak_harga=6;
						}else if(strlen(number_format($harga))==9){
						$jarak_harga=6;
						}				
					if(strlen(number_format($harga))==10){
                    $echo.=number_format($harga,2);
					}else{
                    $limit_spasi = 13;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga))+$jarak_harga));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($harga,2);
					}
                    
					
					//jumlah --------------------
                    if(strlen(number_format($jumlah))==1){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==2){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==3){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==4){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==5){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==6){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==7){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==8){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==9){
						$jarak_total=4;
						}else if(strlen(number_format($jumlah))==10){
						$jarak_total=4;
						}	
									
					if(strlen(number_format($jumlah))==11){
                    $echo.=number_format($jumlah,2);
					}else{
                    $limit_spasi = 23;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($jumlah))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($jumlah,2);
					}
					
					//end jumlah ------------------------------------------------------
                    
				}
				$echo.="\r\n";
				$no++;
            	$subtotals += $jumlah;
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="=";
            }
            $echo .= "\r\n";
            
            /*$echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->note;
            
            $echo .= "\r\n";
            $echo .= "\r\n";*/
            $subtotal = $subtotals;
            if($i_page==$jml_page)
                {
            {
                // ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Note";
                $limit_spasi = (7-2);
                for($i=0;$i<($limit_spasi-strlen("Note"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->note;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->note));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Total";
                
                $limit_spasi = (11);
                for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=" ";
				
				//$subtotal = $header->Jumlah;
				//jumlah --------------------
                    if(strlen(number_format($subtotal))==1){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==2){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==3){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==4){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==5){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==6){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==7){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==8){
						$jarak_totals=5;
						}else if(strlen(number_format($subtotal))==9){
						$jarak_totals=5;
						}				
					if(strlen(number_format($subtotal))==10){
                    $echo.=number_format($subtotal,2);
					}else{
                    $limit_spasi = 14;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($subtotal))+$jarak_totals));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($subtotal,2);
					}
					
					//end jumlah ------------------------------------------------------
					
                
                //$echo.=$arr_epson["cond"].number_format($header->Total); 
                
                $echo.="\r\n";  
                // -----------------------------------------------------
            }
            
            
            
            
            {
                // ----------------------------------------------------
                for($i=0;$i<72;$i++)
                {
                    $echo.=" ";
                }
                $echo.=" ";
                
                $echo.=$arr_epson["cond"]."Disc ".$header->DiscHarga."%";
                {
                    $echo.=" ";
                }
                $echo.=" ";
                
				$diskons = $subtotal*$header->DiscHarga/100;
				//jumlah --------------------
                    if(strlen(number_format($diskons))==1){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==2){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==3){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==4){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==5){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==6){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==7){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==8){
						$jarak_totalss=5;
						}else if(strlen(number_format($diskons))==9){
						$jarak_totalss=5;
						}				
					if(strlen(number_format($diskons))==10){
                    $echo.=number_format($diskons,2);
					}else{
                    $limit_spasi = 14;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($diskons))+$jarak_totalss));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($diskons,2);
					}
					
					//end jumlah ------------------------------------------------------
                
                //$echo.=$arr_epson["cond"].number_format($header->DiscHarga);         
                
                // -----------------------------------------------------
                $echo .= "\r\n";
            }
            
            
            
            {
                // ----------------------------------------------------
                for($i=0;$i<72;$i++)
                {
                    $echo.=" ";
                }
                $echo.=" ";
                
                $echo.=$arr_epson["cond"]."PPn ".$header->PPn_."%";
                $limit_spasi = 11;
                for($i=0;$i<($limit_spasi-strlen("PPn ".$header->PPn_."%"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=" ";
                
                
				$nilppn=$subtotal*$header->PPn_/100;
				//jumlah --------------------
                    if(strlen(number_format($nilppn))==1){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==2){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==3){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==4){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==5){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==6){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==7){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==8){
						$jarak_totalsss=5;
						}else if(strlen(number_format($nilppn))==9){
						$jarak_totalsss=5;
						}				
					if(strlen(number_format($nilppn))==10){
                    $echo.=number_format($nilppn,2);
					}else{
                    $limit_spasi = 14;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($nilppn))+$jarak_totalsss));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($nilppn,2);
					}
					
					//end jumlah ------------------------------------------------------
                
                //$echo.=$arr_epson["cond"].$header->PPn;         
                
                // -----------------------------------------------------
                $echo .= "\r\n";
            }
            
            
            {
                // ----------------------------------------------------
                for($i=0;$i<72;$i++)
                {
                    $echo.=" ";
                }
                $echo.=" ";
                
                $echo.=$arr_epson["cond"]."Total";
                $limit_spasi = 11;
                for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=" ";
                
                $grand_totals = $subtotal + ($subtotal*$header->PPn_/100) - $diskons;
                //$grand_totals = $header->Total;
				
				//jumlah --------------------
                    if(strlen(number_format($grand_totals))==1){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==2){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==3){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==4){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==5){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==6){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==7){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==8){
						$jarak_totalsx=5;
						}else if(strlen(number_format($grand_totals))==9){
						$jarak_totalsx=5;
						}				
					if(strlen(number_format($grand_totals))==10){
                    $echo.=number_format($grand_totals);
					}else{
                    $limit_spasi = 14;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($grand_totals))+$jarak_totalsx));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($grand_totals,2);
					}
					
					//end jumlah ------------------------------------------------------
                
                //$echo.=$arr_epson["cond"].$header->Total;         
                
                // -----------------------------------------------------
                $echo .= "\r\n";
            }
            
            //$echo .= "\r\n";
            $limit_spasi = 7;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

             $echo.="Dibuat oleh";
            $limit_spasi = 25;
            for($i=0;$i<($limit_spasi-strlen("Dibuat oleh"));$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 25;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 25;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 3;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 2;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )       (                 )      (                )       (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            }
            
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"delivery-order");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 100;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 100;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"delivery-order");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "purchase-order",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='purchase-order-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function mail()
	{
		$mylib = new globallib();
		$tglsekarang = date('d-m-Y');
		list($vtgl, $bulan, $tahun) = explode('-',$tglsekarang);
		if($bulan*1==1){
			$bulan="12";
			$tahun=($tahun*1)-1;
		}else if($bulan*1==2){
			$bulan="01";
			$tahun=$tahun;
		}else if($bulan*1==3){
			$bulan="02";
			$tahun=$tahun;
		}else if($bulan*1==4){
			$bulan="03";
			$tahun=$tahun;
		}else if($bulan*1==5){
			$bulan="04";
			$tahun=$tahun;
		}else if($bulan*1==6){
			$bulan="05";
			$tahun=$tahun;
		}else if($bulan*1==7){
			$bulan="06";
			$tahun=$tahun;
		}else if($bulan*1==8){
			$bulan="07";
			$tahun=$tahun;
		}else if($bulan*1==9){
			$bulan="08";
			$tahun=$tahun;
		}else if($bulan*1==10){
			$bulan="09";
			$tahun=$tahun;
		}else if($bulan*1==11){
			$bulan="10";
			$tahun=$tahun;
		}else if($bulan*1==12){
			$bulan="11";
			$tahun=$tahun;
		}
		$awalbulan = '01-'.$bulan.'-'.$tahun;
		
		
		$tgldari = $mylib->ubah_tanggal($awalbulan);
		$tglsampai = $mylib->ubah_tanggal($tglsekarang);
		
		$data['v_date_from']=$awalbulan;
		$data['v_date_to']=$tglsampai;
		$data['kirim_email']="Y";
		
		$data['kirim_email']='Y';				
		$data['judul'] = 'Laporan Waiting Approve Purchase Order ';
		
		$list_data1 = $this->purchase_order_model->getDatax1($tgldari,$tglsampai);
		$list_data2 = $this->purchase_order_model->getDatax2($tgldari,$tglsampai);
		$list_data3 = $this->purchase_order_model->getDatax3($tgldari,$tglsampai);
		
		if(!empty($list_data1)){
		$data['list_data']=	$list_data1;
		$tipe ='reminder_po_1';
		$data['listemail1'] = $this->purchase_order_model->getlistemail1($tipe);
		$data['datapo1']=$list_data1;
		$this->load->view("transaksi/purchase_order/report1", $data);
		}
		
		if(!empty($list_data2)){
		$data['list_data']=	$list_data2;
		$tipe ='reminder_po_2';
		$data['listemail2'] = $this->purchase_order_model->getlistemail2($tipe);
		$data['datapo2']=$list_data2;
		$this->load->view("transaksi/purchase_order/report2", $data);
		}
		
		if(!empty($list_data3)){
		$data['list_data']=	$list_data3;
		$tipe ='reminder_po_3';
		$tipes ='reminder_po_3_cc';
		$data['listemail3'] = $this->purchase_order_model->getlistemail3($tipe);
		$data['listemail3_cc'] = $this->purchase_order_model->getlistemail3_cc($tipes);
		$data['datapo3']=$list_data3;
		$this->load->view("transaksi/purchase_order/report3", $data);
		}
			
	}

		
}

?>