<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Recipe extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        error_reporting(0);                              
        $this->load->library('globallib');
        $this->load->model('globalmodel');
		$this->load->model('transaksi/recipe_model');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$id = $this->uri->segment(4);	
			$user = $this->session->userdata('username');
			$view_value = $this->recipe_model->view_value($user);
			
			$data["search_keyword"] = "";
			
			$resSearch = "";
			$arr_search["search"]= array();
			$id_search = "";
			if($id*1>0)
			{	
				$resSearch = $this->globalmodel->getSearch($id,"recipe",$user);	
				$arrSearch = explode("&",$resSearch->query_string);
				
				$id_search = $resSearch->id;
				
				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					
					$data["search_keyword"] = $search_keyword[1];
				}
			}
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if($id_search)
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/recipe/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/recipe/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}
			
            $config['total_rows'] = $this->recipe_model->num_tabel_row($arr_search["search"]);
            $data['data'] = $this->recipe_model->getTabelList($config['per_page'], $page, $arr_search["search"]);
            $data['view_value'] = $view_value;
            $data['track'] = $mylib->print_track();
            
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
            $this->load->view('transaksi/recipe/tabellist', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }     

    function search()
    {
        $mylib = new globallib();
        
		$user = $this->session->userdata('username');
		
		// hapus dulu yah
		$this->db->delete('ci_query', array('module' => 'recipe','AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "recipe",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/transaksi/recipe/index/'.$query_id.'');
		
	} 
	
    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            
            $user = $this->session->userdata('username');
			
	    	$this->load->view('transaksi/recipe/form_add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function edit_form($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            
			$user = $this->session->userdata('username');
            $view_value = $this->recipe_model->view_value($user);
            
			$data['header']	= $this->recipe_model->getheader($id);
			$data['detail']	= $this->recipe_model->getdetail($id);
			
            $data['track'] 		= $mylib->print_track();
            $data['view_value'] = $view_value;
            
            $this->load->view('transaksi/recipe/form_edit', $data);

        } 
        else 
        {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
                              
        $resep_id = $this->input->post('resep_id');
        $pcode = $this->input->post('PCode');
		$v_status = $this->input->post('v_status');
        $v_jenis = $this->input->post('v_jenis');
        $satuan = $this->input->post('Satuan');
        $qty = $this->input->post('Qty');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        // detail pcode
		$pcode1 = $this->input->post('pcode');
		$v_namabarang1 = $this->input->post('v_namabarang');
		$v_qty1 = $this->input->post('v_qty');
		$v_satuan1 = $this->input->post('v_satuan');
		
        if($flag=="add")
        {
            
        	$v_no_dokumen = $this->recipe_model->getresepid($v_jenis);
            //echo $v_no_dokumen;
			//echo "Sistem Recipe Sedang Maintenance, Thanks :)";
			//die();
            $this->insertNewHeader($v_no_dokumen, $pcode, $v_status, $v_jenis, $satuan, $qty, $user);
       		 
       		$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       	} 
        else if($flag=="edit")
        {
        	$v_no_dokumen = $resep_id;
        	
            $this->updateHeader($v_no_dokumen, $pcode, $v_status, $v_jenis, $satuan, $qty, $user);
        	
        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }
        
        
        $counter=1;
        for($x=0;$x< count($pcode1);$x++)
		{
			$pcode 				= strtoupper(addslashes(trim($pcode1[$x])));
			$v_namabarang 		= $mylib->save_char($v_namabarang1[$x]);
			$v_qty 				= $mylib->save_int($v_qty1[$x]);
			$v_satuan			= $v_satuan1[$x];
			
			$detail 	= $this->recipe_model->cekGetDetail($pcode,$v_no_dokumen);
			
			if($pcode!="")
			{  
				if($detail->PCode==$pcode)
				{
					$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan);
					$counter++;
				}
				else
				{
					$this->insertDetail($v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan);
					$counter++;
				}
			}
		}
        
       redirect('/transaksi/recipe/edit_form/'.$v_no_dokumen.'');
    }

    function insertNewHeader($v_no_dokumen, $pcode, $v_status, $v_jenis, $satuan, $qty, $user)
    {   
        
        $data = array(
            'resep_id' => $v_no_dokumen,
            'PCode' => $pcode,
            'Status' => $v_status,
            'Jenis' => $v_jenis,
            'Satuan' => $satuan,
            'Qty' => $qty,
            'AddDate' => date("Y-m-d H:i:s"),
            'AddUser' => $user,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
        );
        
        $this->db->insert('resep_header', $data); 
    }

    function updateHeader($v_no_dokumen, $pcode, $v_status, $v_jenis, $satuan, $qty, $user)
    {
        $data = array(
            'Resep_id' => $v_no_dokumen,
            'PCode' => $pcode,
            'Status' => $v_status,
            'Jenis' => $v_jenis,
            'Satuan' => $satuan,
            'Qty' => $qty,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
        );
        
        $this->db->update('resep_header', $data, array('resep_id' => $v_no_dokumen));
        
    }

    function insertDetail($no_dokumen,$pcode,$namabarang,$qty,$satuan)
    {
        if ($pcode) 
        {
            $data = array(
                'resep_id' => $no_dokumen,
                'PCode' => $pcode,
                'Qty' => $qty,
                'Satuan' => $satuan,
            );
            
            $this->db->insert('resep_detail', $data);
        } 
    }
    
    function updateDetail($flag,$no_dokumen,$pcode,$namabarang,$qty,$satuan)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty);
    	 
        if ($pcode) 
        {
            $data = array(
                'Qty' => $new_qty,
                'Satuan' => $satuan,
            );
            
            $this->db->update('resep_detail', $data, array('resep_id' => $no_dokumen,'PCode' => $pcode));
        } 
    }
	
	function delete_detail()
	{
		$sid = $this->uri->segment(4);	
		$pcode = $this->uri->segment(5);	
		
		
		$this->db->delete('resep_detail', array('resep_id' => $sid,'PCode' => $pcode));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode '.$pcode.'</strong> berhasil','class' => 'success'));
		
		redirect('/transaksi/recipe/edit_form/'.$sid.''); 
	}
	
	function recalculate($resepid){
		$compartion_data = $this->recipe_model->getdata('C', $resepid);
		foreach($compartion_data as $data){
			$resep_id = $data['resep_id'];
			$pcode = $data['PCode'];
			$qty = $data['Qty'];
			$satuan = $data['Satuan'];
			$harga = $this->recipe_model->updateharga($resep_id, $pcode);
		}
		
		foreach($compartion_data as $data){
			$resep_id = $data['resep_id'];
			$pcode = $data['PCode'];
			$qty = $data['Qty'];
			$satuan = $data['Satuan'];
			$harga = $this->recipe_model->updateharga($resep_id, $pcode);
		}
		
		$compartion_data = $this->recipe_model->getdata('R',$resepid);
		foreach($compartion_data as $data){
			$resep_id = $data['resep_id'];
			$pcode = $data['PCode'];
			$qty = $data['Qty'];
			$satuan = $data['Satuan'];
			$harga = $this->recipe_model->updateharga($resep_id, $pcode);
		}
		//echo "<script>alert('Selesai')</script>";
		if($resepid=='all')
			redirect('/transaksi/recipe/'); 
		else
			redirect('/transaksi/recipe/edit_form/'.$resepid); 
	}
}

?>