<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class edit_transaksi_pos extends authcontroller {
	
	
    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/edit_transaksi_posmodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
        	$id = $this->uri->segment(4);
        	$user = $this->session->userdata('username');
        	
        	$data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_supplier"] = "";
            $data["search_status"] = "";
            $data["search_tgl_awal"] = "01-".date("m-Y");
            $data["search_tgl_akhir"] = date("t-m-Y");
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "edit_transaksi", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_supplier = explode("=", $arrSearch[2]); // search supplier
                    $arr_search["search"]["supplier"] = $search_supplier[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];
                    $search_tgl_awal= explode("=", $arrSearch[4]); // search tgl_awal
                    $arr_search["search"]["tgl_awal"] = $search_tgl_awal[1];
                    $search_tgl_akhir= explode("=", $arrSearch[5]); // search tgl_ahkir
                    $arr_search["search"]["tgl_akhir"] = $search_tgl_akhir[1];
                    $search_total= explode("=", $arrSearch[6]); // search total
                    $arr_search["search"]["total"] = $search_total[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_supplier"] = $search_supplier[1];
                    $data["search_status"] = $search_status[1];
                    $data["search_tgl_awal"] = $mylib->ubah_tanggal($search_tgl_awal[1]);
                    $data["search_tgl_akhir"] = $mylib->ubah_tanggal($search_tgl_akhir[1]);
                    $data["search_total"] = $search_total[1];
                }
            }
            
            
             if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/edit_transaksi_pos/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/edit_transaksi_pos/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
            
        	
        	// pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
        	
            //$this->load->view('transaksi/edit_transaksi_pos/index.php');
            $config['total_rows'] = $this->edit_transaksi_posmodel->num_edit_transaksi_pos_row($arr_search["search"]);
            $data['list'] = $this->edit_transaksi_posmodel->getList($config['per_page'], $page, $arr_search["search"]);
            $data['username']=$user;
            
            $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
            $this->load->view('transaksi/edit_transaksi_pos/list_edit_transaksi_pos.php',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function search() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'edit_transaksi', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_supplier=".$this->input->post('search_supplier');
		$search_value .= "&search_status=".$this->input->post('search_status');
		$search_value .= "&search_tgl_awal=".$mylib->ubah_tanggal($this->input->post('v_tgl_mulai'));
		$search_value .= "&search_tgl_akhir=".$mylib->ubah_tanggal($this->input->post('v_tgl_akhir'));
		$search_value .= "&search_total=".$this->input->post('search_total');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "edit_transaksi",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/edit_transaksi_pos/index/' . $query_id . '/');
    }
    
    function add_request() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
            $this->load->view('transaksi/edit_transaksi_pos/add_request.php');
        } else {
            $this->load->view('denied');
        }
    }
    
    function add_request_regist() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
        	$data['travel']=$this->edit_transaksi_posmodel->getTourTravel();
            $this->load->view('transaksi/edit_transaksi_pos/add_request_regist.php',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_request($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
        	$data['list_data'] = $this->edit_transaksi_posmodel->getData($id);
        	$data['detail'] = $this->edit_transaksi_posmodel->getstruk($data['list_data']->NoDokumen);
            $this->load->view('transaksi/edit_transaksi_pos/edit_request.php',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_request_regist($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
        	$data['travel']=$this->edit_transaksi_posmodel->getTourTravel();
        	$data['list_data'] = $this->edit_transaksi_posmodel->getData($id);
        	$data['detail'] = $this->edit_transaksi_posmodel->getregist($data['list_data']->NoDokumen);
            $this->load->view('transaksi/edit_transaksi_pos/edit_request_regist.php',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_request($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
        	$data['list_data'] = $this->edit_transaksi_posmodel->getData($id);
        	$data['detail'] = $this->edit_transaksi_posmodel->getstruk($data['list_data']->NoDokumen);
            $this->load->view('transaksi/edit_transaksi_pos/view_request.php',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_request_regist($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $nm = $mylib->getUser();
        if ($sign == "Y") {
        	$data['travel']=$this->edit_transaksi_posmodel->getTourTravel();
        	$data['list_data'] = $this->edit_transaksi_posmodel->getData($id);
        	$data['detail'] = $this->edit_transaksi_posmodel->getregist($data['list_data']->NoDokumen);
            $this->load->view('transaksi/edit_transaksi_pos/view_request_regist.php',$data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function getstruk($nostruk){
    	$result = $this->edit_transaksi_posmodel->getstruk($nostruk);
    	
		if(count($result)>0)
			echo json_encode($result[0]);
		else
			echo json_encode(array('nostruk'=>'','tanggal'=>'','kasir'=>'','status'=>'','totalnilai'=>0,'kdagent'=>'','statuskomisi'=>''));
	}
	
	function getregist($noregist){
    	$result = $this->edit_transaksi_posmodel->getregist($noregist);
    	
		if(count($result)>0)
			echo json_encode($result[0]);
		else
			echo json_encode(array('KdRegister'=>'','Tanggal'=>'','statuskomisi'=>'','Jumlah'=>0,'NoStiker'=>''));
	}
	
	function save() {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$user = $this->session->userdata('username');
        $nostruk = $this->input->post('nostruk');
        $tanggal = $this->input->post('tanggal');
        $type = $this->input->post('v_type');
        $kasir = $this->input->post('kasir');
        $alasan = $this->input->post('alasan');
        $nostiker = $this->input->post('nostiker');
        $nostikerlama = $this->input->post('kdagentlama');
        
        $data = array('NoDokumen'=> $nostruk,
        			  'Created_Date'=>date('Y-m-d'),
        			  'JenisDokumen' => 'POS',
        			  'Type' => $type,
        			  'Alasan'=> $alasan,
        			  'NoStiker'=> $nostiker,
        			  'NoStikerLama'=> $nostikerlama,
        			  'Add_User_Request' => $user,
        			  'Add_Date_Request'=> date('Y-m-d h:i:s')
        			);
        $id = $this->edit_transaksi_posmodel->insert_log($data);
        
		redirect('transaksi/edit_transaksi_pos');
	}
	
	function save_regist() {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$user = $this->session->userdata('username');
        $nodokumen = $this->input->post('noregist');
        $tanggal = $this->input->post('tanggal');
        $type = $this->input->post('v_type');
        $alasan = $this->input->post('alasan');
        
        $tanggalbaru = $mylib->ubah_tanggal($this->input->post('tanggalbaru'));
        $travelbaru = $this->input->post('travelbaru');
        
        $KdTravelLama = $this->input->post('KdTravelLama');
        
        $data = array('NoDokumen'=> $nodokumen,
                      'Created_Date'=>date('Y-m-d'),
        			  'JenisDokumen' => 'REGIST',
        			  'Type' => $type,
        			  'Alasan'=> $alasan,
        			  'Tanggal'=> $tanggalbaru,
        			  'TanggalLama'=> $tanggal,
        			  'KdTravel'=> $travelbaru,
        			  'KdTravelLama'=> $KdTravelLama,
        			  'Add_User_Request' => $user,
        			  'Add_Date_Request'=> date('Y-m-d h:i:s')
        			);
        $id = $this->edit_transaksi_posmodel->insert_log($data);
        
		redirect('transaksi/edit_transaksi_pos');
	}
	
	function edit() {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$this->load->library('telegramlib');
    	$telegram = new telegramlib();
    	
        $user = $this->session->userdata('username');
    	$id_request = $this->input->post('id_request');
        $nostruk = $this->input->post('nostruk');
        $tanggal = $this->input->post('tanggal');
        $type = $this->input->post('v_type');
        $kasir = $this->input->post('kasir');
        $alasan = $this->input->post('alasan');
        $nostiker = $this->input->post('nostiker');
        $nostikerlama = $this->input->post('kdagentlama');
        $v_approve = $this->input->post('v_approve');
        $v_reject = $this->input->post('v_reject');
        $v_alasan_reject = $this->input->post('v_alasan_reject');
        $v_edit = $this->input->post('v_edit');
        
        //jika hany sekedar simpan
        if($v_edit=="1"){
		        $data = array('NoDokumen'=> $nostruk,
		        			  'JenisDokumen' => 'POS',
		        			  'Type' => $type,
		        			  'Alasan'=> $alasan,
		        			  'NoStiker'=> $nostiker,
		        			  'NoStikerLama'=> $nostikerlama,
		        			  'Edit_User_Request' => $user,
		        			  'Edit_Date_Request'=> date('Y-m-d h:i:s')
		        			);
		       $this->db->update('edit_log',$data,array('id_request'=>$id_request));
       //jika approve atau reject
       }else if($v_edit=="2"){
       	
       	  //jika type hapus
       	  if($type=="hapus"){
       	    //jika approve
		       		if($v_approve=="1"){
		       			//update di edit log
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>2,
						                        'Approve_Request'=>$user,
						                        'Approve_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Approve'=>1,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						                  
						//update di transaksi
						$this->db->update(
						                  'transaksi_header',
						                  array(
						                        'Status'=>2),
						                  array('NoStruk'=>$nostruk)
						                  );
						$this->db->update(
						                  'transaksi_detail',
						                  array(
						                        'Status'=>2),
						                  array('NoStruk'=>$nostruk)
						                  );
						$this->delete('transaksi_detail_voucher',array('NoStruk'=>$nostruk));
						                  
						$sts1="Di Approve";
						$sts2="";
					}
			   	    
			   	    //jika reject
			   	    if($v_reject=="1"){
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>3,
						                        'Reject_Request'=>$user,
						                        'Reject_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Reject'=>1,
						                        'Reject_Remark'=>$v_alasan_reject,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						$sts1="Di Reject";
						$sts2=", Karena ".$v_alasan_reject;
					}
					
					//cari telegramID
					$list_data = $this->edit_transaksi_posmodel->getData($id_request);
					$tele = $this->edit_transaksi_posmodel->getTelegram($list_data->Add_User_Request); 
					
					if($tele->TelegramID!=""){
						
						    if($type=="hapus"){
								$tipes="Hapus Stiker";
							}else if($type=="stiker"){
								$tipes="Update Stiker";
							}
					
						//kirim telegram ke add user
						$vtext = "
						INFO : Edit Transaksi POS No. Struk $nostruk dengan Tipe $tipes telah $sts1 $sts2
						
						";
				        
				        //$keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujupos#'.$id);
						//$keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakpos#'.$id);
				        
				        $telegram->chat_id($tele->TelegramID);
				        $telegram->text($vtext);
				        //$telegram->keyboard($keyboard);
				        $telegram->send_with_inline_keyboard();
			        
	                }
	                
	        }else if($type=="stiker"){
				
				//jika approve
		       		if($v_approve=="1"){
		       			//update di edit log
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>2,
						                        'Approve_Request'=>$user,
						                        'Approve_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Approve'=>1,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						                  
						//update di transaksi
						$this->db->update(
						                  'transaksi_header',
						                  array(
						                        'KdAgent'=>$nostiker),
						                  array('NoStruk'=>$nostruk)
						                  );
						$this->db->update(
						                  'transaksi_detail',
						                  array(
						                        'KdAgent'=>$nostiker),
						                  array('NoStruk'=>$nostruk)
						                  );
						                  
						$sts1="Di Approve";
						$sts2="";
					}
			   	    
			   	    //jika reject
			   	    if($v_reject=="1"){
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>3,
						                        'Reject_Request'=>$user,
						                        'Reject_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Reject'=>1,
						                        'Reject_Remark'=>$v_alasan_reject,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						$sts1="Di Reject";
						$sts2=", Karena ".$v_alasan_reject;
					}
					
					//cari telegramID
					$list_data = $this->edit_transaksi_posmodel->getData($id_request);
					$tele = $this->edit_transaksi_posmodel->getTelegram($list_data->Add_User_Request); 
					
					if($tele->TelegramID!=""){
						
						    if($type=="hapus"){
								$tipes="Hapus Stiker";
							}else if($type=="stiker"){
								$tipes="Update Stiker";
							}
					
						//kirim telegram ke add user
						$vtext = "
						INFO : Edit Transaksi POS No. Struk $nostruk dengan Tipe $tipes telah $sts1 $sts2
						
						";
				        
				        //$keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujupos#'.$id);
						//$keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakpos#'.$id);
				        
				        $telegram->chat_id($tele->TelegramID);
				        $telegram->text($vtext);
				        //$telegram->keyboard($keyboard);
				        $telegram->send_with_inline_keyboard();
			        
				    }
			}
	        
	   }
       
        
		redirect('transaksi/edit_transaksi_pos');
	}
	
	function edit_regist() {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$this->load->library('telegramlib');
    	$telegram = new telegramlib();
    	
    	$user = $this->session->userdata('username');
    	$id_request = $this->input->post('id_request');
        $nodokumen = $this->input->post('noregist');
        $tanggal = $this->input->post('tanggal');
        $type = $this->input->post('v_type');
        $alasan = $this->input->post('alasan');
        
        $tanggalbaru = $mylib->ubah_tanggal($this->input->post('tanggalbaru'));
        $travelbaru = $this->input->post('travelbaru');
        $KdTravelLama = $this->input->post('KdTravelLama');
        
        $v_approve = $this->input->post('v_approve');
        $v_reject = $this->input->post('v_reject');
        $v_alasan_reject = $this->input->post('v_alasan_reject');
        $v_edit = $this->input->post('v_edit');
        
        if($v_edit=="1"){
	        $data = array('NoDokumen'=> $nodokumen,
	        			  'JenisDokumen' => 'REGIST',
	        			  'Type' => $type,
	        			  'Alasan'=> $alasan,
	        			  'Tanggal'=> $tanggalbaru,
	        			  'TanggalLama'=> $tanggal,
	        			  'KdTravel'=> $travelbaru,
	        			  'KdTravelLama'=> $KdTravelLama,
	        			  'Edit_User_Request' => $user,
	        			  'Edit_Date_Request'=> date('Y-m-d h:i:s')
	        			);
	        $this->db->update('edit_log',$data,array('id_request'=>$id_request));
        }else if($v_edit=="2"){
       	
       	  //jika type tanggal
       	  if($type=="tanggal"){
       	    //jika approve
		       		if($v_approve=="1"){
		       			//update di edit log
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>2,
						                        'Approve_Request'=>$user,
						                        'Approve_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Approve'=>1,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						                  
						//update di transaksi register
						$this->db->update(
						                  'register',
						                  array(
						                        'Tanggal'=> $tanggalbaru),
						                  array('KdRegister'=>$nodokumen)
						                  );
						                  
						$sts1="Di Approve";
						$sts2="";
					}
			   	    
			   	    //jika reject
			   	    if($v_reject=="1"){
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>3,
						                        'Reject_Request'=>$user,
						                        'Reject_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Reject'=>1,
						                        'Reject_Remark'=>$v_alasan_reject,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						$sts1="Di Reject";
						$sts2=", Karena ".$v_alasan_reject;
					}
					
					//cari telegramID
					$list_data = $this->edit_transaksi_posmodel->getData($id_request);
					$tele = $this->edit_transaksi_posmodel->getTelegram($list_data->Add_User_Request); 
					
					if($tele->TelegramID!=""){
						
						    if($type=="tanggal"){
								$tipes="Ubah Tanggal";
							}else if($type=="travel"){
								$tipes="Ubah Travel";
							}
					
						//kirim telegram ke add user
						$vtext = "
						INFO : Edit Transaksi Register No. Registrasi $nodokumen dengan Tipe $tipes telah $sts1 $sts2
						
						";
				        
				        //$keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujupos#'.$id);
						//$keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakpos#'.$id);
				        
				        $telegram->chat_id($tele->TelegramID);
				        $telegram->text($vtext);
				        //$telegram->keyboard($keyboard);
				        $telegram->send_with_inline_keyboard();
	        		}
	        		
	        }else if($type=="travel"){
				
				//jika approve
		       		if($v_approve=="1"){
		       			//update di edit log
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>2,
						                        'Approve_Request'=>$user,
						                        'Approve_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Approve'=>1,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						                  
						//update di transaksi
						$this->db->update(
						                  'register',
						                  array(
						                        'KdTravel'=>$travelbaru),
						                  array('KdRegister'=>$nodokumen)
						                  );
						                  
						$sts1="Di Approve";
						$sts2="";
					}
			   	    
			   	    //jika reject
			   	    if($v_reject=="1"){
						$this->db->update(
						                  'edit_log',
						                  array(
						                        'Status_Request'=>3,
						                        'Reject_Request'=>$user,
						                        'Reject_Date'=> date('Y-m-d h:i:s'),
						                        'Status_Reject'=>1,
						                        'Reject_Remark'=>$v_alasan_reject,
						                        'Perangkat'=>1),
						                  array('id_request'=>$id_request)
						                  );
						$sts1="Di Reject";
						$sts2=", Karena ".$v_alasan_reject;
					}
					
					//cari telegramID
					$list_data = $this->edit_transaksi_posmodel->getData($id_request);
					$tele = $this->edit_transaksi_posmodel->getTelegram($list_data->Add_User_Request); 
					
					if($tele->TelegramID!=""){
						
							    if($type=="hapus"){
									$tipes="Hapus Stiker";
								}else if($type=="stiker"){
									$tipes="Update Stiker";
								}
						
							//kirim telegram ke add user
							$vtext = "
							INFO : Edit Transaksi Register No. Registrasi $nodokumen dengan Tipe $tipes telah $sts1 $sts2
							
							";
					        
					        //$keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujupos#'.$id);
							//$keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakpos#'.$id);
					        
					        $telegram->chat_id($tele->TelegramID);
					        $telegram->text($vtext);
					        //$telegram->keyboard($keyboard);
					        $telegram->send_with_inline_keyboard();
					}
			}
	        
	   }
	   
		redirect('transaksi/edit_transaksi_pos');
	}
	
	function delete($id) {
    	
       $this->db->delete('edit_log',array('id_request'=>$id));
        
		redirect('transaksi/edit_transaksi_pos');
	}

    function proses($id) {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	$this->load->library('telegramlib');
    	$telegram = new telegramlib();
    	$user = $this->session->userdata('username');
        
        $list_data = $this->edit_transaksi_posmodel->getData($id);
        
        if($list_data->JenisDokumen=="POS"){
		        $detail = $this->edit_transaksi_posmodel->getstruk($list_data->NoDokumen);
		        
		        //pertama update status di tabel edit_log
		        $this->db->update('edit_log',array('Status_Request'=>1),array('id_request'=>$id));
		        
						        //kirim email
						        $subject = "Request Approval Edit Transaksi POS No. Struk ".$list_data->NoDokumen;
			                    $author  = "Auto System";
			                                
			                    $url = base_url()."index.php/transaksi/edit_transaksi_pos/edit_request/".$list_data->id_request;
			                                
			                    $to = "eno@vci.co.id;trisno@secretgarden.co.id;";
				                //$to = "irf@vci.co.id;";
				                $to_name = "Trisno;";
			                                
			                    $body  = "Dear Mr. Trisno , <br><br>";
			                    $body .= "Mohon lakukan approval Edit Transaksi POS No. Struk ".$list_data->NoDokumen.", Nilai ".number_format($detail[0]['totalnilai'])."<br>
			                    Dengan Alasan ".$list_data->Alasan."<br><br>";
			                    $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
			                    
			                    $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
			                    
			    if($list_data->Type=="hapus"){
					$tipe="Hapus Stiker";
				}else if($list_data->Type=="stiker"){
					$tipe="Update Stiker";
				}
			    
			    //kirim juga ke telegream dengan username trisno1402                
			    $tele = $this->edit_transaksi_posmodel->getTelegram('trisno1402');   
			    $textstruk= "<pre>" . str_pad('No. Srtuk',10,' '). str_pad(': '.$list_data->NoDokumen,9,' ',STR_PAD_RIGHT) . "</pre>";
			    $texttanggal= "<pre>" . str_pad('Tanggal',10,' '). str_pad(': '.$detail[0]['tanggal'],9,' ',STR_PAD_RIGHT) . "</pre>";             
		        $textkasir= "<pre>" . str_pad('Kasir',10,' '). str_pad(': '.$detail[0]['kasir'],9,' ',STR_PAD_RIGHT) . "</pre>";
		        $textalasaan= "<pre>" . str_pad('Alasan',10,' '). str_pad(': '.$list_data->Alasan,9,' ',STR_PAD_RIGHT) . "</pre>";
		        $texttype= "<pre>" . str_pad('Tipe',10,' '). str_pad(': '.$tipe,9,' ',STR_PAD_RIGHT) . "</pre>";
		        $textstiker= "<pre>" . str_pad('No. Stiker',10,' '). str_pad(': '.$list_data->NoStiker,9,' ',STR_PAD_RIGHT) . "</pre>";
		        $textajukan= "<pre>" . str_pad('Diajukan',10,' '). str_pad(': '.$list_data->Add_User_Request,9,' ',STR_PAD_RIGHT) . "</pre>";
		        
		        $vtext = "
				<b>Edit Transaksi POS ID $id</b>
				        
				$textstruk
				$texttanggal
				$textkasir
				$texttype
				$textstiker
				$textajukan
				$textalasaan
				
				";
		        
		        $keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujupos#'.$id);
				$keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakpos#'.$id);
		        
		        $telegram->chat_id($tele->TelegramID);
		        $telegram->text($vtext);
		        $telegram->keyboard($keyboard);
		        $telegram->send_with_inline_keyboard();
		        
        }else if($list_data->JenisDokumen=="REGIST"){
        	$detail = $this->edit_transaksi_posmodel->getregist($list_data->NoDokumen);
        	
        	//pertama update status di tabel edit_log
		        $this->db->update('edit_log',array('Status_Request'=>1),array('id_request'=>$id));
		        
						        //kirim email
						        $subject = "Request Approval Edit Registrasi No. Struk ".$list_data->NoDokumen;
			                    $author  = "Auto System";
			                                
			                    $url = base_url()."index.php/transaksi/edit_transaksi_pos/edit_request_regist/".$list_data->id_request;
			                                
			                    //$to = "eno@vci.co.id;trisno@secretgarden.co.id;";
				                //$to = "irf@vci.co.id;";
				                $to_name = "Trisno;";
			                                
			                    $body  = "Dear Mr. Trisno , <br><br>";
			                    $body .= "Mohon lakukan approval Edit Transaksi Register No. Registrasi ".$list_data->NoDokumen.", Dengan Alasan ".$list_data->Alasan."<br><br>";
			                    $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;
			                    
			                    $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
			                    
			    if($list_data->Type=="tanggal"){
					$tipe="Ubah Tanggal";
				}else if($list_data->Type=="travel"){
					$tipe="Ubah Travel";
				}
			    
			    //kirim juga ke telegream dengan username trisno1402                
			    $tele = $this->edit_transaksi_posmodel->getTelegram('trisno1402');   
			    $textstruk= "<pre>" . str_pad('No. Srtuk',10,' '). str_pad(': '.$list_data->NoDokumen,9,' ',STR_PAD_RIGHT) . "</pre>";
			    $texttanggal= "<pre>" . str_pad('Tanggal',10,' '). str_pad(': '.$detail[0]['Tanggal'],9,' ',STR_PAD_RIGHT) . "</pre>";
			    $textstiker= "<pre>" . str_pad('Travel',10,' '). str_pad(': '.$detail[0]['nama_travel'],9,' ',STR_PAD_RIGHT) . "</pre>";             
		        $textkasir= "<pre>" . str_pad('TL',10,' '). str_pad(': '.$detail[0]['nama_tour_leader'],9,' ',STR_PAD_RIGHT) . "</pre>";
		        $texttype= "<pre>" . str_pad('Tipe',10,' '). str_pad(': '.$tipe,9,' ',STR_PAD_RIGHT) . "</pre>";
		        $textajukan= "<pre>" . str_pad('Diajukan',10,' '). str_pad(': '.$list_data->Add_User_Request,9,' ',STR_PAD_RIGHT) . "</pre>";
		        $textalasaan= "<pre>" . str_pad('Alasan',10,' '). str_pad(': '.$list_data->Alasan,9,' ',STR_PAD_RIGHT) . "</pre>";
		        
		        
		        $vtext = "
				<b>Edit Transaksi REGISTER ID $id</b>
				        
				$textstruk
				$texttanggal
				$textstiker
				$textkasir
				$texttype				
				$textajukan
				$textalasaan
				
				";
		        
		        $keyboard[0][0] = array('text'=>'Approve', 'callback_data' => 'setujuregist#'.$id);
				$keyboard[0][1] = array('text'=>'Reject', 'callback_data' => 'tolakregist#'.$id);
		        
		        $telegram->chat_id($tele->TelegramID);
		        $telegram->text($vtext);
		        $telegram->keyboard($keyboard);
		        $telegram->send_with_inline_keyboard();
			
		}
		redirect('transaksi/edit_transaksi_pos');
	}
	
	/*
	function proses() {
    	echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$this->load->library('telegramlib');
    	$telegram = new telegramlib();
    	
    	$user = $this->session->userdata('username');
        $nostruk = $this->input->post('nostruk');
        $tanggal = $this->input->post('tanggal');
        $type = $this->input->post('v_type');
        $kasir = $this->input->post('kasir');
        $alasan = $this->input->post('alasan');
        $nostiker = $this->input->post('nostiker');
        $nostikerlama = $this->input->post('kdagentlama');
        
        $data = array('NoDokumen'=> $nostruk,
        			  'JenisDokumen' => 'POS',
        			  'Type' => $type,
        			  'Alasan'=> $alasan,
        			  'NoStiker'=> $nostiker,
        			  'NoStikerLama'=> $nostikerlama,
        			  'UserName' => $user
        			);
        $id = $this->edit_transaksi_posmodel->insert_log($data);
        $vtext = "
		<b>Edit Transaksi POS</b>
		        
		No Struk : $nostruk
		Tanggal  : $tanggal
		Kasir    : $kasir
		Alasan   : $alasan
		Type     : $type
		Stiker   : $nostiker
		Diajukan oleh : $user";
        
        $keyboard[0][0] = array('text'=>'Setuju', 'callback_data' => 'Setujupos#'.$id);
		$keyboard[0][1] = array('text'=>'Tolak', 'callback_data' => 'Tolakpos#'.$id);
        
        $telegram->chat_id('188790806');
        $telegram->text($vtext);
        $telegram->keyboard($keyboard);
        $telegram->send_with_inline_keyboard();
        
		redirect('transaksi/edit_transaksi_pos');
	}
	*/
	
	function hapus($nostruk){
		$this->edit_transaksi_posmodel->hapus($nostruk);
	}
	
	function update_stiker($nostruk, $nostiker){
		$this->edit_transaksi_posmodel->update_stiker($nostruk, $nostiker);
	}
}

?>