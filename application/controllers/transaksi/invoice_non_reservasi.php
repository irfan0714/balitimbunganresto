<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Invoice_non_reservasi extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/invoice_non_reservasi_model');
    }

    function index()
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
 
			$data['track'] = $mylib->print_track();

            $this->load->view('transaksi/invoice_non_reservasi/invoice_non_reservasi_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search()
    {
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'invoice_reservasi', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_status=".$this->input->post('search_status');

		$data = array(
            'query_string' => $search_value,
            'module' => "invoice_reservasi",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/invoice_non_reservasi/index/' . $query_id . '');
    }


    function getList()
    {

    	    $keyword = $this->uri->segment(4);
            $status = $this->uri->segment(5);

            //echo  $keyword." - ".$status;die;

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '100';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;



            $data['data'] = $this->invoice_non_reservasi_model->getInvoiceReservasiList($keyword,$status,$config['per_page']);

			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

		$this->load->view('transaksi/invoice_non_reservasi/invoice_non_reservasi_getlist', $data);
	}


    public function ajax_view_tambahan() {
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $user = $this->session->userdata('username');
        $mylib = new globallib();

        $beo = $this->input->post('v_beo');
        $no_invoice = $this->input->post('v_no_invoice');

        //detail item tambahan
        $menu_tambahan = $this->globalmodel->getQuery("     * FROM invoice_reservasi_detail a WHERE a.NoDokumen='$no_invoice' AND a.Tambahan='Y'
        										");


        if (empty($menu_tambahan)){
		?>

							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data Item Tambahan</td>
							    <input type="hidden" id="totalan_tambahan_detail" name="totalan_tambahan_detail" value="0">
							</tr>";

		<?php
		}else{
		$no=1;
		$grnttl=0;
	    foreach ($menu_tambahan as $cetak) {
	    ?>

								<tr>
								<td align='left'><?=$cetak['Keterangan'];?> </td>
								<td align="right"><?=number_format($cetak['Nilai']);?></td>
							    <td align="center">

							    <button type="button" class="btn btn-danger btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Close" title="" onclick="delete_detail_tambahan('<?php echo $cetak["id"]; ?>', '<?php echo base_url(); ?>');" >
                                <i class="entypo-trash"></i>
                                </button>

							    </td>

							</tr>";

		<?php $grnttl+=$cetak['Nilai'];$no++;} ?>

		                    <tr class="title_table">
							    <td colspan="2" align="left">Total</td>
								<td align='right'> <?php echo number_format($grnttl);?> <input type="hidden" id="totalan_tambahan_detail" name="totalan_tambahan_detail" value="<?php echo $grnttl;?>"></td>

							</tr>
		<?php }

    }


    public function ajax_add() {
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $user = $this->session->userdata('username');
        $mylib = new globallib();

        $no_invoice = $this->input->post('v_no_invoice');
        $tgl_jatuh_tempo = $mylib->ubah_tanggal($this->input->post('tgl_jatuh_tempo'));
        $customer = $this->input->post('customer');
        $alamat = $this->input->post('alamat');
        $tlp = $this->input->post('tlp');
        $tanggal2 = $this->input->post('tgltrans');

        // echo $tanggal2;

        // die();

        //detail
        $tanggal1 = $this->input->post('tanggal');
        $nostruk1 = $this->input->post('nostruk');
        $pcode1 = $this->input->post('pcode');
        $qty1 = $this->input->post('qty');
        $harga1 = $this->input->post('harga');
        $disc1 = $this->input->post('disc');
        $netto1 = $this->input->post('netto');

        $v_ket_tambahan1 = $this->input->post('v_ket_tambahan');
        $v_harga_tambahan1 = $this->input->post('v_harga_tambahan');


        //header
        $totalanqty = $this->input->post('totalanqty2');
        $totalandisc = $this->input->post('totalandisc2');
        $totalannet = $this->input->post('totalannet2');
        $totalan_tambahan = $this->input->post('totalan_tambahan');
        $grand_totals = $this->input->post('grand_totals');


        $tglApp = date('Y-m-d');
        if($no_invoice==""){
		   $no_faktur = $this->generate_number_invoice();
		}else{
		   $no_faktur = $no_invoice;
		}

        $nomor_invoice = $no_faktur;
        $tanggal_invoice = $tglApp;
        $totalnilaibelanja = $totalannet;
        $due_date = $tgl_jatuh_tempo;
        $piutang = $grand_totals;

          //insert ke invoice_non_reservasi_customer
		  $datax = array('NoDokumen'=>$nomor_invoice,
		  				'Customer'=>$customer,
		  				'Alamat'=>$alamat,
		  				'Tlp'=>$tlp,
				        'AddUser'=>$user,
				        'AddDate'=>date('Y-m-d'));

         $this->db->insert("invoice_non_reservasi_customer", $datax);

          //insert ke invoice_non_reservasi
		  $data = array('NoDokumen'=>$nomor_invoice,
				        'Tanggal'=>$tanggal_invoice,
                        'TglTrans'=>$tanggal2,
				        'Jatuh_Tempo'=>$due_date,
				        'Total_Nilai'=>$piutang,
				        'Total_Qty'=>$totalanqty,
				        'Total_Disc'=>$totalandisc,
				        'Total_Netto'=>$totalannet,
				        'Total_tambahan'=>$totalan_tambahan,
				        'AddUser'=>$user,
				        'AddDate'=>date('Y-m-d'));

         $this->db->insert("invoice_non_reservasi", $data);

                       // detail list transaksi
				        for ($y = 0; $y < count($nostruk1); $y++)
				        {
				        	$tanggal = $tanggal1[$y];
				        	$nostruk = $nostruk1[$y];
				        	$pcode = $pcode1[$y];
					        $qty = $qty1[$y];
					        $harga = $harga1[$y];
					        $disc = $disc1[$y];
					        $netto = $netto1[$y];


				        	$v_total_hidden = $v_total_hidden1[$y];

				        	//data
				        	$data_detail_list = array(
				        								'NoDokumen'=>$nomor_invoice,
				        								'NoStruk'=>$nostruk,
				        								'Tanggal'=>$tanggal,
				        								'PCode'=>$pcode,
				        								'Qty'=>$qty,
				        								'Harga'=>$harga,
				        								'Disc'=>$disc,
				        								'Netto'=>$netto,
				        								'Keterangan'=>''
				        								);

				        	//insert
				        	$this->db->insert("invoice_non_reservasi_detail", $data_detail_list);

				        	//upodate_status_ditransaksi_detailnya
				        	$this->db->update('transaksi_header',array('statusinvoice'=>'1'),array('NoStruk'=>$nostruk));
				        }

                        // detail tambahan
				        for ($x = 0; $x < count($v_ket_tambahan1); $x++)
				        {
				        	$v_ket_tambahan = $v_ket_tambahan1[$x];
				        	$v_harga_tambahan = $v_harga_tambahan1[$x];

				        	//data
				        	$data_detail_invoice = array(
				        								'NoDokumen'=>$nomor_invoice,
				        								'NoStruk'=>'-',
				        								'Tanggal'=>date('Y-m-d'),
				        								'PCode'=>'-',
				        								'Qty'=>0,
				        								'Harga'=>$v_harga_tambahan,
				        								'Disc'=>0,
				        								'Netto'=>$v_harga_tambahan,
				        								'Keterangan'=>$v_ket_tambahan,
				        								'Tambahan'=>'Y'
				        								);

				        	//insert
				        	if($v_ket_tambahan!=""){
				        	$this->db->insert("invoice_non_reservasi_detail", $data_detail_invoice);
				        	}
				        }

        echo json_encode(array("status" => TRUE));
    }


    public function ajax_update_invoice() {

        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $user = $this->session->userdata('username');
        $mylib = new globallib();

        $beo = $this->input->post('v_beo');
        $no_invoice = $this->input->post('v_no_invoice');
        $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
        $tglApp = $getTgl[0]['TglTrans'];

        //pertama dapatkan NoStiker BEO di Konfirmasi Reservasi
        $nosticker = $this->invoice_non_reservasi_model->getStikerBEO($beo);
        $nostkr="";
        $tglkon="";
        for($x=0; $x<count($nosticker); $x++){
        	if(($x+1)==count($nosticker)){
				$koma="";
			}else{
				$koma=",";
			}
			$nostkr.="'".$nosticker[$x]['NoSticker']."'".$koma;
			$tglkon.="'".$nosticker[$x]['TglKonfirmasi']."'".$koma;
		}

        //jumlah belanja
        $jumlah_belanja = $this->globalmodel->getQuery("  SUM(a.`TotalNilai`) AS Total_Belanja
														FROM
														  transaksi_header a
														WHERE a.`KdAgent` IN ($nostkr)
														  AND a.`Tanggal` IN ($tglkon)
														  AND a.`KdStore` = '00'
														ORDER BY a.`Tanggal`,
														  a.`KdAgent` ASC;
        											 ");

        //detail item makanan
        $menu = $this->globalmodel->getQuery("  DISTINCT(b.`NamaLengkap`) AS menu
														FROM
														  transaksi_detail a
														INNER JOIN masterbarang b ON a.`PCode` = B.`PCode`
														WHERE a.`KdAgent` IN ($nostkr)
														  AND a.`Tanggal` IN ($tglkon)
														  AND a.`KdStore` = '00'
														ORDER BY a.`Tanggal`,
														  a.`KdAgent` ASC;
        											 ");

        $list_menu="";
        for($y=0; $y<count($menu); $y++){
        	if(($y+1)==count($menu)){
				$koma=".";
			}else{
				$koma=",";
			}
			$list_menu.=$menu[$y]['menu'].$koma;
		}

        //dapatkan uang muka menjadi voucher dengan BEO ini
        $nilai_voucher = $this->invoice_non_reservasi_model->getUangMuka($beo);
        $jatuh_tempo = $this->globalmodel->getQuery(" DATE_ADD('$tglApp', INTERVAL 14 DAY) AS due_date;");
        $info_beo = $this->globalmodel->getQuery(" * FROM trans_reservasi a WHERE a.NoDokumen='$beo'");

        $nomor_invoice = $no_invoice;
        $tanggal_invoice = $tglApp;
        $NoReservasi = $beo;
        $keterangan = $info_beo[0]['adultParticipants']+$info_beo[0]['childParticipants']+$info_beo[0]['kidsParticipants']." PAX. CODE GROUP : ".$info_beo[0]['GroupCode'];
        $totalnilaibelanja = $jumlah_belanja[0]['Total_Belanja'];
        $uang_muka = $nilai_voucher[0]['nilai'];
        $due_date = $jatuh_tempo[0]['due_date'];
        $piutang = $totalnilaibelanja - $uang_muka;

          //insert ke invoice_reservasi
		  $data = array('Tanggal'=>$tanggal_invoice,
				        'Jatuh_Tempo'=>$due_date,
				        'NoReservasi'=>$NoReservasi,
				        'Keterangan'=>strtoupper($list_menu)."".$keterangan,
				        'Total_Nilai'=>$piutang,
				        'Uang_Muka'=>$uang_muka,
				        'AddUser'=>$user,
				        'AddDate'=>date('Y-m-d'));

        $this->db->update("invoice_reservasi", $data ,array('NoDokumen'=>$nomor_invoice));

        echo json_encode(array("status" => TRUE));
    }

    public function ajax_edit_invoice() {
        $sgv= $this->uri->segment(4);
        $inv = $this->uri->segment(5);
        $ar= $this->uri->segment(6);
        $thn= $this->uri->segment(7);
        $rmw= $this->uri->segment(8);
        $cont = $this->uri->segment(9);
        $nodok = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;
        $data = $this->invoice_non_reservasi_model->get_by_id($nodok);
        echo json_encode($data);
    }

    public function generate_number_invoice() {

    		$cekInv="InvoiceNonReservasi AS Invoice FROM setup";
            $getInv = $this->globalmodel->getQuery($cekInv);
            $inv=$getInv[0]['Invoice'];

    	    $no = $inv."/INV/AR/";
    	    $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
            $tglApp = $getTgl[0]['TglTrans'];
            $pisah_tanggal = explode('-',$tglApp);
            $thn = substr($tglApp, 0, 4);
            $bln = $this->dateRomawi(substr($tglApp, 5, 2));
			$zql=" NoDokumen FROM invoice_non_reservasi WHERE YEAR(Tanggal)='".$pisah_tanggal[0]."' AND MONTH(Tanggal)='".$pisah_tanggal[1]."' ORDER BY invoice_non_reservasi.NoDokumen DESC limit 1";
            $getNoDokumen = $this->globalmodel->getQuery($zql);

            if (!empty($getNoDokumen)) {
                $nodokumen = $getNoDokumen[0]['NoDokumen'];
                $noLast = (substr($nodokumen, -4) *1) +1;
                if (strlen($noLast) == "1") {
                    $noDokNew = "0000" . $noLast;
                } elseif (strlen($noLast) == "2") {
                    $noDokNew = "000" . $noLast;
                } elseif (strlen($noLast) == "3") {
                    $noDokNew = "00" . $noLast;
                } elseif (strlen($noLast) == "4") {
                    $noDokNew = "0" . $noLast;
                } elseif (strlen($noLast) == "5") {
                    $noDokNew = $noLast;
                } else {
                    $noDokNew = "00001";
                }
            } else {
                $noDokNew = "00001";
            }

        $invoice = $no . $thn . "/" . $bln . "/" . $noDokNew;

        return $invoice;
    }


    function dateRomawi($bulan) {
        switch ($bulan) {
            case 1:
                $blnRom = 'I';
                break;
            case 2:
                $blnRom = 'II';
                break;
            case 3:
                $blnRom = 'III';
                break;
            case 4:
                $blnRom = 'IV';
                break;
            case 5:
                $blnRom = 'V';
                break;
            case 6:
                $blnRom = 'VI';
                break;
            case 7:
                $blnRom = 'VII';
                break;
            case 8:
                $blnRom = 'VIII';
                break;
            case 9:
                $blnRom = 'IX';
                break;
			case 10:
                $blnRom = 'X';
                break;
            case 11:
                $blnRom = 'XI';
                break;
            case 12:
                $blnRom = 'XII';
                break;
        }
        return $blnRom;
    }


    function bayar()
    {
    			  $mylib = new globallib();

    			    $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;

    			  //update status di invoice reservasi
    			  $this->db->update('invoice_non_reservasi',array('Status'=>1),array('NoDokumen'=>$id));

    			  $data = $this->invoice_non_reservasi_model->get_by_id($id);

    			  $info_customer = $this->globalmodel->getQuery(" * FROM invoice_non_reservasi_customer a WHERE a.NoDokumen='".$data->NoReservasi."'");

    			  //update trans_reservasi status_invoice
    			  $this->db->update('trans_reservasi',array('status_invoice'=>1),array('NoDokumen'=>$data->NoReservasi));

    	              //insert juga ke tabel Piutang
	    	          $data_piutang = array('NoDokumen'=>$data->NoDokumen,
									        'NoFaktur'=>$data->NoDokumen,
									        'TglTransaksi'=>$data->Tanggal,
									        'KdCustomer'=>$info_customer[0]['Customer'],
									        'TipeTransaksi'=>'INR',
									        'JatuhTempo'=>$data->Jatuh_Tempo,
									        'NilaiTransaksi'=>$data->Total_Nilai,
									        'Sisa'=>$data->Total_Nilai);
    	               $this->db->insert("piutang", $data_piutang);

    	               $pisah = explode('-',$data->Tanggal);
    	               //insert juga ke tabel Mutasi Piutang
	    	          $data_mutasi_piutang = array('Tahun'=>$pisah[0],
									        'Bulan'=>$pisah[1],
									        'NoDokumen'=>$data->NoDokumen,
									        'Tanggal'=>$data->Tanggal,
									        'KdCustomer'=>$info_customer[0]['Customer'],
									        'Jumlah'=>$data->Total_Nilai,
									        'TipeTransaksi'=>'INR',
									        'MataUang'=>'IDR-1',
									        'Kurs'=>'1');
    	               $this->db->insert("mutasi_piutang", $data_mutasi_piutang);

    	echo json_encode(array("status" => TRUE));
    }


     function void_invoice()
    {
    			  $mylib = new globallib();

    			    $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;



    			  $this->db->delete('piutang',array('NoDokumen'=>$id));
    			  $this->db->delete('mutasi_piutang',array('NoDokumen'=>$id));

    			  $getNoStruk= $this->globalmodel->getQuery(" DISTINCT(a.NoStruk) AS NoStruk FROM invoice_non_reservasi_detail a WHERE a.NoDokumen='$id'");

    			  for($x=0;$x<count($getNoStruk);$x++){
    			  	if($getNoStruk[$x]['NoStruk']!="-" OR $getNoStruk[$x]['NoStruk']!=""){
						$this->db->update('transaksi_header',array('statusinvoice'=>0),array('NoStruk'=>$getNoStruk[$x]['NoStruk']));
					}
				  }

    			  $this->db->delete('invoice_reservasi',array('NoDokumen'=>$id));
    			  $this->db->delete('invoice_reservasi_customer',array('NoDokumen'=>$id));
    			  $this->db->delete('invoice_reservasi_detail',array('NoDokumen'=>$id));

    	echo json_encode(array("status" => TRUE));
    }


    function hapus_invoice()
    {
    			  $mylib = new globallib();

    			    $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;

    			  //update status di invoice reservasi
    			  $this->db->delete('invoice_non_reservasi',array('NoDokumen'=>$id));
    			  $this->db->delete('invoice_non_reservasi_customer',array('NoDokumen'=>$id));
    			  $this->db->delete('invoice_non_reservasi_detail',array('NoDokumen'=>$id));

    			  echo json_encode(array("status" => TRUE));
    }


    function delete_detail_tambahan()
    {
    			  $mylib = new globallib();

    			    $id_tambahan = $this->uri->segment(4);

					$this->db->delete('invoice_reservasi_detail',array('id'=>$id_tambahan));

    	echo json_encode(array("status" => TRUE));
    }

    function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln )
    {
        $query = "SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
        AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$thn.$bln.$kode."'
        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        $col_primary_ok = '';
        if(count($row)>0){
			list($col_primary_ok) = $row;
		}

        $counter = ((substr($col_primary_ok, 7, 4) * 1)+1)*3 + 2 * 3;
        //$counter = (substr($col_primary_ok, 10, 1) * 3) + 2 * 3;
        $counter_fa = $thn.$bln.sprintf($kode. sprintf("%04s", $counter));
        return $counter_fa;

    }

    function ajax_travel(){
		$var = $this->input->post('id');
		$query = $this->invoice_non_reservasi_model->getTravelList($var);

	 echo "<option value=''> -- Pilih Travel --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[KdTravel]>$cetak[Nama]</option>";

	    }
    }

    public function ajax_cek() {
        //$this->_validate();
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $user = $this->session->userdata('username');
        $mylib = new globallib();

        $detail_struk= $this->input->post('detail_struk');
        $pisah_detail_struk= explode("#",$detail_struk);
        $tanda_pager = substr_count($detail_struk, "#");

        $strk="";
        for($x=0; $x<($tanda_pager*1); $x++){
        	if(($x+1)==($tanda_pager*1)){
				$koma="";
			}else{
				$koma=",";
			}
			$strk.="'".$pisah_detail_struk[$x]."'".$koma;
		}

                                        //    $zql = "   a.`NoStruk`,
                                        //    			  a.Tanggal,
                                        //               c.Tanggal as TglTrans,
										// 			  a.`PCode`,
										// 			  b.`NamaLengkap`,
										// 			  a.`Qty`,
										// 			  a.`Harga`,
										// 			  a.`Disc1`,
										// 			  IF(a.NoKassa='51',a.`Netto` * 1.155,IF(a.NoKassa='54',a.`Netto` * 1.155,IF(a.NoKassa='62',a.`Netto` * 1.155,a.`Netto`))) AS Netto,
										// 			  c.`TotalNilai` AS TotalNilai
										// 			FROM
										// 			  transaksi_detail a
										// 			  INNER JOIN masterbarang b
										// 			    ON a.`PCode` = b.`PCode`
										// 			  INNER JOIN transaksi_header c
										// 			    ON a.`NoStruk` = c.`NoStruk`
										// 			WHERE a.`Netto`<>0 AND c.`statusinvoice` = '0' AND a.`NoStruk` IN ($strk)
                                        //             UNION ALL
                                        //             SELECT a.`NoStruk`,
                                        //               a.Tanggal,
                                        //               c.Tanggal as TglTrans,
                                        //               a.`PCode`,
                                        //               b.`NamaLengkap`,
                                        //               a.`Qty`,
                                        //               a.`Harga`,
                                        //               a.`Disc1`,
                                        //               a.`Netto`,
                                        //               c.`TotalNilai` AS TotalNilai
                                        //             FROM
                                        //               transaksi_detail_sunset a
                                        //               INNER JOIN masterbarang b
                                        //                 ON a.`PCode` = b.`PCode`
                                        //               INNER JOIN transaksi_header_sunset c
                                        //                 ON a.`NoStruk` = c.`NoStruk`
                                        //             WHERE a.`Netto`<>0 AND c.`statusinvoice` = '0' AND a.`NoStruk` IN ($strk)
                                        //             UNION ALL 
                                        //             (
                                        //             SELECT 
                                        //               a.`notrans` AS NoStruk ,
                                        //               a.`add_date` AS Tanggal,
                                        //               c.tanggal AS TglTrans,
                                        //               a.`PCode`,
                                        //               b.`NamaLengkap`,
                                        //               SUM(a.`qty`) AS Qty,
                                        //               a.`harga` AS Harga,
                                        //               0 AS Disc1,
                                        //               SUM(a.`qty`)*a.`Harga` AS Netto,
                                        //               SUM(a.`qty`)*a.`Harga` AS TotalNilai
                                        //               FROM `ticket` a INNER JOIN `masterbarang` b ON a.`PCode` = b.`PCode`
                                        //               INNER JOIN `ticket_head` c ON a.`notrans` = c.`notrans`
                                        //               WHERE a.`harga_asli` <> 0
                                        //               AND c.`statusinvoice` = '0'
                                        //               AND a.`notrans` IN ($strk)
                                        //               GROUP BY a.`PCode`
                                        //             )
                                                     
                                        //             ;
                                        //             ";
                                                    
        $zql = "
                c.`NoKassa`,
                d.`PriceIncludeTax`,
                a.`NoStruk`,
                a.Tanggal,
                c.Tanggal AS TglTrans,
                a.`PCode`,
                b.`NamaLengkap`,
                a.`Qty`,
                a.`Harga`,
                a.`Disc1`,
                IF(
                    d.`PriceIncludeTax` = 'Y',
                    a.`Netto` * 1.155,
                    a.`Netto`
                ) AS Netto,
                c.`TotalNilai` AS TotalNilai
                FROM
                transaksi_detail a
                INNER JOIN masterbarang b
                    ON a.`PCode` = b.`PCode`
                INNER JOIN transaksi_header c
                    ON a.`NoStruk` = c.`NoStruk`
                INNER JOIN kassa d
                    ON c.`NoKassa` = d.`id_kassa`
                WHERE a.`Netto` <> 0
                AND c.`statusinvoice` = '0'
                AND a.`NoStruk` IN ($strk)
                UNION
                ALL
                SELECT
                c.`NoKassa`,
                d.`PriceIncludeTax`,
                a.`NoStruk`,
                a.Tanggal,
                c.Tanggal AS TglTrans,
                a.`PCode`,
                b.`NamaLengkap`,
                a.`Qty`,
                a.`Harga`,
                a.`Disc1`,
                a.`Netto`,
                c.`TotalNilai` AS TotalNilai
                FROM
                transaksi_detail_sunset a
                INNER JOIN masterbarang b
                    ON a.`PCode` = b.`PCode`
                INNER JOIN transaksi_header_sunset c
                    ON a.`NoStruk` = c.`NoStruk`
                INNER JOIN kassa d
                    ON c.`NoKassa` = d.`id_kassa`
                WHERE a.`Netto` <> 0
                AND c.`statusinvoice` = '0'
                AND a.`NoStruk` IN ($strk)
                UNION
                ALL
                (SELECT
                c.last_ip AS NoKassa,
                d.`PriceIncludeTax`,
                a.`notrans` AS NoStruk,
                a.`add_date` AS Tanggal,
                c.tanggal AS TglTrans,
                a.`PCode`,
                b.`NamaLengkap`,
                SUM(a.`qty`) AS Qty,
                a.`harga` AS Harga,
                0 AS Disc1,
                SUM(a.`qty`) * a.`Harga` AS Netto,
                SUM(a.`qty`) * a.`Harga` AS TotalNilai
                FROM
                `ticket` a
                INNER JOIN `masterbarang` b
                    ON a.`PCode` = b.`PCode`
                INNER JOIN `ticket_head` c
                    ON a.`notrans` = c.`notrans`
                    INNER JOIN kassa d
                    ON c.last_ip = d.`id_kassa`
                WHERE a.`harga_asli` <> 0
                AND c.`statusinvoice` = '0'
                AND a.`notrans` IN ($strk)
                GROUP BY a.`PCode`);
                ";
        //echo $zql;die; 
        //c.`ttl_amount` AS TotalNilai
        $menu = $this->globalmodel->getQuery($zql);


        if (empty($menu)){
		?>

							<tr>
								<td colspan='100%' align='center'>Tidak Ada Data</td>

							</tr>
							<tr class="title_table">
							    <td colspan="4" align="left">Total</td>
								<td align='right'> <?php echo number_format(0);?> <input type="hidden" id="totalan" name="totalan" value="<?php echo number_format(0);?>"> <input type="hidden" id="totalan2" name="totalan2" value="<?php echo '0';?>"></td>
								<td align='right'> <?php echo number_format(0);?> <input type="hidden" id="totalan" name="totalan" value="<?php echo number_format(0);?>"> <input type="hidden" id="totalan2" name="totalan2" value="<?php echo '0';?>"></td>
								<td align='right'> <?php echo number_format(0);?> <input type="hidden" id="totalan" name="totalan" value="<?php echo number_format(0);?>"> <input type="hidden" id="totalan2" name="totalan2" value="<?php echo '0';?>"></td>
								<td align='right'> <?php echo number_format(0);?> <input type="hidden" id="totalan" name="totalan" value="<?php echo number_format(0);?>"> <input type="hidden" id="totalan2" name="totalan2" value="<?php echo '0';?>"></td>

							</tr>
							";

		<?php }else{
		$no=1;
		$grnttl=0;
		$netttl=0;
		$disttl=0;
	    foreach ($menu as $cetak) {
	    ?>
                    
	    					<tr>
                                
							    <td align='center'><?php echo $no;?></td>
							    <td align='left' style="display: none;"> <input style="text-align: center;width:90px;" type="text"  class="form-control-new" readonly  id="tanggal<?php echo $no;?>" name="tanggal[]" value="<?php echo strtoupper($cetak['Tanggal']);?>"> </td>
								<td align='left'> <input style="text-align: center;width:90px;" type="text"  class="form-control-new" readonly  id="nostruk<?php echo $no;?>" name="nostruk[]" value="<?php echo strtoupper($cetak['NoStruk']);?>"> </td>
								<td align='left'> <input style="text-align: left;width:90px;" type="text"  class="form-control-new" readonly  id="pcode<?php echo $no;?>" name="pcode[]" value="<?php echo strtoupper($cetak['PCode']);?>"> </td>
								<td align='left'> <input style="text-align: left;width:225px;" type="text"  class="form-control-new" readonly  id="namabrg<?php echo $no;?>" name="namabrg[]" value="<?php echo strtoupper($cetak['NamaLengkap']);?>"> </td>
								<td align='center'> <input type="text" class="form-control-new" readonly  id="qty<?php echo $no;?>" name="qty[]" style="text-align: right;width:30px;" value="<?php echo $cetak['Qty'];?>"></td>
								<td align="right"> <input style="text-align: right;width:80px;" type="text" class="form-control-new" readonly  id="harga<?php echo $no;?>" name="harga[]" value="<?php echo $cetak['Harga'];?>"> </td>
								<td align="right"> <input style="text-align: right;width:80px;" type="text" class="form-control-new" readonly  id="disc<?php echo $no;?>" name="disc[]" value="<?php echo $cetak['Disc1'];?>"> </td>
							    <td align="right"> <input style="text-align: right;width:80px;" type="text" class="form-control-new" readonly  id="netto<?php echo $no;?>" name="netto[]" value="<?php echo $cetak['Netto'];?>"> <input style="text-align: right" type="hidden" class="form-control-new" readonly  id="total_hidden<?php echo $no;?>" name="total_hidden[]" value="<?php echo round($hrga*$cetak['Qty']);?>"> </td>

							</tr>";
                            <script type="text/javascript">
                        $('#tgltrans').val('<?php echo strtoupper($cetak['TglTrans']);?>');
                    </script>
		<?php $grnttl+=round($cetak['Qty']);$no++;
		      $netttl+=round($cetak['Netto']);
          $totttl=round($cetak['TotalNilai']);
		      $disttl+=round($cetak['Disc1']);} ?>

		                    <tr class="title_table">
							    <td colspan="4" align="left">Total</td>
								<td align='right'> <?php echo number_format($grnttl);?> <input type="hidden" id="totalanqty" name="totalanqty" value="<?php echo number_format($grnttl);?>"> <input type="hidden" id="totalanqty2" name="totalanqty2" value="<?php echo $grnttl;?>"></td>
								<td align='right'> &nbsp;</td>

								<td align='right'> <?php echo number_format($disttl);?> <input type="hidden" id="totalandisc" name="totalandisc" value="<?php echo number_format($disttl);?>"> <input type="hidden" id="totalandisc2" name="totalandisc2" value="<?php echo $disttl;?>"></td>

							   <td align='right'> <?php echo number_format($totttl);?> <input type="hidden" id="totalannet" name="totalannet" value="<?php echo number_format($totttl);?>"> <input type="hidden" id="totalannet2" name="totalannet2" value="<?php echo round($totttl);?>"></td>

							</tr>
		<?php }

    }


    function getDataReservasi()
    {
	     $travel = $this->input->post('trv');
	     $query = $this->invoice_non_reservasi_model->getDataReservasi($travel);

	      echo "<option value=''> -- Pilih BEO --</option>";
	      foreach ($query as $cetak) {
		  echo "<option value=$cetak[NoDokumen]>$cetak[NoDokumen]</option>";
		  }
    }

	function create_pdf() {

		            $sgv= $this->uri->segment(4);
			        $inv = $this->uri->segment(5);
			        $ar= $this->uri->segment(6);
			        $thn= $this->uri->segment(7);
			        $rmw= $this->uri->segment(8);
			        $cont = $this->uri->segment(9);
			        $id = $sgv."/".$inv."/".$ar."/".$thn."/".$rmw."/".$cont;

        $NoTransaksi = $id;
        //echo $NoTransaksi;die;

        $data['getHeader'] = $this->invoice_non_reservasi_model->getTravelDetail($NoTransaksi);
		$data['getDetail'] = $this->invoice_non_reservasi_model->getDetailInvoice($NoTransaksi);
		$data['getTambahan'] = $this->invoice_non_reservasi_model->getBiayaTambahan($NoTransaksi);
        $html = $this->load->view('transaksi/invoice_non_reservasi/pdf_invoice_non_reservasi', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_komisi.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }

}

?>
