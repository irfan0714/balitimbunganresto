<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pi_marketing extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/pi_marketing_model');
        $this->load->model('proses/posting_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_supplier"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "pi_marketing", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_supplier = explode("=", $arrSearch[2]); // search supplier
                    $arr_search["search"]["supplier"] = $search_supplier[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_supplier"] = $search_supplier[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/pi_marketing/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/pi_marketing/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

			$data['supplier'] = $this->pi_marketing_model->getSupplier();

            $config['total_rows'] = $this->pi_marketing_model->num_pi_marketing_row($arr_search["search"]);
            $data['data'] = $this->pi_marketing_model->getPiMarketingList($config['per_page'], $page, $arr_search["search"]);
			//$data['data'] = $this->pi_marketing_model->getDeliveryOrderList($config['per_page'], 0, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/pi_marketing/pi_marketing_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'pi_marketing', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_supplier=".$this->input->post('search_supplier');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pi_marketing",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pi_marketing/index/' . $query_id . '');
    }

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
			
			//hapus dulu di detail_temp
			$this->db->delete('pi_marketing_detail_temp',array('NoDokumen'=>'000000','AddUser'=>$user));
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            $data['supplier'] = $this->pi_marketing_model->getSupplier();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pi_marketing/add_pi_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_pi_marketing($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->pi_marketing_model->getHeader($id);
			$data['header_'] = $this->pi_marketing_model->getHeaderJml($id);			
            $data['supplier'] = $this->pi_marketing_model->getSupplier();
			$data['subdivisi'] = $this->pi_marketing_model->getSubdivisi();
            $data['detail_list'] = $this->pi_marketing_model->getDetailList($id);
			$data['detail_list_akun'] = $this->pi_marketing_model->getDetailAkunList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pi_marketing/edit_pi_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_pi_marketing($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->pi_marketing_model->getHeader($id);
			$data['header_'] = $this->pi_marketing_model->getHeaderJml($id);			
            $data['supplier'] = $this->pi_marketing_model->getSupplier();
			$data['subdivisi'] = $this->pi_marketing_model->getSubdivisi();
            $data["detail_list"] = $this->pi_marketing_model->getDetailList($id);
			$data["detail_list_akun"] = $this->pi_marketing_model->getDetailAkunList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pi_marketing/view_pi_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit_jurnal_pi_marketing($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->pi_marketing_model->getHeader($id);
			$data['header_'] = $this->pi_marketing_model->getHeaderJml($id);			
            $data['supplier'] = $this->pi_marketing_model->getSupplier();
			$data['subdivisi'] = $this->pi_marketing_model->getSubdivisi();
            $data["detail_list"] = $this->pi_marketing_model->getDetailList($id);
			$data["detail_list_akun"] = $this->pi_marketing_model->getDetailAkunList($id);
            $data['track'] = $mylib->print_track();
            $data['KunciTotal'] = $this->pi_marketing_model->getKunciTotal($id);
            $this->load->view('transaksi/pi_marketing/edit_jurnal_pi_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $rgno = $this->input->post('rgno');
        $nopro = $this->input->post('nopro');
        $v_supplier = $this->input->post('v_supplier');
        $v_note = $this->input->post('v_note');
        $flag = $this->input->post('flag');		
        $user = $this->session->userdata('username');
        		
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
        
        // detail pi
        $v_nmbarang1 = $this->input->post('v_nmbarang');
        $v_Qty1 = $this->input->post('v_Qty');
        $v_Harga1 = $this->input->post('v_Harga');
        $v_PPn1 = $this->input->post('v_PPn');
        $v_subtotal1 = $this->input->post('v_subtotal');
		$v_sJumlah1 = $this->input->post('v_sJumlah');
		
		//total sebelum PPn
		$v_Jumlah = $this->input->post('v_Jumlah');
		//ppn
		$v_NilaiPPn = $this->input->post('v_NilaiPPn');
		//total setelah PPn
		$v_Total = $this->input->post('v_Total');
		
		
		//detail rek
		$v_kd_rek1 = $this->input->post('v_kd_rek');
        $v_subdivisi1 = $this->input->post('v_subdivisi');
        $v_deskripsi1 = $this->input->post('v_deskripsi');
        $v_jml1 = $this->input->post('v_jml');

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Sales Return
        	$v_no_dokumen = $mylib->get_code_counter2($this->db->database, "pi_marketing","NoDokumen", "PIM", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di Sales Return Header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen),$rgno, $nopro, $v_note, $v_supplier, $user);
            
			//update detail
			for ($x = 0; $x < count($v_nmbarang1); $x++) 
	        {
				
				$v_nmbarang = $v_nmbarang1[$x];
				$v_Qty = $v_Qty1[$x];
				$v_Harga = $v_Harga1[$x];
				$v_PPn = $v_PPn1[$x];
				$v_subtotal = $v_subtotal1[$x];
				$v_sJumlah = $v_sJumlah1[$x];
				
				if($v_nmbarang!=""){
				$data_detail=array(
								'NoDokumen'=>$v_no_dokumen,
								'NoUrut'=>$x+1,
								'NamaBarang'=>$v_nmbarang,
								'Qty'=>$v_Qty,
								'HargaSatuan'=>$v_Harga,
								'Jumlah'=>$v_subtotal,
								'PPN'=>$v_PPn,
								'Total'=>$v_sJumlah
								 );
				$this->db->insert('pi_marketing_detail',$data_detail);
			   }
			   
			}
			
			//detail rek
			for ($y = 0; $y < count($v_kd_rek1); $y++) 
	        {
				
				$v_kd_rek = $v_kd_rek1[$y];
				$v_subdivisi = $v_subdivisi1[$y];
				$v_deskripsi = $v_deskripsi1[$y];
				$v_jml = $v_jml1[$y];
				
				if($v_kd_rek!=""){
				$data_detail_akun=array(
								'NoDokumen'=>$v_no_dokumen,
								'NoUrut'=>$y+1,
								'KdRekening'=>$v_kd_rek,
								'KdSubdivisi'=>$v_subdivisi,
								'Deskripsi'=>$v_deskripsi,
								'Jumlah'=>$v_jml
								 );
				$this->db->insert('pi_marketing_detail_akun',$data_detail_akun);
			   }
			   
			}
			
			//hapus dulu di detail_temp
			$this->db->delete('pi_marketing_detail_temp',array('NoDokumen'=>'000000','AddUser'=>$user));
									
		} 
		
		
		else if ($flag == "edit") 
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			$v_status = $this->input->post('v_status');
			
			//update deliveyorder
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen),$rgno, $nopro, $v_note, $v_supplier, $v_Jumlah,$v_NilaiPPn,$v_Total,$v_status,  $user);
            
			//update detail
			for ($x = 0; $x < count($v_nmbarang1); $x++) 
	        {
				
				$v_nmbarang = $v_nmbarang1[$x];
				$v_Qty = $v_Qty1[$x];
				$v_Harga = $v_Harga1[$x];
				$v_PPn = $v_PPn1[$x];
				$v_subtotal = $v_subtotal1[$x];
				$v_sJumlah = $v_sJumlah1[$x];
				
				if($v_nmbarang!=""){
				$data_detail=array(
								'HargaSatuan'=>$v_Harga,
								'Jumlah'=>$v_subtotal,
								'PPn'=>$v_PPn,
								'Total'=>$v_sJumlah
								 );
				$this->db->update('pi_marketing_detail',$data_detail,array('NoDokumen'=>$v_no_dokumen,'NoUrut'=>$x+1));
			   }
			   
			}
			
			//detail rek
			for ($y = 0; $y < count($v_kd_rek1); $y++) 
	        {
				
				$v_kd_rek = $v_kd_rek1[$y];
				$v_subdivisi = $v_subdivisi1[$y];
				$v_deskripsi = $v_deskripsi1[$y];
				$v_jml = $v_jml1[$y];
				
				if($v_kd_rek!=""){
				$data_detail_akun=array(
								'NoDokumen'=>$v_no_dokumen,
								'NoUrut'=>$y+1,
								'KdRekening'=>$v_kd_rek,
								'KdSubdivisi'=>$v_subdivisi,
								'Deskripsi'=>$v_deskripsi,
								'Jumlah'=>$v_jml
								 );
				$this->db->insert('pi_marketing_detail_akun',$data_detail_akun);
			   }
			   
			}
			
			//cek apakah status sudah close = 1
			
			if($v_status=='1'){
				//update budget realisasi
				//pertama ambil nilai total dari proposal_detail dan proposal_target
				
				$bln = date('m');
				$thn = date('Y');
				$var  = "Realisasi".$bln;
								
				//cari KdAktivitas
				$aktivitas = $this->pi_marketing_model->getNilaiPro($nopro);
				
				//ambil nilai realisasi di bulan tertentu dan tambahkan dengan yang baru
				$budgeting = $this->pi_marketing_model->getBudgeting($aktivitas->KdAktivitas,$var);
				$tmbh_jmlh = $v_Jumlah + $budgeting->jml_realisasi;
				
				$data_bgt=array(
				$var=>$tmbh_jmlh
				);
				$where_bgt=array(
				'Tahun'=>$thn,
				'KdAktivitas'=>$aktivitas->KdAktivitas
				);
				//update budget terpakai
				$this->db->update('budget',$data_bgt,$where_bgt);
				
				//update FlagKonfirmasi di RGM
				$this->db->update('rg_marketing',array('FlagKonfirmasi'=>'1'),array('NoDokumen'=>$rgno));
				
				
				// ============================================================================================
				
				//ambil kode Supplier dan ppn
				$sql ="SELECT a.`KdSupplier`,a.`PPn` FROM `pi_marketing` a WHERE a.`NoDokumen`='".$v_no_dokumen."'";
			    $rek = $this->pi_marketing_model->Querydata($sql);
				foreach($rek as $v)
				{
				$ppn		=$v['PPN'];	
				$kdsupplier	=$v['KdSupplier'];	
				}
		        $tgl = $mylib->ubah_tanggal($v_tgl_dokumen);
				$tgljt = $this->pi_marketing_model->getjatuhtempo($tgl, $kdsupplier);
				$interface = $this->pi_marketing_model->getinterface();
				
				//table hutang
				$datahutang	= array(
								'NoDokumen'			=> $v_no_dokumen,
								'NoFaktur'			=> $v_no_dokumen,		
								'KdSupplier'		=> $kdsupplier,
								'TipeTransaksi'		=> 'IM', 			
								'Tanggal'			=> $tgl,	
								'JatuhTempo'		=> $tgljt,	
								'NilaiTransaksi'	=> $v_Total,	
								'Sisa'				=> $v_Total,	
								'Kurs'				=> 1, 			
								'MataUang'			=> 'IDR',
								'RGNo'				=> $rgno,
								'KdRekening'        => $interface[0]['HutangLain']
							);
															
		        $this->db->insert('hutang', $datahutang);
				
				//table mutasi_hutang
				list($tahun, $bulan, $tg) = explode('-', $tgl);
				$datamutasihutang	= array(
										'Tahun'				=> $tahun,
										'Bulan'				=> $bulan,
										'Tanggal'			=> $tgl,
										'NoDokumen'			=> $v_no_dokumen,	
										'KdSupplier'		=> $kdsupplier,			
										'MataUang'			=> 'IDR',				
										'Kurs'				=> 1, 				
										'Jumlah'			=> $v_Total,    	
										'SelisihKurs'		=> 0, 					
										'TipeTransaksi'		=> 'IM' 							
									);					
									
		        $this->db->insert('mutasi_hutang', $datamutasihutang);
						
						
				// ============================================================================================
				
				
			}
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/pi_marketing/edit_pi_marketing/' . $v_no_dokumen . '');
    }
    
    function save_data_edit_jurnal() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $rgno = $this->input->post('rgno');
        $nopro = $this->input->post('nopro');
        $v_supplier = $this->input->post('v_supplier');
        $v_note = $this->input->post('v_note');
        $flag = $this->input->post('flag');		
        $user = $this->session->userdata('username');
        $userid = $this->session->userdata('userid');
        		
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
        
        // detail pi
        $v_nmbarang1 = $this->input->post('v_nmbarang');
        $v_Qty1 = $this->input->post('v_Qty');
        $v_Harga1 = $this->input->post('v_Harga');
        $v_PPn1 = $this->input->post('v_PPn');
        $v_subtotal1 = $this->input->post('v_subtotal');
		$v_sJumlah1 = $this->input->post('v_sJumlah');
		
		//total sebelum PPn
		$v_Jumlah = $this->input->post('v_Jumlah');
		//ppn
		$v_NilaiPPn = $this->input->post('v_NilaiPPn');
		//total setelah PPn
		$v_Total = $this->input->post('v_Total');
		
		
		//detail rek
		$v_kd_rek1 = $this->input->post('v_kd_rek');
        $v_subdivisi1 = $this->input->post('v_subdivisi');
        $v_deskripsi1 = $this->input->post('v_deskripsi');
        $v_jml1 = $this->input->post('v_jml');

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Sales Return
        	$v_no_dokumen = $mylib->get_code_counter2($this->db->database, "pi_marketing","NoDokumen", "PIM", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di Sales Return Header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen),$rgno, $nopro, $v_note, $v_supplier, $user);
            
			//update detail
			for ($x = 0; $x < count($v_nmbarang1); $x++) 
	        {
				
				$v_nmbarang = $v_nmbarang1[$x];
				$v_Qty = $v_Qty1[$x];
				$v_Harga = $v_Harga1[$x];
				$v_PPn = $v_PPn1[$x];
				$v_subtotal = $v_subtotal1[$x];
				$v_sJumlah = $v_sJumlah1[$x];
				
				if($v_nmbarang!=""){
				$data_detail=array(
								'NoDokumen'=>$v_no_dokumen,
								'NoUrut'=>$x+1,
								'NamaBarang'=>$v_nmbarang,
								'Qty'=>$v_Qty,
								'HargaSatuan'=>$v_Harga,
								'Jumlah'=>$v_subtotal,
								'PPN'=>$v_PPn,
								'Total'=>$v_sJumlah
								 );
				$this->db->insert('pi_marketing_detail',$data_detail);
			   }
			   
			}
			
			//detail rek
			for ($y = 0; $y < count($v_kd_rek1); $y++) 
	        {
				
				$v_kd_rek = $v_kd_rek1[$y];
				$v_subdivisi = $v_subdivisi1[$y];
				$v_deskripsi = $v_deskripsi1[$y];
				$v_jml = $v_jml1[$y];
				
				if($v_kd_rek!=""){
				$data_detail_akun=array(
								'NoDokumen'=>$v_no_dokumen,
								'NoUrut'=>$y+1,
								'KdRekening'=>$v_kd_rek,
								'KdSubdivisi'=>$v_subdivisi,
								'Deskripsi'=>$v_deskripsi,
								'Jumlah'=>$v_jml
								 );
				$this->db->insert('pi_marketing_detail_akun',$data_detail_akun);
			   }
			   
			}
			
			//hapus dulu di detail_temp
			$this->db->delete('pi_marketing_detail_temp',array('NoDokumen'=>'000000','AddUser'=>$user));
									
		} 
		
		
		else if ($flag == "edit") 
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			$v_status = $this->input->post('v_status');
			
			//update deliveyorder
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen),$rgno, $nopro, $v_note, $v_supplier, $v_Jumlah,$v_NilaiPPn,$v_Total,$v_status,  $user);
            
			//update detail
			for ($x = 0; $x < count($v_nmbarang1); $x++) 
	        {
				
				$v_nmbarang = $v_nmbarang1[$x];
				$v_Qty = $v_Qty1[$x];
				$v_Harga = $v_Harga1[$x];
				$v_PPn = $v_PPn1[$x];
				$v_subtotal = $v_subtotal1[$x];
				$v_sJumlah = $v_sJumlah1[$x];
				
				if($v_nmbarang!=""){
				$data_detail=array(
								'HargaSatuan'=>$v_Harga,
								'Jumlah'=>$v_subtotal,
								'PPn'=>$v_PPn,
								'Total'=>$v_sJumlah
								 );
				$this->db->update('pi_marketing_detail',$data_detail,array('NoDokumen'=>$v_no_dokumen,'NoUrut'=>$x+1));
			   }
			   
			}
			
			//detail rek
			for ($y = 0; $y < count($v_kd_rek1); $y++) 
	        {
				
				$v_kd_rek = $v_kd_rek1[$y];
				$v_subdivisi = $v_subdivisi1[$y];
				$v_deskripsi = $v_deskripsi1[$y];
				$v_jml = $v_jml1[$y];
				
				if($v_kd_rek!=""){
				$data_detail_akun=array(
								'NoDokumen'=>$v_no_dokumen,
								'NoUrut'=>$y+1,
								'KdRekening'=>$v_kd_rek,
								'KdSubdivisi'=>$v_subdivisi,
								'Deskripsi'=>$v_deskripsi,
								'Jumlah'=>$v_jml
								 );
				$this->db->insert('pi_marketing_detail_akun',$data_detail_akun);
			   }
			   
			}
			
			//cek apakah status sudah close = 1
			
			if($v_status=='1'){
				//update budget realisasi
				//pertama ambil nilai total dari proposal_detail dan proposal_target
				
				$bln = date('m');
				$thn = date('Y');
				$var  = "Realisasi".$bln;
								
				//cari KdAktivitas
				$aktivitas = $this->pi_marketing_model->getNilaiPro($nopro);
				
				//ambil nilai realisasi di bulan tertentu dan tambahkan dengan yang baru
				$budgeting = $this->pi_marketing_model->getBudgeting($aktivitas->KdAktivitas,$var);
				$tmbh_jmlh = $v_Jumlah + $budgeting->jml_realisasi;
				
				$data_bgt=array(
				$var=>$tmbh_jmlh
				);
				$where_bgt=array(
				'Tahun'=>$thn,
				'KdAktivitas'=>$aktivitas->KdAktivitas
				);
				//update budget terpakai
				$this->db->update('budget',$data_bgt,$where_bgt);
				
				//update FlagKonfirmasi di RGM
				$this->db->update('rg_marketing',array('FlagKonfirmasi'=>'1'),array('NoDokumen'=>$rgno));
				
				
				// ============================================================================================
				
				//ambil kode Supplier dan ppn
				$sql ="SELECT a.`KdSupplier`,a.`PPn` FROM `pi_marketing` a WHERE a.`NoDokumen`='".$v_no_dokumen."'";
			    $rek = $this->pi_marketing_model->Querydata($sql);
				foreach($rek as $v)
				{
				$ppn		=$v['PPN'];	
				$kdsupplier	=$v['KdSupplier'];	
				}
		        $tgl = $mylib->ubah_tanggal($v_tgl_dokumen);
				$tgljt = $this->pi_marketing_model->getjatuhtempo($tgl, $kdsupplier);
				$interface = $this->pi_marketing_model->getinterface();
				
				//table hutang
				$datahutang	= array(
								'NoDokumen'			=> $v_no_dokumen,
								'NoFaktur'			=> $v_no_dokumen,		
								'KdSupplier'		=> $kdsupplier,
								'TipeTransaksi'		=> 'IM', 			
								'Tanggal'			=> $tgl,	
								'JatuhTempo'		=> $tgljt,	
								'NilaiTransaksi'	=> $v_Total,	
								'Sisa'				=> $v_Total,	
								'Kurs'				=> 1, 			
								'MataUang'			=> 'IDR',
								'RGNo'				=> $rgno,
								'KdRekening'        => $interface[0]['HutangLain']
							);
															
		        $this->db->insert('hutang', $datahutang);
				
				//table mutasi_hutang
				list($tahun, $bulan, $tg) = explode('-', $tgl);
				$datamutasihutang	= array(
										'Tahun'				=> $tahun,
										'Bulan'				=> $bulan,
										'Tanggal'			=> $tgl,
										'NoDokumen'			=> $v_no_dokumen,	
										'KdSupplier'		=> $kdsupplier,			
										'MataUang'			=> 'IDR',				
										'Kurs'				=> 1, 				
										'Jumlah'			=> $v_Total,    	
										'SelisihKurs'		=> 0, 					
										'TipeTransaksi'		=> 'IM' 							
									);					
									
		        $this->db->insert('mutasi_hutang', $datamutasihutang);
						
						
				// ============================================================================================
				
				
			}
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }
        
        //posting jurnal juga
        $tglx = $mylib->ubah_tanggal($v_tgl_dokumen);
        list($tahuny, $bulany, $tgy) = explode('-', $tglx);
		$noreferensi = $this->pi_marketing_model->getNoRefernsi($v_no_dokumen);
		$this->posting_pembelian_non_stok($tahuny,$bulany,$userid,$v_no_dokumen,$noreferensi->NoReferensi);
        
        redirect('/transaksi/pi_marketing/edit_jurnal_pi_marketing/' . $v_no_dokumen . '');
    }

    function insertNewHeader($v_no_dokumen, $v_tgl_dokumen,$rgno, $nopro, $v_note, $v_supplier, $user) 
    {
    	$this->pi_marketing_model->locktables('pi_marketing');

        $data = array
			        (
			        'NoDokumen' => $v_no_dokumen,
			        'TglDokumen' => $v_tgl_dokumen,
			        'RGNo' => $rgno,
			        'NoProposal' => $nopro,
			        'Keterangan' => $v_note,
			        'KdSupplier' => $v_supplier,
			        'adddate' =>date('Y-m-d'),
			        'adduser' => $user);

        $this->db->insert('pi_marketing', $data);

        $this->pi_marketing_model->unlocktables();

    }
    
    
  
    function updateHeader($v_no_dokumen, $v_tgl_dokumen,$rgno, $nopro, $v_note, $v_supplier, $v_Jumlah, $v_NilaiPPn,$v_Total,$v_status,  $user) 
    {
        $this->pi_marketing_model->locktables('pi_marketing');

        $data = array
			        (
			        'TglDokumen' => $v_tgl_dokumen,
			        'RGNo' => $rgno,
			        'NoProposal' => $nopro,
			        'Keterangan' => $v_note,
			        'KdSupplier' => $v_supplier,
					'Jumlah'=>$v_Jumlah,
					'NilaiPPn'=>$v_NilaiPPn,
					'Total'=>$v_Total,
					'Status'=>$v_status,
			        'AddDate' =>date('Y-m-d'),
			        'AddUser' => $user,
			        'EditDate' =>date('Y-m-d'),
			        'EditUser' => $user);

        $this->db->update('pi_marketing', $data, array('NoDokumen' => $v_no_dokumen));

        $this->pi_marketing_model->unlocktables();
    }

	function delete_trans($id) 
    {
			
			
            $this->db->delete('pi_marketing', array('NoDokumen' => $id));	

			
            $this->db->delete('pi_marketing_detail', array('NoDokumen' => $id));
			
			
			$this->db->delete('pi_marketing_detail_akun', array('NoDokumen' => $id));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/pi_marketing/');
    }

    function delete_detail() 
    {
	
        $nodok = $this->uri->segment(4);
		
			//delete detail akun
		    $this->db->delete('pi_marketing_detail_akun', array('NoDokumen'=>$nodok));
		    
		    //update header dengan menghapus edit user
			$data = array(
				'AddDate'=>'0000-00-00',
				'AddUser'=>''
			);
		    $this->db->update('pi_marketing', $data, array('NoDokumen'=>$nodok));
			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus Rekening berhasil', 'class' => 'success'));

        redirect('/transaksi/pi_marketing/edit_pi_marketing/' . $nodok . '');
    } 
    
    function delete_detail_edit_jurnal() 
    {
	
        $nodok = $this->uri->segment(4);
		
			//delete detail akun
		    $this->db->delete('pi_marketing_detail_akun', array('NoDokumen'=>$nodok));
		    //update header dengan menghapus edit user
			$data = array(
				'AddDate'=>'0000-00-00',
				'AddUser'=>''
			);
		    $this->db->update('pi_marketing', $data, array('NoDokumen'=>$nodok));
			
			//edit di tabel jurnal_detail juga
			$NoReferensi = $this->pi_marketing_model->getNoRefernsi($nodok);
			//delete jurnaldetail
			$this->db->delete('jurnaldetail', array('NoReferensi'=>$NoReferensi->NoReferensi));
			//delete hutang
			$this->db->delete('hutang', array('NoDokumen'=>$nodok));
			//delete mutasi_hutang
			$this->db->delete('mutasi_hutang', array('NoDokumen'=>$nodok));
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus Rekening berhasil', 'class' => 'success'));

        redirect('/transaksi/pi_marketing/edit_jurnal_pi_marketing/' . $nodok . '');
    }           

	function save_detail_temp($rgno) 
    {
		
		//kan sudah dapet no Purchase Request
			$user = $this->session->userdata('username');
			//kan sudah dapet no Purchase Request			
			$detail_rg_marketing = $this->pi_marketing_model->getRgMarketingDetail($rgno);
			$subdivisi = $this->pi_marketing_model->getSubdivisi();
		
			/*$z=1;
			foreach( $detail_rg_marketing AS $val){
			
				$data=array(
							'NoDokumen'=>'000000',
							'NoUrut'=>$z,
							'NamaBarang'=>$val['NamaBarang'],
							'Qty'=>$val['Qty'],
							'HargaSatuan'=>$val['HargaSatuan'],
							'Jumlah'=>$val['Jumlah'],
							'PPN'=>$val['PPN'],
							'Total'=>$val['Jumlah'],
							'AddUser'=>$user
							);	
				$this->db->insert('pi_marketing_detail_temp',$data);
			$z++;
			}
			
			$detail_list = $this->pi_marketing_model->getDetailListTemp('000000');*/
			
		    ?>
			<div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="entypo-list"></i> Purchase Invoice Detail</a></li>
                <li><a href="#tab2" data-toggle="tab"><i class="entypo-pencil"></i> Purchase Invoice Akun</a></li>
              </ul>
              <div class="tab-content">
			  <div class="tab-pane fade in active" id="tab1">
		            <div class="table-responsive">
				
				
					<table class="table table-bordered responsive" id="TabelDetail">
        				<thead class="title_table">
							<tr>
							    <th width="300"><center>Nama Barang</center></th>               
							    <th width="50"><center>Qty RG</center></th>
							    <th width="30"><center>Qty</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>PPn (%)</center></th>
							    <th width="100"><center>Sub Total</center></th>
							</tr>
						</thead>
						<tbody>
						<input type="hidden" name="grdTotal" id="grdTotal" value=""/>
						  <?php 
						  $Sid=1;
						  $totals = 0;
						  $tots_ppn = 0;
						  foreach($detail_rg_marketing as $val)
						  {?>
							
							  <tr>
								<td align="left"><?php echo $val["NamaBarang"]; ?><input type="hidden" name="v_nmbarang[]" value="<?php echo $val["NamaBarang"]; ?>"></td>
								<td align="center"><?php echo $val["Qty"]; ?></td>
								<td align="right"><input readonly style="text-align: right; width: 60px;" type="text" class="form-control-new" dir="rtl" name="v_Qty[]" id="v_Qty_<?php echo $Sid; ?>" value="<?php echo $val["Qty"]; ?>" ></td>
                                <td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal, total dan grand total." name="v_Harga[]" id="v_Harga_<?php echo $Sid; ?>" value="<?php echo $val["HargaSatuan"]; ?>" onkeydown="HitungHarga(event, 'harga', this);" dir="rtl"></td>
							  	<td align="right"><input style="text-align: right; width: 50px;" type="text" class="form-control-new" dir="rtl" name="v_PPn[]" id="v_PPn_<?php echo $Sid; ?>" onkeydown="HitungHarga2(event, 'harga', this);" onblur="HitungHarga3('harga', this);" value="<?php echo $val["PPN"]; ?>"></td>
							  	<td align="right"><input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_subtotal[]" id="v_subtotal_<?php echo $Sid; ?>" dir="rtl" value="<?php echo $val["Total"]; ?>" ></td>
								<td style="display: none"><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $Sid; ?>" dir="rtl" value="<?php echo $val['Total'] ?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
								<td style="display: none"><input type="text" name="ppn_[]" id="ppn_<?php echo $Sid; ?>" value="<?= $val["Jumlah"]*$val["PPN"]/100;?>" dir="rtl" class="form-control-new" readonly="readonly"/></td>
							  </tr>							
							<?php  
							//style="display: none"
							$Sid++; 
							$totals+=$val["Total"];
							$tots_ppn+=$val["Jumlah"]*$val["PPN"]/100;} ?>
						</tbody>	
						
						   <tr style="color: black; font-weight: bold;">
                                <td colspan="4" rowspan="3">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL
                                
                                </td>
                                <td style="text-align: right;"><input dir="rtl" readonly style="text-align: right;" class="form-control-new" type="text" name="v_Jumlah" id="v_Jumlah" value="<?php echo $totals;?>"></td>
                            </tr>
                            
                            <!--<tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC<!--<input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" onchange="pickThis4(this)" value="<?php echo number_format($header->DiscHarga,0);?>" > (%)--></td>
                                <!--<td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_pot_disc" id="v_pot_disc" value="<?php echo number_format($header->Diskon,0);?>"></td>
                            </tr>-->
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN<!-- <input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn_" id="v_PPn_" value="<?php echo number_format($header->PPN,0);?>" onchange="pickThis5(this)"> (%) --></td>
                                <td style="text-align: right;">
                                    <input readonly dir="rtl" style="text-align: right;" class="form-control-new" type="text" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo $tots_ppn;?>">
                                </td>
                            </tr>
                            
                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                </td>
                                <td style="text-align: right;">
                                    <input readonly dir="rtl" style="text-align: right;" class="form-control-new" type="text" name="v_Total" id="v_Total" value="<?php echo $totals+$tots_ppn;?>">
                                </td>
                            </tr>
											
					</table>
	        	
				</div>
			    </div>
				
				
				<div class="tab-pane body fade" id="tab2">
		            <div class="table-responsive">
					
			
					<table class="table table-bordered responsive" id="TabelDetail2">
        				<thead class="title_table">
							<tr>
								<th width="200"><center>No Rekening</center></th>
							    <th width="200"><center>Nama Rekening</center></th>
							    <th width="200"><center>SubDivisi</center></th>
								<th><center>Deskripsi<center>
							    <th width="80"><center>Jumlah<center>
								</th>
								<th width="30"><center>
									    	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Tambah Baris" title="" name="btn_tambah_baris" id="btn_tambah_baris" value="tambah" onClick="detailNew()">
												<i class="entypo-plus"></i>
											</button><center>
								</th>
							</tr>
						</thead>
						<tbody>
						
						<?php $no_=1; ?>
									
						  <tr id="baris<?php echo $no_; ?>">
						    <td>
							<input type="text" class="form-control-new" size="15" name="v_kd_rek[]" id="v_kd_rek<?php echo $no_;?>">
							<a href="javascript:void(0)" id="get_norek<?php echo $no_;?>" onclick="pickThis2(this)" class="btn btn-info btn-sm sm-new tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Cari PCode" title=""><i class="entypo-search"></i></a>
							</td>
		                    <td>
							<input type="text" class="form-control-new" size="50" name="v_nm_rek[]" id="v_nm_rek<?php echo $no_;?>">
							</td>
							<td>
										<select class="form-control-new" name="v_subdivisi[]" id="v_subdivisi<?php echo $no_;?>" style="width: 200px;">
						            		<option value="">Pilih Subdivisi</option>
						            		<?php
						            		foreach($subdivisi as $val2)
						            		{
						            			$selected="";
												if($val["KdSubDivisi"]==$val2["KdSubdivisi"])
												{
													$selected='selected="selected"';
												}
												?><option <?php echo $selected; ?> value="<?php echo $val2["KdSubdivisi"]; ?>"><?php echo $val2["NamaSubDivisi"]; ?></option><?php
											}
						            		?>
						            	</select> 
									</td>
							<td>
							<input type="text" size="58" class="form-control-new" name="v_deskripsi[]" id="v_deskripsi<?php echo $no_;?>">
							</td>
		                    <td>
							<input style="text-align: right;" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan total nilai rekening." id="v_jml<?php echo $no_;?>" name="v_jml[]" class="form-control-new" type="text" onkeydown="HitungHarga2(event, 'harga', this);" dir="rtl">
		                    </td>
							<td align="center">
						                	<button type="button" class="btn btn-info btn-sm sm-new tooltip-primary"  data-toggle="tooltip" data-placement="top" data-original-title="Hapus" title="" name="btn_del_detail_target_<?php echo $no_;?>]" id="btn_del_detail_target_<?php echo $no_;?>" value="Save" onclick='deleteRow(this)'>
												<i class="entypo-trash"></i>
											</button>
						    </td>
		                  </tr>
						
						</tbody>										
									
					</table>
					
					<table>
					<tr style="color: black; font-weight: bold;">
                                <td width="1005">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL &nbsp;&nbsp;&nbsp;
                                
                                </td>
                                <td style="text-align: right;"><input dir="rtl" readonly style="text-align: right;" class="form-control-new" type="text" name="v_Total_rek" id="v_Total_rek" value="<?php echo $header_->total_rek;?>"></td>
                            </tr>
					</table>
				</div>
			    </div>
			
				
			
			
			</div>
			</div>
			<?php
			
	}
	
	
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "R E T U R N";
		$data["header"] 	= $this->pi_marketing_model->getHeader($nodok);
		$data["detail"] 	= $this->pi_marketing_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_sr', $data);
	}
	
	
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "sales-return.txt";
		
		$header 	= $this->pi_marketing_model->getHeader($nodok);
		$detail     = $this->pi_marketing_model->getDetail_cetak($nodok);
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["NamaSatuan"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "R E T U R N";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->returnno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->returnno;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->adddate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->adddate));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Gudang";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Keterangan; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Kepada";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Kepada"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                
                
                $echo.="\r\n";
                
                $echo.=$arr_epson["cond"]."Alamat";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Alamat"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Alamat;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Alamat));$i++)
                {
                    $echo.=" ";
                }
                $echo.="\r\n";             
                   
            }          
           
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->note;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->retunno,"sales-return");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->retunno,"sales-return");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "sales-return",
		            'noreferensi' => $header->retunno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='sales-return-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function posting_pembelian_non_stok($tahun,$bulan,$userid,$no,$noref){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'PIM', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PIM', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref));
        
        $nodok = '';
        $noposting1 = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 1;

		$rekening = $this->posting_model->getinterface();
        $header = $this->posting_model->getPembelianNonStok($tahun, $bulan);
        				
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $tgldokumen = $header[$m]['TglDokumen'];
            $kdsubdivisi = $header[$m]['KdSubdivisi'];
            $deskripsi = $header[$m]['Deskripsi'];
            $ketheader = $header[$m]['Keterangan'];
            $jumlah  = $header[$m]['Jumlah'];
            						
            if ($nodok <> $nodokumen) {
        		$new_no = $this->posting_model->getNewNo($bulan, $tahun);
            	$noposting = $new_no->counter;
                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $ketheader,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PIM',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PIM",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $jumlah,
                "Kredit" => 0,
                "KeteranganDetail" => $deskripsi,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $totdebit += $jumlah;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi"  => $tgldokumen,
	                "KodeJurnal"    => "PIM",
	                "KdDepartemen"  => "00",
	                "Project"       => "00",
	                "CostCenter"    => "00",
					"KdSubDivisi"	=> '25',
	                "KdRekening"    => $rekening[0]['HutangLain'],
	                "Debit"         => 0,
	                "Kredit"        => $totdebit,
	                "KeteranganDetail" => $ketheader,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
		            );
		            
		            $this->db->insert('jurnaldetail', $datadtl);
	                $nilkredit = 0;
	                $counterid = 1;
	                $totdebit = 0;
	                $totkredit = 0;
		        }	
			}	
		}
		
        if ($m > 0) {
            $datadtl = array(
				"NoReferensi"	=> $noposting,
                "TglTransaksi"	=> $tgldokumen,
                "KodeJurnal"	=> "PIM",
                "KdDepartemen"	=> "00",
                "Project"		=> "00",
                "CostCenter"	=> "00",
                "KdRekening"	=>  $rekening[0]['HutangLain'],
				"KdSubDivisi"	=> '25',
                "Debit"		=> 0,
                "Kredit"	=> $totdebit,
                "KeteranganDetail" => $ketheader,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
	}
		
}

?>