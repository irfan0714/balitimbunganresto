<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Tax_number extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/tax_number_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
                
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            
            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            $data['cari']="";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "tax_number", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]);   
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                    
                    $data['cari']=$data["search_keyword"];
                }
            }
            
            
            
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '10';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['total_rows'] = $this->tax_number_model->num_list_pajak_row($arr_search["search"]);
            
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/tax_number/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/tax_number/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
            
            $data['ListPajak'] = $this->tax_number_model->ListNoFakturPajak($config['per_page'],$page, $arr_search["search"]);
            
            $data['track'] = $mylib->print_track();
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('transaksi/tax_number/tax_number_list',$data);
            
        } else {
            $this->load->view('denied');
        }
    }
    
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'tax_number', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "tax_number",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/tax_number/index/' . $query_id . '');
    }
    
    function preview($nofaktur) 
    {
    	$mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['data'] = $this->tax_number_model->getDataHeader($nofaktur);
            $data['row'] = $this->tax_number_model->getDataDetail($nofaktur);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/tax_number/preview', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function simpan(){
		$nofaktur		= $this->input->post('nfk');
        $nofakturpajak	= $this->input->post('nfkp');
        $this->db->update('invoice_pembelian_header',array('NoFakturPajak'=>$nofakturpajak),array('NoFaktur'=>$nofaktur));
        $data=array('status'=>true);
	    echo json_encode($data);
	}
    
    function view($nofaktur){
    	
    	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No.Fakktur ' . $nofaktur . '</strong> berhasil ( Klik Notifikasi Ini Untuk Detail ).','nofaktur'=>$nofaktur,'nofakturpajak'=>$nofakturpajak, 'class' => 'success'));
		redirect('/transaksi/tax_number/');
	}

}

?>