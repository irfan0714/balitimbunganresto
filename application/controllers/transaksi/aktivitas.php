<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Aktivitas extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/aktivitasmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 			= $this->uri->segment_array();
  		    $arr 			= "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] 	= $mylib->restrictLink($arr);
	     	$id 			= $this->input->post('stSearchingKey');
	        $with 			= $this->input->post('searchby');
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '20';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/aktivitas/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$with 					  = $this->input->post('searchby');
			$id   					  = "";
			$flag1					  = "";
			if($with!=""){
		        $id    = $this->input->post('stSearchingKey');
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/aktivitas/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
				 	$config['base_url']     = base_url().'index.php/transaksi/aktivitas/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
	        $config['total_rows']  = $this->aktivitasmodel->num_aktivitas_row($id,$with);
	        $data['data']  = $this->aktivitasmodel->get_aktivitas_List($config['per_page'],$page,$id,$with);
	        $data['rekening'] = $this->aktivitasmodel->getRekeningList();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/aktivitas/aktivitas_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
    
    function save_aktivitas(){
    	
    	$aktivitas = $this->input->post('namaaktivitas');
		$kdrekening= $this->input->post('rekening');
        
    	$user = $this->session->userdata('username');
    	$data = array(
    		  'NamaAktivitas'	=> $aktivitas,
			  'KdRekening' => $kdrekening,
              'AddDate'		=> date('Y-m-d'),
              'AddUser'		=> $user
			);
		$this->db->insert('aktivitas', $data);
    	
    }
	
	function edit_aktivitas(){
    	
    	$kdaktivitas = $this->input->post('id');
		$aktivitas = $this->input->post('namaaktivitas');
		$kdrekening= $this->input->post('rekening');
        
    	$user = $this->session->userdata('username');
    	$data = array(
    		  'NamaAktivitas'	=> $aktivitas,
			  'KdRekening' => $kdrekening,
              'EditDate'		=> date('Y-m-d'),
              'EditUser'		=> $user
			);
		$this->db->update('aktivitas', $data,array('KdAktivitas'=>$kdaktivitas));
    	
    }
	
	function delete_aktivitas(){
    	
    	$kdaktivitas = $this->input->post('id');
		$this->db->delete('aktivitas',array('KdAktivitas'=>$kdaktivitas));
    	
    }
    
    function ajax(){
		$KdRekening = $this->input->post('id');
		$query = $this->aktivitasmodel->getRekening($KdRekening);
		 
	 echo "<option value=''> -- Pilih --</option>";
     foreach ($query as $cetak) {
	 echo "<option value=$cetak[KdRekening]>$cetak[KdRekening] - $cetak[NamaRekening]</option>";
	 
	    }
    }
    
    function edit($KdAktivitas) 
    {
		$data = $this->aktivitasmodel->get_by_aktivitas($KdAktivitas);
		echo json_encode($data);			
    }
    
  }
?>