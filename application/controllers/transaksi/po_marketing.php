<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Po_marketing extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/po_marketing_model');
    }

    function index()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_supplier"] = "";
            $data["search_status"] = "";
            $data["search_tgl_awal"] = "";
            $data["search_tgl_akhir"] = "";


            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "po_marketing", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                // print_r($resSearch);

                // die();

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_supplier = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["supplier"] = $search_supplier[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];
                    $search_tgl_awal = explode("=", $arrSearch[4]); // tgl awal
                    $arr_search["search"]["tgl_awal"] = $search_tgl_awal[1];
                    $search_tgl_akhir = explode("=", $arrSearch[5]); // tgl akhir
                    $arr_search["search"]["tgl_akhir"] = $search_tgl_akhir[1];


                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_supplier"] = $search_supplier[1];
                    $data["search_status"] = $search_status[1];
                    $data["search_tgl_awal"] = $search_tgl_awal[1];
                    $data["search_tgl_akhir"] = $search_tgl_akhir[1];

                    // print_r($data);

                    // die();

                }
            }

            // print_r($arr_search["search"]);

            // die();

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/po_marketing/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/po_marketing/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            //$data['mgudang'] = $this->po_marketing_model->getGudang();
			//$data['mcustomer'] = $this->po_marketing_model->getCustomer();
			$data['ses_login'] = $this->session->userdata('username');
            $config['total_rows'] = $this->po_marketing_model->num_po_marketing_row($arr_search["search"]);
            $data['data'] = $this->po_marketing_model->getpo_marketingList($config['per_page'], $page, $arr_search["search"]);
			//$data['data'] = $this->po_marketing_model->getDeliveryOrderList($config['per_page'], 0, $arr_search["search"]);
			$data['supplier'] = $this->po_marketing_model->getSupplier();
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/po_marketing/po_marketing_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function ExportPOM(){

        $mylib = new globallib();


        $tgl_awal = $this->input->post('tgl_awal1');
        $tgl_akhir = $this->input->post('tgl_akhir1');
        $supplier = $this->input->post('supplier1');

        $where_sup = "AND po_marketing.KdSupplier = '".$supplier."'";


                

        $this->load->library('excel');

        require_once './application/third_party/PHPExcel.php';
        require_once './application/third_party/PHPExcel/IOFactory.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        $default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000'),
        );

        $acc_default_border = array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => 'c7c7c7'),
        );
        $outlet_style_header = array(
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 10,
                'name' => 'Arial',
                'bold' => true,
            ),
        );
        $top_header_style = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 15,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $account_value_style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
            ),
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ),
        );
        $text_align_style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff03'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
                'size' => 12,
                'name' => 'Arial',
                'bold' => true,
            ),
        );

        $lang_sales_history = "Sales History";
        $lang_sale_id = $this->lang->line('sale_id');
        $lang_type = $this->lang->line('type');
        $lang_date_time = $this->lang->line('date_time');
        $lang_products = $this->lang->line('products');
        $lang_qty = $this->lang->line('quantity');
        $lang_total_qty = $this->lang->line('total_quantity');
        $lang_sub_total = $this->lang->line('sub_total');
        $lang_tax = $this->lang->line('tax');
        $lang_grand_total = $this->lang->line('grand_total');
        $lang_total = $this->lang->line('total');

                

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "PT NATURA PESONA MANDIRI");
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:I2');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "PEMBELIAN NON STOK ");
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:I3');
        $objPHPExcel->getActiveSheet()->setCellValue('A3', "PERIODE ".$tgl_awal." sd ".$tgl_akhir);

        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($top_header_style);

        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($top_header_style);

        $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray($top_header_style);        
        $objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('G3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('H3')->applyFromArray($top_header_style);
        $objPHPExcel->getActiveSheet()->getStyle('I3')->applyFromArray($top_header_style);

        $objPHPExcel->getActiveSheet()->setCellValue('A4', "NO");
        $objPHPExcel->getActiveSheet()->setCellValue('B4', "NO DOKUMEN");
        $objPHPExcel->getActiveSheet()->setCellValue('C4', "TANGGAL");
        $objPHPExcel->getActiveSheet()->setCellValue('D4', "NO PRM");
        $objPHPExcel->getActiveSheet()->setCellValue('E4', "SUPPLIER");
        $objPHPExcel->getActiveSheet()->setCellValue('F4', "KETERANGAN");
        $objPHPExcel->getActiveSheet()->setCellValue('G4', "TOTAL");
        $objPHPExcel->getActiveSheet()->setCellValue('H4', "APPROVAL");
        $objPHPExcel->getActiveSheet()->setCellValue('I4', "STATUS");

        $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('F4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('G4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('H4')->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray($style_header);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        $jj = 5;

        $total_sub_amt = 0;
        $total_tax_amt = 0;
        $total_grand_amt = 0;

        $sql = "SELECT
                      `po_marketing`.NoDokumen,
                      DATE_FORMAT(
                        po_marketing.TglDokumen,
                        '%d-%m-%Y'
                      ) AS TglDokumen,
                      `po_marketing`.NoPr,
                      `po_marketing`.Keterangan,
                      `po_marketing`.Total,
                      `po_marketing`.status,
                      `po_marketing`.Approval_By AS Approval_By_,
                      DATE_FORMAT(
                        po_marketing.Approval_Date,
                        '%d-%m-%Y'
                      ) AS Approval_Date_,
                      `po_marketing`.Approval_Status AS Approval_Status_,
                      `po_marketing`.Approval_Remarks AS Approval_Remarks_,
                      supplier.*
                    FROM
                      `po_marketing`
                      INNER JOIN pr_marketing
                        ON po_marketing.`NoPr` = pr_marketing.`NoDokumen`
                      INNER JOIN supplier
                        ON po_marketing.`KdSupplier` = supplier.`KdSupplier`
                    WHERE 1
                      AND po_marketing.TglDokumen BETWEEN '$tgl_awal'
                      AND '$tgl_akhir'
                      ".$where_sup."
                    ORDER BY po_marketing.TglDokumen ASC";

        $orderResult = $this->db->query($sql);

            // echo $sql;
            // die();

        $no =1;
        $status = "";
        $orderData = $orderResult->result();
        for ($d = 0; $d < count($orderData); ++$d) {
            $nodok = $orderData[$d]->NoDokumen;
            $tanggal = $orderData[$d]->TglDokumen;
            $nopr = $orderData[$d]->NoPr;
            $supplier = $orderData[$d]->Nama;
            $keterangan = $orderData[$d]->Keterangan;
            $total = $orderData[$d]->Total;
            $approval = $orderData[$d]->Approval_By_;
            $appdate = $orderData[$d]->Approval_Date_;


            if($orderData[$d]->status == 0){
                $status = "Pending";
            }elseif($orderData[$d]->status == 1){
                $status = "Open";
            }else{
                $status = "Void";
            }


            

            $objPHPExcel->getActiveSheet()->setCellValue("A$jj", "$no");
            $objPHPExcel->getActiveSheet()->setCellValue("B$jj", "$nodok");
            $objPHPExcel->getActiveSheet()->setCellValue("C$jj", "$tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue("D$jj", "$nopr");
            $objPHPExcel->getActiveSheet()->setCellValue("E$jj", "$supplier");
            $objPHPExcel->getActiveSheet()->setCellValue("F$jj", "$keterangan");
            $objPHPExcel->getActiveSheet()->setCellValue("G$jj", "$total");
            $objPHPExcel->getActiveSheet()->setCellValue("H$jj", "$approval"."/$appdate");
            $objPHPExcel->getActiveSheet()->setCellValue("I$jj", "$status");


            

            $objPHPExcel->getActiveSheet()->getStyle("A$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("B$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("C$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("D$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("E$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("F$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("G$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("H$jj")->applyFromArray($account_value_style_header);
            $objPHPExcel->getActiveSheet()->getStyle("I$jj")->applyFromArray($account_value_style_header);
           

            ++$jj;

            $no++;

            unset($order_id);
            unset($ordered_dtm);
            unset($subTotal);
            unset($gstTotal);
            unset($grandTotal);
            unset($total_item_qty);
        }
        unset($orderResult);
        unset($orderData);

        
        $objPHPExcel->getActiveSheet()->getStyle("A$jj")->applyFromArray($text_align_style);
        $objPHPExcel->getActiveSheet()->getStyle("B$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("C$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("D$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("E$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("F$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("G$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("H$jj")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->getStyle("I$jj")->applyFromArray($style_header);


        

        $objPHPExcel->getActiveSheet()->getRowDimension("$jj")->setRowHeight(30);
                // print_r($orderData);

                // return $orderData;
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Pembelian Non Stok '.$tgl_awal.'-'.$tgl_akhir.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    function search()
    {
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'po_marketing', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_supplier=".$this->input->post('search_supplier');
		$search_value .= "&search_status=".$this->input->post('search_status');
        $search_value .= "&tgl_awal=".$mylib->ubah_tanggal($this->input->post('tgl_awal'));
        $search_value .= "&tgl_akhir=".$mylib->ubah_tanggal($this->input->post('tgl_akhir'));



		$data = array(
            'query_string' => $search_value,
            'module' => "po_marketing",
            'AddUser' => $user
        );

        // echo $search_value;

        // die();

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/po_marketing/index/' . $query_id . '');
    }

    function add_new()
    {

        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";

            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');

			//hapus dulu di detail_temp
			$this->db->delete('po_marketing_detail_temp',array('NoDokumen'=>'000000','AddUser'=>$user));

			$data['nil_proposal'] = $this->po_marketing_model->cek_nil_proposal($id);
			$data['cek_penggunaan_proposal'] = $this->po_marketing_model->cek_apakah_ada_yang_pakai_proposal_ini($id);

            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			$data['gudang'] = $this->po_marketing_model->getGudang();
            $data['supplier'] = $this->po_marketing_model->getSupplier();
            $data['currency'] = $this->po_marketing_model->getCurrency();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/po_marketing/add_po_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_po_marketing($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->po_marketing_model->getHeader($id);
            $data['gudang'] = $this->po_marketing_model->getGudang();
            $data['supplier'] = $this->po_marketing_model->getSupplier();
            $data['currency'] = $this->po_marketing_model->getCurrency();
            /*echo "<pre>";
        	print_r($data['header']);
        	echo "<hr>";
            print_r($data['currency']);
            echo "</pre>";
            die;*/
            $data['detail_list'] = $this->po_marketing_model->getDetailList($id);
			$data['nil_proposal'] = $this->po_marketing_model->cek_nil_proposal($id);
			$data['cek_penggunaan_proposal'] = $this->po_marketing_model->cek_apakah_ada_yang_pakai_proposal_ini($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/po_marketing/edit_po_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }


	function view_po_marketing($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['ses_login'] = $this->session->userdata('username');
            $data['header'] = $this->po_marketing_model->getHeader($id);
            $data['gudang'] = $this->po_marketing_model->getGudang();
            $data['supplier'] = $this->po_marketing_model->getSupplier();
            $data['currency'] = $this->po_marketing_model->getCurrency();
            $data["detail_list"] = $this->po_marketing_model->getDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/po_marketing/view_po_marketing', $data);
        } else {
            $this->load->view('denied');
        }
    }


	function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_gudang = $this->input->post('v_gudang');
        $v_supplier = $this->input->post('v_supplier');
        $v_top = $this->input->post('v_top');
        $v_NoPr = $this->input->post('v_NoPr');
        $v_currencycode = $this->input->post('v_currencycode');
        $v_note = $this->input->post('v_note');
        $v_TglTerima = $this->input->post('v_tgl_terima');
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');

        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));

		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;

        // detail
        $v_nmbarang1 = $this->input->post('v_nmbarang');
        $v_qty_request1 = $this->input->post('v_qty_request');
        $v_Qty1 = $this->input->post('v_Qty');
        $v_Harga1 = $this->input->post('v_Harga');
        $v_Disc1 = $this->input->post('v_Disc');
        $v_Potongan1 = $this->input->post('v_Potongan');
        $v_subtotal1 = $this->input->post('v_subtotal');

        //jml
        $v_Jumlah = $this->input->post('v_Jumlah');
        $v_DiscHarga = $this->input->post('v_DiscHarga');
		$v_Diskon = $this->input->post('v_pot_disc');
        $v_NilaiPPn = $this->input->post('v_NilaiPPn');
        $v_PPn = $this->input->post('v_PPn');
        $v_Total = $this->input->post('v_Total');

        //echo $market_pcode1." - ".$market_qty1." - ".$market_satuan1;die;

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Delivery Order
        	$v_no_dokumen = $mylib->get_code_counter2($this->db->database, "po_marketing","NoDokumen", "POM", $data['bulan'], $data['tahun']);

			//kedua masukkan di Delivery Order Header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_NoPr, $mylib->ubah_tanggal($v_TglTerima),$v_supplier, $v_note, $v_gudang, $v_top, $v_currencycode ,$user );

						// insert detail
				        for ($x = 0; $x < count($v_nmbarang1); $x++)
				        {
				            $v_nmbarang = $v_nmbarang1[$x];
							$v_qty_request = $v_qty_request1[$x];
							$v_Qty = $v_Qty1[$x];
							$v_Harga = $v_Harga1[$x];
							$v_Disc = $v_Disc1[$x];
							$v_Potongan = $v_Potongan1[$x];
							$v_subtotal = $v_subtotal1[$x];
							$v_total = $v_subtotal1[$x];

				            if ($v_nmbarang != "")
				            {
				            	if($v_Qty*1!=0){

										$data=array(
										'NoDokumen'=>$v_no_dokumen,
										'NoUrut'=>$x+1,
										'NamaBarang'=>$v_nmbarang,
										'Qty'=>$v_qty_request,
										'QtyTerima'=>$v_Qty,
										'HargaSatuan'=>$v_Harga,
										'Disc'=>$v_Disc,
										'Potongan'=>$v_Potongan,
										'Jumlah'=>$v_subtotal,
										'Total'=>$v_total
										);
										$this->db->insert('po_marketing_detail',$data);

										/*//update ke detail pr
							            $this->db->update('pr_marketing_detail',
									    array('QtyPOM'=>$v_Qty),
										array('NoDokumen'=>$v_NoPr,'NamaBarang'=>$v_nmbarang));*/

								}
				            }
				        }

				                       //update juga yang ada di QtyPOM PRM
							           //kemudian kurang angka yang ada di QtyPOM PRM
						                $qty_pom = $this->po_marketing_model->getQtyPom($v_no_dokumen);

						                foreach($qty_pom AS $v){
											$NamaBarangPOM = $v['NamaBarang'];
											$QtyTerima_POM = $v['QtyTerima'];

											//ambil QtyPO di PR
											$qty_po_pr = $this->po_marketing_model->getQtyPoPrm($v_NoPr, $NamaBarangPOM);
											$updateQtyPoPr = $qty_po_pr->QtyPOM + $QtyTerima_POM;


											//update ke PR
											$this->db->update('pr_marketing_detail',array('QtyPOM'=>$updateQtyPoPr),array('NoDokumen'=>$v_NoPr,'NamaBarang'=>$NamaBarangPOM));
						                }
				//delete temp
				$this->db->delete('po_marketing_detail_temp',array('NoDokumen'=>'000000','AddUser'=>$user));

		}


		else if ($flag == "edit")
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');

			//update deliveyorder
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_NoPr, $mylib->ubah_tanggal($v_TglTerima),$v_supplier, $v_note, $v_gudang, $v_top, $v_currencycode , str_replace(",", "", $v_Jumlah),$v_DiscHarga,str_replace(",", "", $v_Diskon),$v_PPn, str_replace(",", "", $v_NilaiPPn), str_replace(",", "", $v_Total), $v_status, $user );

				        // ubah detail
				        $grand_total = 0;
				        for ($x = 0; $x < count($v_nmbarang1); $x++)
				        {

				            $v_nmbarang = $v_nmbarang1[$x];
							$v_qty_request = $v_qty_request1[$x];
							$v_Qty = $v_Qty1[$x];
							$v_Harga = $v_Harga1[$x];
							$v_Disc = $v_Disc1[$x];
							$v_Potongan = $v_Potongan1[$x];
							$v_subtotal = $v_subtotal1[$x];
							$v_total = $v_subtotal1[$x];

				            if ($v_nmbarang != "")
				            {
				            	$this->updateDetail($v_no_dokumen, $x+1, $v_nmbarang, $v_qty_request, $v_Qty ,str_replace(",", "", $v_Harga), $v_Disc, str_replace(",", "", $v_Potongan),str_replace(",", "", $v_subtotal), str_replace(",", "", $v_total));
							}

							$grand_total += $v_total;

				        }


						       //update juga yang ada di QtyPOM PRM
					           //kemudian kurang angka yang ada di QtyPOM PRM
				                $qty_pom = $this->po_marketing_model->getQtyPom($v_no_dokumen);

				                foreach($qty_pom AS $v){
									$NamaBarangPOM = $v['NamaBarang'];
									$QtyTerima_POM = $v['QtyTerima'];

									//ambil QtyPO di PR
									$qty_po_pr = $this->po_marketing_model->getQtyPoPrm($v_NoPr, $NamaBarangPOM);
									if($qty_po_pr->QtyPOM == $QtyTerima_POM){
										$updateQtyPoPr = $qty_po_pr->QtyPOM;
									}else if($qty_po_pr->QtyPOM > $QtyTerima_POM){
										$selisih = $qty_po_pr->QtyPOM - $QtyTerima_POM;
										$updateQtyPoPr = $qty_po_pr->QtyPOM - $selisih;
									}else if($qty_po_pr->QtyPOM < $QtyTerima_POM){
										$selisih = $QtyTerima_POM - $qty_po_pr->QtyPOM;
										$updateQtyPoPr = $qty_po_pr->QtyPOM + $selisih;
									}

									//update ke PR
									$this->db->update('pr_marketing_detail',array('QtyPOM'=>$updateQtyPoPr),array('NoDokumen'=>$v_NoPr,'NamaBarang'=>$NamaBarangPOM));
					             }


				// update FlagKonfirmasi di PRM
				//$this->db->update('pr_marketing',array('FlgKonfirmPO'=>'1'),array('NoDokumen'=>$v_NoPr));

				//echo $v_status." - ".$v_Total;die;
				// kirim email ketika save & send
				if($v_status=="1")
                        {
                        	$mylib = new globallib();

	                        	if($grand_total*1<=5000000)
	                            {
	                            	// sari
	                                $subject = "Request Approval Proposal ".$v_no_dokumen;
	                                $author  = "Auto System";

	                                $url = "http://sys.bebektimbungan.com/index.php/transaksi/po_marketing/view_po_marketing/".$v_no_dokumen;

	                                $to = "crfg@secretgarden.co.id;irf@vci.co.id;";
	                                $to_name = "Charles;Irfan;Irfan;";

	                                $body  = "Dear Charles, <br><br>";
	                                $body .= "Mohon lakukan approval untuk Proposal Marketing ".$v_no_dokumen." senilai ".$v_currencycode." ".$grand_total."<br><br>";
	                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;

	                                $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
	                            }
	                            else if($grand_total*1>5000000 && $grand_total*1<10000000)
	                            {
	                                // trisno
	                                $subject = "Request Approval Proposal Marketing ".$v_no_dokumen;
	                                $author  = "Auto System";

	                                $url = "http://sys.bebektimbungan.com/index.php/transaksi/po_marketing/view_po_marketing/".$v_no_dokumen;

	                                $to = "eno@vci.co.id;irf@vci.co.id;";
	                                $to_name = "Trisno;Irfan;Irfan;";

	                                $body  = "Dear Bambang Sutrisno, <br><br>";
	                                $body .= "Mohon lakukan approval untuk Proposal Marketing ".$v_no_dokumen." senilai ".$v_currencycode." ".$grand_total."<br><br>";
	                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;

	                                $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
	                            }
	                            else if($grand_total*1>=10000000)
	                            {
	                                // henny
	                                $subject = "Request Approval Proposal Marketing ".$v_no_dokumen;
	                                $author  = "Auto System";

	                                $url = "http://sys.bebektimbungan.com/index.php/transaksi/po_marketing/view_po_marketing/".$v_no_dokumen;

	                                $to = "hst@vci.co.id;irf@vci.co.id;";
	                                $to_name = "Henny;Irfan;Irfan;";

	                                $body  = "Dear Henny, <br><br>";
	                                $body .= "Mohon lakukan approval untuk Proposal Marketing ".$v_NoDokumen." senilai ".$v_currencycode." ".$grand_total."<br><br>";
	                                $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk Proses Approval Atau Copy Paste Link ini di broswer anda : ".$url;

	                                $mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
	                            }

						}


						/*else if($v_Status=="void")
                        {
                        	$q = "
	                            UPDATE
	                                ".$db["master"].".trans_order_barang_header
	                            SET
	                                Status = '2',
	                                EditDate = NOW(),
	                                EditUser = '".$ses_login."'
	                             WHERE 1
	                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
	                        ";
	                        //echo $q."<hr/>";
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan trans_order_barang_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die();
	                        }

		                    $msg = "Berhasil Void ".$v_NoDokumen;
		                    echo "<script>alert('".$msg."');</script>";
		                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";
		                    die();
                        }
						else if($v_Status=="unvoid")
                        {
                        	$q = "
	                            UPDATE
	                                ".$db["master"].".trans_order_barang_header
	                            SET
	                                Status = '0',
	                                EditDate = NOW(),
	                                EditUser = '".$ses_login."'
	                             WHERE 1
	                                AND ".$db["master"].".trans_order_barang_header.NoDokumen = '".$v_NoDokumen."'
	                        ";
	                        //echo $q."<hr/>";
	                        if(!mysql_query($q))
	                        {
	                            $msg = "Gagal menyimpan trans_order_barang_header";
	                            echo "<script>alert('".$msg."');</script>";
	                            die();
	                        }

		                    $msg = "Berhasil UnVoid ".$v_NoDokumen;
		                    echo "<script>alert('".$msg."');</script>";
		                    echo "<script>parent.CallAjaxForm('search', '".$v_NoDokumen."');</script>";
		                    die();
                        }*/


            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        //update ststus flgkonfimpo
        $cek_detail_pr_marketing = $this->po_marketing_model->getPrMarketingDetail($v_NoPr);
        if(empty($cek_detail_pr_marketing)){
			    //update FlagKonfirmasi di PRM
          $sql = "UPDATE pr_marketing SET FlgKonfirmPO = '1' WHERE NoDokumen ='$v_NoPr'";
          $this->db->query($sql);
				// $this->db->update('pr_marketing',array('FlgKonfirmPO'=>'1'),array('NoDokumen'=>$v_NoPr));
		}

        redirect('/transaksi/po_marketing/edit_po_marketing/' . $v_no_dokumen . '');
    }

    function insertNewHeader($v_no_dokumen, $v_tgl_dokumen, $v_NoPr, $v_TglTerima ,$v_supplier, $v_note, $v_gudang, $v_top, $v_currencycode ,$user )
    {
        $this->po_marketing_model->locktables('po_marketing');

        $data = array(
            'NoDokumen' => $v_no_dokumen,
            'TglDokumen' => $v_tgl_dokumen,
            'NoPr' => $v_NoPr,
            'TglTerima' => $v_TglTerima,
            'KdSupplier' => $v_supplier,
            'Keterangan' => $v_note,
            'KdGudang' => $v_gudang,
            'TOP' => $v_top,
            'currencycode' => $v_currencycode,
            'AddDate'=>date('Y-m-d'),
            'AddUser' => $user
        );

        $this->db->insert('po_marketing', $data);

        $this->po_marketing_model->unlocktables();

    }


    function updateHeader($v_no_dokumen, $v_tgl_dokumen, $v_NoPr, $v_TglTerima,$v_supplier, $v_note, $v_gudang, $v_top, $v_currencycode , $v_Jumlah,$v_DiscHarga,$v_Diskon,$v_PPn, $v_NilaiPPn, $v_Total, $v_status, $user )
    {

        $this->po_marketing_model->locktables('po_marketing');

        $data = array(
            'TglDokumen' => $v_tgl_dokumen,
            'NoPr' => $v_NoPr,
            'TglTerima' => $v_TglTerima,
            'KdSupplier' => $v_supplier,
            'Keterangan' => $v_note,
            'KdGudang' => $v_gudang,
            'TOP' => $v_top,
            'currencycode' => $v_currencycode,
            'Jumlah'=>$v_Jumlah,
            'DiscHarga'=>$v_DiscHarga,
            'Diskon'=>$v_Diskon,
			'PPn'=>$v_PPn,
            'NilaiPPn'=>$v_NilaiPPn,
            'Total'=>$v_Total,
            'Status'=>$v_status,
            'EditDate'=>date('Y-m-d'),
            'EditUser' => $user
        );
        $this->db->update('po_marketing', $data, array('NoDokumen' => $v_no_dokumen));

        $this->po_marketing_model->unlocktables();
    }

    function updateDetail($v_no_dokumen, $x, $v_nmbarang, $v_qty_request, $v_Qty ,$v_Harga, $v_Disc, $v_Potongan,$v_subtotal, $v_total)
    {
        $this->po_marketing_model->locktables('po_marketing_detail');


            $data = array
                (
	             'NamaBarang'=>$v_nmbarang,
	             'Qty'=>$v_qty_request,
	             'QtyTerima'=>$v_Qty,
	             'HargaSatuan'=>$v_Harga,
	             'Disc'=>$v_Disc,
	             'Potongan'=>$v_Potongan,
	             'Jumlah'=>$v_subtotal,
	             'Total'=>$v_total)
            ;

            $this->db->update('po_marketing_detail', $data, array('NoDokumen'=>$v_no_dokumen,'NoUrut'=>$x));


        $this->po_marketing_model->unlocktables();
    }



	function delete_trans($id)
    {

            $this->db->delete('po_marketing', array('NoDokumen' => $id));

            $this->db->delete('po_marketing_detail', array('NoDokumen' => $id));


            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));


        redirect('/transaksi/po_marketing/');
    }


	function save_detail_temp($nopr)
    {

		//kan sudah dapet no Purchase Request
			$user = $this->session->userdata('username');
			$detail_pr_marketing = $this->po_marketing_model->getPrMarketingDetail($nopr);

			/*$z=1;
			foreach( $detail_pr_marketing AS $val){

				$data=array(
							'NoDokumen'=>'000000',
							'NoUrut'=>$z,
							'NamaBarang'=>$val['NamaBarang'],
							'Qty'=>$val['Qty'],
							'AddUser'=>$user
							);
				$this->db->insert('po_marketing_detail_temp',$data);
			$z++;
			}

			$detail_list = $this->po_marketing_model->getDetailListTemp('000000');*/

		    ?>
			<thead class="title_table">
							<tr>
							    <th width="300"><center>Nama Barang</center></th>
							    <th width="50"><center>Sisa Qty Requst</center></th>
							    <th width="30"><center>Qty</center></th>
							    <th width="100"><center>Harga</center></th>
							    <th width="100"><center>Disc (%)</center></th>
							    <th width="100"><center>Potongan (IDR)</center></th>
							    <th width="100"><center>Sub Total</center></th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden" name="grdTotal" id="grdTotal" value=""/>
						  <?php
						  $Sid=1;
						  foreach($detail_pr_marketing as $val){
						  if((($val["QtyPOM"]*1)-($val["Qty"]*1))<0)
						  {?>

							  <tr>
								<td align="left"><?php echo $val["NamaBarang"]; ?><input type="hidden" name="v_nmbarang[]" value="<?php echo $val["NamaBarang"]; ?>"></td>
								<td align="center"><?php echo $val["Qty"]-$val["QtyPOM"]; ?><input type="hidden" name="v_qty_request[]" id="v_qty_request<?php echo $Sid; ?>" value="<?php echo $val["Qty"]-$val["QtyPOM"]; ?>"></td>
								<td align="right"><input style="text-align: right; width: 60px;" type="text" class="form-control-new" title="Double Klik Untuk Copy Qty" ondblclick="CopyQtyPR(<?php echo $Sid; ?>)" name="v_Qty[]" id="v_Qty_<?php echo $Sid; ?>" ></td>
                                <td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" data-toggle="tooltip" data-placement="top" data-original-title="enter agar mendapatkan subtotal, total dan grand total." name="v_Harga[]" id="v_Harga_<?php echo $Sid; ?>" value="0" onkeydown="HitungHarga(event, 'harga', this);" dir="rtl" ></td>
							  	<td align="right"><input style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_Disc[]" id="v_Disc_<?php echo $Sid; ?>" onkeydown="HitungHarga2(event, 'harga', this);"  value="<?php echo 0; ?>"></td>
							  	<td align="right"><input style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_Potongan[]" id="v_Potongan_<?php echo $Sid; ?>" onkeydown="HitungHarga3(event, 'harga', this);" value="<?php echo 0; ?>" ></td>
							  	<td align="right"><input readonly style="text-align: right; width: 100px;" type="text" class="form-control-new" name="v_subtotal[]" id="v_subtotal_<?php echo $Sid; ?>" dir="rtl" value="0" ></td>
								<td style="display: none"><input type="text" name="v_sJumlah[]" id="v_sJumlah_<?php echo $Sid; ?>" value="0" dir="rtl" class="form-control-new" readonly="readonly"/></td>
							  </tr>
							<?php   } $Sid++;
							}
							?>
						</tbody>

						<tr style="color: black; font-weight: bold;">
                                <td colspan="5" rowspan="4">
                                    <!--Terbilang : <?php echo "Satu Juta Rupiah"; ?> -->
                                </td>
                                <td style="text-align: right;">
                                TOTAL

                                </td>
                                <td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_Jumlah" id="v_Jumlah" value="<?php echo number_format($header->Jumlah,0);?>"></td>
                            </tr>

                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">DISC <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total." style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_DiscHarga" id="v_DiscHarga" onkeydown="HitungHarga(event, 'diskon', this);" value="<?php echo number_format($header->DiscHarga,0);?>" > (%)</td>
                                <td style="text-align: right;"><input readonly style="text-align: right;" class="form-control-new" type="text" name="v_pot_disc" id="v_pot_disc" value="<?php echo number_format($header->Diskon,0);?>"></td>
                            </tr>

                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">PPN <input data-toggle="tooltip" data-placement="top" data-original-title="enter disini untuk mendapatkan grand total."style="text-align: right; width: 50px;" type="text" class="form-control-new" name="v_PPn" id="v_PPn" value="<?php echo number_format($header->PPn_,0);?>" onkeydown="HitungHarga(event, 'ppn', this);"> (%)</td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" name="v_NilaiPPn" id="v_NilaiPPn" value="<?php echo number_format($header->NilaiPPn,0);?>">
                                </td>
                            </tr>

                            <tr style="color: black; font-weight: bold;">
                                <td style="text-align: right;">
                                    GRAND TOTAL
                                </td>
                                <td style="text-align: right;">
                                    <input readonly style="text-align: right;" class="form-control-new" type="text" name="v_Total" id="v_Total" value="<?php echo number_format($header->Total,0);?>">
                                </td>
                            </tr>
			<?php

    }

    function approve()
    {
     $pono = $this->input->post('id');
     $user = $this->session->userdata('username');

     $data = array(
    				  'Approval_By' => $user,
					  'Approval_Date' => date('Y-m-d'),
					  'Approval_Status' => '1'
    	            );

    	$where =array(
    				  'NoDokumen' => $pono
    				 );
		$this->db->update('po_marketing',$data,$where);

     $data['sukses']=true;
     echo json_encode($data);
    }

    function reject()
    {
     $pono = $this->input->post('id');
     $remark = $this->input->post('al');
     $user = $this->session->userdata('username');

     $data = array(
    				  'Approval_By' => $user,
					  'Approval_Date' => date('Y-m-d'),
					  'Approval_Status' => '2',
					  'Approval_Remarks'=>$remark
    	            );

    	$where =array(
    				  'NoDokumen' => $pono
    				 );
		$this->db->update('po_marketing',$data,$where);

     $data['sukses']=true;
     echo json_encode($data);
    }

    function top(){
    	    $supp = $this->input->post('id');

			$data_top = $this->po_marketing_model->getTop($supp);
			$data['top']=$data_top->TOP;

            echo json_encode($data);

	}


    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();

		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');


		$data["judul"]		= "PURCHASE ORDER";
		$data["header"] 	= $this->po_marketing_model->getHeader($nodok);
		$data["detail"] 	= $this->po_marketing_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();

        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_pom', $data);
	}


	function doPrint_old()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();

		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";

		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();

		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();


		$total_spasi = 135;
	    $total_spasi_header = 80;


	    $jml_detail  = 8;
	    $ourFileName = "purchase_order_marketing.txt";

		$header = $this->po_marketing_model->getHeader($nodok);
		$detail = $this->po_marketing_model->getDetail_cetak($nodok);
		$note_header = substr($header->Keterangan_,0,40);

		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            //$arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,60);
            $arr_data["detail_qty"][$counter] = $val["QtyTerima"];
            $arr_data["detail_satuan"][$counter] = $val["HargaSatuan"];
			$arr_data["detail_namasatuan"][$counter] = $val["HargaSatuan"];
			$counter++;
		}

        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);

        $nama_dokumen = "Purchase Order";

        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n";
                $echo.="\r\n";
            }

            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n";
            }
            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo .= $arr_epson["cond"].$nama_dokumen;

            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.= $arr_epson["cond"]."No : ".$header->NoDokumen;

            $echo.="\r\n";

            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglDokumen_;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglDokumen_));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------

                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Suppplier";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Supplier"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Nama;

                $echo.="\r\n";
                // -----------------------------------------------------
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Mata Uang";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Mata Uang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->currencycode_;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->currencycode_));$i++)
                {
                    $echo.=" ";
                }


                $echo.="\r\n";

                $echo.=$arr_epson["cond"]."Estimasi";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Estimasi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglTerima_;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglTerima_));$i++)
                {
                    $echo.=" ";
                }
                $echo.="\r\n";

            }


            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 70;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Harga Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Harga Satuan"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."SubTotal";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("SunTotal"));$i++)
            {
                $echo.=" ";
            }

            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }

            $echo.="\r\n";

            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;

            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            //$pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
				$namasatuan = $arr_data["detail_namasatuan"][$i_detail];

	            if($namabarang)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 70;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$qty;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($qty));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($satuan,2,',','.');
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen(number_format($satuan,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$namasatuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($namasatuan));$i++)
                    {
                        $echo.=" ";
                    }

				}
				$echo.="\r\n";
				$no++;

            }

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";

            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->Keterangan_;

            $echo .= "\r\n";
            $echo .= "\r\n";


            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }


            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";

            }

            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }

            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }

            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=" (               )             (                 )         (                )";

            $echo .= "\r\n";
            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"purchase-order");

			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);


			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);

			}

            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"purchase-order");

			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "purchase-order",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='purchase-order-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);

		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;

	}


	function doPrint()
	{
		$this->load->helper('terbilang');
		$this->load->library('globallib');
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();

		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";

		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();

		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();


		$total_spasi = 100;
	    $total_spasi_header = 80;
	    $jml_detail  = 8;
	    $ourFileName = "purchase-order-marketing.txt";

		$header = $this->po_marketing_model->getHeader($nodok);
		$detail = $this->po_marketing_model->getDetail_cetak($nodok);
		$note_header = substr($header->Keterangan_,0,40);

		/*echo "<pre>";
		print_r($data["hitungsi"]);
		echo "</pre>";
		die;*/

		$note_header = substr($header->note,0,40);

		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            //$arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,60);
            $arr_data["detail_qty"][$counter] = $val["QtyTerima"];
            //$arr_data["detail_satuan"][$counter] = $val["HargaSatuan"];
            $arr_data["detail_harga"][$counter] = $val["HargaSatuan"];

			$counter++;
		}

        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);

        $nama_dokumen = "PURCHASE ORDER";

        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n";
                $echo.="\r\n";
            }

            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";
            }
            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo .= $arr_epson["cond"].$nama_dokumen;

            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$nodok)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.= $arr_epson["cond"]."No : ".$nodok;

            $echo.="\r\n";

            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglDokumen_;

                $limit_spasi = 45;
                for($i=0;$i<($limit_spasi-strlen($header->TglDokumen_));$i++)
                {
                    $echo.=" ";
                }

                $echo.=$arr_epson["cond"]."Supplier";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Supplier"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Nama;

                $limit_spasi = 45;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }

                $echo.="\r\n";
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Mata Uang";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Mata Uang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->currencycode_;

                $limit_spasi = 45;
                for($i=0;$i<($limit_spasi-strlen($header->currencycode_));$i++)
                {
                    $echo.=" ";
                }


                $echo.=$arr_epson["cond"]."TOP";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("TOP"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->top_;

                $limit_spasi = 45;
                for($i=0;$i<($limit_spasi-strlen($header->top_));$i++)
                {
                    $echo.=" ";
                }

                $echo.="\r\n";
            }

            // baris 3
            {
                $echo.=$arr_epson["cond"]."Estimasi";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Estimasi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglTerima_;

                $limit_spasi = 45;
                for($i=0;$i<($limit_spasi-strlen($header->TglTerima_));$i++)
                {
                    $echo.=" ";
                }


                $echo.=$arr_epson["cond"]."Contact";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Contact"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Contact;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Contact));$i++)
                {
                    $echo.=" ";
                }

                $echo.="\r\n";
            }

            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }

            /*$echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }*/

            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 45;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }

            /*$echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }*/

            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 15;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Total";
            $limit_spasi = 25;
            for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
            {
                $echo.=" ";
            }

            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }

            $echo.="\r\n";

            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;

            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            //$pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            //$satuan = $arr_data["detail_satuan"][$i_detail];
	            $harga = $arr_data["detail_harga"][$i_detail];
	            $total = $qty*$harga;
	            if($namabarang)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }

                    /*$echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }*/

                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 46;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($qty,0,',','.');
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    /*$echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }*/
                    /*$jarak_harga=5;
                    $echo.=number_format($harga,0,',','.');
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_harga));$i++)
                    {
                        $echo.="-";
                    }*/


                        if(strlen(number_format($harga,0,',','.'))==1){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==2){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==3){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==4){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==5){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==6){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==7){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==8){
						$jarak_total=5;
						}else if(strlen(number_format($harga,0,',','.'))==9){
						$jarak_total=5;
						}
					if(strlen(number_format($harga,0,',','.'))==10){
                    $echo.=number_format($harga,0,',','.');
					}else{
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($harga,0,',','.');
					}

                    $limit_spasi = 18;
                    for($i=0;$i<($limit_spasi-strlen(number_format($harga,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

					    if(strlen(number_format($total,0,',','.'))==1){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==2){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==3){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==4){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==5){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==6){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==7){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==8){
						$jarak_total_=5;
						}else if(strlen(number_format($total,0,',','.'))==9){
						$jarak_total_=5;
						}
					if(strlen(number_format($total,0,',','.'))==10){
                    $echo.=number_format($total,0,',','.');
					}else{
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($total,0,',','.'))+$jarak_total_));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($total,0,',','.');
					}

				}
				$echo.="\r\n";
				$no++;

            }

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


						$limit_spasi = 65;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="Subtotal";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Subtotal"));$i++)
						{
							$echo.=" ";
						}
						if(strlen(number_format($header->Jumlah,0,',','.'))==1){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==2){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==3){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==4){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==5){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==6){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==7){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==8){
						$jarak_header->Jumlah=5;
						}else if(strlen(number_format($header->Jumlah,0,',','.'))==9){
						$jarak_header->Jumlah=5;
						}
						if(strlen(number_format($header->Jumlah,0,',','.'))==10){
						$echo.=number_format($header->Jumlah,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($header->Jumlah,0,',','.'))+$jarak_header->Jumlah));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($header->Jumlah,0,',','.');
						}


						$echo .= "\r\n";

						$limit_spasi = 65;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="Diskon";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Diskon"));$i++)
						{
							$echo.=" ";
						}

						if(strlen(number_format($header->DiscHarga,0,',','.'))==1){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==2){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==3){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==4){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==5){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==6){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==7){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==8){
						$jarak_diskon=5;
						}else if(strlen(number_format($header->DiscHarga,0,',','.'))==9){
						$jarak_diskon=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($header->DiscHarga,0,',','.'))+$jarak_diskon));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($header->DiscHarga,0,',','.');


						$echo .= "\r\n";
						$limit_spasi = 65;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="PPN";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("PPN"));$i++)
						{
							$echo.=" ";
						}

						if(strlen(number_format($header->PPn,0,',','.'))==1){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==2){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==3){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==4){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==5){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==6){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==7){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==8){
						$jarak_ppn=5;
						}else if(strlen(number_format($header->PPn,0,',','.'))==9){
						$jarak_ppn=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($header->PPn,0,',','.'))+$jarak_ppn));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($header->PPn,0,',','.');





						$echo .= "\r\n";
						$limit_spasi = 65;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="Grand Total";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Grand Total"));$i++)
						{
							$echo.=" ";
						}

						if(strlen(number_format($header->Total,0,',','.'))==1){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==2){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==3){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==4){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==5){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==6){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==7){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==8){
						$jarak_grand_total=5;
						}else if(strlen(number_format($header->Total,0,',','.'))==9){
						$jarak_grand_total=5;
						}

						if(strlen(number_format($header->Total,0,',','.'))==10){
						$echo.=number_format($header->Total,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($header->Total,0,',','.'))+$jarak_grand_total));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($header->Total,0,',','.');
						}


						/*$echo .= "\r\n";
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Terbilang: ".terbilang($header->Total,$style=4)." Rupiah";

						$echo .= "\r\n";
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Note : ".$header->Keterangan_;*/

            $echo .= "\r\n";
            $echo .= "\r\n";


            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.="Dibuat Oleh,";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Dibuat Oleh,"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Diketahui Oleh,";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Diketahui Oleh,"));$i++)
            {
                $echo.=" ";

            }

            $echo.="Disetujui Oleh,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Disetujui Oleh,"));$i++)
            {
                $echo.=" ";
            }

            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }

            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=" (               )             (                 )         (                )";

            $echo .= "\r\n";
            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($nodok,"purchase-order");

			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 100;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);


			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 100;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);

			}

            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($nodok,"purchase-order");


		        $data = array(
		            'form_data' => "sales-invoice",
		            'noreferensi' => $nodok,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);

		}

		$paths = "path/to/";
	    $name_text_file='purchase-order-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);

		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;

	}

	function create_pdf() {
        $id = $this->uri->segment(4);
        $this->load->library('printreportlib');
        $printlib = new printreportlib();
        $data['header']= $this->po_marketing_model->getHeader($id);
        $data['detail']= $this->po_marketing_model->getDetail_cetak($id);
        $data['pt'] = $printlib->getNamaPT();

		$data['nomor']=$id;
        $html = $this->load->view('transaksi/po_marketing/pdf_po_marketing',$data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_po_marketing.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }

}

?>
