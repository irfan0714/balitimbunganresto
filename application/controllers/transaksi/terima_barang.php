<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class terima_barang extends authcontroller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/terima_barangmodel');
    }

    function index()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);
            $id = $this->input->post('stSearchingKey');
            $id2 = $this->input->post('date1');
            $with = $this->input->post('searchby');
            if ($with == "TglDokumen" || $with == "TglPengiriman") {
                $id = $mylib->ubah_tanggal($id2);
            }
            $this->load->library('pagination');

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = '</span>';
            $config['per_page'] = '12';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['base_url'] = base_url() . 'index.php/transaksi/terima_barang/index/';
            $page = $this->uri->segment(4);
            $config['uri_segment'] = 4;
            $flag1 = "";
            if ($with != "") {
                if ($id != "" && $with != "") {
                    $config['base_url'] = base_url() . 'index.php/transaksi/terima_barang/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                } else {
                    $page = "";
                }
            } else {
                if ($this->uri->segment(5) != "") {
                    $with = $this->uri->segment(4);
                    $id = $this->uri->segment(5);
                    if ($with == "TglPenerimaan") {
                        $id = $mylib->ubah_tanggal($id);
                    }
                    $config['base_url'] = base_url() . 'index.php/transaksi/terima_barang/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                }
            }
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $thnbln = $data['tahun'] . $data['bulan'];
            $data['header'] = array("No Dokumen", "Tanggal", "Supplier", "Nama Supplier", "Gudang", "No Order", "Keterangan", "Konfirmasi");
            $config['total_rows'] = $this->terima_barangmodel->num_terima_barang_row($id, $with, $thnbln);
            $data['data'] = $this->terima_barangmodel->getTerimaBarangList($config['per_page'], $page, $id, $with, $thnbln);
            $data['track'] = $mylib->print_track();
            $this->pagination->initialize($config);
            $this->load->view('transaksi/terima_barang/terima_baranglist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $aplikasi = $this->terima_barangmodel->getDate();
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['aplikasi'] = $aplikasi;
            $tgl = '01' . '-' . $data['bulan'] . '-' . $data['tahun'];
            $data['tanggal'] = $aplikasi->TglTrans;
            if (($data['tahun'] == substr($data['tanggal'], -4)) && ($data['bulan'] == substr($data['tanggal'], 3, 2)))
                $data['tanggal'] = $aplikasi->TglTrans;
            else
                $data['tanggal'] = $tgl;
            $data['gudang'] = $this->terima_barangmodel->getGudang();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/terima_barang/add_terima_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function getSupplier()
    {
        $kode = strtoupper($this->input->post('kdsupplier'));
        $detail = $this->terima_barangmodel->getSupplier($kode);
        if (count($detail) != 0) {
            $nilai = $detail->Nama . "*_*" . $detail->Payment . "*_*" . $detail->TOP . "*_*" . $detail->LimitKredit . "*_*" . $detail->LimitFaktur . "*_*" . $detail->PPn . "*_*" . $detail->KdGroupext;
        } else {
            $nilai = "";
        }
        echo $nilai;
    }

    function getPCode()
    {
        $kode = $this->input->post('pcode');
        $kodeext = $this->input->post('kdgroupext');
        $detail = $this->terima_barangmodel->getPCodeDet($kode, $kodeext);
        if (count($detail) != 0) {
            $nilai = '01*_*' . $detail->PCode . '*_*' . $detail->NamaLengkap . "*_*" . //012
                $detail->SatuanSt . '*_*' . $detail->NamaSt . '*_*' .  //34
                $detail->Harga0b . '*_*' . $detail->Konv0st . '*_*' .  //56
                $detail->Harga1b . '*_*' . $detail->Konv1st . '*_*' .  //78
                $detail->Harga2b . '*_*' . $detail->Konv2st . '*_*' .  //90
                $detail->Harga3b . '*_*' . $detail->Konv3st . '*_*' .  //12
                $detail->KdKategori . '*_*' . $detail->KdBrand . '*_*' .  //34
                $detail->Satuan0 . '*_*' . $detail->Nama0 . '*_*' .   //56
                $detail->Satuan1 . '*_*' . $detail->Nama1 . '*_*' .   //78
                $detail->Satuan2 . '*_*' . $detail->Nama2 . '*_*' .   //90
                $detail->Satuan3 . '*_*' . $detail->Nama3 . '*_*' .   //12
                $detail->PCodeExt . '*_*' . $detail->PPnB;   //34
        } else {
            $nilai = "";
        }
        echo $nilai;
    }

    function delete_terima_barang()
    {
        $id = $this->input->post('kode');
        $header = $this->terima_barangmodel->getHeader($id);
        $detail = $this->terima_barangmodel->getDetail($id);

        $tahun = substr($header->TglDokumen, 0, 4);
        $bulan = substr($header->TglDokumen, 5, 2);
        $this->terima_barangmodel->locktables('trans_terima_detail,trans_terima_header,mutasi_barang,stock_simpan');

        $this->db->update('trans_terima_header', array('status' => 'B'), array('NoDokumen' => $id));
        $this->db->update('trans_terima_detail', array('status' => 'B'), array('NoDokumen' => $id));

        $fieldmasuk1 = "GMasuk" . $bulan;
        $fieldmasuk2 = "GNMasuk" . $bulan;
        $fieldakhir1 = "GAkhir" . $bulan;
        $fieldakhir2 = "GNAkhir" . $bulan;
        $statusstock = 'G'; //WIEOK
        $kdgudang = $header->KdGudang;

        for ($m = 0; $m < count($detail); $m++) {
            $pcode = $detail[$m]['PCode'];
            $qty = $detail[$m]['QtyInput'];
            $qtypcs = $detail[$m]['QtyPcs'];
            $hpp = $detail[$m]['Hpp'];
            $hpppcs = $detail[$m]['Hpp'] / $detail[$m]['KonversiSt'];
            $stok = $this->terima_barangmodel->getStok($fieldmasuk1, $fieldmasuk2, $fieldakhir1, $fieldakhir2, $tahun, $kdgudang, $pcode, $statusstock);
            $datastok = array(
                $fieldmasuk1 => $stok->$fieldmasuk1 - $qtypcs,
                $fieldmasuk2 => $stok->$fieldmasuk2 - ($qty * $hpp),
                $fieldakhir1 => $stok->$fieldakhir1 - $qtypcs,
                $fieldakhir2 => $stok->$fieldakhir2 - ($qty * $hpp)
            );
            $this->db->update('stock_simpan', $datastok, array('Tahun' => $tahun, 'KdGudang' => $kdgudang, 'PCode' => $pcode));
            if ($header->SumberOrder == "O") {
                $this->terima_barangmodel->getUpdateQtyKirim($header->NoOrder, $pcode, -$qtypcs);
            }
        }
        if ($header->SumberOrder == "O") {
            $StatusOrder = $this->terima_barangmodel->getStatusOrder($header->NoOrder);
            if ($StatusOrder->Item > 0 && $StatusOrder->Item == $StatusOrder->Status) {
                $this->db->update('trans_order_barang_header', array("FlagTutup" => "Y"), array('NoDokumen' => $header->NoOrder));
            } else {
                $this->db->update('trans_order_barang_header', array("FlagTutup" => "T"), array('NoDokumen' => $header->NoOrder));
            }
            if ($StatusOrder->Kirim == 0) {
                $this->db->update('trans_order_barang_header', array("FlagPengiriman" => "T"), array('NoDokumen' => $header->NoOrder));
            }
        }
        $this->db->delete('mutasi_barang', array('KdTransaksi' => 'FB', 'NoTransaksi' => $id));
        $this->terima_barangmodel->unlocktables();
    }

    function cetak()
    {
        $data = $this->varCetak();
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
    }

    function printThis()
    {
        $data = $this->varCetak();
        $id = $this->uri->segment(4);
        $data['fileName2'] = "terima_barang.sss";
        $data['fontstyle'] = chr(27) . chr(80);
        $data['nfontstyle'] = "";
        $data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
        $data['string1'] = "     Dibuat Oleh                       Menyetujui";
        $data['string2'] = "(                     )         (                      )";
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer', $data);
    }

    function varCetak()
    {
        $this->load->library('printreportlib');
        $mylib = new printreportlib();
        $id = $this->uri->segment(4);
        $header = $this->terima_barangmodel->getHeader($id);
        $data['header'] = $header;
        $detail = $this->terima_barangmodel->getDetail($id);
        $data['judul1'] = array("No Terima", "Tanggal Terima", "Supplier", "Keterangan");
        $data['niljudul1'] = array($header->NoDokumen, $header->Tgl, $header->KdSupplier . " - " . stripslashes($header->Nama), stripslashes($header->Keterangan));
        $data['judul2'] = array("No Order", "Lokasi");
        $data['niljudul2'] = array(stripslashes($header->NoOrder), stripslashes($header->NamaLokasi));
        $data['judullap'] = "Penerimaan Barang";
        $data['url'] = "terima_barang/printThis/" . $id;
        $data['colspan_line'] = 4;
        $data['tipe_judul_detail'] = array("normal", "normal", "kanan", "normal");
        $data['judul_detail'] = array("KdBarang", "Nama", "Qty", "");
        $data['panjang_kertas'] = 30;
        $default_page_written = 21;
        $data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
        if ($data['panjang_per_hal'] != 0) {
            $data['tot_hal'] = ceil((int)count($detail) / (int)$data['panjang_per_hal']);
        } else {
            $data['tot_hal'] = 1;
        }
        $list_detail = array();
        $detail_page = array();
        $counterRow = 0;
        $max_field_len = array(0, 0, 0, 0);
        for ($m = 0; $m < count($detail); $m++) {
            unset($list_detail);
            $counterRow++;
            $list_detail[] = stripslashes($detail[$m]['PCode']);
            $list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
            $list_detail[] = $detail[$m]['QtyInput'];
            $list_detail[] = $detail[$m]['NamaSatuan'];
            $detail_page[] = $list_detail;
            $max_field_len = $mylib->get_max_field_len($max_field_len, $list_detail);
            if ($data['panjang_per_hal'] != 0) {
                if (((int)$m + 1) % $data['panjang_per_hal'] == 0) {
                    $data['detail'][] = $detail_page;
                    if ($m != count($detail) - 1) {
                        unset($detail_page);
                    }
                }
            }
        }
        $data['detail'][] = $detail_page;
        $data['footer1'] = array(); //array("Jumlah Order","PPn Jual (".$header->PPn."%)","Total Order");
        $data['footer2'] = array(); //array(round($header->Jumlah),round($header->PPn/100*$header->Jumlah),round($header->Total));
        $data['max_field_len'] = $max_field_len;
        $data['banyakBarang'] = $counterRow;
        return $data;
    }

    function edit_terima_barang($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->terima_barangmodel->getHeader($id);
            $data['detail'] = $this->terima_barangmodel->getDetail($id);
            $data['mgudang'] = $this->terima_barangmodel->getGudang();
            $data['gudang'] = $data['header']->KdGudang;
            if ($data['header']->SumberOrder == "M") {
                $data['sumberM'] = "checked='checked'";
                $data['sumberO'] = "";
                $data['fromPO'] = "0";
            } else {
                $data['sumberO'] = "checked='checked'";
                $data['sumberM'] = "";
                $data['fromPO'] = "1";
            }
            if ($data['header']->Payment == "C") {
                $data['sumberC'] = "checked='checked'";
                $data['sumberK'] = "";
                $data['topdisabled'] = "disabled";
            } else {
                $data['sumberK'] = "checked='checked'";
                $data['sumberC'] = "";
                $data['topdisabled'] = "";
            }
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/terima_barang/edit_terima_barang', $data);

        } else {
            $this->load->view('denied');
        }
    }

    function getsumber()
    {
        $order = $this->input->post('order');
        if ($order != "") {
            $data = $this->terima_barangmodel->getOrderHeader($order);
            $hasil = $this->terima_barangmodel->getOrderDetail($order);
        }
        $strdet = "";
        $strdetail = "";
        if (count($data) <> 0) {
            for ($s = count($hasil) - 1; $s >= 0; $s--) {
                $satuan = $this->terima_barangmodel->getSatuan($hasil[$s]['PCode']);
                $strdet .= $hasil[$s]['PCode'] . "~" . $hasil[$s]['QtyInput'] . "~" . $hasil[$s]['QtyPcs'] . "~" . $hasil[$s]['NamaLengkap'] . "~" . $hasil[$s]['Satuan'] . "~" . $hasil[$s]['PCodeExt'] . "~" . $hasil[$s]['PCodeBarang'] . "~" . $hasil[$s]['Disc1'] . "~" . $hasil[$s]['Disc2'] . "~" . $hasil[$s]['Harga'] . "~" . $hasil[$s]['Jumlah'] . "~" . $hasil[$s]['PPn'] . "~" .
                    round($hasil[$s]['Total']) . "~" . $hasil[$s]['KonversiSt'] . "~" . $hasil[$s]['SatuanSt'] . "~" . $hasil[$s]['NamaSatuanSt'] . "~" . $hasil[$s]['KdBrand'] . "~" . $hasil[$s]['KdKategori'] . "~" . $hasil[$s]['NamaSatuan'] . "~" . $hasil[$s]['NamaKategori'] . "~" .
                    $hasil[$s]['NamaBrand'] . "~" .  //20
                    $satuan->Satuan1 . "~" .     //21
                    $satuan->Nama1 . "~" .            //22
                    $satuan->Satuan2 . "~" .     //23
                    $satuan->Nama2 . "~" .            //24
                    $satuan->Satuan3 . "~" .     //25
                    $satuan->Nama3 . "**";           //26

            }
            $strdetail = $data->NoDokumen . "-.-" . $data->KdGudang . "-.-" . $data->KdSupplier . "-.-" . $data->KdGroupext . "-.-" . $data->Payment . "-.-" . $data->TOP . "-.-" . $data->Keterangan . "-.-" . $data->Jumlah . "-.-" . $data->PPn . "-.-" . $data->NilaiPPn . "-.-" . $data->Total . "-.-" . $data->Nama . "-.-" . $strdet;
        }
        echo $strdetail;
    }

    function save_new_terima()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        $sumber = $this->input->post('hiddensumber');
        $tgljto = $this->input->post('tgljto');
        $user = $this->session->userdata('userid');
        $flag = $this->input->post('flag');
        $no = $this->input->post('nodok');
        $tgl = $this->input->post('tgl');
        $kdsupplier = strtoupper($this->input->post('hiddensupplier'));
        $kdgudang = $this->input->post('hiddengudang');
        $payment = $this->input->post('sumber');
        $top = $this->input->post('top');
        $sumberMO = $this->input->post('hiddensumberMO');
        $noorder = trim(strtoupper(addslashes($this->input->post('hiddennoorder'))));
        $ket = trim(strtoupper(addslashes($this->input->post('ket'))));
        $jumlah = $this->input->post('jumlah');
        $ppn = $this->input->post('ppn');
        $total = $this->input->post('total');
        $pcode1 = $this->input->post('pcode');
        $qty1 = $this->input->post('qty');
        $pcodesave1 = $this->input->post('savepcode');
        $satuan1 = $this->input->post('satuan');
        $hargab1 = $this->input->post('hargab');
        $disc11 = $this->input->post('disc1');
        $disc21 = $this->input->post('disc2');
        $totalb1 = $this->input->post('totalb');
        $konversi1 = $this->input->post('konversi');
        $satuanst1 = $this->input->post('satuanst');
        $nilsatuan1 = $this->input->post('nilsatuan');
        $nilsatuanst1 = $this->input->post('nilsatuanst');
        $kdkategori1 = $this->input->post('kdkategori');
        $kdbrand1 = $this->input->post('kdbrand');

        $kdgroupext = strtoupper($this->input->post('kdgroupext'));
        $nilaippn = $this->input->post('nilaippn');
        $extcode1 = $this->input->post('extcode');
        $jumlahb1 = $this->input->post('jumlahb');
        $ppnb1 = $this->input->post('ppnb');

        if ($no == "") {
            $counter = "1";
            $no = $this->insertNewHeader($flag, $mylib->ubah_tanggal($tgl), $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $mylib->ubah_tanggal($tgljto), $sumberMO, $noorder, $kdgudang);
        } else {
            $counter = $this->updateHeader($flag, $no, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $mylib->ubah_tanggal($tgljto), $sumberMO, $noorder, $kdgudang, $mylib->ubah_tanggal($tgl));
        }
        //echo "<pre>";print_r($pcode1);
        //echo "<pre>";print_r($satuan1);
        for ($x0 = 0; $x0 < count($pcode1); $x0++) {
            $pcode = strtoupper(addslashes(trim($pcode1[$x0])));
            $qty = trim($qty1[$x0]);
            $pcodesave = $pcodesave1[$x0];
            $satuan = $satuan1[$x0];
            $extcode = $extcode1[$x0];
            $hargab = $hargab1[$x0];
            $disc1 = $disc11[$x0];
            $disc2 = $disc21[$x0];
            $jumlahb = $jumlahb1[$x0];
            $ppnb = $ppnb1[$x0];
            $totalb = $totalb1[$x0];
            $konversi = $konversi1[$x0];
            $satuanst = $satuanst1[$x0];
            $nilsatuan = $nilsatuan1[$x0];
            $nilsatuanst = $nilsatuanst1[$x0];
            $kdkategori = $kdkategori1[$x0];
            $kdbrand = $kdbrand1[$x0];
            if ($pcode != "") {
                //echo "masuk";die();
                $this->insertDetail($flag, $no, $counter, $pcode, $qty, $satuan, $extcode, $user, $pcodesave, $hargab, $disc1, $disc2, $jumlahb, $ppnb, $totalb, $konversi, $satuanst, $nilsatuan, $nilsatuanst, $kdkategori, $kdbrand, $kdsupplier, $kdgudang, $mylib->ubah_tanggal($tgl), $sumberMO, $ket, $noorder);
                $counter++;
            }
        }
        redirect('/transaksi/terima_barang/');
    }

    function save_new_item()
    {
        $mylib = new globallib();
        $user = $this->session->userdata('userid');
        $flag = $this->input->post('flag');
        $no = $this->input->post('no');
        $tgl = $this->input->post('tgl');
        $kdsupplier = strtoupper($this->input->post('kdsupplier'));
        $kdgudang = $this->input->post('gudang');
        $payment = $this->input->post('sumber');
        $top = $this->input->post('top');
        $tgljto = $this->input->post('tgljto');
        $sumberMO = $this->input->post('sumberMO');
        $noorder = trim(strtoupper(addslashes($this->input->post('noorder'))));
        $ket = trim(strtoupper(addslashes($this->input->post('ket'))));
        $jumlah = $this->input->post('jumlah');
        $ppn = $this->input->post('ppn');
        $total = $this->input->post('total');
        $pcode = strtoupper(addslashes(trim($this->input->post('pcode'))));
        $qty = $this->input->post('qty');
        $disc1 = $this->input->post('disc1');
        $disc2 = $this->input->post('disc2');
        $satuan = $this->input->post('satuan');
        $hargab = $this->input->post('hargab');
        $totalb = $this->input->post('totalb');
        $konversi = $this->input->post('konversi');
        $satuanst = $this->input->post('satuanst');
        $nilsatuan = $this->input->post('nilsatuan');
        $nilsatuanst = $this->input->post('nilsatuanst');
        $kdkategori = $this->input->post('kdkategori');
        $kdbrand = $this->input->post('kdbrand');
        $pcodesave = $this->input->post('pcodesave');

        $kdgroupext = strtoupper($this->input->post('kdgroupext'));
        $nilaippn = $this->input->post('nilaippn');
        $extcode = $this->input->post('extcode');
        $jumlahb = $this->input->post('jumlahb');
        $ppnb = $this->input->post('ppnb');

        $message = "";

        if ($no == "") {
            $counter = "1";
            $no = $this->insertNewHeader($flag, $mylib->ubah_tanggal($tgl), $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $mylib->ubah_tanggal($tgljto), $sumberMO, $noorder, $kdgudang);
        } else {
            $counter = $this->updateHeader($flag, $no, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $mylib->ubah_tanggal($tgljto), $sumberMO, $noorder, $kdgudang, $mylib->ubah_tanggal($tgl));
        }
        $this->insertDetail($flag, $no, $counter, $pcode, $qty, $satuan, $extcode, $user, $pcodesave, $hargab, $disc1, $disc2, $jumlahb, $ppnb, $totalb, $konversi, $satuanst, $nilsatuan, $nilsatuanst, $kdkategori, $kdbrand, $kdsupplier, $kdgudang, $mylib->ubah_tanggal($tgl), $sumberMO, $ket, $noorder);
        echo $no;
    }

    function insertNewHeader($flag, $tgl, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $tgljto, $sumberMO, $noorder, $kdgudang)
    {
        $this->terima_barangmodel->locktables('counter,trans_terima_header');
        $new_no = $this->terima_barangmodel->getNewNo($tgl);
        $no = $new_no->NoTerimaBarang;
        $data = array(
            'NoDokumen' => $no,
            'TglDokumen' => $tgl,
            'TglJto' => $tgljto,
            'SumberOrder' => $sumberMO,
            'NoOrder' => $noorder,
            'Keterangan' => $ket,
            'KdSupplier' => $kdsupplier,
            'KdGroupext' => $kdgroupext,
            'KdGudang' => $kdgudang,
            'Payment' => $payment,
            'Top' => $top,
            'AddDate' => $tgl,
            'AddUser' => $user,
            'Jumlah' => $jumlah,
            'PPn' => $ppn,
            'NilaiPPn' => $nilaippn,
            'Total' => $total,
            'Status' => ' '
        );
        $this->db->insert('trans_terima_header', $data);
        if ($sumberMO == "O") {
            $this->db->update('trans_order_barang_header', array("FlagPengiriman" => "Y"), array('NoDokumen' => $noorder));
        }
        $this->terima_barangmodel->unlocktables();
        return $no;
    }

    function updateHeader($flag, $no, $kdsupplier, $kdgroupext, $ket, $user, $jumlah, $ppn, $nilaippn, $total, $payment, $top, $tgljto, $sumberMO, $noorder, $kdgudang, $tgl)
    {
        $this->terima_barangmodel->locktables('trans_terima_header,trans_terima_detail');
        $count = $this->terima_barangmodel->getCounter($no);
        $counter = (int)$count->Counter + 1;
        $data = array(
            'TglDokumen' => $tgl,
            'Keterangan' => $ket,
            'TglJto' => $tgljto,
            'SumberOrder' => $sumberMO,
            'NoOrder' => $noorder,
            'KdSupplier' => $kdsupplier,
            'KdGroupext' => $kdgroupext,
            'KdGudang' => $kdgudang,
            'Jumlah' => $jumlah,
            'PPn' => $ppn,
            'NilaiPPn' => $nilaippn,
            'Total' => $total
        );
        if ($flag == "edit") {
            $data['EditDate'] = $tgl;
            $data['EditUser'] = $user;
        }
        $this->db->update('trans_terima_header', $data, array('NoDokumen' => $no));
        $data = array(
            'TglDokumen' => $tgl
        );
        $this->db->update('trans_terima_detail', $data, array('NoDokumen' => $no));
        $this->terima_barangmodel->unlocktables();
        return $counter;
    }

    function insertDetail($flag, $no, $counter, $pcode, $qty, $satuan, $extcode, $user, $pcodesave, $hargab, $disc1, $disc2, $jumlahb, $ppnb, $totalb, $konversi, $satuanst, $nilsatuan, $nilsatuanst, $kdkategori, $kdbrand, $kdsupplier, $kdgudang, $tgl, $sumberMO, $ket, $noorder)
    {
        $tgltrans = $this->session->userdata('Tanggal_Trans');
        $this->terima_barangmodel->locktables('trans_terima_detail,trans_order_barang_detail,mutasi_barang,stock_simpan');
        $qtybef = $this->terima_barangmodel->getQtyBefore($no, $pcode);

        $qtypcs = $qty * $konversi;
        $hpppcs = $hargab / $konversi;
        if ($pcodesave == "") {
            $data = array(
                'NoDokumen' => $no,
                'TglDokumen' => $tgl,
                'KdSupplier' => $kdsupplier,
                'KdGudang' => $kdgudang,
                'PCode' => $pcode,
                'PCodeExt' => $extcode,
                'Counter' => $counter,
                'QtyInput' => $qty,
                'QtyPcs' => $qtypcs,
                'Satuan' => $satuan,
                'Harga' => $hargab,
                'Disc1' => $disc1,
                'Disc2' => $disc2,
                'Jumlah' => $jumlahb,
                'PPn' => $ppnb,
                'Total' => $totalb,
                'NamaSatuan' => $nilsatuan,
                'SatuanSt' => $satuanst,
                'NamaSatuanSt' => $nilsatuanst,
                'KonversiSt' => $konversi,
                'KdKategori' => $kdkategori,
                'KdBrand' => $kdbrand,
                'Status' => ' '
            );
            if ($flag == "add") {
                $data['AddDate'] = $tgl;
                $data['AddUser'] = $user;
            } else {
                $data['EditDate'] = $tgltrans;
                $data['EditUser'] = $user;
            }
            $this->db->insert('trans_terima_detail', $data);
            if ($sumberMO == "O") {
                $this->terima_barangmodel->getUpdateQtyKirim($noorder, $pcode, $qtypcs);
            }
        } else if ($pcodesave != $pcode) {
            $data = array(
                'PCode' => $pcode,
                'PCodeExt' => $extcode,
                'QtyInput' => $qty,
                'QtyPcs' => $qtypcs,
                'Satuan' => $satuan,
                'Harga' => $hargab,
                'Disc1' => $disc1,
                'Disc2' => $disc2,
                'Jumlah' => $jumlahb,
                'PPn' => $ppnb,
                'Total' => $totalb,
                'NamaSatuan' => $nilsatuan,
                'SatuanSt' => $satuanst,
                'NamaSatuanSt' => $nilsatuanst,
                'KonversiSt' => $konversi,
                'KdKategori' => $kdkategori,
                'KdBrand' => $kdbrand
            );
            if ($flag == "edit") {
                $data['EditDate'] = $tgltrans;
                $data['EditUser'] = $user;
            }
            $this->db->update('trans_terima_detail', $data, array('NoDokumen' => $no, 'PCode' => $pcodesave));
            if ($sumberMO == "O") {
                $this->terima_barangmodel->getUpdateQtyKirim($noorder, $pcodesave, -$qtybef->QtyPcs);
                $this->terima_barangmodel->getUpdateQtyKirim($noorder, $pcode, $qtypcs);
            }
        } else if ($pcodesave == $pcode) {
            $data = array(
                'QtyInput' => $qty,
                'QtyPcs' => $qtypcs,
                'Satuan' => $satuan,
                'Harga' => $hargab,
                'Disc1' => $disc1,
                'Disc2' => $disc2,
                'Jumlah' => $jumlahb,
                'PPn' => $ppnb,
                'Total' => $totalb,
                'NamaSatuan' => $nilsatuan,
                'SatuanSt' => $satuanst,
                'NamaSatuanSt' => $nilsatuanst,
                'KonversiSt' => $konversi,
                'KdKategori' => $kdkategori,
                'KdBrand' => $kdbrand
            );
            if ($flag == "edit") {
                $data['EditDate'] = $tgltrans;
                $data['EditUser'] = $user;
            }
            $this->db->update('trans_terima_detail', $data, array('NoDokumen' => $no, 'PCode' => $pcode));
            if ($sumberMO == "O") {
                $this->terima_barangmodel->getUpdateQtyKirim($noorder, $pcode, -$qtybef->QtyPcs + $qtypcs);
            }
        }
        if ($sumberMO == "O") {
            $StatusOrder = $this->terima_barangmodel->getStatusOrder($noorder);
            if ($StatusOrder->Item > 0 && $StatusOrder->Item == $StatusOrder->Status) {
                $this->db->update('trans_order_barang_header', array("FlagTutup" => "Y"), array('NoDokumen' => $noorder));
            } else {
                $this->db->update('trans_order_barang_header', array("FlagTutup" => "T"), array('NoDokumen' => $noorder));
            }
        }
        //--------------------stock-------------------------------
        $tahun = substr($tgl, 0, 4);
        $bulan = substr($tgl, 5, 2);
        $fieldmasuk1 = "GMasuk" . $bulan;
        $fieldmasuk2 = "GNMasuk" . $bulan;
        $fieldakhir1 = "GAkhir" . $bulan;
        $fieldakhir2 = "GNAkhir" . $bulan;
        $statusstock = 'G';
        $stok = $this->terima_barangmodel->getStok($fieldmasuk1, $fieldmasuk2, $fieldakhir1, $fieldakhir2, $tahun, $kdgudang, $pcode, $statusstock);
        $hpp = $hargab;

        if ($pcodesave == "") {
            $datamutasi = array(
                'KdTransaksi' => "FB",
                'NoTransaksi' => $no,
                'TglTransaksi' => $tgl,
                'PCode' => $pcode,
                'Qty' => $qtypcs,
                'Harga' => $hpppcs,
                'Jumlah' => $qty * $hpp,
                'KdGudang' => $kdgudang,
                'JenisMutasi' => "M",
                'Keterangan' => $ket
            );
            $this->db->insert('mutasi_barang', $datamutasi);
            if (empty($stok)) {
                $datastok = array(
                    'Tahun' => $tahun,
                    'KdGudang' => $kdgudang,
                    'PCode' => $pcode,
                    'Status' => $statusstock,
                    $fieldmasuk1 => $qtypcs,
                    $fieldmasuk2 => $qty * $hpp,
                    $fieldakhir1 => $qtypcs,
                    $fieldakhir2 => $qty * $hpp
                );
                $this->db->insert('stock_simpan', $datastok);
            } else {
                $datastok = array(
                    $fieldmasuk1 => $stok->$fieldmasuk1 + $qtypcs,
                    $fieldmasuk2 => $stok->$fieldmasuk2 + ($qty * $hpp),
                    $fieldakhir1 => $stok->$fieldakhir1 + $qtypcs,
                    $fieldakhir2 => $stok->$fieldakhir2 + ($qty * $hpp)
                );
                $this->db->update('stock_simpan', $datastok, array('Tahun' => $tahun, 'KdGudang' => $kdgudang, 'PCode' => $pcode, 'Status' => $statusstock));
            }
        } else {
            $datamutasi = array(
                'PCode' => $pcode,
                'Qty' => $qtypcs,
                'Harga' => $hpppcs,
                'Jumlah' => $qty * $hpp
            );
            $this->db->update('mutasi_barang', $datamutasi, array('KdTransaksi' => 'FB', 'NoTransaksi' => $no, 'PCode' => $pcodesave));
            $stok = $this->terima_barangmodel->getStok($fieldmasuk1, $fieldmasuk2, $fieldakhir1, $fieldakhir2, $tahun, $kdgudang, $pcodesave, $statusstock);
            $hpp = $hargab;

            /*
            echo $pcodesave."<br>";
			echo $pcode."<br>";
			echo $kdgudang."<br>";
			echo $qtybef->Qty."<br>";
			echo $qtypcs."<br>";
			*/

            $datastok = array(
                $fieldmasuk1 => $stok->$fieldmasuk1 - $qtybef->QtyPcs,
                $fieldmasuk2 => $stok->$fieldmasuk2 - ($qtybef->Qty * $qtybef->Hpp),
                $fieldakhir1 => $stok->$fieldakhir1 - $qtybef->QtyPcs,
                $fieldakhir2 => $stok->$fieldakhir2 - ($qtybef->Qty * $qtybef->Hpp)
            );
            //echo "<pre>";print_r($datastok);
            $this->db->update('stock_simpan', $datastok, array('Tahun' => $tahun, 'KdGudang' => $kdgudang, 'PCode' => $pcodesave));

            $stok = $this->terima_barangmodel->getStok($fieldmasuk1, $fieldmasuk2, $fieldakhir1, $fieldakhir2, $tahun, $kdgudang, $pcode, $statusstock);

            $datastok = array(
                $fieldmasuk1 => $stok->$fieldmasuk1 + $qtypcs,
                $fieldmasuk2 => $stok->$fieldmasuk2 + ($qty * $hpp),
                $fieldakhir1 => $stok->$fieldakhir1 + $qtypcs,
                $fieldakhir2 => $stok->$fieldakhir2 + ($qty * $hpp)
            );
            //echo "<pre>";print_r($datastok);
            $this->db->update('stock_simpan', $datastok, array('Tahun' => $tahun, 'KdGudang' => $kdgudang, 'PCode' => $pcode));
        }
        $data = array(
            'Hpp' => $hpp
        );
        $this->db->update('trans_terima_detail', $data, array('NoDokumen' => $no, 'PCode' => $pcode));
        //=====================================
        $this->terima_barangmodel->unlocktables();
    }

    function delete_item()
    {
        $mylib = new globallib();
        $tgltrans = $this->session->userdata('Tanggal_Trans');

        $tahun = substr($tgltrans, 0, 4);
        $bulan = substr($tgltrans, 5, 2);
        $fieldmasuk1 = "GMasuk" . $bulan;
        $fieldmasuk2 = "GNMasuk" . $bulan;
        $fieldakhir1 = "GAkhir" . $bulan;
        $fieldakhir2 = "GNAkhir" . $bulan;
        $statusstock = 'G';

        $id = $this->input->post('no');
        $sumberMO = $this->input->post('sumberMO');
        $noorder = $this->input->post('noorder');
        $pcode = $this->input->post('pcode');
        $kdgudang = strtoupper($this->input->post('gudang'));
        $jumlah = $this->input->post('jumlah');
        $ppn = $this->input->post('ppn');
        $nilaippn = $this->input->post('nilaippn');
        $total = $this->input->post('total');
        $this->terima_barangmodel->locktables('trans_terima_detail,mutasi_barang,stock_simpan');
        $stok = $this->terima_barangmodel->getStok($fieldmasuk1, $fieldmasuk2, $fieldakhir1, $fieldakhir2, $tahun, $kdgudang, $pcode, $statusstock);
        $qtybef = $this->terima_barangmodel->getQtyBefore($id, $pcode);
        $this->db->delete('mutasi_barang', array('KdTransaksi' => 'FB', 'NoTransaksi' => $id, 'PCode' => $pcode));
        $datastok = array(
            $fieldmasuk1 => $stok->$fieldmasuk1 - $qtybef->QtyPcs,
            $fieldmasuk2 => $stok->$fieldmasuk2 - ($qtybef->Qty * $qtybef->Hpp),
            $fieldakhir1 => $stok->$fieldakhir1 - $qtybef->QtyPcs,
            $fieldakhir2 => $stok->$fieldakhir2 - ($qtybef->Qty * $qtybef->Hpp)
        );
        $this->db->update('stock_simpan', $datastok, array('Tahun' => $tahun, 'KdGudang' => $kdgudang, 'PCode' => $pcode, 'Status' => $statusstock));
        $this->db->delete('trans_terima_detail', array('NoDokumen' => $id, 'PCode' => $pcode));

        $data = array(
            'Jumlah' => $jumlah,
            'PPn' => $ppn,
            'NilaiPPn' => $nilaippn,
            'Total' => $total
        );
        $this->db->update('trans_terima_header', $data, array('NoDokumen' => $id));

        if ($sumberMO == "O") {
            $this->terima_barangmodel->getUpdateQtyKirim($noorder, $pcode, -$qtybef->QtyPcs);
            $StatusOrder = $this->terima_barangmodel->getStatusOrder($noorder);
            if ($StatusOrder->Item > 0 && $StatusOrder->Item == $StatusOrder->Status) {
                $this->db->update('trans_order_barang_header', array("FlagTutup" => "Y"), array('NoDokumen' => $noorder));
            } else {
                $this->db->update('trans_order_barang_header', array("FlagTutup" => "T"), array('NoDokumen' => $noorder));
            }
            if ($StatusOrder->Kirim == 0) {
                $this->db->update('trans_order_barang_header', array("FlagPengiriman" => "T"), array('NoDokumen' => $noorder));
            }
        }
        $this->terima_barangmodel->unlocktables();
    }
}

?>