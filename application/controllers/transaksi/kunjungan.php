<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Kunjungan extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/kunjungan_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_salesman"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "kunjungan_header", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_salesman = explode("=", $arrSearch[1]); // search salesman
                    $arr_search["search"]["salesman"] = $search_salesman[1];
                    $search_status = explode("=", $arrSearch[2]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_salesman"] = $search_salesman[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/kunjungan/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/kunjungan/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
            
            $config['total_rows'] = $this->kunjungan_model->num_kunjungan_row($arr_search["search"]);
            $data['data'] = $this->kunjungan_model->getKunjunganList($config['per_page'], $page, $arr_search["search"]);
			$data['salesman'] = $this->kunjungan_model->getSalesman();
			
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/kunjungan/kunjungan_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
		
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'kunjungan_header', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_salesman=".$this->input->post('search_salesman');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "kunjungan_header",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/kunjungan/index/' . $query_id . '');
    }

    function add_new() 
    {
	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $data['salesman'] = $this->kunjungan_model->getSalesman();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/kunjungan/add_kunjungan', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_kunjungan($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->kunjungan_model->getHeader($id);			
            $data['salesman'] = $this->kunjungan_model->getSalesman();
            $data['detail_list'] = $this->kunjungan_model->getDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/kunjungan/edit_kunjungan', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_kunjungan($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->kunjungan_model->getHeader($id);			
            $data['salesman'] = $this->kunjungan_model->getSalesman();
            $data['detail_list'] = $this->kunjungan_model->getDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/kunjungan/view_kunjungan', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_salesman = $this->input->post('v_salesman');
        $v_note = $this->input->post('v_note');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
		$data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        // detail
        $v_kdtour1 = $this->input->post('kdtour');
        
        if ($flag == "add")
		{
		    //pertama generate No Kunjungan
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "kunjungan_header","noku", "KJ", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di kunjungan header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_salesman, $v_note, $user);
        
			
			for ($x = 0; $x < count($v_kdtour1); $x++) 
			{
            $kdtour = strtoupper(addslashes(trim($v_kdtour1[$x])));
            $nourut = $x + 1;
			
			//ketiga masukkan di kunjungan Detail
			$this->insertDetail($v_no_dokumen,$nourut,$kdtour);			
			}			
		} 
		
		
		else if ($flag == "edit") 
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			$v_status = $this->input->post('v_status');
			
			//update kunjungan header
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_salesman, $v_note, $v_status, $user);
            
				        // ubah detail
				        for ($x = 0; $x < count($v_kdtour1); $x++) 
				        {
				            $kdtour = strtoupper(addslashes(trim($v_kdtour1[$x])));
							$nourut= $x + 1;

				            if ($kdtour != "") 
				            {       
							//cek delivery order detail
				            		$detail = $this->kunjungan_model->cekGetDetail($kdtour,$v_no_dokumen);
				            		
				            		if($detail->KdTour==$kdtour)
									{
										$this->updateDetail($v_no_dokumen, $nourut, $kdtour);
									}
									else
									{
										$this->insertDetail($v_no_dokumen,$nourut,$kdtour);
									}
	
				            }
				        }
				      
            		
        }
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
   

        
        redirect('/transaksi/kunjungan/edit_kunjungan/' . $v_no_dokumen . '');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $salesman, $note, $user) 
    {
        $this->kunjungan_model->locktables('kunjungan_header');

        $data = array(
            'noku' => $no_dokumen,
            'tgl_kunjungan' => $tgl_dokumen,
            'KdSalesman' => $salesman,
			'note' => $note,
            'status' => 1,            
            'AddDate'=>date('Y-m-d'),
            'AddUser' => $user
        );

        $this->db->insert('kunjungan_header', $data);

        $this->kunjungan_model->unlocktables();

    }
    
  
    function updateHeader($no_dokumen, $tgl_dokumen, $salesman, $note, $status, $user) 
    {
        $this->kunjungan_model->locktables('kunjungan_header');

        $data = array(
            'tgl_kunjungan' => $tgl_dokumen,
            'KdSalesman' => $salesman,
			'note' => $note,
			'status'=> 1,            
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->update('kunjungan_header', $data, array('noku' => $no_dokumen));
        
        $this->kunjungan_model->unlocktables();
    }

    function insertDetail($no_dokumen, $nourut, $kdtour) 
    {
        $this->kunjungan_model->locktables('kunjungan_detail');

        if ($kdtour) {
            $data = array(
                'noku' => $no_dokumen,
                'no_urut' => $nourut,
                'KdTour' => $kdtour
            );

            $this->db->insert('kunjungan_detail', $data);
        }

        $this->kunjungan_model->unlocktables();
    }
	
	
	
    function updateDetail($no_dokumen,$nourut,$kdtour)
    {
		$this->kunjungan_model->locktables('kunjungan_detail');

        if ($kdtour) 
        {
            $data = array(
                'nourut' => $nourut
            );
            
            $this->db->update('kunjungan_detail', $data, array('noku' => $no_dokumen,'KdTour' => $kdtour));
        } 
        
        $this->kunjungan_model->unlocktables();
    }
	
	function delete_trans($id) 
    {       
	        //kunjungan detail
            //$this->db->delete('kunjungan_detail', array('noku' => $id));	

			//kunjungan header
            //$this->db->delete('kunjungan_header', array('noku' => $id));
            
            $data = array(
			'status'=> 2
	        );

	        $this->db->update('kunjungan_header', $data, array('noku' => $id));			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses Batal <strong>No Kunjungan ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/kunjungan/');
    }

    function delete_detail() 
    {
        $id_kunj_detail = $this->uri->segment(4);
        $kdtour = $this->uri->segment(5);
        $noku= $this->uri->segment(6);
		
		//baru hapus di deliveryorder
		$this->db->delete('kunjungan_detail', array('id_kunj_detail' => $id_kunj_detail, 'noku' => $noku, 'KdTour' => $kdtour));
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>Kode Travel ' . $kdtour . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/kunjungan/edit_kunjungan/' . $noku . '');
    }           

	function updateStock3($thn_periode,$v_warehouse,$pcode,$detail_quantity,$stock_tabel_field,$tabel_field)
    {
    	$jml_update=$stock_tabel_field-$detail_quantity;
        $this->kunjungan_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->kunjungan_model->unlocktables();
    }
    
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "S U R A T   J A L A N";
		$data["header"] 	= $this->kunjungan_model->getHeader($nodok);
		$data["detail"] 	= $this->kunjungan_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_do', $data);
	}
	
	
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "delivery-order.txt";
		
		$header = $this->kunjungan_model->getHeader($nodok);
		$detail = $this->kunjungan_model->getDetail_cetak($nodok);
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "S U R A T  J A L A N";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->dono)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->dono;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->adddate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->adddate));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Gudang";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Keterangan; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Kepada";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Kepada"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                
                
                $echo.="\r\n";
                
                $echo.=$arr_epson["cond"]."Alamat";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Alamat"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Alamat;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Alamat));$i++)
                {
                    $echo.=" ";
                }
                $echo.="\r\n";             
                   
            }          
           
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->note;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->dono,"delivery-order");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->dono,"delivery-order");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "delivery-order",
		            'noreferensi' => $header->dono,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='delivery-order-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
		
}

?>