<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class pelunasan_hutang extends authcontroller {

    function __construct() {
        parent::__construct();
//        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/pelunasan_hutangmodel');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
                
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "pelunasan_hutang", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_jenis = explode("=", $arrSearch[1]); // search jenis
                    $arr_search["search"]["jenis"] = $search_jenis[1];
                    $search_keyword = explode("=", $arrSearch[2]); // tgldari
                    $arr_search["search"]["tgldari"] = $search_keyword[1];
                    $search_keyword = explode("=", $arrSearch[3]); // search keyword
                    $arr_search["search"]["tglsampai"] = $search_keyword[1];
                }
            }
            
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/pelunasan_hutang/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/pelunasan_hutang/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->pelunasan_hutangmodel->num_pelunasan_hutang_row($arr_search["search"]);
            $data['data'] = $this->pelunasan_hutangmodel->getPelunasanHutangList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('transaksi/pelunasan_hutang/pelunasan_hutang_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'pelunasan_hutang', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_jenis=".$this->input->post('Jenis');
		$search_value .= "&search_tgldari=".$mylib->ubah_tanggal($this->input->post('tgldari'));
		$search_value .= "&search_tglsampai=".$mylib->ubah_tanggal($this->input->post('tglsampai'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pelunasan_hutang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pelunasan_hutang/index/' . $query_id . '');
    }

    function add_new() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['supplier'] = $this->pelunasan_hutangmodel->getSupplier();
            $data['matauang'] = $this->pelunasan_hutangmodel->getMataUang();
            $data['kasbank'] = $this->pelunasan_hutangmodel->getKasBank();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pelunasan_hutang/add_pelunasan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $data['data'] = $this->pelunasan_hutangmodel->getDataHeader($notransaksi);
            $data['RekSupplier'] = $this->pelunasan_hutangmodel->getNoRekSupplier($data['data']['KdSupplier']);
            $seri = substr($notransaksi, 0,2);
            if($seri=="PM"){
				$data['row'] = $this->pelunasan_hutangmodel->getDataDetail2($notransaksi);	
			}else{
            	$data['row'] = $this->pelunasan_hutangmodel->getDataDetail($notransaksi);
            }
            $data['dataums'] = $this->pelunasan_hutangmodel->getDataOutStandingUM($notransaksi);
            $data['track'] = $mylib->print_track();
            $data['flag']='view';
            $data['kasbank'] = $this->pelunasan_hutangmodel->getKasBank();
            
            $this->load->view('transaksi/pelunasan_hutang/view_pelunasan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function edit($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $header = $this->pelunasan_hutangmodel->getDataHeader($notransaksi);
            $data['RekSupplier'] = $this->pelunasan_hutangmodel->getNoRekSupplier($header['KdSupplier']);
            $KdSupplier = $header['KdSupplier'];
            $data['data'] = $header;
            $data['flag']='edit';
            $seri = substr($notransaksi, 0,2);
            if($seri=="PM"){
				$data['row'] = $this->pelunasan_hutangmodel->getDataDetail2($notransaksi);	
			}else{
            	$data['row'] = $this->pelunasan_hutangmodel->getDataDetail($notransaksi);
            }
            $data['dataums'] = $this->pelunasan_hutangmodel->getOutStandingUM($KdSupplier);
            $data['track'] = $mylib->print_track();
            $data['kasbank'] = $this->pelunasan_hutangmodel->getKasBank();
            $data['rekeningpph'] = $this->pelunasan_hutangmodel->getRekeningPPH();
            $this->load->view('transaksi/pelunasan_hutang/view_pelunasan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
	function getDetail(){
		$mylib = new globallib();
		$data = $this->input->post('data');
		
		$KdSupplier = $data[0];
		$MataUang = $data[1];
		$type = $data[2];
		$tgl = $mylib->ubah_tanggal($data[3]);
		
		//akan ada filter jika type 1 / Hutang Datang atau 2 / Hutang Lain lain
		if($type=="1"){
		$row = $this->pelunasan_hutangmodel->getDetail($KdSupplier,$MataUang, $tgl);
		}else{
		$row = $this->pelunasan_hutangmodel->getDetail2($KdSupplier,$MataUang, $tgl);	
		}
		$data['num'] = count($row);
		$data['row'] = $row;
		 	
		$this->load->view('transaksi/pelunasan_hutang/pelunasan_hutang_detail', $data);	
	}

   
    function save_data() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $norencanabayar = $this->input->post('notransaksi');
        $tgl = $mylib->ubah_tanggal($this->input->post('tgldokumen'));
        $kdkasbank = $this->input->post('KdKasBank');
        $kd_uang = $this->input->post('kd_uang');
        $kurs = $this->input->post('kurs');
        $nobukti = $this->input->post('NoBukti');
        $kdSupplier = $this->input->post('KdSupplier');
        $keterangan = addslashes($this->input->post('keterangan'));
        $v_type = $this->input->post('v_type');
        $biayaadmin = $this->input->post('vbiayaadmin');
        $pembulatan = $this->input->post('vpembulatan');
        $pph = $this->input->post('vpph');
        $flag = $this->input->post('flag');
        $noreksupplier = $this->input->post('NoRekSupplier');
        $kdrekeningpph = $this->input->post('vkdrekeningpph');
        
        
        // detail
        $nofaktur = $this->input->post('NoFaktur');
        $sisa = $this->input->post('Sisa');
        $bayar = $this->input->post('Bayar');
        
        $noum = $this->input->post('NoUM');
        $nilaium = $this->input->post('NilaiUM');
        
        $user = $this->session->userdata('username');
        try {
	        $this->db->trans_begin();
	        if(substr($norencanabayar,0,2)=='RH'){
	        	$notransaksi = $this->pelunasan_hutangmodel->getNoTransaksi($tgl);
	        }else{
				$notransaksi = $this->pelunasan_hutangmodel->getNoTransaksi2($tgl);	
			}
			$nopv = $this->pelunasan_hutangmodel->getNoPV($kdkasbank,$tgl);	
				
	        $this->pelunasan_hutangmodel->UpdateHeader($notransaksi, $tgl, $kdkasbank, $kd_uang, $kurs, $nobukti, $kdSupplier, $keterangan, $user, $biayaadmin,$pembulatan, $pph, $norencanabayar, $nopv, $noreksupplier);
			
			$total = $this->pelunasan_hutangmodel->insertDetail($notransaksi,$nofaktur,$sisa,$bayar,$norencanabayar);
			$this->pelunasan_hutangmodel->insertDetailUM($notransaksi,$noum,$nilaium,$norencanabayar);

	        $this->pelunasan_hutangmodel->insertHutang($tgl,$notransaksi, $nofaktur, $kdSupplier,$kd_uang,$kurs, $bayar);	
			
			if($tgl>='2017-03-01'){
				$this->pelunasan_hutangmodel->insertPVHeader($nopv,$tgl,$kdkasbank,$kdSupplier,$keterangan,$total,$nobukti,$user,$biayaadmin,$pembulatan,$pph,$noreksupplier);
				$this->pelunasan_hutangmodel->insertPVDetail($nopv,$tgl,$kdSupplier,$bayar,$nobukti, $nofaktur, $user, $biayaadmin,$pembulatan, $pph,$norencanabayar,$kdrekeningpph);	
			}	
			$this->db->trans_commit();
		}catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
			$this->db->trans_rollback();
		}
		$link = "<script>
					window.open('".base_url()."index.php/keuangan/payment/cetak/".$nopv ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
					window.location.href = '".base_url()."index.php/transaksi/pelunasan_hutang"."';
				</script>";
		echo $link;
        //redirect('/transaksi/pelunasan_hutang');
    }
    
    function ajax_supplier(){
		$var = $this->input->post('id');
		$supplier = $this->pelunasan_hutangmodel->getSupplier($var);
		 
	 	echo "<option value=''> -- Pilih Supplier --</option>";
     	foreach ($supplier as $var) {
	 		echo "<option value=".$var['KdSupplier'].">".$var['Nama']."</option>";
	    }
    }
    
    function test(){
    	$tgl='2017-10-05';
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
    	$tahun = $tahun-2000;
    	$blnthn = $bulan.'-'.$tahun;
    	$sql = "SELECT NoTransaksi FROM pelunasan_hutang_header WHERE NoTransaksi LIKE 'PH%$blnthn' ORDER BY NoTransaksi DESC";
		
		$result =  $this->pelunasan_hutangmodel->getArrayResult($sql);
		print_r($result);
		if(count($result)==0){
			$notransaksi = 'PH00001-' . $blnthn;
		}else{
			$nourut = substr($result[0]['NoTransaksi'],2,5);
			echo 'a : '.$nourut ."<br>";
			$nourut = 100001 + ($nourut*1);
			echo 'b : '.$nourut ."<br>";
			$nourut = substr($nourut,-5);
			echo 'c : '.$nourut ."<br>";
			$notransaksi = 'PH' . $nourut .'-'.$blnthn;
		}
	}
}

?>