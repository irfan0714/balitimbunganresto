<?php
// Febri
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Reservasi_konfirmasi extends authcontroller {
	
    function __construct()
    {
        parent::__construct();    
        //error_reporting(0);                              
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/reservasi_konfirmasi_model', 'konfirmasi');
       	$this->load->model('master/tour_leadermodel');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$id = $this->uri->segment(4);	
			$user = $this->session->userdata('username');
			
			$data["search_keyword"] = "";
			$resSearch = "";
			$arr_search["search"]= array();
			$id_search = "";
			if($id*1>0)
			{	
				$resSearch = $this->globalmodel->getSearch($id,"reservasi_konfirmasi",$user);	
				$arrSearch = explode("&",$resSearch->query_string);
				
				$id_search = $resSearch->id;
				
				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					
					$data["search_keyword"] = $search_keyword[1];
				}
			}
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if($id_search)
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/reservasi_konfirmasi/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/reservasi_konfirmasi/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}
            
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            
            $thnbln = $data['tahun'] . $data['bulan'];
            
            $config['total_rows'] = $this->konfirmasi->num_tabel_row($arr_search["search"]);
            $data['data'] = $this->konfirmasi->getTabelList($config['per_page'], $page, $arr_search["search"]);
            
            $data['track'] = $mylib->print_track();
            
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
            $this->load->view('transaksi/reservasi_konfirmasi/tabellist', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }     

    function search()
    {
        $mylib = new globallib();
        
		$user = $this->session->userdata('username');
		
		// hapus dulu yah
		$this->db->delete('ci_query', array('module' => 'reservasi_konfirmasi','AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "reservasi_konfirmasi",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/transaksi/reservasi_konfirmasi/index/'.$query_id.'');
		
	} 
	
    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg'] = "";
            
            $user = $this->session->userdata('username');
            
            $data['mreservasi']	= $this->globalmodel->getQuery("
				  trans_reservasi.NoDokumen
				FROM
				  trans_reservasi 
				WHERE 1 
				  AND trans_reservasi.status != '2'
				  AND trans_reservasi.status_konfirmasi = '0'
				  AND (SUBSTR(trans_reservasi.NoDokumen, 1,3)='SGV' OR SUBSTR(trans_reservasi.NoDokumen, 1,3)='BFM')
				  AND trans_reservasi.NoDokumen NOT IN (SELECT NoReservasi FROM trans_reservasi_konfirmasi)
				ORDER BY trans_reservasi.TglDokumen DESC
			");
			
			$data['mtourleader']	= $this->globalmodel->getQuery("
				  tourleader.KdTourLeader,
				  tourleader.Nama 
				FROM
				  tourleader 
				WHERE 1 
				ORDER BY tourleader.Nama ASC
			");
			
            $data['track'] 		= $mylib->print_track();
            $data['gsa'] = $this->konfirmasi->getGSA();
            
	    	$this->load->view('transaksi/reservasi_konfirmasi/form_add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function edit_form($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id 				= $this->uri->segment(4);
            $data['bulan'] 		= $this->session->userdata('bulanaktif');
            $data['tahun'] 		= $this->session->userdata('tahunaktif');
			$user 				= $this->session->userdata('username');
            $data['edit']			= TRUE;
			$data['detail_sticker'] = $this->konfirmasi->getDetailSticker($id);
            $data['data'] 		= $this->konfirmasi->getDetailKonfirmasi($id);
	        $data['datatravel']	= $this->konfirmasi->getDetailTourTravel($data['data']->KdTravel);
	        
	        $data['mreservasi']	= $this->globalmodel->getQuery("
				  trans_reservasi.NoDokumen
				FROM
				  trans_reservasi 
				WHERE 1 
				  AND trans_reservasi.status != '2'
				  AND trans_reservasi.status_konfirmasi = '0'
				  AND trans_reservasi.NoDokumen NOT IN (SELECT NoReservasi FROM trans_reservasi_konfirmasi WHERE 1 AND NoReservasi !='".$data['data']->NoReservasi."')
				ORDER BY trans_reservasi.TglDokumen DESC
			");
			
			$data['mtourleader']	= $this->globalmodel->getQuery("
				  tourleader.KdTourLeader,
				  tourleader.Nama 
				FROM
				  tourleader 
				WHERE 1 
				ORDER BY tourleader.Nama ASC
			");
			
            $data['track'] 		= $mylib->print_track();
            $data['gsa'] = $this->konfirmasi->getGSA();
            $this->load->view('transaksi/reservasi_konfirmasi/form_edit', $data);

        } 
        else 
        {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
               
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_jam = $this->input->post('v_jam');
        $v_noreservasi = $this->input->post('v_noreservasi');
        $v_tourtravel = $this->input->post('v_tourtravel');
        $v_tourleader = $this->input->post('v_tourleader');
        $chk_tourleader = $this->input->post('chk_tourleader');
        $v_tourleader_new = $mylib->save_char($this->input->post('v_tourleader_new'));
		$v_tl = $mylib->save_int($this->input->post('v_tl'));
        $v_tg = $mylib->save_int($this->input->post('v_tg'));
        $v_dewasa = $mylib->save_int($this->input->post('v_dewasa'));
        $v_anak = $mylib->save_int($this->input->post('v_anak'));
        $v_batita = $mylib->save_int($this->input->post('v_batita'));
        $v_nostiker = $mylib->save_char($this->input->post('v_nostiker'));
        $v_gsa = $mylib->save_char($this->input->post('v_gsa'));
        $v_novoucher = $mylib->save_char($this->input->post('v_novoucher'));
        $v_keterangan = $mylib->save_char($this->input->post('v_keterangan'));
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');
		
		$nosticker1 = $this->input->post('nostiker');
		
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
		
		if($flag=="add")
        {
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_reservasi_konfirmasi","NoDokumen", "KK", $data['bulan'], $data['tahun']);
            
       		if($chk_tourleader=="on")
       		{
       			$nama   = strtoupper(addslashes(trim($v_tourleader_new)));
       			$idnama = preg_replace("/[^A-Z0-9._-]/i", "_", substr($nama,0,2));
        		$kdtourleader     = $idnama.substr("00000".$this->tour_leadermodel->getidcounter($idnama)->Counter,-4);
        		
        		$this->insertTourLeader($kdtourleader, $nama);
        		
        		$this->insertData($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_jam, $v_noreservasi, $v_tourtravel, $kdtourleader, $v_tl, $v_tg, $v_dewasa, $v_anak, $v_batita, $v_nostiker, $v_gsa, $v_novoucher, $v_keterangan, $flag, $base_url, $user);
			}
			else
			{
				$this->insertData($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_jam, $v_noreservasi, $v_tourtravel, $v_tourleader, $v_tl, $v_tg, $v_dewasa, $v_anak, $v_batita, $v_nostiker, $v_gsa, $v_novoucher, $v_keterangan, $flag, $base_url, $user);				
			}
			
			//detail sticker
			for($x = 0; $x < count($nosticker1); $x++){
				$nosticker      = $nosticker1[$x];
				if($nosticker != ""){
					$this->db->insert('trans_reservasi_konfirmasi_sticker',array('NoDokumen'=>$v_no_dokumen,'NoSticker'=>$nosticker));
				}				
			}
			
			//update no_voucher_travel
			//cari voucher internal di voucher beo
			$voucher_internal = $this->globalmodel->getQuery("
				   * FROM `voucher_beo` a WHERE a.`BEO`='$v_noreservasi';
			");
			if(!empty($voucher_internal)){
				
			   $this->db->update('voucher',array('novoucher_travel'=>$v_novoucher),array('novoucher'=>$voucher_internal[0]['no_voucher']));	
			  			   
			}
			
			//update trans_reservasi status_konfirmasi = 1
			$this->updateReservasi($v_noreservasi);
			
			$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       	} 
        else if($flag=="edit")
        {
        	$v_no_dokumen 	= $this->input->post('v_no_dokumen');
        	$v_status 		= $this->input->post('v_status');
        	
        	if($chk_tourleader=="on")
       		{
       			$nama   = strtoupper(addslashes(trim($v_tourleader_new)));
       			$idnama = preg_replace("/[^A-Z0-9._-]/i", "_", substr($nama,0,2));
        		$kdtourleader     = $idnama.substr("00000".$this->tour_leadermodel->getidcounter($idnama)->Counter,-4);
        		
        		$this->insertTourLeader($kdtourleader, $nama);
        		
        		$this->updateData($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_jam, $v_noreservasi, $v_tourtravel, $kdtourleader, $v_tl, $v_tg, $v_dewasa, $v_anak, $v_batita, $v_nostiker, $v_gsa, $v_novoucher, $v_keterangan, $v_status, $flag, $base_url, $user);
			}
			else
			{
				$this->updateData($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_jam, $v_noreservasi, $v_tourtravel, $v_tourleader, $v_tl, $v_tg, $v_dewasa, $v_anak, $v_batita, $v_nostiker, $v_gsa, $v_novoucher, $v_keterangan, $v_status, $flag, $base_url, $user);
			}
			
			//detail sticker
			for($x = 0; $x < count($nosticker1); $x++){
				$nosticker      = $nosticker1[$x];
				if($nosticker != ""){
					$this->db->insert('trans_reservasi_konfirmasi_sticker',array('NoDokumen'=>$v_no_dokumen,'NoSticker'=>$nosticker));
				}				
			}
			
        	if($v_status==1)
        	{
        		$nilai = $this->konfirmasi->getNilai($v_noreservasi);
				$this->updateReservasi($v_noreservasi);	
				//$this->insertpiutang($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_tourtravel,'I', $mylib->ubah_tanggal($v_tgl_dokumen), $nilai);
			}
			
			
			//update no_voucher_travel
			//cari voucher internal di voucher beo
			$voucher_internal = $this->globalmodel->getQuery("
				   * FROM `voucher_beo` a WHERE a.`BEO`='$v_noreservasi';
			");
			if(!empty($voucher_internal)){
				
			   $this->db->update('voucher',array('novoucher_travel'=>$v_novoucher),array('novoucher'=>$voucher_internal[0]['no_voucher']));	
			}
        	
        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }
        
       redirect('/transaksi/reservasi_konfirmasi/edit_form/'.$v_no_dokumen.'');
    }

    function insertData($no_dokumen, $tgl_dokumen, $jam, $noreservasi, $tourtravel, $tourleader, $tl, $tg,  $dewasa, $anak, $batita, $nostiker, $gsa, $novoucher, $keterangan, $flag, $base_url, $user)
    {   
        $this->konfirmasi->locktables('trans_reservasi_konfirmasi');
        
        $data = array(
            'NoDokumen' => $no_dokumen,
            'TglKonfirmasi' => $tgl_dokumen,
            'JamKonfirmasi' => $jam,
            'NoReservasi' => $noreservasi,
            'KdTravel' => $tourtravel,
            'TourLeader' => $tourleader,
            'JumlahRombongan' => $dewasa+$anak+$batita,
			'JmlTourLeader'=>$tl,
			'JmlTourGuide'=>$tg,
            'Dewasa' => $dewasa,
            'Anak' => $anak,
            'Batita' => $batita,
            'NoVoucher' => $novoucher,
            'NoStiker' => $nostiker,
            'gsa' => $gsa,
            'keterangan' => $keterangan,
            'status' => "1",
            'AddDate' => date("Y-m-d H:i:s"),
            'AddUser' => $user,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
        );
        
        $this->db->insert('trans_reservasi_konfirmasi', $data);
        
        $this->konfirmasi->unlocktables();
        
    }

	function updateData($no_dokumen, $tgl_dokumen, $jam, $noreservasi, $tourtravel, $tourleader, $tl, $tg, $dewasa, $anak, $batita, $nostiker, $gsa, $novoucher, $keterangan, $status, $flag, $base_url, $user)
    {
        $this->konfirmasi->locktables('trans_reservasi_konfirmasi');

        $data = array(
            'TglKonfirmasi' => $tgl_dokumen,
            'JamKonfirmasi' => $jam,
            'NoReservasi' => $noreservasi,
            'KdTravel' => $tourtravel,
            'TourLeader' => $tourleader,
            'JumlahRombongan' => $dewasa+$anak+$batita,
			'JmlTourLeader'=>$tl,
			'JmlTourGuide'=>$tg,
            'Dewasa' => $dewasa,
            'Anak' => $anak,
            'Batita' => $batita,
            'NoVoucher' => $novoucher,
            'NoStiker' => $nostiker,
            'gsa' => $gsa,
            'keterangan' => $keterangan,
            'status' => $status,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
        );	
        
        $this->db->update('trans_reservasi_konfirmasi', $data, array('NoDokumen' => $no_dokumen));
        
        //cek jika ada no voucher
			if($novoucher!=""){
				$namatravels = $this->konfirmasi->getNamaTravel($tourtravel);
				//insert ke table voucher
				$this->db->insert("voucher",
								array(
								'novoucher'=>$novoucher,
								'nominal'=>0,
								'expDate'=>$mylib->ubah_tanggal($tgl_dokumen),
								'addDate'=>date('Y-m-d H:i:s'),
								'userDate'=>$user,
								'status'=>0,
								'notrans'=>'',
								'keterangan'=>'Travel '.$namatravels->Nama,
								'terpakai'=>0,
								'jenis'=>'5',
								'rupdisc'=>'D',
								'ketentuan'=>'')
								);
			}
        
        $this->konfirmasi->unlocktables();
    }
    
    function updateReservasi($no_dokumen)
    {
    	$this->konfirmasi->locktables('trans_reservasi');

        $data = array(
            'status_konfirmasi' => "1"
        );	
        
        $this->db->update('trans_reservasi', $data, array('NoDokumen' => $no_dokumen));
        
        $this->konfirmasi->unlocktables();
	}
	
	function insertpiutang($nodokumen, $tgl, $kdcustomer,$tipe, $jt, $nilai){
		
		list($tahun, $bulan, $tgl1) = explode('-', $tgl);
		$this->konfirmasi->locktables('piutang');
		
		$data = array(
					'NoDokumen'=> $nodokumen,
					'NoFaktur' => $nodokumen,
					'TglTransaksi' => $tgl,
					'KdCustomer' => $kdcustomer,
					'TipeTransaksi' => 'I',
					'JatuhTempo' => $jt,
					'Nilai' => $nilai,
					'Sisa' => $nilai
					);
					
		$this->db->insert('piutang', $data);
		$this->konfirmasi->unlocktables();
		
		$this->konfirmasi->locktables('mutasi_piutang');
		$datamutasi = array(
					'Tahun' => $tahun,
					'Bulan' => $bulan,
					'Tanggal' => $tgl,
					'NoDokumen'=> $nodokumen,
					'KdCustomer' => $kdcustomer,
					'Jumlah' => $nilai,	
					'TipeTransaksi' => 'I'
					);
		$this->db->insert('mutasi_piutang', $datamutasi);
		$this->konfirmasi->unlocktables();
	}
    
    function insertTourLeader($kdtourleader, $nama)
    {   
        $this->konfirmasi->locktables('tourleader');
        
        $data = array(
            'KdTourLeader' => $kdtourleader,
            'Nama' => $nama,
            'Aktif' => "A",
            'AddDate' => date("Y-m-d"),
            'EditDate' => date("Y-m-d"),
        );
        
        $this->db->insert('tourleader', $data);
        
        $this->konfirmasi->unlocktables();
        
    }
    
	function getAjax()
	{
		$mylib	= new globallib();
		$ajax	= $this->input->post('ajax');
		 
		if($ajax=="search_keyword_noreservasi")
		{
			$v_keyword 		= $mylib->save_char($this->input->post('v_keyword'));	
						
			unset($arr_keyword);
	        $arr_keyword[0] = "trans_reservasi.NoDokumen";
			
			$search_keyword = $mylib->search_keyword($v_keyword, $arr_keyword);
			$where_keyword = $search_keyword;
				
			$data['mreservasi']	= $this->globalmodel->getQuery("
				  trans_reservasi.NoDokumen
				FROM
				  trans_reservasi 
				WHERE 
				  1
				  ".$where_keyword." 
				  AND trans_reservasi.status != '2'
				  AND trans_reservasi.status_konfirmasi = '0'
				ORDER BY trans_reservasi.TglDokumen DESC
			");
			
			?>
	    	<select name="v_noreservasi" id="v_noreservasi" class="form-control-new" style="width: 270px;" onchange="CallAjax('ajax_noreservasi', this.value)">
	    		<option value="">Pilih</option>
	    		<?php
	    		foreach($data['mreservasi'] as $val)
	    		{
					?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
				}
	    		?>
	    	</select>
	    	<?php
		}
		else if($ajax=="ajax_noreservasi")
		{
			$v_nodok	= $mylib->save_char($this->input->post('v_nodok'));	
	        $row		= $this->konfirmasi->getReservasi($v_nodok);
	        
	        if($v_nodok)
	        {
	        	$rowtravel	= $this->konfirmasi->getDetailTourTravel($row->KdTravel);
	        	
	        	echo $rowtravel->KdTravel;
	        	echo "||";
	        	echo $rowtravel->Nama;
	        	echo "||";
	        	echo $rowtravel->Contact;
	        	echo "||";
	        	echo $rowtravel->Phone;
	        	echo "||";
	        	echo $rowtravel->Email;
	        	echo "||";
	        	echo $rowtravel->Alamat;        	
	        	echo "||";
	        	echo $row->adultParticipants;        	
	        	echo "||";
	        	echo $row->childParticipants;        	
	        	echo "||";
	        	echo $row->kidsParticipants;        	
	        }
		}	 
		else if($ajax=="search_keyword_tourleader")
		{
			$v_keyword 		= $mylib->save_char($this->input->post('v_keyword'));	
						
			unset($arr_keyword);
	        $arr_keyword[0] = "tourleader.KdTourLeader";
	        $arr_keyword[1] = "tourleader.Nama";
			
			$search_keyword = $mylib->search_keyword($v_keyword, $arr_keyword);
			$where_keyword = $search_keyword;
				
			$data['mtourleader']	= $this->globalmodel->getQuery("
				  tourleader.KdTourLeader,
				  tourleader.Nama 
				FROM
				  tourleader 
				WHERE 
				  1 
				  ".$where_keyword." 
				ORDER BY tourleader.Nama ASC,
				  tourleader.KdTourLeader ASC
			");
			
			?>
	    	<select name="v_tourleader" id="v_tourleader" class="form-control-new" style="width: 255px;">
	    		<option value="">Pilih</option>
	    		<?php
	    		foreach($data['mtourleader'] as $val)
	    		{
					?><option value="<?php echo $val["KdTourLeader"]; ?>"><?php echo $val["Nama"]; ?></option><?php
				}
	    		?>
	    	</select>
	    	<?php
		}	
		
		exit();
	}
	
	function delete_detail_sticker() 
    {
        $id = $this->uri->segment(4);
        $nodok= $this->uri->segment(5);
        $nostiker = $this->uri->segment(6);
		
		$this->db->delete('trans_reservasi_konfirmasi_sticker',array('id'=>$id));

        redirect('/transaksi/reservasi_konfirmasi/edit_form/' . $nodok . '');
    }

	function viewPrintPdf()
	{
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		$data["judul"]		= "KONFIRMASI KEDATANGAN";
		$data["aksi"]		= "view";
		
		$data['data'] 		= $this->konfirmasi->getDetailKonfirmasi($nodok);
	    $data['datareservasi']	= $this->konfirmasi->getReservasi($data['data']->NoReservasi);
	    $data['detailreservasi']= $this->konfirmasi->getDetailReservasi($data['data']->NoReservasi);
	    $data['datatravel']	= $this->konfirmasi->getDetailTourTravel($data['data']->KdTravel);
		
        $this->load->view('transaksi/cetak_transaksi/cetak_reservasi_konfirmasi_pdf', $data);
	}

	function doPrintPdf()
	{
		$nodok 					= $this->uri->segment(4);
		$data["judul"]			= "KONFIRMASI KEDATANGAN";
		$data["aksi"]			= "print";
		$data['data'] 			= $this->konfirmasi->getDetailKonfirmasi($nodok);
	    $data['datareservasi']	= $this->konfirmasi->getReservasi($data['data']->NoReservasi);
	    $data['detailreservasi']= $this->konfirmasi->getDetailReservasi($data['data']->NoReservasi);
	    $data['datatravel']		= $this->konfirmasi->getDetailTourTravel($data['data']->KdTravel);
		
        $html = $this->load->view('transaksi/cetak_transaksi/cetak_reservasi_konfirmasi_pdf', $data, true);
		 
		//this the the PDF filename that user will get to download
		$name_file = preg_replace("/[^A-Z0-9._-]/i", "_", $data["judul"]);
		$pdfFilePath = $name_file."_".$data['data']->NoReservasi.".pdf";
		 
		//load mPDF library
		$this->load->library('m_pdf');
		//actually, you can pass mPDF parameter on this load() function
		$pdf = $this->m_pdf->load();
		//generate the PDF!
		$pdf->WriteHTML($html);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");
	}
	
	function cek_stiker() 
    {
    			  $mylib = new globallib(); 
    			  
    			    $nostiker= $this->uri->segment(4);
			        $tgl = $mylib->ubah_tanggal($this->uri->segment(5));
			        $sgv= $this->uri->segment(6);
			        $beo= $this->uri->segment(7);
			        $sm= $this->uri->segment(8);
			        $thn = $this->uri->segment(9);
			        $rmw = $this->uri->segment(10);
			        $cont = $this->uri->segment(11);
			        
			        $noreservasi = $sgv."/".$beo."/".$sm."/".$thn."/".$rmw."/".$cont;
    			  
    			    $cek_stiker = $this->konfirmasi->cekStiker($nostiker,$tgl,$noreservasi);
    			    if(count($cek_stiker)>0){
						echo json_encode(array("status" => TRUE));
					}else{
						echo json_encode(array("status" => FALSE));
					}
    	
    	
    }
}

?>