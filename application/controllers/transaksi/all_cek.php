<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class All_cek extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/all_cek_model');
    }
    
	
    function index() 
    {
     
    } 
    
    function cek_tutup_bulan(){
    	$mylib = new globallib(); 	
    	$tglinput = $mylib->ubah_tanggal($this->input->post('tgl'));
    	$jenis = $this->input->post('jenis');
	 	$cekperiode = $mylib->CekPeriode($jenis, $tglinput);
	 	
	 	echo $cekperiode['Valid'];
	}
	
    function cek_tgl_approve_so(){
     $mylib = new globallib(); 	
	 $tgl = $mylib->ubah_tanggal($this->input->post('tgl'));
	 $gudang = $this->input->post('gdg');
     $query = $this->all_cek_model->getCek($gudang,$tgl);
     //echo $query->tglapprove." - ".$tgl;die;
     if($query->tglapprove >= $tgl){
	 	$hasil="0";
	 }else{
	 	$hasil="1";
	 }
	 echo $hasil;
	}
	
	function cek_tgl_approve_so_type2(){
     $mylib = new globallib(); 	
	 $tgl = $mylib->ubah_tanggal($this->input->post('tgl'));
	 $gudang_from = $this->input->post('gdg1');
	 $gudang_to = $this->input->post('gdg2');
     $query = $this->all_cek_model->getCekType2($gudang_from,$gudang_to,$tgl);
     
      if($query->tglapprove >= $tgl){
	 	$hasil="0";
	 }else{
	 	$hasil="1";
	 }
	 echo $hasil;
	}
    
    function cek_tgl_approve_so_native(){
     $mylib = new globallib(); 	
	 $tgl = $mylib->ubah_tanggal($this->input->post('tgl'));
	 $tanggal = explode("/",$tgl);
	
	 $gudang = $this->input->post('gdg');
     $query = $this->all_cek_model->getCek($gudang,$tanggal[0]);
     
    if($query->tglapprove >= $tanggal[0]){
	 	$hasil="0";
	 }else{
	 	$hasil="1";
	 }
	 echo $hasil;
	}
	
	function test(){
    	$mylib = new globallib(); 	
    	$tglinput = $mylib->ubah_tanggal('05-12-2017');
    	$jenis = 'Stock';
	 	$cekperiode = $mylib->CekPeriode($jenis, $tglinput);
	 	
	 	print_r($cekperiode);
	}
  		
}

?>