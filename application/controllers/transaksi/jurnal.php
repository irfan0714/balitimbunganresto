<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class jurnal extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/jurnalmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '15';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/jurnal/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/jurnal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/jurnal/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$thnbln = $data['tahun'].$data['bulan'];
			$data['header']		 		= array("No Dokumen","Tanggal","Jenis","No Transaksi","Jumlah Debit","Jumlah Kredit","Keterangan","Username");
	        $config['total_rows']		= $this->jurnalmodel->num_jurnal_row($id,$with,$thnbln);		
	        $data['data']	= $this->jurnalmodel->getJurnalList($config['per_page'],$page,$id,$with,$thnbln);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/jurnal/jurnallist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new($pesan){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->jurnalmodel->getDate();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$data['aplikasi'] = $aplikasi;
			$tgl = '01'.'-'.$data['bulan'].'-'.$data['tahun'];
			$data['tanggal'] = $aplikasi->TglTrans;
			if(($data['tahun']==substr($data['tanggal'],-4))&&($data['bulan']==substr($data['tanggal'],3,2)))
			     $data['tanggal'] = $aplikasi->TglTrans;
		    else
			     //$data['tanggal'] = $tgl;
			     $data['tanggal'] = $aplikasi->TglTrans;
			$data['mjenis'] = array("1"=>"Umum","2"=>"Adjustment");
			$data['mdept'] = $this->jurnalmodel->getDept();
			$data['mproject'] = $this->jurnalmodel->getProject();
			$data['mcostcenter'] = $this->jurnalmodel->getCostCenter();
			$data['sbdivisi'] = $this->jurnalmodel->getSubdivisi();
			$data['pesan'] = $pesan;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/jurnal/add_jurnal',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "jurnal.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$mylib2 = new globallib();
		$id = $this->uri->segment(4);
		$header	 = $this->jurnalmodel->getHeader($id);
		$data['header']	 = $header;
		$detail	 = $this->jurnalmodel->getDetailForPrint($id);
//		 print_r($detail);

        $data['judul1'] = array("No Jurnal","Project","Keterangan");
		$data['niljudul1'] = array($header->NoDokumen,$header->KdProject." - ".stripslashes($header->NamaProject),stripslashes($header->Keterangan));
		$data['judul2'] = array("Tanggal","Cost Center");
		$data['niljudul2'] = array($header->Tanggal,$header->KdCostCenter." - ".stripslashes($header->NamaCostCenter));
		$data['judullap'] = "Transaksi Jurnal";
		$data['url'] = "jurnal/printThis/".$id;
		$data['colspan_line'] = 4;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan","kanan");
		$data['judul_detail'] = array("Rekening","Nama","Keterangan","Debit","Kredit");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0,0,0);
		for($m=0;$m<count($detail);$m++)
		{
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['KdRekening']);
			$list_detail[] = stripslashes($detail[$m]['NamaRekening']);
			$list_detail[] = stripslashes($detail[$m]['Keterangan']);
			$list_detail[] = $mylib2->ubah_format($detail[$m]['Debit']);
			$list_detail[] = $mylib2->ubah_format($detail[$m]['Kredit']);
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array("Jumlah Debit","Jumlah Kredit");
		$data['footer2']  = array($mylib2->ubah_format($header->JumlahDebit),$mylib2->ubah_format($header->JumlahKredit));
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		$data['string1'] = "Dibuat Oleh";
		$data['string2'] = "Menyetujui";
		$data['string3'] = "(____________________)";
		$data['string4'] = "(____________________)";
		return $data;
	}

    function delete_jurnal(){
     	$id = $this->input->post('kode');
		$ret = "";		
		
		$this->jurnalmodel->locktables('trans_jurnal_header,trans_jurnal_detail');
		$this->db->update('trans_jurnal_header', array('Status'=>'B'),array('NoDokumen' => $id)); 
		$this->db->update('trans_jurnal_detail', array('Status'=>'B'),array('NoDokumen' => $id)); 
		$this->jurnalmodel->unlocktables();

		echo $ret;   
	}

    function edit_jurnal($id){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
 			$id = $this->uri->segment(4);
			$aplikasi = $this->jurnalmodel->getDate();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$data['aplikasi'] = $aplikasi;
			$data['header']	= $this->jurnalmodel->getHeader($id);
	    	$data['detail']	= $this->jurnalmodel->getDetail($id);
			$data['mjenis'] = array("1"=>"Umum","2"=>"Adjustment");
			$data['mdept'] = $this->jurnalmodel->getDept();
			$data['mproject'] = $this->jurnalmodel->getProject();
			$data['mcostcenter'] = $this->jurnalmodel->getCostCenter();
			$data['sbdivisi'] = $this->jurnalmodel->getSubdivisi();
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/jurnal/edit_jurnal', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	
	function getkdrekening()
	{
		$kode = $this->input->post('kdrekening');
		$valreken = $this->jurnalmodel->findrekening($kode);
		if(count($valreken)!=0)
		{
			$kode = $valreken->kdrekening."*-*".$valreken->namarekening;
		}
		else
		{
			$kode = "";
		}
		echo $kode;
	}

	function save_new_jurnal(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$dept = $this->input->post('dept');
		$project = $this->input->post('project');
		$costcenter = $this->input->post('costcenter');
		$notransaksi = strtoupper(addslashes($this->input->post('notrans')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = $this->input->post('kdrekening');
		$jumlahdebit = $this->input->post('jumlahdebit');
		$jumlahkredit = $this->input->post('jumlahkredit');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$counter = $this->input->post('urutan');
		$itemjenis = $this->input->post('itemjenis');
		$nobukti = $this->input->post('nobukti');
		$savenobukti = $this->input->post('savenobukti');
		$subdivisi = $this->input->post('subdivisi');
		$debit = $this->input->post('debit');
		$kredit = $this->input->post('kredit');
		if($no=="")
		{
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$dept,$project,$costcenter,$ket,$user,$jumlahdebit,$jumlahkredit,$jenis,$notransaksi);
		}
		else
		{
			$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$dept,$project,$costcenter,$ket,$user,$jumlahdebit,$jumlahkredit,$jenis,$notransaksi);
		}
		for($x=0;$x<count($kdrekening);$x++)
		{
			$kdrekening1 = strtoupper(addslashes(trim($kdrekening[$x])));
			$subdivisi1 = $subdivisi[$x];
			$debit1 = $debit[$x];
			$kredit1 = $kredit[$x];
			$savekdrekening1 = $savekdrekening[$x];
			$keterangan1 = $keterangan[$x];
			$counter1 = $counter[$x];
			if($kdrekening1!=""){
				$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter1,$kdrekening1,$subdivisi1,$debit1,$kredit1,$keterangan1,$user,$savekdrekening1);
			}
		}
		redirect('/transaksi/jurnal/');
	}
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$dept = $this->input->post('dept');
		$project = $this->input->post('project');
		$costcenter = $this->input->post('costcenter');
		$notransaksi = strtoupper(addslashes($this->input->post('notrans')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = strtoupper(addslashes($this->input->post('kdrekening')));
		$jumlahdebit = $this->input->post('jumlahdebit');
		$jumlahkredit = $this->input->post('jumlahkredit');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$counter = $this->input->post('urutan');
		$subdivisi = $this->input->post('subdivisi');
		$debit = $this->input->post('debit');
		$kredit = $this->input->post('kredit');
		if($no=="")
		{
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$dept,$project,$costcenter,$ket,$user,$jumlahdebit,$jumlahkredit,$jenis,$notransaksi);
		}
		else
		{
			$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$dept,$project,$costcenter,$ket,$user,$jumlahdebit,$jumlahkredit,$jenis,$notransaksi);
		}
		$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter,$kdrekening,$subdivisi,$debit,$kredit,$keterangan,$user,$savekdrekening);
		echo $no;
	}

	function insertNewHeader($flag,$tgl,$dept,$project,$costcenter,$ket,$user,$jumlahdebit,$jumlahkredit,$jenis,$notransaksi)
	{
		$this->jurnalmodel->locktables('counter,trans_jurnal_header');
		$new_no = $this->jurnalmodel->getNewNo($tgl);
		$no = $new_no->NoJurnal;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'NoDokumen'	=> $no,
			'TglDokumen' => $tgl,
			'Jenis' => $jenis,
			'KdDepartemen' => $dept,
			'KdProject' => $project,
			'KdCostCenter' => $costcenter,
			'NoTransaksi' => $notransaksi,
			'Keterangan' => $ket ,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'JumlahDebit' => $jumlahdebit,
			'JumlahKredit' => $jumlahkredit,
			'Status' => ' '
		);
		$this->db->insert('trans_jurnal_header', $data);
		$this->jurnalmodel->unlocktables();
		return $no;
	}
	
	function updateHeader($flag,$no,$tgl,$dept,$project,$costcenter,$ket,$user,$jumlahdebit,$jumlahkredit,$jenis,$notransaksi)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->jurnalmodel->locktables('trans_jurnal_header');
		$data = array(
		    'TglDokumen' => $tgl,
			'Jenis' => $jenis,
			'KdDepartemen' => $dept,
		    'KdProject' => $project,
			'KdCostCenter' => $costcenter,
			'NoTransaksi' => $notransaksi,
			'Keterangan' => $ket ,
			'JumlahDebit' => $jumlahdebit,
			'JumlahKredit' => $jumlahkredit
		);
		if($flag=="edit")
		{
			$data['EditDate'] = $tgl1;
			$data['EditUser'] = $user;
			$this->db->update('trans_jurnal_detail', array('EditDate'=> $tgl1,'EditUser'=>$user), array('NoDokumen' => $no));
		}
		$this->db->update('trans_jurnal_header', $data, array('NoDokumen' => $no));
		$this->jurnalmodel->unlocktables();
	}

	function insertDetail($flag,$no,$tgl,$counter,$kdrekening,$subdivisi,$debit,$kredit,$keterangan,$user,$savekdrekening)
	{
		//echo $flag." - ".$no." - ".$tgl." - ".$counter." - ".$kdrekening." - ".$subdivisi." - ".$debit." - ".$kredit." - ".$keterangan." - ".$user." - ".$savekdrekening;die;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->jurnalmodel->locktables('trans_jurnal_detail');
		if($savekdrekening==""){
			
			$data = array(
				'NoDokumen'	=> $no,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Debit' => $debit,
				'Kredit' => $kredit,
				'KdSubDivisi' => $subdivisi,
				'Keterangan' => $keterangan,
				'Urutan'	=> $counter,
				'Status' => ' '
			);		
			
			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			
			$this->db->insert('trans_jurnal_detail', $data);
			
			if($counter==1){
				$this->db->update('trans_jurnal_header',array('keterangan'=>$keterangan),array('NoDokumen'=>$no));
			}
			
		}
		else 
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Debit' => $debit,
				'Kredit' => $kredit,
				'KdSubDivisi' => $subdivisi,
				'Keterangan' => $keterangan
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_jurnal_detail', $data, array('NoDokumen' => $no,'Urutan'=>$counter));
		}
		$this->jurnalmodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$counter = $this->input->post('urutan');
		$jumlahdebit = $this->input->post('jumlahdebit');
		$jumlahkredit = $this->input->post('jumlahkredit');
		$this->jurnalmodel->locktables('trans_jurnal_detail');
		$this->db->update('trans_jurnal_detail',  array('Status'=>'B'), array('NoDokumen' => $id,'Urutan'=>$counter));
		$this->db->update('trans_jurnal_header',  array('JumlahDebit'=>$jumlahdebit,'JumlahKredit'=>$jumlahkredit,), array('NoDokumen' => $id));
		$this->jurnalmodel->unlocktables();
	}
	
	
	function tarik()
	{
		$hasil = $this->jurnalmodel->tarik();
		
		foreach($hasil AS $val){
			$NoDok = $val['NoDokumen'];
			$Ket = $val['Keterangan'];
			
			$this->db->update('trans_jurnal_header',array('keterangan'=>$Ket),array('NoDokumen'=>$NoDok));
		}
		
		echo "DONE";die;
	}
	
	function ajax_cek_tanggal(){
		$mylib = new globallib();
		$tglinput = $mylib->ubah_tanggal($this->input->post('tgl'));
		$cekperiode = $mylib->CekPeriode('GL', $tglinput);
		
		$data['hasil']= $cekperiode['Valid'];
		$data['tanggal']= $cekperiode['TglAkhir'];
		
		echo json_encode($data);
	}
	
	function test(){
		$mylib = new globallib();
		$tglinput = '2017-01-01';
		$cekperiode = $mylib->CekPeriode('GL', $tglinput);
		print_r($cekperiode);
	}
}
?>