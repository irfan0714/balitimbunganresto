<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class tutup_ticketing extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
         $this->load->model('proses/tutup_ticketing_model');
    }


    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
	        $data['track'] = $mylib->print_track();
			$data['msg'] = "";
			$last_update_query = $this->tutup_ticketing_model->getLastDate();
			$data['tanggal']= $last_update_query;
	        $this->load->view('proses/tutupkasirticketing/rekap', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	function doThis()
	{
		$mylib = new globallib();
		$id = $this->session->userdata('userid');
		$tgl = $this->tutup_ticketing_model->getLastDate();//echo $tgl->TglTrans;die();
		$tahun = substr($tgl->TglTrans,0,4);
		$this->cetak($tgl->TglTrans); // cetak rekap harian hanya untuk counter
		return;
	}
	
        
        function cetak($tgl)
	{

			$id = $this->session->userdata('username');
            $data['store']		= $this->tutup_ticketing_model->aplikasi();
			$data['header']		= $this->tutup_ticketing_model->all_trans($tgl,$id);
			//$data['total']		= $this->tutup_ticketing_model->total_trans($tgl,$id);

			if(!empty($data['header'])){
				$filename	='tutuptiket';
				$ext		='ctk';
				$this->load->helper('print');
				$html = $this->load->view('proses/tutupkasirticketing/cetak_transaksi',$data, TRUE); // jika ada printernya
				header('Content-Type: application/ctk');
				header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
				header('Cache-Control: private, max-age=0, must-revalidate');
				header('Pragma: public');
				print $html;
            }
        }
}
?>