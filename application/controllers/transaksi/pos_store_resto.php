<?php
include APPPATH . '/controllers/auth/authcontroller' . EXT;
class pos_store_resto extends authcontroller {

	function __construct() {
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('transaksi/pos_store_resto_model', 'pos_model');
	}

	function index() {
		$mylib = new globallib();
//        $mylib = new globallib();
		$sign = $mylib->getAllowList("all");
		//print_r($this->session->userdata);die();
		$nm = $mylib->getUser();
		if ($sign == "Y") {
			$data['msg'] = "";
			$data['sales_temp'] = $this->pos_model->sales_temp($nm);
			$data['sales_temp_count'] = 0;
			$ttl = 0;
			$data['TotalNetto'] = 0;
			$data['Charge'] = 0;
			$data['ttlall'] = 0;
			$data['TotalQty'] = 0;

			$data['store'] = $this->pos_model->aplikasi();
			$data['mUang'] = $this->pos_model->getCurrency();
			$data['gsa_list'] = $this->pos_model->getGsaList();
			$data['kartu'] = $this->pos_model->getKartu();
			$data['edc'] = $this->pos_model->getEDC();

			//echo $_SERVER['REMOTE_ADDR'];die();
			$kassa = $this->pos_model->cekidkassa($_SERVER['REMOTE_ADDR']); // cek no kasir
			// $id_kassa = $kassa[0]['id_kassa'];
			// $kode_store = $kassa[0]['KdStore'];

			$id_kassa = '52';
			$kode_store = '22';

			$data['NoKassa'] = array(array('id_kassa' => $id_kassa,
				'KdStore' => $kode_store,
			));

			$data['struk'] = date("H:i:s"); //wscrev

			//		print_r($data['NoKassa']);die();
			$data['track'] = $mylib->print_track();
			//----------------untuk keperluan lcd----
			$ipaddress = $_SERVER['REMOTE_ADDR'];
			$data['iplokal'] = $ipaddress;
			//---------------eo untuk keper....------

			$this->setCookies('penjualanPOS', '', 86400);
			$this->load->view('transaksi/pos_store_resto/tampil', $data);
		} else {
			$this->load->view('denied');
		}
	}

	function insert_temporary() {
//        echo "<pre>"; //        print_r($_POST);//        echo "</pre>";
		$jsdata = $this->input->post('brgDetail');
		$j2 = str_replace("datajson =", "", $jsdata);
		$brgB = json_decode($j2);
		$tgl = $this->pos_model->aplikasi();
		$tgldok = $tgl[0]['TglTrans'];
		$EditFlg = $this->input->post('EditFlg');
		$NoUrut = $this->input->post('NoUrut');
		$kdbrg = $this->input->post('kdbrg1');
		$jumlah = $this->input->post('qty1');
		$harganya = $this->input->post('jualmtanpaformat1');
		$totalnya = $this->input->post('nettotanpaformat1');
		$Struk = $this->input->post('no');
		$disc = $this->input->post('disk1');
		$kassa = $this->input->post('kassa');
		$kasir = $this->input->post('kasir');
		$store = $this->input->post('store');
//        echo $brgB[0]->PPN;
		//        echo "<pre>";      print_r($brgB);echo "</pre>";die();
		if ($jumlah == 0) {
			$this->pos_model->DeleteRecord($kdbrg, $kasir);
		} else {
			$sql = "select NoUrut+1 as NoUrut from sales_temp order by AutoID desc limit 1";
			$qry = $this->db->query($sql);
			$row = $qry->result_array();

			if ($qry->num_rows() == 0) {
				$NoUrut = 1;
			} else {
				$NoUrut = $row[0]['NoUrut'];
			}

			$hasil = $this->pos_model->sales_temp_cek($kdbrg, $kasir);

			$dis_potongan = 0;
			$hrg = $harganya;

			if ($hasil == 0) {
				$data = array(
					'NoUrut' => $NoUrut,
					'NoStruk' => $Struk,
					'KodeBarang' => $kdbrg,
					'Qty' => $jumlah,
					'Disc' => $disc,
					'Harga' => $harganya,
					'Netto' => $hrg,
					//'Netto'        => $totalnya,
					'NoKassa' => $kassa,
					'Kasir' => $kasir,
					'Tanggal' => $tgldok,
					'Waktu' => date('H:i:s'),
					'KdStore' => $store,
					'Status' => 1,
					'Service_charge' => $brgB[0]->Service_charge,
					'Komisi' => $brgB[0]->Komisi,
					'PPN' => $brgB[0]->PPN,
				);
				if ($jumlah != "" or $jumlah != 0) {
					$this->db->insert('sales_temp', $data);
				}
			} else {
				if ($jumlah != 0) {
					$this->pos_model->sales_temp_add($jumlah, $kdbrg, $Struk, $EditFlg, $kasir, $dis_potongan);
				} else {
					$this->pos_model->DeleteRecord($kdbrg, $kasir);
				}
			}
			$this->CekBonusPromo($Struk, $kasir, $brgB);
		}
		//$this->index();

		redirect('/transaksi/pos_store_resto/');
	}

	function CekBonusPromo($struk, $kasir, $cekDapatBonus) {
		$dataR = $this->pos_model->ambilDataTemp($struk, $kasir);
		//print_r($dataR);
		foreach ($dataR as $b) {
//            $cekDapatBonus = $this->pos_model->CekBonus($b['KodeBarang'], $b['Tanggal']);
			//print_r($cekDapatBonus);die();
			foreach ($cekDapatBonus as $dp) {
				if (!empty($dp->RupBar)) {
					if ($dp->RupBar == "P") {
						if (!empty($dp->Opr1)) {
							if ($b['TTL'] >= $dp->Nilai1) {
								$dis_potongan = $b['Qty'] * $b['Harga'] * $dp->Nilai / 100;
							} else {
								$dis_potongan = 0;
							}
						} else {
							$dis_potongan = $dp->Nilai;
						}
						$this->db->update('sales_temp', array('RupBar' => $dp->RupBar, 'ketPromo' => $dp->Nilai, 'Disc' => $dis_potongan, 'Netto' => ($b['Qty'] * $b['Harga']) - $dis_potongan), array('KodeBarang' => $b['KodeBarang'], "NoStruk" => $struk, 'Kasir' => $kasir));
					}
				}
			}
		}
	}

	function LastRecord() {
		$mylib = new globallib();
		$nm = $mylib->getUser();
		$this->pos_model->LastRecord(1, $nm);
	}

	function CustomerView($pelanggan) {
		$this->pos_model->customer($pelanggan);
	}

	function VoucherCustomer($id_voucher) {
		$this->pos_model->voucher($id_voucher);
	}

	function EditRecord($x) {
//        echo $x;
		$coo = $this->getCookie("penjualanPOS");
		if (!empty($coo)) {
			$det = json_decode($coo);
			//echo "<pre>";
			//for($x=0;$x < count($detPOS);$x++){
			$pcode = $det[$x - 1]->PCode;
			$hrg = $det[$x - 1]->Harga1c;
			$nama = $det[$x - 1]->NamaLengkap;
//                $hpp    = $det[$x]->HPP;
			//                $Service_charge = $det[$x]->Service_charge;
			//                $Komisi= $det[$x]->Komisi;
			//                $RupBar = $det[$x]->RupBar;
			//                $PPN  = $det[$x]->PPN;
			$qty = $det[$x - 1]->Qty;
			$Netto = $det[$x - 1]->Netto;
			$disc = $det[$x - 1]->disc;
			$hsl = $pcode . "||" . $nama . "||" . $hrg . "||" . $qty . "||" . $Netto . "||" . $disc;
		}
		echo $hsl;
		//die();
		//$this->pos_model->EditRecord($NoUrut);
	}

	function save_trans() {
		//echo "<pre>";print_r($_POST);echo "</pre>";die();
		//$coo =  $this->getCookie("penjualanPOS");
		//if(!empty($coo)){
		//    $detPOS = json_decode($coo);
		//echo "<pre>";print_r($detPOS);echo "</pre>";
		//}
		//die();
		//$nostruk            = $this->input->post('confirm_struk');

		$mylib = new globallib();
		$nokassa = $this->input->post('confirm_kassa');
		$totalnya = $this->input->post('total_biaya');
		$dpp = $this->input->post('dpp');
		$tax = $this->input->post('tax');
		$id_customer = $this->input->post('id_compliment');
		if ($id_customer == '') {
			$id_customer = $this->input->post('id_voucher');
		}

		$id_members = $this->input->post('id_members');
		$id_card = $this->input->post('id_card');
		$nm_card = $this->input->post('nm_card');
		$jenis_discount = $this->input->post('jenis_discount');

		$nama_customer = $this->input->post('nama_customer');
		$gender_customer = $this->input->post('gender_customer');
		$tgl_customer = $this->input->post('tgl_customer');
		$totalItem = $this->input->post('TotalItem2');

		$userdisc = $this->input->post('iduserdiscount');
		$nilaidisc = $this->input->post('id_discount');

		$listvoucher = $this->input->post('listvoucher');
		$listvouchpakai = $this->input->post('listvouchpakai');
		$listjenis = $this->input->post('listjenis');

		$voucher1 = explode('##', $listvoucher);
		$vouchpakai1 = explode('##', $listvouchpakai);
		$vouchjenis1 = explode('##', $listjenis);

		$listpcode = $this->input->post('listpcode');
		$listqty = $this->input->post('listqty');
		$listharga = $this->input->post('listharga');
		$listdisc = $this->input->post('listdisc');
		$listnetto = $this->input->post('listnetto');
		$listkomisi = $this->input->post('listkomisi');

		$pcode1 = explode('##', $listpcode);
		$qty1 = explode('##', $listqty);
		$harga1 = explode('##', $listharga);
		$disc1 = explode('##', $listdisc);
		$netto1 = explode('##', $listnetto);
		$komisi1 = explode('##', $listkomisi);

		$id_kredit = $this->input->post('id_kredit');
		$id_kredit_edc = $this->input->post('id_kredit_edc');
		$id_debet = $this->input->post('id_debet');
		$id_debet_edc = $this->input->post('id_debet_edc');
		$id_voucher = $this->input->post('id_voucher');
		$id_compliment = $this->input->post('id_compliment');
		$Uang = $this->input->post('Uang'); //Currency
		list($matauang, $nilaiMataUang) = explode("-", $Uang);
		$point = $this->input->post('bayar_point');
		$cash = $this->input->post('cash_bayar'); // * $nilaiMataUang);
		$kredit = $this->input->post('kredit_bayar');
		$debet = $this->input->post('debet_bayar');
		$voucher = $this->input->post('voucher_bayar');
		$jenis_voucher = $this->input->post('jenis_voucher');
		$valas = $this->input->post('valas_bayar');
		$service_charge = $this->input->post('service_charge');
		$discount = $this->input->post('discount_bayar');

		$id_valas = $this->input->post('id_valas');
		$id_kurs = $this->input->post('id_kurs');
		$gdg = $this->pos_model->GetGudang($nokassa); //get kode gudang
		$gudang = (!empty($gdg)) ? $gdg : "00";

		$pembulatan = $this->input->post('rounding_hide');
		$total_bayar = $this->input->post('total_bayar_hide');
		$kembali = $this->input->post('cash_kembali');
		$apl = $this->pos_model->aplikasi();
		//$tgl = $apl[0]['TglTrans'];
		//$tgl = date("Y-m-d");
		//print_r($tgl);

		$tgl = $this->input->post('tgltrans');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 0, 4);

		$kd_agent = $this->input->post('kdagent');
		$kd_gsa = $this->input->post('gsa');
		$ketr = $this->input->post('no'); //wscrev

		$hasil = $this->pos_model->cekno($nokassa, $ketr, $tgl); //wscrev
		if ($hasil == 0) //wscrev
		{
			$no = $this->pos_model->ambil_No($tahun, $bulan, $tgl, $nokassa);
			//date_default_timezone_set("Asia/Jakarta");
			$Jamsekarang = date("H:i:s");
			$mylib = new globallib();
			$nm = $mylib->getUser();
			$ip = $_SERVER['REMOTE_ADDR'];

			//wscrev
			$this->pos_model->do_simpan_header($no, $nm, $gudang, $nokassa, $tgl, $Jamsekarang, $totalItem, $pembulatan, $totalnya, $dpp, $tax, $discount, $kd_agent, $kd_gsa, $userdisc, $nilaidisc, $id_valas, $id_kurs, $ketr, $id_members, $id_card, $nm_card, $jenis_discount); // simpan trans header

			$bruto = 0;
			$mmaks = count($pcode1);
			for ($m = 0; $m < $mmaks - 1; $m++) {
				$pcode = $pcode1[$m];
				$qty = (int) $qty1[$m];
				$hrg = (int) $harga1[$m];
				$disc = (int) $disc1[$m];
				$netto = (int) $netto1[$m];
				$komisi = $komisi1[$m];
				$id_temp = strtoupper($id_voucher);
				$vou = substr($id_temp, 0, 2);
				$user = $this->session->userdata('username');
				if ($user == 'krisna337') {
					print_r($vou);
				}

				// print_r($vou);
				// echo "|";
				if ($vou !== 'VL' && $vou !== 'VI') {
					if ($user == 'krisna337') {
						echo "masukVL";
					}
					// echo "masukVL";
					if ($id_customer != '');
					{
						//$komisi = 0; //voucher karyawan dan compliment tidak ada komisi.
					}

				}
				$hpp = 0;
				$Service_charge = 0;
				//$komisi = $this->pos_model->cekkomisi($pcode);
				$RupBar = 0;
				$PPN = 10;
				$bruto = $bruto + ($qty * $hrg);

				$this->pos_model->do_simpan_detail($no, $gudang, $nokassa, $tgl, $Jamsekarang, $nm, $pcode, $hrg, $hpp, $Service_charge, $komisi, $RupBar,
					$PPN, $qty, $netto, $disc, $kd_agent); // simpan trans detail
				$this->pos_model->do_simpan_mutasi($no, $nm, $gudang, $nokassa, $tgl, $Jamsekarang, $nm, $pcode, $hrg, $hpp, $Service_charge, $komisi, $RupBar,
					$PPN, $qty, $netto, $disc); // simpan trans mutasi

				$cekSGDG = $this->pos_model->cekStockGDG($pcode, $tahun, $gudang); // cek stock gudang

				$fieldakhir = "GAkhir" . $bulan;
				$fieldkeluar = "GKeluar" . $bulan;

				//===== update stock gudang ======
				if (empty($cekSGDG)) {
// buat baru
					$data = array(
						'Tahun' => $tahun,
						'PCode' => $pcode,
						'KdGudang' => $gudang,
						$fieldkeluar => $qty,
						$fieldakhir => ($qty * -1),
					);
					$this->db->insert('stock', $data);
				} else {
					$dataK = array(
						$fieldkeluar => $cekSGDG[0][$fieldkeluar] + $qty,
						$fieldakhir => $cekSGDG[0][$fieldakhir] - $qty,
					);
					$this->db->update('stock', $dataK, array("Tahun" => $tahun, "PCode" => $pcode, "KdGudang" => $gudang));
				}
				// == end stock ===*/
			}

			$mmaks = count($voucher1);
			for ($m = 0; $m < $mmaks - 1; $m++) {
				$voucher0 = $voucher1[$m];
				$vouchpakai0 = (int) $vouchpakai1[$m];
				$vouchjenis0 = $vouchjenis1[$m];
				$this->pos_model->do_simpan_voucher($no, $nokassa, $tgl, $voucher0, $vouchpakai0, $vouchjenis0, $voucher);
			}

			// $this->pos_model->clear_trans($nm); // hapus temp
			$this->pos_model->bayar_trans($no, $cash, $kredit, $debet, $jenis_voucher, $voucher, $total_bayar, $id_customer, $nama_customer, $gender_customer, $tgl_customer, $kd_agent, $service_charge, $valas, $point, $id_kredit, $id_kredit_edc, $id_debet, $id_debet_edc);
			$this->pos_model->save_trans_bayar($no, $nokassa, $nama_customer, $id_kredit, $kredit, $id_debet, $debet, $id_voucher, $voucher, $cash, $gudang, $Uang, $valas, $id_valas, $id_kurs, $id_members, $id_card, $nm_card, $point);
			//$ttl 	=	$this->pos_model->do_hitung_bonus($no);
			//$ttl; die();
			$this->db->update('transaksi_header', array('Status' => '1'), array("NoStruk" => $no));
			$this->db->update('transaksi_detail', array('Status' => '1'), array("NoStruk" => $no));

			if ($id_members != "") {
				//ambil nilai point 1
				$jmlpoint = $this->pos_model->ambil_point($id_members);
				$nilaipoint = $this->pos_model->nilai_point($id_members);
				//kurangi point
				$this->db->update('member', array("JumlahPoint" => ($jmlpoint[0]['JumlahPoint'] - $point) + floor($totalnya / $nilaipoint[0]['NilaiPoint'])), array('KdMember' => $id_members));
			}

			if (($id_compliment != '') && ($totalnya == 0)) {
				$this->pos_model->do_simpan_compliment($id_compliment, $bruto);
			}

			/* Insert to log
	         */
			$datalog = array(
				'nostruk' => $no,
				'kasir' => $nm,
				'status' => '1',
				'ip' => $ip,
			);
			$this->db->insert('log_nostruk', $datalog);

			//$this->cetak($no);
			$this->setCookies('penjualanPOS', '', 86400);

			$data['sts'] = "1";
			$data['nama'] = "Total Bayar :";
			$data['harga'] = $totalnya;

			//$this->load->view('/transaksi/pos_store_resto/test_lcd',$data);
			//$this->load->view('transaksi/sales/tampil','');
			// $this->index();
			$this->konfirm_cetak($no, $nokassa);
			//redirect('/transaksi/pos_store_resto/');
		} else {
			redirect('/transaksi/pos_store_resto/'); //wscrev
		}
	}

	function save_trans_feeding_fish() {
		//echo "<pre>";print_r($_POST);echo "</pre>";die();

		$mylib = new globallib();

		$tiket_enty = $this->input->post('tiket_enty');
		$SubKatTicket = $this->input->post('SubKatTicket');
		$NoTrans = $this->input->post('NoTrans');
		$ttlall2 = $this->input->post('ttlall2');
		$TotalItem2 = $this->input->post('TotalItem2');
		$dpp = $this->input->post('dpp');
		$tax = $this->input->post('tax');
		$NamaLengkap0 = $this->input->post('NamaLengkap0');
		$qty10 = $this->input->post('qty10');
		$jualm10 = $this->input->post('jualm10');
		$netto10 = $this->input->post('netto10');
		$nmticket0 = $this->input->post('nmticket0');
		$jenis0 = $this->input->post('jenis0');
		$tipe0 = $this->input->post('tipe0');
		$charge0 = $this->input->post('charge0');
		$nominalVouch0 = $this->input->post('nominalVouch0');
		$jml = $this->input->post('jml');
		$nokassa = $this->input->post('kassa');
		$kasir = $this->input->post('kasir');
		$store = $this->input->post('store');
		$brgDetail = $this->input->post('brgDetail');
		$tampils_jenis = $this->input->post('tampils_jenis');
		$TourTravel = $this->input->post('TourTravel');
		$TourGuide = $this->input->post('TourGuide');
		$PetugasId = $this->input->post('PetugasId');
		$NoIdentitas = $this->input->post('NoIdentitas');
		$Country = $this->input->post('Country');
		$NamaRombongan = $this->input->post('NamaRombongan');
		$TotalNettoHidde = $this->input->post('TotalNettoHidde');
		$SerCharge2 = $this->input->post('SerCharge2');
		$minimum = $this->input->post('minimum');
		$nil_disc = $this->input->post('nil_disc');
		$id_discount = $this->input->post('id_discount');
		$confirm_struk = $this->input->post('confirm_struk');
		$confirm_kassa = $this->input->post('confirm_kassa');
		$service_charge = $this->input->post('service_charge');
		$total_biaya = $this->input->post('total_biaya');
		$listvoucher = $this->input->post('listvoucher');
		$listvouchket = $this->input->post('listvouchket');
		$listvouchsaldo = $this->input->post('listvouchsaldo');
		$listvouchpakai = $this->input->post('listvouchpakai');
		$listjenis = $this->input->post('listjenis');
		$Uang = $this->input->post('Uang');
		$pilihan = $this->input->post('pilihan');
		$cash_bayar = $this->input->post('cash_bayar');
		$jml_free_ticket = $this->input->post('jml_free_ticket');
		$rounding_hide = $this->input->post('rounding_hide');
		$total_bayar_hide = $this->input->post('total_bayar_hide');
		$cash_kembali_hide = $this->input->post('cash_kembali_hide');
		$id_customer = "";
		$id_compliment = "";
		if ($pilihan == "cash") {
			$cash = "cash";
			$kredit = "";
			$debet = "";
		} else if ($pilihan == "kredit") {
			$cash = "";
			$kredit = "kredit";
			$debet = "";
		} else if ($pilihan == "debit") {
			$cash = "";
			$kredit = "";
			$debet = "debit";
		}

		$apl = $this->pos_model->aplikasi();
		$tgl = $apl[0]['TglTrans'];

		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 0, 4);

		$kd_agent = '';
		$kd_gsa = '';
		$ketr = $NoTrans; //wscrev

		$hasil = $this->pos_model->cekno($nokassa, $ketr, $tgl); //wscrev
		if ($hasil == 0) //wscrev
		{
			$no = $this->pos_model->ambil_No($tahun, $bulan, $tgl, $nokassa);
			//date_default_timezone_set("Asia/Jakarta");
			$Jamsekarang = date("H:i:s");
			$mylib = new globallib();
			$nm = $mylib->getUser();
			$ip = $_SERVER['REMOTE_ADDR'];

			$gudang = '11';
			$totalItem = $qty10;
			$totalnya = $total_biaya;
			$pembulatan = $rounding_hide;
			$dpp = $total_biaya;
			$tax = 0;
			$discount = 0;
			$kd_agent = '';
			$kd_gsa = '';
			$userdisc = '';
			$nilaidisc = 0;
			$id_valas = '';
			$id_kurs = '';
			$ketr = '';
			$id_members = '';
			$id_card = '';
			$nm_card = '';
			$jenis_discount = '';

			//wscrev
			$this->pos_model->do_simpan_header($no, $nm, $gudang, $nokassa, $tgl, $Jamsekarang, $totalItem, $pembulatan, $totalnya, $dpp, $tax, $discount, $kd_agent, $kd_gsa, $userdisc, $nilaidisc, $id_valas, $id_kurs, $ketr, $id_members, $id_card, $nm_card, $jenis_discount); // simpan trans header

			$bruto = 0;
			if ($nmticket0 != '') {
				$pcode = $nmticket0;
				$qty = $qty10;
				$hrg = $jualm10;
				$disc = 0;
				$netto = $netto10;
				$komisi = 0;
				$id_temp = '';
				$vou = substr($id_temp, 0, 2);
				$user = $this->session->userdata('username');
				if ($user == 'krisna337') {
					print_r($vou);
				}

				// print_r($vou);
				// echo "|";
				if ($vou !== 'VL' && $vou !== 'VI') {
					if ($user == 'krisna337') {
						echo "masukVL";
					}
					// echo "masukVL";
					if ($id_customer != '');
					{
						//$komisi = 0; //voucher karyawan dan compliment tidak ada komisi.
					}

				}
				$hpp = 0;
				$Service_charge = 0;
				$komisi = 0;
				$RupBar = 0;
				$PPN = 10;
				$bruto = $bruto + ($qty * $hrg);

				$this->pos_model->do_simpan_detail($no, $gudang, $nokassa, $tgl, $Jamsekarang, $nm, $pcode, $hrg, $hpp, $Service_charge, $komisi, $RupBar,
					$PPN, $qty, $netto, $disc, $kd_agent); // simpan trans detail
				$this->pos_model->do_simpan_mutasi($no, $nm, $gudang, $nokassa, $tgl, $Jamsekarang, $nm, $pcode, $hrg, $hpp, $Service_charge, $komisi, $RupBar,
					$PPN, $qty, $netto, $disc); // simpan trans mutasi
			}

			$jenis_voucher = "";
			$voucher = "";
			$total_bayar = $total_bayar_hide;
			$id_customer = "";
			$nama_customer = "";
			$gender_customer = "";
			$tgl_customer = "";
			$kd_agent = "";
			$service_charge = "0";
			$valas = "0";
			$point = "0";
			$id_kredit = "";
			$id_debet = "";
			$id_voucher = "";
			$Uang = $netto10;
			$valas = "0";
			$id_valas = "";
			$id_kurs = "";
			$id_members = "";
			$id_card = "";
			$nm_card = "";
			$this->pos_model->bayar_trans($no, $cash, $kredit, $debet, $jenis_voucher, $voucher, $total_bayar, $id_customer, $nama_customer, $gender_customer, $tgl_customer, $kd_agent, $service_charge, $valas, $point, $id_kredit, $id_kredit_edc, $id_debet, $id_debet_edc);
			//$this->pos_model->save_trans_bayar($no, $nokassa, $nama_customer, $id_kredit, $kredit, $id_debet, $debet, $id_voucher, $voucher, $cash, $gudang, $Uang,$valas,$id_valas,$id_kurs, $id_members, $id_card, $nm_card, $point);
			//$ttl 	=	$this->pos_model->do_hitung_bonus($no);
			//$ttl; die();
			$this->db->update('transaksi_header', array('Status' => '1'), array("NoStruk" => $no));
			$this->db->update('transaksi_detail', array('Status' => '1'), array("NoStruk" => $no));

			if ($id_members != "") {
				//ambil nilai point 1
				$jmlpoint = $this->pos_model->ambil_point($id_members);
				$nilaipoint = $this->pos_model->nilai_point($id_members);
				//kurangi point
				$this->db->update('member', array("JumlahPoint" => ($jmlpoint[0]['JumlahPoint'] - $point) + floor($totalnya / $nilaipoint[0]['NilaiPoint'])), array('KdMember' => $id_members));
			}

			if (($id_compliment != '') && ($totalnya == 0)) {
				$this->pos_model->do_simpan_compliment($id_compliment, $bruto);
			}

			/* Insert to log
	         */
			$datalog = array(
				'nostruk' => $no,
				'kasir' => $nm,
				'status' => '1',
				'ip' => $ip,
			);
			$this->db->insert('log_nostruk', $datalog);

			//$this->cetak($no);
			$this->setCookies('penjualanPOS', '', 86400);

			$data['sts'] = "1";
			$data['nama'] = "Total Bayar :";
			$data['harga'] = $totalnya;

			//$this->load->view('/transaksi/pos_store_resto/test_lcd',$data);
			//$this->load->view('transaksi/sales/tampil','');
			// $this->index();
			$this->konfirm_cetak_feeding_fish($no, $nokassa);
			//redirect('/transaksi/pos_store_resto/');
		} else {
			redirect('/transaksi/ticketing/'); //wscrev
		}
	}

	function clear_trans() {
		$mylib = new globallib();
		$nostruk = $this->input->post('nostruk');
		$btl = $this->uri->segment(4);
		if (empty($btl)) {
			$this->pos_model->save_trans_header($nostruk);
			$this->pos_model->save_trans($nostruk);
			$this->pos_model->clear_trans($nostruk);
		} else {
			$nm = $mylib->getUser();
			$this->pos_model->clear_kasir($nm);
		}
		$this->index();
		redirect('/transaksi/pos_store_resto/');
	}

	function no_struk($user) {
		//cek nomor di sales temp
		$cek_temp = $this->pos_model->sales_temp($user);
		$temp = $this->pos_model->no_struk_temp();
		if (!empty($cek_temp)) {
			$z = $cek_temp[0]['NoStruk'];
		} else {
			if (empty($temp)) {
				$b = $this->pos_model->no_struk();
				$z = $b[0]['NoStruk'] + 1;
			} else {
				$z = $temp[0]['NoStruk'] + 1;
			}
		}
		return $z;
	}

	function cekkode() {
		$kd = $this->input->post('PCode');
		if (strlen($kd) == 13) {
			$cekbarcode = $this->pos_model->cekBarcode($kd);
		} else {
			$cek = $this->pos_model->cekPCode($kd);
		}

		if (!empty($cek)) {
			echo $hasil = "ok";
		} else {
			echo $hasil = "tidak";
		}
//            echo $kd;die();
		return $hasil;
	}

	function konfirm_cetak($notrans, $kassa) {
		$data['notrans'] = $notrans;
		$data['kassa'] = $kassa;
		$this->load->view('transaksi/pos_store_resto/pos_print', $data);
	}

	function konfirm_cetak_feeding_fish($notrans, $kassa) {
		$data['notrans'] = $notrans;
		$data['kassa'] = $kassa;
		$this->load->view('transaksi/pos_store_resto/pos_print_feeding_fish', $data);
	}

	function cetak($no, $kassa) {
		// $no	= $this->uri->segment(4);

		$data['store'] = $this->pos_model->aplikasi();
		$data['header'] = $this->pos_model->all_trans($no);
		$data['detail'] = $this->pos_model->det_trans($no);

		//$this->load->view('transaksi/pos_store_resto/cetak_transaksi_wireness', $data); // jika ada printernya
		if($data['header'][0]['KdCustomer'] != ''){
            #dikomen data voucher karyawan belum bener
            $sisaVoucherEmployee = $this->pos_model->get_voucher_employee($data['header'][0]['KdCustomer'],$data['header'][0]['Tanggal']);

        }else{
            $sisaVoucherEmployee = array();
        }

        // echo "<pre>";print_r($sisaVoucherEmployee);die();
        $data['GetSisaVoucherEmployee'] = $sisaVoucherEmployee;
		$html = $this->load->view('transaksi/pos_store_resto/cetak_transaksi_wireness_dos', $data);

		$filename = 'pos';
		$ext = 'ctk';

		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;

	}

	function cetak_feeding_fish($no, $kassa) {
		// $no	= $this->uri->segment(4);

		$data['store'] = $this->pos_model->aplikasi();
		$data['header'] = $this->pos_model->all_trans($no);
		$data['detail'] = $this->pos_model->det_trans($no);

		//$this->load->view('transaksi/pos_store_resto/cetak_transaksi_wireness', $data); // jika ada printernya

		$html = $this->load->view('transaksi/pos_store_resto/cetak_transaksi_wireness_dos', $data);

		$filename = 'pos';
		$ext = 'ctk';

		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;

	}

	function cetakstrukakhir() {
		// $no	= $this->uri->segment(4);
		$kassa = $this->pos_model->cekidkassa($_SERVER['REMOTE_ADDR']);
		//$kassa = '11';
		$no = $this->pos_model->laststruk($kassa[0]['id_kassa']);
		$data['store'] = $this->pos_model->aplikasi();
		$data['header'] = $this->pos_model->all_trans($no);
		$data['detail'] = $this->pos_model->det_trans($no);

		$html = $this->load->view('transaksi/pos_store_resto/cetak_transaksi_wireness_dos', $data);

		$filename = 'pos';
		$ext = 'ctk';

		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;

	}

	function DetailItemForSales($PCode) {
		$det = array();
		$pcode1 = explode('~', $PCode);
		$mmaks = count($pcode1);
		if ($mmaks == 3) {
			$qty0 = $pcode1[0];
			$pcode0 = $pcode1[1];
			$tgl0 = $pcode1[2];
		} else {
			$qty0 = 1;
			$pcode0 = $pcode1[0];
			$tgl0 = $pcode1[1];
		}
		$b = $this->pos_model->DetailItemForSales($pcode0, $qty0, $tgl0);

//        echo"<pre>";print_r($b);echo"</pre>";
		if ($b != "salah" and $b != "non aktif") {
// jika oke jalankan add cookie
			//

			$det[] = $b[0];
			//print_r($det);
			//            $this->setCookies('penjualanPOS',$det,86400);
			$this->setCookies('penjualanPOS', json_encode($det), 86400);
			// die();
			echo "ok";
		} else {
// barang salah
			echo $b;
		}
	}

	function cekvalas($pvalas) {
		$b = $this->pos_model->DetailValas($pvalas);
		echo $b;
	}

	function displaycust($kirim) {
		$kirim1 = explode('~', $kirim);
		$data['nama'] = $kirim1[0];
		$data['harga'] = $kirim1[1];
		$data['total'] = $kirim1[2];
		$this->load->view('transaksi/pos_store_resto/test_lcd', $data);
		echo "ok";
	}

	function EditDetailItemForSales() {
		$pcode = $this->input->post('PCode');
		$qty = $this->input->post('Qty');
//        die;
		$det = array();
		$b = $this->pos_model->DetailItemForSales($pcode);

//        echo"<pre>";print_r($b);echo"</pre>";
		if ($b != "salah") {
// jika oke jalankan add cookie
			//
			$coo = $this->getCookie("penjualanPOS");
//            $k1 =  $this->getCookie("penjualanPOS");
			//            $k2 = str_replace("[[","[",$k1);
			//            $k3 = str_replace("]]","]",$k2);
			//            $coo = $k3;

			if (!empty($coo)) {
				$c = json_decode($coo);
				for ($z = 0; $z < count($c); $z++) {
					//  echo $c[$z]->PCode;
					//  echo $b[0]['PCode'];
					if ($c[$z]->PCode == $b[0]['PCode']) {
						$ar = array(
							"PCode" => $c[$z]->PCode,
							"NamaLengkap" => $c[$z]->NamaLengkap,
							"Harga1c" => $c[$z]->Harga1c,
							"Barcode1" => $c[$z]->Barcode1,
							"Service_charge" => $c[$z]->Service_charge,
							"Komisi" => $c[$z]->Komisi,
							"Jenis" => $c[$z]->Jenis,
							"RupBar" => $c[$z]->RupBar,
							"Perhitungan" => $c[$z]->Perhitungan,
							"Period1" => $c[$z]->Period1,
							"Period2" => $c[$z]->Period2,
							"Beban" => $c[$z]->Beban,
							"NoRekening" => $c[$z]->NoRekening,
							"Nilai" => $c[$z]->Nilai,
							"HadiahBarang" => $c[$z]->HadiahBarang,
							"Opr1" => $c[$z]->Opr1,
							"Nilai1" => $c[$z]->Nilai1,
							"Opr2" => $c[$z]->Opr2,
							"Nilai2" => $c[$z]->Nilai2,
							"Campur" => $c[$z]->Campur,
							"PPN" => $c[$z]->PPN,
							"Qty" => $qty,
							"Netto" => ((int) $qty) * (int) $c[$z]->Harga1c,
							"disc" => 0,
							"HPP" => $c[$z]->HPP,
						);
						$det[$z] = $ar;
						$hsl = 1;
					} else {
						$det[$z] = $c[$z];
						$hsl = 0;
					}
				} // jika tidak ada yg sama add row
				if ($hsl == 0) {
					$det[] = $b[0];
				}
			} else {
				$det[] = $b[0];
			}
			//print_r($det);
			//            $this->setCookies('penjualanPOS',$det,86400);
			$this->setCookies('penjualanPOS', json_encode($det), 86400);
			//die();
			echo "ok";
		} else {
// barang salah
			echo $b;
		}
	}

	function setCookies($cookie_name, $cookie_value, $days) {
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day
	}

	function getCookie($cookie_name) {
		if (!isset($_COOKIE[$cookie_name])) {
			$hsl = "";
		} else {
			$hsl = $_COOKIE[$cookie_name];
		}

		return $hsl;
	}

	function carddisc($NamaCard, $KdStore, $KodeGroup) {
		$data = $this->pos_model->getDiscCard($NamaCard, $KdStore, $KodeGroup);
		if (!empty($data)) {
			echo json_encode($data);
		} else {
			echo json_encode($data['cek'] = "NO");
		}

	}

}

?>