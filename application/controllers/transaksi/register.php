<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Register extends authcontroller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/register_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $user           = $this->session->userdata('username');
        if ($sign == "Y") {
            $segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);
            $id = addslashes(trim($this->input->post('stSearchingKey')));
            $id2 = $this->input->post('date1');
            $with = $this->input->post('searchby');
            if ($with == "Tanggal") {
                $id = $mylib->ubah_tanggal($id2);
            }
            $this->load->library('pagination');

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = '</span>';
            $config['per_page'] = '10';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['base_url'] = base_url() . 'index.php/transaksi/register/index/';
            $page = $this->uri->segment(4);
            $config['uri_segment'] = 4;
            $flag1 = "";
            if ($with != "") {

                if ($id != "" && $with != "") {
                    $config['base_url'] = base_url() . 'index.php/transaksi/register/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                } else {
                    $page = "";
                }
            } else {
                if ($this->uri->segment(5) != "") {

                    $with = $this->uri->segment(4);
                    $id = $this->uri->segment(5);
                    if ($with == "Tanggal") {
                        $id = $mylib->ubah_tanggal($id);
                    }
                    $config['base_url'] = base_url() . 'index.php/transaksi/register/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                }
            }
            $data['judul']			= "Register";
            $data['header']			= array("No Transaksi", "Tanggal","Kode Agent", "Nama","No Stiker","Jumlah rombongan", "No Mobil");
            $config['total_rows']	= $this->register_model->num_komisi_row(addslashes($id), $with);
            $data['otorisasi']	= $this->register_model->cek_otorisasi($user);
            $data['data']			= $this->register_model->getRegisterList($config['per_page'], $page, addslashes($id), $with);

            $data['track']			= $mylib->print_track();
            $this->pagination->initialize($config);
            $this->load->view('transaksi/register/register_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $aplikasi = $this->register_model->getDate();
//			$data['mperusahaan'] = $this->retur_barangmodel->getPerusahaan();
            $data['aplikasi']   = $aplikasi;
            $data['tl']         =  $this->register_model->getTourLeader();
            $data['tourtravel'] = $this->register_model->getTourTravel();
            $data['judul']      = "Form Register Tour";
            $data['track']  = $mylib->print_track();
            $this->load->view('transaksi/register/add_register', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function approveregis(){
		$kode           = $this->input->post('kode');
		$user           = $this->session->userdata('username');
		$data = array(
					'Approval_By'=>$user,
					'Approval_date'=>date('Y-m-d H:i:s')
					);
		$this->db->update('register',$data,array('KdRegister'=>$kode));
		echo json_encode(array('success'=>true));
	}

    function save_new_register() {
//        echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();

        $user           = $this->session->userdata('userid');
        $flag           = $this->input->post('flag');
        $no             = $this->input->post('nodok');
        $tgl            = $this->input->post('tgl');
        $jam            = $this->input->post('jam');
        $kode           = $this->input->post('kode');
		$bank           = $this->input->post('bank');
		$norekbank      = $this->input->post('norekbank');
        $ket            = trim(strtoupper(addslashes($this->input->post('ket'))));
        $jumlah         = $this->input->post('jumlah');
        $nilai_bypass   = $this->input->post('nilai_bypass')*1;
        $stiker         = $mylib->save_char($this->input->post('nostiker'));
        //a24
        $tourtravel     = $this->input->post('tourtravel');

		if($nilai_bypass==0){
			$ByPass="N";
		}else{
			$ByPass="Y";
		}


        if ($no == "") {
            //insert new header
            $no = $this->insertNewHeader($flag, $user,$tgl,$jam,$jumlah,$kode,$ket,$stiker, $norekbank, $bank, $tourtravel, $ByPass);
            //insert new detail
        } else {
            $this->updateHeader($flag, $no, $nobukti, $ket, $total, $tgl, $user);
        }
        redirect('/transaksi/register/');
    }

    function insertNewHeader($flag, $user,$tgl,$jam,$jumlah,$kode,$ket,$stiker, $norekbank, $bank, $tourtravel, $ByPass){
        //$this->register_model->locktables('keuangan_pvheader');
        $mylib = new globallib();
        $bulan = substr($tgl, 3, 2);
        $tahun = substr($tgl, -2);
        $no         = $this->get_no_counter('register','KdRegister',$tahun,$bulan);
//die();
        $data = array(
            'KdRegister'    => $no,
            'KdTourLeader'  => $kode,
            'Jam'           => $jam,
            'Tanggal'       => $mylib->ubah_tanggal($tgl),
            'NoStiker'      => $stiker,
            'Jumlah'        => $jumlah,
			'Bank'			=> $bank,
			'NoRekBank'	=> $norekbank,
            'Keterangan'    => $ket,
            'AddDate' => date("Y-m-d"),
            'AddUser' => $user,
            'KdTravel' => $tourtravel,
            'ByPass' => $ByPass
        );
        $this->db->insert('register', $data);
        //$this->register_model->unlocktables();
    }

    function updateHeader($flag, $no, $nobukti, $ket, $total, $tgl, $user) {
        // $tgl = $this->session->userdata('Tanggal_Trans');
        // $this->register_model->locktables('keuangan_pvheader,keuangan_pvdetail');
        $data = array(
            'NoTransaksi' => $no,
            'NoBukti' => $nobukti,
            'Keterangan' => $ket,
            'Total' => $total,
            'EditDate' => date("Y-m-d"),
            'EditUser' => $user
        );
        if ($flag == "edit") {
            $data['EditDate'] = date("Y-m-d");
            $data['EditUser'] = $user;
            $this->db->update('finance_komisi_detail', array('EditDate' => date("Y-m-d"), 'EditUser' => $user), array('NoTransaksi' => $no));
        }
        $this->db->update('finance_komisi_header', $data, array('NoTransaksi' => $no));
        $this->register_model->unlocktables();
    }

    function get_no_counter( $table_name, $col_primary, $thn,$bln) {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,4) = '" .$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 4, 7) * 1) + 1;
        $counter_fa = sprintf($thn . $bln. sprintf("%07s", $counter));
        return $counter_fa;

    }

    function getlistBarang() {
        $field = $this->input->post('kdagent');
        if (!empty($field)) {
            $NoAgent = "WHERE a.KdAgent = '$field'";
        } else {
            $NoAgent = "";
        }
        $detail = $this->register_model->getKomisi($NoAgent);
        // print_r($detail);
        $nilai = "";
        for ($a = 0; $a < count($detail); $a++) {
            $nilai .= $detail[$a]['NoStruk'] . "||" . $detail[$a]['TglJual'] . "||" . $detail[$a]['PCode'] . "||" . $detail[$a]['NamaLengkap'] . "||" . $detail[$a]['Qty'] . "||" . $detail[$a]['Komisi'] . "||" . $detail[$a]['Harga'] . "||" . $detail[$a]['Nilai'] . "**";
        }
        echo count($detail) . "##" . $nilai;
    }

    function getPCode() {
        $kode = $this->input->post('pcode');
        $name = $this->input->post('nama');
        $tgl = $this->input->post('tgl');
        $bulan = substr($tgl, 3, 2);
        $tahun = substr($tgl, 6, 4);
        //$fieldmasuk = "QtyMasuk" . $bulan;
        //$fieldakhir = "QtyAkhir" . $bulan;
        //$fieldkeluar = "QtyKeluar" . $bulan;
        $detail = $this->register_model->getPCodeDet($kode, $name);
//                print_r($detail);
        if (!empty($detail)) {
            $nilai = $detail->NamaRekening . "*&^%" . $detail->KdRekening; // . "*&^%" . $detail->HargaBeliAkhir;
        } else {
            $nilai = "";
        }
        echo $nilai;
    }

    function getRealPCode() {
        $kode = $this->input->post('pcode');
        if (strlen($kode) == 13) {
            $mylib = new globallib();
            $hasil = $mylib->findBarcode($kode);
            print_r($hasil);
            die();
            $pcode_hasil = $hasil['nilai'];
            if (count($pcode_hasil) != 0) {
                $pcode = $pcode_hasil[0]['PCode'];
            } else {
                $pcode = "";
            }
        } else {
            $valpcode = $this->register_model->ifPCodeBarcode($kode);
            if (count($valpcode) != 0) {
                $pcode = $valpcode->KdRekening;
            } else {
                $pcode = "";
            }
        }
        echo $pcode;
    }

    function edit_komisi($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->register_model->getHeader($id);
            $data['detail'] = $this->register_model->getDetail($id);

            $this->load->view('transaksi/register/edit', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function cetak() {
        $data = $this->varCetak();
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_register', $data);
    }

    function versistruk() {
        $data = $this->varCetak();
        $no = $this->uri->segment(4);
        $ip_address = $_SERVER['REMOTE_ADDR'];
        $ip = "192.168.0.75";
        $printer = $this->register_model->NamaPrinter($ip);
		//print_r($printer); die();
//        print_r($_SERVER['REMOTE_ADDR']);
//        die();
        $data['ip'] = $printer[0]['ip'];
        $data['nm_printer'] = $printer[0]['nm_printer'];
        $data['store'] = $this->register_model->aplikasi();
		$dataheader		= $this->register_model->getHeader($no);
        $data['header'] = $dataheader;
        $data['detail'] = $this->register_model->getHeaderForPrint($no);

        if (!empty($data['header'])) {
            //$this->load->view('proses/cetak_tutup',$data); // jika untuk tes
			$printqty	= $dataheader->PrintQty;
			$iscopy		= ($printqty>0)?1:0;
			$data['iscopy']	= $iscopy;

			$dataprintqty	= array('statuskomisi'=>0,'PrintQty'	=> $printqty+1);
			$this->db->update('register', $dataprintqty, array('KdRegister' => $no));

            $this->load->view('transaksi/register/cetak_strukregister', $data); // jika ada printernya
        }
    }

    function printThis() {
        $data = $this->varCetak();
        $id = $this->uri->segment(4);
        $data['fileName2'] = "lainlain.sss";
        $data['fontstyle'] = chr(27) . chr(80);
        $data['nfontstyle'] = "";
        $data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
        $data['pindah_hal'] = "\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
        $data['string1'] = "     Dibuat Oleh,                     Disetujui Oleh,";
        $data['string2'] = "(                     )         (                      )";
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_lain', $data);
    }

    function varCetak() {
        $this->load->library('printreportlib');
        $mylib = new globallib();
        $printreport = new printreportlib();
        $id = $this->uri->segment(4);
        $header = $this->register_model->getHeader($id);
        $data['header'] = $header;
        $detail = $this->register_model->getDetailForPrint($id);
        $data['judul1'] = array("No Register", "Tanggal", "Keterangan");
        $data['niljudul1'] = array($header->KdRegister, $header->Tanggal, stripslashes($header->Keterangan));
        $data['judul2'] = "";
        $data['niljudul2'] = "";
        $data['judullap'] = "Register Harian";
        $data['colspan_line'] = 4;
        $data['url'] = "register/printThis/" . $id;
        $data['url2'] = "register/versistruk/" . $id;
        $data['tipe_judul_detail'] = array("normal", "normal", "normal", "kanan");
        $data['judul_detail'] = array("PCode", "Nama Barang", "Qty", "Harga");
        $data['panjang_kertas'] = 30;
        $jmlh_baris_lain = 19;
        $data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
        $jml_baris_detail = count($detail) + $this->register_model->getCountDetail($id);
        if ($data['panjang_per_hal'] == 0) {
            $data['tot_hal'] = 1;
        } else {
            $data['tot_hal'] = ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
        }
        $list_detail = array();
        $detail_attr = array();
        $list_detail_attr = array();
        $detail_page = array();
        $new_array = array();
        $counterBaris = 0;
        $counterRow = 0;
        $max_field_len = array(0, 0, 0);
        $sum_netto = 0;
//                print_r($detail);
        for ($m = 0; $m < count($detail); $m++) {
//			$attr = $this->register_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
            unset($list_detail);
            $counterRow++;
            $list_detail[] = stripslashes($detail[$m]['PCode']);
            $list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
            $list_detail[] = stripslashes($detail[$m]['Qty']);
            $list_detail[] = number_format($detail[$m]['Harga'], 0, '', '.');
            $detail_page[] = $list_detail;
            $max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
            if ($data['panjang_per_hal'] != 0) {
                if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
                    $data['detail'][] = $detail_page;
                    if ($m != count($detail) - 1) {
                        unset($detail_page);
                    }
                }
            }
            $netto = $detail[$m]['Harga'];
            $sum_netto = $sum_netto + ($netto);
        }
        $data['judul_netto'] = array("Total");
        $data['isi_netto'] = array(number_format($sum_netto, 0, '', '.'));
        $data['detail'][] = $detail_page;
        $data['max_field_len'] = $max_field_len;
        $data['banyakBarang'] = $counterRow;
        return $data;
    }

	// =====================================================================

	function cetakprinter($notiket) {
		//------------------------------------------------------------------
		$data['reset']  =chr(27).'@';
		$data['plength']=chr(27).'C';
		$data['lmargin']=chr(27).'l';
		$data['cond']   =chr(15);
		$data['ncond']  =chr(18);
		$data['dwidth'] =chr(27).'!'.chr(24);
		$data['ndwidth']=chr(27).'!'.chr(14);
		$data['draft']  =chr(27).'x'.chr(48);
		$data['nlq']    =chr(27).'x'.chr(49);
		$data['bold']   =chr(27).'E';
		$data['nbold']  =chr(27).'F';
		$data['uline']  =chr(27).'!'.chr(129);
		$data['nuline'] =chr(27).'!'.chr(1);
		$data['dstrik'] =chr(27).'G';
		$data['ndstrik']=chr(27).'H';
		$data['elite']  ='';
		$data['pica']   =chr(27).'P';
		$data['height'] =chr(27).'!'.chr(16);
		$data['nheight']=chr(27).'!'.chr(1);
		$data['spasi05']=chr(27)."3".chr(16);
		$data['spasi1'] =chr(27)."3".chr(24);
		$data['fcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';
		$data['pcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';
		$data['op_cash']=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);
		//-------------------------------------------------------------------

		$header			= $this->register_model->getHeader($notiket);
		$store			= $this->register_model->aplikasi();
		$noregister		= $header->KdRegister;

		$data['namapt']		= $store[0]['NamaPT'];
		$data['alamatpt1']	= $store[0]['Alamat1PT'];
		$data['alamatpt2']	= $store[0]['Alamat2PT'];
		$data['telppt']		= $store[0]['TelpPT'];;
		$data['noreg']		= $noregister;
		$data['member']		= $header->KdTourLeader." - ".substr($header->NamaMember,0,20);
		$data['waktukunj']	= date('d-m-Y', strtotime($header->Tanggal))." ".$header->Jam;
		$data['phone']		= $header->Phone;
		$data['kendaraan']	= $header->Keterangan;
		$data['bankrek']	= $header->bankrek;
		$data['sticker']	= $header->NoStiker;
		$data['rombongan']	= $header->nmTravel;

		$data['jumlah']		= $header->Jumlah;

		//------------------------------------------------------------------
		$printqty			= $header->PrintQty;
		$iscopy				= ($printqty>0)?1:0;
		$data['iscopy']		= $iscopy;

		$dataprintqty		= array('statuskomisi'=>0,'statuskomisi_khusus'=>0, 'PrintQty'	=> $printqty+1);
		$this->db->update('register', $dataprintqty, array('KdRegister' => $noregister));

		//-------------------------------------------------------------------
		$this->load->helper('text');
		$data['nl']	= "\r\n";
		$html		= $this->load->view('transaksi/register/cetak_strukregister_new',$data,TRUE);
		$filename	='tiket_'.$notiket;
		$ext		='ctk';
		header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		header("Content-Transfer-Encoding: binary");
		header('Expires: 0');
		header('Pragma: no-cache');
		print $html;
	}

	function cek_stiker()
    {
    			    $mylib = new globallib();

    			    $nostiker= $this->uri->segment(4);
			        $tgl = $mylib->ubah_tanggal($this->uri->segment(5));

    			    $cek_stiker = $this->register_model->cekStiker($nostiker,$tgl);
    			    
    			    if(count($cek_stiker)>0){
						echo json_encode(array("status" => 1));
					}else{
						$limit_stiker = $this->register_model->cekLimitStiker($nostiker,$tgl);
						
						if(!empty($limit_stiker)){
							date_default_timezone_set('Asia/Jakarta');
	    			    	$waktu_ticket		= strtotime($limit_stiker->Waktu);
	    			    	$waktu_sekarang		= strtotime(date("Y-m-d H:i:s"));  
	    			    	$selisih = $waktu_sekarang-$waktu_ticket;//3600 itu 60 Menit
						}
						
    			    	if(empty($limit_stiker)){
							echo json_encode(array("status" => 0));
						}else{
							if($selisih<=7200){
								echo json_encode(array("status" => 0));
							}else{
								echo json_encode(array("status" => 2));
							}
						}
    			    	
						
					}


    }
    
    
    function bypass_sticker()
    {
    	$user= $this->uri->segment(4);
	    $pass = $this->uri->segment(5);

        $cek_otorisasi=$this->register_model->cek_otorisasi($user);

		if(empty($cek_otorisasi)){
		echo json_encode(array('status'=>'0'));
		}else{
		$validasi_user_pass=$this->register_model->validasi_user($user,$pass);
			if(empty($validasi_user_pass)){
				echo json_encode(array('status'=>'1'));
			}else{
				echo json_encode(array('status'=>'2'));
			}
		}
	}

}
