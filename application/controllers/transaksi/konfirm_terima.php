<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class konfirm_terima extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/konfirm_terimamodel');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            //$gudang_admin = $this->globalmodel->getGudangAdmin($user);

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_divisi"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "konfirm_terima", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudung = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudung[1];
                    $search_divisi = explode("=", $arrSearch[2]); // search divisi
                    $arr_search["search"]["divisi"] = $search_divisi[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudung[1];
                    $data["search_divisi"] = $search_divisi[1];
                    $data["search_status"] = $search_status[1];
                }
            }

            foreach ($gudang_admin as $key) {
                $arr_search["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
            }

            $arr_search["search"]["gudang_admin"] = $arr_search["gudang_admin"];

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/konfirm_terima/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/konfirm_terima/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->konfirm_terimamodel->getGudang($arr_search["gudang_admin"]);
            $data['mdivisi'] = $this->konfirm_terimamodel->getDivisi();

            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->konfirm_terimamodel->num_konfirm_terima_row($arr_search["search"]);
            $data['data'] = $this->konfirm_terimamodel->get_konfirm_terima_List($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/konfirm_terima/konfirm_terimalist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() {
        $mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'konfirm_terima', 'AddUser' => $user));

        $search_value = "";
        $search_value .= "search_keyword=" . $mylib->save_char($this->input->post('search_keyword'));
        $search_value .= "&search_gudang=" . $this->input->post('search_gudang');
        $search_value .= "&search_divisi=" . $this->input->post('search_divisi');
        $search_value .= "&search_status=" . $this->input->post('search_status');

        $data = array(
            'query_string' => $search_value,
            'module' => "konfirm_terima",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/konfirm_terima/index/' . $query_id . '');
    }

    function edit_form($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->konfirm_terimamodel->getHeader($id);
            $data['detail'] = $this->konfirm_terimamodel->getDetail($id);
            $data['mgudang'] = $this->konfirm_terimamodel->getGudang($arr_search["gudang_admin"]);
            $data['supplier'] = $this->konfirm_terimamodel->getSupplier();
         $data['track'] = $mylib->print_track();

            $this->load->view('transaksi/konfirm_terima/form_edit', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
	//save data yang dikirim dari view form_edit.php di klik button edit
    function save_data() {
    	
        //echo "<konfirm_terimamodele>";konfirm_terimamodelint_r($_POST);echo "</konfirm_terimamodele>";die();
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        //echo "Hello";die;
        
        $mylib				= new globallib();
        $v_no_dokumen		= $this->input->post('v_no_dokumen');
        $v_no_po			= $this->input->post('v_no_po');
        $v_tgl_dokumen		= $this->input->post('v_tgl_dokumen');
        $v_est_terima		= $this->input->post('v_est_terima');
        $v_divisi			= $this->input->post('v_divisi');
        $v_gudang			= $this->input->post('v_gudang');
        $v_keterangan		= $this->input->post('v_keterangan');
        $v_status			= $this->input->post('v_status');
        $flag				= $this->input->post('flag');
        $base_url			= $this->input->post('base_url');
        $user				= $this->session->userdata('username');
        $v_grdTotal1		= $this->input->post('grdTotal');		
		$nodokumensupplier	= $this->input->post('nodoksupplier');
		$nofpajak			= $this->input->post('nofpajak');
		$ongkir				= $this->input->post('v_ongkir');
		
        // detail
        $v_pcode1	= $this->input->post('pcode');
        $v_harga1	= $this->input->post('harga');
        $v_ppn1	= $this->input->post('ppn');
        $v_jumlah1	= $this->input->post('jumlah');
        $v_total1	= $this->input->post('total');
        
        $tgl = $mylib->ubah_tanggal($v_tgl_dokumen);
        list($tahun, $bulan, $vtgl) = explode('-',$tgl);
        
        $data['bulan'] = $this->session->userdata('bulanaktif');
        $data['tahun'] = $this->session->userdata('tahunaktif');

        for ($x = 0; $x < count($v_pcode1); $x++) {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $harga = $v_harga1[$x];
            $ppn = $v_ppn1[$x];
            $jumlah = $v_jumlah1[$x];
            $total = $v_total1[$x];

            if ($pcode != "") {
                //if ($v_qty * 1 > 0) {
                   // $detail = $this->permintaan_barangmodel->cekGetDetail($pcode, $v_no_dokumen);

                   // if ($detail->PCode == $pcode) {
                        $this->updateDetail($v_no_dokumen, $pcode, $harga, $ppn, $jumlah,  $total, $user,$tgl);
                    //} else {
                      //  $this->insertDetail($flag, $v_no_dokumen, $pcode, $v_namabarang, $v_qty, $v_satuan);
                    //}
                //}
            }
        }
        
        if ($flag == "edit") {
        	
        	//natura harus diganti dengan db_natura
            $NoFaktur_pembelian = $mylib->get_code_counter($this->db->database, "invoice_pembelian_header","NoFaktur", "PI", $bulan, $tahun);
        	//eksekusi pertama
        	        	
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_grdTotal1, $v_grdTotal1, $flag, $user, $nofpajak, $nodokumensupplier, $v_no_po, $NoFaktur_pembelian, $ongkir);

            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }
		
        redirect('/transaksi/konfirm_terima');
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    function updateHeader($v_no_dokumen,$tgl, $v_grdTotal1, $v_grdTotal1, $flag, $user, $nofpajak, $nodokumensupplier, $v_no_po, $NoFaktur_pembelian, $v_ongkir) {
        $this->konfirm_terimamodel->locktables('trans_terima_header,trans_terima_detail, hutang, mutasihutang');

		//table trans_terima_header
        $data	= array(
					'Jumlah'			=> $v_grdTotal1,
					'Total'				=> $v_grdTotal1,
					'BiayaTransport'	=> $v_ongkir,
					'FlagKonfirmasi'	=> 'Y',
					'EditDate'			=> date('Y-m-d'),
					'EditUser'			=> $user
				);
		
        $this->db->update('trans_terima_header', $data, array('NoDokumen' => $v_no_dokumen));
		
		//ambil kode Supplier dan ppn
		$sql ="SELECT a.`KdSupplier`,a.`PPn` FROM `trans_terima_header` a WHERE a.`NoDokumen`='".$v_no_dokumen."'";
	    $rek = $this->konfirm_terimamodel->Querydata($sql);
		foreach($rek as $v)
		{
		$ppn		=$v['PPn'];	
		$kdsupplier	=$v['KdSupplier'];	
		}

		$tgljt = $this->konfirm_terimamodel->getjatuhtempo($tgl, $kdsupplier);
		$interface = $this->konfirm_terimamodel->getinterface();
		
		//table hutang
		$datahutang	= array(
						'NoDokumen'			=> $NoFaktur_pembelian,
						'NoFaktur'			=> $NoFaktur_pembelian,		//tadinya tidak ada
						'KdSupplier'		=> $kdsupplier,
						'TipeTransaksi'		=> 'I', 			//tadinya Jenis RG jadi TipeTransaksi I
						'Tanggal'			=> $tgl,	//tadinya TglTransaksi
						'JatuhTempo'		=> $tgljt,	//tadinya TglJTo
						'NilaiTransaksi'	=> $v_grdTotal1,	//TotalHutang
						'Sisa'				=> $v_grdTotal1,	//TotalBayar
						'Kurs'				=> 1, 				//tadinya Taxrate
						'MataUang'			=> 'IDR', 			//tadinya KdMataUang
						'RGNo'				=> $v_no_dokumen,
						'PONo'				=> $v_no_po,
						'KdRekening'        => $interface[0]['HutangDagang']
					);
													
        $this->db->insert('hutang', $datahutang);
		
		//table mutasi_hutang
		list($tahun, $bulan, $tg) = explode('-', $tgl);
		$datamutasihutang	= array(
								'Tahun'				=> $tahun,
								'Bulan'				=> $bulan,
								'Tanggal'			=> $tgl,
								'NoDokumen'			=> $NoFaktur_pembelian,	//tadinya NoTransaksi
								'KdSupplier'		=> $kdsupplier,			//tadinya tidak ada
								'MataUang'			=> 'IDR',				//tadinya KdMataUang
								'Kurs'				=> 1, 					//tadinya Taxrate
								'Jumlah'			=> $v_grdTotal1,    	//tadinya NilaiTransaksi
								'SelisihKurs'		=> 0, 					//selisih kurs = 0 yang tadinya 1
								'TipeTransaksi'		=> 'I', 					//tadinya Jenis RG jadi TipeTransaksi I						
								'KdRekening'        => $interface[0]['HutangDagang']
							);					
							
        $this->db->insert('mutasi_hutang', $datamutasihutang);
    		
		//table invoice_pembelian_header
		$datapembelianheader	= array(
								'NoFaktur'				=> $NoFaktur_pembelian,	
								'Tanggal'				=> $tgl,
								'JatuhTempo'			=> $tgljt,
								'NoPO'					=> $v_no_po,
								'NoPenerimaan'			=> $v_no_dokumen,	
								'NoFakturSupplier'		=> $nodokumensupplier,
								'NoFakturPajak'		    => $nofpajak,
								'KdSupplier'		    => $kdsupplier,
								'MataUang'				=> 'IDR',
								'Kurs'					=> 1,
								'PPN'					=> $ppn,
								'BiayaTransport'		=> $v_ongkir,
								'AddDate'				=> date('Y-m-d'),
								'AddUser'				=> $user,
								'EditDate'				=> date('Y-m-d'),
								'EditUser'				=> $user				
							);	
				
		$this->db->insert('invoice_pembelian_header', $datapembelianheader);
		
		//table invoice_pembelian _detail
		$sql2="SELECT trans_terima_detail.*, NamaLengkap FROM trans_terima_detail LEFT JOIN masterbarang ON masterbarang.PCode = trans_terima_detail.PCode 
               WHERE 1 AND trans_terima_detail.NoDokumen = '".$v_no_dokumen."' ORDER BY trans_terima_detail.sid DESC";
		$rek2 = $this->konfirm_terimamodel->Querydata($sql2);		
		$no=1;
			foreach($rek2 as $v){
				$counter     	=$no;
				$kdbarang		=$v['PCode'];
				$Qty 			=$v['Qty'];
				$Satuan			=$v['Satuan'];
				$Harga			=$v['Harga'];
				$PPN			=$v['PPn'];
				
				$data_invoice_pembelian_detail=array(
				    'NoFaktur'				=> $NoFaktur_pembelian,
				    'NoUrut'				=> $counter,
				    'KdBarang'				=> $kdbarang,
				    'Qty'					=> $Qty,
				    'Satuan'				=> $Satuan,
				    'Harga'					=> $Harga,		
				    'PPN'					=> $PPN
				);
				
				/**
				echo "<pre>";
		        print_r($data_invoice_pembelian_detail);
		        echo "</pre>";die;
				**/
				$this->db->insert('invoice_pembelian_detail', $data_invoice_pembelian_detail);
				$no=$no+1;	
			}
		
        $this->konfirm_terimamodel->unlocktables();
    }

    function updateDetail($v_no_dokumen, $pcode, $harga, $ppn, $jumlah, $total,$user,$tgl) {
        $mylib = new globallib();

        //$new_qty = $mylib->save_int($qty) + $mylib->save_int($qty_tbl);

        $this->konfirm_terimamodel->locktables('trans_terima_detail,mutasi');

        if ($pcode) {
            $data = array(
                'Harga'				=> $harga,
                'Jumlah'			=> $jumlah,
                'PPn'				=> $ppn,
                'Total'				=> $total,
                'FlagKonfirmasi'	=> 'Y',
                'EditDate'			=> date('Y-m-d'),
                'EditUser'			=> $user
            );
            $this->db->update('trans_terima_detail', $data, array('NoDokumen' => $v_no_dokumen, 'PCode' => $pcode));
			
			$datamutasi = array(
							'Nilai'	=> $jumlah
						);
			$this->db->update('mutasi', $datamutasi, array('NoTransaksi' => $v_no_dokumen, 'KodeBarang' => $pcode));
			
			$fieldgudangmasuk	= 'GNMasuk'.date(m);
			$datastock = array(
							$fieldgudangmasuk	=> $jumlah
						);			
			//$this->db->update('stock', $datastock, array('Tahun' => date('Y'), 'KdGudang' => '', 'PCode'=> $pcode));
        }

        $this->konfirm_terimamodel->unlocktables();
    }

    function update_data_faktur() {
        $mylib = new globallib();
        $v_no_dokumenf		= $this->input->post('v_no_dokumenf');
        $v_no_pof			= $this->input->post('v_no_pof');
        $v_tgl_dokumenf		= $this->input->post('v_tgl_dokumenf');
        $v_FlagKonfirmasif	= $this->input->post('v_FlagKonfirmasif');
        $v_EditUserf	    = $this->input->post('v_EditUserf');
        $nofpajakf			= $this->input->post('nofpajakf');
        $dataedit           = date('Y-m-d');
        $tgleditf           = $mylib->ubah_tanggal($dataedit);
        $v_tgl_dokumenfa    = $mylib->ubah_tanggal($v_tgl_dokumenf);

        //$this->konfirm_terimamodel->locktables('invoice_pembelian_header');

        //if ($v_FlagKonfirmasif == 'Y') {
            /*$dataf = array(
                'NoFakturPajak'		=> $nofpajakf,
                'EditDate'			=> date('Y-m-d'),
                'EditUser'			=> $v_EditUserf
            );
            print_r($v_no_dokumenf);
            $this->db->update('invoice_pembelian_header', $dataf, array('NoPenerimaan' => $v_no_dokumenf, 'NoPO' => $v_no_pof, 'Tanggal' => $v_tgl_dokumenf));
            
            $sqlfaktur = "Update invoice_pembelian_header set NoFakturPajak = '$nofpajakf', EditDate = '$dataedit', EditUser = '$v_EditUserf'  where NoPenerimaan = '$v_no_dokumenf' AND NoPO = '$v_no_pof' AND Tanggal = '$v_tgl_dokumenfa' ";
            $qryfaktur = $this->db->query($sqlfaktur);            
            
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumenf . '</strong> berhasil', 'class' => 'success'));
            */
            $this->konfirm_terimamodel->updatefakturpajak($v_no_dokumenf,$v_no_pof,$v_tgl_dokumenfa,$v_EditUserf,$nofpajakf,$tgleditf);	
        //}

        //$this->konfirm_terimamodel->unlocktables();

        redirect('/transaksi/konfirm_terima');
    }
}

?>