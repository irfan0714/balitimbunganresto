<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Adjus_hargabeli extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/adjus_model');
    }
	//harga beli untuk paket di masterbarang
    function index() 
    {
     //pertama ambil dimasterbarang yang tiprnya PKT
     $brg = $this->adjus_model->getMPcodePkt();
     
		 
		 foreach($brg as $v){
			 $DPcode = $this->adjus_model->getDPcode($v['PCode']);
			 //print_r($DPcode);die;
			 $tot = 0;
			 foreach($DPcode AS $k){
				 $HrgBeli = $this->adjus_model->getHargaBeli($k['DPcode']);
				 $tot += $HrgBeli->HargaBeli * $k['Qty'];
			 }
			 $total = $tot;
			 //echo $total;die;
			 $this->db->update('masterbarang',array('HargaBeli'=>$total),array('PCode'=>$v['PCode']));
		 }
	echo "DONE";die; 
    } 
    
    
    
    
    
    //update paket dimasterbarang
    function jual_paket() 
    {
     $MPcode = $this->adjus_model->getMPcode();
	 //echo "<pre>";print_r($MPcode);echo "</pre>";die;
	 
	 foreach($MPcode as $v){
		 $DPcode = $this->adjus_model->getDPcode($v['MPCode']);
		 if($v['MPCode']=="013720002"){
		 //echo "<pre>";print_r($DPcode);echo "</pre>";die;
		 }
		$tot = 0;
		 foreach($DPcode AS $k){
			 $HrgBelis = $this->adjus_model->getHargaJual($k['DPcode']);
			 $tot += $HrgBelis->Harga1c * $k['Qty'];
		 }
		 $total = $tot;
		 //echo $total;die;
		 $this->db->update('masterbarang',array('Harga1c'=>$total),array('PCode'=>$v['MPCode']));
	 }
    }
    
     //update paket dimasterbarang
    function hargabeli_transaksi() 
    {
     //Get PCode di transaksi_detail
     $Pcode = $this->adjus_model->getPCodeTransaksi();
	 
		
		 foreach($Pcode AS $k){
			 $HrgBeli = $this->adjus_model->getHargaBeli($k['PCode']);
			 $this->db->update('transaksi_detail',array('HargaBeli'=>$HrgBeli->HargaBeli),array('PCode'=>$k['PCode']));
		 }
		
		 
	 
    }
    
    function hargabeli_penerimaan() 
    {
     //Get PCode di penerimaan
     $Pcode = $this->adjus_model->getPCodePenerimaan();
	 
		
		 foreach($Pcode AS $k){
			 $HrgBeli = $this->adjus_model->getHargaBeli($k['PCode']);
			 $this->db->update('trans_penerimaan_lain_detail',array('Harga'=>$HrgBeli->HargaBeli),array('PCode'=>$k['PCode']));
		 }
		
	echo "DONE!";die();	 
	 
    }
  
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'delivery_order', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "delivery_order",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/delivery_order/index/' . $query_id . '');
    }

    function add_new() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			$data['gudang'] = $this->delivery_order_model->getGudang();
            $data['customer'] = $this->delivery_order_model->getCustomer();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/add_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_delivery_order($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);			
            $data['header'] = $this->delivery_order_model->getHeader($id);
            $data['gudang'] = $this->delivery_order_model->getGudang();			
            $data['customer'] = $this->delivery_order_model->getCustomer();
            $data["detail_list"] = $this->delivery_order_model->getDetailList($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/edit_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function view_delivery_order($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['header'] = $this->delivery_order_model->getHeader($id);
            $data['customer'] = $this->delivery_order_model->getCustomer();
            $data["detail_list"] = $this->delivery_order_model->getDetailList($id);
            $data['msatuan'] = $this->delivery_order_model->getSatuan();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/delivery_order/view_delivery_order', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_warehouse = $this->input->post('v_gudang');
        $v_customer = $this->input->post('v_customer');
        $v_contact_person = $this->input->post('v_contactperson');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
		$pisah_periode					= explode("-",$v_tgl_dokumen);
		$hri_periode					= $pisah_periode[0];
		$bln_periode					= $pisah_periode[1];
		$thn_periode					= $pisah_periode[2];
		
			  //cari stock
			  if($bln_periode=='01'){
			$tabel_field='GKeluar01';
		}else if($bln_periode=='02'){
			$tabel_field='GKeluar02';
		}else if($bln_periode=='03'){
			$tabel_field='GKeluar03';
		}else if($bln_periode=='04'){
			$tabel_field='GKeluar04';
		}else if($bln_periode=='05'){
			$tabel_field='GKeluar05';
		}else if($bln_periode=='06'){
			$tabel_field='GKeluar06';
		}else if($bln_periode=='07'){
			$tabel_field='GKeluar07';
		}else if($bln_periode=='08'){
			$tabel_field='GKeluar08';
		}else if($bln_periode=='09'){
			$tabel_field='GKeluar09';
		}else if($bln_periode=='10'){
			$tabel_field='GKeluar10';
		}else if($bln_periode=='11'){
			$tabel_field='GKeluar11';
		}else if($bln_periode=='12'){
			$tabel_field='GKeluar12';
		}
		
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        // detail
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_qty1 = $this->input->post('v_qty');
        $v_satuan1 = $this->input->post('v_satuan');
        
        //echo $market_pcode1." - ".$market_qty1." - ".$market_satuan1;die;

        if ($flag == "add")
		{
		    //pertama generate No Dokument di Delivery Order
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "deliveryorder","dono", "DO", $data['bulan'], $data['tahun']);
			
			//kedua masukkan di Delivery Order Header
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_contact_person, $v_warehouse, $v_note, $user, $v_customer);
        
			
			for ($x = 0; $x < count($v_pcode1); $x++) 
			{
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];	
			
			//ketiga masukkan di Delivery Order Detail
			//$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$v_satuan,$user);
			
			//cocokan konversi terlebih dahulu
				$konversi = $this->delivery_order_model->getKonversi($pcode,$v_satuan);
				
				//jika konversi ada datanya
				if(!empty($konversi)){
					//jika Satuan_To sama dengan Satuan yang dipilih
					if($konversi->Satuan_To==$v_satuan){
						$QtyPcs = $v_qty;
					//jika tidak sama
					}else{
						$QtyPcs = $v_qty * $konversi->amount;
					}
				//jika tidak ada datanya konversi
				}else{
					$QtyPcs = $v_qty;
				}
			
			$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$user, $v_satuan, $QtyPcs);

			//keempat Masukkan di Mutasi
			$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
			
			//kelima masukkan di stock GMasuk[bulan]
			//a. cek dulu apakah di stok udah ada? kalau belum ada insert kalau udah ada update
			$stock = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
			//print_r($stock);die;
			        if($stock->Tahun==$thn_periode and $stock->KdGudang==$v_warehouse and  $stock->PCode==$pcode )
					{
						$this->updateStock($thn_periode,$v_warehouse,$pcode,$stock->$tabel_field,$v_qty,$tabel_field);
					}
					else
					{
						
						$this->insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field);
					}
			
			}			
		} 
		
		
		else if ($flag == "edit") 
		{
			//ambil post $v_no_dokumen
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			
			//update deliveyorder
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_contact_person, $v_warehouse, $v_note, $v_status, $user, $v_customer);
            
				        // ubah detail
				        for ($x = 0; $x < count($v_pcode1); $x++) 
				        {
				        	$v_no_dokumen = $this->input->post('v_no_dokumen');
				            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
							$v_namabarang = trim($v_namabarang1[$x]);
							$v_qty = $mylib->save_int($v_qty1[$x]);
							$v_satuan = $v_satuan1[$x];

				            if ($pcode != "") 
				            {
				            	if($v_qty*1>0)
				            	{   
				            	    
									//cek mutasi
									$mutasi = $this->delivery_order_model->cekGetMutasi($v_no_dokumen,$v_warehouse,$mylib->ubah_tanggal($v_tgl_dokumen),$pcode);
							        
							        if($mutasi->NoTransaksi==$v_no_dokumen and $mutasi->KdTransaksi=='R' and $mutasi->Gudang==$v_warehouse and  $mutasi->Tanggal==$mylib->ubah_tanggal($v_tgl_dokumen) and $mutasi->KodeBarang==$pcode )
									{
										$this->updateMutasi($v_no_dokumen,$v_warehouse,$mylib->ubah_tanggal($v_tgl_dokumen),$pcode,$v_qty);
									}
									else
									{
										$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
									}
									
									//ambil qty dari deliveryorderdetail
									$detail2 = $this->delivery_order_model->cekGetDetail($market_pcode,$v_no_dokumen);
									
									//ambil GKeluar di stock
									$stock2 = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
						
									$this->updateStock2($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$v_qty,$tabel_field);
									
									
									//cek delivery order detail
				            		$detail = $this->delivery_order_model->cekGetDetail($pcode,$v_no_dokumen);
									
									//cocokan konversi terlebih dahulu
									$konversi = $this->delivery_order_model->getKonversi($pcode,$v_satuan);
									
									//jika konversi ada datanya
									if(!empty($konversi)){
										//jika Satuan_To sama dengan Satuan yang dipilih
										if($konversi->Satuan_To==$v_satuan){
											$QtyPcs = $v_qty;
										//jika tidak sama
										}else{
											$QtyPcs = $v_qty * $konversi->amount;
										}
									//jika tidak ada datanya konversi
									}else{
										$QtyPcs = $v_qty;
									}
				            		
				            		if($detail->inventorycode==$pcode)
									{
										$this->updateDetail($flag, $v_no_dokumen, $pcode, $v_qty, $market_satuan,$detail->Qty,$user, $v_satuan, $QtyPcs);
									}
									else
									{
										$this->insertDetail($flag, $v_no_dokumen, $pcode, $v_qty, $v_satuan, $user , $v_satuan, $QtyPcs);
									}
									
									
								}
				            }
				        }
				        
				        
			//update di deliveyorder dan deliveryorderdetail
			for ($x = 0; $x < count($v_pcode1); $x++) 
	        {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];

            if ($pcode != "") 
            {
            	if($v_qty*1>0)
            	{
            		//cek apakah sudah ada di deliveryorder
            		$detail = $this->delivery_order_model->cekGetDetail($pcode,$v_no_dokumen);
            		
            		if($detail->inventorycode==$pcode)
					{
						//update deliveryorderdetail
						$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_qty,$detail->Qty, $user, $v_satuan, $QtyPcs);
					}
					else
					{
						//insert jika itu record baru
						$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_qty,$user , $v_satuan, $QtyPcs);
					}				
				}
            }
			
			//keempat Masukkan di Mutasi
			$this->insertMutasi($v_no_dokumen, $v_warehouse, $mylib->ubah_tanggal($v_tgl_dokumen), $pcode, $v_qty, $user, $v_note);
			
			//kelima masukkan di stock GMasuk[bulan]
			//a. cek dulu apakah di stok udah ada? kalau belum ada insert kalau udah ada update
			$stock = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);
			
			        if($stock->Tahun==$thn_periode and $stock->KdGudang==$v_warehouse and  $stock->PCode==$pcode )
					{
						$this->updateStock($thn_periode,$v_warehouse,$pcode,$stock->$tabel_field,$v_qty,$tabel_field);
					}
					else
					{
						
						$this->insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field);
					}
            		
        }
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/delivery_order/edit_delivery_order/' . $v_no_dokumen . '');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $v_contact_person, $v_warehouse, $v_note, $user, $v_customer) 
    {
        $this->delivery_order_model->locktables('deliveryorder');

        $data = array(
            'dono' => $no_dokumen,
            'dodate' => $tgl_dokumen,
            'contactperson' => $v_contact_person,
            'warehousecode' => $v_warehouse,
            'status' => 0,
            'note' => $v_note,
            'adddate'=>date('Y-m-d'),
            'adduser' => $user,
            'customerid' => $v_customer
        );

        $this->db->insert('deliveryorder', $data);

        $this->delivery_order_model->unlocktables();

    }
    
  
    function updateHeader($no_dokumen, $tgl_dokumen, $v_contact_person, $v_warehouse, $v_note, $v_status,$user, $v_customer) 
    {
        $this->delivery_order_model->locktables('deliveryorder');

        $data = array(
            'dodate' => $tgl_dokumen,
            'contactperson' => $v_contact_person,
            'warehousecode'=>$v_warehouse,
			'status'=>$v_status,
            'note' => $v_note,
            'editdate' => date('Y-m-d'),
            'edituser' => $user,
            'customerid' => $v_customer
        );

        $this->db->update('deliveryorder', $data, array('dono' => $no_dokumen));
        
        $this->delivery_order_model->unlocktables();
    }

    function insertDetail($flag, $no_dokumen, $pcode, $qty, $user , $v_satuan, $QtyPcs) 
    {
        $this->delivery_order_model->locktables('deliveryorderdetail');

        if ($pcode) {
            $data = array(
                'dono' => $no_dokumen,
                'inventorycode' => $pcode,
                'quantity' => $qty,
				'satuan'=>$v_satuan,
				'QtyPcs'=>$QtyPcs,
                'adddate'=>date('Y-m-d'),
                'adduser'=>$user
            );

            $this->db->insert('deliveryorderdetail', $data);
        }

        $this->delivery_order_model->unlocktables();
    }
	
	function insertStock($thn_periode,$v_warehouse,$pcode,$v_qty,$tabel_field) 
    {
        $this->delivery_order_model->locktables('stock');
        if ($pcode) {
            $data = array(
                'Tahun' => $thn_periode,
                'KdGudang' => $v_warehouse,
                'PCode' => $pcode,
                $tabel_field=>$v_qty
            );

            $this->db->insert('stock', $data);
        }

        $this->delivery_order_model->unlocktables();
    }
   	
	function insertMutasi($v_no_dokumen, $v_warehouse, $v_tgl_dokumen, $pcode, $v_qty, $user, $v_note)
    {
        $this->delivery_order_model->locktables('mutasi');

        if ($pcode) {
            $data = array(
                'NoTransaksi' => $v_no_dokumen,
                'Jenis' =>'I',
				'KdTransaksi' => 'R',
				'Gudang' => $v_warehouse,
				'Tanggal'=>$v_tgl_dokumen,
				'KodeBarang'=>$pcode,
				'Qty'=>$v_qty,
				'Status'=>1,
                'Kasir'=>$user,
				'Keterangan'=> $v_note
            );

            $this->db->insert('mutasi', $data);
        }

        $this->delivery_order_model->unlocktables();
    }
	
    function updateDetail($flag,$no_dokumen,$pcode,$qty,$qty_tbl,$user, $satuan, $QtyPcs)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);
    	 
        $this->delivery_order_model->locktables('deliveryorderdetail');

        if ($pcode) 
        {
            $data = array(
                'quantity' => $new_qty,
				'satuan'=>$satuan,
				'QtyPcs'=>$QtyPcs,
                'editdate'=>date('Y-m-d'),
                'edituser'=>$user
            );
            
            $this->db->update('deliveryorderdetail', $data, array('dono' => $no_dokumen,'inventorycode' => $pcode));
        } 
        
        $this->delivery_order_model->unlocktables();
    }
	
	function updateStock($thn_periode,$v_warehouse,$pcode,$jml_stock,$v_qty,$tabel_field)
    {
    	$jml_tambah= $jml_stock + $v_qty;
        $this->delivery_order_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_tambah
            );
            
            $this->db->update('stock', $data, array('Tahun' => $thn_periode,'KdGudang' => $v_warehouse,'PCode' => $pcode));
        } 
        
        $this->delivery_order_model->unlocktables();
    }
    
    function updateStock2($thn_periode,$v_warehouse,$market_pcode,$detail_quantity,$stock_tabel_field,$market_qty,$tabel_field)
    {
    	$jml_update=($stock_tabel_field-$detail_quantity)+$market_qty;
        $this->delivery_order_model->locktables('stock');

        if ($market_pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $market_pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->delivery_order_model->unlocktables();
    }
    
    function updateMutasi($v_no_dokumen,$v_warehouse,$v_tgl_dokumen,$market_pcode,$market_qty)
    {
    	$this->delivery_order_model->locktables('mutasi');

        if ($market_pcode) 
        {
            $data = array(
                'Qty'=>$market_qty
            );
            
            $where= array(
            'NoTransaksi'=>$v_no_dokumen,
            'Jenis'=>'I',
            'KdTransaksi'=>'R',
            'Gudang'=>$v_warehouse,
            'Tanggal'=>$v_tgl_dokumen,
            'KodeBarang'=>$market_pcode            
            );
            
            $this->db->update('mutasi', $data, $where);
        } 
        
        $this->delivery_order_model->unlocktables();
    }
	
	function delete_trans($id) 
    {
	        
			//stock
			//cek
			$detail2 = $this->delivery_order_model->cekGetDetail3($id);
			
			foreach($detail2 as $val){
			$sid = $val['sid'];
			$nodok=  $val['dono'];
			$quantity = $val['quantity'];
			$pcode=  $val['inventorycode'];
			$warehousecode = $val['warehousecode'];
			$adddate = $val['adddate'];
			
			$detail2 = $this->delivery_order_model->cekGetDetail2($pcode,$nodok);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  if($bln_periode=='01'){
				$tabel_field='GKeluar01';
			}else if($bln_periode=='02'){
				$tabel_field='GKeluar02';
			}else if($bln_periode=='03'){
				$tabel_field='GKeluar03';
			}else if($bln_periode=='04'){
				$tabel_field='GKeluar04';
			}else if($bln_periode=='05'){
				$tabel_field='GKeluar05';
			}else if($bln_periode=='06'){
				$tabel_field='GKeluar06';
			}else if($bln_periode=='07'){
				$tabel_field='GKeluar07';
			}else if($bln_periode=='08'){
				$tabel_field='GKeluar08';
			}else if($bln_periode=='09'){
				$tabel_field='GKeluar09';
			}else if($bln_periode=='10'){
				$tabel_field='GKeluar10';
			}else if($bln_periode=='11'){
				$tabel_field='GKeluar11';
			}else if($bln_periode=='12'){
				$tabel_field='GKeluar12';
			}			
			//ambil GKeluar di stock
			$stock2 = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			}
			
			//mutasi
			$detail3 = $this->delivery_order_model->cekGetDetail3($id);
			foreach($detail3 as $val){
			$sid = $val['sid'];
			$nodok=  $val['dono'];
			$quantity = $val['quantity'];
			$pcode=  $val['inventorycode'];
			$warehousecode = $val['warehousecode'];
			$adddate = $val['adddate'];
			$this->db->delete('mutasi', array('Tanggal' => $adddate, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));			
			}
			
			//deliveryorderdetail
            $this->db->delete('deliveryorderdetail', array('dono' => $id));	

			//deliveryorder
            $this->db->delete('deliveryorder', array('dono' => $id));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/delivery_order/');
    }

    function delete_detail() 
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);
		
        //pertama harus kurangi angka di stock
		 //ambil qty dari deliveryorderdetail
			$detail2 = $this->delivery_order_model->cekGetDetail2($pcode,$nodok);
			$v_warehouse = $detail2->warehousecode;
			$v_tgl_dokumen = $detail2->adddate;
            $pisah_periode					= explode("-",$v_tgl_dokumen);
			$thn_periode					= $pisah_periode[0];
			$bln_periode					= $pisah_periode[1];
			$hri_periode					= $pisah_periode[2];
			//echo $thn_periode;die;
				  //cari stock
				  if($bln_periode=='01'){
				$tabel_field='GKeluar01';
			}else if($bln_periode=='02'){
				$tabel_field='GKeluar02';
			}else if($bln_periode=='03'){
				$tabel_field='GKeluar03';
			}else if($bln_periode=='04'){
				$tabel_field='GKeluar04';
			}else if($bln_periode=='05'){
				$tabel_field='GKeluar05';
			}else if($bln_periode=='06'){
				$tabel_field='GKeluar06';
			}else if($bln_periode=='07'){
				$tabel_field='GKeluar07';
			}else if($bln_periode=='08'){
				$tabel_field='GKeluar08';
			}else if($bln_periode=='09'){
				$tabel_field='GKeluar09';
			}else if($bln_periode=='10'){
				$tabel_field='GKeluar10';
			}else if($bln_periode=='11'){
				$tabel_field='GKeluar11';
			}else if($bln_periode=='12'){
				$tabel_field='GKeluar12';
			}			
			//ambil GKeluar di stock
			$stock2 = $this->delivery_order_model->cekGetStock($thn_periode,$v_warehouse,$pcode,$tabel_field);						
			$this->updateStock3($thn_periode,$v_warehouse,$pcode,$detail2->quantity,$stock2->$tabel_field,$tabel_field);
			
		   //hapus dimutasi
			$this->db->delete('mutasi', array('Tanggal' => $v_tgl_dokumen, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
			
		    //baru hapus di deliveryorder
			$this->db->delete('deliveryorderdetail', array('sid' => $detail2->sid, 'inventorycode' => $pcode, 'dono' => $nodok));
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/delivery_order/edit_delivery_order/' . $nodok . '');
    }           

	function updateStock3($thn_periode,$v_warehouse,$pcode,$detail_quantity,$stock_tabel_field,$tabel_field)
    {
    	$jml_update=$stock_tabel_field-$detail_quantity;
        $this->delivery_order_model->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $tabel_field=>$jml_update
            );
            $where = array
		            (
		            'Tahun' => $thn_periode,
		            'KdGudang' => $v_warehouse,
		            'PCode' => $pcode
		            ); 
            $this->db->update('stock', $data,$where );
        } 
        
        $this->delivery_order_model->unlocktables();
    }
	
	function satuan()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->delivery_order_model->getSatuanDetail($pcode);
     
     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }     
    }
    
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "S U R A T   J A L A N";
		$data["header"] 	= $this->delivery_order_model->getHeader($nodok);
		$data["detail"] 	= $this->delivery_order_model->getDetail_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_do', $data);
	}
	
	
	function doPrint()
	{
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    
	    $jml_detail  = 8;
	    $ourFileName = "delivery-order.txt";
		
		$header = $this->delivery_order_model->getHeader($nodok);
		$detail = $this->delivery_order_model->getDetail_cetak($nodok);
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["inventorycode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
			$arr_data["detail_namasatuan"][$counter] = $val["NamaSatuan"];
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "S U R A T  J A L A N";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->dono)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->dono;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
            	// ----------------------------------------------------
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->adddate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->adddate));$i++)
                {
                    $echo.=" ";
                }
                // -----------------------------------------------------
                
                // -----------------------------------------------------
                $echo.=$arr_epson["cond"]."Gudang";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Keterangan; 
                
                $echo.="\r\n";  
                // -----------------------------------------------------  
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Kepada";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Kepada"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                
                
                $echo.="\r\n";
                
                $echo.=$arr_epson["cond"]."Alamat";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Alamat"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Alamat;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Alamat));$i++)
                {
                    $echo.=" ";
                }
                $echo.="\r\n";             
                   
            }          
           
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
				$namasatuan = $arr_data["detail_namasatuan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namasatuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($namasatuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->note;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->dono,"delivery-order");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->dono,"delivery-order");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "delivery-order",
		            'noreferensi' => $header->dono,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='delivery-order-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	
	function samainso($nodok_so,$gudang){
		$datafisik = $this->adjus_model->getDataSO($nodok_so);
		
		foreach($datafisik AS $val){
			
			$pcode = $val['PCode'];
			$qtyfisik = $val['QtyFisik'];
		
			$this->db->update('stock',array('GAkhir02'=>$qtyfisik,'GAwal03'=>$qtyfisik),array('PCode'=>$pcode,'KdGudang'=>$gudang,'Tahun'=>'2018'));
		}
		
		echo "DONE";die;
	}
	
		
}

?>