<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class pelunasan_piutang extends authcontroller {

    function __construct() {
        parent::__construct();
//        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/pelunasan_piutangmodel');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
                
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "pelunasan_piutang", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                }
            }
            
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/pelunasan_piutang/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/pelunasan_piutang/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->pelunasan_piutangmodel->num_pelunasan_piutang_row($arr_search["search"]);
            $data['data'] = $this->pelunasan_piutangmodel->getPelunasanHutangList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('transaksi/pelunasan_piutang/pelunasan_piutang_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'pelunasan_piutang', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pelunasan_piutang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pelunasan_piutang/index/' . $query_id . '');
    }

    function add_new() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $data['customer'] = $this->pelunasan_piutangmodel->getCustomer();
            $data['matauang'] = $this->pelunasan_piutangmodel->getMataUang();
            $data['kasbank'] = $this->pelunasan_piutangmodel->getKasBank();
            $data['rekeningpph'] = $this->pelunasan_piutangmodel->getRekeningPPH();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pelunasan_piutang/add_pelunasan_piutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
     function view($notransaksi) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            $header = $this->pelunasan_piutangmodel->getDataHeader($notransaksi);
            $data['data'] = $header;
            
            $data['rekeningpph'] = $this->pelunasan_piutangmodel->getRekeningPPH();
            $data['row'] = $this->pelunasan_piutangmodel->getDataDetail($notransaksi);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/pelunasan_piutang/view_pelunasan_piutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
	
	function getDetail(){
		$data = $this->input->post('data');
		$type = $this->input->post('type');
		
		$KdCustomer = $data[0];
		$type = $type[0];
		//echo 'aa '.$type;
		$row = $this->pelunasan_piutangmodel->getDetail($KdCustomer, $type);
        $data['rekeningpph'] = $this->pelunasan_piutangmodel->getRekeningPPH();
		
		$data['num'] = count($row);
		$data['row'] = $row;
		 	
		$this->load->view('transaksi/pelunasan_piutang/pelunasan_piutang_detail', $data);	
	}

   
    function save_data() 
    {
        $mylib = new globallib();
        $tgl = $mylib->ubah_tanggal($this->input->post('tgldokumen'));
        $kdkasbank = $this->input->post('KdKasBank');
        
        $nobukti = $this->input->post('NoBukti');
        $kdcustomer = $this->input->post('KdCustomer');
        $keterangan = $this->input->post('keterangan');
        $type = $this->input->post('v_type');
        $jenis = $this->input->post('v_jenis');
        $cnno = $this->input->post('v_cnno');
        $biayaadmin = $this->input->post('vbiayaadmin');
        $pembulatan = $this->input->post('vpembulatan');
        $pph = $this->input->post('vpph');
        $kdrekeningpph = $this->input->post('vkdrekeningpph');
        //echo 'a '.$jenis;die();
        // detail
        $nofaktur = $this->input->post('NoFaktur');
        $sisa = $this->input->post('vSisa');
        $bayar = $this->input->post('Bayar');
        
        $user = $this->session->userdata('username');

        $notransaksi = $this->pelunasan_piutangmodel->getNoTransaksi($tgl);
        if($jenis=='B')
			$nopv = $this->pelunasan_piutangmodel->getNoRV($kdkasbank,$tgl);	
		else
			$nopv = '';

        $this->pelunasan_piutangmodel->insertHeader($notransaksi, $tgl, $kdkasbank, $nobukti, $kdcustomer, $keterangan, $user, $nopv, $jenis, $cnno, $biayaadmin, $pembulatan, $pph, $kdrekeningpph);
		$total = $this->pelunasan_piutangmodel->insertDetail($notransaksi,$nofaktur,$sisa,$bayar);
		$this->pelunasan_piutangmodel->insertPiutang($tgl,$notransaksi, $nofaktur, $kdcustomer,$bayar,$jenis);	
		$this->pelunasan_piutangmodel->UpdatePiutang($nofaktur,$bayar);	
		$this->pelunasan_piutangmodel->insertMutasiPiutang($tgl,$notransaksi, $kdcustomer,$total, $jenis);	
		if($jenis=='B'){
			$this->pelunasan_piutangmodel->insertRVHeader($nopv,$tgl,$kdkasbank,$kdcustomer,$keterangan,$total,$nobukti,$user,$type,$biayaadmin,$pembulatan,$pph);
			$this->pelunasan_piutangmodel->insertRVDetail($nopv,$tgl,$kdcustomer,$bayar,$nobukti, $nofaktur, $user,$type,$biayaadmin, $pembulatan, $pph,$kdrekeningpph);	
		}else{
			$this->pelunasan_piutangmodel->updatecn($cnno,$total);
			$this->pelunasan_piutangmodel->UpdatePiutangCN($cnno,$total);	
		}
		
		redirect('/transaksi/pelunasan_piutang');
    }
    
    function ajax_customer(){
		$type = $this->input->post('Type');
		if($type==1){
			$rows = $this->pelunasan_piutangmodel->getCustomer();
		}else{
			$rows = $this->pelunasan_piutangmodel->getTravel();
		}
		$select = '<select class="form-control-new" name="KdCustomer" id="KdCustomer" onchange="getCN;getDetail()">';
		foreach($rows as $row){
			$select .= "<option value='".$row['KdCustomer']."'>".$row['Nama']."</option>";
		}
		$select .= "</select>";
		
		echo $select;
		
	}
	
	function getCN(){
		$kdcustomer = $this->input->post('Customer');
		$rows = $this->pelunasan_piutangmodel->getCN($kdcustomer);
		$select = '<select class="form-control-new" name="v_cnno" id="v_cnno" onchange="getNilaiCN()">';
		$select .= "<option value=''>--Pilih CN--</option>";
		foreach($rows as $row){
			$select .= "<option value='".$row['CNNo']."'>".$row['CNNo'].' - '.$row['Nilai']."</option>";
		}
		$select .= "</select>";
		
		echo $select;
	}
	
	function getNilaiCN(){
		$cnno = $this->input->post('creditno');
		$row = $this->pelunasan_piutangmodel->getNilaiCN($cnno);
		
		echo $row[0]['Nilai'];
	}
}

?>