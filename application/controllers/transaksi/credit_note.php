<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class credit_note extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->helper('terbilang');
        $this->load->model('transaksi/credit_note_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "credit_note", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search_customer"][""] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            
	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/credit_note/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/credit_note/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
			$data['mcustomer'] = $this->credit_note_model->getCustomer();
			
            $config['total_rows'] = $this->credit_note_model->num_credit_note_row($arr_search["search"]);
			$data['data'] = $this->credit_note_model->getcreditNoteList($config['per_page'], $page, $arr_search["search"]);
			
			
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/credit_note/credit_note_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'credit_note', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "credit_note",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/credit_note/index/' . $query_id . '');
    }
	
	function batal() 
    {
    	    $user = $this->session->userdata('username');
			$this->db->delete('creditnotedtl_temp',array('cnno'=>'00000','adduser'=>$user));
	
			redirect('/transaksi/credit_note/');	
			
    }
    
    	
	function add_new() 
    {
    
	        $mylib = new globallib();
	        $sign = $mylib->getAllowList("add");
        	if ($sign == "Y") {
	            $data['msg'] = "";
	            
	            $user = $this->session->userdata('username');
	            $userlevel = $this->session->userdata('userlevel');
	            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
	            
	            //hapus dulu ditemporari
			     //$this->db->delete('creditnotedtl_temp',array('adduser'=>$user));
	            
				$data['mUang']	= $this->credit_note_model->getCurrency();
	            $data['mcustomer'] = $this->credit_note_model->getCustomer();
	            $data['subdivisi'] = $this->credit_note_model->getSubdivisi();
	            //$data['datado'] = $this->credit_note_model->getCreditNoteDetailList($user);
							
	            $data['track'] = $mylib->print_track();
	            $this->load->view('transaksi/credit_note/add_credit_note', $data);
	            //redirect('/transaksi/credit_note/add_new/');
        	} else {
	            $this->load->view('denied');
    		}
        
    }

    function edit_credit_note($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->credit_note_model->getCurrency();
            $dataheader = $this->credit_note_model->getHeader($id);
            $data['header'] = $dataheader;
			if($dataheader->cntype == 'CS' || $dataheader->cntype == 'DS')
            	$data['mcustomer'] = $this->credit_note_model->getCustomer();
            else
            	$data['mcustomer'] = $this->credit_note_model->getTravel();
            $data['subdivisi'] = $this->credit_note_model->getSubdivisi();
			$data['hitungdo'] = $this->credit_note_model->hitungcreditNoteDetailList2($id);
			$data['hitungdodetail'] = $this->credit_note_model->hitungcreditNoteDetailList3($id);
            $data['datado'] = $this->credit_note_model->getcreditNoteDetailList2($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/credit_note/edit_credit_note', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
     function view_credit_note($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->credit_note_model->getCurrency();
            $dataheader = $this->credit_note_model->getHeader($id);
            $data['header'] = $dataheader;
			if($dataheader->cntype == 'CS' || $dataheader->cntype == 'DS')
            	$data['mcustomer'] = $this->credit_note_model->getCustomer();
            else
            	$data['mcustomer'] = $this->credit_note_model->getTravel();
			
            $data['subdivisi'] = $this->credit_note_model->getSubdivisi();
			$data['hitungdo'] = $this->credit_note_model->hitungcreditNoteDetailList2($id);
			$data['hitungdodetail'] = $this->credit_note_model->hitungcreditNoteDetailList3($id);
            $data['datado'] = $this->credit_note_model->getcreditNoteDetailList2($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/credit_note/view_credit_note', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
   
	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $cndate = $this->input->post('v_tanggal_dokumen');
        $v_subdivisi = $this->input->post('v_subdivisi');
        $v_customer = $this->input->post('v_customer');
        $v_type = $this->input->post('v_type');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
		$currencycode = $this->input->post('Uang');
		$diskon = $this->input->post('diskon');
		$ppn = $this->input->post('ppn');
		$v_rekening = $this->input->post('v_rekening');
		$pisah = explode('-',$v_rekening);
		$norekening = $pisah[0];
	
		//detail
		$v_cndid1 = $this->input->post('v_cndid');
		$v_coano1 = $this->input->post('v_coano');
		$v_subdivisi1 = $this->input->post('v_subdivisi');
		$v_dekripsi1 = $this->input->post('v_dekripsi');
		$v_amount1 = $this->input->post('v_amount');
		
		//summary
		$cnamount = $this->input->post('grandtotalhidden');		
        
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($cndate));
		
		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;
		
        if ($flag == "add")
		{
		
		$v_cnno = $mylib->get_code_counter($this->db->database, "creditnote","cnno", $v_type, $data['bulan'], $data['tahun']); 	
		//pertama insertheader terlebih dahulu
		$this->insertNewHeader($v_cnno, $mylib->ubah_tanggal($cndate),$v_type,$norekening, $v_customer, "1", $v_note, $user, $cnamount, $currencycode, $diskon, $ppn);
		
		for ($x = 0; $x < count($v_coano1); $x++)
			{
            $coano = $v_coano1[$x];
            $v_subdivisi2 = $v_subdivisi1[$x];
            $deskripsi = $v_dekripsi1[$x];
            $amount = $v_amount1[$x];
            
            $data_detail=array(
			  'cnno' => $v_cnno,
			  'coano' => $coano,
			  'KdSubdivisi'=>$v_subdivisi2,
			  'value' => $amount,
			  'description' => $deskripsi,
			  'adddate' => date('Y-m-d'),
			  'adduser' => $user
            );
			
				if($coano!=""){
					$this->db->insert('creditnotedtl', $data_detail);
				}	
			
			}	
				
		} 
		
		else if ($flag == "edit") 
		{
			//echo "<pre>";print_r($_POST);echo "</pre>";die;
			$v_cnno = $this->input->post('v_cnno');
			//update header
			$this->updateHeader($v_cnno,$mylib->ubah_tanggal($cndate),$v_type, $v_customer, $v_status, $v_note, $user, $this->input->post('grandtotalhidden'), $currencycode, $diskon, $ppn);	
			
			//insert baru detail
			for ($x = 0; $x < count($v_coano1); $x++)
			{
            $coano = $v_coano1[$x];
            $v_subdivisi2 = $v_subdivisi1[$x];
            $deskripsi = $v_dekripsi1[$x];
            $amount = $v_amount1[$x];
            
            $data_detail=array(
			  'cnno' => $v_cnno,
			  'coano' => $coano,
			  'KdSubdivisi'=>$v_subdivisi2,
			  'value' => $amount,
			  'description' => $deskripsi,
			  'adddate' => date('Y-m-d'),
			  'adduser' => $user
            );
			
				if($coano!=""){
				$this->db->insert('creditnotedtl', $data_detail);
				}
			}
			
			$sum_value= $this->credit_note_model->getValue($v_cnno);
			$data_update = array(
								'cnamount'=>$sum_value->total,
								'cnamountremain'=>$sum_value->total
								);
			$this->db->update('creditnote',$data_update,array('cnno'=>$v_cnno));
			
				if($v_status=='1'){
					//cek total value yang sudah diinsert di creditnotedetail diatas
					$cek_total_amount = $this->credit_note_model->cekDatacreditNoteDetail($v_cnno);
					$grandtotal = $cek_total_amount[0]['grandtotal'];
				
					//update juga ke tabel piutang
					$cek_ditabel_piutang = $this->credit_note_model->cekTabelPiutang($v_cnno);
					if(!empty($cek_ditabel_piutang)){
						$this->updateNewPiutang($v_cnno, $mylib->ubah_tanggal($cndate), $v_customer,$v_type, $grandtotal);
					}else{
						$this->insertNewPiutang($v_cnno, $mylib->ubah_tanggal($cndate), $v_customer,$v_type,$currencycode, $grandtotal);
					}
					
					
				
					//update juga ke tabel mutasi piutang
					$cek_ditabel_mutasi_piutang = $this->credit_note_model->cekTabelMutasiPiutang($v_cnno);
					if(!empty($cek_ditabel_mutasi_piutang)){
						$this->updateNewMutasipiutang($v_cnno, $mylib->ubah_tanggal($cndate), $v_customer, $grandtotal);	
					}else{
						$this->insertNewMutasiPiutang($data['tahun'], $data['bulan'], $v_cnno, $mylib->ubah_tanggal($cndate), $v_customer,$currencycode, $grandtotal,$v_type);
					}
					
					
				
				}			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_cnno . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/transaksi/credit_note/edit_credit_note/'.$v_cnno.'');
    }

    function insertNewHeader($v_cnno, $cndate,$v_type, $norekening, $v_customer, $v_status, $v_note, $user, $cnamount, $currencycode, $diskon, $ppn) 
    {
        $this->credit_note_model->locktables('creditnote');

        $data = array(
				  'cnno' => $v_cnno,
				  'cndate' => $cndate,
				  'cntype'=>$v_type,
				  'KdCustomer' => $v_customer,
				  'note' => $v_note,
				  'status' => 0,
				  'currencycode' => $currencycode,
				  'vatpercent' => $ppn,
				  'discpercent' => $diskon,
				  'cnamount' => $cnamount,
				  'cnamountremain' => $cnamount,
				  'KdRekening'=>$norekening,
				  'adddate' => date('Y-m-d'),
				  'adduser' => $user
				);

        $this->db->insert('creditnote', $data);

        $this->credit_note_model->unlocktables();

    }
    
    function insertNewPiutang($cnno, $tgl, $v_customer,$v_type,$currencycode, $grandtotal) 
    {
        $this->credit_note_model->locktables('piutang');

        $data = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno,
				  'Kdcustomer' => $v_customer,
				  'TipeTransaksi' => $v_type,
				  'TglTransaksi' => $tgl,
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal,
				  'MataUang'=>$currencycode
				);

        $this->db->insert('piutang', $data);

        $this->credit_note_model->unlocktables();

    }
    
    function insertNewMutasiPiutang($tahun, $bulan, $cnno, $tgl, $v_customer ,$currencycode, $grandtotal,$v_type )
    {
        $this->credit_note_model->locktables('mutasi_piutang');

        $data = array(
        		  'Tahun'=>$tahun,
        		  'Bulan'=>$bulan,
        		  'Tanggal' => $tgl,
				  'NoDokumen' => $cnno,
				  'Kdcustomer' => $v_customer,
				  'MataUang'=>$currencycode,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => $v_type
				);

        $this->db->insert('mutasi_piutang', $data);

        $this->credit_note_model->unlocktables();

    }
    
    function updateNewPiutang($cnno, $tgl, $v_customer,$v_type, $grandtotal) 
    {
        $this->credit_note_model->locktables('piutang');

        $data = array(
				  'Kdcustomer' => $v_customer,
				  'TipeTransaksi' => $v_type,
				  'TglTransaksi' => $tgl,
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno
				);

        $this->db->update('piutang', $data, $where);

        $this->credit_note_model->unlocktables();

    }
    
    function updateNewMutasipiutang($cnno, $tgl, $v_customer, $grandtotal) 
    {
        $this->credit_note_model->locktables('mutasi_piutang');

        $data = array(
        		  'Tanggal' => $tgl,
				  'Kdcustomer' => $v_customer,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'I'
				);
				
		$where=array(
		          'NoDokumen' => $cnno
					);

        $this->db->update('mutasi_piutang', $data, $where);

        $this->credit_note_model->unlocktables();

    }
    
    function updateNewpiutang2($cnno, $grandtotal) 
    {
        $this->credit_note_model->locktables('piutang');

        $data = array(
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $cnno,
				  'NoFaktur' => $cnno
				);

        $this->db->update('piutang', $data, $where);

        $this->credit_note_model->unlocktables();

    }
  
	function updateHeader($cnno,$cndate,$v_type, $v_customer, $v_status, $v_note, $user, $cnamount, $currencycode, $diskon, $ppn) 
    {
    	//echo $cnno." - ".$cndate." - ". $v_customer." - ". $v_status." - ". $v_note." - ". $user." - ". $cnamount." - ". $currencycode." - ". $diskon." - ". $ppn;die;
        $this->credit_note_model->locktables('creditnote');

        $data2 = array
				(
				  'cndate' => $cndate,
				  'cntype'=>$v_type,
				  'KdCustomer' => $v_customer,
				  'note' => $v_note,
				  'status' => $v_status,
				  'currencycode' => $currencycode,
				  'vatpercent' => $ppn,
				  'discpercent' => $diskon,
				  'cnamount' => $cnamount,
				  'cnamountremain' => $cnamount,
				  'editdate' => date('Y-m-d'),
				  'edituser' => $user
				);
				
		$where2=array(
		'cnno'=>$cnno
					);

        $this->db->update('creditnote', $data2, $where2);
        
        $this->credit_note_model->unlocktables();
    }

	
	
	function delete_trans($cnno) 
    {
	       
			$this->db->delete("creditnote",array('cnno'=>$cnno));
			$this->db->delete("creditnotedtl",array('cnno'=>$cnno));
			$this->db->delete("piutang",array('NoDokumen'=>$cnno));
			$this->db->delete("mutasi_piutang",array('NoDokumen'=>$cnno));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $cnno . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/credit_note/');
    }

    function delete_detail() //delete detail add
    {
    	
        $cndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $user = $this->uri->segment(6);
        
        $data=array(
        			'cndid'=>$cndid,
        			'coano'=>$coano,
        			'adduser'=>$user
        			);
		
        $this->db->delete("creditnotedtl_temp",$data);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/credit_note/add_new/');
    } 
    
    function delete_detail2() //delete detail add
    {
    	
        $cndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $cnno = $this->uri->segment(6);
        $user = $this->session->userdata('username');
        
                    /*$data=array(
        			'cndid'=>$cndid,
        			'coano'=>$coano,
        			'adduser'=>$user
        			);*/
        			
        			$data=array(
        			'cndid'=>$cndid,
        			'coano'=>$coano
        			);
		
        $this->db->delete("creditnotedtl",$data);
        
        //cek total value yang sudah diinsert di creditnotedetail diatas
		$cek_total_amount = $this->credit_note_model->cekDatacreditNoteDetail($cnno);
		$grandtotal = $cek_total_amount[0]['grandtotal'];
			
		//insert juga ke tabel piutang
		$this->updateNewPiutang2($cnno, $grandtotal);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/credit_note/edit_credit_note/' .$cnno.'');
    }


	
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "CREDIT NOTE";
		$data["header"] 	= $this->credit_note_model->getHeader($nodok);
		$data["detail"] 	= $this->credit_note_model->getDetail_cetak($nodok);
		$data["hitungsi"] 	= $this->credit_note_model->hitungSalesInvoice_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_si', $data);
	}
	
	
	function doPrint()
	{
		$this->load->helper('terbilang');
		$this->load->library('globallib');
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 120;
	    $total_spasi_header = 80;
	    $jml_detail  = 8;
	    $ourFileName = "sales-invoice.txt";
		
		$header 	= $this->credit_note_model->getHeader($nodok);
		$detail 	= $this->credit_note_model->getDetail_cetak($nodok);
		$hitungsi 	= $this->credit_note_model->hitungSalesInvoice_cetak($nodok);
		
		/*echo "<pre>";
		print_r($data["hitungsi"]);
		echo "</pre>";
		die;*/
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
            $arr_data["detail_harga"][$counter] = $val["Harga1c"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "I N V O I C E";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->invoiceno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->invoiceno;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->sidate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->sidate));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->duedate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->duedate));$i++)
                {
                    $echo.=" ";
                }
               
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Pelanggan";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Pelanggan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                                
                $echo.="\r\n";    
            }
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 60;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 5;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Total";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            $harga = $arr_data["detail_harga"][$i_detail];
	            $total = $qty*$harga;
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 60;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,0,',','.');
                    $limit_spasi = 5;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=number_format($harga,0,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_harga));$i++)
                    {
                        $echo.=" ";
                    }
                    
					    if(strlen(number_format($total,0,',','.'))==1){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==2){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==3){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==4){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==5){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==6){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==7){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==8){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==9){
						$jarak_total=5;
						}				
					if(strlen(number_format($total,0,',','.'))==10){
                    $echo.=number_format($total,0,',','.');
					}else{
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($total,0,',','.'))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($total,0,',','.');
					}
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
			
			
            
			foreach($hitungsi as $val)
			{
				$tothal = $val['total'];
				$diskon = $val['diskon'];
				
				if($diskon ==0){
				$potongan_diskon = 0;
				}else{
				$potongan_diskon =($diskon/100)*$total;
				}
				
				$ppn=(10/100)*$tothal;
				
				$grand_total=($tothal-$potongan_diskon)+$ppn;
				
			}
			
											
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Subtotal";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Subtotal"));$i++)
						{
							$echo.=" ";
						}
						if(strlen(number_format($tothal,0,',','.'))==1){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==2){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==3){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==4){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==5){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==6){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==7){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==8){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==9){
						$jarak_tothal=5;
						}
						if(strlen(number_format($tothal,0,',','.'))==10){
						$echo.=number_format($tothal,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($tothal,0,',','.'))+$jarak_tothal));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($tothal,0,',','.');
						}
						
				
						$echo .= "\r\n";						
						
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Diskon";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Diskon"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($potongan_diskon,0,',','.'))==1){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==2){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==3){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==4){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==5){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==6){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==7){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==8){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==9){
						$jarak_diskon=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($potongan_diskon,0,',','.'))+$jarak_diskon));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($potongan_diskon,0,',','.');
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="PPN";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("PPN"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($ppn,0,',','.'))==1){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==2){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==3){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==4){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==5){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==6){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==7){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==8){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==9){
						$jarak_ppn=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($ppn,0,',','.'))+$jarak_ppn));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($ppn,0,',','.');
						
						
						
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Grand Total";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Grand Total"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==1){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total0,',','.'))==2){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==3){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==4){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==5){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==6){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==7){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==8){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==9){
						$jarak_grand_total=5;
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==10){
						$echo.=number_format($grand_total,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($grand_total,0,',','.'))+$jarak_grand_total));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($grand_total,0,',','.');
						}
						
						
						$echo .= "\r\n";						
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Terbilang: ".terbilang($grand_total,$style=4)." Rupiah";
						
						$echo .= "\r\n";
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Note : ".$header->note;
			
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			
		        $data = array(
		            'form_data' => "sales-invoice",
		            'noreferensi' => $header->invoiceno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        
		}

		$paths = "path/to/";
	    $name_text_file='sales-invoice-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
	function cekrekening(){
		$kdrekening = $this->input->post('kdrekening');
		$query = $this->credit_note_model->cekRekening($kdrekening);
		
		if(!empty($query)){
			$data['success']=TRUE;
		}else{
			$data['success']=FALSE;
		}
		
		echo json_encode($data);
	}
	
	function ajax_customer(){
		$type = $this->input->post('Type');
		if($type=='CS' || $type == 'DS'){
			$rows = $this->credit_note_model->getCustomer();
		}else{
			$rows = $this->credit_note_model->getTravel();
		}
		$select = '<select class="form-control-new" name="v_customer" id="v_customer" onchange="getDetail()">';
		foreach($rows as $row){
			$select .= "<option value='".$row['KdCustomer']."'>".$row['Nama']."</option>";
		}
		$select .= "</select>";
		
		echo $select;
		
	}

}

?>