<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Market_list extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/market_listmodel');
    }

    function index()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_divisi"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "market_list", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudung = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudung[1];
                    $search_divisi = explode("=", $arrSearch[2]); // search divisi
                    $arr_search["search"]["divisi"] = $search_divisi[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudung[1];
                    $data["search_divisi"] = $search_divisi[1];
                    $data["search_status"] = $search_status[1];
                }
            }

			foreach($gudang_admin as $key)
			{
				$arr_search["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}

            $arr_search["search"]["gudang_admin"] = $arr_search["gudang_admin"];

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/market_list/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/market_list/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->market_listmodel->getGudang($arr_search["gudang_admin"]);
            $data['mdivisi'] = $this->market_listmodel->getDivisi();

            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->market_listmodel->num_market_list_row($arr_search["search"]);
            $data['data'] = $this->market_listmodel->getPermintaanBarangList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/market_list/market_listlist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function otoritas()
    {
    	echo "Hello";
        /*$notransaksi     = $this->input->post('NoDokumen');
        $nodok           = 'PB';
        $tgltransaksi    = $this->input->post('Tanggal');
        $tipe            = $this->input->post('tipe');
        $username        = $this->session->userdata('username');
        $sysdate         = date("Y-m-d H:i:s");*/



        $this->load->view('transaksi/market_list/form_otoritas');

    }



    function search()
    {
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'market_list', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_divisi=".$this->input->post('search_divisi');
		$search_value .= "&search_status=".$this->input->post('search_status');

		$data = array(
            'query_string' => $search_value,
            'module' => "market_list",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/market_list/index/' . $query_id . '');
    }

    function add_new()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";

            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');

            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            $data["market_list"] = $this->market_listmodel->getMarketList($userlevel);

			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}

            $data['mgudang'] = $this->market_listmodel->getGudang($arr_search["list_gudang_admin"]);
            $data['mdivisi'] = $this->market_listmodel->getDivisi();
            $data['msatuan'] = $this->market_listmodel->getSatuan();

            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/market_list/add_market_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_market_list($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $userlevel = $this->session->userdata('userlevel');
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->market_listmodel->getHeader($id);
            $data['detail'] = $this->market_listmodel->getDetail($id);
            $data["market_list"] = $this->market_listmodel->getMarketList($userlevel);

			$user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);

			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}

            $data['mgudang'] 	= $this->market_listmodel->getGudang($arr_search["list_gudang_admin"]);

            $data['mdivisi'] = $this->market_listmodel->getDivisi();
            $data['msatuan'] = $this->market_listmodel->getSatuan();
            $data["ceknodok"] = $this->market_listmodel->cekNodok($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/market_list/edit_market_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        $v_no_dokumen = $this->input->post('v_no_dokumen');
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_est_terima = $this->input->post('v_est_terima');
        $v_divisi = $this->input->post('v_divisi');
        $v_gudang = $this->input->post('v_gudang');
        $v_keterangan = $this->input->post('v_keterangan');
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');

        // detail
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_qty1 = $this->input->post('v_qty');
        $v_satuan1 = $this->input->post('v_satuan');

        // market
        $market_pcode1 = $this->input->post('market_pcode');
        $market_namabarang1 = $this->input->post('market_namabarang');
        $market_qty1 = $this->input->post('market_qty');
        $market_satuan1 = $this->input->post('market_satuan');

        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));

        if ($flag == "add")
		{
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "market_list_barang_header","NoDokumen", "ML", $xbulan, $xtahun);

            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $flag, $base_url, $user);
        }
		else if ($flag == "edit")
		{
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        for ($x = 0; $x < count($v_pcode1); $x++)
        {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];

            if ($pcode != "")
            {
            	if($v_qty*1>0)
            	{
					$detail = $this->market_listmodel->cekGetDetail($pcode,$v_no_dokumen);

            		//cocokan konversi terlebih dahulu
					$konversi = $this->market_listmodel->getKonversi($pcode,$v_satuan);

					//jika konversi ada datanya
					if(!empty($konversi)){
						//jika Satuan_To sama dengan Satuan yang dipilih
						if($konversi->Satuan_To==$v_satuan){
							$QtyPcs = $v_qty;
						//jika tidak sama
						}else{
							$QtyPcs = $v_qty * $konversi->amount;
						}
					//jika tidak ada datanya konversi
					}else{
						$QtyPcs = $v_qty;
					}

            		if($detail->PCode==$pcode)
					{
						$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs,$detail->Qty);
					}
					else
					{
						$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs);
					}
				}
            }
        }

		// insert detail market
        for ($x = 0; $x < count($market_pcode1); $x++)
        {
            $market_pcode = strtoupper(addslashes(trim($market_pcode1[$x])));
            $market_namabarang = trim($market_namabarang1[$x]);
            $market_qty = $mylib->save_int($market_qty1[$x]);
            $market_satuan = $market_satuan1[$x];

            if ($market_pcode != "")
            {
            	if($market_qty*1>0)
            	{
            		$detail = $this->market_listmodel->cekGetDetail($market_pcode,$v_no_dokumen);

            		if($detail->PCode==$market_pcode)
					{
						$this->updateDetail($flag, $v_no_dokumen, $market_pcode, $market_namabarang, $market_qty, $market_satuan,$detail->Qty);
					}
					else
					{
						$this->insertDetail($flag, $v_no_dokumen, $market_pcode, $market_namabarang, $market_qty, $market_satuan);
					}
				}
            }
        }

        redirect('/transaksi/market_list/edit_market_list/' . $v_no_dokumen . '');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $flag, $base_url, $user)
    {
        $this->market_listmodel->locktables('market_list_barang_header');

        $data = array(
            'NoDokumen' => $no_dokumen,
            'TglDokumen' => $tgl_dokumen,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->insert('market_list_barang_header', $data);

        $this->market_listmodel->unlocktables();

        // return $no;
    }

    function updateHeader($no_dokumen, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $status, $flag, $base_url, $user)
    {
        $this->market_listmodel->locktables('market_list_barang_header,market_list_barang_detail');

        $data = array(
            'TglDokumen' => $tgl_dokumen,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'Status' => $status,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );

        $this->db->update('market_list_barang_header', $data, array('NoDokumen' => $no_dokumen));

        if($status==1)
        {
        	$mylib = new globallib();

        	$to = "";
            $to_name = "";

            $header = $this->market_listmodel->getHeader($no_dokumen);

            $sql = "
                	employee.email,
                    employee.employee_name
                FROM
                    employee
                WHERE
                    1
                    AND employee.username IN('".$header->AddUser."', '".$header->EditUser."')
                ORDER BY
                    employee.employee_name ASC
            ";
            $arrData = $this->globalmodel->getQuery($sql);
            foreach($arrData as $val)
            {
                $to .= $val["email"].";";
                $to_name .= $val["employee_name"].";";
			}

        	$subject = "Notifikasi PB ".$no_dokumen;
            $author  = "Auto System";

            $to .= "samsul@secretgarden.co.id;";
            $to_name .= "Samsul Hidayat;";

            $url = "http://sys.bebektimbungan.com/index.php/transaksi/market_list/edit_market_list/".$no_dokumen;

            $body  = "Dear ".$to_name.", <br><br>";
            $body .= "PB ".$no_dokumen." sudah di kirim oleh <b>".$user." pada tanggal ".date('d-m-Y')." </b><br><br>";
            $body .= "<a href='".$url."' target='_blank'>KLIK DISINI</a> untuk melihat PB Atau Copy Paste Link ini di broswer anda : ".$url;

			$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		}

        $this->market_listmodel->unlocktables();
    }

    function insertDetail($flag, $no_dokumen, $pcode, $namabarang, $qty, $satuan,$QtyPcs)
    {
        $this->market_listmodel->locktables('market_list_barang_detail');

        if ($pcode) {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'PCode' => $pcode,
                'NamaBarang' => $namabarang,
                'Qty' => $qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs
            );

            $this->db->insert('market_list_barang_detail', $data);
        }

        $this->market_listmodel->unlocktables();
    }

    function updateDetail($flag,$no_dokumen,$pcode,$namabarang,$qty,$satuan,$QtyPcs,$qty_tbl)
    {
    	$mylib = new globallib();

    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);

        $this->market_listmodel->locktables('market_list_barang_detail');

        if ($pcode)
        {
            $data = array(
                'NamaBarang' => $namabarang,
                'Qty' => $new_qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs
            );

            $this->db->update('market_list_barang_detail', $data, array('NoDokumen' => $no_dokumen,'PCode' => $pcode));
        }

        $this->market_listmodel->unlocktables();
    }



    function delete_trans($id)
    {
        $data["cekdata"] = $this->market_listmodel->cekNodok($id);

        if ($data["cekdata"]->NoDokumen != "") {
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus data gagal, karena sudah dibuat PR', 'class' => 'danger'));
        } else {
            $this->db->delete('market_list_barang_header', array('NoDokumen' => $id));
            $this->db->delete('market_list_barang_detail', array('NoDokumen' => $id));
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        }

        redirect('/transaksi/market_list/');
    }

    function delete_detail()
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);

        $this->db->delete('market_list_barang_detail', array('sid' => $sid, 'PCode' => $pcode, 'NoDokumen' => $nodok));
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/market_list/edit_market_list/' . $nodok . '');
    }

	function satuan()
    {
     $pcode = $this->input->post('pcode');
     $query = $this->market_listmodel->getSatuanDetail($pcode);
     $query2 = $this->market_listmodel->cekStatusBarang($pcode);

     foreach ($query2->result_array() as $cetak2) {
     	if($cetak2['Status']=='T'){
     	$msg_alert="PCode ".$pcode." Sudah Tidak Aktif";
		 echo "<script>alert('".$msg_alert."');</script>";
		 }
      }


     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }
    }

    function cekbarang()
    {
     $pcode = $this->input->post('pcode');
     $query2 = $this->market_listmodel->cekStatusBarang($pcode);

     foreach ($query2->result_array() as $cetak2) {
     	if($cetak2['Status']=='T'){
		 $data['Nama'] = "";
		 }else{
		 $data['Nama'] = $cetak2['NamaLengkap'];
		 }
      }
      echo json_encode($data);
    }


	function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();

		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');


		$data["judul"]		= "MARKET LIST";
		$data["header"] 	= $this->market_listmodel->getHeader($nodok);
		$data["detail"] 	= $this->market_listmodel->getDetail($nodok);
		$data["pt"] 		= $printlib->getNamaPT();

        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_pb', $data);
	}

	function doPrint()
	{
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();

		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";

		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();

		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();


		$total_spasi = 135;
	    $total_spasi_header = 80;


	    $jml_detail  = 8;
	    $ourFileName = "permintaan-barang.txt";

		$header = $this->market_listmodel->getHeader($nodok);
		$detail = $this->market_listmodel->getDetail($nodok);

		$note_header = substr($header->Keterangan,0,40);

		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,60);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            $arr_data["detail_satuan"][$counter] = $val["Satuan"];

			$counter++;
		}

        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);

        $nama_dokumen = "PERMINTAAN BARANG";

        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n";
                $echo.="\r\n";
            }

            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n";
            }
            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo .= $arr_epson["cond"].$nama_dokumen;

            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.= $arr_epson["cond"]."No : ".$header->NoDokumen;

            $echo.="\r\n";

            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Tanggal;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Tanggal));$i++)
                {
                    $echo.=" ";
                }

                $echo.=$arr_epson["cond"]."Divisi";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Divisi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->NamaDivisi;

                $echo.="\r\n";
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Tanggal Terima";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal Terima"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglTerima;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglTerima));$i++)
                {
                    $echo.=" ";
                }

                $echo.=$arr_epson["cond"]."Gudang";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->NamaGudang;

                $echo.="\r\n";
            }

            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }

            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }

            $echo.="\r\n";

            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;

            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];

	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }

				}
				$echo.="\r\n";
				$no++;

            }

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";

            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->Keterangan;

            $echo .= "\r\n";
            $echo .= "\r\n";


            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.="Dibuat Oleh";

            $limit_spasi = 27;
            for($i=0;$i<($limit_spasi-strlen("Dibuat Oleh"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Hormat Kami";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Hormat Kami"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Diketahui Oleh";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Diketahui Oleh"));$i++)
            {
                $echo.=" ";

            }

            $echo.="Diterima Oleh";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Diterima Oleh"));$i++)
            {
                $echo.=" ";
            }

            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }

            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=" (  $user  )         (               )             (                 )         (                )";

            $echo .= "\r\n";
            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"permintaan-barang");

			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);


			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);

			}

            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"permintaan-barang");

			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "permintaan-barang",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='permintaan-barang-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);

		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;

	}

}

?>
