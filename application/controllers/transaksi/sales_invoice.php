<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Sales_invoice extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->helper('terbilang');
        $this->load->model('transaksi/sales_invoice_model2');
    }

    function index()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "delivery_order", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }


            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/sales_invoice/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/sales_invoice/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->sales_invoice_model2->getGudang();
			$data['mcustomer'] = $this->sales_invoice_model2->getCustomer();

            $config['total_rows'] = $this->sales_invoice_model2->num_sales_invoice_row($arr_search["search"]);
			$data['data'] = $this->sales_invoice_model2->getSalesInvoiceList($config['per_page'], $page, $arr_search["search"]);

			//untuk cek apakah di user tersebut pernah input tapi ngeback atau ngeclose paksa
			$data['datado'] = $this->sales_invoice_model2->getSalesInvoiceDetailList($user);
			if(!empty($data['datado'])){
				$data['param']='1';
			}else{
				$data['param']='0';
			}


            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/sales_invoice/sales_invoice_list', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search()
    {
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'sales_invoice', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');

		$data = array(
            'query_string' => $search_value,
            'module' => "delivery_order",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/sales_invoice/index/' . $query_id . '');
    }

	function batal()
    {
    	    $user = $this->session->userdata('username');
			$hapus_record_terlewat = $this->sales_invoice_model2->cekGetDetail3($user);
			foreach($hapus_record_terlewat as $val){
				 $dono = $val['dono'];
				 $salesinvoicedetailid = $val['salesinvoicedetailid'];

				 $this->db->update('deliveryorder',array('hasinvoice'=>0),array('dono'=>$dono));
			     $this->db->delete('salesinvoicedetail_temp',array('salesinvoicedetailid'=>$salesinvoicedetailid));
				}

			redirect('/transaksi/sales_invoice/');

    }


	function add_new($param)
    {
    	if($param=='1'){
			        $mylib = new globallib();
			        $sign = $mylib->getAllowList("add");
		        	if ($sign == "Y") {
		            $data['msg'] = "";

		            $user = $this->session->userdata('username');
		            $userlevel = $this->session->userdata('userlevel');
		            $gudang_admin = $this->globalmodel->getGudangAdmin($user);

		            //hapus dulu ditemporari
			         $user = $this->session->userdata('username');
					 $hapus_record_terlewat = $this->sales_invoice_model2->cekGetDetail3($user);
					 foreach($hapus_record_terlewat as $val){
					 $dono = $val['dono'];
					 $salesinvoicedetailid = $val['salesinvoicedetailid'];

					 $this->db->update('deliveryorder',array('hasinvoice'=>0),array('dono'=>$dono));
				     $this->db->delete('salesinvoicedetail_temp',array('salesinvoicedetailid'=>$salesinvoicedetailid));
					  }

					$data['mUang']	= $this->sales_invoice_model2->getCurrency();
		            $data['customer'] = $this->sales_invoice_model2->getCustomer();
		            $data['datado'] = $this->sales_invoice_model2->getSalesInvoiceDetailList($user);
					$data['hitungdo'] = $this->sales_invoice_model2->hitungSalesInvoiceDetailList($user);

		            $data['track'] = $mylib->print_track();
		            //$this->load->view('transaksi/sales_invoice/add_sales_invoice', $data);
		            redirect('/transaksi/sales_invoice/add_new/');
		        } else {
		            $this->load->view('denied');
		        }

        }else{
					$mylib = new globallib();
			        $sign = $mylib->getAllowList("add");
			        if ($sign == "Y") {
			            $data['msg'] = "";

			            $user = $this->session->userdata('username');
			            $userlevel = $this->session->userdata('userlevel');
			            $gudang_admin = $this->globalmodel->getGudangAdmin($user);

						$data['mUang']	= $this->sales_invoice_model2->getCurrency();
			            $data['customer'] = $this->sales_invoice_model2->getCustomer();
			            $data['datado'] = $this->sales_invoice_model2->getSalesInvoiceDetailList($user);
						$data['hitungdo'] = $this->sales_invoice_model2->hitungSalesInvoiceDetailList($user);

			            $data['track'] = $mylib->print_track();
			            $this->load->view('transaksi/sales_invoice/add_sales_invoice', $data);
			        } else {
			            $this->load->view('denied');
			        }
		}
    }

    function edit_sales_invoice($id)
    {

        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->sales_invoice_model2->getCurrency();
            $data['header'] = $this->sales_invoice_model2->getHeader($id);

            $data['customer'] = $this->sales_invoice_model2->getCustomer();
			$data['hitungdo'] = $this->sales_invoice_model2->hitungSalesInvoiceDetailList2($id);
            $data['datado'] = $this->sales_invoice_model2->getSalesInvoiceDetailList2($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/sales_invoice/edit_sales_invoice', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function view_sales_invoice($id)
    {

        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->sales_invoice_model2->getCurrency();
            $data['header'] = $this->sales_invoice_model2->getHeader($id);

            $data['customer'] = $this->sales_invoice_model2->getCustomer();
			$data['hitungdo'] = $this->sales_invoice_model2->hitungSalesInvoiceDetailList2($id);
            $data['datado'] = $this->sales_invoice_model2->getSalesInvoiceDetailList2($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/sales_invoice/view_sales_invoice', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $sidate = $this->input->post('v_invoice_date');
		$duedate = $this->input->post('v_invoice_due_date');
		$currencycode = $this->input->post('Uang');
        $grandtotal = $this->input->post('v_grandtotal');
        $pDiscount = $this->input->post('pDiscount');
		$nDiskon = $this->input->post('nDiskon');

        $v_customer = $this->input->post('v_customer');
        $v_note = $this->input->post('v_note');
		$v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');

		list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($sidate));

		$data['bulan'] = $xbulan;
        $data['tahun'] = $xtahun;

        if ($flag == "add")
		{
		$v_invoiceno = $mylib->get_code_counter($this->db->database, "salesinvoice","invoiceno", "SI", $data['bulan'], $data['tahun']);
		//pertama insertheader terlebih dahulu
		$this->insertNewHeader($v_invoiceno, $mylib->ubah_tanggal($sidate), $mylib->ubah_tanggal($duedate), $v_customer, $v_status, $v_note, $user, $grandtotal, $currencycode,$pDiscount);

		//insert juga ke tabel piutang
		$this->insertNewPiutang($v_invoiceno, $mylib->ubah_tanggal($sidate), $mylib->ubah_tanggal($duedate), $v_customer,  $grandtotal);

		//insert juga ke tabel mutasi piutang
		$this->insertNewMutasiPiutang($data['tahun'], $data['bulan'], $v_invoiceno, $mylib->ubah_tanggal($sidate), $v_customer,$currencycode,  $grandtotal);

		//ambil data dari salesinvoicedetail_temp dengan invoiceno 00000 dan adduser yang sama
		$ambil_detail_sales_invoice = $this->sales_invoice_model2->getDataSalesInvoiceDetail($user);

			foreach($ambil_detail_sales_invoice as $val)
			    {
				  $data=array(
						  'invoiceno' => $v_invoiceno,
						  'dono' => $val['dono'],
						  'batchnumber' => $val['batchnumber'],
						  'quantity' => $val['quantity'],
						  'inventorycode' => $val['inventorycode'],
                          'gross' => $val['nettprice'],
                          'disc' => $val['nettprice'] * ($pDiscount/100),
						  'nettprice' => $val['nettprice'] - ($val['nettprice'] * ($pDiscount/100)),
						  'sellprice' => $val['sellprice'] - ($val['sellprice'] * ($pDiscount/100)),
						  'adddate' => $val['adddate'],
						  'adduser' => $val['adduser'],
						  'editdate' => $val['editdate'],
						  'edituser' => $val['edituser']
				  			); 
				  //pindahkan dengan menyimpan dari salesinvoicedetail_temp ke salesinvoicedetail
				  $this->db->insert("salesinvoicedetail",$data);
				}
		//hapus data yang ada di salesinvoicedetail_temp
		$this->db->delete("salesinvoicedetail_temp",array('invoiceno'=>'00000','adduser'=>$user));

		}

		else if ($flag == "edit")
		{
			$v_invoiceno =  $this->input->post('v_invoiceno');
			$this->updateHeader2($v_invoiceno,$mylib->ubah_tanggal($sidate), $mylib->ubah_tanggal($duedate), $v_customer, $v_status, $v_note, $user, $grandtotal, $currencycode); //marubah status dan grandtotal di salesinvoice
			$this->updatePiutang($v_invoiceno,$mylib->ubah_tanggal($sidate), $mylib->ubah_tanggal($duedate), $v_customer, $grandtotal);
			$this->updateMutasiPiutang($v_invoiceno,$mylib->ubah_tanggal($sidate), $mylib->ubah_tanggal($duedate), $v_customer, $grandtotal);
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        redirect('/transaksi/sales_invoice/edit_sales_invoice/' . $v_invoiceno . '');
    }

    function insertNewHeader($v_invoiceno, $sidate, $duedate, $v_customer, $v_status, $note, $user, $grandtotal, $currencycode,$pDiscount)
    {
        $this->sales_invoice_model2->locktables('salesinvoice');

        $data = array(
				'invoiceno'=>$v_invoiceno,
				'sidate'=>$sidate,
				'duedate'=>$duedate,
                'customerid'=>$v_customer,
				'discpercentage'=>$pDiscount,
				'status'=>0,
				'note'=>$note,
				'adddate'=>date('Y-m-d'),
				'adduser'=>$user,
				'grandtotal'=>$grandtotal,
				'currencycode'=>$currencycode
				);
        $this->db->insert('salesinvoice', $data);

        $this->sales_invoice_model2->unlocktables();

    }

    function insertNewPiutang($v_invoiceno, $sidate, $duedate, $v_customer, $grandtotal )
    {
        $this->sales_invoice_model2->locktables('piutang');

        $data = array(
				  'NoDokumen' => $v_invoiceno,
				  'NoFaktur' => $v_invoiceno,
				  'TglTransaksi' => $sidate,
				  'KdCustomer' => $v_customer,
				  'TipeTransaksi' => 'ID',
				  'JatuhTempo' => $duedate,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);

        $this->db->insert('piutang', $data);

        $this->sales_invoice_model2->unlocktables();

    }


    function insertNewMutasiPiutang($tahun, $bulan, $v_invoiceno, $sidate, $v_customer,$currencycode , $grandtotal )
    {
        $this->sales_invoice_model2->locktables('mutasi_piutang');

        $data = array(
        		  'Tahun'=>$tahun,
        		  'Bulan'=>$bulan,
        		  'Tanggal' => $sidate,
				  'NoDokumen' => $v_invoiceno,
				  'KdCustomer' => $v_customer,
				  'MataUang'=>$currencycode,
				  'Kurs'=>1,
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'ID'
				);

        $this->db->insert('mutasi_piutang', $data);

        $this->sales_invoice_model2->unlocktables();

    }


	function updateHeader2($v_invoiceno,$v_tgl_invoice, $v_tgl_duedate, $v_customer, $v_status, $v_note, $user, $grandtotal, $currencycode)
    {
        $this->sales_invoice_model2->locktables('salesinvoice');

        $data = array
				(
				'sidate'=>$v_tgl_invoice,
				'duedate'=>$v_tgl_duedate,
				'customerid'=>$v_customer,
				'status'=>$v_status,
				'note'=>$v_note,
				'editdate'=>date('Y-m-d'),
				'edituser'=>$user,
				'grandtotal'=>$grandtotal,
				'currencycode'=>$currencycode);

		$where=array(
		'invoiceno'=>$v_invoiceno
					);

        $this->db->update('salesinvoice', $data, $where);

        $this->sales_invoice_model2->unlocktables();
    }


    function updatePiutang($v_invoiceno,$sidate, $duedate, $v_customer, $grandtotal)
    {
        $this->sales_invoice_model2->locktables('piutang');

        $data = array
				(
				  'TglTransaksi' => $sidate,
				  'KdCustomer' => $v_customer,
				  'TipeTransaksi' => 'I',
				  'JatuhTempo' => $duedate,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);

		$where=array(
		          'NoDokumen' => $v_invoiceno,
				  'NoFaktur' => $v_invoiceno
					);

        $this->db->update('piutang', $data, $where);

        $this->sales_invoice_model2->unlocktables();
    }


    function updateMutasiPiutang($v_invoiceno,$sidate,  $v_customer, $currencycode, $grandtotal)
    {
    	//echo $sidate;die;
        $this->sales_invoice_model2->locktables('mutasi_piutang');

        $data = array(
        		  'Tanggal' => $sidate,
				  'KdCustomer' => $v_customer,
				  'MataUang'=>$currencycode,
				  'Kurs'=>1,
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'ID'
				);

		$where=array(
		          'NoDokumen' => $v_invoiceno
					);

        $this->db->update('mutasi_piutang', $data, $where);

        $this->sales_invoice_model2->unlocktables();
    }


	function delete_trans($id)
    {

			$hapus_record_terlewat = $this->sales_invoice_model2->cekGetDetail4($id);
			//print_r($hapus_record_terlewat);die;
			foreach($hapus_record_terlewat as $val){
				 $dono = $val['dono'];
				 $salesinvoicedetailid = $val['salesinvoicedetailid'];
				 $invoiceno= $val['invoiceno'];

				 $this->db->update('deliveryorder',array('hasinvoice'=>0),array('dono'=>$dono));
			     $this->db->delete('salesinvoicedetail',array('salesinvoicedetailid'=>$salesinvoicedetailid));
				 $this->db->delete('salesinvoice',array('invoiceno'=>$invoiceno));
				 $this->db->delete('piutang',array('NoDokumen'=>$invoiceno));
				 $this->db->delete('mutasi_piutang',array('NoDokumen'=>$invoiceno));
				}

            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));


        redirect('/transaksi/sales_invoice/');
    }

    function delete_detail() //delete detail add
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);

        //pertama harus kurangi angka di stock
		 //ambil qty dari salesinvoicedetail
			$cek_tinggal_brp = $this->sales_invoice_model2->cekGetDetail2($pcode,$nodok);
			if(($cek_tinggal_brp->jml)<=1){
			$this->db->update('deliveryorder',array('hasinvoice'=>0),array('dono'=>$nodok));
			$this->db->delete('salesinvoicedetail',array('salesinvoicedetailid'=>$sid));
			}else{
			$this->db->delete('salesinvoicedetail',array('salesinvoicedetailid'=>$sid));
			}


        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/sales_invoice/add_new/');
    }


	function delete_detail2() //delete detail edit
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);
		$si_number = $this->uri->segment(7);

        //pertama harus kurangi angka di stock
		 //ambil qty dari salesinvoicedetail
			$cek_tinggal_brp = $this->sales_invoice_model2->cekGetDetail2($pcode,$nodok);
			if(($cek_tinggal_brp->jml)<=1){
			$this->db->update('deliveryorder',array('hasinvoice'=>0),array('dono'=>$nodok));
			$this->db->delete('salesinvoicedetail',array('salesinvoicedetailid'=>$sid));
			}else{
			$this->db->delete('salesinvoicedetail',array('salesinvoicedetailid'=>$sid));
			}


        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/sales_invoice/edit_sales_invoice/'.$si_number.'/');
    }

    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();

		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');


		$data["judul"]		= "I N V O I C E";
		$data["header"] 	= $this->sales_invoice_model2->getHeader($nodok);
		$data["detail"] 	= $this->sales_invoice_model2->getDetail_cetak($nodok);
		$data["hitungsi"] 	= $this->sales_invoice_model2->hitungSalesInvoice_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();

        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_si', $data);
	}


	function doPrint()
	{
		$this->load->helper('terbilang');
		$this->load->library('globallib');
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();

		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";

		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();

		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();


		$total_spasi = 120;
	    $total_spasi_header = 80;
	    $jml_detail  = 8;
	    $ourFileName = "sales-invoice.txt";

		$header 	= $this->sales_invoice_model2->getHeader($nodok);
		$detail 	= $this->sales_invoice_model2->getDetail_cetak($nodok);
		$hitungsi 	= $this->sales_invoice_model2->hitungSalesInvoice_cetak($nodok);

		/*echo "<pre>";
		print_r($data["hitungsi"]);
		echo "</pre>";
		die;*/

		$note_header = substr($header->note,0,40);

		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
            $arr_data["detail_harga"][$counter] = $val["nettprice"];

			$counter++;
		}

        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);

        $nama_dokumen = "I N V O I C E";

        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n";
                $echo.="\r\n";
            }

            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n";
            }
            //$echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo .= $arr_epson["cond"].$nama_dokumen;

            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->invoiceno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.= $arr_epson["cond"]."No : ".$header->invoiceno;

            $echo.="\r\n";

            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->sidate;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->sidate));$i++)
                {
                    $echo.=" ";
                }

                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->duedate;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->duedate));$i++)
                {
                    $echo.=" ";
                }

                $echo.="\r\n";
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Pelanggan";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Pelanggan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Nama;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }

                $echo.="\r\n";
            }

            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 60;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 5;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Total";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
            {
                $echo.=" ";
            }

            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }

            $echo.="\r\n";

            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;

            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            $harga = $arr_data["detail_harga"][$i_detail];
	            $total = $qty*$harga;
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 60;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($qty,0,',','.');
                    $limit_spasi = 5;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=number_format($harga,0,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_harga));$i++)
                    {
                        $echo.=" ";
                    }

					    if(strlen(number_format($total,0,',','.'))==1){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==2){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==3){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==4){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==5){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==6){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==7){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==8){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==9){
						$jarak_total=5;
						}
					if(strlen(number_format($total,0,',','.'))==10){
                    $echo.=number_format($total,0,',','.');
					}else{
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($total,0,',','.'))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($total,0,',','.');
					}

				}
				$echo.="\r\n";
				$no++;

            }

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";



			foreach($hitungsi as $val)
			{
				$tothal = $val['total'];
				$diskon = $val['diskon'];

				if($diskon ==0){
				$potongan_diskon = 0;
				}else{
				$potongan_diskon =($diskon/100)*$total;
				}

				$ppn=(10/100)*$tothal;

				$grand_total=($tothal-$potongan_diskon)+$ppn;

			}

			if($i_page==$jml_page)
            {

						/*$echo.=$arr_epson["cond"]."Note";
		                $limit_spasi = (7-2);
		                for($i=0;$i<($limit_spasi-strlen("Note"));$i++)
		                {
		                    $echo.=" ";
		                }
		                $echo.=": ";

		                $echo.=$arr_epson["cond"].$header->note;

						$limit_spasi = 82;*/
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="Subtotal";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Subtotal"));$i++)
						{
							$echo.=" ";
						}
						if(strlen(number_format($tothal,0,',','.'))==1){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==2){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==3){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==4){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==5){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==6){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==7){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==8){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==9){
						$jarak_tothal=5;
						}
						if(strlen(number_format($tothal,0,',','.'))==10){
						$echo.=number_format($tothal,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($tothal,0,',','.'))+$jarak_tothal));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($tothal,0,',','.');
						}


						$echo .= "\r\n";


						/*$echo.=$arr_epson["cond"]."Terbilang";
		                $limit_spasi = (7-2);
		                for($i=0;$i<($limit_spasi-strlen("Terbilang"));$i++)
		                {
		                    $echo.=" ";
		                }
		                $echo.=": ";

		                $echo.=$arr_epson["cond"].terbilang($grand_total,$style=4)." Rupiah"; */

						//$limit_spasi = 45 - $limit_spasi-strlen("Terbilang");
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="Diskon";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Diskon"));$i++)
						{
							$echo.=" ";
						}

						if(strlen(number_format($potongan_diskon,0,',','.'))==1){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==2){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==3){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==4){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==5){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==6){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==7){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==8){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==9){
						$jarak_diskon=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($potongan_diskon,0,',','.'))+$jarak_diskon));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($potongan_diskon,0,',','.');


						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="PPN";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("PPN"));$i++)
						{
							$echo.=" ";
						}

						if(strlen(number_format($ppn,0,',','.'))==1){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==2){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==3){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==4){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==5){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==6){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==7){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==8){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==9){
						$jarak_ppn=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($ppn,0,',','.'))+$jarak_ppn));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($ppn,0,',','.');





						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}

						$echo.="Grand Total";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Grand Total"));$i++)
						{
							$echo.=" ";
						}

						if(strlen(number_format($grand_total,0,',','.'))==1){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total0,',','.'))==2){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==3){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==4){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==5){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==6){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==7){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==8){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==9){
						$jarak_grand_total=5;
						}

						if(strlen(number_format($grand_total,0,',','.'))==10){
						$echo.=number_format($grand_total,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($grand_total,0,',','.'))+$jarak_grand_total));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($grand_total,0,',','.');
						}


						//$echo .= "\r\n";
						$echo .= $spasi_awal;
						//$echo .= $arr_epson["cond"]."Terbilang: ".terbilang($grand_total,$style=4)." Rupiah";

						//$echo .= "\r\n";
						$echo .= $spasi_awal;
						//$echo .= $arr_epson["cond"]."Note : ".$header->note;

            $echo .= "\r\n";
            //$echo .= "\r\n";


            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";

            }

            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }

            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }

            $limit_spasi = 9;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=" (               )             (                 )         (                )";

            $echo .= "\r\n";
            //$echo .= "\r\n";


			}


			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");

			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."]"));$i++)
			    {
			        $echo.=" ";
			    }

			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."]".chr(18);


			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."]"));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."]".chr(18);

			}

            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");


		        $data = array(
		            'form_data' => "sales-invoice",
		            'noreferensi' => $header->invoiceno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);

		}

		$paths = "path/to/";
	    $name_text_file='sales-invoice-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);

		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;

	}

}

?>
