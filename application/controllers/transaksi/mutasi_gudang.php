<?php
// Febri
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Mutasi_gudang extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        error_reporting(0);                              
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/persediaan/mutasi_gudang_model', 'mutasigudang');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$id = $this->uri->segment(4);	
			$user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
			
			$data["search_keyword"] = "";
			$data["search_gudang"] = "";
			$data["search_tujuan"] = "";
            $data["search_status"] = "";
			$resSearch = "";
			$arr_search["search"]= array();
			$id_search = "";
			if($id*1>0)
			{	
				$resSearch = $this->globalmodel->getSearch($id,"mutasi_gudang",$user);	
				$arrSearch = explode("&",$resSearch->query_string);
				
				$id_search = $resSearch->id;
				
				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					$search_gudung = explode("=", $arrSearch[1]); // search gudang
					$arr_search["search"]["gudang"] = $search_gudung[1];
					$search_tujuan = explode("=", $arrSearch[2]); // search tujuan
					$arr_search["search"]["tujuan"] = $search_tujuan[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];
					
					$data["search_keyword"] = $search_keyword[1];
					$data["search_gudang"] = $search_gudung[1];
					$data["search_tujuan"] = $search_tujuan[1];
                    $data["search_status"] = $search_status[1];
				}
			}
            
			foreach($gudang_admin as $key)
			{
				$arr_search["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
            
            $arr_search["search"]["gudang_admin"] = $arr_search["gudang_admin"];
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if($id_search)
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/mutasi_gudang/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/transaksi/mutasi_gudang/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}
			
            $data['mgudang'] = $this->mutasigudang->getGudang($arr_search["list_gudang_admin"]);
            $data['minmut'] = $this->mutasigudang->getIntMut();
            
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            
            $thnbln = $data['tahun'] . $data['bulan'];
            
            $config['total_rows'] = $this->mutasigudang->num_tabel_row($arr_search["search"]);
            $data['data'] = $this->mutasigudang->getTabelList($config['per_page'], $page, $arr_search["search"]);
            
            $data['track'] = $mylib->print_track();
            
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            
            $this->load->view('transaksi/persediaan/mutasi_gudang/tabellist', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }    

    function otoritas()
    {
        $this->load->view('transaksi/persediaan/mutasi_gudang/form_otoritas');
    } 

    function search()
    {
        $mylib = new globallib();
        
		$user = $this->session->userdata('username');
		
		// hapus dulu yah
		$this->db->delete('ci_query', array('module' => 'mutasi_gudang','AddUser' => $user)); 
		
		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_tujuan=".$this->input->post('search_tujuan');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "mutasi_gudang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);
        
		$query_id = $this->db->insert_id();
		
        redirect('/transaksi/mutasi_gudang/index/'.$query_id.'');
		
	} 
	
    function add_new()
    {
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y")
    	{
     		$data['msg']	   	= "";
            
            $user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            
			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
			
            $data['mgudang_asal'] 	= $this->mutasigudang->getGudang($arr_search["list_gudang_admin"]);
            $data['mgudang_tujuan'] = $this->mutasigudang->getGudang();
            $data['minmut'] 	= $this->mutasigudang->getIntMut();
            $data['msatuan'] 	= $this->mutasigudang->getSatuan();
            $data['mpb'] 		= $this->mutasigudang->getNopb();
            $data['track'] 		= $mylib->print_track();
            
	    	$this->load->view('transaksi/persediaan/mutasi_gudang/form_add',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function edit_form($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['bulan'] 		= $this->session->userdata('bulanaktif');
            $data['tahun'] 		= $this->session->userdata('tahunaktif');
            
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            
			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}
			
            $data['header'] 	= $this->mutasigudang->getHeader($id);
            $data['detail'] 	= $this->mutasigudang->getDetail($id);
            $data['mgudang_asal'] 	= $this->mutasigudang->getGudang($arr_search["list_gudang_admin"]);
            $data['mgudang_tujuan'] = $this->mutasigudang->getGudang();
            $data['minmut'] 	= $this->mutasigudang->getIntMut();
            $data['msatuan']	= $this->mutasigudang->getSatuan();
            $data['track'] 		= $mylib->print_track();
            
            $this->load->view('transaksi/persediaan/mutasi_gudang/form_edit', $data);

        } 
        else 
        {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
               
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_type = $this->input->post('v_type');
        $v_gudang_from = $this->input->post('v_gudang_from');
        $v_gudang_to = $this->input->post('v_gudang_to');
        $v_no_pb = $this->input->post('v_no_pb');
        $v_keterangan = $mylib->save_char($this->input->post('v_keterangan'));
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');
        
        // detail pcode
		$pcode1 = $this->input->post('pcode');
		$v_namabarang1 = $this->input->post('v_namabarang');
		$v_qty1 = $this->input->post('v_qty');
		$v_satuan1 = $this->input->post('v_satuan');
		$v_keterangan_pcode1 = $this->input->post('v_keterangan_pcode');
		
		list($tgl, $bln, $thn) = explode('-',$v_tgl_dokumen);
		
        $data['bulan'] = $bln ; // $this->session->userdata('bulanaktif');
        $data['tahun'] = $thn ; // $this->session->userdata('tahunaktif');
		
		if($flag=="add")
        {
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "trans_mutasi_header","NoDokumen", "MS", $data['bulan'], $data['tahun']);
            
            $this->insertNewHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_type, $v_gudang_from, $v_gudang_to, $v_no_pb, $v_keterangan, $v_status, $flag, $base_url, $user);
       		
       		$this->session->set_flashdata('msg', array('message' => 'Proses tambah <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
       	} 
        else if($flag=="edit")
        {
        	$v_no_dokumen = $this->input->post('v_no_dokumen');
        	
            $this->updateHeader($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_type, $v_gudang_from, $v_gudang_to, $v_keterangan, $v_status, $flag, $base_url, $user);
        	
        	$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen '.$v_no_dokumen.'</strong> berhasil','class' => 'success'));
        }
        
    	// hapus detail jika ada
    	//$this->db->delete('trans_pr_detail', array('NoDokumen' => $v_no_dokumen)); 
        
		$counter=1;
        for($x=0;$x< count($pcode1);$x++)
		{
			$pcode 				= strtoupper(addslashes(trim($pcode1[$x])));
			$v_namabarang 		= $mylib->save_char($v_namabarang1[$x]);
			$v_qty 				= $mylib->save_int($v_qty1[$x]);
			$v_satuan			= $v_satuan1[$x];
			$v_keterangan_pcode	= $mylib->save_char($v_keterangan_pcode1[$x]);
			
			if($pcode!="")
			{   
			
				//cari nilai atau harga1c di masterbarang untuk setiap PCodenya
				$harga = $this->mutasigudang->getHarga($pcode);
				$nilai=$harga->harga1c;
				
				//insert atau update ke mutasi dan stock
				$this->_insert_mutasi($mylib->ubah_tanggal($v_tgl_dokumen), $v_no_dokumen, $v_gudang_from, $pcode , $v_qty, $nilai, "MM", "O");
				$this->_update_stock($mylib->ubah_tanggal($v_tgl_dokumen), $v_gudang_from, $pcode, $v_qty, $nilai, "O");
								
				$detail 	= $this->mutasigudang->cekGetDetail($pcode,$v_no_dokumen);
				
				//cocokan konversi terlebih dahulu
				$konversi = $this->mutasigudang->getKonversi($pcode,$v_satuan);
				
				//jika konversi ada datanya
				if(!empty($konversi)){
					//jika Satuan_To sama dengan Satuan yang dipilih
					if($konversi->Satuan_To==$v_satuan){
						$QtyPcs = $v_qty;
					//jika tidak sama
					}else{
						$QtyPcs = $v_qty * $konversi->amount;
					}
				//jika tidak ada datanya konversi
				}else{
					$QtyPcs = $v_qty;
				}
							
				if($detail->PCode==$pcode)
				{
					$this->updateDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs,$v_keterangan_pcode,$detail->Qty);
					$counter++;
				}
				else
				{
					$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs,$v_keterangan_pcode);
					$counter++;
				}
				
				//update QtyMutasi di permintaan barang detail
				$this->updateQtyMutasi($v_no_pb,$pcode,$v_qty);
				
			}
		}
		
		
        redirect('/transaksi/mutasi_gudang/edit_form/'.$v_no_dokumen.'');
    }

    function insertNewHeader($no_dokumen, $tgl_dokumen, $intmut, $gudang_from, $gudang_to, $no_pb, $keterangan, $status, $flag, $base_url, $user)
    {   
        //echo "00";
        $this->mutasigudang->locktables('trans_mutasi_header');
        //echo "aa";
        $data = array(
            'NoDokumen' => $no_dokumen,
            'TglDokumen' => $tgl_dokumen,
            'PurposeId' => $intmut,
            'KdGudang_From' => $gudang_from,
            'KdGudang_To' => $gudang_to,
            'NoPb' => $no_pb,
            'Keterangan' => $keterangan,
            'Status' => "0",
            'AddDate' => date("Y-m-d H:i:s"),
            'AddUser' => $user,
            'EditDate' => date("Y-m-d H:i:s"),
            'EditUser' => $user
        );
        //echo "bb";
        $this->db->insert('trans_mutasi_header', $data);
        
        $this->mutasigudang->unlocktables();
        
        // return $no;
    }

    function updateHeader($no_dokumen, $tgl_dokumen, $intmut, $gudang_from, $gudang_to, $keterangan, $status, $flag, $base_url, $user)
    {
        //$this->mutasigudang->locktables('trans_mutasi_header');

        $data = array(
            'TglDokumen' => $tgl_dokumen,
            'PurposeId' => $intmut,
            'KdGudang_From' => $gudang_from,
            'KdGudang_To' => $gudang_to,
            'Keterangan' => $keterangan,
            'Status' => $status,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );
        $this->db->update('trans_mutasi_header', $data, array('NoDokumen' => $no_dokumen));
        
        if($status=="1")
        {
        	$mylib = new globallib();
        	
            $this->mutasigudang->get_mutasi_stock($no_dokumen); 
        	
        	$to = "";
            $to_name = "";
            
            $header = $this->mutasigudang->getHeader($no_dokumen);
            
            $sql = "
                	employee.email,        
                    employee.employee_name
                FROM
                    employee
                WHERE
                    1
                    AND employee.username IN('".$header->AddUser."', '".$header->EditUser."')
                ORDER BY
                    employee.employee_name ASC
            ";
            $arrData = $this->globalmodel->getQuery($sql);
            foreach($arrData as $val)
            {
                $to .= $val["email"].";";
                $to_name .= $val["employee_name"].";";
			}
                                
        	$subject = "Notifikasi MS ".$no_dokumen;
            $author  = "Auto System";
            
            $to .= "samsul@secretgarden.co.id;";
            $to_name .= "Samsul Hidayat;";
            
            $body  = "MS dengan nomor <b>'.$no_dokumen.'</b> telah berhasil dikirim";
            
			$mylib->send_email_multiple($subject, $body, $to, $to_name, $author);
		}
        
        //return $counter;
    }

    function insertDetail($flag,$no_dokumen,$pcode,$namabarang,$qty,$satuan,$QtyPcs,$keterangan)
    {
        $this->mutasigudang->locktables('trans_mutasi_detail');

        if ($pcode) 
        {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'PCode' => $pcode,
                'NamaBarang' => $namabarang,
                'Qty' => $qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs,
                'Keterangan' => $keterangan
            );
            
            $this->db->insert('trans_mutasi_detail', $data);
        } 
        
        $this->mutasigudang->unlocktables();
    }

    function updateDetail($flag,$no_dokumen,$pcode,$namabarang,$qty,$satuan,$QtyPcs,$keterangan,$qty_tbl)
    {
    	$mylib = new globallib();
    	
    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);
    	
//		cocokan konversi terlebih dahulu
		$konversi = $this->mutasigudang->getKonversi($pcode,$v_satuan);
				
		//jika konversi ada datanya
		if(!empty($konversi)){
			//jika Satuan_To sama dengan Satuan yang dipilih
			if($konversi->Satuan_To==$v_satuan){
				$QtyPcs = $new_qty;
			//jika tidak sama
			}else{
				$QtyPcs = $new_qty * $konversi->amount;
			}
		//jika tidak ada datanya konversi
		}else{
			$QtyPcs = $new_qty;
		}
 
        $this->mutasigudang->locktables('trans_mutasi_detail');

        if ($pcode) 
        {
            $data = array(
                'NamaBarang' => $namabarang,
                'Qty' => $new_qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs,
                'Keterangan' => $keterangan
            );
            
            $this->db->update('trans_mutasi_detail', $data, array('NoDokumen' => $no_dokumen,'PCode' => $pcode));
        } 
        
        $this->mutasigudang->unlocktables();
    }
    
    function updateQtyMutasi($v_no_pb,$pcode,$v_qty)
    {
    	//echo $v_no_pb." - ".$pcode." - ".$v_qty;die;
    	$mylib = new globallib();
    	 
        $this->mutasigudang->locktables('permintaan_barang_detail');

        if ($pcode) 
        {
            $data = array(
                'QtyMutasi' => $v_qty
            );
            
            $where = array (
            'NoDokumen' => $v_no_pb,
            'PCode'=> $pcode
            );
            
            $this->db->update('permintaan_barang_detail', $data, $where);
        } 
        
        $this->mutasigudang->unlocktables();
    }
    
    function _insert_mutasi($tgl, $notrans, $gudang, $kdbarang, $qty, $nilai, $kdtransaksi, $jenis){
		
		$data = array(
                    'NoTransaksi' => $notrans ,
                    'Jenis' => $jenis,
                    'KdTransaksi' => $kdtransaksi,
                    'Gudang' => $gudang,
                    'Tanggal' => $tgl,
                    'KodeBarang' => $kdbarang,
                    'Qty' => $qty,
                    'Nilai' => $nilai,
                    'Status' =>1
                );
        
        $this->db->insert('mutasi', $data);
	}
	
	function _update_stock($tgl, $gudang, $kdbarang, $qty, $nilai, $jenis){
		
		list($tahun, $bulan, $tanggal) = explode('-',$tgl);
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;
        
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $bulan; 
			$fieldnupdate = "GNKeluar" . $bulan; 
			$vqty = $qty*-1;
			$vnilai = $qty*$nilai*-1;
		}else{
			$fieldupdate = "GMasuk" .$bulan;
			$fieldnupdate = "GNMasuk" .$bulan;
			$vqty = $qty;
			$vnilai = $qty*$nilai;
		}
        
		$query = $this->db->get_where('stock', array(
		            'Tahun' => $tahun,
		            'KdGudang' => $gudang,
		            'PCode' => $kdbarang,
		            'Status' => 'G'
        			));

        $count = $query->num_rows(); 
        if($count>0){
			$sql = "Update stock set $fieldupdate = $fieldupdate+$qty, $fieldakhir=$fieldakhir+$vqty,
					$fieldnupdate = $fieldnupdate+$nilai, $fieldnakhir=$fieldnakhir+$vnilai	
					 where Tahun='$tahun' and KdGudang='$gudang' and PCode='$kdbarang'";
			$this->db->query($sql);
		}else{
			$data = array(
					'Tahun' => $tahun,
		            'KdGudang' => $gudang,
		            'PCode' => $kdbarang,
		            'Status' => 'G',
		            $fieldupdate => $qty,
		            $fieldnupdate => $nilai,
		            $fieldakhir => $vqty,
		            $fieldnakhir => $vnilai);
		    $this->db->insert('stock', $data);
		}
	}

	function delete_trans($id)
	{
		$detail = $this->mutasigudang->ambilDetail($id);
			/*echo "<pre>";
			print_r($detail);
			echo "</pre>";die;*/
		//khusus update stock
		foreach($detail as $val){
		    $nodok = $val['NoDokumen'];
			$pcode=  $val['PCode'];
			
			$ambilQty = $this->mutasigudang->ambilQty($pcode,$nodok);
		
			$Qty = $ambilQty->Qty;
			$KdGudang = $ambilQty->KdGudang;
			$TglDokumen = $ambilQty->TglDokumen;
			
			$pisah_periode			= explode("-",$TglDokumen);
			$tahun					= $pisah_periode[0];
			$bulan					= $pisah_periode[1];
			$hari					= $pisah_periode[2];
			
			$fieldakhir = "GAkhir" . $bulan;
			$fieldnakhir = "GNAkhir" . $bulan;
		
			//jenis untuk penerimaan lain adalah I (Input)
			$jenis='O';
			
			if($jenis=='O'){
				$fieldupdate = "GKeluar" . $bulan; 
				$fieldnupdate = "GNKeluar" . $bulan; 
				$vqty = $qty*-1;
				$vnilai = $qty*$nilai*-1;
			}else{
				$fieldupdate = "GMasuk" .$bulan;
				$fieldnupdate = "GNMasuk" .$bulan;
				$vqty = $qty;
				$vnilai = $qty*$nilai;
			}
			
				//ambil Qty di stock
				$field_stock = $this->mutasigudang->cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir);	
				/*echo "<pre>";
				print_r($field_stock);
				echo "</pre>";die;*/
				$this->_update_stock_jika_hapus_detail($tahun,$KdGudang,$pcode,$Qty,$field_stock->$fieldupdate,$fieldupdate,$field_stock->$fieldakhir,$fieldakhir);
				
				//hapus mutasi
				$this->db->delete('mutasi', array('Tanggal'=>$TglDokumen,'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
		}
		
		$this->db->delete('trans_mutasi_header', array('NoDokumen' => $id));
		$this->db->delete('trans_mutasi_detail', array('NoDokumen' => $id));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen '.$id.'</strong> berhasil','class' => 'success'));
	
		redirect('/transaksi/mutasi_gudang/'); 
	}

	function delete_detail()
	{
		$sid = $this->uri->segment(4);	
		$pcode = $this->uri->segment(5);	
		$nodok = $this->uri->segment(6);	
		
		//pertama harus kurangi angka di stock
		//ambil Qty dari trans_mutasi_detail
		$ambilQty = $this->mutasigudang->ambilQty($pcode,$nodok);
		
		$Qty = $ambilQty->Qty;
		$KdGudang = $ambilQty->KdGudang;
		$TglDokumen = $ambilQty->TglDokumen;
		
		    $pisah_periode					= explode("-",$TglDokumen);
			$tahun					= $pisah_periode[0];
			$bulan					= $pisah_periode[1];
			$hari					= $pisah_periode[2];
			
		$fieldakhir = "GAkhir" . $bulan;
		$fieldnakhir = "GNAkhir" . $bulan;
		
		//jenis untuk trans_mutasi_detail adalah O
        $jenis='O';
		
        if($jenis=='O'){
			$fieldupdate = "GKeluar" . $bulan; 
			$fieldnupdate = "GNKeluar" . $bulan; 
			$vqty = $qty*-1;
			$vnilai = $qty*$nilai*-1;
		}else{
			$fieldupdate = "GMasuk" .$bulan;
			$fieldnupdate = "GNMasuk" .$bulan;
			$vqty = $qty;
			$vnilai = $qty*$nilai;
		}
		
			//ambil Qty di stock
			$field_stock = $this->mutasigudang->cekGetStock($tahun,$KdGudang,$pcode,$fieldupdate,$fieldnupdate,$fieldakhir,$fieldnakhir);	
			/*echo "<pre>";
			print_r($field_stock);
			echo "</pre>";die;*/
			$this->_update_stock_jika_hapus_detail($tahun,$KdGudang,$pcode,$Qty,$field_stock->$fieldupdate,$fieldupdate,$field_stock->$fieldakhir,$fieldakhir);
		
			//Hapus Mutasi
			$this->db->delete('mutasi', array('Tanggal' => $TglDokumen, 'KodeBarang' => $pcode, 'NoTransaksi' => $nodok));
			
		
		$this->db->delete('trans_mutasi_detail', array('sid' => $sid,'PCode' => $pcode,'NoDokumen' => $nodok));
		$this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode '.$pcode.'</strong> berhasil','class' => 'success'));
		
		redirect('/transaksi/mutasi_gudang/edit_form/'.$nodok.''); 
	}
	
	function _update_stock_jika_hapus_detail($tahun,$KdGudang,$pcode,$Qty,$nilai_lama_di_stock1,$field_sasaran1,$nilai_lama_di_stock2,$field_sasaran2)
    {
		
    	$jml_update_untuk_field1=$nilai_lama_di_stock1-$Qty;
		$jml_update_untuk_field2=$nilai_lama_di_stock2-$Qty;
        $this->mutasigudang->locktables('stock');

        if ($pcode) 
        {
            $data = array(
                $field_sasaran1=>$jml_update_untuk_field1,
				$field_sasaran2=>$jml_update_untuk_field2
            );
            $where = array
		            (
		            'Tahun' => $tahun,
		            'KdGudang' => $KdGudang,
		            'PCode' => $pcode
		            ); 
			
            $this->db->update('stock', $data,$where );
        } 
        
        $this->mutasigudang->unlocktables();
    }
	
	function getAjax()
	{ 
		 $mylib	= new globallib();
		 $ajax	= $this->input->post('ajax');
		 
		 if($ajax=="search_keyword_pb")
		 {
		 	$v_keyword 		= $mylib->save_char($this->input->post('v_keyword'));	
	        $data['mpb']	= $this->mutasigudang->getNopb($v_keyword);
			
			?>
	    	<select name="v_no_pb" id="v_no_pb" class="form-control-new" style="width: 200px;" onchange="CallAjax('ajax_nopb', this.value)">
	    		<option value="">Pilih No PB</option>
	    		<?php
	    		foreach($data['mpb'] as $val)
	    		{
					?><option value="<?php echo $val["NoDokumen"]; ?>"><?php echo $val["NoDokumen"]; ?></option><?php
				}
	    		?>
	    	</select>
	    	<?php
		 }
		 else if($ajax=="ajax_nopb")
		 {
		 	$v_nopb	= $mylib->save_char($this->input->post('v_nopb'));	
	        $result	= $this->mutasigudang->getDetailPCodePb($v_nopb);
	        	        
	        if($v_nopb)
	        {
	        	?>
	        	<td colspan="100%" >
					<table class="table table-bordered responsive">
						<thead>
							<tr>
							    <th width="100"><center>PCode</center></th>
							    <th><center>Nama Barang</center></th>                 
							    <th width="50"><center>Qty PB</center></th>      
							    <th width="50"><center>Qty</center></th>
							    <th width="100"><center>Satuan</center></th>
							    <th width="100"><center>Keterangan</center></th>
							</tr>
						</thead>
						<tbody>
						
						<?php
						$no=1;
						foreach($result as $val)
						{
							?>
							<tr id="baris<?php echo $no ?>">
								<td align="center">
									<input type="hidden" name="pcode[]" id="pcode<?php echo $no; ?>" value="<?php echo $val["PCode"]; ?>" />
									<input type="hidden" name="v_namabarang[]" id="v_namabarang<?php echo $no; ?>" value="<?php echo $val["NamaBarang"]; ?>"/>	
									<input type="hidden" name="v_satuan[]" id="v_satuan<?php echo $no; ?>" value="<?php echo $val["Satuan"]; ?>"/>	
									<?php echo $val["PCode"]; ?>
								</td>
								<td><?php echo $val["NamaBarang"]; ?></td>
								<td align="right"><?php echo number_format($val["Qty"]); ?></td>
								<td align="right"><input type="text" name="v_qty[]" id="v_qty<?php echo $no; ?>" onblur="cek(this);"  value="" class="form-control-new" size="10" style="text-align: right;" /></td>
								<td align="center"><?php echo $val["NamaSatuan"]; ?></td>
								<td align="center">&nbsp;</td>
						    </tr>
							<?php
							$no++;
						}
						
						?>
							
						</tbody>
					</table>
	        	</td>
	        	<?php
			}
		 }
	}
	
	function satuan()
    {        
     $pcode = $this->input->post('pcode');
     $query = $this->mutasigudang->getSatuanDetail($pcode);
     
     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }     
    }
	
	function viewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		$data["judul"]		= "MUTASI ANTAR GUDANG";
		
		$data["header"] 	= $this->mutasigudang->getHeader($nodok);
		$data["detail"] 	= $this->mutasigudang->getDetail($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_mutasi_gudang', $data);
	}
	
	
	function cek_stock()
    {  
    
     $mylib_ = new globallib();
          
     $pcode = $this->input->post('id');
	 $qty = $this->input->post('qty');
	 $gudang = $this->input->post('gdg');
	 $tanggal = $mylib_->ubah_tanggal($this->input->post('tgl'));
	 
	 $stock = $mylib_->cek_keseluruhan_stock($pcode,$gudang,$tanggal);
	
	  //gudang 15 adalah gudang produksi itu sebenernya menggunakan program lain/pabrik yang di FG kan langsung jadi stock,
	  //tapi untuk kasus ini di perbolehkan by pass untuk gudang 15 Produksi
	  
	  if($gudang!="15"){
	  	 if($qty==$stock){
		    $data=array('status'=>FALSE);
		 }else if($qty<$stock){
		    $data=array('status'=>FALSE);
		 }else{
		    $data=array('status'=>TRUE);
		 }
	  }else{
	  	$data=array('status'=>FALSE);
	  }
	     
		 echo json_encode($data);
	
    }
	
	
	function doPrint()
	{
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 135;
	    $total_spasi_header = 80;
	    
	    $jml_detail  = 8;
	    $ourFileName = "mutasi-gudang.txt";
		
		$header = $this->mutasigudang->getHeader($nodok);
		$detail = $this->mutasigudang->getDetail($nodok);
		
		$note_header = substr($header->Keterangan,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,40);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            $arr_data["detail_satuan"][$counter] = $val["Satuan"];
            $arr_data["detail_keterangan"][$counter] = substr($val["Keterangan"],0,35);
			
			 $counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen ="MUTASI ANTAR GUDANG";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.= $arr_epson["cond"]."No : ".$header->NoDokumen; 
            $echo.="\r\n";
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->TglDokumen;
                
                $limit_spasi = 60;
                for($i=0;$i<($limit_spasi-strlen($header->TglDokumen));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Gudang Asal";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang Asal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->gudang_asal;
               
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Type";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Type"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->purpose;
                
                $limit_spasi = 60;
                for($i=0;$i<($limit_spasi-strlen($header->purpose));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Gudang Tujuan";
                
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang Tujuan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                $echo.=$arr_epson["cond"].$header->gudang_tujuan; 
                
                $echo.="\r\n";    
            }
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 45;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 14;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Keterangan";
            $limit_spasi = 35;
            for($i=0;$i<($limit_spasi-strlen("Keterangan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
            	$detail_keterangan = $arr_data["detail_keterangan"][$i_detail];
	            
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 45;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty);
                    $limit_spasi = 17;
                    for($i=0;$i<($limit_spasi-strlen($qty));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$detail_keterangan;
                    $limit_spasi = 35;
                    for($i=0;$i<($limit_spasi-strlen($detail_keterangan));$i++)
                    {
                        $echo.=" ";
                    }
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->Keterangan;
            
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Dibuat Oleh";

            $limit_spasi = 27;
            for($i=0;$i<($limit_spasi-strlen("Dibuat Oleh"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Hormat Kami";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Hormat Kami"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Diketahui Oleh";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Diketahui Oleh"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Diterima Oleh";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Diterima Oleh"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (  $user  )         (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"mutasi-gudang");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"mutasi-gudang");
			
			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "mutasi-gudang",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='mutasi-gudang-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}
	
}

?>