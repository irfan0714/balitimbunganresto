<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Permintaan_khusus extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/permintaan_khususmodel');
    }

    function index()
    {
      // echo "TES";
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_divisi"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "permintaan_khusus", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudung = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudung[1];
                    $search_divisi = explode("=", $arrSearch[2]); // search divisi
                    $arr_search["search"]["divisi"] = $search_divisi[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudung[1];
                    $data["search_divisi"] = $search_divisi[1];
                    $data["search_status"] = $search_status[1];
                }
            }

			foreach($gudang_admin as $key)
			{
				$arr_search["gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}

            $arr_search["search"]["gudang_admin"] = $arr_search["gudang_admin"];

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/permintaan_khusus/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/permintaan_khusus/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->permintaan_khususmodel->getGudang($arr_search["gudang_admin"]);
            $data['mdivisi'] = $this->permintaan_khususmodel->getDivisi();

            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->permintaan_khususmodel->num_permintaan_barang_row($arr_search["search"]);
            $data['data'] = $this->permintaan_khususmodel->getPermintaanBarangList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/permintaan_khusus/permintaan_baranglist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function otoritas()
    {
    	echo "Hello";
        /*$notransaksi     = $this->input->post('NoDokumen');
        $nodok           = 'PB';
        $tgltransaksi    = $this->input->post('Tanggal');
        $tipe            = $this->input->post('tipe');
        $username        = $this->session->userdata('username');
        $sysdate         = date("Y-m-d H:i:s");*/



        $this->load->view('transaksi/permintaan_khusus/form_otoritas');

    }



    function search()
    {
    	$mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'permintaan_khusus', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_divisi=".$this->input->post('search_divisi');
		$search_value .= "&search_status=".$this->input->post('search_status');

		$data = array(
            'query_string' => $search_value,
            'module' => "permintaan_khusus",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/permintaan_khusus/index/' . $query_id . '');
    }

    function add_new()
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";

            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');

            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
            $data["market_list"] = $this->permintaan_khususmodel->getMarketList($userlevel);

			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}

            $data['mgudang'] = $this->permintaan_khususmodel->getGudang($arr_search["list_gudang_admin"]);
            $data['mdivisi'] = $this->permintaan_khususmodel->getDivisi();
            $data['msatuan'] = $this->permintaan_khususmodel->getSatuan();

            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/permintaan_khusus/add_permintaan_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_permintaan_khusus($id)
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $userlevel = $this->session->userdata('userlevel');
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');
            $data['header'] = $this->permintaan_khususmodel->getHeader($id);
            $data['detail'] = $this->permintaan_khususmodel->getDetail($id);
            $data["market_list"] = $this->permintaan_khususmodel->getMarketList($userlevel);

			$user = $this->session->userdata('username');
            $gudang_admin = $this->globalmodel->getGudangAdmin($user);

			foreach($gudang_admin as $key)
			{
				$arr_search["list_gudang_admin"][$key["KdGudang"]] = $key["KdGudang"];
			}

            $data['mgudang'] 	= $this->permintaan_khususmodel->getGudang($arr_search["list_gudang_admin"]);

            $data['mdivisi'] = $this->permintaan_khususmodel->getDivisi();
            $data['msatuan'] = $this->permintaan_khususmodel->getSatuan();
            $data["ceknodok"] = $this->permintaan_khususmodel->cekNodok($id);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/permintaan_khusus/edit_permintaan_barang', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save_data()
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();
        $v_no_dokumen = $this->input->post('v_no_dokumen');
        $v_no_pb = $this->input->post('v_no_pb');

        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_est_terima = $this->input->post('v_est_terima');
        $v_divisi = $this->input->post('v_divisi');
        $v_gudang = $this->input->post('v_gudang');
        $v_keterangan = $this->input->post('v_keterangan');
        $v_status = $this->input->post('v_status');
        $flag = $this->input->post('flag');
        $base_url = $this->input->post('base_url');
        $user = $this->session->userdata('username');

        // detail
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_qty1 = $this->input->post('v_qty');
        $v_satuan1 = $this->input->post('v_satuan');

        // market
        $market_pcode1 = $this->input->post('market_pcode');
        $market_namabarang1 = $this->input->post('market_namabarang');
        $market_qty1 = $this->input->post('market_qty');
        $market_satuan1 = $this->input->post('market_satuan');

        list($xtahun, $xbulan, $xtgl) = explode('-',$mylib->ubah_tanggal($v_tgl_dokumen));

        if ($flag == "add")
		      {
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "permintaan_khusus_header","NoDokumen", "PK", $xbulan, $xtahun);
          $v_no_pb = "-";

            // $this->insertNewHeaderPB($v_no_pb, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $flag, $base_url, $user);

            $this->insertNewHeader($v_no_pb,$v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $flag, $base_url, $user);
          }
		    else if ($flag == "edit")
		      {
            //insert ke PB Klo Status Open
            if($v_status==1){
              $v_no_pb = $mylib->get_code_counter($this->db->database, "permintaan_barang_header","NoDokumen", "PB", $xbulan, $xtahun);
              $this->insertNewHeaderPB($v_no_pb, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $flag, $base_url, $user);
            }

            // echo $v_no_dokumen;die();

            $this->updateHeader($v_no_pb,$v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $mylib->ubah_tanggal($v_est_terima), $v_divisi, $v_gudang, $v_keterangan, $v_status, $flag, $base_url, $user);

            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
          }

        for ($x = 0; $x < count($v_pcode1); $x++)
        {
            $pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
            $v_namabarang = trim($v_namabarang1[$x]);
            $v_qty = $mylib->save_int($v_qty1[$x]);
            $v_satuan = $v_satuan1[$x];

            if ($pcode != "")
            {
            	if($v_qty*1>0)
            	{
					$detail = $this->permintaan_khususmodel->cekGetDetail($pcode,$v_no_dokumen);

            		//cocokan konversi terlebih dahulu
					$konversi = $this->permintaan_khususmodel->getKonversi($pcode,$v_satuan);

					//jika konversi ada datanya
					if(!empty($konversi)){
						//jika Satuan_To sama dengan Satuan yang dipilih
						if($konversi->Satuan_To==$v_satuan){
							$QtyPcs = $v_qty;
						//jika tidak sama
						}else{
							$QtyPcs = $v_qty * $konversi->amount;
						}
					//jika tidak ada datanya konversi
					}else{
						$QtyPcs = $v_qty;
					}

          //insert ke PB
          // echo $v_status;
          // echo $v_no_pb;
          // die();


          if($detail->PCode==$pcode)
					{
						$this->updateDetail($v_status,$flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs,$detail->Qty);
					}
					else
					{
						$this->insertDetail($flag,$v_no_dokumen,$pcode,$v_namabarang,$v_qty,$v_satuan,$QtyPcs);
					}
				}
            }
        }

        //insert ke PB
        if($v_status == '1'){

          $sql    = "Select * from permintaan_khusus_detail where NoDokumen = '$v_no_dokumen'";

          $qry = $this->db->query($sql);
          $result = $qry->result_array();
          foreach($result as $row){
            $this->insertDetailPB($v_no_pb,$row['PCode'],$row['NamaBarang'],$row['Qty'],$row['Satuan'],$row['QtyPcs']);
          }
        }


        redirect('/transaksi/permintaan_khusus/edit_permintaan_khusus/' . $v_no_dokumen . '');
    }

    function insertNewHeader($no_pb,$no_dokumen, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $flag, $base_url, $user)
    {
        $this->permintaan_khususmodel->locktables('permintaan_khusus_header');


        $data = array(
            'NoDokumen' => $no_dokumen,
            'NoPB' => $no_pb,
            'TglDokumen' => $tgl_dokumen,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );




        $this->db->insert('permintaan_khusus_header', $data);


        $this->permintaan_khususmodel->unlocktables();

        // return $no;
    }

    function insertNewHeaderPB($no_pb, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $flag, $base_url, $user)
    {
        $this->permintaan_khususmodel->locktables('permintaan_barang_header');


        $data = array(
            'NoDokumen' => $no_pb,
            'TglDokumen' => $tgl_dokumen,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'Status' => '1',
            'AddDate' => date('Y-m-d'),
            'AddUser' => $user,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );




        $this->db->insert('permintaan_barang_header', $data);


        $this->permintaan_khususmodel->unlocktables();

        // return $no;
    }

    function updateHeader($no_pb,$no_dokumen, $tgl_dokumen, $est_terima, $divisi, $gudang, $keterangan, $status, $flag, $base_url, $user)
    {
        $this->permintaan_khususmodel->locktables('permintaan_khusus_header,permintaan_khusus_detail');



        $data = array(
            'TglDokumen' => $tgl_dokumen,
            'NoPB' => $no_pb,
            'TglTerima' => $est_terima,
            'KdGudang' => $gudang,
            'KdDivisi' => $divisi,
            'Keterangan' => $keterangan,
            'Status' => $status,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
        );
        $this->db->where('NoDokumen', $no_dokumen);
        $this->db->update('permintaan_khusus_header', $data);
        // $this->db->update('permintaan_khusus_header', $data, array('NoDokumen' => NoDokumen));





        $this->permintaan_khususmodel->unlocktables();
    }

    function insertDetail($flag, $no_dokumen, $pcode, $namabarang, $qty, $satuan,$QtyPcs)
    {
        $this->permintaan_khususmodel->locktables('permintaan_khusus_detail');

        if ($pcode) {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'PCode' => $pcode,
                'NamaBarang' => $namabarang,
                'Qty' => $qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs
            );

            $this->db->insert('permintaan_khusus_detail', $data);
        }

        $this->permintaan_khususmodel->unlocktables();
    }

    function insertDetailPB($no_dokumen, $pcode, $namabarang, $qty, $satuan,$QtyPcs)
    {
        $this->permintaan_khususmodel->locktables('permintaan_barang_detail');

        if ($pcode) {
            $data = array(
                'NoDokumen' => $no_dokumen,
                'PCode' => $pcode,
                'NamaBarang' => $namabarang,
                'Qty' => $qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs
            );

            $this->db->insert('permintaan_barang_detail', $data);
        }

        $this->permintaan_khususmodel->unlocktables();
    }

    function updateDetail($status,$flag,$no_dokumen,$pcode,$namabarang,$qty,$satuan,$QtyPcs,$qty_tbl)
    {
    	$mylib = new globallib();

    	$new_qty = $mylib->save_int($qty)+$mylib->save_int($qty_tbl);

        $this->permintaan_khususmodel->locktables('permintaan_khusus_detail');

        if ($pcode)
        {
            $data = array(
                'NamaBarang' => $namabarang,
                'Qty' => $new_qty,
                'Satuan' => $satuan,
                'QtyPcs'=>$QtyPcs
            );

            $this->db->update('permintaan_khusus_detail', $data, array('NoDokumen' => $no_dokumen,'PCode' => $pcode));
        }

        $this->permintaan_khususmodel->unlocktables();
    }



    function delete_trans($id)
    {
        $data["cekdata"] = $this->permintaan_khususmodel->cekNodok($id);

        if ($data["cekdata"]->NoDokumen != "") {
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus data gagal, karena sudah dibuat PR', 'class' => 'danger'));
        } else {
            $this->db->delete('permintaan_khusus_header', array('NoDokumen' => $id));
            $this->db->delete('permintaan_khusus_detail', array('NoDokumen' => $id));
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        }

        redirect('/transaksi/permintaan_khusus/');
    }

    function delete_detail()
    {
        $sid = $this->uri->segment(4);
        $pcode = $this->uri->segment(5);
        $nodok = $this->uri->segment(6);

        $this->db->delete('permintaan_khusus_detail', array('sid' => $sid, 'PCode' => $pcode, 'NoDokumen' => $nodok));
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>PCode ' . $pcode . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/permintaan_khusus/edit_permintaan_khusus/' . $nodok . '');
    }

    function getrecipe(){
      $result = $this->globalmodel->getrecipe();
    	echo json_encode($result);
    }

    function pilihrecipe(){
      $result = $this->globalmodel->pilihrecipe();
    	echo json_encode($result);
    }

	function satuan()
    {
     $pcode = $this->input->post('pcode');
     $query = $this->permintaan_khususmodel->getSatuanDetail($pcode);
     $query2 = $this->permintaan_khususmodel->cekStatusBarang($pcode);

     foreach ($query2->result_array() as $cetak2) {
     	if($cetak2['Status']=='T'){
     	$msg_alert="PCode ".$pcode." Sudah Tidak Aktif";
		 echo "<script>alert('".$msg_alert."');</script>";
		 }
      }


     echo "<option value=''> -- Pilih --</option>";
     foreach ($query->result_array() as $cetak) {
	 echo "<option value=$cetak[Satuan]>$cetak[NamaSatuan]</option>";
      }
    }

    function cekbarang()
    {
     $pcode = $this->input->post('pcode');
     $query2 = $this->permintaan_khususmodel->cekStatusBarang($pcode);

     foreach ($query2->result_array() as $cetak2) {
     	if($cetak2['Status']=='T'){
		 $data['Nama'] = "";
		 }else{
		 $data['Nama'] = $cetak2['NamaLengkap'];
		 }
      }
      echo json_encode($data);
    }


	function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();

		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');


		$data["judul"]		= "PERMINTAAN KHUSUS";
		$data["header"] 	= $this->permintaan_khususmodel->getHeader($nodok);
		$data["detail"] 	= $this->permintaan_khususmodel->getDetail($nodok);
		$data["pt"] 		= $printlib->getNamaPT();

        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_pk', $data);
	}

	function doPrint()
	{
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();

		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";

		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();

		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();


		$total_spasi = 135;
	    $total_spasi_header = 80;


	    $jml_detail  = 8;
	    $ourFileName = "permintaan-khusus.txt";

		$header = $this->permintaan_khususmodel->getHeader($nodok);
		$detail = $this->permintaan_khususmodel->getDetail($nodok);

		$note_header = substr($header->Keterangan,0,40);

		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaBarang"],0,60);
            $arr_data["detail_qty"][$counter] = $val["Qty"];
            $arr_data["detail_satuan"][$counter] = $val["Satuan"];

			$counter++;
		}

        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);

        $nama_dokumen = "PERMINTAAN KHUSUS";

        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n";
                $echo.="\r\n";
            }

            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";

                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n";
            }
            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo .= $arr_epson["cond"].$nama_dokumen;

            $echo.="\r\n";

            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->NoDokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.= $arr_epson["cond"]."No : ".$header->NoDokumen;

            $echo.="\r\n";

            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->Tanggal;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Tanggal));$i++)
                {
                    $echo.=" ";
                }

                $echo.=$arr_epson["cond"]."Divisi";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Divisi"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->NamaDivisi;

                $echo.="\r\n";
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Tanggal Terima";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal Terima"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->TglTerima;

                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->TglTerima));$i++)
                {
                    $echo.=" ";
                }

                $echo.=$arr_epson["cond"]."Gudang";

                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Gudang"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";

                $echo.=$arr_epson["cond"].$header->NamaGudang;

                $echo.="\r\n";
            }

            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";


            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 7;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 75;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }

            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }

            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }

            $echo.="\r\n";

            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;

            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];

	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 7;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 75;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=number_format($qty,2,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,2,',','.')));$i++)
                    {
                        $echo.=" ";
                    }

                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 20;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }

				}
				$echo.="\r\n";
				$no++;

            }

            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";

            $echo .= $spasi_awal;
            $echo .= $arr_epson["cond"]."Note : ".$header->Keterangan;

            $echo .= "\r\n";
            $echo .= "\r\n";


            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.="Dibuat Oleh";

            $limit_spasi = 27;
            for($i=0;$i<($limit_spasi-strlen("Dibuat Oleh"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Hormat Kami";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Hormat Kami"));$i++)
            {
                $echo.=" ";
            }

            $echo.="Diketahui Oleh";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Diketahui Oleh"));$i++)
            {
                $echo.=" ";

            }

            $echo.="Diterima Oleh";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Diterima Oleh"));$i++)
            {
                $echo.=" ";
            }

            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }

            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }

            $echo.=" (  $user  )         (               )             (                 )         (                )";

            $echo .= "\r\n";
            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"permintaan-khusus");

			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);


			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);

			}

            $echo .= "\r\n";

			$TotalLogPrint = $this->globalmodel->getLogPrint($header->NoDokumen,"permintaan-barang");

			if($user!="hendri1003" && $user!="febri0202")
	        {
		        $data = array(
		            'form_data' => "permintaan-khusus",
		            'noreferensi' => $header->NoDokumen,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        }
		}

		$paths = "path/to/";
	    $name_text_file='permintaan-khusus-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);

		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;

	}

}

?>
