<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class pos_touch extends authcontroller {
	function __construct(){
        parent::__construct();
        error_reporting(E_ALL);
        $this->load->library('globallib');
        $this->load->model('transaksi/pos_touch_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
			//print_r($data);die();
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
	        $config['per_page']       = '50';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/pos_touch/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			$ipaddres                 = $this->session->userdata('ip_address');
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/pos_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/pos_touch/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("Tanggal","Table", "Total Item","Total Disajikan","From");
	        $config['total_rows']		= $this->pos_touch_model->num_order_row($id,$with, $ipaddres);
	        $data['data']	= $this->pos_touch_model->getList($config['per_page'],$page,$id,$with, $ipaddres);
			$data['tanggal'] = $this->pos_touch_model->getDate();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/pos_touch/pos_touch_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	function add_new($param='',$transaksi=''){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
    		$aplikasi = $this->pos_touch_model->getDate();
		    $ipaddres                   = $this->session->userdata('ip_address');
			$nokassa                    = $this->pos_touch_model->getnokassa($ipaddres);
			$datakassa                  = $this->pos_touch_model->getkassa($ipaddres);
			$data['NoKassa'] 			= $nokassa;
			$data['msg'] 				= "";
			$data['datakassa'] 			= $datakassa;
			$user_id = $this->session->userdata('userid');
			$username = $this->session->userdata('username');	
			$data['user_id'] 			= $user_id;
			$data['username'] 			= $username;
			$kddivisi                   = $datakassa[0]['KdKategori'];
			
			$data['barang']	 		    = $this->pos_touch_model->getBarang($kddivisi);
			$data['kategori']	 		= $this->pos_touch_model->getKategori();
			$data['subkategori']	 	= $this->pos_touch_model->getSubKategori();
			$data['userid']	 	        = $this->pos_touch_model->getUserId();
			$data['lokasi']	 	        = $this->pos_touch_model->getLokasi($kddivisi); 
			$data['keterangan']	 	    = $this->pos_touch_model->getKeterangan();
			$data['store']				= $this->pos_touch_model->aplikasi();
			$data['kurs1'] = $this->pos_touch_model->kurs('CNY');
			$data['kurs2'] = $this->pos_touch_model->kurs('USD');
			$data['tanggal'] = $aplikasi->TglTrans2;
			
			if($param!=''){
				if($transaksi == 'order-touch'){
					$data['dataorder']   = $this->pos_touch_model->getDetailOrder($param);
					$data['headerorder'] = $this->pos_touch_model->geHeaderOrder($param);
					$data['cek_save'] = "T";
				}else{
					$data['dataorder']   = $this->pos_touch_model->getDetailTrans($param);
					$data['headerorder'] = $this->pos_touch_model->getHeaderTrans($param);
					$data['cek_save'] = "T";
				}

			}else{
				$data['dataorder']   = array(array( 'KdMeja'=>'',
													'TotalGuest'=>0, 
													'KdAgent'=>'',
													'PCode'=>'',
													'NamaLengkap'=>'',
													'Qty'=>0,
													'Satuan'=>'',
													'Harga'=>0,
													'Diskon'=>0, 
													'Komisi'=>0, 
													'KDisc'=>0,
													'Berat'=>0,
													'DiscInternal'=>0));

				$data['headerorder'] = array(array( 'NoTrans'=>0,
													'NoStruk'=>'',
													'nilaidisc'=>0,
													'userdisc'=>'',
													'Voucher'=>0,
													'VoucherTravel'=>0));
				$data['cek_save'] = "T";
			}


			//----------------untuk keperluan lcd----
			$ipaddress			= $_SERVER['REMOTE_ADDR']; 
			$data['iplokal']	= $ipaddress;
			//---------------eo untuk keper....------

			// echo "<pre>";print_r($data['datakassa']);echo "</pre>";die;
			
			$this->load->view('transaksi/pos_touch/add_pos_touch',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	function edit($no){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$aplikasi = $this->pos_touch_model->getDate();
			$data['header'] = $this->pos_touch_model->getHeader($no);
			if($data['header']->FlagKonfirmasi=='T'&&$data['header']->TglDokumen2==$aplikasi->TglTrans){
				$data['detail'] = $this->pos_touch_model->getDetail($no);
				$data['qtybarcode'] = $aplikasi->QtyBarcode;
				$hasil = $this->pos_touch_model->FindKendaraan($data['header']->KdKendaraan,"edit",$no);
				$string = "";
				if(!empty($hasil)){
					for($p=0;$p<count($hasil);$p++)
					{
						$string .= $hasil[$p]['NoAmbil']."**";
					}
				}
				$data['noambilkendaraan'] = $string;
				$this->load->view('transaksi/pos_touch/edit_pos_touch',$data);
			}
			else
			{
				redirect('/transaksi/pos_touch/');
			}
		}
		else{
			$this->load->view('denied');
		}
    }
	
	function findBarcode()
	{
		$barc = $this->input->post("barcode");
		$hasil = $this->pos_touch_model->FindBar($barc);
		$string = "";
		if(!empty($hasil))
		{
			$string = $hasil->PCode;
		}
		echo $string;
	}
	
	function VoucherCustomer($id_voucher)
    {
        $this->pos_touch_model->voucher($id_voucher);
    }
    
    function ketentuandisc($pcode)
    {
        $disc = $this->pos_touch_model->ketentuandisc($pcode);
        echo $disc;
    }
    
	function hargagofood($pcode) {
		$harga = $this->pos_touch_model->hargagofood($pcode);
		echo $harga;
	}

	function hargagrabfood($pcode) {
		$harga = $this->pos_touch_model->hargagrabfood($pcode);
		echo $harga;
	}

	function hargatokopedia($pcode) {
		$harga = $this->pos_touch_model->hargatokopedia($pcode);
		echo $harga;
	}

	function hargashopee($pcode) {
		$harga = $this->pos_touch_model->hargashopee($pcode);
		echo $harga;
	}

	function hargatraveloka($pcode) {
		$harga = $this->pos_touch_model->hargatraveloka($pcode);
		echo $harga;
	}
    
    function groupdisc($namacard, $store, $pcode)
    {
        $persen_disc = $this->pos_touch_model->getgroupdisc($namacard, $store, $pcode);
        echo $persen_disc;
    }
	
	function getRealPCode()
	{
		$kode = $this->input->post('pcode');
		//echo $kode;
		$valpcode = $this->pos_touch_model->ifPCodeBarcode($kode);
		//echo count($valpcode);
		if(count($valpcode)!=0)
		{
			$pcode = $valpcode->PCode.'*&^%'.$valpcode->Jenis.'*&^%'.$valpcode->NamaInitial."*&^%".$valpcode->KonversiJualKecil."*&^%".$valpcode->KonversiBesarKecil."*&^%".$valpcode->KonversiTengahKecil."*&^%".$valpcode->KdSatuanJual."*&^%".$valpcode->NamaSatuan;
		}
		else
		{
			$pcode = "";
		}
		echo $pcode;
	}
	
	function getPCode()
	{
		$kode = $this->input->post('pcode');
		$valpcode = $this->pos_touch_model->ifPCodeBarcode($kode);
		if(count($valpcode)!=0)
		{
			$detail = $this->pos_touch_model->getPCodeDet($pcode);
			$nilai = $detail->NamaInitial."-".$detail->KonversiJualKecil."-".$detail->KonversiBesarKecil."-".$detail->KonversiTengahKecil."-".$detail->KdSatuanJual."-".$detail->NamaSatuan."-".$detail->PCode."-".$jenis_kode;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}
	function getcontact()
	{
		$kode = $this->input->post('pelanggan');
		$detail = $this->pos_touch_model->getcontact($kode);
		if(count($detail)!=0)
		{
			$nilai = $detail->Nama;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}
	
	function getpersonal()
	{
		$kode = $this->input->post('personal');
		$detail = $this->pos_touch_model->getpersonal($kode);
		if(count($detail)!=0)
		{
			$nilai = $detail->NamaPersonal;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}
	
	function insert_temporary()
	{
		$EditFlg    = $this->input->post('EditFlg');
		$NoUrut     = $this->input->post('NoUrut');
		$pcode      = $this->input->post('pcode');
		$qty        = $this->input->post('qty');
		$satuan     = $this->input->post('satuan');
		$keterangan = $this->input->post('keterangan');
		$notrans    = $this->input->post('notrans');
		$kassa   = $this->input->post('kassa');
		$kasir   = $this->input->post('kasir');
		$store	 = $this->input->post('store');
		$tgltrans = $this->session->userdata('Tanggal_Trans');
		//echo "aaaaa";
				
		if($notrans == "--")
		{
		   //print_r($NoTrans);
		   $Trans = $this->pos_touch_model->no_trans($kassa);
		   $notrans = $Trans[0]['NoTrans'];
		   //print_r($NoTrans);die();
		}	
		if($EditFlg == 1)
		{
			$this->pos_touch_model->DeleteRecord($NoUrut,$kassa);
		}
		$hasil = $this->pos_touch_model->order_temp_cek($pcode,$kassa);		
		if($hasil==0) // or $qty<1)
		{
				if($EditFlg == 0)
				{
					$sql = "select NoUrut+1 as NoUrut from order_temp where NoKassa='$kassa' order by NoUrut desc limit 1";
					$qry = $this->db->query($sql);
					$row = $qry->result_array();
					
					if($qry->num_rows() == 0)
					{
						$NoUrut = 1;
					}
					else
					{
						$NoUrut = $row[0]['NoUrut'];
					}
				}
				$data = array(
						'NoUrut'       => $NoUrut,
						'NoTrans'	   => $notrans,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Satuan'       => $satuan,
						'Keterangan'   => $keterangan,
						'NoKassa'	   => $kassa,
						'Kasir'		   => $kasir,
						'Tanggal'	   => $tgltrans,
						'Waktu'		   => date('H:i:s'),
						'KdStore'	   => $store,
						'Status'	   => 1
						);
						
						$this->db->insert('order_temp', $data);		
		}
		else
		{
			$this->pos_touch_model->order_temp_add($qty,$pcode,$notrans,$kassa,$keterangan);
		}	
		echo $notrans;
	}
	function cetak()
	{
		$data = $this->varCetak();	
		$this->load->view('transaksi/pos_touch/cetak',$data);
	}
	function cetak2()
	{
		$data = $this->varCetak2();	
		$this->load->view('transaksi/pos_touch/cetak',$data);
	}
	function printThis()
	{
		$nokassa = $this->uri->segment(4);
		$notrans = $this->uri->segment(5);
		$hasil = $this->pos_touch_model->getprintjob($nokassa);	
		$PrintLangsung = true;
		if(count($hasil)>0)
		{
		    if($hasil[0]['jenisprint']=='1')
			{
			    $PrintLangsung = false;
				for($s=count($hasil)-1;$s>=0;$s--)
				{
					switch($hasil[$s]['jenis'])
						{
							case ($hasil[$s]['jenis'] == 1) : print("Ini Makanan!");
							break;
							case ($hasil[$s]['jenis'] == 2) : print("Ini Minuman!");
							break;
							case ($hasil[$s]['jenis'] == 3) : print("Ini Semua!");
							break;
							default : print("None!");
						}
						/*if(($hasil[$s]['jenis'])=='3')
						{
							$data = array(
								'nokassa'       => $hasil[$s]['kassa2print'],
								'lpt'	   => $hasil[$s]['lpt'],
								'jenis'        => $hasil[$s]['jenis'],
								'nokassadokumen'  => $nokassa,
								'nodokumen'       => $notrans,
								'jenisdokumen'       => "SO"
							);			
							$this->db->insert('printjob', $data);
						}
						if(($hasil[$s]['jenis'])=='1')
						{
							print("Ini Makanan!");
							//$data = $this->varCetakMakanan($nokassa,$notrans);
							//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so3', $data);
						}
						if(($hasil[$s]['jenis'])=='2')
						{
							print("Ini Minuman!");
							//$data = $this->varCetakMakanan($nokassa,$notrans);
							//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so3', $data);
						}*/
				}
			}
		}
		if($PrintLangsung)
		{
			$data = $this->varCetak();
			$data['fileName'] = "order.sss";   
			$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so', $data);
		}
	}
	function printThis2()
	{
		$data = $this->varCetak2();
		$data['fileName'] = "order.ccc";   
        //echo "masuk";die();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so', $data);
		//$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_so2', $data);
	}
	function varCetak()
	{
	    $nokassa = $this->uri->segment(4);
		$notrans = $this->uri->segment(5);
	    $data['store']		= $this->pos_touch_model->aplikasi();
		$data['header']		= $this->pos_touch_model->all_trans($nokassa,$notrans);
		$data['detail']		= $this->pos_touch_model->det_trans($nokassa,$notrans);
		$data['url'] = "transaksi/pos_touch/printThis/".$nokassa."/".$notrans;
		return $data;
	}
	
	function varCetakMakanan($kassa,$notrans)
	{
	    //$nokassa = $this->uri->segment(4);
		//$notrans = $this->uri->segment(5);
	    $data['store']		= $this->pos_touch_model->aplikasi();
		$data['header']		= $this->pos_touch_model->all_trans($kassa,$notrans);
		$data['detail']		= $this->pos_touch_model->det_trans_food($kassa,$notrans);
		$data['url'] = "transaksi/pos_touch/printThis/".$kassa."/".$notrans;
		return $data;
	}
	
	function varCetak2()
	{
	    $nokassa = $this->uri->segment(4);
		$notrans = $this->uri->segment(5);
	    $data['store']		= $this->pos_touch_model->aplikasi();
		$data['header']		= $this->pos_touch_model->all_trans($nokassa,$notrans);
		$data['detail']		= $this->pos_touch_model->det_trans($nokassa,$notrans);
		$data['url'] = "transaksi/pos_touch/printThis2/".$nokassa."/".$notrans;
		return $data;
	}

	function save_trans()
	{
	    $transaksi 		= $this->input->post('transaksi');
		if($transaksi=="yes")
		{
			$no_struk    = $this->input->post('no_struk');
			if($no_struk!=""){
				$this->db->delete('transaksi_header',array('NoStruk'=>$no_struk));
				$this->db->delete('transaksi_detail',array('NoStruk'=>$no_struk));
				$this->db->delete('transaksi_detail_bayar',array('NoStruk'=>$no_struk));
			}

			if($this->input->post("ajax_post")){
				$trans_status = "0";
			}else{
				$trans_status = "1";
			}


			$EditFlg    = $this->input->post('EditFlg');
			$nourut1    = $this->input->post('nourut');
			$pcode1     = $this->input->post('pcode');
			$qty1       = $this->input->post('qty');
			$harga1     = $this->input->post('harga');
			$disc1     = $this->input->post('disc');
			$netto1     = $this->input->post('netto');
			$satuan1    = $this->input->post('satuan');
			$keterangan1= $this->input->post('keterangan');
			$printer1    = $this->input->post('printer');
			$komisi1    = $this->input->post('komisi');
			$notrans    = $this->input->post('notrans');
			$kassa   = $this->input->post('kassa');
			$kasir   = $this->input->post('kasir');
			$store	 = $this->input->post('store');
			$idbayar	 = $this->input->post('idbayar');
			$idnilaibayar	 = $this->input->post('idnilaibayar');
			$idnilaidiscount = $this->input->post('idnilaidiscount');
			$idbayartunai	 = $this->input->post('idbayartunai');
			$idbayargopay	 = $this->input->post('idbayargopay');
			$idbayarkredit	 = $this->input->post('idbayarkredit');
			$idbayardebet	 = $this->input->post('idbayardebet');
			$idbayarvoucher	 = $this->input->post('idbayarvoucher');
			$idbayarpoint	 = $this->input->post('idbayarpoint');
			$idvoucher	 	 = $this->input->post('idvoucher');
			$idcompliment	 = $this->input->post('idcompliment');
			$iscounter		 = $this->input->post('iscounter');
			$kdmember		 = $this->input->post('kdmember');
			$namacard		 = $this->input->post('namacard');
			$nocard		 	 = $this->input->post('nocard');
			
			$userdisc = $this->input->post('iduserdiscount');
			$nilaidisc = $this->input->post('idpersendiscount');
			
			$id1kurs = $this->input->post('id1kurs');
			$id2kurs = $this->input->post('id2kurs');
			$valas1 = $this->input->post('idnilai1valas');
			$valas2 = $this->input->post('idnilai2valas');
			$fromordertouch = $this->input->post('FromOrderTouch');
			$orderid = $this->input->post('OrderID');

			$kartubankkredit = $this->input->post('kartubankkredit');
			$edcbankkartukredit = $this->input->post('edcbankkartukredit');
			$kartubankdebet = $this->input->post('kartubankdebet');
			$edcbankkartudebet = $this->input->post('edcbankkartudebet');
			
			$idkurs = 0;
			$idvalas = "";
			$valas = 0;
			$idbayar2 = "";
			
			if($valas1<>0)
			{
				$idkurs = $id1kurs;
				$idvalas = "CNY";
				$valas = $valas1;
				$idbayar2 = "L";
			}
			if($valas2<>0)
			{
				$idkurs = $id2kurs;
				$idvalas = "USD";
				$valas = $valas2;
				$idbayar2 = "L";
			}
			
			$idcustomer = $idvoucher;
		    if($idbayar=='C')
			{
			   $idcustomer = $idcompliment;
			}
			if($idbayar=='U')
				$idbayar = 'L'; //seragamkan dengan pos.
			else
				$idbayar = $idbayar.$idbayar2;
			
			$listvoucher = $this->input->post('listvoucher');
			$listvouchpakai = $this->input->post('listvouchpakai');
			$listjenis = $this->input->post('listjenis');
			$listphoto = $this->input->post('listkodephoto');
			
			$voucher1    = explode('##',$listvoucher);
			$vouchpakai1 = explode('##',$listvouchpakai);
			$vouchjenis1 = explode('##',$listjenis);
			$kdphoto1 = explode('##',$listphoto);
			
			$kategorikassa	 = $this->input->post('kategorikassa');
			$tgltrans = $this->session->userdata('Tanggal_Trans');
			$userid = $this->session->userdata('userid');
			$username = $this->session->userdata('username');
			$tahun = substr($tgltrans,0,4);
			$bulan = substr($tgltrans,5,2);
			$adddate        = date('y-m-d');
			//$Trans = $this->pos_touch_model->no_trans($bulan,$tahun,$kategorikassa);
			//$notrans = $Trans[0]['NoTrans'];
			if($no_struk!=""){
				$notrans = $no_struk;
			}else{
				$notrans = $this->pos_touch_model->ambil_No($tahun, $bulan, $tgltrans,$kassa);
			}
		    

			//$nokassa 		= $this->input->post('kassa');
			//$totalnya 		= $this->input->post('total_biaya');
			//$pelanggan	    = $this->input->post('pelanggan');
			$idmeja	        = $this->input->post('idtable');
			$idguest        = $this->input->post('idguest');
			$idagent       = $this->input->post('idagent');
			if($idguest=="")$idguest=0;
			$personal	    = $this->input->post('idpersonal');
			$jenisprint	    = $this->input->post('jenisprint');
			
			$total	    = $this->input->post('totalsales');
			$sc	    = $this->input->post('scsales');
			$tax	    = $this->input->post('taxsales');
			$nettosales_   = $this->input->post('nettosales_');
			$nettosales   = $this->input->post('nettosales');
			$brutosales   = $this->input->post('brutosales');
			$Jamsekarang = date("H:i:s");
			
			if($iscounter==1){
				$scptg = 0;
				$dpp = $total / 1.1;
				$tax = $total - $dpp;
			}else if(substr($idmeja,0,2)=='GF'){
				$scptg=0;
				$dpp =$total;
			}else{
				$scptg = 8;
				$dpp = $total;
			}
			
			$total_sc =0;
			for($x0=0;$x0<count($pcode1);$x0++)
			{
				$pcode = strtoupper(addslashes(trim($pcode1[$x0])));
				$nourut = $nourut1[$x0];
				$qty = trim($qty1[$x0]);
				$harga = trim($harga1[$x0]);
				$disc = trim($disc1[$x0]);
				$netto = trim($netto1[$x0]);
				$satuan = $satuan1[$x0];
				$keterangan = $keterangan1[$x0];
				$komisi = $komisi1[$x0];
				$printer = $printer1[$x0];
				
				if($idcustomer!='')
				{
					$komisi = 0; //voucher karyawan dan compliment tidak ada komisi.
				}

				#cek SC dan PPN
				$nil_SC_PPN = $this->pos_touch_model->getServiceCharge($pcode);

				$nil_sc = $nil_SC_PPN['SC'];
				$nil_ppn = $nil_SC_PPN['PPN'];

				if($pcode!="" && $qty>0){
					$data = array(
						//'NoUrut'       => $nourut,
						'NoKassa'	   => $kassa,
						'Gudang'	   => "00",
						'NoStruk'	   => $notrans,
						'Tanggal'	   => $tgltrans,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Harga'        => round($harga,2),
						'Netto'        => round($netto,2),
						'Disc1'		   => round($disc,2),
						'Status'       => $trans_status,
						'Service_charge'  => $nil_sc,
						'PPN'          => $nil_ppn,
						'komisi'       => $komisi,
						'printer'      => $printer,
						'KdMeja'	   => $idmeja,
						'KdAgent'	   => $idagent
					);
				    $this->db->insert('transaksi_detail', $data);
				}

				$sc = (($harga * $qty)-$disc)*($nil_sc/100);
				$total_sc += $sc;

			}
			
			$data = array(
						'NoKassa'	   => $kassa,
						'Gudang'	   => "00",
						'NoStruk'	   => $notrans,
						'Tanggal'	   => $tgltrans,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'TotalItem'    => $x0,
						'TotalNilaiPem'=> $nettosales_,
						'TotalNilai'   => $nettosales,
						'TotalBayar'   => $idnilaibayar,
						'Kembali'      => $idnilaibayar-$nettosales,
						'Tunai'        => $idbayartunai,
						'KKredit'      => $idbayarkredit,
						'KDebit'       => $idbayardebet,
						'GoPay'		   => $idbayargopay,
						'Voucher'      => $idbayarvoucher,
						'Discount'     => $idnilaidiscount,
						'BankDebet'	   => $kartubankdebet, 
						'EDCBankDebet' => $edcbankkartudebet, 
						'BankKredit'   => $kartubankkredit, 
						'EDCBankKredit'=> $edcbankkartukredit, 
						'Status'       => $trans_status,
						'Ttl_charge'  => $total_sc,
						'DPP'          => $dpp+$sc,
						'TAX'          => $tax,
						'KdMeja'	   => $idmeja,
						'KdAgent'	   => $idagent,
						'KdCustomer'   => $idcustomer,
						'userdisc' => $userdisc,
						'nilaidisc' => $nilaidisc,
						'valas' => $valas,
						'kurs' =>  $idkurs,
						'valuta' => $idvalas,
						'TotalGuest'=>$idguest,
						'Point' => $idbayarpoint,
						'KdMember' => $kdmember,
						'NamaCard' => $namacard,
						'NoCard' => $nocard
					);
			$this->db->insert('transaksi_header', $data);
			
			if($vouchjenis1[0]=="5"){
				$this->db->update('transaksi_header',array('Voucher'=>0,'VoucherTravel'=>$idbayarvoucher),array('NoStruk'=>$notrans));
			}
			
			$data = array(
			
						'NoKassa'	   => $kassa,
						'Gudang'	   => "00",
						'NoStruk'	   => $notrans,
						'Jenis'	       => $idbayar,
						'NilaiTunai'       => $idbayartunai,
						'NilaiKredit'      => $idbayarkredit,
						'NilaiDebet'       => $idbayardebet,
						'NilaiVoucher'     => $idbayarvoucher,
						'NilaiGoPay'	     => $idbayargopay,
						'NilaiPoint'	     => $idbayarpoint,
						'KdMember'			=> $kdmember,
						'Currency' => 'IDR-1',
						'valas' => $valas,
						'kurs' =>  $idkurs,
						'valuta' => $idvalas
					);
			$this->db->insert('transaksi_detail_bayar', $data);
			
			if(!$this->input->post("ajax_post")){
				$mmaks = count($voucher1);
				for($m=0;$m<$mmaks-1;$m++)
				{
					$voucher0 = $voucher1[$m];
					$vouchpakai0 = (int)$vouchpakai1[$m];
					$vouchjenis0 = $vouchjenis1[$m];
					$this->pos_touch_model->do_simpan_voucher($notrans,$kassa,$tgltrans,$voucher0,$vouchpakai0,$vouchjenis0);
				}
				
				$photocnt = count($kdphoto1);
				for($m=0;$m<$photocnt-1;$m++)
				{
					$kdphoto0 = $kdphoto1[$m];
					$this->pos_touch_model->do_simpan_photo($notrans,$kassa,$tgltrans,$kdphoto0);
				}
				
				if($idbayar=='C')
			    {
					$this->pos_touch_model->do_simpan_compliment($idcompliment,$brutosales);
			    }
			    
			    if($fromordertouch==1){
					$this->pos_touch_model->updateorder($orderid, $notrans);
				}
				
				if($kdmember<>''){
					$this->pos_touch_model->updatepointmember($kdmember, $nettosales, $idbayarpoint,$notrans);
				}
			}
			
	   		$ipaddres          = $this->session->userdata('ip_address');
			$printer = $this->pos_touch_model->NamaPrinter($ipaddres);

			$ip = $printer[0]['ip'];
			$nm_printer = $printer[0]['nm_printer'];
			$kdkategori = $printer[0]['KdKategori'];
			$store = $this->pos_touch_model->aplikasi();
			$kurs1 = $this->pos_touch_model->kurs('CNY');
			$kurs2 = $this->pos_touch_model->kurs('USD');
		    
			if($this->input->post("ajax_post")){
				echo json_encode(array('no_struk'=>$notrans,'cek_save'=>'Y')); die();
			}else{
				$this->konfirm_cetak($notrans,$kassa,$idmeja);
			}
		} 
		else
	       $this->index();
	    
	}
	
	function konfirm_cetak($notrans,$kassa){
		$data['notrans'] = $notrans;
		$data['kassa'] = $kassa;
		$this->load->view('transaksi/pos_touch/pos_touch_print', $data);
	}
	
	function cetakstruk($notrans,$kassa){
		$store = $this->pos_touch_model->aplikasi();
		$data['store'] = $store;
		$cek = $this->pos_touch_model->det_trans($notrans,$kassa);
		
		if(!empty($cek)){
			$res = $cek;
		}else{
			$res = $this->pos_touch_model->det_trans_second($notrans,$kassa);
		}
		
		$data['header'] = $this->pos_touch_model->all_trans($notrans,$kassa);
		if($data['header'][0]['KdMember'] <> ''){
			$point = $this->pos_touch_model->get_point($notrans,'I');
		}else{
			$point =0;
		}
		if($data['header'][0]['KdCustomer'] != ''){
			#dikomen data voucher karyawan belum bener
			$sisaVoucherEmployee = $this->pos_touch_model->get_voucher_employee($data['header'][0]['KdCustomer'],$data['header'][0]['Tanggal']);

		}else{
			$sisaVoucherEmployee = array();
		}
		// echo "<pre>";print_r($sisaVoucherEmployee);die();
		
		$data['GetSisaVoucherEmployee'] = $sisaVoucherEmployee; 
		$data['GetPoint'] = $point;
		$data['detail'] = $res;
		$this->load->helper('print');
		if($data['header'][0]['IsCounter']==1)
			$html = $this->load->view('transaksi/pos_touch/cetak_transaksi_counter_wireness_dos', $data, TRUE);
		else
			$html = $this->load->view('transaksi/pos_touch/cetak_transaksi_wireness_dos', $data, TRUE);
		
		$filename	='ptouch';
		$ext		='ctk';
		//header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		//header("Content-Transfer-Encoding: binary");
		//header('Expires: 0');
		//header('Pragma: no-cache');
		
		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;
	}
	
	function cetakNoRek(){
		$store = $this->pos_touch_model->aplikasi();
		$data['namaBank'] = "BCA";
		$data['namaRekening'] = "Bali Boga Natura";
		$data['noRekening'] = "288-0505087";
		$this->load->helper('print');
		$html = $this->load->view('transaksi/pos_touch/cetak_norek', $data, TRUE);
		$filename	='rekening';
		$ext		='ctk';
		
		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;
	}

	function cetakWifi(){
		$store = $this->pos_touch_model->aplikasi();
		$data['accessPoint'] = "balitimbungan";
		$data['password'] = "balitimbungan";
		$this->load->helper('print');
		$html = $this->load->view('transaksi/pos_touch/cetak_wifi', $data, TRUE);
		$filename	='wifi';
		$ext		='ctk';
		
		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;
	}

	function clear_trans()
	{
	    $notrans 		= $this->input->post('notrans');
		$nokassa 		= $this->input->post('kassa1');
		//echo $notrans;
		//echo $nokassa;
		$this->pos_touch_model->clear_trans($notrans,$nokassa);
		$this->add_new();
	}
	function preview($parameter)
	{
		$store = $this->pos_touch_model->aplikasi();
		$data['store'] = $store;
		$param  = explode('bxbxb',urldecode($parameter));
		$header = $param[0];
		$detail = $param[1];
		
		$dheader = explode("_",$header);
		$ddetail = explode("axaxa",$detail);
		// echo "<pre>";print_r($detail);die();
		$data['header'][0]['NoKassa'] = $dheader[0];
		$data['header'][0]['NoTrans'] = $dheader[10];
		$data['header'][0]['Tanggal'] = date('y-m-d');
		$data['header'][0]['Waktu']   = date('H:i:s');
		$data['header'][0]['Kasir']   = $dheader[1];
		$data['header'][0]['TotalItem']  = count($ddetail)-1;
		$data['header'][0]['TotalRupiah'] = $dheader[3];
		$data['header'][0]['PPnPersen'] = 10; //$dheader[4]/($dheader[3]-$dheader[5]+$dheader[8])*100;
		$data['header'][0]['TAX'] = $dheader[4];
		$data['header'][0]['Ttl_Charge'] = $dheader[6];		
		$data['header'][0]['TotalNilai'] = $dheader[5];
		$data['header'][0]['IDAgent'] = $dheader[8];
		$data['header'][0]['Voucher'] = $dheader[9];
		$dguest = explode('_gxgxg_',$header);
		$data['header'][0]['TotalGuest'] = $dguest[1];
		$dmeja = explode('_mxmxm_',$header);
		$data['header'][0]['KdMeja'] = $dmeja[1];
		
		//--------------------------------------------
			$nettofull=0;
		for($m=0;$m<count($ddetail)-1;$m++)
		{
		    $ddetail1 = explode("_",$ddetail[$m]);
			$discount	= $dheader[7]+0;
			
			$qty	= $ddetail1[2];
			$satuan	= $ddetail1[3];
			$harga	= $ddetail1[4] ; //- ($discount/100 * $ddetail1[4]);
			$netto	= $qty*$harga;
			$disc = $ddetail1[5];
			$pdisc = $ddetail1[6];
			
		    $data['detail'][] = array( 'PCode' => $ddetail1[0],             
      								   'NamaStruk' => $ddetail1[1],
									   'Qty' => $qty,
									   'Satuan' => $satuan,
									   'Harga' => $harga,
									   'Netto' => $netto,
									   'Disc' => $disc, 
									   'PDisc' => $pdisc);
			$nettofull +=$netto;
		}
		//$sc	= (5/100) * $nettofull;
		
		//$tax	= ($nettofull + $sc) / 10;
		//$data['header'][0]['TAX'] = $tax;
		//$data['header'][0]['Ttl_Charge'] = $sc;	
		$this->load->helper('text');
		$data['store']		= $this->pos_touch_model->aplikasi();
		
		$nokassa = $dheader[0];	
        $datakassa  = $this->pos_touch_model->getnoipkassa($nokassa);	
        $data['reset']  =chr(27).'@';
		$data['plength']=chr(27).'C';
		$data['lmargin']=chr(27).'l';
		$data['cond']   =chr(15);
		$data['ncond']  =chr(18);
		$data['dwidth'] =chr(27).'!'.chr(24);
		$data['ndwidth']=chr(27).'!'.chr(14);
		$data['draft']  =chr(27).'x'.chr(48);
		$data['nlq']    =chr(27).'x'.chr(49);
		$data['bold']   =chr(27).'E';
		$data['nbold']  =chr(27).'F';
		$data['uline']  =chr(27).'!'.chr(129);
		$data['nuline'] =chr(27).'!'.chr(1);
		$data['dstrik'] =chr(27).'G';
		$data['ndstrik']=chr(27).'H';
		$data['elite']  ='';
		$data['pica']   =chr(27).'P';
		$data['height'] =chr(27).'!'.chr(16);
		$data['nheight']=chr(27).'!'.chr(1);
		$data['spasi05']=chr(27)."3".chr(16);
		$data['spasi1'] =chr(27)."3".chr(24);
		$data['fcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';
		$data['pcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';
		$data['op_cash']=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);
		$data['nl']	= "\r\n";
		$data['ip'] = $datakassa[0]['ip'];
		$data['nm_printer'] = $datakassa[0]['nm_printer'];	
		$data['kdkategori'] = $datakassa[0]['KdKategori'];
		
		$html = $this->load->view('transaksi/pos_touch/cetak_transaksi_preview_dos', $data, TRUE);
		
		$filename	='ptouch';
		$ext		='ctk';
		//header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		//header("Content-Transfer-Encoding: binary");
		//header('Expires: 0');
		//header('Pragma: no-cache');
		
		header('Content-Type: application/ctk');
		header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');
		print $html;
	}
	
	function cektable(){
		$kdmeja = $this->input->post('data');
		$tgl = date('y-m-d');
		$OpenOrder = $this->pos_touch_model->getOpenOrder($tgl, $kdmeja);
		echo json_encode($OpenOrder);
	}

	function cekServiceCharge($pcode)
    {
        $KdGroupBarang = $this->pos_touch_model->getServiceCharge($pcode);
        echo json_encode($KdGroupBarang);
    }
}