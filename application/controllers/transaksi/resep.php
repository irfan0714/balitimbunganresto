<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Resep extends authcontroller {

	public function __construct()
	{
		parent::__construct();
        $this->load->library('globallib');
        $this->load->library('printreportlib');
        $this->load->model('transaksi/resep/resep_model');      
	}

	public function index()
	{
		$mylib = new globallib();
        $sign = $mylib->getAllowList("all");

        if ($sign == "Y") {
         	$segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);


        	$menu = $this->input->post('menu');
        	$data['judul']			= "Resep";

        	if($menu=='')
        	{
        		$data['data'] = $this->resep_model->getMenu();
        	}
        	else
        	{
        		$data['data'] = $this->resep_model->getmenusearch($menu);
        	}

        	$data['samedit'] = $this->resep_model->dataSamEdit();
			$this->load->view('transaksi/resep/view',$data);
		}
		else
		{
			$this->load->view('denied');
		}
	}


	function add_new() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");

        if ($sign == "Y") {

            $data['judul']  = "Form Add Resep";
            $data['tampil'] = FALSE;
            $data['track']  = $mylib->print_track();
            $data['namabarang'] = '';
            $data['PCode']	='';
		    $data['error'] = "";
            $data['nama'] = $this->resep_model->getnama();

            $this->load->view('transaksi/resep/add_resep', $data);
        } 
        else 
        {
            $this->load->view('denied');
        }
    }

    function show()
	{
		$mylib = new globallib();
        $sign = $mylib->getAllowList("add");

        $pcode = $this->input->post('pcodetouch');
        $nama = $this->input->post('nama');
        $kode = $this->input->post('kode');
        $harga = $this->input->post('harga');
        $satuan = $this->input->post('satuan');
        $isi = $this->input->post('isi');
        $submit = $this->input->post('submit');

        if ($sign = "Y") 
        {
        	if($submit)
        	{
        		$cek = mysql_num_rows(mysql_query("SELECT resep_id, resep_header.Pcode, NamaLengkap FROM resep_header
													INNER JOIN masterbarang_touch
													ON masterbarang_touch.`PCode` = resep_header.`Pcode`
													where NamaLengkap = '$nama'
													ORDER BY NamaLengkap ASC"));

        		if($cek)
        		{
        			
		        	 	$data['judul']      = "Form Add Resep";
		        		$data['namabarang'] = $nama;
		       			$data['PCode'] 		= $pcode;
		       			$data['error'] 		= "* Menu Sudah Ada Silahkan Masukkan Menu Yang Lain *";
		        		//$data['kode']		= $kode;
		        		$data['kolom'] 		= '';
		        		$data['tampil'] 	= FALSE;

						$this->load->view('transaksi/resep/add_resep',$data);
        		}
        		else
        		{
		        	if($nama!="")
		        	{

		        	 	$data['judul']      = "Form Add Resep";
		        		$data['namabarang'] = $nama;
		       			$data['PCode'] = $pcode;
		        		//$data['kode']		= $kode;
		        		$data['kolom'] = '';
		        		$data['tampil'] = TRUE;
		        		$data['namaresep']='';

		       			$data['error'] = "";

		       			$data['errorkode'] = "";

		        		$data['data'] = $this->resep_model->getnamamaster($nama);

		        		
		        		//$data['datalast'] = $this->resep_model->getlast();
		        		

						$this->load->view('transaksi/resep/add_resep',$data);
		       		}
		       		else
		       		{
		        	 	$data['judul']      = "Form Add Resep";
		       			$data['tampil'] = FALSE;
		       			$data['namabarang'] = $nama;

		       			$data['error'] = "";

		       			$data['PCode'] = $pcode;
		        		//$data['kode']		= $kode;
		       			$this->load->view('transaksi/resep/add_resep',$data);
		       		}
		       	}
	       	}
       	}
    	else
        {
            $this->load->view('denied');
	    }

	}

	function getlast($pcode)
	{
		$row  = $this->resep_model->getlast($pcode);

		//print_r($row);

		echo json_encode($row);
				
		/*$data['num'] = count($row);
		$data['row'] = $row;
		 	
		$this->load->view('transaksi/resep/add_resep',$data);*/



		//$data['datalast'] = $this->resep_model->getlast();
	}

	function getsatuan($pcode, $satuan)
	{	
		$row  = $this->resep_model->get_isi_perpack($pcode, $satuan);

		//print_r($row);die;

		echo json_encode($row);
	}

    function edit() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");

        $id 			= $this->input->post('id');
        $namalengkap 	= $this->input->post('namalengkap');
        $qty 			= $this->input->post('qty');
        $satuan 		= $this->input->post('satuan');
        $isi 			= $this->input->post('isi');
        $namamenu 		= $this->input->post('namamenu');
        $pcodenamamenu	= $this->input->post('pcodenamamenu');
        $id_resep 		= $this->uri->segment(4);


        if ($sign == "Y") {

            $data['judul']      = "Form Add Resep";
            $data['id']			= $id;
            $data['id_resep']	= $id_resep;
            $data['namalengkap'] = $namalengkap;
            $data['qty'] 		= $qty;
            $data['satuan'] 	= $satuan;
            $data['isi'] 		= $isi;
            $data['pcode'] 		= $pcodenamamenu;
            $data['track']  	= $mylib->print_track();
            $data['namamenu']	= $namamenu;
            $data['data'] 		= $this->resep_model->getdataheader($id_resep);

            $this->load->view('transaksi/resep/edit', $data);
        } else {
            $this->load->view('denied');
        }
    }


    function save_new_resep() {
//        echo "<pre>";print_r($_POST);echo "</pre>";die();
        $mylib = new globallib();


		$tgl 				= date('Y-m-d');
        $user           	= $this->session->userdata('userid');
        $kode           	= $this->input->post('kode');
        $pcodet           	= $this->input->post('pcodekd');
        $namabarang     	= $this->input->post('namabarang');
        $isi            	= $this->input->post('isi');
        $qty            	= $this->input->post('qty');
        $satuan         	= $this->input->post('satuan');
        $hargapack      	= $this->input->post('harga');
        $namamenucombo      = $this->input->post('namamenu');
        $save      			= $this->input->post('save');
        $nama 				= $this->input->post('nama');
        $namabarang         = $this->input->post('namabarang');
        $namaresep			= $this->input->post('namaresep');
        $koderesep			= $this->input->post('koderesep');
       
       //print_r($namabarang);die;


        if($save)
        {
        	/*for($i=0;$i<count($qty);$i++)
        	{
	        	if (is_numeric($qty[$i])==true)
	        	{
	        		echo "angka"; echo '<br>';
	        	
	        	}
	        	else
	        	{	
	        		echo "baris ke ".$i." bukan angka";echo '<br>';
	        	}
	        }

	        echo "selesai";die;*/

        	 	$data['judul']      = "Form Add Resep";
        		$data['namabarang'] = $nama;
       			$data['PCode'] = '';
        		//$data['kode']		= $kode;
        		$data['kolom'] = '';
        		$data['tampil'] = TRUE;


       			$data['error'] = "";
        		$data['data'] = $this->resep_model->getnamamaster($nama);
        		//$data['datalast'] = $this->resep_model->getlast();


			$dataheader = array(
									'Pcode'		=> $pcodet,
									'Status'	=> 'A',
									'AddUser'	=> $user,
									'AddDate'	=> $tgl,
								);

			$this->db->insert('resep_header', $dataheader);
		   	$insert_id = $this->db->insert_id();


		   	for ($i=0;$i<count($kode); $i++) 
			{	
				$datadetail[$i]	= array(
								'resep_id'		=> $insert_id,
								'Pcode' 		=> $kode[$i],
								'IsiPerPack' 	=> $isi[$i],
								'Qty' 			=> $qty[$i],
								'Satuan' 		=> $satuan[$i],
								'HargaPerPack'	=> $hargapack[$i]	
									);
				
				
					$this->db->insert('resep_detail', $datadetail[$i]);
			}
       		redirect('/transaksi/resep/','refresh');
   		}
    }
    
function insertNewDetail($user, $kode,$isi,$qty,$satuan,$hargapack, $tgl){
       
        $mylib = new globallib();
        $bulan = substr($tgl, 5, 2);
        $tahun = substr($tgl, 0, 4);

        $data = array(
        	'Pcode'			=> $kode,
        	'IsiPerPack'	=> $isi,
        	'Qty'			=> $qty,
        	'Satuan'		=> $satuan,
        	'HargaPerPack'	=> $hargapack	
            
        );

         $dataheader = array(
        	'Pcode'			=> $kode,
        	'Status'		=> 'A',
        	'AddUser'		=> $user,
        	'AddDate'		=> $tgl
            
        );

        $this->db->insert('resep_detail', $data);
        $this->db->insert('resep_header', $dataheader);
    }

    function update()
	{

		$tgl 				= date('Y-m-d');
        $user           	= $this->session->userdata('userid');
        $kode           	= $this->input->post('Pcode');
        $namabarang     	= $this->input->post('namabarang');
        $isi            	= $this->input->post('isi');
        $qty            	= $this->input->post('qty');
        $satuan         	= $this->input->post('satuan');
        $hargapack      	= $this->input->post('harga');
        $namamenucombo      = $this->input->post('namamenu');

        $id_resep = $this->input->post('idresep');



      	$this->resep_model->deleted($id_resep);

        for ($i=0;$i<count($kode); $i++) 
        {
			$data[$i] = array(
							'resep_id'  => $id_resep,
							'PCode'	 	=> $kode[$i],
							'IsiPerPack' => $isi[$i],
							'Qty' 		 => $qty[$i],
							'Satuan'     => $satuan[$i],
							'HargaPerPack' => $hargapack[$i]
							);

		//$this->db->update('resep_detail', $data[$i], array('PCode'=> $kode[$i]));

			$this->db->insert('resep_detail', $data[$i]);
			
		}
		//print_r($data);die;

		$dataheader = array (
								'EditUser' => $user,
								'EditDate' => $tgl
								);
		/*print_r($data);
		die;*/

		$this->db->update('resep_header', $dataheader, array('resep_id'=> $id_resep));

        redirect('/transaksi/resep/','refresh');
	
	}


	function versistruk()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip         = "192.168.0.193";
		$printer    = $this->resep_model->NamaPrinter($ip);
		
		$data['store'] = $this->resep_model->aplikasi();
		$data['header'] = $this->resep_model->getHeader($no);
		//=================
		//belum verif
		$data['detail'] = $this->resep_model->getHeaderForPrint2($no);

		//=====================
		
		/*print_r($data['header']);die;
		if(!empty($data['header'])){
			$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi', $data); // jika ada printernya
		}*/
	}

	function versistruk2()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		$data['store'] = $this->resep_model->aplikasi();
		$data['header'] = $this->resep_model->getHeader($no);
		//============belum verif=============
		$data['detail'] = $this->resep_model->getHeaderForPrint2($no);
		
		$data['reset']  =chr(27).'@';
		$data['plength']=chr(27).'C';
		$data['lmargin']=chr(27).'l';
		$data['cond']   =chr(15);
		$data['ncond']  =chr(18);
		$data['dwidth'] =chr(27).'!'.chr(24);
		$data['ndwidth']=chr(27).'!'.chr(14);
		$data['draft']  =chr(27).'x'.chr(48);
		$data['nlq']    =chr(27).'x'.chr(49);
		$data['bold']   =chr(27).'E';
		$data['nbold']  =chr(27).'F';
		$data['uline']  =chr(27).'!'.chr(129);
		$data['nuline'] =chr(27).'!'.chr(1);
		$data['dstrik'] =chr(27).'G';
		$data['ndstrik']=chr(27).'H';
		$data['elite']  ='';
		$data['pica']   =chr(27).'P';
		$data['height'] =chr(27).'!'.chr(16);
		$data['nheight']=chr(27).'!'.chr(1);
		$data['spasi05']=chr(27)."3".chr(16);
		$data['spasi1'] =chr(27)."3".chr(24);
		$data['fcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';
		$data['pcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';
		$data['op_cash']=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);
		
		//if(!empty($data['header'])){
	//		//$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
	//		$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi2_new', $data); // jika ada printernya
	//	}
		
		$this->load->helper('text');
		$this->load->helper('print');
		$data['nl']	= "\r\n";
		$html		= $this->load->view('transaksi/resep/cetak_strukresep2 _new', $data,TRUE);
		$filename	='komisi'.$no;
		$ext		='ctk';
		header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		header("Content-Transfer-Encoding: binary");
		header('Expires: 0');
		header('Pragma: no-cache');
		print $html;
	}

	function cetak()
	{
		$data = $this->varCetak();

		$this->load->view('transaksi/cetak_transaksi/cetak_resep', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib       = new globallib();
		$printreport = new printreportlib();
		$id          = $this->uri->segment(4);
		$header      = $this->resep_model->getHeader($id);
		$data['header'] = $header;

		//print_r($header);die;

		$detail = $this->resep_model->getDetailForPrint($id);
		$data['judul1'] = array("Nama Menu","Nama Toko");
		$data['niljudul1'] = array($header[0]['NamaLengkap'],$header[0]['NamaSubDivisi']);
		$data['judul2'] = "";
		$data['niljudul2'] = "";
		$data['judullap'] = "Standard Menu Per Bahan";
		$data['colspan_line'] = 4;
		//$data['url'] = "resep/printThis/".$id;
		//$data['url2'] = "resep/versistruk/" . $id;
		//$data['url3'] = "resep/versistruk2/" . $id;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan","normal","normal","kanan","kanan");
		$data['judul_detail'] = array("No", "PCode", "Nama Barang", "IsiPerPack", "Qty", "Satuan", "HargaPerPack", "Nilai");
		$data['panjang_kertas'] =30;
		$jmlh_baris_lain = 19;
		$data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
		$jml_baris_detail = count($detail);
		if($data['panjang_per_hal'] == 0){
			$data['tot_hal'] = 1;
		}
		else
		{
			$data['tot_hal'] = ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
		}
		$list_detail = array();
		$detail_attr = array();
		$list_detail_attr = array();
		$detail_page = array();
		$new_array = array();
		$counterBaris = 0;
		$counterRow   = 0;
		$max_field_len= array(0,0,0);
		$sum_nilai = 0;
		$sum_harga = 0;
		//                print_r($detail);
		$no=1;
		  for ($m = 0; $m < count($detail); $m++) {
		//			$attr = $this->komisi_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
		unset($list_detail);
		$counterRow++;
		$list_detail[] = $no;
		$list_detail[] = stripslashes($detail[$m]['Pcode']);
		$list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
		$list_detail[] = stripslashes($detail[$m]['IsiPerPack']);
		$list_detail[] = number_format(stripslashes($detail[$m]['Qty']),2,',','');
		$list_detail[] = stripslashes($detail[$m]['Satuan']);
		$list_detail[] = stripslashes($detail[$m]['HargaPerPack']);
		$list_detail[] = number_format(((stripslashes($detail[$m]['HargaPerPack'])/(stripslashes($detail[$m]['IsiPerPack'])))*(stripslashes($detail[$m]['Qty']))),0,'','.');

		$detail_page[] = $list_detail;
		$max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
		if ($data['panjang_per_hal'] != 0) {
		if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
		$data['detail'][] = $detail_page;
		if ($m != count($detail) - 1) {
		unset($detail_page);
		}
		}
		}
		$harga = ((stripslashes($detail[$m]['HargaPerPack'])/(stripslashes($detail[$m]['IsiPerPack'])))*(stripslashes($detail[$m]['Qty'])));

		//$sum_harga += stripslashes($detail[$m]['HargaPerPack']);
		$sum_nilai = $sum_nilai + ($harga);
		
		$no++;
		}

		$hargajual 					= $header[0]['Harga1c'];
		$komisi 					= ($hargajual*20/100);

		$service_charge 			= ($hargajual*$header[0]['Service_charge']/100);

		$ppn 						= (($hargajual + $service_charge)*($header[0]['PPN'])/100);

		$harga_setelah_service_cas	= $hargajual + $service_charge + $ppn;
		//$harga_setelah_ppn 			= $hargajual + $ppn;
		//$harga_total_setelah 		= $harga_setelah_service_cas + $harga_setelah_ppn;
		
		/*echo ($hargajual);
		echo ($service_charge);
		echo ($ppn);die;*/

		$service_pajak  			= $harga_setelah_service_cas;

		//$harga_jual_round 			= number_format($hargajual,0,'','.');
		//$komisi_round 				= number_format($hargajual,0,'','.');


		$net_sales					= $hargajual - $komisi;
		$net_sales_round 			= number_format($net_sales,0,'','.');


		$biaya_tambahan_20 			= ((($sum_nilai*20)/100)); 
		$estimasi_cog_perunit 		= ($sum_nilai + $biaya_tambahan_20);

		/*echo $net_sales; echo '<br>';
		echo $net_sales_round;echo '<br>';
		echo $estimasi_cog_perunit;echo '<br>';
		echo $net_sales - $estimasi_cog_perunit;echo '<br>';*/


		$isi_cogs 					= $sum_nilai;
		$isi_cogs1 					= @($estimasi_cog_perunit / $net_sales); 

		$net_profit					= ($net_sales - $estimasi_cog_perunit);
		$net_profit_persen 			= @($net_profit / $net_sales);

		//1

		$data['judul_netto'] 		= array("Estimasi Biaya/Unit");
		$data['biaya_tambahan'] 	= array("Biaya Tambahan 20%");		
		$data['estimasi_COGunit'] 	= array("Estimasi COG/Unit");

		$data['isi_netto'] 			= array(number_format($estimasi_cog_perunit, 0, '', '.')); //disini
		$data['sum_harga'] 			= array(number_format($sum_nilai, 0, '', '.'));
		$data['isi_biaya_tambahan'] = array(number_format($biaya_tambahan_20, 0, '', '.'));

		//2

		$data['komisi'] 			= array("Komisi 20%");
		$data['harga_jual'] 		= array("Harga Jual");
		$data['harga_jual_setelah'] = array("Harga Jual Setelah Service Charge & Pajak");


		$data['isi_komisi'] 			= array(number_format($komisi,0,'','.'));
		$data['isi_harga_jual'] 		= array(number_format($hargajual,0,'','.'));
		$data['isi_harga_jual_setelah'] = array(number_format($service_pajak,0,'','.'));


		//3

		$data['net_sales'] 				= array("Net Sales");
		$data['cogs'] 					= array("COGS");
		$data['net_profit'] 			= array("Net Profit");


		$data['isi_net_sales'] 			= array($net_sales_round);
		$data['isi_net_profit'] 		= array(number_format($net_profit,0,'','.'));
		$data['isi_cogs_persen'] 		= array(number_format($isi_cogs1,2,'.','.')); //disini
		$data['net_profit_persen'] 		= array(number_format($net_profit_persen, 0, '', '.'));

		$data['isi_cogs1'] 				= array(number_format($estimasi_cog_perunit,0,'','.'));


		$data['isi_harga_jual'] 		= array(number_format($hargajual,0,'','.'));
		$data['isi_harga_jual_setelah'] = array(number_format($service_pajak,0,'','.'));

		$data['dibuat'] 	= array("Dibuat Oleh");		
		$data['nama0'] 	= array("Apriansyah");
		$data['finance'] 	= array("Finance");

		$data['mengetahui'] 	= array("Mengetahui,");
		$data['nama'] 	= array("Bambang Sutrisno");
		$data['div'] 	= array("Sr Mgr F & A");

		$data['menyetujui'] 	= array("Menyetujui");
		$data['nama1'] 	= array("Billy Hartono Salim");
		$data['div1'] 	= array("CEO");


		$data['detail'][] = $detail_page;
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;

	}

	function printThis()
	{
		//echo "asd";die;
		$data = $this->varCetak();
		$id   = $this->uri->segment(4);
		$data['fileName2'] = "lainlain.sss";
		$data['fontstyle'] = chr(27) . chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['pindah_hal'] = "\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['string1'] = "     Dibuat Oleh,                     Disetujui Oleh,";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_lain', $data);
	}

	function cetakexcel()
	{
		$id          = $this->uri->segment(4);
		//echo $id;die;
		$data['header'] = $this->resep_model->getHeader($id);
		$data['detail'] = $this->resep_model->getDetailForPrint($id);
		
		$html = $this->load->view('transaksi/cetak_transaksi/cetak_resep_excel', $data, true);
		
		header('Content-Type: application/vnd.ms-excel');
    	header('Content-Disposition: attachment; filename="resep.xls"');
    	print $html;
	}


}


/* End of file resep.php */
/* Location: ./application/controllers/resep.php */