<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Pemotongan_hutang extends authcontroller {

    function __construct() {
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->helper('terbilang');
        $this->load->model('transaksi/pemotongan_hutang_model');
    }

    function index() 
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_customer"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "pemotongan_hutang", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    $search_gudang = explode("=", $arrSearch[1]); // search gudang
                    $arr_search["search"]["gudang"] = $search_gudang[1];
                    $search_customer = explode("=", $arrSearch[2]); // search customer
                    $arr_search["search"]["customer"] = $search_customer[1];
                    $search_status = explode("=", $arrSearch[3]); // search status
                    $arr_search["search"]["status"] = $search_status[1];

                    $data["search_keyword"] = $search_keyword[1];
                    $data["search_gudang"] = $search_gudang[1];
                    $data["search_customer"] = $search_customer[1];
                    $data["search_status"] = $search_status[1];
                }
            }
            
	
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
 
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/pemotongan_hutang/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/pemotongan_hutang/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
			$data['msupplier'] = $this->pemotongan_hutang_model->getSupplierList();
			
            $config['total_rows'] = $this->pemotongan_hutang_model->num_pemotongan_hutang_row($arr_search["search"]);
            
			$data['data'] = $this->pemotongan_hutang_model->getDnaList($config['per_page'], $page, $arr_search["search"]);
					
            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/pemotongan_hutang/pemotongan_hutang_list', $data);
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	//echo "<pre>";print_r($_POST);echo "</pre>";die;
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'pemotongan_hutang', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$search_value .= "&search_gudang=".$this->input->post('search_gudang');
		$search_value .= "&search_customer=".$this->input->post('search_customer');
		$search_value .= "&search_status=".$this->input->post('search_status');
		
		$data = array(
            'query_string' => $search_value,
            'module' => "pemotongan_hutang",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/pemotongan_hutang/index/' . $query_id . '');
    }
	
	function batal() 
    {
    	    redirect('/transaksi/pemotongan_hutang/');	
			
    }
    
    	
	function add_new() 
    {
    				
			        $mylib = new globallib();
			        $sign = $mylib->getAllowList("add");
		        	if ($sign == "Y") {
		            $data['msg'] = "";
		            
		            $user = $this->session->userdata('username');
		            $userlevel = $this->session->userdata('userlevel');
		            $gudang_admin = $this->globalmodel->getGudangAdmin($user);
		            
		            $data['spp'] 	= "";
		            $data['debitno'] = "";
					$data['mUang']	= $this->pemotongan_hutang_model->getCurrency();
		            $data['msupplier'] = $this->pemotongan_hutang_model->getSupplier();
		            $data['datado'] = $this->pemotongan_hutang_model->getDebitNoteDetailList($user);
								
		            $data['track'] = $mylib->print_track();
		            $this->load->view('transaksi/pemotongan_hutang/add_pemotongan_hutang', $data);
		        } else {
		            $this->load->view('denied');
		        }
    }

    function edit_pemotongan_hutang($id)
    {
    	//echo $id;die;
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->pemotongan_hutang_model->getCurrency();
            $data['header'] = $this->pemotongan_hutang_model->getHeader($id);
            $data['detail_list'] = "";
			
            $data['msupplier'] = $this->pemotongan_hutang_model->getSupplier();
            $data['anak'] = $this->pemotongan_hutang_model->getDnno();
            $data['datadna'] = $this->pemotongan_hutang_model->getDebitNoteAllocationDetail($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/pemotongan_hutang/edit_pemotongan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    /*function view_pemotongan_hutang($id)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->pemotongan_hutang_model->getCurrency();
            $data['header'] = $this->pemotongan_hutang_model->getHeader($id);
			
            $data['msupplier'] = $this->pemotongan_hutang_model->getSupplier();
			$data['hitungdo'] = $this->pemotongan_hutang_model->hitungDebitNoteDetailList2($id);
			$data['hitungdodetail'] = $this->pemotongan_hutang_model->hitungDebitNoteDetailList3($id);
            $data['datado'] = $this->pemotongan_hutang_model->getDebitNoteDetailList2($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/pemotongan_hutang/view_pemotongan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }*/
    
    function view_pemotongan_hutang($id)
    {
    	//echo $id;die;
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
			$data['mUang']	= $this->pemotongan_hutang_model->getCurrency();
            $data['header'] = $this->pemotongan_hutang_model->getHeader($id);
			
            $data['msupplier'] = $this->pemotongan_hutang_model->getSupplier();
            $data['anak'] = $this->pemotongan_hutang_model->getDnno();
            $data['datadna'] = $this->pemotongan_hutang_model->getDebitNoteAllocationDetail($id);
            $data['track'] = $mylib->print_track();
			
            $this->load->view('transaksi/pemotongan_hutang/view_pemotongan_hutang', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function getDebitCreditNo()
	{
		
	 	$KdSupplier	= $this->input->post('KdSupplier');
	 	$key	= $this->pemotongan_hutang_model->getRequirement($KdSupplier);
		$supplierid = $key->supplierid;
		$data = $this->pemotongan_hutang_model->getSibling($supplierid);
		echo "<option value=''> -- Pilih -- </option>";
		for($a=0;$a<count($data);$a++)
		{
			echo "<option value='".$data[$a]['dnno']."'>".$data[$a]['dnno'].", Total ".$data[$a]['sisa_']."</option>";
			
		}
		
			
	}

	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $dnadate = $this->input->post('v_tanggal_dokumen');
        $v_supplier = $this->input->post('KdSupplier');
        $v_debitno = $this->input->post('debitno');
		$currencycode = $this->input->post('Uang');
		$v_amount = $this->input->post('v_amount');
		$v_amount_allocation = $this->input->post('v_amount_allocation');
		$v_amount_idr = $this->input->post('v_amount_idr');
		$v_amount_allocation_idr = $this->input->post('v_amount_allocation_idr');
		$v_status = $this->input->post('v_status');
		
		$v_no_debit_note1 = $this->input->post('v_no_debit_note');
		$v_nilaitransaksi1 = $this->input->post('v_nilaitransaksi');
		$v_bayar1 = $this->input->post('v_bayar');
		$v_sisa1 = $this->input->post('v_sisa');
		$v_jthtempo1 = $this->input->post('v_jthtempo');
		
		$v_dnadetailid2 = $this->input->post('v_dnadetailid2');
		$v_dnallocationno2 = $this->input->post('v_dnallocationno2');
		$v_dnno2 = $this->input->post('v_dnno2');
		$v_nofaktur2 = $this->input->post('v_invoice2');
		$v_invamount2 = $this->input->post('v_invamount2');
		$v_allocation2 = $this->input->post('v_allocation2');
		$v_invamountidr2 = $this->input->post('v_invamountidr2');
		$v_allocationidr2 = $this->input->post('v_allocationidr2');		
		$v_invamount2a = $this->input->post('v_invamount');
		$v_bayar2 = $this->input->post('v_bayar');
        
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        
		$data['bulan'] = date('m');
        $data['tahun'] = date('Y');
		
        if ($flag == "add")
		{
			                //generate dnno
							$v_dna = $mylib->get_code_counter($this->db->database, "dnallocation","dnallocationno", "DA", $data['bulan'], $data['tahun']); 
						
							//insert ke detail
							$v_tot_bayar=0;
							for ($x = 0; $x < count($v_no_debit_note1); $x++)
							{
								$v_invno = $v_no_debit_note1[$x];
								$v_invamount = $v_nilaitransaksi1[$x];
								$v_allocation = $v_bayar1[$x];
								//$v_sisa = $v_sisa1[$x];
								$v_jthtempo = $v_jthtempo1[$x];
								
								
								$data_detail=array(
								  'dnallocationno'=>$v_dna,
								  'invno'=>$v_invno,
								  'invamount' => $v_invamount,
								  'allocation' => $v_allocation,
								  'adddate' => date('Y-m-d'),
								  'adduser' => $user
								);
														
								$this->db->insert('dnallocationdetail', $data_detail);
								
							$v_tot_bayar+=$v_allocation;	
					        }
					        
					        //insert ke header
					        $this->insertNewHeader($v_dna, $dnadate, $v_supplier, $v_debitno, $user, $currencycode, $v_tot_bayar );
						
			         
		} 
		
		else if ($flag == "edit") 
		{
			$v_dna = $this->input->post('v_dnallocationno');
			$v_tgl_pemotongan_hutang = $this->input->post('v_tanggal_dokumen');
			$dnno = $this->input->post('v_dnno');
					
			//update detail
			$v_tot_bayars=0;
			for ($x = 0; $x < count($v_dnadetailid2); $x++)
			{
					$v_dnadetailid = $v_dnadetailid2[$x];	
		            $v_dnallocationno = $v_dnallocationno2[$x];
		            $v_dnno = $v_dnno2[$x];
		            $v_nofaktur = $v_nofaktur2[$x];
		            $v_invamount = $v_invamount2[$x];
		            $v_invamount2b = $v_invamount2a[$x];
		            $v_allocation = $v_allocation2[$x];
		            $v_invamountidr = $v_invamountidr2[$x];
		            $v_allocationidr = $v_allocationidr2[$x];
		            $v_bayar = $v_bayar2[$x];
		            //$v_sisa = $v_sisa1[$x];
					$v_jthtempo = $v_jthtempo1[$x];
					
		            $data_detail_edit=array(
					  'invamount' => $v_invamount2b,
					  'allocation' => $v_bayar,
					  'editdate' => date('Y-m-d'),
					  'edituser' => $user
		            );
		            $where_detail_edit=array(
		              'dnadetailid' => $v_dnadetailid,
					  'dnallocationno' => $v_dnallocationno
		            );
											
					$this->db->update('dnallocationdetail', $data_detail_edit,$where_detail_edit);
					
					
					if($v_status=="1"){
						
					            //update hutang dengan no. debet itu sendiri
								//ambil nilai dnamount didebit note
								$isi_sisa_hutang_dn= $this->pemotongan_hutang_model->getsissahutang($v_dnno);
								$isi_sisa_hutang = $isi_sisa_hutang_dn->Sisa;
								$ssisa = $isi_sisa_hutang - $v_bayar;

								//update di DN nya ----------------------
								$data_jml_=array(
												'Sisa'=>$ssisa 
												);
								$data_dimana=array(
												'NoDokumen'=>$v_dnno
												);
								$this->db->update('hutang', $data_jml_,$data_dimana);
								
								//update di Invoice nya -------------------
								$sisa= $v_invamount2b - $v_bayar;
								$data_jml_2=array(
												'Sisa'=>$sisa 
												);
								$data_dimana2=array(
												'NoDokumen'=>$v_nofaktur
												);
								$this->db->update('hutang', $data_jml_2,$data_dimana2);
								
								$norek = $this->pemotongan_hutang_model->getKdRekening($v_nofaktur);
								
								//insert ke tabel hutang dengan no allocation nya ---------------
								$data_insert_hutang=array(
													 'NoDokumen'=>$v_dna,
													 'NoFaktur'=>$v_nofaktur,
													 'KdSupplier'=>$v_supplier,
													 'TipeTransaksi'=>'A',
													 'Tanggal'=>$mylib->ubah_tanggal($dnadate),
													 'JatuhTempo'=>$mylib->ubah_tanggal($dnadate),
													 'NilaiTransaksi'=>$v_bayar,
													 'Sisa'=>0,
													 'MataUang'=>'IDR',
													 'Kurs'=>1,
													 'KdRekening' => $norek
													);
								//echo "<pre>";print_r($data_insert_hutang);echo "</pre>";die;
								$this->db->insert('hutang', $data_insert_hutang);
								
								//update dnamountremain di debit note
									//pertama dapatkan dnamountremain di debit note
									$vv_dnamountremain = $this->pemotongan_hutang_model->getDnamountremain($v_dnno);
									$v_dnremain = ( $vv_dnamountremain->dnamountremain ) - $v_bayar;
									$data_debit_note = array(
															'dnamountremain'=>$v_dnremain 
															);
									$this->db->update('debitnote',$data_debit_note,array('dnno'=>$v_dnno));
									
									
							}
							
							
					$v_tot_bayars+=$v_bayar;
						
            
           }
           
            //update header
			$this->updateHeader($v_dna,$mylib->ubah_tanggal($v_tgl_pemotongan_hutang), $v_debitno , $v_status, $v_supplier , $currencycode, $user, $v_tot_bayars);	
			
			$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_dna . '</strong> berhasil', 'class' => 'success'));
           
			
        }
        redirect('/transaksi/pemotongan_hutang/edit_pemotongan_hutang/'.$v_dna.'');
    }

    function insertNewHeader($v_dna, $dnadate, $v_supplier, $v_debitno, $user, $currencycode, $v_tot_bayar ) 
    {
		$allocationtotal = $v_tot_bayar;
		$allocationidrtotal = $v_tot_bayar;
        $data = array(
				  'dnallocationno' => $v_dna,
				  'dnadate' => $dnadate,
				  'dnno' => $v_debitno,
				  'supplierid' => $v_supplier,
				  'currencycode' => $currencycode,
				  'allocationtotal' => $allocationtotal,
				  'allocationidrtotal' => $allocationidrtotal,
				  'adddate' => date('Y-m-d'),
				  'adduser' => $user,
				  'status' => 0
				);

        $this->db->insert('dnallocation', $data);

        $this->pemotongan_hutang_model->unlocktables();

    }
    
    
    function updateNewHutang($dnno, $tgl, $v_supplier,$v_type, $grandtotal) 
    {
        $this->pemotongan_hutang_model->locktables('hutang');

        $data = array(
				  'KdSupplier' => $v_supplier,
				  'TipeTransaksi' => $v_type,
				  'Tanggal' => $tgl,
				  'JatuhTempo' => $tgl,
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $dnno,
				  'NoFaktur' => $dnno
				);

        $this->db->update('hutang', $data, $where);

        $this->pemotongan_hutang_model->unlocktables();

    }
    
    function updateNewMutasiHutang($dnno, $tgl, $v_supplier, $grandtotal) 
    {
        $this->pemotongan_hutang_model->locktables('mutasi_hutang');

        $data = array(
        		  'Tanggal' => $tgl,
				  'KdSupplier' => $v_supplier,
				  'Kurs'=>1, 
				  'Jumlah' => $grandtotal,
				  'TipeTransaksi' => 'I'
				);
				
		$where=array(
		          'NoDokumen' => $dnno
					);

        $this->db->update('mutasi_hutang', $data, $where);

        $this->pemotongan_hutang_model->unlocktables();

    }
    
    function updateNewHutang2($dnno, $grandtotal) 
    {
        $this->pemotongan_hutang_model->locktables('hutang');

        $data = array(
				  'NilaiTransaksi' => $grandtotal,
				  'Sisa' => $grandtotal
				);
				
		$where = array(
				  'NoDokumen' => $dnno,
				  'NoFaktur' => $dnno
				);

        $this->db->update('hutang', $data, $where);

        $this->pemotongan_hutang_model->unlocktables();

    }
  
	function updateHeader($v_dnallocationno, $v_tgl_pemotongan_hutang, $dnno ,$v_status, $v_supplier , $currencycode, $user, $v_tot_bayars) 
    {
    	$this->pemotongan_hutang_model->locktables('dnallocation');
		$allocationtotal = $v_tot_bayars;
        $data2 = array
				(
				  'dnadate' => $v_tgl_pemotongan_hutang,
				  'dnno' => $dnno,
				  'supplierid' => $v_supplier,
				  'currencycode' => $currencycode,
				  'allocationtotal' => $allocationtotal,
				  'editdate' => date('Y-m-d'),
				  'edituser' => $user,
				  'status' => $v_status
				);
				
		$where2=array(
		'dnallocationno'=>$v_dnallocationno
					);

        $this->db->update('dnallocation', $data2, $where2);
        
        $this->pemotongan_hutang_model->unlocktables();
    }

	
	
	function delete_trans($dnallocationno) 
    {
	        //kembalikan nilai yang ada di hutang
	        //ambil nilai allocation di dnallocation
	        $isi_allocation = $this->pemotongan_hutang_model->ambil_allocation($dnallocationno);
		        foreach($isi_allocation AS $val){
					$nofak = $val['invno'];
					$isi_sisa_hutang = $this->pemotongan_hutang_model->ambil_sisa_hutang($nofak);
					$balikkan_nilai_sisa = $isi_sisa_hutang->sisa + $val['allocation'];
					//update sisa di hutang berdasarkan faktur tersebut
					$this->db->update('hutang',array('sisa'=>$balikkan_nilai_sisa),array('NoDokumen'=>$nofak));
					
					
					//kembalikan nilai yang ada di debit note
					$isi_dnamountremain_debit= $this->pemotongan_hutang_model->ambil_dnamountremain_debit($nofak);
					$balikkan_dnamountremain = $isi_dnamountremain_debit->dnamountremain + $val['allocation'];
					//update dnamountremain di debit note
					$this->db->update('debitnote',array('dnamountremain'=>$balikkan_dnamountremain),array('dnno'=>$nofak));
					
				}
	        //hapus insert di hutang
	        $this->db->delete('hutang', array('NoDokumen'=>$dnallocationno));
	        
	        //delete
			$this->db->delete("dnallocation",array('dnallocationno'=>$dnallocationno));
			$this->db->delete("dnallocationdetail",array('dnallocationno'=>$dnallocationno));
			
			
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $dnallocationno . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/transaksi/pemotongan_hutang/');
    }

    function delete_detail() //delete detail add
    {
    	
        $dndid = $this->uri->segment(4);
        $coano = $this->uri->segment(5);
        $user = $this->uri->segment(6);
        
        $data=array(
        			'dndid'=>$dndid,
        			'coano'=>$coano,
        			'adduser'=>$user
        			);
		
        $this->db->delete("debitnotedtl_temp",$data);			
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Rekening ' . $coano . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/pemotongan_hutang/add_new/');
    } 
    
    function delete_detail2() //delete detail add
    {
    	
        $dnadetailid = $this->uri->segment(4);
        $dnallocationno = $this->uri->segment(5);
        $user = $this->session->userdata('username');
        
        $data=array(
        			'dnadetailid'=>$dnadetailid,
        			'dnallocationno'=>$dnallocationno
        			);
		
        $this->db->delete("dnallocationdetail",$data);
        
        //update total yang ada di dnallocation
			$ambil_total = $this->pemotongan_hutang_model->ambilTotal($v_dna);
			
			$data_jml=array(
			'allocationtotal'=>$ambil_total->jml_allocation,
			'allocationidrtotal'=>$ambil_total->jml_allocationidr
			);
			$dimana=array('dnallocationno'=>$v_dnallocationno);
			
			$this->db->update('dnallocation', $data_jml,$dimana);
			
						
		
        $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>Dokumen ' . $dnallocationno . '</strong> berhasil', 'class' => 'success'));

        redirect('/transaksi/pemotongan_hutang/edit_pemotongan_hutang/' .$dnallocationno.'');
    }


	function ambilhutang()
    {  
     $mylib = new globallib();     
     $supplier = $this->input->post('spl');
     $tgl = $this->input->post('tanggal');
     
     $query = $this->pemotongan_hutang_model->getAmbilHutang($supplier,$mylib->ubah_tanggal($tgl));
     
      echo '
	 		<thead class="title_table">
							<tr>
								<th width="30"><center>No</center></th>
								<th width="120"><center>Invoice No</center></th> 
								<th><center>Supplier</center></th>
								<th width="150"><center>Jatuh Tempo</center></th>
							    <th width="150"><center>Nilai Transaksi</center></th>
								<th width="150"><center>Sisa Bayar</center></th>
							    <th width="150"><center>Bayar</center></th>
								<th width="50"><center>Delete</center></th>
							</tr>
						</thead>
						<tbody>
	 	  ';
	 if(empty($query)){
		echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
		}else{
			 $no=1;
		     foreach ($query->result_array() as $cetak) {
			 echo "
			 		            <tr id='baris$no'>									
									<td align='center'>$no</td>
									<td align='center'>$cetak[NoFaktur]
									<input type='hidden' name='v_no_debit_note[]' id='v_no_debit_note$no' value='$cetak[NoFaktur]'/></td>
									<td>$cetak[Nama]</td>
									<td align='center'>$cetak[JatuhTempo_]
									<input type='hidden' name='v_jthtempo[]' id='v_jthtempo$no' value='$cetak[JatuhTempo]'/>
									</td>
									<td align='right'>$cetak[NilaiTransaksi1]
									<input type='hidden' name='v_nilaitransaksi[]' id='v_nilaitransaksi$no' value='$cetak[NilaiTransaksi2]'/>
									</td>
									<td align='right'>$cetak[sisa1]
									<input type='hidden' name='v_sisa[]' id='v_sisa$no' value='$cetak[sisa2]'/></td>
									<td><input type='text' class='form-control-new' name='v_bayar[]' id='v_bayar$no' value='0'  onblur='calculate(this)' style='text-align: right; width: 100%;' /></td>
									<td align='center'>
					                	<button type='button' class='btn btn-info btn-sm sm-new tooltip-primary'  data-toggle='tooltip' data-placement='top' data-original-title='Delete' title='' title='' name='btn_del_detail_$no' id='btn_del_detail_$no' onclick='deleteRow(this)' >
											<i class='entypo-trash'></i>
										</button>
					                </td>
								</tr>
			 	  ";
				$no++;
		      }
		    echo "<tr style='display:none'><td colspan='100%' align='center' >
		              <input style='display:none' name='jmlh' id='jmlh' value='0' /></td></tr>
		              <tr style='display:none'  colspan='100%' align='center' id='amounts'></tr> ";    
		}   
    }
    //style='display:none'
    
    
    function ambilamount()
    { 
     $dnno = $this->input->post('debit');
     
     $query = $this->pemotongan_hutang_model->getAmountDebit($dnno);
     foreach($query->result_array() as $cetak){
	 	echo "<td><input style='display:none' type='text' name='amount' id='amount' value='$cetak[dnamountremain]' /></td>";
	 }
        
    }
    
    function vewPrint()
	{
		$this->load->library('printreportlib');
		$printlib = new printreportlib();
		
		$nodok 	= $this->uri->segment(4);
		$data["user"] = $this->session->userdata('username');
		
		
		$data["judul"]		= "I N V O I C E";
		$data["header"] 	= $this->pemotongan_hutang_model->getHeader($nodok);
		$data["detail"] 	= $this->pemotongan_hutang_model->getDetail_cetak($nodok);
		$data["hitungsi"] 	= $this->pemotongan_hutang_model->hitungSalesInvoice_cetak($nodok);
		$data["pt"] 		= $printlib->getNamaPT();
		
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_si', $data);
	}
	
	
	function doPrint()
	{
		$this->load->helper('terbilang');
		$this->load->library('globallib');
		$this->load->library('printreportlib');
		$mylib = new globallib();
		$printlib = new printreportlib();
		
		$nodok = $this->uri->segment(4);
		$user = $this->session->userdata('username');
		$spasi_awal = " ";
		
		$arr_epson = array();
		$arr_epson = $mylib->sintak_epson();
		
		$echo = "";
		$echo .= $spasi_awal;
		$pt = $printlib->getNamaPT();
		
		
		$total_spasi = 120;
	    $total_spasi_header = 80;
	    $jml_detail  = 8;
	    $ourFileName = "sales-invoice.txt";
		
		$header 	= $this->pemotongan_hutang_model->getHeader($nodok);
		$detail 	= $this->pemotongan_hutang_model->getDetail_cetak($nodok);
		$hitungsi 	= $this->pemotongan_hutang_model->hitungSalesInvoice_cetak($nodok);
		
		/*echo "<pre>";
		print_r($data["hitungsi"]);
		echo "</pre>";
		die;*/
		
		$note_header = substr($header->note,0,40);
		
		$echo="";
		$counter = 1;
		foreach($detail as $val)
		{
            $arr_data["detail_pcode"][$counter] = $val["PCode"];
            $arr_data["detail_namabarang"][$counter] = substr($val["NamaLengkap"],0,60);
            $arr_data["detail_qty"][$counter] = $val["quantity"];
            $arr_data["detail_satuan"][$counter] = $val["SatuanSt"];
            $arr_data["detail_harga"][$counter] = $val["Harga1c"];
			
			$counter++;
		}
        
        $curr_jml_detail = count($detail);
        $jml_page = ceil($curr_jml_detail/$jml_detail);
        
        $nama_dokumen = "I N V O I C E";
        
        $grand_total = 0;
        for($i_page=1;$i_page<=$jml_page;$i_page++)
        {
            if($i_page%2==0)
            {
                $echo.="\r\n"; 
                $echo.="\r\n"; 
            }
            
            // header
            {
                $echo.=$arr_epson["cond"].$pt->Nama;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat1;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"].$pt->Alamat2;
                $echo.="\r\n";    
                
                $echo.=$arr_epson["cond"]."Phone:".$pt->TelpPT;
                $echo.="\r\n"; 
            }
            $echo.="\r\n";
			
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen($nama_dokumen)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $arr_epson["cond"].$nama_dokumen;
            
            $echo.="\r\n";       
            
            $echo.=$arr_epson["ncond"];
            $limit_spasi = ceil(($total_spasi_header/2)) - (strlen("No : ".$header->invoiceno)/2);
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                    
            $echo.= $arr_epson["cond"]."No : ".$header->invoiceno;    
            
            $echo.="\r\n";    
            
            // baris 1
            {
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->sidate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->sidate));$i++)
                {
                    $echo.=" ";
                }
                
                $echo.=$arr_epson["cond"]."Tanggal";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Tanggal"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->duedate;         
                
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->duedate));$i++)
                {
                    $echo.=" ";
                }
               
                $echo.="\r\n";    
            }

            // baris 2
            {
                $echo.=$arr_epson["cond"]."Pelanggan";
                $limit_spasi = (20-2);
                for($i=0;$i<($limit_spasi-strlen("Pelanggan"));$i++)
                {
                    $echo.=" ";
                }
                $echo.=": ";
                
                $echo.=$arr_epson["cond"].$header->Nama;  
                 
                $limit_spasi = 65;
                for($i=0;$i<($limit_spasi-strlen($header->Nama));$i++)
                {
                    $echo.=" ";
                }
                                
                $echo.="\r\n";    
            }
            
            $echo.=$arr_epson["cond"];
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
            
            
            $echo .= $spasi_awal;
            $echo.=$arr_epson["cond"]."NO";
            $limit_spasi = 3;
            for($i=0;$i<($limit_spasi-strlen("NO"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."PCode";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("PCode"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Nama Barang";
            $limit_spasi = 60;
            for($i=0;$i<($limit_spasi-strlen("Nama Barang"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Qty";
            $limit_spasi = 5;
            for($i=0;$i<($limit_spasi-strlen("Qty"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Satuan";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Satuan"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Harga";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Harga"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.=$arr_epson["cond"]."Total";
            $limit_spasi = 20;
            for($i=0;$i<($limit_spasi-strlen("Total"));$i++)
            {
                $echo.=" ";
            }
            
            $echo .= $spasi_awal;
            $echo.="\r\n";
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            
            $echo.="\r\n";
            
            $no     = (($i_page * $jml_detail) - $jml_detail)+1;
            $no_end = $no + $jml_detail;
            
            for($i_detail=$no;$i_detail<$no_end;$i_detail++)
            {
	            $pcode = $arr_data["detail_pcode"][$i_detail];
	            $namabarang = $arr_data["detail_namabarang"][$i_detail];
	            $qty = $arr_data["detail_qty"][$i_detail];
	            $satuan = $arr_data["detail_satuan"][$i_detail];
	            $harga = $arr_data["detail_harga"][$i_detail];
	            $total = $qty*$harga;
	            if($pcode)
	            {
	            	$echo .= $spasi_awal;
                    $echo.=chr(15);
                    $echo.=$no;
                    $limit_spasi = 3;
                    for($i=0;$i<($limit_spasi-strlen($no));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$pcode;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($pcode));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$namabarang;
                    $limit_spasi = 60;
                    for($i=0;$i<($limit_spasi-strlen($namabarang));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=number_format($qty,0,',','.');
                    $limit_spasi = 5;
                    for($i=0;$i<($limit_spasi-strlen(number_format($qty,0,',','.')));$i++)
                    {
                        $echo.=" ";
                    }
                    
                    $echo.=$arr_epson["cond"].$satuan;
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-strlen($satuan));$i++)
                    {
                        $echo.=" ";
                    }
                    $echo.=number_format($harga,0,',','.');
                    $limit_spasi = 10;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($harga,0,',','.'))+$jarak_harga));$i++)
                    {
                        $echo.=" ";
                    }
                    
					    if(strlen(number_format($total,0,',','.'))==1){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==2){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==3){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==4){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==5){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==6){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==7){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==8){
						$jarak_total=5;
						}else if(strlen(number_format($total,0,',','.'))==9){
						$jarak_total=5;
						}				
					if(strlen(number_format($total,0,',','.'))==10){
                    $echo.=number_format($total,0,',','.');
					}else{
                    $limit_spasi = 15;
                    for($i=0;$i<($limit_spasi-(strlen(number_format($total,0,',','.'))+$jarak_total));$i++)
                    {
                        $echo.=" ";
                    }
					$echo.=number_format($total,0,',','.');
					}
                    
				}
				$echo.="\r\n";
				$no++;
            	
            }
 
            for($i=1;$i<=$total_spasi;$i++)
            {
                $echo.="-";
            }
            $echo .= "\r\n";
			
			
            
			foreach($hitungsi as $val)
			{
				$tothal = $val['total'];
				$diskon = $val['diskon'];
				
				if($diskon ==0){
				$potongan_diskon = 0;
				}else{
				$potongan_diskon =($diskon/100)*$total;
				}
				
				$ppn=(10/100)*$tothal;
				
				$grand_total=($tothal-$potongan_diskon)+$ppn;
				
			}
			
											
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Subtotal";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Subtotal"));$i++)
						{
							$echo.=" ";
						}
						if(strlen(number_format($tothal,0,',','.'))==1){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==2){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==3){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==4){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==5){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==6){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==7){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==8){
						$jarak_tothal=5;
						}else if(strlen(number_format($tothal,0,',','.'))==9){
						$jarak_tothal=5;
						}
						if(strlen(number_format($tothal,0,',','.'))==10){
						$echo.=number_format($tothal,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($tothal,0,',','.'))+$jarak_tothal));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($tothal,0,',','.');
						}
						
				
						$echo .= "\r\n";						
						
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Diskon";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Diskon"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($potongan_diskon,0,',','.'))==1){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==2){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==3){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==4){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==5){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==6){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==7){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==8){
						$jarak_diskon=5;
						}else if(strlen(number_format($potongan_diskon,0,',','.'))==9){
						$jarak_diskon=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($potongan_diskon,0,',','.'))+$jarak_diskon));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($potongan_diskon,0,',','.');
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="PPN";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("PPN"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($ppn,0,',','.'))==1){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==2){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==3){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==4){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==5){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==6){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==7){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==8){
						$jarak_ppn=5;
						}else if(strlen(number_format($ppn,0,',','.'))==9){
						$jarak_ppn=5;
						}
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($ppn,0,',','.'))+$jarak_ppn));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($ppn,0,',','.');
						
						
						
						
						
						$echo .= "\r\n";
						$limit_spasi = 90;
						for($i=0;$i<$limit_spasi;$i++)
						{
							$echo.=" ";
						}
						
						$echo.="Grand Total";
						$limit_spasi = 12;
						for($i=0;$i<($limit_spasi-strlen("Grand Total"));$i++)
						{
							$echo.=" ";
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==1){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total0,',','.'))==2){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==3){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==4){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==5){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==6){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==7){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==8){
						$jarak_grand_total=5;
						}else if(strlen(number_format($grand_total,0,',','.'))==9){
						$jarak_grand_total=5;
						}
						
						if(strlen(number_format($grand_total,0,',','.'))==10){
						$echo.=number_format($grand_total,0,',','.');
						}else{
						$limit_spasi = 15;
						for($i=0;$i<($limit_spasi-(strlen(number_format($grand_total,0,',','.'))+$jarak_grand_total));$i++)
						{
							$echo.=" ";
						}
						$echo.=number_format($grand_total,0,',','.');
						}
						
						
						$echo .= "\r\n";						
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Terbilang: ".terbilang($grand_total,$style=4)." Rupiah";
						
						$echo .= "\r\n";
						$echo .= $spasi_awal;
						$echo .= $arr_epson["cond"]."Note : ".$header->note;
			
            $echo .= "\r\n";
            $echo .= "\r\n";
            
            
            $limit_spasi = 15;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Penerima";
            $limit_spasi = 29;
            for($i=0;$i<($limit_spasi-strlen("Penerima"));$i++)
            {
                $echo.=" ";
            }
            
            $echo.="Pengirim";
            $limit_spasi = 28;
            for($i=0;$i<($limit_spasi-strlen("Pengirim"));$i++)
            {
                $echo.=" ";
                
            }
            
            $echo.="Mengetahui,";
            $limit_spasi = 10;
            for($i=0;$i<($limit_spasi-strlen("Mengetahui,"));$i++)
            {
                $echo.=" ";
            }
            
            $limit_enter = 4;
            for($i=0;$i<$limit_enter;$i++)
            {
                $echo .= "\r\n";
            }
            
            $limit_spasi = 12;
            for($i=0;$i<$limit_spasi;$i++)
            {
                $echo.=" ";
            }
                  
            $echo.=" (               )             (                 )         (                )";
            
            $echo .= "\r\n";
            $echo .= "\r\n";
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			if($TotalLogPrint*1>0)
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }
			    
			    $echo.=chr(15)."COPIED : ".(($TotalLogPrint*1)+1)."  HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);        
       
				 
			}
			else
			{
			    //$echo .= $arr_epson["reset"];
			    $limit_spasi = 135;
			    for($i=0;$i<($limit_spasi-strlen("HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s")));$i++)
			    {
			        $echo.=" ";
			    }

			   $echo.=chr(15)."HAL [".$i_page."/".$jml_page."] printed : ".date("d-m-Y H:i:s").chr(18);
			   
			}
            
            $echo .= "\r\n";  
			
			$TotalLogPrint = $this->globalmodel->getLogPrint($header->invoiceno,"sales-invoice");
			
			
		        $data = array(
		            'form_data' => "sales-invoice",
		            'noreferensi' => $header->invoiceno,
		            'userid' => $user,
		            'print_date' => date('Y-m-d H:i:s'),
		            'print_page' => "Setengah Letter"
		        );

		        $this->db->insert('log_print', $data);
	        
		}

		$paths = "path/to/";
	    $name_text_file='sales-invoice-'.$user.'.txt';
	    $mylib->create_txt_report($paths,$name_text_file,$echo);
	    
		header("Content-type: application/txt");
		header("Content-Disposition: attachment; filename=" . $name_text_file);
		$content = read_file($paths."/".$name_text_file);
		echo $content;
		
	}

}

?>