<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Generate_Invoice_VCI extends authcontroller {
    function __construct()
    {
        parent::__construct();    
        error_reporting(0);                              
        $this->load->library('globallib');
		$this->load->model('transaksi/generate_invoice_vcimodel');
    }

    function index()
    {    
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
        	$user = $this->session->userdata('username');
			$tahun = array('2017','2018','2019');
			$bulan = array('1','2','3','4','5','6','7','8','9','10','11','12');
			$data['tahun'] = $tahun;
			$data['bulan'] = $bulan;
			
            $this->load->view('transaksi/generate_invoice_vci/views', $data);  
        } 
        else 
        {
            $this->load->view('denied');
        }
    }     

    function getdetail(){
		$mylib = new globallib();
		$data = $this->input->post('data');
		
		$v_tglawal = date("Y-m-d",strtotime($data[0]));
		$v_tglakhir = date("Y-m-d",strtotime($data[1]));

		if($this->generate_invoice_vcimodel->ceksudahada($v_tglawal, $v_tglakhir)){
			$data['sudahada'] = true;
			$this->load->view('transaksi/generate_invoice_vci/tampil', $data);  
		}else{
			// $tglawal = $thn.'-'.$bln.'-'.'01';					
			// $tglakhir = $thn.'-'.$bln.'-'.'31';
			$tglawal = $v_tglawal;
			$tglakhir = $v_tglakhir;
			$url = 'http://192.168.0.17/sss/update_npm.php?tgl1='.$tglawal.'&tgl2='.$tglakhir;
			$aContext = array(
		    	'http' => array(
		        	'request_fulluri' => true,
		    	),
			);
			$cxContext = stream_context_create($aContext);
			try {
		     	$resp = file_get_contents($url, False, $cxContext);
		        $result = json_decode($resp, true);
		        $data = array();
		        $lengkap = true;
		        $datavci = array();
		        $tempdatapcode = array();
		        $datapcode = array();
		        for($i=0;$i<count($result);$i++){
					$pcodevci = $result[$i]['PCode'];
					$qty = $result[$i]['Qty'];
					$namabarangvci = $result[$i]['NamaBarang'];
					
					$hasil = $this->generate_invoice_vcimodel->getharga($pcodevci);
					$hargabeli = $this->generate_invoice_vcimodel->gethargabeli($pcodevci, $tglakhir);
					if(count($hasil)>0){
						$namalengkap = $hasil[0]['NamaLengkap'];
						$harga = $hasil[0]['Harga'];
						$pcode = $hasil[0]['PCode'];	
					}else{
						$namalengkap = "Belum ada konversi / harga jual - " . $namabarangvci;
						$harga = 0;
						$pcode = $pcodevci;
						$lengkap = false;
					}
					if(count($hargabeli)>0){
						$hargacontract = $hargabeli[0]['HargaContract'];
					}else{
						$namalengkap = "Belum ada konversi / harga beli - " . $namabarangvci;
						$hargacontract = 0;
						$pcode = $pcodevci;
						$lengkap = false;
					}
					if(empty($tempdatapcode[$pcode])){
						$datapcode[] = $pcode;
						$tempdatapcode[$pcode]=1;
					}
					$datavci[$pcode]['Qty'] += $qty;
					$datavci[$pcode]['Harga'] = $harga;
					$datavci[$pcode]['PCode'] = $pcode;
					$datavci[$pcode]['NamaBarang'] = $namalengkap;
					$datavci[$pcode]['HargaContract'] = $hargacontract;
				}
				for($i=0;$i<count($datapcode);$i++){	
					$pcodevci = $datapcode[$i];
					$pcode = $datavci[$pcodevci]['PCode'];
					$qty = $datavci[$pcodevci]['Qty'];
					$harga = $datavci[$pcodevci]['Harga'];
					$namalengkap = $datavci[$pcodevci]['NamaBarang'];
					$hargacontract = $datavci[$pcodevci]['HargaContract'];
					
					$data['data'][$i] = array('PCode'=> $pcode, 'NamaLengkap'=> $namalengkap, 'Qty'=> $qty, 'Harga'=> $harga, 'HargaContract'=> $hargacontract);
				}
				
				$data['Lengkap'] = $lengkap;
				$data['sudahada'] = false;
				$data['excel'] = $excel;
				//print_r($data);die();
				$this->load->view('transaksi/generate_invoice_vci/tampil', $data);  
			}
			catch (Exception $e) {
		    	echo "Koneksi gagal.";
				
			}
		}
	}

	function getDetailTombolAjaib() {
		$data1 = $this->input->post('data');
		
		$v_tglawal = date("Y-m-d",strtotime($data1[0]));
		$v_tglakhir = date("Y-m-d",strtotime($data1[1]));

		$this->db->select('pcode AS PCode,nama_barang AS NamaLengkap, qty AS Qty, harga_jual AS Harga, harga_contract AS HargaContract ');
		$this->db->from('parking_data_distribusi');
		$this->db->where('tanggal BETWEEN "'.$v_tglawal.'" AND "'.$v_tglakhir.'" ');
		$a = $this->db->get();
		$result = $a->result_array();
		$data['data'] = $result;
		$data['Lengkap'] = false;
		$this->load->view('transaksi/generate_invoice_vci/tampil_ajaib',$data);  
	}
	
	function simpan(){
		$mylib = new globallib();
		
		$v_start_date = $this->input->post('v_start_date');
		$v_end_date = $this->input->post('v_end_date');

		$tahun = date("Y",strtotime($v_end_date));
		$bulan = date("m",strtotime($v_end_date));

		$pcode = $this->input->post('PCode');
		$qty = $this->input->post('Qty');
		$harga = $this->input->post('Harga');
		$hargacontract = $this->input->post('HargaContract');
		$excel = $this->input->post('excel');
		

		if($excel=='excel'){
			// $tglawal = $tahun.'-0'.$bulan.'-'.'01';					
			// $tglakhir = $tahun.'-0'.$bulan.'-'.'31';
			$tglawal = date("Y-m-d",strtotime($v_start_date));
			$tglakhir = date("Y-m-d",strtotime($v_end_date));
			$url = 'http://192.168.0.17/sss/update_npm.php?tgl1='.$tglawal.'&tgl2='.$tglakhir; 
			$aContext = array(
		    	'http' => array(
		        	'request_fulluri' => true,
		    	),
			);
			$cxContext = stream_context_create($aContext);
			
		     	$resp = file_get_contents($url, False, $cxContext);
		        $result = json_decode($resp, true);
		        $data = array();
		        $lengkap = true;
		        $datavci = array();
		        $tempdatapcode = array();
		        $datapcode = array();
		        for($i=0;$i<count($result);$i++){
					$pcodevci = $result[$i]['PCode'];
					$qty = $result[$i]['Qty'];
					$namabarangvci = $result[$i]['NamaBarang'];
					
					$hasil = $this->generate_invoice_vcimodel->getharga($pcodevci);
					$hargabeli = $this->generate_invoice_vcimodel->gethargabeli($pcodevci, $tglakhir);
					if(count($hasil)>0){
						$namalengkap = $hasil[0]['NamaLengkap'];
						$harga = $hasil[0]['Harga'];
						$pcode = $hasil[0]['PCode'];	
					}else{
						$namalengkap = "Belum ada konversi / harga jual - " . $namabarangvci;
						$harga = 0;
						$pcode = $pcodevci;
						$lengkap = false;
					}
					if(count($hargabeli)>0){
						$hargacontract = $hargabeli[0]['HargaContract'];
					}else{
						$namalengkap = "Belum ada konversi / harga beli - " . $namabarangvci;
						$hargacontract = 0;
						$pcode = $pcodevci;
						$lengkap = false;
					}
					if(empty($tempdatapcode[$pcode])){
						$datapcode[] = $pcode;
						$tempdatapcode[$pcode]=1;
					}
					$datavci[$pcode]['Qty'] += $qty;
					$datavci[$pcode]['Harga'] = $harga;
					$datavci[$pcode]['PCode'] = $pcode;
					$datavci[$pcode]['NamaBarang'] = $namalengkap;
					$datavci[$pcode]['HargaContract'] = $hargacontract;
				}
				for($i=0;$i<count($datapcode);$i++){	
					$pcodevci = $datapcode[$i];
					$pcode = $datavci[$pcodevci]['PCode'];
					$qty = $datavci[$pcodevci]['Qty'];
					$harga = $datavci[$pcodevci]['Harga'];
					$namalengkap = $datavci[$pcodevci]['NamaBarang'];
					$hargacontract = $datavci[$pcodevci]['HargaContract'];
					
					$data['data'][$i] = array('PCode'=> $pcode, 'NamaLengkap'=> $namalengkap, 'Qty'=> $qty, 'Harga'=> $harga, 'HargaContract'=> $hargacontract);
				}
				$data['Lengkap'] = $lengkap;
				$data['sudahada'] = false;
				$data['excel'] = $excel;
				$this->load->view('transaksi/generate_invoice_vci/tampil_excel', $data);  
			
			
		}else{
		
		// if($bulan<10)
		// $bulan = '0'.$bulan;
		
		$tglawal = date("Y-m-d",strtotime($v_start_date));
		$tglakhir = date("Y-m-d",strtotime($v_end_date));
		$dono = $mylib->get_code_counter($this->db->database, "deliveryorder","dono", "DO", $bulan, $tahun);
		$rgno = $mylib->get_code_counter($this->db->database, "trans_terima_header","NoDokumen", "RG", $bulan, $tahun);
		$invoiceno = $mylib->get_code_counter($this->db->database, "salesinvoice","invoiceno", "SI", $bulan, $tahun);
		$invoicepembelianno = $mylib->get_code_counter($this->db->database, "invoice_pembelian_header","NoFaktur", "PI", $bulan, $tahun);
		
		$this->generate_invoice_vcimodel->simpan($rgno, $invoicepembelianno,$dono, $invoiceno, $pcode, $qty, $harga, $hargacontract, $tahun, $bulan,$tglakhir );
		
		redirect('transaksi/generate_invoice_vci');
		}
	}
	
	function mail($tglawal=null, $today = null)
	{
		$mylib = new globallib();
			
		if ($today == null)
		     $today = date("Y-m-d");
		list($thn,$bln,$tgl) = explode('-',$today);
		
		if($tglawal==null)
			$tglawal = $thn.'-'.$bln.'-'.'01';		
		
						
		$tglakhir = $today;
		
		$url = 'http://192.168.0.17/sss/update_npm.php?tgl1='.$tglawal.'&tgl2='.$tglakhir;

		$urltd = 'http://192.168.0.17/sss/update_npm.php?tgl1='.$tglakhir.'&tgl2='.$tglakhir;
		
		$aContext = array(
			'http' => array(
				'request_fulluri' => true,
			),
		);
		
		$aContexttd = array(
			'http' => array(
				'request_fulluri' => true,
			),
		);
		
		$cxContext = stream_context_create($aContext);
		$cxContexttd = stream_context_create($aContexttd);
		try {
			$resp = file_get_contents($url, False, $cxContext);
			$result = json_decode($resp, true);
			
			$resptd = file_get_contents($urltd, False, $cxContexttd);
			$resulttd = json_decode($resptd, true);
			
			$data = array();
			$lengkap = true;
			$datavci = array();
			$tempdatapcode = array();
			$datapcode = array();
			for($i=0;$i<count($result);$i++){
				$pcodevci = $result[$i]['PCode'];
				$qty = $result[$i]['Qty'];
				$namabarangvci = $result[$i]['NamaBarang'];
				
				$hasil = $this->generate_invoice_vcimodel->getharga($pcodevci);
				$hargabeli = $this->generate_invoice_vcimodel->gethargabeli($pcodevci, $tglakhir);
				if(count($hasil)>0){
					$namalengkap = $hasil[0]['NamaLengkap'];
					$harga = $hasil[0]['Harga'];
					$pcode = $hasil[0]['PCode'];	
				}else{
					$namalengkap = "Belum ada konversi / harga jual - " . $namabarangvci;
					$harga = 0;
					$pcode = $pcodevci;
					$lengkap = false;
				}
				if(count($hargabeli)>0){
					$hargacontract = $hargabeli[0]['HargaContract'];
				}else{
					$namalengkap = "Belum ada konversi / harga beli - " . $namabarangvci;
					$hargacontract = 0;
					$pcode = $pcodevci;
					$lengkap = false;
				}
				
				if(empty($tempdatapcode[$pcode])){
					$datapcode[] = $pcode;
					$tempdatapcode[$pcode]=1;
				}
				$datavci[$pcode]['Qty'] += $qty;
				$datavci[$pcode]['QtyTd'] = 0;
				$datavci[$pcode]['Harga'] = $harga;
				$datavci[$pcode]['PCode'] = $pcode;
				$datavci[$pcode]['NamaBarang'] = $namalengkap;
				$datavci[$pcode]['HargaContract'] = $hargacontract;
			}
			for($i=0;$i<count($resulttd);$i++){
				$pcodevci = $result[$i]['PCode'];
				$qty = $result[$i]['Qty'];
				$namabarangvci = $result[$i]['NamaBarang'];
				
				$hasil = $this->generate_invoice_vcimodel->getharga($pcodevci);
				if(count($hasil)>0){
					$namalengkap = $hasil[0]['NamaLengkap'];
					$harga = $hasil[0]['Harga'];
					$pcode = $hasil[0]['PCode'];	
				}else{
					$namalengkap = "Belum ada konversi / harga jual - " . $namabarangvci;
					$harga = 0;
					$pcode = $pcodevci;
					$lengkap = false;
				}
				if(empty($tempdatapcode[$pcode])){
					$datapcode[] = $pcode;
					$tempdatapcode[$pcode]=1;
				}
				$datavci[$pcode]['QtyTd'] += $qty;
			}
			
			for($i=0;$i<count($datapcode);$i++){	
				$pcodevci = $datapcode[$i];
				$pcode = $datavci[$pcodevci]['PCode'];
				$qty = $datavci[$pcodevci]['Qty'];
				$qtytd = $datavci[$pcodevci]['QtyTd'];
				$harga = $datavci[$pcodevci]['Harga'];
				$namalengkap = $datavci[$pcodevci]['NamaBarang'];
				$hargacontract = $datavci[$pcodevci]['HargaContract'];
				
				$data['data'][$i] = array('PCode'=> $pcode, 'NamaLengkap'=> $namalengkap, 'QtyTd'=>$qtytd,'Qty'=> $qty, 'Harga'=> $harga, 'HargaBeli'=>$hargacontract);
			}
			$data['listemail'] = $this->generate_invoice_vcimodel->getlistemail();
			$data['tgl_awal'] = $tglawal ;
			$data['tgl_akhir'] = $tglakhir;
			$this->load->view('transaksi/generate_invoice_vci/tampil_mail', $data);  
		}
		catch (Exception $e) {
			echo "Koneksi gagal.";
			
		}
	}
	
	function test($pcode){
		$hasil = $this->getharga($pcode);
		print_r($hasil);
	}
}

?>