<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class purchase_invoice extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/purchase_invoicemodel');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
                
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            

            $data["search_keyword"] = "";
            $data["search_gudang"] = "";
            $data["search_divisi"] = "";
            $data["search_status"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "purchase_invoice", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                }
            }
            
			
            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/purchase_invoice/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/purchase_invoice/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

           
            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->purchase_invoicemodel->num_purchase_invoice_row($arr_search["search"]);
            $data['data'] = $this->purchase_invoicemodel->getPurchaseInvoiceList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $this->load->view('transaksi/invoice_pembelian/invoice_pembelianlist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'purchase_invoice', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "purchase_invoice",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/purchase_invoice/index/' . $query_id . '');
    }

    function add_new() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['supplier'] = $this->purchase_invoicemodel->getSupplier();
            $data['matauang'] = $this->purchase_invoicemodel->getMataUang();
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/invoice_pembelian/add_invoice_pembelian', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
     function view($nofaktur) 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $user = $this->session->userdata('username');
            $userlevel = $this->session->userdata('userlevel');
            
            
            $data['data'] = $this->purchase_invoicemodel->getDataHeader($nofaktur);
            $data['row'] = $this->purchase_invoicemodel->getDataDetail($nofaktur);
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/invoice_pembelian/view_invoice_pembelian', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    function getPO($kdsupplier){
    	$datapo= $this->purchase_invoicemodel->getPO($kdsupplier);
    	
    	echo json_encode($datapo);
		
	}
	
	function getRG($NoPO, $tgl){
		$mylib = new globallib();
    	$datarg= $this->purchase_invoicemodel->getRG($NoPO, $mylib->ubah_tanggal($tgl));
    	
    	echo json_encode($datarg);
		
	}
	
	function getDetail(){
		$NoPenerimaan = $this->input->post('NoPenerimaan');
		$row = $this->purchase_invoicemodel->getDetail($NoPenerimaan);
		
		$data['num'] = count($row);
		$data['row'] = $row;
		 	
		$this->load->view('transaksi/invoice_pembelian/invoice_pembelian_detail', $data);	
	}

   
    function save_data() 
    {
        $mylib = new globallib();
        $tgl = $mylib->ubah_tanggal($this->input->post('tanggal'));
        $mata_uang = $this->input->post('mata_uang');
        $kdsupplier = $this->input->post('KdSupplier');
        $jatuhtempo = $this->input->post('jatuhtempo');
        $kurs = $this->input->post('kurs');
        $nofaktursupplier = $this->input->post('nofaktursupplier');
        $ppn = $this->input->post('ppn');
        $nopo = $this->input->post('NoPO');
        $nofakturpajak = $this->input->post('nofakturpajak');
        $nopenerimaan = $this->input->post('NoPenerimaan');
        $gtotal = $this->input->post('gtotal');
        
        // detail
        $nourut = $this->input->post('nourut');
        $pcode = $this->input->post('pcode');
        $nopenerimaandtl = $this->input->post('nopenerimaandtl');
        $qty = $this->input->post('qty');
        $satuan = $this->input->post('satuan');
        $harga = $this->input->post('harga');
        
        $user = $this->session->userdata('username');

        $nofaktur = $this->purchase_invoicemodel->getNoFaktur($tgl);

        $this->purchase_invoicemodel->insertHeader($nofaktur,$tgl,$jatuhtempo,$nopo,$nofaktursupplier,$nofakturpajak,$kdsupplier,$mata_uang,$kurs,$ppn, $user);
		$this->purchase_invoicemodel->insertDetail($nofaktur,$nourut,$pcode,$nopenerimaandtl,$qty,$satuan,$harga,$ppn);
		$this->purchase_invoicemodel->insertHutang($tgl,$nofaktur, $kdsupplier,$mata_uang,$kurs, $gtotal, $jatuhtempo);	
		$this->purchase_invoicemodel->insertMutasiHutang($tgl,$nofaktur, $kdsupplier,$mata_uang,$kurs, $gtotal);	
		$this->purchase_invoicemodel->UpdateRG($nopenerimaan);	
		
        redirect('/transaksi/purchase_invoice');
    }
}

?>