<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Split_order extends authcontroller {
	function __construct(){
        parent::__construct();
        $this->load->library('globallib');
		$this->load->model('globalmodel');
        $this->load->model('transaksi/split_order_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
			//print_r($data);die();
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '12';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/split_order/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/split_order/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/split_order/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("No Kassa","No Transaksi","Tanggal","Waktu","Total Item","Table","Person", "Aksi");
	        $config['total_rows']		= $this->split_order_model->num_order_row($id,$with);
	        $data['data']	= $this->split_order_model->getList($config['per_page'],$page,$id,$with);
			$data['tanggal'] = $this->split_order_model->getDate();
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/split_order/split_order_list', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }
	
	function add_split_order($meja)
    {
    	
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
        	$data['header'] = $this->split_order_model->getHeader($meja);
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/split_order/add_split_order', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
	function edit_split_order($meja) 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        $notrans = $this->input->post('v_trans');
        $kdmeja = $this->input->post('v_meja');
        $tgl = $this->input->post('v_tgl_dokumen');
        $jmlsplit = $this->input->post('v_jml_split');
        
        $data['header'] = $this->split_order_model->getHeader($kdmeja);
        $data['detail'] = $this->split_order_model->getDetail($kdmeja);
        $data['sumdetail'] = $this->split_order_model->getSumDetail($kdmeja);
        
        $data['jmlsplit']=$jmlsplit;
        $data['track'] = $mylib->print_track();
        $this->load->view('transaksi/split_order/edit_split_order', $data);
    }
    
    function pecah_order() 
    {
        $mylib = new globallib();
        
        $kdmeja= $this->uri->segment(4);
	    $tgl = $this->uri->segment(5);
		$jmlsplit= $this->uri->segment(6);
        
        $data['header'] = $this->split_order_model->getHeader($kdmeja);
        $data['detail'] = $this->split_order_model->getDetail($kdmeja);
        $data['sumdetail'] = $this->split_order_model->getSumDetail($kdmeja);
        
         $data['table']=$kdmeja;
         
        $data['jmlsplit']=$jmlsplit;
        $data['track'] = $mylib->print_track();
        
        $this->load->view('transaksi/split_order/detail_split_order', $data);
    }
	
	function save_data() 
    {
        echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $notrans = $this->input->post('v_trans');
        $kdmeja = $this->input->post('v_meja');
        $totalitem = $this->input->post('v_totalitem');
        $tgl = $this->input->post('v_tgl_dokumen');
        $jmlsplit = $this->input->post('v_jml_split');
        $nokassa = $this->input->post('v_nokassa');
        
        //detail
        $v_pcode1 = $this->input->post('pcode');
        $v_qty1 = $this->input->post('qty');
        
        //update status trans_order_header dan detail
        $this->db->update('trans_order_header',array('Status'=>2),array('NoTrans'=>$notrans));
        $this->db->update('trans_order_detail',array('Status'=>2),array('NoTrans'=>$notrans));
        
        //buat no_trans kembali
        $ambil_no_trans = $this->split_order_model->getNoHeader();
        $new_no_trans = $ambil_no_trans->NoTrans+1; 
        
        $data_header = array
		             ('NoTrans'=>$new_no_trans,
		             'NoKassa'=>$nokassa,
		             'Tanggal'=>$mylib->ubah_tanggal($tgl),
		             'Waktu'=>'',
					 'Kasir'=>'',
					 'KdStore'=>'00',
		             'TotalItem'=>$totalitem,
		             'Status'=>1,
		             'KdPersonal'=>'',
		             'KdMeja'=>$kdmeja,
		             'KdContact'=>'',
		             'nokasst'=>'',
		             'nostruk'=>'',
		             'TotalGuest'=>'',
		             'AddDate'=>'',
		             'keterangan'=>'',
		             'KdAgent'=>'',
		             'IsCommit'=>'1');
        
        $this->db->insert('trans_order_header',$data_header);
        
        for ($x = 0; $x < count($v_pcode1); $x++) 
			{
             
	            for ($y = 0; $y < $jmlsplit; $y++) 
				{
					$xplus=$x+1;
					$split_ = "split$xplus";
                    $v_split = $this->input->post($split_);
                
					$pcode = strtoupper(addslashes(trim($v_pcode1[$x])));
                    $v_qty = $mylib->save_int($v_qty1[$x]);
					$split = strtoupper(addslashes(trim($v_split[$y])));
					
					//simpan ke detail
					$data_baru = array 
								('NoTrans'=>$new_no_trans,
					             'NoUrut'=>$y+1,
					             'NoKassa'=>$nokassa,
					             'Tanggal'=>$mylib->ubah_tanggal($tgl),
					             'Waktu'=>'',
					             'Kasir'=>'',
					             'KdStore'=>'00',
					             'PCode'=>$pcode,
					             'Qty'=>$split,
					             'Satuan'=>'',
					             'Keterangan'=>'',
					             'Status'=>1,
					             'KdPersonal'=>'',
					             'KdMeja'=>$kdmeja,
					             'KdContact'=>'');
					$this->db->insert('trans_order_detail',$data_baru);
				}
				
				
                    
            }
        
		redirect('/transaksi/split_order/edit_split_order/' . $new_no_trans . '');
        
    }
	
	function save_header(){
	    $notrans = $this->input->post('no_trans');
	    $meja = $this->input->post('meja');
	    $id = $this->input->post('id'); //Qty
	    
		//pertama update header terlebih dahulu
		//$this->db->update('trans_order_header',array('Status'=>2),array('NoTrans'=>$notrans));
		
		$hrf = $this->abjad($id);
		$kdmeja=$meja."".$hrf;
		//insert baru dengan ambil nilai terakhir
		$ambil_no_trans = $this->split_order_model->getNoHeader();
        $new_no_trans = $ambil_no_trans->NoTrans+1;
		
		$cek = $this->split_order_model->getCek($new_no_trans);
		
		if(empty($cek)){
		$sql = "
				INSERT INTO `trans_order_header`
				(`NoTrans`,
				 `NoKassa`,
				 `Tanggal`,
				 `Waktu`,
				 `Kasir`,
				 `KdStore`,
				 `TotalItem`,
				 `Status`,
				 `KdPersonal`,
				 `KdMeja`,
				 `KdContact`,
				 `nokasst`,
				 `nostruk`,
				 `TotalGuest`,
				 `AddDate`,
				 `keterangan`,
				 `KdAgent`,
				 `IsCommit`)
				 
				   SELECT 
					'".$new_no_trans."' AS NoTrans,
					NoKassa,
					Tanggal,
					Waktu,
					Kasir,
					KdStore,
					TotalItem,
					'0' AS STATUS,
					KdPersonal,
					'".$kdmeja."',
					KdContact,
					nokasst,
					nostruk,
					TotalGuest,
					ADDDATE,
					'".$notrans."',
					KdAgent,
					IsCommit
					
					FROM
					`trans_order_header`
					WHERE
					NoTrans = '".$notrans."';
					
					;
				";
		$this->db->query($sql);	
		
		//meja aslinya di cancel
		$this->db->update('trans_order_header',array('Status'=>'2'),array('NoTrans'=>$notrans,'KdMeja'=>$meja));	
		}
		$data['notrans']=$new_no_trans;
		echo json_encode($data);
	}
	
	function save_split_order(){
	    $notrans = $this->input->post('no_trans');
	    $kdmeja = $this->input->post('meja');
	    
		//insert baru dengan ambil nilai terakhir
		$ambil_no_trans = $this->split_order_model->getNoHeader();
        $new_no_trans = $ambil_no_trans->NoTrans+1;
		
		$cek = $this->split_order_model->getCek($new_no_trans);
		
		if(empty($cek)){
		$sql = "
				INSERT INTO `trans_order_header`
				(`NoTrans`,
				 `NoKassa`,
				 `Tanggal`,
				 `Waktu`,
				 `Kasir`,
				 `KdStore`,
				 `TotalItem`,
				 `Status`,
				 `KdPersonal`,
				 `KdMeja`,
				 `KdContact`,
				 `nokasst`,
				 `nostruk`,
				 `TotalGuest`,
				 `AddDate`,
				 `keterangan`,
				 `KdAgent`,
				 `IsCommit`)
				 
				   SELECT 
					'".$new_no_trans."' AS NoTrans,
					NoKassa,
					Tanggal,
					Waktu,
					Kasir,
					KdStore,
					TotalItem,
					'0' AS STATUS,
					KdPersonal,
					'".$kdmeja."',
					KdContact,
					nokasst,
					nostruk,
					TotalGuest,
					ADDDATE,
					'".$notrans."',
					KdAgent,
					IsCommit
					
					FROM
					`trans_order_header`
					WHERE
					NoTrans = '".$notrans."';
					
					;
				";
		$this->db->query($sql);	
		
		//meja aslinya di cancel
		$this->db->update('trans_order_header',array('Status'=>'2'),array('NoTrans'=>$notrans,'KdMeja'=>$meja));	
		}
		
		$data['notrans']=$new_no_trans;
		echo json_encode($data);
	}
	
	function save_detail(){
	    $notrans_old = $this->input->post('no_trans_old');
		$notrans_new = $this->input->post('no_trans_new');
        $qty_split = $this->input->post('qty_split');
		$pcode = $this->input->post('pc');
		$meja = $this->input->post('meja');
		$id = $this->input->post('id');
		$no = $this->input->post('no');

		$hrf = $this->abjad($id);
		$kdmeja=$meja."".$hrf;
		
		//cek apakah PCode yang sama ada atau tidak?
		$cek_pcode = $this->split_order_model->cekPCode($notrans_new,$pcode);
		if(empty($cek_pcode)){	
		
			//insert detail baru
		    $sql="
			INSERT INTO `trans_order_detail`
            (`NoTrans`,
             `NoUrut`,
             `NoKassa`,
             `Tanggal`,
             `Waktu`,
             `Kasir`,
             `KdStore`,
             `PCode`,
             `Qty`,
             `Satuan`,
             `Keterangan`,
             `Note_split`,
             `Status`,
             `KdPersonal`,
             `KdMeja`,
             `KdContact`)
             
				SELECT
				'".$notrans_new."',
				'".$no."',
				NoKassa,
				Tanggal,
				Waktu,
				Kasir,
				KdStore,
				'".$pcode."',
				'".$qty_split."',
				Satuan,
				Keterangan,
				'".$notrans_old."',
				'1',
				KdPersonal,
				'".$kdmeja."',
				KdContact
				
				FROM
				`trans_order_detail`
				WHERE NoTrans='".$notrans_old."'
				AND pcode='".$pcode."';
			";
		    $this->db->query($sql);
		
		}else{
		$qty_split_ =$qty_split+$cek_pcode->Qty;
		$this->db->update('trans_order_detail',array('Qty'=>$qty_split_),array('NoTrans'=>$notrans_new));
	    }
		
			
		
		//sum qyt notrans baru
		$sum_baru = $cek = $this->split_order_model->getSum($notrans_new);	
		$tot_sum_baru = $sum_baru->tot_qty;
		
		//update header
		$this->db->update('trans_order_header',array('TotalItem'=>$tot_sum_baru),array('NoTrans'=>$notrans_new));
		
		//voidkan notrans lama baik di header dan detail
		$this->db->update('trans_order_header',array('Status'=>2,'Keterangan'=>'Split Order'),array('NoTrans'=>$notrans_old));
		$this->db->update('trans_order_detail',array('Status'=>2,'Note_split'=>'Split Order'),array('NoTrans'=>$notrans_old));
	
	}
	
	
	function save_detail_new(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die();
	    $notrans_old = $this->input->post('no_trans_old');		
        $qty_split = $this->input->post('qty_split');
		$pcode = $this->input->post('pc');
		$kdmeja = $this->input->post('meja');
		$jmlrecord = $this->input->post('jmlrecord');
		$tambah_urutan_id = $this->input->post('tambah_urutan_id');
		//explode qty_split
		$pecah_qty_split = explode("-",$qty_split);
		//explode pcode
		$pecah_pcode = explode("-",$pcode);
		//explode kdmeja
		$pecah_meja = explode("-",$kdmeja);
		
		//insert baru dengan ambil nilai terakhir
		$ambil_no_trans = $this->split_order_model->getNoHeader();
        $notrans_new  = $ambil_no_trans->NoTrans+$tambah_urutan_id;
		
		for($x=0;$x<$jmlrecord;$x++){
			
			$split_qty = $pecah_qty_split[$x];
			$split_pcode = $pecah_pcode[$x];
			$split_meja = $pecah_meja[$x];
		
		
		    if(!empty($split_qty)){
			//insert detail baru
		    $sql="
			INSERT INTO `trans_order_detail`
            (`NoTrans`,
             `NoUrut`,
             `NoKassa`,
             `Tanggal`,
             `Waktu`,
             `Kasir`,
             `KdStore`,
             `PCode`,
             `Qty`,
             `Satuan`,
             `Keterangan`,
             `Note_split`,
             `Status`,
             `KdPersonal`,
             `KdMeja`,
             `KdContact`)
             
				SELECT
				'".$notrans_new."',
				'". $x ."',
				NoKassa,
				Tanggal,
				Waktu,
				Kasir,
				KdStore,
				'".$split_pcode."',
				'".$split_qty."',
				Satuan,
				Keterangan,
				'".$notrans_old."',
				'1',
				KdPersonal,
				'".$split_meja."',
				KdContact
				
				FROM
				`trans_order_detail`
				WHERE NoTrans='".$notrans_old."'
				AND pcode='".$split_pcode."';
			";
		    $this->db->query($sql);
		    }
		    
			
			if(($x+1)==$jmlrecord){
						//sum qyt notrans baru
						$sum_baru = $cek = $this->split_order_model->getSum($notrans_new);	
						$tot_sum_baru = $sum_baru->tot_qty;
				if($tot_sum_baru!=0){		
						//insert header
						$sqlz = "
								INSERT INTO `trans_order_header`
								(`NoTrans`,
								 `NoKassa`,
								 `Tanggal`,
								 `Waktu`,
								 `Kasir`,
								 `KdStore`,
								 `TotalItem`,
								 `Status`,
								 `KdPersonal`,
								 `KdMeja`,
								 `KdContact`,
								 `nokasst`,
								 `nostruk`,
								 `TotalGuest`,
								 `AddDate`,
								 `keterangan`,
								 `KdAgent`,
								 `IsCommit`)
								 
								   SELECT 
									'".$notrans_new."' AS NoTrans,
									NoKassa,
									Tanggal,
									Waktu,
									Kasir,
									KdStore,
									'".$tot_sum_baru."',
									'0' AS STATUS,
									KdPersonal,
									'".$pecah_meja[0]."',
									KdContact,
									nokasst,
									nostruk,
									TotalGuest,
									ADDDATE,
									'".$notrans_old."',
									KdAgent,
									IsCommit
									
									FROM
									`trans_order_header`
									WHERE
									NoTrans = '".$notrans_old."';
									
									;
								";
						$this->db->query($sqlz);	
						
						
						//voidkan notrans lama baik di header dan detail
						$this->db->update('trans_order_header',array('Status'=>2,'Keterangan'=>'Split Order'),array('NoTrans'=>$notrans_old));
						$this->db->update('trans_order_detail',array('Status'=>2,'Note_split'=>'Split Order'),array('NoTrans'=>$notrans_old));
			    }else{
				$this->db->delete('trans_order_detail',array('NoTrans'=>$notrans_new));	
				}
			}
		}
		
	echo json_encode(array('success'=>true));
	}
	
	
	
	function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
		    $ipaddres                   = $this->session->userdata('ip_address');
			$nokassa                    = $this->split_order_model->getnokassa($ipaddres);
			$datakassa                  = $this->split_order_model->getkassa($ipaddres);
			$data['NoKassa'] 			= $nokassa;
			$data['msg'] 				= "";
			$data['datakassa'] 			= $datakassa;	
			$user_id = $this->session->userdata('userid');
			$username = $this->session->userdata('username');	
			$data['user_id'] 			= $user_id;
			$data['username'] 			= $username;
			$kddivisi                   = $datakassa[0]['KdKategori'];
			$data['userid']	 	        = $this->split_order_model->getUserId();
			$data['barang']	 		    = $this->split_order_model->getBarang();
			$data['kategori']	 		= $this->split_order_model->getKategori();
			$data['subkategori']	 	= $this->split_order_model->getSubKategori();
			$data['userid']	 	        = $this->split_order_model->getUserId();
			$data['lokasi']	 	        = $this->split_order_model->getLokasi($kddivisi);
			$data['keterangan']	 	    = $this->split_order_model->getKeterangan();
			$data['store']				= $this->split_order_model->aplikasi();
			$aplikasi = $this->split_order_model->getDate();
			$data['tanggal'] = $aplikasi->TglTrans2;
			
			//----------------untuk keperluan lcd----
			$ipaddress			= $_SERVER['REMOTE_ADDR']; 
			$data['iplokal']	= $ipaddress;
			//---------------eo untuk keper....------
			$this->load->view('transaksi/split_order/add_split_order',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	
	function save_trans()
	{
	    $transaksi 		= $this->input->post('transaksi');
		if($transaksi=="yes")
		{
			$EditFlg    = $this->input->post('EditFlg');
			$nourut1    = $this->input->post('nourut');
			$pcode1     = $this->input->post('pcode');
			$qty1       = $this->input->post('qty');
			$keterangan1= $this->input->post('keterangan');
			$kassa   = $this->input->post('kassa');
			$kasir   = $this->input->post('kasir');
			$store	 = $this->input->post('store');
			
			$kategorikassa	 = $this->input->post('kategorikassa');
			$tgltrans = $this->session->userdata('Tanggal_Trans');
			$userid = $this->session->userdata('userid');
			$username = $this->session->userdata('username');
			$tahun = substr($tgltrans,0,4);
			$bulan = substr($tgltrans,5,2);
			
			$idmeja	        = $this->input->post('idtable');
			$idguest        = $this->input->post('idguest');
			$idagent       = $this->input->post('idagent');
			if($idguest=="")$idguest=0;
			$personal	    = $this->input->post('idpersonal');
			$namapersonal = $this->input->post('namapersonal');
			$adddate        = date('y-m-d');
			$Jamsekarang = date("H:i:s");
			
			$data = array(
						'NoKassa'	   => $kassa,
						'NoStruk'	   => '',
						'Tanggal'	   => $adddate,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'TotalItem'    => count($pcode1),
						'Status'	  => "0",
						'KdMeja'	   => $idmeja,
						'KdAgent'	   => $idagent,
						'KdPersonal' => $namapersonal,
						'TotalGuest'   => $idguest
					);
			$this->db->insert('trans_order_header', $data);
			$notrans = $this->db->insert_id();
				
			for($x0=0;$x0<count($pcode1);$x0++)
			{
				$pcode = strtoupper(addslashes(trim($pcode1[$x0])));
				$nourut = $nourut1[$x0];
				$qty = trim($qty1[$x0]);
				$keterangan = $keterangan1[$x0];
				
				if($pcode!="" && $qty>0){
					$data = array(
						'NoKassa'	   => $kassa,
						'NoTrans'	   => $notrans,
						'NoUrut'       => $nourut,
						'Tanggal'	   => $adddate,
						'Waktu'	       => $Jamsekarang,
						'Kasir'		   => $kasir,
						'KdStore'	   => $store,
						'PCode'        => $pcode,
						'Qty'          => round($qty,2),
						'Status'       => "0",
						'Keterangan'	=> $keterangan,
						'KdMeja' 		=> $idmeja
					);
				    $this->db->insert('trans_order_detail', $data);
				}
			}
	   		
			$this->cetakall($notrans);
		} 
		else
	       $this->index();
	    
	}
	
	function cetakmeja($notrans){
		$store = $this->split_order_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->split_order_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->split_order_model->all_trans($notrans);
		$data['detail'] = $this->split_order_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		//print_r($data);
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		redirect('transaksi/split_order','refresh');
	}
	
	function cetakminum($notrans){
		$store = $this->split_order_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->split_order_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->split_order_model->all_trans($notrans);
		$data['detail'] = $this->split_order_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		redirect('transaksi/split_order','refresh');
	}
	
	function cetakmakan($notrans){
		$store = $this->split_order_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->split_order_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->split_order_model->all_trans($notrans);
		$data['detail'] = $this->split_order_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		redirect('transaksi/split_order','refresh');
	}
	
	function cetakall($notrans){
		// Meja
		$store = $this->split_order_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->split_order_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_order'];
		$data['nm_printer'] = $printer[0]['nm_printer_order'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->split_order_model->all_trans($notrans);
		$data['detail'] = $this->split_order_model->det_trans($notrans,1);
		$data['jenis'] = 1;
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		
		//Minuman
		$store = $this->split_order_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->split_order_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_bar'];
		$data['nm_printer'] = $printer[0]['nm_printer_bar'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->split_order_model->all_trans($notrans);
		$data['detail'] = $this->split_order_model->det_trans($notrans,2);
		$data['jenis'] = 2;
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		
		//Makanan
		$store = $this->split_order_model->aplikasi();
		$ipaddres   = $this->session->userdata('ip_address');
		$printer = $this->split_order_model->NamaPrinter($ipaddres);
		
		$data['store'] = $store;
		$data['ip'] = $printer[0]['ipprinter_kitchen'];
		$data['nm_printer'] = $printer[0]['nm_printer_kitchen'];
		$data['kdkategori'] = $printer[0]['KdKategori'];
		$data['header'] = $this->split_order_model->all_trans($notrans);
		$data['detail'] = $this->split_order_model->det_trans($notrans,3);
		$data['jenis'] = 3;
		$this->load->view('transaksi/split_order/cetak_transaksi_wireness', $data);
		
		redirect('transaksi/split_order/add_new/','refresh');
	}
	
	
	function clear_trans()
	{
	    $notrans 		= $this->input->post('notrans');
		$nokassa 		= $this->input->post('kassa1');
		//echo $notrans;
		//echo $nokassa;
		$this->split_order_model->clear_trans($notrans,$nokassa);
		$this->add_new();
	}

	function cektable(){
		$kdmeja = $this->input->post('data');
		$tgl = date('y-m-d');
		$OpenOrder = $this->split_order_model->getOpenOrder($tgl, $kdmeja);
		echo json_encode($OpenOrder);
	}
	
	function abjad($id) {
        switch ($id) {
            case 1:
                $blnRom = 'A';
                break;
            case 2:
                $blnRom = 'B';
                break;
            case 3:
                $blnRom = 'C';
                break;
            case 4:
                $blnRom = 'D';
                break;
            case 5:
                $blnRom = 'E';
                break;
            case 6:
                $blnRom = 'F';
                break;
            case 7:
                $blnRom = 'G';
                break;
            case 8:
                $blnRom = 'H';
                break;
            case 9:
                $blnRom = 'I';
                break;
			case 10:
                $blnRom = 'J';
                break;
            case 11:
                $blnRom = 'K';
                break;
            case 12:
                $blnRom = 'L';
                break;
        }
        return $blnRom;
    }
}