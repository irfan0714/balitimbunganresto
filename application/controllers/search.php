<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class search extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
			$data['cari'] = '';
			$data['track'] = $mylib->print_track();
			$this->load->view('search', $data);
		}
		else{
			$this->load->view('denied');
		}
    }
	function cari()
	{
		redirect("search/");
	}
}
?>