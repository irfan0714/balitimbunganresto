<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Shift extends authcontroller {
	
	function __construct() 
	{
		parent::__construct();
		$this->load->model('transaksi/shiftmodel'); 
		$this->load->library('globallib');
		$this->load->library('form_validation');
	}
	
	function submitShift()
	{
		$tgl 						= $this->input->post('tanggalShift');
		$kdDep 						= $this->input->post('kd');
		$buttonSubmit 				= $this->input->post('submit');
		$buttonCopy 				= $this->input->post('copy');
		
			
		
		$data['combo'] = $this->shiftmodel->ambilDept();
		
		if($buttonSubmit)
		{
			if($tgl == '' OR is_null($tgl) OR $kdDep == '' OR is_null($kdDep))
			{
				$data['status'] ="no";
				$data['data'] = $this->shiftmodel->ambilDept();
				$data['ShiftDate']="";
				$this->load->view('transaksi/shift/shift', $data);
				
			}
			else
			{
				
				$data['status']			= "yes";
				$data['combo']			= $this->shiftmodel->ambilDept();
				$data['list_shift']		= $this->shiftmodel->shift();		
				$datacomplete			= $this->shiftmodel->getDatacomplete($tgl,$kdDep);			
				$data['datacomplete']	= $datacomplete;
				
				if($tgl=='')
				{
					$data['ShiftDate']="";
				}
				else
				{
					$data['ShiftDate']=$tgl;
				}
				
				$this->load->view('transaksi/shift/shift',$data);
			
			}
			
		}
		else if($buttonCopy)
		{
			if($tgl == '' OR is_null($tgl) OR $kdDep == '' OR is_null($kdDep))
			{
				$data['status'] ="no";
				$data['data'] = $this->shiftmodel->ambilDept();
				$data['ShiftDate']="";
				$this->load->view('transaksi/shift/shift', $data);
			}
			else
			{
				
				$tanggalsebelumnya_arr		= $this->shiftmodel->getDataShiftCopy($tgl,$kdDep);
				$tanggalsebelumnya			= $tanggalsebelumnya_arr[0]['ShiftDate'];	
				$data['status']			= "yes";
				$data['combo']			= $this->shiftmodel->ambilDept();
				$data['list_shift']		= $this->shiftmodel->shift();		
				$datacomplete			= $this->shiftmodel->getDatacomplete($tanggalsebelumnya,$kdDep);			
				$data['datacomplete']	= $datacomplete;
				
				if($tgl=='')
				{
					$data['ShiftDate']="";
				}
				else
				{
					$data['ShiftDate']=$tgl;
				}
				
				$this->load->view('transaksi/shift/shift',$data);
			}
				
		}
	
		else
		{
			$data['ShiftDate']="";
			$data['status'] ="no";
			$data['combo'] = $this->shiftmodel->ambilDept();
			$this->load->view('transaksi/shift/shift', $data);
		}

	}
	
	function saveshift()
	{		
		$shf		= $this->input->post('shift');
		$kddep		= $this->input->post('kddep');
		$kdem		= $this->input->post('kodeem');
		$shfDate	= $this->input->post('tanggalShift');
		
		$this->db->trans_start();						//-----------------------------------------------------START TRANSAKSI
		
		$this->shiftmodel->deleted($kddep[1], $shfDate);
		
	
		for ($i=0;$i<count($kdem); $i++) 
		{	
			$datadetail	= array(
							'Shift' 		=> $shf[$i],
							'KdDepartemen' 	=> $kddep[$i],
							'KdEmployee' 	=> $kdem[$i],
							'ShiftDate' 	=> $shfDate,
								);
								
			$this->db->insert('shift', $datadetail);
			
			
		}
		
		$this->db->trans_complete();				//----------------------------------------------------END TRANSAKSI
			
		redirect('hr/shift/submitShift');	
	}
	
	function testing() {
		$shf		= $this->input->post('shift');
		$kddep		= $this->input->post('kddep');
		$kdem		= $this->input->post('kodeem');
		$shfDate	= $this->input->post('tanggalShift');
			
		
		echo "<pre>";
		print_r($tanggalsebelumnya);
		echo "</pre>";
	}
	
}