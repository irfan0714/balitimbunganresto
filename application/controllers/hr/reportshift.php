<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Reportshift extends authcontroller {

    function __construct() {
        parent::__construct();
		$this->load->model('transaksi/reportshiftmodel'); 
		$this->load->library('globallib');
        $this->load->library('report_lib');
    }

    function index() {
	
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {			
			$today					= date('Y-m-d');
			
			
			$nama					= '';
			$data['tgldari']		= $today;
			$data['tglsampai']		= $today;
			$data['nama']			= '';
			$data['tampilkanDT'] = false;
			//$data['excel']			= '';
			$dataresult				= $this->reportshiftmodel->viewData($today,$today,$nama);
			$data['viewdata']		= $dataresult; 
			$this->load->view('transaksi/report_shift/reportshiftview', $data);
//		} else {
//			$this->load->view('denied');
//		}
    }
	
	function view() {
		$mylib = new globallib();
		
		$submit		= $this->input->post('submit');
		$tgldari	= $this->input->post('tgldari');
		$tglsampai	= $this->input->post('tglsampai');
		$nama		= $this->input->post('namakaryawan');
		$today		= date('Y-m-d');
		
		$excel		= $this->input->post('submit');
    
		
		if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai) OR ($nama=='' OR is_null($nama)) )) {
			$tgldari	= $today;
			$tglsampai	= $today;
			$nama		= "";
		}
		
		$data['tgldari']	= $tgldari;
		$data['tglsampai']	= $tglsampai;
		$data['nama']		= $nama;
		$data['submit']		= '';
		$data['tampilkanDT'] = true;
		$dataresult			= $this->reportshiftmodel->getData($tgldari,$tglsampai, $nama);
		$data['viewdata']	= $dataresult;		
		$data['data']		= $dataresult;
		
          
		if($submit=='TAMPIL')
		{
			$this->load->view('transaksi/report_shift/reportshiftview', $data);
		}
		
		else if($submit=='XLS')
		{
			//$this->_printxls($dataresult);
		
				$data['track'] = $mylib->print_track();
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename="reportshift.xls"');
                
                $this->load->view('transaksi/report_shift/tampil', $data);
		}        
		
		
		/*else if($submit=='XLS')
		{	$this->_printxls($dataresult);
                
		}       
		
		*/
		//else
			//$this->tambah();

	}
	
	function _printxls($dataresult) {
		$this->load->helper('xl');
		sendXLHeader('reportshift');
		
		xlsBOF();
		
		xlsWriteLabel(0, 0, 'TANGGAL');
		xlsWriteLabel(0, 1, 'HARI');
		xlsWriteLabel(0, 2, 'MASUK');
		xlsWriteLabel(0, 3, 'KELUAR');	
		xlsWriteLabel(0, 4, 'SHIFT');
		
		$baris	= 1;
		for($i=0;$i<count($dataresult);$i++) {	
			$tgl		= 	$dataresult[$i]['Tanggal'];
			$hari		= 	$dataresult[$i]['Hari'];
			$timein		= 	$dataresult[$i]['TimeIn'];
			$timeout	= 	$dataresult[$i]['TimeOut'];
			$shift		= 	$dataresult[$i]['Shift'];
			
//			xlsWriteNumber($i,0, $tgl);
			xlsWriteLabel($baris, 0, $tgl);
			xlsWriteLabel($baris, 1, $hari);
			xlsWriteLabel($baris, 2, $timein);
			xlsWriteLabel($baris, 3, $timeout);	
			xlsWriteLabel($baris, 4, $shift);
			
			$baris++;
		}		
		xlsEOF();
	}
	
		
	function testing() {
		$tgldari	= '2016-10-01';
		$tglsampai	= '2016-10-01';
		$nama		='i made darma saputra';
		$asdf	= $this->reportshiftmodel->viewData($tgldari,$tglsampai, $nama);
		echo "<pre>";
		print_r($asdf);
		echo "</pre>";
	}
}
