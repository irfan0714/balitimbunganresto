<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Absensi extends authcontroller {

    function __construct() {
        parent::__construct();
		$this->load->model('transaksi/absensimodel'); 
		$this->load->library('globallib');
    }

    function index() {
		
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
//		if($sign =='Y') {			
			$today				= date('Y-m-d');
			$data['tgldari']	= $today;
			$data['tglsampai']	= $today;
			$dataresult			= $this->absensimodel->viewData($today,$today);
			$data['viewdata']	= $dataresult; 
			$this->load->view('transaksi/absensi/viewlist', $data);
//		} else {
//			$this->load->view('denied');
//		}
    }
	
	function view() {
		$submit		= $this->input->post('submit');
		$tgldari	= $this->input->post('tgldari');
		$tglsampai	= $this->input->post('tglsampai');
		$today		= date('Y-m-d');
		if( ($tgldari=='' OR is_null($tgldari)) OR ($tglsampai=='' OR is_null($tglsampai)) ) {
			$tgldari	= $today;
			$tglsampai	= $today;
		}
		$data['tgldari']	= $tgldari;
		$data['tglsampai']	= $tglsampai;
		$dataresult			= $this->absensimodel->viewData($tgldari,$tglsampai);
		$data['viewdata']	= $dataresult;    
		if($submit=='TAMPIL')
			$this->load->view('transaksi/absensi/viewlist', $data);
		else if($submit=='XLS')
			$this->_printxls($dataresult);
		else
			$this->tambah();
	
	}
	
	function _printxls($dataresult) {
		$this->load->helper('xl');
		sendXLHeader('absensi');
		xlsBOF();
		
		xlsWriteLabel(0, 0, 'TANGGAL');
		xlsWriteLabel(0, 1, 'NIK');
		xlsWriteLabel(0, 2, 'NAMA');
		xlsWriteLabel(0, 3, 'MASUK');
		xlsWriteLabel(0, 4, 'KELUAR');	
		xlsWriteLabel(0, 5, 'NOABSEN');
		
		$baris	= 1;
		for($i=0;$i<count($dataresult);$i++) {	
			$tgl		= 	$dataresult[$i]['Tanggal'];
			$nik 		= 	$dataresult[$i]['employee_nik'];
			$nama		= 	$dataresult[$i]['NamaKaryawan'];
			$timein		= 	$dataresult[$i]['TimeIn'];
			$timeout	= 	$dataresult[$i]['TimeOut'];
			$noabsen	= 	$dataresult[$i]['NoAbsen'];
//			xlsWriteNumber($i,0, $tgl);
			xlsWriteLabel($baris, 0, $tgl);
			xlsWriteLabel($baris, 1, $nik);
			xlsWriteLabel($baris, 2, $nama);
			xlsWriteLabel($baris, 3, $timein);
			xlsWriteLabel($baris, 4, $timeout);	
			xlsWriteLabel($baris, 5, $noabsen);
			
			$baris++;
		}		
		xlsEOF();
	}
	
	function tambah($errormsg='') {
		$this->load->library('globallib');
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
	//	if($sign =='Y') {
		$data['errormsg']	= $errormsg;
		$this->load->view('transaksi/absensi/tambahform', $data);
	//	} else {
	//		$this->load->view('denied');
	//	}
	}	
	
	function uploadproses() {
		$config['upload_path']		= './uploads/';
		$config['allowed_types']	= 'txt|csv';
		$config['max_size']			= '8192';
		$config['overwrite']		= true;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload()) {
			$data['errormsg']	= $this->upload->display_errors();
			$this->load->view('transaksi/absensi/tambahform', $data);
		} else {
			$dataupload		= $this->upload->data();
			$filelocation	= $dataupload['full_path'];
			$string			= read_file($filelocation);
			$string_arr		= explode("\n",$string);
			
			$this->db->trans_start();//-----------------------------------------------------START TRANSAKSI
			foreach ($string_arr as $sebaris) {				
			
				if(strlen($sebaris)>10) {
					list($noabsen, $datetime, $c, $d, $namakaryawan, $f, $g, $h)	= explode("\t",$sebaris);
					$datadetail	= array(
									'NoAbsen'		=> $noabsen,
									'PresensiTime'	=> $datetime,
									'NamaKaryawan'	=> $namakaryawan
								);
					$this->db->insert('absensitrn', $datadetail);										
				}				
			}	
			$this->db->trans_complete();//----------------------------------------------------END TRANSAKSI
			
			redirect('hr/absensi/','refresh');
		}	
	}
		
	function testing() {
		$tgldari	= '2016-10-01';
		$tglsampai	= '2016-10-03';
		$asdf	= $this->absensimodel->viewData($tgldari,$tglsampai);
		echo "<pre>";
		print_r($asdf);
		echo "</pre>";
	}
}
