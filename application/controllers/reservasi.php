<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Reservasi extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        //$this->load->model('transaksi/reservasimodel');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/reservasi_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $user = $this->session->userdata('username');

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "reservasi", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                }
            }

            // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;

            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/transaksi/reservasi/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/transaksi/reservasi/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }

            $data['mgudang'] = $this->reservasi_model->getGudang();
            $data['mdivisi'] = $this->reservasi_model->getDivisi();

            $data['bulan'] = $this->session->userdata('bulanaktif');
            $data['tahun'] = $this->session->userdata('tahunaktif');

            $thnbln = $data['tahun'] . $data['bulan'];

            $config['total_rows'] = $this->reservasi_model->num_reservasi_row($arr_search["search"]);
            $data['data'] = $this->reservasi_model->getReservasiList($config['per_page'], $page, $arr_search["search"]);

            $data['track'] = $mylib->print_track();

            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $this->load->view('transaksi/reservasi/listreservasi', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function search() {
        $mylib = new globallib();

        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'reservasi', 'AddUser' => $user));

        $search_value = "";
        $search_value .= "search_keyword=" . $mylib->save_char($this->input->post('search_keyword'));

        $data = array(
            'query_string' => $search_value,
            'module' => "reservasi",
            'AddUser' => $user
        );

        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/transaksi/reservasi/index/' . $query_id . '');
    }

    function edit_reservasi() {
        $thn = $this->uri->segment(7);
        $bln = $this->uri->segment(8);
        $no = $this->uri->segment(9);
        $nodok = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
        $getDataReserv = $this->globalmodel->getQuery(" 
				*,b.Nama,b.Company,b.Nationality,b.Alamat,b.Telepon 
			from 
				trans_reservasi a
			inner join 
				tourtravel b 
			on
				a.KdTravel=b.KdTravel
			where 
				1
				and NoDokumen = '" . $nodok . "'");
        //echo $this->db->last_query();die;
        $data['NoDokumen'] = $getDataReserv[0]['NoDokumen'];
        $data['v_KdTravel'] = $getDataReserv[0]['KdTravel'];
        $data['v_nama_travel'] = $getDataReserv[0]['Nama'];
        $data['v_Company'] = $getDataReserv[0]['Company'];
        $data['v_Alamat'] = $getDataReserv[0]['Alamat'];
        $data['v_Nationality'] = $getDataReserv[0]['Nationality'];
        $data['v_Telepon'] = $getDataReserv[0]['Telepon'];
        $data['v_Participants'] = $getDataReserv[0]['Participants'];
        $data['v_Event'] = $getDataReserv[0]['Event'];
        $data['v_Contact'] = $getDataReserv[0]['Contact'];
        $data['v_Phone'] = $getDataReserv[0]['Phone'];
        $data['v_Sales_In_Charge'] = $getDataReserv[0]['Sales_In_Charge'];
        $data['v_place_herborist'] = $getDataReserv[0]['place_herborist'];
        $data['v_place_the_luwus'] = $getDataReserv[0]['place_the_luwus'];
        $data['v_place_black_eye_coffee'] = $getDataReserv[0]['place_black_eye_coffee'];
        $data['v_place_the_rice_view'] = $getDataReserv[0]['place_the_rice_view'];
        $data['v_place_chappel'] = $getDataReserv[0]['place_chappel'];
        $data['v_func_type_wedding_party'] = $getDataReserv[0]['func_type_wedding_party'];
        $data['v_func_type_coffee_tea_break'] = $getDataReserv[0]['func_type_coffee_tea_break'];
        $data['v_func_type_cocktail_party'] = $getDataReserv[0]['func_type_cocktail_party'];
        $data['v_func_type_breakfast'] = $getDataReserv[0]['func_type_breakfast'];
        $data['v_func_type_birthday_party'] = $getDataReserv[0]['func_type_birthday_party'];
        $data['v_func_type_training'] = $getDataReserv[0]['func_type_training'];
        $data['v_func_type_lunch'] = $getDataReserv[0]['func_type_lunch'];
        $data['v_func_type_meeting'] = $getDataReserv[0]['func_type_meeting'];
        $data['v_func_type_dinner'] = $getDataReserv[0]['func_type_dinner'];
        $data['v_func_type_other'] = $getDataReserv[0]['func_type_other'];
        $data['v_func_type_other_content'] = $getDataReserv[0]['func_type_other_content'];
        $data['v_banquet_venue'] = $getDataReserv[0]['banquet_venue'];
        $data['v_banquet_event'] = $getDataReserv[0]['banquet_event'];
        $data['v_banquet_table_set_up'] = $getDataReserv[0]['banquet_table_set_up'];
        $data['v_banquet_persons'] = $getDataReserv[0]['banquet_persons'];
        $data['v_banquet_registration'] = $getDataReserv[0]['banquet_registration'];
        $data['v_banquet_coffee_break_1'] = $getDataReserv[0]['banquet_coffee_break_1'];
        $data['v_banquet_lunch'] = $getDataReserv[0]['banquet_lunch'];
        $data['v_banquet_coffee_break_2'] = $getDataReserv[0]['banquet_coffee_break_2'];
        $data['v_banquet_dinner'] = $getDataReserv[0]['banquet_dinner'];
        $data['v_equipment_flipchart'] = $getDataReserv[0]['equipment_flipchart'];
        $data['v_equipment_mineral_water'] = $getDataReserv[0]['equipment_mineral_water'];
        $data['v_equipment_lcd_projector'] = $getDataReserv[0]['equipment_lcd_projector'];
        $data['v_equipment_notepad_pencil'] = $getDataReserv[0]['equipment_notepad_pencil'];
        $data['v_equipment_mint'] = $getDataReserv[0]['equipment_mint'];
        $data['v_equipment_standard_set_up'] = $getDataReserv[0]['equipment_standard_set_up'];
        $data['v_equipment_screen'] = $getDataReserv[0]['equipment_screen'];
        $data['v_equipment_sound_system'] = $getDataReserv[0]['equipment_sound_system'];
        $data['v_billing_instruction'] = $getDataReserv[0]['billing_instruction'];
        $data['v_desc_front_office'] = $getDataReserv[0]['desc_front_office'];
        $data['v_desc_f_b_kitchen'] = $getDataReserv[0]['desc_f_b_kitchen'];
        $data['v_desc_engineering_edp'] = $getDataReserv[0]['desc_engineering_edp'];
        $data['v_desc_sport_recreation'] = $getDataReserv[0]['desc_sport_recreation'];
        $data['v_desc_rates'] = $getDataReserv[0]['desc_rates'];
        $data['v_desc_housekeeping'] = $getDataReserv[0]['desc_housekeeping'];
        $data['v_desc_accounting'] = $getDataReserv[0]['desc_accounting'];
        $data['v_desc_graphis'] = $getDataReserv[0]['desc_graphis'];
        $data['v_dp'] = $getDataReserv[0]['dp'];
        $data['v_Total'] = $getDataReserv[0]['Total'];
        $data['v_Remarks_Payment'] = $getDataReserv[0]['Remarks_Payment'];

        $data['type'] = "edit";
        $data['action'] = "save_data_edit";
        $this->load->view('transaksi/reservasi/tampil', $data);
        //print_r($getgetDataReserv);
    }

    function add_reservasi() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $no = "SGV/BEO/SM/";
            $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
            $tglApp = $getTgl[0]['TglTrans'];
            $thn = substr($tglApp, 0, 4);
            $bln = $this->dateRomawi(substr($tglApp, 5, 2));

            $getNoDokumen = $this->globalmodel->getQuery(" NoDokumen from trans_reservasi order by NoDokumen desc limit 1");
            if (!empty($getNoDokumen)) {
                $nodokumen = $getNoDokumen[0]['NoDokumen'];
                $noLast = substr($nodokumen, 19, 5) + 1;
                if (strlen($noLast) == "1") {
                    $noDokNew = "0000" . $noLast;
                } elseif (strlen($noLast) == "2") {
                    $noDokNew = "0000" . $noLast;
                } elseif (strlen($noLast) == "3") {
                    $noDokNew = "000" . $noLast;
                } elseif (strlen($noLast) == "4") {
                    $noDokNew = "00" . $noLast;
                } elseif (strlen($noLast) == "5") {
                    $noDokNew = "0" . $noLast;
                } elseif (strlen($noLast) == "6") {
                    $noDokNew = $noLast;
                } else {
                    $noDokNew = "00001";
                }
            } else {
                $noDokNew = "00001";
            }
            $data['type'] = "add";
            $data['action'] = "save_data";
            $data['NoDokumen'] = $no . $thn . "/" . $bln . "/" . $noDokNew;
            $this->load->view('transaksi/reservasi/tampil', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save_detail_menu() {
        $nodokumen = $this->input->post('nodokumen');
        $v_pcode1 = $this->input->post('pcode');
        $v_namabarang1 = $this->input->post('v_namabarang');
        $v_qty1 = $this->input->post('v_qty');
        $v_harga1 = $this->input->post('v_harga');

        for ($x = 0; $x < count($v_pcode1); $x++) {
            $data = array(
                "NoDokumen" => $nodokumen,
                "PCode" => $v_pcode1[$x],
                "NamaBarang" => $v_namabarang1[$x],
                "Qty" => $v_qty1[$x],
                "Harga" => $v_harga1[$x]
            );

            if ($v_pcode1[$x] != "") {
                $this->globalmodel->queryInsert("trans_reservasi_detail", $data);
            }
        }

        echo "<script>self.close();</script>";
    }

    function save_data() {
        $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
        $tglApp = $getTgl[0]['TglTrans'];
        $User = $this->session->userdata('username');

        $NoDokumen = $this->input->post('NoDokumen');
        $KdTravel = $this->input->post('KdTravel');
        $Participants = $this->input->post('Participants');
        $Event = $this->input->post('Event');
        $Contact = $this->input->post('Contact');
        $Phone = $this->input->post('Phone');
        $Sales_In_Charge = $this->input->post('Sales_In_Charge');
        $place_herborist = $this->input->post('place_herborist');
        $place_the_luwus = $this->input->post('place_the_luwus');
        $place_black_eye_coffee = $this->input->post('place_black_eye_coffee');
        $place_the_rice_view = $this->input->post('place_the_rice_view');
        $place_chappel = $this->input->post('place_chappel');
        $func_type_wedding_party = $this->input->post('func_type_wedding_party');
        $func_type_coffee_tea_break = $this->input->post('func_type_coffee_tea_break');
        $func_type_cocktail_party = $this->input->post('func_type_cocktail_party');
        $func_type_breakfast = $this->input->post('func_type_breakfast');
        $func_type_birthday_party = $this->input->post('func_type_birthday_party');
        $func_type_training = $this->input->post('func_type_training');
        $func_type_lunch = $this->input->post('func_type_lunch');
        $func_type_meeting = $this->input->post('func_type_meeting');
        $func_type_dinner = $this->input->post('func_type_dinner');
        $func_type_other = $this->input->post('func_type_other');
        $func_type_other_content = $this->input->post('func_type_other_content');
        $banquet_venue = $this->input->post('banquet_venue');
        $banquet_event = $this->input->post('banquet_event');
        $banquet_table_set_up = $this->input->post('banquet_table_set_up');
        $banquet_persons = $this->input->post('banquet_persons');
        $banquet_registration = $this->input->post('banquet_registration');
        $banquet_coffee_break_1 = $this->input->post('banquet_coffee_break_1');
        $banquet_lunch = $this->input->post('banquet_lunch');
        $banquet_coffee_break_2 = $this->input->post('banquet_coffee_break_2');
        $banquet_dinner = $this->input->post('banquet_dinner');
        $equipment_flipchart = $this->input->post('equipment_flipchart');
        $equipment_mineral_water = $this->input->post('equipment_mineral_water');
        $equipment_lcd_projector = $this->input->post('equipment_lcd_projector');
        $equipment_notepad_pencil = $this->input->post('equipment_notepad_pencil');
        $equipment_mint = $this->input->post('equipment_mint');
        $equipment_standard_set_up = $this->input->post('equipment_standard_set_up');
        $equipment_screen = $this->input->post('equipment_screen');
        $equipment_sound_system = $this->input->post('equipment_sound_system');
        $billing_instruction = $this->input->post('billing_instruction');
        $desc_front_office = $this->input->post('desc_front_office');
        $desc_f_b_kitchen = $this->input->post('desc_f_b_kitchen');
        $desc_engineering_edp = $this->input->post('desc_engineering_edp');
        $desc_sport_recreation = $this->input->post('desc_sport_recreation');
        $desc_rates = $this->input->post('desc_rates');
        $desc_housekeeping = $this->input->post('desc_housekeeping');
        $desc_accounting = $this->input->post('desc_accounting');
        $desc_graphis = $this->input->post('desc_graphis');
        $dp = $this->input->post('dp');
        $Total = $this->input->post('Total');
        $Remarks_Payment = $this->input->post('Remarks_Payment');
        $AddDate = date($tglApp . " H:i:s");
        $AddUser = $User;

        $data = array(
            "NoDokumen" => $NoDokumen,
            "KdTravel" => $KdTravel,
            "Participants" => $Participants,
            "Event" => $Event,
            "Contact" => $Contact,
            "Phone" => $Phone,
            "Sales_In_Charge" => $Sales_In_Charge,
            "place_herborist" => $place_herborist,
            "place_the_luwus" => $place_the_luwus,
            "place_black_eye_coffee" => $place_black_eye_coffee,
            "place_the_rice_view" => $place_the_rice_view,
            "place_chappel" => $place_chappel,
            "func_type_wedding_party" => $func_type_wedding_party,
            "func_type_coffee_tea_break" => $func_type_coffee_tea_break,
            "func_type_cocktail_party" => $func_type_cocktail_party,
            "func_type_breakfast" => $func_type_breakfast,
            "func_type_birthday_party" => $func_type_birthday_party,
            "func_type_training" => $func_type_training,
            "func_type_lunch" => $func_type_lunch,
            "func_type_meeting" => $func_type_meeting,
            "func_type_dinner" => $func_type_dinner,
            "func_type_other" => $func_type_other,
            "func_type_other_content" => $func_type_other_content,
            "banquet_venue" => $banquet_venue,
            "banquet_event" => $banquet_event,
            "banquet_table_set_up" => $banquet_table_set_up,
            "banquet_persons" => $banquet_persons,
            "banquet_registration" => $banquet_registration,
            "banquet_coffee_break_1" => $banquet_coffee_break_1,
            "banquet_lunch" => $banquet_lunch,
            "banquet_coffee_break_2" => $banquet_coffee_break_2,
            "banquet_dinner" => $banquet_dinner,
            "equipment_flipchart" => $equipment_flipchart,
            "equipment_mineral_water" => $equipment_mineral_water,
            "equipment_lcd_projector" => $equipment_lcd_projector,
            "equipment_notepad_pencil" => $equipment_notepad_pencil,
            "equipment_mint" => $equipment_mint,
            "equipment_standard_set_up" => $equipment_standard_set_up,
            "equipment_screen" => $equipment_screen,
            "equipment_sound_system" => $equipment_sound_system,
            "billing_instruction" => $billing_instruction,
            "desc_front_office" => $desc_front_office,
            "desc_f_b_kitchen" => $desc_f_b_kitchen,
            "desc_engineering_edp" => $desc_engineering_edp,
            "desc_sport_recreation" => $desc_sport_recreation,
            "desc_rates" => $desc_rates,
            "desc_housekeeping" => $desc_housekeeping,
            "desc_accounting" => $desc_accounting,
            "desc_graphis" => $desc_graphis,
            "dp" => $dp,
            "Total" => $Total,
            "Remarks_Payment" => $Remarks_Payment,
            "AddDate" => $AddDate,
            "AddUser" => $AddUser
        );
        $this->globalmodel->queryInsert("trans_reservasi", $data);

        $group_cc = $this->input->post('group_cc');
        for ($i = 0; $i < count($group_cc); $i++) {
            $data_group_cc = array(
                "NoDokumen" => $NoDokumen,
                "group_cc_id" => $group_cc[$i]
            );
            $this->globalmodel->queryInsert("trans_reservasi_cc", $data_group_cc);
        }

        redirect('/transaksi/reservasi/');
    }

    function save_data_edit() {
        $getTgl = $this->globalmodel->getQuery(" TglTrans from aplikasi");
        $tglApp = $getTgl[0]['TglTrans'];
        $User = $this->session->userdata('username');

        $NoDokumen = $this->input->post('NoDokumen');
        $KdTravel = $this->input->post('KdTravel');
        $Participants = $this->input->post('Participants');
        $Event = $this->input->post('Event');
        $Contact = $this->input->post('Contact');
        $Phone = $this->input->post('Phone');
        $Sales_In_Charge = $this->input->post('Sales_In_Charge');
        $place_herborist = $this->input->post('place_herborist');
        $place_the_luwus = $this->input->post('place_the_luwus');
        $place_black_eye_coffee = $this->input->post('place_black_eye_coffee');
        $place_the_rice_view = $this->input->post('place_the_rice_view');
        $place_chappel = $this->input->post('place_chappel');
        $func_type_wedding_party = $this->input->post('func_type_wedding_party');
        $func_type_coffee_tea_break = $this->input->post('func_type_coffee_tea_break');
        $func_type_cocktail_party = $this->input->post('func_type_cocktail_party');
        $func_type_breakfast = $this->input->post('func_type_breakfast');
        $func_type_birthday_party = $this->input->post('func_type_birthday_party');
        $func_type_training = $this->input->post('func_type_training');
        $func_type_lunch = $this->input->post('func_type_lunch');
        $func_type_meeting = $this->input->post('func_type_meeting');
        $func_type_dinner = $this->input->post('func_type_dinner');
        $func_type_other = $this->input->post('func_type_other');
        $func_type_other_content = $this->input->post('func_type_other_content');
        $banquet_venue = $this->input->post('banquet_venue');
        $banquet_event = $this->input->post('banquet_event');
        $banquet_table_set_up = $this->input->post('banquet_table_set_up');
        $banquet_persons = $this->input->post('banquet_persons');
        $banquet_registration = $this->input->post('banquet_registration');
        $banquet_coffee_break_1 = $this->input->post('banquet_coffee_break_1');
        $banquet_lunch = $this->input->post('banquet_lunch');
        $banquet_coffee_break_2 = $this->input->post('banquet_coffee_break_2');
        $banquet_dinner = $this->input->post('banquet_dinner');
        $equipment_flipchart = $this->input->post('equipment_flipchart');
        $equipment_mineral_water = $this->input->post('equipment_mineral_water');
        $equipment_lcd_projector = $this->input->post('equipment_lcd_projector');
        $equipment_notepad_pencil = $this->input->post('equipment_notepad_pencil');
        $equipment_mint = $this->input->post('equipment_mint');
        $equipment_standard_set_up = $this->input->post('equipment_standard_set_up');
        $equipment_screen = $this->input->post('equipment_screen');
        $equipment_sound_system = $this->input->post('equipment_sound_system');
        $billing_instruction = $this->input->post('billing_instruction');
        $desc_front_office = $this->input->post('desc_front_office');
        $desc_f_b_kitchen = $this->input->post('desc_f_b_kitchen');
        $desc_engineering_edp = $this->input->post('desc_engineering_edp');
        $desc_sport_recreation = $this->input->post('desc_sport_recreation');
        $desc_rates = $this->input->post('desc_rates');
        $desc_housekeeping = $this->input->post('desc_housekeeping');
        $desc_accounting = $this->input->post('desc_accounting');
        $desc_graphis = $this->input->post('desc_graphis');
        $dp = $this->input->post('dp');
        $Total = $this->input->post('Total');
        $Remarks_Payment = $this->input->post('Remarks_Payment');
        $AddDate = date($tglApp . " H:i:s");
        $AddUser = $User;

        $data = array(
            "NoDokumen" => $NoDokumen,
            "KdTravel" => $KdTravel,
            "Participants" => $Participants,
            "Event" => $Event,
            "Contact" => $Contact,
            "Phone" => $Phone,
            "Sales_In_Charge" => $Sales_In_Charge,
            "place_herborist" => $place_herborist,
            "place_the_luwus" => $place_the_luwus,
            "place_black_eye_coffee" => $place_black_eye_coffee,
            "place_the_rice_view" => $place_the_rice_view,
            "place_chappel" => $place_chappel,
            "func_type_wedding_party" => $func_type_wedding_party,
            "func_type_coffee_tea_break" => $func_type_coffee_tea_break,
            "func_type_cocktail_party" => $func_type_cocktail_party,
            "func_type_breakfast" => $func_type_breakfast,
            "func_type_birthday_party" => $func_type_birthday_party,
            "func_type_training" => $func_type_training,
            "func_type_lunch" => $func_type_lunch,
            "func_type_meeting" => $func_type_meeting,
            "func_type_dinner" => $func_type_dinner,
            "func_type_other" => $func_type_other,
            "func_type_other_content" => $func_type_other_content,
            "banquet_venue" => $banquet_venue,
            "banquet_event" => $banquet_event,
            "banquet_table_set_up" => $banquet_table_set_up,
            "banquet_persons" => $banquet_persons,
            "banquet_registration" => $banquet_registration,
            "banquet_coffee_break_1" => $banquet_coffee_break_1,
            "banquet_lunch" => $banquet_lunch,
            "banquet_coffee_break_2" => $banquet_coffee_break_2,
            "banquet_dinner" => $banquet_dinner,
            "equipment_flipchart" => $equipment_flipchart,
            "equipment_mineral_water" => $equipment_mineral_water,
            "equipment_lcd_projector" => $equipment_lcd_projector,
            "equipment_notepad_pencil" => $equipment_notepad_pencil,
            "equipment_mint" => $equipment_mint,
            "equipment_standard_set_up" => $equipment_standard_set_up,
            "equipment_screen" => $equipment_screen,
            "equipment_sound_system" => $equipment_sound_system,
            "billing_instruction" => $billing_instruction,
            "desc_front_office" => $desc_front_office,
            "desc_f_b_kitchen" => $desc_f_b_kitchen,
            "desc_engineering_edp" => $desc_engineering_edp,
            "desc_sport_recreation" => $desc_sport_recreation,
            "desc_rates" => $desc_rates,
            "desc_housekeeping" => $desc_housekeeping,
            "desc_accounting" => $desc_accounting,
            "desc_graphis" => $desc_graphis,
            "dp" => $dp,
            "Total" => $Total,
            "Remarks_Payment" => $Remarks_Payment,
            "EditDate" => $AddDate,
            "EditUser" => $AddUser
        );

        $where = array("NoDokumen" => $NoDokumen);
        $this->globalmodel->queryUpdate("trans_reservasi", $data, $where);
        $this->globalmodel->queryDelete("trans_reservasi_cc", $where);

        $group_cc = $this->input->post('group_cc');

        for ($i = 0; $i < count($group_cc); $i++) {

            if ($group_cc[$i] != "") {
                $data_group_cc = array(
                    "NoDokumen" => $NoDokumen,
                    "group_cc_id" => $group_cc[$i]
                );
                $this->globalmodel->queryInsert("trans_reservasi_cc", $data_group_cc);
            }
        }

        redirect('/transaksi/reservasi/');
    }

    function dateRomawi($bulan) {
        switch ($bulan) {
            case 1:
                $blnRom = 'I';
                break;
            case 2:
                $blnRom = 'II';
                break;
            case 3:
                $blnRom = 'III';
                break;
            case 4:
                $blnRom = 'IV';
                break;
            case 5:
                $blnRom = 'V';
                break;
            case 6:
                $blnRom = 'VI';
                break;
            case 7:
                $blnRom = 'VII';
                break;
            case 8:
                $blnRom = 'VIII';
                break;
            case 9:
                $blnRom = 'IX';
                break;
        }
        return $blnRom;
    }

    function create_pdf() {
        //  $id = $this->uri->segment(4);
        $thn = $this->uri->segment(7);
        $bln = $this->uri->segment(8);
        $no = $this->uri->segment(9);
        $nodok = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
        $data['results'] = $this->reservasi_model->getHeader($nodok);

        $html = $this->load->view('transaksi/reservasi/pdf_reservasi', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_reservasi.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }

    function cetak() {
        $data = $this->varCetak();
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_reservasi', $data);
    }

    function versistruk() {
        $data = $this->varCetak();
        // $no = $this->uri->segment(4);
        $ip_address = $_SERVER['REMOTE_ADDR'];
        //$ip = "192.168.0.75";
        //  $printer = $this->reservasi_model->NamaPrinter($ip);
        //print_r($printer); die();
        //        print_r($_SERVER['REMOTE_ADDR']);
        //        die();
//        $data['ip'] = $printer[0]['ip'];
//        $data['nm_printer'] = $printer[0]['nm_printer'];
        // $data['store'] = $this->reservasi_model->aplikasi();
//        $data['header'] = $this->reservasi_model->getHeader($no);
//        $data['detail'] = $this->reservasi_model->getHeaderForPrint($no);

        $thn = $this->uri->segment(7);
        $bln = $this->uri->segment(8);
        $no = $this->uri->segment(9);
        $id = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
        $data['store'] = $this->pos_model->aplikasi();
        $data['header'] = $this->reservasi_model->getHeader($id);
        $data['detail'] = $this->reservasi_model->getHeaderForPrint($id);

        if (!empty($data['header'])) {
            //$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
            $this->load->view('transaksi/reservasi/cetak_strukreservasi', $data); // jika ada printernya
        }
    }

    function printThis() {
        $data = $this->varCetak();
        $id = $this->uri->segment(4);
        $data['fileName2'] = "reservasi.sss";
        $data['fontstyle'] = chr(27) . chr(80);
        $data['nfontstyle'] = "";
        $data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
//		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
//		$data['string2'] = "(                     )         (                      )";
        $data['string1'] = "     Dibuat Oleh                Diperiksa Oleh                   Menyetujui";
        $data['string2'] = "(                     )    (                      )       (                      )";
        $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer', $data);
    }

    function varCetak() {
        $this->load->library('printreportlib');
        $mylib = new printreportlib();
        $thn = $this->uri->segment(7);
        $bln = $this->uri->segment(8);
        $no = $this->uri->segment(9);
        $id = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
        $header = $this->reservasi_model->getHeaderForPrint($id);

        $data['header'] = $header;
        $detail = $this->reservasi_model->getDetailForPrint($id);
//        print_r($detail);
        $data['judul1'] = array("No Dokumen", "Tanggal");
        $data['niljudul1'] = array($header->NoDokumen, $header->AddDate);
        $data['judul2'] = array("Group Name", "Event");
        $data['niljudul2'] = array($header->KdTravel, $header->Event);
        $data['judullap'] = "Reservasi";
        $data['url'] = "reservasi/printThis/" . $id;
        $data['url2'] = "reservasi/versistruk/" . $id;
        $data['colspan_line'] = 4;
        $data['lebar_detail'] = array(15, 30, 30, 15); // total 95  Rekening 	Nama 	Keterangan 	Jumlah
        $data['tipe_judul_detail'] = array("normal", "normal", "normal", "kanan");
        $data['judul_detail'] = array("PCode", "Nama Barang", "Qty", "Harga");
        $data['panjang_kertas'] = 30;
        $default_page_written = 19;
        $data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $default_page_written;
        if ($data['panjang_per_hal'] != 0) {
            $data['tot_hal'] = ceil((int) count($detail) / (int) $data['panjang_per_hal']);
        } else {
            $data['tot_hal'] = 1;
        }
        $list_detail = array();
        $detail_page = array();
        $counterRow = 0;
        $max_field_len = array(0, 0, 0, 0);
        for ($m = 0; $m < count($detail); $m++) {
            unset($list_detail);
            $counterRow++;
            $list_detail[] = stripslashes($detail[$m]['PCode']);
            $list_detail[] = stripslashes(substr($detail[$m]['NamaBarang'], 0, 20));
            $list_detail[] = stripslashes(substr($detail[$m]['Qty'], 0, 20)) . "          ";
            $list_detail[] = number_format($detail[$m]['Harga'], 2, ",", ".");
            $detail_page[] = $list_detail;
            $max_field_len = $mylib->get_max_field_len($max_field_len, $list_detail);
            if ($data['panjang_per_hal'] != 0) {
                if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
                    $data['detail'][] = $detail_page;
                    if ($m != count($detail) - 1) {
                        unset($detail_page);
                    }
                }
            }
        }
        $data['detail'][] = $detail_page;
        // $data['footer1'] = array("Jumlah Payment");
        // $data['footer2'] = array(number_format($header->JumlahPayment, 2, ",", "."));
        $data['brs_footer'] = array(20, 3, 15);
        $data['max_field_len'] = $max_field_len;
        $data['banyakBarang'] = $counterRow;
        $data['string1'] = "Dibuat Oleh";
        $data['string2'] = "Menyetujui";
        $data['string3'] = "(____________________)";
        $data['string4'] = "(____________________)";
        $data['judul_netto'] = array("Total");
        $data['isi_netto'] = "";
        return $data;
    }

    public function ajax_edit_bayar($id) {
        $nodok = str_replace("-", "/", $id);
        $data = $this->reservasi_model->get_by_id($nodok);
        echo json_encode($data);
    }

    public function ajax_update_bayar() {
        $this->_validate();
        $no = $this->input->post('NoDokumen');
        $data = array(
            'NoDokumen' => $no,
            'Jenis_Sisa_Bayar' => $this->input->post('rbtPembayaran'),
            'Kasir' => $this->input->post('Kasir'),
            'Sisa_Tunai' => $this->input->post('PaymentT'),
            'Sisa_Kredit' => $this->input->post('PaymentK'),
            'Sisa_Debit' => $this->input->post('PaymentD'),
            'status' => '3',
            'EditDate' => date('Y-m-d')
        );
        $this->db->update('trans_reservasi', $data, array('NoDokumen' => $no));
        echo json_encode(array("status" => TRUE,"NoDokumen" =>$no));
        $this->cetakStruk($no);
    }

    private function _validate() {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('rbtPembayaran') == '') {
            $data['inputerror'][] = 'Pembayaran';
            $data['error_string'][] = 'pembayran is required';
            $data['status'] = FALSE;
        }

//        if ($this->input->post('lastName') == '') {
//            $data['inputerror'][] = 'lastName';
//            $data['error_string'][] = 'Last name is required';
//            $data['status'] = FALSE;
//        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    function cetakStruk($no) {
//        $thn = $this->uri->segment(7);
//        $bln = $this->uri->segment(8);
//        $no = $this->uri->segment(9);
//        $id = "SGV/BEO/SM/" . $thn . "/" . $bln . "/" . $no;
        $data['store'] = $this->reservasi_model->aplikasi();
        $data['header'] = $this->reservasi_model->getHeader($no);
        $data['detail'] = $this->reservasi_model->getHeaderForPrint($no);

        if (!empty($data['header'])) {
            $this->load->view('transaksi/reservasi/cetak_strukreservasi', $data); // jika ada printernya
        }
    }
}
?>