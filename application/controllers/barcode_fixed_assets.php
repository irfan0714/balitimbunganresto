<?php
class Barcode_fixed_assets extends CI_Controller{
	
	function __construct(){
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('barcodefixedassestmodel','barcodemodel');
    }
	
	function index(){
		//$this->load->view('barcodeview');
		$data['action'] = "cetak_barcode";
		$data['Departement'] = $this->barcodemodel->getDepartement();
		$data['Lokasi'] = $this->barcodemodel->getLokasi();
		$data['Data_Buku'] = $this->barcodemodel->getDataBuku();
		$data['Sort_Data_Buku'] = $this->barcodemodel->getSortDataBuku();
		$data['Detail']= $this->barcodemodel->getDataAssest(0,0,0,0,100,'','');
		$this->load->view('create_barcode_fixed_assets',$data);
	}

	function gambar($kode){
		$height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '74';	
		$width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		$barcodeOPT = array(
			'text' => $kode, 
			'barHeight'=> $height, 
			'factor'=>$width,
		);
			
		$renderOPT = array();
		$render = Zend_Barcode::factory(
		'code39', 'image', $barcodeOPT, $renderOPT
		)->render();
	}
	
	function create(){
		$mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
			$data['SubDivisi'] = $this->barcodemodel->getSubdivisi();
			$data['Product'] = $this->barcodemodel->getBarang();
			$data['action'] = "cetak";
            $this->load->view('create_barcode_fixed_assets',$data);
        } else {
            $this->load->view('denied');
        }
	}
	
	function get_product($SubDivisi){
		$data['Product'] = $this->barcodemodel->getProduct($SubDivisi);
        $this->load->view('formproduct',$data);
	}
	
	function cetak(){
		$kdPro = $this->input->post('KdProduk');
		$qty = $this->input->post('jml');
		$date = date('Y-m-d H:i:s');
		$getBarcode = $this->barcodemodel->getBarcode($kdPro);
		if(!empty($getBarcode)){
			$barcode = $getBarcode[0]['Barcode1'];
			$nama = $getBarcode[0]['NamaInitial'];
			$harga = $getBarcode[0]['Harga1c'];
			//CETAK BARCODE
			if($barcode != 0){	
				$data['barcode'] = $barcode;
				$data['nama'] = $nama;
				$data['harga'] = $harga;
				$data['jml'] = $this->input->post('jml');
				$this->load->helper('print');
				$html = $this->load->view('create_barcode_fixed_assets',$data, TRUE);
				
				$filename	='barcode';
				$ext		='bcode';
				header('Content-Type: application/bcode');
				header('Content-Disposition: inline; filename="'. $filename . '.' . $ext . '"');
				header('Cache-Control: private, max-age=0, must-revalidate');
				header('Pragma: public');
				print $html;
			}
		}
		//INSERT LOG BARCODE
		$data = array(
			'PCode' => $kdPro,
			'Qty' => $qty,
			'Date' => $date
		);
		$this->db->insert('trans_barcode', $data);
		//redirect('barcode/create');
		//$this->load->view('cetak_barcode');
	}


	/*function cetak_barcode(){
		$data['detail']= $this->barcodemodel->getDataAssest();
		$html = $this->load->view('cetak_barcode_fixed_assets', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_komisi.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
		
	}*/

	function cetak_barcode(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$Departement = $this->input->post('Departement');
		$Lokasi = $this->input->post('Lokasi');
		$DataBuku = $this->input->post('DataBuku');
		$ToDataBuku = $this->input->post('SortToDataBuku');
		$FromDataBuku = $this->input->post('SortFromDataBuku');
		$Nama = $this->input->post('Nama');
		$data['detail']= $this->barcodemodel->getDataAssest($Departement,$Lokasi,$DataBuku,$Nama,0,$ToDataBuku,$FromDataBuku);
		$this->load->view('cetak_barcode_fixed_assets_html',$data);
		
	}
	
	function cetak_barcode_kiriman($NoAsset,$Lokasi,$Dept){
		$data['detail']= $this->barcodemodel->getDataAssestKiriman($NoAsset,$Lokasi,$Dept);
		$this->load->view('cetak_barcode_fixed_assets_kiriman_html',$data);
		
	}
}
?>