<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class ServerMasterbarang extends CI_Controller {
	
	function __construct(){
        parent::__construct();
		$this->dbsarinah = $this->load->database('sarinah', TRUE);

    }

  	function index(){
  		$this->updateMasterterbarangTouch();
  		$this->sinkronMasterbarangToClient();
  	}

  	function sinkronMasterbarangToClient() {
  		echo "Start ". date('Y-m-d H:i:s')."<br>";
		$this->db->select('*');
		$this->db->from('masterbarang');
		$this->db->where('FlagPenjualanPOSTouch', 'Y');
		$dataMasterbarang = $this->db->get()->result_array();

		foreach($dataMasterbarang as  $val) {
			$this->dbsarinah->delete('masterbarang', array('PCode' => $val['PCode']));
			$this->dbsarinah->insert('masterbarang',$val);
		}

		$dataMasterbarangsarinah = $this->dbsarinah->get('masterbarang')->result_array();
		$this->db->select('*');
		$this->db->from('masterbarang_touch');
		$dataMasterbarangTouch = $this->db->get()->result_array();

		echo "<pre>";
		print_r($dataMasterbarangTouch);
		echo "<pre>";
		foreach($dataMasterbarangTouch as  $val) {
			$this->dbsarinah->delete('masterbarang_touch', array('PCode' => $val['PCode']));
			$this->dbsarinah->insert('masterbarang_touch',$val);
		}

  	}
	
	function updateMasterterbarangTouch() {
		echo "Start ". date('Y-m-d H:i:s')."<br>";
		$this->db->select('*');
		$this->db->from('masterbarang');
		$this->db->where('FlagPenjualanPOSTouch', 'Y');
		$dataMasterbarang = $this->db->get()->result_array();

		foreach($dataMasterbarang AS $value) {
			$this->db->delete('masterbarang_touch', array('PCode' => $value['PCode']));
			$data['PCode'] = $value['PCode'];
			$data['NamaLengkap'] = $value['NamaLengkap'];
			$data['Harga1c'] = $value['Harga1c'];
			$data['Barcode1'] = $value['Barcode1'];
			$data['Service_charge'] = $value['Service_charge'];
			$data['komisi'] = $value['komisi'];
			$data['Jenis'] = '2';
			$data['Satuan1'] = $value['Satuan1'];
			$data['FlagReady'] = $value['FlagReady'];
			$data['FlagStock'] = $value['FlagStock'];
			$data['Printer'] = '01';
			$data['DiscInternal'] = $value['DiscInternal'];
			$data['DiscLokal'] = $value['DiscLokal'];
			$data['KomisiLokal'] = $value['KomisiLokal'];
			$data['KdKategori'] = $this->getKdKategoriPos($value['SubKategoriPOSTouch']);
			$data['KdSubKategori'] = $value['SubKategoriPOSTouch'];
			$this->db->insert('masterbarang_touch', $data);
		}
		echo "Stop ". date('Y-m-d H:i:s');

	}

	function getKdKategoriPos($KdSubKategoriPos) {
		$this->db->select('KdKategori');
		$this->db->from('subkategoripos');
		$data = $this->db->get()->result_array();
		return $data[0]['KdKategori'];
	}


}
?>