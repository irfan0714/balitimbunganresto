<?php
class npm_json extends CI_Controller{
	function __construct(){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		
        parent::__construct();
		$this->load->library('globallib');
        $this->load->model('json/npm_json_model');
    }
	
	function listdo($tahun,$bulan){
		$data = $this->npm_json_model->getlistdo($tahun,$bulan);
		echo json_encode($data);
	}
	
	function dodetail($dono){
		$data = $this->npm_json_model->getDetailDO($dono);
		echo json_encode($data);
	}
	
	function brand(){
		
		$brand = $this->npm_json_model->getBrand();
		echo json_encode($brand);
	}
	
	function subbrand(){
		
		$subbrand = $this->npm_json_model->getSubBrand();
		echo json_encode($subbrand);
	}
}
?>