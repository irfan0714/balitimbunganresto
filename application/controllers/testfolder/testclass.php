<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class TestClass extends authcontroller {

    function __construct() {
        parent::__construct();
		$this->load->model('test/testmodel');
		
        $this->load->library('globallib');
		$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
		if($sign !='Y') {
			$this->load->view('denied');
		}
    }

    function index() {
//		$mylib			= new globallib();
//		$data['track']	= $mylib->print_track();
		
		$this->load->library('pagination');

		$urisegment	= 6;
		$keyword	= $this->uri->segment($urisegment -2);
		$keywordurl = $keyword;
		$keywordurl2 = $this->uri->segment($urisegment -1);

		if(($keyword=='') || ($keyword=='nokeyword')){
				$keyword	= '';
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
		}
        if($this->input->post('submit')=='Cari'){
			$keyword    = $this->input->post('optionValue');
			$keywordurl2 = $this->input->post('option');
			if($keyword ==''){
				$keywordurl = 'nokeyword';
				$keywordurl2 = 'nokeyword';
			}
			else{
				$keywordurl = $keyword;
			}
		}
		$dataperpage				= 10;
		$config['per_page']         = $dataperpage;
		$config['base_url']         = site_url()."/testfolder/testclass/index/$keywordurl/$keywordurl2/";
		$config['uri_segment']      = $urisegment;
        $config['total_rows']       = $this->testmodel->getNumRowsDataView($keyword, $keywordurl2);
				
		$config['full_tag_open']	= '<ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul>';
		$config['cur_tag_open']		= '<li class="active"><a href="javascript:void(0);">';
		$config['cur_tag_close']	= '</a></li>';
		$config['first_link']		= 'First';
		$config['last_link']		= 'Last';
		$config['num_links']		= 5;

        $this->pagination->initialize($config);
		$fromurisegment				= $this->uri->segment($urisegment);
		$data['viewdata']			= $this->testmodel->viewData($dataperpage, $fromurisegment, $keyword, $keywordurl2);
        
//		$this->load->view('testview/viewtest', $data);
		$this->load->view('testview/viewtest2', $data);
    }
		
	function testlcd() {
		$ipaddress			= $_SERVER['REMOTE_ADDR']; 
			$data['iplokal']	= $ipaddress;
		$this->load->view('testview/testlcd',$data);
	}
	
}