<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Uang_muka_supplier extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/keuangan/uang_muka_supplier_model');

    }

    function index() 
    {
    	$id = $this->uri->segment(4);;
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        $user = $this->session->userdata('username');
        if ($sign == "Y") {
        	if($id*1>0)
			{	
				$resSearch = $this->globalmodel->getSearch($id,"uangmukasupplier",$user);	
				$arrSearch = explode("&",$resSearch->query_string);
				
				$id_search = $resSearch->id;
				
				if($id_search)
				{
					$search_keyword = explode("=", $arrSearch[0]); // search keyword
					$arr_search["search"]["keyword"] = $search_keyword[1];
					
					$data["search_keyword"] = $search_keyword[1];
				}
			}
			
			if($id_search)
			{
				$config['base_url'] = base_url() . 'index.php/keuangan/uang_muka_supplier/index/'.$id_search.'/';
	            $config['uri_segment'] = 5;
	            $page = $this->uri->segment(5);
			}
			else
			{
				$config['base_url'] = base_url() . 'index.php/keuangan/uang_muka_supplier/index/';
	            $config['uri_segment'] = 4;
	            $page = $this->uri->segment(4);
			}
			
        	 // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            //$arr_search["search"]=20;
            $page = 0;
            $config['total_rows'] = $this->uang_muka_supplier_model->num_uang_muka_row($arr_search["search"]);
            $data['data'] = $this->uang_muka_supplier_model->getUangMukaList($config['per_page'], $page, $arr_search["search"]);
            
            
            $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$this->load->view('transaksi/keuangan/uang_muka_supplier/list_uang_muka', $data);
           
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'uangmukasupplier', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "uangmukasupplier",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/keuangan/uang_muka_supplier/index/' . $query_id . '');
    }

    function add_new() 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $data['KasBank'] = $this->uang_muka_supplier_model->getKasBank($user);
            $data['supplier'] = $this->uang_muka_supplier_model->getSupplier();
            $data['subdivisi'] = $this->uang_muka_supplier_model->getSubDivisi();
            $data['rekeningpph'] = $this->uang_muka_supplier_model->getRekeningPPH();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/uang_muka_supplier/add_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
     function edit_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="edit";
            $data['header'] = $this->uang_muka_supplier_model->getListUangMuka($id);
            
            $data['KasBank'] = $this->uang_muka_supplier_model->getKasBank($user);
            $data['supplier'] = $this->uang_muka_supplier_model->getSupplier();
            $data['subdivisi'] = $this->uang_muka_supplier_model->getSubDivisi();
            $data['rekeningpph'] = $this->uang_muka_supplier_model->getRekeningPPH();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/uang_muka_supplier/edit_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    function view_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="view";
            $data['header'] = $this->uang_muka_supplier_model->getListUangMuka($id);
            
            $data['KasBank'] = $this->uang_muka_supplier_model->getKasBank($user);
            $data['supplier'] = $this->uang_muka_supplier_model->getSupplier();
            $data['subdivisi'] = $this->uang_muka_supplier_model->getSubDivisi();
            $data['rekeningpph'] = $this->uang_muka_supplier_model->getRekeningPPH();
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/uang_muka_supplier/edit_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }

	
	function save_data() 
    {
        // echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $v_no_dokumen = $this->input->post('v_no_dokumen');
        $v_no_pv = $this->input->post('v_no_pv');
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_kdkasbank= $this->input->post('v_kas_bank');
        $v_no_referensi = $this->input->post('v_no_ref');
        $v_supplier = $this->input->post('v_supplier');
        $v_dpp = $this->input->post('v_dpp');
        $v_nilaipph = $this->input->post('v_nilaipph');
        $v_nilaippn = $this->input->post('v_nilaippn');
        $v_jumlah = $this->input->post('v_jumlah');
        $v_sisa = $this->input->post('v_sisa');
        $v_jenis_pph = $this->input->post('v_jenis_pph');
        $v_ppn = $this->input->post('v_ppn');
        $v_pph = $this->input->post('v_pph');
        $v_total = $this->input->post('v_total');
		$v_no_rek = $this->input->post('v_no_rek');
		$v_subdivisi = $this->input->post('v_subdivisi');
        $v_note = $this->input->post('v_note');
		$v_rekeningpph = $this->input->post('v_rekeningpph');
		
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        //ini rekening sebelum di ganti menjadi PPH21/PPH23/PPH4(2)
        //$norekum = $v_jenis_pph;
        $interface = $this->uang_muka_supplier_model->getinterface();
        $norekum = $interface['UMPembelian'];
        
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
		
		$bln = substr($mylib->ubah_tanggal($v_tgl_dokumen), 5, 2);
        $thn = substr($mylib->ubah_tanggal($v_tgl_dokumen), 2, 2);
        $tahun = $thn + 2000;

        if ($flag == "add")
		{
			//generate number
        	$v_no_dokumen = $mylib->get_code_counter($this->db->database, "uang_muka_supplier","NoDokumen", "US", $bln, $tahun);
			
			//nomor Payment Voucher
			$kd_no = $this->uang_muka_supplier_model->getKodeBank($v_kdkasbank);
		    $NoPV = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$thn, $bln);
			
			$supplier_name = $this->uang_muka_supplier_model->getSupplierName($v_supplier);
			
			$this->db->trans_begin();
			//insert ke trans_payment_header
			$this->insertpaymentheader($NoPV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_note,$user,$v_jumlah,$supplier_name->Nama,$v_kdkasbank, $v_no_dokumen);
			//insert ke trans_payment_detail
			$this->insertpaymentdetail($NoPV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_dpp,"1",$v_note,$user,$norekum,$v_subdivisi,$v_no_dokumen,$v_nilaippn,$v_nilaipph,$v_rekeningpph);
			
			//insert uang_muka			
            $this->insertNewUangMuka($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_kdkasbank, $v_no_referensi, $v_supplier, $v_jumlah, $v_pph, $v_jenis_pph, $v_total, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user, $v_dpp, $v_nilaipph, $v_rekeningpph, $v_ppn,$v_nilaippn);
	        
	        if ($this->db->trans_status() === FALSE){
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
			}
			$this->session->set_flashdata('msg', array('message' => 'Proses Add <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
			
			$link = "<script>
					window.open('".base_url()."index.php/keuangan/payment/cetak/".$NoPV ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
					window.location.href = '".base_url()."index.php/keuangan/uang_muka_supplier/add_new/"."';
				</script>";
		} 
		
		
		else if ($flag == "edit") 
		{
			$supplier_name = $this->uang_muka_supplier_model->getSupplierName($v_supplier);
			//update ke trans_payment_header
			$this->updatepaymentheader($v_no_pv,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_note,$user,$v_jumlah,$supplier_name->Nama,$v_kdkasbank, $v_no_dokumen);
			//update ke trans_payment_detail
			$this->updatepaymentdetail($v_no_pv,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_dpp,"1",$v_note,$user,$norekum,$v_subdivisi, $v_no_dokumen,$v_nilaippn,$v_nilaipph,$v_rekeningpph);
			
			//update uang_muka
			$this->db->trans_begin();
						
            $this->updateNewUangMuka($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_kdkasbank, $v_no_referensi, $v_supplier, $v_jumlah, $v_pph, $v_jenis_pph, $v_total, $v_no_rek, $v_subdivisi, $v_note, $v_no_pv, $user, $v_dpp, $v_nilaipph, $v_rekeningpph);
        
			if ($this->db->trans_status() === FALSE){
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
			}
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
            
            $link = "<script>
					window.open('".base_url()."index.php/keuangan/payment/cetak/".$v_no_pv ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
					window.location.href = '".base_url()."index.php/keuangan/uang_muka_supplier/add_new/"."';
				</script>";

        }

        
        //redirect('/keuangan/uang_muka_supplier');
		echo $link;
    }
    
    
    function insertNewUangMuka($v_no_dokumen,$v_tgl_dokumen, $v_kdkasbank, $v_no_referensi, $v_supplier, $v_jumlah, $v_pph , $v_jenis_pph, $v_total, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user,$v_nilaidpp=0,$v_nilaipph=0,$v_rekeningpph,$v_ppn,$v_nilaippn)
	{
		
		
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'TglDokumen'=>$v_tgl_dokumen,
             'KdKasBank' => $v_kdkasbank,
             'NoReferensi' => $v_no_referensi,
             'KdSupplier' => $v_supplier,
             'NilaiDPP' => $v_nilaidpp,
             'NilaiPPN' => $v_nilaippn,
             'NilaiPPH' => $v_nilaipph,
             'Jumlah' => $v_jumlah,
             'NoRekening' => $v_no_rek,
             'KdSubDivisi' => $v_subdivisi,
             'NoPV' => $NoPV,
             'KdRekening' => $v_jenis_pph,
             'PPN'=> $v_ppn,
             'RekeningPPH'=> $v_rekeningpph,
             'PPH'=> $v_pph,
             'Total' => $v_total,
             'Sisa' => $v_jumlah,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
		);
		$this->db->insert('uang_muka_supplier', $data);
		
	}
	
	
	function updateNewUangMuka($v_no_dokumen,$v_tgl_dokumen, $v_kdkasbank, $v_no_referensi, $v_supplier, $v_jumlah, $v_pph, $v_jenis_pph, $v_total, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user,$v_nilaidpp=0,$v_nilaipph=0,$v_rekeningpph)
	{
		
		
		$data = array(
             'TglDokumen'=>$v_tgl_dokumen,
             'KdKasBank' => $v_kdkasbank,
             'NoReferensi' => $v_no_referensi,
             'KdSupplier' => $v_supplier,
             'NilaiDPP' => $v_nilaidpp,
             'NilaiPPH' => $v_nilaipph,
             'Jumlah' => $v_jumlah,
             'NoRekening' => $v_no_rek,
             'KdSubDivisi' => $v_subdivisi,
             'NoPV' => $NoPV,
             'PPH' => $v_pph,
             'Total' => $v_total,
             'KdRekening' => $v_jenis_pph,
             'RekeningPPH'=> $v_rekeningpph,
             'Sisa' => $v_jumlah,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
		);
		$where = array('NoDokumen'=>$v_no_dokumen);
		$this->db->update('uang_muka_supplier', $data, $where);
		
	}
	
    
    function updatepaymentheader($nopv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank, $uangmuka)
	{
		
		
		$data = array(
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket.'-'.$uangmuka,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahPayment' => $jumlah,
			'Status' => '1'
		);
		
		$where = array('NoDokumen'=> $nopv);
		
		$this->db->update('trans_payment_header', $data, $where);
		
	}
	
	
	function insertpaymentheader($nopv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank, $uangmuka)
	{
		
		
		$data = array(
			'NoDokumen'	    => $nopv,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket . ' - '.$uangmuka,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahPayment' => $jumlah,
			'Status' => '1'
		);
		$this->db->insert('trans_payment_header', $data);
		
	}
	
	
	function insertpaymentdetail($NoDokumen,$tgl,$Noref,$dpp,$counter,$keterangan,$user,$norek,$subdivisi,$uangmuka,$nilaippn,$nilaipph,$rekeningpph)
	{
		
		$data = array(
			'NoDokumen'	=> $NoDokumen,
			'TglDokumen' => $tgl,
			'KdRekening' => $norek,
			'Jumlah' => $dpp,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan. ' - '.$uangmuka,
			'Urutan'	=> $counter,
			'NoBukti' => $Noref,
			'Status' => '1',
			'AddDate' => date('Y-m-d'),
			'AddUser' => $user
		);
		$this->db->insert('trans_payment_detail', $data);

        #PPN
        if($nilaippn != 0){
            $norek_ppn = '11100050';
            $data = array(
                'NoDokumen' => $NoDokumen,
                'TglDokumen' => $tgl,
                'KdRekening' => $norek_ppn,
                'Jumlah' => $nilaippn,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => '00',
                'Keterangan' => 'PPN '.$keterangan. ' - '.$uangmuka,
                'Urutan'    => $counter,
                'NoBukti' => $Noref,
                'Status' => '1',
                'AddDate' => date('Y-m-d'),
                'AddUser' => $user
            );
            $this->db->insert('trans_payment_detail', $data);
        }


        #PPh
        if($nilaipph != 0){
            $nilaipphminus = intval($nilaipph) * -1; 
            $data = array(
                'NoDokumen' => $NoDokumen,
                'TglDokumen' => $tgl,
                'KdRekening' => $rekeningpph,
                'Jumlah' => $nilaipphminus,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => '00',
                'Keterangan' => 'PPH '.$keterangan. ' - '.$uangmuka,
                'Urutan'    => $counter,
                'NoBukti' => $Noref,
                'Status' => '1',
                'AddDate' => date('Y-m-d'),
                'AddUser' => $user
            );
            $this->db->insert('trans_payment_detail', $data);
        }
		
		
	}
	
	function updatepaymentdetail($NoDokumen,$tgl,$Noref,$jumlah,$counter,$keterangan,$user,$norek,$subdivisi,$uangmuka)
	{
		
		$data = array(
			'TglDokumen' => $tgl,
			'KdRekening' => $norek,
			'Jumlah' => $jumlah,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan. ' - '.$uangmuka,
			'Urutan'	=> $counter,
			'NoBukti' => $Noref,
			'Status' => '1',
			'AddDate' => date('Y-m-d'),
			'AddUser' => $user
		);
		$where = array ('NoDokumen'	=> $NoDokumen);
		$this->db->update('trans_payment_detail', $data, $where);
		
		
	}
	
    
    function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
		
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;	
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
    function cari_supplier(){
		$supplier = $this->input->post('supplier');
	    $query = $this->uang_muka_supplier_model->getSupplier($supplier);
	     	     
	    echo "<option value=''> -- Pilih Supplier --</option>";
	    foreach ($query as $rec) {
			echo "<option value=$rec[KdSupplier]>$rec[Nama]</option>";
		}   
	}
    
    
    function delete_trans($UM, $PV) 
    {
            $this->db->delete('trans_payment_header', array('NoDokumen' => $PV));	
			$this->db->delete('trans_payment_detail', array('NoDokumen' => $PV));
			$this->db->delete('uang_muka', array('NoDokumen' => $UM));
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/keuangan/uang_muka/');
    }
    
    
    
		
}

?>