<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Komisi_komunitas extends authcontroller {

	//put your code here
	function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		//semua meanggunakan model komisi_model
		$this->load->model('transaksi/keuangan/komisi_komunitas_model');
		
		//ini untuk ambil kas bank aja
		$this->load->model('transaksi/keuangan/paymentmodel');
	}

	function index()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("all");
		if($sign == "Y"){
			$segs = $this->uri->segment_array();
			$arr  = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
			$data['link'] = $mylib->restrictLink($arr);
			$id   = addslashes(trim($this->input->post('stSearchingKey')));
			$id2  = $this->input->post('date1');
			$with = $this->input->post('searchby');
			if($with == "TglTransaksi"){
				$id = $mylib->ubah_tanggal($id2);
			}
			$this->load->library('pagination');

			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open'] = '<span class="current">';
			$config['cur_tag_close'] = '</span>';
			$config['per_page'] = '10';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['num_links'] = 2;
			$config['base_url'] = base_url() . 'index.php/keuangan/komisi_komunitas/index/';
			$page = $this->uri->segment(4);
			$config['uri_segment'] = 4;
			$flag1 = "";
			if($with != ""){
				if($id != "" && $with != ""){
					$config['base_url'] = base_url() . 'index.php/keuangan/komisi_komunitas/index/' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
				else
				{
					$page = "";
				}
			}
			else
			{
				if($this->uri->segment(5) != ""){
					$with = $this->uri->segment(4);
					$id   = $this->uri->segment(5);
					if($with == "TglTransaksi"){
						$id = $mylib->ubah_tanggal($id);
					}
					$config['base_url'] = base_url() . 'index.php/keuangan/komisi_komunitas/index/' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
			}
			
			$user = $this->session->userdata('username');
			$data['kasbank']   = $this->paymentmodel->getKasBank($user);
			
			$data['header'] = array("No Transaksi","Tanggal","Nama","Keterangan","Total","Kd Agent");
			$config['total_rows'] = $this->komisi_komunitas_model->num_komisi_row(addslashes($id), $with);
			$data['data'] = $this->komisi_komunitas_model->getKomisiList($config['per_page'], $page, addslashes($id), $with);
			$data['track'] = $mylib->print_track();
			
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			
			$this->load->view('transaksi/keuangan/komisi_komunitas/komisi_list', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	function add_new()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("add");
		if($sign == "Y"){
			$aplikasi = $this->komisi_komunitas_model->getDate();
			$user = $this->session->userdata('username');
			$data['tour_travel'] = $this->komisi_komunitas_model->getTT();
			$data['aplikasi'] = $aplikasi;
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/keuangan/komisi_komunitas/add_komisi', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}
	
	function ajax_tt(){
		$tourtravel= $this->input->post('id');
		$query = $this->komisi_komunitas_model->getCariTT($tourtravel);
		 
		 echo "<option value=''> -- Pilih --</option>";
		 foreach ($query as $cetak) {
		 echo "<option value=$cetak[KdTravel]>$cetak[Nama]</option>";
	 
	    }
    }
	
	
	function getlistOmset()
	{
		$tt  = $this->input->post('kdtourtravel');
		$tg_awal  = $this->input->post('tglawal');
		$tg_akhir = $this->input->post('tglakhir');
		
		$detail = $this->komisi_komunitas_model->ambilReq($tt,$tg_awal,$tg_akhir);// ambil tanggal transaksi dan nomor stiker
				
		$data['data'] = $detail;
		
		$this->load->view('transaksi/keuangan/komisi_komunitas/add_komisi_detail', $data);	
	}
	
	function save_new_komisi()
	{
		$mylib       = new globallib();
		//$mylib->arrprint($_POST);
		//echo "<pre>";print_r($_POST);echo "</pre>";die();
		$user        = $this->session->userdata('username');
		
		$flag        = $this->input->post('flag');
		$no          = $this->input->post('nodok');
		$tgl_awal    = $this->input->post('tgl_awal');
		$tgl_akhir   = $this->input->post('tgl_akhir');
		$v_tourtravel     = $this->input->post('v_tourtravel');
		$ket         = trim(strtoupper(addslashes($this->input->post('ket'))));
		$totomset    = $this->input->post('totomset');
		$totkomisi   = $this->input->post('totkomisi');
		
		//detail
		$tgl1      = $this->input->post('tgl');
		//$nostiker1  = $this->input->post('nostiker');
		$kdtourleader1   = $this->input->post('kdtourleader');
		$omset1    = $this->input->post('omset_temp');
		
		$waktu       = date("h:i:s");
		$tanggal     = date('d-m-Y');
		$bulan = substr($tanggal, 3, 2);
		$tahun = substr($tanggal, - 4);
		
		
		//ini adalah untuk insert di finance_komisi_komunitas_header dan finance_komisi_komunitas_detail ------------------------------------------------------------
	
			$new_no= $this->komisi_komunitas_model->getNewNo($tahun,$bulan);
			$no    = $new_no->NoKomisi;
			$this->db->update('counter', array("NoKomisi"=> $new_no->NoKomisi + 1), array("Tahun"=> $tahun,"Bulan"=> $bulan));
						
			
			//insert new detail komisi
			for($x = 0; $x < count($tgl1); $x++){
				//$nostiker     	= strtoupper(addslashes(trim($nostiker1[$x])));
				$tgl     		= trim($tgl1[$x]);
				$kdtourleader   = trim($kdtourleader1[$x]);
				$omset     		= trim($omset1[$x]);
				
				//insert detail
				$this->insertkomisidetail($no, $tgl, $kdtourleader, $omset, $v_tourtravel);
				//update status di register
				$this->db->update("register",array('statuskomisi_komunitas'=>1),array('Tanggal'=>$mylib->ubah_tanggal($tgl),'KdTravel'=>$v_tourtravel));
						
			}
			
			//insert new header komisi
			$this->insertkomisiheader($user,$no,$v_tourtravel,$tgl_awal,$tgl_akhir, $totomset, $totkomisi, $ket);
			
			
		

		     redirect('/keuangan/komisi_komunitas/');
	}
	
	
	//ini yang insertNewDetail yang diatas
	function insertkomisidetail($no, $tgl, $kdtourleader, $omset, $v_tourtravel)
	{
		$mylib       = new globallib();
		$data = array(
			'NoTransaksi'		=> $no,
			'Tanggal'    		=> $mylib->ubah_tanggal($tgl),
			'KdTourleader'      => $kdtourleader,
			'Omset'      		=> $omset
		);
	
		$this->db->insert('finance_komisi_komunitas_detail', $data);
		//$this->db->update('register', array('statuskomisi_komunitas'=>'1'), array('KdTravel'=> $v_tourtravel));
		//return $no;
	}
	
	
	// fungsi insertNewHeader yang ada diatas
	function insertkomisiheader($user,$no,$v_tourtravel,$tgl_awal,$tgl_akhir, $totomset, $totkomisi, $ket)
	{
	
		$mylib = new globallib();
		
		$data = array(
			'NoTransaksi' => $no,
			'TglAwal'=> $mylib->ubah_tanggal($tgl_awal),
			'TglAkhir'=> $mylib->ubah_tanggal($tgl_akhir),
			'KdTravel'     => $v_tourtravel,
			'Keterangan'  => $ket,
			'TotalOmset'       => $totomset,
			'TotalKomisi'       => $totkomisi,
			'AddDate'     => date("Y-m-d"),
			'AddUser'     => $user
		);
		
		$this->db->insert('finance_komisi_komunitas_header', $data);
		//$this->db->update('register', array('statuskomisi'=>'1'),array('KdTourLeader'=> $kdagent));

		//return $no;
	
	}
	
	function kasir_payment($id) 
    {
		$data = $this->komisi_komunitas_model->getTrans($id);
		echo json_encode($data);			
    }
	
	function bayar_payment() 
    {
    	
    	$mylib = new globallib();
    	//echo "<pre>";print_r($_POST);echo "</pre>";
    	$no_  = $this->input->post('NoTransaksi');
    	$TglPayment  = $this->input->post('v_tgl_payment');
		$KdKasBank  = $this->input->post('KdKasBank');
		$Penerima  = $this->input->post('Penerima');
		$NoBukti  = $this->input->post('NoBukti');
		$Total_komisi  = $this->input->post('Total_komisi');
		$Pembulatan_komisi  = $this->input->post('Pembulatan_komisi');
		$user        = $this->session->userdata('userid');
		
		$selisih_komisi = $Pembulatan_komisi - $Total_komisi ;
		
		
     	$bulan2 = substr($TglPayment, 3, 2);
		$tahun2 = substr($TglPayment, -2);
		
		if (date('Y-m-d',strtotime($TglPayment)) > date('Y-m-d',strtotime('2016-10-31'))){
		    $kd_no = $this->paymentmodel->getKodeBank($KdKasBank);
		    $NoDokumen = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun2,$bulan2);
		}else{
		    $new_no = $this->paymentmodel->getNewNo($TglPayment);
		    $NoDokumen = $new_no->NoPayment;
		}
		
		
		//update dulu ke komisi
		$data_=array(
			'KdKasBank'=>$KdKasBank,
			'Penerima'=>$Penerima,
			'NoDokumen'=>$NoDokumen,
			'NoBukti'=>$NoBukti,
			'Pembulatan'=>$Pembulatan_komisi
		);
		$this->db->update('finance_komisi_komunitas_header',$data_,array('NoTransaksi'=>$no_));
		
                 
    	$sql="SELECT 
			  b.`NoDokumen`,
			  b.`NoBukti`,
			  b.`Keterangan`,
			  '25' AS `KdSubDivisi`,
			  c.`Omset`*a.PersenKomisiKomunitas/100 AS jumlah
			FROM
			  `finance_komisi_komunitas_header` b 
			  INNER JOIN `finance_komisi_komunitas_detail` c 
				ON b.`NoTransaksi` = c.`NoTransaksi` 
				inner join aplikasi a 
			WHERE b.`NoTransaksi` = '$no_' HAVING(jumlah>0)
			;";
		
		$rek = $this->komisi_komunitas_model->Querydata($sql);		
		
		$savekdrekening='42040050';
		$no=1;
		$totkomisi=0;
		for($i=0;$i<count($rek);$i++){
			
			$counter     =$no;
			$jumlah1 	=$rek[$i]['jumlah'];
			$NoBukti 	=$rek[$i]['NoBukti'];
			$keterangan =$rek[$i]['Keterangan'];
			$subdivisi 	=$rek[$i]['KdSubDivisi'];
			$NoDokumen 	=$rek[$i]['NoDokumen'];
			
			$totkomisi += $jumlah1;
			if($i== (count($rek)-1)){
				$jumlah1 += $Total_komisi-$totkomisi;
			}
			
			if($jumlah1>0){
				$this->insertpaymentdetail("add", $NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$jumlah1,$counter,$keterangan,$user,$savekdrekening,$subdivisi);
				$no=$no+1;	
			}
			
		}
		
		//Selisih pembulatan
		if($selisih_komisi>0){
			$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'44010025','25');
		}elseif($selisih_komisi<0){
			$this->insertpaymentdetail("add",$NoDokumen,$mylib->ubah_tanggal($TglPayment),$NoBukti,$selisih_komisi,'99','Selisih Pembulatan '.$selisih_komisi,$user,'43010020','25');	
		}
		
		
		$sql_="SELECT * from finance_komisi_komunitas_header a where a.NoTransaksi='".$no_."'";
		$cek = $this->komisi_komunitas_model->Querydata($sql_);
		$ket = $cek[0]['Keterangan'];
		$total_ = $cek[0]['Total'];
		//insert new header payment	
		$this->insertpaymentheader($no,$mylib->ubah_tanggal($TglPayment),$NoBukti,$ket,$user,$Pembulatan_komisi,$Penerima,$KdKasBank, $NoDokumen);
		 
		//update ke finance komisi header
		$this->db->update('finance_komisi_komunitas_header',array('TglPayment'=>$mylib->ubah_tanggal($TglPayment)),array('NoTransaksi'=>$no_));
					
    }
 
 
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 6, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
	
	function insertpaymentheader($no,$tgl,$NoBukti,$ket,$user,$totkomisi,$Penerima,$KdKasBank, $NoDokumen)
	{
		$this->paymentmodel->locktables('counter,trans_payment_header');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
		
		//die;
		$data = array(
			'NoDokumen'	    => $NoDokumen,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahPayment' => $totkomisi,
			'Status' => '1'
		);
		$this->db->insert('trans_payment_header', $data);
		$this->db->update('finance_komisi_komunitas_header',$data=array('NoDokumen'=>$NoDokumen),$data=array('NoTransaksi'=>$no));
		$this->paymentmodel->unlocktables();
		return $no;
	}
    
	function insertpaymentdetail($flag,$NoDokumen,$tgl,$NoBukti,$jumlah,$counter,$keterangan,$user,$savekdrekening,$subdivisi)
	{
		
		$this->paymentmodel->locktables('trans_payment_detail');
		$data = array(
			'NoDokumen'	=> $NoDokumen,
			'TglDokumen' => $tgl,
			'KdRekening' => $savekdrekening,//komisi marketing
			'Jumlah' => $jumlah,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan,
			'Urutan'	=> $counter,
			'NoBukti' => $NoBukti,
			'Status' => '1',
			'AddDate' => date('Y-m-d'),
			'AddUser' => $user
		);
		$this->db->insert('trans_payment_detail', $data);
		
		$this->paymentmodel->unlocktables();
	}
	
	function versistruk2()
	{
		
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		//echo "hello";die;
		$data['store'] = $this->komisi_komunitas_model->aplikasi();
		$data['header'] = $this->komisi_komunitas_model->getHeader($no);
		$data['detail'] = $this->komisi_komunitas_model->getHeaderForPrint2($no);
		
		$data['reset']  =chr(27).'@';
		$data['plength']=chr(27).'C';
		$data['lmargin']=chr(27).'l';
		$data['cond']   =chr(15);
		$data['ncond']  =chr(18);
		$data['dwidth'] =chr(27).'!'.chr(24);
		$data['ndwidth']=chr(27).'!'.chr(14);
		$data['draft']  =chr(27).'x'.chr(48);
		$data['nlq']    =chr(27).'x'.chr(49);
		$data['bold']   =chr(27).'E';
		$data['nbold']  =chr(27).'F';
		$data['uline']  =chr(27).'!'.chr(129);
		$data['nuline'] =chr(27).'!'.chr(1);
		$data['dstrik'] =chr(27).'G';
		$data['ndstrik']=chr(27).'H';
		$data['elite']  ='';
		$data['pica']   =chr(27).'P';
		$data['height'] =chr(27).'!'.chr(16);
		$data['nheight']=chr(27).'!'.chr(1);
		$data['spasi05']=chr(27)."3".chr(16);
		$data['spasi1'] =chr(27)."3".chr(24);
		$data['fcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'i';
		$data['pcut']   =chr(10).chr(10).chr(10).chr(10).chr(10).chr(13).chr(27).'m';
		$data['op_cash']=chr(27).'p'.chr(0).chr(50).chr(20).chr(20);
		
		//if(!empty($data['header'])){
	//		//$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
	//		$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi2_new', $data); // jika ada printernya
	//	}
		
		$this->load->helper('text');
		$this->load->helper('print');
		$data['nl']	= "\r\n";
		$html		= $this->load->view('transaksi/keuangan/komisi_komunitas/cetak_strukkomisi2 _new', $data,TRUE);
		$filename	='komisikomunitas'.$no;
		$ext		='ctk';
		header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
		header("Content-Transfer-Encoding: binary");
		header('Expires: 0');
		header('Pragma: no-cache');
		print $html;
	}
	
	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib       = new globallib();
		$printreport = new printreportlib();
		$id          = $this->uri->segment(4);
		$header      = $this->komisi_komunitas_model->getHeader($id);
		$data['header'] = $header;
		$detail = $this->komisi_komunitas_model->getDetailForPrint($id);
		$data['judul1'] = array("NoTransaksi","Tanggal","Keterangan","Sticker","Total Sales","Total Komisi");
		$data['niljudul1'] = array($header->NoTransaksi,$header->TglTransaksi,stripslashes($header->Keterangan),$header->KdAgent,number_format($header->TotalSales,0,'','.'),number_format($header->Total,0,'','.'));
		$data['judul2'] = "";
		$data['niljudul2'] = "";
		$data['judullap'] = "Pembayaran Komisi Komunitas";
		$data['colspan_line'] = 4;
		$data['url'] = "komisi/printThis/" . $id;
		$data['url2'] = "komisi/versistruk/" . $id;
		$data['url3'] = "komisi/versistruk2/" . $id;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("PCode", "Nama Barang", "Qty", "Harga");
		$data['panjang_kertas'] = 30;
		$jmlh_baris_lain = 19;
		$data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
		$jml_baris_detail = count($detail);
		if($data['panjang_per_hal'] == 0){
			$data['tot_hal'] = 1;
		}
		else
		{
			$data['tot_hal'] = ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
		}
		$list_detail = array();
		$detail_attr = array();
		$list_detail_attr = array();
		$detail_page = array();
		$new_array = array();
		$counterBaris = 0;
		$counterRow   = 0;
		$max_field_len= array(0,0,0);
		$sum_netto = 0;
		//                print_r($detail);
		  for ($m = 0; $m < count($detail); $m++) {
		//			$attr = $this->komisi_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
		unset($list_detail);
		$counterRow++;
		$list_detail[] = stripslashes($detail[$m]['PCode']);
		$list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
		$list_detail[] = stripslashes($detail[$m]['Qty']);
		$list_detail[] = number_format($detail[$m]['Harga'], 0, '', '.');
		$detail_page[] = $list_detail;
		$max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
		if ($data['panjang_per_hal'] != 0) {
		if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
		$data['detail'][] = $detail_page;
		if ($m != count($detail) - 1) {
		unset($detail_page);
		}
		}
		}
		$netto = $detail[$m]['Harga'];
		$sum_netto = $sum_netto + ($netto);
		}
		$data['judul_netto'] = array("Total");
		$data['isi_netto'] = array(number_format($sum_netto, 0, '', '.'));
		$data['detail'][] = $detail_page;
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}
	
	function CetakExcel()
	{
		$id          = $this->uri->segment(4);
		$data['header']      = $this->komisi_komunitas_model->getHeaderForExcel($id);
		$data['detail'] = $this->komisi_komunitas_model->getDetailForPrint($id);
		
		$html = $this->load->view('transaksi/cetak_transaksi/cetak_transaksi_komisi_komunitas_excel', $data, true);
		
		header('Content-Type: application/vnd.ms-excel');
    	header('Content-Disposition: attachment; filename="komisi_komunitas.xls"');
    	print $html;
	}
	

}

?>
