<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class pencairan extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/pencairanmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '15';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/pencairan/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/pencairan/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/pencairan/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$thnbln = $data['tahun'].$data['bulan'];
			$data['header']		 		= array("No Dokumen","Tanggal","Jenis","Jenis Giro","Bank Cair","Jumlah","Keterangan","Username");
	        $config['total_rows']		= $this->pencairanmodel->num_pencairan_row($id,$with,$thnbln);		
	        $data['data']	= $this->pencairanmodel->getpencairanList($config['per_page'],$page,$id,$with,$thnbln);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/pencairan/pencairanlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new($pesan){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->pencairanmodel->getDate();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$tgl = '01'.'-'.$data['bulan'].'-'.$data['tahun'];
			$data['tanggal'] = $aplikasi->TglTrans;
			if(($data['tahun']==substr($data['tanggal'],-4))&&($data['bulan']==substr($data['tanggal'],3,2)))
			     $data['tanggal'] = $aplikasi->TglTrans;
		    else
			     $data['tanggal'] = $tgl;
			$data['mjenis'] = array("P"=>"Giro Payment","R"=>"Giro Receipt");
			$data['mkasbank'] = $this->pencairanmodel->getKasBank();
			$data['pesan'] = $pesan;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/pencairan/add_pencairan',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "pencairan.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$id = $this->uri->segment(4);
		$header	 = $this->pencairanmodel->getHeader($id);
		$data['header']	 = $header;
		$detail	 = $this->pencairanmodel->getDetailForPrint($id);
		$data['judul1'] = array("No Pencairan","Kas Bank","Keterangan");
		$data['niljudul1'] = array($header->NoDokumen,$header->KdKasBank." - ".stripslashes($header->NamaKasBank),stripslashes($header->Keterangan));
		$data['judul2'] = array("Tanggal","Cost Center","Personal");
		$data['niljudul2'] = array($header->Tanggal,$header->KdCostCenter." - ".stripslashes($header->NamaCostCenter),$header->KdPersonal." - ".stripslashes($header->NamaPersonal));
		$data['judullap'] = "Pencairan Giro";
		$data['url'] = "pencairan/printThis/".$id;
		$data['colspan_line'] = 4;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("Rekening","Nama","Keterangan","Jumlah");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0,0);
		for($m=0;$m<count($detail);$m++)
		{
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['KdRekening']);
			$list_detail[] = stripslashes($detail[$m]['NamaRekening']);
			$list_detail[] = stripslashes($detail[$m]['Keterangan']);
			$list_detail[] = $detail[$m]['Jumlah'];
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array("Jumlah Pencairan");
		$data['footer2']  = array($header->JumlahPencairan);
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}

    function delete_pencairan(){
     	$id = $this->input->post('kode');
		$this->pencairanmodel->locktables('trans_pencairan_header,trans_pencairan_detail','bukugiro');
		$this->db->delete('trans_pencairan_header', array('NoDokumen' => $id));
		$this->db->delete('trans_pencairan_detail', array('NoDokumen' => $id));
		$this->db->delete('bukugiro', array('NoTransaksi' => $id, 'Jenis' => 'P'));
		$this->pencairanmodel->unlocktables();
	}

    function edit_pencairan($id){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
			$data['header']	= $this->pencairanmodel->getHeader($id);
	    	$data['detail']	= $this->pencairanmodel->getDetail($id);
			$data['mjenis'] = array("P"=>"Giro Payment","R"=>"Giro Receipt");
			$data['mkasbank'] = $this->pencairanmodel->getKasBank();
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/pencairan/edit_pencairan', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	
	function getnogiro()
	{
		$kode = $this->input->post('nogiro');
		$jenis = $this->input->post('jenistr');
		$bank = $this->input->post('bankcair');
		$valgiro = $this->pencairanmodel->findnogiro($kode,$jenis,$bank);
		if(count($valgiro)!=0)
		{
			$kode = $valgiro->nogiro."*-*".$valgiro->tglterima."*-*".$valgiro->tgljto."*-*".$valgiro->nilaigiro;
		}
		else
		{
			$kode = "";
		}
		echo $kode;
	}

	function save_new_pencairan(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$kasbank = $this->input->post('kasbank');
		$bankcair = $this->input->post('bankcair');
		$ket = trim(strtoupper(addslashes($this->input->post('ket'))));
		$nogiro = $this->input->post('nogiro');
		$tglbuka = $this->input->post('tglbuka');
		$tgljto = $this->input->post('tgljto');
		$jumlah = $this->input->post('jumlah');
		$savenogiro = $this->input->post('savenogiro');
		$jumlahpencairan = $this->input->post('jumlahpencairan');
		$counter = $this->input->post('urutan');
		if(($jenis=="2")&&($no==""))
		{
	       $cekgiro = $this->pencairanmodel->cekgiro($nogiro);
		   $adagiro = count($cekgiro);
		}
		else
		   $adagiro = "";
		if($adagiro=="")
		{
			if($no=="")
			{
				$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$ket,$user,$jumlahpencairan,$jenis,$bankcair);
			}
			else
			{
				$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$ket,$user,$jumlahpencairan,$jenis,$bankcair);
			}
			for($x=0;$x<count($nogiro);$x++)
			{
				$nogiro1 = strtoupper(addslashes(trim($nogiro[$x])));
				$jumlah1 = $jumlah[$x];
				$savenogiro1 = $savenogiro[$x];
				$tglbuka1 = $tglbuka[$x];
				$tgljto1 = $tgljto[$x];
				$counter1 = $counter[$x];
				if($nogiro1!=""){
				    $this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter1,$nogiro1,$jumlah1,$tglbuka1,$tgljto1,$user,$savenogiro1);
				}
			}
			$this->add_new('new');
		}else
		{
		  $this->add_new($nogiro);
		}
	}
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$kasbank = $this->input->post('kasbank');
		$bankcair = $this->input->post('bankcair');
		$ket = trim(strtoupper(addslashes($this->input->post('ket'))));
		$nogiro = strtoupper(addslashes(trim($this->input->post('nogiro'))));
		$tglbuka = $this->input->post('tglbuka');
		$tgljto = $this->input->post('tgljto');
		$jumlah = $this->input->post('jumlah');
		$savenogiro = $this->input->post('savenogiro');
		$counter = $this->input->post('urutan');
		$jumlahpencairan = $this->input->post('jumlahpencairan');

		if($no=="")
		{
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$ket,$user,$jumlahpencairan,$jenis,$bankcair);
		}
		else
		{
			$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$ket,$user,$jumlahpencairan,$jenis,$bankcair);
		}
		$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter,$nogiro,$jumlah,$tglbuka,$tgljto,$user,$savenogiro);
		echo $no;
	}

	function insertNewHeader($flag,$tgl,$kasbank,$ket,$user,$jumlahpencairan,$jenis,$bankcair)
	{
		$this->pencairanmodel->locktables('counter,trans_pencairan_header');
		$new_no = $this->pencairanmodel->getNewNo($tgl);
		$no = $new_no->NoPencairan;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'NoDokumen'	=> $no,
			'TglDokumen' => $tgl,
			'Jenis' => $jenis,
			'KdKasBank' => $kasbank,
			'KdBankCair' => $bankcair,
			'Keterangan' => $ket ,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'jumlahpencairan' => $jumlahpencairan
		);
		$this->db->insert('trans_pencairan_header', $data);
		$this->pencairanmodel->unlocktables();
		return $no;
	}
	function updateHeader($flag,$no,$tgl,$kasbank,$ket,$user,$jumlahpencairan,$jenis,$bankcair)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->pencairanmodel->locktables('trans_pencairan_header');
		$data = array(
		    'TglDokumen' => $tgl,
			'Jenis' => $jenis,
		    'KdKasBank' => $kasbank,
			'KdBankCair' => $bankcair,
			'Keterangan' => $ket ,
			'jumlahpencairan' => $jumlahpencairan
		);
		if($flag=="edit")
		{
			$data['EditDate'] = $tgl1;
			$data['EditUser'] = $user;
			$this->db->update('trans_pencairan_detail', array('EditDate'=> $tgl1,'EditUser'=>$user), array('NoDokumen' => $no));
		}
		$this->db->update('trans_pencairan_header', $data, array('NoDokumen' => $no));
		$this->pencairanmodel->unlocktables();
	}

	function insertDetail($flag,$no,$tgl,$counter,$nogiro,$jumlah,$tglbuka,$tgljto,$user,$savenogiro)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->pencairanmodel->locktables('trans_pencairan_detail');
		if($savenogiro==""){
			$data = array(
				'NoDokumen'	=> $no,
				'TglDokumen' => $tgl,
				'NoGiro' => $nogiro,
				'Jumlah' => $jumlah,
				'TglBuka' => $tglbuka,
				'TglJto' => $tgljto,
				'Urutan'	=> $counter
			);
			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->insert('trans_pencairan_detail', $data);
			$data = array(
				'NoCair'	=> $no,
				'TglCair' => $tgl,
				'Status' => 'C',
				'UserCair' => $user
			);
			$this->db->update('bukugiro', $data, array('NoGiro'=>$nogiro));
		}
		else if($savenogiro!=$nogiro)
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'NoGiro' => $nogiro,
				'Jumlah' => $jumlah,
				'TglBuka' => $tglbuka,
				'TglJto' => $tgljto
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_pencairan_detail', $data, array('NoDokumen' => $no,'NoGiro'=>$savenogiro,'Urutan'=>$counter));
			$data = array(
				'NoCair'	=> $no,
				'TglCair' => $tgl,
				'Status' => 'C',
				'UserCair' => $user
			);
			$this->db->update('bukugiro', $data, array('NoGiro'=>$nogiro));
			$data = array(
				'NoCair'	=> '',
				'TglCair' => '0000-00-00',
				'Status' => 'B',
				'UserCair' => ''
			);
			$this->db->update('bukugiro', $data, array('NoGiro'=>$savenogiro));
		}
		else if($savenogiro==$nogiro)
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'Jumlah' => $jumlah 
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_pencairan_detail', $data, array('NoDokumen' => $no,'NoGiro'=>$nogiro,'Urutan'=>$counter));
	        $data = array(
				'TglCair' => $tgl,
				'UserCair' => $user
			);
			$this->db->update('bukugiro', $data, array('NoGiro'=>$nogiro));
		}
		$this->pencairanmodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$counter = $this->input->post('urutan');
		$nogiro = $this->input->post('nogiro');
		$this->pencairanmodel->locktables('trans_pencairan_detail');
		$this->db->delete('trans_pencairan_detail', array('NoDokumen' => $id,'NoGiro'=>$nogiro,'Urutan'=>$counter));
		$this->pencairanmodel->unlocktables();
	}

}
?>