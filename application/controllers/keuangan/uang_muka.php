<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Uang_muka extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/keuangan/uang_muka_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
        	
        	$id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
            

            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
            	
                $resSearch = $this->globalmodel->getSearch($id, "uang_muka", $user);
                $arrSearch = explode("&", $resSearch->query_string);

                $id_search = $resSearch->id;
                
                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];
                    
                    $data["search_keyword"] = $search_keyword[1];
                }
            }
        	
        	 // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/keuangan/uang_muka/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);

            } else {
                $config['base_url'] = base_url() . 'index.php/keuangan/uang_muka/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
   
            }
            
            //$arr_search["search"]=20;
            //$page = 0;
            $config['total_rows'] = $this->uang_muka_model->num_uang_muka_row($arr_search["search"]);
            $data['data'] = $this->uang_muka_model->getUangMukaList($config['per_page'], $page, $arr_search["search"]);
            
            
            $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$this->load->view('transaksi/keuangan/uang_muka/list_uang_muka', $data);
           
        } else {
            $this->load->view('denied');
        }
    } 
  
    function search() 
    {
    	
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'uang_muka', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		
		$data = array(
            'query_string' => $search_value,
            'module' => "uang_muka",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/keuangan/uang_muka/index/' . $query_id . '');
    }

    function add_new() 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $data['KasBank'] = $this->uang_muka_model->getKasBank($user);
            $data['employee'] = $this->uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->uang_muka_model->getSubDivisi();
            $data['rekeningpph'] = $this->uang_muka_model->getRekeningPPH();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/uang_muka/add_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
     function edit_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="edit";
            $data['header'] = $this->uang_muka_model->getListUangMuka($id);
            
            $data['KasBank'] = $this->uang_muka_model->getKasBank($user);
            $data['employee'] = $this->uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->uang_muka_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/uang_muka/edit_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    function view_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="view";
            $data['header'] = $this->uang_muka_model->getListUangMuka($id);
            
            $data['KasBank'] = $this->uang_muka_model->getKasBank($user);
            $data['employee'] = $this->uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->uang_muka_model->getSubDivisi();
            $data['rekeningpph'] = $this->uang_muka_model->getRekeningPPH();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/uang_muka/edit_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }

	
	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $v_no_dokumen = $this->input->post('v_no_dokumen');
        $v_no_pv = $this->input->post('v_no_pv');
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_kdkasbank= $this->input->post('v_kas_bank');
        $v_no_referensi = $this->input->post('v_no_ref');
        $v_employee = $this->input->post('v_employee');
        $v_jumlah = $this->input->post('v_jumlah');
        $v_sisa = $this->input->post('v_sisa');
		$v_no_rek = $this->input->post('v_no_rek');
		$v_subdivisi = $this->input->post('v_subdivisi');
		$v_note = $this->input->post('v_note');
        $v_dpp = $this->input->post('v_dpp');
        $v_ppn = $this->input->post('v_ppn');
        $v_pph = $this->input->post('v_pph');
        $v_nilaippn = $this->input->post('v_nilaippn');
        $v_nilaipph = $this->input->post('v_nilaipph');
        $v_rekeningpph = $this->input->post('v_rekeningpph');
		
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        
        
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
		
		$bln = substr($mylib->ubah_tanggal($v_tgl_dokumen), 5, 2);
        $thn = substr($mylib->ubah_tanggal($v_tgl_dokumen), 2, 2);
        $tahun = $thn + 2000;

        if ($flag == "add")
		{
			//generate number
			$this->db->trans_start();
			$v_no_dokumen = $mylib->get_code_counter($this->db->database, "uang_muka","NoDokumen", "UM", $bln, $tahun);
			
			//nomor Payment Voucher
			$kd_no = $this->uang_muka_model->getKodeBank($v_kdkasbank);
		    $NoPV = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$thn, $bln);
			
			$employee_name = $this->uang_muka_model->getEmployeeName($v_employee);
			
			//insert ke trans_payment_header
			$this->insertpaymentheader($NoPV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_note,$user,$v_jumlah,$employee_name->employee_name,$v_kdkasbank, $v_no_dokumen);
			
			//insert ke trans_payment_detail
			$this->insertpaymentdetail($NoPV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_dpp,"1",$v_note,$user,$v_no_rek,$v_subdivisi,$v_no_dokumen,$v_nilaippn,$v_nilaipph,$v_rekeningpph);
			
			//insert uang_muka			
            $this->insertNewUangMuka($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_kdkasbank, $v_no_referensi, $v_employee, $v_jumlah, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user, $v_dpp, $v_nilaipph, $v_rekeningpph, $v_ppn,$v_nilaippn);	
			$this->db->trans_complete();
			
			$this->session->set_flashdata('msg', array('message' => 'Proses Add <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
			
			$link = "<script>
							window.open('".base_url()."index.php/keuangan/payment/cetak/".$NoPV ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
							window.location.href = '".base_url()."index.php/keuangan/uang_muka/add_new/"."';
						</script>";
		} 
		
		
		else if ($flag == "edit") 
		{
			$employee_name = $this->uang_muka_model->getEmployeeName($v_employee);
			$this->db->trans_start();
			//update ke trans_payment_header
			$this->updatepaymentheader($v_no_pv,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_note,$user,$v_jumlah,$employee_name->employee_name,$v_kdkasbank, $v_no_dokumen);
			//update ke trans_payment_detail
			$this->updatepaymentdetail($v_no_pv,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_dpp,"1",$v_note,$user,$v_no_rek,$v_subdivisi,$v_no_dokumen,$v_nilaippn,$v_nilaipph,$v_rekeningpph);
			
			//update uang_muka			
            $this->updateNewUangMuka($v_no_dokumen, $mylib->ubah_tanggal($v_tgl_dokumen), $v_kdkasbank, $v_no_referensi, $v_employee, $v_jumlah, $v_no_rek, $v_subdivisi, $v_note, $v_no_pv, $user, $v_dpp, $v_nilaipph, $v_rekeningpph, $v_ppn,$v_nilaippn);
        
			$this->db->trans_complete();
			
            $this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
            
            $link = "<script>
							window.open('".base_url()."index.php/keuangan/payment/cetak/".$v_no_pv ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
							window.location.href = '".base_url()."index.php/keuangan/uang_muka/add_new/"."';
						</script>";
        }
		
        //redirect('/keuangan/uang_muka/edit_uang_muka/' . $v_no_dokumen . '');
        
		echo $link;
    }
    
    
    function insertNewUangMuka($v_no_dokumen,$v_tgl_dokumen, $v_kdkasbank, $v_no_referensi, $v_employee, $v_jumlah, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user,$v_nilaidpp=0,$v_nilaipph=0,$v_rekeningpph,$v_ppn,$v_nilaippn)
	{
		//$this->uang_muka_model->locktables('uang_muka');
		
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'TglDokumen'=>$v_tgl_dokumen,
             'KdKasBank' => $v_kdkasbank,
             'NoReferensi' => $v_no_referensi,
             'Employee' => $v_employee,
             'NilaiDPP' => $v_nilaidpp,
             'NilaiPPN' => $v_nilaippn,
             'NilaiPPH' => $v_nilaipph,
             'Jumlah' => $v_jumlah,
             'NoRekening' => $v_no_rek,
             'KdSubDivisi' => $v_subdivisi,
             'NoPV' => $NoPV,
             'PPN' => $v_ppn,
             'RekeningPPH'=> $v_rekeningpph,
             'Sisa' => $v_jumlah,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
		);
		$this->db->insert('uang_muka', $data);
		//$this->uang_muka_model->unlocktables();
	}
	
	
	function updateNewUangMuka($v_no_dokumen,$v_tgl_dokumen, $v_kdkasbank, $v_no_referensi, $v_employee, $v_jumlah, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user)
	{
		//$this->uang_muka_model->locktables('uang_muka');
		
		$data = array(
             'TglDokumen'=>$v_tgl_dokumen,
             'KdKasBank' => $v_kdkasbank,
             'NoReferensi' => $v_no_referensi,
             'Employee' => $v_employee,
             'Jumlah' => $v_jumlah,
             'NoRekening' => $v_no_rek,
             'KdSubDivisi' => $v_subdivisi,
             'NoPV' => $NoPV,
             'Sisa' => $v_jumlah,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
		);
		$where = array('NoDokumen'=>$v_no_dokumen);
		$this->db->update('uang_muka', $data, $where);
		//$this->uang_muka_model->unlocktables();
	}
	
    
    function updatepaymentheader($nopv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank, $uangmuka)
	{
		//$this->uang_muka_model->locktables('trans_payment_header');
		
		$data = array(
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket . ' - '.$uangmuka,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahPayment' => $jumlah,
			'Status' => '1'
		);
		
		$where = array('NoDokumen'=> $nopv);
		
		$this->db->update('trans_payment_header', $data, $where);
		//$this->uang_muka_model->unlocktables();
	}
	
	
	function insertpaymentheader($nopv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank, $uangmuka)
	{
		//$this->uang_muka_model->locktables('trans_payment_header');
		
		$data = array(
			'NoDokumen'	    => $nopv,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket . ' - '.$uangmuka,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahPayment' => $jumlah,
			'Status' => '1'
		);
		$this->db->insert('trans_payment_header', $data);
		//$this->uang_muka_model->unlocktables();
	}
	
	
	function insertpaymentdetail($NoDokumen,$tgl,$Noref,$dpp,$counter,$keterangan,$user,$norek,$subdivisi,$nouangmuka,$nilaippn,$nilaipph,$rekeningpph)
	{
		$interface = $this->uang_muka_model->getinterface();
		//$this->uang_muka_model->locktables('trans_payment_detail');
		$data = array(
			'NoDokumen'	=> $NoDokumen,
			'TglDokumen' => $tgl,
			'KdRekening' => $interface['UMKaryawan'],
			'Jumlah' => $dpp,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan.'-'.$nouangmuka,
			'Urutan'	=> $counter,
			'NoBukti' => $Noref,
			'Status' => '1',
			'AddDate' => date('Y-m-d'),
			'AddUser' => $user
		);
		$this->db->insert('trans_payment_detail', $data);

        #PPN
        if($nilaippn != 0){
            $norek_ppn = '11100050';
            $data = array(
                'NoDokumen' => $NoDokumen,
                'TglDokumen' => $tgl,
                'KdRekening' => $norek_ppn,
                'Jumlah' => $nilaippn,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => '00',
                'Keterangan' => 'PPN '.$keterangan. ' - '.$nouangmuka,
                'Urutan'    => $counter,
                'NoBukti' => $Noref,
                'Status' => '1',
                'AddDate' => date('Y-m-d'),
                'AddUser' => $user
            );
            $this->db->insert('trans_payment_detail', $data);
        }


        #PPh
        if($nilaipph != 0){
            $nilaipphminus = intval($nilaipph) * -1; 
            $data = array(
                'NoDokumen' => $NoDokumen,
                'TglDokumen' => $tgl,
                'KdRekening' => $rekeningpph,
                'Jumlah' => $nilaipphminus,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => '00',
                'Keterangan' => 'PPH '.$keterangan. ' - '.$nouangmuka,
                'Urutan'    => $counter,
                'NoBukti' => $Noref,
                'Status' => '1',
                'AddDate' => date('Y-m-d'),
                'AddUser' => $user
            );
            $this->db->insert('trans_payment_detail', $data);
        }
		
		//$this->uang_muka_model->unlocktables();
	}
	
	function updatepaymentdetail($NoDokumen,$tgl,$Noref,$jumlah,$counter,$keterangan,$user,$norek,$subdivisi,$nouangmuka)
	{
		$interface = $this->uang_muka_model->getinterface();
		
		//$this->uang_muka_model->locktables('trans_payment_detail');
		
		$data = array(
			'TglDokumen' => $tgl,
			'KdRekening' => $interface['UMKaryawan'],
			'Jumlah' => $jumlah,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan.'-'.$nouangmuka,
			'Urutan'	=> $counter,
			'NoBukti' => $Noref,
			'Status' => '1',
			'AddDate' => date('Y-m-d'),
			'AddUser' => $user
		);
		$where = array ('NoDokumen'	=> $NoDokumen);
		$this->db->update('trans_payment_detail', $data, $where);
		
		//$this->uang_muka_model->unlocktables();
	}
	
    
    function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
		
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;	
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
    function cari_karyawan(){
		$employee = $this->input->post('employee');
	    $query = $this->uang_muka_model->getEmployee($employee);
	     	     
	    echo "<option value=''> -- Pilih Karyaan --</option>";
	    foreach ($query as $rec) {
			echo "<option value=$rec[employee_id]>$rec[employee_name]</option>";
		}   
	}
    
    
    
    function delete_trans($UM, $PV) 
    {
            $this->db->delete('trans_payment_header', array('NoDokumen' => $PV));	
			$this->db->delete('trans_payment_detail', array('NoDokumen' => $PV));
			$this->db->delete('uang_muka', array('NoDokumen' => $UM));
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/keuangan/uang_muka/');
    }
    
    
    
		
}

?>