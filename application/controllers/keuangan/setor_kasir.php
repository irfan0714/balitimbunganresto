<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class setor_kasir extends authcontroller {
    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/keuangan/setor_model');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
            $segs = $this->uri->segment_array();
            $arr = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
            $data['link'] = $mylib->restrictLink($arr);
            $id = addslashes(trim($this->input->post('stSearchingKey')));
            $id2 = $this->input->post('tgl');
            $counter = $this->input->post('master');
            $with = $this->input->post('searchby');
            if ($with == "TglTransaksi") {
                $id = $mylib->ubah_tanggal($id2);
            }
            $this->load->library('pagination');

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = '</span>';
            $config['per_page'] = '10';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            $config['base_url'] = base_url() . 'index.php/finance/setoran/index/';
            $page = $this->uri->segment(4);
            $config['uri_segment'] = 4;
            $flag1 = "";
            if ($with != "") {
                if ($id != "" && $with != "") {
                    $config['base_url'] = base_url() . 'index.php/finance/setoran/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                    $config['per_page'] = '10';
                } else if ($counter != "" || $with != "") {
                    $config['base_url'] = base_url() . 'index.php/finance/setoran/index/' . $with . "/" . $counter . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                    $config['per_page'] = '50';
                } else {
                    $config['base_url'] = base_url() . 'index.php/finance/setoran/index/';// . $with . "/" . $id . "/";
                    $page = "";
                    $config['per_page'] = '10';
                }
            } else {
                if ($this->uri->segment(5) != "") {
                    $with = $this->uri->segment(4);
                    $id = $this->uri->segment(5);
                    if ($with == "TglTransaksi") {
                        $id = $mylib->ubah_tanggal($id);
                    }
                    $config['base_url'] = base_url() . 'index.php/finance/setoran/index/' . $with . "/" . $id . "/";
                    $page = $this->uri->segment(6);
                    $config['uri_segment'] = 6;
                    $config['per_page'] = '10';
                }
            }
            
            $config['total_rows'] = $this->setor_model->num_setor_row(addslashes($id), $counter, $with);
            $data['datalist'] = $this->setor_model->getSetorList($config['per_page'], $page, $id, $counter, $with);
            //$data['master'] = $this->setor_model->getMaster();
            $data['track'] = $mylib->print_track();
            $data['judul'] = "List Setoran Kasir";
            $this->pagination->initialize($config);
            $this->load->view('transaksi/keuangan/setoran/viewsetoranlist', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function add_new() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $aplikasi = $this->setor_model->getDate();
            $data['aplikasi']   = $aplikasi;
            $data['judul']      = "Setoran Kasir";
            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);
            //$data['master'] = $this->setor_model->getMaster();
            $this->load->view('transaksi/keuangan/setoran/addsetoran', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function view_setoran($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("view");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $data['viewsetoran'] = $this->setor_model->getDetail($id);
            $data['edit'] = false;
            $this->load->view('finance/setoran/viewsetorandetail', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function edit_setoran($id) {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("edit");
        if ($sign == "Y") {
            $id = $this->uri->segment(4);
            $vSetoran = $this->setor_model->getDetail($id);

            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);
            $data['NoTransaksi']= $vSetoran->NoTransaksi;
            $data['Jam']        = $vSetoran->Jam;
            $data['Nik']        = $vSetoran->Nik;
            $data['NamaKasir']  = $vSetoran->NamaKasir;
            $data['tgl']        = $vSetoran->TglTransaksi;
            $data['NilaiSetoran'] = $vSetoran->NilaiSetoran;
            $data['Tunai']      =  $vSetoran->Tunai;
            $data['KKredit']    = $vSetoran->KKredit;
            $data['KDebit']     = $vSetoran->KKredit;
            $data['Voucher']    = $vSetoran->KKredit;
            $data['Selisih']    = $vSetoran->KKredit;
            $data['Keterangan'] = $vSetoran->KKredit;
            $data['judul']      = "Edit Setoran Kasir";

            $this->load->view('transaksi/keuangan/setoran/edit', $data);
        } else {
            $this->load->view('denied');
        }
    }

    function save_new() {
//        echo "<pre>";
//        print_r($_POST);
//        echo "</pre>";
        $user = $this->session->userdata('userid');
        $nsetoran   = $this->input->post('nsetoran');
        $jam        = $this->input->post('jam');
        $selisih    = $this->input->post('selisih');
        $jumlah     = $this->input->post('jumlah');
        $kredit     = $this->input->post('kredit');
        $debit      = $this->input->post('debit');
        $voucher    = $this->input->post('voucher');
        $kode       = $this->input->post('kode');
        $flag       = $this->input->post('flag');
        $tgl        = $this->input->post('tgl');
        $kdcounter = $this->input->post('master');
        $nodok   = $this->input->post('nodok');
        $jam = $this->input->post('jam');
        $nama = $this->input->post('nama');
        $nilai = $this->input->post('nilai');
        $ket = strtoupper(trim($this->input->post('keterangan')));
        if ($flag == "add") {
            $this->insertNew($tgl, $kdcounter, $nama, $nilai, $ket, $user,$nsetoran,$jam,$jumlah,$kredit,$debit,$voucher,$kode,$selisih);
        } else {
            $this->updateSetoran($nodok, $jam, $ket,$nsetoran,$selisih ,$user);
        }
        redirect('/keuangan/setor_kasir/');
    }
    function get_no_counter( $table_name, $col_primary, $nodepan)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1, 4) = '" .$nodepan. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
    ";
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 4, 5) * 1) + 1;
        $counter_fa = sprintf($nodepan . sprintf("%05s", $counter));
//        $counter_fa = sprintf("%02s", $divisi) . sprintf("%02s", $subdivisi) . $kategori . sprintf("%05s", $counter);
//        die();
        return $counter_fa;

    }
    function insertNew($tgl, $kdcounter, $nama, $nilai, $ket, $user,$nsetoran,$jam,$jumlah,$kredit,$debit,$voucher,$kode,$selisih) {
        $mylib = new globallib();
        //echo $tgl;
//        echo substr($tgl, 8, 2).substr($tgl,3 ,2);
        $no =  $this->get_no_counter('finance_setor','NoTransaksi',substr($tgl, 8, 2).substr($tgl,3 ,2));
        $data = array(



            'NoTransaksi'   => $no,
            'TglTransaksi'  => $mylib->ubah_tanggal($tgl),
            'KdCounter'     => $kdcounter,
            'Jam'           => $jam,
            'Nik'           => $kode,
            'NamaKasir'     => $nama,
            'NilaiSetoran'  => $nsetoran,
            'Tunai'         => $jumlah,
            'KKredit'       => $kredit,
            'KDebit'        => $debit,
            'Voucher'       => $voucher,
            'Selisih'       => $selisih,
            'Keterangan'    => $ket,
            'AddDate'   => date("Y-m-d"),
            'AddUser'   => $user
        );
       // print_r($data);
        $this->db->insert('finance_setor', $data);
        return $no;
    }

    /**
     * @param $nodok
     * @param $jam
     * @param $ket
     * @param $nsetoran
     * @param $selisih
     */
    function updateSetoran($nodok, $jam, $ket,$nsetoran,$selisih, $user){
        $mylib = new globallib();
        $tgl = $this->session->userdata('Tanggal_Trans');
        $data = array(
            'Jam'           => $jam,
            'NilaiSetoran'  => $nsetoran,
            'Selisih'       => $selisih,
            'Keterangan'    => $ket,
            'Keterangan'    => $ket,
            'EditDate'      => date("Y-m-d"),
            'EditUser'      => $user
        );
        $this->db->update('finance_setor', $data, array('NoTransaksi' => $no));
    }

    function delete_setoran() {
        $mylib = new globallib();
        $id = $this->input->post('kode');
        $tgl2 = $this->session->userdata('Tanggal_Trans');
        $tgl = $mylib->ubah_tanggal($tgl2);
        $tahun = substr($tgl2, 6, 4);
        $lastNo = $this->setor_model->getNewNo($tahun);
        $NoDelete = $id;
        if ((int) $lastNo->NoSetor == (int) $NoDelete + 1) {
         //   $this->db->update("setup_no", array("NoSetor" => $NoDelete[1]), array("Tahun" => $tahun));
        }

       // $this->db->delete('finance_setor', array('NoTransaksi' => $id));
       // redirect('/finance/setoran/');
    }

}

?>