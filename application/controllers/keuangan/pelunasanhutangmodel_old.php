<?php
class Pelunasanhutangmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getpelunasanList($num,$offset,$id,$with)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="where h.Status<>'B'";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
    	$sql = "select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,NoBukti,h.KdSupplier,Keterangan,JumlahRetur,SisaRetur,JumlahPayment,UserName,h.Jenis,Nama,Jenis
				from trans_pelunasanhutang_header h 
				left join user on user.id = h.AddUser
				left join supplier on supplier.kdsupplier = h.kdsupplier
				$clause 
				order by cast(NoDokumen as unsigned) desc Limit $offset,$num
			";
		return $this->getArrayResult($sql);
    }
    
    function num_pelunasanhutang_row($id,$with){
     	$clause="where Status<>'B'";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
		$sql = "SELECT NoDokumen FROM trans_pelunasanhutang_header $clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
    function findrekening($id){
	    $db2 = $this->load->database('gl',true);
		$sql = "SELECT kdrekening,namarekening FROM rekening Where kdrekening='$id'";
		$qry = $db2->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function findnobukti($id){
		$sql = "select NoTransaksi,date_format(TglTransaksi,'%d-%m-%Y') as TglTransaksi,a.KdSupplier as Kode,Nama,TotalHutang as Nilai,
TotalHutang-TotalBayar as Sisa,date_format(TglJto,'%d-%m-%Y') as TglJto,kdrekening as Rekening from hutang a, supplier b where Status<>'B' and Jenis in ('F','K') 
and a.KdSupplier=b.KdSupplier and NoTransaksi='$id'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	
	function getTipe(){
	    $db2 = $this->load->database('gl',true);
    	$sql = "SELECT KdRekening, NamaRekening from rekening order by KdRekening";
		$qry = $db2->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
    }

	function getNewNo($tgl)
	{
		$tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoLunasHutang=NoLunasHutang+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoLunasHutang FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	function getolddata($no,$counter)
	{
		$sql = "SELECT Jenis,NoBukti,Jumlah from trans_pelunasanhutang_detail where NoDokumen='$no' and Urutan='$counter'";
		return $this->getRow($sql);
	}
	function updatehutang($nobukti,$jumlah)
	{
		$sql = "Update hutang set TotalBayar=TotalBayar+$jumlah where NoTransaksi='$nobukti'";
		$this->db->query($sql);
	}
	function getDetail($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,NamaRekening,Urutan,Jenis,NoBukti,Hutang,Nama from(
		SELECT * from trans_pelunasanhutang_detail Where NoDokumen='$id'
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from e2sss.rekening
		)rekening
		on rekening.KdRekening = d.KdRekening
		left join
		(
			select KdSupplier,Nama from supplier
		)supplier
		on supplier.KdSupplier = d.KdSupplier";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,NamaRekening,Urutan,Jenis,NoBukti,Hutang from(
		SELECT * from trans_pelunasanhutang_detail Where NoDokumen='$id' order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from e2sss.rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
		$sql = "select NoDokumen,Tanggal,h.KdSupplier,Nama,Keterangan,h.NoBukti,h.JumlahRetur,SisaRetur,JumlahPayment,TglRetur,Jenis
from(
SELECT NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,Keterangan,
KdSupplier,NoBukti,JumlahRetur,SisaRetur,JumlahPayment,Jenis,date_format(TglRetur,'%d-%m-%Y') as TglRetur
from trans_pelunasanhutang_header where NoDokumen='$id')h
left JOIN
(
select KdSupplier,Nama from supplier
)supplier
on supplier.KdSupplier = h.KdSupplier";
        return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>