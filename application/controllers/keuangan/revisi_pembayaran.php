<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Revisi_pembayaran extends authcontroller {

    function __construct() {
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/keuangan/revisi_pembayaranmodel','revisi');
    }

    function index() {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") 
        {
            $tanggal = $this->revisi->getDate();
            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            $date = $tahun . "-" . $bulan . "-1";
            $maxtgl = date("t", strtotime($date));

			$data['v_no_struk'] = "";
            $data['v_start_date'] = '01' . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = $maxtgl . '-' . $bulan . '-' . $tahun;
            
            $data['excel'] = "";
            $data['print'] = "";
            
            $data['tampilkanDT'] = false;
            $data['tampilkanDR'] = false;
            $data['tampilkanRR'] = false;
            $data['tampilkanRT'] = false;
            
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/revisi_pembayaran/views', $data);
        } else {
            $this->load->view('denied');
        }
    }

	function search_report()
	{
		$mylib = new globallib();
        $v_no_struk = $this->input->post("v_no_struk");
        $search_report = $this->input->post("search_report");
        
	    $data['hasil'] = "";
	    $data['v_no_struk'] = "";
	    $data['tampilkanDT'] = false;
	    
        if ($search_report) 
        {
	        $data['tampilkanDT'] = true;
	        $data['v_no_struk'] = $v_no_struk;
	        $data['hasil'] = $this->revisi->getReport($v_no_struk);
	        //$data['hasil_voucher'] = $this->revisi->getVoucher($v_no_struk);
        } 
        
        $data['track'] = $mylib->print_track();
        
        $this->load->view('transaksi/keuangan/revisi_pembayaran/views', $data);
	}

	function simpan_pembayaran()
	{
        $mylib = new globallib();
        
		$v_nostruk = $this->input->post("v_nostruk");
		$grand_total = $mylib->save_int($this->input->post("grand_total"));
		$net_tunai = $mylib->save_int($this->input->post("net_tunai"));
		$v_kembali = $mylib->save_int($this->input->post("v_kembali"));
		
		$v_tunai = $mylib->save_int($this->input->post("v_tunai"));
		$v_kredit = $mylib->save_int($this->input->post("v_kredit"));
		$v_debit = $mylib->save_int($this->input->post("v_debit"));
		$v_voucher = $mylib->save_int($this->input->post("v_voucher"));
		
		$user = $this->session->userdata('username');
        
        $jns ="";
        if($v_tunai*1>0)
        {
            $jns .= "T";
        }
        if($v_kredit*1>0)
        {
            $jns .= "K";
        }
        if($v_debit*1>0)
        {
            $jns .= "D";
        }
        if($v_voucher*1>0)
        {
            $jns .= "V";
        }
		
		$dataHeader = array(
            'Tunai' => $v_tunai,
            'KKredit' => $v_kredit,
            'KDebit' => $v_debit,
            'Voucher' => $v_voucher,
            'EditDate' => date('Y-m-d'),
            'EditUser' => $user
    	);
    	
    	
		$dataBayar = array(
            'Jenis' => $jns,
            'NilaiTunai' => $v_tunai,
            'NilaiKredit' => $v_kredit,
            'NilaiDebet' => $v_debit,
            'NilaiVoucher' => $v_voucher
        );
	
        $this->db->update('transaksi_header', $dataHeader, array('noStruk' => $v_nostruk));
        $this->db->update('transaksi_detail_bayar', $dataBayar, array('noStruk' => $v_nostruk));
	
		$this->session->set_flashdata('msg', array('message' => 'Proses Revisi Pembayaran nomor struk <b>'.$v_nostruk.'</b> berhasil','class' => 'success'));
		
		redirect('/keuangan/revisi_pembayaran/');
	}

}

?>