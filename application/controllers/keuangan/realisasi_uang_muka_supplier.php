<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Realisasi_uang_muka_supplier extends authcontroller {

    function __construct() {
        parent::__construct();
        //error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/keuangan/realisasi_uang_muka_supplier_model');
        $this->load->model('transaksi/keuangan/uang_muka_supplier_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
        	
        	$id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
			
            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "realisasi_um_supplier", $user);
                $id_search=FALSE;
                if(!empty($resSearch)){
				$arrSearch = explode("&", $resSearch->query_string);
				$id_search = $resSearch->id;
				}

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                }
            }
        	
        	 // pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '20';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/keuangan/realisasi_uang_muka_supplier/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/keuangan/realisasi_uang_muka_supplier/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
            
            //$arr_search["search"]=20;
            //$page = 0;
            $config['total_rows'] = $this->realisasi_uang_muka_supplier_model->num_uang_muka_row($arr_search["search"]);
            $data['data'] = $this->realisasi_uang_muka_supplier_model->getRealisasiUangMukaList($config['per_page'], $page, $arr_search["search"]);
    		//$data['search_keyword'] = '';       
            
            
            $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$this->load->view('transaksi/keuangan/realisasi_uang_muka_supplier/list_realisasi_uang_muka', $data);
           
        } else {
            $this->load->view('denied');
        }
    } 
  
    
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'realisasi_um_supplier', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$data = array(
            'query_string' => $search_value,
            'module' => "realisasi_um_supplier",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/keuangan/realisasi_uang_muka_supplier/index/' . $query_id . '');
    } 
    
    
    function add_new() 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $data['KasBank'] = $this->realisasi_uang_muka_supplier_model->getKasBank($user);
            $data['supplier'] = $this->realisasi_uang_muka_supplier_model->getSupplier();
            $data['subdivisi'] = $this->realisasi_uang_muka_supplier_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/realisasi_uang_muka_supplier/add_realisasi_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
	function view_realisasi_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="view";
            $data['header'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiUangMuka($id);
            $data['detail_realisasi'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiDetail($id);
			$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiDetailPV($id);
			
			$data['row'] = $this->realisasi_uang_muka_supplier_model->getDataFakturView($data['header']->KdSupplier, $data['header']->TglDokumen, $id);
			$data['dept'] = $this->realisasi_uang_muka_supplier_model->getDept();
			
			$data['totbayar']= $this->realisasi_uang_muka_supplier_model->getTotalBayar($id);
			
            $data['KasBank'] = $this->realisasi_uang_muka_supplier_model->getKasBank($user);
            $data['supplier'] = $this->realisasi_uang_muka_supplier_model->getSupplier();
            $data['subdivisi'] = $this->realisasi_uang_muka_supplier_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/realisasi_uang_muka_supplier/edit_realisasi_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
	
	function save_data() 
    {
        $mylib = new globallib();
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_kdkasbank= $this->input->post('v_kas_bank');
        $v_no_referensi = $this->input->post('v_no_ref');
        $v_supplier = $this->input->post('v_supplier');
		$v_note = $this->input->post('v_note');
		$vpph = $this->input->post('vpph');
		$vpembulatan = $this->input->post('vpembulatan');
		$vtotalsbayars = $this->input->post('vtotalbayar');
		
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
		
		//detail realisasi uang muka
		$v_no_um1 = $this->input->post('v_no_um');
		$v_no_pv1 = $this->input->post('v_no_pv');
		$v_jml_um1 = $this->input->post('vSisaUM');
		$v_realisasi1 = $this->input->post('h_realisasi');
		$v_tot_realisasi = $this->input->post('h_tot_realisasi');
		$v_kd_kassbank1 = $this->input->post('v_kd_kassbank');
		$v_kd_subdivisi1 = $this->input->post('v_kd_subdivisi');
		
		
		//detail realisasi uang muka payment voucher
        $nofaktur = $this->input->post('NoFaktur');
        $sisa = $this->input->post('vSisa');
		$bayar = $this->input->post('vBayar');
		
		$supplier_name = $this->realisasi_uang_muka_supplier_model->getSupplierName($v_supplier);
		$nama_supplier = $supplier_name->Nama;
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        list($thn,$bln,$tgl) = explode("-",$mylib->ubah_tanggal($v_tgl_dokumen));
		//$thn = substr($mylib->ubah_tanggal($v_tgl_dokumen), 2, 2);
		$this->db->trans_begin();
		
        if ($flag == "add")
		{
			//generate number
        	$v_no_dokumen = $mylib->get_code_counter2($this->db->database, "realisasi_uang_muka_supplier","NoDokumen", "RUS", $bln, $thn);
			
			//insert ke tabel realisasi_uang_muka
			$this->realisasi_uang_muka_supplier_model->insertHeaderRealisasi($v_no_dokumen,$mylib->ubah_tanggal($v_tgl_dokumen),$v_supplier,$v_no_referensi,$v_kdkasbank,$v_tot_realisasi,0,$v_note,$user,$vpph,$vpembulatan);
									
			//insert ke tabel realisasi_uang_muka_detail
			//$notedn = 'Uang Muka '.$supplier_name;
			$notedn = 'Realisasi Uang Muka '.$nama_supplier;
			for ($x = 0; $x < count($v_no_um1); $x++) 
			{
				$v_no_um = $v_no_um1[$x];
				$v_no_pv = $v_no_pv1[$x];
				$v_kd_subdivisi = $v_kd_subdivisi1[$x];
				$v_kd_kassbank = $v_kd_kassbank1[$x];
				$v_jml_um = $v_jml_um1[$x];
				$v_realisasi = $v_realisasi1[$x];
				
				//insert detail realisasi_uang_muka
				$this->realisasi_uang_muka_supplier_model->insertDetailRealisasi($v_no_dokumen,$v_no_um,$v_no_pv,$v_realisasi);
									
				//update sisa di uang muka
				$this->realisasi_uang_muka_supplier_model->updateUangMuka($v_no_um, $v_realisasi);	
				if($v_realisasi>0){
                    $notedn .= ' '.$v_no_um.' ';
                }
				
				//--------------------------------------------------------------------------------------------					
			    //hitung selisih
			    if($v_jml_um > $v_realisasi && $v_realisasi>0){
			    	//penerimaan receipt voucher
					$selisih = $v_jml_um - $v_realisasi;
					
					//nomor Receipt Voucher
					$kd_nor = $this->realisasi_uang_muka_supplier_model->getKodeBank($v_kdkasbank);
				    $interface = $this->realisasi_uang_muka_supplier_model->getInterface();
				
				    $No_RV = $this->get_no_counter( $kd_nor->KdPenerimaan,"trans_receipt_header", "NoDokumen",$thn, $bln);
		    
		    		$this->realisasi_uang_muka_supplier_model->insertreceiptheader($No_RV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,"Pengembalian Uang Muka ".$v_no_um,$user,$selisih,$nama_supplier,$v_kd_kassbank);
					$this->realisasi_uang_muka_supplier_model->insertreceiptdetail($No_RV,$mylib->ubah_tanggal($v_tgl_dokumen),$selisih,$x,"Pengembalian Uang Muka ".$v_no_um,$user,$interface['UMPembelian'],"25");					
																			
				}
				    					
			}
			
			for ($y = 0; $y < count($nofaktur); $y++) 
			{
				$vnofaktur = $nofaktur[$y];
				$vsisa = $sisa[$y];
				$vbayar = $bayar[$y];
				
				//insert realisasi_uang_muka_payment
				if($vbayar>0){
					$rekhutang = $this->realisasi_uang_muka_supplier_model->getRekFaktur($vnofaktur);	
					$this->realisasi_uang_muka_supplier_model->insertNewDetailPayment($v_no_dokumen,$vnofaktur,$vsisa,$vbayar);	
				}
				
			}
		}
		
		$this->GenerateDN($mylib->ubah_tanggal($v_tgl_dokumen),$v_supplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa, $v_no_dokumen, $vpph, $vpembulatan,$rekhutang );
		
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		}else{
		    $this->db->trans_commit();
		}
		$this->session->set_flashdata('msg', array('message' => 'Proses Add <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        redirect('/keuangan/realisasi_uang_muka_supplier/');
    }
	    
	function GenerateDN($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa, $v_no_dokumen, $vpph, $vpembulatan, $rekhutang){
		$mylib = new globallib();
		$interface = $this->realisasi_uang_muka_supplier_model->getInterface();
		
		list($thn, $bln, $tgl ) = explode('-', $vtgl);
		
		$user = $this->session->userdata('username');
		$dnno = $mylib->get_code_counter($this->db->database, "debitnote","dnno", "DN", $bln, $thn); 
		$dnallocationno = $mylib->get_code_counter($this->db->database, "dnallocation","dnallocationno", "DA", $bln, $thn); 
		
		$this->realisasi_uang_muka_supplier_model->insertdebitnote($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa,$dnno , $vpph, $vpembulatan ,$interface,$rekhutang,$v_no_dokumen);
		$this->realisasi_uang_muka_supplier_model->inserthutang($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa,$dnno,$interface,$rekhutang);
		$this->realisasi_uang_muka_supplier_model->insertallocation($vtgl, $kdsupplier, $notedn,$v_tot_realisasi, $nofaktur, $bayar, $sisa,$dnno,$dnallocationno,$v_no_dokumen);
	}    
	
    function getData(){
    	$mylib = new globallib();     
    	$var = $this->input->post('data');
    	$tgl= $var[1];
    	$kdsupplier = $var[0];
    	
     	$data['data'] = $this->realisasi_uang_muka_supplier_model->getDataUangMuka($kdsupplier);
     	$data['row'] = $this->realisasi_uang_muka_supplier_model->getDataFaktur($kdsupplier, $mylib->ubah_tanggal($tgl));
    	$this->load->view('transaksi/keuangan/realisasi_uang_muka_supplier/realisasi_uang_muka_supplier_detail', $data);
     
      
    }
	
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
    	$thn -= 2000;
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
		
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;	
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }
    
	function cari_supplier(){
		$supplier = $this->input->post('supplier');
	    $query = $this->realisasi_uang_muka_supplier_model->getSupplier($supplier);
	     	     
	    echo "<option value=''> -- Pilih Supplier --</option>";
	    foreach ($query as $rec) {
			echo "<option value=$rec[KdSupplier]>$rec[Nama]</option>";
		}   
	}
    
    function test(){
    	$col_primary_ok='NH17100003';
    	$thn = '17';
    	$bln = '10';
    	$kode='NH';
    	
		$counter = (substr($col_primary_ok, 7, 4) * 1) + 1;	
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        echo substr($col_primary_ok, 7, 4).'-'.$counter_fa;
	}
	
	function create_pdf() {
        $id = $this->uri->segment(4);
        
        $getHeader = $this->realisasi_uang_muka_supplier_model->getListRealisasiUangMukaPdf($id);
        /*$data['detail_realisasi'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiDetail($id);
		$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiDetailPVPdf($id);
		*/
		
            $data['detail_realisasi'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiDetail($id);
			$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_supplier_model->getListRealisasiDetailPV($id);
			
			$data['row'] = $this->realisasi_uang_muka_supplier_model->getDataFakturView($getHeader->KdSupplier, $getHeader->TglDokumen, $getHeader->NoDokumen);
			
			
        $user = $this->session->userdata('username');
		$data['totbayar']= $this->realisasi_uang_muka_supplier_model->getTotalBayar($id);
		
		$data['results'] = $getHeader;
        $html = $this->load->view('transaksi/keuangan/realisasi_uang_muka_supplier/pdf_realisasi_uang_muka_supplier', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_rum.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }
}

?>