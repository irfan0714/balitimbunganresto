<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class konf_selisih_bank extends authcontroller {

	//put your code here
	function __construct()
	{
		parent::__construct();
		$this->load->library('globallib');
		$this->load->model('transaksi/keuangan/selisih_model');
	}

	function index()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("all");
		if($sign == "Y"){
			$segs = $this->uri->segment_array();
			$arr  = "index.php/" . $segs[1] . "/" . $segs[2] . "/";
			$data['link'] = $mylib->restrictLink($arr);
			$id   = addslashes(trim($this->input->post('stSearchingKey')));
			$id2  = $this->input->post('date1');
			$with = $this->input->post('searchby');
			if($with == "TglTransaksi"){
				$id = $mylib->ubah_tanggal($id2);
			}
			$this->load->library('pagination');

			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</div>';
			$config['cur_tag_open'] = '<span class="current">';
			$config['cur_tag_close'] = '</span>';
			$config['per_page'] = '10';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['num_links'] = 2;
			$config['base_url'] = base_url() . 'index.php/keuangan/komisi/index/';
			$page = $this->uri->segment(4);
			$config['uri_segment'] = 4;
			$flag1 = "";
			if($with != ""){
				if($id != "" && $with != ""){
					$config['base_url'] = base_url() . 'index.php/keuangan/komisi/index/' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
				else
				{
					$page = "";
				}
			}
			else
			{
				if($this->uri->segment(5) != ""){
					$with = $this->uri->segment(4);
					$id   = $this->uri->segment(5);
					if($with == "TglTransaksi"){
						$id = $mylib->ubah_tanggal($id);
					}
					$config['base_url'] = base_url() . 'index.php/keuangan/komisi/index/' . $with . "/" . $id . "/";
					$page = $this->uri->segment(6);
					$config['uri_segment'] = 6;
				}
			}
			$data['header'] = array("No Transaksi","Tanggal","Nama","Keterangan","Total","Kd Agent");
			$config['total_rows'] = $this->selisih_model->num_komisi_row(addslashes($id), $with);
			$data['data'] = $this->selisih_model->getKomisiList($config['per_page'], $page, addslashes($id), $with);
			$data['track'] = $mylib->print_track();

            $data['judul'] = "Konfirm Selisih Bank";
            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);

            $this->pagination->initialize($config);
			$this->load->view('transaksi/keuangan/selisih/list', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	function add_new()
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("add");
		if($sign == "Y"){
			$aplikasi = $this->selisih_model->getDate();
			//			$data['mperusahaan'] = $this->retur_barangmodel->getPerusahaan();
			$data['aplikasi'] = $aplikasi;

            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
			$data['track'] = $mylib->print_track();
            $data['v_start_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['v_end_date'] = date('d') . '-' . $bulan . '-' . $tahun;
            $data['tr1'] = $this->uri->segment(1);
            $data['tr2'] = $this->uri->segment(2);

            $bulan = $this->session->userdata('bulanaktif');
            $tahun = $this->session->userdata('tahunaktif');
            $data['judul'] = "Add Konfirm Selisih Bank";
			$this->load->view('transaksi/keuangan/selisih/add', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	function save_new_komisi()
	{
		//print_r($_POST);die();
		$mylib       = new globallib();
		$user        = $this->session->userdata('userid');
		$flag        = $this->input->post('flag');
		$no          = $this->input->post('nodok');
		$tgl         = $this->input->post('tgl');
		$nama        = $this->input->post('namak');
		$kdagent     = $this->input->post('kdagent');
		$ket         = trim(strtoupper(addslashes($this->input->post('ket'))));
		$total       = $this->input->post('total');
		$totaljual   = $this->input->post('totjual');
		$nostruk1    = $this->input->post('nostruk');
		$tgljual1    = $this->input->post('tgljual');
		$pcode1      = $this->input->post('pcode');
		$harga1      = $this->input->post('harga');
		$qty1        = $this->input->post('qty');
		$persentase1 = $this->input->post('persentase');
		$pcodesave1  = $this->input->post('savepcode');
		$waktu       = date("h:i:s");//$this->input->post('waktu');
		if($no == ""){
			//insert new header
			$no = $this->insertNewHeader($flag, $nama, $tgl, $kdagent, $ket, $total, $user,$waktu,$totaljual);
			//insert new detail
			for($x = 0; $x < count($pcode1); $x++){
				$pcode      = strtoupper(addslashes(trim($pcode1[$x])));
				$nostruk    = trim($nostruk1[$x]);
				$tgljual    = trim($tgljual1[$x]);
				$harga      = trim($harga1[$x]);
				$qty        = trim($qty1[$x]);
				$persentase = trim($persentase1[$x]);
				$pcodesave  = $pcodesave1[$x];
				if($pcode != ""){
					$this->insertNewDetail($no, $nostruk, $tgljual, $pcode, $qty, $persentase, $harga, $user);
				}
			}
		}
		else
		{
			$this->updateHeader($flag, $no, $nobukti, $ket, $total, $tgl, $user);
			for($x = 0; $x < count($pcode1); $x++){
				$pcode     = strtoupper(addslashes(trim($pcode1[$x])));
				$ketdet    = trim($ketdet1[$x]);
				$qty       = trim($qty1[$x]);
				$pcodesave = $pcodesave1[$x];
				//if ($pcode != "") {
				$this->updateDetail($flag, $no, $pcode, $ketdet, $qty, $tgl, $user);
				//}
			}
		}
		redirect('/keuangan/komisi/');
	}

	function insertNewHeader($flag, $nama, $tgl, $kdagent, $ket, $total, $user,$waktu,$totaljual)
	{
		//$this->selisih_model->locktables('keuangan_pvheader');
		$mylib = new globallib();
		$bulan = substr($tgl, 3, 2);
		$tahun = substr($tgl, - 4);
		$new_no= $this->selisih_model->getNewNo($tahun,$bulan);
		$no    = $new_no->NoKomisi;
		$this->db->update('counter', array("NoKomisi"=> $new_no->NoKomisi + 1), array("Tahun"=> $tahun,"Bulan"=> $bulan));
		$data = array(
			'NoTransaksi' => $no,
			'TglTransaksi'=> $mylib->ubah_tanggal($tgl),
			'KdAgent'     => $kdagent,
			'Nama'        => $nama,
			'Keterangan'  => $ket,
			'Total'       => $total,
			'AddDate'     => date("Y-m-d"),
			'AddUser'     => $user,
			'Waktu'       => $waktu,
			'TotalSales'  => $totaljual
		);
		$this->db->insert('finance_komisi_header', $data);
		//$this->selisih_model->unlocktables();
		return $no;
	}

	function updateHeader($flag, $no, $nobukti, $ket, $total, $tgl, $user)
	{
		// $tgl = $this->session->userdata('Tanggal_Trans');
		// $this->selisih_model->locktables('keuangan_pvheader,keuangan_pvdetail');
		$data = array(
			'NoTransaksi'=> $no,
			'NoBukti'    => $nobukti,
			'Keterangan' => $ket,
			'Total'      => $total,
			'EditDate'   => date("Y-m-d"),
			'EditUser'   => $user
		);
		if($flag == "edit"){
			$data['EditDate'] = date("Y-m-d");
			$data['EditUser'] = $user;
			$this->db->update('finance_komisi_detail', array('EditDate'=> date("Y-m-d"),'EditUser'=> $user), array('NoTransaksi'=> $no));
		}
		$this->db->update('finance_komisi_header', $data, array('NoTransaksi'=> $no));
		$this->selisih_model->unlocktables();
	}

	function insertNewDetail($no, $nostruk, $tgljual, $pcode, $qty, $persentase, $harga, $user)
	{
		$data = array(
			'NoTransaksi'=> $no,
			'PCode'      => $pcode,
			'NoStruk'    => $nostruk,
			'TglJual'    => $tgljual,
			'Qty'        => $qty,
			'Harga'      => $harga,
			'Persentase' => $persentase,
			'AddDate'    => date("Y-m-d"),
			'AddUser'    => $user
		);
		$this->db->insert('finance_komisi_detail', $data);
		//$this->selisih_model->unlocktables();
		return $no;
	}

	//    function updateDetail($flag, $no, $pcode, $ketdet, $qty, $tgl, $user) {
	//        $data = array(
	//            'NoTransaksi' => $no,
	//            'KdRekening' => $pcode,
	//            'Ket' => $ketdet,
	//            'Jumlah' => $qty,
	//            'EditDate' => date("Y - m - d"),
	//            'EditUser' => $user
	//        );
	//        $this->db->update('keuangan_pvdetail', $data, array('NoTransaksi' => $no, 'KdRekening' => $pcode));
	//        //$this->selisih_model->unlocktables();
	//        return $no;
	//    }

	function getlist()
	{
//        print_r($_POST);
        $tgl1   = $this->input->post('tgl1');
        $tgl2   = $this->input->post('tgl2');
		$pil    = $this->input->post('pil');


		// cek sudah ambil belum tgl dan struk
		//$cek = $this->selisih_model->($field,$tgl2);
		//echo "<pre>";print_r($cek);echo "</pre>";
		$struk ="";
		$cek ="";
		if (count($cek) > 0){ //jika nostruk udah ada di komisi detail..buat transaksi baru & masukan nostruk yg belum ada
            $where = "";
		}else{
            $where = "";
		}
		
		//$where = "";
		$detail= $this->selisih_model->getListData($where);
        //print_r($detail);
		$nilai = "";
		for($a = 0; $a < count($detail); $a++){
			$nilai .= $detail[$a]['Tanggal'] . "||" . $detail[$a]['NoStruk'] . "||" . $detail[$a]['KKredit'] . "||" . $detail[$a]['KDebit'] . "**";
		}
		echo count($detail) . "##" . $nilai;


	}

	function getPCode()
	{
		$kode   = $this->input->post('pcode');
		$name   = $this->input->post('nama');
		$tgl    = $this->input->post('tgl');
		$bulan  = substr($tgl, 3, 2);
		$tahun  = substr($tgl, 6, 4);
		//$fieldmasuk = "QtyMasuk" . $bulan;
		//$fieldakhir = "QtyAkhir" . $bulan;
		//$fieldkeluar = "QtyKeluar" . $bulan;
		$detail = $this->selisih_model->getPCodeDet($kode, $name);
		//                print_r($detail);
		if(!empty($detail)){
			$nilai = $detail->NamaRekening . "*&^%" . $detail->KdRekening; // . "*&^%" . $detail->HargaBeliAkhir;
		}
		else
		{
			$nilai = "";
		}
		echo $nilai;
	}

	function getRealPCode()
	{
		$kode = $this->input->post('pcode');
		if(strlen($kode) == 13){
			$mylib = new globallib();
			$hasil = $mylib->findBarcode($kode);
			print_r($hasil);
			die();
			$pcode_hasil = $hasil['nilai'];
			if(count($pcode_hasil) != 0){
				$pcode = $pcode_hasil[0]['PCode'];
			}
			else
			{
				$pcode = "";
			}
		}
		else
		{
			$valpcode = $this->selisih_model->ifPCodeBarcode($kode);
			if(count($valpcode) != 0){
				$pcode = $valpcode->KdRekening;
			}
			else
			{
				$pcode = "";
			}
		}
		echo $pcode;
	}

	function edit_komisi($id)
	{
		$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
		if($sign == "Y"){
			$id = $this->uri->segment(4);
			$data['header'] = $this->selisih_model->getHeader($id);
			$data['detail'] = $this->selisih_model->getDetail($id);

			$this->load->view('keuangan/komisi/edit_komisi', $data);
		}
		else
		{
			$this->load->view('denied');
		}
	}

	//    function delete_komisi() {
	//        $mylib = new globallib();
	//        $id = $this->input->post('kode');
	//        //$header = $this->retur_barangmodel->getSumber($id);
	//        $user = $this->session->userdata('userid');
	//        $tgl2 = $this->session->userdata('Tanggal_Trans');
	//        $tgl = $mylib->ubah_tanggal($tgl2);
	//        //$getHeader = $this->retur_barangmodel->getHeader($id);
	//        $getDetail = $this->selisih_model->getDetail($id);
	//        $tahun = substr($getHeader->TglTransaksi, 6, 4);
	//        $lastNo = $this->selisih_model->getNewNo($tahun);
	//        $NoDelete = $id;
	//        $pcode1 = $this->input->post('pcode');
	//        $qty1 = $this->input->post('qty');
	//        $pcodesave1 = $this->input->post('savepcode');
	//
	//        if ((int) $lastNo->NoPaymentv == (int) $NoDelete + 1) {
	//            $this->db->update("setup_no", array("NoPaymentv" => $NoDelete[1]), array("Tahun" => $tahun));
	//        }
	//        $this->selisih_model->locktables('keuangan_pvheader,keuangan_pvdetail');
	//
	//        for ($x = 0; $x < count($pcode1); $x++) {
	//            $pcode = strtoupper(addslashes(trim($pcode1[$x])));
	//            $qty = trim($qty1[$x]);
	//            $pcodesave = $pcodesave1[$x];
	//        }
	//
	//        $this->db->delete('keuangan_pvheader', array('NoTransaksi' => $id));
	//        $this->db->delete('keuangan_pvdetail', array('NoTransaksi' => $id));
	//        $this->selisih_model->unlocktables();
	//    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_komisi', $data);
	}

	function versistruk()
	{
		$data       = $this->varCetak();
		$no         = $this->uri->segment(4);
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip         = "192.168.0.75";
		$printer    = $this->selisih_model->NamaPrinter($ip);
		//print_r($printer); die();
		//        print_r($_SERVER['REMOTE_ADDR']);
		//        die();
		$data['ip'] = $printer[0]['ip'];
		$data['nm_printer'] = $printer[0]['nm_printer'];
		$data['store'] = $this->selisih_model->aplikasi();
		$data['header'] = $this->selisih_model->getHeader($no);
		$data['detail'] = $this->selisih_model->getHeaderForPrint($no);
		/*$dtl = $this->selisih_model->getHeaderForPrint($no);
		echo "<pre>";print_r($dtl);echo "</pre>";
		$tes = $dtl['KdAgent'];
		echo $tes;die();
		$data['totsales'] = $this->selisih_model->getTotalSales($stiker,$tgl);*/

		if(!empty($data['header'])){
			//$this->load->view('proses / cetak_tutup',$data); // jika untuk tes
			$this->load->view('transaksi/keuangan/komisi/cetak_strukkomisi', $data); // jika ada printernya
		}
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id   = $this->uri->segment(4);
		$data['fileName2'] = "lainlain.sss";
		$data['fontstyle'] = chr(27) . chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['pindah_hal'] = "\r\n\r\n\r\n\r\n\r\n" . chr(27) . chr(48) . "\r\n" . chr(27) . chr(50);
		$data['string1'] = "     Dibuat Oleh,                     Disetujui Oleh,";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_lain', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib       = new globallib();
		$printreport = new printreportlib();
		$id          = $this->uri->segment(4);
		$header      = $this->selisih_model->getHeader($id);
		$data['header'] = $header;
		$detail = $this->selisih_model->getDetailForPrint($id);
		$data['judul1'] = array("NoTransaksi","Tanggal","Keterangan","Sticker","Total Sales","Total Komisi");
		$data['niljudul1'] = array($header->NoTransaksi,$header->TglTransaksi,stripslashes($header->Keterangan),$header->KdAgent,number_format($header->TotalSales,0,'','.'),number_format($header->Total,0,'','.'));
		$data['judul2'] = "";
		$data['niljudul2'] = "";
		$data['judullap'] = "Pembayaran Komisi";
		$data['colspan_line'] = 4;
		$data['url'] = "komisi/printThis/" . $id;
		$data['url2'] = "komisi/versistruk/" . $id;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("PCode", "Nama Barang", "Qty", "Harga");
		$data['panjang_kertas'] = 30;
		$jmlh_baris_lain = 19;
		$data['panjang_per_hal'] = (int) $data['panjang_kertas'] - (int) $jmlh_baris_lain;
		$jml_baris_detail = count($detail) + $this->selisih_model->getCountDetail($id);
		if($data['panjang_per_hal'] == 0){
			$data['tot_hal'] = 1;
		}
		else
		{
			$data['tot_hal'] = 1;//ceil((int) $jml_baris_detail / (int) $data['panjang_per_hal']);
		}
		$list_detail = array();
		$detail_attr = array();
		$list_detail_attr = array();
		$detail_page = array();
		$new_array = array();
		$counterBaris = 0;
		$counterRow   = 0;
		$max_field_len= array(0,0,0);
		$sum_netto = 0;
		//                print_r($detail);
		  for ($m = 0; $m < count($detail); $m++) {
		//			$attr = $this->selisih_model->getDetailAttrCetak($id,$detail[$m]['PCode'],$detail[$m]['Counter']);
		unset($list_detail);
		$counterRow++;
		$list_detail[] = stripslashes($detail[$m]['PCode']);
		$list_detail[] = stripslashes($detail[$m]['NamaLengkap']);
		$list_detail[] = stripslashes($detail[$m]['Qty']);
		$list_detail[] = number_format($detail[$m]['Harga'], 0, '', '.');
		$detail_page[] = $list_detail;
		$max_field_len = $printreport->get_max_field_len($max_field_len, $list_detail);
		if ($data['panjang_per_hal'] != 0) {
		if (((int) $m + 1) % $data['panjang_per_hal'] == 0) {
		$data['detail'][] = $detail_page;
		if ($m != count($detail) - 1) {
		unset($detail_page);
		}
		}
		}
		$netto = $detail[$m]['Harga'];
		$sum_netto = $sum_netto + ($netto);
		}
		$data['judul_netto'] = array("Total");
		$data['isi_netto'] = array(number_format($sum_netto, 0, '', '.'));
		$data['detail'][] = $detail_page;
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}

}

?>
