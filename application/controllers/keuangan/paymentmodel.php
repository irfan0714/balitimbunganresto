<?php
class Paymentmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getpaymentList($num,$offset,$id,$with,$thnbln)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="where h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
    	$sql = "select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,NoBukti,Keterangan,JumlahPayment,NamaKasBank,NamaCostCenter,NamaPersonal,UserName,h.Jenis,NoGiro,KdBankCair
				from trans_payment_header h 
				left join kasbank on kasbank.KdKasBank = h.KdKasBank
				left join costcenter on costcenter.KdCostCenter = h.KdCostCenter
				left join personal on personal.KdPersonal = h.KdPersonal
				left join user on user.id = h.AddUser
				$clause 
				order by TglDokumen desc Limit $offset,$num
			";
		return $this->getArrayResult($sql);
    }
    
    function num_payment_row($id,$with,$thnbln){
     	$clause="where Status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
		$sql = "SELECT NoDokumen FROM trans_payment_header $clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans,DefaultKdDepartemen,DefaultKdProject,DefaultKdCostCenter from aplikasi";
        return $this->getRow($sql);
    }
	function getKasBank(){
    	$sql = "SELECT KdKasBank,NamaKasBank from kasbank order by KdKasBank";
		return $this->getArrayResult($sql);
    }
	function getPersonal(){
    	$sql = "SELECT KdPersonal,NamaPersonal from personal order by KdPersonal";
		return $this->getArrayResult($sql);
    }
	function getCostCenter(){
		$sql = "SELECT KdCostCenter,NamaCostCenter FROM costcenter order by KdCostCenter";
		return $this->getArrayResult($sql);
	}
	function cekgiro($nogiro){
    	$sql = "SELECT * from bukugiro where NoGiro='$nogiro'";
		return $this->getArrayResult($sql);
    }
    function findrekening($id){
		$sql = "SELECT kdrekening,namarekening FROM rekening Where kdrekening='$id' and tingkat='3'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getNewNo($tgl)
	{
		$tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoPayment=NoPayment+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoPayment FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	function getGiro($id)
	{
		$sql = "select Status from bukugiro Where NoTransaksi='$id'";
        return $this->getArrayResult($sql);
	}
	function getDetail($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,NamaRekening,Urutan from(
		SELECT * from trans_payment_detail Where NoDokumen='$id' and Status<>'B'
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,NamaRekening,Urutan from(
		SELECT * from trans_payment_detail Where NoDokumen='$id' and Status<>'B'
		order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
		$sql = "select NoDokumen,Tanggal,NamaKasBank,NamaCostCenter,NamaPersonal,NoBukti,Keterangan,h.KdKasBank,h.KdCostCenter,h.KdPersonal,JumlahPayment,Jenis,NoGiro,KdBankCair,TglCair
from(
SELECT NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,NoBukti,Keterangan,
KdKasBank,KdCostCenter,KdPersonal,JumlahPayment,Jenis,NoGiro,KdBankCair,date_format(TglCair,'%d-%m-%Y') as TglCair
from trans_payment_header where NoDokumen='$id' and Status<>'B')h
left JOIN
(
select KdKasBank,NamaKasBank from kasbank
)kasbank
on kasbank.KdKasBank = h.KdKasBank
left JOIN
(
select KdCostCenter,NamaCostCenter from costcenter
)costcenter
on costcenter.KdCostCenter = h.KdCostCenter
left JOIN
(
select KdPersonal,NamaPersonal from personal
)personal
on personal.KdPersonal = h.KdPersonal;";
        return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>