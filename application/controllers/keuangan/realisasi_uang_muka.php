<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class Realisasi_uang_muka extends authcontroller {

    function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->library('globallib');
        $this->load->model('globalmodel');
        $this->load->model('transaksi/keuangan/realisasi_uang_muka_model');
        $this->load->model('transaksi/keuangan/uang_muka_model');
        $this->load->model('proses/posting_model');
    }

    function index() 
    {
        $mylib = new globallib();
        $sign = $mylib->getAllowList("all");
        if ($sign == "Y") {
        	
        	$id = $this->uri->segment(4);
            $user = $this->session->userdata('username');
			
            $data["search_keyword"] = "";
            $resSearch = "";
            $arr_search["search"] = array();
            $id_search = "";
            if ($id * 1 > 0) {
                $resSearch = $this->globalmodel->getSearch($id, "realisasi_um_karyawan", $user);
                $arrSearch = explode("&", $resSearch->query_string);
				
                $id_search = $resSearch->id;

                if ($id_search) {
                    $search_keyword = explode("=", $arrSearch[0]); // search keyword
                    $arr_search["search"]["keyword"] = $search_keyword[1];

                    $data["search_keyword"] = $search_keyword[1];
                }
            }
        	
        	// pagination
            $this->load->library('pagination');
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['per_page'] = '25';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['num_links'] = 2;
            
            if ($id_search) {
                $config['base_url'] = base_url() . 'index.php/keuangan/realisasi_uang_muka/index/' . $id_search . '/';
                $config['uri_segment'] = 5;
                $page = $this->uri->segment(5);
            } else {
                $config['base_url'] = base_url() . 'index.php/keuangan/realisasi_uang_muka/index/';
                $config['uri_segment'] = 4;
                $page = $this->uri->segment(4);
            }
            
            //$arr_search["search"]=20;
            //$page = 0;
            $config['total_rows'] = $this->realisasi_uang_muka_model->num_uang_muka_row($arr_search["search"]);
            $data['data'] = $this->realisasi_uang_muka_model->getRealisasiUangMukaList($config['per_page'], $page, $arr_search["search"]);
            
            
            $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
			$this->load->view('transaksi/keuangan/realisasi_uang_muka/list_realisasi_uang_muka', $data);
           
        } else {
            $this->load->view('denied');
        }
    }
    
    function search() 
    {
    	$mylib = new globallib();
    	
        $user = $this->session->userdata('username');

        // hapus dulu yah
        $this->db->delete('ci_query', array('module' => 'realisasi_um_karyawan', 'AddUser' => $user));

		$search_value = "";
		$search_value .= "search_keyword=".$mylib->save_char($this->input->post('search_keyword'));
		$data = array(
            'query_string' => $search_value,
            'module' => "realisasi_um_karyawan",
            'AddUser' => $user
        );
		
        $this->db->insert('ci_query', $data);

        $query_id = $this->db->insert_id();

        redirect('/keuangan/realisasi_uang_muka/index/' . $query_id . '');
    } 
  
    
    function add_new() 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            
            $data['KasBank'] = $this->realisasi_uang_muka_model->getKasBank($user);
            $data['employee'] = $this->realisasi_uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->realisasi_uang_muka_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/realisasi_uang_muka/add_realisasi_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
     function edit_realisasi_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="edit";
            $data['header'] = $this->realisasi_uang_muka_model->getListRealisasiUangMuka($id);
            $data['detail_realisasi'] = $this->realisasi_uang_muka_model->getListRealisasiDetail($id);
			$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_model->getListRealisasiDetailPV($id);
			$data['dept'] = $this->realisasi_uang_muka_model->getDept();
			
			$data['PVRV'] = $this->realisasi_uang_muka_model->getNoPayReceipt($id);
			
            $data['KasBank'] = $this->realisasi_uang_muka_model->getKasBank($user);
            $data['employee'] = $this->realisasi_uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->realisasi_uang_muka_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/realisasi_uang_muka/edit_realisasi_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
    
    function jurnal_edit_realisasi_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="edit";
            $data['header'] = $this->realisasi_uang_muka_model->getListRealisasiUangMuka($id);
            $data['detail_realisasi'] = $this->realisasi_uang_muka_model->getListRealisasiDetail($id);
			$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_model->getListRealisasiDetailPV($id);
			$data['dept'] = $this->realisasi_uang_muka_model->getDept();
			
			$data['PVRV'] = $this->realisasi_uang_muka_model->getNoPayReceipt($id);
			
            $data['KasBank'] = $this->realisasi_uang_muka_model->getKasBank($user);
            $data['employee'] = $this->realisasi_uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->realisasi_uang_muka_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/realisasi_uang_muka/jurnal_edit_realisasi_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
	
	
	function view_realisasi_uang_muka($id) 
    {
    	$mylib = new globallib();
        $user = $this->session->userdata('username');
        $userlevel = $this->session->userdata('userlevel');
        $sign = $mylib->getAllowList("add");
        if ($sign == "Y") {
            $data['msg'] = "";
            $data['otorisasi']="view";
            $data['header'] = $this->realisasi_uang_muka_model->getListRealisasiUangMuka($id);
            $data['detail_realisasi'] = $this->realisasi_uang_muka_model->getListRealisasiDetail($id);
			$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_model->getListRealisasiDetailPV($id);
			$data['dept'] = $this->realisasi_uang_muka_model->getDept();
			
            $data['KasBank'] = $this->realisasi_uang_muka_model->getKasBank($user);
            $data['employee'] = $this->realisasi_uang_muka_model->getEmployee();
            $data['subdivisi'] = $this->realisasi_uang_muka_model->getSubDivisi();
           
            $data['track'] = $mylib->print_track();
            $this->load->view('transaksi/keuangan/realisasi_uang_muka/edit_realisasi_uang_muka', $data);
        } else {
            $this->load->view('denied');
        }
    }
    
	
	function save_data() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_kdkasbank= $this->input->post('v_kas_bank');
        $v_no_referensi = $this->input->post('v_no_ref');
        $v_employee = $this->input->post('v_employee');
		$v_note = $this->input->post('v_note');
		
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
		
		//detail realisasi uang muka
		$v_no_um1 = $this->input->post('v_no_um');
		$v_no_pv1 = $this->input->post('v_no_pv');
		$v_jml_um1 = $this->input->post('v_jml_um');
		$v_realisasi1 = $this->input->post('v_realisasi');
		$v_tot_realisasi1 = $this->input->post('v_tot_realisasi');
		$v_kd_kassbank1 = $this->input->post('v_kd_kassbank');
		$v_kd_subdivissi1 = $this->input->post('v_kd_subdivissi');
		
		//detail realisasi uang muka payment voucher
        $kdrekening1 = $this->input->post('kdrekening');
        $jumlah1 = $this->input->post('jumlah');
		$subdivisi1 = $this->input->post('subdivisi');
		$dept1 = $this->input->post('dept');
		$keterangan1 = $this->input->post('keterangan');
		$v_tot_pv1 = $this->input->post('v_tot_pv');
		
		//employee_name
		$employee_name = $this->uang_muka_model->getEmployeeName($v_employee);
		
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        $bln = substr($mylib->ubah_tanggal($v_tgl_dokumen), 5, 2);
		$thn = substr($mylib->ubah_tanggal($v_tgl_dokumen), 2, 2);

        if ($flag == "add")
		{
			//generate number
        	$v_no_dokumen = $mylib->get_code_counter2($this->db->database, "realisasi_uang_muka","NoDokumen", "RUM", $data['bulan'], $data['tahun']);
			
			$this->db->trans_begin();
			
			//insert ke tabel realisasi_uang_muka
			$this->insertNewHeader(
									$v_no_dokumen,
									$mylib->ubah_tanggal($v_tgl_dokumen),
									$v_employee,
									$v_no_referensi,
									$v_kdkasbank,
									$v_tot_realisasi1,
									$v_tot_pv1,
									$v_note,
									$user);
									
			//insert ke tabel realisasi_uang_muka_detail
			for ($x = 0; $x < count($v_no_um1); $x++) 
			{
				$v_no_um = $v_no_um1[$x];
				$v_no_pv = $v_no_pv1[$x];
				$v_kd_subdivissi = $v_kd_subdivissi1[$x];
				$v_kd_kassbank = $v_kd_kassbank1[$x];
				$v_jml_um = $mylib->save_int($v_jml_um1[$x]);
				$v_realisasi = $mylib->save_int($v_realisasi1[$x]);
				if($v_realisasi1[$x]!=''){
					//--------------------------------------------------------------------------------------------					
				    											
				    if($v_jml_um >= $v_realisasi){
				    	//insert detail realisasi_uang_muka
				    	$this->insertNewDetail(
											$v_no_dokumen,
											$v_no_um,
											$v_no_pv,
											$v_realisasi);
											
						//update sisa di uang muka
						$this->updateUangMuka(
										$v_no_um,
										$v_realisasi);	
					
				    	//penerimaan receipt voucher
						$selisih = $v_jml_um - $v_realisasi;
						
						//nomor Receipt Voucher
						$kd_nor = $this->uang_muka_model->getKodeBank($v_kdkasbank);
					    $No_RV = $this->get_no_counter( $kd_nor->KdPenerimaan,"trans_receipt_header", "NoDokumen",$thn, $bln);
			    
			    		$this->insertreceiptheader($No_RV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,"Pengembalian Uang Muka ".$v_no_um,$user,$selisih,$employee_name->employee_name,$v_kd_kassbank);
						$this->insertreceiptdetail($No_RV,$mylib->ubah_tanggal($v_tgl_dokumen),$selisih,$x,"Pengembalian Uang Muka ".$v_no_um,$user,$kd_nor->KdRekening,'25');					
																				
					}else if($v_jml_um < $v_realisasi){
						//insert detail realisasi_uang_muka
				    	$this->insertNewDetail(
											$v_no_dokumen,
											$v_no_um,
											$v_no_pv,
											$v_jml_um);
											
						//update sisa di uang muka
						$this->updateUangMuka(
										$v_no_um,
										$v_jml_um);	
										
						//pengeluaran payment voucher
						$selisih = $v_realisasi - $v_jml_um;
	            		
						//nomor Payment Voucher
						$kd_no = $this->uang_muka_model->getKodeBank($v_kdkasbank);
					    $No_PV = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$thn, $bln);

						//insert uang_muka		
					    $no_um = $mylib->get_code_counter($this->db->database, "uang_muka","NoDokumen", "UM", $bln, 2000+$thn);	
	            		$this->insertNewUangMuka($no_um, $mylib->ubah_tanggal($v_tgl_dokumen), $v_kdkasbank, $v_no_referensi, $v_employee, $selisih, '-', '25', $v_note, $No_PV, $user);	
						
						$this->insertNewDetail(
											$v_no_dokumen,
											$no_um,
											$No_PV,
											$selisih);
						
											
						//insert ke trans_payment_header
						$this->insertpaymentheader($No_PV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$v_note,$user,$selisih,$employee_name->employee_name,$v_kdkasbank, $v_no_dokumen);
						//insert ke trans_payment_detail
						$this->insertpaymentdetail($No_PV,$mylib->ubah_tanggal($v_tgl_dokumen),$v_no_referensi,$selisih,"1","Selisih Pengeluaran Dari ".$v_no_dokumen,$user,$kd_no->KdRekening,$v_kd_subdivissi);
						
					}
				}
				    					
			}
			
			for ($y = 0; $y < count($kdrekening1); $y++) 
			{
				$kdrekening = $kdrekening1[$y];
				$jumlah = $mylib->save_int($jumlah1[$y]);
				$dept = $dept1[$y];
				$subdivisi = $subdivisi1[$y];
				$keterangan = $keterangan1[$y];
				
				//insert realisasi_uang_muka_payment
				$this->insertNewDetailPayment(
									$v_no_dokumen,
									$kdrekening,
									$jumlah,
									$dept,
									$subdivisi,
									$keterangan);															
			}
			
			if ($this->db->trans_status() === FALSE){
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
			}
			$this->session->set_flashdata('msg', array('message' => 'Proses Add <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
		} 
		
		
		else if ($flag == "edit") 
		{
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			
			$this->db->trans_begin();
			//update realisasi_uang_muka
			$this->updateHeader(
									$v_no_dokumen,
									$mylib->ubah_tanggal($v_tgl_dokumen),
									$v_employee,
									$v_no_referensi,
									$v_kdkasbank,
									$v_tot_realisasi1,
									$v_tot_pv1,
									$v_note,
									$user);
									
			//update realisasi_uang_muka_detail
			for ($x = 0; $x < count($v_no_um1); $x++) 
			{
				$v_no_um = $v_no_um1[$x];
				$v_no_pv = $v_no_pv1[$x];
				$v_realisasi = $mylib->save_int($v_realisasi1[$x]);
				
				//insert detail realisasi_uang_muka
				$this->updateDetail(
									$v_no_dokumen,
									$v_no_um,
									$v_no_pv,
									$v_realisasi);
									
				//update sisa di uang muka
				$this->updateUangMuka(
									$v_no_um,
									$v_realisasi);	
			}
			
			//update realisasi_uang_muka_payment
			$this->realisasi_uang_muka_model->DeleteDetailPayment($v_no_dokumen);
			for ($y = 0; $y < count($kdrekening1); $y++) 
			{
				$kdrekening = $kdrekening1[$y];
				$jumlah = $mylib->save_int($jumlah1[$y]);
				$dept = $dept1[$y];
				$subdivisi = $subdivisi1[$y];
				$keterangan = $keterangan1[$y];
				
				//insert realisasi_uang_muka_payment
				$this->insertNewDetailPayment(
									$v_no_dokumen,
									$kdrekening,
									$jumlah,
									$dept,
									$subdivisi,
									$keterangan);
									
																		
			}
			if ($this->db->trans_status() === FALSE){
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
			}
			$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/keuangan/realisasi_uang_muka/edit_realisasi_uang_muka/' . $v_no_dokumen . '');
    }
    
    function save_data_jurnal() 
    {
        //echo "<pre>";print_r($_POST);echo "</pre>";die;
        $mylib = new globallib();
        
        $v_tgl_dokumen = $this->input->post('v_tgl_dokumen');
        $v_kdkasbank= $this->input->post('v_kas_bank');
        $v_no_referensi = $this->input->post('v_no_ref');
        $v_employee = $this->input->post('v_employee');
		$v_note = $this->input->post('v_note');
		
        $flag = $this->input->post('flag');
        $user = $this->session->userdata('username');
        $userid = $this->session->userdata('userid');
		
		//detail realisasi uang muka
		$v_no_um1 = $this->input->post('v_no_um');
		$v_no_pv1 = $this->input->post('v_no_pv');
		$v_jml_um1 = $this->input->post('v_jml_um');
		$v_realisasi1 = $this->input->post('v_realisasi');
		$v_tot_realisasi1 = $this->input->post('v_tot_realisasi');
		$v_kd_kassbank1 = $this->input->post('v_kd_kassbank');
		$v_kd_subdivissi1 = $this->input->post('v_kd_subdivissi');
		
		//detail realisasi uang muka payment voucher
        $kdrekening1 = $this->input->post('kdrekening');
        $jumlah1 = $this->input->post('jumlah');
		$subdivisi1 = $this->input->post('subdivisi');
		$dept1 = $this->input->post('dept');
		$keterangan1 = $this->input->post('keterangan');
		$v_tot_pv1 = $this->input->post('v_tot_pv');
		
		//employee_name
		$employee_name = $this->uang_muka_model->getEmployeeName($v_employee);
		
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        
        $bln = substr($mylib->ubah_tanggal($v_tgl_dokumen), 5, 2);
		$thn = substr($mylib->ubah_tanggal($v_tgl_dokumen), 2, 2);

        if ($flag == "edit") 
		{
			$v_no_dokumen = $this->input->post('v_no_dokumen');
			
			$this->db->trans_begin();
			//update realisasi_uang_muka
			$this->updateHeader(
									$v_no_dokumen,
									$mylib->ubah_tanggal($v_tgl_dokumen),
									$v_employee,
									$v_no_referensi,
									$v_kdkasbank,
									$v_tot_realisasi1,
									$v_tot_pv1,
									$v_note,
									$user);
									
			//update realisasi_uang_muka_detail
			for ($x = 0; $x < count($v_no_um1); $x++) 
			{
				$v_no_um = $v_no_um1[$x];
				$v_no_pv = $v_no_pv1[$x];
				$v_realisasi = $mylib->save_int($v_realisasi1[$x]);
				
				//insert detail realisasi_uang_muka
				$this->updateDetail(
									$v_no_dokumen,
									$v_no_um,
									$v_no_pv,
									$v_realisasi);
									
				//update sisa di uang muka
				$this->updateUangMuka(
									$v_no_um,
									$v_realisasi);	
			}
			
			//update realisasi_uang_muka_payment
			$this->realisasi_uang_muka_model->DeleteDetailPayment($v_no_dokumen);
			for ($y = 0; $y < count($kdrekening1); $y++) 
			{
				$kdrekening = $kdrekening1[$y];
				$jumlah = $mylib->save_int($jumlah1[$y]);
				$dept = $dept1[$y];
				$subdivisi = $subdivisi1[$y];
				$keterangan = $keterangan1[$y];
				
				//insert realisasi_uang_muka_payment
				$this->insertNewDetailPayment(
									$v_no_dokumen,
									$kdrekening,
									$jumlah,
									$dept,
									$subdivisi,
									$keterangan);
									
																		
			}
			if ($this->db->trans_status() === FALSE){
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
			}
			
			//posting jurnal juga
			$tglx = $mylib->ubah_tanggal($v_tgl_dokumen);
            list($tahuny, $bulany, $tgy) = explode('-', $tglx);
			$noreferensi = $this->realisasi_uang_muka_model->getReferensi($v_no_dokumen);
			$this->posting_realisasi_um($tahuny,$bulany,$userid,$v_no_dokumen,$noreferensi->NoReferensi);
			
			$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $v_no_dokumen . '</strong> berhasil', 'class' => 'success'));
        }

        
        redirect('/keuangan/realisasi_uang_muka/jurnal_edit_realisasi_uang_muka/' . $v_no_dokumen . '');
    }
    
    function insertpaymentheader($nopv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank, $uangmuka)
	{
		
		$data = array(
			'NoDokumen'	    => $nopv,
			'TglDokumen'    => $tgl,
			'Jenis'         => '1',
			'Penerima'      => $Penerima,
			'KdKasBank' => $KdKasBank,
			'KdCostCenter' => '0',
			'KdPersonal' => '0',
			'NoGiro' => ' ',
			'KdBankCair' => ' ',
			'TglCair' => ' ',
			'NoBukti' => $NoBukti,
			'Keterangan' => 'Realisasi Uang Muka No. '.$uangmuka,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahPayment' => $jumlah,
			'Status' => '1'
		);
		$this->db->insert('trans_payment_header', $data);
		
	}
	
	function insertpaymentdetail($NoDokumen,$tgl,$Noref,$jumlah,$counter,$keterangan,$user,$norek,$subdivisi)
	{
		$interface = $this->realisasi_uang_muka_model->getinterface();
		
		$data = array(
			'NoDokumen'	=> $NoDokumen,
			'TglDokumen' => $tgl,
			'KdRekening' => $interface['UMKaryawan'],
			'Jumlah' => $jumlah,
            'KdSubdivisi'     => $subdivisi,
            'KdDepartemen'    => '00',
			'Keterangan' => $keterangan,
			'Urutan'	=> $counter,
			'NoBukti' => $Noref,
			'Status' => '1',
			'AddDate' => date('Y-m-d'),
			'AddUser' => $user
		);
		$this->db->insert('trans_payment_detail', $data);
		
		
	}
	
	//receipt voucher header
	function insertreceiptheader($norv,$tgl,$NoBukti,$ket,$user,$jumlah,$Penerima,$KdKasBank)
	{
		$data = array(
			'NoDokumen'	=> $norv,
			'TglDokumen' => $tgl,
			'Jenis' => 1,
			'KdKasBank' => $KdKasBank,
            'TerimaDari'      => $Penerima,
			'KdCostCenter' => 0,
			'KdPersonal' => 0,
			'NoGiro' => "",
			'KdBankCair' => "",
			'TglCair' => "",
			'NoBukti' => $NoBukti,
			'Keterangan' => $ket,
			'AddDate'    => date('Y-m-d'),
			'AddUser'    => $user,
			'JumlahReceipt' => $jumlah,
			'Status' => ' '
		);
		$this->db->insert('trans_receipt_header', $data);
		
	}
	
	function insertreceiptdetail($NoDokumen,$tgl,$jumlah,$counter,$keterangan,$user,$norek,$subdivisi)
	{
		$interface = $this->realisasi_uang_muka_model->getinterface();
		$data = array(
				'NoDokumen'	    => $NoDokumen,
				'TglDokumen'    => $tgl,
				'KdRekening'    => $interface['UMKaryawan'],
				'Jumlah'        => $jumlah,
                'KdSubdivisi'   => $subdivisi,
                'KdDepartemen'  => "",
				'Keterangan'    => $keterangan,
				'Urutan'	    => $counter,
				'Status'        => "",
				'AddDate'		=> date('Y-m-d'),
				'AddUser'		=> $user
			);
		$this->db->insert('trans_receipt_detail', $data);
		
		
	}
    
    function insertNewHeader($v_no_dokumen,$v_tgl_dokumen,$v_employee,$v_no_referensi,$v_kdkasbank,$v_jumlah,$v_tot_pv1,$v_note,$user)
	{
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'TglDokumen'=>$v_tgl_dokumen,
			 'Employee' => $v_employee,
			 'NoReferensi' => $v_no_referensi,
             'KdKasBank' => $v_kdkasbank,
             'JmlRealisasi' => $v_jumlah,
             'JmlPV' => $v_tot_pv1,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
			);
		$this->db->insert('realisasi_uang_muka', $data);
		
	}
	
	function updateHeader($v_no_dokumen,$v_tgl_dokumen,$v_employee,$v_no_referensi,$v_kdkasbank,$v_jumlah,$v_tot_pv1,$v_note,$user)
	{	
		$data = array(
			 'TglDokumen'=>$v_tgl_dokumen,
			 'Employee' => $v_employee,
			 'NoReferensi' => $v_no_referensi,
             'KdKasBank' => $v_kdkasbank,
             'JmlRealisasi' => $v_jumlah,
             'JmlPV'=> $v_tot_pv1,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
			);
		$where = array('NoDokumen'=>$v_no_dokumen);
		$this->db->update('realisasi_uang_muka', $data, $where);
		
	}
	
	function insertNewDetail($v_no_dokumen,$v_no_um,$v_no_pv,$v_realisasi)
	{	
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'NoUM'=>$v_no_um,
			 'NoPV' => $v_no_pv,
			 'Realisasi' => $v_realisasi
			);	
		$this->db->insert('realisasi_uang_muka_detail', $data);
		
	}
	
	function updateDetail($v_no_dokumen,$v_no_um,$v_no_pv,$v_realisasi)
	{	
		$data = array(
             'NoUM'=>$v_no_um,
			 'NoPV' => $v_no_pv,
			 'Realisasi' => $v_realisasi
			);
		$where = array('NoDokumen'=>$v_no_dokumen, 'NoUM'=>$v_no_um);	
		$this->db->update('realisasi_uang_muka_detail', $data, $where);
		
	}
	
	function updateUangMuka($v_no_um,$v_realisasi)
	{	
		//echo 'real '.$v_realisasi.' sasi '.$v_no_um;
		if($v_realisasi>0){
			$sql = "update uang_muka set Sisa=0 where NoDokumen='$v_no_um' ";
			$this->db->query($sql);
		}
	}
	
	function insertNewDetailPayment($v_no_dokumen,$kdrekening,$jumlah,$dept,$subdivisi,$keterangan)
	{	
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'KdRekening'=>$kdrekening,
			 'Jumlah' => $jumlah,
			 'KdDepartemen' => $dept,
			 'KdSubDivisi' => $subdivisi,
			 'keterangan' => $keterangan
			);
			
		$this->db->insert('realisasi_uang_muka_payment', $data);
		
	}
	
	function updateDetailPayment($v_no_dokumen,$kdrekening,$jumlah,$dept,$subdivisi,$keterangan)
	{	
		$data = array(
             'KdRekening'=>$kdrekening,
			 'Jumlah' => $jumlah,
			 'KdDepartemen' => $dept,
			 'KdSubDivisi' => $subdivisi,
			 'keterangan' => $keterangan
			);
		$where = array('NoDokumen'=>$v_no_dokumen, 'KdRekening'=>$kdrekening);	
		$this->db->update('realisasi_uang_muka_payment', $data, $where);
		
	}
	
	function insertNewUangMuka($v_no_dokumen,$v_tgl_dokumen, $v_kdkasbank, $v_no_referensi, $v_employee, $v_jumlah, $v_no_rek, $v_subdivisi, $v_note, $NoPV, $user)
	{
		//$this->uang_muka_model->locktables('uang_muka');
		
		$data = array(
			 'NoDokumen'=>$v_no_dokumen,
             'TglDokumen'=>$v_tgl_dokumen,
             'KdKasBank' => $v_kdkasbank,
             'NoReferensi' => $v_no_referensi,
             'Employee' => $v_employee,
             'Jumlah' => $v_jumlah,
             'NoRekening' => $v_no_rek,
             'KdSubDivisi' => $v_subdivisi,
             'NoPV' => $NoPV,
             'Sisa' => 0,
             'Keterangan'=>$v_note,
             'AddDate' => date('Y-m-d'),
             'AddUser' => $user
		);
		$this->db->insert('uang_muka', $data);
		//$this->uang_muka_model->unlocktables();
	}
    
    function getData()
    {  
     $mylib = new globallib();     
     $employee_id= $this->input->post('id');
     
     $query = $this->realisasi_uang_muka_model->getDataUangMuka($employee_id);
    
     
      echo '
            <table class="table table-bordered responsive">
        	<thead class="title_table">
							<tr>
								<th width="30"><center>No</center></th>
								<th width="120"><center>Tanggal</center></th> 
								<th width="150"><center>No. Dokumen</center></th>
								<th><center>Keterangan</center></th>
								<th width="150"><center>Jumlah</center></th>
							    <th width="150"><center>Realisasi</center></th>
							</tr>
						</thead>
						<tbody>
						
	 	  ';
		  $cek = $query->result_array();
	 	
			 if(empty($cek)){
				echo "<tr><td colspan='100%' align='center'>Tidak Ada Data</td></tr>";
				}else{
					 $no=1;
				     foreach ($query->result_array() as $cetak) {
					 echo "								
										<tr>
											<td align='center'>$no</td>
											<td align='center'>$cetak[Tanggal]
											<td align='center'>$cetak[NoDokumen]
											<input type='hidden' class='form-control-new' name='v_no_um[]' value='$cetak[NoDokumen]'>
											<input type='hidden' class='form-control-new' name='v_no_pv[]' value='$cetak[NoPV]'>
											</td>
											<td align='left'>$cetak[Keterangan]</td>
											<td align='right'>$cetak[Sisa]
											<input type='hidden' class='form-control-new' name='v_kd_kassbank[]' id='v_kd_kassbank$no' value='$cetak[KdKasBank]' style='text-align: right; width: 100%;' />
											<input type='hidden' class='form-control-new' name='v_kd_subdivissi[]' id='v_kd_subdivissi$no' value='$cetak[KdSubDivisi]' style='text-align: right; width: 100%;' />
											</td>
											<td>
											<input type='hidden' class='form-control-new' name='v_jml_um[]' id='v_jml_um$no' value='$cetak[Jumlah]' style='text-align: right; width: 100%;' />
											<input type='text' class='form-control-new' name='v_realisasi[]' id='v_realisasi$no' value='' onKeyUp='calculate(this)' style='text-align: right; width: 100%;' /></td>
										</tr>
					 	  ";
						$no++;
				      }
				    echo "    <tr>
							  <td colspan='4'></td>
							  <td align='Right'><b>Total</b></td>
							  <td><input readonly style='text-align:right;' type='text' class='form-control-new' name='v_tot_realisasi' id='v_tot_realisasi' value=''></td>
							  </tr>
							  </tbody>
				              </table> ";    
				}
	 
	 echo '<br>';	
	if(!empty($cek)){	  
	 echo '<table class="table table-bordered responsive" id="detail">
	 		<thead class="title_table">
							<tr id="baris0">
								<th width="30"><center><img src="http://localhost/natura/public/images/table_add.png" width="16" height="16" border="0" onClick="detailNew()"></center></th>
								<th width="300"><center>Nama Rekening</center></th> 
								<th width="150"><center>Jumlah</center></th>
								<th width="200"><center>SubDivisi</center></th>
								<th width="200"><center>Departemen</center></th>
							    <th><center>Keterangan</center></th>
							</tr>
						</thead>
						<tbody>
	 	  ';
	 $subdivisi = $this->realisasi_uang_muka_model->getSubDivisi();
	 $dept = $this->realisasi_uang_muka_model->getDept();
	 $counter = 1;
	
	
	 echo "
		
	 	<tr id='baris$counter'>
		<td nowrap><img src='http://localhost/natura/public/images/del.png' width='16' height='16' id='del$counter' border='0' onClick='deleteRow(this)'></td>	
		<td nowrap><input class='form-control-new' type='text' id='namarekening$counter' name='namarekening[]' size='40'  value='' onkeyup='PilihRekening(this)' onkeydown='keyShortcut(event,namarekening,this)'></td>	
		
		<td nowrap><input class='form-control-new' type='text' id='jumlah$counter' name='jumlah[]' size='15' value='' value='0' onKeyUp='calculate2(this)' style='text-align:right;'></td>
        <td nowrap>
            <select class='form-control-new' size='1' id='subdivisi$counter' name='subdivisi[]' >";
               
                for($a = 0;$a<count($subdivisi);$a++){
                    $select = '';
                    if($vsubdivisi==$subdivisi[$a]['KdSubDivisi']){
                        $select = 'selected';
                    }
                    
                 echo '<option value='.$subdivisi[$a]['KdSubDivisi'].'>'.$subdivisi[$a]['NamaSubDivisi'].'</option>';
               
                }
                
        echo    "</select>
        </td>
        <td nowrap>
            <select class='form-control-new' size='1' id='dept$counter' name='dept[]' onkeydown='keyShortcut(event,dept,this)'>
                ";
                for($a = 0;$a<count($dept);$a++){
                    $select = '';
                    if($vdept==$dept[$a]['KdDepartemen']){
                        $select = 'selected';
                    }
                    
                    echo '<option  value= '.$dept[$a]['KdDepartemen'].'>'.$dept[$a]['NamaDepartemen'].'</option>';
                
                }
                
        echo    "</select>
        </td>
		<td nowrap><input class='form-control-new' type='text' id='keterangan$counter' name='keterangan[]' size='70' maxlength='150' value='' onkeydown='keyShortcut(event,keterangan,this)'>
		<input type='hidden' id='kdrekening$counter' name='kdrekening[]' value='$kdrekening' readonly'>
		<input type='hidden' id='tmpkdrekening$counter' name='tmpkdrekening[]' value='$kdrekening'>
		<input type='hidden' id='savekdrekening$counter' name='savekdrekening[]' value='$kdrekening'>
		<input type='hidden' id='tmpjumlah$counter' name='tmpjumlah[]' value='$jumlah'>
		<input type='hidden' id='urutan$counter' name='urutan[]' value='$counter'>
		</td>
		</tr>
				";  
			  
		 echo " </tbody>
				</table> 
				";
				
		 echo "
							  <table class='table table-bordered responsive'>
							  <tbody>
							  <tr>
							  <td width='310' align='Right'><b>Total &nbsp;&nbsp;&nbsp;</b></td>
							  <td><input readonly style='text-align:right;' type='text' class='form-control-new' name='v_tot_pv' id='v_tot_pv' value=''></td>
							  </tr>
							  </tbody>
				              </table>
				";
		  
		}
    
    }
	
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
		
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 7, 4) * 1) + 1;	
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }

 	function cari_karyawan(){
		$employee = $this->input->post('employee');
	    $query = $this->uang_muka_model->getEmployee($employee);
	     	     
	    echo "<option value=''> -- Pilih Karyaan --</option>";
	    foreach ($query as $rec) {
			echo "<option value=$rec[employee_id]>$rec[employee_name]</option>";
		}   
	}
    	
	function delete_trans($UM) 
    {
			$this->db->delete('realisasi_uang_muka', array('NoDokumen' => $UM));
			$this->db->delete('realisasi_uang_muka_detail', array('NoDokumen' => $UM));
			$this->db->delete('realisasi_uang_muka_payment', array('NoDokumen' => $UM));
            $this->session->set_flashdata('msg', array('message' => 'Proses hapus <strong>No Dokumen ' . $id . '</strong> berhasil', 'class' => 'success'));
        

        redirect('/keuangan/realisasi_uang_muka/');
    }
    
    function create_pdf() {
        $id = $this->uri->segment(4);
        //$getHeader = $this->uang_muka_model->getHeader($nodok);	
        
        
            $getHeader = $this->realisasi_uang_muka_model->getListRealisasiUangMukaPdf($id);
            $data['detail_realisasi'] = $this->realisasi_uang_muka_model->getListRealisasiDetail($id);
			$data['detail_realisasi_pv'] = $this->realisasi_uang_muka_model->getListRealisasiDetailPV($id);
			$data['dept'] = $this->realisasi_uang_muka_model->getDept();
			
			$data['PVRV'] = $this->realisasi_uang_muka_model->getNoPayReceipt($id);
        
        	
		$user = $this->session->userdata('username');
		
		$data['results'] = $getHeader;
        $html = $this->load->view('transaksi/keuangan/realisasi_uang_muka/pdf_realisasi_uang_muka', $data, true);
        $this->load->library('m_pdf');

        $pdfFilePath = "the_pdf_rum.pdf";
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);

        $pdf->Output();
        exit;
    }
    
    function posting_realisasi_um($tahun, $bulan, $userid, $no , $noref){
		$this->db->delete('jurnalheader', array('KodeJurnal' => 'RUM', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref, 'NoTransaksi' => $no));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'RUM', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref));
                
        $nodok = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 0;

		$rekening = $this->posting_model->getinterface();
        $header = $this->posting_model->getrealisasi_UM_Jurnal($tahun, $bulan, $no);
        	 
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$keterangan = 'Realisasi Uang Muka ';
			$keterangandetail = $header[$m]['Keterangan'];
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdsubdivisi = $header[$m]['KdSubDivisi'];
            $kdrekdetail = $header[$m]['KdRekening'];
            $debit = $header[$m]['Debet'];
            $credit = $header[$m]['Kredit'];
									
            if ($nodok <> $nodokumen) {
            	$new_no = $this->posting_model->getNewNo($bulan, $tahun);
                $noposting = $new_no->counter;
                                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $keterangan,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'RUM',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $counterid++;
            $totkredit += $debit;
            $totdebit += $credit;
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "RUM",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekdetail,
				"KdSubDivisi"	=> $kdsubdivisi,
                "Debit" => $debit,
                "Kredit" => $credit,
                "KeteranganDetail" => $keterangandetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            	
		    $nodok = $nodokumen;
		}
	}
    
		
}

?>