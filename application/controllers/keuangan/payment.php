<?php
include(APPPATH.'/controllers/auth/authcontroller'.EXT);
class payment extends authcontroller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/keuangan/paymentmodel');
        $this->load->model('proses/posting_model');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
			$arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
				$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '15';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/keuangan/payment/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/keuangan/payment/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/keuangan/payment/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$thnbln = $data['tahun'].$data['bulan'];
			$data['header']		 		= array("No Dokumen","Tanggal","Kas Bank","Dibayar kepada","Jumlah","Nomor Bukti","Keterangan","Username");
	        $config['total_rows']		= $this->paymentmodel->num_payment_row($id,$with,$thnbln);		
	        $data['data']	= $this->paymentmodel->getpaymentList($config['per_page'],$page,$id,$with,$thnbln);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/keuangan/payment/paymentlist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new($pesan){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->paymentmodel->getDate();
			
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$tgl = '01'.'-'.$data['bulan'].'-'.$data['tahun'];
			$data['tanggal'] = date('d-m-Y');
			$data['aplikasi'] = $aplikasi;
			
			$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			$user = $this->session->userdata('username');
			$data['mkasbank']   = $this->paymentmodel->getKasBank($user);
			$data['mpersonal']  = $this->paymentmodel->getPersonal();
			$data['mcostcenter'] = $this->paymentmodel->getCostCenter();
			$data['mSubdivisi'] = $this->paymentmodel->getSubdivisi();
			$data['mDivisi']    = $this->paymentmodel->getDivisi();
			$data['mDept']      = $this->paymentmodel->getDept();
			$data['pesan'] = $pesan;
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/keuangan/payment/add_payment',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_payment', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "payment.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
//		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
//		$data['string2'] = "(                     )         (                      )";
        $data['string1'] = "     Dibuat Oleh                Diperiksa Oleh                   Menyetujui";
        $data['string2'] = "(                     )    (                      )       (                      )";
        
        $data['string1'] = "   Dibuat Oleh          Diterima oleh         Diperiksa Oleh        Menyetujui";
        $data['string2'] = "(                )   (                  )  (                  )  (               )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer_keuangan', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$id = $this->uri->segment(4);
		$header	 = $this->paymentmodel->getHeader($id);

		$data['header']	 = $header;
		$detail	 = $this->paymentmodel->getDetailForPrint($id);
//        print_r($detail);
		$data['judul1'] = array("No Payment","Kas Bank","Keterangan");
		$data['niljudul1'] = array($header->NoDokumen,$header->KdKasBank." - ".stripslashes($header->NamaKasBank),substr(stripslashes($header->Keterangan),0,25));
		$data['judul2'] = array("Tanggal","Nomor Bukti","Dibayar Kepada");
		$data['niljudul2'] = array($header->Tanggal,$header->NoBukti,$header->Penerima);
		$data['judullap'] = "Payment Voucher";
		$data['url'] = "payment/printThis/".$id;
		$data['colspan_line'] = 4;
        $data['lebar_detail'] = array(15,30,30,15);// total 95  Rekening 	Nama 	Keterangan 	Jumlah
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("Rekening","Nama","Keterangan","Jumlah");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0,0);
		for($m=0;$m<count($detail);$m++)
		{
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['KdRekening']);
			$list_detail[] = stripslashes($detail[$m]['NamaRekening']);
			$list_detail[] = substr(stripslashes($detail[$m]['Keterangan']),0,25);
			$list_detail[] = number_format($detail[$m]['Jumlah'],2,",",".");
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array("Jumlah Payment");
		$data['footer2']  = array(number_format($header->JumlahPayment,2,",","."));
        $data['brs_footer']= array(20,3,15);
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		$data['string1'] = "Dibuat Oleh";
		$data['string2'] = "Menyetujui";
		$data['string3'] = "(____________________)";
		$data['string4'] = "(____________________)";
		return $data;
	}

    function delete_payment(){
    	$mylib = new globallib();
     	$id = $this->input->post('kode');
		$lanjut = 1;
		$ret = "";
		
		$rec = $this->paymentmodel->getHeader($id);
		$tgl = $mylib->ubah_tanggal($rec->Tanggal);
		$cek = $mylib->CekPeriode('Kas', $tgl);
		
		if($cek['Valid']==0){
			$lanjut=0;
			$ret = 'closed';
		}
		if($lanjut==1)
		{
			$this->paymentmodel->locktables('trans_payment_header,trans_payment_detail');
			$this->db->update('trans_payment_header', array('Status'=>'B'), array('NoDokumen' => $id));
			$this->db->update('trans_payment_detail', array('Status'=>'B'), array('NoDokumen' => $id));
			$this->paymentmodel->unlocktables();
		}
		echo $ret;   
	}

    function edit_payment($id){
//        echo "masu";die();
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
//            echo $id;die();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$data['header']	= $this->paymentmodel->getHeader($id);
//            print_r($data['header']);die();
	    	$data['detail']	= $this->paymentmodel->getDetail($id);
			$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			$user = $this->session->userdata('username');
			$data['mkasbank']   = $this->paymentmodel->getKasBank($user);
			$data['mpersonal'] = $this->paymentmodel->getPersonal();
			$data['mcostcenter'] = $this->paymentmodel->getCostCenter();
            $data['mSubdivisi'] = $this->paymentmodel->getSubdivisi();
            $data['mDivisi']    = $this->paymentmodel->getDivisi();
            $data['mDept']      = $this->paymentmodel->getDept();
            
            //cek apakah ada di pelunasan_hutang_header
            $cek= $this->paymentmodel->getHasilCek($id);
            if(!empty($cek)){
				$data['hasil_cek']="Y";//ada
			}else{
				$data['hasil_cek']="N";//tidak ada
			}
            
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/keuangan/payment/edit_payment', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function jurnal_edit_payment($id){
//        echo "masu";die();
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
//            echo $id;die();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$data['header']	= $this->paymentmodel->getHeader($id);
//            print_r($data['header']);die();
	    	$data['detail']	= $this->paymentmodel->getDetail($id);
			$data['mjenis'] = array("1"=>"Kas/Bank","2"=>"Giro");
			$user = $this->session->userdata('username');
			$data['mkasbank']   = $this->paymentmodel->getKasBank($user);
			$data['mpersonal'] = $this->paymentmodel->getPersonal();
			$data['mcostcenter'] = $this->paymentmodel->getCostCenter();
            $data['mSubdivisi'] = $this->paymentmodel->getSubdivisi();
            $data['mDivisi']    = $this->paymentmodel->getDivisi();
            $data['mDept']      = $this->paymentmodel->getDept();
            
            //cek apakah ada di pelunasan_hutang_header
            $cek= $this->paymentmodel->getHasilCek($id);
            if(!empty($cek)){
				$data['hasil_cek']="Y";//ada
			}else{
				$data['hasil_cek']="N";//tidak ada
			}
            
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/keuangan/payment/jurnal_edit_payment', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
    
    function cek(){
	    $pv = $this->input->post('no');
	    $cek = $this->paymentmodel->cek($pv);
	    if(!empty($cek)){
			$data['cek']="Y";
		}else{
			$data['cek']="N";
		}
		echo json_encode($data);
	}
	
	function getkdrekening()
	{
		$kode = $this->input->post('kdrekening');
		$valreken = $this->paymentmodel->findrekening($kode);
		if(count($valreken)!=0)
		{
			$kode = $valreken->kdrekening."*-*".$valreken->namarekening;
		}
		else
		{
			$kode = "";
		}
		echo $kode;
	}

	function save_new_payment(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
        $jenis = $this->input->post('jenistr');
        $kasbank = $this->input->post('kasbank');
        $personal = $this->input->post('personal');
        $costcenter = $this->input->post('costcenter');
		$dept       = $this->input->post('dept');
		$subdivisi  = $this->input->post('subdivisi');
		$penerima   = $this->input->post('penerima');
		
		$file1 = '';
		$file2 = '';
		if(isset($_FILES['file']['tmp_name'][0])){
			if($_FILES['file']['tmp_name'][0] != null || $_FILES['file']['tmp_name'][0] != '')
       			$file1 = file_get_contents($_FILES['file']['tmp_name'][0]);		
		}
		
		if(isset($_FILES['file']['tmp_name'][1])){
		   	if($_FILES['file']['tmp_name'][1] != null || $_FILES['file']['tmp_name'][1] != '')
       			$file2 = file_get_contents($_FILES['file']['tmp_name'][1]);	
       	}
		
//            die();
		/*
		$nogiro = strtoupper(addslashes($this->input->post('nogiro')));
		$tglcair = $this->input->post('tglcair');
		$bankcair = $this->input->post('bankcair');
		*/
		$nogiro = "";
		$tglcair = "0000-00-00";
		$bankcair = "";
		$nobukti = strtoupper(addslashes($this->input->post('nobukti')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = $this->input->post('kdrekening');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$counter = $this->input->post('urutan');
		if(($jenis=="2")&&($no==""))
		{
	       $cekgiro = $this->paymentmodel->cekgiro($nogiro);
		   $adagiro = count($cekgiro);
		}
		else
		   $adagiro = "";
		if($adagiro=="")
		{
			if($no=="")
			{
				$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair,$penerima,$file1,$file2);
			}
			else
			{
				$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair,$penerima, $file1, $file2);
			}
			for($x=0;$x<count($kdrekening);$x++)
			{
				$kdrekening1 = strtoupper(addslashes(trim($kdrekening[$x])));
				$jumlah1 = $jumlah[$x];
				$savekdrekening1 = $savekdrekening[$x];
				$keterangan1 = $keterangan[$x];
				$counter1 = $counter[$x];
                $dept1      = $dept[$x];
                $subdivisi1 = $subdivisi[$x];
				if($kdrekening1!=""){
					$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter1,$kdrekening1,$jumlah1,$keterangan1,$user,$savekdrekening1,$dept1,$subdivisi1);
				}
			}
			if($flag=="edit"){
				//redirect('/keuangan/payment/');
				$link = "<script>
							window.open('".base_url()."index.php/keuangan/payment/cetak/".$no ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
							window.location.href = '".base_url()."index.php/keuangan/payment/"."';
						</script>";
			}else{
				//redirect('/keuangan/payment/add_new/new');
				$link = "<script>
							window.currentChild = false;
							const child  = window.open('".base_url()."index.php/keuangan/payment/cetak/".$no ."','popuppage','scrollbars=yes, width=900,height=500,top=50,left=50');
							currentChild = child;
							if(currentChild){
								window.location.href = '".base_url()."index.php/keuangan/payment/edit_payment/".$no."';
							}else{
								alert('Pop Up Disable, Enable POP UP!');
								window.location.href = '".base_url()."index.php/keuangan/payment/edit_payment/".$no."';
							}
						</script>";
			}
			echo $link;
		}else
		{
		  $this->add_new($nogiro);
		}
	}
	
	function save_new_payment_jurnal(){
		//echo "<pre>";print_r($_POST);echo "</pre>";die;
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
        $jenis = $this->input->post('jenistr');
        $kasbank = $this->input->post('kasbank');
        $personal = $this->input->post('personal');
        $costcenter = $this->input->post('costcenter');
		$dept       = $this->input->post('dept');
		$subdivisi  = $this->input->post('subdivisi');
		$penerima   = $this->input->post('penerima');
		
		$file1 = '';
		$file2 = '';
		if(isset($_FILES['file']['tmp_name'][0])){
			if($_FILES['file']['tmp_name'][0] != null || $_FILES['file']['tmp_name'][0] != '')
       			$file1 = file_get_contents($_FILES['file']['tmp_name'][0]);		
		}
		
		if(isset($_FILES['file']['tmp_name'][1])){
		   	if($_FILES['file']['tmp_name'][1] != null || $_FILES['file']['tmp_name'][1] != '')
       			$file2 = file_get_contents($_FILES['file']['tmp_name'][1]);	
       	}
		
		$nogiro = "";
		$tglcair = "0000-00-00";
		$bankcair = "";
		$nobukti = strtoupper(addslashes($this->input->post('nobukti')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = $this->input->post('kdrekening');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$counter = $this->input->post('urutan');
		if(($jenis=="2")&&($no==""))
		{
	       $cekgiro = $this->paymentmodel->cekgiro($nogiro);
		   $adagiro = count($cekgiro);
		}
		else
		   $adagiro = "";
		if($adagiro=="")
		{
			if($no=="")
			{
				$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair,$penerima,$file1,$file2);
			}
			else
			{
				$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair,$penerima, $file1, $file2);
			}
			for($x=0;$x<count($kdrekening);$x++)
			{
				$kdrekening1 = strtoupper(addslashes(trim($kdrekening[$x])));
				$jumlah1 = $jumlah[$x];
				$savekdrekening1 = $savekdrekening[$x];
				$keterangan1 = $keterangan[$x];
				$counter1 = $counter[$x];
                $dept1      = $dept[$x];
                $subdivisi1 = $subdivisi[$x];
				if($kdrekening1!=""){
					$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter1,$kdrekening1,$jumlah1,$keterangan1,$user,$savekdrekening1,$dept1,$subdivisi1);
				}
			}
			
			//posting jurnal kembali
			//ambil no referensi di jurnal header
            $noreferensi = $this->paymentmodel->getReferensi($no);
            $pecah = explode('-',$tgl);
            $tahun = $pecah[2];
            $bulan = $pecah[1];
			$this->posting_payment($tahun, $bulan, $user, $no, $noreferensi->NoReferensi);
			
			$this->session->set_flashdata('msg', array('message' => 'Proses update <strong>No Dokumen ' . $no . '</strong> berhasil', 'class' => 'success'));
			
			redirect('/keuangan/payment/jurnal_edit_payment/'.$no);
		}else
		{
		  $this->add_new($nogiro);
		}
	}
	
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenistr');
		$kasbank = $this->input->post('kasbank');
		$personal = $this->input->post('personal');
		$costcenter = $this->input->post('costcenter');
		/*
		$nogiro = strtoupper(addslashes($this->input->post('nogiro')));
		$tglcair = $this->input->post('tglcair');
		$bankcair = $this->input->post('bankcair');
		*/
		$nogiro = "";
		$tglcair = "0000-00-00";
		$bankcair = "";
		$nobukti = strtoupper(addslashes($this->input->post('nobukti')));
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$kdrekening = strtoupper(addslashes($this->input->post('kdrekening')));
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$counter = $this->input->post('urutan');
        $dept       = $this->input->post('dept');
        $subdivisi  = $this->input->post('subdivisi');
        $penerima   = $this->input->post('penerima');
		$jumlahpayment = $this->input->post('jumlahpayment');
		
		$file1 = '';
		$file2 = '';
		if(isset($_FILES['file1'])){
			if($_FILES['file1']['tmp_name'] != null || $_FILES['file1']['tmp_name'] != ''){
				$file1 = file_get_contents($_FILES['file1']['tmp_name']);
			}
		}
		if(isset($_FILES['file2'])){
       		if($_FILES['file2']['tmp_name'] != null || $_FILES['file2']['tmp_name'] != ''){
				$file2 = file_get_contents($_FILES['file2']['tmp_name']);
			}
       	}
       		
		if(($jenis=="2")&&($no==""))
		{
	       $cekgiro = $this->paymentmodel->cekgiro($nogiro);
		   $adagiro = count($cekgiro);
		}
		else
		   $adagiro = "";
		if($adagiro=="")
		{
			if($no=="")
			{
				$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair,$penerima, $file1, $file2);
			}
			else
			{
				$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$mylib->ubah_tanggal($tglcair),$bankcair,$penerima,$file1,$file2);
			}
			$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening,$dept,$subdivisi);
			echo $no;
		}
		else
		{
		    echo "adagiro";
		}
	}
	
	function posting_payment($tahun, $bulan, $userid, $no, $noref)
    {
        $this->db->delete('jurnalheader', array('KodeJurnal' => 'PV', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref, 'NoTransaksi' => $no));
        $this->db->delete('jurnaldetail', array('KodeJurnal' => 'PV', 'Bulan' => $bulan, 'Tahun' => $tahun,'NoReferensi' => $noref));
        
        $nodok = '';
        $noposting1 = '';
        $nilkredit = 0;
        $totdebit = 0;
        $totkredit = 0;
        $counterid = 1;

        $header = $this->posting_model->getpaymentJurnal($tahun, $bulan, $no);
        				
        for ($m = 0; $m < count($header); $m++) {
            $nodokumen = $header[$m]['NoDokumen'];
			$divisiheader = $header[$m]['divisiheader'];
			$divisidetail = $header[$m]['divisidetail'];
			
			$tgldokumen = $header[$m]['TglDokumen'];
            $kdrekening = $header[$m]['KdRekening'];
            $rekeningheader = $header[$m]['RekeningHeader'];
            $debit	= $header[$m]['Debit'];
            $valdebit = $debit >0 ? $debit : 0;
            $valkredit = $debit >0 ? 0 : $debit*-1;
            $ketdetail = $header[$m]['KetDetail'];
            $ketheader = $header[$m]['KetHeader'];
									
            if ($nodok <> $nodokumen) {
            	if($header[$m]['NoPosting'] == NULL || $header[$m]['NoPosting'] == ''){
            		$new_no = $this->posting_model->getNewNo($bulan, $tahun);
                	$noposting = $new_no->counter;
                	$this->db->update('trans_payment_header', array('NoPosting' => $noposting), array('NoDokumen' => $nodokumen));
                }else
                	$noposting = $header[$m]['NoPosting'];
                
                $data = array("KdDepartemen" => '00',
                    "NoReferensi"   => $noposting,
                    "TglTransaksi"  => $tgldokumen,
                    "JenisJurnal"   => 'U',
                    "Keterangan"    => $ketheader,
                    "Project"       => '00',
                    "CostCenter"    => '00',
                    "KodeJurnal"    => 'PV',
                    "NoTransaksi"   => $nodokumen,
                    "Bulan"         => $bulan,
                    "Tahun"         => $tahun,
                    "AddDate"       => $tgldokumen,
                    "AddName"       => $userid);
                $this->db->insert('jurnalheader', $data);
            }   
            
            $datadtl = array("NoReferensi" => $noposting,
                "TglTransaksi" => $tgldokumen,
                "KodeJurnal" => "PV",
                "KdDepartemen" => "00",
                "Project" => "00",
                "CostCenter" => "00",
                "KdRekening" => $kdrekening,
				"KdSubDivisi"	=> $divisidetail,
                "Debit" => $valdebit,
                "Kredit" => $valkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
            $nilkredit = $nilkredit + $debit;
            $totdebit += $valdebit;
            $totkredit += $valkredit;
            $counterid = $counterid + 1;
            
            $nodok = $nodokumen;
            if($m < (count($header)-1)){
				if($nodokumen != $header[$m+1]['NoDokumen']){
					$datadtl = array("NoReferensi" => $noposting,
	                "TglTransaksi"  => $tgldokumen,
	                "KodeJurnal"    => "PV",
	                "KdDepartemen"  => "00",
	                "Project"       => "00",
	                "CostCenter"    => "00",
					"KdSubDivisi"	=> $divisiheader,
	                "KdRekening"    => $rekeningheader,
	                "Debit"         => 0,
	                "Kredit"        => $nilkredit,
	                "KeteranganDetail" => $ketdetail,
	                "Bulan" => $bulan,
	                "Tahun" => $tahun,
	                "JenisJurnal" => "U",
	                "AddDate" => $tgldokumen,
	                "AddName" => $userid,
	                "Counter" => $counterid
		            );
		            $this->db->insert('jurnaldetail', $datadtl);
	                $nilkredit = 0;
	                $counterid = 1;
	                $totdebit = 0;
	                $totkredit = 0;
		        }	
			}	
		}
		
        if ($m > 0) {
            $datadtl = array(
				"NoReferensi"	=> $noposting,
                "TglTransaksi"	=> $tgldokumen,
                "KodeJurnal"	=> "PV",
                "KdDepartemen"	=> "00",
                "Project"		=> "00",
                "CostCenter"	=> "00",
                "KdRekening"	=> $rekeningheader,
				"KdSubDivisi"	=> $divisiheader,
                "Debit"		=> 0,
                "Kredit"	=> $nilkredit,
                "KeteranganDetail" => $ketdetail,
                "Bulan" => $bulan,
                "Tahun" => $tahun,
                "JenisJurnal" => "U",
                "AddDate" => $tgldokumen,
                "AddName" => $userid,
                "Counter" => $counterid
            );
            $this->db->insert('jurnaldetail', $datadtl);
        }
    }
    
	function get_no_counter( $kode,$table_name, $col_primary, $thn,$bln)
    {
        $query = "
        SELECT
            " . $table_name . "." . $col_primary . "
        FROM
            " . $table_name . "
        WHERE
            1
            AND SUBSTR(" . $table_name . "." . $col_primary . ", 1,6) = '" .$kode.$thn.$bln. "'

        ORDER BY
            " .$table_name . "." . $col_primary . " DESC
        LIMIT
            0,1
        ";
        //echo $query;
        $qry = mysql_query($query);
        $row = mysql_fetch_array($qry);
        list($col_primary_ok) = $row;

        $counter = (substr($col_primary_ok, 6, 4) * 1) + 1;
        $counter_fa = $kode.sprintf($thn . $bln. sprintf("%04s", $counter));
        return $counter_fa;

    }

	function insertNewHeader($flag,$tgl,$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$tglcair,$bankcair,$penerima,$file1,$file2)
	{
		$this->paymentmodel->locktables('counter,trans_payment_header');
		$bulan = substr($tgl, 5, 2);
		$tahun = substr($tgl, 2, 2);
		if (date('Y-m-d',strtotime($tgl)) > date('Y-m-d',strtotime('2016-10-31'))){
		 
		    $kd_no = $this->paymentmodel->getKodeBank($kasbank);
		    $no = $this->get_no_counter( $kd_no->KdPembayaran,"trans_payment_header", "NoDokumen",$tahun,$bulan);
		}else{
		    $new_no = $this->paymentmodel->getNewNo($tgl);
		    $no = $new_no->NoPayment;
		}
		//die;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'NoDokumen'	    => $no,
			'TglDokumen'    => $tgl,
			'Jenis'         => $jenis,
			'Penerima'      => $penerima,
			'KdKasBank' => $kasbank,
			'KdCostCenter' => $costcenter,
			'KdPersonal' => $personal,
			'NoGiro' => $nogiro,
			'KdBankCair' => $bankcair,
			'TglCair' => $tglcair,
			'NoBukti' => $nobukti,
			'Keterangan' => $ket,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'JumlahPayment' => $jumlahpayment,
			'Lampiran1' => $file1,
			'Lampiran2' => $file2,
			'Status' => ' '
		);
		$this->db->insert('trans_payment_header', $data);
		$this->paymentmodel->unlocktables();
		return $no;
	}
	function updateHeader($flag,$no,$tgl,$kasbank,$costcenter,$personal,$nobukti,$ket,$user,$jumlahpayment,$jenis,$nogiro,$tglcair,$bankcair,$penerima,$file1,$file2)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_header');
		$lampiran = $this->paymentmodel->getLampiran($no);
		if($file1=='')
			$file1 = $lampiran[0]['Lampiran1'];
		if($file2=='')
			$file2 = $lampiran[0]['Lampiran2'];
			
		if($jenis=='1')
		{
		   $nogiro = "";
		   $bankcair = "";
		   $tglcair = "0000-00-00";
		}
		$data = array(
		    'TglDokumen'    => $tgl,
			'Jenis'         => $jenis,
            'Penerima'      => $penerima,
		    'KdKasBank' => $kasbank,
			'KdCostCenter' => $costcenter,
			'KdPersonal' => $personal,
			'NoGiro' => $nogiro,
			'KdBankCair' => $bankcair,
			'TglCair' => $tglcair,
			'NoBukti' => $nobukti,
			'Keterangan' => $ket,
			'JumlahPayment' => $jumlahpayment,
			'Lampiran1' => $file1,
			'Lampiran2' => $file2
		);
		if($flag=="edit")
		{
			$data['EditDate'] = $tgl1;
			$data['EditUser'] = $user;
			$this->db->update('trans_payment_detail', array('EditDate'=> $tgl1,'EditUser'=>$user), array('NoDokumen' => $no));
		}
		$this->db->update('trans_payment_header', $data, array('NoDokumen' => $no));

		$this->paymentmodel->unlocktables();
	}

	function insertDetail($flag,$no,$tgl,$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening,$dept,$subdivisi)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->paymentmodel->locktables('trans_payment_detail');
		if($savekdrekening==""){
			$data = array(
				'NoDokumen'	=> $no,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => $dept,
				'Keterangan' => $keterangan,
				'Urutan'	=> $counter,
				'Status' => ' '
			);
			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->insert('trans_payment_detail', $data);
		}
		else 
		{
			$data = array(
			    'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
                'KdSubdivisi'     => $subdivisi,
                'KdDepartemen'    => $dept,
				'Keterangan' => $keterangan
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_payment_detail', $data, array('NoDokumen' => $no,'Urutan'=>$counter));
		}
		$this->paymentmodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$counter = $this->input->post('urutan');
		$kdrekening = $this->input->post('kdrekening');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$this->paymentmodel->locktables('trans_payment_detail');
		$this->db->update('trans_payment_detail', array('Status' => 'B') ,array('NoDokumen' => $id,'Urutan'=>$counter));
		$this->db->update('trans_payment_header', array('JumlahPayment' => $jumlahpayment) ,array('NoDokumen' => $id));
		$this->paymentmodel->unlocktables();
	}
	
	function delete_item_jurnal()
	{
		$id = $this->input->post('no');
		$counter = $this->input->post('urutan');
		$kdrekening = $this->input->post('kdrekening');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$this->paymentmodel->locktables('trans_payment_detail');
		$this->db->update('trans_payment_detail', array('Status' => 'B','Urutan'=>'9'.$counter) ,array('NoDokumen' => $id,'Urutan'=>$counter));
		$this->paymentmodel->unlocktables();
	}
	
	function getrekening(){
		$kdrekening = $this->input->post('KdRekening');
		
		$rek = $this->paymentmodel->getrekening($kdrekening);
		$jsArray = "";

	    $cnt = count($rek);
	    for($i=0; $i<$cnt; $i++){
	        $jsArray.= $rek[$i]['KdRekening'].'#';
	    }
	    //Removes the remaining comma so you don't get a blank autocomplete option.
	    $jsArray = substr($jsArray, 0, -1);

	    echo $jsArray;
	}

}
?>
