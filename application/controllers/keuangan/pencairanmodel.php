<?php
class Pencairanmodel extends CI_Model {
	
    function __construct(){
        parent::__construct();
    }

    function getpencairanList($num,$offset,$id,$with,$thnbln)
	{
	 	if($offset !=''){
			$offset = $offset;
		}            
        else{
        	$offset = 0;
        }
		$clause="where h.status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
		if($id!=""){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
    	$sql = "select NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,Keterangan,JumlahPencairan,NamaKasBank,UserName,h.Jenis,h.KdKasBank
				from trans_pencairan_header h 
				left join kasbank on kasbank.KdKasBank = h.KdBankCair
				left join user on user.id = h.AddUser
				$clause 
				order by cast(NoDokumen as unsigned) desc Limit $offset,$num
			";
		return $this->getArrayResult($sql);
    }
    
    function num_pencairan_row($id,$with,$thnbln){
     	$clause="where Status<>'B' and DATE_FORMAT(TglDokumen,'%Y%m')='$thnbln'";
     	if($id!=''){
			if($with=="NoDokumen"){
				$clause .= " and $with like '%$id%'";
			}
			else
			{
				$clause .= " and $with = '$id'";
			}
		}
		$sql = "SELECT NoDokumen FROM trans_pencairan_header $clause";
        return $this->NumResult($sql);
	}
	
	function getDate(){
    	$sql = "SELECT date_format(TglTrans,'%d-%m-%Y') as TglTrans from aplikasi";
        return $this->getRow($sql);
    }
	function getKasBank(){
    	$sql = "SELECT KdKasBank,NamaKasBank from kasbank order by KdKasBank";
		return $this->getArrayResult($sql);
    }
	function getPersonal(){
    	$sql = "SELECT KdPersonal,NamaPersonal from personal order by KdPersonal";
		return $this->getArrayResult($sql);
    }
	function getCostCenter(){
		$sql = "SELECT KdCostCenter,NamaCostCenter FROM costcenter order by KdCostCenter";
		return $this->getArrayResult($sql);
	}
	function cekgiro($nogiro){
    	$sql = "SELECT * from bukugiro where NoGiro='$nogiro'";
		return $this->getArrayResult($sql);
    }
    function findnogiro($id,$jenis,$bank){
		$sql = "SELECT nogiro,tglterima,tgljto,nilaigiro FROM bukugiro Where nogiro='$id' and jenis='$jenis' and bankcair='$bank'";
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}

	function getNewNo($tgl)
	{
		$tahun = substr($tgl,0,4);
		$bulan = substr($tgl,5,2);
		$sql = "Update counter set NoPencairan=NoPencairan+1 where Tahun='$tahun' and Bulan='$bulan'";
		$this->db->query($sql);
		$sql = "SELECT NoPencairan FROM counter where Tahun='$tahun' and Bulan='$bulan'";
		return $this->getRow($sql);
	}
	function getDetail($id)
	{
		$sql = "select d.NoGiro,Jumlah,TglBuka,TglJto,Urutan from(
		SELECT * from trans_pencairan_detail Where NoDokumen='$id' and Tahun='$tahun' and Bulan='$bulan'
		order by Urutan
		)d";
        return $this->getArrayResult($sql);
	}
	function getDetailForPrint($id)
	{
		$sql = "select d.KdRekening,Jumlah,Keterangan,NamaRekening,Urutan from(
		SELECT * from trans_pencairan_detail Where NoDokumen='$id' and Tahun='$tahun' and Bulan='$bulan' order by Urutan
		) d
		left join
		(
			select KdRekening,NamaRekening from e2sss.rekening
		)rekening
		on rekening.KdRekening = d.KdRekening";
        return $this->getArrayResult($sql);
	}
	function getHeader($id)
	{
		$sql = "select NoDokumen,Tanggal,NamaKasBank,Keterangan,h.KdKasBank,JumlahPencairan,Jenis,KdBankCair from(
SELECT NoDokumen,date_format(TglDokumen,'%d-%m-%Y') as Tanggal,Keterangan,
KdKasBank,JumlahPencairan,Jenis,KdBankCair
from trans_pencairan_header where NoDokumen='$id' and Tahun='$tahun' and Bulan='$bulan')h
left JOIN
(
select KdKasBank,NamaKasBank from kasbank
)kasbank
on kasbank.KdKasBank = h.KdKasBank;";
        return $this->getRow($sql);
	}
	function locktables($table)
	{
		$this->db->simple_query("LOCK TABLES $table");
	}
	function unlocktables()
	{
		$this->db->simple_query("UNLOCK TABLES");
	}
	function getRow($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->row();
        $qry->free_result();
        return $row;
	}
	function getArrayResult($sql)
	{
		$qry = $this->db->query($sql);
        $row = $qry->result_array();
        $qry->free_result();
        return $row;
	}
	function NumResult($sql)
	{
		$qry = $this->db->query($sql);
        $num = $qry->num_rows();
        $qry->free_result();
        return $num;
	}
}
?>