<?php
class pelunasanhutang extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->library('globallib');
        $this->load->model('transaksi/pelunasanhutangmodel');
    }

    function index(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("all");
    	if($sign=="Y")
		{
		 	$segs 		  = $this->uri->segment_array();
  		    $arr 		  = "index.php/".$segs[1]."/".$segs[2]."/";
		 	$data['link'] = $mylib->restrictLink($arr);
	     	$id 		  = $this->input->post('stSearchingKey');
			$id2 		  = $this->input->post('date1');
	        $with 		  = $this->input->post('searchby');
			if($with=="TglDokumen")
			{
				$id = $mylib->ubah_tanggal($id2);
			}
	        $this->load->library('pagination');

	        $config['full_tag_open']  = '<div class="pagination">';
	        $config['full_tag_close'] = '</div>';
	        $config['cur_tag_open']   = '<span class="current">';
	        $config['cur_tag_close']  = '</span>';
	        $config['per_page']       = '15';
	        $config['first_link'] 	  = 'First';
	        $config['last_link'] 	  = 'Last';
	        $config['num_links']  	  = 2;
			$config['base_url']       = base_url().'index.php/transaksi/pelunasanhutang/index/';
			$page					  = $this->uri->segment(4);
			$config['uri_segment']    = 4;
			$flag1					  = "";
			if($with!=""){
		        if($id!=""&&$with!=""){
					$config['base_url']     = base_url().'index.php/transaksi/pelunasanhutang/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			 	else{
					$page ="";
				}
			}
			else{
				if($this->uri->segment(5)!=""){
					$with 					= $this->uri->segment(4);
				 	$id 					= $this->uri->segment(5);
					if($with=="TglDokumen")
					{
						$id = $mylib->ubah_tanggal($id);
					}
				 	$config['base_url']     = base_url().'index.php/transaksi/pelunasanhutang/index/'.$with."/".$id."/";
					$page 					= $this->uri->segment(6);
					$config['uri_segment']  = 6;
				}
			}
			$data['header']		 		= array("No Dokumen","Tanggal","Kode","Supplier","Retur/DN","Jumlah Awal","Sisa","Jumlah Bayar","Keterangan","Username","Jenis");
	        $config['total_rows']		= $this->pelunasanhutangmodel->num_pelunasanhutang_row($id,$with);		
	        $data['data']	= $this->pelunasanhutangmodel->getpelunasanList($config['per_page'],$page,$id,$with);
	        $data['track'] = $mylib->print_track();
			$this->pagination->initialize($config);
	        $this->load->view('transaksi/pelunasanhutang/pelunasanhutanglist', $data);
	    }
		else{
			$this->load->view('denied');
		}
    }

    function add_new(){
     	$mylib = new globallib();
    	$sign  = $mylib->getAllowList("add");
    	if($sign=="Y"){
			$aplikasi = $this->pelunasanhutangmodel->getDate();
			$data['bulan'] = $this->session->userdata('bulanaktif');
			$data['tahun'] = $this->session->userdata('tahunaktif');
			$tgl = '01'.'-'.$data['bulan'].'-'.$data['tahun'];
			$data['tanggal'] = $aplikasi->TglTrans;
			if(($data['tahun']==substr($data['tanggal'],-4))&&($data['bulan']==substr($data['tanggal'],3,2)))
			     $data['tanggal'] = $aplikasi->TglTrans;
		    else
			     $data['tanggal'] = $tgl;
			$data['listjenis'] = Array ( Array ( 'Jenis' => '2', 'NamaJenis'=>'Hutang' ));
			$data['track'] = $mylib->print_track();
	    	$this->load->view('transaksi/pelunasanhutang/add_pelunasanhutang',$data);
    	}
		else{
			$this->load->view('denied');
		}
    }

	function cetak()
	{
		$data = $this->varCetak();
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi', $data);
	}

	function printThis()
	{
		$data = $this->varCetak();
		$id = $this->uri->segment(4);
		$data['fileName2'] = "pelunasan.sss";
		$data['fontstyle'] = chr(27).chr(80);
		$data['nfontstyle'] = "";
		$data['spasienter'] = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n".chr(27).chr(48)."\r\n".chr(27).chr(50);
		$data['string1'] = "     Dibuat Oleh                       Menyetujui";
		$data['string2'] = "(                     )         (                      )";
		$this->load->view('transaksi/cetak_transaksi/cetak_transaksi_printer', $data);
	}

	function varCetak()
	{
		$this->load->library('printreportlib');
		$mylib = new printreportlib();
		$id = $this->uri->segment(4);
		$header	 = $this->pelunasanhutangmodel->getHeader($id);
		$data['header']	 = $header;
		$detail	 = $this->pelunasanhutangmodel->getDetailForPrint($id);
		$data['judul1'] = array("No Pelunasan","Kas Bank","Keterangan");
		$data['niljudul1'] = array($header->NoDokumen,$header->KdKasBank." - ".stripslashes($header->NamaKasBank),stripslashes($header->Keterangan));
		$data['judul2'] = array("Tanggal","Cost Center","Personal");
		$data['niljudul2'] = array($header->Tanggal,$header->KdCostCenter." - ".stripslashes($header->NamaCostCenter),$header->KdPersonal." - ".stripslashes($header->NamaPersonal));
		$data['judullap'] = "Pelunasan Hutang";
		$data['url'] = "pelunasanhutang/printThis/".$id;
		$data['colspan_line'] = 4;
		$data['tipe_judul_detail'] = array("normal","normal","normal","kanan");
		$data['judul_detail'] = array("Rekening","Nama","Keterangan","Jumlah");
		$data['panjang_kertas'] = 30;
		$default_page_written = 19;
		$data['panjang_per_hal'] = (int)$data['panjang_kertas'] - (int)$default_page_written;
		if($data['panjang_per_hal']!=0){
			$data['tot_hal'] = ceil((int)count($detail)/ (int)$data['panjang_per_hal']);
		}
		else
		{
			$data['tot_hal'] = 1;
		}
		$list_detail = array();
		$detail_page = array();
		$counterRow = 0;
		$max_field_len = array(0,0,0,0);
		for($m=0;$m<count($detail);$m++)
		{
			unset($list_detail);
			$counterRow++;
			$list_detail[] = stripslashes($detail[$m]['KdRekening']);
			$list_detail[] = stripslashes($detail[$m]['NamaRekening']);
			$list_detail[] = stripslashes($detail[$m]['Keterangan']);
			$list_detail[] = $detail[$m]['Jumlah'];
			$detail_page[] = $list_detail;
			$max_field_len = $mylib->get_max_field_len($max_field_len,$list_detail);
			if($data['panjang_per_hal']!=0){
				if(((int)$m+1) % $data['panjang_per_hal'] ==0){
					$data['detail'][] = $detail_page;
					if($m!=count($detail)-1){
						unset($detail_page);
					}
				}
			}
		}
		$data['detail'][] = $detail_page;
		$data['footer1']  = array("Jumlah Pelunasan");
		$data['footer2']  = array($header->JumlahPayment);
		$data['max_field_len'] = $max_field_len;
		$data['banyakBarang'] = $counterRow;
		return $data;
	}

    function delete_pelunasan(){
     	$id = $this->input->post('kode');
		$noreturcndn = $this->input->post('noreturcndn');
		$jumlahpayment = $this->input->post('jumlahpayment');
		$this->pelunasanhutangmodel->locktables('trans_pelunasanhutang_header,trans_pelunasanhutang_detail','bukugiro','hutang');
		$detail	= $this->pelunasanhutangmodel->getDetail($id);
		for($m=0;$m<count($detail);$m++)
		{
		   $nobukti = $detail[$m]['NoBukti'];
		   $bayar = $detail[$m]['Hutang'];
		   $this->pelunasanhutangmodel->updatehutang($nobukti,$bayar*-1);
		}
		$this->pelunasanhutangmodel->updatehutang($noreturcndn,$jumlahpayment*-1);
		$this->db->update('trans_pelunasanhutang_header', array('Status' => 'B'), array('NoDokumen' => $id));
		$this->db->update('trans_pelunasanhutang_detail', array('Status' => 'B'), array('NoDokumen' => $id));
		$this->pelunasanhutangmodel->unlocktables();
		echo $ret;   
	}

    function edit_pelunasan($id){
     	$mylib = new globallib();
		$sign  = $mylib->getAllowList("edit");
    	if($sign=="Y"){
			$id = $this->uri->segment(4);
			$data['header']	= $this->pelunasanhutangmodel->getHeader($id);
	    	$data['detail']	= $this->pelunasanhutangmodel->getDetail($id);
			$data['listjenis'] = Array ( Array ( 'Jenis' => '2', 'NamaJenis'=>'Hutang' ));
			$data['track'] = $mylib->print_track();
			$this->load->view('transaksi/pelunasanhutang/edit_pelunasanhutang', $data);
    	}
		else{
			$this->load->view('denied');
		}
    }
	
	function getkdrekening()
	{
		$kode = $this->input->post('kdrekening');
		$valreken = $this->pelunasanhutangmodel->findrekening($kode);
		if(count($valreken)!=0)
		{
			$kode = $valreken->kdrekening."*-*".$valreken->namarekening;
		}
		else
		{
			$kode = "";
		}
		echo $kode;
	}
	
	function getnobukti()
	{
		$kode = $this->input->post('nobukti');
		$valbukti = $this->pelunasanhutangmodel->findnobukti($kode);
		if(count($valbukti)!=0)
		{
			$kode = $valbukti->NoTransaksi."*-*".$valbukti->Sisa."*-*".$valbukti->Rekening."*-*".$valbukti->Nama;
		}
		else
		{
			$kode = "";
		}
		echo $kode;
	}

	function save_new_pelunasan(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('nodok');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenis');
		$kdsupplier = $this->input->post('kdsupplier');
		$noreturcndn = $this->input->post('noreturcndn');
		$tglretur = $this->input->post('tglretur');
		$jumlahretur = $this->input->post('jumlahretur');
		$sisaretur = $this->input->post('sisaretur');
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$jumlahpayment = $this->input->post('jumlahpayment');
		
		$kdrekening = $this->input->post('kdrekening');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$counter = $this->input->post('urutan');
		$itemjenis = $this->input->post('itemjenis');
		$nobukti = $this->input->post('nobukti');
		$savenobukti = $this->input->post('savenobukti');
		$hutang = $this->input->post('hutang');

		if($no=="")
		{
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$mylib->ubah_tanggal($tglretur),$jenis,$kdsupplier,$noreturcndn,$jumlahretur,$sisaretur,$ket,$user,$jumlahpayment);
		}
		else
		{
			$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$mylib->ubah_tanggal($tglretur),$jenis,$kdsupplier,$noreturcndn,$jumlahretur,$sisaretur,$ket,$user,$jumlahpayment);
		}
		for($x=0;$x<count($kdrekening);$x++)
		{
			$kdrekening1 = strtoupper(addslashes(trim($kdrekening[$x])));
			$jumlah1 = $jumlah[$x];
			$savekdrekening1 = $savekdrekening[$x];
			$keterangan1 = $keterangan[$x];
			$counter1 = $counter[$x];
			$itemjenis1 = $itemjenis[$x];
			$nobukti1 = strtoupper(addslashes(trim($nobukti[$x])));
			$savenobukti1 = $savenobukti[$x];
			$hutang1 = $hutang[$x];
			if($kdrekening1!=""){
				$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter1,$kdrekening1,$jumlah1,$keterangan1,$user,$savekdrekening1,$itemjenis1,$nobukti1,$savenobukti1,$hutang1,$noreturcndn,$kdsupplier);
			}
		}
		$this->add_new();
	}
    function save_new_item(){
		$mylib = new globallib();
		$user = $this->session->userdata('userid');
		$flag = $this->input->post('flag');
		$no = $this->input->post('no');
		$tgl = $this->input->post('tgl');
		$jenis = $this->input->post('jenis');
		$kdsupplier = $this->input->post('kdsupplier');
		$noreturcndn = $this->input->post('noreturcndn');
		$tglretur = $this->input->post('tglretur');
		$jumlahretur = $this->input->post('jumlahretur');
		$sisaretur = $this->input->post('sisaretur');
		$ket = strtoupper(addslashes($this->input->post('ket')));
		$jumlahpayment = $this->input->post('jumlahpayment');
		
		$kdrekening = strtoupper(addslashes($this->input->post('kdrekening')));
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$savekdrekening = $this->input->post('savekdrekening');
		$counter = $this->input->post('urutan');
		$itemjenis = $this->input->post('itemjenis');
		$nobukti = $this->input->post('nobukti');
		$savenobukti = $this->input->post('savenobukti');
		$hutang = $this->input->post('hutang');

		if($no=="")
		{
			$no = $this->insertNewHeader($flag,$mylib->ubah_tanggal($tgl),$mylib->ubah_tanggal($tglretur),$jenis,$kdsupplier,$noreturcndn,$jumlahretur,$sisaretur,$ket,$user,$jumlahpayment);
		}
		else
		{
			$this->updateHeader($flag,$no,$mylib->ubah_tanggal($tgl),$mylib->ubah_tanggal($tglretur),$jenis,$kdsupplier,$noreturcndn,$jumlahretur,$sisaretur,$ket,$user,$jumlahpayment);
		}
		$this->insertDetail($flag,$no,$mylib->ubah_tanggal($tgl),$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening,$itemjenis,$nobukti,$savenobukti,$hutang,$noreturcndn,$kdsupplier);
		echo $no;
	}

	function insertNewHeader($flag,$tgl,$tglretur,$jenis,$kdsupplier,$noreturcndn,$jumlahretur,$sisaretur,$ket,$user,$jumlahpayment)
	{
		$this->pelunasanhutangmodel->locktables('counter,trans_pelunasanhutang_header');
		$new_no = $this->pelunasanhutangmodel->getNewNo($tgl);
		$no = $new_no->NoLunasHutang;
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$data = array(
			'NoDokumen'	=> $no,
			'TglDokumen' => $tgl,
			'Jenis' => $jenis,
			'KdSupplier' => $kdsupplier,
			'NoBukti' => $noreturcndn,
			'JumlahRetur' => $jumlahretur,
			'SisaRetur' => $sisaretur,
			'JumlahPayment' => $jumlahpayment,
			'Keterangan' => $ket,
			'AddDate'    => $tgl1,
			'AddUser'    => $user,
			'Status' => ' '
		);
		$this->db->insert('trans_pelunasanhutang_header', $data);
		$this->pelunasanhutangmodel->unlocktables();
		return $no;
	}
	function updateHeader($flag,$no,$tgl,$tglretur,$jenis,$kdsupplier,$noreturcndn,$jumlahretur,$sisaretur,$ket,$user,$jumlahpayment)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->pelunasanhutangmodel->locktables('trans_pelunasanhutang_header');
		$data = array(
		    'TglDokumen' => $tgl,
			'Jenis' => $jenis,
			'KdSupplier' => $kdsupplier,
			'NoBukti' => $noreturcndn,
			'JumlahRetur' => $jumlahretur,
			'SisaRetur' => $sisaretur,
			'JumlahPayment' => $jumlahpayment,
			'Keterangan' => $ket,
			'Status' => ' '
		);
		if($flag=="edit")
		{
			$data['EditDate'] = $tgl1;
			$data['EditUser'] = $user;
			$this->db->update('trans_pelunasanhutang_detail', array('EditDate'=> $tgl1,'EditUser'=>$user), array('NoDokumen' => $no));
		}
		$this->db->update('trans_pelunasanhutang_header', $data, array('NoDokumen' => $no));
		$this->pelunasanhutangmodel->unlocktables();
	}
	function insertDetail($flag,$no,$tgl,$counter,$kdrekening,$jumlah,$keterangan,$user,$savekdrekening,$itemjenis,$nobukti,$savenobukti,$hutang,$noreturcndn,$kdsupplier)
	{
		$tgl1 = $this->session->userdata('Tanggal_Trans');
		$this->pelunasanhutangmodel->locktables('trans_pelunasanhutang_detail');
		if($savekdrekening==""){
			$data = array(
				'NoDokumen'	=> $no,
				'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
				'Keterangan' => $keterangan,
				'Urutan'	=> $counter,
				'Jenis' => $itemjenis,
				'NoBukti' => $nobukti,
				'KdSupplier' => $kdsupplier,
				'Hutang' => $hutang,
				'Status' => ' '
			);
			if($flag=="add")
			{
				$data['AddDate'] = $tgl1;
				$data['AddUser'] = $user;
			}
			else
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->insert('trans_pelunasanhutang_detail', $data);
			$this->pelunasanhutangmodel->updatehutang($nobukti,$jumlah);
			$this->pelunasanhutangmodel->updatehutang($noreturcndn,$jumlah);
		}
		else 
		{
		    $olddata = $this->pelunasanhutangmodel->getolddata($no,$counter);
			$oldjenis = $olddata->Jenis;
			$oldnobukti = $olddata->NoBukti; 
			$oldbayar = $olddata->Jumlah;
			$this->pelunasanhutangmodel->updatehutang($oldnobukti,$oldbayar*-1);
			$this->pelunasanhutangmodel->updatehutang($noreturcndn,$oldbayar*-1);
			$data = array(
			    'TglDokumen' => $tgl,
				'KdRekening' => $kdrekening,
				'Jumlah' => $jumlah,
				'Keterangan' => $keterangan,
				'Jenis' => $itemjenis,
				'NoBukti' => $nobukti,
				'KdSupplier' => $kdsupplier,
				'Hutang' => $hutang,
				'Status' => ' '
			);
			if($flag=="edit")
			{
				$data['EditDate'] = $tgl1;
				$data['EditUser'] = $user;
			}
			$this->db->update('trans_pelunasanhutang_detail', $data, array('NoDokumen' => $no,'Urutan'=>$counter));
			$this->pelunasanhutangmodel->updatehutang($nobukti,$jumlah);
			$this->pelunasanhutangmodel->updatehutang($noreturcndn,$jumlah);
		}
		$this->pelunasanhutangmodel->unlocktables();
	}
	function delete_item()
	{
		$id = $this->input->post('no');
		$counter = $this->input->post('urutan');
		$noreturcndn = $this->input->post('noreturcndn');
		$this->pelunasanhutangmodel->locktables('trans_pelunasanhutang_detail');
		$olddata = $this->pelunasanhutangmodel->getolddata($id,$counter);
		$oldnobukti = $olddata->NoBukti; 
		$oldbayar = $olddata->Jumlah;
		$this->pelunasanhutangmodel->updatehutang($oldnobukti,$oldbayar*-1);
		$this->pelunasanhutangmodel->updatehutang($noreturcndn,$oldbayar*-1);
		$this->db->delete('trans_pelunasanhutang_detail', array('NoDokumen' => $id,'Urutan'=>$counter));
		$this->pelunasanhutangmodel->unlocktables();
	}

}
?>